﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        void assertAllChecks()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql1=@"SELECT count(*)
FROM [Accounting_2006].[dbo].[CostCenterAccount]
  where
	[accountPID]<>(Select PID from [Accounting_2006].[dbo].Account where ID=accountID)
    or [costCenterPID]<>(Select PID from [Accounting_2006].[dbo].CostCenter where ID=costCenterID)
    or [creditAccount]<>(Select creditAccount from [Accounting_2006].[dbo].Account where ID=accountID)
    or [childAccountCount]<>(Select childCount from [Accounting_2006].[dbo].Account where ID=accountID)
    or [childCostCenterCount]<>(Select childCount from [Accounting_2006].[dbo].CostCenter where ID=costCenterID)
";
                string sql2=@"Select count(*) from [Accounting_2006].[dbo].CostCenterLeafAccount
where childCostCenterAccountID not in (Select id from [Accounting_2006].[dbo].CostCenterAccount)
or parentCostCenterAccountID not in (Select id from [Accounting_2006].[dbo].CostCenterAccount)
";
                string sql3 = @"Select count(*) from [Accounting_2006].[dbo].CostCenterLeafAccount
where childCostCenterAccountID not in 
(Select id from [Accounting_2006].[dbo].CostCenterAccount where [childCostCenterCount]=0 and [childAccountCount]=0)
";
                string sql4 = @"Select count(*) from [Accounting_2006].[dbo].CostCenterLeafAccount
where childCostCenterAccountID in 
(Select id from [Accounting_2006].[dbo].CostCenterAccount where [childCostCenterCount]<>0 or [childAccountCount]<>0)
";
                string sql5 = @"Select count(*) from [Accounting_2006].[dbo].CostCenterLeafAccount
where parentCostCenterAccountID not in 
(Select id from [Accounting_2006].[dbo].CostCenterAccount where [childCostCenterCount]<>0 or [childAccountCount]<>0)
";
                string sql6 = @"Select count(*) from [Accounting_2006].[dbo].CostCenterLeafAccount
where parentCostCenterAccountID  in 
(Select id from [Accounting_2006].[dbo].CostCenterAccount where [childCostCenterCount]=0 and [childAccountCount]=0)";
                string[][] checks=new string[][]{
                    new string[]{"CostCenterAccount table redundent fields are invalid",sql1}
                    ,new string[]{"CostCenterLeafAccount table contains invalid CostCenterAccount references",sql2}
                    ,new string[]{"CostCenterLeafAccount contains childCostCenterAccountID that are not child cost center account",sql3}
                    ,new string[]{"CostCenterLeafAccount contains childCostCenterAccountID that are parent cost center account",sql4}
                    ,new string[]{"CostCenterLeafAccount contains parentCostCenterAccountID that are not parent cost center accounts",sql5}
                    ,new string[]{"CostCenterLeafAccount contains parentCostCenterAccountID that are child cost center accounts",sql6}
                };
                string err="";
                for(int i=0;i<checks.Length;i++)
                {
                    if((int)reader.ExecuteScalar(checks[i][1])>0)
                    {
                        err = INTAPS.StringExtensions.AppendOperand(err, "\n", checks[i][0]);
                    }
                }
                if (!string.IsNullOrEmpty(err))
                    throw new ServerUserMessage(err);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        void assertNewEntryToCostCenterLeafAccount(int csaID, CostCenterAccount parentCsa)
        {
            return;
            int cnt = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CostCenterLeafAccount where childCostCenterAccountID={1}", this.DBName, parentCsa.id));
            if (cnt > 0)
                throw new ServerUserMessage("BUG:" + GetCostCenterAccountWithDescription( parentCsa.id).code + " has an entry in CostCenterLeafAccount as a child");
            cnt = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CostCenterLeafAccount where parentCostCenterAccountID={1}", this.DBName, csaID));
            if (cnt > 0)
                throw new ServerUserMessage("BUG:" + GetCostCenterAccountWithDescription( csaID).code + " has an entry in CostCenterLeafAccount as a parent");
            if (!parentCsa.isControlAccount)
                throw new ServerUserMessage(parentCsa.id + " is not a control account");
        }

        void assertCostCenterLeafAccountNoParentChild()
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                int c = (int)helper.ExecuteScalar(string.Format(
@"Select COUNT(*) from {0}.dbo.CostCenterLeafAccount 
where childCostCenterAccountID in (Select parentCostCenterAccountID from {0}.dbo.CostCenterLeafAccount)", this.DBName));
                if (c > 0)
                    throw new ServerUserMessage("BUG: Cost center leaf account table has none leaf entries");
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        void assertCostCenterAccountChildCountFields()
        {
            return;
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                int c = (int)helper.ExecuteScalar(string.Format(
@"Select 
count(*)
 from {0}.dbo.CostCenterAccount
csa inner join {0}.dbo.Account ac
on csa.accountID=ac.id
inner join {0}.dbo.CostCenter cs
on csa.costCenterID=cs.id
where csa.childCostCenterCount<>cs.childCount
or csa.childAccountCount<>ac.childCount
", this.DBName));
                if (c > 0)
                    throw new ServerUserMessage("BUG: CostCenterAccount table has wrong child count fields.");
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        
        void BlockWithTransaction<AccountType>(int acountID, string err) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {

                if (CountTransactionByFilter<AccountType>(dspReader, typeof(AccountType).Name + ".ID=" + acountID) > 0
                    )
                    throw new ServerUserMessage(err);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }

        }
        void BlockWithTransaction(int csaID, string err)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {

                if (CountTransactionByFilter(dspReader, "accountID=" + csaID) > 0
                    )
                    throw new ServerUserMessage(err);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }

        }
        void BlockControllAccount<AccountType>(int accountID, string err) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string sql = "Select count(*) from {0}.dbo.{1} where PID={2}";
                sql = string.Format(sql, this.DBName, typeof(AccountType).Name, accountID);
                if (((int)dspReader.ExecuteScalar(sql) > 0)
                    )
                    throw new ServerUserMessage(err);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void verifyAccountData<AccountType>(AccountType ac, ref object dateCreation, ref object dateActivation, ref object dateDeactivation) where AccountType : AccountBase, new()
        {
            if (AccountingBDE.IsFutureDate(ac.CreationDate))
                ac.CreationDate = DateTime.Now; 
                //throw new ServerUserMessage("Creation date can't be in the future.");
            if (!AccountingBDE.IsWithinValidRange(ac.CreationDate))
                throw new ServerUserMessage("Invalid creation date.");
            dateCreation = ac.CreationDate;
            if (ac.Status != AccountStatus.Pending)
            {
                if (AccountingBDE.IsFutureDate(ac.ActivateDate))
                    ac.ActivateDate = DateTime.Now;
                    //throw new ServerUserMessage("Activation date can't be in the future.");
                if (!AccountingBDE.IsWithinValidRange(ac.ActivateDate))
                    throw new ServerUserMessage("Invalid activation date.");
                if (ac.ActivateDate < ac.CreationDate)
                    throw new ServerUserMessage("Activation date can't be before creation date.");
                dateActivation = ac.ActivateDate;
            }
            if (ac.Status == AccountStatus.Deactivated)
            {
                if (AccountingBDE.IsFutureDate(ac.DeactivateDate))
                    throw new ServerUserMessage("Deactivation date can't be in the future.");
                if (!AccountingBDE.IsWithinValidRange(ac.DeactivateDate))
                    throw new ServerUserMessage("Invalid deactivation date.");
                dateDeactivation = ac.DeactivateDate;
            }
            if (IsCodeUsed<AccountType>(dspWriter, ac.Code, ac.id))
                throw new ServerUserMessage(AccountBase.GetAccountTypeDescription<AccountType>(true) + " code " + ac.Code + " allready used.");
            if (ac.Code.IndexOf(':') != -1)
                throw new ServerUserMessage("The symbol ':' can't be used in account/cost center codes");
            if (ac.Name.IndexOf(':') != -1)
                throw new ServerUserMessage("The symbol ':' can't be used in account/cost center names");
        }

        void logOldAccountData<AccountType>(int AID, int accountID) where AccountType : AccountBase, new()
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(AccountType).Name, "ID=" + accountID);
        }
        void logOldCostCenterAccount(int AID, int csAccountID)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(CostCenterAccount).Name, "ID=" + csAccountID);
        }
        public class CostCenterAccountCashe
        {
            AccountingBDE bde;
            public Dictionary<int, Dictionary<int, CostCenterAccount>> costCenterAccountCache1 = new Dictionary<int, Dictionary<int, CostCenterAccount>>();
            public Dictionary<int,CostCenterAccount> costCenterAccountCache2=new Dictionary<int,CostCenterAccount>();
            public Dictionary<int, Account> accountCache = new Dictionary<int, Account>();
            public Dictionary<int, CostCenter> costCenterCache = new Dictionary<int, CostCenter>();
            public bool fullyCashed = false;
            public CostCenterAccountCashe(AccountingBDE bde)
                : this(bde, false)
            {
            }
            public CostCenterAccountCashe(AccountingBDE bde,bool buildFullCashe)
            {
                this.bde = bde;
                if (buildFullCashe)
                {
                    SQLHelper reader= bde.GetReaderHelper();
                    try
                    {
                        Console.WriteLine("Loading cost center table");
                        CostCenterAccount[] all = reader.GetSTRArrayByFilter<CostCenterAccount>(null);
                        foreach (CostCenterAccount csa in all)
                        {
                            costCenterAccountCache2.Add(csa.id, csa);
                            if (costCenterAccountCache1.ContainsKey(csa.costCenterID))
                            {
                                Dictionary<int, CostCenterAccount> costCenterCashe = costCenterAccountCache1[csa.costCenterID];
                                costCenterCashe.Add(csa.accountID, csa);
                            }
                            else
                            {
                                Dictionary<int, CostCenterAccount> costCenterCashe = new Dictionary<int, CostCenterAccount>();
                                costCenterCashe.Add(csa.accountID, csa);
                                costCenterAccountCache1.Add(csa.costCenterID, costCenterCashe);
                            }
                        }
                        foreach (Account account in reader.GetSTRArrayByFilter<Account>(null))
                            accountCache.Add(account.id, account);
                        foreach (CostCenter costcenter in reader.GetSTRArrayByFilter<CostCenter>(null))
                            costCenterCache.Add(costcenter.id, costcenter);
                        this.fullyCashed = true;
                    }
                    finally
                    {
                        bde.ReleaseHelper(reader);
                    }
                }
            }
            public int[] ExpandParents<AccountType>(int PID) where AccountType : AccountBase, new()
            {
                List<int> ps = new List<int>();
                while (PID != -1)
                {
                    ps.Add(PID);
                    PID = GetAccount<AccountType>(PID).PID;
                }
                return ps.ToArray();
            }
            public Account GetAccount(int AccountID)
            {
                if (accountCache.ContainsKey(AccountID))
                    return accountCache[AccountID];
                if (fullyCashed)
                    return null;
                Account account = bde.GetAccount<Account>(AccountID);
                if (account == null)
                    return null;
                accountCache.Add(account.id, account);
                return account;
            }
            public CostCenter GetCostCenter(int AccountID)
            {
                if (costCenterCache.ContainsKey(AccountID))
                    return costCenterCache[AccountID];
                if (fullyCashed)
                    return null;
                CostCenter costCenter = bde.GetAccount<CostCenter>(AccountID);
                if (costCenter == null)
                    return null;
                costCenterCache.Add(costCenter.id, costCenter);
                return costCenter;
            }
            public AccountType GetAccount<AccountType>(int AccountID) where AccountType : AccountBase, new()
            {
                if(typeof(AccountType) == typeof(Account))
                    return GetAccount(AccountID) as AccountType;
                return GetCostCenter(AccountID) as AccountType;
            }
            public CostCenterAccount getCostCenterAccount(int costCenterID, int accountID)
            {
                if (costCenterAccountCache1.ContainsKey(costCenterID))
                {
                    Dictionary<int, CostCenterAccount> costCenterCashe = costCenterAccountCache1[costCenterID];
                    if (costCenterCashe.ContainsKey(accountID))
                        return costCenterCashe[accountID];
                    if (fullyCashed)
                        return null;
                    CostCenterAccount csa = bde.GetCostCenterAccount(costCenterID, accountID);
                    if (csa == null)
                        return null;
                    costCenterCashe.Add(accountID, csa);
                    costCenterAccountCache2.Add(csa.id, csa);
                    return csa;
                }
                else
                {
                    if (fullyCashed)
                        return null;
                    CostCenterAccount csa = bde.GetCostCenterAccount(costCenterID, accountID);
                    if (csa == null)
                        return null;
                    Dictionary<int, CostCenterAccount> costCenterCashe = new Dictionary<int, CostCenterAccount>();
                    costCenterCashe.Add(accountID, csa);
                    costCenterAccountCache1.Add(costCenterID, costCenterCashe);
                    costCenterAccountCache2.Add(csa.id, csa);
                    return csa;
                }
            }
            public CostCenterAccount getCostCenterAccount(int csaID)
            {
                if (costCenterAccountCache2.ContainsKey(csaID))
                    return costCenterAccountCache2[csaID];
                if (fullyCashed)
                    return null;
                CostCenterAccount csa = bde.GetCostCenterAccount(csaID);
                costCenterAccountCache2.Add(csa.id, csa);
                if (costCenterAccountCache1.ContainsKey(csa.costCenterID))
                {
                    Dictionary<int, CostCenterAccount> costCenterCashe = costCenterAccountCache1[csa.costCenterID];
                    costCenterCashe.Add(csa.accountID, csa);
                }
                else
                {
                    Dictionary<int, CostCenterAccount> costCenterCashe = new Dictionary<int, CostCenterAccount>();
                    costCenterCashe.Add(csa.accountID, csa);
                    costCenterAccountCache1.Add(csa.costCenterID, costCenterCashe);
                }
                return csa;
            }
        }
        public void rebuildLeaveEntries(CostCenterAccountCashe cache, int csaID,bool fastMode)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    List<CostCenterAccount> allParents = new List<CostCenterAccount>();
                    CostCenterAccount costCenterAccount = cache.getCostCenterAccount(csaID);
                    if (!fastMode)
                        dspWriter.Delete(this.DBName, "CostCenterLeafAccount", "childCostCenterAccountID=" + csaID);
                    if (!costCenterAccount.isControlAccount)
                    {
                        //int[] costCenterParents = ExpandParents<CostCenter>(dspWriter, costCenterAccount.costCenterID);
                        //int[] accountParents = ExpandParents<Account>(dspWriter, costCenterAccount.accountID);
                        int[] costCenterParents =cache.ExpandParents<CostCenter>(costCenterAccount.costCenterID);
                        int[] accountParents =cache.ExpandParents<Account>(costCenterAccount.accountID);
                        for (int i = 0; i < accountParents.Length; i++)
                            for (int j = 0; j < costCenterParents.Length; j++)
                            {
                                if (i == 0 && j == 0)
                                    continue;
                                CostCenterAccount aparent = cache.getCostCenterAccount(costCenterParents[j], accountParents[i]);
                                if (aparent != null)
                                    allParents.Add(aparent);
                            }
                        foreach (CostCenterAccount csa in allParents)
                        {
                            if (!fastMode)
                                assertNewEntryToCostCenterLeafAccount(csaID, csa);
                            dspWriter.Insert(this.DBName, "CostCenterLeafAccount", new string[] { "childCostCenterAccountID", "parentCostCenterAccountID", "accountID", "costCenterID" }
                                , new object[] { csaID, csa.id, csa.accountID, csa.costCenterID });
                        }
                    }
                    if (!fastMode)
                        assertCostCenterLeafAccountNoParentChild();
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }



        //updates parent child relationships for link methods
        void GenericLink<AccountType>(int AID, int Parent, int Child, out AccountType childAccount, out AccountType parentAccount)
            where AccountType : AccountBase, new()
        {

            childAccount = GetAccount<AccountType>(Child);
            parentAccount = null;
            if (Parent != -1)
            {
                parentAccount = GetAccount<AccountType>(Parent);
                if (parentAccount == null)
                    throw new ServerUserMessage(AccountBase.GetAccountTypeDescription<AccountType>(true) + " not in the database ID:" + parentAccount.id);
            }
            changeParent<AccountType>(AID, childAccount.id, Parent);
            return;
            //if (childAccount.childCount == 0 && parentAccount != null)
            //{
            //    changeParent<AccountType>(AID, childAccount.id, Parent);
            //    return;
            //}
            //if (Parent != -1)
            //{
            //    BlockWithCostCenterAccount<AccountType>(Parent, "Parent item are already used.");
            //    BlockWithTransaction<AccountType>(Parent, "Parent item has already transactions");
            //}
            //BlockWithCostCenterAccount<AccountType>(Child, "Child item are already used.");
            //BlockWithTransaction<AccountType>(Child, "Child item has already transactions");
            //lock (dspWriter)
            //{
                
            //    if (childAccount.PID == Parent)
            //        throw new ServerUserMessage("Already linked");
            //    if (Parent != -1)
            //        BlockWithTransaction<AccountType>(Parent, AccountBase.GetAccountTypeDescription<AccountType>(true) + " can't become parent as it allready have transactions.");
            //    if (Parent == Child)
            //        throw new ServerUserMessage("Parent and child are the same.");
            //    dspWriter.setReadDB(this.DBName);
            //    dspWriter.BeginTransaction();
            //    try
            //    {
            //        childAccount.PID = Parent;
            //        updateAccountRaw<AccountType>(AID, childAccount);
            //        if (Parent != -1)
            //        {
            //            int ccount = getDirectChildCount<AccountType>(dspWriter, parentAccount);
            //            parentAccount.childCount = ccount;
            //            updateAccountRaw<AccountType>(AID, parentAccount);
            //        }
            //        dspWriter.CommitTransaction();
            //    }
            //    catch
            //    {
            //        dspWriter.RollBackTransaction();
            //        throw;
            //    }
            //    finally
            //    {
            //        dspWriter.restoreReadDB();
            //    }
            //}
        }

        private void BlockWithCostCenterAccount<AccountType>(int accountID, string message)
            where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                if ((int)reader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CostCenterAccount where {1}={2}", this.DBName
                    , AccountBase.GetCostCenterAccountIDField<AccountType>(), accountID)) > 0)
                    throw new ServerUserMessage(message);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        private void updateCostCenterAccountRaw(int AID, CostCenterAccount csa, bool filter,bool rebuildLeaves)
        {
            if (filter)
                INTAPS.ClientServer.ObjectFilters.filterUpdate(AID, csa);
            logOldCostCenterAccount(AID, csa.id);
            dspWriter.UpdateSingleTableRecord(this.DBName, csa, new string[] { "__AID" }, new object[] { AID });
            if (csa.isControlAccount)
            {
                dspWriter.Delete(this.DBName, "CostCenterLeafAccount", "childCostCenterAccountID=" + csa.id);
                assertCostCenterLeafAccountNoParentChild();
            }
            else if (rebuildLeaves)
                rebuildLeaveEntries(new CostCenterAccountCashe(this), csa.id, false);
        }
        //integrity test
        void testChildCountIntegirty<AccountType>(INTAPS.RDBMS.SQLHelper helper, int accountID) where AccountType : AccountBase, new()
        {
            AccountType ac = GetAccountInternal<AccountType>(helper, accountID);
            if (ac == null)
                throw new ServerUserMessage(AccountBase.GetAccountTypeDescription<AccountType>(true) + " ID not found in database:" + accountID);
            if (ac.childCount != getAccountChildCountInternal<AccountType>(helper, accountID))
                throw new ServerUserMessage("Child count not properly set for " + AccountBase.GetCostCenterAccountIDField<AccountType>() + ":" + accountID);
        }
        void testAccountStructureIntegirty<AccountType>(INTAPS.RDBMS.SQLHelper helper, int accountID)
        where AccountType : AccountBase, new()
        {
            if (accountID == -1)
                return;
            testChildCountIntegirty<AccountType>(helper, accountID);
        }
        public int createCostCenterAccountID(Dictionary<int, Dictionary<int, int>> reuseID,int costCenterID,int accountID)
        {
            if (reuseID != null)
            {
                if (reuseID.ContainsKey(costCenterID))
                {
                    Dictionary<int, int> r = reuseID[costCenterID];
                    if (r.ContainsKey(accountID))
                        return r[accountID];
                }
            }
            return INTAPS.ClientServer.AutoIncrement.GetKey("Accounting.CostCenterAccountID");
        }
        public int CreateAccount<AccountType>(int AID, AccountType ac) where AccountType : AccountBase, new()
        {
            return CreateAccount<AccountType>(AID, null, ac,false,true);
        }
        public int CreateAccount<AccountType>(int AID, AccountType ac, bool overrideCreditAccount, bool rebuildLeaves) where AccountType : AccountBase, new()
        {
            return CreateAccount<AccountType>(AID, null, ac, overrideCreditAccount, rebuildLeaves);
        }
        int CreateAccount<AccountType>(int AID, Dictionary<int, Dictionary<int, int>> reuseCSAID, AccountType ac, bool overrideCreditAccount,bool rebuildLeaves) where AccountType : AccountBase, new()
        {
            INTAPS.ClientServer.ObjectFilters.filterAdd(AID, ac);

            lock (dspWriter)
            {
                dspWriter.setReadDB(m_DBName);
                dspWriter.BeginTransaction();
                try
                {
                    AccountType parent = null;
                    if (ac.PID != -1)
                    {
                        BlockWithTransaction<AccountType>(ac.PID, "This will make the parent account a control account but as the parent account allready have transactions it can't be made a controll account.");
                        parent = GetAccount<AccountType>(ac.PID);
                        if (parent == null)
                            throw new ServerUserMessage("Invalid parent ID:" + ac.PID);
                    }
                    object dateCreation = DBNull.Value;
                    object dateActivation = DBNull.Value;
                    object dateDeactivation = DBNull.Value;
                    verifyAccountData<AccountType>(ac, ref dateCreation, ref dateActivation, ref dateDeactivation);
                    if(ac.id<1)
                        ac.id = INTAPS.ClientServer.AutoIncrement.GetKey("Accounting." + AccountBase.GetCostCenterAccountIDField<AccountType>());
                    ac.childCount = 0;

                    //transfer credit account setting from parent
                    if (!overrideCreditAccount)
                    {
                        if (ac.PID != -1 && typeof(AccountType) == typeof(Account))
                        {
                            (ac as Account).CreditAccount = (parent as Account).CreditAccount;
                        }
                    }
                    dspWriter.InsertSingleTableRecord(m_DBName, ac, new string[] { "__AID" }, new object[] { AID });

                    //fix parent child count
                    if (ac.PID != -1)
                    {
                        //UpdateAccount<AccountType>(AID, parent);
                        fixChildCount<AccountType>(AID, parent.id,rebuildLeaves);
                    }

                    if (typeof(AccountType) == typeof(Account))
                    {
                        Account account = ac as Account;
                        if (account.PID == -1)
                            CreateCostCenterAccount(AID, CostCenter.ROOT_COST_CENTER, ac.id, rebuildLeaves);
                        else
                            transferCostCentersFromParent(AID,reuseCSAID, ac.PID, ac.id,rebuildLeaves);
                    }

                    testAccountStructureIntegirty<AccountType>(dspWriter, ac.id);
                    testAccountStructureIntegirty<AccountType>(dspWriter, ac.PID);
                    dspWriter.CommitTransaction();
                    return ac.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        private void fixChildCount<AccountType>(int AID, int parentAccountID,bool rebuildLeaves) where AccountType : AccountBase, new()
        {
            string sql = string.Format("Select count(*) from {0}.dbo.{1} where PID={2}", this.DBName,
                typeof(AccountType).Name, parentAccountID);
            int newChildCount = (int)dspWriter.ExecuteScalar(sql);
            Dictionary<int, bool> isCC = new Dictionary<int, bool>();
            foreach (CostCenterAccount csa in GetCostCenterAccountsOf<AccountType>(parentAccountID))
            {
                isCC.Add(csa.id,csa.isControlAccount);
            }
            sql = string.Format("Update {0}.dbo.{1} set childCount={2} where id={3}"
                , this.DBName, typeof(AccountType).Name, newChildCount, parentAccountID);
            dspWriter.ExecuteNonQuery(sql);
            sql = string.Format("Update {0}.dbo.CostCenterAccount set child{1}Count={2} where {3}={4}"
                , this.DBName, typeof(AccountType).Name, newChildCount, AccountBase.GetCostCenterAccountIDField<AccountType>(), parentAccountID);
            dspWriter.ExecuteNonQuery(sql);

            foreach (CostCenterAccount csa in GetCostCenterAccountsOf<AccountType>(parentAccountID))
            {
                if (csa.isControlAccount == isCC[csa.id])
                    continue;
                if (csa.isControlAccount)
                {
                    sql = string.Format("Delete from {0}.dbo.CostCenterLeafAccount where childCostCenterAccountID={1}", this.DBName, csa.id);
                    dspWriter.ExecuteNonQuery(sql);
                }
                else if(rebuildLeaves)
                    rebuildLeaveEntries(new CostCenterAccountCashe(this), csa.id, false);
            }
        }
        private void updateAccountRaw<AccountType>(int AID, AccountType ac) where AccountType : AccountBase, new()
        {
            INTAPS.ClientServer.ObjectFilters.filterUpdate(AID, ac);
            logOldAccountData<AccountType>(AID, ac.id);
            dspWriter.UpdateSingleTableRecord(m_DBName, ac, new string[] { "__AID" }, new object[] { AID });
        }

        public void UpdateAccount<AccountType>(int AID, AccountType ac) where AccountType : AccountBase, new()
        {
            UpdateAccount<AccountType>(AID, ac,true);
        }
        public void UpdateAccount<AccountType>(int AID, AccountType ac,bool rebuildLeaves) where AccountType : AccountBase, new()
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    AccountType oldac = GetAccount<AccountType>(ac.id);
                    ac.Status = oldac.Status;
                    ac.childCount = getDirectChildCount<AccountType>(dspWriter, ac);
                    if (ac.PID != oldac.PID)
                    {
                        throw new ServerUserMessage("You can't change parent using the update method");
                    }
                    if (typeof(AccountType) == typeof(Account))
                    {
                        Account parent = GetAccount<Account>(ac.PID);
                        if(parent!=null)
                            (ac as Account).CreditAccount = parent.CreditAccount;

                    }
                    object dateCreation = DBNull.Value;
                    object dateActivation = DBNull.Value;
                    object dateDeactivation = DBNull.Value;
                    verifyAccountData<AccountType>(ac, ref dateCreation, ref dateActivation, ref dateDeactivation);
                    updateAccountRaw<AccountType>(AID, ac);

                    if (typeof(AccountType) == typeof(Account))
                    {
                        CostCenterAccount[] csas = GetCostCenterAccountsOfInternal<Account>(dspWriter, ac.id);
                        bool toomany = csas.Length > 1000;
                        int c = csas.Length;
                        if (toomany)
                        {
                            Console.WriteLine();
                        }

                        foreach (CostCenterAccount csa in csas)
                        {
                            if (toomany)
                            {
                                //Console.CursorTop--;
                                Console.WriteLine((c--).ToString() + "                        ");
                            }

                            csa.creditAccount = ((Account)(object)ac).CreditAccount;
                            csa.childAccountCount = ((Account)(object)ac).childCount;
                            updateCostCenterAccountRaw(AID, csa, false,rebuildLeaves);
                        }
                    }
                    else
                    {
                        CostCenterAccount[] csas = GetCostCenterAccountsOfInternal<CostCenter>(dspWriter, ac.id);
                        bool toomany = csas.Length > 1000;
                        int c = csas.Length;
                        if (toomany)
                        {
                            Console.WriteLine();
                        }
                        foreach (CostCenterAccount csa in csas)
                        {
                            if (toomany)
                            {
                                //Console.CursorTop--;
                                Console.WriteLine((c--).ToString() + "                        ");
                            }
                            csa.childCostCenterCount = ((CostCenter)(object)ac).childCount;
                            updateCostCenterAccountRaw(AID, csa, false,rebuildLeaves);
                        }
                    }

                    testAccountStructureIntegirty<AccountType>(dspWriter, ac.id);
                    testAccountStructureIntegirty<AccountType>(dspWriter, ac.PID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }


        public void DeleteAccount<AccountType>(int AID, int accountID, bool deleteComplete) where AccountType : AccountBase, new()
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if (isControlAccount<AccountType>(accountID))
                    {
                        int N;
                        foreach (AccountType ch in GetChildAcccount<AccountType>(accountID, 0, -1, out N))
                        {
                            DeleteAccount<AccountType>(AID, ch.id, deleteComplete);
                        }
                    }
                    if (deleteComplete)
                        BlockWithTransaction<AccountType>(accountID, "It is not allowed to delete " + AccountBase.GetAccountTypeDescription<AccountType>(false) + " with transactions. ID:{0}".format(accountID));

                    INTAPS.ClientServer.ObjectFilters.filterDelete(AID, typeof(AccountType), accountID);


                    AccountType ac = GetAccount<AccountType>(accountID);
                    if (ac == null)
                        throw new ServerUserMessage("Account/accounting center ID:" + accountID + " not found in the database");
                    dspWriter.Delete(m_DBName, "AccountBalance", "AccountID=" + accountID);
                    logOldAccountData<AccountType>(AID, accountID);

                    CostCenterAccount[] costCenterAccounts;
                    costCenterAccounts = GetCostCenterAccountsOfInternal<AccountType>(dspWriter, accountID);
                    foreach (CostCenterAccount csa in costCenterAccounts)
                    {
                        deleteCostCenterAccountRaw(AID, csa.id,deleteComplete);
                    }

                    dspWriter.DeleteSingleTableRecrod<AccountType>(m_DBName, new object[] { accountID });
                    //fix parent child count
                    if (ac.PID != -1)
                    {
                        AccountType parent = GetAccount<AccountType>(ac.PID);
                        if (parent == null)
                            throw new ServerUserMessage("Invalid parent ID:" + ac.PID);
                        fixChildCount<AccountType>(AID, parent.id,true);
                    }

                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }


        // BDE exposed
        public void DeleteChildAccounts<AccountType>(int AID, int accountID) where AccountType : AccountBase, new()
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    int N;
                    this.PushProgress("Retrieving child accounts");
                    AccountType[] chs=GetChildAcccount<AccountType>(accountID,0,-1,out N);
                    this.PopProgress();
                    this.PushProgress("Deleteing accounts");
                    int i=0;
                    foreach(AccountType ch in chs)
                    {
                        i++;
                        this.SetProgress(i + " of " + N, (double)i / (double)N);
                        DeleteAccount<AccountType>(AID, ch.id, true);
                        
                    }
                    
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                    this.PopProgress();
                }
            }
        }

        public void ActivateCostCenterAccount(int AID, int csAccountID, DateTime ActivateDate)
        {
            CostCenterAccount csa = GetCostCenterAccount(csAccountID);
            if (csa.status == AccountStatus.Activated)
                throw new ServerUserMessage("Account already active.");
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    if (csa.status == AccountStatus.Deactivated)
                        if (csa.deactivateDate > ActivateDate)
                            throw new ServerUserMessage("Activation date can't be the date the acccount is deactivated.");
                    if (AccountingBDE.IsFutureDate(ActivateDate))
                        throw new ServerUserMessage("Activation date can't be in the future.");
                    if (!AccountingBDE.IsWithinValidRange(ActivateDate))
                        throw new ServerUserMessage("Invalid activation date.");
                    CostCenter costCenter = GetAccount<CostCenter>(csa.costCenterID);
                    Account account = GetAccount<Account>(csa.accountID);
                    if (costCenter.Status != AccountStatus.Activated || account.Status != AccountStatus.Activated)
                        throw new ServerUserMessage("The related account and/or cost center is not activated");
                    try
                    {
                        dspWriter.BeginTransaction();
                        csa.status = AccountStatus.Activated;
                        csa.activateDate = ActivateDate;
                        updateCostCenterAccountRaw(AID, csa, true,true);
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }

            }
        }
        public void DectivateCostCenterAccount(int AID, int csAccountID, DateTime deactivationDate)
        {
            CostCenterAccount csa = GetCostCenterAccount(csAccountID);
            if (csa.status != AccountStatus.Activated)
                throw new ServerUserMessage("Account active.");
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    if (AccountingBDE.IsFutureDate(deactivationDate))
                        throw new ServerUserMessage("Activation date can't be in the future.");
                    if (!AccountingBDE.IsWithinValidRange(deactivationDate))
                        throw new ServerUserMessage("Invalid activation date.");
                    if (csa.activateDate > deactivationDate)
                        throw new ServerUserMessage("Dectivation date can't be before activation date.");
                    CostCenter costCenter = GetAccount<CostCenter>(csa.costCenterID);
                    Account account = GetAccount<Account>(csa.accountID);
                    try
                    {
                        dspWriter.BeginTransaction();
                        csa.status = AccountStatus.Deactivated;
                        csa.deactivateDate = deactivationDate;
                        updateCostCenterAccountRaw(AID, csa, true,true);
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }

            }
        }
        public void ActivateAcount<AccountType>(int AID, int AccountID, DateTime ActivateDate) where AccountType : AccountBase, new()
        {
            AccountType ac = GetAccount<AccountType>(AccountID);
            if (ac.Status == AccountStatus.Activated)
                throw new ServerUserMessage("Account already active.");
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    if (AccountingBDE.IsFutureDate(ActivateDate))
                        throw new ServerUserMessage("Activation date can't be in the future.");
                    if (!AccountingBDE.IsWithinValidRange(ActivateDate))
                        throw new ServerUserMessage("Invalid activation date.");
                    if (ActivateDate < ac.CreationDate)
                        throw new ServerUserMessage("Activation date can't be before creation date.");
                    dspWriter.BeginTransaction();
                    try
                    {
                        ac.Status = AccountStatus.Activated;
                        ac.ActivateDate = ActivateDate;
                        updateAccountRaw<AccountType>(AID, ac);
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }

            }
        }
        public void DeactivateAccount<AccountType>(int AID, int AccountID, DateTime DeactivateDate) where AccountType : AccountBase, new()
        {
            AccountType ac = GetAccount<AccountType>(AccountID);
            //if (ac.Status != AccountStatus.Activated)
            //    throw new ServerUserMessage("Account inactive.");
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    if (AccountingBDE.IsFutureDate(DeactivateDate))
                        throw new ServerUserMessage("Activation date can't be in the future.");
                    if (!AccountingBDE.IsWithinValidRange(DeactivateDate))
                        throw new ServerUserMessage("Invalid activation date.");
                    dspWriter.BeginTransaction();
                    try
                    {
                        ac.Status = AccountStatus.Deactivated;
                        ac.DeactivateDate = DeactivateDate;
                        updateAccountRaw<AccountType>(AID, ac);
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void LinkAccount(int AID, int Parent, int Child)
        {
            Account childAccount, parentAccount;
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    GenericLink<Account>(AID, Parent, Child, out childAccount, out parentAccount);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        bool costCenterHasFullAccountHirarchy(int costCenterID, int accountID, int exceptAccountID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                int childAccountCount = (int)reader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.Account where PID={1} and ID<>{2}", this.DBName, accountID, exceptAccountID));
                int costCenterAccount = (int)reader.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CostCenterAccount where accountPID={1} and costCenterID={2} and accountID<>{3}", this.DBName, accountID, costCenterID, exceptAccountID));
                if (costCenterAccount > childAccountCount)
                    throw new ServerUserMessage(string.Format("BUG: there are more cost centers created using the childs of account {0} for cost center {1} than there are child accounts", accountID, costCenterID));
                return childAccountCount == costCenterAccount;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        private void transferCostCentersFromParent(int AID, Dictionary<int, Dictionary<int, int>> reuseID, int Parent, int Child, bool rebuildLeaves)
        {
            CostCenterAccount[] parentCostCenterAccounts = dspWriter.GetSTRArrayByFilter<CostCenterAccount>("accountID=" + Parent);
            foreach (CostCenterAccount csa in parentCostCenterAccounts)
            {
                if (costCenterHasFullAccountHirarchy(csa.costCenterID, Parent, Child))
                    if (GetCostCenterAccountInternal(dspWriter, csa.costCenterID, Child) == null)
                        CreateCostCenterAccount(AID, reuseID, csa.costCenterID, Child, rebuildLeaves);
            }
        }
        public void LinkCostCenter(int AID, int Parent, int Child)
        {
            CostCenter childCostCenter, parentCostCenter;
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    GenericLink<CostCenter>(AID, Parent, Child, out childCostCenter, out parentCostCenter);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public int CreateCostCenterAccount(int AID, int costCenterID, int accountID, bool rebuildLeaves)
        {
            return CreateCostCenterAccount(AID, null, costCenterID, accountID, rebuildLeaves);
        }
        public int CreateCostCenterAccount(int AID, int costCenterID, int accountID)
        {
            return CreateCostCenterAccount(AID, null, costCenterID, accountID,true);
        }
        int CreateCostCenterAccount(int AID, Dictionary<int, Dictionary<int, int>> id, int costCenterID, int accountID, bool rebuildLeaves)
        {
            Account ac = GetAccount<Account>(accountID);
            CostCenter cs = GetAccount<CostCenter>(costCenterID);
            if (ac == null)
                throw new ServerUserMessage("Invalid template account. AccountID:"+accountID);

            if (cs == null)
                throw new ServerUserMessage("Invalid accounting center");

            int ret = -1;
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    CostCenter parentCostCenter = cs;
                    while (parentCostCenter.PID != -1)
                    {
                        if (GetCostCenterAccount(parentCostCenter.PID, accountID) == null)
                            CreateCostCenterAccount(AID,id, parentCostCenter.PID, accountID,rebuildLeaves);
                        parentCostCenter = GetAccount<CostCenter>(parentCostCenter.PID);
                    }
                    CostCenterAccount csa;

                    //create cost center account for the account and all its childs accounts at once
                    List<int> childAccounts = new List<int>();
                    childAccounts.Add(ac.id);
                    GetAllChildAccount<Account>(dspWriter, ac.id, childAccounts);
                    List<int> created = new List<int>();
                    bool tooManyChilds = childAccounts.Count > 500;
                    int c = childAccounts.Count;
                    if (tooManyChilds)
                    {
                        Console.WriteLine(childAccounts.Count + " child acounts found");
                    }
                    foreach (int aid in childAccounts)
                    {
                        if (tooManyChilds)
                        {
                            //Console.CursorTop--;
                            Console.WriteLine(c + "                         ");
                            c--;
                        }

                        if ((csa = GetCostCenterAccountInternal(dspWriter, costCenterID, aid)) == null)
                        {
                            Account anAccount = GetAccount<Account>(aid);
                            csa = new CostCenterAccount();
                            
                            

                            csa.accountID = anAccount.id;
                            csa.accountPID = anAccount.PID;
                            csa.costCenterID = cs.id;
                            csa.costCenterPID = cs.PID;
                            csa.creditAccount = anAccount.CreditAccount;
                            csa.childAccountCount = anAccount.childCount;
                            csa.childCostCenterCount = cs.childCount;
                            csa.id = createCostCenterAccountID(id,csa.costCenterID,csa.accountID);
                            INTAPS.ClientServer.ObjectFilters.filterAdd(AID, csa);
                            dspWriter.InsertSingleTableRecord(this.DBName, csa, new string[] { "__AID" }, new object[] { AID });
                        }
                        if (csa.accountID == accountID)
                            ret = csa.id;
                        created.Add(csa.id);
                    }

                    //if the parents of the account are not added to the cost center, add them too
                    int PID = ac.PID;
                    while (PID != -1)
                    {
                        Account parent = GetAccount<Account>(PID);
                        csa = GetCostCenterAccountInternal(dspWriter, costCenterID, parent.id);
                        if (csa == null)
                        {
                            csa = new CostCenterAccount();
                            csa.accountID = parent.id;
                            csa.accountPID = parent.PID;
                            csa.costCenterID = costCenterID;
                            csa.costCenterPID = cs.PID;
                            csa.creditAccount = parent.CreditAccount;
                            csa.childAccountCount = parent.childCount;
                            csa.childCostCenterCount = cs.childCount;
                            csa.id = createCostCenterAccountID(id, csa.costCenterID, csa.accountID);
                            dspWriter.InsertSingleTableRecord(this.DBName, csa, new string[] { "__AID" }, new object[] { AID });
                            created.Add(csa.id);
                        }
                        else
                            break;
                        PID = parent.PID;
                    }

                    //build leaves entries for each of the newely created accounts
                    if (tooManyChilds)
                    {
                        c = created.Count;
                    }
                    if (rebuildLeaves)
                    {
                        foreach (int cr in created)
                        {
                            if (tooManyChilds)
                            {
                                //Console.CursorTop--;
                                Console.WriteLine(c + "                         ");
                                c--;
                            }

                            rebuildLeaveEntries(new CostCenterAccountCashe(this), cr, false);
                        }
                    }
                    //NOTICE: High periformance penality debug code
                    assertCostCenterAccountChildCountFields();
                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void DeleteCostCenterAccount(int AID, int csaID)
        {
            BlockWithTransaction(csaID, "Account can't be deleted because it got transactions");
            INTAPS.ClientServer.ObjectFilters.filterDelete(AID, typeof(CostCenterAccount), csaID);
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    CostCenterAccount csa = GetCostCenterAccount(csaID);
                    if (csa == null)
                        throw new ServerUserMessage("Cost center account not found");

                    //remove the acount and all its descendents
                    List<int> childAccounts = new List<int>();
                    childAccounts.Add(csa.accountID);
                    GetAllChildAccount<Account>(dspWriter, csa.accountID, childAccounts);
                    List<int> created = new List<int>();
                    int p = 0;
                    Console.WriteLine();
                    foreach (int aid in childAccounts)
                    {
                        CostCenterAccount acsa = GetCostCenterAccountInternal(dspWriter, csa.costCenterID, aid);
                        if (acsa != null)
                            deleteCostCenterAccountRaw(AID, acsa.id,true);
                        //Console.CursorTop--;
                        Console.WriteLine((childAccounts.Count - (p++)) + "                       ");
                    }

                    //remove the accounts from parents for which no child is no more included on this cost center
                    int PID = csa.accountPID;
                    while (PID != -1)
                    {
                        Account parent = GetAccount<Account>(PID);
                        CostCenterAccount acsa = GetCostCenterAccountInternal(dspWriter, csa.costCenterID, parent.id);
                        if (acsa != null)
                        {
                            int count = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.CostCenterAccount where accountPID={1} and costCenterID={2}", this.DBName, parent.id, csa.costCenterID));
                            if (count == 0)
                                deleteCostCenterAccountRaw(AID, acsa.id,true);
                            else
                                break;
                        }
                        else
                            break;
                        PID = parent.PID;
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        private void deleteCostCenterAccountRaw(int AID, int csaID,bool complete)
        {
            if(complete)
                BlockWithTransaction(csaID, "It is not allowed to delete an account with transaction");
            INTAPS.ClientServer.ObjectFilters.filterDelete(AID, typeof(CostCenterAccount), csaID);
            dspWriter.Delete(this.DBName, "CostCenterLeafAccount", "childCostCenterAccountID=" + csaID);
            dspWriter.Delete(this.DBName, "CostCenterLeafAccount", "parentCostCenterAccountID=" + csaID);
            dspWriter.Delete(this.DBName, "AccountDependancy", "csaID=" + csaID);
            assertCostCenterLeafAccountNoParentChild();
            logOldCostCenterAccount(AID, csaID);
            dspWriter.DeleteSingleTableRecrod<CostCenterAccount>(this.DBName, csaID);
        }
        public bool DocumentExists(int ID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return ((int)reader.ExecuteScalar(String.Format("Select count(*) from {0}.dbo.Document where ID={1}", this.DBName, ID))) > 0;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public double GetNetCostCenterAccountBalanceAsOf(int accountID, int costCenerID, int itemID, DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(accountID, costCenerID);
            if (csa == null)
                return 0;
            return GetNetBalanceAsOf(csa.id, itemID, date);
        }
        public double GetNetCostCenterAccountBalanceAsOfH(int accountID, int costCenerID, int itemID, DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(accountID, costCenerID);
            if (csa == null)
                return 0;
            return GetNetBalanceAsOfH(csa.id, itemID, date);
        }


        public AccountTransaction[] GetTransactionsOfTypeReference(DocumentTypedReference tref)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                int NRec;
                AccountBalance _totalTran;
                string sql = @"SELECT     AccountTransaction.*
FROM         {0}.dbo.AccountTransaction INNER JOIN
                      {0}.dbo.DocumentSerial ON AccountTransaction.BatchID = DocumentSerial.documentID where typeID={1} and reference='{2}'";
                return GetTransactionBySQL(reader, string.Format(sql, this.DBName, tref.typeID, tref.reference), 0, -1, out NRec, out _totalTran);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public AuditInfo GetDocumentAuditInfo(int docID)
        {
            return ApplicationServer.SecurityBDE.getAuditInfoFromTable(this.DBName, "Document", "ID=" + docID);
        }

        public DocumentType GetTypedAccountDocument<DocumentType>(int documentID) where DocumentType : AccountDocument, new()
        {
            return GetAccountDocument(documentID, true) as DocumentType;
        }

        public List<int> GetJoinedAccountIDs<AccountType>(int costCenterID) where AccountType : AccountBase, new()
        {
            System.Data.SqlClient.SqlDataReader reader = null;
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string sql = string.Format("Select {0} from {1}.dbo.CostCenterAccount where {2}={3}",
                    AccountBase.GetCostCenterAccountComplementIDField<AccountType>(), this.DBName,
                    AccountBase.GetCostCenterAccountIDField<AccountType>(), costCenterID);
                List<int> ret = new List<int>();
                reader = dspReader.ExecuteReader(sql);
                while (reader.Read())
                {
                    ret.Add((int)reader[0]);
                }

                return ret;
            }
            finally
            {
                if (reader == null && !reader.IsClosed)
                    reader.Close();
                base.ReleaseHelper(dspReader);
            }
        }

        public AccountTransaction[] GetTransactions(int[] accountID, int itemID,DateTime date1,DateTime date2,int pageIndex,int pageSize,out int NRecord)
        {
            if (accountID.Length == 0)
            {
                NRecord = 0;
                return new AccountTransaction[0];
            }
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string cr = null;
                if (accountID.Length == 1)
                {
                    cr = string.Format("accountID={0} and itemID={1} and tranTicks>={2} and tranTicks<{3}", accountID[0], itemID, date1.Ticks, date2.Ticks);
                }
                else
                {
                    string list=null;
                    foreach (int aid in accountID)
                    {
                        if (list == null)
                            list = aid.ToString();
                        else
                            list = list + "," + aid;
                    }
                    cr = string.Format("accountID in ({0}) and itemID={1} and tranTicks>={2} and tranTicks<{3}", list, itemID, date1.Ticks, date2.Ticks);
                }
                AccountBalance bal;
                return GetTransactionByFilter(dspReader, cr, pageIndex, pageSize, out NRecord, out bal);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void assertCostCenterAccountTransactionRelation(INTAPS.RDBMS.SQLHelper helper)
        {
            string sql = "Select top 1 accountID from {0}.dbo.AccountTransaction where accountID not in (Select id from {0}.dbo.CostCenterAccount)";
            object id = helper.ExecuteScalar(string.Format(sql, this.DBName));
            if (id is int)
                throw new ServerUserMessage("Invalid accountID refered in AccountTransaction table. accountID:"+id);
        }
        public void rebuildCostCenterLeafAccountTable(int AID)
        {
            PushProgress("Caching entire accounting structure..");
            CostCenterAccountCashe cache = new CostCenterAccountCashe(this, true);
            dspWriter.Delete(this.DBName, "CostCenterLeafAccount", null);
            int c;
            PopProgress();
            PushProgress("Processing cost center accounts..");

            Console.WriteLine("Processing " + (c=cache.costCenterAccountCache2.Count) + " items");
            int prog = 0;
            try
            {
                foreach (KeyValuePair<int, CostCenterAccount> kv in cache.costCenterAccountCache2)
                {
                    //Console.CursorTop--;
                    Console.WriteLine((c - prog) + "                                             ");
                    rebuildLeaveEntries(cache, kv.Key, true);
                    prog++;
                    SetProgress((double)prog / (double)c);
                }
            }
            finally
            {
                PopProgress();
            }
        }
        public void changeParent<AccountType>(int AID, int accountID, int newParentID) where AccountType : AccountBase, new()
        {
            changeParent<AccountType>(AID, accountID, newParentID, true);
        }
        public void changeParent<AccountType>(int AID,int accountID, int newParentID,bool rebuildCostCenterLeaveEntries) where AccountType : AccountBase, new()
        {
            AccountType account = GetAccount<AccountType>(accountID);
            //if (account.childCount > 0)
                //throw new ServerUserMessage("Changing parent is supported only for leaf accounts");
            if (newParentID == -1)
                throw new ServerUserMessage("Changing a leaf account to root account is not supported");
            BlockWithTransaction<AccountType>(newParentID, "An account with transaction can't be a parent account");
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    try
                    {
                        Dictionary<int, Dictionary<int, int>> reuseID = new Dictionary<int, Dictionary<int, int>>();
                        AccountType oldParent = GetAccount<AccountType>(account.PID);
                        AccountType newParent = GetAccount<AccountType>(newParentID);
                        account.PID = newParentID;
                        updateAccountRaw<AccountType>(AID, account);
                        dspWriter.Update(this.DBName, "CostCenterAccount", new string[] { typeof(AccountType).Name + "PID","__AID" }
                                , new object[] { newParent.id,AID }
                                , typeof(AccountType).Name + "ID=" + account.id);


                        if (newParentID != -1)
                        {
                            int ccount = getDirectChildCount<AccountType>(dspWriter, newParent);
                            newParent.childCount = ccount;
                            dspWriter.Update(this.DBName, "CostCenterAccount"
                                , new string[] { "child" + typeof(AccountType).Name + "Count", "__AID" }
                                , new object[] { ccount, AID }
                                , AccountBase.GetCostCenterAccountIDField<AccountType>() + "=" + newParent.id);
                            updateAccountRaw<AccountType>(AID, newParent);
                        }
                        if (oldParent != null)
                        {
                            int ccount = getDirectChildCount<AccountType>(dspWriter, oldParent);
                            oldParent.childCount = ccount;
                            dspWriter.Update(this.DBName, "CostCenterAccount"
                                , new string[] { 
                                    "child" + typeof(AccountType).Name + "Count",
                                    "__AID" }
                                , new object[] { 
                                    ccount, 
                                    AID }
                                , AccountBase.GetCostCenterAccountIDField<AccountType>() + "=" + oldParent.id);

                            oldParent.childCount = ccount;
                            updateAccountRaw<AccountType>(AID, oldParent);
                        }
                        if (rebuildCostCenterLeaveEntries)
                            rebuildCostCenterLeafAccountTable(AID);
                        //assertCostCenterAccountTransactionRelation(dspWriter);
                        //assertCostCenterLeafAccountNoParentChild();
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }


       
    }
}