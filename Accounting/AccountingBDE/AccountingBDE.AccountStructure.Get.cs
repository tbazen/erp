using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        Dictionary<int, Account> _accountCache = new Dictionary<int, Account>();
        Dictionary<string, Account> _accountCacheCode = new Dictionary<string, Account>();
        
        Dictionary<int, CostCenter> _costCenterCache = new Dictionary<int, CostCenter>();
        Dictionary<string, CostCenter> _costCenterCacheCode = new Dictionary<string, CostCenter>();

        Dictionary<ulong, CostCenterAccount> _costCenterAccountCacheLong = new Dictionary<ulong, CostCenterAccount>();
        Dictionary<int, CostCenterAccount> _costCenterAccountCache = new Dictionary<int, CostCenterAccount>();

        //UNUSED: this code will be used when in the future server side memory caching of accounting structure is implemented
        void loadCache()
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                _accountCache=new Dictionary<int,Account>();
                _accountCacheCode = new Dictionary<string, Account>();
                foreach (Account ac in dspReader.GetSTRArrayByFilter<Account>(""))
                {
                    _accountCache.Add(ac.id, ac);
                    _accountCacheCode.Add(ac.Code, ac);
                }

                _costCenterCache = new Dictionary<int, CostCenter>();
                _costCenterCacheCode = new Dictionary<string, CostCenter>();
                foreach (CostCenter cs in dspReader.GetSTRArrayByFilter<CostCenter>(""))
                {
                    _costCenterCache.Add(cs.id, cs);
                    _costCenterCacheCode.Add(cs.Code, cs);
                }

                _costCenterAccountCache = new Dictionary<int, CostCenterAccount>();
                _costCenterAccountCacheLong = new Dictionary<ulong, CostCenterAccount>();
                foreach (CostCenterAccount csa in dspReader.GetSTRArrayByFilter<CostCenterAccount>(""))
                {
                    _costCenterAccountCache.Add(csa.id, csa);
                    _costCenterAccountCacheLong.Add(csa.longID, csa);
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            
        }
        //Account/Cost Center related internal get methods
        protected bool IsCodeUsed<AccountType>(INTAPS.RDBMS.SQLHelper dsp, string AccountCode, int Except) where AccountType : AccountBase, new()
        {
            //string sql = "Select count(*) from {0}.dbo.{1} where Code='{2}' and Status={3}  and ID<>{4}";
            //return (int)dsp.ExecuteScalar(String.Format(sql, this.DBName, typeof(AccountType).Name, AccountCode, (int)AccountStatus.Activated, Except)) > 0;
            string sql = "Select count(*) from {0}.dbo.{1} where Code='{2}' and ID<>{3}";
            return (int)dsp.ExecuteScalar(String.Format(sql, this.DBName, typeof(AccountType).Name, AccountCode, Except)) > 0;
        }
        protected bool IsControlOf<AccountType>(INTAPS.RDBMS.SQLHelper dsp, int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            int[] ps = ExpandParents<AccountType>(dsp, testAccount);
            foreach (int p in ps)
                if (p == acountID)
                    return true;
            return false;
        }
        protected void GetLeafAccounts<AccountType>(INTAPS.RDBMS.SQLHelper reader, AccountType account, List<AccountType> leaves)
            where AccountType : AccountBase, new()
        {
            int N;
            int acID = account == null ? -1 : account.id;
            AccountType[] ac = reader.GetSTRArrayByFilter<AccountType>("PID=" + acID, 0, -1, out N);
            if (ac.Length == 0)
            {
                leaves.Add(account);
                return;
            }
            foreach (AccountType theAccount in ac)
                GetLeafAccounts<AccountType>(reader, theAccount, leaves);
        }
        protected bool isCreditCostCenterAccountInternal(RDBMS.SQLHelper reader, int csAccountID)
        {
            object _val = reader.ExecuteScalar(String.Format("Select CreditAccount from {0}.dbo.CostCenterAccount where id={1}", this.DBName, csAccountID));
            if (_val is bool)
                return (bool)_val;
            return false;
        }
        protected int getAccountChildCountInternal<AccountType>(INTAPS.RDBMS.SQLHelper dsp, int AccountID) where AccountType : AccountBase, new()
        {

            string sql = "Select childCount from {0}.dbo.{1} where ID={2}";
            return (int)dsp.ExecuteScalar(string.Format(sql, this.DBName, typeof(AccountType).Name, AccountID));
        }
        protected void GetAllChildAccount<AccountType>(INTAPS.RDBMS.SQLHelper helper, int pid, List<int> ac) where AccountType : AccountBase, new()
        {
            int[] chs = helper.GetColumnArray<int>(String.Format("Select id from {0}.dbo.{1} where PID={2}", this.DBName, typeof(AccountType).Name, pid), 0);
            ac.AddRange(chs);
            foreach (int ch in chs)
            {
                GetAllChildAccount<AccountType>(helper, ch, ac);
            }

        }
        // BDE exposed
        public int[] getAllChildAccounts<AccountType>(int pid) where AccountType : AccountBase, new()
        {
            List<int> ret = new List<int>();
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                GetAllChildAccount<AccountType>(dspReader, pid, ret);
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        protected AccountType[] GetAccountByFilter<AccountType>(INTAPS.RDBMS.SQLHelper dsp, string Filter) where AccountType : AccountBase, new()
        {
            AccountType[] ret = dsp.GetSTRArrayByFilter<AccountType>(Filter);
            return ret;
        }
        protected AccountType GetAccountInternal<AccountType>(INTAPS.RDBMS.SQLHelper dsp, int AccountID) where AccountType : AccountBase, new()
        {
            AccountType[] _ret = GetAccountByFilter<AccountType>(dsp, "id=" + AccountID);
            if (_ret.Length == 0)
                return null;
            return _ret[0];
        }
        public void testValidAccountID<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            testValidAccountID<AccountType>(accountID, "Invalid account ID:" + accountID);
        }
        public void testValidAccountID<AccountType>(int accountID, string msg) where AccountType : AccountBase, new()
        {
            AccountType ac = GetAccount<AccountType>(accountID);
            if (ac == null)
                throw new ServerUserMessage(msg);
        }
        protected void verifyCostCenterAccountExists(SQLHelper dspReader, int csaID, out CostCenterAccount csa)
        {
            csa = GetCostCenterAccountInternal(dspReader, csaID);
            if (csa == null)
                throw new ServerUserMessage("Cost center account doesn't exist ID:" + csaID);
        }
        protected int getDirectChildCount<AccountType>(INTAPS.RDBMS.SQLHelper helper, AccountType parent) where AccountType : AccountBase, new()
        {
            int ccount = (int)helper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.{1} where PID={2}", this.DBName, typeof(AccountType).Name, parent.id));
            return ccount;
        }
        protected string GetCostCenterAccountString(CostCenterAccount csac)
        {

            if (this.IsSingleCostCenter())
                return GetAccount<Account>(csac.accountID).CodeName;
            string csstr;
            csstr = "Account:" + GetAccount<Account>(csac.accountID).CodeName + " Cost Center:" + GetAccount<CostCenter>(csac.costCenterID).CodeName;
            return csstr;
        }
        //Cost center account related internal get methods
        protected CostCenterAccount GetCostCenterAccountInternal(INTAPS.RDBMS.SQLHelper dsp, int csaID)
        {
            CostCenterAccount[] _ret = dsp.GetSTRArrayByFilter<CostCenterAccount>("id=" + csaID);
            if (_ret.Length == 0)
                return null;
            return _ret[0];
        }
        protected CostCenterAccountWithDescription DescribeCostCenterAccount(INTAPS.RDBMS.SQLHelper dsp, CostCenterAccount account)
        {
            if (account == null)
                return null;
            CostCenterAccountWithDescription desc = new CostCenterAccountWithDescription(account);
            desc.account = GetAccountInternal<Account>(dsp, account.accountID);
            desc.costCenter = GetAccountInternal<CostCenter>(dsp, account.costCenterID);
            if (desc.costCenter.id == CostCenter.ROOT_COST_CENTER)
            {
                desc.code = desc.account.Code;
                desc.name = desc.account.Name;
            }
            else
            {
                desc.code = CostCenterAccount.CreateCode(desc.costCenter.Code, desc.account.Code);
                desc.name = CostCenterAccount.CreateName(desc.costCenter.Name, desc.account.Name);
            }
            return desc;
        }
        protected CostCenterAccount GetCostCenterAccountInternal(INTAPS.RDBMS.SQLHelper dsp, int costCenterID, int accountID)
        {
            CostCenterAccount[] _ret = dsp.GetSTRArrayByFilter<CostCenterAccount>("accountID=" + accountID + " and costCenterID=" + costCenterID);
            if (_ret.Length == 0)
                return null;
            return _ret[0];
        }
        public CostCenterAccount[] GetCostCenterAccountsOf<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {

                return GetCostCenterAccountsOfInternal<AccountType>(helper, accountID);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public int[] GetCostCenterAccountIDssOf<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {

                return GetCostCenterAccountIDsOfInternal<AccountType>(helper, accountID);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        private CostCenterAccount[] GetCostCenterAccountsOfInternal<AccountType>(INTAPS.RDBMS.SQLHelper helper, int accountID) where AccountType : AccountBase, new()
        {
            CostCenterAccount[] costCenterAccounts;
            costCenterAccounts = helper.GetSTRArrayByFilter<CostCenterAccount>(AccountBase.GetCostCenterAccountIDField<AccountType>() + "=" + accountID);
            return costCenterAccounts;
        }
        private int[] GetCostCenterAccountIDsOfInternal<AccountType>(INTAPS.RDBMS.SQLHelper helper, int accountID) where AccountType : AccountBase, new()
        {
            int[] costCenterAccounts;
            costCenterAccounts = helper.GetColumnArray<int>(string.Format("Select id from {0}.dbo.CostCenterAccount where {1}={2}",this.DBName,AccountBase.GetCostCenterAccountIDField<AccountType>(),accountID));
            return costCenterAccounts;
        }
        //Public get methods
        public TransactionItem GetTransactionItem(int itemID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                if (itemID == TransactionItem.DEFAULT_CURRENCY)
                    return TransactionItem.DefaultCurrency;

                if (itemID == TransactionItem.MATERIAL_QUANTITY)
                    return TransactionItem.MaterialQuantity;
                TransactionItem[] items = helper.GetSTRArrayByFilter<TransactionItem>("id=" + itemID);
                if (items.Length == 0)
                    return null;
                return items[0];
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public bool IsControlOf<AccountType>(int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return IsControlOf<AccountType>(reader, acountID, testAccount);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public bool IsDirectControlOf<AccountType>(int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return GetAccountInternal<AccountType>(reader, testAccount).PID == acountID;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public int GetAccountID<AccountType>(string AccountCode) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                //string sql = "Select ID from {0}.dbo.{1} where Status={2} and Code='{3}'";
                //object obj = dspReader.ExecuteScalar(String.Format(sql, this.DBName, typeof(AccountType).Name, (int)AccountStatus.Activated, AccountCode));
                string sql = "Select ID from {0}.dbo.{1} where Code='{2}'";
                object obj = dspReader.ExecuteScalar(String.Format(sql, this.DBName, typeof(AccountType).Name, AccountCode));
                
                if (obj is int)
                    return (int)obj;
                return -1;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountType[] GetLeafAccounts<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                List<AccountType> _ret = new List<AccountType>();
                GetLeafAccounts<AccountType>(reader, this.GetAccountInternal<AccountType>(reader, accountID), _ret);
                return _ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public AccountType GetAccount<AccountType>(string AccountCode) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                AccountType[] _ret = GetAccountByFilter<AccountType>(dspReader, "code='" + AccountCode + "'");
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountType GetAccount<AccountType>(int AccountID) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetAccountInternal<AccountType>(dspReader, AccountID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public int GetCostCenterAccountAccountID<AccountType>(int csaID) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object ret = dspReader.ExecuteScalar(string.Format("Select {2} from {0}.dbo.CostCenterAccount where id={1}", this.DBName, csaID,AccountBase.GetCostCenterAccountIDField<AccountType>()));
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountType[] GetChildAllAcccount<AccountType>(int ParentAccount) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetAccountByFilter<AccountType>(dspReader, "PID=" + ParentAccount);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountType[] GetChildAcccount<AccountType>(int ParentAccount, int index, int pageSize, out int NRecords) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                AccountType[] ret = dspReader.GetSTRArrayByFilter<AccountType>("PID=" + ParentAccount, index, pageSize, out NRecords);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountType[] SearchAccount<AccountType>(string query, int index, int pageSize, out int NRecords)
            where AccountType : AccountBase, new()
        {
            return this.SearchAccount<AccountType>(query, -1, index, pageSize, out NRecords);
        }
        public AccountType[] SearchAccount<AccountType>(string query, int category, int index, int pageSize, out int NRecords)
            where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string filter;
                if (!string.IsNullOrEmpty(query))
                    filter = "(Name like '%" + query + "%' or  Code like '" + query + "%')";
                else
                    filter = "";
                if (category == -1)
                    return dspReader.GetSTRArrayByFilter<AccountType>(filter, index, pageSize, out NRecords);
                List<int> ch = new List<int>();
                ch.Add(category);
                GetAllChildAccount<AccountType>(dspReader, category, ch);
                if (string.IsNullOrEmpty(filter))
                    filter = "";
                else
                    filter += " AND ";
                if (ch.Count == 1)
                    filter += "pid=" + category;
                else
                {
                    filter += "pid in(";
                    for (int i = 0; i < ch.Count; i++)
                        if (i == 0)
                            filter += ch[i];
                        else
                            filter += "," + ch[i];
                    filter += ")";
                }
                return dspReader.GetSTRArrayByFilter<AccountType>(filter, index, pageSize, out NRecords);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CostCenterAccount GetDefaultCostCenterAccount(int accountID)
        {
            return GetCostCenterAccount(CostCenter.ROOT_COST_CENTER, accountID);
        }
        public CostCenterAccount GetCostCenterAccount(int csaID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetCostCenterAccountInternal(dspReader, csaID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(int csaID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CostCenterAccount cs = GetCostCenterAccountInternal(dspReader, csaID);
                return DescribeCostCenterAccount(dspReader, cs);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CostCenterAccount GetCostCenterAccount(int costCenterID, int accountID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetCostCenterAccountInternal(dspReader, costCenterID, accountID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public int GetCostCenterAccountID(int costCenterID, int accountID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CostCenterAccount csa = GetCostCenterAccountInternal(dspReader, costCenterID, accountID);
                return csa == null ? -1 : csa.id;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CostCenterAccount GetOrCreateCostCenterAccount(int AID,int costCenterID, int accountID)
        {
            CostCenterAccount ret= GetCostCenterAccount(costCenterID, accountID);
            if (ret != null)
                return ret;
            return GetCostCenterAccount(CreateCostCenterAccount(AID, costCenterID, accountID));
        }
        public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(int costCenterID, int accountID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CostCenterAccount cs = GetCostCenterAccountInternal(dspReader, costCenterID, accountID);
                return DescribeCostCenterAccount(dspReader, cs);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CostCenterAccount GetCostCenterAccount(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetCostCenterAccountInternal(dspReader, code);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        private CostCenterAccount GetCostCenterAccountInternal(INTAPS.RDBMS.SQLHelper dspReader, string code)
        {
            string[] parts = code.Split(':');
            if (parts.Length == 0 || parts.Length > 2)
                throw new ServerUserMessage("Invalid code format");
            CostCenter cs;
            if (parts.Length == 2)
            {
                cs = GetAccount<CostCenter>(parts[0]);
                if (cs == null)
                    return null;
            }
            else
                cs = _defaultCostCenter;
            Account ac = GetAccount<Account>(parts[parts.Length - 1]);
            if (ac == null)
                return null;
            return GetCostCenterAccountInternal(dspReader, cs.id, ac.id);
        }
        public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CostCenterAccount cs = GetCostCenterAccountInternal(dspReader, code);
                return DescribeCostCenterAccount(dspReader, cs);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public bool IsSingleCostCenter()
        {
            INTAPS.RDBMS.SQLHelper helper = this.GetReaderHelper();
            try
            {
                return (int)helper.ExecuteScalar("Select count(*) from " + this.DBName + ".dbo.CostCenter") == 1;
            }
            finally
            {
                this.ReleaseHelper(helper);
            }
        }
        // BDE exposed
        public int[] ExpandParents<AccountType>(int PID) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return ExpandParents<Account>(reader,PID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public int[] ExpandParents<AccountType>(INTAPS.RDBMS.SQLHelper dsp, int PID) where AccountType : AccountBase, new()
        {
            List<int> ps = new List<int>();
            while (PID != -1)
            {
                ps.Add(PID);
                PID = GetAccountInternal<AccountType>(dsp, PID).PID;
            }
            return ps.ToArray();
        }
        public bool isInSameHeirarchy<AccountType>(int accountID1, int accountID2) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper helper = this.GetReaderHelper();
            try
            {
                return isInSameHeirarchyInternal<AccountType>(helper, accountID1, accountID2);
            }
            finally
            {
                this.ReleaseHelper(helper);
            }
        }
        bool isInSameHeirarchyInternal<AccountType>(INTAPS.RDBMS.SQLHelper reader, int accountID1, int accountID2) where AccountType : AccountBase, new()
        {
            return IsParentOf<AccountType>(reader, accountID1, accountID2) || IsParentOf<AccountType>(reader, accountID2, accountID1);
        }
        // BDE exposed
        public bool isControlAccount<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object ch=dspReader.ExecuteScalar(string.Format("Select TOP 1 ID from {0}.dbo.{1} where PID={2}"
                    ,this.DBName,typeof(AccountType).Name,accountID));
                return ch is int;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
    }
}