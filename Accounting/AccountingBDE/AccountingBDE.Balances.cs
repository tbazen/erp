﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Data;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        public AccountBalance GetFlow(int costCenterID,int accountID, int ItemID, DateTime from, DateTime to)
        {
            CostCenterAccount csa = GetCostCenterAccount(costCenterID, accountID);
            if (csa == null)
                return new AccountBalance(-1, 0, 0, 0);
            return GetFlow(csa.id, ItemID, from, to);
        }
        public AccountBalance GetFlow(int AccountID, int ItemID, DateTime from, DateTime to)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                AccountBalance bal1 = GetBalanceAsOf(AccountID, ItemID, from);
                AccountBalance bal2 = GetBalanceAsOf(AccountID, ItemID, to);
                AccountBalance bal = new AccountBalance();
                bal.AccountID = AccountID;
                bal.ItemID = ItemID;
                bal.TotalDebit = bal2.TotalDebit - bal1.TotalDebit;
                bal.TotalCredit = bal2.TotalCredit - bal1.TotalCredit;
                return bal;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public double GetEndNetBalance(int csAccountID, int itemID)
        {
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                bool creditAccount = isCreditCostCenterAccountInternal(reader, csAccountID);
                return GetEndBalance(csAccountID, itemID).GetBalance(creditAccount);
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        public AccountBalance GetEndBalance(int accountID, int itemID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {

                CostCenterAccount csa;
                verifyCostCenterAccountExists(helper, accountID, out csa);
                AccountBalance ret = new AccountBalance(accountID, itemID);
                if (csa.isControlAccount)
                {
                    string sql = @"SELECT     
                                MAX(tranTicks) as tranTicks,MAX(lastOrderN) as lastOrderN
                                ,SUM(AccountBalance.TotalCredit) AS TotalCredit, SUM(AccountBalance.TotalDebit) AS TotalDebit
                            FROM         {0}.dbo.CostCenterLeafAccount INNER JOIN
                            {0}.dbo.AccountBalance ON CostCenterLeafAccount.childCostCenterAccountID = AccountBalance.AccountID
                            where ItemID={1} and parentCostCenterAccountID={2}";
                    DataTable dt = dspWriter.GetDataTable(string.Format(sql, this.DBName, itemID, accountID));
                    if (dt.Rows.Count == 0)
                        return ret;
                    DataRow row = dt.Rows[0];
                    if (row[0] is DateTime)
                    {
                        ret.tranTicks= (long)row[0];
                        ret.LastOrderN = -1;
                        ret.TotalCredit = (double)row[2];
                        ret.TotalDebit = (double)row[3];
                        return ret;
                    }
                    else
                        return ret;

                }
                else
                {
                    AccountBalance[] _ret = helper.GetSTRArrayByFilter<AccountBalance>("accountID=" + accountID + " and itemID=" + itemID);
                    if (_ret.Length == 0)
                        return ret;
                    return _ret[0];
                }
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountBalance[] GetEndBalances(int accountID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetSTRArrayByFilter<AccountBalance>("accountID=" + accountID);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public AccountBalance[] GetEndItemBalances(int ItemID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetSTRArrayByFilter<AccountBalance>("ItemID=" + ItemID);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public double GetNetBalanceAsOf(int csAccountID, int itemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                bool creditAccount = isCreditCostCenterAccountInternal(reader, csAccountID);
                return GetBalanceAsOf(csAccountID, itemID, date).GetBalance(creditAccount);
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        public double GetNetBalanceAsOfH(int csAccountID, int itemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                bool creditAccount = isCreditCostCenterAccountInternal(reader, csAccountID);
                return GetBalanceAsOfH(csAccountID, itemID, date).GetBalance(creditAccount);
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        public double GetNetBalanceAsOf(int costCenterID,int accountID, int itemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                CostCenterAccount csa = GetCostCenterAccount(costCenterID, accountID);
                if (csa == null)
                    return 0;
                return GetBalanceAsOf(csa.id, itemID, date).GetBalance(csa.creditAccount);
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public double GetNetBalanceAfter(int csAccountID, int itemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                bool creditAccount = isCreditCostCenterAccountInternal(reader, csAccountID);
                return GetBalanceAfter(csAccountID, itemID, date).GetBalance(creditAccount);
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public AccountBalance GetBalanceAfter(int AccountID, int ItemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                AccountBalance bal = new AccountBalance(AccountID, ItemID);
                CostCenterAccount csa = GetCostCenterAccount(AccountID);
                if (csa == null)
                    return bal;
                if (csa.isControlAccount)
                {
                    string sql = @"Select 
                                sum(case TransactionType when 0 then case when Amount>0 then TotalDbBefore+Amount else TotalDbBefore end else
                                case when Amount>0 then Amount else 0 end end) as TotalDebit,
                                sum(case TransactionType when 0 then case when Amount<0 then TotalCrBefore-Amount else TotalCrBefore end else
                                case when Amount<0 then -Amount else 0 end end) as TotalCredit
                                 from {0}.dbo.AccountTransaction t inner join
                                (SELECT     childCostCenterAccountID,max(orderN) as orderN
                                FROM         {0}.dbo.CostCenterLeafAccount INNER JOIN
                                      {0}.dbo.AccountTransaction ON CostCenterLeafAccount.childCostCenterAccountID = AccountTransaction.AccountID
                                      where parentCostCenterAccountID={1} and ItemID={2} and tranTicks<={3}
                                      group by childCostCenterAccountID) as MaxBal
                                        on t.AccountID=MaxBal.childCostCenterAccountID  and ItemID={2} and t.OrderN=maxBal.orderN";

                    DataTable dt = dspReader.GetDataTable(string.Format(sql, this.DBName, AccountID, ItemID, date.Ticks));
                    if (dt.Rows.Count == 0)
                        return bal;
                    DataRow row = dt.Rows[0];
                    bal.TotalDebit = row[0] is double ? (double)row[0] : 0;
                    bal.TotalCredit = row[1] is double ? (double)row[1] : 0;
                    docDepAddAccountDependancy(AccountID, ItemID, date.Ticks, false);
                    return bal;
                }
                else
                {
                    string sql = "SELECT     max(orderN) as orderN FROM {0}.dbo.AccountTransaction where AccountID={1} and ItemID={2} and tranTicks<={3}";
                    object _ordern = dspReader.ExecuteScalar(string.Format(sql, this.DBName, AccountID, ItemID, date.Ticks));
                    if (!(_ordern is int))
                        return bal;
                    sql = @"Select 
                                (case TransactionType when 0 then case when Amount>0 then TotalDbBefore+Amount else TotalDbBefore end else
                                case when Amount>0 then Amount else 0 end end) as TotalDebit,
                                (case TransactionType when 0 then case when Amount<0 then TotalCrBefore-Amount else TotalCrBefore end else
                                case when Amount<0 then -Amount else 0 end end) as TotalCredit
                            from {0}.dbo.AccountTransaction where AccountID={1}  and ItemID={2} and orderN={3}";

                    DataTable dt = dspReader.GetDataTable(string.Format(sql, this.DBName, AccountID, ItemID, _ordern));
                    if (dt.Rows.Count == 0)
                        return bal;
                    DataRow row = dt.Rows[0];
                    bal.TotalDebit = row[0] is double ? (double)row[0] : 0;
                    bal.TotalCredit = row[1] is double ? (double)row[1] : 0;
                    docDepAddAccountDependancy(AccountID, ItemID, date.Ticks, false);
                    return bal;
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountBalance GetBalanceAsOf(int costCenterID, int AccountID, int ItemID, DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(costCenterID, AccountID);
            if (csa == null)
                return new AccountBalance(-1, 0, 0, 0);
            return GetBalanceAsOf(csa.id, ItemID, date);
        }
        public AccountBalance GetBalanceAsOf(int csAccountID, int ItemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetBalanceAsOf(dspReader,csAccountID,ItemID,date);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountBalance GetBalanceAsOfH(int csAccountID, int ItemID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetBalanceAsOfH(dspReader, csAccountID, ItemID, date);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        AccountBalance getBalanceAsOfForControlAccountH(INTAPS.RDBMS.SQLHelper dspReader, CostCenterAccount csa, int ItemID, DateTime date)
        {
            AccountBalance ret = new AccountBalance(csa.id, ItemID);
            int[] childs = dspReader.GetColumnArray<int>("Select childCostCenterAccountID from {0}.dbo.CostCenterLeafAccount where parentCostCenterAccountID={1}".format(this.DBName, csa.id));
            if (childs.Length == 0)
                return ret;
            foreach (int ch in childs)
                ret.AddBalance(getAccountBalanceLeafH(dspReader, ch, ItemID, date));
            /*StringBuilder cr = new StringBuilder();
            for (int i = 0; i < childs.Length; i++)
            {
                if (i > 0)
                    cr.Append(",");
                cr.Append(childs[i].ToString());
            }
            string sql = "SELECT   AccountID,max(tranTicks) as orderN FROM {0}.dbo.AccountTransaction1 where AccountID in ({1}) and ItemID={2} and tranTicks<={3} group by AccountID";
            DataTable rows = dspReader.GetDataTable(string.Format(sql, this.DBName, cr.ToString(), ItemID, date.Ticks));
            foreach (DataRow row in rows.Rows)
            {
                sql = @"Select 
                            TotalDbBefore as TotalDebit,
                            TotalCrBefore as TotalCredit
                        from {0}.dbo.AccountTransaction1 where AccountID={1}  and ItemID={2} and tranTicks={3}";

                DataTable dt = dspReader.GetDataTable(string.Format(sql, this.DBName, row[0], ItemID, row[1]));
                if (dt.Rows.Count == 0)/
                    continue;
                DataRow thisRow = dt.Rows[0];
                ret.TotalDebit += thisRow[0] is double ? (double)thisRow[0] : 0;
                ret.TotalCredit += thisRow[1] is double ? (double)thisRow[1] : 0;
            }*/
            docDepAddAccountDependancy(csa.id, ItemID, date.Ticks, true);
            return ret;
        }
        public AccountBalance GetBalanceAsOfH(INTAPS.RDBMS.SQLHelper dspReader, int csAccountID, int ItemID, DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(csAccountID);
            if (csa == null)
                return new AccountBalance(csAccountID, ItemID);
            if (csa.isControlAccount)
            {
                return getBalanceAsOfForControlAccountH(dspReader, csa, ItemID, date);
            }
            else
            {
                return getAccountBalanceLeafH(dspReader, csAccountID, ItemID, date);
            }
        }

        private AccountBalance getAccountBalanceLeafH(SQLHelper dspReader, int csAccountID, int ItemID, DateTime date)
        {
            AccountBalance bal = new AccountBalance(csAccountID, ItemID);
            string sql = "SELECT     max(tranTicks) as tt FROM {0}.dbo.AccountTransaction where AccountID={1} and ItemID={2} and tranTicks<={3}";
            object _ticks = dspReader.ExecuteScalar(string.Format(sql, this.DBName, csAccountID, ItemID, date.Ticks));
            if (!(_ticks is long))
                return bal;
            sql = @"Select 
                            TotalDbBefore as TotalDebit,
                            TotalCrBefore as TotalCredit
                        from {0}.dbo.AccountTransaction where AccountID={1}  and ItemID={2} and tranTicks={3}";

            DataTable dt = dspReader.GetDataTable(string.Format(sql, this.DBName, csAccountID, ItemID, _ticks));
            if (dt.Rows.Count == 0)
                return bal;
            DataRow row = dt.Rows[0];
            bal.TotalDebit = row[0] is double ? (double)row[0] : 0;
            bal.TotalCredit = row[1] is double ? (double)row[1] : 0;
            docDepAddAccountDependancy(csAccountID, ItemID, date.Ticks, true);
            return bal;
        }

        AccountBalance getBalanceAsOfForControlAccount(INTAPS.RDBMS.SQLHelper dspReader,CostCenterAccount csa, int ItemID, DateTime date)
        {
            AccountBalance ret = new AccountBalance(csa.id, ItemID);
            int[] childs = dspReader.GetColumnArray<int>("Select childCostCenterAccountID from {0}.dbo.CostCenterLeafAccount where parentCostCenterAccountID={1}".format(this.DBName, csa.id));
            if (childs.Length == 0)
                return ret;
            StringBuilder cr = new StringBuilder();
            for(int i=0;i<childs.Length;i++)
            {
                if (i > 0)
                    cr.Append(",");
                cr.Append(childs[i].ToString());
            }
            string sql = "SELECT   AccountID,max(orderN) as orderN FROM {0}.dbo.AccountTransaction where AccountID in ({1}) and ItemID={2} and tranTicks<{3} group by AccountID";
            DataTable rows = dspReader.GetDataTable(string.Format(sql, this.DBName, cr.ToString(), ItemID, date.Ticks));
            foreach (DataRow row in rows.Rows)
            {
                sql = @"Select 
                            (case TransactionType when 0 then case when Amount>0 then TotalDbBefore+Amount else TotalDbBefore end else
                            case when Amount>0 then Amount else 0 end end) as TotalDebit,
                            (case TransactionType when 0 then case when Amount<0 then TotalCrBefore-Amount else TotalCrBefore end else
                            case when Amount<0 then -Amount else 0 end end) as TotalCredit
                        from {0}.dbo.AccountTransaction where AccountID={1}  and ItemID={2} and orderN={3}";

                DataTable dt = dspReader.GetDataTable(string.Format(sql, this.DBName, row[0], ItemID, row[1]));
                if (dt.Rows.Count == 0)
                    continue;
                DataRow thisRow= dt.Rows[0];
                ret.TotalDebit += thisRow[0] is double ? (double)thisRow[0] : 0;
                ret.TotalCredit += thisRow[1] is double ? (double)thisRow[1] : 0;
            }
            docDepAddAccountDependancy(csa.id, ItemID, date.Ticks, true);
            return ret;
        }


        public AccountBalance GetBalanceAsOf(INTAPS.RDBMS.SQLHelper dspReader,int csAccountID, int ItemID, DateTime date)
        {
            AccountBalance bal = new AccountBalance(csAccountID, ItemID);
            CostCenterAccount csa = GetCostCenterAccount(csAccountID);
            if (csa == null)
                return bal;
            if (csa.isControlAccount)
            {
                return getBalanceAsOfForControlAccount(dspReader, csa, ItemID, date);
            }
            else
            {
                string sql = "SELECT     max(orderN) as orderN FROM {0}.dbo.AccountTransaction where AccountID={1} and ItemID={2} and tranTicks<{3}";
                object _ordern = dspReader.ExecuteScalar(string.Format(sql, this.DBName, csAccountID, ItemID, date.Ticks));
                if (!(_ordern is int))
                    return bal;
                sql = @"Select 
                            (case TransactionType when 0 then case when Amount>0 then TotalDbBefore+Amount else TotalDbBefore end else
                            case when Amount>0 then Amount else 0 end end) as TotalDebit,
                            (case TransactionType when 0 then case when Amount<0 then TotalCrBefore-Amount else TotalCrBefore end else
                            case when Amount<0 then -Amount else 0 end end) as TotalCredit
                        from {0}.dbo.AccountTransaction where AccountID={1}  and ItemID={2} and orderN={3}";

                DataTable dt = dspReader.GetDataTable(string.Format(sql, this.DBName, csAccountID, ItemID, _ordern));
                if (dt.Rows.Count == 0)
                    return bal;
                DataRow row = dt.Rows[0];
                bal.TotalDebit = row[0] is double ? (double)row[0] : 0;
                bal.TotalCredit = row[1] is double ? (double)row[1] : 0;
                docDepAddAccountDependancy(csAccountID, ItemID, date.Ticks, true);
                return bal;
            }
        }
        public AccountBalance[] GetBalancesAsOf(int accountID, DateTime date)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                AccountBalance[] endBalances=helper.GetSTRArrayByFilter<AccountBalance>("accountID=" + accountID);
                List<AccountBalance> ret = new List<AccountBalance>();
                foreach (AccountBalance bal in endBalances)
                {
                    AccountBalance balAsOf= GetBalanceAsOf(accountID, bal.ItemID, date);
                    if (balAsOf.IsZero)
                        continue;
                    ret.Add(balAsOf);
                }
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public void getCombinedBalanceAsOf(int[] accountID, int[] itemID, DateTime begDate, out double db, out double crd)
        {
            db = 0;
            crd = 0;

            foreach (int a in accountID)
                foreach (int i in itemID)
                {
                    AccountBalance bal = GetBalanceAsOf(a, i, begDate);
                    db += bal.TotalDebit;
                    crd += bal.TotalCredit;
                }
        }

    }
}
