﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS.Accounting.BDE
{
    partial class AccountingBDE
    {
        class DocumentDependancyList
        {
            public List<DocumentDependancy> docDependancy = new List<DocumentDependancy>();
            public List<AccountDependancy> acDependenacy = new List<AccountDependancy>();
        }
        class ChangedDocumentDependancyList
        {
            public List<DocumentDependancy> docDependancy = new List<DocumentDependancy>();
            public List<AccountDependancy> acDependenacy = new List<AccountDependancy>();
        }
        bool docDepUpdateDependency = false;
        void docDepInit()
        {
            docDepUpdateDependency = "true".Equals(System.Configuration.ConfigurationManager.AppSettings["updateDependencies"]);
        }
        Stack<DocumentDependancyList> dependancyStack = null;
        Stack<ChangedDocumentDependancyList> changedDependancyStack = null;
        void docDepStartChangeCollect()
        {
            if (changedDependancyStack == null)
                changedDependancyStack = new Stack<ChangedDocumentDependancyList>();
            changedDependancyStack.Push(new ChangedDocumentDependancyList());
        }
        
        void docDepEndChangeCollect(int AID,int documentID,HashSet<int> visited)
        {
            if (changedDependancyStack == null || changedDependancyStack.Count == 0)
                throw new INTAPS.ClientServer.ServerUserMessage("BUG: dependancy change collect stack empty");
            ChangedDocumentDependancyList dep = changedDependancyStack.Pop();
            List<int> docsToUpdate = new List<int>();
            foreach (AccountDependancy ac in dep.acDependenacy)
            {
                string sql = @"Select documentID from {0}.dbo.AccountDependancy
                where csaID={1} and itemID={2} and tranTicks>={3} and documentID<>{4}";
                docsToUpdate.AddRange(dspWriter.GetColumnArray<int>(sql.format(this.DBName, ac.csaID, ac.itemID, ac.tranTicks, documentID)));
            }
            foreach (DocumentDependancy docd in dep.docDependancy)
            {
                string sql = @"Select documentID from {0}.dbo.DocumentDependancy
                where dependancyDocumentID={1} and documentID<>{2}";
                docsToUpdate.AddRange(dspWriter.GetColumnArray<int>(sql.format(this.DBName, docd.dependancyDocumentID,documentID)));
            }
            HashSet<int> done = new HashSet<int>();
            if (docsToUpdate.Count > 0)
            {
                if (!docDepUpdateDependency)
                {
                    INTAPS.ClientServer.ApplicationServer.EventLoger.Log(INTAPS.ClientServer.EventLogType.Debug, "Warning: document Dependecy update disabled");
                    return;
                }

                Console.WriteLine("Updating document dependencies");
            }
            foreach (int docID in docsToUpdate)
            {
                if (done.Contains(docID))
                    continue;
                
                AccountDocument doc = GetAccountDocument(docID, true);
                Console.WriteLine("Posting DocID:{0}",docID);
                if (visited.Contains(docID))
                    throw new ClientServer.ServerUserMessage("Circular document dependency found.");
                PostGenericDocument(AID, doc);
                done.Add(docID);

            }
            if (docsToUpdate.Count > 0)
                Console.WriteLine("Updated {0} document dependencies",done.Count);
        }
        void docDepCancelChangeCollect()
        {
            if (changedDependancyStack == null || changedDependancyStack.Count == 0)
                //throw new INTAPS.ClientServer.ServerUserMessage("BUG: dependancy change collect stack empty");
                return;
            changedDependancyStack.Pop();
        }
        void docDepStart()
        {
            if (dependancyStack == null)
                dependancyStack = new Stack<DocumentDependancyList>();
            dependancyStack.Push(new DocumentDependancyList());

        }
        
        public object docDepSuspend()
        {
            if (!docDependacyMode)
                return null;
            return dependancyStack.Pop();
        }
        public void docDepResume(object list)
        {
            if (list == null)
                return;
            dependancyStack.Push((DocumentDependancyList)list);
        }
        void docDepEnd(int documentID)
        {
            if (!docDependacyMode)
                throw new INTAPS.ClientServer.ServerUserMessage("BUG: dependancy stack empty");
            DocumentDependancyList dep = dependancyStack.Pop();
            foreach (AccountDependancy ac in dep.acDependenacy)
            {
                ac.documentID = documentID;
                dspWriter.InsertSingleTableRecord(this.DBName, ac);
            }
            foreach (DocumentDependancy docd in dep.docDependancy)
            {
                if (docd.dependancyDocumentID == documentID)
                    continue;
                docd.documentID = documentID;
                dspWriter.InsertSingleTableRecord(this.DBName, docd);
            }
        }
        void docDepCancel()
        {
            if (!docDependacyMode)
                return;
            //throw new INTAPS.ClientServer.ServerUserMessage("BUG: dependancy stack empty");
            dependancyStack.Pop();
        }
        bool docDependacyMode
        {
            get
            {
                return dependancyStack != null && dependancyStack.Count > 0;
            }
        }
        DocumentDependancyList docDepList
        {
            get
            {
                if (dependancyStack == null || dependancyStack.Count == 0)
                    return null;
                return dependancyStack.Peek();
            }
        }
        private void docDepAddDocDependancy(int dependancyDocumentID)
        {
            DocumentDependancyList list = docDepList;
            if (list == null)
                return;
            foreach (DocumentDependancy dd in list.docDependancy)
                if (dd.dependancyDocumentID == dependancyDocumentID)
                    return;
            list.docDependancy.Add(new DocumentDependancy()
            {
                dependancyDocumentID = dependancyDocumentID
            }
            );
        }
        private void docDepAddAccountDependancy(int csaID, int itemID, long tranTicks, bool exclusive)
        {
            DocumentDependancyList list = docDepList;
            if (list == null)
                return;
            int[] las = getLeaveCostCenterAccounts(csaID);
            if (las.Length == 0)
                las = new int[] { csaID };
            foreach (int la in las)
            {
                bool skip = false;
                foreach (AccountDependancy acdep in list.acDependenacy)
                {
                    if (csaID == acdep.csaID && itemID == acdep.itemID)
                    {
                        if (exclusive == acdep.exclusive)
                        {
                            if (tranTicks <= acdep.tranTicks)
                            {
                                skip = true;
                                break;
                            }
                        }
                        if (acdep.tranTicks > tranTicks)
                        {
                            skip = true;
                            break;
                        }

                    }
                }
                if (skip)
                    continue;
                list.acDependenacy.Add(new AccountDependancy()
                {
                    csaID = la,
                    itemID = itemID,
                    tranTicks = tranTicks,
                    exclusive = exclusive,
                }
                );
            }
        }
        private void docDepPreventInDependancyMode(string opName)
        {
            throw new ClientServer.ServerUserMessage("BUG: {0} is not allowed in dependancy mode", opName);
        }
        private void docDepClearDependancy(int docID)
        {
            dspWriter.Delete(this.DBName, "AccountDependancy", "documentID=" + docID);
            dspWriter.Delete(this.DBName, "DocumentDependancy", "documentID=" + docID);
        }

        private void docDepAccountUpdated(int csaID, int itemID, long tranTicks)
        {
            if (changedDependancyStack == null || changedDependancyStack.Count == 0)
                return;
            ChangedDocumentDependancyList list = changedDependancyStack.Peek();

            bool skip = false;
            foreach (AccountDependancy acdep in list.acDependenacy)
            {
                if (csaID == acdep.csaID && itemID == acdep.itemID)
                {
                    if (tranTicks <= acdep.tranTicks)
                    {
                        skip = true;
                        break;
                    }
                }
            }
            if (skip)
                return;
            list.acDependenacy.Add(new AccountDependancy()
            {
                csaID = csaID,
                itemID = itemID,
                tranTicks = tranTicks,
            }
            );
        }
        private void docDepDocumentUpdated(int updatedDocumentID)
        {
            if (changedDependancyStack == null || changedDependancyStack.Count == 0)
                return;
            ChangedDocumentDependancyList list = changedDependancyStack.Peek();
            foreach (DocumentDependancy dd in list.docDependancy)
                if (dd.dependancyDocumentID == updatedDocumentID)
                    return;
            list.docDependancy.Add(new DocumentDependancy()
            {
                dependancyDocumentID = updatedDocumentID
            }
            );
        }
    }
}
