using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.RDBMS;
using INTAPS.ClientServer;


namespace INTAPS.Accounting.BDE
{
    public delegate bool AccountDocumentFilterDelegate(AccountDocument doc);
    class DocumentRDBMSFilter:IObjectFilter<AccountDocument>
    {
        public AccountDocumentFilterDelegate filterDelegate;

        public bool Include(AccountDocument obj, object[] additional)
        {
            return filterDelegate(obj);
        }
    }
    public partial class AccountingBDE
    {
        class DocumentCache : INTAPS.CachedObjectBase<int, AccountDocument>
        {
            AccountingBDE parent;
            public DocumentCache(AccountingBDE parent)
            {
                this.parent = parent;
            }
            protected override AccountDocument getObjectFromStore(int id)
            {
                return parent.GetAccountDocument(id, true);
            }            
        }
        DocumentCache docCache;

        Dictionary<int, DocumentHandler> _m_documentHandlerInfo=null;
        Dictionary<int, IDocumentServerHandler> _m_documentHandler=null;
        List<DocumentType> _m_documentTypes=null;
        List<DocumentType> m_documentTypes
        {
            get
            {
                if (_m_documentTypes == null)
                    LoadDocumentTypes();
                return _m_documentTypes;
            }
        }
        Dictionary<int, DocumentHandler> m_documentHandlerInfo
        {
            get
            {
                if (_m_documentHandlerInfo == null)
                    LoadDocumentHandlers();
                return _m_documentHandlerInfo;
            }
        }
        Dictionary<int, IDocumentServerHandler> m_documentHandler
        {
            get
            {
                if (_m_documentHandler == null)
                    LoadDocumentHandlers();
                return _m_documentHandler;
            }
        }
        List<int> _closingDocumentTypes = new List<int>();
        
        public void RegisterClosingDocumentType(int dt)
        {
            _closingDocumentTypes.Add(dt);
        }
        void LoadDocumentHandlers()
        {
            INTAPS.RDBMS.SQLHelper reader = this.GetReaderHelper();
            try
            {
                DocumentHandler[] h = reader.GetSTRArrayByFilter<DocumentHandler>(null);
                _m_documentHandlerInfo = new Dictionary<int, DocumentHandler>();
                _m_documentHandler = new Dictionary<int, IDocumentServerHandler>();
                foreach (DocumentHandler dh in h)
                {
                    Type t = Type.GetType(dh.serverClass);
                    if (t == null)
                    {
                        INTAPS.ClientServer.ApplicationServer.EventLoger.Log("Couldn't load document handler server class " + dh.serverClass);
                        continue;
                    }
                    System.Reflection.ConstructorInfo c = t.GetConstructor(new Type[] { typeof(AccountingBDE) });
                    if (c == null)
                    {
                        INTAPS.ClientServer.ApplicationServer.EventLoger.Log("Couldn't load document handler server class constructor for " + dh.serverClass);
                        continue;
                    }
                    try
                    {
                        _m_documentHandler.Add(dh.documentTypeID, (IDocumentServerHandler)c.Invoke(new object[] { this }));
                    }
                    catch (Exception ex)
                    {
                        INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Error instantiating document handler " + dh.serverClass, ex);
                        _m_documentHandler.Add(dh.documentTypeID, null);
                    }
                    _m_documentHandlerInfo.Add(dh.documentTypeID, dh);
                }
            }
            finally
            {
                this.ReleaseHelper(reader);
            }
        }
        void LoadDocumentTypes()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                _m_documentTypes = new List<DocumentType>();
                _m_documentTypes.AddRange(dspReader.GetSTRArrayByFilter<DocumentType>(null));
            }
            catch (Exception ex)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Error loading document types.", ex);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void initDocCache()
        {
            docCache = new DocumentCache(this);
            //this.WriterHelper.linkTransaction(docCache);
        }
        public void testDocCacheIntegerity()
        {
            foreach (KeyValuePair<int, AccountDocument> doc in docCache)
            {
                if (!DocumentExists(doc.Key))
                    ApplicationServer.EventLoger.Log("DocCache Inegerity Error: Document ID {0} is doesn't exist in database".format(doc.Key));
            }
        }
        void VerifyDocumentExists(INTAPS.RDBMS.SQLHelper helper, int docID)
        {
            if ((int)helper.ExecuteScalar("Select count(*) from " + this.DocumentTableDB + ".dbo.Document where id=" + docID) == 0)
                throw new ServerUserMessage("Document not in database");
        }


        public void VerifyUniqueRefNo(AccountDocument doc)
        {
            if (FindFirstDocument(doc.PaperRef, GetDocumentTypeByType(doc.GetType()).id) != null)
                throw new ServerUserMessage("Reference number " + doc.PaperRef + " is allready used");
        }
        public virtual string DocumentTableDB
        {
            get
            {
                return this.DBName;
            }
        }
        public virtual string DocumentIDKeyName
        {
            get
            {
                return this.BDEName + ".DocumentID";
            }
        }
        public DocumentType GetDocumentTypeByClass(string className)
        {
            foreach (DocumentType dt in m_documentTypes)
                if (dt.documentClass == className)
                    return dt;
            throw new ServerUserMessage("Undefined document type " + className);
        }
        public DocumentType GetDocumentTypeByType(Type type)
        {
            return GetDocumentTypeByTypeNoException(type);
            //if (ret == null)
            //    throw new ServerUserMessage("Undefined document type " + type);
            //return ret;
        }
        public DocumentType[] GetDocumentTypeByTypeWithInhertiance(Type type)
        {
            List<DocumentType> ret = new List<DocumentType>();
            foreach (DocumentType dt in m_documentTypes)
            {
                Type dtType = dt.GetTypeObject();
                if (dtType == null)
                    continue;
                if (dtType== type || dtType.IsSubclassOf(type))
                    ret.Add(dt);
            }
            return ret.ToArray();
        }
        // BDE exposed
        public DocumentType GetDocumentTypeByTypeNoException(Type type)
        {
            foreach (DocumentType dt in m_documentTypes)
                if (dt.GetTypeObject() == type)
                    return dt;
            return null;
        }
        public DocumentType GetDocumentTypeByID(int ID)
        {
            foreach (DocumentType dt in m_documentTypes)
                if (dt.id == ID)
                    return dt;
            return null;
        }
        public DocumentType GetDocumentTypeByDocumentID(int docID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object typeID = dspReader.ExecuteScalar(string.Format("Select DocumentTypeID from {0}.dbo.Document where ID={1}", this.DBName, docID));
                if(typeID is int)
                    return GetDocumentTypeByID((int)typeID);
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public DocumentType[] GetAllDocumentTypes()
        {
            return m_documentTypes.ToArray();
        }
        public DocumentType GetAccountingDocumentType(int typeID)
        {
            return GetDocumentTypeByID(typeID);
        }
        public DocumentHandler[] GetAllDocumentHandlerInfo()
        {
            DocumentHandler[] ret = new DocumentHandler[m_documentHandlerInfo.Count];
            m_documentHandlerInfo.Values.CopyTo(ret, 0);
            return ret;
        }
        public IDocumentServerHandler GetDocumentServerHandler(int typeID)
        {
            return m_documentHandler[typeID];
        }
        public Object InvokeDocumentHandlerMethod(int typeID, string method, object[] pars)
        {
            IDocumentServerHandler h = m_documentHandler[typeID];
            return h.GetType().GetMethod(method).Invoke(h, pars);
        }

        public bool HasDocumentHandler(int documentTypeID)
        {
            return m_documentHandler.ContainsKey(documentTypeID);
        }
        public IDocumentServerHandler GetDocumentHandler(int documentTypeID)
        {
            if (m_documentHandler.ContainsKey(documentTypeID))
                return m_documentHandler[documentTypeID];
            throw new ServerUserMessage("Document handler not found:" + documentTypeID);
        }
        public DocumentTypedReference[] getAllDocumentReferences(int docID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                DocumentSerial[] ss= dspReader.GetSTRArrayByFilter<DocumentSerial>("documentID=" + docID);
                DocumentTypedReference[] ret = new DocumentTypedReference[ss.Length];
                for (int i = 0; i < ret.Length; i++)
                {
                    ret[i] = new DocumentTypedReference(ss[i].typeID, ss[i].reference, ss[i].primaryDocument);
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        
        public int PostGenericDocument(int AID, AccountDocument doc)
        {
            lock (dspWriter)
            {
                DocumentType dt = this.GetDocumentTypeByType(doc.GetType());
                if (dt==null || !m_documentHandler.ContainsKey(dt.id))
                    throw new ServerUserMessage("Document handler not loaded for " + doc.GetType());
                IDocumentServerHandler h = m_documentHandler[dt.id];
                docDepStart();
                docDepStartChangeCollect();
                bool docDepDone = false;
                try
                {
                    int ret = h.Post(AID, doc);
                    docDepEnd(ret);
                    docDepDone = true;
                    docDepEndChangeCollect(AID, ret,new HashSet<int>());
                    return ret;
                }
                catch
                {
                    docDepCancelChangeCollect();
                    if(!docDepDone)
                        docDepCancel();
                    throw;
                }
                
            }
        }
        public void DeleteGenericDocument(int AID, int docID)
        {
            lock (dspWriter)
            {
                int typeID = this.GetAccountDocument(docID, false).DocumentTypeID;
                if (!m_documentHandler.ContainsKey(typeID))
                    throw new ServerUserMessage("Document handler not loaded for " + typeID);
                IDocumentServerHandler h = m_documentHandler[typeID];
                docDepStartChangeCollect();
                try
                {
                    h.DeleteDocument(AID, docID);
                    docDepEndChangeCollect(AID, docID,new HashSet<int>());
                }
                catch
                {
                    docDepCancelChangeCollect();
                    throw;
                }
                
            }
        }
        public void ReverseGenericDocument(int AID, int docID)
        {
            lock (dspWriter)
            {
                int typeID = this.GetAccountDocument(docID, false).DocumentTypeID;
                if (!m_documentHandler.ContainsKey(typeID))
                    throw new ServerUserMessage("Document handler not loaded for " + typeID);
                IDocumentServerHandler h = m_documentHandler[typeID];
                h.ReverseDocument(AID, docID);
            }
        }
        public string GetDocumentHTML(int accountDocumentID)
        {
            AccountDocument doc;
            doc = GetAccountDocument(accountDocumentID, true);
            if (doc == null)
                return "<h2>" + System.Web.HttpUtility.HtmlEncode("Invalid document ID:" + accountDocumentID) + "</h2>";
            if (m_documentHandler.ContainsKey(doc.DocumentTypeID))
                return m_documentHandler[doc.DocumentTypeID].GetHTML(doc);
            return doc.BuildHTML();
        }
        public string GetDocumentHTML(AccountDocument doc)
        {
            doc.DocumentTypeID = GetDocumentTypeByType(doc.GetType()).id;
            if (m_documentHandler.ContainsKey(doc.DocumentTypeID))
                return m_documentHandler[doc.DocumentTypeID].GetHTML(doc);
            return doc.BuildHTML();
        }
        public AccountDocument[] GetAccountingDocumentByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter, bool fullData)
        {
            DataTable tab;
            if (fullData)
                tab = dsp.GetDataTable("Select ID,DocumentTypeID,tranTicks,PaperRef,ShortDescription,LongDescription,reversed,DocumentData from " + this.DocumentTableDB + ".dbo.[Document] where " + filter + " order by tranTicks");
            else
                tab = dsp.GetDataTable("Select ID,DocumentTypeID,tranTicks,PaperRef,ShortDescription,LongDescription,reversed from " + this.DocumentTableDB + ".dbo.[Document] where " + filter + " order by tranTicks");
            AccountDocument[] ret = new AccountDocument[tab.Rows.Count];
            int i = 0;
            foreach (DataRow row in tab.Rows)
            {
                int dt = (int)row[1];
                int docID= (int)row[0];
                AccountDocument t;
                if (fullData)
                {
                    t = AccountDocument.DeserializeXML((string)row[7], GetDocumentTypeByID(dt).GetTypeObject());
                    DocumentSerial[] ser = getDocumentSerialsInternal(dsp, docID);
                    foreach (System.Reflection.FieldInfo fi in t.GetType().GetFields())
                    {
                        object[] atr=fi.GetCustomAttributes(typeof(DocumentTypedReferenceFieldAttribute), false);
                        if (atr != null && atr.Length > 0)
                        {
                            DocumentTypedReferenceFieldAttribute tra = (DocumentTypedReferenceFieldAttribute)atr[0];
                            foreach (string key in tra.types)
                            {
                                DocumentSerialType type= getDocumentSerialTypeInternal(dsp, key);
                                if (type == null)
                                    continue;
                                foreach (DocumentSerial s in ser)
                                {
                                    if (s.typeID == type.id)
                                    {
                                        fi.SetValue(t, s.typedReference);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                    t = new AccountDocument();
                t.AccountDocumentID = docID;
                t.DocumentTypeID = dt;
                t.tranTicks = (long)row[2];
                t.PaperRef = row[3] as string;
                t.ShortDescription = row[4] as string;
                t.LongDescription = row[5] as string;
                t.reversed = (bool)row[6];
                ret[i++] = t;
                docDepAddDocDependancy(t.AccountDocumentID);
            }
            return ret;
        }
        public DocType GetAccountDocument<DocType>(int documentID) where DocType:AccountDocument
        {
            return GetAccountDocument(documentID, true) as DocType;
        }

        public AccountDocument GetDeletedAccountDocument(int documentID, int daid)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                DataTable dataTable = dspReader.GetDataTable(string.Format("Select * from {0}.dbo.Document_Deleted where ID={1} and __DAID={2}", this.DBName, documentID, daid));
                if (dataTable.Rows.Count == 0)
                    return null;
                int docTypeId = Convert.ToInt32(dataTable.Rows[0].ItemArray[1]);
                string xmlData = dataTable.Rows[0].ItemArray[6].ToString();
                AccountDocument r = AccountDocument.DeserializeXML(xmlData, GetDocumentTypeByID(docTypeId).GetTypeObject());
                return r;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public DocumentTypedReference GetDeletedDocumentSerial(int documentID, int daid)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                DocumentSerial[] ret = dspReader.GetSTRArrayBySQL<DocumentSerial>(string.Format("Select * from {2}.dbo.DocumentSerial_Deleted where DocumentID={0} and __DAID={1} and [primaryDocument]=1", documentID, daid, this.DBName));
                if (ret.Length == 0)
                    return null;
                return ret[0].typedReference;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountDocument GetAccountDocument(int documentID, bool fullData)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetAccountDocumentInternal(dspReader, documentID, fullData);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
               
            }
        }
        public AccountDocument GetAccountDocumentNoException(int documentID, bool fullData)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                AccountDocument[] _ret = GetAccountingDocumentByFilter(dspReader, "ID=" + documentID, fullData);
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        private AccountDocument GetAccountDocumentInternal(INTAPS.RDBMS.SQLHelper dsp, int documentID, bool fullData)
        {
            AccountDocument[] _ret = GetAccountingDocumentByFilter(dsp, "ID=" + documentID, fullData);
            if (_ret.Length == 0)
                return null;
            docDepAddDocDependancy(_ret[0].AccountDocumentID);
            return _ret[0];
        }

        
        public AccountDocument[] GetAccountDocuments(string query, int type, bool date, DateTime from, DateTime to, int index, int pageSize, out int NRecord)
        {
            return GetAccountDocuments(new DocumentSearchPar() { query = query, type = type, date = date, from = from, to = to }, null,index, pageSize, out NRecord);
        }
        public AccountDocument[] GetAccountDocuments(DocumentSearchPar pars,AccountDocumentFilterDelegate filterDeligate,  int index, int pageSize, out int NRecord)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {

                dspReader.setReadDBUnrestorable(this.DocumentTableDB);
                string filter = null;
                if (!string.IsNullOrEmpty(pars.query))
                {
                    filter = "(PaperRef like '%" + INTAPS.RDBMS.SQLHelper.EscapeString(pars.query) + "%' ";
                    filter += " or ShortDescription like '%" + INTAPS.RDBMS.SQLHelper.EscapeString(pars.query) + "%' ";
                    filter += " or LongDescription like '%" + INTAPS.RDBMS.SQLHelper.EscapeString(pars.query) + "%')";
                }
                if (pars.type != -1)
                {
                    if (string.IsNullOrEmpty(filter))
                        filter = "DocumentTypeID=" + pars.type;
                    else
                        filter += " and DocumentTypeID=" + pars.type;
                }
                else if(pars.includeType!=null)
                {
                    if (pars.includeType.Length> 0)
                    {
                        string allDocFilter = " DocumentTypeID in (";
                        for (int i = 0; i < pars.includeType.Length; i++)
                        {
                            if (i == 0)
                                allDocFilter += pars.includeType[i];
                            else
                                allDocFilter += "," + pars.includeType[i];
                        }
                        allDocFilter += ")";
                        if (string.IsNullOrEmpty(filter))
                            filter = allDocFilter;
                        else
                            filter += " and " + allDocFilter;
                    }
                }
                else
                {
                    if (m_documentTypes.Count > 0)
                    {
                        string allDocFilter = " DocumentTypeID in (";
                        for (int i = 0; i < m_documentTypes.Count; i++)
                        {
                            if (i == 0)
                                allDocFilter += m_documentTypes[i].id;
                            else
                                allDocFilter += "," + m_documentTypes[i].id;
                        }
                        allDocFilter += ")";
                        if (string.IsNullOrEmpty(filter))
                            filter = allDocFilter;
                        else
                            filter += " and " + allDocFilter;
                    }
                }
                if (pars.date)
                {
                    if (!string.IsNullOrEmpty(filter))
                        filter += " and";
                    filter += " (tranTicks >= " + pars.from.Ticks + " and tranTicks<" + pars.to.Ticks + ")";
                }
                List<AccountDocument> _ret= new List<AccountDocument>();
                if(filterDeligate==null)
                    _ret.AddRange( dspReader.GetSTRArrayByFilter<AccountDocument>(filter, index, pageSize, out NRecord, null));
                else
                {
                    _ret.AddRange(dspReader.GetSTRArrayByFilter<AccountDocument>(filter, index, pageSize, out NRecord, new DocumentRDBMSFilter() {filterDelegate=filterDeligate }));
                }

                AccountDocument[] ret = new AccountDocument[_ret.Count];
                for (int i = 0; i < _ret.Count; i++)
                {
                    Type doct = GetAccountingDocumentType(_ret[i].DocumentTypeID).GetTypeObject();
                    if (doct == null)
                        throw new ServerUserMessage("Unknow document type " + _ret[i].DocumentTypeID);
                    string xml = dspReader.ExecuteScalar("Select DocumentData from " + this.DocumentTableDB + ".dbo.Document where ID=" + _ret[i].AccountDocumentID) as string;
                    ret[i] = AccountDocument.DeserializeXML(xml, doct);
                    ret[i].CoypFrom(_ret[i]);
                    docDepAddDocDependancy(ret[i].AccountDocumentID);
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountDocument FindFirstDocument(string paperRef, int type)
        {
            return FindFirstDocument(paperRef, type, false, DateTime.Now, DateTime.Now, null, false);
        }
        public AccountDocument FindFirstDocument(string paperRef, int type, AccountDocumentFilterDelegate filterDelegate, bool includeFullDocument)
        {
            return FindFirstDocument(paperRef, type, false, DateTime.Now, DateTime.Now, filterDelegate, includeFullDocument);
        }
        public AccountDocument FindFirstDocument(string paperRef, int type, bool date, DateTime from, DateTime to, AccountDocumentFilterDelegate filterDelegate, bool includeFullDocument)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                dspReader.setReadDBUnrestorable(this.DocumentTableDB);
                string filter = null;
                if (!string.IsNullOrEmpty(paperRef))
                {
                    filter = "(PaperRef='" + INTAPS.RDBMS.SQLHelper.EscapeString(paperRef) + "')";
                }
                if (type != -1)
                {
                    if (string.IsNullOrEmpty(filter))
                        filter = "DocumentTypeID=" + type;
                    else
                        filter += " and DocumentTypeID=" + type;
                }
                else
                {
                    if (m_documentTypes.Count > 0)
                    {
                        string allDocFilter = " DocumentTypeID in (";
                        for (int i = 0; i < m_documentTypes.Count; i++)
                        {
                            if (i == 0)
                                allDocFilter += m_documentTypes[i].id;
                            else
                                allDocFilter += "," + m_documentTypes[i].id;
                        }
                        allDocFilter += ")";
                        if (string.IsNullOrEmpty(filter))
                            filter = allDocFilter;
                        else
                            filter += " and " + allDocFilter;
                    }
                }

                if (date)
                {
                    if (!string.IsNullOrEmpty(filter))
                        filter += " and";
                    filter += " (tranTicks >= " + from.Ticks + " and tranTicks<" + to.Ticks + ")";
                }
                SQLObjectReader<AccountDocument> reader;
                if (includeFullDocument)
                    reader = dspReader.GetObjectReader<AccountDocument>(filter, new string[] { "DocumentData" });
                else
                    reader = dspReader.GetObjectReader<AccountDocument>(filter);
                try
                {
                    while (reader.Read())
                    {
                        AccountDocument doc = reader.Current;
                        if (includeFullDocument)
                        {
                            AccountDocument theDoc = AccountDocument.DeserializeXML((string)reader.CurrentAdditionalData[0], GetDocumentTypeByID(doc.DocumentTypeID).GetTypeObject()) as AccountDocument;
                            theDoc.CoypFrom(doc);
                            doc = theDoc;
                        }
                        if (filterDelegate == null)
                            return doc;
                        if (filterDelegate(doc))
                            return doc;
                    }
                }
                finally
                {
                    if (reader != null && !reader.IsClosed)
                        reader.Close();
                }
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        internal AccountDocument[] GetDocumentsByAccount(int[] accountID1, bool debit1,
                            int[] accountID2, bool debit2,
                            int itemID, int[] docTypes, DateTime fromDate, DateTime toDate, bool fullData, int pageIndex, int pageSize, out int N)
        {
            throw new NotImplementedException("Not implemented for multiple cost center mode");
        }
        //scheduling
        public int getMaterializePendingCount(DateTime time)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = "Select count(*) from {0}.dbo.Document where tranTicks<{1} and scheduled<>0 and materialized=0";
                return (int)reader.ExecuteScalar(string.Format(sql, this.DBName, time.Ticks));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public int[] getMaterializePendingDocumentIDs(DateTime time)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = "Select ID from {0}.dbo.Document where tranTicks<{1} and scheduled<>0 and materialized=0";
                return reader.GetColumnArray<int>(string.Format(sql, this.DBName, time.Ticks), 0);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }


        bool isDetailAccount(Account ac)
        {
            return ac.Code.IndexOf("ITEM-") == 0 || ac.Code.IndexOf("CUST-") == 0;
        }
        string getBatchHTML(AccountTransaction[] batch, bool separateDetails)
        {
            
            var tableFinancial = new BIZNET.iERP.bERPHtmlTable();
            var groupFinancial = tableFinancial.createBodyGroup();

            tableFinancial.styles.Add("width:100%");
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(groupFinancial
            , new BIZNET.iERP.bERPHtmlTableCell("Account Code", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Account Name", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Debit", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Credit", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Note", "LedgerGridHeadCell"));

            var tableFinancialDetail = new BIZNET.iERP.bERPHtmlTable();
            //tableFinancialDetail.CellSpacing = 0;
            tableFinancialDetail.styles.Add("width:100%");
            var groupFinancialDetail = tableFinancialDetail.createBodyGroup();
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(groupFinancialDetail
           , new BIZNET.iERP.bERPHtmlTableCell("Account Code", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Account Name", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Debit", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Credit", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Note", "LedgerGridHeadCell"));

            var tableMaterial = new BIZNET.iERP.bERPHtmlTable();
            tableMaterial.styles.Add("width:100%");
            //tableMaterial.CellSpacing = 0;
            var groupMaterial = tableMaterial.createBodyGroup();
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(groupMaterial
             , new BIZNET.iERP.bERPHtmlTableCell("Account Code", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Account Name", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Debit(Quantity)", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Credit(Quantity)", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Note", "LedgerGridHeadCell"));


            var tableBudget = new BIZNET.iERP.bERPHtmlTable();
            tableBudget.styles.Add("width:100%");
            //tableBudget.CellSpacing = 0;
            var groupBudget = tableBudget.createBodyGroup();
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(groupBudget
            , new BIZNET.iERP.bERPHtmlTableCell("Account Code", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Account Name", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Debit", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Credit", "LedgerGridHeadCell")
            , new BIZNET.iERP.bERPHtmlTableCell("Note", "LedgerGridHeadCell"));

            BIZNET.iERP.bERPHtmlTableCell cell1, cell2;
            int k = 0;
            bool hasFinancial = false;
            bool hasFinancialDetail = false;
            bool hasMaterial = false;
            bool hasBudget = false;

            Dictionary<int, BIZNET.iERP.bERPHtmlTableRow> costCenterRowFinancial = new Dictionary<int, BIZNET.iERP.bERPHtmlTableRow>();
            Dictionary<int, BIZNET.iERP.bERPHtmlTableRow> costCenterRowFinancialDetail = new Dictionary<int, BIZNET.iERP.bERPHtmlTableRow>();
            Dictionary<int, BIZNET.iERP.bERPHtmlTableRow> costCenterRowMaterial = new Dictionary<int, BIZNET.iERP.bERPHtmlTableRow>();
            Dictionary<int, BIZNET.iERP.bERPHtmlTableRow> costCenterRowBudget = new Dictionary<int, BIZNET.iERP.bERPHtmlTableRow>();
            double dbFinTotal = 0, crFinTotal = 0;
            double dbFinDetailTotal = 0, crFinDetailTotal = 0;
            double dbInvTotal = 0, crInvTotal = 0;
            double dbBudgetTotal = 0, crBudgetTotal = 0;
            foreach (TransactionOfBatch t in batch)
            {
                if (AccountBase.AmountEqual(t.Amount, 0))
                    continue;
               
                CostCenterAccount csa = this.GetCostCenterAccount(t.AccountID);
                Account ac = this.GetAccount<Account>(csa.accountID);
                CostCenter cs = this.GetAccount<CostCenter>(csa.costCenterID);
                BIZNET.iERP.bERPHtmlTable table;
                Dictionary<int, BIZNET.iERP.bERPHtmlTableRow> costCenterRow;
                if (t.ItemID == TransactionItem.DEFAULT_CURRENCY)
                {
                    if (separateDetails && isDetailAccount(ac))
                    {
                        hasFinancialDetail = true;
                        table = tableFinancialDetail;
                        costCenterRow = costCenterRowFinancialDetail;
                        if (t.Amount > 0)
                            dbFinDetailTotal += t.Amount;
                        else
                            crFinDetailTotal -= t.Amount;
                    }
                    else
                    {
                        hasFinancial = true;
                        table = tableFinancial;
                        costCenterRow = costCenterRowFinancial;
                        if (t.Amount > 0)
                            dbFinTotal += t.Amount;
                        else
                            crFinTotal -= t.Amount;
                    }
                }
                else if (t.ItemID == TransactionItem.MATERIAL_QUANTITY)
                {
                    hasMaterial = true;
                    table = tableMaterial;
                    costCenterRow = costCenterRowMaterial;
                    if (t.Amount > 0)
                        dbInvTotal += t.Amount;
                    else
                        crInvTotal -= t.Amount;
                }
                else
                {
                    hasBudget= true;
                    table = tableBudget;
                    costCenterRow = costCenterRowBudget;
                    if (t.Amount > 0)
                        dbBudgetTotal += t.Amount;
                    else
                        crBudgetTotal -= t.Amount;
                }
                BIZNET.iERP.bERPHtmlTableRow csRow;
                if (costCenterRow.ContainsKey(cs.id))
                    csRow = costCenterRow[cs.id];
                else
                {
                    csRow = BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(table.groups[0], new BIZNET.iERP.bERPHtmlTableCell(BIZNET.iERP.TDType.body, "Cost Center:" + cs.NameCode, "LedgeSubTotalHeadCell",1, 5));
                    costCenterRow.Add(cs.id, csRow);

                }
                int insertAt = 0;
                int index = 0;
                bool found = false;
                foreach (var row in table.groups[0].rows)
                {
                    if (found)
                    {
                        if (row.cells.Count ==  1)
                            break;
                        index++;
                    }
                    if (!found)
                    {
                        found = csRow == row;
                    }
                    insertAt++;
                }
                BIZNET.iERP.bERPHtmlTableRow entryRow;
                table.groups[0].rows.Insert(insertAt,
                    entryRow = BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(
                    index % 2 == 0 ? "LedgerGridEvenRowCell" : "LedgerGridOddRowCell"
                    , new BIZNET.iERP.bERPHtmlTableCell(ac.Code)
                    , new BIZNET.iERP.bERPHtmlTableCell(ac.Name)
                    , cell1 = new BIZNET.iERP.bERPHtmlTableCell(t.Amount > 0 ? t.Amount.ToString("0,0.00") : "")
                    , cell2 = new BIZNET.iERP.bERPHtmlTableCell(t.Amount < 0 ? (-t.Amount).ToString("0,0.00") : "")
                    , new BIZNET.iERP.bERPHtmlTableCell(t.Note == null ? "" : t.Note)
                    ));

                cell1.styles.Add("text-align:right");
                cell2.styles.Add("text-align:right");
                cell1.styles.Add("border-left:black thin solid");
                cell1.styles.Add("border-right:black thin solid");
                cell2.styles.Add("border-left:black thin solid");
                cell2.styles.Add("border-right:black thin solid");

                k++;
            }
            groupFinancial.rows.Add(
                    BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(
                    "LedgerGridFooterCell" 
                    , new BIZNET.iERP.bERPHtmlTableCell("Total",2)
                    , cell1 = new BIZNET.iERP.bERPHtmlTableCell(dbFinTotal.ToString("0,0.00") )
                    , cell2 = new BIZNET.iERP.bERPHtmlTableCell(crFinTotal.ToString("0,0.00") )
                    , new BIZNET.iERP.bERPHtmlTableCell("")
                    ));

            groupFinancialDetail.rows.Add(
                     BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(
                    "LedgerGridFooterCell"
                    , new BIZNET.iERP.bERPHtmlTableCell("Total", 2)
                    , cell1 = new BIZNET.iERP.bERPHtmlTableCell(dbFinDetailTotal.ToString("0,0.00"))
                    , cell2 = new BIZNET.iERP.bERPHtmlTableCell(crFinDetailTotal.ToString("0,0.00"))
                    , new BIZNET.iERP.bERPHtmlTableCell("")
                    ));
            groupMaterial.rows.Add(
                     BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(
                    "LedgerGridFooterCell" 
                    , new BIZNET.iERP.bERPHtmlTableCell("Total",2)
                    , cell1 = new BIZNET.iERP.bERPHtmlTableCell(dbInvTotal.ToString("0,0.00") )
                    , cell2 = new BIZNET.iERP.bERPHtmlTableCell(crInvTotal.ToString("0,0.00") )
                    , new BIZNET.iERP.bERPHtmlTableCell("")
                    ));

            groupBudget.rows.Add(
                     BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(
                    "LedgerGridFooterCell"
                    , new BIZNET.iERP.bERPHtmlTableCell("Total", 2)
                    , cell1 = new BIZNET.iERP.bERPHtmlTableCell(dbInvTotal.ToString("0,0.00"))
                    , cell2 = new BIZNET.iERP.bERPHtmlTableCell(crInvTotal.ToString("0,0.00"))
                    , new BIZNET.iERP.bERPHtmlTableCell("")
                    ));
            string ret = "";
            if (hasFinancial)
            {
                if (hasMaterial||hasFinancialDetail )
                    ret += "<h3>Financial</h3>";
                ret += BIZNET.iERP.bERPHtmlBuilder.ToString(tableFinancial);
            }
            if (hasFinancialDetail)
            {
                if (hasFinancial||hasMaterial)
                    ret += "<h3>Axillary Transaction</h3>";
                ret += BIZNET.iERP.bERPHtmlBuilder.ToString(tableFinancialDetail);
            }
            if (hasMaterial)
            {
                ret += "<h3>Inventory Transactions</h3>";
                ret += BIZNET.iERP.bERPHtmlBuilder.ToString(tableMaterial);
            }
            if (hasBudget)
            {
                ret += "<h3>Budget Transactoins</h3>";
                ret += BIZNET.iERP.bERPHtmlBuilder.ToString(tableBudget);
            }
            if (!hasMaterial && !hasFinancial && !hasFinancialDetail && !hasBudget)
                ret = "<h3>No accounting entry for this document</h3>";
            return ret;
        }
        public string getSummerizedBatchHTML(
            AccountTransaction[] batch
            , bool separateDetails
            )
        {
            AccountTransaction[] tran = summerizeTransaction(batch);
            return getBatchHTML(tran,separateDetails);
        }
        public AccountTransaction[] summerizeTransaction(AccountTransaction[] batch)
        {
            Dictionary<string, AccountTransaction> _batches = new Dictionary<string, AccountTransaction>();
            Dictionary<int, CostCenterAccountWithDescription> cache = new Dictionary<int, CostCenterAccountWithDescription>();
            
            foreach (AccountTransaction t in batch)
            {
                if(!cache.ContainsKey(t.AccountID))
                    cache.Add(t.AccountID,this.GetCostCenterAccountWithDescription(t.AccountID));
                string key = cache[t.AccountID].code + ":" + t.ItemID;
                if (_batches.ContainsKey(key))
                {
                    AccountTransaction old = _batches[key];
                    old.Amount += t.Amount;
                    if(old.documentID!=t.documentID)
                        old.documentID = -1;
                    old.tranTicks = old.tranTicks < t.tranTicks ? old.tranTicks : t.tranTicks;
                    old.ID = -1;
                    if (old.Note == null || !old.Note.Equals(t.Note))
                        old.Note = "";
                }
                else
                    _batches.Add(key, t.clone());
            }
            AccountTransaction[] tran = new AccountTransaction[_batches.Count];
            _batches.Values.CopyTo(tran, 0);
            Array.Sort(tran, new Comparison<AccountTransaction>(delegate(AccountTransaction a, AccountTransaction b)
                {
                    CostCenterAccountWithDescription ac1 = cache[a.AccountID];
                    CostCenterAccountWithDescription ac2 = cache[b.AccountID];
                    if (a.Amount * b.Amount < 0)
                        return b.Amount.CompareTo(a.Amount);
                    return ac1.code.CompareTo(ac2.code);
                }
                ));
            return tran;
        }
        public TransactionOfBatch[] summerizeTransactionOfBatch(TransactionOfBatch[] batch)
        {
            Dictionary<ulong, TransactionOfBatch> _batches = new Dictionary<ulong, TransactionOfBatch>();
            foreach (TransactionOfBatch t in batch)
            {
                ulong key = t.ItemAccountID;
                if (_batches.ContainsKey(key))
                {
                    TransactionOfBatch old = _batches[key];
                    old.Amount += t.Amount;
                    old.ID = -1;
                    if (old.Note == null || !old.Note.Equals(t.Note))
                        old.Note = "";
                }
                else
                {
                    if(!AccountBase.AmountEqual(t.Amount,0))
                        _batches.Add(key, t.clone());
                }
            }
            TransactionOfBatch[] tran = new TransactionOfBatch[_batches.Count];
            _batches.Values.CopyTo(tran, 0);
            return tran;
        }
        public string getDocumentEntriesHTML(int docID,bool separateDetails)
        {
            AccountDocument doc = this.GetAccountDocument(docID, true);
            if (doc == null)
                return "<h2>Document not found ID:" + docID + "</h2>";
            string batchHTML = getSummerizedBatchHTML(this.GetTransactionsOfDocument(docID),separateDetails);
            DocumentSerial[] refs = getDocumentSerials(docID);
            string refsHtml="";
            foreach(DocumentSerial r in refs)
            {
                refsHtml+=string.Format("<br/><strong>{0}: </strong>{1}", System.Web.HttpUtility.HtmlEncode(getDocumentSerialType(r.typeID).name), System.Web.HttpUtility.HtmlEncode(r.reference));
            }
            string ret="<h2>" + System.Web.HttpUtility.HtmlEncode(this.GetAccountingDocumentType(doc.DocumentTypeID).name) + "</h2>"
                    + "<h2>" + System.Web.HttpUtility.HtmlEncode("Date: " + AccountBase.FormatTime(doc.DocumentDate)) + "</h2>"
                    + "<h2>" + System.Web.HttpUtility.HtmlEncode("Ref: " + doc.PaperRef) + "</h2>"
                    + "<h2>Affected accounts</h2>"
                    + batchHTML
                    + "<h2>More Information</h2>"
                    + this.GetDocumentHandler(doc.DocumentTypeID).GetHTML(doc)
                    ;
            if(!string.IsNullOrEmpty(refsHtml))
            {
                ret += "<h2>References</h2>"
                    + refsHtml;
            }
            return ret;
        }
        public AccountDocument getPreviousDocument(int docTypeID,long t,bool fulldata)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                DataTable dt=reader.GetDataTable(string.Format("Select top 1 ID,tranTicks from {0}.dbo.Document where tranTicks<={1} and DocumentTypeID={2} order by tranTicks desc", this.DBName, t, docTypeID));
                if (dt.Rows.Count==0)
                    return null;
                int id = (int)dt.Rows[0][0]; 
                long ticks = (long)dt.Rows[0][1];
                return GetAccountDocument(id, fulldata);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public AccountDependancy[] getAccountDependancy(int documentID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<AccountDependancy>("documentID=" + documentID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public string getMachineryNumber(int cashaccountid)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                DataTable dt = reader.GetDataTable(string.Format("SELECT machineNumber FROM Main_2006.[dbo].[CashRegisterMachineNumber] WHERE [cashAccountID] = {0} ", cashaccountid));
                if (dt.Rows.Count == 0)
                    return "";
                return (string)dt.Rows[0][0];
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
    }
}