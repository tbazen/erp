using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        public const string DOCUMENT_TYPED_REFERENCE_FORMAT = @"[\w\d\-\/\\_&]{1,}";
        private void deleteAccountDocument(int docID,bool forUpdate)
        {
            docDepClearDependancy(docID);
            if(!forUpdate)
                docDepDocumentUpdated(docID);
            dspWriter.Delete(this.DocumentTableDB, "Document", "ID=" + docID);
        }
        void DeleteAccountDocument(int AID,int documentID,bool delDoc,bool forUpdate)
        {
            INTAPS.ClientServer.ObjectFilters.filterDelete(AID, typeof(AccountDocument), documentID,delDoc);
            lock (dspWriter)
            {
                AccountDocument doc = this.GetAccountDocumentInternal(dspWriter, documentID, false);
                if (delDoc)
                {
                    if (doc.reversed)
                        throw new ServerUserMessage("It is not allowed to delete reversed documents.");
                    if (GetDocumentTypeByID(doc.DocumentTypeID).system)
                        throw new ServerUserMessage("It is not allowed to delete system documents. Try document reversal");
                }
                DeleteTransaction(AID,delDoc, doc,forUpdate);
            }
        }
        public void DeleteAccountDocument(int AID,int documentID,bool forUpdate)
        {
            lock (dspWriter)
            {
                if (DocumentExists(documentID))
                {
                    DeleteAccountDocument(AID, documentID, true, forUpdate);
                }
            }
        }
        public void ReverseDocument(int AID, int documentID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    AccountDocument doc = GetAccountDocumentInternal(dspWriter, documentID, true);
                    TransactionReversalDocument tr = new TransactionReversalDocument(doc);
                    tr.DocumentDate = doc.DocumentDate;
                    ReverseDocument(AID, tr);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public int RegisterDocumetType(DocumentType docType)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    Type t = docType.GetTypeObject();
                    if (t == null)
                        throw new ServerUserMessage("Couldn't load assembly for " + docType.documentClass + "," + docType.assemblyName);
                    DocumentType existing = GetDocumentTypeByTypeNoException(t);
                    if (docType.id == -1)
                    {
                        if (existing != null)
                            throw new ServerUserMessage("Document type " + docType.documentClass + "," + docType.assemblyName + " already registered. ID=" + existing.id);
                        int id = INTAPS.ClientServer.AutoIncrement.GetKey(this.BDEName + ".DocumentType");
                        docType.id = id;
                        dspWriter.InsertSingleTableRecord<DocumentType>(this.DocumentTableDB, docType);
                        m_documentTypes.Add(docType);
                    }
                    else
                    {
                        if (existing.id != docType.id)
                            throw new ServerUserMessage("Document type " + docType.documentClass + "," + docType.assemblyName + " already registered. ID=" + existing.id);
                        int index = m_documentTypes.IndexOf(existing);
                        dspWriter.UpdateSingleTableRecord(this.DocumentTableDB, docType);
                        m_documentTypes[index] = docType;
                    }
                    dspWriter.CommitTransaction();
                    return docType.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
            
        }
        void DeleteDocumentType(int docTypeID)
        {
            lock (dspWriter)
            {
                DocumentType existing = GetDocumentTypeByID(docTypeID);
                dspWriter.DeleteSingleTableRecrod<DocumentType>(this.DBName, docTypeID);
                m_documentTypes.Remove(existing);
            }
        }
        public void UpdateAccountDocumentData(int AID,AccountDocument _doc)
        {
            lock (dspWriter)
            {
                logDocumentPreviousVersion(AID, _doc.AccountDocumentID);
//                dspWriter.Update(this.DocumentTableDB, "Document", new string[] { "DocumentData","__AID" }, new object[] { _doc.ToXML(),AID}, "ID=" + _doc.AccountDocumentID);
                dspWriter.UpdateSingleTableRecord(this.DocumentTableDB, _doc, new string[] { "DocumentData","__AID" }, new object[] { _doc.ToXML(),AID});
            }
        }
        public void UpdateAccountDocumentNotes(AccountDocument doc, TransactionOfBatch[] trans)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    dspWriter.UpdateSingleTableRecord<AccountDocument>(m_DBName, doc
                            , new string[] { "DocumentData" }, new object[] { doc.ToXML() }
                            );

                    if (trans != null)
                    {
                        TransactionOfBatch[] oldTran = GetTransactionsOfDocument(doc.AccountDocumentID);
                        if (oldTran.Length != trans.Length)
                            throw new ServerUserMessage("Transaction length do not mactch for update account document notes methd.");

                        for (int i = 0; i < oldTran.Length; i++)
                        {
                            dspWriter.Update(m_DBName, "AccountTransaction"
                            , new string[] { "Note" }
                , new object[] { trans[i].Note }
                , "id=" + trans[i].ID);
                        }
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public void RescheduleAccountDocument(int AID,int docID, DateTime time)
        {
            AccountDocument doc = GetAccountDocument(docID, true);
            if (!doc.scheduled)
                throw new ServerUserMessage("Document is not scheduled");
            if (doc.materialized)
                throw new ServerUserMessage("Document is already materialized");
            if(time.Subtract(DateTime.Now).TotalSeconds<=FUTURE_TOLERANCE_SECONDS)
                throw new ServerUserMessage("The reschedule date is not in the future.");
            doc.DocumentDate = time;
            PostGenericDocument(AID,doc);
        }

        public void MaterilizeDocument(int AID, AccountDocument matDoc)
        {
            AccountDocument doc = GetAccountDocument(matDoc.AccountDocumentID, true);
            if (!doc.scheduled)
                throw new ServerUserMessage("Document is not scheduled");
            if (doc.materialized)
                throw new ServerUserMessage("Document is already materialized");
            matDoc.scheduled = true;
            matDoc.materialized = true;
            PostGenericDocument(AID, matDoc);
        }

    }
}