using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        public int GetTransactionNumber()
        {
            return INTAPS.ClientServer.AutoIncrement.GetCurrentValue("AuditID");
        }        
    }
}