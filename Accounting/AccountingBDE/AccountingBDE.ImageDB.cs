using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;
using System.Drawing.Imaging;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE : BDEBase
    {
        public ImageItem LoadImage(int imageID, int imageIndex, bool full, int Width, int Height)
        {
            INTAPS.RDBMS.SQLHelper helper = this.GetReaderHelper();
            helper.setReadDBUnrestorable(ApplicationServer.MainDB);
            try
            {
                ImageItem ret = new ImageItem();

                ret.itemIndex = imageIndex;
                ret.imageID = imageID;
                DataTable data = helper.GetDataTable("Select data,title from " + ApplicationServer.MainDB + ".dbo.ImageItem where imageID=" + imageID + " and itemIndex=" + imageIndex);
                foreach (DataRow row in data.Rows)
                {
                    ret.title = (string)row[1];
                    byte[] imgdata = (byte[])row[0];
                    if (full)
                    {
                        ret.imageData = imgdata;
                        return ret;
                    }

                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imgdata);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                    ms.Dispose();

                    System.Drawing.Bitmap bm = new System.Drawing.Bitmap(Width, Height);
                    System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bm);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    if (img.Width * Height > Width * img.Height)
                    {
                        int drawHeight = img.Height * Width / img.Width;
                        g.DrawImage(img, new System.Drawing.Rectangle(0, (Height - drawHeight) / 2, Width, drawHeight));
                    }
                    else
                    {
                        int drawWidth = img.Width * Height / img.Height;
                        g.DrawImage(img, new System.Drawing.Rectangle((Width - drawWidth) / 2, 0, drawWidth, Height));
                    }
                    g.Dispose();

                    System.Drawing.Imaging.ImageCodecInfo ici = GetencoderInfo("image/jpeg");
                    EncoderParameters eps = new EncoderParameters(1);
                    eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (int)90);
                    System.IO.MemoryStream strm = new System.IO.MemoryStream();
                    bm.Save(strm, ImageFormat.Jpeg);
                    ret.imageData = new byte[strm.Length];
                    Array.Copy(strm.GetBuffer(), ret.imageData, strm.Length);
                    return ret;
                }
                return null;
            }
            finally
            {
                this.ReleaseHelper(helper);
            }
        }
        public void GetImageSize(int imageID, int imageIndex, out int width, out int height, out string title)
        {
            INTAPS.RDBMS.SQLHelper helper = this.GetReaderHelper();
            try
            {
                helper.setReadDBUnrestorable(ApplicationServer.MainDB);
                DataTable data = helper.GetDataTable("Select data,title from ImageData where imageID=" + imageID + " and itemIndex=" + imageIndex);
                foreach (DataRow row in data.Rows)
                {
                    title = (string)row[1];
                    byte[] imgdata = (byte[])row[0];

                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imgdata);
                    System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                    width = img.Width;
                    height = img.Height;
                    ms.Dispose();
                    return;
                }
                title = null;
                width = -1;
                height = -1;
            }
            finally
            {
                this.ReleaseHelper(helper);
            }
        }
        public ImageItem[] GetImageItems(int imageID)
        {
            INTAPS.RDBMS.SQLHelper helper = this.GetReaderHelper();
            try
            {
                helper.setReadDBUnrestorable(ApplicationServer.MainDB);
                return helper.GetSTRArrayByFilter<ImageItem>("imageID=" + imageID);
            }
            finally
            {
                this.ReleaseHelper(helper);
            }
        }
    }
}