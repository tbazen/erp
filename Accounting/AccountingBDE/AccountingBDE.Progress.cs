using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE : BDEBase
    {
        protected bool _logProgress = false;
        protected List<double> _progOffset=new List<double>();
        protected List<double> _childWeight = new List<double>();
        protected bool _stopProgress=false;
        protected List<string> _stageMessage = new List<string>();


        public void clearProgress()
        {
            _progOffset.Clear();
            _childWeight.Clear();
            _stageMessage.Clear();
        }
        public int PushProgress(string msg)
        {
            lock (_childWeight)
            {
                _logProgress = true;
                _stageMessage.Add(msg);
                _progOffset.Add(0.0);
                _childWeight.Add(1.0);
                return _progOffset.Count - 1;
            }
        }

        public void SetChildWeight(double w)
        {
            lock (_childWeight)
            {
                int head = _stageMessage.Count - 1;
                if (head < 0)
                {
                    ApplicationServer.EventLoger.Log("Progres stack underflow");
                    return;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("Progres stack overflow");
                    return;
                }
                _childWeight[head] = w;
            }
        }

        public void PopProgress()
        {
            lock (_childWeight)
            {
                PopProgress(_stageMessage.Count - 1);
            }
        }
        public void PopProgress(int upto)
        {
            lock (_childWeight)
            {
                int head = _progOffset.Count - 1;

                if (head < 0)
                {
                    ApplicationServer.EventLoger.Log("Progres stack underflow");
                    return;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("Progres stack overflow");
                    return;
                }
                while (head >= upto)
                {
                    _stageMessage.RemoveAt(head);
                    _progOffset.RemoveAt(head);
                    _childWeight.RemoveAt(head);
                    head--;
                    if (head >= 0)
                    {
                        if (head >= _progOffset.Count )
                        {
                            ApplicationServer.EventLoger.Log("_progOffset index inconsistency");
                            return;
                        }
                        if (head >= _childWeight.Count)
                        {
                            ApplicationServer.EventLoger.Log("_childWeight index inconsistency");
                            return;
                        }
                        _progOffset[head] += _childWeight[head];
                    }
                }
            }
        }
        public double GetProccessProgress(out string message)
        {
            
                int head = _progOffset.Count - 1;

                if (head < 0)
                {
                    message = "";
                    return 1;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("Progres stack overflow");
                    message = "";
                    return 0;
                }
                message = _stageMessage[head];
                double ret = 0;
                for (int i = head; i >= 0; i--)
                {
                    ret = ret * _childWeight[i] + _progOffset[i];
                }
                if (ret > 1.0)
                    return 1.0;
                if (ret < 0.0)
                    return 0.0;
                return ret;
            
        }

        public void StopLongProccess()
        {
            lock (_childWeight)
            {
                _stopProgress = true;
            }
        }
        public void SetProgress(string message, double prog)
        {
            lock (_childWeight)
            {
                int head = _progOffset.Count - 1;

                if (head < 0)
                {
                    ApplicationServer.EventLoger.Log("Progres stack underflow");
                    return;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("Progres stack overflow");
                    return;
                }
                if (head >= _progOffset.Count)
                {
                    ApplicationServer.EventLoger.Log("_progOffset index inconsistency");
                    return;
                }
                _stageMessage[head] = message;
                _progOffset[head] = prog;
            }
        }

        public void SetProgress(string message)
        {
            lock (_childWeight)
            {
                int head = _progOffset.Count - 1;

                if (head < 0)
                {
                    ApplicationServer.EventLoger.Log("Progres stack underflow");
                    return;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("Progres stack overflow");
                    return;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("_stageMessage index inconsistency");
                    return;
                }

                _stageMessage[head] = message;
            }
        }
        
        public void SetProgress(double prog)
        {
            lock (_childWeight)
            {
                int head = _progOffset.Count - 1;
                if (head < 0)
                {
                    ApplicationServer.EventLoger.Log("Progress stack underflow");
                    return;
                }
                if (head >= _stageMessage.Count)
                {
                    ApplicationServer.EventLoger.Log("Progress stack overflow");
                    return;
                }
                if (head >= _progOffset.Count)
                {
                    ApplicationServer.EventLoger.Log("_progOffset index inconsistency");
                    return;
                }
                _progOffset[head] = prog;
            }
        }
    }
}
