using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    partial class AccountingBDE
    {
        Dictionary<int, IReportProcessor> _ReportProcessors=null;
        ReportType[] _m_ReportTypes=null;
        ReportType[] m_ReportTypes
        {
            get
            {
                if (_m_ReportTypes == null)
                    InitializeReporting();
                return _m_ReportTypes;
            }
        }
        
        public Dictionary<int, IReportProcessor> ReportProcessors
        {
            get {
                if (_ReportProcessors == null)
                    InitializeReporting();
                return _ReportProcessors; 
            }
        }
        public string webServerClientAddress
        {
            get
            {
                string httpEndPoint = System.Configuration.ConfigurationManager.AppSettings["httpEndPoint"];
                if (string.IsNullOrEmpty(httpEndPoint))
                    return null;
                httpEndPoint=httpEndPoint.TrimEnd('/');
                int i1 = httpEndPoint.IndexOf("http://", 0, System.StringComparison.CurrentCultureIgnoreCase);
                if (i1 == -1)
                    i1 = 0;
                else
                    i1 += "http://".Length;
                int i2 = httpEndPoint.IndexOf(":", i1 + 1, System.StringComparison.CurrentCultureIgnoreCase);
                if (i2 == -1)
                    i2 = httpEndPoint.Length;
                string dnsName = httpEndPoint.Substring(i1, i2 - i1);
                if (dnsName == "*")
                {
                    httpEndPoint = httpEndPoint.Replace("*", System.Net.Dns.GetHostName());
                }
                return httpEndPoint;
            }

        }
        void InitializeReporting()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                _m_ReportTypes = dspReader.GetSTRArrayByFilter<ReportType>(null);
                _ReportProcessors = new Dictionary<int, IReportProcessor>();
                foreach (ReportType rt in _m_ReportTypes)
                {
                    Type t = Type.GetType(rt.serverClass);
                    if (t == null)
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, "Report type " + rt.serverClass + " failed to load. Check avialability of assembly");
                        continue;
                    }
                    System.Reflection.ConstructorInfo ci = t.GetConstructor(new Type[] { });
                    if (ci == null)
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, "Report type " + rt.serverClass + " don't have parameterless constuctor.");
                        continue;
                    }
                    try
                    {
                        IReportProcessor rep = (IReportProcessor)ci.Invoke(new object[] { });
                        _ReportProcessors.Add(rt.id, rep);
                    }
                    catch (Exception ex)
                    {
                        ApplicationServer.EventLoger.LogException("Error loading report type " + rt.serverClass, ex);
                    }

                }
                foreach (KeyValuePair<int, IReportProcessor> kv in _ReportProcessors)
                {
                    ((ReportProcessorBase)kv.Value).vars.Add("conFinance", ReportProcessorBase.CreateConnection(base.BDEName));
                    ((ReportProcessorBase)kv.Value).vars.Add("webserver", new INTAPS.Evaluator.EData(webServerClientAddress));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new INTAPS.Accounting.BDE.AFAccount(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new TodayFunction());
                    ((ReportProcessorBase)kv.Value).funcs.Add(new GetTicksFunction());
                    ((ReportProcessorBase)kv.Value).funcs.Add(new TimeSpanText());
                    ((ReportProcessorBase)kv.Value).funcs.Add(new INTAPS.Evaluator.JSONObject()); 
                    ((ReportProcessorBase)kv.Value).funcs.Add(new GetBalance(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new GetBalanceSingleValue(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new GetFlow(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new GetFlowBal(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new GetIndent(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new ChartOfAcccountFunction(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new TrialBalanceFunction(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new FlowTableFunction(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new TransactionRegisterFunction(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new DocumentListFunction(this));
                    ((ReportProcessorBase)kv.Value).funcs.Add(new EvaluateEHTMLFunction(this));

                }
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error loading report definations", ex);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ReportDefination[] GetReports(int reportTypeID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<ReportDefination>("reportTypeID=" + reportTypeID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ReportType[] GetAllReportTypes()
        {
            return m_ReportTypes;
        }
        public string EvaluateEHTML(int reportTypeID, EHTMLData data, object[] parameter,out string headerItems)
        {
            PushProgress("Evaluating " + reportTypeID);
            try
            {
                EHTML eh = new EHTML(data);
                IReportProcessor p = ReportProcessors[reportTypeID];
                p.PrepareEvaluator(eh, parameter);
                return eh.Evaluate(out headerItems);
            }
            finally
            {
                PopProgress();
            }
        }
        public string EvaluateEHTML(int reportID,object[] parameter,out string headerItems)
        {
            ReportDefination def= GetEHTMLDAta(reportID); 
            EHTMLData data = def.formula;
            return EvaluateEHTML(def.reportTypeID, data, parameter,out headerItems);
        }
        // BDE exposed
        public string EvaluateEHTML(string code, object[] parameter, out string headerItems)
        {
            ReportDefination def = GetReportDefination(code);
            if (def == null)
                throw new ServerUserMessage("HTML formula set not found code:" + code);
            EHTMLData data = def.formula;
            return EvaluateEHTML(def.reportTypeID, data, parameter,out headerItems);
        }
        public ReportDefination GetEHTMLDAta(int reportID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReportDefination[] def = dspReader.GetSTRArrayByFilter<ReportDefination>("id=" + reportID);
                if (def.Length == 0)
                    throw new ServerUserMessage("Report defination not found in the database. Report ID:"+reportID);
                return def[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public ReportDefination GetReportDefination(string code)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetReportDefinationInternal(dspReader, code);

            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public ReportDefination GetReportDefinationInfo(string code)
        {
            ReportDefination ret = GetReportDefination(code);
            if (ret == null)
                return null;
            ret.formula = null;
            return ret;
        }
        private ReportDefination GetReportDefinationInternal(INTAPS.RDBMS.SQLHelper dsp, string code)
        {
            ReportDefination[] ret = dsp.GetSTRArrayByFilter<ReportDefination>(string.Format("code='{0}'", code));
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        public int GetReportDefID(int reportTypeID, string name)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object val = dspReader.ExecuteScalar("Select id from ReportDefination where reportTypeID=" + reportTypeID + " and name='" + name+"'");
                if (!(val is int))
                    throw new ServerUserMessage("Report defination not found in the database. " + reportTypeID + " , " + name);
                return (int)val;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public int GetReportDefID(string code)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object val = dspReader.ExecuteScalar("Select id from ReportDefination where code='" + code+ "'");
                if (!(val is int))
                    return -1;
                return (int)val;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public INTAPS.Evaluator.FunctionDocumentation[] GetFunctionDocumentations(int reportTypeID)
        {
            EHTML eh = new EHTML(new EHTMLData());
            IReportProcessor p = ReportProcessors[reportTypeID];
            p.PrepareEvaluator(eh, p.GetDefaultPars());
            return eh.GetAvialableFunctions();
        }
        public INTAPS.Evaluator.EData EvaluateEHTML(int reportTypeID, EHTMLData data, object[] parameter, string symbol)
        {
            EHTML eh = new EHTML(data);
            IReportProcessor p = ReportProcessors[reportTypeID];
            p.PrepareEvaluator(eh, parameter);
            return eh.Evaluate(symbol.ToUpper());
        }
        public EHTML getEHTMLEvaluator(int reportTypeID, EHTMLData data, object[] parameter)
        {
            EHTML eh = new EHTML(data);
            IReportProcessor p = ReportProcessors[reportTypeID];
            p.PrepareEvaluator(eh, parameter);
            return eh;
        }
        public ReportDefination[] GetReportByCategory(int catID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetReportByCategoryInternal(catID, dspReader);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        private static ReportDefination[] GetReportByCategoryInternal(int catID, INTAPS.RDBMS.SQLHelper dspReader)
        {
            return dspReader.GetSTRArrayByFilter<ReportDefination>("categoryID=" + catID);
        }
        public ReportCategory[] GetChildReportCategories(int pid)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<ReportCategory>("pid=" + pid);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        
    }
    
}
