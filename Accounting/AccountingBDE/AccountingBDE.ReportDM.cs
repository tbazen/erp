using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    partial class AccountingBDE
    {
        public int SaveEHTMLData(int AID,ReportDefination r)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    bool create = r.id == -1;
                    if (create)
                        r.id = AutoIncrement.GetKey("reportDefinationID");
                    string sql;
                    if (!string.IsNullOrEmpty(r.code))
                    {
                        if (r.id == -1)
                            sql = string.Format("Select count(*) from {0}.dbo.ReportDefination where code='{1}'", this.DBName, r.code);
                        else
                            sql = string.Format("Select count(*) from {0}.dbo.ReportDefination where code='{1}' and id<>{2}", this.DBName, r.code,r.id);
                        if ((int)dspWriter.ExecuteScalar(sql) > 0)
                            throw new ServerUserMessage("Code " + r.code + " already used");
                    }
                    if (create)
                        dspWriter.InsertSingleTableRecord(m_DBName, r, new string[] { "__AID" }, new object[] { AID });
                    else
                    {
                        logOldReportDefination(AID, r.id);
                        dspWriter.UpdateSingleTableRecord(m_DBName, r, new string[] { "__AID" }, new object[] { AID });
                    }
                    dspWriter.CommitTransaction();
                    return r.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public int SaveReportCategory(int AID,ReportCategory cat)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    if (cat.id < 1)
                    {
                        int catid = AutoIncrement.GetKey("Accounting.ReportCategoryID");
                        cat.id = catid;
                        dspWriter.InsertSingleTableRecord(m_DBName, cat);
                    }
                    else
                    {
                        int catID = cat.id;
                        logOldReportCategory(AID, catID);
                        dspWriter.UpdateSingleTableRecord(m_DBName, cat);
                    }
                    dspWriter.CommitTransaction();
                    return cat.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        private void logOldReportCategory(int AID, int catID)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.ReportDefination", "id=" + catID);
        }
        public void DeleteReportCategory(int AID,int catID)
        {

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    try
                    {
                        ReportDefination[] defs = GetReportByCategoryInternal(catID, dspWriter);
                        foreach (ReportDefination d in defs)
                        {
                            logOldReportDefination(AID, d.id);
                            dspWriter.DeleteSingleTableRecrod<ReportDefination>(m_DBName, d.id);
                        }
                        logOldReportCategory(AID, catID);
                        dspWriter.DeleteSingleTableRecrod<ReportCategory>(m_DBName, catID);
                        dspWriter.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        dspWriter.RollBackTransaction();
                        throw ex;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public void DeleteReport(int AID, int reportID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    logOldReportDefination(AID, reportID);
                    dspWriter.DeleteSingleTableRecrod<ReportDefination>(this.DBName, reportID, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        private void logOldReportDefination(int AID, int reportID)
        {
            dspWriter.logDeletedData(AID,this.DBName+".dbo.ReportDefination","id=" + reportID);
        }
    }
}
