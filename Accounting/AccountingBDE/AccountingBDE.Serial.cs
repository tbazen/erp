using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        public SerialBatch[] GetAllActiveSerialBatches()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                SerialBatch[] ret= dspReader.GetSTRArrayByFilter<SerialBatch>("active=1");
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public SerialBatch GetSerialBatch(int batchID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                SerialBatch[] _ret= dspReader.GetSTRArrayByFilter<SerialBatch>("active=1 and id="+batchID);
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public int GetNextSerial(int batchID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                SerialBatch[] sb = dspReader.GetSTRArrayByFilter<SerialBatch>("id=" + batchID);
                if (sb.Length == 0)
                    throw new ServerUserMessage("The serial batch not found in the database.");
                object max = dspReader.ExecuteScalar("Select max(val) from "+this.DBName+".dbo.UsedSerial where batchID="+batchID);
                if (max is int)
                {
                    int ret = Math.Max((int)max+1,sb[0].serialFrom);
                    if (ret > sb[0].serialTo)
                        throw new ServerUserMessage("Serial numbers are used up.");
                    return ret;
                }
                return sb[0].serialFrom;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public void assertValidAccount<AccountType>(int accountID, string message) where AccountType : AccountBase, new()
        {
            if (GetAccount<AccountType>(accountID) == null)
                throw new ServerUserMessage(message);
        }
        public void assertValidCostCenterAccount(int accountID, string message)
        {
            if (GetCostCenterAccount(accountID) == null)
                throw new ServerUserMessage(message);
        }
    }
}
