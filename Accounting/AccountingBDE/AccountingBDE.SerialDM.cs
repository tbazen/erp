using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        public int CreateBatch(int AID,SerialBatch batch)
        {
            lock (dspWriter)
            {
                
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    batch.id = INTAPS.ClientServer.AutoIncrement.GetKey("Account.SerialBatchID");
                    dspWriter.InsertSingleTableRecord(m_DBName, batch, new string[]{"__AID"},new object[]{AID});
                    dspWriter.CommitTransaction();
                    return batch.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void DeleteSerialBatch(int AID, int id)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.InsertSingleTableRecord(m_DBName, GetSerialBatch(id), new string[] { "__AID" }, new object[] { AID }, "SerialBatch_Deleted");
                    dspWriter.DeleteSingleTableRecrod<SerialBatch>(m_DBName, id);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void UseSerial(int AID,UsedSerial ser)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    SerialBatch[] sb = dspWriter.GetSTRArrayByFilter<SerialBatch>("id=" + ser.batchID);
                    if (sb.Length == 0)
                        throw new ServerUserMessage("The serial batch not found in the database.");

                    SerialBatch batch = sb[0];
                    if (!batch.active)
                        throw new ServerUserMessage("The serial numbers are used up.");
                    ser.UseSerial(m_DBName, AID, dspWriter, batch);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public UsedSerial GetSerial(int batchID,int val)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                UsedSerial[] used = dspReader.GetSTRArrayByFilter<UsedSerial>("batchID=" + batchID + " and val=" + val);
                if (used.Length == 1)
                    return used[0];
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
    }
}
