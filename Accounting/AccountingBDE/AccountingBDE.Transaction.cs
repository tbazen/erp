using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;
using System.Data.SqlClient;

namespace INTAPS.Accounting.BDE
{
    
    public partial class AccountingBDE
    {
        public class RecordTransactionParameters
        {
            public AccountDocument document;
            public DateTime docDate;
            public TransactionOfBatch[] batch;
            public bool allowInsert;
        }
        public void VerifyBookBalance(INTAPS.RDBMS.SQLHelper helper)
        {
            return;
            //int[] roots = helper.GetColumnArray<int>("Select ID from " + this.DBName + ".dbo.Account where PID=-1", 0);
            //double tdb = 0, tcr = 0;
            //foreach (int a in roots)
            //{
            //    int csa = GetCostCenterAccountID(CostCenter.ROOT_COST_CENTER, a);
            //    foreach (AccountBalance acbal in GetEndBalances(csa))
            //    {
            //        tdb += acbal.TotalDebit;
            //        tcr += acbal.TotalCredit;
            //    }
            //}
            //if (!AccountBase.AmountEqual(tdb, tcr))
            //    throw new ServerUserMessage("Book is not balanced");

        }
        public int VerifyTransactionConsistency(INTAPS.RDBMS.SQLHelper helper, int accountID, int itemID, out string error)
        {
            string sql = string.Format("Select TransactionType,TotalDbBefore,TotalCrBefore,Amount,ID,tranTicks,OrderN from {0}.dbo.AccountTransaction where AccountID={1} and ItemID={2} order by OrderN"
                , this.DBName, accountID, itemID);
            double totaldebit = 0, totalcredit = 0;
            SqlDataReader reader = helper.ExecuteReader(sql);
            try
            {
                DateTime prevTime = DateTime.MinValue;
                int tid = 0;
                int orderN = 1;
                while (reader.Read())
                {
                    TransactionOfBatch tran = new TransactionOfBatch(accountID, (double)reader[3], "");
                    tran.TransactionType = (TransactionType)reader[0];
                    tran.OrderN = (int)reader[6];
                    double debBefore = (double)reader[1];
                    double crdBefore = (double)reader[2];
                    tid = (int)reader[4];
                    if (!AccountBase.AmountEqual(totaldebit, debBefore)
                        || !AccountBase.AmountEqual(totalcredit, crdBefore))
                    {
                        error = "Balance sequence mismatch";
                        return tid;
                    }

                    long tranTicks = (long)reader[5];
                    if (tranTicks < prevTime.Ticks)
                    {
                        error = "Time sequence error";
                        return tid;
                    }
                    if (orderN != tran.OrderN)
                    {
                        error = "Order numebr sequence error";
                        return tid;
                    }
                    tran.TotalCrBefore = totalcredit;
                    tran.TotalDbBefore = totaldebit;
                    totalcredit = tran.NewCredit;
                    totaldebit = tran.NewDebit;
                    orderN++;

                }
                reader.Close();
                AccountBalance bal = GetEndBalance(accountID, itemID);
                if (!AccountBase.AmountEqual(bal.TotalCredit, totalcredit) || !AccountBase.AmountEqual(bal.TotalDebit, totaldebit))
                {
                    error = "Main Balance mismatch";
                    return tid;
                }
                if (bal.LastOrderN != orderN - 1)
                {
                    error = "Last order number mismatch";
                    return tid;
                }

                error = null;
                return -1;
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                    reader.Close();
            }
        }

        private void FixTransAboveNew(int AccountID, int ItemID, int orderN, double NewDebit, double NewCredit, int NewOrderN)
        {

            //get maximum debit/credit transaction order number for transaction
            string sql = "Select min(OrderN) from "
                + this.DBName
                + ".dbo.AccountTransaction where AccountID=" + AccountID
                + " and ItemID=" + ItemID
                + " and OrderN>=" + orderN
                + " and TransactionType=" + ((int)TransactionType.Opening);
            object openingOrdern = dspWriter.ExecuteScalar(sql); 
            sql = "Select max(OrderN) from "
                + this.DBName
                + ".dbo.AccountTransaction where AccountID=" + AccountID
                + " and ItemID=" + ItemID
                + " and OrderN>=" + orderN
                + " and TransactionType=" + ((int)TransactionType.DebitCredit);
            object upperOrdernDbCr = openingOrdern is int?openingOrdern: dspWriter.ExecuteScalar(sql);
            
            //get maximum order number for all types of transaction
            sql = "Select max(OrderN) from "
                + this.DBName
                + ".dbo.AccountTransaction where AccountID=" + AccountID
                + " and ItemID=" + ItemID
                + " and OrderN>=" + orderN;
            object upperOrdernAll = dspWriter.ExecuteScalar(sql);

            sql = "Select min(OrderN) from " + this.DBName
                + ".dbo.AccountTransaction where AccountID=" + AccountID
                + " and ItemID=" + ItemID
                + " and OrderN>=" + orderN;
            //TENATIVE COMMENT
                //+ " and TransactionType=" + ((int)TransactionType.DebitCredit);
            object lowerOrdern = dspWriter.ExecuteScalar(sql);
            long lastTranTimeDbCr;
            if (upperOrdernDbCr is int)
            {
                int min = (int)lowerOrdern;
                int maxDbCr = (int)upperOrdernDbCr;
                sql = "Select tranTicks from " + this.DBName
                    + ".dbo.AccountTransaction where AccountID=" + AccountID
                    + " and ItemID=" + ItemID + " and OrderN=" + maxDbCr;
                lastTranTimeDbCr = (long)dspWriter.ExecuteScalar(sql);
                sql = "Select TotalDbBefore from "
                    + this.DBName + ".dbo.AccountTransaction where AccountID=" + AccountID
                    + " and ItemID=" + ItemID + " and OrderN=" + min;
                double oldDebit = (double)dspWriter.ExecuteScalar(sql);
                sql = "Select TotalCrBefore from " + this.DBName
                    + ".dbo.AccountTransaction where AccountID=" + AccountID
                    + " and ItemID=" + ItemID + " and OrderN=" + min;
                double oldCredit = (double)dspWriter.ExecuteScalar(sql);
                double deltaDB = NewDebit - oldDebit;
                double deltaCR = NewCredit - oldCredit;
               sql = @"Update " + this.DBName + ".dbo.AccountTransaction set TotalDbBefore=TotalDbBefore+" + deltaDB
                    + ", TotalCrBefore=TotalCrBefore+" + deltaCR
                    + " where AccountID=" + AccountID
                    + " and ItemID=" + ItemID + " and OrderN>=" + min + " and OrderN<=" + maxDbCr;
                dspWriter.ExecuteNonQuery(sql);
                
                AccountBalance bal = GetEndBalance(AccountID, ItemID);
                if (bal.LastOrderN == 0)
                    throw new ServerUserMessage("BUG:Account has no balance set but FixTransAbove is called");
                if (bal.LastOrderN == maxDbCr)
                {
                    dspWriter.ExecuteNonQuery("UPDATE " + this.DBName + @".dbo.[AccountBalance]
                        SET [TotalCredit] = [TotalCredit]+" + deltaCR + @"
                        ,[TotalDebit] = [TotalDebit]+" + deltaDB + @"
                        WHERE accountID=" + AccountID + " and itemID=" + ItemID);
                }
            }
            if (upperOrdernAll is int)
            {
                int  deltaOlderN = NewOrderN + 1 - (int)lowerOrdern;
                if (deltaOlderN != 0)
                {
                    sql = @"Update " + this.DBName + ".dbo.AccountTransaction set OrderN=OrderN+" + deltaOlderN
                    + " where AccountID=" + AccountID
                    + " and ItemID=" + ItemID + " and OrderN>=" + lowerOrdern;
                    dspWriter.ExecuteNonQuery(sql);

                    dspWriter.ExecuteNonQuery("UPDATE " + this.DBName + @".dbo.[AccountBalance]
                        SET [LastOrderN] =[LastOrderN]+ " + deltaOlderN 
                        +" WHERE accountID=" + AccountID + " and itemID=" + ItemID);
                }
            }
        }
        bool _updateLedger = true;
        public bool UpdateLedger
        {
            get { return _updateLedger; }
            set { _updateLedger = value; }
        }        
        private void UpdateAccountBalance(DateTime time, ulong itemAccountID, double NewDebit, double NewCrdit, int InsertAt, int NewOrderN)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    int AccountID;
                    int ItemID;
                    AccountBalance.Split(itemAccountID, out AccountID, out ItemID);
                    //if insertion point set
                    if (InsertAt != -1)
                    {
                        //fix the transaction above the current one
                        if (_updateLedger)
                            FixTransAboveNew(AccountID, ItemID, InsertAt, NewDebit, NewCrdit, NewOrderN);
                    }
                    else
                    {
                        //check weather a balance entry exist for the account-item id pair.
                        bool Insert = ((int)dspWriter.ExecuteScalar("Select count(*) from " + this.DBName + ".dbo.AccountBalance where AccountID=" + AccountID + " and ItemID=" + ItemID)) == 0;
                        if (Insert)
                        {
                            dspWriter.Insert(m_DBName, "AccountBalance", new string[] { "TotalDebit", "TotalCredit", "LastOrderN", "tranTicks", "AccountID", "ItemID" }
                            , new object[] { NewDebit, NewCrdit, NewOrderN, time.Ticks, AccountID, ItemID });
                        }
                        else
                        {
                            //throw new ServerUserMessage("BUG: Database incosistency, trying to do insert transaction operation on an account without transaction");
                            dspWriter.Update(m_DBName, "AccountBalance", new string[] { "TotalDebit", "TotalCredit", "LastOrderN", "tranTicks" }
                            , new object[] { NewDebit, NewCrdit, NewOrderN, time.Ticks }
                            , "AccountID=" + AccountID + " and ItemID=" + ItemID);
                        }
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        private void CheckSystemIntegrity()
        {
        }
        private bool IsParentOf<AccountType>(INTAPS.RDBMS.SQLHelper dsp, int Parent, int Child) where AccountType : AccountBase, new()
        {
            int depth = 0;
            int PID = Child;
            do
            {
                PID = GetAccountInternal<AccountType>(dsp, PID).PID;
                if (PID == -1)
                    return false;
                if (PID == Parent)
                    return true;
                depth++;
            }
            while (depth <= MAX_DEPTH);
            throw new ServerUserMessage("Maximu depth reached while searching for parent");
        }
        void PreviewTransaction(int docID, DateTime docDate, TransactionOfBatch[] batch, out int[] Accounts, out int[] Items, out double[] NewCredit, out double[] NewDebit, out bool OpeningTransaction, out Dictionary<ulong, int> insert, bool AllowInsert)
        {
            int progStackIndex = PushProgress("Analying transactions");
            try
            {
                //the transactoin type of the batch. Mixing of types is not allowed.
                TransactionType tt = TransactionType.Opening;

                Dictionary<ulong, double[]> preview = new Dictionary<ulong, double[]>(); //new balance for each of the account-item pair
                //insertion points(orderN) for inserting transactions
                insert = new Dictionary<ulong, int>();
                int i;
                
                //sum of all transactions, should be zero
                double total = 0;
                //iterate through all transaction in the batch
                for (i = 0; i < batch.Length; i++)
                {
                    TransactionOfBatch tran = batch[i];
                    total += tran.Amount;
                    //save transaction type of first transaction, fail if subsequent transactions don't match the type of the first one
                    if (i == 0)
                        tt = tran.TransactionType;
                    else
                    {
                        if (tt != tran.TransactionType)
                            throw new ServerUserMessage("All transactions in a single batch have to be of the same type.");
                    }
                    // get CostCenterAccount object also check validity of tran.AccountID
                    CostCenterAccount csac = GetCostCenterAccountInternal(dspWriter, tran.AccountID);
                    if (csac == null)
                        throw new ServerUserMessage("Account doesn't exist:"+tran.AccountID);
                    
                    //get Account object also check validty of csac.accountID. The database integrity constraint should prevent csac.accountID from ever being invalid
                    Account ac = GetAccount<Account>(csac.accountID);
                    if (ac == null)
                        throw new ServerUserMessage("Template account doesn't exist:" + csac.accountID);
                    
                    //get Costcener object also check validty of csac.costCenterID. The database integrity constraint should prevent csac.costCenterID from ever being invalid
                    CostCenter cs= GetAccount<CostCenter>(csac.costCenterID);
                    if (cs == null)
                        throw new ServerUserMessage("Accounting center doesn't exist:" + csac.costCenterID);
                    
                    //prevent posting to an invative cost center account, account and cost center
                    if (csac.status != AccountStatus.Activated)
                        throw new ServerUserMessage(String.Format("Account {0} is deactivated",CostCenterAccount.CreateCode(cs.Code,ac.Code)));
                    if (ac.Status != AccountStatus.Activated)
                        throw new ServerUserMessage(String.Format("Template account {0} is deactivated",ac.Code));
                    if (cs.Status != AccountStatus.Activated)
                        throw new ServerUserMessage(String.Format("Accounting center {0} is deactivated", cs.Code));

                    //prevent posting to a none leaf account
                    if (csac.isControlAccount)
                    {
                        throw new ServerUserMessage("You can't directly record transactions on controll accounts. " + GetCostCenterAccountString(csac));
                    }

                    //get AccountBalance object. If there are no transaction in the cost center account it will be null
                    AccountBalance bal = GetEndBalance(csac.id, tran.ItemID);

                    //TransactionOfBatch trn = tran;

                    //if the account-item combination is already found in the batch, add the transaction the one alrady in the preview table
                    if (preview.ContainsKey(tran.ItemAccountID))
                    {
                        if (tt == TransactionType.Opening)
                            throw new ServerUserMessage("Multiple opening transactions for a single account on in single batch.");
                        double[] dbcr = preview[tran.ItemAccountID];
                        tran.TotalDbBefore = dbcr[0];
                        tran.TotalCrBefore = dbcr[1];
                        dbcr[0] = tran.NewDebit;
                        dbcr[1] = tran.NewCredit;
                    }
                    else
                    {
                        int ins;
                        //bal.tranTicks is later than the batch date, we do transaction insertion logic
                        if (bal != null && bal.tranTicks > docDate.Ticks)
                        {
                            //insertion is not explicitly allowed, fail
                            if (!AllowInsert)
                                throw new ServerUserMessage("Transaction date and time before the last transaction.");
                            //get the orderN of the first transaction that is later than transaction we are inserting
                            //if there aren't any such transaction, it indicates corrupt database
                            object val = dspWriter.ExecuteScalar("Select Min(OrderN) from " + this.DBName + ".dbo.AccountTransaction where AccountID=" + tran.AccountID + " and ItemID=" + tran.ItemID + " and tranTicks>" + docDate.Ticks + "");
                            if (!(val is int))
                                throw new ServerUserMessage("Inconsitency in database. Last transaction date set for account(" + GetCostCenterAccountString(csac) + ") but no transaction found in the database that mataches it.");
                            
                            //save the orderN that will be pushed up to make way for the new transaction
                            ins = (int)val;
                            
                            //load the transaction that is pushed up
                            if (_updateLedger)
                            {
                                DataTable tab = dspWriter.GetDataTable("Select TotalDbBefore,TotalCrBefore from " + this.DBName + ".dbo.AccountTransaction where AccountID=" + csac.id + " and ItemID=" + tran.ItemID + " and OrderN=" + ins);

                                if (tab.Rows.Count == 0)
                                    throw new ServerUserMessage("Inconsitency in database. Last transaction date set for account(" + GetCostCenterAccountString(csac) + ") but couldn't retrieve the corresponding transaction information.");
                                if (tab.Rows.Count > 1)
                                    throw new ServerUserMessage("Inconsitency in database. Account:" + GetCostCenterAccountString(csac) + " and OrderN:" + ins + " repeated " + tab.Rows.Count + " times in AccountingTransaction table.");
                                //load before balances from the transactio that is being pushed up
                                tran.TotalDbBefore = (double)tab.Rows[0][0];
                                tran.TotalCrBefore = (double)tab.Rows[0][1];
                            }
                        }
                        else
                        {
                            //not inserting
                            ins = -1;
                            //use the balances of the AccountBalance entery for the before balances
                            tran.TotalDbBefore = bal.TotalDebit;
                            tran.TotalCrBefore = bal.TotalCredit;
                        }
                        //insert the new balances and insertion point to the respective tables
                        double[] dbcr = new double[2];
                        dbcr[0] = tran.NewDebit;
                        dbcr[1] = tran.NewCredit;
                        preview.Add(tran.ItemAccountID, dbcr);
                        insert.Add(tran.ItemAccountID, ins);
                    }
                    SetProgress((double)(i + 1) / (double)batch.Length);
                }
                if (!AccountBase.AmountEqual(total, 0))
                    throw new ServerUserMessage("It is not possible to enter unabalanced set of transactions. Debit-credit:{0}",total);
                //prepare return arrays
                Accounts = new int[preview.Count];
                Items = new int[preview.Count];
                NewDebit = new double[preview.Count];
                NewCredit = new double[preview.Count];
                i = 0;
                foreach (KeyValuePair<ulong, double[]> p in preview)
                {
                    //split the preview keys for accounts and items value
                    //use the values in preview table for NewDebit and NewCredit values
                    AccountBalance.Split(p.Key, out Accounts[i], out Items[i]);
                    NewDebit[i] = p.Value[0];
                    NewCredit[i] = p.Value[1];
                    i++;
                }
                OpeningTransaction = tt == TransactionType.Opening;
            }
            finally
            {
                PopProgress(progStackIndex);
            }
        }
        private void logDocumentPreviousVersion(int AID, int docID)
        {
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.Document", "ID=" + docID);
        }
        protected virtual void DeleteTransaction(int AID, bool deleteDoc, AccountDocument doc, bool forUpdate)
        {
            lock (dspWriter)
            {

                int progStackIndex = -1;
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    progStackIndex = PushProgress("Deleting transaction");
                    SetChildWeight(0.2);
                    PushProgress("Retrieving transaction");
                    AccountTransaction[] trans = GetTransactionsOfDocument(doc.AccountDocumentID);
                    
                    //call filters for deletion of individual AccountTransaction object. The filter for the document is called in 
                    //later in this method
                    foreach (TransactionOfBatch b in trans)
                    {
                        INTAPS.ClientServer.ObjectFilters.filterDelete(AID, typeof(AccountTransaction), trans);
                        docDepAccountUpdated(b.AccountID, b.ItemID, doc.tranTicks);    
                    }
                    PopProgress();

                    List<ulong> AffectedAccountItems = new List<ulong>();
                    List<int> AffectedAccounts = new List<int>();
                    int prog = 0;

                    SetChildWeight(0.4);
                    PushProgress("Deleting transaction and fixing ledger");
                    
                    //iterate the transaction in reverse order. It is made in reverse order to handle a case in which
                    //there are multiple transactions in a bacth affecting a single account, in which case the last transaction posted needs to be
                    //deleted first
                    for (int aci = trans.Length - 1; aci >= 0; aci--)
                    {
                        AccountTransaction tran = trans[aci];
                        if (!AffectedAccounts.Contains(tran.AccountID))
                            AffectedAccounts.Add(tran.AccountID);
                        if (!AffectedAccountItems.Contains(tran.ItemAccountID))
                            AffectedAccountItems.Add(tran.ItemAccountID);
                        AccountBalance bal = GetEndBalance(tran.AccountID, tran.ItemID);
                        if (bal.LastOrderN < 1)
                            throw new ServerUserMessage("BUG: delete transaction function trying to delete an account without transaction");
                        dspWriter.Delete(m_DBName, "AccountTransaction", "ID=" + tran.ID);

                        //if we are deleting the last transaction of an account just update the accountbalance table
                        if (bal.LastOrderN == tran.OrderN)
                        {
                            int NRec;
                            AccountBalance _totalTran;
                            object depList=docDepSuspend();
                            AccountTransaction[] prev = GetTransactionByFilter(dspWriter, "OrderN=" + (bal.LastOrderN - 1) + " and AccountID=" + tran.AccountID + " and ItemID=" + tran.ItemID, 0, -1, out NRec, out _totalTran);
                            docDepResume(depList);
                            //if there is no more transaction left for the account delete the account balance entry
                            if (prev.Length == 0)
                                dspWriter.Delete(m_DBName, "AccountBalance", "AccountID=" + tran.AccountID + " and ItemID=" + tran.ItemID);
                            else
                            {
                                dspWriter.Update(m_DBName, "AccountBalance"
                            , new string[] { "TotalDebit", "TotalCredit", "LastOrderN", "tranTicks" }
                            , new object[] { prev[0].NewDebit, prev[0].NewCredit, prev[0].OrderN, prev[0].tranTicks }
                            , "AccountID=" + tran.AccountID + " and ItemID=" + tran.ItemID);
                            }
                        }
                        else
                        {
                            //fix the tranactions above the current one
                            if(_updateLedger)
                                FixTransAboveNew(tran.AccountID, tran.ItemID, tran.OrderN, tran.TotalDbBefore, tran.TotalCrBefore, tran.OrderN - 1);
                        }
                        SetProgress((double)(++prog) / trans.Length);
                    }
                    PopProgress();

                    SetChildWeight(0.15);
                    PushProgress("Finalziing");
                    if (deleteDoc)
                    {
                        deleteDocumentSerials(AID, doc, false,forUpdate);
                        logDocumentPreviousVersion(AID, doc.AccountDocumentID);
                        INTAPS.ClientServer.ObjectFilters.filterDelete(AID, typeof(AccountDocument), doc.AccountDocumentID);
                        deleteAccountDocument(doc.AccountDocumentID,forUpdate);
                    }
                    PopProgress();

                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                    PopProgress(progStackIndex);
                }
            }
        }

        public CallTimer recordTransactionInternalTimer = null;
        
        //algorithm
        public void RecordTransactionInternal(int AID,AccountDocument accountDoc, DateTime docDate, TransactionOfBatch[] batch, bool AllowInsert)
        {
            INTAPS.ClientServer.ObjectFilters.filterAdd(AID, new RecordTransactionParameters() { document = accountDoc, docDate = docDate, batch = batch, allowInsert = AllowInsert });
            int i;
            int[] AffectedAccounts;
            int[] AffectedItems;
            Dictionary<ulong, int> InsertAt;
            double[] NewDebits, NewCredits;
            bool Opening;
            int progStackIndex = PushProgress("Recording transactions");
            try
            {
                SetChildWeight(0.7);
                PreviewTransaction(accountDoc.AccountDocumentID, docDate, batch, out AffectedAccounts, out AffectedItems, out NewCredits, out NewDebits, out Opening, out InsertAt, AllowInsert);

                int[] TranIDs = new int[batch.Length];
                int[] OrderNs = new int[batch.Length];
                //table for storing new order numbers
                Dictionary<ulong, int> NewOrderN = new Dictionary<ulong, int>();
                #region collect new transaction ids and order numbers

                for (i = 0; i < batch.Length; i++)
                {
                    int TranID = INTAPS.ClientServer.AutoIncrement.GetKey("Accounting.TranID");
                    ulong ItemAccountID = batch[i].ItemAccountID;
                    int AccountID = batch[i].AccountID;
                    int ItemID = batch[i].ItemID;
                    int OrderN;
                    //if the orderN is already added to the NewOrderN table just increment the last value by 1
                    if (NewOrderN.ContainsKey(ItemAccountID))
                    {
                        OrderN = NewOrderN[ItemAccountID] + 1; ;
                        NewOrderN[ItemAccountID] = OrderN;
                    }
                    else
                    {
                        //if appending add 1 from the accountBalance entry
                        //if inserting take the orderN of the transaction being pushed up
                        if (InsertAt[ItemAccountID] == -1)
                            OrderN = GetEndBalance(AccountID, ItemID).LastOrderN + 1;
                        else
                            OrderN = InsertAt[ItemAccountID];
                        NewOrderN.Add(ItemAccountID, OrderN);
                    }
                    OrderNs[i] = OrderN;
                    TranIDs[i] = TranID;
                }
                #endregion
                #region update affected accounts in database
                SetChildWeight(0.05);
                PushProgress("Updating affected account balances");

                for (i = 0; i < AffectedAccounts.Length; i++) //enumerate through all affected leaf accounts
                {
                    ulong iacid = AccountBalance.Combine(AffectedAccounts[i], AffectedItems[i]);
                    //update the account balance for the leaf account
                    UpdateAccountBalance(docDate, iacid, NewDebits[i], NewCredits[i], InsertAt[iacid], NewOrderN[iacid]);
                    docDepAccountUpdated(AffectedAccounts[i], AffectedItems[i], docDate.Ticks);
                    SetProgress((double)(i + 1) / (double)AffectedAccounts.Length);
                }

                PopProgress();

                #endregion

                #region dump transactions
                SetChildWeight(0.05);
                PushProgress("Registering transactions");
                for (i = 0; i < batch.Length; i++)
                {
                    TransactionOfBatch tr = batch[i];
                    dspWriter.Insert(m_DBName, "AccountTransaction", new string[] { "ID", "BatchID", "TransactionType", "AccountID", "ItemID", "Amount", "TotalCrBefore", "TotalDbBefore", "Note", "tranTicks", "OrderN" }
                                                                    , new object[] { TranIDs[i], accountDoc.AccountDocumentID, (int)tr.TransactionType, tr.AccountID, tr.ItemID, tr.Amount, tr.TotalCrBefore, tr.TotalDbBefore, tr.Note, docDate.Ticks, OrderNs[i] });
                    SetProgress((double)(i + 1) / (double)batch.Length);
                }
                PopProgress();
                #endregion
                SetChildWeight(0.1);
                PushProgress("Verifying book balance");
                VerifyBookBalance(dspWriter);
                PopProgress();
            }
            finally
            {
                PopProgress(progStackIndex);
            }
        }

        
        int RecordTransaction(int AID,AccountDocument docID, DateTime docDate, TransactionOfBatch[] batch, bool AllowInsert, bool allowOpening)
        {
            foreach(TransactionOfBatch b in batch)
                INTAPS.ClientServer.ObjectFilters.filterAdd(AID, b);
            if (!allowOpening)
            {
                foreach (TransactionOfBatch tran in batch)
                {
                    if (tran.TransactionType == TransactionType.Opening)
                        throw new ServerUserMessage("Opening transaction not allowed in this transaction.");
                }
            }
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    VerifyDocumentExists(dspWriter, docID.AccountDocumentID);
                    RecordTransactionInternal(AID,docID, docDate, batch, AllowInsert);
                    return docID.AccountDocumentID;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        //Entry Point
        public virtual int RecordTransaction(int AID, AccountDocument Doc, TransactionOfBatch[] batch, bool AllowInsert, bool allowOpening
            ,params DocumentTypedReference [] references)
        {
            //block opening transactions when not explicity specified
            if (!allowOpening)
            {
                foreach (TransactionOfBatch tran in batch)
                {
                    if (tran.TransactionType == TransactionType.Opening)
                        throw new ServerUserMessage("Opening transaction not allowed in this transaction.");
                }
            }
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    if(Doc.DocumentTypeID<1)
                        Doc.DocumentTypeID = GetDocumentTypeByType(Doc.GetType()).id;
                    INTAPS.ClientServer.ObjectFilters.filterAdd(AID, Doc);
                    if (!_closingDocumentTypes.Contains(Doc.DocumentTypeID))
                        if (!AccountBase.IsValidNoneClosingTransactionTime(Doc.DocumentDate))
                            throw new ServerUserMessage("Transactions can't be posted on the last minute of the day. Change the time and try again.");

                    //block future transactions when not explicity specified
                    DateTime now = DateTime.Now;
                    if (Doc.DocumentDate.Subtract(now).TotalSeconds > FUTURE_TOLERANCE_SECONDS)
                    {
                        if (!Doc.scheduled)
                            throw new ServerUserMessage("This document should be scheduled because the date is in the future");
                        if (Doc.materialized)
                            Doc.materializedOn = DateTime.Now;
                    }
                    else
                    {
                        if (Doc.scheduled)
                            throw new ServerUserMessage("This document can't be scheduled because the date is not in the future");
                        Doc.materialized = true;
                        Doc.materializedOn = DateTime.Now;
                    }

                       
                    int DocumentTypeID = GetDocumentTypeByClass(Doc.GetType().ToString()).id;
                    
                    //generate new account document ID if not supplied by the caller
                    if (Doc.AccountDocumentID < 1)
                        Doc.AccountDocumentID = INTAPS.ClientServer.AutoIncrement.GetKey(this.DocumentIDKeyName);
                    else
                        docDepDocumentUpdated(Doc.AccountDocumentID);

                    dspWriter.Insert(this.DocumentTableDB, "Document",
                        new string[]{"ID"
                            ,"DocumentTypeID"
                            ,"tranTicks"
                            ,"PaperRef"
                            ,"ShortDescription"
                            ,"LongDescription"
                            ,"DocumentData"
                            ,"hashCode"
                            ,"reversed"
                            ,"scheduled"
                            ,"materialized"
                            ,"materializedOn"
                            ,"__AID"
                        }
                    , new object[]
                    {
                    Doc.AccountDocumentID
                    ,DocumentTypeID
                    ,Doc.tranTicks
                    ,Doc.PaperRef
                    ,Doc.ShortDescription
                    ,Doc.LongDescription
                    ,Doc.ToXML()
                    ,Doc.GetDocumentHashCode()
                    ,false
                    ,Doc.scheduled
                    ,Doc.materialized
                    ,DateTime.Now
                    ,AID
                    });
                    addDocumentSerials(AID, Doc, references);
                    RecordTransaction(AID,Doc, Doc.DocumentDate, batch, AllowInsert, allowOpening);
                    return Doc.AccountDocumentID;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        //Overload
        public int RecordTransaction(int AID, AccountDocument Doc, TransactionOfBatch[] batch, params DocumentTypedReference[] references)
        {
            return RecordTransaction(AID, Doc, batch, true, true,references);
        }
        //Overload
        public int RecordTransaction(int AID, AccountDocument Doc, TransactionOfBatch[] batch, bool AllowInsert, params DocumentTypedReference[] references)
        {
            return RecordTransaction(AID, Doc, batch, AllowInsert, true, references);
        }
        ////Overload
        //public int RecordTransaction(int AID, int docID, DateTime docDate, TransactionOfBatch[] batch, params DocumentTypedReference[] references)
        //{
        //    return RecordTransaction(AID, docID, docDate, batch, true, true, references);
        //}        
        public int UpdateTransaction(int AID, AccountDocument doc, TransactionOfBatch[] batch, bool allowOpening, params DocumentTypedReference[] references)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    DeleteAccountDocument(AID, doc.AccountDocumentID,true);
                    int ret = RecordTransaction(AID, doc, batch, true, allowOpening, references);
                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void ReverseDocument(int AID, TransactionReversalDocument r)
        {
            //AccountDocument doc = r.reversedDoc;
            if (r.reversedDoc.reversed)
                throw new ServerUserMessage("Document can be reversed only once. To re-reverse the reversal, reverse the reveral document.");
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    TransactionOfBatch[] batch = GetTransactionOfBatchInternal(dspWriter, r.reversedDoc.AccountDocumentID);
                    foreach (TransactionOfBatch t in batch)
                    {
                        t.Amount = -t.Amount;
                    }
                    try
                    {
                        dspWriter.BeginTransaction();
                        int revid = RecordTransaction(AID, r, batch, true, false);
                        logDocumentPreviousVersion(AID, r.reversedDoc.AccountDocumentID);
                        dspWriter.Update(this.DocumentTableDB, "Document", new string[] { "hashCode", "reversed", "__AID" }
                        , new object[] { r.reversedDocHashCode + " Reversed by " + revid, true, AID }, "id=" + r.reversedDoc.AccountDocumentID);
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }

                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void FixTransactionSquence(int accountID, int itemID)
        {

            lock (dspWriter)
            {
                int stackIndex = PushProgress("Fixing ledger");
                dspWriter.BeginTransaction();
                try
                {
                    string sql = string.Format("Select TransactionType,TotalDbBefore,TotalCrBefore,Amount,ID,tranTicks from Accounting.dbo.AccountTransaction where AccountID={0} and ItemID={1} order by OrderN"
                        , accountID, itemID);
                    SetProgress("Loading ledger", 0);
                    DataTable table = dspWriter.GetDataTable(sql);
                    SetProgress(0.1);
                    double totaldebit = 0, totalcredit = 0;
                    int orderN = 1;
                    long tranTime = DateTime.Now.Ticks;

                    SetChildWeight(0.7);
                    PushProgress("Fixing ledger items");
                    foreach (DataRow row in table.Rows)
                    {
                        TransactionOfBatch tran = new TransactionOfBatch(accountID, (double)row[3], "");
                        tran.ItemID = itemID;
                        tran.ID = (int)row[4];
                        tran.TransactionType = (TransactionType)row[0];
                        tran.TotalCrBefore = totalcredit;
                        tran.TotalDbBefore = totaldebit;
                        dspWriter.ExecuteNonQuery("Update " + this.DBName + ".dbo.AccountTransaction set TotalDbBefore=" + tran.TotalDbBefore
                            + ",TotalCrBefore=" + tran.TotalCrBefore
                        + ",OrderN=" + orderN + " where ID=" + tran.ID);
                        totalcredit = tran.NewCredit;
                        totaldebit = tran.NewDebit;
                        tranTime = (long)row[5];
                        SetProgress((double)orderN / (double)table.Rows.Count);
                        orderN++;
                    }
                    PopProgress();
                    SetProgress("Fixing account balance");
                    if (AccountBase.AmountEqual(totaldebit, 0) && AccountBase.AmountEqual(totalcredit, 0) && orderN==1)
                        dspWriter.ExecuteNonQuery("DELETE FROM " + this.DBName + @".[dbo].[AccountBalance]
                            WHERE accountID=" + accountID + " and ItemID=" + itemID);
                    else
                    {
                        AccountBalance bal = this.GetEndBalance(accountID, itemID);
                        if (bal.LastOrderN == 0)
                        {
                            bal.LastOrderN = orderN - 1;
                            bal.tranTicks = tranTime;
                            bal.TotalDebit = totaldebit;
                            bal.TotalCredit = totalcredit;
                            dspWriter.InsertSingleTableRecord<AccountBalance>(this.DBName, bal);
                        }
                        else
                        {
                            this.dspWriter.ExecuteNonQuery("UPDATE " + this.DBName + @".[dbo].[AccountBalance]
                                SET [tranTicks] =" + tranTime + @"
                                    ,[LastOrderN] = " + (orderN - 1) + @"
                                    ,[TotalCredit] = " + totalcredit + @"
                                    ,[TotalDebit] = " + totaldebit + @"
                                WHERE accountID=" + accountID + " and ItemID=" + itemID);
                        }
                    }
                    SetProgress("Fixing control accounts", 0.85);
                    string err;

                    SetProgress(0.9);
                    SetChildWeight(0.1);
                    if (this.VerifyTransactionConsistency(dspWriter, accountID, itemID, out err) != -1)
                        throw new ServerUserMessage(err);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    Console.WriteLine("Failed to fix " + accountID + "\n" + ex.Message);
                }
                finally
                {
                    PopProgress(stackIndex);
                }
            }


        }
        public void MergeTransaction(int[] sources, int destination, bool byDate, DateTime from, DateTime to)
        {
            if (sources == null || sources.Length == 0)
                return;

            List<int> itemIDs = new List<int>();
            foreach (int source in sources)
            {
                foreach (AccountBalance bal in GetEndBalances(source))
                {
                    if (!itemIDs.Contains(bal.ItemID))
                        itemIDs.Add(bal.ItemID);
                }
            }

            lock (dspWriter)
            {
                int stackIndex = PushProgress("Merging transactions");
                try
                {
                    dspWriter.BeginTransaction();
                    SetProgress("Updating accounts", 0);
                    string filter = "";
                    foreach (int source in sources)
                    {
                        if (filter == "")
                            filter = source.ToString();
                        else
                            filter += "," + source;
                    }
                    filter = "accountID in (" + filter + ")";
                    dspWriter.Update(this.DBName, "AccountTransaction", new string[] { "accountID" },
                        new object[] { destination }, filter);
                    SetProgress(0.2);

                    SetChildWeight(0.8);
                    PushProgress("Fixing Transactions");
                    foreach (int itemID in itemIDs)
                    {
                        FixTransactionSquence(destination, itemID);
                        foreach (int source in sources)
                        {
                            if (source != destination)
                                FixTransactionSquence(source, itemID);
                        }
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    PopProgress(stackIndex);
                }
            }
        }
        public void validateNegativeBalance(int csAccountID, int itemID, bool creditAccount, DateTime upto, string msg)
        {
            if (hasNegativeBalance(csAccountID, itemID, creditAccount, upto.AddSeconds(AccountingBDE.FUTURE_TOLERANCE_SECONDS)))
                throw new ServerUserMessage(msg);
        }
        public bool hasNegativeBalance(int csAccountID, int itemID,bool creditAccount,DateTime upto)
        {

            lock (dspWriter)
            {
                string sql = "Select * from {0}.dbo.AccountTransaction where accountID={1} and itemID={2} and tranTicks<{3}";

                SqlDataReader reader = dspWriter.ExecuteReader(string.Format(sql, this.DBName, csAccountID, itemID, upto.Ticks));
                try
                {
                    while (reader.Read())
                    {
                        AccountTransaction tran = ExtractAccountTransactionFromReader(reader);
                        if (creditAccount)
                        {
                            if (AccountBase.AmountGreater(tran.NewDebit, tran.NewCredit))
                                return true;
                        }
                        else
                        {
                            if (AccountBase.AmountGreater(tran.NewCredit, tran.NewDebit))
                                return true;
                        }
                    }
                    return false;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }
        }
    }
}
