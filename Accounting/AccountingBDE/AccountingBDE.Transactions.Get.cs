using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        public AccountTransaction[] GetTransactionOfBatchInternal(INTAPS.RDBMS.SQLHelper dsp, int BatchID)
        {
            int NRec;
            AccountBalance _totalTran;
            return GetTransactionByFilter(dsp, "BatchID=" + BatchID, 0, -1, out NRec, out _totalTran);
        }
        double SumTransactionAmount(INTAPS.RDBMS.SQLHelper helper, string filter, bool joinWithDocument)
        {
            string source;
            if (joinWithDocument)
                source = @"AccountTransaction INNER JOIN Document ON AccountTransaction.BatchID = Document.ID";
            else
                source = "AccountTransaction";
            string sql = @"Select Sum(Amount) from "
                + source + " where " + filter;
            object _ret = helper.ExecuteScalar(sql);
            if (_ret is double)
                return (double)_ret;
            return 0;
        }

        public AccountTransaction[] GetTransactionByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter, int index, int pageSize, out int N, out AccountBalance totalTran)
        {
            return GetTransactionByFilter(dsp, filter, "tranTicks,AccountID,ItemID,OrderN,ID", index, pageSize, out N, out totalTran);
        }
        public AccountTransaction[] GetTransactionByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter, bool joinWithDocument, int index, int pageSize, out int N, out AccountBalance totalTran)
        {
            return GetTransactionByFilter(dsp, filter, "tranTicks,AccountID,ItemID,OrderN,ID", joinWithDocument, false, index, pageSize, out N, out totalTran);
        }
        public AccountTransaction[] GetTransactionByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter, string orderBy, int index, int pageSize, out int N, out AccountBalance totalTran)
        {
            return GetTransactionByFilter(dsp, filter, orderBy,false,false, index, pageSize, out N, out totalTran);
        }
        public AccountTransaction[] GetTransactionByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter, string orderBy, bool joinWithDocument, bool joinWithLeafAccount,int index, int pageSize, out int N, out AccountBalance totalTran)
        {
            string source;
            string prefix = base.DBName + ".dbo.";
            source = prefix + @"AccountTransaction";
            if (joinWithDocument)
                source += " INNER JOIN " + prefix + @"Document ON " + prefix + @"AccountTransaction.BatchID = " + prefix + @"Document.ID";
            if(joinWithLeafAccount)
                source += " INNER JOIN " + prefix + "CostCenterLeafAccount ON " + prefix + "AccountTransaction.AcountID = " + prefix + "childCostCenterAccountID.ID";
            string sql = @"Select 
                    AccountTransaction.ID
                    ,AccountTransaction.BatchID
                    ,AccountTransaction.TransactionType
                    ,AccountTransaction.AccountID
                    ,AccountTransaction.Amount
                    ,AccountTransaction.TotalDbBefore
                    ,AccountTransaction.TotalCrBefore
                    ,AccountTransaction.Note
                    ,AccountTransaction.tranTicks
                    ,AccountTransaction.ItemID
                    ,AccountTransaction.OrderN from " + source + " where " + filter + " order by "+orderBy;
            return GetTransactionBySQL(dsp, sql, index, pageSize, out N, out totalTran);
        }
        public AccountTransaction[] GetTransactionBySQL(INTAPS.RDBMS.SQLHelper dsp, string sql, int index, int pageSize, out int N, out AccountBalance totalTran)
        {
            List<AccountTransaction> _ret = new List<AccountTransaction>();
            IDataReader row = dsp.ExecuteReader(sql);
            
            try
            {
                int i = 0;
                double db = 0, cr = 0;
                AccountTransaction tran = new AccountTransaction();
                tran.AccountID = -1;

                while (row.Read())
                {
                    AccountTransaction t = ExtractAccountTransactionFromReader(row);
                    tran.documentID = t.documentID;
                    tran.Amount = t.Amount;
                    tran.ItemID = t.ItemID;
                    tran.TotalCrBefore = cr;
                    tran.TotalDbBefore = db;
                    tran.TransactionType = t.TransactionType;
                    cr = tran.NewCredit;
                    db = tran.NewDebit;
                    if (pageSize == -1 || (i >= index && i < index + pageSize))
                        _ret.Add(t);
                    i++;
                    docDepAddDocDependancy(tran.documentID);
                }
                totalTran = new AccountBalance();
                totalTran.TotalCredit = cr;
                totalTran.TotalDebit = db;
                N = i;
            }
            finally
            {
                if (row != null && !row.IsClosed)
                    row.Close();
            }
            return _ret.ToArray();
        }

        private AccountTransaction ExtractAccountTransactionFromReader(IDataReader row)
        {
            AccountTransaction t = new AccountTransaction();
            t.ID = (int)row["ID"];
            t.documentID = (int)row["BatchID"];
            t.TransactionType = (TransactionType)(int)row["TransactionType"];
            t.AccountID = (int)row["AccountID"];
            t.Amount = (double)row["Amount"];
            t.TotalDbBefore = (double)row["TotalDbBefore"];
            t.TotalCrBefore = (double)row["TotalCrBefore"];
            t.Note = row["Note"] as string;
            t.tranTicks = (long)row["tranTicks"];
            t.ItemID = (int)row["ItemID"];
            t.OrderN = (int)row["OrderN"];
            docDepAddDocDependancy(t.documentID);
            return t;
        }
        public int CountTransactionByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter)
        {
            //docDepPreventInDependancyMode("CountTransactionByFilter");
            return (int)dsp.ExecuteScalar("Select count(*) from " + m_DBName + ".dbo.AccountTransaction where " + filter);
        }

        
        public int CountTransactionByFilter<AccountType>(INTAPS.RDBMS.SQLHelper dsp, string filter) where AccountType:AccountBase, new()
        {
            //docDepPreventInDependancyMode("CountTransactionByFilter");
            string sql = @"SELECT COUNT(*) FROM {0}.[dbo].[AccountTransaction] inner join
                            {0}.[dbo].CostCenterAccount on AccountTransaction.AccountID=CostCenterAccount.id inner join {0}.[dbo].{1} 
                            on CostCenterAccount.{3}={1}.ID where {2}";
            return (int)dsp.ExecuteScalar(string.Format(sql,this.DBName,typeof(AccountType).Name,filter,AccountBase.GetCostCenterAccountIDField<AccountType>()));
        }
        //unscalable search
        public AccountTransaction[] GetTransactions(int accountID, int itemID, bool filterByTime, DateTime from, DateTime to, int index, int pageSize, out int NResult, out AccountBalance totalTransaction)
        {
            return GetTransactions(accountID, itemID, filterByTime, from, to, null, index, pageSize, out NResult, out totalTransaction);
        }
        public double SumTransactions(int accountID, int itemID, bool filterByTime, DateTime from, DateTime to, int[] docTypes)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string filter = "";
                if (accountID != -1)
                    filter = "AccountID=" + accountID;
                if (itemID != -1)
                    if (string.IsNullOrEmpty(filter))
                        filter = "ItemID=" + itemID;
                    else
                        filter += " and ItemID=" + itemID;
                else
                    throw new ServerUserMessage("Invalid filter");
                if (filterByTime)
                    filter += " and (tranTicks >=" + from.Ticks + " and tranTicks<" + to.Ticks + ")";

                if (docTypes == null || docTypes.Length == 0)
                    return SumTransactionAmount(dspReader, filter, false);
                else
                {
                    filter += " and DocumentTypeID in (" + docTypes[0];
                    for (int i = 1; i < docTypes.Length; i++)
                        filter += "," + docTypes[i];
                    filter += ")";
                    return SumTransactionAmount(dspReader, filter, true);
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountTransaction[] GetTransactions(
            int accountID
            , int itemID
            , bool filterByTime, DateTime from, DateTime to
            , int[] docTypes
            , int index, int pageSize, out int NResult
            , out AccountBalance totalTransaction)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string filter = "";
                if (accountID != -1)
                    filter = "AccountID=" + accountID;
                if (itemID != -1)
                    if (string.IsNullOrEmpty(filter))
                        filter = "ItemID=" + itemID;
                    else
                        filter += " and ItemID=" + itemID;
                else
                    throw new ServerUserMessage("Invalid filter");
                if (filterByTime)
                    filter += " and (AccountTransaction.tranTicks >=" + from.Ticks + " and AccountTransaction.tranTicks<" + to.Ticks + ")";
                AccountTransaction[] _ret;
                if (docTypes == null || docTypes.Length == 0)
                    _ret = GetTransactionByFilter(dspReader, filter, index, pageSize, out NResult,out totalTransaction);
                else
                {
                    filter += " and DocumentTypeID in (" + docTypes[0];
                    for (int i = 1; i < docTypes.Length; i++)
                        filter += "," + docTypes[i];
                    filter += ")";
                    _ret = GetTransactionByFilter(dspReader, filter, true, index, pageSize, out NResult,out totalTransaction);
                }
                return _ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountTransaction GetTransaction(int tranID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                int NRec;
                AccountBalance _totalTran;
                AccountTransaction[] t = GetTransactionByFilter(dspReader, "ID=" + tranID, 0, -1, out NRec, out _totalTran);
                if (t.Length == 0)
                    return null;
                return t[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public AccountTransaction[] GetTransactionsOfDocument(int BatchID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return GetTransactionOfBatchInternal(dspReader, BatchID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }


        public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccountID, int resItemID, int[] accountID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord)
        {
            AccountBalance begBal;
            AccountBalance total;
            return GetLedger(resAccountID, resItemID, accountID, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord, out begBal,out total);
        }
        public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccountID, int resItemID, int[] accountID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord, out AccountBalance begBal,out AccountBalance total)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                string accountFilter = null;
                if (accountID.Length == 1)
                    accountFilter = "AccountID=" + accountID[0];
                else if (accountID.Length > 1 && accountID.Length < 200)
                {
                    foreach (int aid in accountID)
                    {
                        if (string.IsNullOrEmpty(accountFilter))
                            accountFilter = aid.ToString();
                        else
                            accountFilter += "," + aid.ToString();
                    }
                    accountFilter = "AccountID in (" + accountFilter + ")";
                }
                else
                {
                    lock (dspWriter)
                    {
                        dspWriter.Delete(this.DBName, "AccountList", "");
                        dspWriter.InsertColumn<int>(this.DBName, "AccountList", new string[] { "id" }, new object[0], accountID);
                    }
                    accountFilter = string.Format("AccountID in (Select id from {0}.dbo.AccountList)",this.DBName);
                }
                string itemFilter = null;
                if (itemID.Length == 1)
                    itemFilter = "ItemID=" + itemID[0];
                else if (itemID.Length > 1  && itemID.Length < 200)
                {
                    foreach (int iid in itemID)
                    {
                        if (string.IsNullOrEmpty(itemFilter))
                            itemFilter = iid.ToString();
                        else
                            itemFilter += "," + iid.ToString();
                    }
                    itemFilter = "ItemID in (" + itemFilter + ")";
                }
                else
                {
                    lock (dspWriter)
                    {
                        dspWriter.Delete(this.DBName, "ItemList", "");
                        dspWriter.InsertColumn<int>(this.DBName, "ItemList", new string[] { "id" }, new object[0], accountID);
                    }
                    itemFilter = string.Format("ItemID in (Select id from {0}.dbo.ItemList)", this.DBName);
                }
                string dateFilter = "(tranTicks>=" + dateFrom.Ticks + " and tranTicks<" + dateTo.Ticks + ")";
                string cr = null;
                if (accountFilter != null)
                    cr = accountFilter;
                if (itemFilter != null)
                    if (cr == null)
                        cr = itemFilter;
                    else
                        cr += " and " + itemFilter;
                if (cr == null)
                    cr = dateFilter;
                else
                    cr += " and " + dateFilter;
                
                AccountTransaction[] tran = this.GetTransactionByFilter(helper, cr
                    , "tranTicks,AccountID,ItemID,OrderN,ID"
                    , 0, -1, out NRecord, out total);
                total.AccountID = resAccountID;
                total.ItemID = resItemID;


                DateTime begDate = dateFrom;// tran[0].tranTicks;
                double db;
                double crd;
                getCombinedBalanceAsOf(accountID, itemID, begDate, out db, out crd);
                begBal = new AccountBalance(resAccountID, resItemID);
                begBal.TotalDebit = db;
                begBal.TotalCredit = crd;

                Dictionary<ulong, double[]> val = new Dictionary<ulong, double[]>();
                for (int j = 0; j < accountID.Length; j++)
                    for(int k=0;k<itemID.Length;k++)
                {
                    AccountBalance bal = GetBalanceAsOf(accountID[j], itemID[k], dateFrom);
                    val.Add(bal.ItemAccountID, new double[] { bal.TotalDebit, bal.TotalCredit });
                }

                if (tran.Length == 0)
                {
                    return tran;
                }
                
                AccountTransaction[] ret = new AccountTransaction[tran.Length];
                int i = 0;
                
                foreach (AccountTransaction t in tran)
                {

                    ret[i] = new AccountTransaction();
                    ret[i].TotalDbBefore = db;
                    ret[i].TotalCrBefore = crd;
                    ret[i].AccountID = t.AccountID;
                    ret[i].ItemID = t.ItemID;
                    ret[i].tranTicks = t.tranTicks;
                    ret[i].documentID = t.documentID;
                    
                    ret[i].ID = t.ID;
                    ret[i].Note = t.Note;
                    ret[i].OrderN = t.OrderN;
                    
                    ulong accountItem = t.ItemAccountID;

                    double oldDb = db;
                    double oldCr = crd;

                    db += t.NewDebit - t.TotalDbBefore;
                    crd += t.NewCredit-t.TotalCrBefore;

                    ret[i].Amount = db - oldDb - (crd - oldCr);

                    
                    ret[i].TransactionType = TransactionType.DebitCredit;
                    i++;
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccountID, int resItemID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord, out AccountBalance begBal,out AccountBalance total)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                int[] leafAccounts = getLeaveCostCenterAccounts(helper, resAccountID);
                if (leafAccounts.Length == 0)
                    return GetLedger(resAccountID, resItemID, new int[] { resAccountID }, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord, out begBal,out total);
                return GetLedger(resAccountID, resItemID, leafAccounts, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord, out begBal,out total);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }

        public int[] getLeaveCostCenterAccounts(INTAPS.RDBMS.SQLHelper helper, int csAccountID)
        {
            int[] leafAccounts = helper.GetColumnArray<int>(string.Format("Select childCostCenterAccountID from {0}.dbo.CostCenterLeafAccount where parentCostCenterAccountID={1}", this.DBName, csAccountID), 0);
            return leafAccounts;
        }
        public int[] getLeaveCostCenterAccounts(int csAccountID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getLeaveCostCenterAccounts(reader, csAccountID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
            
        }
        public bool CostCenterAccountHasTransaction(int csaID)
        {
            AccountBalance[] bal = GetEndBalances(csaID);
            return bal.Length > 0;
        }
        public bool AccountHasTransaction<AccountType>(int accountID) where AccountType:AccountBase,new()
        {
            INTAPS.RDBMS.SQLHelper reader=base.GetReaderHelper();
            try
            {
                foreach (CostCenterAccount csa in GetCostCenterAccountsOfInternal<AccountType>(reader, accountID))
                {
                    if (CostCenterAccountHasTransaction(csa.id))
                        return true;
                }
                return false;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }



        public bool CostCenterAccountHasTransaction(int csaID,long ticksFrom,long ticksTo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string cr=null;
                if (ticksFrom != -1)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", "tranTicks>=" + ticksFrom);
                if (ticksTo!= -1)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", "tranTicks<" + ticksTo);
                string sql = "Select count(*) from {0}.dbo.AccountTransaction where accountID={1} {2}".format(this.DBName,csaID,cr==null?null: " AND "+cr);
                return ((int)reader.ExecuteScalar(sql)) > 0;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
            AccountBalance[] bal = GetEndBalances(csaID);
            return bal.Length > 0;
        }
        public bool AccountHasTransaction<AccountType>(int accountID,long ticksFrom,long ticksTo) where AccountType : AccountBase, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                foreach (CostCenterAccount csa in GetCostCenterAccountsOfInternal<AccountType>(reader, accountID))
                {
                    if (CostCenterAccountHasTransaction(csa.id,ticksFrom,ticksTo))
                        return true;
                }
                return false;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
    }
}
