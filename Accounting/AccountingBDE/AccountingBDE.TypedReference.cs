using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;
using System.Data.SqlClient;

namespace INTAPS.Accounting.BDE
{
    public partial class AccountingBDE
    {
        // BDE exposed
        public int createDocumentSerialBatch(int AID, DocumentSerialBatch serialBatch)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    serialBatch.assertValidBatch();
                    serialBatch.id = AutoIncrement.GetKey("Accounting.DocumentSerialBatchID");
                    dspWriter.InsertSingleTableRecord(this.DBName, serialBatch, new string[] { "__AID" }, new object[] { AID });
                    verifyDocumentSerialBatch(dspWriter, serialBatch.typeID, serialBatch.costCenterID);
                    dspWriter.CommitTransaction();
                    return serialBatch.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public void updateDocumentSerialBatch(int AID, DocumentSerialBatch serialBatch)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    serialBatch.assertValidBatch();
                    DocumentSerialBatch oldData = getDocumentSerialBatch(serialBatch.id);
                    if (oldData == null)
                        throw new ServerUserMessage("Serial document serial batch not found in the database. ID:" + serialBatch.id);
                    if (oldData.costCenterID != serialBatch.costCenterID)
                        throw new ServerUserMessage("Accounting center of a serial batch can't be changed");

                    dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(DocumentSerialBatch).Name, "id=" + serialBatch.id);
                    dspWriter.UpdateSingleTableRecord(this.DBName, serialBatch, new string[] { "__AID" }, new object[] { AID });
                    verifyDocumentSerialBatch(dspWriter, serialBatch.typeID, serialBatch.costCenterID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public void deleteDocumentSerialBatch(int AID, int serialBatchID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    DocumentSerialBatch oldData = getDocumentSerialBatch(serialBatchID);
                    if (oldData == null)
                        throw new ServerUserMessage("Serial document serial batch not found in the database. ID:" + serialBatchID);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(DocumentSerialBatch).Name, "id=" + serialBatchID);
                    dspWriter.DeleteSingleTableRecrod<DocumentSerialBatch>(this.DBName, serialBatchID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public void addDocumentSerials(int AID, AccountDocument doc, params DocumentTypedReference[] refs)
        {

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {

                    List<int> typeIDs = new List<int>();

                    for (int i = 0; i < refs.Length; i++)
                    {
                        validTypedReferenceFormat(refs[i].reference);
                        DocumentSerialType type = getDocumentSerialType(refs[i].typeID);
                        if (type == null)
                            throw new ServerUserMessage("Invalid document serial type ID:" + refs[i].typeID);
                        if (isDocumentSerialVoidInternal(dspWriter, refs[i].typeID, refs[i].reference))
                            throw new ServerUserMessage("Reference " + refs[i].reference + "  is void");
                        if (type.serialType == DocumentSerializationMode.Serialized)
                        {
                            DocumentSerialBatch[] batch = getSerialBatchByTimeInternal(dspWriter, refs[i].typeID, doc.DocumentDate);
                            if (batch.Length == 0)
                                throw new ServerConfigurationError("Serial batch not found in the database");
                            DocumentSerialBatch inRange = null;
                            foreach (DocumentSerialBatch b in batch)
                            {
                                if (b.inRange(refs[i].serial))
                                {
                                    inRange = b;
                                    break;
                                }
                            }
                            if (inRange == null)
                                throw new ServerUserMessage("Serial number " + refs[i].serial + " not in allowed range");
                            refs[i].reference = inRange.FormatSerial(refs[i].serial);

                        }
                        if (typeIDs.Contains(refs[i].typeID))
                        {
                            string strRefs = null;
                            foreach (DocumentTypedReference s in refs)
                                strRefs = INTAPS.StringExtensions.AppendOperand(strRefs, ", ", "[{0}|{1}]".format(s.typeID, s.reference));
                            throw new ServerUserMessage("A single document can only  have one serial number for a given serial number type,documentID is {0}, documentTypereferenceID is {1}, documentTypeID is {2}, short description={3}, ref {4}, refs {5}", doc.AccountDocumentID, refs[i].typeID, doc.GetType(), doc.ShortDescription, refs[i].reference, strRefs);
                        }
                        typeIDs.Add(refs[i].typeID);
                    }

                    DocumentSerial ds = new DocumentSerial();
                    ds.documentID = doc.AccountDocumentID;
                    for (int i = 0; i < refs.Length; i++)
                    {
                        ds.typedReference = refs[i];
                        DocumentSerial primary = getPrimaryDocumentSerialInternal(dspWriter, ds.typeID, ds.reference);
                        if (primary == null)
                            ds.primaryDocument = true;
                        else
                        {
                            if (ds.primaryDocument)
                            {
                                primary.primaryDocument = false;
                                dspWriter.UpdateSingleTableRecord(this.DBName, primary);
                            }
                        }
                        dspWriter.InsertSingleTableRecord(this.DBName, ds, new string[] { "__AID" }, new object[] { AID });
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void removeDocumentSerial(int AID, int docID, DocumentTypedReference r)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName+".dbo.DocumentSerial", string.Format("documentID={0} and typeID={1} and reference='{2}'", docID, r.typeID, r.reference));
                    dspWriter.DeleteSingleTableRecrod<DocumentSerial>(this.DBName, docID, r.typeID, r.reference);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void validTypedReferenceFormat(string reference)
        {
            if (string.IsNullOrEmpty(reference))
                throw new ServerUserMessage("Empty reference is not allowed");

            System.Text.RegularExpressions.Match m = System.Text.RegularExpressions.Regex.Match(reference, DOCUMENT_TYPED_REFERENCE_FORMAT, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (m.Length != reference.Length)
                throw new ServerUserMessage("Invalid document reference:" + reference);
        }
        void deleteDocumentSerials(int AID, AccountDocument doc, bool voidToo, bool forUpdate)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    foreach (DocumentSerial s in getDocumentSerialsInternal(dspWriter, doc.AccountDocumentID))
                    {
                        if (s.primaryDocument)
                        {
                            if (countDocumentsByTypedReference(new DocumentTypedReference(s.typeID, s.reference, true)) > 1)
                            {
                                //if (forUpdate)
                                //{
                                int docID = (int)dspWriter.ExecuteScalar(string.Format("Select top 1 documentID from {0}.dbo.DocumentSerial where typeID={1} and reference='{2}' and documentID<>{3}"
                                    , this.DBName, s.typeID, s.reference, s.documentID));
                                string cr = string.Format("typeID={0} and reference='{1}' and documentID={2}", s.typeID, s.reference, docID);
                                dspWriter.logDeletedData(AID, this.DBName + ".dbo.DocumentSerial", cr);
                                dspWriter.Update(this.DBName, "DocumentSerial", new string[] { "primaryDocument", "__AID" }, new object[] { true, AID }, cr);
                                //}
                                //else
                                //{
                                //    throw new ServerUserMessage("This document is a primary document in the transaction journal. Delete related documents first");
                                //}
                            }
                        }
                    }


                    dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(DocumentSerial).Name, "documentID=" + doc.AccountDocumentID);

                    dspWriter.Delete(this.DBName, typeof(DocumentSerial).Name, "documentID=" + doc.AccountDocumentID);
                    foreach (DocumentSerial s in getDocumentSerialsInternal(dspWriter, doc.AccountDocumentID))
                    {
                        int serTypeID = getDocumentSerialBatchInternal(dspWriter, s.typeID).typeID;
                        if (voidToo)
                            voidSerialNo(AID, doc.DocumentDate, s.typedReference, "Document deletion");
                    }

                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public DocumentSerial[] getDocumentSerials(int documentID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getDocumentSerialsInternal(reader, documentID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public DocumentSerialType[] getAllDocumentSerialTypes()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<DocumentSerialType>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        // BDE exposed
        public DocumentSerialType getDocumentSerialType(int serialTypeID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getDocumentSerialTypeInternal(reader, serialTypeID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        private static DocumentSerialType getDocumentSerialTypeInternal(INTAPS.RDBMS.SQLHelper reader, int serialTypeID)
        {
            DocumentSerialType[] ret = reader.GetSTRArrayByFilter<DocumentSerialType>("id=" + serialTypeID);
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        // BDE exposed
        public DocumentSerialType getDocumentSerialType(string prefix)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getDocumentSerialTypeInternal(reader, prefix);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public DocumentSerialType getDocumentSerialType(string prefix,bool throwException)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {                
                DocumentSerialType ret= getDocumentSerialTypeInternal(reader, prefix);
                if (ret == null && throwException)
                    throw new ServerUserMessage("Reference type " + prefix);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        private static DocumentSerialType getDocumentSerialTypeInternal(INTAPS.RDBMS.SQLHelper reader, string prefix)
        {
            DocumentSerialType[] ret = reader.GetSTRArrayByFilter<DocumentSerialType>("code='" + prefix + "'");
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        // BDE exposed
        public DocumentSerialBatch[] getAllDocumentSerialBatches(int serialTypeID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<DocumentSerialBatch>("typeID=" + serialTypeID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public DocumentSerialBatch getDocumentSerialBatch(int serialBatchID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getDocumentSerialBatchInternal(reader, serialBatchID);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        private static DocumentSerialBatch getDocumentSerialBatchInternal(INTAPS.RDBMS.SQLHelper reader, int serialBatchID)
        {
            DocumentSerialBatch[] ret = reader.GetSTRArrayByFilter<DocumentSerialBatch>("id=" + serialBatchID);
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        DocumentSerial[] getDocumentSerialsInternal(INTAPS.RDBMS.SQLHelper helper, int docID)
        {
            return helper.GetSTRArrayByFilter<DocumentSerial>("documentID=" + docID);
        }
        DocumentSerial getDocumentSerialInternal(INTAPS.RDBMS.SQLHelper helper, int typeID, string reference)
        {
            DocumentSerial[] ret = helper.GetSTRArrayByFilter<DocumentSerial>("typeID=" + typeID + " and reference='" + reference + "'");
            if (ret.Length == 0)
                return null;
            ret[0].documentID = -1;
            return ret[0];
        }
        DocumentSerial[] getDocumentSerialsInternal(INTAPS.RDBMS.SQLHelper helper, int typeID, string reference)
        {
            return helper.GetSTRArrayByFilter<DocumentSerial>("typeID=" + typeID + " and serialNo='" + reference + "'");
        }
        public DocumentSerial getPrimaryDocumentSerial(int typeID, string reference)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getPrimaryDocumentSerialInternal(reader, typeID, reference);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        DocumentSerial getPrimaryDocumentSerialInternal(INTAPS.RDBMS.SQLHelper helper, int typeID, string reference)
        {
            DocumentSerial[] ret = helper.GetSTRArrayByFilter<DocumentSerial>("typeID=" + typeID + " and reference='" + reference + "' and primaryDocument=1");
            if (ret.Length > 1)
                throw new ServerUserMessage("Database inconsitency more than one primary documents found for a reference number");
            if (ret.Length == 0)
                return null;
            object dep = docDepSuspend();
            AccountDocument doc = GetAccountDocument(ret[0].documentID, false);
            docDepResume(dep);
            ret[0].primaryDoc = doc;
            return ret[0];
        }
        bool isDocumentSerialVoidInternal(INTAPS.RDBMS.SQLHelper helper, int typeID, string serialNo)
        {
            string sql = @"Select max(serialNo) from {0}.dbo.DocumentSerialVoided where typeID={1} and reference='{2}'";
            object max = helper.ExecuteScalar(string.Format(sql, this.DBName, typeID, serialNo));
            return max is int;
        }        
        DocumentSerialBatch[] getSerialBatchByTimeInternal(INTAPS.RDBMS.SQLHelper helper, int serialTypeID, DateTime time)
        {
            string sql = @"typeID={0} and  fromDate<='{1}'
                            and (timeUpperBound=0 or (timeUpperBound=1 and toDate>'{1}'))";
            return helper.GetSTRArrayByFilter<DocumentSerialBatch>(string.Format(sql, serialTypeID, time));
        }
        public DocumentSerialBatch getSerialBatchBySerialNo(int serialTypeID, int serialNo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getSerialBatchBySerialNo(reader, serialTypeID, serialNo);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public DocumentSerialBatch getSerialBatchBySerialNo(INTAPS.RDBMS.SQLHelper helper, int serialTypeID, int serialNo)
        {

            string sql = @"typeID={0} and  fromSer<='{1}'
                            and (toSer=-1 or (toSer>='{1}'))";
            DocumentSerialBatch[] ret = helper.GetSTRArrayByFilter<DocumentSerialBatch>(string.Format(sql, serialTypeID, serialNo));
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        // BDE exposed
        public int[] getDocumentListByTypedReference(DocumentTypedReference tref)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = "Select id from {0}.dbo.Document d inner join {0}.dbo.DocumentSerial s on d.id=s.documentID where typeID={1} and reference='{2}' order by primaryDocument desc,tranTicks";
                return reader.GetColumnArray<int>(string.Format(sql, this.DBName, tref.typeID, tref.reference), 0);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public AccountDocumentType[] getDocumentsByTypedReference<AccountDocumentType>(DocumentTypedReference tref) where AccountDocumentType : AccountDocument, new()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = "Select id from {0}.dbo.Document d inner join {0}.dbo.DocumentSerial s on d.id=s.documentID where typeID={1} and reference='{2}' and DocumentTypeID={3}";
                int[] ids = reader.GetColumnArray<int>(string.Format(sql, this.DBName, tref.typeID, tref.reference
                    , GetDocumentTypeByType(typeof(AccountDocumentType)).id
                    ), 0);
                AccountDocumentType[] ret = new AccountDocumentType[ids.Length];
                for (int i = 0; i < ret.Length; i++)
                {
                    ret[i] = GetAccountDocumentInternal(reader, ids[i], true) as AccountDocumentType;
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public int countDocumentsByTypedReference(DocumentTypedReference tref)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = "Select count(*) from {0}.dbo.Document d inner join {0}.dbo.DocumentSerial s on d.id=s.documentID where typeID={1} and reference='{2}'";
                return (int)reader.ExecuteScalar(string.Format(sql, this.DBName, tref.typeID, tref.reference
                    ));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public AccountDocument getDocumentByPrimaryReference(DocumentTypedReference tref, bool fullData)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = "Select id from {0}.dbo.Document d inner join {0}.dbo.DocumentSerial s on d.id=s.documentID where typeID={1} and reference='{2}' and primaryDocument=1";
                int[] ret = reader.GetColumnArray<int>(string.Format(sql, this.DBName, tref.typeID, tref.reference), 0);
                if (ret.Length > 1)
                    throw new ServerUserMessage(string.Format("BUG: multiple primary references found. Type:{0} reference:{1}", tref.typeID, tref.reference));
                if (ret.Length == 0)
                    return null;
                return GetAccountDocumentInternal(reader, ret[0], fullData);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public JournalPrimaryEntry[] GetDocumentJournal(bool filterByDate, DateTime time1, DateTime time2, int[] _docTypes, int[] _refTypes, string textFilter,int pageIndex,int pageSize,TransactionJournalField sortBy,  out int NRecord)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                string sql = @"SELECT     DocumentSerial.documentID, DocumentSerial.typeID, DocumentSerial.reference,tranTicks,ShortDescription
FROM         {0}.dbo.[Document] INNER JOIN
                      {0}.dbo.DocumentSerial ON [Document].ID = DocumentSerial.documentID
WHERE     (DocumentSerial.primaryDocument = 1)";
                sql = string.Format(sql, this.DBName);
                if (filterByDate)
                {
                    sql += string.Format(" and tranTicks>={0} and tranTicks<{1}", time1.Ticks, time2.Ticks);
                }
                if (_docTypes != null && _docTypes.Length > 0)
                {

                    if (_docTypes.Length == 0)
                        sql += " and DocumentTypeID=" + _docTypes[0];
                    else
                    {
                        string typeList = _docTypes[0].ToString();
                        for (int i = 1; i < _docTypes.Length; i++)
                        {
                            typeList += "," + _docTypes[i];
                        }
                        sql += " and DocumentTypeID in (" + typeList + ")";
                    }
                }
                if (_refTypes != null && _refTypes.Length > 0)
                {

                    if (_refTypes.Length == 0)
                        sql += " and typeID=" + _refTypes[0];
                    else
                    {
                        string typeList = _refTypes[0].ToString();
                        for (int i = 1; i < _refTypes.Length; i++)
                        {
                            typeList += "," + _refTypes[i];
                        }
                        sql += " and typeID in (" + typeList + ")";
                    }
                }
                if (!string.IsNullOrEmpty(textFilter))
                {
                    sql += " and (reference like '" + textFilter + "%' or shortDescription like '%" + textFilter + "%')";
                }
                sql += " order by " + (sortBy == TransactionJournalField.Date ? "tranTicks" : "DocumentSerial.reference");
                DataTable table = reader.GetDataTable(sql);
                //SqlDataReader row = reader.ExecuteReader(sql);
                List<JournalPrimaryEntry> ret = new List<JournalPrimaryEntry>();
                int j = -1;
                foreach (DataRow row in table.Rows)
                //while (row.Read())
                {
                    j++;
                    if (pageSize != -1)
                    {
                        if (j < pageIndex||j >= pageIndex + pageSize)
                            continue;
                    }
                    JournalPrimaryEntry s = new JournalPrimaryEntry();
                    s.documentID = (int)row[0];
                    s.typeID = (int)row[1];
                    s.reference = (string)row[2];
                    s.tranTicks= (long)row[3];
                    s.primaryShortDescription = row[4] as string;
                    s.instanceCount = getDocumentTypedReferenceCountInternal(reader, s.typeID, s.reference);
                    ret.Add(s);
                }
                NRecord = j+1;
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(reader);
            }

        }
        int getDocumentTypedReferenceCountInternal(INTAPS.RDBMS.SQLHelper reader, int typeID, string reference)
        {
            string countSql = "Select count(*) from {0}.dbo.DocumentSerial where typeID={1} and reference='{2}'";
            return (int)reader.ExecuteScalar(string.Format(countSql, this.DBName, typeID, reference));
        }

        public int getDocumentTypedReferenceCount(int typeID, string reference)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return getDocumentTypedReferenceCountInternal(reader, typeID, reference);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        // BDE exposed
        public string formatSerialNo(int typeID, int serialNo)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                DocumentSerialBatch sb = getSerialBatchBySerialNo(reader, typeID, serialNo);
                if (sb == null)
                    return null;
                return sb.FormatSerial(serialNo);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        // BDE exposed
        public void voidSerialNo(int AID, DateTime time, DocumentTypedReference reference, string reason)
        {

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    DocumentSerialType type = getDocumentSerialType(reference.typeID);
                    if (type == null)
                        throw new ServerUserMessage("Invalid document serial type ID:" + reference.typeID);

                    if (type.serialType == DocumentSerializationMode.Serialized)
                    {
                        assertSerialReference(reference, time);
                    }
                    if (getDocumentSerialInternal(dspWriter, reference.typeID, reference.reference) != null)
                        throw new ServerUserMessage("Reference " + reference.reference + " already is used");
                    if (isDocumentSerialVoidInternal(dspWriter, reference.typeID, reference.reference))
                        throw new ServerUserMessage("Reference " + reference.reference + " already is void");
                    DocumentSerialVoided v = new DocumentSerialVoided();
                    v.typedReference = reference;
                    v.voidReason = reason;
                    dspWriter.InsertSingleTableRecord(this.DBName, v, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        private DocumentSerialBatch assertSerialReference(DocumentTypedReference reference, DateTime time)
        {
            DocumentSerialBatch[] batch = getSerialBatchByTimeInternal(dspWriter, reference.typeID, time);
            if (batch.Length == 0)
                throw new ServerConfigurationError("Serial batch not found in the database");
            DocumentSerialBatch inRange = null;
            foreach (DocumentSerialBatch b in batch)
            {
                if (b.inRange(reference.serial))
                {
                    inRange = b;
                    break;
                }
            }
            if (inRange == null)
                throw new ServerUserMessage("Serial number " + reference.serial + " not in allowed range");
            return inRange;
        }
        public void undoVoidSerialNo(int AID, DateTime time, DocumentTypedReference reference)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    assertSerialReference(reference, time);
                    if (!isDocumentSerialVoidInternal(dspWriter, reference.typeID, reference.reference))
                        throw new ServerUserMessage("Serial no " + reference.reference + " is not void");
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.DocumentSerialVoided", "typeID=" + reference.typeID + " and reference='" + reference.reference + "'");
                    dspWriter.DeleteSingleTableRecrod<DocumentSerialVoided>(this.DBName, reference.typeID, reference.reference);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        
        private void verifyDocumentSerialBatch(RDBMS.SQLHelper helper, int serialBatchTypeID, int costCenterID)
        {
            int i, j;
            int[] allCostCenters = helper.GetColumnArray<int>(string.Format("Select distinct costCenterID from {0}.dbo.DocumentSerialBatch where typeID={1}", this.DBName, serialBatchTypeID), 0);
            for (i = 0; i < allCostCenters.Length - 1; i++)
            {

                for (j = i + 1; j < allCostCenters.Length; j++)
                {
                    if (this.isInSameHeirarchyInternal<CostCenter>(helper, allCostCenters[i], allCostCenters[j]))
                        throw new ServerUserMessage("Cost center conflict");
                }
            }
            DocumentSerialBatch[] list = helper.GetSTRArrayByFilter<DocumentSerialBatch>("typeID=" + serialBatchTypeID + " and costCenterID=" + costCenterID);

            for (i = 0; i < list.Length - 1; i++)
            {
                DocumentSerialBatch bi = list[i];
                for (j = i + 1; j < list.Length; j++)
                {
                    DocumentSerialBatch bj = list[j];
                    DocumentSerialBatch b1, b2;
                    /*//check time overlap
                    if (bi.fromDate < bj.fromDate)
                    {
                        b1 = bi; b2 = bj;
                    }
                    else if (bi.fromDate > bj.fromDate)
                    {
                        b1 = bj; b2 = bi;
                    }
                    else
                        throw new ServerUserMessage("Time conflict in serial batches");
                    if(!b1.timeUpperBound)
                        throw new ServerUserMessage("Time conflict in serial batches");
                    if(b1.toDate>b2.fromDate)
                        throw new ServerUserMessage("Time conflict in serial batches");*/

                    //check serial overlap
                    if (bi.fromSer < bj.fromSer)
                    {
                        b1 = bi; b2 = bj;
                    }
                    else if (bi.fromSer > bj.fromSer)
                    {
                        b1 = bj; b2 = bi;
                    }
                    else
                        throw new ServerUserMessage("Serial number conflict in serial batches");
                    if (b1.toSer == -1)
                        throw new ServerUserMessage("Serial number conflict in serial batches");
                    if (b1.toSer >= b2.fromSer)
                        throw new ServerUserMessage("Serial number conflict in serial batches");
                }
            }
        }
        // BDE exposed
        public int createDocumentSerialType(int AID, DocumentSerialType serialType)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    DocumentSerialType[] test;
                    if ((test = dspWriter.GetSTRArrayByFilter<DocumentSerialType>("name='" + serialType.name + "'")).Length > 0)
                        throw new ServerUserMessage("Another document serial type with the same name already created");

                    if ((test = dspWriter.GetSTRArrayByFilter<DocumentSerialType>("code='" + serialType.code + "'")).Length > 0)
                        throw new ServerUserMessage("Another document serial type with the same code already created");
                    serialType.id = AutoIncrement.GetKey("Accounting.DocumentSerialTypeID");
                    dspWriter.InsertSingleTableRecord(this.DBName, serialType, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                    return serialType.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public void updateDocumentSerialType(int AID, DocumentSerialType serialType)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    DocumentSerialType oldData = getDocumentSerialType(serialType.id);
                    if (oldData == null)
                        throw new ServerUserMessage("Serial document type not found in the database. ID:" + serialType.id);
                    DocumentSerialType[] test;

                    if ((test = dspWriter.GetSTRArrayByFilter<DocumentSerialType>("name='" + serialType.name + "' and id<>" + serialType.id)).Length > 0)
                        throw new ServerUserMessage("Another document serial type with the same name already created");

                    if ((test = dspWriter.GetSTRArrayByFilter<DocumentSerialType>("code='" + serialType.code + "' and  id<>" + serialType.id)).Length > 0)
                        throw new ServerUserMessage("Another document serial type with the same code already created");
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(DocumentSerialType).Name, "id=" + serialType.id);
                    dspWriter.UpdateSingleTableRecord(this.DBName, serialType, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public void deleteDocumentSerialType(int AID, int documentSerialTypeID)
        {
            string sql = @"SELECT count(*)  FROM [Accounting_2006].[dbo].[DocumentSerial] where typeID={1}";
            sql = string.Format(sql, this.DBName, documentSerialTypeID);

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if ((int)dspWriter.ExecuteNonQuery(sql) > 0)
                        throw new ServerUserMessage("Document serial type is used hence can't be deleted");

                    DocumentSerialType oldData = getDocumentSerialType(documentSerialTypeID);
                    if (oldData == null)
                        throw new ServerUserMessage("Serial document type not found in the database. ID:" + documentSerialTypeID);
                    foreach (DocumentSerialBatch batch in getAllDocumentSerialBatches(documentSerialTypeID))
                        deleteDocumentSerialBatch(AID, batch.id);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(DocumentSerialType).Name, "id=" + documentSerialTypeID);
                    dspWriter.DeleteSingleTableRecrod<DocumentSerialType>(this.DBName, documentSerialTypeID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public DocumentTypedReference getNextTypedReference(int typeID, DateTime date)
        {
            return getCurrentSerialNo(typeID, date,1);
        }
        public DocumentTypedReference getCurrentTypedReference(int typeID, DateTime date)
        {
            return getCurrentSerialNo(typeID, date, 0);
        }
        private DocumentTypedReference getCurrentSerialNo(int typeID, DateTime date,int advance)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                DocumentSerialBatch[] batches = getSerialBatchByTimeInternal(dspReader, typeID, date);
                if (batches.Length > 1)
                    throw new ServerUserMessage("Overlapping document reference ranges found for reference type:" + getDocumentSerialType(typeID).name);
                if (batches.Length == 0)
                    throw new ServerUserMessage("No document refernce range found for reference type: {0}",getDocumentSerialType(typeID).name);
                object max = dspReader.ExecuteScalar(string.Format("Select max(cast (reference as int)) from {0}.dbo.DocumentSerial where typeID={1} and {2}"
                    , this.DBName, typeID, batches[0].toSer == -1 ? string.Format("cast (reference as int)>={0}", batches[0].fromSer) : string.Format("cast (reference as int)>={0} and cast (reference as int)<={1}", batches[0].fromSer, batches[0].toSer)
                    ));
                object maxv = dspReader.ExecuteScalar(string.Format("Select max(cast (reference as int)) from {0}.dbo.DocumentSerialVoided where typeID={1} and {2}"
                    , this.DBName, typeID, batches[0].toSer == -1 ? string.Format("cast (reference as int)>={0}", batches[0].fromSer) : string.Format("cast (reference as int)>={0} and cast (reference as int)<={1}", batches[0].fromSer, batches[0].toSer)
                    ));
                int serialVal;
                if (max is int || maxv is int)
                {
                    int _max = max is int ? (int)max : -1;
                    int _maxv=maxv is int?(int)maxv:-1;
                    if(_max<_maxv)
                       _max=_maxv;
                    serialVal = (int)_max + advance;
                    if (batches[0].fromSer > serialVal)
                        serialVal = batches[0].fromSer;
                    return new DocumentTypedReference(typeID, batches[0].FormatSerial(serialVal), false);
                }
                return new DocumentTypedReference(typeID, batches[0].FormatSerial(batches[0].fromSer), false);;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

    }
}