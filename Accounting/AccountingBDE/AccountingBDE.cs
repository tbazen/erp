using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    
    public partial class AccountingBDE : BDEBase
    {
        public static double FUTURE_TOLERANCE_SECONDS = 120;
        const int MAX_DEPTH = 100;
        CostCenter __defaultCostCenter = null;
        protected CostCenter _defaultCostCenter
        {
            get
            {
                if (__defaultCostCenter == null)
                {
                    __defaultCostCenter = GetAccount<CostCenter>(CostCenter.ROOT_COST_CENTER);
                    if (__defaultCostCenter == null)
                        throw new ServerUserMessage("Default cost center not found, accounting module can't load");
                    if (__defaultCostCenter.PID != -1)
                        throw new ServerUserMessage("Default cost center is not root cost center, accounting module can't load");
                }
                return __defaultCostCenter;
            }
        }

        static bool AccountBalanceEqual(AccountBalance[] bal1, AccountBalance[] bal2)
        {
            int[] Items = new int[bal1.Length + bal2.Length];
            int k = 0;
            foreach (AccountBalance b in bal1)
            {
                for (int i = 0; i < k; i++)
                {
                    Items[k] = b.ItemID;
                    continue;
                }
                bool Found = false;
                foreach (AccountBalance b2 in bal2)
                {
                    if (b.ItemID == b2.ItemID)
                    {
                        Found = true;
                        if (!AccountBase.AmountEqual(b.DebitBalance, b2.DebitBalance))
                            return false;
                    }
                }
                if (!Found && !AccountBase.AmountEqual(b.DebitBalance, 0))
                    return false;
                Items[k++] = b.ItemID;
            }

            foreach (AccountBalance b in bal2)
            {
                for (int i = 0; i < k; i++)
                {
                    Items[k] = b.ItemID;
                    continue;
                }
                bool Found = false;
                foreach (AccountBalance b2 in bal1)
                {
                    if (b.ItemID == b2.ItemID)
                    {
                        Found = true;
                        if (!AccountBase.AmountEqual(b.DebitBalance, b2.DebitBalance))
                            return false;
                    }
                }
                if (!Found && !AccountBase.AmountEqual(b.DebitBalance, 0))
                    return false;
                Items[k++] = b.ItemID;
            }
            return true;

        }

        public AccountingBDE(string bdeName, string DBName, INTAPS.RDBMS.SQLHelper writer, string ReadOnlyConnectionString)
            : base(bdeName, DBName, writer, ReadOnlyConnectionString)
        {
            INTAPS.Evaluator.CalcGlobal.Initialize();
            //initDocCache();
            docDepInit();
        }
        static DateTime ValidDateMin;
        static DateTime ValidDateMax;
        static AccountingBDE()
        {
            ValidDateMin = new DateTime(1600, 1, 1);
            ValidDateMax = new DateTime(2100, 1, 1);
        }
        public static bool IsWithinValidRange(DateTime d)
        {
            return d > ValidDateMin && d < ValidDateMax;
        }
        public static bool IsFutureDate(DateTime d)
        {
            return DateTime.Now.Subtract(d).TotalHours < -1;
        }
        public DateTime getServerTime()
        {
            return DateTime.Now;
        }
    }
}
