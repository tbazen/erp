using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{

    public partial class AccountingBDE : BDEBase
    {
        public void createTranTable(int AID, int accountID, int itemID)
        {
            string createSql = @"USE AccountingTransaction
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON

CREATE TABLE dbo.[tran_{0}_{1}](
	[id] [int] NOT NULL,
	[documentID] [int] NOT NULL,
	[orderN] [int] NOT NULL,
	[tranTicks] [bigint] NOT NULL,
	[amount] [float] NOT NULL,
	[totalDbBefore] [float] NOT NULL,
	[totalCrBefore] [float] NOT NULL,
	[note] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Transaction_{0}_{1}] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



CREATE NONCLUSTERED INDEX [IX_DocID] ON dbo.[tran_{0}_{1}] 
(
	[documentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


CREATE UNIQUE NONCLUSTERED INDEX [IX_Tran_OrderN] ON dbo.[tran_{0}_{1}] 
(
	[orderN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_Tran_ticks] ON dbo.[tran_{0}_{1}] 
(
	[tranTicks] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

";
            dspWriter.ExecuteNonQuery(string.Format(createSql, accountID, itemID));
        }

        public bool tranTableExists(int accountID, int itemID)
        {
            return dspWriter.ExecuteScalar(string.Format("Select Object_ID('AccountingTransaction.dbo.tran_{0}_{1}')", accountID, itemID)) is int;
        }
        public double GetNetCostCenterAccountBalanceAsOfTT(int accountID, int costCenerID, int itemID, DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(accountID, costCenerID);
            if (csa == null)
                return 0;
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                bool creditAccount = isCreditCostCenterAccountInternal(reader, csa.id);
                return GetBalanceAsOfTT(reader,csa.id, itemID, date).GetBalance(creditAccount);
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        public AccountBalance GetBalanceAsOfTT(INTAPS.RDBMS.SQLHelper dspReader, int csAccountID, int ItemID, DateTime date)
        {
            AccountBalance bal = new AccountBalance(csAccountID, ItemID);
            CostCenterAccount csa = GetCostCenterAccount(csAccountID);
            if (csa == null)
                return bal;
            if (csa.isControlAccount)
            {
                AccountBalance ret = new AccountBalance(csa.id, ItemID);
                int[] childs = dspReader.GetColumnArray<int>("Select childCostCenterAccountID from {0}.dbo.CostCenterLeafAccount where parentCostCenterAccountID={1}".format(this.DBName, csa.id));
                if (childs.Length == 0)
                    return ret;
                foreach (int ch in childs)
                    ret.AddBalance(GetBalanceAsOfTT(dspReader, ch, ItemID, date));
                return ret;
            }
            else
            {
                string sql = "SELECT     max(orderN) as orderN FROM AccountingTransaction.dbo.tran_{0}_{1} where tranTicks<{2}";
                object _ordern = dspReader.ExecuteScalar(string.Format(sql, csAccountID, ItemID, date.Ticks));
                if (!(_ordern is int))
                    return bal;
                sql = @"Select 
                            (case when amount>0 then totalDbBefore+Amount else totalDbBefore end) as TotalDebit,
                            (case when amount<0 then totalCrBefore-Amount else totalCrBefore end) as TotalCredit
                        from AccountingTransaction.dbo.tran_{0}_{1} where orderN={2}";

                DataTable dt = dspReader.GetDataTable(string.Format(sql, csAccountID, ItemID, _ordern));
                if (dt.Rows.Count == 0)
                    return bal;
                DataRow row = dt.Rows[0];
                bal.TotalDebit = row[0] is double ? (double)row[0] : 0;
                bal.TotalCredit = row[1] is double ? (double)row[1] : 0;
                docDepAddAccountDependancy(csAccountID, ItemID, date.Ticks, true);
                return bal;
            }
        }
    }

}
