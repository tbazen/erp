﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.Accounting.BDE
{
    public class AccountingEvaluatorUtils
    {
        public static EData toEData(object obj)
        {
            if (obj == null)
            {
                return new EData(DataType.Empty, null);
            }
            else if (obj is string)
            {
                return new EData(DataType.Text, obj);
            }
            else if (obj is int)
            {
                return new EData(DataType.Int, obj);
            }
            else if (obj is Int16)
            {
                return new EData(DataType.Int, (int)(Int16)obj);
            }
            else if (obj is double)
            {
                return new EData(DataType.Float, obj);
            }
            else if (obj is float)
            {
                return new EData(DataType.Float, (double)(float)obj);
            }
            else
                throw new ClientServer.ServerUserMessage(".net type not supported by evaluator-"+obj.GetType());
        }
    }
}
