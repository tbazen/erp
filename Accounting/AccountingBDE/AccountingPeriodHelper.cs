using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using INTAPS.ClientServer;
namespace INTAPS.Accounting.BDE
{
    public partial class AccountingPeriodHelper
    {
        public static AccountingPeriodType GetEthiopianMonthPayPeriod<AccountingPeriodType>(int EthiopianYear, int Month)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType ret = new AccountingPeriodType();
            ret.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, Month, EthiopianYear)).GridDate;
            if (Month < 12)
                ret.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, Month + 1, EthiopianYear)).GridDate;
            else
                ret.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 1, EthiopianYear + 1)).GridDate;
            ret.name = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(Month, ApplicationServer.languageID) + " " + EthiopianYear.ToString();
            return ret;
        }

        public static AccountingPeriodType GetGregorianMonthPayPeriod<AccountingPeriodType>(int Year, int Month)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType ret = new AccountingPeriodType();
            ret.fromDate = new DateTime(Year, Month, 1);
            if (Month < 12)
                ret.toDate = new DateTime(Year, Month + 1, 1);
            else
                ret.toDate = new DateTime(Year + 1, 1, 1);
            ret.name = INTAPS.Ethiopic.EtGrDate.GetGrigMonthName(Month) + " " + Year.ToString();
            return ret;
        }

        public static bool ValidatePeriod<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, int ParentFrom, int ParentTo, int ThisPeriodFrom, int ThisPeriodTo)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType thisFrom;
            AccountingPeriodType thisTo;
            AccountingPeriodType ppParentFrom;
            AccountingPeriodType ppParentTo;

            if (ThisPeriodFrom == -1 && ThisPeriodTo == -1)
            {
                return ParentFrom == -1 && ParentTo == -1;
            }
            if (ThisPeriodFrom != -1 && ThisPeriodTo == -1)
            {
                if (ParentTo != -1)
                    return false;
                if (ParentFrom == -1)
                    return true;
                thisFrom = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ThisPeriodFrom);
                ppParentFrom = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ParentFrom);
                return ppParentFrom.fromDate <= thisFrom.fromDate;

            }
            thisFrom = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ThisPeriodFrom);
            thisTo = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ThisPeriodTo);
            if (thisFrom.fromDate > thisTo.fromDate)
                return false;
            if (ParentFrom == -1 && ParentTo == -1)
                return true;
            if (ParentFrom != -1 && ParentTo == -1)
            {
                ppParentFrom = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ParentFrom);
                return ppParentFrom.fromDate <= thisFrom.fromDate;
            }
            ppParentFrom = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ParentFrom);
            ppParentTo = GetPayPeriodInternal<AccountingPeriodType>(dspReader, table, ParentTo);
            return ppParentFrom.fromDate <= thisFrom.fromDate && ppParentTo.toDate >= thisTo.toDate; ;

        }
        public static AccountingPeriodType GetPeriodForDate<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dsp, string table, DateTime date)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType[] periods = GetPayPeriodByFilter<AccountingPeriodType>(dsp, table, "fromDate<='" + date + "' and toDate>'" + date + "'");
            if (periods.Length == 0)
                return null;
            if (periods.Length > 1)
                throw new ServerUserMessage("Overlapping periods found");
            return periods[0];
        }
        public static AccountingPeriodType[] GetTouchedPeriods<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dsp, string table, DateTime date1, DateTime date2)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            if (date2.Equals(date1))
                return new AccountingPeriodType[0];
            if (table.Equals(InternationalDayPeriod.NAME, StringComparison.CurrentCultureIgnoreCase))
            {
                List<AccountingPeriodType> ret = new List<AccountingPeriodType>();
                DateTime date = date1.Date;
                while (date < date2)
                {
                    ret.Add( (new InternationalDayPeriod(date)) as AccountingPeriodType);
                    date=date.AddDays(1);
                }
                return ret.ToArray();
            }
            return GetPayPeriodByFilter<AccountingPeriodType>(dsp, table, "(fromDate>='" + date1 + "' and fromDate<'" + date2 + "')"
                + " or (fromDate<='" + date1 + "' and toDate>'" + date1 + "')");

        }
        public static AccountingPeriodType[] GetPayPeriodByFilter<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dsp, string table, string Filter)
            where AccountingPeriodType : AccountingPeriod, new()
        {
           

            DataTable dat = dsp.GetDataTable("Select ID,Name,FromDate,ToDate,Days,Hours from " + dsp.ReadDB + ".dbo." + table
                + (string.IsNullOrEmpty(Filter) ? "" : " where " + Filter));
            AccountingPeriodType[] ret = new AccountingPeriodType[dat.Rows.Count];
            int i = 0;
            foreach (DataRow row in dat.Rows)
            {
                ret[i] = new AccountingPeriodType();
                ret[i].id = (int)row[0];
                ret[i].name = (string)row[1];
                ret[i].fromDate = (DateTime)row[2];
                ret[i].toDate = (DateTime)row[3];
                ret[i].days = (double)row[4];
                ret[i].hours = (double)row[5];
                i++;
            }
            dat = null;
            return ret;
        }

        public static AccountingPeriodType GetPayPeriodInternal<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dsp, string table, int PID)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType[] _ret = GetPayPeriodByFilter<AccountingPeriodType>(dsp, table, "ID=" + PID);
            if (_ret.Length == 0)
                throw new ServerUserMessage("Payment period not found in the datbase.");
            return _ret[0];
        }

        public static int CreatePayPeriod<AccountingPeriodType>(string DB, INTAPS.RDBMS.SQLHelper dspWriter, string table, AccountingPeriodType pp)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType pt;
            Console.WriteLine("Creating period name:{0} from:{1} to:{2}", pp.name, pp.fromDate, pp.toDate);

            if ((pt = GetPeriodForDate<AccountingPeriodType>(dspWriter, table, pp.fromDate)) != null)
                throw new ServerUserMessage("The peariod being created overlaps with {0}", pt.name);

            pp.id = INTAPS.ClientServer.AutoIncrement.GetKey("WSIS.PeriodID");
            dspWriter.Insert(DB, table, new string[] { "ID", "Name", "FromDate", "ToDate", "Days", "Hours" }
                , new object[] { pp.id, pp.name, pp.fromDate, pp.toDate, pp.days, pp.hours });
            return pp.id;
        }

        public static void UpdatePayPeriod<AccountingPeriodType>(string DB, INTAPS.RDBMS.SQLHelper dspWriter, string table, AccountingPeriodType pp)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType pt;
            if ((pt = GetPeriodForDate<AccountingPeriodType>(dspWriter, table, pp.fromDate)) != null && pt.id!=pp.id)
                throw new ServerUserMessage("The period update with overlap with {0}", pt.name);

            dspWriter.Update(DB, table, new string[] { "Days", "Hours" }
                , new object[] { pp.days, pp.hours }, "ID=" + pp.id);
        }

        public static AccountingPeriodType[] GetPayPeriods<AccountingPeriodType>(string DB, INTAPS.RDBMS.SQLHelper dspWriter, INTAPS.RDBMS.SQLHelper dspReader, string table, int Year, bool Ethiopian)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType[] ret = new AccountingPeriodType[12];
            for (int m = 1; m <= 12; m++)
            {
                AccountingPeriodType epp;
                if (Ethiopian)
                    epp = AccountingPeriodHelper.GetEthiopianMonthPayPeriod<AccountingPeriodType>(Year, m);
                else
                    epp = AccountingPeriodHelper.GetGregorianMonthPayPeriod<AccountingPeriodType>(Year, m);
                AccountingPeriodType[] ps = GetPayPeriodByFilter<AccountingPeriodType>(dspReader, table, " FromDate='" + epp.fromDate + "' and ToDate='" + epp.toDate + "'");
                if (ps.Length == 0)
                {
                    epp.id = CreatePayPeriod(DB, dspWriter, table, epp);
                }
                else
                    epp = ps[0];
                ret[m - 1] = epp;
            }
            return ret;
        }

        public static AccountingPeriodType GetPayPeriod<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, int PID)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            AccountingPeriodType[] _ret = GetPayPeriodByFilter<AccountingPeriodType>(dspReader, table, "ID=" + PID);
            if (_ret.Length == 0)
                return null;
            return _ret[0];
        }

        public static bool WithinValidPariod<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, int PeriodFrom, int PeriodTo, int Period)
            where AccountingPeriodType : AccountingPeriod, new()
        {
            return ValidatePeriod<AccountingPeriodType>(dspReader, table, PeriodFrom, PeriodTo, Period, Period);
        }

        public static int PeriodDifference<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, AccountingPeriodType period1, AccountingPeriodType period2)
        where AccountingPeriodType : AccountingPeriod, new()
        {
            int sign = 1;
            if (period2.fromDate < period1.fromDate)
            {
                AccountingPeriodType temp = period1;
                temp = period1;
                period1 = period2;
                period2 = temp;
                sign = -1;
            }
            return sign * (int)dspReader.ExecuteScalar("Select count(*) from " + dspReader.ReadDB + "dbo.[" + table + "] where FromDate>='" + period1.fromDate + "' and ToDate<'" + period2.toDate + "'");
        }

        public static AccountingPeriodType[] GetPeriodsInBetweenLeft<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, AccountingPeriodType period1, AccountingPeriodType period2)
        where AccountingPeriodType : AccountingPeriod, new()
        {
            return dspReader.GetSTRArrayByFilter<AccountingPeriodType>("FromDate>='" + period1.fromDate + "' and ToDate<'" + period2.toDate + "'");
        }

        public static AccountingPeriodType[] GetPeriodsInBetweenRight<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, AccountingPeriodType period1, AccountingPeriodType period2)
        where AccountingPeriodType : AccountingPeriod, new()
        {
            return dspReader.GetSTRArrayByFilter<AccountingPeriodType>("FromDate>'" + period1.fromDate + "' and ToDate<='" + period2.toDate + "'");
        }

        public static AccountingPeriodType GetNextPeriod<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, AccountingPeriodType period)
        where AccountingPeriodType : AccountingPeriod, new()
        {
            object date = dspReader.ExecuteScalar(string.Format("Select min(fromDate) from {0}.dbo.{1} where FromDate>'{2}'"
            , dspReader.ReadDB, table, period.fromDate));
            if (date is DateTime)
                return GetPeriodForDate<AccountingPeriodType>(dspReader, table, (DateTime)date);
            return null;
        }
        public static AccountingPeriodType GetPreviousPeriod<AccountingPeriodType>(INTAPS.RDBMS.SQLHelper dspReader, string table, AccountingPeriodType period)
        where AccountingPeriodType : AccountingPeriod, new()
        {
            object date = dspReader.ExecuteScalar(string.Format("Select max(fromDate) from {0}.dbo.{1} where FromDate<'{2}'"
            , dspReader.ReadDB, table, period.fromDate));
            if (date is DateTime)
                return GetPeriodForDate<AccountingPeriodType>(dspReader, table, (DateTime)date);
            return null;
        }

    }
}
