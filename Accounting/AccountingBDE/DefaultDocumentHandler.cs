using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting.BDE
{
    public class DefaultDocumentHandler:IDocumentServerHandler
    {
        // Fields
        protected AccountingBDE bdeAccounting;

        // Methods
        public DefaultDocumentHandler(AccountingBDE bdeFinance)
        {
            this.bdeAccounting = bdeFinance;
        }

        public virtual void DeleteDocument(int AID,int docID)
        {
            //throw new ClientServer.ServerUserMessage("Delete is not supported for this document type.");
            bdeAccounting.DeleteAccountDocument(AID, docID,false);
        }

        public virtual void ReverseDocument(int AID,int docID)
        {
            throw new ClientServer.ServerUserMessage("Reverse is not supported");
        }

        public bool CheckPostPermission(int docID, AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            throw new NotImplementedException();
        }

        public string GetHTML(AccountDocument _doc)
        {
            return _doc.BuildHTML();
        }
    }

}
