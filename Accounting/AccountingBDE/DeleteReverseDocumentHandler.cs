using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting.BDE
{
    public class DeleteReverseDocumentHandler
    {
        // Fields
        protected AccountingBDE bdeAccounting;

        // Methods
        public DeleteReverseDocumentHandler(AccountingBDE bdeFinance)
        {
            this.bdeAccounting = bdeFinance;
        }

        public virtual void DeleteDocument(int AID,int docID)
        {
            lock (this.bdeAccounting.WriterHelper)
            {
                this.bdeAccounting.WriterHelper.BeginTransaction();
                try
                {
                    this.bdeAccounting.DeleteAccountDocument(AID,docID,false);
                    this.bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    this.bdeAccounting.WriterHelper.RollBackTransaction();
                    throw ex;
                }
            }
        }

        public virtual void ReverseDocument(int AID,int docID)
        {
            throw new ClientServer.ServerUserMessage("Reverse is not supported");
        }
        public virtual string GetHTML(AccountDocument _doc)
        {
            return _doc.BuildHTML();
        }
    }

}
