using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
using System.Web;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    
    public partial class EHTML : ISymbolProvider
    {
        public void AddFunction(IFunction f)
        {
            m_funcs.Add(f.Symbol.ToUpper(), f);
            m_docs.Add(f.Symbol.ToUpper(), new FunctionDocumentation(f));
        }
        public void AddVariable(string symbol, EData value)
        {
            int index = m_vars.IndexOf(symbol.ToUpper());
            if (index!=-1)
            {
                //throw new ServerUserMessage(symbol + " already defined.");
                m_values[index] = value;
                m_evaluated[index] = true;
                m_expression[index] = null;
            }
            else
            {
                m_vars.Add(symbol.ToUpper());
                m_values.Add(value);
                m_evaluated.Add(true);
                m_expression.Add(null);

            }
        }
        void InitFunctions()
        {
            m_funcs = new Dictionary<string, IFunction>();
            m_docs = new Dictionary<string, FunctionDocumentation>();
        }
        public string Evaluate(out string headerItems)
        {
            EData data = GetData("_html_");
            if (data.Type == DataType.Error)
                throw new ServerUserMessage(data.Value.ToString());
            headerItems = null;
            if (m_vars.Contains("_HEADERS_"))
            {
                EData headers = GetData("_HEADERS_");
                if (headers.Type == DataType.Text)
                {
                    headerItems = string.IsNullOrEmpty(headers.Value as string) ? "" : headers.Value.ToString();
                }
            }
            return data.ToString();
        }
        public EData Evaluate(string symbol)
        {
            EData data = GetData(symbol);
            return data;
        }
        public void setValue(string symbol, EData value)
        {
            string s = symbol.ToUpper();
            int index = m_vars.IndexOf(s);
            if (index == -1)
            {
                m_vars.Add(s);
                m_values.Add(value);
                m_evaluated.Add(true);
            }
            else
            {
                m_values[index] = value;
                m_evaluated[index] = true;
            }
        }
        
    }
}
