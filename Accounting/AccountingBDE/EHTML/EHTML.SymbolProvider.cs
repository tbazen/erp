using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public partial class EHTML : ISymbolProvider
    {
        Dictionary<string,IFunction> m_funcs;
        Dictionary<string,FunctionDocumentation> m_docs;
        List<string> m_vars;
        List<EData> m_values;
        List<bool> m_evaluated;
        List<string> m_expression;
        
        public EHTML(EHTMLData data)
        {
            InitFunctions();

            m_vars = new List<string>();
            m_vars.AddRange(data.variables);
            m_values = new List<EData>();
            m_values.AddRange(data.values);
            m_evaluated = new List<bool>();
            m_expression = new List<string>();
            m_expression.AddRange(data.expressions);

            m_vars.Add("_HTML_");
            m_values.Add(EData.Empty);
            m_evaluated.Add(false);
            m_expression.Add(data.mainFormula);
            for (int i = 0; i < data.variables.Length; i++)
            {
                m_vars[i] = data.variables[i].ToUpper();
                if (data.expressions[i] == null)
                {
                    m_evaluated.Add(true);
                }
                else
                {
                    m_evaluated.Add(false);
                }
            }
        }
        
        #region ISymbolProvider Members

        public IFunction GetFunction(string symbol)
        {
            symbol = symbol.ToUpper();
            if (m_funcs.ContainsKey(symbol))
                return m_funcs[symbol];
            if (CalcGlobal.Functions.Contains(symbol))
                return (IFunction) CalcGlobal.Functions[symbol];
            throw new ServerUserMessage("Unknown function " + symbol);
        }
        List<string> refs=new List<string>();
        public EData GetData(string symbol)
        {
            symbol = symbol.ToUpper();
            int i;
            if((i=m_vars.IndexOf(symbol))==-1)
                throw new ServerUserMessage("Undefind variable " + symbol);
            if (refs.Contains(symbol))
                throw new ServerUserMessage("Circular reference " + symbol);
            if (m_evaluated[i])
                return m_values[i];

            try
            {
                refs.Add(symbol);
                if (m_expression[i] != null)
                {
                    Symbolic s = new Symbolic();
                    s.SetSymbolProvider(this);                    
                    s.Expression = m_expression[i];
                    EData res = s.Evaluate();
                    m_values[i] = res;
                    m_evaluated[i] = true;
                    return res;
                }
                throw new ServerUserMessage("Undefind variable " + symbol);
            }
            finally
            {
                refs.Remove(symbol);
            }
        }

        public IFunction GetFunction(URLIden iden)
        {
            return GetFunction(iden.Element);
        }

        public EData GetData(URLIden iden)
        {
            return GetData(iden.Element);
        }

        public FunctionDocumentation[] GetAvialableFunctions()
        {
            FunctionDocumentation[] ret = new FunctionDocumentation[m_docs.Count
                /*+CalcGlobal.DocumentedFunctions.Count*/];
            m_docs.Values.CopyTo(ret, 0);
            //CalcGlobal.DocumentedFunctions.Values.CopyTo(ret, m_docs.Count);
             
            return ret;
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            if(m_docs.ContainsKey(Symbol))
                return m_docs[Symbol];
            if (CalcGlobal.DocumentedFunctions.ContainsKey(Symbol))
                 return (FunctionDocumentation) CalcGlobal.DocumentedFunctions[Symbol];
             throw new ServerUserMessage("Undefined function " + Symbol);
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return GetDocumentation(iden.Element);
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return GetDocumentation(f.Symbol);
        }

        public bool SymbolDefined(string Name)
        {
            Name = Name.ToUpper();
            foreach (string v in m_vars)
                if (v == Name)
                    return true;
            return false;
        }

        #endregion
    }
}
