﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.Accounting.BDE
{
    public class EvaluateEHTMLFunction:IVarParamCountFunction
    {
        int _nPar;
        AccountingBDE _bde;
        public EvaluateEHTMLFunction(AccountingBDE bde)
        {
            this._bde = bde;
        }
        public IVarParamCountFunction Clone()
        {
            EvaluateEHTMLFunction ret = new EvaluateEHTMLFunction(_bde);
            ret._nPar = _nPar;
            return ret;
        }

        public bool SetParCount(int n)
        {
            _nPar = n;
            return n > 0;
        }

        public EData Evaluate(EData[] Pars)
        {
            string code=Pars[0].Value  as string;
            if (code==null)
                return new FSError("First parameter should be HTML formula defination code-EvalHtml").ToEData();

            List<object> pars = new List<object>();
            for (int i = 0; i < (Pars.Length - 1) / 2; i++)
            {
                string name=Pars[2*i+1].Value as string;
                if(name==null)
                    return new FSError("Parameter "+(2*i+2)+" should be var name-EvalHtml").ToEData();
                pars.Add(name);
                pars.Add(Pars[2 * i + 2]);
            }
            string headers;
            string res=_bde.EvaluateEHTML(code, pars.ToArray(),out headers);
            return new EData(DataType.Text, res);
        }

        public string Name
        {
            get { return "Evaluate HTML Formula"; }
        }

        public int ParCount
        {
            get { return _nPar; }
        }

        public string Symbol
        {
            get { return "EvalHTML"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
}
