﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.Accounting.BDE
{
    class HTMLChartFunction : IVarParamCountFunction
    {
        int _npar;
        public IVarParamCountFunction Clone()
        {
            return new HTMLChartFunction() { _npar = _npar };
        }

        //data,labels,type
        public bool SetParCount(int n)
        {
            bool support = n >= 3 && n <= 4;
            _npar = n;
            return support;
        }

        public EData Evaluate(EData[] Pars)
        {
            string canvsID = Pars[0].Value as string;
            if (canvsID == null)
                return new FSError("First parameter should be canvas ID- HMTLChartFunction").ToEData();

            ListData data = Pars[1].Value as ListData;
            if (data == null)
                return new FSError("Second parameter should be list data - HMTLChartFunction").ToEData();
            ListData labels = null;
            labels = Pars[2].Value as ListData;
            if(labels==null)
                return new FSError("Third parameter should be list data - HMTLChartFunction").ToEData();

            //chart type: 1-line chart, 2-bar chart, 3-pie chart
            int chartType = 1;
            if (Pars.Length > 3)
            {
                chartType = Convert.ToInt32(Pars[3].Value);
            }
            string script = @" var chart_data{0}= {
		                        labels : {1},
		                        datasets :{2}
	                        }";

            return new EData(DataType.Text, script);
        }

        public string Name
        {
            get { throw new NotImplementedException(); }
        }

        public int ParCount
        {
            get { throw new NotImplementedException(); }
        }

        public string Symbol
        {
            get { throw new NotImplementedException(); }
        }

        public FunctionType Type
        {
            get { throw new NotImplementedException(); }
        }
    }
}
