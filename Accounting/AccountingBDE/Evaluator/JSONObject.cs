namespace INTAPS.Evaluator
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Runtime.Serialization.Formatters.Binary;
    
    internal class JSONObject : IVarParamCountFunction, IFunction
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new JSONObject() { m_ParCount = this.m_ParCount };
        }

        public EData Evaluate(EData[] Pars)
        {
            string xml = Pars[0].Value as string;
            Type type = System.Type.GetType(Pars[1].Value as string);
            
            /*Type[] types = new Type[Pars.Length-2];
            for(int i=2;i<Pars.Length;i++)
            {
                types[i - 2] = System.Type.GetType(Pars[i].Value as string);
            }*/

            object val = Newtonsoft.Json.JsonConvert.DeserializeObject(xml, type);

            covertJOBjectToString(val);
            return new EData(val);
        }
        static void covertJOBjectToString(object obj)
        {
            if (obj == null)
                return;
            if(obj.GetType().IsArray)
            {
                Array ar = (Array)obj;
                int index=0;
                foreach(Object el in ar)
                {
                    if (obj is Newtonsoft.Json.Linq.JObject)
                    {
                        ar.SetValue(el.ToString(),index);
                    }
                    else
                        covertJOBjectToString(el);
                    index++;
                }
                return;
            }
            foreach (System.Reflection.PropertyInfo pi in obj.GetType().GetProperties())
            {
                if (pi.GetSetMethod() == null)
                    continue;
                bool isStatic = false;
                
                foreach(System.Reflection.MethodInfo mi in pi.GetAccessors())
                {
                    if(mi.IsStatic)
                    {
                        isStatic = true;
                        break;
                    }
                }
                if (isStatic)
                    continue;
                if (pi.GetIndexParameters().Length > 0)
                    continue;
                object prop = pi.GetValue(obj);
                if (prop is Newtonsoft.Json.Linq.JObject)
                    pi.SetValue(obj, prop.ToString());
                else
                    covertJOBjectToString(prop);
            }
            foreach (System.Reflection.FieldInfo fi in obj.GetType().GetFields())
            {
                if (fi.IsStatic)
                    continue;
                object fld = fi.GetValue(obj);
                if (fld is Newtonsoft.Json.Linq.JObject)
                    fi.SetValue(obj, fld.ToString());
                else
                    covertJOBjectToString(fld);
            }
        }
        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return n>1;
        }

        public string Name
        {
            get
            {
                return "JSON Object from string";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "JSONObject";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }

    internal class BinObject : IVarParamCountFunction, IFunction
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new BinObject() { m_ParCount = this.m_ParCount };
        }

        public EData Evaluate(EData[] Pars)
        {
            byte[] bytes = Pars[0].Value as byte[];
            BinaryFormatter s = new BinaryFormatter();
            return new EData(s.Deserialize(new System.IO.MemoryStream(bytes)));
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return n > 1;
        }

        public string Name
        {
            get
            {
                return "BinObject";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "BinObject";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

