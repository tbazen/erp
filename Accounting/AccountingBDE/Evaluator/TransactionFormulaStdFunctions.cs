using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
using System.Data.SqlClient;
using System.Data;
namespace INTAPS.Accounting.BDE
{
    public class httpEndPoint:IFunction
    {

        public EData Evaluate(EData[] Pars)
        {
            return new EData(System.Configuration.ConfigurationManager.AppSettings["httpEndPoint"]);
        }

        public string Name
        {
            get { return "http root"; }
        }

        public int ParCount
        {
            get { return 0; }
        }

        public string Symbol
        {
            get { return "httpRoot"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class TodayFunction : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            return new EData(DataType.DateTime, DateTime.Today);
        }

        public string Name
        {
            get { return "Get Start of Day Time"; }
        }

        public int ParCount
        {
            get { return 0; }
        }

        public string Symbol
        {
            get { return "Today"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class GetTicksFunction : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            
            return new EData(DataType.LongInt, ((DateTime)Pars[0].Value).Ticks);
        }

        public string Name
        {
            get { return "Ticks"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "Ticks"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }

    public class TimeSpanText : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {

            var ticks = Convert.ToInt64(Pars[0].Value);
            var ts = new TimeSpan(ticks);
            if (ts.TotalSeconds < 10)
            {
                return new EData("Just Now");
            }
            else if (ts.TotalMinutes<1)
            {
                return new EData(Math.Round(ts.TotalSeconds) + " Seconds");
            }
            else if (ts.TotalHours < 1)
            {
                return new EData(Math.Round(ts.TotalMinutes) + " Minutes");
            }
            else if (ts.TotalDays< 1)
            {
                return new EData(Math.Round(ts.TotalHours) + " Hours");
            }
            else
                return new EData(Math.Round(ts.TotalDays) + " Days");
        }

        public string Name
        {
            get { return "TicksText"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "TicksText"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class GetBalance : IVarParamCountFunction
    {
        int nPar = 1;
        AccountingBDE m_Accounting;
        public GetBalance(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members
        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length == 0)
                return new EData(DataType.Error, "Account number not specified.");
            int ItemID = 0;
            if (Pars.Length > 1)
                ItemID = Convert.ToInt32(Pars[1].Value);
            if (Pars[0].Value is ListData)
            {
                ListData acs = (ListData)Pars[0].Value;
                int[] acids = new int[acs.elements.Length];
                int i;
                for (i = 0; i < acids.Length; i++)
                    acids[i] = Convert.ToInt32(acs[i].Value);
                DateTime dt;
                if (Pars.Length > 2)
                {
                    dt = (DateTime)Pars[2].Value;
                }
                else
                    dt = DateTime.Now;
                AccountBalance[] bal = new AccountBalance[acids.Length];
                for(i=0;i<acids.Length;i++)
                    bal[i]=m_Accounting.GetBalanceAsOf(acids[i], ItemID, dt);
                ListData _ret = new ListData(bal.Length);
                for (i = 0; i < acids.Length; i++)
                {
                    ListData d = new ListData(new EData[]{new EData(DataType.Float,bal[i].TotalDebit)
                    ,new EData(DataType.Float,bal[i].TotalCredit)});
                    _ret.elements[i] = new EData(DataType.ListData, d);
                }
                return new EData(DataType.ListData, _ret);
            }
            int AcccountID = Convert.ToInt32(Pars[0].Value);
            if (Pars.Length > 2)
                if (Pars[2].Type == DataType.DateTime)
                {
                    double bal = m_Accounting.GetNetBalanceAsOf(AcccountID, ItemID, (DateTime)Pars[2].Value);
                    return new EData(DataType.Float, bal);
                }
                else
                    return new FSError("Third parameter must be balance date-GetBalance").ToEData();
            return new EData(DataType.Float, m_Accounting.GetNetBalanceAsOf(AcccountID, ItemID,DateTime.Now));
        }

        public string Name
        {
            get
            {
                return "Account Balance";
            }
        }

        public int ParCount
        {
            get
            {
                return nPar;
            }
        }

        public string Symbol
        {
            get { return "AcBal"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion

        #region IVarParamCountFunction Members

        public IVarParamCountFunction Clone()
        {
            return new GetBalance(m_Accounting);
        }

        public bool SetParCount(int n)
        {
            nPar = n;
            return n < 4;
        }

        #endregion
    }
    public class GetBalanceSingleValue : IVarParamCountFunction
    {
        int nPar = 1;
        AccountingBDE m_Accounting;
        public GetBalanceSingleValue(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members
        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length == 0)
                return new EData(DataType.Error, "Account number not specified.");
            int ItemID = 0;
            if (Pars.Length > 1)
                ItemID = Convert.ToInt32(Pars[1].Value);

            DateTime dt;
            if (Pars.Length > 2)
            {
                dt = (DateTime)Pars[2].Value;
            }
            else
                dt = DateTime.Now;

            if (Pars[0].Value is ListData)
            {
                ListData acs = (ListData)Pars[0].Value;
                int[] acids = new int[acs.elements.Length];
                int i;
                AccountBalance[] bal=new AccountBalance[acids.Length];
                for (i = 0; i < acids.Length; i++)
                {
                    acids[i] = Convert.ToInt32(acs[i].Value);
                    bal[i] = m_Accounting.GetBalanceAsOf(acids[i], ItemID, dt);
                }
                ListData _ret = new ListData(bal.Length);

                for (i = 0; i < acids.Length; i++)
                {
                    Account ac = m_Accounting.GetAccount<Account>(acids[i]);
                    _ret.elements[i] = new EData(DataType.Float, ac.CreditAccount ? -bal[i].DebitBalance : bal[i].DebitBalance);
                }
                return new EData(DataType.ListData, _ret);
            }
            else
            {
                int AcccountID = Convert.ToInt32(Pars[0].Value);
                Account ac = m_Accounting.GetAccount<Account>(AcccountID);
                AccountBalance bal = m_Accounting.GetBalanceAsOf(AcccountID, ItemID, dt);
                return new EData(DataType.Float, ac.CreditAccount ? -bal.DebitBalance : bal.DebitBalance);
            }
        }

        public string Name
        {
            get
            {
                return "Account Balance Single Value";
            }
        }

        public int ParCount
        {
            get
            {
                return nPar;
            }
        }

        public string Symbol
        {
            get { return "AcBalSV"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion

        #region IVarParamCountFunction Members

        public IVarParamCountFunction Clone()
        {
            return new GetBalanceSingleValue(m_Accounting);
        }

        public bool SetParCount(int n)
        {
            nPar = n;
            return n < 4;
        }

        #endregion
    }
    public class GetIndent : IVarParamCountFunction
    {
        int nPar = 2;
        AccountingBDE m_Accounting;
        public GetIndent(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members
        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length == 0)
                return new EData(DataType.Error, "Account number not specified.");
            if (Pars[0].Type == DataType.Error)
                return Pars[0]; 
            int AcccountID = Convert.ToInt32(Pars[0].Value);

            INTAPS.RDBMS.SQLHelper reader = m_Accounting.GetReaderHelper();
            try
            {
                return new EData(DataType.Int, m_Accounting.ExpandParents<Account>(reader, m_Accounting.GetAccount<Account>(AcccountID).PID).Length);
            }
            finally
            {
                m_Accounting.ReleaseHelper(reader);
            }
        }

        public string Name
        {
            get
            {
                return "Indent";
            }
        }

        public int ParCount
        {
            get
            {
                return nPar;
            }
        }

        public string Symbol
        {
            get { return "Indent"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion

        #region IVarParamCountFunction Members

        public IVarParamCountFunction Clone()
        {
            return new GetIndent(m_Accounting);
        }

        public bool SetParCount(int n)
        {
            nPar = n;
            return n == 1;
        }

        #endregion
    }
    public class GetFlow : IVarParamCountFunction
    {
        AccountingBDE m_Accounting;
        public GetFlow(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members

        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length < 3)
                return new EData(DataType.Error, "To few arugments - GetFlow.");
            int AcccountID = (int)Pars[0].Value;
            int ItemID = 0;
            DateTime d1 = (DateTime)Pars[1].Value;
            DateTime d2 = (DateTime)Pars[2].Value;
            ListData ret = new ListData(2);
            AccountBalance bal = m_Accounting.GetFlow(AcccountID, ItemID, d1, d2);
            ret[0] = new EData(DataType.Float, bal.TotalDebit);
            ret[1] = new EData(DataType.Float, bal.TotalCredit);
            return new EData(DataType.ListData, ret);
        }

        public string Name
        {
            get
            {
                return "Account flow";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get { return "AcFlow"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion

        #region IVarParamCountFunction Members

        public IVarParamCountFunction Clone()
        {
            return new GetFlow(m_Accounting);
        }

        public bool SetParCount(int n)
        {
            return n == 3;
        }

        #endregion
    }
    public class GetFlowBal : IVarParamCountFunction
    {
        AccountingBDE m_Accounting;
        public GetFlowBal(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members

        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length < 3)
                return new EData(DataType.Error, "Too few arugments - GetFlow.");
            int AcccountID = (int)Pars[0].Value;
            int ItemID = 0;
            DateTime d1 = (DateTime)Pars[1].Value;
            DateTime d2 = (DateTime)Pars[2].Value;
            ListData ret = new ListData(2);
            CostCenterAccount ac = m_Accounting.GetCostCenterAccount(AcccountID);
            AccountBalance bal = m_Accounting.GetFlow(AcccountID, ItemID, d1, d2);
            return new EData(DataType.Float, ac.creditAccount ? -bal.DebitBalance : bal.DebitBalance);
        }

        public string Name
        {
            get
            {
                return "Account flow balance";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get { return "AcFlowBal"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion

        #region IVarParamCountFunction Members

        public IVarParamCountFunction Clone()
        {
            return new GetFlowBal(m_Accounting);
        }

        public bool SetParCount(int n)
        {
            return n == 3;
        }

        #endregion
    }
    public class AFAccount : IVarParamCountFunction
    {
        AccountingBDE m_Accounting;
        public AFAccount(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members

        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length == 0)
                return new EData(DataType.Error, "Account number not specified.");
            int ret;
            if (Pars.Length==1)
                ret= m_Accounting.GetAccountID<Account>(Pars[0].Value.ToString());
            else
            {
                int costCenterAccountID;
                if (Pars[1].Type==DataType.Int || Pars[1].Type == DataType.Float)
                    costCenterAccountID = Convert.ToInt32(Pars[1].Value);
                else
                    costCenterAccountID = m_Accounting.GetAccountID<CostCenter>(Pars[1].Value.ToString());
                int accountID;
                if (Pars[0].Type == DataType.Int || Pars[0].Type == DataType.Float)
                    accountID = Convert.ToInt32(Pars[0].Value);
                else
                    accountID = m_Accounting.GetAccountID<Account>(Pars[0].Value.ToString());

                ret = m_Accounting.GetCostCenterAccountID(costCenterAccountID,accountID);
            }
            return new EData(DataType.Int, ret);
        }

        public IVarParamCountFunction Clone()
        {
            return new AFAccount(this.m_Accounting) { n_pars = n_pars };
        }
        int n_pars = 1;
        public bool SetParCount(int n)
        {
            if (n < 1 || n > 2)
                return false;
            n_pars = n;
            return true;
        }

        public string Name
        {
            get
            {
                return "Get Account";
            }
        }

        public int ParCount
        {
            get
            {
                return n_pars;
            }
        }

        public string Symbol
        {
            get { return "Account"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
        #endregion
    }

    public class ChartOfAcccountFunction : IVarParamCountFunction
    {
        AccountingBDE m_Accounting;
        public ChartOfAcccountFunction(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        int m_nPar = 2;
        #region IVarParamCountFunction Members

        public bool SetParCount(int n)
        {
            m_nPar = n;
            return n < 3;
        }

        public IVarParamCountFunction Clone()
        {
            ChartOfAcccountFunction f = new ChartOfAcccountFunction(m_Accounting);
            f.m_nPar = m_nPar;
            return f;
        }

        #endregion

        #region IFunction Members

        public string Name
        {
            get { return "Chart of Accounts"; }
        }

        public string Symbol
        {
            get { return "ChartOfAccounts"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public int ParCount
        {
            get { return m_nPar; }
        }
        void GetChildAccounts(INTAPS.RDBMS.SQLHelper reader, string fields, int PID, List<int> exclude, List<EData> data)
        {
            string sql = "Select ID," + fields + " from " +
                m_Accounting.DBName + ".dbo.Account where PID=" + PID + " order by Code";
            DataTable t = reader.GetDataTable(sql);
            foreach (DataRow row in t.Rows)
            {
                ListData rowData = new ListData(t.Columns.Count);
                int i = 0;
                foreach (object o in row.ItemArray)
                {
                    rowData.elements[i] = INTAPS.Evaluator.DBConSql.SqlToEData(o);
                    i++;
                }
                data.Add(new EData(DataType.ListData, rowData));
                int aid = (int)row[0];
                if (!exclude.Contains(aid))
                {
                    GetChildAccounts(reader, fields, aid, exclude, data);
                }
            }
        }
        public EData Evaluate(EData[] Pars)
        {
            string fields = Pars[0].ToString();
            List<int> exclude = new List<int>();
            if (Pars.Length > 1)
            {
                foreach (EData data in ((ListData)Pars[1].Value).elements)
                {
                    exclude.Add(Convert.ToInt32(data.Value));
                }
            }
            INTAPS.RDBMS.SQLHelper reader = m_Accounting.GetReaderHelper();
            try
            {
                List<EData> data = new List<EData>();
                GetChildAccounts(reader, fields, -1, exclude, data);
                return new EData(DataType.ListData, new ListData(data.ToArray()));
            }
            finally
            {
                m_Accounting.ReleaseHelper(reader);
            }


        }

        #endregion
    }

    public class TrialBalanceFunction : IFunction
    {
        AccountingBDE m_Accounting;
        public TrialBalanceFunction(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        int m_nPar = 2;
        #region IFunction Members

        public string Name
        {
            get { return "Trial Balance"; }
        }

        public string Symbol
        {
            get { return "TrialBalance"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public int ParCount
        {
            get { return 3; }
        }
        void GetChildAccounts(INTAPS.RDBMS.SQLHelper reader,int balanceItem,DateTime balanceDate, string fields, int PID, List<int> exclude, List<EData> data,out int NChilds)
        {
            string sql = "Select ID," + fields + ",CreditAccount from " +
                m_Accounting.DBName + ".dbo.Account where PID=" + PID + " order by Code";
            DataTable t = reader.GetDataTable(sql);
            NChilds = 0;

            foreach (DataRow row in t.Rows)
            {
                ListData rowData = new ListData(t.Columns.Count+1);
                int i = 0;
                foreach (object o in row.ItemArray)
                {
                    if(i<rowData.elements.Length)
                        rowData.elements[i] = INTAPS.Evaluator.DBConSql.SqlToEData(o);
                    i++;
                }
                
                
                Console.WriteLine(data.Count+"                 ");
                //Console.CursorTop--;
                int aid = (int)row[0];
                int nch=0;
                int insertIndex = data.Count;
                if (!exclude.Contains(aid))
                {
                    GetChildAccounts(reader,balanceItem,balanceDate, fields, aid, exclude, data,out nch);
                }
                if (nch == 0)
                {
                    AccountBalance bal = m_Accounting.GetBalanceAsOf(aid, balanceItem, balanceDate);
                    if (Account.AmountGreater(bal.TotalCredit + bal.TotalDebit, 0))
                    {
                        if ((bool)row[t.Columns.Count - 1])
                        {
                            rowData[t.Columns.Count - 1] = new EData(DataType.Float, 0.0d);
                            rowData[t.Columns.Count] = new EData(DataType.Float, bal.TotalCredit - bal.TotalDebit);
                        }
                        else
                        {
                            rowData[t.Columns.Count - 1] = new EData(DataType.Float, bal.TotalDebit - bal.TotalCredit);
                            rowData[t.Columns.Count] = new EData(DataType.Float, 0.0d);
                        }
                        data.Insert(insertIndex,new EData(DataType.ListData,  rowData));
                    }
                }
                NChilds++;
            }
        }
        public EData Evaluate(EData[] Pars)
        {
            List<int> exclude = new List<int>();
            foreach (EData data in ((ListData)Pars[0].Value).elements)
            {
                exclude.Add(Convert.ToInt32(data.Value));
            }
            int itemID = Convert.ToInt32(Pars[1].Value);
            DateTime date = Convert.ToDateTime(Pars[2].Value);

            INTAPS.RDBMS.SQLHelper reader = m_Accounting.GetReaderHelper();
            try
            {
                List<EData> data = new List<EData>();
                int nch;
                GetChildAccounts(reader,itemID,date, "Name,Code", -1, exclude, data,out nch);
                return new EData(DataType.ListData, new ListData(data.ToArray()));
            }
            finally
            {
                m_Accounting.ReleaseHelper(reader);
            }


        }

        #endregion
    }

    public class FlowTableFunction : IFunction
    {
        AccountingBDE m_Accounting;
        public FlowTableFunction(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        int m_nPar = 2;
        #region IFunction Members

        public string Name
        {
            get { return "Flow Table"; }
        }

        public string Symbol
        {
            get { return "FlowTable"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public int ParCount
        {
            get { return 6; }
        }
        void GetChildAccounts(INTAPS.RDBMS.SQLHelper reader, int balanceItem, DateTime balanceDate1, DateTime balanceDate2, string fields, int costCenterID, int PID, List<int> exclude, List<EData> data, out int NChilds)
        {
            string sql = "Select ID," + fields + ",CreditAccount from " +
                m_Accounting.DBName + ".dbo.Account where PID=" + PID + " order by Code";
            DataTable t = reader.GetDataTable(sql);
            NChilds = 0;
            int c = t.Rows.Count;
            Console.WriteLine();
            foreach (DataRow row in t.Rows)
            {
                //Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                   ",c--,row[2]);
                ListData rowData = new ListData(t.Columns.Count + 2);
                int i = 0;
                foreach (object o in row.ItemArray)
                {
                    if (i < rowData.elements.Length)
                        rowData.elements[i] = INTAPS.Evaluator.DBConSql.SqlToEData(o);
                    i++;
                }
                int accountID = (int)row[0];
                int nch = 0;
                int insertIndex = data.Count;
                if (!exclude.Contains(accountID))
                {
                    GetChildAccounts(reader, balanceItem, balanceDate1, balanceDate2, fields, costCenterID, accountID, exclude, data, out nch);
                }
                if (nch == 0)
                {
                    CostCenterAccount csa = m_Accounting.GetCostCenterAccount(costCenterID, accountID);
                    if (csa != null)
                    {
                        AccountBalance bal = m_Accounting.GetFlow(csa.id, balanceItem, balanceDate1,balanceDate2);
                        if (Account.AmountGreater(bal.TotalCredit + bal.TotalDebit, 0))
                        {
                            rowData[t.Columns.Count - 1] = new EData(DataType.Float, bal.TotalDebit);
                            rowData[t.Columns.Count] = new EData(DataType.Float, bal.TotalCredit);
                            if ((bool)row[t.Columns.Count - 1]) //credit account
                            {
                                rowData[t.Columns.Count+1] = new EData(DataType.Float, bal.TotalCredit - bal.TotalDebit);
                            }
                            else
                            {
                                rowData[t.Columns.Count + 1] = new EData(DataType.Float, bal.TotalDebit - bal.TotalCredit);
                            }
                            data.Insert(insertIndex, new EData(DataType.ListData, rowData));
                        }
                    }
                }
                NChilds++;
            }
            //Console.CursorTop--;
            Console.WriteLine("                   ");
            //Console.CursorTop--;
        }
        public EData Evaluate(EData[] Pars)
        {
            int costCenterID= Convert.ToInt32(Pars[0].Value);
            
            List<int> include = new List<int>();
            foreach (EData data in ((ListData)Pars[1].Value).elements)
            {
                if(data.Value is string)
                    include.Add(m_Accounting.GetAccount<Account>((string)data.Value).id);
                else
                    include.Add(Convert.ToInt32(data.Value));
            }
            List<int> exclude = new List<int>();
            foreach (EData data in ((ListData)Pars[2].Value).elements)
            {
                if (data.Value is string)
                    exclude.Add(m_Accounting.GetAccount<Account>((string)data.Value).id);
                else
                    exclude.Add(Convert.ToInt32(data.Value));
            }
            int itemID = Convert.ToInt32(Pars[3].Value);
            DateTime date = Convert.ToDateTime(Pars[4].Value);
            DateTime date2 = Convert.ToDateTime(Pars[5].Value);

            INTAPS.RDBMS.SQLHelper reader = m_Accounting.GetReaderHelper();
            try
            {
                List<EData> lst = new List<EData>();
                int nch;
                foreach(int inc in include)
                    GetChildAccounts(reader, itemID, date, date2, "Name,Code", costCenterID, inc, exclude, lst, out nch);
                return new EData(DataType.ListData, new ListData(lst.ToArray()));
            }
            finally
            {
                m_Accounting.ReleaseHelper(reader);
            }


        }

        #endregion
    }
    public class TransactionRegisterFunction : IFunction
    {
        AccountingBDE m_Accounting;
        public TransactionRegisterFunction(AccountingBDE accounting)
        {
            m_Accounting = accounting;
        }
        #region IFunction Members

        public string Name
        {
            get { return "Transaction Register"; }
        }

        public string Symbol
        {
            get { return "AC_TranList"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public int ParCount
        {
            get { return 7; }
        }

        public EData Evaluate(EData[] Pars)
        {
            ListData acs = (ListData)Pars[0].Value;
            int[] accountID1 = new int[acs.elements.Length];
            bool debit1 = (bool)Pars[1].Value;

            for (int j = 0; j < accountID1.Length; j++)
                accountID1[j] = (int)acs[j].Value;

            acs = (ListData)Pars[2].Value;
            int[] accountID2 = new int[acs.elements.Length];
            bool debit2 = (bool)Pars[3].Value;

            for (int j = 0; j < accountID2.Length; j++)
                accountID2[j] = (int)acs[j].Value;

            DateTime fromDate = (DateTime)Pars[4].Value;
            DateTime toDate = (DateTime)Pars[5].Value;

            ListData ld = (ListData)Pars[6].Value;
            int[] ids = new int[ld.elements.Length];
            for (int i = 0; i < ids.Length; i++)
                ids[i] = Convert.ToInt32(ld.elements[i]);
            int N;
            AccountDocument[] trans = m_Accounting.GetDocumentsByAccount(
                accountID1, debit1, accountID2, debit2, 0, ids
                , fromDate, toDate, false, 0, -1, out N);
            ListData _ret = new ListData(trans.Length);
            for (int i = 0; i < trans.Length; i++)
            {
                ListData row = new ListData(4);
                row[0] = new EData(DataType.DateTime, trans[i].DocumentDate);
                row[1] = new EData(DataType.Text, trans[i].PaperRef);
                row[2] = new EData(DataType.Text, trans[i].ShortDescription);
                row[3] = new EData(DataType.Int, trans[i].DocumentTypeID);
                _ret[i] = new EData(DataType.ListData, row);
            }
            return new EData(DataType.ListData, _ret);
        }

        #endregion
    }
    public class DocumentListFunction : IDynamicFormulaFunction
    {
        AccountingBDE m_accounting = null;
        AccountDocument m_doc = null;
        string DateVar = "DATE", RefVar = "REF", TypeIDVar = "TYPEID",TransVar = "TRANS";

        bool fullDataLoaded = false;
        void LoadFullData()
        {
            if (!fullDataLoaded)
            {
                INTAPS.RDBMS.SQLHelper reader = m_accounting.GetReaderHelper();
                try
                {
                    string xml = reader.ExecuteScalar("Select DocumentData from " + m_accounting.DBName + ".dbo.Document where ID=" + m_doc.AccountDocumentID) as string;
                    AccountDocument fullDoc = AccountDocument.DeserializeXML(xml, m_accounting.GetDocumentTypeByID(m_doc.DocumentTypeID).GetTypeObject());
                    fullDoc.CoypFrom(m_doc);
                    m_doc = fullDoc;
                    fullDataLoaded = true;
                }
                finally
                {
                    m_accounting.ReleaseHelper(reader);
                }
            }
        }

        public DocumentListFunction(AccountingBDE accounting)
        {
            m_accounting = accounting;
        }
        #region IDynamicFormulaFunction Members

        public EData GetData(string symbol)
        {
            if (m_doc == null)
                return EData.Empty;
            string su = symbol.ToUpper();
            if (su == DateVar)
                return new EData(DataType.DateTime, m_doc.DocumentDate);
            if (su == TypeIDVar)
                return new EData(DataType.Int, m_doc.DocumentTypeID);
            if (su == TransVar)
            {
                TransactionOfBatch[] trans = m_accounting.GetTransactionsOfDocument(m_doc.AccountDocumentID);
                ListData ld = new ListData(trans.Length);
                for (int i = 0; i < trans.Length; i++)
                {
                    ListData row = new ListData(5);
                    row[0] = new EData(DataType.Int, trans[i].AccountID);
                    row[1] = new EData(DataType.Float, trans[i].ItemID);
                    row[2] = new EData(DataType.Float, trans[i].Amount);
                    row[3] = new EData(DataType.Float, trans[i].NewDebit);
                    row[4] = new EData(DataType.Float, trans[i].NewCredit);
                    ld[i] = new EData(DataType.ListData, row);
                }
                return new EData(DataType.ListData, ld);
            }
            if (su == RefVar)
                return new EData(DataType.Text, m_doc.PaperRef);
            return new FSError("Invalid symbol for DocumentListFunction " + symbol).ToEData();
        }

        public bool Defines(string symbol)
        {
            string su = symbol.ToUpper();
            if (su == DateVar
                || su == TypeIDVar
                || su == TransVar
            || su == RefVar)
                return true;
            return false;
        }

        #endregion

        #region IFormulaFunction Members
        ISymbolProvider m_provider = null;
        public ISymbolProvider ISymbolProvider
        {
            set { m_provider = value; }
        }

        public string[] Dependencies
        {
            get { return new string[] { }; }
        }

        #endregion

        #region IVarParamCountFunction Members
        int m_nPar = 3;
        public bool SetParCount(int n)
        {
            //0:Date From  1:Date To 2:Expression 3:Date Var 4:Type ID var, 5: Trans Var 6:Ref var
            m_nPar = n;
            return n > 1;
        }

        public IVarParamCountFunction Clone()
        {
            DocumentListFunction ret = new DocumentListFunction(m_accounting);
            ret.m_nPar = m_nPar;
            return ret;
        }

        #endregion

        #region IFunction Members

        public string Name
        {
            get { return "Documents List"; }
        }

        public string Symbol
        {
            get { return "DocList"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public int ParCount
        {
            get { return m_nPar; }
        }

        public EData Evaluate(EData[] Pars)
        {
            DateTime fromDate = Convert.ToDateTime(Pars[0].Value);
            DateTime toDate = Convert.ToDateTime(Pars[1].Value);
            Symbolic exp = null;
            if (Pars.Length > 2)
            {
                exp = new Symbolic();
                exp.SetSymbolProvider(new FormulaFunctionSymbolProvider(m_provider, this));
                exp.Expression = Pars[2].Value as string;
            }
            string filter = "";
            if (Pars.Length > 3)
                filter = Pars[3].Value as string;
            if (Pars.Length > 4)
                DateVar = (Pars[4].Value as string).ToUpper();
            if (Pars.Length > 5)
                TypeIDVar = (Pars[5].Value as string).ToUpper();
            if (Pars.Length > 6)
                TransVar = (Pars[6].Value as string).ToUpper();
            if (Pars.Length > 7)
                RefVar = (Pars[7].Value as string).ToUpper();
            INTAPS.RDBMS.SQLObjectReader<AccountDocument> reader = null;
            INTAPS.RDBMS.SQLHelper helper = m_accounting.GetReaderHelper();
            try
            {

                string cr = "tranTicks>=" + fromDate.Ticks + " and tranTicks<" + toDate.Ticks + "";
                if (!string.IsNullOrEmpty(filter))
                    cr += " and " + filter;
                int count = (int)helper.ExecuteScalar("Select count(*) from " + m_accounting.DBName + ".dbo.Document where " + cr);
                reader = helper.GetObjectReader<AccountDocument>(cr, "tranTicks,PaperRef");


                List<EData> _ret = new List<EData>();
                while (reader.Read())
                {

                    Console.WriteLine(count + "                 ");
                    //Console.CursorTop--;
                    count--;
                    m_doc = reader.Current;
                    if (exp != null)
                    {
                        EData data = exp.Evaluate();
                        if (!(data.Value is bool))
                            continue;
                        if (!((bool)data.Value))
                            continue;
                    }
                    ListData row = new ListData(3);
                    row[0] = new EData(DataType.DateTime, m_doc.DocumentDate);
                    row[1] = new EData(DataType.Text, m_doc.PaperRef);
                    row[2] = new EData(DataType.Int, m_doc.DocumentTypeID);
                    row[3] = new EData(DataType.Int, m_doc.AccountDocumentID);
                    _ret.Add(new EData(DataType.ListData, row));
                }
                return new EData(DataType.ListData, new ListData(_ret.ToArray()));
            }
            finally
            {
                if (reader != null && !reader.IsClosed)
                    reader.Close();
                m_accounting.ReleaseHelper(helper);
            }
        }
        #endregion
    }
}


