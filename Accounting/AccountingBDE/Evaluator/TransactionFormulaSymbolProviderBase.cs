using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
namespace INTAPS.Accounting.BDE
{
    public class TransactionFormulaSymbolProviderBase : ISymbolProvider
    {
        protected AccountingBDE m_bde;
        protected Dictionary<string, IFunction> m_Functions;
        protected void AddFunction(IFunction func)
        {
            m_Functions.Add(func.Symbol.ToUpper(), func);
        }
        public TransactionFormulaSymbolProviderBase(AccountingBDE bde)
        {
            m_bde = bde;
            m_Functions = new Dictionary<string, IFunction>();
            AddFunction(new GetBalance(m_bde));
            AddFunction(new GetFlow(m_bde));
        }
        #region ISymbolProvider Members

        public FunctionDocumentation[] GetAvialableFunctions()
        {
            FunctionDocumentation[] ret = new FunctionDocumentation[m_Functions.Count];
            int i = 0;
            foreach (string key in m_Functions.Keys)
            {
                ret[i++] = new FunctionDocumentation(m_Functions[key]);
            }
            return ret;
        }

        public EData GetData(URLIden iden)
        {
            return EData.Empty;
        }

        public EData GetData(string symbol)
        {
            return EData.Empty;
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return new FunctionDocumentation(f);
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            return GetDocumentation(GetFunction(Symbol));
        }

        public IFunction GetFunction(URLIden iden)
        {
            return null;
        }

        public IFunction GetFunction(string symbol)
        {
            return m_Functions[symbol.ToUpper()];
        }

        public bool SymbolDefined(string Name)
        {
            return m_Functions.ContainsKey(Name.ToUpper());
        }

        #endregion
    }
}
