using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace INTAPS.Accounting
{
    public interface IDocumentServerHandler
    {
        bool CheckPostPermission(int docID,AccountDocument _doc, UserSessionData userSession);
        int Post(int AID,AccountDocument _doc);
        string GetHTML(AccountDocument _doc);
        void DeleteDocument(int AID,int docID);
        void ReverseDocument(int AID,int docID);
    }
}
