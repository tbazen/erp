using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting.BDE
{
    public interface IReportProcessor
    {
        void PrepareEvaluator(EHTML data, params object[] parameters);
        object[] GetDefaultPars();
    }
}
