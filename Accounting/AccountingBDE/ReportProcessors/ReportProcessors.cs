using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public class ReportProcessorBase : IReportProcessor
    {
        public Dictionary<string, EData> vars;
        public List<IFunction> funcs;
        public ReportProcessorBase()
        {
            vars = new Dictionary<string, EData>();
            funcs = new List<IFunction>();
        }
        public virtual object[] GetDefaultPars()
        {
            return new object[0];
        }
        #region IReportProcessor Members
        public static EData CreateConnection(string conName)
        {
            string conStr = System.Configuration.ConfigurationManager.ConnectionStrings[conName+"RO"].ConnectionString;
            if (conStr == null)
                throw new ServerUserMessage("Connection not found " + conName);
            DBConSql dbconSql = new DBConSql(conStr);
            return new EData(DataType.DBCon, dbconSql);
        }
        public virtual void PrepareEvaluator(EHTML data, params object[] parameters)
        {
            foreach(KeyValuePair<string,EData> kv in vars)
            {
                data.AddVariable(kv.Key, kv.Value);
            }
            foreach (IFunction f in funcs)
            {
                data.AddFunction(f);
            }
        }
        #endregion
    }
    public class DateFilterProcessor : ReportProcessorBase
    {
        public DateFilterProcessor()
        {
        }
        public override void PrepareEvaluator(EHTML data, params object[] parameters)
        {
            DateTime d1 = (DateTime)parameters[0];
            DateTime d2 = (DateTime)parameters[1];
            data.AddVariable("date1", new EData(DataType.DateTime, d1));
            data.AddVariable("date2", new EData(DataType.DateTime, d2));
            base.PrepareEvaluator(data, parameters);
        }
        public override object[] GetDefaultPars()
        {
            return new object[] { DateTime.Now, DateTime.Now };
        }
    }
    public class ProgramaticProcessor : ReportProcessorBase
    {
        public override void PrepareEvaluator(EHTML data, params object[] parameters)
        {
            for (int i = 0; i < parameters.Length / 2; i++)
            {
                string name = parameters[2 * i] as string;
                if (name == null)
                    throw new ServerUserMessage("Parameter " + (2 * i) + " should be variable name.");
                if (!(parameters[2*i+1]is EData))
                    throw new ServerUserMessage("Parameter " + (2 * i+1) + " should be of type EData.");
                EData val=(EData)parameters[2*i+1];
                data.AddVariable(name, val);
            }
            base.PrepareEvaluator(data, parameters);
        }
    }
}
