﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;

namespace INTAPS.Accounting.BDE
{
    public class TransactionSummerizer
    {
        AccountingBDE accounting;
        public Dictionary<ulong, TransactionOfBatch> summary = new Dictionary<ulong, TransactionOfBatch>();
        List<Account> summaryRoots = new List<Account>();
        public TransactionSummerizer(AccountingBDE accounting, string[] summaryRootCodes)
        {
            this.accounting = accounting;
            foreach (string r in summaryRootCodes)
            {
                Account a = accounting.GetAccount<Account>(r);
                summaryRoots.Add(a);
            }
        }
        public void addTransaction(int AID, TransactionOfBatch tran)
        {
            addTransaction(AID, new TransactionOfBatch[] { tran });
        }
        public void addTransaction(int AID, params AccountTransaction[] trans)
        {
            TransactionOfBatch[] t = new TransactionOfBatch[trans.Length];
            for (int i = 0; i < t.Length; i++)
                t[i] = new TransactionOfBatch(trans[i]);
            addTransaction(AID, t);
        }
        public void addTransaction(int AID, IEnumerable<TransactionOfBatch> trans)
        {
            foreach (TransactionOfBatch t in trans)
            {
                CostCenterAccount csa = accounting.GetCostCenterAccount(t.AccountID);
                Account ac = accounting.GetAccount<Account>(csa.accountID);
                Account sumAccount = null;
                bool isDetailAccount = false;
                foreach (Account ra in summaryRoots)
                {
                    if (ra != null)
                    {
                        string sumPrefix = ra.Code + "-";
                        if (ac.Code.IndexOf(sumPrefix) == 0)
                        {
                            isDetailAccount = true;
                            sumAccount = accounting.GetAccount<Account>(ac.Code.Substring(sumPrefix.Length));
                            if (sumAccount == null)
                            {
                                ac = accounting.GetAccount<Account>(ac.PID);
                                if (ac != null)
                                {
                                    if (ac.Code.IndexOf(sumPrefix) == 0)
                                    {
                                        sumAccount = accounting.GetAccount<Account>(ac.Code.Substring(sumPrefix.Length));
                                    }
                                    else
                                        sumAccount = null;
                                }
                            }
                            break;
                        }
                    }
                }
                if (sumAccount == null)
                    if (isDetailAccount)
                        throw new ServerUserMessage("Summary account not found for {0}", ac.Code);
                    else
                        continue;
                int costCenterAccountID = accounting.GetOrCreateCostCenterAccount(AID, csa.costCenterID, sumAccount.id).id;
                ulong accountItem=AccountBalance.Combine(costCenterAccountID,t.ItemID);
                if (summary.ContainsKey(accountItem))
                {
                    TransactionOfBatch sumTran = summary[accountItem];
                    sumTran.Amount += t.Amount;
                    sumTran.Note = "";
                }
                else
                {
                    TransactionOfBatch tclone = t.clone();
                    tclone.AccountID = costCenterAccountID;
                    summary.Add(accountItem, tclone);
                }
            }
        }

        public TransactionOfBatch[] getSummary()
        {
            TransactionOfBatch[] ret = new TransactionOfBatch[summary.Count];
            summary.Values.CopyTo(ret,0);
            return ret;
        }
    }
}
