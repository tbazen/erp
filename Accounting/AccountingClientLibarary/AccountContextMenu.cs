using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
namespace INTAPS.Accounting.Client
{
    [Flags]
    public enum AccountContextMenuStyle
    {
        CreateRoot = 1,
        AddChild = 2,
        Edit = 4,
        ActivateDeactive = 8,
        Delete = 16,
        Hirarchy = 32,
        Ledger = 64
    }
    public class AccountCostCenterContextMenu<AccountType>: ContextMenuStrip
        where AccountType:AccountBase,new()
    {
        protected System.Windows.Forms.ToolStripMenuItem miCreateRoot;
        protected System.Windows.Forms.ToolStripMenuItem miAddChild;
        protected System.Windows.Forms.ToolStripMenuItem miEditAccount;
        protected System.Windows.Forms.ToolStripMenuItem miActivateAccount;
        protected System.Windows.Forms.ToolStripMenuItem miDeactivateAccount;
        protected System.Windows.Forms.ToolStripMenuItem miDeleteAcount;
        protected System.Windows.Forms.ToolStripMenuItem miGroupUnder;
        protected System.Windows.Forms.ToolStripMenuItem miClearChildAccounts;
        protected Account m_account;
        protected CostCenter m_costCenter;
        protected CostCenterAccount m_costCenterAccount;
        protected AccountType TypedAccount
        {
            get
            {
                if (typeof(AccountType) == typeof(Account))
                    return m_account as AccountType;
                return m_costCenter as AccountType;
            }
        }
        public AccountCostCenterContextMenu()
        {
            this.miCreateRoot = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddChild = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.miActivateAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeactivateAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteAcount = new System.Windows.Forms.ToolStripMenuItem();
            this.miGroupUnder = new System.Windows.Forms.ToolStripMenuItem();
            this.miClearChildAccounts = new System.Windows.Forms.ToolStripMenuItem();
            this.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCreateRoot,
            this.miAddChild,
            this.miEditAccount,
            this.miActivateAccount,
            this.miDeactivateAccount,
            this.miDeleteAcount,
            this.miGroupUnder,
            this.miClearChildAccounts
            });


            this.miCreateRoot.Text = "Create Root Account";
            this.miCreateRoot.Click += new System.EventHandler(this.miCreateRoot_Click);
            
            this.miAddChild.Text = "Add Child Account";
            this.miAddChild.Click += new System.EventHandler(this.miAddChild_Click);

            this.miEditAccount.Text = "Edit Account Information";
            this.miEditAccount.Click += new System.EventHandler(this.miEditAccount_Click);
            
            this.miActivateAccount.Text = "Activate Account";
            this.miActivateAccount.Click += new System.EventHandler(this.miActivateAccount_Click);
            
            this.miDeactivateAccount.Text = "Deactivate Account";
            this.miDeactivateAccount.Click += new System.EventHandler(this.miDeactivateAccount_Click);
            
            this.miDeleteAcount.Text = "Delete Account";
            this.miDeleteAcount.Click += new System.EventHandler(this.miDeleteAcount_Click);
            
            this.miGroupUnder.Text = "Group under";
            this.miGroupUnder.Click += new System.EventHandler(this.miGroupUnder_Click);

            this.miClearChildAccounts.Text = "Delete Child Accounts";
            this.miClearChildAccounts.Click += new EventHandler(miClearChildAccounts_Click);
        }

        void miClearChildAccounts_Click(object sender, EventArgs e)
        {
            DeleteChildAccountsFromDB();
        }

        public void SetAccount(Account account)
        {
            m_account = account;
        }
        public void SetCostCenter(CostCenter costCenter)
        {
            m_costCenter= costCenter;
        }

        public void SetCostCenterAccount(CostCenterAccount costCenterAccount)
        {
            m_costCenterAccount = costCenterAccount;
        }

        protected override void OnOpening(System.ComponentModel.CancelEventArgs e)
        {
            bool Selected = TypedAccount != null;
            miAddChild.Enabled = Selected;
            miEditAccount.Enabled = Selected;
            miActivateAccount.Enabled = Selected && TypedAccount.Status != AccountStatus.Activated;
            miDeactivateAccount.Enabled = Selected && TypedAccount.Status == AccountStatus.Activated;
            miGroupUnder.Enabled = Selected;
            miDeleteAcount.Enabled = Selected;
        }
        
        private void miActivateAccount_Click(object sender, EventArgs e)
        {
            DateTime date;
            if (GetDateBox.Show("Activate account", "Actiation date", out date) == DialogResult.OK)
            {
                try
                {
                    INTAPS.Accounting.Client.AccountingClient.ActivateAcount<AccountType>(TypedAccount.ObjectIDVal, date);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't activate account", ex);
                }
            }
        }
        private void miDeactivateAccount_Click(object sender, EventArgs e)
        {
            DateTime date;
            if (GetDateBox.Show("Deactivate account", "Deactiation date", out date) == DialogResult.OK)
            {
                try
                {
                    INTAPS.Accounting.Client.AccountingClient.DeactivateAccount<AccountType>(TypedAccount.ObjectIDVal, date);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't deactivate account", ex);
                }
            }
        }
        private void miDeleteAcount_Click(object sender, EventArgs e)
        {
            try
            {
                if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the account?"))
                {
                    DeleteAccountFromDB();
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete account.", ex);
            }

        }

        protected virtual void DeleteAccountFromDB()
        {
            INTAPS.Accounting.Client.AccountingClient.DeleteAccount<AccountType>(TypedAccount.ObjectIDVal);
        }
        protected virtual void DeleteChildAccountsFromDB()
        {
            new DeleteAccountProgressForm<AccountType>(TypedAccount.id).Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
        private void miAddChild_Click(object sender, EventArgs e)
        {
            AccountEditor<AccountType> ac = new AccountEditor<AccountType>(TypedAccount.ObjectIDVal);
            ac.ShowDialog();
        }
        private void miGroupUnder_Click(object sender, EventArgs e)
        {
            AccountPicker<AccountType> ch = AccountPicker<AccountType>.Create();
            if (ch.ShowDialog() == DialogResult.OK)
            {
                //try
                //{
                int selectedAccountID = ch.selectedAccount.id;
                LongProcessProgressForm l = new LongProcessProgressForm();
                l.showLongProcess("Rebuilding accounting structure"
                    , new INTAPS.UI.HTML.ProcessParameterless(delegate()
                {

                    INTAPS.Accounting.Client.AccountingClient.LinkAccount<AccountType>(selectedAccountID, TypedAccount.ObjectIDVal);
                })
            , new LongProcessFinishHandler(delegate(object ret, Exception ex)
                {
                    if (ex != null)
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Grouping operation failed.", ex);
                    TypedAccount.PID = selectedAccountID;
                }
            ));
                l.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);

                //}
                //catch (Exception ex)
                //{
                //    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Grouping operation failed.", ex);
                //}
            }
        }
        private void miUnlink_Click(object sender, EventArgs e)
        {
            try
            {
                if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to remove the account from the group?\n The account will not be deleted."))
                {
                    INTAPS.Accounting.Client.AccountingClient.LinkAccount<AccountType>(-1, TypedAccount.ObjectIDVal);
                    TypedAccount.PID = -1;
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't remove from group.", ex);
            }
        }
        private void miCreateRoot_Click(object sender, EventArgs e)
        {
            AccountEditor<AccountType> ac = new AccountEditor<AccountType>(-1);
            ac.ShowDialog();
        }
        private void miEditAccount_Click(object sender, EventArgs e)
        {
            AccountEditor<AccountType> ac = new AccountEditor<AccountType>((AccountType)TypedAccount.Clone());
            ac.ShowDialog();
        }

        public virtual void SetStyle(AccountContextMenuStyle style)
        {
            miDeactivateAccount.Visible =
            miActivateAccount.Visible = (style & AccountContextMenuStyle.ActivateDeactive) == AccountContextMenuStyle.ActivateDeactive;
            miAddChild.Visible = (style & AccountContextMenuStyle.AddChild) == AccountContextMenuStyle.AddChild;
            miCreateRoot.Visible = (style & AccountContextMenuStyle.CreateRoot) == AccountContextMenuStyle.CreateRoot;
            miDeleteAcount.Visible = (style & AccountContextMenuStyle.Delete) == AccountContextMenuStyle.Delete;
            miEditAccount.Visible = (style & AccountContextMenuStyle.Edit) == AccountContextMenuStyle.Edit;
            miGroupUnder.Visible = (style & AccountContextMenuStyle.Hirarchy) == AccountContextMenuStyle.Hirarchy;
            
        }
    }
    public class AccountContextMenu : AccountCostCenterContextMenu<Account>
    {
        private System.Windows.Forms.ToolStripMenuItem miAddToCostCenter;
        IHTMLLedgerFormater _ledgerFormater = null;
        
        public AccountContextMenu()
        {

            this.miAddToCostCenter = new System.Windows.Forms.ToolStripMenuItem();

            this.miAddToCostCenter.Text = "Add to costcenter";
            this.miAddToCostCenter.Click += new EventHandler(miAddToCostCenter_Click);
            this.Items.Add(miAddToCostCenter);
        }

        void miAddToCostCenter_Click(object sender, EventArgs e)
        {
            try
            {
                AccountingClient.CreateCostCenterAccount(m_costCenter.id, m_account.id);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to add account to cost center", ex);
            }
        }
        
        
        public override void SetStyle(AccountContextMenuStyle style)
        {
            base.SetStyle(style);
        }
        private void miShowLedger_Click(object sender, EventArgs e)
        {
            if (_ledgerFormater == null)
                throw new Exception("BUG: Please set ledger formater for the context menu");
            if (m_costCenterAccount == null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid account");
                return;
            }
            
            LedgerViewerForm lv = new LedgerViewerForm(m_costCenterAccount, _ledgerFormater);
            lv.Show();
        }
        public IHTMLLedgerFormater LedgerFormater
        {
            get { return _ledgerFormater; }
            set { _ledgerFormater = value; }
        }
        protected override void OnOpening(System.ComponentModel.CancelEventArgs e)
        {
            bool Selected = m_account != null;
            bool costCenterAcountSelected=m_costCenterAccount!=null;
            base.miAddChild.Enabled = Selected;
            base.miEditAccount.Enabled = Selected;
            base.miActivateAccount.Enabled = Selected && m_account.Status != AccountStatus.Activated;
            base.miDeactivateAccount.Enabled = Selected && m_account.Status == AccountStatus.Activated;
            base.miGroupUnder.Enabled = Selected;
            if (Selected || costCenterAcountSelected)
            {
                if (costCenterAcountSelected)
                    base.miDeleteAcount.Text = "Remove Account from Cost Center";
                else
                    base.miDeleteAcount.Text = "Delete Account";
                base.miDeleteAcount.Enabled = Selected;
            }
            bool uncreatedCostCenterAccount = m_costCenterAccount == null && m_account != null && m_costCenter != null;
            this.miAddToCostCenter.Enabled = uncreatedCostCenterAccount;
            if (uncreatedCostCenterAccount)
            {
                this.miAddToCostCenter.Text = "Add account to " + m_costCenter.NameCode;
            }
        }
        protected override void DeleteAccountFromDB()
        {
            if (m_costCenterAccount == null || m_costCenterAccount.costCenterID == CostCenter.ROOT_COST_CENTER)
                base.DeleteAccountFromDB();
            else
                AccountingClient.DeleteCostCenterAccount( m_costCenterAccount.id);
        }
    }
    public class CostCenterContextMenu : AccountCostCenterContextMenu<CostCenter>
    {
        public CostCenterContextMenu()
        {
        }
        protected override void OnOpening(System.ComponentModel.CancelEventArgs e)
        {
            base.OnOpening(e);
            miCreateRoot.Text = "Create Root Accounting Center"; ;
            miAddChild.Text="Add Child Accounting Center";
            miEditAccount.Text="Edit Accounting Center Information";
            miActivateAccount.Text = "Activate Accounting Center";
            miDeactivateAccount.Text="Deactivate Accounting Center";
            miDeleteAcount.Text="Delete";
            miGroupUnder.Text="Change Parent";
        }
        protected override void OnOpened(EventArgs e)
        {
            base.OnOpened(e);
            bool Selected = m_costCenter != null;
        }
        
    }
}
