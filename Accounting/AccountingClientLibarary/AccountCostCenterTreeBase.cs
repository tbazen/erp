using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace INTAPS.Accounting.Client
{
    
    public class AccountCostCenterTreeBase<AccountType>:TreeView
        where AccountType:AccountBase,new()
    {
        const int MAX_CHILDS = 50;
        public event EventHandler ViewStructureChanged;
        public event EventHandler SelectedAccountChanged;
        List<int> LoadedNodes;
        Dictionary<int, TreeNode> _allNodes=new Dictionary<int,TreeNode>();
        Font _controlAccountFont;
        AccountExplorerType _explorerType = AccountExplorerType.ALL;
        public AccountCostCenterTreeBase()
        {
            LoadedNodes = new List<int>();
            base.FullRowSelect = true;
            base.HideSelection = false;
            AccountClientEvents<AccountType>.AccountCreated += new AccountClientEvents<AccountType>.AccountCreatedHandler(AccountingClient_AccountCreated);
            AccountClientEvents<AccountType>.AccountUpdated += new AccountClientEvents<AccountType>.AccountUpdatedHandler(AccountingClient_AccountUpdated);
            AccountClientEvents<AccountType>.AccountDeletd += new AccountClientEvents<AccountType>.AccountDeletedHandler(AccountingClient_AccountDeletd);

            _controlAccountFont = new Font("Arial Rounded MT", 8, FontStyle.Bold);
        }
        protected override void Dispose(bool disposing)
        {
            AccountClientEvents<AccountType>.AccountCreated -= new AccountClientEvents<AccountType>.AccountCreatedHandler(AccountingClient_AccountCreated);
            AccountClientEvents<AccountType>.AccountUpdated -= new AccountClientEvents<AccountType>.AccountUpdatedHandler(AccountingClient_AccountUpdated);
            AccountClientEvents<AccountType>.AccountDeletd -= new AccountClientEvents<AccountType>.AccountDeletedHandler(AccountingClient_AccountDeletd);

            base.Dispose(disposing);
        }

        public AccountExplorerType ExplorerType
        {
            get
            {
                return _explorerType;
            }
            set
            {
                _explorerType = value;
            }
        }
        void AccountingClient_AccountCreated(AccountType ac)
        {
            AddAccount(ac);
        }

        void AccountingClient_AccountDeletd(int accountID)
        {
            DeleteAccount(accountID);
        }

        void AccountingClient_AccountUpdated(AccountType ac)
        {
            UpdateAccount(ac);
        }
        void AddAccountNode(TreeNodeCollection parent, AccountType ac)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = -1;
            parent.Add(n);
            SetNode(n,ac);
            _allNodes.Add(ac.id, n);
            if(ac.childCount>0)
            {
                TreeNode ph = new TreeNode();
                ph.Tag = ac.id;
                n.Nodes.Add(ph);
            }
        }
        protected virtual void SetNode(TreeNode n, AccountType ac)
        {
            n.Text = ac.Code+" - "+ ac.Name;
            n.ToolTipText = string.IsNullOrEmpty(ac.Description) ? "" : ac.Description;
            n.Tag = ac;
            if (ac.childCount > 0)
            {
                n.NodeFont = this._controlAccountFont;
                n.SelectedImageIndex = 1;
                n.ImageIndex = 1;
                //n.ImageKey = "x";
            }
            else
            {
                n.SelectedImageIndex = 0;
                n.ImageIndex = 0;
            }
        }

        void LoadChilds(TreeNodeCollection nodes, int PID)
        {
            if (LoadedNodes.Contains(PID))
                return;
            LoadedNodes.Add(PID);
            int N;
            AccountType[] acs;
            acs=GetChilds(PID, out N);
            foreach (AccountType a in acs)
            {
                if(typeof(AccountType) == typeof(Account))
                {
                    switch (_explorerType)
                    {
                        case AccountExplorerType.Main:
                            if (a.Code.Equals("ITEM",StringComparison.CurrentCultureIgnoreCase)
                                || a.Code.Equals("CUST", StringComparison.CurrentCultureIgnoreCase)
                                || a.Code.Equals("BUDGET", StringComparison.CurrentCultureIgnoreCase)
                                )
                                continue;
                            break;
                        case AccountExplorerType.ITEM:
                            if (!a.Code.StartsWith("ITEM", StringComparison.CurrentCultureIgnoreCase))
                                continue;
                            break;
                        case AccountExplorerType.CUST:
                            if (!a.Code.StartsWith("CUST", StringComparison.CurrentCultureIgnoreCase))
                                continue;
                            break;
                        case AccountExplorerType.BUDGET:
                            if (!a.Code.StartsWith("BUDGET", StringComparison.CurrentCultureIgnoreCase))
                                continue;
                            break;

                        default:
                            break;
                    }
                }
                AddAccountNode(nodes, a);
            }
            if (acs.Length < N)
            {
                TreeNode n = new TreeNode(N - acs.Length + " accounts not shown!");
                nodes.Add(n);
            }
        }

        protected virtual AccountType[] GetChilds(int PID, out int N)
        {
            return INTAPS.Accounting.Client.AccountingClient.GetChildAcccount<AccountType>(PID, 0, MAX_CHILDS, out N);
        }
        protected virtual AccountType GetAccount(int acountID)
        {
            return INTAPS.Accounting.Client.AccountingClient.GetAccount<AccountType>(acountID);
        }

        void LoadChildOfChilds(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                if (n.Tag is AccountType)
                    LoadChilds(n.Nodes, ((AccountType)n.Tag).ObjectIDVal);
            }
        }
        List<int> _expanded = new List<int>();
        protected override void OnAfterExpand(TreeViewEventArgs e)
        {
            AccountType ac = e.Node.Tag as AccountType;
            if (ac != null)
            {
                if (!_expanded.Contains(ac.id))
                    _expanded.Add(ac.id);
            }
            if (ViewStructureChanged != null)
                ViewStructureChanged(this, null);
            base.OnAfterExpand(e);
        }
        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            base.OnAfterSelect(e);
            if (SelectedAccountChanged != null)
                SelectedAccountChanged(this, null);
        }
        protected override void OnAfterCollapse(TreeViewEventArgs e)
        {
            AccountType ac = e.Node.Tag as AccountType;
            if (ac != null)
            {
                if (_expanded.Contains(ac.id))
                    _expanded.Remove(ac.id);
            }
            base.OnAfterCollapse(e);
            if (ViewStructureChanged != null)
                ViewStructureChanged(this, null);
        }
        public void LoadData(int Root)
        {
            _allNodes.Clear();
            LoadedNodes.Clear();
            this.Nodes.Clear();
            TreeNodeCollection nodes;
            if(Root!=-1)
            {
                AddAccountNode(this.Nodes,this.GetAccount(Root));
                nodes=this.Nodes[0].Nodes;
            }
            else
                nodes=this.Nodes;
            LoadChilds(nodes,Root);
            //LoadChildOfChilds(nodes);
            foreach (int id in _expanded)
            {
                if(_allNodes.ContainsKey(id))
                _allNodes[id].Expand();
            }
        }

        public new void ExpandAll()
        {
            base.ExpandAll();
        }
        protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
        {
            if (e.Node.Nodes.Count == 1 && e.Node.Nodes[0].Tag is int)
            {
                int PID=(int)e.Node.Nodes[0].Tag;
                e.Node.Nodes.Clear();
                LoadChilds(e.Node.Nodes, PID);
            }
            //LoadChildOfChilds(e.Node.Nodes);
        }
        void SearchTreeRec(TreeNodeCollection nodes, int ID, List<TreeNode> ret)
        {
            foreach (TreeNode n in nodes)
            {
                if (n.Tag is AccountType)
                {
                    if (((AccountType)n.Tag).ObjectIDVal == ID)
                        ret.Add(n);
                    SearchTreeRec(n.Nodes, ID, ret);
                }
            }
        }
        List<TreeNode> SearchTree(TreeNodeCollection nodes, int ID)
        {
            List<TreeNode> ret = new List<TreeNode>();
            SearchTreeRec(nodes, ID, ret);
            return ret;
        }
        public void AddAccount(AccountType ac)
        {
            if (ac.PID==-1)
            {
                AddAccountNode(this.Nodes, ac);
            }
            else 
            {
                foreach(TreeNode n in SearchTree(this.Nodes, ac.PID))
                    AddAccountNode(n.Nodes, ac);
            }
        }
        public void UpdateAccount(AccountType ac)
        {
            foreach (TreeNode n in SearchTree(this.Nodes, ac.ObjectIDVal))
            {
                AccountType old = n.Tag as AccountType;
                if (old.PID != ac.PID)
                {
                    DeleteAccount(old.id);
                    AddAccount(ac);
                }
                else
                {
                    if (old.childCount>0 && ac.childCount==0)
                        n.Nodes.Clear();
                    SetNode(n, ac);
                }
            }
        }
        public Account SelectedParentAccount
        {
            get
            {
                if (this.SelectedNode == null)
                    return null;
                if (this.SelectedNode.Parent == null)
                    return null;
                return this.SelectedNode.Parent.Tag as Account;
            }
        }
        [System.ComponentModel.DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [System.ComponentModel.Browsable(false)]
        public AccountType SelectedAccount
        {
            get
            {
                if(this.SelectedNode==null)
                    return null;
                return this.SelectedNode.Tag as AccountType;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public void DeleteAccount(int ID)
        {
            foreach (TreeNode n in SearchTree(this.Nodes, ID))
            {
                n.Remove();
                this.LoadedNodes.Remove(ID);
                this._allNodes.Remove(ID);
            }
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            TreeViewHitTestInfo hi = HitTest(e.X, e.Y);
            this.SelectedNode = hi.Node;
            base.OnMouseDown(e);
        }
        Dictionary<int,object> getVisisbleAccountStructure(TreeNodeCollection nodes)
        {
            Dictionary<int, object> ret = new Dictionary<int,object>();
            foreach (TreeNode n in nodes)
            {
                if (n.Tag is AccountType)
                {
                    if (n.Nodes.Count > 0 && n.IsExpanded)
                        ret.Add((n.Tag as AccountType).ObjectIDVal, getVisisbleAccountStructure(n.Nodes));
                    else
                        ret.Add((n.Tag as AccountType).ObjectIDVal, n.Tag as AccountType);
                }
            }
            return ret;
        }
        internal void setVisisbleAccountStructure(TreeNodeCollection nodes, Dictionary<int, object> list)
        {
            foreach (TreeNode n in nodes)
            {
                if (!(n.Tag is AccountType))
                    continue;
                int ID = (n.Tag as AccountType).ObjectIDVal;
                if (list.ContainsKey(ID))
                {
                    object val = list[ID];
                    if (val is Dictionary<int, object>)
                    {
                        n.Expand();
                        setVisisbleAccountStructure(n.Nodes, (Dictionary<int, object>)val);
                    }
                }
            }
        }
        internal void setVisisbleAccountStructure(Dictionary<int,object> list)
        {
            setVisisbleAccountStructure(this.Nodes, list);
        }
        internal Dictionary<int, object> getVisisbleAccountStructure()
        {
            return getVisisbleAccountStructure(this.Nodes);

        }

        internal void SetByID(int accountID)
        {
            if (!SetByID(this.Nodes, accountID))
                this.SelectedNode = null;
        }
        bool SetByID(TreeNodeCollection nodes, int accountID)
        {
            foreach (TreeNode n in nodes)
            {
                if (!(n.Tag is AccountType))
                    continue;

                if((n.Tag as AccountType).id==accountID)
                {
                    this.SelectedNode = n;
                    return true;
                }
                if (n.Nodes.Count > 0)
                {
                    bool ret = SetByID(n.Nodes, accountID);
                    if (ret)
                        return true;
                }
            }
            return false;
        }
    }


}
