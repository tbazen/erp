using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class AccountEditor<AccountType>: Form
        where AccountType:AccountBase,new()
    {
        public AccountType FormData;
        INTAPS.UI.EnterKeyNavigation m_Nav;
        bool New = false;
        public AccountEditor()
        {
            InitializeComponent();
            m_Nav = new INTAPS.UI.EnterKeyNavigation();
            m_Nav.AddNavigableItem(dtpCreationDate);
            m_Nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtName));
            m_Nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtCode));
            m_Nav.AddNavigableItem(new INTAPS.UI.ComboNavigator(comboAccountType));
            m_Nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtMoreInfo));
            m_Nav.AddNavigableItem(new INTAPS.UI.ButtonNavigator(btnOk));
            m_Nav.StartEdit();
            //Account special
            optCredit.Visible = optDebit.Visible = typeof(AccountType) == typeof(Account);

            foreach(StandardAccountType type in Enum.GetValues(typeof(StandardAccountType)))
            {
                var name = Account.getAccountTypeName(type);
                if (name == null)
                    name = type.ToString();
                comboAccountType.Items.Add(name);
            }
        }
        void UpdateStatusDateControls()
        {
        }
        public AccountEditor(AccountType ac)
            : this()
        {
            FormData = ac;
            dtpCreationDate.Value = ac.CreationDate;
            txtName.Text = ac.Name;
            txtCode.Text = ac.Code;
            if (typeof(AccountType) == typeof(Account))
            {
                optCredit.Checked = (ac as Account).CreditAccount;
                optDebit.Checked = !(ac as Account).CreditAccount;
                comboAccountType.SelectedIndex = ArrayExtension.indexOf<StandardAccountType>(
                    (StandardAccountType[])Enum.GetValues(typeof(StandardAccountType)), (ac as Account).accountType);
            }
            txtMoreInfo.Text = ac.Description == null ? "" : ac.Description;
            New = false;
        }
        public AccountEditor(int PID):this()
        {
            FormData = new AccountType();
            FormData.PID = PID;
            if (PID != -1)
            {
                AccountType ac = INTAPS.Accounting.Client.AccountingClient.GetAccount<AccountType>(PID);
                txtCode.Text = ac.Code;
                if (typeof(AccountType) == typeof(Account))
                {
                    optCredit.Enabled = optDebit.Enabled = false;
                    optCredit.Checked = (ac as Account).CreditAccount;
                    optDebit.Checked = !(ac as Account).CreditAccount;
                    comboAccountType.SelectedIndex = ArrayExtension.indexOf<StandardAccountType>(
                    (StandardAccountType[])Enum.GetValues(typeof(StandardAccountType)), (ac as Account).accountType);
                }
            }
            else
                txtCode.Text = "";
            
            New = true;

            
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateStatusDateControls();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            FormData.CreationDate = dtpCreationDate.Value;
            FormData.Name = txtName.Text.Trim();
            FormData.Code = txtCode.Text.Trim();
            if (FormData.Name == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please provide name of account.");
                txtName.SelectAll();
                txtName.Focus();
                return;
            }
            if (FormData.Code == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please provide code of account.");
                txtCode.SelectAll();
                txtCode.Focus();
                return;
            }
            if(comboAccountType.SelectedIndex==-1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select account type.");
                txtCode.SelectAll();
                txtCode.Focus();
                return;
            }
            FormData.Status = AccountStatus.Activated;
            FormData.ActivateDate = DateTime.Now;
            FormData.DeactivateDate = DateTime.Now;
            FormData.Description = txtMoreInfo.Text;
            if (typeof(AccountType) == typeof(Account))
            {
                (FormData as Account).CreditAccount = optCredit.Checked;
                (FormData as Account).accountType = ((StandardAccountType[])Enum.GetValues(typeof(StandardAccountType)))[comboAccountType.SelectedIndex];
            }
            try
            {
                if (New)
                {
                    FormData.ObjectIDVal = INTAPS.Accounting.Client.AccountingClient.CreateAccount<AccountType>(FormData);
                }
                else
                    INTAPS.Accounting.Client.AccountingClient.UpdateAccount(FormData);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save account information.", ex);
            }
        }

        private void dtpDeactivatedDate_Load(object sender, EventArgs e)
        {

        }

        private void AccountEditor_Load(object sender, EventArgs e)
        {

        }
    }
}