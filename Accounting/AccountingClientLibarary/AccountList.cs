using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class AccountList<AccountType>:ListView
        where AccountType:AccountBase,new()
    {
        public AccountList()
        {
            this.Columns.Add("Code");
            this.Columns.Add("Name");
            this.Columns.Add("Balance");
            this.View = View.Details;
            this.FullRowSelect = true;
        }
        public void ShowList(AccountType[] ac, int itemID)
        {
            this.Items.Clear();
            foreach (AccountType a in ac)
            {
                ListViewItem li = new ListViewItem(a.Code);
                li.Tag=a;
                li.SubItems.Add(a.Name);                
                li.SubItems.Add("");
                this.Items.Add(li);
            }
        }
        public AccountType SelectedAccount
        {
            get
            {
                if (this.SelectedItems.Count == 0)
                    return null;
                return this.SelectedItems[0].Tag as AccountType;
            }
        }
    }

}
