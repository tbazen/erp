using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI;

namespace INTAPS.Accounting.Client
{
    class AccountListGrid: EDataGridView
    {
        public AccountListGrid()
        {
            
        }
        public void SetUpGrid()
        {
            this.Columns.Clear();
            int index;
            this.Columns.Add("accountCode", "Account Code");
            index = this.Columns.Add("accountName", "Account Name");
            this.Columns[index].ReadOnly = true;
            this.Columns[index].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            
            this.Columns.Add("costCenterCode", "Cost Center Code");
            index = this.Columns.Add("costCenterName", "Cost Center Name");
            this.Columns[index].ReadOnly = true;
            this.Columns[index].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

        }
        protected override void OnCellEndEdit(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 0:case 2:
                    string val = this[e.ColumnIndex, e.RowIndex].Value as string;
                    if (string.IsNullOrEmpty(val))
                    {
                        row.Tag = null;
                        for (int i = 1; i < row.Cells.Count; i++)
                            row.Cells[i].Value = null;
                    }
                    else
                    {

                        try
                        {
                            if (e.ColumnIndex == 0)
                            {
                                Account ac = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(val);
                                SetRowAccount(row, ac);
                            }
                            else
                            {
                                CostCenter cs = INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>(val);
                                SetRowCostCenter(row, cs);
                            }
                        }
                        catch (Exception ex)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(ex.Message);
                        }

                    }
                    break;
            }
        }
        protected override void OnEnterComit(DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = this.Rows[e.RowIndex];
            switch (e.ColumnIndex)
            {
                case 0:
                    this.CurrentCell = this[2, row.Index];
                    break;
                case 2:
                    this.CurrentCell = this[0, row.Index + 1];
                    break;

            }
        }
        public int AccountCount
        {
            get
            {
                int ret=0;
                foreach (DataGridViewRow row in this.Rows)
                    if (row.Tag is CostCenterAccount)
                        ret++;
                return ret;
            }
        }
        public CostCenterAccount this[int index]
        {
            get
            {
                foreach (DataGridViewRow row in this.Rows)
                {
                    if (row.Tag is Account)
                    {
                        if (index == 0)
                            return (CostCenterAccount)row.Tag;
                        index--;
                    }
                }
                throw new IndexOutOfRangeException();
            }
        }
        private static void SetRowAccount(DataGridViewRow row, Account ac)
        {
            row.Cells[1].Value = ac.Name;
            row.Cells[1].Tag = ac;
            CostCenter cs = row.Cells[3].Tag as CostCenter;
            if (cs == null)
                row.Tag = null;
            else
            {
                CostCenterAccount csa = Accounting.Client.AccountingClient.GetCostCenterAccount(cs.id,ac.id);
                row.Tag = csa;
            }
        }
        private static void SetRowCostCenter(DataGridViewRow row, CostCenter cs)
        {
            row.Cells[3].Value = cs.Name;
            row.Cells[3].Tag = cs;
            Account ac = row.Cells[1].Tag as Account;
            if (ac == null)
                row.Tag = null;
            else
            {
                CostCenterAccount csa = Accounting.Client.AccountingClient.GetCostCenterAccount(cs.id,ac.id);
                row.Tag = csa;
            }
        }
    }
}
