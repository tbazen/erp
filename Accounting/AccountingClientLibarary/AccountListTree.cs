﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace INTAPS.Accounting.Client
{
    delegate void LoadSearchResultDelegate(Account[] accounts);
    public partial class AccountListTree : UserControl
    {
        public event EventHandler AccountExplorerTypeChanged;
        const int MAX_ITEMS = 50;
        bool _treeMode = false;
        Thread _searchThread;
        public event EventHandler SelectedAccountChanged;
        public event EventHandler AccountDoubleClicked;
        public AccountListTree()
        {
            InitializeComponent();
            cmbAccountExplorerType.SelectedIndex = 0;
            accountTree.DoubleClick += new EventHandler(accountTree_DoubleClick);
            listView.DoubleClick += new EventHandler(listView_DoubleClick);
            setMode(true);
            imageList.Images.Add(Resources.account);
            imageList.Images.Add(Resources.contAc2);
            imageList.Images.Add(Resources.cs);
            imageList.Images.Add(Resources.contCs);
            listView.LargeImageList = listView.SmallImageList =imageList;

        }
        public AccountExplorerType explorerType
        {
            get
            {
                switch (cmbAccountExplorerType.SelectedIndex)
                {
                    case 1:
                        return AccountExplorerType.ITEM;
                    case 2:
                        return AccountExplorerType.CUST;
                    case 3:
                        return AccountExplorerType.BUDGET;
                    default:
                        return AccountExplorerType.Main;
                }
            }
        }
        void listView_DoubleClick(object sender, EventArgs e)
        {
            if (AccountDoubleClicked != null)
                AccountDoubleClicked(this, null);
        }

        void accountTree_DoubleClick(object sender, EventArgs e)
        {
            if (AccountDoubleClicked != null)
                AccountDoubleClicked(this, null);
        }
        public AccountTree getTree()
        {
            return accountTree;
        }

        public List<int> getRootAccounts()
        {
            List<int> rootAccounts = new List<int>();
            foreach (TreeNode node in accountTree.Nodes)
            {
                rootAccounts.Add(((Account)node.Tag).id);
            }
            return rootAccounts;
        }
        void setMode(bool treeMode)
        {
            _treeMode = treeMode;
            accountTree.Visible = treeMode;
            listView.Visible = !treeMode;
            buttonClear.Visible = !treeMode;
        }
        void checkAndSearch()
        {
            if (textQuery.Modified)
            {
                if (_searchThread == null || !_searchThread.IsAlive

                    )
                {
                    textQuery.Modified = false;
                    _searchThread = new Thread(new ParameterizedThreadStart(searchMethod));
                    _searchThread.Start(textQuery.Text.Trim());
                }
            }
        }
        Account[] accounts;
        CostCenterAccount[] css = null;
        void searchMethod(object par)
        {
            string q = (string)par;
            int N;
            accounts = AccountingClient.SearchAccount<Account>(q, 0, MAX_ITEMS, out N);
            if (accountTree.CostCenterAccountMode)
            {
                css = new CostCenterAccount[accounts.Length];
                for (int i = 0; i < accounts.Length; i++)
                {
                    css[i] = AccountingClient.GetCostCenterAccount(accountTree.CostCenterID, accounts[i].id);
                }
            }
            this.BeginInvoke(new INTAPS.UI.HTML.ProcessParameterless(loadSearchResult));
        }
        void loadSearchResult()
        {
            listView.Items.Clear();
            if (accounts != null)
            {
                int i=0;
                foreach (Account a in accounts)
                {
                    ListViewItem li = new ListViewItem(a.Code);
                    li.SubItems.Add(a.Name);
                    listView.Items.Add(li);
                    li.Tag = a;
                    if (a != null && css[i]!=null)
                    {
                        li.ForeColor = System.Drawing.Color.DarkBlue;
                        li.StateImageIndex = li.ImageIndex = a.childCount > 0 ? 3 : 2;
                    }
                    else
                    {
                        if (a != null)
                        {
                            li.StateImageIndex = li.ImageIndex = a.childCount > 0 ? 1 : 0;
                        }
                        li.ForeColor = System.Drawing.Color.Gray;
                    }
                    i++;
                }
            }
            checkAndSearch();
        }
        private void textQuery_TextChanged(object sender, EventArgs e)
        {
            if (textQuery.Text.Trim() == "")
            {
                if (!_treeMode)
                    setMode(true);
                textQuery.Modified = false;
            }
            else
            {
                if (_treeMode)
                    setMode(false);
                checkAndSearch();
            }
        }

        public int CostCenterID
        {
            get
            {
                return accountTree.CostCenterID;
            }
            set
            {
                accountTree.CostCenterID = value;
            }
        }

        public Account SelectedAccount 
        {
            get
            {
                if (_treeMode)
                    return accountTree.SelectedAccount;
                if (listView.SelectedItems.Count == 0)
                    return null;
                return listView.SelectedItems[0].Tag as Account;
            }
            set
            {
                if (_treeMode)
                {
                    accountTree.SelectedAccount = value;
                }
                else
                {
                    listView.SelectedItems.Clear();
                    if (value == null)
                    {
                        return;
                    }
                    ListViewItem item = null;
                    foreach (ListViewItem li in listView.Items)
                    {
                        if (((Account)li.Tag).id == value.id)
                        {
                            item = li;
                            break;
                        }
                    }
                    if (item != null)
                        item.Selected = true;
                }

            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textQuery.Text = "";
        }

        private void accountTree_SelectedAccountChanged(object sender, EventArgs e)
        {
            if (_treeMode)
            {
                if (SelectedAccountChanged != null)
                    SelectedAccountChanged(this, null);
            }
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_treeMode)
            {
                if (SelectedAccountChanged != null)
                    SelectedAccountChanged(this, null);
            }
        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (listView.Items.Count == 1)
                    listView.Items[0].Selected=true;
            }
        }

        private void cmbAccountExplorerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AccountExplorerTypeChanged != null)
                AccountExplorerTypeChanged(sender, e);
        }
        
        public class ObjectValue
        {
            public int id;
            public object value;
        }
        ObjectValue[] dictionaryToObjectArray(Dictionary<int, object> dic)
        {
            ObjectValue[] ret = new ObjectValue[dic.Count];
            int i = 0;
            foreach (KeyValuePair<int, object> kv in dic)
            {
                if (kv.Value is Dictionary<int, object>)
                {
                    ret[i] = new ObjectValue() { id = kv.Key, value = dictionaryToObjectArray((Dictionary<int, object>)kv.Value) };
                }
                else
                    ret[i] = new ObjectValue() { id = kv.Key, value = null };
                i++;
            }
            return ret;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.Filter = "*.accounts|*.accounts";
            if(d.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm)==DialogResult.OK)
            {
                Dictionary<int, object> str = accountTree.getVisisbleAccountStructure();
                ObjectValue[] ov = dictionaryToObjectArray(str);
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ObjectValue[]));
                System.IO.FileStream sw= System.IO.File.Create(d.FileName);
                s.Serialize(sw, ov);
                sw.Close();
            }
        }
        void expandAccountTree(TreeNodeCollection nodes, ObjectValue[] ov)
        {
            foreach (ObjectValue o in ov)
            {
                if (o.value is ObjectValue[])
                {
                    foreach (TreeNode n in nodes)
                    {
                        if (((AccountBase)n.Tag).id == o.id)
                        {
                            n.Expand();
                            expandAccountTree(n.Nodes, (ObjectValue[])o.value);
                            break;
                        }
                    }
                }
            }
        }
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "*.accounts|*.accounts";
            if (d.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
            {
                try
                {
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ObjectValue[]));
                    System.IO.FileStream sw = System.IO.File.OpenRead(d.FileName);
                    ObjectValue[] ov = s.Deserialize(sw) as ObjectValue[];
                    sw.Close();
                    expandAccountTree(accountTree.Nodes, ov);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }
    }
}
