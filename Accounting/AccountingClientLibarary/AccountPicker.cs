﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class AccountPicker<AccountType> : Form
        where AccountType:AccountBase,new()
    {
        public AccountType selectedAccount = null;
        AccountCostCenterTreeBase<AccountType> _tree;
        bool _onlyLeafAccount=false;
        public bool OnlyLeafAccount
        {
            get
            {
                return _onlyLeafAccount;
            }
            set
            {
                if (_onlyLeafAccount == value)
                    return;
                _onlyLeafAccount = value;
            }
        }
        public AccountPicker(AccountCostCenterTreeBase<AccountType> tree)
        {
            InitializeComponent();
            this.Text = AccountBase.GetAccountTypeDescription <AccountType>(true) +" picker";
            _tree = tree;
            _tree.Dock = DockStyle.Fill;
            this.Controls.Add(_tree);
            _tree.BringToFront();
            _tree.LoadData(-1);
            _tree.DoubleClick += new EventHandler(_tree_DoubleClick);
        }
        public void filterByCostCenter(int costCenterID)
        {
            if (typeof(AccountType) == typeof(Account))
            {
                (_tree as AccountTree).CostCenterID = costCenterID;
            }
        }
        void _tree_DoubleClick(object sender, EventArgs e)
        {
            if (_tree.SelectedAccount != null)
            {
                if (_onlyLeafAccount)
                {
                    if (_tree.SelectedAccount.isControl)
                        return;
                }
                selectedAccount = _tree.SelectedAccount;
                this.DialogResult = DialogResult.OK;
            }
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        internal static AccountPicker<AccountType> Create()
        {
            if (typeof(AccountType) == typeof(Account))
            {
                AccountTree acTree = new AccountTree();
                acTree.CostCenterAccountMode = false;
                return new AccountPicker<Account>(acTree) as AccountPicker<AccountType>;
            }
            return new AccountPicker<CostCenter>(new CostCenterTree()) as AccountPicker<AccountType>;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            AccountType selected= _tree.SelectedAccount;
            if(selected==null)
                return;
            AccountEditor<AccountType> acnt = new AccountEditor<AccountType>(selected.ObjectIDVal);
            if (acnt.ShowDialog(this) == DialogResult.OK)
            {
                selectedAccount = acnt.FormData;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
