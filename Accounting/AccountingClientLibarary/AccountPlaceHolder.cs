using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class AccountPlaceHolderGen<AccountType> : TextBox
        where AccountType:AccountBase,new()
    {
        public event EventHandler AccountChanged;
        AccountPicker<AccountType> m_picker;
        AccountType m_picked;
        Button m_btnPick;
        bool m_changed;
        bool m_allowAdd = false;
        bool _onlyLeafAccount = false;
        public bool OnlyLeafAccount
        {
            get
            {
                return _onlyLeafAccount;
            }
            set
            {
                if (_onlyLeafAccount == value)
                    return;
                _onlyLeafAccount = value;
            }
        }
        public bool AllowAdd
        {
            get
            { 
                return m_allowAdd; 
            }
            set
            {
                m_allowAdd = value;
                if (value && m_btnNew == null)
                {
                    m_btnNew = new Button();
                    m_btnNew.Text = "+";
                    m_btnNew.Dock = DockStyle.Right;
                    m_btnNew.Width = 20;
                    m_btnNew.Click += new EventHandler(m_btnNew_Click);
                }
            }
        }

        void m_btnNew_Click(object sender, EventArgs e)
        {
            //AccountEditor f=new AccountEditor(
        }
        Button m_btnNew;
        public AccountPlaceHolderGen()
        {
            m_btnPick = new Button();
            m_btnPick.Text = "...";
            m_btnPick.BackColor = System.Drawing.Color.FromArgb(0x17, 0x7B, 0xAD);
            m_btnPick.Dock = DockStyle.Right;
            m_btnPick.Width = 20;
            m_btnPick.Click += new EventHandler(m_btnPick_Click);
            m_changed = false;
            this.Controls.Add(m_btnPick);
        }
        public bool Changed
        {
            get
            {
                return m_changed;
            }
        }
        virtual protected AccountPicker<AccountType> CreatePicker()
        {
            AccountPicker<AccountType> ret;
            if (typeof(AccountType) == typeof(Account))
            {
                AccountTree acTree = new AccountTree();
                acTree.CostCenterAccountMode = false;
                ret = new AccountPicker<Account>(acTree) as AccountPicker<AccountType>;
            }
            else
                ret = new AccountPicker<CostCenter>(new CostCenterTree()) as AccountPicker<AccountType>;
            ret.OnlyLeafAccount = _onlyLeafAccount;
            return ret;
        }
        void m_btnPick_Click(object sender, EventArgs e)
        {
            if (m_picker == null)
                m_picker = CreatePicker();
            if (m_picker.ShowDialog() == DialogResult.OK)
            {
                this.account = m_picker.selectedAccount;
                m_changed = true;
                OnChanged();
            }
        }
        [Browsable(false)]
        public AccountType account
        {
            get
            {
                return m_picked;
            }
            set
            {
                m_picked = value;
                if (m_picked == null)
                    this.Text = "";
                else
                    this.Text = m_picked.CodeName;
                base.Modified = false;
            }
        }
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            this.SelectAll();
        }

        protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                autoComplete();    
            }
        }
        protected override void OnLeave(EventArgs e)
        {
            if (base.Modified)
            {
                autoComplete();
            }
            base.OnLeave(e);
        }
        private void autoComplete()
        {
            string txt = this.Text.Trim();
            if (txt == "")
                this.account = null;
            else
            {
                try
                {
                    this.account = INTAPS.Accounting.Client.AccountingClient.GetAccount<AccountType>(txt);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(ex.Message);
                    this.account = m_picked;
                    this.SelectAll();
                }
            }
            m_changed = true;
            base.Modified = false;
            OnChanged();
        }
        public void OnChanged()
        {
            if (AccountChanged!= null)
                AccountChanged(this, null);
        }

        public void SetByID(int ID)
        {
            if (ID == -1)
                this.account = null;
            else
            {
                try
                {
                    this.account = INTAPS.Accounting.Client.AccountingClient.GetAccount<AccountType>(ID);
                }
                catch
                {
                    m_picked = null;
                    this.Text = "error";
                }
            }
            base.Modified = false;
        }

        public int GetAccountID()
        {
            if (m_picked == null)
                return -1;
            return m_picked.ObjectIDVal;
        }
    }
    public class AccountPlaceHolder : AccountPlaceHolderGen<Account>
    {
    }
    public class CostCenterPlaceHolder: AccountPlaceHolderGen<CostCenter>
    {
    }
    public class AccountDataGridViewCell : DataGridViewTextBoxCell
    {
        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
        {
            CostCenterAccountWithDescription ac = value as CostCenterAccountWithDescription;
            if (ac == null)
                return "";
            return ac.code;
        }
    }
}
