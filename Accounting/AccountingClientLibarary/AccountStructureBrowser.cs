﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace INTAPS.Accounting.Client
{
    public partial class AccountStructureBrowser : Form
    {

        public CostCenterAccountWithDescription PickedAccount
        {
            get 
            { 
                return browserControl.PickedAccount; 
            }
            set 
            {
                browserControl.PickedAccount = value;
            }
        }

        public AccountStructureBrowser()
            : this(false)
        {
        }
        
        public AccountStructureBrowser(bool picker)
        {
            InitializeComponent();
            if(!picker)
                panelPickerButtons.Hide();
        }

       

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (browserControl.PickedAccount != null)
                this.DialogResult = DialogResult.OK;
            else
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select an acount");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
