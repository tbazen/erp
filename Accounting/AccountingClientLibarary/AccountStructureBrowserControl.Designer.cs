﻿namespace INTAPS.Accounting.Client
{
    partial class AccountStructureBrowserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerAccounts = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAccounts)).BeginInit();
            this.splitContainerAccounts.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerAccounts
            // 
            this.splitContainerAccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerAccounts.Location = new System.Drawing.Point(0, 0);
            this.splitContainerAccounts.Name = "splitContainerAccounts";
            this.splitContainerAccounts.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerAccounts.Panel1
            // 
            this.splitContainerAccounts.Panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            // 
            // splitContainerAccounts.Panel2
            // 
            this.splitContainerAccounts.Panel2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.splitContainerAccounts.Size = new System.Drawing.Size(450, 435);
            this.splitContainerAccounts.SplitterDistance = 146;
            this.splitContainerAccounts.TabIndex = 1;
            // 
            // AccountStructureBrowserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerAccounts);
            this.Name = "AccountStructureBrowserControl";
            this.Size = new System.Drawing.Size(450, 435);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerAccounts)).EndInit();
            this.splitContainerAccounts.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerAccounts;
    }
}
