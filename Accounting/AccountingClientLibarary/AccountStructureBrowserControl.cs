﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace INTAPS.Accounting.Client
{
    public partial class AccountStructureBrowserControl : UserControl
    {
        public event EventHandler CostCenterViewStructureChanged;
        public event EventHandler AccountViewStructureChanged;
        public event EventHandler SelectedCostCenterChanged;
        public event EventHandler SelectedAccountChanged;

        AccountListTree acTree; 
        CostCenterTree csTree;
        AccountContextMenu ctxtAccount = new AccountContextMenu();
        CostCenterContextMenu ctxtCostCenter = new CostCenterContextMenu();
        CostCenterAccountWithDescription _pickedAccount = null;
        public ContextMenuStrip costCenterContextMenu
        {
            get
            {
                return ctxtCostCenter;
            }
        }
        public ContextMenuStrip accountContextMenu
        {
            get
            {
                return ctxtAccount;
            }
        }
        public CostCenterAccountWithDescription PickedAccount
        {
            get
            {
                return _pickedAccount;
            }
            set
            {
                _pickedAccount = value;
            }
        }


        public AccountStructureBrowserControl()
        {
            InitializeComponent();
            if (AccountingClient.IsConnected)
            {
                initializeAccountTree();
                initializeCostAccountTree();

                splitContainerAccounts.Panel1.Controls.Add(csTree);
                splitContainerAccounts.Panel2.Controls.Add(acTree);
                acTree.Dock = DockStyle.Fill;
                csTree.Dock = DockStyle.Fill;
                csTree.AfterSelect += new TreeViewEventHandler(csTree_AfterSelect);
                ctxtAccount.LedgerFormater = new SimpleLedgerFormater();
                acTree.getTree().ViewStructureChanged += new EventHandler(acTree_ViewStructureChanged);
                csTree.ViewStructureChanged += new EventHandler(csTree_ViewStructureChanged);
                acTree.SelectedAccountChanged += new EventHandler(acTree_SelectedAccountChanged);
                csTree.SelectedAccountChanged += new EventHandler(csTree_SelectedAccountChanged);
            }
        }
        public List<int> getRootAccounts()
        {
            return acTree.getRootAccounts();
        }
        void csTree_SelectedAccountChanged(object sender, EventArgs e)
        {
            if (SelectedCostCenterChanged != null)
                SelectedCostCenterChanged(this, null);
        }

        void acTree_SelectedAccountChanged(object sender, EventArgs e)
        {
            if (SelectedAccountChanged != null)
                SelectedAccountChanged(this, null);
        }

        void csTree_ViewStructureChanged(object sender, EventArgs e)
        {
            if (CostCenterViewStructureChanged != null)
                CostCenterViewStructureChanged(this, null);
        }

        void acTree_ViewStructureChanged(object sender, EventArgs e)
        {
            if (AccountViewStructureChanged != null)
                AccountViewStructureChanged(this, null);
        }

        void csTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            acTree.CostCenterID = csTree.SelectedAccount == null ? CostCenter.ROOT_COST_CENTER : csTree.SelectedAccount.id;
        }
        private void initializeAccountTree()
        {
            acTree = new AccountListTree();
            acTree.AccountExplorerTypeChanged += acTree_AccountExplorerTypeChanged;
            AccountTree accountTree = acTree.getTree();
            accountTree.ExplorerType = acTree.explorerType;
            accountTree.LoadData(-1);
            acTree.ContextMenuStrip = ctxtAccount;
            acTree.SelectedAccountChanged += new EventHandler(acTree_BeforeSelect);
        }

        void acTree_AccountExplorerTypeChanged(object sender, EventArgs e)
        {
           if(acTree!=null)
           {
               AccountTree accountTree = acTree.getTree();
               accountTree.ExplorerType = acTree.explorerType;
               accountTree.LoadData(-1);
           }
        }
        private void initializeCostAccountTree()
        {
            csTree = new CostCenterTree();
            csTree.LoadData(-1);
            csTree.ContextMenuStrip = ctxtCostCenter;
            csTree.BeforeSelect += new TreeViewCancelEventHandler(csTree_BeforeSelect);
        }
        void acTree_BeforeSelect(object sender, EventArgs e)
        {
            Account ac = acTree.SelectedAccount;
            if (ac != null)
                _pickedAccount = AccountingClient.GetCostCenterAccountWithDescription(acTree.CostCenterID, ac.id);
            else
                _pickedAccount = null;
            ctxtAccount.SetAccount(ac);
            ctxtAccount.SetCostCenterAccount(_pickedAccount);
            ctxtCostCenter.SetAccount(ac);
            ctxtCostCenter.SetCostCenterAccount(_pickedAccount);
        }
        void csTree_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            CostCenter cs = e.Node.Tag as CostCenter;
            acTree.CostCenterID = cs == null ? -1 : cs.id;
            if (cs != null)
                _pickedAccount = AccountingClient.GetCostCenterAccountWithDescription(acTree.CostCenterID, cs.id);
            else
                _pickedAccount = null;

            ctxtAccount.SetCostCenter(cs);
            ctxtAccount.SetCostCenterAccount(_pickedAccount);
            ctxtCostCenter.SetCostCenter(cs);
            ctxtCostCenter.SetCostCenterAccount(_pickedAccount);
        }
        public Dictionary<int, object> getVisisbleAccountStructure()
        {
            return acTree.getTree().getVisisbleAccountStructure();
        }
        public Dictionary<int, object> getVisisbleCostCenterStructure()
        {
            return csTree.getVisisbleAccountStructure();
        }

        public void setVisisbleAccountStructure(Dictionary<int, object> list)
        {
            acTree.getTree().setVisisbleAccountStructure(list);
        }

        public void setVisisbleCostCenterStructure(Dictionary<int, object> list)
        {
            csTree.setVisisbleAccountStructure(list);
        }
        
        [System.ComponentModel.DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [System.ComponentModel.Browsable(false)]
        public Account SelectedAccount { get { return acTree.SelectedAccount; } set { acTree.SelectedAccount = value; } }
        
        [System.ComponentModel.DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [System.ComponentModel.Browsable(false)]
        public CostCenter SelectedCostCenter { get { return csTree.SelectedAccount; } set { csTree.SelectedAccount = value; } }

        public void SetAccountByID(int accountID)
        {
            acTree.getTree().SetByID(accountID);
        }

        public void SetCostCenterByID(int costCenterID)
        {
            csTree.SetByID(costCenterID);
        }

        public void reloadTrees()
        {
            Dictionary<int, object> acv = acTree.getTree().getVisisbleAccountStructure();
            acTree.getTree().LoadData(-1);
            acTree.getTree().setVisisbleAccountStructure(acv);
            acv = csTree.getVisisbleAccountStructure();
            csTree.LoadData(-1);
            csTree.setVisisbleAccountStructure(acv);
        }
    }
}
