﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Drawing;

namespace INTAPS.Accounting.Client
{
    public class AccountTree:AccountCostCenterTreeBase<Account>
    {
        int _costCenterID = CostCenter.ROOT_COST_CENTER;
        CostCenter _costCenter=null;
        List<int> _costCenterAccounts=null;
        bool _costCenterAccountMode = true;
        
        ImageList _imageList;
        public bool CostCenterAccountMode
        {
            get
            {
                return _costCenterAccountMode;
            }
            set
            {
                _costCenterAccountMode = value;
            }
        }
        [System.ComponentModel.DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [System.ComponentModel.Browsable(false)]

        public int CostCenterID
        {
            get
            {
                return _costCenterID;
            }
            set
            {
                _costCenterID = value;
                if (_costCenterAccountMode)
                {
                    _costCenter = AccountingClient.GetAccount<CostCenter>(value);
                    _costCenterAccounts = AccountingClient.GetJoinedAccountIDs<CostCenter>(_costCenterID);
                    setCostCenterID(this.Nodes);
                }
            }
        }
        public AccountTree()
        {
            if(AccountingClient.IsConnected)
                 _costCenter= AccountingClient.GetAccount<CostCenter>(CostCenter.ROOT_COST_CENTER);
            AccountingClient.CostCenterAccountCreated += new CostCenterAccountCreatedHandler(AccountingClient_CostCenterAccountCreated);
            AccountingClient.CostCenterAccountDeleted += new CostCenterAccountDeletedHandler(AccountingClient_CostCenterAccountDeleted);
            AccountingClient.CostCenterAccountUpdate += new CostCenterAccountUpdateHandler(AccountingClient_CostCenterAccountUpdate);

            
            _imageList = new ImageList();
            _imageList.Images.Add(Resources.account);
            _imageList.Images.Add(Resources.contAc2);
            _imageList.Images.Add(Resources.cs);
            _imageList.Images.Add(Resources.contCs);
            this.ImageList = _imageList;
        }
        protected override void Dispose(bool disposing)
        {
            AccountingClient.CostCenterAccountCreated -= new CostCenterAccountCreatedHandler(AccountingClient_CostCenterAccountCreated);
            AccountingClient.CostCenterAccountDeleted -= new CostCenterAccountDeletedHandler(AccountingClient_CostCenterAccountDeleted);
            AccountingClient.CostCenterAccountUpdate -= new CostCenterAccountUpdateHandler(AccountingClient_CostCenterAccountUpdate);
            base.Dispose(disposing);
        }
        void AccountingClient_CostCenterAccountUpdate(CostCenterAccount csa)
        {
            this.CostCenterID = _costCenterID;
        }

        void AccountingClient_CostCenterAccountDeleted(int csaID)
        {
            this.CostCenterID = _costCenterID;
        }

        void AccountingClient_CostCenterAccountCreated(CostCenterAccount csa)
        {
            this.CostCenterID = _costCenterID;
        }

        void setCostCenterID(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                setNodeCostCenter(n, n.Tag as Account);
                if (n.IsExpanded)
                {
                    if (n.Nodes.Count > 0)
                        setCostCenterID(n.Nodes);
                }
            }
        }
        
        protected override void OnAfterExpand(TreeViewEventArgs e)
        {
            base.OnAfterExpand(e);
            if (!_costCenterAccountMode)
                return;
            foreach (TreeNode n in e.Node.Nodes)
            {
                setNodeCostCenter(n, n.Tag as Account);
            }
        }
        
        protected override void SetNode(System.Windows.Forms.TreeNode n, Account ac)
        {
            base.SetNode(n, ac);
            if(_costCenterAccountMode)
                setNodeCostCenter(n, ac);
        }
        
        private void setNodeCostCenter(System.Windows.Forms.TreeNode n, Account ac)
        {
            if (n == null)
                return;
            if (ac!=null && _costCenterAccounts!=null && _costCenterAccounts.Contains(ac.id))// AccountingClient.GetCostCenterAccount(_costCenterID, ac.id) != null)
            {
                n.ForeColor = System.Drawing.Color.DarkBlue;
                n.SelectedImageIndex = n.ImageIndex = ac.childCount > 0 ? 3 : 2;
            }
            else
            {
                if (ac != null)
                {
                    n.SelectedImageIndex = n.ImageIndex = ac.childCount > 0 ? 1 : 0;
                }
                n.ForeColor = System.Drawing.Color.Gray;
            }
        }        
    }
}
