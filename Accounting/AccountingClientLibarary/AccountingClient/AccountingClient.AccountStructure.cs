using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;

using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
using System.Linq;

namespace INTAPS.Accounting.Client
{
    public partial class AccountingClient
    {

        public static int CreateAccount<AccountType>(AccountType ac) where AccountType : AccountBase, new()
        {
            // int id = AccountingServer.CreateAccount<AccountType>(ApplicationClient.SessionID, ac);
            int id = RESTAPIClient.Call<int>(path + "CreateAccount", new { sessionID= ApplicationClient.SessionID, ac= new TypeObject(ac) });
            ac.id = id;
            AccountClientEvents<AccountType>.OnAccountCreated(ac);
            return ac.id;
        }
        public static void UpdateAccount<AccountType>(AccountType ac) where AccountType : AccountBase, new()
        {
            //AccountingServer.UpdateAccount<AccountType>(ApplicationClient.SessionID, ac);
            RESTAPIClient.Call<VoidRet>(path + "UpdateAccount", new { sessionID= ApplicationClient.SessionID, ac= new TypeObject(ac) });
            AccountClientEvents<AccountType>.OnAccountUpdated(ac);
        }
        public static void DeleteAccount<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            //AccountingServer.DeleteAccount<AccountType>(ApplicationClient.SessionID, accountID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteAccount", new { sessionID= ApplicationClient.SessionID, accountID= accountID, AccountType = typeof(AccountType).Name });
            AccountClientEvents<AccountType>.OnAccountDeletd(accountID);
        }
        public static void ActivateAcount<AccountType>(int AccountID, DateTime ActivateDate) where AccountType : AccountBase, new()
        {
            //AccountingServer.ActivateAcount<AccountType>(ApplicationClient.SessionID, AccountID, ActivateDate);
            RESTAPIClient.Call<VoidRet>(path + "ActivateAcount", new { sessionID =ApplicationClient.SessionID, AccountID =AccountID, ActivateDate= ActivateDate, AccountType = typeof(AccountType).Name });
        }
        public static void DeactivateAccount<AccountType>(int AccountID, DateTime DeactivateDate) where AccountType : AccountBase, new()
        {
            //AccountingServer.DeactivateAccount<AccountType>(ApplicationClient.SessionID, AccountID, DeactivateDate);
            RESTAPIClient.Call<VoidRet>(path + "DeactivateAccount", new { sessionID= ApplicationClient.SessionID, AccountID= AccountID, DeactivateDate= DeactivateDate, AccountType = typeof(AccountType).Name });
        }
        public static void ActivateCostCenterAccount(int csAccountID, DateTime ActivateDate)
        {
            //AccountingServer.ActivateCostCenterAccount(ApplicationClient.SessionID, csAccountID, ActivateDate);
            RESTAPIClient.Call<VoidRet>(path + "ActivateCostCenterAccount", new { sessionID=ApplicationClient.SessionID, csAccountID= csAccountID, ActivateDate= ActivateDate });
        }
        public static void DeactivateCostCenterAccount(int csAccountID, DateTime DeactivateDate)
        {
            //AccountingServer.DectivateCostCenterAccount(ApplicationClient.SessionID, csAccountID, DeactivateDate);
            RESTAPIClient.Call<VoidRet>(path + "DectivateCostCenterAccount", new { sessionID=ApplicationClient.SessionID, csAccountID= csAccountID, deactivationDate= DeactivateDate });
        }
        public static void LinkAccount<AccountType>(int Parent, int Child) where AccountType : AccountBase, new()
        {
            //AccountingServer.LinkAccount<AccountType>(ApplicationClient.SessionID, Parent, Child);
            RESTAPIClient.Call<VoidRet>(path + "LinkAccount", new { sessionID= ApplicationClient.SessionID, Parent= Parent, Child= Child, AccountType = typeof(AccountType).Name });
            AccountClientEvents<AccountType>.OnAccountUpdated(GetAccount<AccountType>(Child));
        }
        public static int CreateCostCenterAccount(int costCenterID, int accountID)
        {
            //int id = AccountingServer.CreateCostCenterAccount(ApplicationClient.SessionID, costCenterID, accountID);
            int id = RESTAPIClient.Call<int>(path + "CreateCostCenterAccount", new { sessionID= ApplicationClient.SessionID, costCenterID= costCenterID, accountID= accountID });
            //OnCostCenterAccountCreated(AccountingServer.GetCostCenterAccount(ApplicationClient.SessionID, id));
            OnCostCenterAccountCreated(RESTAPIClient.Call<CostCenterAccount> (path + "GetCostCenterAccount2", new { sessionID= ApplicationClient.SessionID, csaID= id }));
            return id;
        }
        public static void DeleteCostCenterAccount(int csaID)
        {
            //AccountingServer.DeleteCostCenterAccount(ApplicationClient.SessionID, csaID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteCostCenterAccount", new { sessionID= ApplicationClient.SessionID, csaID= csaID });
            OnCostCenterAccountDeleted(csaID);
        }

        //get
        public static TransactionItem GetTransactionItem(int itemID)
        {
            //return AccountingServer.GetTransactionItem(ApplicationClient.SessionID, itemID);
            return RESTAPIClient.Call<TransactionItem>(path + "GetTransactionItem", new { sessionID= ApplicationClient.SessionID, itemID= itemID });
        }
        public static bool IsControlOf<AccountType>(int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            //return AccountingServer.IsControlOf<AccountType>(ApplicationClient.SessionID, acountID, testAccount);
            return RESTAPIClient.Call<bool>(path + "IsControlOf", new { sessionID= ApplicationClient.SessionID, acountID= acountID, testAccount= testAccount });
        }
        public static bool IsDirectControlOf<AccountType>(int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            //return AccountingServer.IsDirectControlOf<AccountType>(ApplicationClient.SessionID, acountID, testAccount);
            return RESTAPIClient.Call<bool>(path + "IsDirectControlOf", new { sessionID= ApplicationClient.SessionID, acountID= acountID, testAccount= testAccount });
        }
        public static int GetAccountID<AccountType>(string AccountCode) where AccountType : AccountBase, new()
        {
            //return AccountingServer.GetAccountID<AccountType>(ApplicationClient.SessionID, AccountCode);
            return RESTAPIClient.Call<int>(path + "GetAccountID", new { sessionID= ApplicationClient.SessionID, AccountCode=AccountCode, AccountType = typeof(AccountType).Name });
        }
        public static AccountType[] GetLeafAccounts<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            //return AccountingServer.GetLeafAccounts<AccountType>(ApplicationClient.SessionID, accountID);
            return RESTAPIClient.Call<AccountType[]>(path + "GetLeafAccounts", new { sessionID=ApplicationClient.SessionID, accountID=accountID, AccountType=typeof(AccountType).Name });
        }
        public static AccountType GetAccount<AccountType>(string AccountCode) where AccountType : AccountBase, new()
        {
            //return AccountingServer.GetAccount<AccountType>(ApplicationClient.SessionID, AccountCode);
            return RESTAPIClient.Call<AccountType>(path + "GetAccount2", new { sessionID= ApplicationClient.SessionID, AccountCode = AccountCode, AccountType = typeof(AccountType).Name });
        }
        public static AccountType GetAccount<AccountType>(int AccountID) where AccountType : AccountBase, new()
        {
            if (AccountID < 1)
                return null;
            //return AccountingServer.GetAccount<AccountType>(ApplicationClient.SessionID, AccountID);
            return RESTAPIClient.Call<AccountType>(path + "GetAccount", new { sessionID=ApplicationClient.SessionID, AccountID = AccountID, AccountType= typeof(AccountType).Name });
        }
        public static AccountType[] GetChildAllAcccount<AccountType>(int ParentAccount) where AccountType : AccountBase, new()
        {
            //return AccountingServer.GetChildAllAcccount<AccountType>(ApplicationClient.SessionID, ParentAccount);
            return RESTAPIClient.Call<AccountType[]>(path + "GetChildAllAcccount", new { sessionID= ApplicationClient.SessionID, ParentAccount=ParentAccount,AccountType=typeof(AccountType).Name });
        }
        public class GetChildAcccountOut
        {
            public int NRecords;
            public TypeObject[] _ret;
        }
        public static AccountType[] GetChildAcccount<AccountType>(int ParentAccount, int index, int pageSize, out int NRecords) where AccountType : AccountBase, new()
        {
            //return AccountingServer.GetChildAcccount<AccountType>(ApplicationClient.SessionID, ParentAccount, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<GetChildAcccountOut>(path + "GetChildAcccount", new { sessionID= ApplicationClient.SessionID, ParentAccount= ParentAccount, index= index, pageSize = pageSize,AccountType=typeof(AccountType).Name });
            NRecords = _ret.NRecords;
            var res = _ret._ret.Select(x => x.GetConverted() as AccountType).ToArray();
            return  res;
        }
        public class SearchAccount2Out
        {
            public int NRecords;
            public TypeObject[] _ret;
        }
        public static AccountType[] SearchAccount<AccountType>(string query, int index, int pageSize, out int NRecords)
            where AccountType : AccountBase, new()
        {
            //return AccountingServer.SearchAccount<AccountType>(ApplicationClient.SessionID, query, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<SearchAccount2Out>(path + "SearchAccount2", new { sessionID = ApplicationClient.SessionID, query = query, index = index, pageSize= pageSize,AccountType=typeof(AccountType).Name });
            NRecords = _ret.NRecords;
            return _ret._ret.Select(x => x.GetConverted() as AccountType).ToArray();
        }
        public class SearchAccountOut
        {
            public int NRecords;
            public TypeObject[] _ret;
        }
        public static AccountType[] SearchAccount<AccountType>(string query, int category, int index, int pageSize, out int NRecords)
            where AccountType : AccountBase, new()
        {
            //return AccountingServer.SearchAccount<AccountType>(ApplicationClient.SessionID, query, category, index, pageSize, out NRecords);
            var _ret = RESTAPIClient.Call<SearchAccountOut>(path + "SearchAccount", new { sessionID= ApplicationClient.SessionID, query= query, category= category, index= index, pageSize= pageSize,AccountType=typeof(AccountType).Name });
            NRecords = _ret.NRecords;
            return _ret._ret.Select(x => x.GetConverted() as AccountType).ToArray();
        }
        public static CostCenterAccount GetDefaultCostCenterAccount(int accountID)
        {
            //return AccountingServer.GetDefaultCostCenterAccount(ApplicationClient.SessionID, accountID);
            return RESTAPIClient.Call<CostCenterAccount>(path + "GetDefaultCostCenterAccount", new { sessionID= ApplicationClient.SessionID, accountID= accountID });
        }
        public static CostCenterAccount GetCostCenterAccount(int csaID)
        {
            //return AccountingServer.GetCostCenterAccount(ApplicationClient.SessionID, csaID);
            return RESTAPIClient.Call<CostCenterAccount>(path + "GetCostCenterAccount2", new { sessionID=ApplicationClient.SessionID, csaID= csaID });
        }
        public static CostCenterAccount GetCostCenterAccount(int costCenterID, int accountID)
        {
            //return AccountingServer.GetCostCenterAccount(ApplicationClient.SessionID, costCenterID, accountID);
            return RESTAPIClient.Call<CostCenterAccount>(path + "GetCostCenterAccount", new { sessionID= ApplicationClient.SessionID, costCenterID= costCenterID, accountID=accountID });
        }
        public static CostCenterAccount GetCostCenterAccount(string code)
        {
            //return AccountingServer.GetCostCenterAccount(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<CostCenterAccount>(path + "GetCostCenterAccount3", new { sessionID= ApplicationClient.SessionID, code= code });
        }
        public static string GetCostCenterAccountCodeName(int csaID)
        {
            CostCenterAccount csa = GetCostCenterAccount(csaID);
            Account ac = GetAccount<Account>(csa.accountID);
            CostCenter cs = GetAccount<CostCenter>(csa.costCenterID);
            return GetCostCenterAccountCodeName(ac, cs);
        }

        internal static string GetCostCenterAccountCodeName(Account ac, CostCenter cs)
        {
            return cs.Code + ':' + ac.Code + " - " + cs.Name + ':' + ac.Name;
        }
        public static CostCenterAccountWithDescription GetCostCenterAccountWithDescription(int csaID)
        {
            //return AccountingServer.GetCostCenterAccountWithDescription(ApplicationClient.SessionID, csaID);
            return RESTAPIClient.Call<CostCenterAccountWithDescription>(path + "GetCostCenterAccountWithDescription", new { sessionID= ApplicationClient.SessionID, csaID= csaID });
        }
        public static CostCenterAccountWithDescription GetCostCenterAccountWithDescription(string csaCode)
        {
            //return AccountingServer.GetCostCenterAccountWithDescription(ApplicationClient.SessionID, csaCode);
            return RESTAPIClient.Call<CostCenterAccountWithDescription>(path + "GetCostCenterAccountWithDescription2", new { sessionID= ApplicationClient.SessionID, code= csaCode });
        }
        public class GetLedgerParOut
        {
            public int NRecord; public AccountBalance begBal; public AccountBalance total;
            public INTAPS.Accounting.AccountTransaction[] _ret;
        }
        static public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccountID, int resItemID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord, out AccountBalance begBal, out AccountBalance total)
        {
            //return AccountingServer.GetLedger(ApplicationClient.SessionID, resAccountID, resItemID, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord, out begBal, out total);
            var _ret = RESTAPIClient.Call<GetLedgerParOut>(path + "GetLedger", new { sessionID= ApplicationClient.SessionID, resAccountID= resAccountID, resItemID= resItemID, itemID= itemID, dateFrom= dateFrom, dateTo= dateTo, pageIndex = pageIndex, pageSize= pageSize,AccountType=typeof(AccountTransaction[]).Name });
            NRecord = _ret.NRecord;
            begBal = _ret.begBal;
            total = _ret.total;
            return (AccountTransaction[])_ret._ret;
        }
        static public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(int costCenterID, int accountID)
        {
            //return AccountingServer.GetCostCenterAccountWithDescription(ApplicationClient.SessionID, costCenterID, accountID);
            return RESTAPIClient.Call<CostCenterAccountWithDescription>(path + "GetCostCenterAccountWithDescription3", new { sessionID= ApplicationClient.SessionID, costCenterID= costCenterID, accountID= accountID });
        }

        static public void materializeDocument(int docID, DateTime materialzationDate)
        {
            //AccountingServer.materializeDocument(ApplicationClient.SessionID, docID, materialzationDate);
            RESTAPIClient.Call<VoidRet>(path + "materializeDocument", new { sessionID= ApplicationClient.SessionID, docID= docID, materialzationDate= materialzationDate });
        }

        static public int getMaterializePendingCount(DateTime time)
        {
            //return AccountingServer.getMaterializePendingCount(ApplicationClient.SessionID, time);
            return RESTAPIClient.Call<int>(path + "getMaterializePendingCount", new { sessionID= ApplicationClient.SessionID, time= time });
        }

        static public int[] getMaterializePendingDocumentIDs(DateTime time)
        {
            //return AccountingServer.getMaterializePendingDocumentIDs(ApplicationClient.SessionID, time);
            return RESTAPIClient.Call<int[]>(path + "getMaterializePendingDocumentIDs", new { sessionID= ApplicationClient.SessionID, time= time });
        }

        static public void RescheduleAccountDocument(int docID, DateTime time)
        {
            //AccountingServer.RescheduleAccountDocument(ApplicationClient.SessionID, docID, time);
            RESTAPIClient.Call<VoidRet>(path + "RescheduleAccountDocument", new { sessionID= ApplicationClient.SessionID, docID= docID, time= time });
        }


        public static bool IsConnected { get { return ApplicationClient.isConnected(); } }
        
        static public CostCenterAccount[] GetCostCenterAccountsOf<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            // return AccountingServer.GetCostCenterAccountsOf<AccountType>(ApplicationClient.SessionID, accountID);
            return RESTAPIClient.Call<CostCenterAccount[]>(path + "GetCostCenterAccountsOf", new { sessionID= ApplicationClient.SessionID, accountID= accountID, AccountType = typeof(AccountType).Name });
        }


        public static double GetAverageUnitPrice(double quantity,double price)
        {
            if (AccountBase.AmountEqual(quantity,0))
                return 0;
            return price / quantity;
        }
        public static double GetAverageUnitPrice(int costCenterID,int accountID, DateTime dateTime, out bool hasQuantity)
        {
            hasQuantity = false;
            CostCenterAccount csa = GetCostCenterAccount(costCenterID, accountID);
            if (csa == null)
                return 0;
            double amount=GetNetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, dateTime);
            double quantity = GetNetBalanceAsOf(csa.id, TransactionItem.MATERIAL_QUANTITY, dateTime);
            if (AccountBase.AmountEqual(quantity, 0))
                return 0;
            hasQuantity = true;
            return amount / quantity;
        }

        internal static List<int> GetJoinedAccountIDs<AccountType>(int costCenterID) where AccountType:AccountBase, new()
        {
            //return AccountingServer.GetJoinedAccountIDs<AccountType>(ApplicationClient.SessionID, costCenterID);
            return RESTAPIClient.Call<List<int>>(path + "GetJoinedAccountIDs", new { sessionID= ApplicationClient.SessionID, costCenterID= costCenterID
                , AccountType = typeof(AccountType).Name });
        }
    }

}
