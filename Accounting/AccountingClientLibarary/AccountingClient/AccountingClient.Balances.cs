﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
namespace INTAPS.Accounting.Client
{
    public partial class AccountingClient
    {
        public static double GetEndNetBalance(int AccountID)
        {
            //return AccountingServer.GetEndNetBalance(ApplicationClient.SessionID, AccountID, 0);
            return RESTAPIClient.Call<double>(path + "GetEndNetBalance", new { sessionID= ApplicationClient.SessionID, AccountID = AccountID, itemID=0 });
        }
        public static double GetEndNetBalance(int AccountID, int itemID)
        {
            //return AccountingServer.GetEndNetBalance(ApplicationClient.SessionID, AccountID, itemID);
            return RESTAPIClient.Call<double>(path + "GetEndNetBalance", new { SessionID= ApplicationClient.SessionID, AccountID=AccountID, itemID= itemID });
        }
        static public AccountBalance GetEndBalance(int accountID, int itemID)
        {
            //return AccountingServer.GetEndBalance(ApplicationClient.SessionID, accountID, itemID);
            return RESTAPIClient.Call<AccountBalance>(path + "GetEndBalance", new { sessionID= ApplicationClient.SessionID, accountID=accountID, itemID= itemID });
        }
        static public AccountBalance[] GetEndBalances(int accountID)
        {
            //return AccountingServer.GetEndBalances(ApplicationClient.SessionID, accountID);
            return RESTAPIClient.Call<AccountBalance[]>(path + "GetEndBalances", new { sessionID= ApplicationClient.SessionID, accountID= accountID });
        }
        public static AccountBalance[] GetEndItemBalances(int materialID)
        {
            //return AccountingServer.GetEndItemBalances(ApplicationClient.SessionID, materialID);
            return RESTAPIClient.Call<AccountBalance[]>(path + "GetEndItemBalances", new { sessionID= ApplicationClient.SessionID, itemID= materialID });
        }

        public static double GetNetBalanceAsOf(int AccountID, DateTime date)
        {
            //return AccountingServer.GetNetBalanceAsOf(ApplicationClient.SessionID, AccountID, TransactionItem.DEFAULT_CURRENCY, date);
            return RESTAPIClient.Call<double>(path + "GetNetBalanceAsOf", new { sessionID= ApplicationClient.SessionID, csAccountID= AccountID, itemID= TransactionItem.DEFAULT_CURRENCY, date= date });
        }
        public static double GetNetBalanceAsOf2(int costCenterID,int accountID, DateTime date)
        {
            //CostCenterAccount csa = AccountingServer.GetCostCenterAccount(ApplicationClient.SessionID, costCenterID, accountID);
            CostCenterAccount csa =RESTAPIClient.Call<CostCenterAccount>(path + "GetCostCenterAccount", new { sessionID= ApplicationClient.SessionID, costCenterID= costCenterID, accountID= accountID });
            if (csa == null)
                return 0;
            //return AccountingServer.GetNetBalanceAsOf(ApplicationClient.SessionID, csa.id, TransactionItem.DEFAULT_CURRENCY, date);
            return RESTAPIClient.Call<double>(path + "GetNetBalanceAsOf", new { sessionID= ApplicationClient.SessionID, csAccountID= csa.id, itemID= TransactionItem.DEFAULT_CURRENCY, date= date });
        }
        public static double GetNetCostCenterAccountBalanceAsOf(int costCenterID, int AccountID, DateTime date)
        {
            return GetNetCostCenterAccountBalanceAsOf(costCenterID, AccountID, TransactionItem.DEFAULT_CURRENCY,date);
        }
        public static double GetNetCostCenterAccountBalanceAsOf(int costCenterID, int AccountID, int itemID,DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(costCenterID,AccountID);
            if (csa == null)
                return 0;
            return GetNetBalanceAsOf(csa.id,itemID, date);
        }
        public static double GetNetBalanceAsOf(int AccountID, int itemID, DateTime date)
        {
            //return AccountingServer.GetNetBalanceAsOf(ApplicationClient.SessionID, AccountID, itemID, date);
            return RESTAPIClient.Call<double>(path + "GetNetBalanceAsOf", new { sessionID= ApplicationClient.SessionID, csAccountID= AccountID, itemID= itemID, date= date });
        }
        static public AccountBalance GetBalanceAsOf(int AccountID, int ItemID, DateTime date)
        {
            //return AccountingServer.GetBalanceAsOf(ApplicationClient.SessionID, AccountID, ItemID, date);
            return RESTAPIClient.Call<AccountBalance>(path + "GetBalanceAsOf", new { sessionID= ApplicationClient.SessionID, AccountID=AccountID, ItemID= ItemID, date= date });
        }
        static public AccountBalance GetBalanceAsOf(int AccountID,  DateTime date)
        {
            //return AccountingServer.GetBalanceAsOf(ApplicationClient.SessionID, AccountID, TransactionItem.DEFAULT_CURRENCY, date);
            return RESTAPIClient.Call<AccountBalance>(path + "GetBalancesAsOf2", new { sessionID= ApplicationClient.SessionID, AccountID= AccountID, ItemID= TransactionItem.DEFAULT_CURRENCY, date= date });
        }
        static public AccountBalance[] GetBalancesAsOf(int AccountID,  DateTime date)
        {
            //return AccountingServer.GetBalancesAsOf(ApplicationClient.SessionID, AccountID, date);
            return RESTAPIClient.Call<AccountBalance[]>(path + "GetBalancesAsOf", new { sessionID= ApplicationClient.SessionID, accountID=AccountID, date= date });
        }
        
    }
}
