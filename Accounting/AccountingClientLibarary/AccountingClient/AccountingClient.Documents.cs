﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
namespace INTAPS.Accounting.Client
{
    public partial class AccountingClient
    {

        static void LoadDocumentHandlers()
        {
            // _documentTypes = AccountingServer.GetAllDocumentTypes(ApplicationClient.SessionID);
            _documentTypes = RESTAPIClient.Call<DocumentType[]>(path + "GetAllDocumentTypes", new { sessionID= ApplicationClient.SessionID });
            //DocumentHandler[] dhs = AccountingServer.GetAllDocumentHandlerInfo(ApplicationClient.SessionID);
            DocumentHandler[] dhs =RESTAPIClient.Call<DocumentHandler[]>(path + "GetAllDocumentHandlerInfo", new { sessionID= ApplicationClient.SessionID });
           _documentHandlerInfo = new Dictionary<int, DocumentHandler>();
            foreach (DocumentHandler dh in dhs)
            {
                _documentHandlerInfo.Add(dh.documentTypeID, dh);
            }
        }

        public static IGenericDocumentClientHandler GetDocumentHandler(int documentTypeID)
        {
            if (!documentHandlerInfo.ContainsKey(documentTypeID))
                return null;
            DocumentHandler dh = GetDocumentHandlerInfo(documentTypeID);
            if (dh.clientClass == null)
                return null;
            Type t = Type.GetType(dh.clientClass);
            if (t == null)
                return null;
            System.Reflection.ConstructorInfo c = t.GetConstructor(new Type[0]);
            if (c == null)
                return null;
            IGenericDocumentClientHandler ret= (IGenericDocumentClientHandler)c.Invoke(new object[0]);
            ret.setAccountingClient(_defaultClient);
            return ret;
        }
        public static DocumentHandler GetDocumentHandlerInfo(int documentTypeID)
        {
            if (!documentHandlerInfo.ContainsKey(documentTypeID))
                throw new Exception("Document handler information not found. ID:" + documentTypeID);
            return documentHandlerInfo[documentTypeID];
        }
        public static DocumentType GetAccountingDocumentType(int typeID)
        {
            //return AccountingServer.GetAccountingDocumentType(ApplicationClient.SessionID, typeID);
            return RESTAPIClient.Call<DocumentType>(path + "GetAccountingDocumentType", new { SessionID= ApplicationClient.SessionID, typeID= typeID });
        }
        public static void ReverseDocument(TransactionReversalDocument r)
        {
            //AccountingServer.ReverseDocument(ApplicationClient.SessionID, r);
            RESTAPIClient.Call<VoidRet>(path + "ReverseDocument", new { sessionID= ApplicationClient.SessionID, r= r });
        }
        public class GetAccountDocumentsOut
        {
            public int NRecord;
            public AccountDocument[] _ret;
        }
        public static AccountDocument[] GetAccountDocuments(string query, int type, bool date
            , DateTime from, DateTime to, int index, int pageSize, out int NRecord)
        {
            //return AccountingServer.GetAccountDocuments(ApplicationClient.SessionID, query, type, date, from, to, index, pageSize, out NRecord);
            var _ret = RESTAPIClient.Call<GetAccountDocumentsOut>(path + "GetAccountDocuments", new { sessionID= ApplicationClient.SessionID, query= query, type= type, date= date, from= from, to= to, index=index, pageSize= pageSize },documentConverter);
            NRecord = _ret.NRecord;
            return _ret._ret;
        }
        public static void DeleteAccountDocument(int documentID)
        {
            //AccountingServer.DeleteAccountDocument(ApplicationClient.SessionID, documentID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteAccountDocument", new { sessionID= ApplicationClient.SessionID, documentID= documentID });
        }
        public static void ReverseAccountDocument(int documentID)
        {
            //AccountingServer.ReverseAccountDocument(ApplicationClient.SessionID, documentID);
            RESTAPIClient.Call<VoidRet>(path + "ReverseAccountDocument", new { sessionID= ApplicationClient.SessionID, documentID= documentID });
        }
        public static DocumentType[] GetAllDocumentTypes()
        {
            return documentTypes;
        }
        public static DocumentType GetDocumentTypeByID(int typeID)
        {
            foreach (DocumentType dt in documentTypes)
                if (dt.id == typeID)
                    return dt;
            return null;
        }
        public static AccountDocument GetAccountDocument(int ID, bool fullData)
        {
            //return AccountingServer.GetAccountDocument(ApplicationClient.SessionID, ID, fullData);
            return RESTAPIClient.Call<AccountDocument>(path + "GetAccountDocument", new { sessionID= ApplicationClient.SessionID, documentID= ID, fullData= fullData },documentConverter);
        }
        public static DocumentType GetDocumentTypeByType(Type type)
        {
            //return AccountingServer.GetDocumentTypeByType(ApplicationClient.SessionID, type);
            return RESTAPIClient.Call<DocumentType>(path + "GetDocumentTypeByType", new { sessionID= ApplicationClient.SessionID, type= type });
        }
        public static string GetDocumentHTML(int docID)
        {
            //return AccountingServer.GetDocumentHTML(ApplicationClient.SessionID, docID);
            return RESTAPIClient.Call<string>(path + "GetDocumentHTML", new { sessionID= ApplicationClient.SessionID, accountDocumentID= docID });
        }
        public static void DeleteGenericDocument(int docID)
        {
            //AccountingServer.DeleteGenericDocument(ApplicationClient.SessionID, docID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteGenericDocument", new { sessionID= ApplicationClient.SessionID, docID= docID });
        }
        public static void ReverseGenericDocument(int docID)
        {
           // AccountingServer.ReverseGenericDocument(ApplicationClient.SessionID, docID);
           RESTAPIClient.Call<VoidRet>(path + "ReverseGenericDocument", new { sessionID= ApplicationClient.SessionID, docID= docID });
        }
        public static Object InvokeDocumentHandlerMethod(int typeID, string method, object[] pars)
        {
            //return AccountingServer.InvokeDocumentHandlerMethod(ApplicationClient.SessionID, typeID, method, pars);
            return RESTAPIClient.Call<Object>(path + "InvokeDocumentHandlerMethod", new { sessionID= ApplicationClient.SessionID, typeID= typeID, method=method, pars= pars });
        }
        public static int PostGenericDocument(AccountDocument doc)
        {
            foreach (IAccountingClientFilter filter in filters)
                if (!filter.PostDocument(doc))
                    return -1;
            //            doc.AccountDocumentID=AccountingServer.PostGenericDocument(ApplicationClient.SessionID, doc);
            doc.AccountDocumentID =RESTAPIClient.Call<int>(path + "PostGenericDocument", new { sessionID= ApplicationClient.SessionID, doc= new BinObject(doc) });
            OnDocumentPosted(doc);
            return doc.AccountDocumentID;
        }
        public static bool HasDocumentHandler(int docTypeID)
        {
            return documentHandlerInfo.ContainsKey(docTypeID) && !string.IsNullOrEmpty( documentHandlerInfo[docTypeID].clientClass);
        }
        static public string GetDocumentHTML(AccountDocument doc)
        {
            //return AccountingServer.GetDocumentHTML(ApplicationClient.SessionID, doc);
            return RESTAPIClient.Call<string>(path + "GetDocumentHTML2", new { sessionID= ApplicationClient.SessionID, doc= new BinObject(doc) });
        }
        static public string getDocumentEntriesHTML(int docID)
        {
            //return AccountingServer.getDocumentEntriesHTML(ApplicationClient.SessionID, docID);
            return RESTAPIClient.Call<string>(path + "getDocumentEntriesHTML", new { sessionID= ApplicationClient.SessionID, docID= docID }, documentConverter);
        }
    }
}
