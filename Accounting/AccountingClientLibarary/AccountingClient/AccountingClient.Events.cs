using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
namespace INTAPS.Accounting.Client
{
    public delegate void DocumentPostedHandler(AccountDocument doc);
    public delegate void CostCenterAccountCreatedHandler(CostCenterAccount csa);
    public delegate void CostCenterAccountUpdateHandler(CostCenterAccount csa);
    public delegate void CostCenterAccountDeletedHandler(int csaID);
    public delegate void ReportUpdateHandler(ReportDefination def);
    public partial class AccountingClient
    {
        public static event DocumentPostedHandler DocumentPosted;
        public static event CostCenterAccountCreatedHandler CostCenterAccountCreated;
        public static event CostCenterAccountUpdateHandler CostCenterAccountUpdate;
        public static event CostCenterAccountDeletedHandler CostCenterAccountDeleted;
        public static event ReportChangedHandler ReportChanged;
        static void OnReportChanged(DataChange change, ReportDefination def)
        {
            if (ReportChanged != null)
                ReportChanged(change, def);
        }

        public static void OnDocumentPosted(AccountDocument doc)
        {
            if (DocumentPosted != null)
                DocumentPosted(doc);
        }

        public static void OnCostCenterAccountCreated(CostCenterAccount csa)
        {
            if (CostCenterAccountCreated != null)
                CostCenterAccountCreated(csa);
        }

        public static void OnCostCenterAccountUpdate(CostCenterAccount csa)
        {
            if (CostCenterAccountUpdate != null)
                CostCenterAccountUpdate(csa);
        }

        public static void OnCostCenterAccountDeleted(int csaID)
        {
            if (CostCenterAccountDeleted != null)
                CostCenterAccountDeleted(csaID);
        }
    }
    public class AccountClientEvents<AccountType>
    {
        public delegate void AccountCreatedHandler(AccountType ac);
        public delegate void AccountUpdatedHandler(AccountType ac);
        public delegate void AccountDeletedHandler(int accountID);

        public static event AccountCreatedHandler AccountCreated;
        public static event AccountUpdatedHandler AccountUpdated;
        public static event AccountDeletedHandler AccountDeletd;
        internal static void OnAccountCreated(AccountType ac)
        {
            INTAPS.UI.UIFormApplicationBase.runInUIThread(
    new INTAPS.UI.HTML.ProcessParameterless(delegate()
{

    try
    {
        if (AccountCreated != null)
            AccountCreated(ac);
    }
    catch (Exception ex)
    {
        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(ex.Message, ex);
    }
}));
        }

        internal static void OnAccountUpdated(AccountType ac)
        {
            INTAPS.UI.UIFormApplicationBase.runInUIThread(
                new INTAPS.UI.HTML.ProcessParameterless(delegate()
            {
                try
                {
                    if (AccountUpdated != null)
                        AccountUpdated(ac);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(ex.Message, ex);
                }
            }));
        }

        internal static void OnAccountDeletd(int id)
        {

            INTAPS.UI.UIFormApplicationBase.runInUIThread(
                new INTAPS.UI.HTML.ProcessParameterless(delegate()
            {
                try
                {
                    if (AccountDeletd != null)
                        AccountDeletd(id);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(ex.Message, ex);
                }
            }));
        }


    }
}
