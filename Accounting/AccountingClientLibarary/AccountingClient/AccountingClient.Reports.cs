﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
using System.Linq;
namespace INTAPS.Accounting.Client
{
    public interface IReportClientHandler
    {
        Form LoadReport(ReportDefination def);
        Form LoadCreateForm(int categoryID);
        object[] GetParameters(Form f);
    }
    public delegate void ReportChangedHandler(DataChange change, ReportDefination def);

    public partial class AccountingClient
    {
        static void InitializeReporting()
        {
            INTAPS.Evaluator.CalcGlobal.Initialize();
            // _reportTypes = AccountingServer.GetAllReportTypes(ApplicationClient.SessionID);
            _reportTypes =RESTAPIClient.Call<ReportType[]>(path + "GetAllReportTypes", new { sessionID= ApplicationClient.SessionID });

             _reportClientHandlers = new Dictionary<int, IReportClientHandler>();
            foreach (ReportType rt in reportTypes)
            {
                try
                {
                    Type t = Type.GetType(rt.clientClass);
                    if (t == null)
                        throw new Exception("Couldn't load type " + rt.clientClass);
                    System.Reflection.ConstructorInfo c = t.GetConstructor(new Type[] { typeof(ReportType) });
                    if (c == null)
                        throw new Exception("Couldn't load constructor for " + rt.clientClass);
                    object o = c.Invoke(new object[] { rt });
                    _reportClientHandlers.Add(rt.id, (IReportClientHandler)o);
                }
                catch (Exception ex)
                {
                    while (ex != null)
                    {
                        Console.WriteLine(ex.Message);
                        ex = ex.InnerException;
                    }
                }
            }
        }
        public static int SaveEHTMLData(ReportDefination r)
        {
            DataChange change = r.id == -1 ? DataChange.Create : DataChange.Update;
            //r.id = AccountingServer.SaveEHTMLData(ApplicationClient.SessionID, r);
            r.id =RESTAPIClient.Call<int>(path + "SaveEHTMLData", new { sessionID= ApplicationClient.SessionID, r = r });
            OnReportChanged(change, r);
            return r.id;
        }
        public static ReportDefination GetEHTMLData(int reportID)
        {
            // return AccountingServer.GetEHTMLData(ApplicationClient.SessionID, reportID);
            return RESTAPIClient.Call<ReportDefination>(path + "GetEHTMLData", new { sessionID= ApplicationClient.SessionID, reportID= reportID });
        }
        public class EvaluateEHTML4Out
        {
            public string headerItems;
            public String _ret;
        }
        public static string EvaluateEHTML(int reportID, object[] parameter,out string headerItems)
        {
            //return AccountingServer.EvaluateEHTML(ApplicationClient.SessionID,  reportID, parameter,out headerItems);
            var _ret = RESTAPIClient.Call<EvaluateEHTML4Out>(path + "EvaluateEHTML4", new { sessionID = ApplicationClient.SessionID, reportID = reportID,
                parameter = new BinObject(parameter)
            });
            headerItems = _ret.headerItems;
            return _ret._ret;
        }
        public class EvaluateEHTML2Out
        {
            public string headerItems;
            public string _ret;
        }
        public static string EvaluateEHTML(int reportTypeID, EHTMLData data, object[] parameter,out string headerItems)
        {
            // return AccountingServer.EvaluateEHTML(ApplicationClient.SessionID, reportTypeID, data, parameter,out headerItems);
            var _ret =RESTAPIClient.Call<EvaluateEHTML2Out>(path + "EvaluateEHTML2", new {
                sessionID = ApplicationClient.SessionID,
                reportTypeID = reportTypeID,
                data =data,
                parameter = parameter.Select(x => new TypeObject(x)).ToArray() });
            headerItems = _ret.headerItems;
            return _ret._ret;
        }
        public static INTAPS.Evaluator.EData EvaluateEHTML(int reportTypeID, EHTMLData data, object[] parameter, string symbol)
        {
            // return AccountingServer.EvaluateEHTML(ApplicationClient.SessionID, reportTypeID, data, parameter, symbol);
            return (INTAPS.Evaluator.EData)RESTAPIClient.Call<BinObject>(path + "EvaluateEHTML3", new {
                sessionID = ApplicationClient.SessionID,
                reportTypeID = reportTypeID,
                data = data,
                parameter = parameter.Select(x => new TypeObject(x)).ToArray(),
                symbol = symbol }).Deserialized();
        }
        public static ReportType[] GetAllReportTypes()
        {
            //return AccountingServer.GetAllReportTypes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<ReportType[]>(path + "GetAllReportTypes", new { sessionID= ApplicationClient.SessionID });
        }
        public static ReportDefination[] GetReports(int reportTypeID)
        {
            //return AccountingServer.GetReports(ApplicationClient.SessionID, reportTypeID);
            return RESTAPIClient.Call<ReportDefination[]>(path + "GetReports", new { sessionID= ApplicationClient.SessionID, reportTypeID=reportTypeID });
        }
        public static void PopulateReportSelector(int rootCat, IReportSelector selector)
        {
            PopulateReportSelector(rootCat, null, selector);
        }
        public static void PopulateReportSelector(int rootCat, object parentItem, IReportSelector selector)
        {
            ReportCategory[] categories = GetChildReportCategories(rootCat);

            foreach (ReportCategory cat in categories)
            {
                object parent = selector.CreateCategoryItem(parentItem, cat);
                PopulateReportSelector(cat.id, parent, selector);
            }
            foreach (ReportDefination def in GetReportByCategory(rootCat))
            {
                selector.CreateReportItem(parentItem, def, reportClientHandlers.ContainsKey(def.reportTypeID));
            }

        }
        public static INTAPS.Evaluator.FunctionDocumentation[] GetFunctionDocumentations(int reportTypeID)
        {
            // return AccountingServer.GetFunctionDocumentations(ApplicationClient.SessionID, reportTypeID);
            return RESTAPIClient.Call<INTAPS.Evaluator.FunctionDocumentation[]>(path + "GetFunctionDocumentations", new { sessionID= ApplicationClient.SessionID, reportTypeID= reportTypeID });
        }
        public static int GetReportDefID(int reportTypeID, string name)
        {
            //return AccountingServer.GetReportDefID(ApplicationClient.SessionID, reportTypeID, name);
            return RESTAPIClient.Call<int>(path + "GetReportDefID", new { sessionID= ApplicationClient.SessionID, reportTypeID= reportTypeID });
        }
        public static ReportDefination[] GetReportByCategory(int catID)
        {
            //return AccountingServer.GetReportByCategory(ApplicationClient.SessionID, catID);
            return RESTAPIClient.Call<ReportDefination[]>(path + "GetReportByCategory", new { sessionID= ApplicationClient.SessionID, catID= catID });
        }
        public static ReportCategory[] GetChildReportCategories(int pid)
        {
            // return AccountingServer.GetChildReportCategories(ApplicationClient.SessionID, pid);
            return RESTAPIClient.Call<ReportCategory[]>(path + "GetChildReportCategories", new { sessionID= ApplicationClient.SessionID, pid= pid });
        }
        public static int SaveReportCategory(ReportCategory cat)
        {
            //  return AccountingServer.SaveReportCategory(ApplicationClient.SessionID, cat);
            return RESTAPIClient.Call<int>(path + "SaveReportCategory", new { sessionID=ApplicationClient.SessionID, cat= cat });
        }
        public static void DeleteReportCategory(int catID)
        {
            //AccountingServer.DeleteReportCategory(ApplicationClient.SessionID, catID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteReportCategory", new { sessionID=ApplicationClient.SessionID, catID= catID });
        }
            }
}
