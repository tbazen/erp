﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
namespace INTAPS.Accounting.Client
{
    public partial class AccountingClient
    {
        public static AccountTransaction GetTransaction(int tranID)
        {
            // return AccountingServer.GetTransaction(ApplicationClient.SessionID, tranID);
            return RESTAPIClient.Call<AccountTransaction>(path + "GetTransaction", new { SessionID=ApplicationClient.SessionID, tranID= tranID });
        }
        public class GetTransactionsOut
        {
            public int NResult; public AccountBalance bal;
            public AccountTransaction[] _ret;
        }
       
        public static AccountTransaction[] GetTransactions(int accountID, int itemID, bool filterByDate, DateTime from, DateTime to, int index, int pageSize, out int NResult, out AccountBalance totalTransaction)
        {
            //return AccountingServer.GetTransactions(ApplicationClient.SessionID, accountID, itemID, filterByDate, from, to, index, pageSize, out NResult, out totalTransaction);
            var _ret = RESTAPIClient.Call<GetTransactionsOut>(path + "GetTransactions", new { SessionID=ApplicationClient.SessionID, accountID=accountID, itemID= itemID, filterByTime= filterByDate, from= from, to= to, index= index , pageSize = pageSize });
            NResult = _ret.NResult;
            totalTransaction = _ret.bal;
            return _ret._ret;
        }
        public static AccountTransaction[] GetTransactionOfDocument(int BatchID)
        {
            // return AccountingServer.GetTransactionOfDocument(ApplicationClient.SessionID, BatchID);
            return RESTAPIClient.Call<AccountTransaction[]>(path + "GetTransactionOfDocument", new { sessionID=ApplicationClient.SessionID, BatchID= BatchID });
        }
        public static AccountTransaction[] GetTransactions(int accountID, int itemID, int pageIndex, int pageSize, out int N)
        {
            AccountBalance bal;
            return GetTransactions(accountID, itemID, false, DateTime.Now, DateTime.Now, pageIndex, pageSize, out N, out bal);
        }
        public static int GetTransactionNumber()
        {
            //return AccountingServer.GetTransactionNumber(ApplicationClient.SessionID);
            return RESTAPIClient.Call<int>(path + "GetTransactionNumber", new { sessionID=ApplicationClient.SessionID });
        }
        public static void MergeTransaction(int[] sources, int destination, bool byDate, DateTime from, DateTime to)
        {
           // AccountingServer.MergeTransaction(ApplicationClient.SessionID, sources, destination, byDate, from, to);
           RESTAPIClient.Call<VoidRet>(path + "MergeTransaction", new { sessionID=ApplicationClient.SessionID, sources= sources, destination= destination, byDate= byDate, from= from, to= to });
        }
        public class GetLedger2Out
        {
            public int NRecord;
            public INTAPS.Accounting.AccountTransaction[] _ret;
        }
        static public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccount, int resItem, int[] accountID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord)
        {
            //return AccountingServer.GetLedger(ApplicationClient.SessionID, resAccount, resItem, accountID, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord);
            var _ret = RESTAPIClient.Call<GetLedger2Out>(path + "GetLedger2", new { sessionID=ApplicationClient.SessionID, resAccount=resAccount, resItem=resItem, accountID= accountID, itemID= itemID, dateFrom= dateFrom, dateTo= dateTo, pageIndex= pageIndex, pageSize= pageSize });
            NRecord = _ret.NRecord;
            return _ret._ret;
        }

    }
}
