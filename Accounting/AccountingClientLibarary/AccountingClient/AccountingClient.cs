using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Windows.Forms;
using System.Drawing;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace INTAPS.Accounting.Client
{
    public class AccountDocumentConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(AccountDocument).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader,
            Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null; 
            JObject item = JObject.Load(reader);
            var typeID = item["documentTypeID"].Value<int>();
            var type = AccountingClient.GetDocumentTypeByID(typeID).GetTypeObject();
            return item.ToObject(type);
        }
        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
    public partial class AccountingClient
    {
        public static AccountDocumentConverter documentConverter = new AccountDocumentConverter();
        public class DefaultAccountingClient : IAccountingClient
        {
            public int PostGenericDocument(AccountDocument doc)
            {
                return AccountingClient.PostGenericDocument(doc);
            }
        }

        //public static INTAPS.Accounting.IAccountingService AccountingServer;
        static String path;
        static List<IAccountingClientFilter> filters = new List<IAccountingClientFilter>();
        static DefaultAccountingClient _defaultClient = new DefaultAccountingClient();
        public static bool SingleCostCenter = false;

        static DocumentType[] _documentTypes = null;
        static Dictionary<int, DocumentHandler> _documentHandlerInfo = null;
        static Dictionary<int, IReportClientHandler> _reportClientHandlers = null;
        static ReportType[] _reportTypes = null;

        public static DocumentType[] documentTypes
        {
            get
            {
                if (_documentTypes == null)
                    InitializeReporting();
                return _documentTypes; ;
            }
        }
        public static ReportType[] reportTypes
        {
            get
            {
                if (_reportTypes == null)
                {
                    InitializeReporting();
                }
                return _reportTypes;
            }
        }
        public static Dictionary<int, IReportClientHandler> reportClientHandlers
        {
            get
            {
                if (_reportClientHandlers == null)
                {
                    InitializeReporting();
                }
                return _reportClientHandlers;
            }
        }
        public static Dictionary<int, DocumentHandler> documentHandlerInfo
        {
            get
            {
                if (_documentHandlerInfo == null)
                {
                    LoadDocumentHandlers();
                }
                return _documentHandlerInfo;
            }
        }


        public static DefaultAccountingClient AccountingClientDefaultInstance
        {
            get
            {
                return _defaultClient;
            }
        }


        public static void Connect(string url)
        {
            path = url + "/api/erp/accounting/";

            InitializeReporting();
            //SingleCostCenter = AccountingServer.IsSingleCostCenter(ApplicationClient.SessionID);
            SingleCostCenter = RESTAPIClient.Call<bool>(path + "IsSingleCostCenter", new { sessionID= ApplicationClient.SessionID });
            LoadDocumentHandlers();
        }
        public static void ConnectForReporting(string url)
        {
            path = url + "/api/erp/accounting/";
            InitializeReporting();

        }        

        public static SerialBatch[] GetAllActiveSerialBatches()
        {
            //return AccountingServer.GetAllActiveSerialBatches(ApplicationClient.SessionID);
            return RESTAPIClient.Call<SerialBatch[]>(path + "GetAllActiveSerialBatches", new { SessionID=ApplicationClient.SessionID });
        }
        public static void DeleteSerialBatch(int id)
        {
            //AccountingServer.DeleteSerialBatch(ApplicationClient.SessionID, id);
            RESTAPIClient.Call<VoidRet>(path + "DeleteSerialBatch", new { SessionID= ApplicationClient.SessionID, id= id });
        }
        public static int GetNextSerial(int batchID)
        {
            //return AccountingServer.GetNextSerial(ApplicationClient.SessionID, batchID);
            return RESTAPIClient.Call<int>(path + "GetNextSerial", new { SessionID=ApplicationClient.SessionID, batchID=batchID });
        }
        public static void UseSerial(UsedSerial ser)
        {
            //AccountingServer.UseSerial(ApplicationClient.SessionID, ser);
            RESTAPIClient.Call<VoidRet>(path + "UseSerial", new { SessionID=ApplicationClient.SessionID, ser=ser });
        }
        public static void CreateBatch(SerialBatch batch)
        {
            //batch.id = AccountingServer.CreateBatch(ApplicationClient.SessionID, batch);
            batch.id =RESTAPIClient.Call<int>(path + "CreateBatch", new { SessionID=ApplicationClient.SessionID, batch=batch });
        }


        public static ImageItem LoadImage(int imageID, int imageIndex, bool full, int Width, int Height)
        {
            //return AccountingServer.LoadImage(ApplicationClient.SessionID, imageID, imageIndex, full, Width, Height);
            return RESTAPIClient.Call<ImageItem>(path + "LoadImage", new { sessionID=ApplicationClient.SessionID, imageID=imageID, imageIndex=imageIndex, full=full, Width=Width, Height= Height });
        }
        public static Image LoadImageFormated(int imageID, int imageIndex, bool full, int Width, int Height)
        {
            //ImageItem img = AccountingServer.LoadImage(ApplicationClient.SessionID, imageID, imageIndex, full, Width, Height);
            ImageItem img =RESTAPIClient.Call<ImageItem>(path + "LoadImage", new { sessionID= ApplicationClient.SessionID, imageID= imageID, imageIndex= imageIndex, full= full, Width= Width, Height= Height });
            if (img == null)
                return null;
            return Image.FromStream(new System.IO.MemoryStream(img.imageData));
        }
        public class GetImageSizeOut
        {
            public int width; public int height; public string title;

        }
        public static void GetImageSize(int imageID, int imageIndex, out int width, out int height, out string title)
        {
            //AccountingServer.GetImageSize(ApplicationClient.SessionID, imageID, imageIndex, out width, out height, out title);
            var _ret=RESTAPIClient.Call<GetImageSizeOut>(path + "GetImageSize", new { sessionID=ApplicationClient.SessionID, imageID=imageID, imageIndex= imageIndex });
            width = _ret.width;
            height = _ret.height;
            title = _ret.title;

        }
        public static ImageItem[] GetImageItems(int imageID)
        {
            //return AccountingServer.GetImageItems(ApplicationClient.SessionID, imageID);
            return RESTAPIClient.Call<ImageItem[]>(path + "GetImageItems", new { sessionID=ApplicationClient.SessionID, imageID=imageID });
        }
        public class GetProccessProgressOut
        {
            public string message;
            public double _ret;
        }
        public static double GetProccessProgress(out string message)
        {
            //return AccountingServer.GetProccessProgress(ApplicationClient.SessionID, out message);
            var _ret= RESTAPIClient.Call<GetProccessProgressOut>(path + "GetProccessProgress", new { sessionID= ApplicationClient.SessionID });
            message = _ret.message;
            return _ret._ret;
        }
        public static void StopLongProccess()
        {
            //AccountingServer.StopLongProccess(ApplicationClient.SessionID);
            RESTAPIClient.Call<VoidRet>(path + "StopLongProccess", new { sessionID= ApplicationClient.SessionID });
        }

        static public DateTime getServerTime()
        {
            //return AccountingServer.getServerTime(ApplicationClient.SessionID);
            return RESTAPIClient.Call<DateTime>(path + "getServerTime", new { sessionID= ApplicationClient.SessionID });
        }


        //filters
        public static void RegisterFilter(IAccountingClientFilter filter)
        {
            if (filters.Contains(filter))
                filters.Remove(filter);
            filters.Add(filter);
        }
        public static void RemoveFilter(IAccountingClientFilter filter)
        {
            filters.Remove(filter);
        }

        public static INTAPS.Accounting.DocumentSerialType[] getAllDocumentSerialTypes()
        {
            //return AccountingServer.getAllDocumentSerialTypes(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.Accounting.DocumentSerialType[]>(path + "getAllDocumentSerialTypes", new { sessionID= ApplicationClient.SessionID });
        }

        public static INTAPS.Accounting.DocumentSerialBatch[] getAllDocumentSerialBatches(int serialTypeID)
        {
            //return AccountingServer.getAllDocumentSerialBatches(ApplicationClient.SessionID, serialTypeID);
            return RESTAPIClient.Call<INTAPS.Accounting.DocumentSerialBatch[]>(path + "getAllDocumentSerialBatches", new { sessionID= ApplicationClient.SessionID, serialTypeID= serialTypeID });
        }

        public static int createDocumentSerialType(INTAPS.Accounting.DocumentSerialType serialType)
        {
            //return AccountingServer.createDocumentSerialType(ApplicationClient.SessionID, serialType);
            return RESTAPIClient.Call<int>(path + "createDocumentSerialType", new { sessionID= ApplicationClient.SessionID, serialType= serialType });
        }

        public static void updateDocumentSerialType(INTAPS.Accounting.DocumentSerialType serialType)
        {
            //AccountingServer.updateDocumentSerialType(ApplicationClient.SessionID, serialType);
            RESTAPIClient.Call<VoidRet>(path + "updateDocumentSerialType", new { sessionID=ApplicationClient.SessionID, serialType= serialType });
        }

        public static void deleteDocumentSerialType(int documentSerialTypeID)
        {
            //AccountingServer.deleteDocumentSerialType(ApplicationClient.SessionID, documentSerialTypeID);
            RESTAPIClient.Call<VoidRet>(path + "deleteDocumentSerialType", new { sessionID= ApplicationClient.SessionID, documentSerialTypeID= documentSerialTypeID });
        }

        public static int createDocumentSerialBatch(INTAPS.Accounting.DocumentSerialBatch serialBatch)
        {
            //return AccountingServer.createDocumentSerialBatch(ApplicationClient.SessionID, serialBatch);
            return RESTAPIClient.Call<int>(path + "createDocumentSerialBatch", new { sessionID= ApplicationClient.SessionID, serialBatch= serialBatch });
        }

        public static void updateDocumentSerialBatch(INTAPS.Accounting.DocumentSerialBatch serialBatch)
        {
            //AccountingServer.updateDocumentSerialBatch(ApplicationClient.SessionID, serialBatch);
            RESTAPIClient.Call<VoidRet>(path + "updateDocumentSerialBatch", new { sessionID= ApplicationClient.SessionID, serialBatch= serialBatch });
        }

        public static void deleteDocumentSerialBatch(int serialBatchID)
        {
            //AccountingServer.deleteDocumentSerialBatch(ApplicationClient.SessionID, serialBatchID);
            RESTAPIClient.Call<VoidRet>(path + "deleteDocumentSerialBatch", new { sessionID= ApplicationClient.SessionID, serialBatchID= serialBatchID });
        }

        public static INTAPS.Accounting.DocumentSerialType getDocumentSerialType(string prefix)
        {
            //return AccountingServer.getDocumentSerialType(ApplicationClient.SessionID, prefix);
            return RESTAPIClient.Call<INTAPS.Accounting.DocumentSerialType>(path + "getDocumentSerialType", new { sessionID= ApplicationClient.SessionID, prefix= prefix });
        }

        public static INTAPS.Accounting.DocumentSerialType getDocumentSerialType(int serialTypeID)
        {
            //return AccountingServer.getDocumentSerialType(ApplicationClient.SessionID, serialTypeID);
            return RESTAPIClient.Call<INTAPS.Accounting.DocumentSerialType>(path + "getDocumentSerialType2", new { sessionID= ApplicationClient.SessionID, serialTypeID= serialTypeID });
        }

        public static int[] getDocumentListByTypedReference(INTAPS.Accounting.DocumentTypedReference tref)
        {
            //return AccountingServer.getDocumentListByTypedReference(ApplicationClient.SessionID, tref);
            return RESTAPIClient.Call<int[]>(path + "getDocumentListByTypedReference", new { sessionID= ApplicationClient.SessionID, tref= tref });
        }

        public static void voidSerialNo(System.DateTime time, INTAPS.Accounting.DocumentTypedReference reference, string reason)
        {
           // AccountingServer.voidSerialNo(ApplicationClient.SessionID, time, reference, reason);
           RESTAPIClient.Call<VoidRet>(path + "voidSerialNo", new { sessionID= ApplicationClient.SessionID, time= time, reference= reference, reason= reason });
        }

        public static string formatSerialNo(int typeID, int serialNo)
        {
            //return AccountingServer.formatSerialNo(ApplicationClient.SessionID, typeID, serialNo);
            return RESTAPIClient.Call<string>(path + "formatSerialNo", new { sessionID= ApplicationClient.SessionID, typeID= typeID, serialNo= serialNo });
        }

        public static int[] getAllChildAccounts<AccountType>(int pid) where AccountType : AccountBase, new()
        {
            //return AccountingServer.getAllChildAccounts<AccountType>(ApplicationClient.SessionID, pid);
            return RESTAPIClient.Call<int[]>(path + "getAllChildAccounts", new { sessionID= ApplicationClient.SessionID, pid= pid });
        }

        public static bool isControlAccount<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            //return AccountingServer.isControlAccount<AccountType>(ApplicationClient.SessionID, accountID);
            return RESTAPIClient.Call<bool>(path + "isControlAccount", new { sessionID= ApplicationClient.SessionID, accountID= accountID });
        }
        public class EvaluateEHTMLOut
        {
            public string headerItems;
            public String _ret;
        }
        public static string EvaluateEHTML(string code, object[] parameter, out string headers)
        {
            //return AccountingServer.EvaluateEHTML(ApplicationClient.SessionID, code, parameter, out headers);
            var _ret = RESTAPIClient.Call<EvaluateEHTMLOut>(path + "EvaluateEHTML", new { sessionID= ApplicationClient.SessionID, code= code,parameter=new BinObject(parameter) });
            headers = _ret.headerItems;
            return _ret._ret;
        }

        public static INTAPS.Accounting.ReportDefination GetReportDefinationInfo(string code)
        {
            //return AccountingServer.GetReportDefinationInfo(ApplicationClient.SessionID, code);
            return RESTAPIClient.Call<INTAPS.Accounting.ReportDefination>(path + "GetReportDefinationInfo", new { sessionID= ApplicationClient.SessionID, code= code });
        }

        public static INTAPS.Accounting.DocumentSerial[] getDocumentSerials(int documentID)
        {
            //return AccountingServer.getDocumentSerials(ApplicationClient.SessionID, documentID);
            return RESTAPIClient.Call<INTAPS.Accounting.DocumentSerial[]>(path + "getDocumentSerials", new { sessionID= ApplicationClient.SessionID, documentID= documentID });
        }

        public static INTAPS.Accounting.AccountBalance GetBalanceAfter(int AccountID, int ItemID, System.DateTime date)
        {
            //return AccountingServer.GetBalanceAfter(ApplicationClient.SessionID, AccountID, ItemID, date);
            return RESTAPIClient.Call<INTAPS.Accounting.AccountBalance>(path + "GetBalanceAfter", new { sessionID= ApplicationClient.SessionID, AccountID= AccountID, ItemID= ItemID, date= date });
        }

        public static double GetNetBalanceAfter(int csAccountID, int itemID, System.DateTime date)
        {
            //return AccountingServer.GetNetBalanceAfter(ApplicationClient.SessionID, csAccountID, itemID, date);
            return RESTAPIClient.Call<double>(path + "GetNetBalanceAfter", new { sessionID = ApplicationClient.SessionID, csAccountID = csAccountID, itemID = itemID, date = date });
        }

        public static int[] ExpandParents<AccountType>(int PID) where AccountType : AccountBase, new()
        {
            //return AccountingServer.ExpandParents<AccountType>(ApplicationClient.SessionID, PID);
            return RESTAPIClient.Call<int[]>(path + "ExpandParents", new { sessionID= ApplicationClient.SessionID, PID= PID });
        }

        public static INTAPS.Accounting.DocumentType GetDocumentTypeByTypeNoException(System.Type type)
        {
            //return AccountingServer.GetDocumentTypeByTypeNoException(ApplicationClient.SessionID, type);
            return RESTAPIClient.Call<INTAPS.Accounting.DocumentType>(path + "GetDocumentTypeByTypeNoException", new { sessionID= ApplicationClient.SessionID, type= type });
        }

        public static void DeleteReport(int reportID)
        {
            //AccountingServer.DeleteReport(ApplicationClient.SessionID, reportID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteReport", new { sessionID= ApplicationClient.SessionID, reportID= reportID });
        }

        public static void DeleteChildAccounts<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            //AccountingServer.DeleteChildAccounts<AccountType>(ApplicationClient.SessionID, accountID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteChildAccounts", new { sessionID= ApplicationClient.SessionID, accountID= accountID });
            AccountClientEvents<AccountType>.OnAccountUpdated(AccountingClient.GetAccount<AccountType>(accountID));
        }

        public static INTAPS.Accounting.AccountDependancy[] getAccountDependancy(int documentID)
        {
            //return AccountingServer.getAccountDependancy(ApplicationClient.SessionID, documentID);
            return RESTAPIClient.Call<INTAPS.Accounting.AccountDependancy[]>(path + "getAccountDependancy", new { sessionID= ApplicationClient.SessionID, documentID= documentID });
        }
    }

}
