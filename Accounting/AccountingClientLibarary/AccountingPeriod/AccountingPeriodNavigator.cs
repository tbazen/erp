using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    abstract public class AccountingPeriodNavigator<AccuntingPeriodType> where AccuntingPeriodType:AccountingPeriod
    {
        public event EventHandler PeriodSelectionChanged;
        ToolStripButton m_prevYear, m_prevMonth, m_nextMonth, m_nextYear;
        ToolStripLabel m_display;
        int CurrentYear;
        int CurrentMonth;
        AccuntingPeriodType[] Months;
        public AccuntingPeriodType SelectedPeriod
        {
            get
            {
                return Months[CurrentMonth];
            }
            set
            {
                int m=0;
                foreach (AccuntingPeriodType pt in Months)
                {
                    if (pt.id == value.id)
                    {
                        CurrentMonth = m;

                        break;
                    }
                }
            }
        }
        public AccuntingPeriodType SelectedPrevPeriod
        {
            get
            {
                if (CurrentMonth == 0)
                    return GetPayPeriods(CurrentYear - 1)[0];
                return Months[CurrentMonth-1];
            }
        }
        public AccountingPeriodNavigator(ToolStripButton prevYear, ToolStripButton prevMonth, ToolStripButton nextMonth, ToolStripButton nextYear, ToolStripLabel display,int year)
        {
            m_prevYear = prevYear;
            m_prevMonth = prevMonth;
            m_nextMonth = nextMonth;
            m_nextYear = nextYear;
            m_display = display;
            m_prevYear.Click += new EventHandler(m_prevYear_Click);
            m_prevMonth.Click += new EventHandler(m_prevMonth_Click);
            m_nextMonth.Click += new EventHandler(m_nextMonth_Click);
            m_nextYear.Click += new EventHandler(m_nextYear_Click);

            CurrentYear = year;
            LoadYearPeriods();
            SetPeriodLabel();
        }
        virtual protected void OnPeriodSelectionChanged()
        {
            if (PeriodSelectionChanged != null)
                PeriodSelectionChanged(this, null);
        }
        void SetPeriodLabel()
        {
            m_display.Text = CurrentYear + "/" + (CurrentMonth + 1);
        }
        protected abstract AccuntingPeriodType[] GetPayPeriods(int Year);
        protected virtual void LoadYearPeriods()
        {
            //Months = PayrollClient.GetPayPeriods(CurrentYear, PayrollClient.Ethiopian);
            Months = GetPayPeriods(CurrentYear);
            DateTime now = DateTime.Now;
            int m;
            for (m = 0; m < Months.Length; m++)
            {
                if (Months[m].InPeriod(now))
                {
                    CurrentMonth = m;
                    break;
                }
            }
            if (m == Months.Length)
                CurrentMonth = 0;
        }
        void m_nextYear_Click(object sender, EventArgs e)
        {
            CurrentYear++;
            LoadYearPeriods();
            SetPeriodLabel();
            OnPeriodSelectionChanged();
        }

        void m_nextMonth_Click(object sender, EventArgs e)
        {
            if (CurrentMonth == Months.Length - 1)
            {
                CurrentMonth = 0;
                CurrentYear++;
                LoadYearPeriods();
            }
            else
            {
                CurrentMonth++;
            }
            SetPeriodLabel();
            OnPeriodSelectionChanged();

        }

        void m_prevMonth_Click(object sender, EventArgs e)
        {
            if (CurrentMonth > 0)
                CurrentMonth--;
            else
            {
                CurrentYear--;
                LoadYearPeriods();
                CurrentMonth = Months.Length - 1;
            }
            SetPeriodLabel();
            OnPeriodSelectionChanged();
        }

        void m_prevYear_Click(object sender, EventArgs e)
        {
            CurrentYear--;
            LoadYearPeriods();
            SetPeriodLabel();
            OnPeriodSelectionChanged();
        }
    }
}
