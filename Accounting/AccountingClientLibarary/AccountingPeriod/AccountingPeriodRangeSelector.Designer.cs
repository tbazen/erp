namespace INTAPS.Accounting.Client
{
    partial class AccountingPeriodRangeSelector<AccountingPeriodType, AccountingPeriodSelectorType> 
        where AccountingPeriodType : AccountingPeriod
        where AccountingPeriodSelectorType : AccountingPeriodSelector<AccountingPeriodType>, new()
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbTo = new AccountingPeriodSelectorType();
            this.chkTo = new System.Windows.Forms.CheckBox();
            this.cmbFrom = new AccountingPeriodSelectorType();
            this.chkAllTime = new System.Windows.Forms.CheckBox();
            this.lblFrom = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbTo
            // 
            this.cmbTo.Location = new System.Drawing.Point(88, 60);
            this.cmbTo.Name = "cmbTo";
            this.cmbTo.Size = new System.Drawing.Size(338, 21);
            this.cmbTo.TabIndex = 21;
            this.cmbTo.Anchor = System.Windows.Forms.AnchorStyles.Top 
                | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            // 
            // chkTo
            // 
            this.chkTo.AutoSize = true;
            this.chkTo.Location = new System.Drawing.Point(11, 60);
            this.chkTo.Name = "chkTo";
            this.chkTo.Size = new System.Drawing.Size(44, 17);
            this.chkTo.TabIndex = 20;
            this.chkTo.Text = "To\"";
            this.chkTo.UseVisualStyleBackColor = true;
            this.chkTo.CheckedChanged += new System.EventHandler(this.chkTo_CheckedChanged);

            // 
            // cmbFrom
            // 
            this.cmbFrom.Location = new System.Drawing.Point(88, 33);
            this.cmbFrom.Name = "cmbFrom";
            this.cmbFrom.Size = new System.Drawing.Size(338, 21);
            this.cmbFrom.TabIndex = 19;
            this.cmbFrom.Anchor = System.Windows.Forms.AnchorStyles.Top 
                | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            // 
            // chkAllTime
            // 
            this.chkAllTime.AutoSize = true;
            this.chkAllTime.Location = new System.Drawing.Point(3, 3);
            this.chkAllTime.Name = "chkAllTime";
            this.chkAllTime.Size = new System.Drawing.Size(63, 17);
            this.chkAllTime.TabIndex = 18;
            this.chkAllTime.Text = "All Time";
            this.chkAllTime.UseVisualStyleBackColor = true;
            this.chkAllTime.CheckedChanged += new System.EventHandler(this.chkAllTime_CheckedChanged);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(8, 33);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(30, 13);
            this.lblFrom.TabIndex = 17;
            this.lblFrom.Text = "From";
            // 
            // AccountingPeriodRangeSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmbTo);
            this.Controls.Add(this.chkTo);
            this.Controls.Add(this.cmbFrom);
            this.Controls.Add(this.chkAllTime);
            this.Controls.Add(this.lblFrom);
            this.Name = "AccountingPeriodRangeSelector";
            this.Size = new System.Drawing.Size(429, 87);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AccountingPeriodSelectorType cmbTo;
        private System.Windows.Forms.CheckBox chkTo;
        private AccountingPeriodSelectorType cmbFrom;
        private System.Windows.Forms.CheckBox chkAllTime;
        private System.Windows.Forms.Label lblFrom;
    }
}
