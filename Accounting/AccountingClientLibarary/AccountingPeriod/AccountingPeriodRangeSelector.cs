using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
namespace INTAPS.Accounting.Client
{
    public abstract partial class AccountingPeriodRangeSelector<AccountingPeriodType,AccountingPeriodSelectorType > : UserControl 
        where AccountingPeriodType:AccountingPeriod
        where AccountingPeriodSelectorType:AccountingPeriodSelector<AccountingPeriodType>,new()

    {
        public event EventHandler PeriodRangeChanged;
        public AccountingPeriodRangeSelector()
        {
            InitializeComponent();
            cmbTo.NullSelection = "Forever";
            chkAllTime.Checked = true;
        }
        protected abstract AccountingPeriodType[] GetAccountingPeriods(int Year);
        protected abstract AccountingPeriodType GetAccountingPeriod(int periodID);
        public void LoadData(int year)
        {
            cmbFrom.LoadData(year);
            cmbTo.LoadData(year);
            chkAllTime_CheckedChanged(null, null);
            chkTo_CheckedChanged(null, null);
        }
        private void chkAllTime_CheckedChanged(object sender, EventArgs e)
        {
            if (PeriodRangeChanged != null)
                PeriodRangeChanged(this, null);
            cmbFrom.Enabled = !chkAllTime.Checked;
            chkTo.Enabled = !chkAllTime.Checked;
            cmbTo.Enabled = !chkAllTime.Checked && chkTo.Checked;
        }
        private void chkTo_CheckedChanged(object sender, EventArgs e)
        {
            if (PeriodRangeChanged != null)
                PeriodRangeChanged(this, null);

            cmbTo.Enabled = chkTo.Checked && !chkAllTime.Checked;
            if (chkTo.Checked)
                lblFrom.Text = "Period From";
            else
                lblFrom.Text = "Period";
        }
        
        public bool IsDataValid()
        {
            if (!chkAllTime.Checked && cmbFrom.SelectedPeriod==null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select start period.");
                return false;
            }
            return true;
        }
        public void SetData(PayPeriodRange pr)
        {
            chkAllTime.Checked = pr.AllPeriod;
            chkTo.Checked = !pr.SinglePeriod;
            if (!pr.AllPeriod)
            {
                cmbFrom.SelectedPeriod = GetAccountingPeriod(pr.PeriodFrom);
                if (!pr.SinglePeriod)
                {
                    if (pr.FromNowOn)
                        cmbTo.SelectedPeriod = null;
                    else
                        cmbTo.SelectedPeriod = GetAccountingPeriod(pr.PeriodTo);
                }
            }
        }
        public PayPeriodRange GetData()
        {
            PayPeriodRange ret;
            if (chkAllTime.Checked)
            {
                ret.PeriodFrom = -1;
                ret.PeriodTo = -1;
            }
            else
            {
                ret.PeriodFrom = cmbFrom.SelectedPeriod.id;
                if (chkTo.Checked)
                {
                    if (cmbTo.SelectedPeriod==null)
                        ret.PeriodTo = -1;
                    else
                        ret.PeriodTo = cmbTo.SelectedPeriod.id;
                }
                else
                    ret.PeriodTo = ret.PeriodFrom;
            }
            
            return ret;
        }

        private void cmbFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PeriodRangeChanged != null)
                PeriodRangeChanged(this, null);

        }

        private void cmbTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PeriodRangeChanged != null)
                PeriodRangeChanged(this, null);

        }

    }
}
