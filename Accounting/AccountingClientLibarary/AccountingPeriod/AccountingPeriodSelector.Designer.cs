namespace INTAPS.Accounting.Client
{
    partial class AccountingPeriodSelector<AccountingPeriodType> where AccountingPeriodType : AccountingPeriod
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.btnNexYear = new System.Windows.Forms.Button();
            this.btnPrevYear = new System.Windows.Forms.Button();
            this.lblYear = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbMonth
            // 
            this.cmbMonth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.Location = new System.Drawing.Point(0, 3);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(161, 21);
            this.cmbMonth.TabIndex = 21;
            this.cmbMonth.SelectedIndexChanged += new System.EventHandler(this.cmbMonth_SelectedIndexChanged);
            // 
            // btnNexYear
            // 
            this.btnNexYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNexYear.Location = new System.Drawing.Point(243, 1);
            this.btnNexYear.Name = "btnNexYear";
            this.btnNexYear.Size = new System.Drawing.Size(33, 23);
            this.btnNexYear.TabIndex = 22;
            this.btnNexYear.Text = ">";
            this.btnNexYear.UseVisualStyleBackColor = true;
            this.btnNexYear.Click += new System.EventHandler(this.btnNexYear_Click);
            // 
            // btnPrevYear
            // 
            this.btnPrevYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevYear.Location = new System.Drawing.Point(167, 1);
            this.btnPrevYear.Name = "btnPrevYear";
            this.btnPrevYear.Size = new System.Drawing.Size(33, 23);
            this.btnPrevYear.TabIndex = 22;
            this.btnPrevYear.Text = "<";
            this.btnPrevYear.UseVisualStyleBackColor = true;
            this.btnPrevYear.Click += new System.EventHandler(this.btnPrevYear_Click);
            // 
            // lblYear
            // 
            this.lblYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblYear.Location = new System.Drawing.Point(196, 1);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(52, 23);
            this.lblYear.TabIndex = 23;
            this.lblYear.Text = "##";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AccountingPeriodSelector
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.btnPrevYear);
            this.Controls.Add(this.btnNexYear);
            this.Controls.Add(this.cmbMonth);
            this.Controls.Add(this.lblYear);
            this.Name = "AccountingPeriodSelector";
            this.Size = new System.Drawing.Size(279, 26);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.Button btnNexYear;
        private System.Windows.Forms.Button btnPrevYear;
        private System.Windows.Forms.Label lblYear;
    }
}
