using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public abstract partial class AccountingPeriodSelector<AccountingPeriodType> : UserControl where AccountingPeriodType : AccountingPeriod
    {
        public event EventHandler SelectionChanged;
        string m_nullSelection = null;
        AccountingPeriodType[] months;
        int currentYear;
        
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            this.Height = cmbMonth.Height+3;
            cmbMonth.Top = 0;
            btnNexYear.Top = 0;
            btnPrevYear.Top=0;
            lblYear.Top = 0;

            btnNexYear.Height = cmbMonth.Height;
            btnPrevYear.Height = cmbMonth.Height;
            lblYear.Height = cmbMonth.Height;

        }
        
        public AccountingPeriodSelector()
        {
            InitializeComponent();
        }
        
        public string NullSelection
        {
            get { return m_nullSelection; }
            set { m_nullSelection = value; }
        }

        public virtual void LoadData(int year)
        {
            currentYear = year;
            LoadYearPeriods();
            DateTime now = DateTime.Now;
            foreach (AccountingPeriodType pp in months)
            {
                if (pp.InPeriod(now))
                {
                    cmbMonth.SelectedItem = pp;
                    return;
                }
            }
        }
        protected abstract AccountingPeriodType[] GetPayPeriods(int Year);
        protected abstract int GetAccountPeriodYear(int year);
        private void LoadYearPeriods()
        {
            lblYear.Text = currentYear.ToString();
            cmbMonth.Items.Clear();
            months = GetPayPeriods(currentYear);
            
            if (m_nullSelection != null)
                cmbMonth.Items.Add(m_nullSelection);
            if (months != null)
                cmbMonth.Items.AddRange(months);
        }
        [Browsable(false)]
        public AccountingPeriodType SelectedPeriod
        {
            get
            {
                return cmbMonth.SelectedItem as AccountingPeriodType;
            }
            set
            {
                if (value == null)
                {
                    cmbMonth.SelectedItem = m_nullSelection;
                    return;
                }
                foreach (AccountingPeriodType pp in months)
                {
                    if (pp.id == value.id)
                    {
                        cmbMonth.SelectedItem = pp;
                        return;
                    }
                }
                currentYear = GetAccountPeriodYear(value.id);
                LoadYearPeriods();
                foreach (AccountingPeriodType pp in months)
                {
                    if (pp.id == value.id)
                    {
                        cmbMonth.SelectedItem = pp;
                        return;
                    }
                }
            }
        }

        private void btnNexYear_Click(object sender, EventArgs e)
        {
            currentYear++;
            LoadYearPeriods();
            cmbMonth.SelectedIndex = 0;
        }

        private void btnPrevYear_Click(object sender, EventArgs e)
        {
            currentYear--;
            LoadYearPeriods();
            cmbMonth.SelectedIndex = 0;
        }

        private void cmbMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectionChanged != null)
                SelectionChanged(this, null);
        }
    }
}
