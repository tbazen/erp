using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class ChildAccountDropdownBase<AccountType> : ComboBox
        where AccountType:AccountBase,new()
    {
        public event EventHandler SelectedAccountChanged;
        protected const int MAX_ITEMS = 200;
        AccountType[] m_childs;
        bool m_IgnoreEvents = false;
        public ChildAccountDropdownBase()
        {
            this.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        public virtual void LoadData(int parentAccount)
        {
            m_childs = this.GetChildAccounts(parentAccount);
            this.Items.Clear();
            for (int i = 0; i < m_childs.Length; i++)
            {
                this.Items.Add(this.GetAccountString(m_childs[i]));
            }
            
        }
        public AccountType SelectedAccount
        {
            get
            {
                if (m_childs == null)
                    return null;
                int index = this.SelectedIndex;
                if (index == -1)
                    return null;
                return m_childs[index];
            }
            set
            {
                if (value == null)
                    SetByID(-1);
                else
                {
                    SetByID(value.id);
                }
            }
        }
        public void SetByID(int accountID)
        {
            try
            {
                m_IgnoreEvents = true;
                if (m_childs != null && accountID != -1)
                {
                    for (int i = 0; i < m_childs.Length; i++)
                    {
                        if (m_childs[i].id == accountID)
                        {
                            this.SelectedIndex = i;
                            return;

                        }
                    }
                }
                this.SelectedIndex = -1;
            }
            finally
            {
                m_IgnoreEvents = false;
            }
        }
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if (m_IgnoreEvents)
                return;
            if (SelectedAccountChanged != null)
                SelectedAccountChanged(this, e);
        }

        protected virtual AccountType[] GetChildAccounts(int parentAccount)
        {
            return INTAPS.Accounting.Client.AccountingClient.GetLeafAccounts<AccountType>(parentAccount);// GetChildAcccount(parentAccount, 0, MAX_ITEMS, out N);
        }
        public virtual string GetAccountString(AccountType a)
        {
            return a.CodeName;
        }
    }
    public class ChildAccountDropdown : ChildAccountDropdownBase<Account>
    {
    }
    public class ChildCostCenterDrowdown : ChildAccountDropdownBase<Account>
    {
    }
}
