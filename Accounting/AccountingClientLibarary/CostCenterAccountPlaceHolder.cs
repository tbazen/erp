﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class CostCenterAccountPlaceHolder: TextBox
    {
        public event EventHandler AccountChanged;
        AccountStructureBrowser m_picker;
        CostCenterAccountWithDescription m_picked;
        Button m_btnPick;
        bool m_changed;
        bool m_allowAdd = false;

        public bool AllowAdd
        {
            get
            {
                return m_allowAdd;
            }
            set
            {
                m_allowAdd = value;
                if (value && m_btnNew == null)
                {
                    m_btnNew = new Button();
                    m_btnNew.Text = "+";
                    m_btnNew.Dock = DockStyle.Right;
                    m_btnNew.Width = 20;
                    m_btnNew.Click += new EventHandler(m_btnNew_Click);
                }
            }
        }

        void m_btnNew_Click(object sender, EventArgs e)
        {
            //AccountEditor f=new AccountEditor(
        }
        Button m_btnNew;
        public CostCenterAccountPlaceHolder()
        {
            m_btnPick = new Button();
            m_btnPick.Text = "...";
            m_btnPick.Dock = DockStyle.Right;
            m_btnPick.Width = 20;
            m_btnPick.Click += new EventHandler(m_btnPick_Click);
            m_changed = false;
            this.Controls.Add(m_btnPick);
        }
        public bool Changed
        {
            get
            {
                return m_changed;
            }
        }
        virtual protected AccountStructureBrowser CreatePicker()
        {
            return new AccountStructureBrowser(true);
        }
        void m_btnPick_Click(object sender, EventArgs e)
        {
            m_picker = CreatePicker();
            if (m_picker.ShowDialog() == DialogResult.OK)
            {
                this.costCenterAccountWithDescription = m_picker.PickedAccount;
                m_changed = true;
                OnChanged();
            }
        }
        [Browsable(false)]
        public CostCenterAccountWithDescription costCenterAccountWithDescription
        {
            get
            {
                return m_picked;
            }
            set
            {
                m_picked = value;
                if (m_picked == null)
                    this.Text = "";
                else
                    this.Text = m_picked.CodeName;
            }
        }
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            this.SelectAll();
        }

        protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string txt = this.Text.Trim();
                if (txt == "")
                    this.costCenterAccountWithDescription = null;
                else
                {
                    try
                    {
                        this.costCenterAccountWithDescription = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccountWithDescription(txt);
                    }
                    catch (Exception ex)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(ex.Message);
                        this.costCenterAccountWithDescription = m_picked;
                        this.SelectAll();
                    }
                }
                m_changed = true;
                OnChanged();
            }
        }
        public void OnChanged()
        {
            if (AccountChanged != null)
                AccountChanged(this, null);
        }

        public void SetByID(int ID)
        {
            if (ID == -1)
                this.costCenterAccountWithDescription = null;
            else
            {
                try
                {
                    this.costCenterAccountWithDescription = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccountWithDescription(ID);
                }
                catch
                {
                    m_picked = null;
                    this.Text = "error";
                }
            }
        }
        public void SetByID(int costCenterID,int accountID)
        {
            if (costCenterID== -1||accountID==-1)
                this.costCenterAccountWithDescription = null;
            else
            {
                try
                {
                    this.costCenterAccountWithDescription = INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccountWithDescription(costCenterID, accountID);
                }
                catch
                {
                    m_picked = null;
                    this.Text = "error";
                }
            }
        }

        public int getCostCenterAccountID()
        {
            if (m_picked == null)
                return -1;
            return m_picked.id;
        }
    }


}
