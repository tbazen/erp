﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class CostCenterTree:AccountCostCenterTreeBase<CostCenter>
    {
        ImageList _imageList;
        public CostCenterTree()
        {
            _imageList = new ImageList();
            _imageList.Images.Add(Resources.cs);
            _imageList.Images.Add(Resources.contCs);
            this.ImageList = _imageList;
        }
        
    }
    
}

