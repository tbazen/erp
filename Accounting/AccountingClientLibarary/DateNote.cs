using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DateNoteInput : Form
    {
        public DateTime date;
        public string note;
        public DateNoteInput(string title,string prompt,DateTime defaultDate,string defaultNote)
        {
            InitializeComponent();
            this.Text = title;
            lblPrompt.Text = prompt;
            dtpDate.Value = defaultDate;
            txtMoreInfo.Text = defaultNote;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            date = dtpDate.Value;
            note = txtMoreInfo.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void DateNoteInput_Load(object sender, EventArgs e)
        {

        }        
    }
}