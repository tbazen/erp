using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DeleteAccountProgressForm<AccountType>: Form
        where AccountType:AccountBase, new()
    {
        int accountID;
        public DeleteAccountProgressForm(int accountID)
        {
            InitializeComponent();
            this.accountID = accountID;

        }

//        protected override bool ShowAnmiation
//        {
//            get
//            {
//                return false;
//            }
//        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            timer1.Enabled = true;
            new System.Threading.Thread(delegate()
            {
                try
                {
                    INTAPS.Accounting.Client.AccountingClient.DeleteChildAccounts<AccountType>(accountID);
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Accounts succesfully deleted");
                        timer1.Enabled=false;
                        this.Enabled=true;
                        this.Close();
                    }
                   ));
                }
                catch (Exception ex)
                {
                    this.Invoke(new INTAPS.UI.HTML.ProcessSingleParameter<Exception>(delegate(Exception exx)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                        timer1.Enabled = false;
                        this.Enabled = true;
                    }
                    ),ex);
                }
            }).Start();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string message;
            progressBar.Value= Math.Max(0,(int)Math.Min( AccountingClient.GetProccessProgress(out message)*100,100));
            lblStage.Text = message;
        }

    }
    
}