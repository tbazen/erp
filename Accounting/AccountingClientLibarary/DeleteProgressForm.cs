using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DeleteProgressForm: INTAPS.UI.Windows.AsyncForm
    {
        public List<AccountDocument> docs;
        public bool reverse;
        public DeleteProgressForm(List<AccountDocument> doc, bool reverse)
        {
            InitializeComponent();
            this.docs = doc;
            this.reverse = reverse;

        }
        protected override bool ShowAnmiation
        {
            get
            {
                return false;
            }
        }
        protected override void WaitIsOver(INTAPS.UI.Windows.ServerDataDownloader d)
        {
            ///this.Enabled = true;
            DeleteDocumentProcess dp = (DeleteDocumentProcess)d;
            if (dp.ex != null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to delete/reverse transaction", dp.ex);
                this.DialogResult = DialogResult.Cancel;
            }
            else
                this.DialogResult = DialogResult.OK;
            this.Close();
        }
        protected override void CheckProgress()
        {
            string msg1;
            progressBar.Value=(int)(100* 
                INTAPS.Accounting.Client.AccountingClient.GetProccessProgress(out msg1)
                );
            lblStage.Text = msg1;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //this.Enabled = false;
            base.WaitFor(new DeleteDocumentProcess(docs, reverse));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
    public class DeleteDocumentProcess : INTAPS.UI.Windows.ServerDataDownloader
    {
        public List<AccountDocument> docs;
        public Exception ex=null;
        public bool reverse;
        public DeleteDocumentProcess(List<AccountDocument> docs, bool reverse)
        {
            this.docs = docs;
            this.reverse = reverse;
        }


        public override void RunDownloader()
        {
            try
            {
                foreach (AccountDocument doc in docs)
                {
                    if (reverse)
                    {
                        INTAPS.Accounting.Client.AccountingClient.ReverseGenericDocument(doc.AccountDocumentID);
                    }
                    else
                    {
                        INTAPS.Accounting.Client.AccountingClient.DeleteGenericDocument(doc.AccountDocumentID);

                    }
                }
            }
            catch (Exception ex)
            {
                this.ex = ex;
            }
        }

        public override string Description
        {
            get
            {
                if (reverse) 
                    return "Reversing transaction";
                return "Deleting transaction";

            }
        }
    }
}