﻿namespace INTAPS.Accounting.Client
{
    partial class DocumentSerialBatchEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.textType = new System.Windows.Forms.TextBox();
            this.textSerTo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textSerFrom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkUnboundSerial = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textFormat = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dcTo = new INTAPS.Ethiopic.DualCalendar();
            this.dcFrom = new INTAPS.Ethiopic.DualCalendar();
            this.label3 = new System.Windows.Forms.Label();
            this.checkTimeUnbound = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.costCenter = new INTAPS.Accounting.Client.CostCenterPlaceHolder();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(536, 11);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 28);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(444, 11);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 28);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // textType
            // 
            this.textType.Location = new System.Drawing.Point(18, 15);
            this.textType.Name = "textType";
            this.textType.ReadOnly = true;
            this.textType.Size = new System.Drawing.Size(593, 20);
            this.textType.TabIndex = 2;
            // 
            // textSerTo
            // 
            this.textSerTo.Location = new System.Drawing.Point(138, 51);
            this.textSerTo.Name = "textSerTo";
            this.textSerTo.Size = new System.Drawing.Size(330, 20);
            this.textSerTo.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "To:";
            // 
            // textSerFrom
            // 
            this.textSerFrom.Location = new System.Drawing.Point(138, 25);
            this.textSerFrom.Name = "textSerFrom";
            this.textSerFrom.Size = new System.Drawing.Size(330, 20);
            this.textSerFrom.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "From:";
            // 
            // checkUnboundSerial
            // 
            this.checkUnboundSerial.AutoSize = true;
            this.checkUnboundSerial.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkUnboundSerial.ForeColor = System.Drawing.Color.White;
            this.checkUnboundSerial.Location = new System.Drawing.Point(138, 103);
            this.checkUnboundSerial.Name = "checkUnboundSerial";
            this.checkUnboundSerial.Size = new System.Drawing.Size(86, 21);
            this.checkUnboundSerial.TabIndex = 12;
            this.checkUnboundSerial.Text = "Unbound";
            this.checkUnboundSerial.UseVisualStyleBackColor = true;
            this.checkUnboundSerial.CheckedChanged += new System.EventHandler(this.checkUnboundSerial_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkUnboundSerial);
            this.groupBox1.Controls.Add(this.textSerFrom);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textFormat);
            this.groupBox1.Controls.Add(this.textSerTo);
            this.groupBox1.Location = new System.Drawing.Point(18, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(593, 130);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial Number Range";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(6, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Display Format:";
            // 
            // textFormat
            // 
            this.textFormat.Location = new System.Drawing.Point(138, 77);
            this.textFormat.Name = "textFormat";
            this.textFormat.Size = new System.Drawing.Size(330, 20);
            this.textFormat.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dcTo);
            this.groupBox2.Controls.Add(this.dcFrom);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.checkTimeUnbound);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(18, 214);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(593, 104);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Applicable Period for the Range";
            // 
            // dcTo
            // 
            this.dcTo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dcTo.ForeColor = System.Drawing.Color.White;
            this.dcTo.Location = new System.Drawing.Point(138, 38);
            this.dcTo.Name = "dcTo";
            this.dcTo.ShowEthiopian = true;
            this.dcTo.ShowGregorian = true;
            this.dcTo.ShowTime = false;
            this.dcTo.Size = new System.Drawing.Size(411, 23);
            this.dcTo.TabIndex = 13;
            this.dcTo.VerticalLayout = false;
            // 
            // dcFrom
            // 
            this.dcFrom.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dcFrom.ForeColor = System.Drawing.Color.White;
            this.dcFrom.Location = new System.Drawing.Point(138, 19);
            this.dcFrom.Name = "dcFrom";
            this.dcFrom.ShowEthiopian = true;
            this.dcFrom.ShowGregorian = true;
            this.dcFrom.ShowTime = false;
            this.dcFrom.Size = new System.Drawing.Size(411, 23);
            this.dcFrom.TabIndex = 13;
            this.dcFrom.VerticalLayout = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "From:";
            // 
            // checkTimeUnbound
            // 
            this.checkTimeUnbound.AutoSize = true;
            this.checkTimeUnbound.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkTimeUnbound.ForeColor = System.Drawing.Color.White;
            this.checkTimeUnbound.Location = new System.Drawing.Point(138, 67);
            this.checkTimeUnbound.Name = "checkTimeUnbound";
            this.checkTimeUnbound.Size = new System.Drawing.Size(86, 21);
            this.checkTimeUnbound.TabIndex = 12;
            this.checkTimeUnbound.Text = "Unbound";
            this.checkTimeUnbound.UseVisualStyleBackColor = true;
            this.checkTimeUnbound.CheckedChanged += new System.EventHandler(this.checkTimeUnbound_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "To:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(15, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Accounting Center:";
            // 
            // costCenter
            // 
            this.costCenter.account = null;
            this.costCenter.AllowAdd = false;
            this.costCenter.Location = new System.Drawing.Point(156, 52);
            this.costCenter.Name = "costCenter";
            this.costCenter.OnlyLeafAccount = false;
            this.costCenter.Size = new System.Drawing.Size(455, 20);
            this.costCenter.TabIndex = 14;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 333);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(639, 46);
            this.panel1.TabIndex = 15;
            // 
            // DocumentSerialBatchEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(639, 379);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.costCenter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "DocumentSerialBatchEditor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Document  Serial Reference Number Range Editor";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.TextBox textType;
        private System.Windows.Forms.TextBox textSerTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textSerFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkUnboundSerial;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkTimeUnbound;
        private System.Windows.Forms.Label label4;
        private Ethiopic.DualCalendar dcTo;
        private Ethiopic.DualCalendar dcFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textFormat;
        private CostCenterPlaceHolder costCenter;
        private System.Windows.Forms.Panel panel1;
    }
}