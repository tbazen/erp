﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DocumentSerialBatchEditor : Form
    {
        int _batchID = -1;
        DocumentSerialType _type;
        public DocumentSerialBatchEditor(DocumentSerialType type)
        {
            InitializeComponent();
            textType.Text = type.name;
            _type = type;
        }

        public DocumentSerialBatchEditor(DocumentSerialType type,DocumentSerialBatch batch)
            :this(type)
        {
            textSerFrom.Text = batch.fromSer.ToString();
            if (batch.toSer == -1)
                checkUnboundSerial.Checked = true;
            else
                textSerTo.Text = batch.toSer.ToString();

            dcFrom.DateTime = batch.fromDate;
            if (!batch.timeUpperBound)
                checkTimeUnbound.Checked = true;
            else
                dcTo.DateTime = batch.toDate;
            costCenter.SetByID(batch.costCenterID);
            textFormat.Text = batch.displayFormat;
            _batchID = batch.id;
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            DocumentSerialBatch batch = new DocumentSerialBatch();
            if (!INTAPS.UI.Helper.ValidateIntegerTextBox(textSerFrom, "Enter serial range lower bound", out batch.fromSer))
                return;
            if (checkUnboundSerial.Checked)
                batch.toSer = -1;
            else if (!INTAPS.UI.Helper.ValidateIntegerTextBox(textSerTo, "Enter serial range upper bound", out batch.toSer))
                    return;
            batch.fromDate = dcFrom.DateTime.Date;
            batch.timeUpperBound = !checkTimeUnbound.Checked;
            batch.toDate= dcTo.DateTime.Date.AddDays(1);
            batch.displayFormat = textFormat.Text;
            batch.costCenterID = costCenter.GetAccountID();
            batch.typeID = _type.id;
            batch.id = _batchID;
            if (batch.costCenterID == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select accounting center");
                return;
            }
            try
            {
                if (_batchID == -1)
                    _batchID = AccountingClient.createDocumentSerialBatch(batch);
                else
                    AccountingClient.updateDocumentSerialBatch(batch);
                this.DialogResult = DialogResult.OK;
                
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to save serial batch", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void checkUnboundSerial_CheckedChanged(object sender, EventArgs e)
        {
            textSerTo.Enabled = !checkUnboundSerial.Checked;
        }

        private void checkTimeUnbound_CheckedChanged(object sender, EventArgs e)
        {
            dcTo.Enabled = !checkTimeUnbound.Checked;
        }        
    }
}
