﻿namespace INTAPS.Accounting.Client
{
    partial class DocumentSerialSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gridTypes = new System.Windows.Forms.DataGridView();
            this.colSerialTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSerialTypeDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.splitDetails = new System.Windows.Forms.SplitContainer();
            this.buttonAddType = new System.Windows.Forms.Button();
            this.buttonAddRange = new System.Windows.Forms.Button();
            this.gridBatches = new System.Windows.Forms.DataGridView();
            this.colBatchFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchCostCenter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchDateFrom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchDateTo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchFormat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBatchDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonBatch = new System.Windows.Forms.Button();
            this.listBaches = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitDetails)).BeginInit();
            this.splitDetails.Panel1.SuspendLayout();
            this.splitDetails.Panel2.SuspendLayout();
            this.splitDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridBatches)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridTypes);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitDetails);
            this.splitContainer1.Size = new System.Drawing.Size(925, 341);
            this.splitContainer1.SplitterDistance = 308;
            this.splitContainer1.TabIndex = 0;
            // 
            // gridTypes
            // 
            this.gridTypes.AllowUserToAddRows = false;
            this.gridTypes.AllowUserToDeleteRows = false;
            this.gridTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTypes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSerialTypeName,
            this.colSerialTypeDelete});
            this.gridTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTypes.Location = new System.Drawing.Point(0, 0);
            this.gridTypes.Name = "gridTypes";
            this.gridTypes.ReadOnly = true;
            this.gridTypes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridTypes.Size = new System.Drawing.Size(308, 341);
            this.gridTypes.TabIndex = 0;
            this.gridTypes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTypes_CellClick);
            this.gridTypes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTypes_CellContentClick);
            this.gridTypes.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTypes_CellContentDoubleClick);
            this.gridTypes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTypes_CellDoubleClick);
            // 
            // colSerialTypeName
            // 
            this.colSerialTypeName.HeaderText = "Name";
            this.colSerialTypeName.Name = "colSerialTypeName";
            this.colSerialTypeName.ReadOnly = true;
            this.colSerialTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colSerialTypeName.Width = 200;
            // 
            // colSerialTypeDelete
            // 
            this.colSerialTypeDelete.HeaderText = "";
            this.colSerialTypeDelete.Name = "colSerialTypeDelete";
            this.colSerialTypeDelete.ReadOnly = true;
            this.colSerialTypeDelete.Width = 30;
            // 
            // splitDetails
            // 
            this.splitDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitDetails.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitDetails.Location = new System.Drawing.Point(0, 0);
            this.splitDetails.Name = "splitDetails";
            this.splitDetails.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitDetails.Panel1
            // 
            this.splitDetails.Panel1.Controls.Add(this.buttonAddType);
            this.splitDetails.Panel1.Controls.Add(this.buttonAddRange);
            this.splitDetails.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitDetails_Panel1_Paint);
            // 
            // splitDetails.Panel2
            // 
            this.splitDetails.Panel2.Controls.Add(this.gridBatches);
            this.splitDetails.Size = new System.Drawing.Size(613, 341);
            this.splitDetails.SplitterDistance = 109;
            this.splitDetails.TabIndex = 0;
            // 
            // buttonAddType
            // 
            this.buttonAddType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAddType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddType.ForeColor = System.Drawing.Color.White;
            this.buttonAddType.Location = new System.Drawing.Point(14, 78);
            this.buttonAddType.Name = "buttonAddType";
            this.buttonAddType.Size = new System.Drawing.Size(122, 28);
            this.buttonAddType.TabIndex = 1;
            this.buttonAddType.Text = "&Add Reference Type";
            this.buttonAddType.UseVisualStyleBackColor = false;
            this.buttonAddType.Click += new System.EventHandler(this.buttonAddType_Click);
            // 
            // buttonAddRange
            // 
            this.buttonAddRange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddRange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAddRange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddRange.ForeColor = System.Drawing.Color.White;
            this.buttonAddRange.Location = new System.Drawing.Point(142, 78);
            this.buttonAddRange.Name = "buttonAddRange";
            this.buttonAddRange.Size = new System.Drawing.Size(107, 28);
            this.buttonAddRange.TabIndex = 1;
            this.buttonAddRange.Text = "&Add Serial Range";
            this.buttonAddRange.UseVisualStyleBackColor = false;
            this.buttonAddRange.Click += new System.EventHandler(this.buttonAddRange_Click);
            // 
            // gridBatches
            // 
            this.gridBatches.AllowUserToAddRows = false;
            this.gridBatches.AllowUserToDeleteRows = false;
            this.gridBatches.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridBatches.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colBatchFrom,
            this.colBatchCostCenter,
            this.colBatchDateFrom,
            this.colBatchDateTo,
            this.colBatchFormat,
            this.colBatchDelete});
            this.gridBatches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridBatches.Location = new System.Drawing.Point(0, 0);
            this.gridBatches.Name = "gridBatches";
            this.gridBatches.ReadOnly = true;
            this.gridBatches.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridBatches.Size = new System.Drawing.Size(613, 228);
            this.gridBatches.TabIndex = 0;
            this.gridBatches.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridBatches_CellClick);
            this.gridBatches.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridBatches_CellDoubleClick);
            // 
            // colBatchFrom
            // 
            this.colBatchFrom.HeaderText = "Range";
            this.colBatchFrom.Name = "colBatchFrom";
            this.colBatchFrom.ReadOnly = true;
            this.colBatchFrom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colBatchCostCenter
            // 
            this.colBatchCostCenter.HeaderText = "Accounting Center";
            this.colBatchCostCenter.Name = "colBatchCostCenter";
            this.colBatchCostCenter.ReadOnly = true;
            this.colBatchCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colBatchDateFrom
            // 
            this.colBatchDateFrom.HeaderText = "Date From";
            this.colBatchDateFrom.Name = "colBatchDateFrom";
            this.colBatchDateFrom.ReadOnly = true;
            this.colBatchDateFrom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colBatchDateTo
            // 
            this.colBatchDateTo.HeaderText = "Date To";
            this.colBatchDateTo.Name = "colBatchDateTo";
            this.colBatchDateTo.ReadOnly = true;
            this.colBatchDateTo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colBatchFormat
            // 
            this.colBatchFormat.HeaderText = "Format";
            this.colBatchFormat.Name = "colBatchFormat";
            this.colBatchFormat.ReadOnly = true;
            this.colBatchFormat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colBatchDelete
            // 
            this.colBatchDelete.HeaderText = "";
            this.colBatchDelete.Name = "colBatchDelete";
            this.colBatchDelete.ReadOnly = true;
            this.colBatchDelete.Width = 30;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 200;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Range";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Accounting Center";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Date From";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Date To";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Format";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(939, 373);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(931, 347);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Typed References";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listBaches);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(931, 347);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Serial Batch";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonBatch
            // 
            this.buttonBatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonBatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBatch.ForeColor = System.Drawing.Color.White;
            this.buttonBatch.Location = new System.Drawing.Point(5, 13);
            this.buttonBatch.Name = "buttonBatch";
            this.buttonBatch.Size = new System.Drawing.Size(107, 28);
            this.buttonBatch.TabIndex = 2;
            this.buttonBatch.Text = "&Add Batch";
            this.buttonBatch.UseVisualStyleBackColor = false;
            this.buttonBatch.Click += new System.EventHandler(this.buttonBatch_Click);
            // 
            // listBaches
            // 
            this.listBaches.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader1});
            this.listBaches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBaches.FullRowSelect = true;
            this.listBaches.Location = new System.Drawing.Point(3, 50);
            this.listBaches.Name = "listBaches";
            this.listBaches.Size = new System.Drawing.Size(925, 294);
            this.listBaches.TabIndex = 3;
            this.listBaches.UseCompatibleStateImageBehavior = false;
            this.listBaches.View = System.Windows.Forms.View.Details;
            this.listBaches.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBaches_KeyDown);
            // 
            // columnName
            // 
            this.columnName.Text = "Batch ID";
            this.columnName.Width = 129;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Batch Name";
            this.columnHeader2.Width = 158;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "From";
            this.columnHeader3.Width = 205;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "To";
            this.columnHeader1.Width = 151;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonBatch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(925, 47);
            this.panel1.TabIndex = 4;
            // 
            // DocumentSerialSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(939, 373);
            this.Controls.Add(this.tabControl1);
            this.Name = "DocumentSerialSetting";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Document Reference Settings";
            this.Load += new System.EventHandler(this.DocumentSerialSetting_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTypes)).EndInit();
            this.splitDetails.Panel1.ResumeLayout(false);
            this.splitDetails.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitDetails)).EndInit();
            this.splitDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridBatches)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView gridTypes;
        private System.Windows.Forms.SplitContainer splitDetails;
        private System.Windows.Forms.DataGridView gridBatches;
        private System.Windows.Forms.Button buttonAddRange;
        private System.Windows.Forms.Button buttonAddType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSerialTypeName;
        private System.Windows.Forms.DataGridViewButtonColumn colSerialTypeDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchCostCenter;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchDateFrom;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchDateTo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBatchFormat;
        private System.Windows.Forms.DataGridViewButtonColumn colBatchDelete;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonBatch;
        private System.Windows.Forms.ListView listBaches;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Panel panel1;
    }
}