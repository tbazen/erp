﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DocumentSerialSetting : Form
    {
        DocumentSerialType _currentType = null;
        public DocumentSerialSetting()
        {
            InitializeComponent();
            reloadSerialTypes();
            loadSerialBatches();
        }

        private void reloadSerialTypes()
        {
            gridTypes.Rows.Clear();
            bool currentExists = false;
            foreach (DocumentSerialType t in AccountingClient.getAllDocumentSerialTypes())
            {
                int index = gridTypes.Rows.Add(t.name, "X");
                gridTypes.Rows[index].Tag = t;
                if (_currentType!=null && t.id == _currentType.id)
                    currentExists = true;
            }
            if (currentExists)
                reloadBatches(_currentType);
            else
                reloadBatches(null);
        }
        private void reloadBatches(DocumentSerialType type)
        {
            gridBatches.Rows.Clear();
            buttonAddRange.Enabled = type != null;
            _currentType = type;
            if (type == null)
                return;
            foreach (DocumentSerialBatch b in AccountingClient.getAllDocumentSerialBatches(type.id))
            {
                int index = gridBatches.Rows.Add(b.rangeText, AccountingClient.GetAccount<CostCenter>(b.costCenterID).CodeName
                , b.fromDate.ToString("MMM dd,yyyy")
                , b.timeUpperBound ? b.toDate.AddDays(-1).ToString("MMM dd,yyyy") : ""
                , b.displayFormat
                , "X"
                );
                gridBatches.Rows[index].Tag = b;
            }
        }


        private void buttonAddType_Click(object sender, EventArgs e)
        {
            DocumentSerialTypeEditor edit = new DocumentSerialTypeEditor();
            if (edit.ShowDialog(this) == DialogResult.OK)
                reloadSerialTypes();

        }

        private void gridTypes_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void gridTypes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= gridTypes.Rows.Count)
            {
                reloadBatches(null);
                return;
            }
            DocumentSerialType st = gridTypes.Rows[e.RowIndex].Tag as DocumentSerialType;
            if (st == null)
                return;
            if (e.ColumnIndex == colSerialTypeDelete.Index)
            {

                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete " + st.name))
                    return;
                try
                {
                    AccountingClient.deleteDocumentSerialType(st.id);
                    reloadSerialTypes();
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error deleting serial type", ex);
                }
            }
            else
            {
                if (_currentType==null || st.id != _currentType.id)
                    reloadBatches(st);
            }
        }

        private void gridTypes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void gridTypes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.RowIndex >= gridTypes.Rows.Count)
                return;
            DocumentSerialType st = gridTypes.Rows[e.RowIndex].Tag as DocumentSerialType;
            if (st == null)
                return;
            DocumentSerialTypeEditor edit = new DocumentSerialTypeEditor(st);
            if (edit.ShowDialog(this) == DialogResult.OK)
                reloadSerialTypes();
        }

        private void buttonAddRange_Click(object sender, EventArgs e)
        {
            if (_currentType == null)
                return;
            DocumentSerialBatchEditor edit = new DocumentSerialBatchEditor(_currentType);
            if (edit.ShowDialog(this) == DialogResult.OK)
                reloadBatches(_currentType);
        }

        private void DocumentSerialSetting_Load(object sender, EventArgs e)
        {

        }

        private void gridBatches_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (_currentType == null)
                return;
                
            if (e.RowIndex < 0 || e.RowIndex >= gridTypes.Rows.Count)
                return;
            DocumentSerialBatch st = gridBatches.Rows[e.RowIndex].Tag as DocumentSerialBatch;
            if (st == null)
                return;
            
            DocumentSerialBatchEditor edit = new DocumentSerialBatchEditor(_currentType, st);
            if (edit.ShowDialog(this) == DialogResult.OK)
                reloadBatches(_currentType);
        }

        private void gridBatches_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (_currentType == null)
                return;
            if (e.RowIndex < 0 || e.RowIndex >= gridTypes.Rows.Count)
                return;

            DocumentSerialBatch batch = gridBatches.Rows[e.RowIndex].Tag as DocumentSerialBatch;
            if (batch == null)
                return;
            if (e.ColumnIndex == colBatchDelete.Index)
            {

                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete " + batch.rangeText))
                    return;
                try
                {
                    AccountingClient.deleteDocumentSerialBatch(batch.id);
                    reloadBatches(_currentType);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error deleting serial batch", ex);
                }
            }

        }
        void loadSerialBatches()
        {
            listBaches.Items.Clear();
            foreach (SerialBatch b in AccountingClient.GetAllActiveSerialBatches())
            {
                ListViewItem li = new ListViewItem(b.id.ToString());
                li.SubItems.Add(b.note);
                li.SubItems.Add(b.serialFrom.ToString());
                li.SubItems.Add(b.serialTo.ToString());
                li.Tag = b;
                listBaches.Items.Add(li);
            }
        }
        private void splitDetails_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void listBaches_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (listBaches.SelectedItems.Count > 0)
                {
                    if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected serial batch?"))
                        return;
                    SerialBatch batch = listBaches.SelectedItems[0].Tag as SerialBatch;
                    try
                    {
                        AccountingClient.DeleteSerialBatch(batch.id);
                        loadSerialBatches();
                    }
                    catch(Exception ex)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                    }
                }
            }
        }

        private void buttonBatch_Click(object sender, EventArgs e)
        {
            SerialBatchEditor sbe = new SerialBatchEditor();
            if (sbe.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
            {
                try
                {
                    AccountingClient.CreateBatch(sbe.formData);
                    loadSerialBatches();
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
        }
    }
}
