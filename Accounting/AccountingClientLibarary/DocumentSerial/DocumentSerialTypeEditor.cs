﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DocumentSerialTypeEditor : Form
    {
        int _typeID;
        public DocumentSerialTypeEditor()
        {
            InitializeComponent();
            _typeID = -1;
        }
        public DocumentSerialTypeEditor(DocumentSerialType st)
            :this()
        {
            _typeID = st.id;
            textName.Text = st.name;
            textCode.Text = st.code;
            textDescription.Text = st.description;
            checkSerialized.Checked = st.serialType == DocumentSerializationMode.Serialized;
            textPrefix.Text = st.prefix;
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                DocumentSerialType st = new DocumentSerialType();
                st.id = _typeID;
                st.name = textName.Text;
                st.description = textDescription.Text;
                st.code = textCode.Text;
                st.serialType = checkSerialized.Checked ? DocumentSerializationMode.Serialized : DocumentSerializationMode.Manual;
                st.prefix = textPrefix.Text;
                if (st.id == -1)
                    st.id = AccountingClient.createDocumentSerialType(st);
                else
                    AccountingClient.updateDocumentSerialType(st);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error saving serial type", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

    }
}
