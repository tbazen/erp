using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.Accounting.Client
{
    public class DummyIFunction : IFunction
    {
        FunctionDocumentation m_doc;
        public DummyIFunction(FunctionDocumentation doc)
        {
            m_doc = doc;
        }


        #region IFunction Members

        public EData Evaluate(EData[] Pars)
        {
            return EData.Empty;
        }

        public string Name
        {
            get { return m_doc.Description; }
        }

        public int ParCount
        {
            get { return m_doc.NPars; }
        }

        public string Symbol
        {
            get { return m_doc.FunctionName; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion
    }
    public class ClientSymbolProvider:ISymbolProvider
    {
        Dictionary<string,FunctionDocumentation> m_docs;
        Dictionary<string, IFunction> m_funcs;
        private FunctionDocumentation[] functionDocumentation;
        public ClientSymbolProvider(FunctionDocumentation[] docs)
        {
            m_docs = new Dictionary<string, FunctionDocumentation>();
            m_funcs = new Dictionary<string, IFunction>();
            foreach (FunctionDocumentation doc in docs)
            {
                m_docs.Add(doc.FunctionName.ToUpper(), doc);
                m_funcs.Add(doc.FunctionName.ToUpper(), new DummyIFunction(doc));
            }
        }

        #region ISymbolProvider Members

        public FunctionDocumentation[] GetAvialableFunctions()
        {
            FunctionDocumentation[] ret = new FunctionDocumentation[m_docs.Count
                +CalcGlobal.DocumentedFunctions.Count];
            m_docs.Values.CopyTo(ret, 0);
            CalcGlobal.DocumentedFunctions.Values.CopyTo(ret, m_docs.Count);
            return ret;

        }

        public EData GetData(URLIden iden)
        {
            return EData.Empty;
        }

        public EData GetData(string symbol)
        {
            return EData.Empty;
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return new FunctionDocumentation(f);
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return GetDocumentation(iden.Element);
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            if (m_docs.ContainsKey(Symbol))
                return m_docs[Symbol];
            if (CalcGlobal.DocumentedFunctions.ContainsKey(Symbol))
                return (FunctionDocumentation)CalcGlobal.DocumentedFunctions[Symbol];
            if (CalcGlobal.Functions.ContainsKey(Symbol))
                return new FunctionDocumentation((IFunction)CalcGlobal.Functions[Symbol]);
            return null;
        }

        public IFunction GetFunction(URLIden iden)
        {
            return GetFunction(iden.Element);
        }

        public IFunction GetFunction(string symbol)
        {
            if (m_funcs.ContainsKey(symbol))
                return m_funcs[symbol];
            if (CalcGlobal.Functions.ContainsKey(symbol))
                return (IFunction)CalcGlobal.Functions[symbol];
            return null;
        }

        public bool SymbolDefined(string Name)
        {
            return false;
        }
        #endregion
    }
}
