namespace INTAPS.Accounting.Client
{
    partial class DateRangeFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFrom = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpTo = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.chkTo = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timeFrom = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboToTime = new System.Windows.Forms.ComboBox();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(65, 3);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(204, 20);
            this.dtpFrom.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpFrom.TabIndex = 0;
            this.dtpFrom.Changed += new System.EventHandler(this.etdtp_Changed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(15, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "From:";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(78, 3);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(204, 20);
            this.dtpTo.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpTo.TabIndex = 0;
            this.dtpTo.Changed += new System.EventHandler(this.etdtp_Changed);
            // 
            // chkTo
            // 
            this.chkTo.AutoSize = true;
            this.chkTo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.chkTo.Location = new System.Drawing.Point(17, 6);
            this.chkTo.Name = "chkTo";
            this.chkTo.Size = new System.Drawing.Size(45, 21);
            this.chkTo.TabIndex = 2;
            this.chkTo.Text = "To:";
            this.chkTo.UseVisualStyleBackColor = true;
            this.chkTo.CheckedChanged += new System.EventHandler(this.chkTo_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.timeFrom);
            this.panel1.Controls.Add(this.dtpFrom);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 58);
            this.panel1.TabIndex = 3;
            // 
            // timeFrom
            // 
            this.timeFrom.CustomFormat = "hh:mm:ss tt";
            this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeFrom.Location = new System.Drawing.Point(154, 29);
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.Size = new System.Drawing.Size(115, 23);
            this.timeFrom.TabIndex = 2;
            this.timeFrom.Value = new System.DateTime(2012, 7, 18, 0, 0, 0, 0);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboToTime);
            this.panel2.Controls.Add(this.timeTo);
            this.panel2.Controls.Add(this.dtpTo);
            this.panel2.Controls.Add(this.chkTo);
            this.panel2.Location = new System.Drawing.Point(292, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(298, 58);
            this.panel2.TabIndex = 4;
            // 
            // comboToTime
            // 
            this.comboToTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboToTime.FormattingEnabled = true;
            this.comboToTime.Items.AddRange(new object[] {
            "12:00:00 AM (Start of day)",
            "11:59:15 PM (End of day)",
            "11:59:45 PM (After closing time)"});
            this.comboToTime.Location = new System.Drawing.Point(17, 28);
            this.comboToTime.Name = "comboToTime";
            this.comboToTime.Size = new System.Drawing.Size(135, 23);
            this.comboToTime.TabIndex = 3;
            this.comboToTime.SelectedIndexChanged += new System.EventHandler(this.comboToTime_SelectedIndexChanged);
            // 
            // timeTo
            // 
            this.timeTo.CustomFormat = "hh:mm:ss tt";
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo.Location = new System.Drawing.Point(168, 29);
            this.timeTo.Name = "timeTo";
            this.timeTo.Size = new System.Drawing.Size(114, 23);
            this.timeTo.TabIndex = 2;
            this.timeTo.Value = new System.DateTime(2012, 7, 18, 0, 0, 0, 0);
            this.timeTo.ValueChanged += new System.EventHandler(this.timeTo_ValueChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.panel2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(605, 61);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // DateRangeFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "DateRangeFilter";
            this.Size = new System.Drawing.Size(605, 61);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private INTAPS.Ethiopic.ETDateTimeDropDown dtpFrom;
        private System.Windows.Forms.Label label1;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpTo;
        private System.Windows.Forms.CheckBox chkTo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.DateTimePicker timeFrom;
        private System.Windows.Forms.DateTimePicker timeTo;
        private System.Windows.Forms.ComboBox comboToTime;
    }
}
