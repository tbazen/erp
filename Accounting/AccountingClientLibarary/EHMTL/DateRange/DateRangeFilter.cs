using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class DateRangeFilter : UserControl
    {
        INTAPS.UI.EtGrPickerPair m_pairFrom,m_pairTo;
        public event EventHandler FilterUpdated;
        
        public DateRangeFilter()
        {
            InitializeComponent();
            chkTo_CheckedChanged(null, null);
            m_pairFrom= new INTAPS.UI.EtGrPickerPair(dtpFrom, timeFrom);
            m_pairTo= new INTAPS.UI.EtGrPickerPair(dtpTo, timeTo);
            m_pairFrom.SetValue(DateTime.Today);
            DateTime date = DateTime.Now;
            m_pairTo.SetValue(new DateTime(date.Year, date.Month, date.Day, 23, 59, 15));            
        }

        void OnFilterUpdate()
        {
            if (FilterUpdated != null)
                FilterUpdated(this, null);
        }

        private void etdtp_Changed(object sender, EventArgs e)
        {
            OnFilterUpdate();
        }

        public DateTime filterDateFrom
        {
            get
            {
                return  m_pairFrom.GetValue();
            }
            set
            {
                m_pairFrom.SetValue(value);
            }
        }

        public DateTime filterDateTo
        {
            get
            {
                if (chkTo.Checked)
                    return m_pairTo.GetValue();
                DateTime date=dtpFrom.Value.Date;
                return new DateTime(date.Year,date.Month,date.Day,timeTo.Value.Hour,timeTo.Value.Minute,timeTo.Value.Second);
            }
            set
            {
                chkTo.Checked = true;
                m_pairTo.SetValue(value);
            }
        }

        private void chkTo_CheckedChanged(object sender, EventArgs e)
        {
            dtpTo.Enabled = chkTo.Checked;
        }


        private void comboToTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime date=m_pairTo.GetValue();
            switch (comboToTime.SelectedIndex)
            {
                case 0: m_pairTo.SetValue(new DateTime(date.Year, date.Month, date.Day, 0, 0, 0));
                    break;
                case 1: m_pairTo.SetValue(new DateTime(date.Year, date.Month, date.Day, 23, 59, 15));
                    break;
                case 2: m_pairTo.SetValue(new DateTime(date.Year, date.Month, date.Day, 23, 59, 45));
                    break;
            }
        }

        private void timeTo_ValueChanged(object sender, EventArgs e)
        {
            comboToTime.SelectedIndex = -1;
        }
    }
}
