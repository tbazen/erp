using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class RFDateRangeParameter: EHTMLForm
    {
        protected DateRangeFilter m_filter;
        public RFDateRangeParameter()
        {
            m_filter = new DateRangeFilter();
            this.Controls.Add(m_filter);
            m_filter.BringToFront();
            m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }
        public override object[] GetParamters()
        {
            DateTime date1 = m_filter.filterDateFrom;
            DateTime date2 = m_filter.filterDateTo;
            return new object[] { date1,date2};
        }
    }
    
    public class RCHDateRangeParameter : GenericReportClientHandler<RFDateRangeParameter>
    {
        public RCHDateRangeParameter(ReportType rt)
            : base(rt)
        {
        }
    }
    public class RCHDateRangeParameterFinance : RCHDateRangeParameter
    {
        public RCHDateRangeParameterFinance(ReportType rt)
            : base(rt)
        {
        }
    }


}
