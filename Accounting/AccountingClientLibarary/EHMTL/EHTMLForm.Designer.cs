namespace INTAPS.Accounting.Client
{
    partial class EHTMLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EHTMLForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.pageHTML = new System.Windows.Forms.TabPage();
            this.controlBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.pageFormula = new System.Windows.Forms.TabPage();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.dataGridViewFormula = new INTAPS.UI.EDataGridView();
            this.colVariable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFormula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buttonAdd = new System.Windows.Forms.ToolStripButton();
            this.buttonUp = new System.Windows.Forms.ToolStripButton();
            this.buttonDown = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonEvaluateSub = new System.Windows.Forms.ToolStripButton();
            this.buttonEvaluate = new System.Windows.Forms.ToolStripButton();
            this.buttonSave = new System.Windows.Forms.ToolStripButton();
            this.buttonImport = new System.Windows.Forms.ToolStripButton();
            this.buttonExport = new System.Windows.Forms.ToolStripButton();
            this.formulaEditor = new INTAPS.Evaluator.FormulaEditor();
            this.pageGeneral = new System.Windows.Forms.TabPage();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textStylesheet = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.browserController = new INTAPS.UI.HTML.BowserController();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.buttonCalculate = new System.Windows.Forms.ToolStripButton();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.labelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.progProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.tabControl.SuspendLayout();
            this.pageHTML.SuspendLayout();
            this.pageFormula.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFormula)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            this.browserController.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.pageHTML);
            this.tabControl.Controls.Add(this.pageFormula);
            this.tabControl.Controls.Add(this.pageGeneral);
            this.tabControl.Location = new System.Drawing.Point(12, 38);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(619, 398);
            this.tabControl.TabIndex = 0;
            // 
            // pageHTML
            // 
            this.pageHTML.Controls.Add(this.controlBrowser);
            this.pageHTML.Location = new System.Drawing.Point(4, 22);
            this.pageHTML.Name = "pageHTML";
            this.pageHTML.Padding = new System.Windows.Forms.Padding(3);
            this.pageHTML.Size = new System.Drawing.Size(611, 372);
            this.pageHTML.TabIndex = 0;
            this.pageHTML.Text = "HTML";
            this.pageHTML.UseVisualStyleBackColor = true;
            // 
            // controlBrowser
            // 
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(3, 3);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(605, 366);
            this.controlBrowser.StyleSheetFile = "berp.css";
            this.controlBrowser.TabIndex = 0;
            // 
            // pageFormula
            // 
            this.pageFormula.Controls.Add(this.splitContainer);
            this.pageFormula.Location = new System.Drawing.Point(4, 22);
            this.pageFormula.Name = "pageFormula";
            this.pageFormula.Padding = new System.Windows.Forms.Padding(3);
            this.pageFormula.Size = new System.Drawing.Size(611, 372);
            this.pageFormula.TabIndex = 1;
            this.pageFormula.Text = "Report Formula";
            this.pageFormula.UseVisualStyleBackColor = true;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(3, 3);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.dataGridViewFormula);
            this.splitContainer.Panel1.Controls.Add(this.toolStrip1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.formulaEditor);
            this.splitContainer.Size = new System.Drawing.Size(605, 366);
            this.splitContainer.SplitterDistance = 439;
            this.splitContainer.TabIndex = 1;
            // 
            // dataGridViewFormula
            // 
            this.dataGridViewFormula.AllowUserToAddRows = false;
            this.dataGridViewFormula.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFormula.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colVariable,
            this.colFormula});
            this.dataGridViewFormula.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFormula.HorizontalEnterNavigationMode = true;
            this.dataGridViewFormula.Location = new System.Drawing.Point(0, 25);
            this.dataGridViewFormula.Name = "dataGridViewFormula";
            this.dataGridViewFormula.Size = new System.Drawing.Size(439, 341);
            this.dataGridViewFormula.SuppessEnterKey = false;
            this.dataGridViewFormula.TabIndex = 0;
            this.dataGridViewFormula.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFormula_CellEndEdit);
            this.dataGridViewFormula.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewFormula_RowEnter);
            this.dataGridViewFormula.SelectionChanged += new System.EventHandler(this.dataGridViewFormula_SelectionChanged);
            // 
            // colVariable
            // 
            this.colVariable.HeaderText = "Variable";
            this.colVariable.Name = "colVariable";
            this.colVariable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colFormula
            // 
            this.colFormula.HeaderText = "Formula";
            this.colFormula.Name = "colFormula";
            this.colFormula.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFormula.Width = 300;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonAdd,
            this.buttonUp,
            this.buttonDown,
            this.toolStripSeparator3,
            this.buttonEvaluateSub,
            this.buttonEvaluate,
            this.buttonSave,
            this.buttonImport,
            this.buttonExport});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(439, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buttonAdd
            // 
            this.buttonAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(23, 22);
            this.buttonAdd.Text = "+";
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonUp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(23, 22);
            this.buttonUp.Text = "^";
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(23, 22);
            this.buttonDown.Text = "V";
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // buttonEvaluateSub
            // 
            this.buttonEvaluateSub.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonEvaluateSub.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEvaluateSub.Name = "buttonEvaluateSub";
            this.buttonEvaluateSub.Size = new System.Drawing.Size(82, 22);
            this.buttonEvaluateSub.Text = "Evaluate(sub)";
            // 
            // buttonEvaluate
            // 
            this.buttonEvaluate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonEvaluate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEvaluate.Name = "buttonEvaluate";
            this.buttonEvaluate.Size = new System.Drawing.Size(55, 22);
            this.buttonEvaluate.Text = "Evaluate";
            this.buttonEvaluate.Click += new System.EventHandler(this.buttonEvaluate_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonSave.Image = ((System.Drawing.Image)(resources.GetObject("buttonSave.Image")));
            this.buttonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(35, 22);
            this.buttonSave.Text = "Save";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonImport.Image = ((System.Drawing.Image)(resources.GetObject("buttonImport.Image")));
            this.buttonImport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(47, 22);
            this.buttonImport.Text = "Import";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click_1);
            // 
            // buttonExport
            // 
            this.buttonExport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonExport.Image = ((System.Drawing.Image)(resources.GetObject("buttonExport.Image")));
            this.buttonExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(44, 22);
            this.buttonExport.Text = "Export";
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click_1);
            // 
            // formulaEditor
            // 
            this.formulaEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formulaEditor.Font = new System.Drawing.Font("Batang", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formulaEditor.Formula = "";
            this.formulaEditor.Location = new System.Drawing.Point(0, 0);
            this.formulaEditor.MultiLine = true;
            this.formulaEditor.Name = "formulaEditor";
            this.formulaEditor.Size = new System.Drawing.Size(162, 366);
            this.formulaEditor.TabIndex = 0;
            this.formulaEditor.Text = "";
            this.formulaEditor.TextChanged += new System.EventHandler(this.formulaEditor_TextChanged);
            // 
            // pageGeneral
            // 
            this.pageGeneral.BackColor = System.Drawing.Color.Gray;
            this.pageGeneral.Controls.Add(this.textDescription);
            this.pageGeneral.Controls.Add(this.label2);
            this.pageGeneral.Controls.Add(this.textStylesheet);
            this.pageGeneral.Controls.Add(this.label3);
            this.pageGeneral.Controls.Add(this.textCode);
            this.pageGeneral.Controls.Add(this.label4);
            this.pageGeneral.Controls.Add(this.textName);
            this.pageGeneral.Controls.Add(this.label1);
            this.pageGeneral.Location = new System.Drawing.Point(4, 22);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(611, 372);
            this.pageGeneral.TabIndex = 2;
            this.pageGeneral.Text = "Description";
            // 
            // textDescription
            // 
            this.textDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textDescription.Location = new System.Drawing.Point(106, 112);
            this.textDescription.Multiline = true;
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(485, 243);
            this.textDescription.TabIndex = 1;
            this.textDescription.TextChanged += new System.EventHandler(this.textDescription_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(8, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Description:";
            // 
            // textStylesheet
            // 
            this.textStylesheet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textStylesheet.Location = new System.Drawing.Point(106, 51);
            this.textStylesheet.Name = "textStylesheet";
            this.textStylesheet.Size = new System.Drawing.Size(485, 20);
            this.textStylesheet.TabIndex = 1;
            this.textStylesheet.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(8, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Stylesheet:";
            // 
            // textCode
            // 
            this.textCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textCode.Location = new System.Drawing.Point(106, 77);
            this.textCode.Name = "textCode";
            this.textCode.Size = new System.Drawing.Size(485, 20);
            this.textCode.TabIndex = 1;
            this.textCode.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(8, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Code:";
            // 
            // textName
            // 
            this.textName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textName.Location = new System.Drawing.Point(106, 16);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(485, 20);
            this.textName.TabIndex = 1;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // browserController
            // 
            this.browserController.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.buttonCalculate});
            this.browserController.Location = new System.Drawing.Point(0, 0);
            this.browserController.Name = "browserController";
            this.browserController.ShowBackForward = false;
            this.browserController.ShowRefresh = false;
            this.browserController.Size = new System.Drawing.Size(643, 25);
            this.browserController.TabIndex = 1;
            this.browserController.Text = "bowserController1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonCalculate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(96, 22);
            this.buttonCalculate.Text = "Generate Report";
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Variable";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Formula";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 300;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelStatus,
            this.progProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 432);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(643, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = false;
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(200, 17);
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // progProgress
            // 
            this.progProgress.Name = "progProgress";
            this.progProgress.Size = new System.Drawing.Size(400, 16);
            // 
            // EHTMLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 454);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.browserController);
            this.Controls.Add(this.tabControl);
            this.Name = "EHTMLForm";
            this.ShowIcon = false;
            this.Text = "EHTML Form";
            this.Controls.SetChildIndex(this.tabControl, 0);
            this.Controls.SetChildIndex(this.browserController, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.tabControl.ResumeLayout(false);
            this.pageHTML.ResumeLayout(false);
            this.pageFormula.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFormula)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.pageGeneral.ResumeLayout(false);
            this.pageGeneral.PerformLayout();
            this.browserController.ResumeLayout(false);
            this.browserController.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage pageHTML;
        private INTAPS.UI.HTML.ControlBrowser controlBrowser;
        private System.Windows.Forms.TabPage pageFormula;
        private INTAPS.UI.HTML.BowserController browserController;
        private System.Windows.Forms.ToolStripButton buttonCalculate;
        private INTAPS.UI.EDataGridView dataGridViewFormula;
        private System.Windows.Forms.SplitContainer splitContainer;
        private INTAPS.Evaluator.FormulaEditor formulaEditor;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buttonAdd;
        private System.Windows.Forms.TabPage pageGeneral;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVariable;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFormula;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton buttonUp;
        private System.Windows.Forms.ToolStripButton buttonDown;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton buttonEvaluateSub;
        private System.Windows.Forms.ToolStripButton buttonEvaluate;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel labelStatus;
        private System.Windows.Forms.ToolStripProgressBar progProgress;
        private System.Windows.Forms.TextBox textStylesheet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripButton buttonSave;
        private System.Windows.Forms.ToolStripButton buttonImport;
        private System.Windows.Forms.ToolStripButton buttonExport;
        private System.Windows.Forms.TextBox textCode;
        private System.Windows.Forms.Label label4;

    }
}