using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Evaluator;
using INTAPS.UI.Windows;

namespace INTAPS.Accounting.Client
{
    public partial class EHTMLForm : AsyncForm, INTAPS.UI.HTML.IHTMLBuilder, INTAPS.UI.HTML.IHTMLBuilderWithHeader
    {
        protected ReportDefination m_def;
        bool m_modfied = false;

        Symbolic m_fePar = new Symbolic();
        public EHTMLForm()
        {
            InitializeComponent();
            browserController.SetBrowser(controlBrowser);
            formulaEditor.m_Parent = m_fePar;
        }
        public virtual void SetDef(ReportDefination def)
        {
            m_def = def;
            Populate(m_def.formula);
            textName.Text = def.name;
            textStylesheet.Text = def.styleSheet;
            this.Text = "Report " + def.name;
            textCode.Text = def.code == null ? "" : def.code;
            textDescription.Text = def.description;
            m_fePar.SetSymbolProvider(new ClientSymbolProvider(AccountingClient.GetFunctionDocumentations(m_def.reportTypeID)));
            m_modfied = false;
        }
        void OnModified()
        {
            m_modfied = true;
        }
        private void Populate(EHTMLData data)
        {
            dataGridViewFormula.Rows.Clear();
            if (data == null)
                dataGridViewFormula.Rows.Add("mainFormula", "");
            else
            {
                dataGridViewFormula.Rows.Add("mainFormula", data.mainFormula);
                for (int i = 0; i < data.variables.Length; i++)
                {
                    dataGridViewFormula.Rows.Add(data.variables[i], data.expressions[i]);
                }
            }
            dataGridViewFormula[0, 0].ReadOnly = true;
        }
        EHTMLData ExtractData()
        {
            if (dataGridViewFormula.Rows.Count == 0)
                return new EHTMLData(0);
            int n = dataGridViewFormula.Rows.Count - 1;
            EHTMLData ret = new EHTMLData(n);
            for (int i = 0; i < n; i++)
            {
                ret.variables[i] = dataGridViewFormula[0, i + 1].Value.ToString();
                ret.expressions[i] = dataGridViewFormula[1, i + 1].Value.ToString();
            }
            ret.mainFormula = dataGridViewFormula[1, 0].Value.ToString();
            if (!ret.Valid())
                throw new Exception("Invalid formula set.");
            return ret;
        }
        protected void SetTabDock(DockStyle style)
        {
            tabControl.BringToFront();
            tabControl.Dock = style;
        }
        protected virtual string Evaluate(object[] pars, out string headerItems)
        {
            return AccountingClient.EvaluateEHTML(m_def.reportTypeID, m_def.formula, pars,out headerItems);
        }
        protected virtual void SaveData()
        {
            m_def.styleSheet = textStylesheet.Text;
            m_def.name = textName.Text;
            m_def.description = textDescription.Text;
            m_def.code = textCode.Text.Trim();
            m_def.id = AccountingClient.SaveEHTMLData(m_def);
            m_modfied = false;
        }
        protected void Update(EHTMLData data)
        {
            m_def.formula= data;
        }
        protected virtual void LoadData()
        {
            
        }
        class ReportCalculatorProgress: INTAPS.UI.Windows.ServerDataDownloader
        {
            EHTMLForm parent;
            object[] _pars;
            public Exception error = null;
            public string html;
            public string headerItems;
            public ReportCalculatorProgress(EHTMLForm par,object [] pars)
            {
                this.parent = par;
                _pars = pars;
            }
            public override void RunDownloader()
            {
                try
                {
                    html = parent.Evaluate(_pars,out headerItems);
                }
                catch (Exception ex)
                {
                    error = ex;
                }
            }

            public override string Description
            {
                get { return "Calculating..please wait";}
            }
        }
        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            
            Update(ExtractData());
            base.WaitFor(new ReportCalculatorProgress(this, GetParamters()));
            tabControl.SelectedIndex = 0;
            
        }
        protected override void CheckProgress()
        {
            string msg;
            double prog = AccountingClient.GetProccessProgress(out msg);
            labelStatus.Text = msg;
            progProgress.Value = (int)(100 * prog);
        }
        string _reportHtml = "";
        string _headerItems = null;
        protected override void WaitIsOver(ServerDataDownloader d)
        {
            ReportCalculatorProgress prog = (ReportCalculatorProgress)d;
            if (prog.error == null)
            {
                controlBrowser.StyleSheetFile = textStylesheet.Text;
                _reportHtml = prog.html;
                _headerItems = prog.headerItems;
                controlBrowser.LoadControl(this,prog.headerItems);
            }
            else
                UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(prog.error.Message);
            labelStatus.Text = "";
            progProgress.Value = 0;
            base.WaitIsOver(d);
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            dataGridViewFormula.Rows.Add();
            OnModified();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            
        }
        private void dataGridViewFormula_SelectionChanged(object sender, EventArgs e)
        {
            int index=dataGridViewFormula.CurrentRow == null || dataGridViewFormula.CurrentRow.Index == -1?-1:dataGridViewFormula.CurrentRow.Index;
            if (index==-1)
            {
                buttonUp.Enabled = false;
                buttonDown.Enabled = false;
                buttonEvaluate.Enabled = false;
                buttonEvaluateSub.Enabled = false;
            }
            else
            {
                buttonUp.Enabled = index>0;
                buttonDown.Enabled = index<dataGridViewFormula.Rows.Count-1;
                buttonEvaluate.Enabled = true;
                buttonEvaluateSub.Enabled = true;
            }

        }
        bool m_IgnoreEvent = false;
        private void dataGridViewFormula_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (m_IgnoreEvent) return;
            m_IgnoreEvent = true;
            try
            {
                if (e.RowIndex > -1)
                    formulaEditor.Formula = dataGridViewFormula[1, e.RowIndex].Value as string;
            }
            finally
            {
                m_IgnoreEvent = false;
            }
        }
        private void formulaEditor_TextChanged(object sender, EventArgs e)
        {
            if (m_IgnoreEvent) return;
            m_IgnoreEvent = true;
            try
            {
                int cur = dataGridViewFormula.CurrentRow.Index;
                if (cur > -1)
                    dataGridViewFormula[1, cur].Value = formulaEditor.Formula;
            }
            finally
            {
                m_IgnoreEvent = false;
                OnModified();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            INTAPS.UI.UIStateHandler.LoadFormState(INTAPS.UI.UIFormApplicationBase.CurrentAppliation.GetSetting("Accounting.ReportForm", ""), this);
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (m_modfied)
            {
                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to close with out saving the changes?"))
                {
                    e.Cancel = true;
                    return;
                }
            }
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.SetSetting( "Accounting.ReportForm",INTAPS.UI.UIStateHandler.GetFormState(this));
            base.OnClosing(e);
        }
        public virtual object[] GetParamters()
        {
            return new object[0];
        }
        private void dataGridViewFormula_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            OnModified();
        }
        private void dataGridViewFormula_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            OnModified();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            
        }

        private void textDescription_TextChanged(object sender, EventArgs e)
        {
            OnModified();
        }

        private void textName_TextChanged(object sender, EventArgs e)
        {
            OnModified();
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            
            int i=dataGridViewFormula.CurrentRow.Index;
            if (i < 1)
                return;
            DataGridViewRow row = dataGridViewFormula.Rows[i];
            dataGridViewFormula.Rows.RemoveAt(i);
            dataGridViewFormula.Rows.Insert(i - 1, row);
            dataGridViewFormula.CurrentCell = row.Cells[0];
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            int i = dataGridViewFormula.CurrentRow.Index;
            if (i >= dataGridViewFormula.Rows.Count-1)
                return;
            
            DataGridViewRow row = dataGridViewFormula.Rows[i];
            dataGridViewFormula.Rows.RemoveAt(i);
            dataGridViewFormula.Rows.Insert(i + 1, row);
            dataGridViewFormula.CurrentCell = row.Cells[0];
        }

        private void buttonEvaluate_Click(object sender, EventArgs e)
        {
            try
            {
                Update(ExtractData());
                string sym = ((string)dataGridViewFormula.CurrentRow.Cells[0].Value).ToUpper();
                EData dat = AccountingClient.EvaluateEHTML(m_def.reportTypeID, ExtractData(), GetParamters(), sym);
                controlBrowser.LoadTextPage("Report", 
                    "<h3>"+dat.Type.ToString()+"</h3>"
                    +System.Web.HttpUtility.HtmlEncode(dat.ToString())
                    );
                tabControl.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error generating report.", ex);
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Update(ExtractData());
                SaveData();
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserInformation("Save succesfull!");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error saving report.", ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonExport_Click_1(object sender, EventArgs e)
        {
            try
            {
                Update(ExtractData());
                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "*.reportdef|*.reportdef";
                d.FileName = m_def.name + ".reportdef";
                m_def.styleSheet = textStylesheet.Text;
                m_def.description = textDescription.Text;
                m_def.name = textName.Text;
                if (d.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
                {
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ReportDefination));
                    System.IO.StreamWriter sw = System.IO.File.CreateText(d.FileName);
                    s.Serialize(sw, m_def);
                    sw.Close();
                }

            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error exporting report.", ex);
            }
        }

        private void buttonImport_Click_1(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog d = new OpenFileDialog();
                d.Filter = "*.reportdef|*.reportdef";
                if (d.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) == DialogResult.OK)
                {
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ReportDefination));
                    System.IO.StreamReader sreader = System.IO.File.OpenText(d.FileName);
                    ReportDefination def = s.Deserialize(sreader) as ReportDefination;
                    def.id = m_def.id;
                    def.categoryID = m_def.categoryID;
                    def.reportTypeID = m_def.reportTypeID;
                    sreader.Close();
                    SetDef(def);
                    OnModified();
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error importing report.", ex);
            }
        }

        public void Build()
        {
            
        }

        public string GetOuterHtml()
        {
            return _reportHtml;
        }

        public virtual bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return false;
        }

        public void SetHost(UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Report"; }
        }

        public string getHeaderItems()
        {
            return _headerItems;
        }
    }
}