using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class GenericReportClientHandler<FormType>:IReportClientHandler
        where FormType:EHTMLForm,new()
    {
        ReportType m_type;
        public GenericReportClientHandler(ReportType type)
        {
            m_type = type;
        }
        #region IReportClientHandler Members

        public System.Windows.Forms.Form LoadReport(ReportDefination def)
        {
            FormType ft = new FormType();
            ft.SetDef(def);
            return ft;
        }

        public System.Windows.Forms.Form LoadCreateForm(int categoryID)
        {
            ReportDefination def = new ReportDefination(-1, m_type.id,categoryID,"", m_type.name,m_type.description);
            FormType ft = new FormType();
            ft.SetDef(def);
            return ft;
        }

        public object[] GetParameters(Form f)
        {
            return ((EHTMLForm)f).GetParamters();
        }

        #endregion
    }
    
}
