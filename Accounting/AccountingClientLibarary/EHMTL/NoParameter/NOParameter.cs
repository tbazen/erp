using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class RFNoParameter:EHTMLForm
    {
        public RFNoParameter()
        {
            base.SetTabDock(DockStyle.Fill);
        }
    }
    public class RCHNoParameter : GenericReportClientHandler<RFNoParameter>
    {
        public RCHNoParameter(ReportType rt)
            : base(rt)
        {
        }
    }
    public class RCHNoParameterFinance : RCHNoParameter
    {
        public RCHNoParameterFinance(ReportType rt)
            : base(rt)
        {
        }
    }
}
