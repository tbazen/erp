namespace INTAPS.Accounting.Client
{
    partial class ReportManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportManager));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.treeView = new System.Windows.Forms.TreeView();
            this.listView = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.itemAddCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.itemDeleteCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.itemRenameCateogry = new System.Windows.Forms.ToolStripMenuItem();
            this.toolList = new System.Windows.Forms.ToolStrip();
            this.buttonDelete = new System.Windows.Forms.ToolStripButton();
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.contextTree.SuspendLayout();
            this.toolList.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.treeView);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.listView);
            this.splitContainer.Panel2.Controls.Add(this.toolList);
            this.splitContainer.Size = new System.Drawing.Size(593, 412);
            this.splitContainer.SplitterDistance = 230;
            this.splitContainer.TabIndex = 0;
            // 
            // treeView
            // 
            this.treeView.AllowDrop = true;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(230, 412);
            this.treeView.TabIndex = 0;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView_DragDrop);
            this.treeView.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView_DragEnter);
            this.treeView.DragOver += new System.Windows.Forms.DragEventHandler(this.treeView_DragOver);
            this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
            // 
            // listView
            // 
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colCode,
            this.colType});
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.Location = new System.Drawing.Point(0, 25);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(359, 387);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.SelectedIndexChanged += new System.EventHandler(this.listView_SelectedIndexChanged);
            this.listView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView_MouseDown);
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 184;
            // 
            // colCode
            // 
            this.colCode.Text = "Code";
            this.colCode.Width = 111;
            // 
            // contextTree
            // 
            this.contextTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemAddCategory,
            this.itemDeleteCategory,
            this.itemRenameCateogry});
            this.contextTree.Name = "contextTree";
            this.contextTree.Size = new System.Drawing.Size(148, 70);
            this.contextTree.Opening += new System.ComponentModel.CancelEventHandler(this.contextTree_Opening);
            // 
            // itemAddCategory
            // 
            this.itemAddCategory.Name = "itemAddCategory";
            this.itemAddCategory.Size = new System.Drawing.Size(147, 22);
            this.itemAddCategory.Text = "Add Category";
            this.itemAddCategory.Click += new System.EventHandler(this.itemAddCategory_Click);
            // 
            // itemDeleteCategory
            // 
            this.itemDeleteCategory.Name = "itemDeleteCategory";
            this.itemDeleteCategory.Size = new System.Drawing.Size(147, 22);
            this.itemDeleteCategory.Text = "Delete";
            this.itemDeleteCategory.Click += new System.EventHandler(this.itemDeleteCategory_Click);
            // 
            // itemRenameCateogry
            // 
            this.itemRenameCateogry.Name = "itemRenameCateogry";
            this.itemRenameCateogry.Size = new System.Drawing.Size(147, 22);
            this.itemRenameCateogry.Text = "Rename";
            this.itemRenameCateogry.Click += new System.EventHandler(this.itemRenameCateogry_Click);
            // 
            // toolList
            // 
            this.toolList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonDelete});
            this.toolList.Location = new System.Drawing.Point(0, 0);
            this.toolList.Name = "toolList";
            this.toolList.Size = new System.Drawing.Size(359, 25);
            this.toolList.TabIndex = 1;
            this.toolList.Text = "toolStrip1";
            // 
            // buttonDelete
            // 
            this.buttonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonDelete.Enabled = false;
            this.buttonDelete.Image = ((System.Drawing.Image)(resources.GetObject("buttonDelete.Image")));
            this.buttonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(44, 22);
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 78;
            // 
            // ReportManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 412);
            this.Controls.Add(this.splitContainer);
            this.Name = "ReportManager";
            this.ShowIcon = false;
            this.Text = "Report Manager";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.contextTree.ResumeLayout(false);
            this.toolList.ResumeLayout(false);
            this.toolList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colCode;
        private System.Windows.Forms.ContextMenuStrip contextTree;
        private System.Windows.Forms.ToolStripMenuItem itemAddCategory;
        private System.Windows.Forms.ToolStripMenuItem itemDeleteCategory;
        private System.Windows.Forms.ToolStripMenuItem itemRenameCateogry;
        private System.Windows.Forms.ToolStrip toolList;
        private System.Windows.Forms.ToolStripButton buttonDelete;
        private System.Windows.Forms.ColumnHeader colType;
    }
}