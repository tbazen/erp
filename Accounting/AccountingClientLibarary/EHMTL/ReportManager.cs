using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS;
namespace INTAPS.Accounting.Client
{
    public partial class ReportManager: Form
    {
        public ReportManagerSelector selector;
        bool m_changed = false;
        public bool Changed
        {
            get
            {
                return m_changed;
            }
        }
        public void OnChanged()
        {
            m_changed = true;
        }

        public ReportManager()
        {
            InitializeComponent();
            selector = new ReportManagerSelector(this);
        }
        class NodeList
        {
            public ReportCategory category;
            public TreeNodeCollection nodes;
            public List<ListViewItem> items;
            public NodeList(ReportCategory cat, TreeNodeCollection n)
            {
                category = cat;
                nodes = n;
                items = new List<ListViewItem>();
            }
        }
        public class ReportManagerSelector: ReportSelectorBase, IReportSelector
        {
            ReportManager parent;
            NodeList rootNodeList;
            public ReportManagerSelector(ReportManager parent)
            {
                this.parent = parent;
                rootNodeList = new NodeList(new ReportCategory() { id = -1, name = "Root Category", pid = -1 }, parent.treeView.Nodes);
            }
            #region IReportSelector Members

            public object CreateCategoryItem(object parentItem, ReportCategory rt)
            {
                
                TreeNode n = new TreeNode(rt.name);
                NodeList nl = new NodeList(rt, n.Nodes);
                n.Tag = nl;
                if (parentItem == null)
                {
                    parent.treeView.Nodes.Add(n);
                }
                else
                {
                    ((NodeList)parentItem).nodes.Add(n);
                }
                return nl;

            }

            public object CreateReportItem(object parentItem, ReportDefination rd, bool enabled)
            {
                ListViewItem li = new ListViewItem(rd.name);
                li.SubItems.Add(rd.code);
                li.Tag = rd;
                if (parentItem == null)
                {
                    rootNodeList.items.Add(li);
                }
                else
                {
                    ((NodeList)parentItem).items.Add(li);
                }
                return li;
            }

            #endregion
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            listView.Items.Clear();
            if (treeView.SelectedNode == null)
                return;
            foreach (ListViewItem li in ((NodeList)treeView.SelectedNode.Tag).items)
                listView.Items.Add(li);
        }

        private void contextTree_Opening(object sender, CancelEventArgs e)
        {
            bool selected = treeView.SelectedNode != null;
            itemDeleteCategory.Enabled = selected;
            itemRenameCateogry.Enabled = selected;
            if (selected)
            {
                itemAddCategory.Tag = treeView.SelectedNode.Tag;
                itemAddCategory.Text = "Add Child Category";
            }
            else
            {
                itemAddCategory.Tag = null;
                itemAddCategory.Text = "Add Root Category";
            }
        }

        private void itemAddCategory_Click(object sender, EventArgs e)
        {
            INTAPS.UI.Windows.InputForm input = new INTAPS.UI.Windows.InputForm();
            if (input.Show("Report Manager", "Enter report category", "") != DialogResult.OK)
                return;
            if (string.IsNullOrEmpty(input.InputString))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter valid category.");
                return;
            }
            NodeList nl=itemAddCategory.Tag as NodeList;
            ReportCategory cat=new ReportCategory();
            cat.pid=nl==null?-1:nl.category.id;
            cat.name = input.InputString;
            try
            {
                cat.id = INTAPS.Accounting.Client.AccountingClient.SaveReportCategory(cat);
                selector.CreateCategoryItem(nl, cat);
                OnChanged();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Eror saving report category", ex);
            }
        }

        private void itemDeleteCategory_Click(object sender, EventArgs e)
        {
            NodeList nl = itemAddCategory.Tag as NodeList;
            if (nl == null || nl.category == null)
                return;            
            try
            {
                INTAPS.Accounting.Client.AccountingClient.DeleteReportCategory(nl.category.id);
                treeView.SelectedNode.Remove();
                OnChanged();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Eror saving report category", ex);
            }
        }

        private void itemRenameCateogry_Click(object sender, EventArgs e)
        {
            NodeList nl = itemAddCategory.Tag as NodeList;
            if (nl == null || nl.category==null)
                return;
            try
            {
                INTAPS.UI.Windows.InputForm i=new INTAPS.UI.Windows.InputForm();
                if(i.Show("Report Subsystem","Enter category name",nl.category.name)!=DialogResult.OK)
                    return;
                nl.category.name=i.InputString;
                INTAPS.Accounting.Client.AccountingClient.SaveReportCategory(nl.category);
                treeView.SelectedNode.Text=nl.category.name;
                OnChanged();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Eror saving report category", ex);
            }
        }

        private void treeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point p = treeView.PointToClient(Cursor.Position);
                treeView.SelectedNode = treeView.HitTest(p).Node;
                contextTree.Show(treeView, p);
            }
        }
        ReportDefination ProcessDragOperation(DragEventArgs e,out NodeList nl)
        {
            nl = null;
            e.Effect = DragDropEffects.None;
            if (!e.Data.GetDataPresent(typeof(ReportDefination)))
                return null;
            ReportDefination dragedItem = e.Data.GetData(typeof(ReportDefination)) as ReportDefination;
            if (dragedItem == null)
                return null;
            //Point p = treeView.PointToClient(new Point(e.X, e.Y));
            Point p = treeView.PointToClient(Cursor.Position);
            TreeNode n = treeView.HitTest(p).Node;
            if (n == null)
                return null;
            e.Effect = DragDropEffects.None;
            nl = n.Tag as NodeList;
            if (nl == null)
                return null;
            if (nl.category.id == dragedItem.categoryID)
                return null;
            e.Effect = DragDropEffects.Move;
            return dragedItem;
        }
        private void treeView_DragEnter(object sender, DragEventArgs e)
        {
            NodeList nl;
            ProcessDragOperation(e,out nl);
        }

        private void listView_MouseDown(object sender, MouseEventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;
            if (e.Button == MouseButtons.Left)
            {
                listView.DoDragDrop(listView.SelectedItems[0].Tag, DragDropEffects.Move);
            }
        }

        private void treeView_DragOver(object sender, DragEventArgs e)
        {
            NodeList nl;
            ProcessDragOperation(e, out nl);
        }

        private void treeView_DragDrop(object sender, DragEventArgs e)
        {
            NodeList nl;
            ReportDefination dragedItem = ProcessDragOperation(e,out nl);
            if (dragedItem == null)
                return;
            int catID = dragedItem.categoryID;
            dragedItem.categoryID = nl.category.id;
            try
            {
                INTAPS.Accounting.Client.AccountingClient.SaveEHTMLData(dragedItem);
                OnChanged();
            }
            catch (Exception ex)
            {
                dragedItem.categoryID = catID;
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error moving report.", ex);
            }
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonDelete.Enabled = listView.SelectedItems.Count > 0;
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;
            ReportDefination def = listView.SelectedItems[0].Tag as ReportDefination;
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the report '{0}'".format(def.name)))
                return;
            try
            {
                INTAPS.Accounting.Client.AccountingClient.DeleteReport(def.id);
                ((NodeList)treeView.SelectedNode.Tag).items.Remove(listView.SelectedItems[0]);
                listView.SelectedItems[0].Remove();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error deleteng report.", ex);
            }
        }
    }
}