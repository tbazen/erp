using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class ReportSelectorBase
    {
        ReportTypeSelector rtSelectore = new ReportTypeSelector();
        protected bool SelectReportType(out ReportType selected)
        {
            if (rtSelectore.ShowDialog() == DialogResult.OK)
            {
                selected = rtSelectore.pickedType;
                return true;
            }
            selected = null;
            return false;
        }
        public void ShowReport(ReportDefination def)
        {
            Form f = Accounting.Client.AccountingClient.reportClientHandlers[def.reportTypeID].LoadReport(def);
            f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
        public void CreateReport(int typeID,int categoryID)
        {
            Form f = Accounting.Client.AccountingClient.reportClientHandlers[typeID].LoadCreateForm(categoryID);
            f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
    }

    public class ToolStripDrowDownReportSelector : ReportSelectorBase, IReportSelector
    {
        ToolStripDropDownButton m_dropDown;
        public ToolStripDrowDownReportSelector(ToolStripDropDownButton button)
        {
            m_dropDown = button;
            Accounting.Client.AccountingClient.ReportChanged += new INTAPS.Accounting.Client.ReportChangedHandler(ToolStripDrowDownReportSelector_ReportChanged);
            
            m_dropDown.DropDown.Items.Add(new ToolStripSeparator());

            ToolStripMenuItem item = new ToolStripMenuItem("Manage Reports");
            item.Click += new EventHandler(ShowReportManager);
            m_dropDown.DropDown.Items.Add(item);
            
            item = new ToolStripMenuItem("Refresh Menu");
            item.Click += new EventHandler(RefreshMenu);
            m_dropDown.DropDown.Items.Add(item);

        }

        void RefreshMenu(object sender, EventArgs e)
        {
            while (m_dropDown.DropDown.Items.Count > 3)
                m_dropDown.DropDown.Items.RemoveAt(0);
            Accounting.Client.AccountingClient.PopulateReportSelector(-1, this);
        }

        void ShowReportManager(object sender, EventArgs e)
        {
            ReportManager rm = new ReportManager();
            Accounting.Client.AccountingClient.PopulateReportSelector(-1, rm.selector);
            rm.ShowDialog();
            if (rm.Changed)
            {
                RefreshMenu(null, null);
            }
        }


        void ToolStripDrowDownReportSelector_ReportChanged(INTAPS.ClientServer.Client.DataChange change, ReportDefination def)
        {
            ToolStripMenuItem typeItem = null;
            foreach (ToolStripItem mi in this.m_dropDown.DropDown.Items)
            {
                ReportCategory cat=mi.Tag as ReportCategory;
                if (cat == null)
                    continue;
                if (cat.id == def.categoryID)
                {
                    typeItem = mi as ToolStripMenuItem;
                    break;
                }
            }
            if (typeItem == null)
                return;
            ToolStripMenuItem reportItem = null;
            if (change != INTAPS.ClientServer.Client.DataChange.Create)
            {
                foreach (ToolStripItem mi in typeItem.DropDown.Items)
                {
                    ReportDefination rd = mi.Tag as ReportDefination;
                    if (rd == null)
                        continue;
                    if (rd.id == def.id)
                    {
                        reportItem = mi as ToolStripMenuItem;
                        break;
                    }
                }
                if (reportItem == null)
                    return;
            }
            switch (change)
            {
                case INTAPS.ClientServer.Client.DataChange.Create:
                    this.CreateReportItem(typeItem, def, Accounting.Client.AccountingClient.reportClientHandlers.ContainsKey(def.reportTypeID));
                    break;
                case INTAPS.ClientServer.Client.DataChange.Update:
                    reportItem.Text = def.name;
                    reportItem.Tag = def;
                    break;
                case INTAPS.ClientServer.Client.DataChange.Delete:
                    typeItem.DropDown.Items.Remove(reportItem);
                    break;
            }
        }
        #region IReportSelector Members

        public object CreateCategoryItem(object parentItem, ReportCategory category)
        {
            ToolStripMenuItem paritem = new ToolStripMenuItem(category.name);
            ToolStripMenuItem catParent = parentItem as ToolStripMenuItem;
            if (catParent == null)
                m_dropDown.DropDown.Items.Insert(m_dropDown.DropDown.Items.Count - 3, paritem);
            else
            {
                catParent.DropDown.Items.Insert(catParent.DropDown.Items.Count-2, paritem);
            }
            paritem.DropDown.Items.Add(new ToolStripSeparator());
            paritem.Tag = category;
            
            ToolStripMenuItem newItem = new ToolStripMenuItem("New Report");
            newItem.Click += new EventHandler(OnCreateNewReport);
            newItem.Tag = category;
            paritem.DropDown.Items.Add(newItem);
            
            
            return paritem;
        }

        public object CreateReportItem(object parentItem, ReportDefination rd, bool enabled)
        {
            ToolStripMenuItem newItem = new ToolStripMenuItem(rd.name);
            newItem.Tag = rd;
            newItem.Click += new EventHandler(OnShowReport);
            newItem.Enabled = enabled;
            ToolStripMenuItem catParent = parentItem as ToolStripMenuItem;
            if (catParent == null)
                m_dropDown.DropDown.Items.Insert(m_dropDown.DropDown.Items.Count - 3, newItem);
            else
                ((ToolStripMenuItem)parentItem).DropDown.Items.Insert(catParent.DropDown.Items.Count - 2, newItem);
            
            return newItem;
        }

        void OnCreateNewReport(object sender, EventArgs e)
        {
            try
            {
                ReportCategory cat= ((ToolStripMenuItem)sender).Tag as ReportCategory;
                ReportType t;
                if(base.SelectReportType(out t))
                    base.CreateReport(t.id,cat.id);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to load report form", ex);
            }
        }

        void OnShowReport(object sender, EventArgs e)
        {
            try
            {
                ReportDefination def = ((ToolStripMenuItem)sender).Tag as ReportDefination;
                base.ShowReport(def);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to load report form", ex);
            }
        }

        #endregion
    }

}
