using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace INTAPS.Accounting.Client
{
    public partial class ReportTypeSelector : Form
    {
        public ReportType pickedType=null;
        public ReportTypeSelector()
        {
            InitializeComponent();
            ReportType[] types=Accounting.Client.AccountingClient.GetAllReportTypes();
            foreach (ReportType t in types)
            {
                ListViewItem li = new ListViewItem(t.name);
                li.Tag = t;
                listView.Items.Add(li);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;
            pickedType = listView.SelectedItems[0].Tag as ReportType;
            this.DialogResult = DialogResult.OK;
        }

        private void listView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnOk_Click(null, null);    
        }
    }
}