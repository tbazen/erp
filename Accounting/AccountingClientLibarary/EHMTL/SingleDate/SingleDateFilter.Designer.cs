namespace INTAPS.Accounting.Client
{
    partial class SingleDateFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.etdtp = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.label1 = new System.Windows.Forms.Label();
            this.comboToTime = new System.Windows.Forms.ComboBox();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // etdtp
            // 
            this.etdtp.Location = new System.Drawing.Point(51, 9);
            this.etdtp.Name = "etdtp";
            this.etdtp.Size = new System.Drawing.Size(222, 20);
            this.etdtp.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.etdtp.TabIndex = 0;
            this.etdtp.Changed += new System.EventHandler(this.etdtp_Changed);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date:";
            // 
            // comboToTime
            // 
            this.comboToTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboToTime.FormattingEnabled = true;
            this.comboToTime.Items.AddRange(new object[] {
            "12:00:00 AM (Start of day)",
            "11:59:15 PM (End of day)",
            "11:59:45 PM (After closing time)"});
            this.comboToTime.Location = new System.Drawing.Point(8, 34);
            this.comboToTime.Name = "comboToTime";
            this.comboToTime.Size = new System.Drawing.Size(135, 21);
            this.comboToTime.TabIndex = 5;
            this.comboToTime.SelectedIndexChanged += new System.EventHandler(this.comboToTime_SelectedIndexChanged);
            // 
            // timeTo
            // 
            this.timeTo.CustomFormat = "hh:mm:ss tt";
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo.Location = new System.Drawing.Point(159, 35);
            this.timeTo.Name = "timeTo";
            this.timeTo.Size = new System.Drawing.Size(114, 20);
            this.timeTo.TabIndex = 4;
            this.timeTo.Value = new System.DateTime(2012, 7, 18, 0, 0, 0, 0);
            // 
            // SingleDateFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboToTime);
            this.Controls.Add(this.timeTo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.etdtp);
            this.Name = "SingleDateFilter";
            this.Size = new System.Drawing.Size(280, 61);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private INTAPS.Ethiopic.ETDateTimeDropDown etdtp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboToTime;
        private System.Windows.Forms.DateTimePicker timeTo;
    }
}
