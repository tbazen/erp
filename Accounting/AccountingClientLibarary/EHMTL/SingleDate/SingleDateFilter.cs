using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class SingleDateFilter : UserControl
    {
        public event EventHandler FilterUpdated;
        INTAPS.UI.EtGrPickerPair datePair;
        public SingleDateFilter()
        {
            InitializeComponent();
            datePair = new INTAPS.UI.EtGrPickerPair(etdtp, timeTo);
            DateTime date = DateTime.Now;
            datePair.SetValue(new DateTime(date.Year, date.Month, date.Day, 23, 59, 15));            


        }
        void OnFilterUpdate()
        {
            if (FilterUpdated != null)
                FilterUpdated(this, null);
        }
        private void etdtp_Changed(object sender, EventArgs e)
        {
            OnFilterUpdate();
        }
        public DateTime FilterDate
        {
            get
            {
                return datePair.GetValue();
            }
        }

        private void comboToTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime date = datePair.GetValue();
            switch (comboToTime.SelectedIndex)
            {
                case 0: datePair.SetValue(new DateTime(date.Year, date.Month, date.Day, 0, 0, 0));
                    break;
                case 1: datePair.SetValue(new DateTime(date.Year, date.Month, date.Day, 23, 59, 15));
                    break;
                case 2: datePair.SetValue(new DateTime(date.Year, date.Month, date.Day, 23, 59, 45));
                    break;
            }
        }
    }
}
