using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public class RFSingleDateParameter: EHTMLForm
    {
        protected SingleDateFilter m_filter;
        public RFSingleDateParameter()
        {
            m_filter = new SingleDateFilter();
            this.Controls.Add(m_filter);
            m_filter.BringToFront();
            m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }
        public override object[] GetParamters()
        {
            DateTime date = m_filter.FilterDate.Date;
            return new object[] { date, date.AddDays(1) };
        }
    }
    public class RCHSingleDateParameter : GenericReportClientHandler<RFSingleDateParameter>
    {
        public RCHSingleDateParameter(ReportType rt)
            : base(rt)
        {
        }
    }
    public class RCHSingleDateParameterFinance : RCHSingleDateParameter
    {
        public RCHSingleDateParameterFinance(ReportType rt)
            : base(rt)
        {
        }
    }
}
