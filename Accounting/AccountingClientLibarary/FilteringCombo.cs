using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    class FilteringCombo<ItemType>:ComboBox
    {
        List<string> m_items=new List<string>();
        List<ItemType> m_values = new List<ItemType>();
        List<int> m_displayed = new List<int>();
        public void AddItemPair(string item, ItemType val)
        {
            m_items.Add(item);
            m_values.Add(val);
            base.Items.Add(item);
            m_displayed.Add(m_items.Count-1);
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (base.Items.Count > 0)
                    this.SelectedIndex = 0;
            }
            base.OnKeyDown(e);
        }
        protected override void OnTextChanged(EventArgs e)
        {
            if (m_ignoreTextChangeEvent) return;
            m_ignoreTextChangeEvent = true;
            try
            {
                int selectedItemIndex = base.SelectedIndex;
                FilterBy(base.Text);
                if (base.Items.Count > selectedItemIndex)
                    base.SelectedIndex = selectedItemIndex;
                base.DroppedDown = true;
            }
            finally
            {
                m_ignoreTextChangeEvent = false;
            }
        }
        public void FilterBy(string text)
        {
            m_displayed.Clear();
            for (int k = base.Items.Count - 1; k >= 0; k--)
                base.Items.RemoveAt(0);
            for(int i=0;i<m_items.Count;i++)
            {
                if (m_items[i].ToUpper().Contains(text.ToUpper()))
                {
                    m_displayed.Add(i);
                    base.Items.Add(m_items[i]);
                }
            }
        }
        bool m_ignoreTextChangeEvent = false;
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            m_ignoreTextChangeEvent = true;
            try
            {
                base.OnSelectedIndexChanged(e);
            }
            finally
            {
                m_ignoreTextChangeEvent = false;
            }
        }
        public ItemType SelectedObject
        {
            get
            {
                if (base.SelectedIndex == -1)
                    throw new Exception("No Item Selected");
                return m_values[m_displayed[base.SelectedIndex]];
            }
        }
        public void ResetFilter()
        {
            m_displayed.Clear();
            base.Items.Clear();
            for(int i=0;i<m_items.Count;i++)
            {
                base.Items.Add(m_items[i]);
                m_displayed.Add(i);
            }
        }

        internal void ClearItemPairs()
        {
            m_displayed.Clear();
            m_items.Clear();
            m_values.Clear();
            base.Items.Clear();
        }

        internal void AddRange(ItemType[] items)
        {
            
            foreach (ItemType itm in items)
            {
                AddItemPair(itm.ToString(), itm);
            }
        }
    }
}
