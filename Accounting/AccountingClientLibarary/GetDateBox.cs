using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class GetDateBox : Form
    {
        public GetDateBox()
        {
            InitializeComponent();
            
        }
        public static DialogResult Show(string Caption, string Prompt, out DateTime value)
        {
            GetDateBox db = new GetDateBox();
            db.Text = Caption;
            db.lblPrompot.Text = Prompt;
            if (db.ShowDialog() == DialogResult.OK)
            {
                value = db.dtpValue.DateTime;
                return DialogResult.OK;
            }
            value = DateTime.Now;
            return DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}