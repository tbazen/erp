﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting.Client
{
    public interface IAccountingClientFilter
    {
        bool PostDocument(AccountDocument doc);
        bool DeleteDocument(int docID);
        bool ReverseDocument(int docID);
    }
}
