using INTAPS.UI;
namespace INTAPS.Accounting.Client
{
    partial class LedgerViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.comboItem = new FilteringCombo<TransactionItem>();
            this.dtpTo = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.dtpFrom = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.label1 = new System.Windows.Forms.Label();
            this.pageNavigator = new INTAPS.UI.PageNavigator();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblItemLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.wbLedger = new INTAPS.UI.HTML.ControlBrowser();
            this.bowserController = new INTAPS.UI.HTML.BowserController();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.comboItem);
            this.pnlTop.Controls.Add(this.dtpTo);
            this.pnlTop.Controls.Add(this.dtpFrom);
            this.pnlTop.Controls.Add(this.label1);
            this.pnlTop.Controls.Add(this.pageNavigator);
            this.pnlTop.Controls.Add(this.label4);
            this.pnlTop.Controls.Add(this.txtCode);
            this.pnlTop.Controls.Add(this.label2);
            this.pnlTop.Controls.Add(this.txtName);
            this.pnlTop.Controls.Add(this.lblItemLabel);
            this.pnlTop.Controls.Add(this.label3);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(654, 121);
            this.pnlTop.TabIndex = 1;
            // 
            // comboItem
            // 
            this.comboItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboItem.FormattingEnabled = true;
            this.comboItem.Location = new System.Drawing.Point(85, 94);
            this.comboItem.Name = "comboItem";
            this.comboItem.Size = new System.Drawing.Size(267, 21);
            this.comboItem.TabIndex = 51;
            this.comboItem.SelectedIndexChanged += new System.EventHandler(this.comboItem_SelectedIndexChanged);
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(341, 61);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(123, 20);
            this.dtpTo.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpTo.TabIndex = 50;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(139, 61);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(123, 20);
            this.dtpFrom.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpFrom.TabIndex = 50;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(285, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "To:";
            // 
            // pageNavigator
            // 
            this.pageNavigator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pageNavigator.Location = new System.Drawing.Point(373, 84);
            this.pageNavigator.Name = "pageNavigator";
            this.pageNavigator.NRec = 0;
            this.pageNavigator.PageSize = 0;
            this.pageNavigator.RecordIndex = 0;
            this.pageNavigator.Size = new System.Drawing.Size(278, 31);
            this.pageNavigator.TabIndex = 48;
            this.pageNavigator.PageChanged += new System.EventHandler(this.pageNavigator_PageChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 46;
            this.label4.Text = "From:";
            // 
            // txtCode
            // 
            this.txtCode.BackColor = System.Drawing.Color.White;
            this.txtCode.Location = new System.Drawing.Point(94, 29);
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = true;
            this.txtCode.Size = new System.Drawing.Size(328, 20);
            this.txtCode.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "Code";
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.Color.White;
            this.txtName.Location = new System.Drawing.Point(94, 3);
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            this.txtName.Size = new System.Drawing.Size(328, 20);
            this.txtName.TabIndex = 34;
            // 
            // lblItemLabel
            // 
            this.lblItemLabel.AutoSize = true;
            this.lblItemLabel.Location = new System.Drawing.Point(5, 97);
            this.lblItemLabel.Name = "lblItemLabel";
            this.lblItemLabel.Size = new System.Drawing.Size(27, 13);
            this.lblItemLabel.TabIndex = 33;
            this.lblItemLabel.Text = "Item";
            this.lblItemLabel.Click += new System.EventHandler(this.lblItemLabel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Name";
            // 
            // wbLedger
            // 
            this.wbLedger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbLedger.Location = new System.Drawing.Point(0, 146);
            this.wbLedger.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbLedger.Name = "wbLedger";
            this.wbLedger.Size = new System.Drawing.Size(654, 221);
            this.wbLedger.StyleSheetFile = "berp.css";
            this.wbLedger.TabIndex = 2;
            // 
            // bowserController
            // 
            this.bowserController.Location = new System.Drawing.Point(0, 121);
            this.bowserController.Name = "bowserController";
            this.bowserController.ShowBackForward = true;
            this.bowserController.ShowRefresh = true;
            this.bowserController.Size = new System.Drawing.Size(654, 25);
            this.bowserController.TabIndex = 51;
            this.bowserController.Text = "bowserController1";
            // 
            // LedgerViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.wbLedger);
            this.Controls.Add(this.bowserController);
            this.Controls.Add(this.pnlTop);
            this.Name = "LedgerViewer";
            this.Size = new System.Drawing.Size(654, 367);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private INTAPS.UI.HTML.ControlBrowser wbLedger;
        private PageNavigator pageNavigator;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpTo;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpFrom;
        private System.Windows.Forms.Label label1;
        private INTAPS.UI.HTML.BowserController bowserController;
        protected System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblItemLabel;
        private FilteringCombo<TransactionItem> comboItem;
    }
}
