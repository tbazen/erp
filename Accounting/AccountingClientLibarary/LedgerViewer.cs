using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.HTML;
using System.Web.UI.HtmlControls;

namespace INTAPS.Accounting.Client
{
    public partial class LedgerViewer : UserControl, IHTMLBuilder
    {
        INTAPS.Accounting.AccountTransaction[] m_displayed;
        AccountBalance m_legerTotal;
        HtmlTable m_table;
        protected Account m_account;
        protected CostCenter m_costCenter;
        protected CostCenterAccount m_costCenterAccount;
        bool m_DebitCreditMode = true;
        TransactionItem m_transactionItem = TransactionItem.DefaultCurrency;
        IHTMLLedgerFormater _ledgerFormater = null;
        
        public bool DebitCreditMode
        {
            get { return m_DebitCreditMode; }
            set { m_DebitCreditMode = value; }
        }
        protected virtual TransactionItem[] GetTransactionItems()
        {
            return null;
        }
        bool m_IgnoreEvents = false;
        void SetupItemSelector()
        {
            m_IgnoreEvents = true;
            try
            {
                TransactionItem[] items = GetTransactionItems();
                if (items == null)
                {
                    lblItemLabel.Visible = false;
                    comboItem.Visible = false;
                    return;
                }
                lblItemLabel.Visible = true;
                comboItem.Visible = true;
                comboItem.ClearItemPairs();
                comboItem.AddRange(items);
                if (items.Length == 1)
                    comboItem.SelectedIndex = 0;
            }
            finally
            {
                m_IgnoreEvents = false;
            }
        }
        Button buttonFullLedger;
        bool _fullLedger = false;
        public LedgerViewer()
        {
            InitializeComponent();
            buttonFullLedger = new Button();
            buttonFullLedger.Text = "Full Ledger";
            buttonFullLedger.Click += new EventHandler(buttonFullLedger_Click);
            buttonFullLedger.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            buttonFullLedger.Left = 10;
            buttonFullLedger.Top = pnlTop.Height - buttonFullLedger.Height - 10;
            pnlTop.Controls.Add(buttonFullLedger);
            pageNavigator.NRec = 0;
            pageNavigator.RecordIndex = 0;
            pageNavigator.PageSize = 100;
            bowserController.SetBrowser(wbLedger);
            bowserController.ShowBackForward = true;

        }

        void buttonFullLedger_Click(object sender, EventArgs e)
        {
            pageNavigator.Enabled = false;
            _fullLedger = true;
            wbLedger.RefreshPage();
        }
        public void LoadData(int aid,int csid,IHTMLLedgerFormater formater)
        {
            _ledgerFormater = formater;
            LoadAccountData(aid, csid);
            
            pageNavigator.RecordIndex = 0;
            
            dtpTo.Value = DateTime.Now;
            this.Build();
            wbLedger.LoadControl(this);
            SetupItemSelector();
        }

        private void LoadAccountData(int aid, int csid)
        {
            m_account = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(aid);
            m_costCenter = INTAPS.Accounting.Client.AccountingClient.GetAccount<CostCenter>(csid);
            m_costCenterAccount = AccountingClient.GetCostCenterAccount(csid,aid);
        }
        void LoadDataInternal()
        {
            Cursor.Current = Cursors.WaitCursor;
            txtName.Text = m_account.Name;
            txtCode.Text = m_account.Code;
            int NRec;
            AccountBalance beg;
            if (_fullLedger)
                m_displayed = INTAPS.Accounting.Client.AccountingClient.GetLedger(m_costCenterAccount.id, m_transactionItem.ID, new int[] { m_transactionItem.ID }, dtpFrom.Value, dtpTo.Value.AddDays(1), 0, -1, out NRec,out beg, out m_legerTotal);
            else
                m_displayed = INTAPS.Accounting.Client.AccountingClient.GetLedger(m_costCenterAccount.id, m_transactionItem.ID, new int[] { m_transactionItem.ID }, dtpFrom.Value, dtpTo.Value.AddDays(1), pageNavigator.RecordIndex, pageNavigator.PageSize, out NRec, out beg,out m_legerTotal);
            pageNavigator.NRec = NRec;
        }
        #region IHTMLBuilder Members
        public string GetOuterHtml()
        {
            LoadAccountData( m_account.id, m_costCenter.id);
            string html="";
            html += "<b>Account:</b>" + System.Web.HttpUtility.HtmlEncode(AccountingClient.GetCostCenterAccountCodeName(m_account,m_costCenter));
            if(m_transactionItem.ID!=TransactionItem.DEFAULT_CURRENCY)
                html += "<br/><b>Item:</b>" + System.Web.HttpUtility.HtmlEncode(m_transactionItem.Name);
            html+= "<br/><b>From:</b>" + System.Web.HttpUtility.HtmlEncode(dtpFrom.Value.ToString("MMM dd,yyyy"))
                + "<b> to </b>" + System.Web.HttpUtility.HtmlEncode(dtpTo.Value.ToString("MMM dd,yyyy"));
            AccountBalance bal = AccountingClient.GetBalanceAsOf(m_costCenterAccount.id, m_transactionItem.ID,DateTime.Now);
            if (m_DebitCreditMode)
            {
                html+= "<br/><b>Account Balance:</b>" + bal.GetBalance(m_account.CreditAccount).ToString("0,0.00")
                + "<br/><b>Account Total Debit:</b>" + bal.TotalDebit.ToString("0,0.00")
                + "<br/><b>Account Total Credit:</b>" + bal.TotalCredit.ToString("0,0.00") + "<br/>"
                + "<br/><b>Ledger Total Debit:</b>" + m_legerTotal.TotalDebit.ToString("0,0.00")
                + "<br/><b>Ledger Total Credit:</b>" + m_legerTotal.TotalCredit.ToString("0,0.00") + "<br/>";
            }
            else
            {
                html += "<br/><b>Balance:</b>" + bal.GetBalance(m_account.CreditAccount).ToString("0,0.00")
                +"<br/><b>Ledger Total:</b>" + (m_account.CreditAccount ? m_legerTotal.TotalCredit - m_legerTotal.TotalDebit : m_legerTotal.TotalDebit - m_legerTotal.TotalCredit).ToString("0,0.00");
            }
            return html+HTMLBuilder.ControlToString(m_table);
        }
        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            switch (path)
            {
                case "source":
                    int ID = int.Parse((string)query["ID"]);
                    wbLedger.LoadTextPage("", GetDocHTML(ID));
                    break;
            }
            return false;
        }

        private string GetDocHTML(int ID)
        {
            AccountDocument doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(ID, true);
            TransactionOfBatch[] batch = INTAPS.Accounting.Client.AccountingClient.GetTransactionOfDocument(doc.AccountDocumentID);
            INTAPS.Accounting.IGenericDocumentClientHandler h = INTAPS.Accounting.Client.AccountingClient.GetDocumentHandler(doc.DocumentTypeID);
            string docHTML;
            if (h != null)
                docHTML = INTAPS.Accounting.Client.AccountingClient.GetDocumentHTML(doc.AccountDocumentID);
            else
                docHTML= doc.BuildHTML();
            return /*"<h2>" + System.Web.HttpUtility.HtmlEncode(INTAPS.Accounting.Client.AccountingClient.GetAccountingDocumentType(doc.DocumentTypeID).name) + "</h2>"
                    + docHTML
                    + "<h2>Affected accounts</h2>"
                    +*/ INTAPS.Accounting.Client.AccountingClient.getDocumentEntriesHTML(doc.AccountDocumentID);
        }
        public void SetHost(IHTMLDocumentHost host)
        {

        }
        public void Build()
        {
            m_table = new HtmlTable();
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                m_table.CellSpacing = 0;
                LoadDataInternal();
                m_table.Border = 1;
                if (m_DebitCreditMode)
                {
                    HTMLBuilder.CreateHtmlRow(m_table
                        , "LedgerGridHeadCell"
                        , HTMLBuilder.CreateTextCell("Date")
                        , HTMLBuilder.CreateTextCell("Debit")
                        , HTMLBuilder.CreateTextCell("Credit")
                        , HTMLBuilder.CreateTextCell("Balance")
                        , HTMLBuilder.CreateTextCell("Note")
                        , HTMLBuilder.CreateTextCell("Source")
                        , HTMLBuilder.CreateTextCell("Check No")
                        , HTMLBuilder.CreateTextCell("Account ID")
                        );
                }
                else
                {
                    HTMLBuilder.CreateHtmlRow(m_table
                        , "LedgerGridHeadCell"
                        , HTMLBuilder.CreateTextCell("Date")
                        , HTMLBuilder.CreateTextCell("Amount")
                        , HTMLBuilder.CreateTextCell("Balance")
                        , HTMLBuilder.CreateTextCell("Note")
                        , HTMLBuilder.CreateTextCell("Source")
                        );
                }

                foreach (INTAPS.Accounting.AccountTransaction t in m_displayed)
                {
                    TransactionOfBatch[] batch = INTAPS.Accounting.Client.AccountingClient.GetTransactionOfDocument(t.documentID);
                    AccountDocument doc = null;

                    try
                    {
                        doc = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(t.documentID, false);
                    }
                    catch
                    {
                        doc = null;
                    }
                    double dbBalance = t.NewDebit;
                    double crBalance = t.NewCredit;
                    double balance = m_account.CreditAccount ? crBalance - dbBalance : dbBalance - crBalance;
                    string Note;
                    string source;
                    if (string.IsNullOrEmpty(t.Note))
                        if (doc == null || string.IsNullOrEmpty(doc.ShortDescription))
                            if (doc == null || string.IsNullOrEmpty(doc.LongDescription))
                                Note = "";
                            else
                                Note = doc.LongDescription;
                        else
                            Note = doc.ShortDescription;

                    else
                        Note = t.Note;

                    if (doc == null)
                        source = "no source";
                    else
                    {
                        DocumentType dt = INTAPS.Accounting.Client.AccountingClient.GetAccountingDocumentType(doc.DocumentTypeID);
                        source = System.Web.HttpUtility.HtmlEncode(dt.name + (string.IsNullOrEmpty(doc.PaperRef) ? "" : "  " + doc.PaperRef));
                        if (!string.IsNullOrEmpty(doc.ShortDescription))
                            source += "<br/>" + System.Web.HttpUtility.HtmlEncode("Document Note:" + doc.ShortDescription);
                    }
                    if (m_DebitCreditMode)
                    {
                        HTMLBuilder.AddAlternatingStyleRow(m_table, HTMLBuilder.CreateHtmlRow(
                            HTMLBuilder.CreateTextCell(t.execTime.ToString("dd/MM/yyyy HH:mm"))
                            , HTMLBuilder.CreateTextCell(t.Amount > 0 ? t.Amount.ToString("0,0.00") : "", "CurrencyCell")
                            , HTMLBuilder.CreateTextCell(t.Amount < 0 ? (-t.Amount).ToString("0,0.00") : "", "CurrencyCell")
                            , HTMLBuilder.CreateTextCell(Account.AmountEqual(balance, 0) ? "" : balance.ToString("0,0.00"), "CurrencyCell")
                            , HTMLBuilder.CreateTextCell(Note)
                            , doc == null ? HTMLBuilder.CreateTextCell(source)
                            : HTMLBuilder.CreateHtmlCell("<a href=source?ID=" + doc.AccountDocumentID + ">" + source + "</a>")
                            , HTMLBuilder.CreateTextCell(doc == null || _ledgerFormater==null? "" : _ledgerFormater.GetInstrumentReference(doc.AccountDocumentID, false))
                            , HTMLBuilder.CreateTextCell(AccountingClient.GetCostCenterAccountWithDescription(t.AccountID).code)
                            ), new string[] { "LedgerGridOddRowCell", "LedgerGridEvenRowCell" });
                    }
                    else
                    {
                        HTMLBuilder.AddAlternatingStyleRow(m_table, HTMLBuilder.CreateHtmlRow(
                            HTMLBuilder.CreateTextCell(t.execTime.ToString("dd/MM/yyyy HH:mm"))
                            , HTMLBuilder.CreateTextCell((m_account.CreditAccount ? -t.Amount : t.Amount).ToString("0,0.00"), "CurrencyCell")
                            , HTMLBuilder.CreateTextCell(Account.AmountEqual(balance, 0) ? "" : balance.ToString("0,0.00"), "CurrencyCell")
                            , HTMLBuilder.CreateTextCell(Note)
                            , doc == null ? HTMLBuilder.CreateTextCell(source)
                            : HTMLBuilder.CreateHtmlCell("<a href=source?ID=" + doc.AccountDocumentID + ">" + System.Web.HttpUtility.HtmlEncode(source) + "</a>")
                            ), new string[] { "LedgerGridOddRowCell", "LedgerGridEvenRowCell" });
                    }

                }
            }
            catch (Exception ex)
            {
                HTMLBuilder.CreateHtmlRow(HTMLBuilder.CreateHtmlCell(ex.Message));
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        public string WindowCaption
        {
            get { return "Ledger"; }
        }
        #endregion
        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            wbLedger.RefreshPage();
        }
        private void comboItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_IgnoreEvents) return;
            if (comboItem.SelectedIndex == -1)
                return;
            m_transactionItem = comboItem.SelectedObject;
            wbLedger.RefreshPage();
        }
        private void lblItemLabel_Click(object sender, EventArgs e)
        {

        }
    }
    public interface IHTMLLedgerFormater
    {
        string GetInstrumentReference(int id, bool transaction);
    }
}
