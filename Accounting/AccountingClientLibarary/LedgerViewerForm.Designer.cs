namespace INTAPS.Accounting.Client
{
    partial class LedgerViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ledgerViewer = new INTAPS.Accounting.Client.LedgerViewer();
            this.SuspendLayout();
            // 
            // ledgerViewer
            // 
            this.ledgerViewer.DebitCreditMode = true;
            this.ledgerViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ledgerViewer.Location = new System.Drawing.Point(0, 0);
            this.ledgerViewer.Name = "ledgerViewer";
            this.ledgerViewer.Size = new System.Drawing.Size(694, 388);
            this.ledgerViewer.TabIndex = 0;
            // 
            // LedgerViewerForm
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.ClientSize = new System.Drawing.Size(694, 388);
            this.Controls.Add(this.ledgerViewer);
            this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LedgerViewerForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ledger Viewer";
            this.ResumeLayout(false);

        }

        #endregion

        private LedgerViewer ledgerViewer;
    }
}