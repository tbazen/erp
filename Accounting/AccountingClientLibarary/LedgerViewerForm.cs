using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public partial class LedgerViewerForm: Form
    {
        public LedgerViewerForm()
        {
            InitializeComponent();
        }
        public LedgerViewerForm(CostCenterAccount ac,IHTMLLedgerFormater formater):this()
        {
            ledgerViewer.LoadData(ac==null?-1:ac.accountID,ac==null?-1:ac.costCenterID,formater);
        }
    }
}