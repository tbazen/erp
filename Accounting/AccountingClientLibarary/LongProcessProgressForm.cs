﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace INTAPS.Accounting.Client
{
    public delegate void LongProcessFinishHandler(object ret,Exception ex);
    public partial class LongProcessProgressForm : Form
    {
        Exception ex=null;
        Object ret = null;
        public LongProcessProgressForm()
        {
            InitializeComponent();
        }
        public void showLongProcess(string title
            ,Delegate method
            ,LongProcessFinishHandler finishHandler
            ,params object[] pars
            )
        {
            this.Text = title;
           Thread mainThread= new System.Threading.Thread(new ThreadStart(
                new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        try
                        {
                            ret=method.DynamicInvoke(pars);
                        }
                        catch (Exception exp)
                        {
                            ex = exp;
                        }
                    }
            )));
            mainThread.Start();
            new System.Threading.Thread(new ThreadStart(
                new INTAPS.UI.HTML.ProcessParameterless(delegate()
                    {
                        string prevMsg = null;
                        double prevProg = 0;
                        do
                        {
                            Thread.Sleep(1000);
                            if (mainThread.IsAlive)
                            {
                                string msg;
                                double prog = AccountingClient.GetProccessProgress(out msg);
                                if (!msg.Equals(prevMsg) || prevProg != prog)
                                {
                                    prevMsg = msg;
                                    prevProg = prog;
                                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                                    {
                                        if (this.IsHandleCreated)
                                        {
                                            this.labelStatus.Text = msg;
                                            this.progressBar1.Value = (int)Math.Min(100, 100 * prevProg);
                                        }
                                    }
                                    ));
                                }
                            }
                            else
                            {
                                this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                                {
                                    if (finishHandler != null)
                                        finishHandler(ret, ex);
                                    this.Close();
                                }
                                ));
                                return;
                            }
                        }
                        while (true);
                    }
            ))).Start();
            
        }
    }
}
