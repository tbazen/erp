using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.Windows;

namespace INTAPS.Accounting.Client
{
    public partial class MergeTransactionForm: AsyncForm

    {
        AccountListGrid m_list;
        public MergeTransactionForm()
        {
            InitializeComponent();

            m_list = new AccountListGrid();
            m_list.SetUpGrid();
            m_list.Dock = DockStyle.Fill;
            this.Controls.Add(m_list);
            m_list.BringToFront();
        }
        protected override bool ShowAnmiation
        {
            get
            {
                return false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int c = m_list.AccountCount;
            if(c<2)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select at least two accounts");
            }
            int[] acs = new int[c];
            for (int i = 0; i < c; i++)
                acs[i] = m_list[i].id;
            WaitFor(new MergeTransactionsProcess(acs, acs[0], true, dateRange.filterDateFrom, dateRange.filterDateTo));
        }
        protected override void WaitIsOver(ServerDataDownloader d)
        {
            MergeTransactionsProcess dp = (MergeTransactionsProcess)d;
            if (dp.ex != null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to merge transaction", dp.ex);
                this.DialogResult = DialogResult.Cancel;
            }
            else
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        protected override void CheckProgress()
        {
            string msg1;
            progressBar.Value = (int)(100 *
                (INTAPS.Accounting.Client.AccountingClient.GetProccessProgress(out msg1)
                ));
            lblStage.Text = msg1;
        }
    }
    public class MergeTransactionsProcess: INTAPS.UI.Windows.ServerDataDownloader
    {

        private readonly int _Destination;
        private readonly bool _ByDate;
        public Exception ex = null;
        private readonly int[] _Sources;
        private readonly DateTime _From;
        private readonly DateTime _To;

        public MergeTransactionsProcess(int[] sources, int destination, bool byDate, DateTime from, DateTime to)
        {
            _From = from;
            _To = to;
            _ByDate = byDate;
            _Destination = destination;
            _Sources = sources;
        }


        public override void RunDownloader()
        {
            try
            {
                INTAPS.Accounting.Client.AccountingClient.MergeTransaction(_Sources, _Destination, _ByDate, _From, _To);
            }
            catch (Exception ex)
            {
                this.ex = ex;
            }
        }

        public override string Description
        {
            get
            {
                
                return "Merging transactions";

            }
        }
    }

}