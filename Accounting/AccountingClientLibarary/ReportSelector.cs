using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Accounting.Client
{
    public interface IReportSelector
    {
        object CreateCategoryItem(object parentItem,ReportCategory rt);
        object CreateReportItem(object parentItem,ReportDefination rd,bool enabled);
    }

}
