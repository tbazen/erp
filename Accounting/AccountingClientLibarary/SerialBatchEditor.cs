using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI;

namespace INTAPS.Accounting.Client
{
    public partial class SerialBatchEditor : Form
    {
        public SerialBatch formData;
        EnterKeyNavigation m_nav;
        public SerialBatchEditor()
        {
            InitializeComponent();
            formData = new SerialBatch();
            m_nav = new EnterKeyNavigation();
            m_nav.AddNavigableItem(new TextNavigator(txtName));
            m_nav.AddNavigableItem(new TextNavigator(txtFrom));
            m_nav.AddNavigableItem(new TextNavigator(txtTo));
            m_nav.AddNavigableItem(new ButtonNavigator(btnOk));
            m_nav.StartEdit();
        }
        private void btnOk_Click(object sender, EventArgs e)
        {

            if (!INTAPS.UI.Helper.ValidateNonEmptyTextBox(txtName, "Please enter name", out formData.note)
                || !INTAPS.UI.Helper.ValidateIntegerTextBox(txtFrom, "Enter start of range", out formData.serialFrom)
                || !INTAPS.UI.Helper.ValidateIntegerTextBox(txtTo, "Enter end of range", out formData.serialTo)
                )
                return;
            if (formData.serialTo - formData.serialFrom < 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter valid range.");
                return;
            }
            try
            {
                formData.batchDate = dtpActivatedDate.Value;
                formData.expiryDate = dtpActivatedDate.Value.AddYears(1);
                formData.active = true;
                //GenericAccountingClient<IAccountingService>.CreateBatch(formData);
                this.DialogResult = DialogResult.OK;
                this.Close();
                
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't create range.", ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}