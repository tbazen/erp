using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting.BDE;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingService : SessionObjectBase<AccountingBDE>
    {

        
        //data manipulation
        public int CreateAccount<AccountType>(AccountType ac) where AccountType : AccountBase, new()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Create Account Attempt", "", -1);
                try
                {                    
                    _bde.WriterHelper.BeginTransaction();
                    int ret = _bde.CreateAccount<AccountType>(AID, ac);
                    base.WriteAddAudit("Create Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void UpdateAccount<AccountType>(AccountType ac) where AccountType : AccountBase, new()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Update Account Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.UpdateAccount<AccountType>(AID, ac);
                    base.WriteAddAudit("Update Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Update Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeleteAccount<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Delete Account Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeleteAccount<AccountType>(AID, accountID,true);
                    base.WriteAddAudit("Delete Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void ActivateAcount<AccountType>(int AccountID, DateTime ActivateDate) where AccountType : AccountBase, new()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Activate Account Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.ActivateAcount<AccountType>(AID, AccountID, ActivateDate);
                    base.WriteAddAudit("Activate Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Activate Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeactivateAccount<AccountType>(int AccountID, DateTime DeactivateDate) where AccountType : AccountBase, new()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Deactivate Account Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeactivateAccount<AccountType>(AID, AccountID, DeactivateDate);
                    base.WriteAddAudit("Deactivate Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Deactivate Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void LinkAccount<AccountType>(int Parent, int Child)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Link Account Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    if (typeof(AccountType) == typeof(Account))
                        _bde.LinkAccount(AID, Parent, Child);
                    else
                        _bde.LinkCostCenter(AID, Parent, Child);
                    base.WriteAddAudit("Link Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Link Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public int CreateCostCenterAccount(int costCenterID, int accountID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Create Cost Center Account Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int ret = _bde.CreateCostCenterAccount(AID, costCenterID, accountID);
                    base.WriteAddAudit("Create Cost Center Account Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Create Cost Center Account Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void DeleteCostCenterAccount(int csaID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Delete Cost Center Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DeleteCostCenterAccount(AID, csaID);
                    base.WriteAddAudit("Delete Cost Center  Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Cost Center Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        //get
        public TransactionItem GetTransactionItem(int itemID)
        {
            return _bde.GetTransactionItem(itemID);
        }
        public bool IsControlOf<AccountType>(int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            return _bde.IsControlOf<AccountType>(acountID, testAccount);
        }
        public bool IsDirectControlOf<AccountType>(int acountID, int testAccount) where AccountType : AccountBase, new()
        {
            return _bde.IsDirectControlOf<AccountType>(acountID, testAccount);
        }
        public int GetAccountID<AccountType>(string AccountCode) where AccountType : AccountBase, new()
        {
            return _bde.GetAccountID<AccountType>(AccountCode);
        }
        public AccountType[] GetLeafAccounts<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            return _bde.GetLeafAccounts<AccountType>(accountID);
        }
        public AccountType GetAccount<AccountType>(string AccountCode) where AccountType : AccountBase, new()
        {
            return _bde.GetAccount<AccountType>(AccountCode);
        }
        public AccountType GetAccount<AccountType>(int AccountID) where AccountType : AccountBase, new()
        {
            return _bde.GetAccount<AccountType>(AccountID);
        }
        public AccountType[] GetChildAllAcccount<AccountType>(int ParentAccount) where AccountType : AccountBase, new()
        {
            return _bde.GetChildAllAcccount<AccountType>(ParentAccount);
        }
        public AccountType[] GetChildAcccount<AccountType>(int ParentAccount, int index, int pageSize, out int NRecords) where AccountType : AccountBase, new()
        {
            return _bde.GetChildAcccount<AccountType>(ParentAccount, index, pageSize, out NRecords);
        }
        public AccountType[] SearchAccount<AccountType>(string query, int index, int pageSize, out int NRecords)
            where AccountType : AccountBase, new()
        {
            return _bde.SearchAccount<AccountType>(query, index, pageSize, out NRecords);
        }
        public AccountType[] SearchAccount<AccountType>(string query, int category, int index, int pageSize, out int NRecords)
            where AccountType : AccountBase, new()
        {
            return _bde.SearchAccount<AccountType>(query, category, index, pageSize, out NRecords);
        }
        public CostCenterAccount GetDefaultCostCenterAccount(int accountID)
        {
            return _bde.GetDefaultCostCenterAccount(accountID);
        }
        public CostCenterAccount GetCostCenterAccount(int csaID)
        {
            return _bde.GetCostCenterAccount(csaID);
        }
        public CostCenterAccount GetCostCenterAccount(int costCenterID, int accountID)
        {
            return _bde.GetCostCenterAccount(costCenterID, accountID);
        }
        public bool IsSingleCostCenter()
        {
            return _bde.IsSingleCostCenter();
        }
        public CostCenterAccount GetCostCenterAccount(string code)
        {
            return _bde.GetCostCenterAccount(code);
        }

        public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(int csaID)
        {
            return _bde.GetCostCenterAccountWithDescription(csaID);
        }

        public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(string code)
        {
            return _bde.GetCostCenterAccountWithDescription(code);
        }

        public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccountID, int resItemID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord, out AccountBalance begBal, out AccountBalance total)
        {
            return _bde.GetLedger(resAccountID, resItemID, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord, out begBal, out total);
        }
        public CostCenterAccountWithDescription GetCostCenterAccountWithDescription(int costCenterID, int accountID)
        {
            return _bde.GetCostCenterAccountWithDescription(costCenterID, accountID);
        }


        public void materializeDocument(int docID, DateTime materialzationDate)
        {

            DateTime now = DateTime.Now;
            AccountDocument doc = _bde.GetAccountDocument(docID, true);
            if (doc == null)
                throw new Exception("Document doesn't exist in the database");
            if (!doc.scheduled)
                throw new Exception("This document is not scheduled");
            if (doc.materialized)
                throw new Exception("The document is already materialized");
            if (doc.DocumentDate.Subtract(now).TotalSeconds > AccountingBDE.FUTURE_TOLERANCE_SECONDS)
                throw new Exception("Document is still in the future");
            doc.DocumentDate = materialzationDate;
            doc.materialized = true;
            doc.materializedOn = now;
            if (!_bde.GetDocumentHandler(_bde.GetDocumentTypeByType(doc.GetType()).id).CheckPostPermission(doc.AccountDocumentID, doc, base.m_Session))
                throw new Exception("Access Denied");
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("materializeDocument Attempt", -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    _bde.PostGenericDocument(AID, doc);
                    base.WriteExecuteAudit("materializeDocument Success", AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteSerialBatch Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public int getMaterializePendingCount(DateTime time)
        {
            return _bde.getMaterializePendingCount(time);
        }

        public int[] getMaterializePendingDocumentIDs(DateTime time)
        {
            return _bde.getMaterializePendingDocumentIDs(time);
        }

        public void RescheduleAccountDocument(int docID, DateTime time)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("RescheduleAccountDocument attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.RescheduleAccountDocument(AID, docID, time);
                    base.WriteExecuteAudit("RescheduleAccountDocument Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RescheduleAccountDocument Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException("Error Post Generic Document", ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public CostCenterAccount[] GetCostCenterAccountsOf<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            return _bde.GetCostCenterAccountsOf<AccountType>(accountID);
        }

        public void ActivateCostCenterAccount(int csAccountID, DateTime ActivateDate)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("ActivateCostCenterAccount attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.ActivateCostCenterAccount(AID,csAccountID, ActivateDate);
                    base.WriteExecuteAudit("ActivateCostCenterAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ActivateCostCenterAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException("Error ActivateCostCenterAccount", ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void DectivateCostCenterAccount(int csAccountID, DateTime deactivationDate)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DectivateCostCenterAccount attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.DectivateCostCenterAccount(AID, csAccountID, deactivationDate);
                    base.WriteExecuteAudit("DectivateCostCenterAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DectivateCostCenterAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException("Error DectivateCostCenterAccount", ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
            
        }


        internal List<int> GetJoinedAccountIDs<AccountType>(int costCenterID) where AccountType:AccountBase,new ()
        {
            return _bde.GetJoinedAccountIDs<AccountType>(costCenterID);
        }

        public double GetNetCostCenterAccountBalanceAsOf(int costCenterID, int accountid,int itemId, DateTime date)
        {
            CostCenterAccount csa = GetCostCenterAccount(costCenterID, accountid);
            if (csa == null)
                return 0;
            return GetNetBalanceAsOf(csa.id, itemId, date);
        }
    }
}
