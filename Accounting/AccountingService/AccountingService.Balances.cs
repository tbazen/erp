﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting.BDE;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingService : SessionObjectBase<AccountingBDE>
    {
        public AccountBalance GetFlow(int AccountID, int ItemID, DateTime from, DateTime to)
        {
            return _bde.GetFlow(AccountID, ItemID, from, to);
        }
        public double GetEndNetBalance(int csAccountID, int itemID)
        {
            return _bde.GetEndNetBalance(csAccountID, itemID);
        }
        public AccountBalance GetEndBalance(int accountID, int itemID)
        {
            return _bde.GetEndBalance(accountID, itemID);
        }
        public AccountBalance[] GetEndBalances(int accountID)
        {
            return _bde.GetEndBalances(accountID);
        }
        public AccountBalance[] GetEndItemBalances(int materialID)
        {
            return _bde.GetEndItemBalances(materialID);
        }
        public double GetNetBalanceAsOf(int csAccountID, int itemID, DateTime date)
        {
            return _bde.GetNetBalanceAsOf(csAccountID, itemID, date);
        }
        public AccountBalance GetBalanceAsOf(int AccountID, int ItemID, DateTime date)
        {
            return _bde.GetBalanceAsOf(AccountID, ItemID, date);
        }
        public AccountBalance[] GetBalancesAsOf(int accountID, DateTime date)
        {
            return _bde.GetBalancesAsOf(accountID, date);
        }
    }
}