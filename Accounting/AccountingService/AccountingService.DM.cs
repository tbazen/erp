using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
namespace INTAPS.Accounting.Service
{
    public partial class AccountingService
    {
        protected virtual void CheckSalesPointManagmentPermision()
        {
            if (!m_Session.Permissions.IsPermited("Root/Finance/ManageSalesPoint"))
                throw new AccessDeniedException("You don't have permission to manage sales points");
        }
        protected virtual void CheckReportManagePermission()
        {
            if (!m_Session.Permissions.IsPermited("Root/Finance/ManageReport"))
                throw new AccessDeniedException("You don't have permission to manage reports");
        }
        protected virtual void CheckCanManageAccountsPermission(Account ac,AccountProtection protection)
        {
            if (!m_Session.Permissions.IsPermited("Root/Finance/ChartOfAccounts"))
                throw new AccessDeniedException("You don't have permission to manage chart of accounts");
            if(ac!=null)
                if ((ac.protection & protection) !=AccountProtection.None)
                    throw new Exception("You can't make this change because the account is protected");
        }
        protected virtual void CheckSalesTransactionPermission()
        {
            if (!m_Session.Permissions.IsPermited("Root/Finance/Sales"))
                throw new AccessDeniedException("You don't have permission to post sales transactions.");
        }
        protected virtual void CheckExpenseTransactionPermission()
        {
            if (!m_Session.Permissions.IsPermited("Root/Finance/Expense"))
                throw new AccessDeniedException("You don't have permission to post expense transactions.");
        }
        protected virtual void CheckOpenAccountPermission()
        {
            if (!HasOpenAccountPermission())
                throw new AccessDeniedException("You don't have permission to open accounts.");
        }
        private bool HasOpenAccountPermission()
        {
            return m_Session.Permissions.IsPermited("Root/Finance/OpenAccount");
        }
        private bool HasAccountManagerPermission()
        {
            return m_Session.Permissions.IsPermited("Root/Finance/AccountManager");
        }
        
        
        public void DeleteSerialBatch(int id)
        {
            CheckSalesPointManagmentPermision();
            SerialBatch sb = bdeAccounting.GetSerialBatch(id);
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteObjectAudit("DeleteSerialBatch Attempt", id.ToString(), sb, -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    bdeAccounting.DeleteSerialBatch(AID, id);
                    base.WriteExecuteAudit("DeleteSerialBatch Success", AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteSerialBatch Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void UseSerial(UsedSerial ser)
        {
            CheckAccountsAdminRole();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("DeleteSerialBatch Attempt", ser.batchID + "_" + ser.val, ser, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdeAccounting.UseSerial(AID,ser);
                    base.WriteExecuteAudit("UseSerial Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteSerialBatch Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        private void CheckAccountsAdminRole()
        {
            if (!m_Session.Permissions.IsPermited("root/Finance/Admin"))
                throw new AccessDeniedException("You are not authroized to change finance settings.");
        }
        public int CreateBatch(SerialBatch batch)
        {
            CheckSalesPointManagmentPermision();
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteObjectAudit("DeleteSerialBatch Attempt", batch.id.ToString(), batch, -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    int ret = bdeAccounting.CreateBatch(AID,batch);
                    base.WriteExecuteAudit("UseSerial Success", AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreateBatch Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }

        }        
        public void DeleteAccountDocument(int documentID)
        {
            throw new ServerUserMessage("Deleteting account documents through this method is no longer supported");
            if (!m_Session.Permissions.IsPermited("Root/Finance/DeleteDocument"))
                throw new AccessDeniedException();

            AccountDocument doc = bdeAccounting.GetAccountDocument(documentID, true);
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteObjectAudit("DeleteAccountDocument Attempt", documentID.ToString(), doc, -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    bdeAccounting.DeleteAccountDocument(AID, documentID,false);
                    base.WriteExecuteAudit("DeleteAccountDocument Success", AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteAccountDocument Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void ReverseDocument(TransactionReversalDocument r)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("ReverseDocument Attempt", r.GetDocumentHashCode(), r, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.ReverseDocument(AID, r);
                    base.WriteExecuteAudit("ReverseDocument Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ReverseDocument Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void ReverseAccountDocument(int documentID)
        {
            if(!m_Session.Permissions.IsPermited("Root/Finance/ReverseDocument"))
                throw new AccessDeniedException();
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("ReverseAccountDocument Attempt", documentID.ToString(), -1, documentID);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    bdeAccounting.ReverseDocument(AID, documentID);
                    base.WriteExecuteAudit("ReverseAccountDocument Success", AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ReverseAccountDocument Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public int SaveEHTMLData(ReportDefination r)
        {
            CheckReportManagePermission();
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteAddAudit("Save Report Defination Attempt", r.name, -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    int ret = bdeAccounting.SaveEHTMLData(AID, r);
                    base.WriteAddAudit("Save Report Defination Success", ret.ToString(), AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Save Report Defination Filure", r.name, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public int SaveReportCategory(ReportCategory cat)
        {
            CheckReportManagePermission();
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteAddAudit("Save Report Category Attempt", cat.name, -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    int ret = bdeAccounting.SaveReportCategory(AID, cat);
                    base.WriteAddAudit("Save Report Category Success", ret.ToString(), AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Save Report Category Failure", cat.name, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void DeleteReportCategory(int catID)
        {
            CheckReportManagePermission();
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteAddAudit("Delete Report Category Attempt", catID.ToString(), -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    bdeAccounting.DeleteReportCategory(AID, catID);
                    base.WriteAddAudit("Delete Report Category Success", catID.ToString(), AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Report Category Failure", catID.ToString(), ex.Message+"\n"+ex.StackTrace,AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        //moved from material
        public void DeleteGenericDocument(int docID)
        {
            if (!_bde.GetDocumentHandler(_bde.GetAccountDocument(docID, false).DocumentTypeID).CheckPostPermission(docID,null, m_Session))
                throw new AccessDeniedException();
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteAddAudit("Delete Generic Document attempt", docID.ToString(), -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    _bde.DeleteGenericDocument(AID, docID);
                    base.WriteAddAudit("Delete Generic Document Success", docID.ToString(), AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Generic Document Failure", docID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException("Error Delete Generic Docment " + docID.ToString(), ex);
                    throw;
                }
                finally
                {
                    bdeAccounting.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public void ReverseGenericDocument(int docID)
        {
            if (!_bde.GetDocumentHandler(_bde.GetAccountDocument(docID, false).DocumentTypeID).CheckPostPermission(docID,null, m_Session))
                throw new Exception("Access Denied");
            lock (bdeAccounting.WriterHelper)
            {
                int AID = base.WriteAddAudit("Reverse Generic Document attempt", docID.ToString(), -1);
                try
                {
                    bdeAccounting.WriterHelper.BeginTransaction();
                    _bde.ReverseGenericDocument(AID, docID);
                    base.WriteAddAudit("Reverse Generic Document Success", docID.ToString(), AID);
                    bdeAccounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeAccounting.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Reverse Generic Document Failure", docID.ToString(), ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException("Error Reverse Generic Docment " + docID.ToString(), ex);
                    throw new Exception("Error trying to Reverse Generic Docment", ex);
                }
                finally
                {
                    bdeAccounting.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }
        public int PostGenericDocument(AccountDocument doc)
        {
            if (!_bde.GetDocumentHandler(_bde.GetDocumentTypeByType(doc.GetType()).id).CheckPostPermission(doc.AccountDocumentID, doc, base.m_Session))
                throw new AccessDeniedException();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Post Generic Document attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int docID = _bde.PostGenericDocument(AID, doc);
                    base.WriteAddAudit("Post Generic Document Success", docID.ToString(), AID);
                    _bde.WriterHelper.CommitTransaction();
                    return docID;
                }
                    
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Post Generic Document Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException("Error Post Generic Document", ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
    }
}
