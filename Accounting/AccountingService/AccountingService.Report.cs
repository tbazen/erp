using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using System.Linq;
namespace INTAPS.Accounting.Service
{
    public partial class AccountingService
    {
        public ReportDefination GetEHTMLData(int reportID)
        {
            try
            {
                return bdeAccounting.GetEHTMLDAta(reportID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
        }
        public string EvaluateEHTML(int reportID, object[] parameter, out string headerItems)
        {
            try
            {
                return bdeAccounting.EvaluateEHTML(reportID, parameter,out headerItems);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
        }
        public string EvaluateEHTML(int reportTypeID, EHTMLData data, object[] parameter, out string headerItems)
        {
            try
            {
                return bdeAccounting.EvaluateEHTML(reportTypeID, data, parameter, out headerItems);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
        }
        public INTAPS.Evaluator.EData EvaluateEHTML(int reportTypeID, EHTMLData data, object[] parameter, string symbol)
        {
            try
            {
                return bdeAccounting.EvaluateEHTML(reportTypeID, data, parameter, symbol);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
        }        
        public ReportType[] GetAllReportTypes()
        {
            try
            {
                return bdeAccounting.GetAllReportTypes();
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }

        }
        public ReportDefination[] GetReports(int reportTypeID)
        {
            try
            {
                return bdeAccounting.GetReports(reportTypeID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
        }
        public int GetReportDefID(int reportTypeID, string name)
        {
            try
            {
                return bdeAccounting.GetReportDefID(reportTypeID, name);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
            finally
            {
                //bdeAccounting.CheckTransactionIntegrity();
            }
        }
        public ReportDefination[] GetReportByCategory(int catID)
        {
            try
            {
                return bdeAccounting.GetReportByCategory(catID);
            }
            catch(Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
            finally
            {
                //bdeAccounting.CheckTransactionIntegrity();
            }
        }
        public ReportCategory[] GetChildReportCategories(int pid)
        {
            try
            {
                return bdeAccounting.GetChildReportCategories(pid);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message,ex);
            }
            finally
            {
                //bdeAccounting.CheckTransactionIntegrity();
            }
        }
        public INTAPS.Evaluator.FunctionDocumentation[] GetFunctionDocumentations(int reportTypeID)
        {
            try
            {
                var ret= bdeAccounting.GetFunctionDocumentations(reportTypeID);
                return ret;
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            finally
            {
                //bdeAccounting.CheckTransactionIntegrity();
            }
        }
    }
}

