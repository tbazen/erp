using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting.BDE;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingService : SessionObjectBase<AccountingBDE>
    {
        protected Accounting.BDE.AccountingBDE bdeAccounting;
        public AccountingService(UserSessionData session)
            : base(session, (Accounting.BDE.AccountingBDE)ApplicationServer.GetBDE("Accounting"))
        {
            bdeAccounting = _bde;
        }


        public AccountTransaction[] GetTransactions(int accountID, int itemID, bool filterByTime, DateTime from, DateTime to, int index, int pageSize, out int NResult, out AccountBalance bal)
        {
            try
            {
                return bdeAccounting.GetTransactions(accountID, itemID, filterByTime, from, to, index, pageSize, out NResult, out bal);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
           

        }
        public AccountTransaction GetTransaction(int tranID)
        {
            try
            {
                return bdeAccounting.GetTransaction(tranID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            

        }
        public DocumentType GetAccountingDocumentType(int typeID)
        {
            try
            {
                return bdeAccounting.GetAccountingDocumentType(typeID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            

        }
        public SerialBatch[] GetAllActiveSerialBatches()
        {
            try
            {
                return bdeAccounting.GetAllActiveSerialBatches();
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
           
        }
        public int GetNextSerial(int batchID)
        {
            try
            {
                return bdeAccounting.GetNextSerial(batchID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
        }
        public AccountDocument[] GetAccountDocuments(string query, int type, bool date, DateTime from, DateTime to, int index, int pageSize, out int NRecord)
        {
            try
            {
                return bdeAccounting.GetAccountDocuments(query, type, date, from, to, index, pageSize, out NRecord);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            

        }
        public DocumentType[] GetAllDocumentTypes()
        {
            try
            {
                return bdeAccounting.GetAllDocumentTypes();
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            
        }
        public DocumentType GetDocumentTypeByID(int typeID)
        {
            try
            {
                return bdeAccounting.GetDocumentTypeByID(typeID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            

        }
        public AccountTransaction[] GetTransactionOfBatch(int BatchID)
        {
            try
            {
                return bdeAccounting.GetTransactionsOfDocument(BatchID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
            

        }
        public AccountDocument GetAccountDocument(int documentID, bool fullData)
        {
            try
            {
                return bdeAccounting.GetAccountDocument(documentID, fullData);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }
           

        }

        public ImageItem LoadImage(int imageID, int imageIndex, bool full, int Width, int Height)
        {
            return bdeAccounting.LoadImage(imageID, imageIndex, full, Width, Height);
        }
        public void GetImageSize(int imageID, int imageIndex, out int width, out int height, out string title)
        {
            bdeAccounting.GetImageSize(imageID, imageIndex, out width, out height, out title);
        }
        public ImageItem[] GetImageItems(int imageID)
        {
            return bdeAccounting.GetImageItems(imageID);
        }


        public DocumentType GetDocumentTypeByType(Type type)
        {
            return bdeAccounting.GetDocumentTypeByType(type);
        }

        public int GetTransactionNumber()
        {
            return bdeAccounting.GetTransactionNumber();
        }
        public double GetProccessProgress(out string message)
        {
            return _bde.GetProccessProgress(out message);
        }
        public void StopLongProccess()
        {
            _bde.StopLongProccess();
        }
        public void MergeTransaction(int[] sources, int destination, bool byDate, DateTime from, DateTime to)
        {
            if (!HasAccountManagerPermission())
                throw new Exception("You don't have permission to do this.");
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Merge Transactions Attempt", "", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.MergeTransaction(sources, destination, byDate, from, to);
                    base.WriteAddAudit("Merge Transactions Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Merge Transactions Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }

            }
        }
        public INTAPS.Accounting.AccountTransaction[] GetLedger(int resAccount, int resItem, int[] accountID, int[] itemID, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, out int NRecord)
        {
            return _bde.GetLedger(resAccount, resItem, accountID, itemID, dateFrom, dateTo, pageIndex, pageSize, out NRecord);
        }
        //moved from material
        public Object InvokeDocumentHandlerMethod(int typeID, string method, object[] pars)
        {
            return _bde.InvokeDocumentHandlerMethod(typeID, method, pars);
        }

        public string GetDocumentHTML(int accountDocumentID)
        {
            return _bde.GetDocumentHTML(accountDocumentID);
        }
        public DocumentHandler[] GetAllDocumentHandlerInfo()
        {
            return _bde.GetAllDocumentHandlerInfo();
        }

        public string GetDocumentHTML(AccountDocument doc)
        {
            return _bde.GetDocumentHTML(doc);
        }

        public DateTime getServerTime()
        {
            
            return _bde.getServerTime();
        }

        public INTAPS.Accounting.DocumentSerialType[] getAllDocumentSerialTypes()
        {
            return _bde.getAllDocumentSerialTypes();
        }

        public INTAPS.Accounting.DocumentSerialBatch[] getAllDocumentSerialBatches(int serialTypeID)
        {
            return _bde.getAllDocumentSerialBatches(serialTypeID);
        }

        public int createDocumentSerialType(INTAPS.Accounting.DocumentSerialType serialType)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createDocumentSerialType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createDocumentSerialType(AID, serialType);
                    base.WriteExecuteAudit("createDocumentSerialType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createDocumentSerialType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateDocumentSerialType(INTAPS.Accounting.DocumentSerialType serialType)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateDocumentSerialType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateDocumentSerialType(AID, serialType);
                    base.WriteExecuteAudit("updateDocumentSerialType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateDocumentSerialType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteDocumentSerialType(int documentSerialTypeID)
        {
            CheckAccountsAdminRole();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteDocumentSerialType Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteDocumentSerialType(AID, documentSerialTypeID);
                    base.WriteExecuteAudit("deleteDocumentSerialType Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteDocumentSerialType Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int createDocumentSerialBatch(INTAPS.Accounting.DocumentSerialBatch serialBatch)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("createDocumentSerialBatch Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int ret = _bde.createDocumentSerialBatch(AID, serialBatch);
                    base.WriteExecuteAudit("createDocumentSerialBatch Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("createDocumentSerialBatch Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void updateDocumentSerialBatch(INTAPS.Accounting.DocumentSerialBatch serialBatch)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("updateDocumentSerialBatch Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.updateDocumentSerialBatch(AID, serialBatch);
                    base.WriteExecuteAudit("updateDocumentSerialBatch Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("updateDocumentSerialBatch Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void deleteDocumentSerialBatch(int serialBatchID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteDocumentSerialBatch Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteDocumentSerialBatch(AID, serialBatchID);
                    base.WriteExecuteAudit("deleteDocumentSerialBatch Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteDocumentSerialBatch Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.Accounting.DocumentSerialType getDocumentSerialType(string prefix)
        {
            return _bde.getDocumentSerialType(prefix);
        }

        public INTAPS.Accounting.DocumentSerialType getDocumentSerialType(int serialTypeID)
        {
            return _bde.getDocumentSerialType(serialTypeID);
        }

        public int[] getDocumentListByTypedReference(INTAPS.Accounting.DocumentTypedReference tref)
        {
            return _bde.getDocumentListByTypedReference(tref);
        }

        public void voidSerialNo(System.DateTime time, INTAPS.Accounting.DocumentTypedReference reference, string reason)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("voidSerialNo Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.voidSerialNo(AID, time, reference, reason);
                    base.WriteExecuteAudit("voidSerialNo Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("voidSerialNo Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public string formatSerialNo(int typeID, int serialNo)
        {
            return _bde.formatSerialNo(typeID, serialNo);
        }

        public int[] getAllChildAccounts<AccountType>(int pid) where AccountType : AccountBase, new()
        {
            return _bde.getAllChildAccounts<AccountType>(pid);
        }
        public string getDocumentEntriesHTML(int docID)
        {
            return _bde.getDocumentEntriesHTML(docID, true);
        }

        public bool isControlAccount<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            return _bde.isControlAccount<AccountType>(accountID);
        }

        public string EvaluateEHTML(string code, object[] parameter, out string headerItems)
        {
            return _bde.EvaluateEHTML(code, parameter, out headerItems);
        }

        public INTAPS.Accounting.ReportDefination GetReportDefinationInfo(string code)
        {
            return _bde.GetReportDefinationInfo(code);
        }

        public INTAPS.Accounting.DocumentSerial[] getDocumentSerials(int documentID)
        {
            return _bde.getDocumentSerials(documentID);
        }

        public INTAPS.Accounting.AccountBalance GetBalanceAfter(int AccountID, int ItemID, System.DateTime date)
        {
            return _bde.GetBalanceAfter(AccountID, ItemID, date);
        }

        public double GetNetBalanceAfter(int csAccountID, int itemID, System.DateTime date)
        {
            return _bde.GetNetBalanceAfter(csAccountID, itemID, date);
        }

        public int[] ExpandParents<AccountType>(int PID) where AccountType : AccountBase, new()
        {
            return _bde.ExpandParents<AccountType>(PID);
        }

        public INTAPS.Accounting.DocumentType GetDocumentTypeByTypeNoException(System.Type type)
        {
            return _bde.GetDocumentTypeByTypeNoException(type);
        }

        public void DeleteReport(int reportID)
        {
            CheckReportManagePermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeleteReport Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.DeleteReport(AID, reportID);
                    base.WriteExecuteAudit("DeleteReport Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteReport Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteChildAccounts<AccountType>(int accountID) where AccountType : AccountBase, new()
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeleteChildAccounts Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.DeleteChildAccounts<AccountType>(AID, accountID);
                    base.WriteExecuteAudit("DeleteChildAccounts Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteChildAccounts Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public INTAPS.Accounting.AccountDependancy[] getAccountDependancy(int documentID)
        {
            return _bde.getAccountDependancy(documentID);
        }



    }
}
