using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingController : RESTServerControllerBase<AccountingService>
    {
        #region GetTransactionItem
        public class GetTransactionItemPar
        {
            public string sessionID; public int itemID;
        }

        [HttpPost]
        public ActionResult GetTransactionItem(GetTransactionItemPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetTransactionItem(par.itemID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateAcount
        public class ActivateAcountPar
        {
            public string sessionID; public int AccountID; public DateTime ActivateDate;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult ActivateAcount(ActivateAcountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    ((AccountingService)SessionObject).ActivateAcount<Account>(par.AccountID, par.ActivateDate);
                else
                    ((AccountingService)SessionObject).ActivateAcount<CostCenter>(par.AccountID, par.ActivateDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateAccount
        public class CreateAccountPar
        {
            public string sessionID;
            public TypeObject ac;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult CreateAccount(CreateAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                var ac = par.ac.GetConverted();
                if (ac is Account)
                    return Json(((AccountingService)SessionObject).CreateAccount(ac as Account));
                else
                    return Json(((AccountingService)SessionObject).CreateAccount(ac as CostCenter));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateCostCenterAccount
        public class CreateCostCenterAccountPar
        {
            public string sessionID; public int costCenterID; public int accountID;
        }

        public int CreateCostCenterAccount(CreateCostCenterAccountPar par)
        {
            SetSessionObject(par.sessionID);
            return ((AccountingService)SessionObject).CreateCostCenterAccount(par.costCenterID, par.accountID);
        }
        #endregion
        #region DeactivateAccount
        public class DeactivateAccountPar
        {
            public string sessionID; public int AccountID; public DateTime DeactivateDate;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult DeactivateAccount(DeactivateAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    ((AccountingService)SessionObject).DeactivateAccount<Account>(par.AccountID, par.DeactivateDate);
                else
                    ((AccountingService)SessionObject).DeactivateAccount<CostCenter>(par.AccountID, par.DeactivateDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteAccount
        public class DeleteAccountPar
        {
            public string sessionID; public int accountID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult DeleteAccount(DeleteAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    ((AccountingService)SessionObject).DeleteAccount<Account>(par.accountID);
                else
                    ((AccountingService)SessionObject).DeleteAccount<CostCenter>(par.accountID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteCostCenterAccount
        public class DeleteCostCenterAccountPar
        {
            public string sessionID; public int csaID;
        }

        [HttpPost]
        public ActionResult DeleteCostCenterAccount(DeleteCostCenterAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).DeleteCostCenterAccount(par.csaID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccount
        public class GetAccountPar
        {
            public string sessionID; public int AccountID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetAccount(GetAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetAccount<Account>(par.AccountID));
                else
                    return Json(((AccountingService)SessionObject).GetAccount<CostCenter>(par.AccountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccount
        public class GetAccount2Par
        {
            public string sessionID; public string AccountCode;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetAccount2(GetAccount2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetAccount<Account>(par.AccountCode));
                else
                    return Json(((AccountingService)SessionObject).GetAccount<CostCenter>(par.AccountCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountID
        public class GetAccountIDPar
        {
            public string sessionID; public string AccountCode;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetAccountID(GetAccountIDPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetAccountID<Account>(par.AccountCode));
                else
                    return Json(((AccountingService)SessionObject).GetAccountID<CostCenter>(par.AccountCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        //For out NRecords Configuration;
        #region GetChildAcccount
        public class GetChildAcccountPar
        {
            public string sessionID; public int ParentAccount; public int index; public int pageSize;
            public String AccountType;
        }
        public class GetChildAcccountOut
        {
            public int NRecords;
            public TypeObject[] _ret;
        }

        [HttpPost]
        public ActionResult GetChildAcccount(GetChildAcccountPar par)
        {
            try
            {
                var _ret = new GetChildAcccountOut();
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    _ret._ret = ((AccountingService)SessionObject).GetChildAcccount<Account>(par.ParentAccount, par.index, par.pageSize, out _ret.NRecords).Select(x => new TypeObject(x)).ToArray();
                else
                    _ret._ret = ((AccountingService)SessionObject).GetChildAcccount<CostCenter>(par.ParentAccount, par.index, par.pageSize, out _ret.NRecords).Select(x => new TypeObject(x)).ToArray();
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetChildAllAcccount
        public class GetChildAllAcccountPar
        {
            public string sessionID; public int ParentAccount;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetChildAllAcccount(GetChildAllAcccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetChildAllAcccount<Account>(par.ParentAccount));
                else
                    return Json(((AccountingService)SessionObject).GetChildAllAcccount<CostCenter>(par.ParentAccount));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccount
        public class GetCostCenterAccountPar
        {
            public string sessionID; public int costCenterID; public int accountID;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccount(GetCostCenterAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetCostCenterAccount(par.costCenterID, par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccount2
        public class GetCostCenterAccount2Par
        {
            public string sessionID; public int csaID;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccount2(GetCostCenterAccount2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetCostCenterAccount(par.csaID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDefaultCostCenterAccount
        public class GetDefaultCostCenterAccountPar
        {
            public string sessionID; public int accountID;
        }

        [HttpPost]
        public ActionResult GetDefaultCostCenterAccount(GetDefaultCostCenterAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetDefaultCostCenterAccount(par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLeafAccounts
        public class GetLeafAccountsPar
        {
            public string sessionID; public int accountID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetLeafAccounts(GetLeafAccountsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetLeafAccounts<Account>(par.accountID));
                else
                    return Json(((AccountingService)SessionObject).GetLeafAccounts<CostCenter>(par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsControlOf
        public class IsControlOfPar
        {
            public string sessionID; public int acountID; public int testAccount;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult IsControlOf(IsControlOfPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).IsControlOf<Account>(par.acountID, par.testAccount));
                else
                    return Json(((AccountingService)SessionObject).IsControlOf<CostCenter>(par.acountID, par.testAccount));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsDirectControlOf
        public class IsDirectControlOfPar
        {
            public string sessionID; public int acountID; public int testAccount;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult IsDirectControlOf(IsDirectControlOfPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).IsDirectControlOf<Account>(par.acountID, par.testAccount));
                else
                    return Json(((AccountingService)SessionObject).IsDirectControlOf<CostCenter>(par.acountID, par.testAccount));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region LinkAccount
        public class LinkAccountPar
        {
            public string sessionID; public int Parent; public int Child;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult LinkAccount(LinkAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    ((AccountingService)SessionObject).LinkAccount<Account>(par.Parent, par.Child);
                else
                    ((AccountingService)SessionObject).LinkAccount<CostCenter>(par.Parent, par.Child);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchAccount
        public class SearchAccountPar
        {
            public string sessionID; public string query; public int category; public int index; public int pageSize;
            public String AccountType;
        }
        public class SearchAccountOut
        {
            public int NRecords;
            public TypeObject[] _ret;
        }

        [HttpPost]
        public ActionResult SearchAccount(SearchAccountPar par)
        {
            try
            {
                var _ret = new SearchAccountOut();
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    _ret._ret = ((AccountingService)SessionObject).SearchAccount<Account>(par.query, par.category, par.index, par.pageSize, out _ret.NRecords).Select(x=>new TypeObject(x)).ToArray();
                else
                    _ret._ret = ((AccountingService)SessionObject).SearchAccount<CostCenter>(par.query, par.category, par.index, par.pageSize, out _ret.NRecords).Select(x => new TypeObject(x)).ToArray();
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchAccount2
        public class SearchAccount2Par
        {
            public string sessionID; public string query; public int index; public int pageSize;
            public String AccountType;
        }
        public class SearchAccount2Out
        {
            public int NRecords;
            public TypeObject[] _ret;
        }

        [HttpPost]
        public ActionResult SearchAccount2(SearchAccount2Par par)
        {
            try
            {
                var _ret = new SearchAccount2Out();
                SetSessionObject(par.sessionID);

                if ("Account".Equals(par.AccountType))
                    _ret._ret = ((AccountingService)SessionObject).SearchAccount<Account>(par.query, par.index, par.pageSize, out _ret.NRecords).Select(x => new TypeObject(x)).ToArray();
                else
                    _ret._ret = ((AccountingService)SessionObject).SearchAccount<CostCenter>(par.query, par.index, par.pageSize, out _ret.NRecords).Select(x => new TypeObject(x)).ToArray();
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateAccount
        public class UpdateAccountPar
        {
            public string sessionID;
            public TypeObject ac;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult UpdateAccount(UpdateAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                var ac = par.ac.GetConverted();
                if (ac is Account)
                    ((AccountingService)SessionObject).UpdateAccount(ac as Account);
                else
                    ((AccountingService)SessionObject).UpdateAccount(ac as CostCenter);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsSingleCostCenter
        public class IsSingleCostCenterPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult IsSingleCostCenter(IsSingleCostCenterPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).IsSingleCostCenter());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccount3
        public class GetCostCenterAccount3Par
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccount3(GetCostCenterAccount3Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetCostCenterAccount(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccountWithDescription
        public class GetCostCenterAccountWithDescriptionPar
        {
            public string sessionID; public int csaID;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccountWithDescription(GetCostCenterAccountWithDescriptionPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetCostCenterAccountWithDescription(par.csaID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccountWithDescription2
        public class GetCostCenterAccountWithDescription2Par
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccountWithDescription2(GetCostCenterAccountWithDescription2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetCostCenterAccountWithDescription(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLedger
        public class GetLedgerPar
        {
            public string sessionID; public int resAccountID; public int resItemID; public int[] itemID; public DateTime dateFrom; public DateTime dateTo; public int pageIndex; public int pageSize;
        }
        public class GetLedgerParOut
        {
            public int NRecord; public AccountBalance begBal; public AccountBalance total;
            public INTAPS.Accounting.AccountTransaction[] _ret;
        }

        [HttpPost]
        public ActionResult GetLedger(GetLedgerPar par)
        {
            try
            {
                var _ret = new GetLedgerParOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)SessionObject).GetLedger(par.resAccountID, par.resItemID, par.itemID, par.dateFrom, par.dateTo, par.pageIndex, par.pageSize, out _ret.NRecord, out _ret.begBal, out _ret.total);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccountWithDescription3
        public class GetCostCenterAccountWithDescription3Par
        {
            public string sessionID; public int costCenterID; public int accountID;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccountWithDescription3(GetCostCenterAccountWithDescription3Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetCostCenterAccountWithDescription(par.costCenterID, par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region materializeDocument
        public class materializeDocumentPar
        {
            public string sessionID; public int docID; public DateTime materialzationDate;
        }

        [HttpPost]
        public ActionResult materializeDocument(materializeDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).materializeDocument(par.docID, par.materialzationDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region getMaterializePendingCount
        public class getMaterializePendingCountPar
        {
            public string sessionID; public DateTime time;
        }

        [HttpPost]
        public ActionResult getMaterializePendingCount(getMaterializePendingCountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).getMaterializePendingCount(par.time));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region getMaterializePendingDocumentIDs
        public class getMaterializePendingDocumentIDsPar
        {
            public string sessionID; public DateTime time;
        }

        [HttpPost]
        public ActionResult getMaterializePendingDocumentIDs(getMaterializePendingDocumentIDsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).getMaterializePendingDocumentIDs(par.time));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RescheduleAccountDocument
        public class RescheduleAccountDocumentPar
        {
            public string sessionID; public int docID; public DateTime time;
        }

        [HttpPost]
        public ActionResult RescheduleAccountDocument(RescheduleAccountDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).RescheduleAccountDocument(par.docID, par.time);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCostCenterAccountsOf
        public class GetCostCenterAccountsOfPar
        {
            public string sessionID; public int accountID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetCostCenterAccountsOf(GetCostCenterAccountsOfPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetCostCenterAccountsOf<Account>(par.accountID));
                else
                    return Json(((AccountingService)SessionObject).GetCostCenterAccountsOf<CostCenter>(par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ActivateCostCenterAccount
        public class ActivateCostCenterAccountPar
        {
            public string sessionID; public int csAccountID; public DateTime ActivateDate;
        }

        [HttpPost]
        public ActionResult ActivateCostCenterAccount(ActivateCostCenterAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).ActivateCostCenterAccount(par.csAccountID, par.ActivateDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DectivateCostCenterAccount
        public class DectivateCostCenterAccountPar
        {
            public string sessionID; public int csAccountID; public DateTime deactivationDate;
        }

        [HttpPost]
        public ActionResult DectivateCostCenterAccount(DectivateCostCenterAccountPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).DectivateCostCenterAccount(par.csAccountID, par.deactivationDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}