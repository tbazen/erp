using System;
using System.Collections.Generic;
using System.Text;

using INTAPS.Accounting;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingController : RESTServerControllerBase<AccountingService>
    {
        #region GetFlow
        public class GetFlowPar
        {
            public string sessionID; public int AccountID; public int ItemID; public DateTime from; public DateTime to;
        }

        [HttpPost]
        public ActionResult GetFlow(GetFlowPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetFlow(par.AccountID, par.ItemID, par.from, par.to));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEndNetBalance
        public class GetEndNetBalancePar
        {
            public string SessionID; public int AccountID; public int itemID;
        }

        [HttpPost]
        public ActionResult GetEndNetBalance(GetEndNetBalancePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((AccountingService)SessionObject).GetEndNetBalance(par.AccountID, par.itemID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEndBalance
        public class GetEndBalancePar
        {
            public string sessionID; public int accountID; public int itemID;
        }

        [HttpPost]
        public ActionResult GetEndBalance(GetEndBalancePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetEndBalance(par.accountID, par.itemID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEndBalances
        public class GetEndBalancesPar
        {
            public string sessionID; public int accountID;
        }

        [HttpPost]
        public ActionResult GetEndBalances(GetEndBalancesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetEndBalances(par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEndItemBalances
        public class GetEndItemBalancesPar
        {
            public string sessionID; public int itemID;
        }

        [HttpPost]
        public ActionResult GetEndItemBalances(GetEndItemBalancesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetEndItemBalances(par.itemID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetNetBalanceAsOf
        public class GetNetBalanceAsOfPar
        {
            public string sessionID; public int csAccountID; public int itemID; public DateTime date;
        }

        [HttpPost]
        public ActionResult GetNetBalanceAsOf(GetNetBalanceAsOfPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetNetBalanceAsOf(par.csAccountID, par.itemID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBalanceAsOf
        public class GetBalanceAsOfPar
        {
            public string sessionID; public int AccountID; public int ItemID; public DateTime date;
        }

        [HttpPost]
        public ActionResult GetBalanceAsOf(GetBalanceAsOfPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetBalanceAsOf(par.AccountID, par.ItemID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBalancesAsOf2
        public class GetBalancesAsOf2Par
        {
            public string sessionID; public int accountID; public DateTime date;
        }

        [HttpPost]
        public ActionResult GetBalancesAsOf2(GetBalancesAsOf2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetBalancesAsOf(par.accountID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
