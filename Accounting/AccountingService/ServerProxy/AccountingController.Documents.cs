using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;
using System;

namespace INTAPS.Accounting.Service
{

    public partial class AccountingController : RESTServerControllerBase<AccountingService>
    {
        #region GetAccountingDocumentType
        public class GetAccountingDocumentTypePar
        {
            public string SessionID; public int typeID;
        }

        [HttpPost]
        public ActionResult GetAccountingDocumentType(GetAccountingDocumentTypePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((AccountingService)SessionObject).GetAccountingDocumentType(par.typeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountDocuments
        public class GetAccountDocumentsPar
        {
            public string sessionID; public string query; public int type; public bool date; public DateTime from; public DateTime to; public int index; public int pageSize;
        }
        public class GetAccountDocumentsOut
        {
            public int NRecord;
            public AccountDocument[] _ret;
        }

        [HttpPost]
        public ActionResult GetAccountDocuments(GetAccountDocumentsPar par)
        {
            try
            {
                var _ret = new GetAccountDocumentsOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)SessionObject).GetAccountDocuments(par.query, par.type, par.date, par.from, par.to, par.index, par.pageSize, out _ret.NRecord);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteAccountDocument
        public class DeleteAccountDocumentPar
        {
            public string sessionID; public int documentID;
        }

        [HttpPost]
        public ActionResult DeleteAccountDocument(DeleteAccountDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).DeleteAccountDocument(par.documentID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ReverseAccountDocument
        public class ReverseAccountDocumentPar
        {
            public string sessionID; public int documentID;
        }

        [HttpPost]
        public ActionResult ReverseAccountDocument(ReverseAccountDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).ReverseAccountDocument(par.documentID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllDocumentTypes
        public class GetAllDocumentTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllDocumentTypes(GetAllDocumentTypesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetAllDocumentTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentTypeByID
        public class GetDocumentTypeByIDPar
        {
            public string sessionID; public int typeID;
        }

        [HttpPost]
        public ActionResult GetDocumentTypeByID(GetDocumentTypeByIDPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetDocumentTypeByID(par.typeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountDocument
        public class GetAccountDocumentPar
        {
            public string sessionID; public int documentID; public bool fullData;
        }

        [HttpPost]
        public ActionResult GetAccountDocument(GetAccountDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetAccountDocument(par.documentID, par.fullData));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentTypeByType
        public class GetDocumentTypeByTypePar
        {
            public string sessionID; public Type type;
        }

        [HttpPost]
        public ActionResult GetDocumentTypeByType(GetDocumentTypeByTypePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetDocumentTypeByType(par.type));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region GetDocumentTypeByTypeWeb
        public class GetDocumentTypeByTypeWebPar
        {
            public string sessionID; public string type;
        }

        [HttpPost]
        public ActionResult GetDocumentTypeByTypeWeb(GetDocumentTypeByTypeWebPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                Type type = Type.GetType(par.type);
                return Json(((AccountingService)SessionObject).GetDocumentTypeByType(type));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion



        #region InvokeDocumentHandlerMethod
        public class InvokeDocumentHandlerMethodPar
        {
            public string sessionID; public int typeID; public string method; public object[] pars;
        }

        [HttpPost]
        public ActionResult InvokeDocumentHandlerMethod(InvokeDocumentHandlerMethodPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).InvokeDocumentHandlerMethod(par.typeID, par.method, par.pars));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentHTML
        public class GetDocumentHTMLPar
        {
            public string sessionID; public int accountDocumentID;
        }

        [HttpPost]
        public ActionResult GetDocumentHTML(GetDocumentHTMLPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetDocumentHTML(par.accountDocumentID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region PostGenericDocument
        public class PostGenericDocumentPar
        {
            public string sessionID; public BinObject doc;
        }

        [HttpPost]
        public ActionResult PostGenericDocument(PostGenericDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).PostGenericDocument(par.doc.Deserialized() as AccountDocument));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion


        #region PostGenericDocumentWeb
        public class PostGenericDocumentWebPar
        {
            public string sessionID; public object doc; public string dataType;
        }

        [HttpPost]
        public ActionResult PostGenericDocumentWeb(PostGenericDocumentWebPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                var type = Type.GetType(par.dataType);
                var obj = par.doc != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(par.doc.ToString(), type) : null;
                var binObject = new BinObject(obj);
                return PostGenericDocument(new PostGenericDocumentPar { sessionID = par.sessionID, doc = new BinObject(obj) });
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion


        #region GetAllDocumentHandlerInfo
        public class GetAllDocumentHandlerInfoPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllDocumentHandlerInfo(GetAllDocumentHandlerInfoPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetAllDocumentHandlerInfo());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteGenericDocument
        public class DeleteGenericDocumentPar
        {
            public string sessionID; public int docID;
        }

        [HttpPost]
        public ActionResult DeleteGenericDocument(DeleteGenericDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).DeleteGenericDocument(par.docID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ReverseGenericDocument
        public class ReverseGenericDocumentPar
        {
            public string sessionID; public int docID;
        }

        [HttpPost]
        public ActionResult ReverseGenericDocument(ReverseGenericDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).ReverseGenericDocument(par.docID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentHTML2
        public class GetDocumentHTML2Par
        {
            public string sessionID; public BinObject doc;
        }

        [HttpPost]
        public ActionResult GetDocumentHTML2(GetDocumentHTML2Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetDocumentHTML(par.doc.Deserialized() as AccountDocument));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region ReverseDocument
        public class ReverseDocumentPar
        {
            public string sessionID; public TransactionReversalDocument r;
        }

        [HttpPost]
        public ActionResult ReverseDocument(ReverseDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).ReverseDocument(par.r);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentEntriesHTML
        public class GetDocumentEntriesHTMLPar
        {
            public string sessionID; public int docID;
        }

        [HttpPost]
        public ActionResult getDocumentEntriesHTML(GetDocumentEntriesHTMLPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).getDocumentEntriesHTML(par.docID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
