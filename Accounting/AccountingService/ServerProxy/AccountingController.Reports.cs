using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingController : RESTServerControllerBase<AccountingService>
    {
        #region SaveEHTMLData
        public class SaveEHTMLDataPar
        {
            public string sessionID; public ReportDefination r;
        }

        [HttpPost]
        public ActionResult SaveEHTMLData(SaveEHTMLDataPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).SaveEHTMLData(par.r));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEHTMLData
        public class GetEHTMLDataPar
        {
            public string sessionID; public int reportID;
        }

        [HttpPost]
        public ActionResult GetEHTMLData(GetEHTMLDataPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetEHTMLData(par.reportID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EvaluateEHTML4
        public class EvaluateEHTML4Par
        {
            public string sessionID; public int reportID; public BinObject parameter;
        }
        public class EvaluateEHTML4Out
        {
            public string headerItems;
            public String _ret;
        }
        public ActionResult EvaluateEHTML4(EvaluateEHTML4Par par)
        {
            try
            {
                var _ret = new EvaluateEHTML4Out();
                SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)SessionObject).EvaluateEHTML(par.reportID, par.parameter.Deserialized() as object[], out _ret.headerItems);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EvaluateEHTML2
        public class EvaluateEHTML2Par
        {
            public string sessionID; public int reportTypeID; public EHTMLData data; public TypeObject[] parameter;
        }
        public class EvaluateEHTML2Out
        {
            public string headerItems;
            public string _ret;
        }
        public ActionResult EvaluateEHTML2(EvaluateEHTML2Par par)
        {
            try
            {
                var _ret = new EvaluateEHTML2Out();
                SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)SessionObject).EvaluateEHTML(par.reportTypeID, par.data, par.parameter.Select(x => x.GetConverted()).ToArray(), out _ret.headerItems);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllReportTypes
        public class GetAllReportTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllReportTypes(GetAllReportTypesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetAllReportTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReports
        public class GetReportsPar
        {
            public string sessionID; public int reportTypeID;
        }

        [HttpPost]
        public ActionResult GetReports(GetReportsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetReports(par.reportTypeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetFunctionDocumentations
        public class GetFunctionDocumentationsPar
        {
            public string sessionID; public int reportTypeID;
        }

        [HttpPost]
        public ActionResult GetFunctionDocumentations(GetFunctionDocumentationsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetFunctionDocumentations(par.reportTypeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EvaluateEHTML3
        public class EvaluateEHTML3Par
        {
            public string sessionID;
            public int reportTypeID;
            public EHTMLData data;
            public TypeObject[] parameter;
            public string symbol;
        }
        public ActionResult EvaluateEHTML3(EvaluateEHTML3Par par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                var ret = ((AccountingService)SessionObject).EvaluateEHTML(par.reportTypeID, par.data, par.parameter.Select(x => x.GetConverted()).ToArray(), par.symbol);
                return Json(new BinObject(ret));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EvaluateEHTML3WebPar
        public class EvaluateEHTML3WebPar
        {
            public string sessionID;
            public int reportTypeID;
            public EHTMLData data;
            public TypeObject[] parameter;
            public string symbol;
        }
        public class EvaluateEHTML3WebOut
        {
            public INTAPS.Evaluator.EData edata;
            public string edataString;
        }
        public ActionResult EvaluateEHTML3Web(EvaluateEHTML3WebPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                INTAPS.Evaluator.EData ret = ((AccountingService)SessionObject).EvaluateEHTML(par.reportTypeID, par.data, par.parameter.Select(x => x.GetConverted()).ToArray(), par.symbol);

                return Json(new EvaluateEHTML3WebOut { edata = ret, edataString = ret.ToString() });
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReportByCategory
        public class GetReportByCategoryPar
        {
            public string sessionID; public int catID;
        }

        [HttpPost]
        public ActionResult GetReportByCategory(GetReportByCategoryPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetReportByCategory(par.catID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetChildReportCategories
        public class GetChildReportCategoriesPar
        {
            public string sessionID; public int pid;
        }

        [HttpPost]
        public ActionResult GetChildReportCategories(GetChildReportCategoriesPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetChildReportCategories(par.pid));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveReportCategory
        public class SaveReportCategoryPar
        {
            public string sessionID; public ReportCategory cat;
        }

        [HttpPost]
        public ActionResult SaveReportCategory(SaveReportCategoryPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).SaveReportCategory(par.cat));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteReportCategory
        public class DeleteReportCategoryPar
        {
            public string sessionID; public int catID;
        }

        [HttpPost]
        public ActionResult DeleteReportCategory(DeleteReportCategoryPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).DeleteReportCategory(par.catID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReportDefID
        public class GetReportDefIDPar
        {
            public string sessionID; public int reportTypeID; public string name;
        }

        [HttpPost]
        public ActionResult GetReportDefID(GetReportDefIDPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetReportDefID(par.reportTypeID, par.name));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJoinedAccountIDs
        public class GetJoinedAccountIDsPar
        {
            public string sessionID; public int costCenterID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult GetJoinedAccountIDs(GetJoinedAccountIDsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)SessionObject).GetJoinedAccountIDs<Account>(par.costCenterID));
                else
                    return Json(((AccountingService)SessionObject).GetJoinedAccountIDs<CostCenter>(par.costCenterID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
