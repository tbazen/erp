using System;
using System.Collections.Generic;
using System.Text;

using INTAPS.Accounting;
using INTAPS.Accounting.Service;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;

namespace INTAPS.Accounting.Service
{
    public partial class AccountingController : RESTServerControllerBase<AccountingService>
    {
        #region GetTransaction
        public class GetTransactionPar
        {
            public string SessionID; public int tranID;
        }

        [HttpPost]
        public ActionResult GetTransaction(GetTransactionPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((AccountingService)SessionObject).GetTransaction(par.tranID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTransactions
        public class GetTransactionsPar
        {
            public string SessionID; public int accountID; public int itemID; public bool filterByTime; public DateTime from; public DateTime to;
            public int index; public int pageSize;
        }
        public class GetTransactionsOut
        {
            public int NResult; public AccountBalance bal;
            public AccountTransaction[] _ret;
        }
        public GetTransactionsOut GetTransactions(GetTransactionsPar par)
        {
            var _ret = new GetTransactionsOut();
            SetSessionObject(par.SessionID);
            _ret._ret = ((AccountingService)SessionObject).GetTransactions(par.accountID, par.itemID, par.filterByTime, par.from, par.to, par.index, par.pageSize
                , out _ret.NResult, out _ret.bal);
            return _ret;
        }
        #endregion
        #region GetTransactionOfDocument
        public class GetTransactionOfDocumentPar
        {
            public string sessionID; public int BatchID;
        }

        [HttpPost]
        public ActionResult GetTransactionOfDocument(GetTransactionOfDocumentPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetTransactionOfBatch(par.BatchID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetTransactionNumber
        public class GetTransactionNumberPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetTransactionNumber(GetTransactionNumberPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetTransactionNumber());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region MergeTransaction
        public class MergeTransactionPar
        {
            public string sessionID; public int[] sources; public int destination; public bool byDate; public DateTime from; public DateTime to;
        }

        [HttpPost]
        public ActionResult MergeTransaction(MergeTransactionPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).MergeTransaction(par.sources, par.destination, par.byDate, par.from, par.to);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLedger2 
        public class GetLedger2Par
        {
            public string sessionID; public int resAccount; public int resItem; public int[] accountID; public int[] itemID; public DateTime dateFrom; public DateTime dateTo; public int pageIndex; public int pageSize;
        }
        public class GetLedger2Out
        {
            public int NRecord;
            public INTAPS.Accounting.AccountTransaction[] _ret;
        }

        [HttpPost]
        public ActionResult GetLedger2(GetLedger2Par par)
        {
            try
            {
                var _ret = new GetLedger2Out();
                SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)SessionObject).GetLedger(par.resAccount, par.resItem, par.accountID, par.itemID, par.dateFrom, par.dateTo, par.pageIndex, par.pageSize, out _ret.NRecord);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
