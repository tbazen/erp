using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace INTAPS.Accounting.Service
{
    [Route("api/erp/[controller]/[action]")]
    [ApiController]
    public partial class AccountingController : RESTServerControllerBase<AccountingService>
    {

        #region CreateBatch
        public class CreateBatchPar
        {
            public string SessionID; public SerialBatch batch;
        }

        [HttpPost]
        public ActionResult CreateBatch(CreateBatchPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((AccountingService)SessionObject).CreateBatch(par.batch));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllActiveSerialBatches
        public class GetAllActiveSerialBatchesPar
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult GetAllActiveSerialBatches(GetAllActiveSerialBatchesPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((AccountingService)SessionObject).GetAllActiveSerialBatches());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteSerialBatch
        public class DeleteSerialBatchPar
        {
            public string SessionID; public int id;
        }

        [HttpPost]
        public ActionResult DeleteSerialBatch(DeleteSerialBatchPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((AccountingService)SessionObject).DeleteSerialBatch(par.id);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetNextSerial
        public class GetNextSerialPar
        {
            public string SessionID; public int batchID;
        }

        [HttpPost]
        public ActionResult GetNextSerial(GetNextSerialPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((AccountingService)SessionObject).GetNextSerial(par.batchID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UseSerial
        public class UseSerialPar
        {
            public string SessionID; public UsedSerial ser;
        }

        [HttpPost]
        public ActionResult UseSerial(UseSerialPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((AccountingService)SessionObject).UseSerial(par.ser);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region LoadImage
        public class LoadImagePar
        {
            public string sessionID; public int imageID; public int imageIndex; public bool full; public int Width; public int Height;
        }

        [HttpPost]
        public ActionResult LoadImage(LoadImagePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).LoadImage(par.imageID, par.imageIndex, par.full, par.Width, par.Height));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetImageSize
        public class GetImageSizePar
        {
            public string sessionID; public int imageID; public int imageIndex;
        }
        public class GetImageSizeOut
        {
            public int width; public int height; public string title;

        }

        [HttpPost]
        public ActionResult GetImageSize(GetImageSizePar par)
        {
            try
            {
                var _ret = new GetImageSizeOut();
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).GetImageSize(par.imageID, par.imageIndex, out _ret.width, out _ret.height, out _ret.title);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetImageItems
        public class GetImageItemsPar
        {
            public string sessionID; public int imageID;
        }

        [HttpPost]
        public ActionResult GetImageItems(GetImageItemsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).GetImageItems(par.imageID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetProccessProgress
        public class GetProccessProgressPar
        {
            public string sessionID;
        }
        public class GetProccessProgressOut
        {
            public string message;
            public double _ret;
        }

        [HttpPost]
        public ActionResult GetProccessProgress(GetProccessProgressPar par)
        {
            try
            {
                var _ret = new GetProccessProgressOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)SessionObject).GetProccessProgress(out _ret.message);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region StopLongProccess
        public class StopLongProccessPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult StopLongProccess(StopLongProccessPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((AccountingService)SessionObject).StopLongProccess();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetServerTime
        public class GetServerTimePar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getServerTime(GetServerTimePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                return Json(((AccountingService)SessionObject).getServerTime());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllDocumentSerialTypes
        public class GetAllDocumentSerialTypesPar
        {
            public string sessionID;
        }
        public INTAPS.Accounting.DocumentSerialType[] getAllDocumentSerialTypes(GetAllDocumentSerialTypesPar par)
        {
            this.SetSessionObject(par.sessionID);
            return ((AccountingService)base.SessionObject).getAllDocumentSerialTypes();
        }
        #endregion
        #region GetAllDocumentSerialBatches
        public class GetAllDocumentSerialBatchesPar
        {
            public string sessionID; public int serialTypeID;
        }

        [HttpPost]
        public ActionResult getAllDocumentSerialBatches(GetAllDocumentSerialBatchesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).getAllDocumentSerialBatches(par.serialTypeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateDocumentSerialType
        public class CreateDocumentSerialTypePar
        {
            public string sessionID; public INTAPS.Accounting.DocumentSerialType serialType;
        }

        [HttpPost]
        public ActionResult createDocumentSerialType(CreateDocumentSerialTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).createDocumentSerialType(par.serialType));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateDocumentSerialType
        public class UpdateDocumentSerialTypePar
        {
            public string sessionID; public INTAPS.Accounting.DocumentSerialType serialType;
        }

        [HttpPost]
        public ActionResult updateDocumentSerialType(UpdateDocumentSerialTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((AccountingService)base.SessionObject).updateDocumentSerialType(par.serialType);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteDocumentSerialType
        public class DeleteDocumentSerialTypePar
        {
            public string sessionID; public int documentSerialTypeID;
        }

        [HttpPost]
        public ActionResult deleteDocumentSerialType(DeleteDocumentSerialTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((AccountingService)base.SessionObject).deleteDocumentSerialType(par.documentSerialTypeID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateDocumentSerialBatch
        public class CreateDocumentSerialBatchPar
        {
            public string sessionID; public INTAPS.Accounting.DocumentSerialBatch serialBatch;
        }

        [HttpPost]
        public ActionResult createDocumentSerialBatch(CreateDocumentSerialBatchPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).createDocumentSerialBatch(par.serialBatch));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateDocumentSerialBatch
        public class UpdateDocumentSerialBatchPar
        {
            public string sessionID; public INTAPS.Accounting.DocumentSerialBatch serialBatch;
        }

        [HttpPost]
        public ActionResult updateDocumentSerialBatch(UpdateDocumentSerialBatchPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((AccountingService)base.SessionObject).updateDocumentSerialBatch(par.serialBatch);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteDocumentSerialBatch
        public class DeleteDocumentSerialBatchPar
        {
            public string sessionID; public int serialBatchID;
        }

        [HttpPost]
        public ActionResult deleteDocumentSerialBatch(DeleteDocumentSerialBatchPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((AccountingService)base.SessionObject).deleteDocumentSerialBatch(par.serialBatchID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentSerialType
        public class GetDocumentSerialTypePar
        {
            public string sessionID; public string prefix;
        }

        [HttpPost]
        public ActionResult getDocumentSerialType(GetDocumentSerialTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).getDocumentSerialType(par.prefix));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentSerialType2
        public class GetDocumentSerialType2Par
        {
            public string sessionID; public int serialTypeID;
        }

        [HttpPost]
        public ActionResult getDocumentSerialType2(GetDocumentSerialType2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).getDocumentSerialType(par.serialTypeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentListByTypedReference
        public class GetDocumentListByTypedReferencePar
        {
            public string sessionID; public INTAPS.Accounting.DocumentTypedReference tref;
        }

        [HttpPost]
        public ActionResult getDocumentListByTypedReference(GetDocumentListByTypedReferencePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).getDocumentListByTypedReference(par.tref));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region VoidSerialNo
        public class VoidSerialNoPar
        {
            public string sessionID; public System.DateTime time; public INTAPS.Accounting.DocumentTypedReference reference; public string reason;
        }

        [HttpPost]
        public ActionResult voidSerialNo(VoidSerialNoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((AccountingService)base.SessionObject).voidSerialNo(par.time, par.reference, par.reason);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region FormatSerialNo
        public class FormatSerialNo
        {
            public string sessionID; public int typeID; public int serialNo;
        }

        [HttpPost]
        public ActionResult formatSerialNo(FormatSerialNo par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).formatSerialNo(par.typeID, par.serialNo));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllChildAccounts
        public class GetAllChildAccounts
        {
            public string sessionID; public int pid;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult getAllChildAccounts(GetAllChildAccounts par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)base.SessionObject).getAllChildAccounts<Account>(par.pid));
                else
                    return Json(((AccountingService)base.SessionObject).getAllChildAccounts<CostCenter>(par.pid));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsControlAccount
        public class IsControlAccount
        {
            public string sessionID; public int accountID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult isControlAccount(IsControlAccount par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)base.SessionObject).isControlAccount<Account>(par.accountID));
                else
                    return Json(((AccountingService)base.SessionObject).isControlAccount<CostCenter>(par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EvaluateEHTML
        public class EvaluateEHTMLPar
        {
            public string sessionID;
            public string code;
            public BinObject parameter;
        }
        public class EvaluateEHTMLOut
        {
            public string headerItems;
            public String _ret;
        }
        public ActionResult EvaluateEHTML(EvaluateEHTMLPar par)
        {
            try
            {
                var _ret = new EvaluateEHTMLOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((AccountingService)base.SessionObject).EvaluateEHTML(par.code, par.parameter.Deserialized() as object[], out _ret.headerItems);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetReportDefinationInfo
        public class GetReportDefinationInfoPar
        {
            public string sessionID; public string code;
        }

        [HttpPost]
        public ActionResult GetReportDefinationInfo(GetReportDefinationInfoPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).GetReportDefinationInfo(par.code));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentSerials
        public class GetDocumentSerialsPar
        {
            public string sessionID; public int documentID;
        }

        [HttpPost]
        public ActionResult getDocumentSerials(GetDocumentSerialsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).getDocumentSerials(par.documentID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBalanceAfter
        public class GetBalanceAfterPar
        {
            public string sessionID; public int AccountID; public int ItemID; public System.DateTime date;
        }

        [HttpPost]
        public ActionResult GetBalanceAfter(GetBalanceAfterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).GetBalanceAfter(par.AccountID, par.ItemID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetNetBalanceAfter
        public class GetNetBalanceAfterPar
        {
            public string sessionID; public int csAccountID; public int itemID; public System.DateTime date;
        }

        [HttpPost]
        public ActionResult GetNetBalanceAfter(GetNetBalanceAfterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).GetNetBalanceAfter(par.csAccountID, par.itemID, par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ExpandParents
        public class ExpandParentsPar
        {
            public string sessionID; public int PID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult ExpandParents(ExpandParentsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    return Json(((AccountingService)base.SessionObject).ExpandParents<Account>(par.PID));
                else
                    return Json(((AccountingService)base.SessionObject).ExpandParents<CostCenter>(par.PID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDocumentTypeByTypeNoException
        public class GetDocumentTypeByTypeNoExceptionPar
        {
            public string sessionID; public System.Type type;
        }

        [HttpPost]
        public ActionResult GetDocumentTypeByTypeNoException(GetDocumentTypeByTypeNoExceptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).GetDocumentTypeByTypeNoException(par.type));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteReport
        public class DeleteReportPar
        {
            public string sessionID; public int reportID;
        }

        [HttpPost]
        public ActionResult DeleteReport(DeleteReportPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((AccountingService)base.SessionObject).DeleteReport(par.reportID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteChildAccounts
        public class DeleteChildAccountsPar
        {
            public string sessionID; public int accountID;
            public String AccountType;
        }

        [HttpPost]
        public ActionResult DeleteChildAccounts(DeleteChildAccountsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                if ("Account".Equals(par.AccountType))
                    ((AccountingService)base.SessionObject).DeleteChildAccounts<Account>(par.accountID);
                else
                    ((AccountingService)base.SessionObject).DeleteChildAccounts<CostCenter>(par.accountID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAccountDependancy
        public class GetAccountDependancyPar
        {
            public string sessionID; public int documentID;
        }

        [HttpPost]
        public ActionResult getAccountDependancy(GetAccountDependancyPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((AccountingService)base.SessionObject).getAccountDependancy(par.documentID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
