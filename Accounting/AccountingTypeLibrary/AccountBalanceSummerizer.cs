﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting
{
    public class AccountBalanceSummerizer:ICollection<AccountBalance>
    {
        List<AccountBalance> _list = new List<AccountBalance>();


        public void Add(AccountBalance item)
        {
            foreach (AccountBalance bal in _list)
            {
                if (bal.AccountID == item.AccountID && bal.ItemID == item.ItemID)
                {
                    bal.AddBalance(item);
                    break;
                }
            }
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(AccountBalance item)
        {
            throw new InvalidOperationException();
        }

        public void CopyTo(AccountBalance[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(AccountBalance item)
        {
            throw new InvalidOperationException();
        }

        public IEnumerator<AccountBalance> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}
