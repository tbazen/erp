using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using INTAPS.RDBMS;
using System.Runtime.Serialization;

namespace INTAPS.Accounting
{
    public enum DocumentStatus
    {
        Undefined,
        Draft,
        Proposed,
        Rejected,
        Activated,
        Deactiavted
    }

    [Serializable]
    public class DocumentStatusInfo
    {
        public DocumentStatus[] HistoricalStatus;
        public DateTime[] HistoricalDates;
        public DocumentStatus Status
        {
            get
            {
                if (HistoricalStatus == null || HistoricalStatus.Length == 0)
                    return DocumentStatus.Undefined;
                return HistoricalStatus[0];
            }
        }
        public DateTime StatusDate
        {
            get
            {
                if (HistoricalDates == null || HistoricalStatus.Length == 0)
                    return DateTime.MinValue;
                return HistoricalDates[0];
            }
        }
    }
    
    [XmlInclude(typeof(DocumentStatusInfo))]
    [Serializable]
    [SingleTableObject("Document", orderBy = "tranTicks desc,ID desc",inheritAllFields=false)]
    public class AccountDocument
    {
        public static double FUTURE_TOLERANCE_SECONDS = 120;
        public bool IsFutureDate
        {
            get
            {
                return DocumentDate.Subtract(DateTime.Now).TotalSeconds > FUTURE_TOLERANCE_SECONDS;
            }
        }

        [IDField("ID")]
        public int AccountDocumentID=-1;
        [DataField(false)]
        public int DocumentTypeID;
        [DataField(false),XmlIgnore]
        public long tranTicks = -1;
        public DateTime DocumentDate
        {
            get
            {
                return new DateTime(tranTicks);
            }
            set
            {
                tranTicks = value.Ticks;
            }
        }

        [DataField]
        public string PaperRef;
        [DataField]
        public string ShortDescription;
        [DataField]
        public string LongDescription;
        [DataField]
        public bool reversed;
        [DataField]
        public bool scheduled=false;
        [DataField]
        public bool materialized=false;
        [DataField]
        public DateTime materializedOn=DateTime.Now;

       

        public AccountDocument()
        {
            this.DocumentTypeID = -1; 
            AccountDocumentID = -1;
            DocumentDate = DateTime.Now;
            PaperRef = "";
            this.reversed = false;
            this.ShortDescription = null;
            this.LongDescription = null;            
        }
        public virtual string BuildHTML()
        {
            return this.ToString();
        }
        public bool Posted
        {
            get
            {
                return AccountDocumentID != -1;
            }
        }
        public void CoypFrom(AccountDocument accountDocument)
        {
            this.AccountDocumentID = accountDocument.AccountDocumentID;
            this.DocumentDate = accountDocument.DocumentDate;
            this.DocumentTypeID = accountDocument.DocumentTypeID;
            this.LongDescription = accountDocument.LongDescription;
            this.PaperRef = accountDocument.PaperRef;
            this.ShortDescription = accountDocument.ShortDescription;
            this.reversed = accountDocument.reversed;
            this.scheduled = accountDocument.scheduled;
            this.materialized = accountDocument.materialized;
            this.materializedOn = accountDocument.materializedOn;

        }
        public virtual string GetDocumentHashCode()
        {
            return Guid.NewGuid().ToString();
        }

        //serialization
        public virtual string ToXML()
        {
            System.Xml.Serialization.XmlSerializer s = INTAPS.SerializationExtensions.getSerializer(this.GetType());// new XmlSerializer(this.GetType());
            StringWriter sw = new StringWriter();
            s.Serialize(sw, this);
            return sw.ToString();
        }
        public static AccountDocument DeserializeXML(string xml, Type SubType)
        {
            System.Xml.Serialization.XmlSerializer s =INTAPS.SerializationExtensions.getSerializer(SubType);// new XmlSerializer(SubType);
            StringReader sr = new StringReader(xml);
            return (AccountDocument)s.Deserialize(sr);
        }
        
        byte[] ToBinary()
        {
            System.IO.MemoryStream ms = new MemoryStream();
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bs = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            bs.Serialize(ms, this);
            byte[] ret = new byte[ms.Length];
            ms.Seek(0, SeekOrigin.Begin);
            ms.Read(ret, 0, ret.Length);
            return ret;
        }
        
    }

    [XmlInclude(typeof(AccountDocument))]
    [Serializable]
    public class TransactionReversalDocument : AccountDocument
    {
        public AccountDocument reversedDoc;
        public string reversedDocHTML;
        public string reversedDocHashCode;
        public TransactionReversalDocument()
        {
            reversedDoc = null;
            reversedDocHTML = "";
            reversedDocHashCode = "";
        }
        public TransactionReversalDocument(AccountDocument doc)
        {
            reversedDoc = new AccountDocument();
            reversedDoc.CoypFrom(doc);
            reversedDocHTML = doc.BuildHTML();
            reversedDocHashCode = doc.GetDocumentHashCode();
            this.PaperRef = "Reversal " + doc.PaperRef;
            this.ShortDescription = "Reversal " + doc.ShortDescription;            
        }
        public override string GetDocumentHashCode()
        {
            return reversedDocHashCode+"-"+reversedDoc.DocumentTypeID+"-"+reversedDoc.AccountDocumentID;
        }
        public override string BuildHTML()
        {
            if (reversedDoc == null)
                return "Transation Reversal, detail not set.";
            string note=string.IsNullOrEmpty(this.ShortDescription)?""
                :"<b>Note:</b>"+System.Web.HttpUtility.HtmlEncode(this.ShortDescription);

            return "<h3>Transaction Reversal</h3>"
                +note
                +"<h4> Original Document</h4>"+reversedDocHTML;
        }
    }

}