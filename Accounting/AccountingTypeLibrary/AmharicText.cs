using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS
{
	/// <summary>
	/// Summary description for AmharicText.
	/// </summary>
    public enum MatchType
    {
        NoMatch,
        FullMatch,
        FirstFullWord,
        WordMatch,
        AtStartOfWord,
        AtStartOfFirstWord,
        SubStringMatch
    }
	public class AmharicText
	{
        private static char[] SpaceChars = new char[] { ' ', ',', ';', ':', '.' };

        public static bool Contains(string source, string substr)
        {
            source = source.ToUpper();
            substr = substr.ToUpper();
            char[] chArray = source.ToCharArray();
            char[] chArray2 = substr.ToCharArray();
            for (int i = 0; i < chArray.Length; i++)
            {
                if (chArray[i] != chArray2[0])
                {
                    continue;
                }
                int index = 0;
                while (index < chArray2.Length)
                {
                    if ((index + i) >= chArray.Length)
                    {
                        return false;
                    }
                    if (chArray[i + index] != chArray2[index])
                    {
                        break;
                    }
                    index++;
                }
                if (index == chArray2.Length)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool Equals(string source, string substr)
        {
            if (source == null)
            {
                return false;
            }
            if (substr == null)
            {
                return false;
            }
            char[] chArray = source.ToCharArray();
            char[] chArray2 = substr.ToCharArray();
            if (chArray.Length != chArray2.Length)
            {
                return false;
            }
            for (int i = 0; i < chArray.Length; i++)
            {
                if (chArray[i] != chArray2[i])
                {
                    return false;
                }
            }
            return true;
        }

        public static int IndexOf(string source, string substr)
        {
            source = source.ToUpper();
            substr = substr.ToUpper();
            char[] chArray = source.ToCharArray();
            char[] chArray2 = substr.ToCharArray();
            for (int i = 0; i < chArray.Length; i++)
            {
                if (chArray[i] != chArray2[0])
                {
                    continue;
                }
                int index = 0;
                while (index < chArray2.Length)
                {
                    if ((index + i) >= chArray.Length)
                    {
                        return -1;
                    }
                    if (chArray[i + index] != chArray2[index])
                    {
                        break;
                    }
                    index++;
                }
                if (index == chArray2.Length)
                {
                    return i;
                }
            }
            return -1;
        }
        public static string GetLatinConsonant(string name)
        {
            StringBuilder sb = new StringBuilder(name.Length);
            char prev = (char)0;
            foreach (char ch in name)
            {
                if (prev != ch)
                {
                    bool con = true;
                    foreach (char v in new char[] { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' })
                    {
                        if (Char.ToUpper(v) == Char.ToUpper(ch))
                        {
                            con = false;
                            break;
                        }
                    }
                    if (!con)
                    {
                        prev = ch;
                        continue;
                    }
                    sb.Append(ch);
                }
                prev = ch;
            }
            return sb.ToString();
        }

        public static string NormalizeSpace(string name)
        {
            StringBuilder sb = new StringBuilder(name.Length);
            char prev = (char)0;
            foreach (char ch in name)
            {
                if (ch == ' ' && prev == ' ')
                {
                    prev = ch;
                    continue;
                }
                sb.Append(ch);
                prev = ch;
            }
            return sb.ToString();
        }
	}
    public class EDataUtility
    {
        public static INTAPS.Evaluator.EData createEData(object val)
        {
            if (val == null)
                return new INTAPS.Evaluator.EData(INTAPS.Evaluator.DataType.Empty, null);
            if (val is int)
                return new INTAPS.Evaluator.EData(INTAPS.Evaluator.DataType.Int, val);
            if (val is double)
                return new INTAPS.Evaluator.EData(INTAPS.Evaluator.DataType.Float, val);
            if (val is bool)
                return new INTAPS.Evaluator.EData(INTAPS.Evaluator.DataType.Bool, val);
            return new INTAPS.Evaluator.EData(INTAPS.Evaluator.DataType.Text, val.ToString());
        }
    }
}
