using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using INTAPS.RDBMS;
using INTAPS.ClientServer;

namespace INTAPS.Accounting
{
    public enum TransactionType
    {
        DebitCredit=0,
        Opening=1
    }
    [Serializable]
    public class ClonableBase : ICloneable
    {
        #region ICloneable Members

        public virtual object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
    }
    [Serializable]
    [SingleTableObject]
    public class TransactionItem
    {
        public const int DEFAULT_CURRENCY = 0;
        public const int MATERIAL_QUANTITY = 1;
        public const int BUDGET = 2;
        public static TransactionItem DefaultCurrency;
        public static TransactionItem MaterialQuantity;
        static TransactionItem()
        {
            DefaultCurrency=new TransactionItem();
            DefaultCurrency.ID = DEFAULT_CURRENCY;
            DefaultCurrency.Name = "Birr";

            MaterialQuantity = new TransactionItem();
            MaterialQuantity.ID = DEFAULT_CURRENCY;
            MaterialQuantity.Name = "pcs";
        }
        [IDField]
        public int ID;
        [DataField]
        public string Name;
        [DataField]
        public double DisplayUnit;
        [DataField]
        public string DisplayFormat;
        
        public string FormatValue(double val)
        {
            return val.ToString(DisplayFormat);
        }
        public override string ToString()
        {
            return Name;
        }
    }
    [Serializable]
    [SingleTableObject(orderBy="id")]
    public class DocumentType
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        [DataField]
        public string description;
        [DataField]
        public string documentClass;
        [DataField]
        public string assemblyName;
        [DataField]
        public bool system;
        public Type GetTypeObject()
        {
            return Type.GetType(documentClass + "," + assemblyName);
        }
        public override string ToString()
        {
            return name;
        }
    }
    public enum AccountStatus
    {
        Pending = 0,
        Activated = 1,
        Deactivated = 2
    }
    [Flags]
    public enum AccountProtection
    {
        None = 0,
        DenyUserUpdate = 1,
        DenyUserDelete = 2,
        DenyUserPost = 4,
        SystemAccount=5
    }

    [Serializable]
    [SingleTableObject]
    public class AccountBalance : ClonableBase
    {
        [IDField]
        public int AccountID=-1;
        [IDField]
        public int ItemID=-1;
        [DataField]
        public double TotalCredit=0;
        [DataField]
        public double TotalDebit=0;
        [DataField]
        public int LastOrderN=0;
        [DataField]
        public long tranTicks = -1;
        //public DateTime LastTransactionDate
        //{
        //    get
        //    {
        //        return new DateTime(tranTicks);
        //    }
        //    set
        //    {
        //        tranTicks = value.Ticks;
        //    }
        //}


        public bool IsZero
        {
            get
            {
                return AccountBase.AmountEqual(TotalDebit, 0) && AccountBase.AmountEqual(TotalCredit, 0);
            }
        }
        public AccountBalance()
        {
        }
        public AccountBalance(int accountID,int itemID)
        {
            this.AccountID = accountID;
            this.ItemID = itemID;

        }
        public AccountBalance(int accountID, int itemID,double totalDb,double totalCr):this(accountID,itemID)
        {
            this.TotalDebit = totalDb;
            this.TotalCredit = totalCr;

        }

        public AccountBalance(int accountID, int itemID, double amount)
            :this(accountID,itemID,amount>0?amount:0,amount<0?-amount:0)
        {
            
        }
        public ulong ItemAccountID
        {
            get
            {
                ulong liid = (ulong)ItemID;
                return (ulong)((ulong)AccountID << 32) | liid;
            }
        }
        public static void Split(ulong ItemAccountID, out int AccountID, out int ItemID)
        {
            AccountID = (int)(ItemAccountID >> 32);
            ItemID = (int)(ItemAccountID & 0xFFFFFFFF);
        }
        public static ulong Combine(int AccountID, int ItemID)
        {
            ulong liid=(ulong)ItemID;
            return (ulong)((ulong)AccountID << 32) | liid;
        }
        public double DebitBalance
        {
            get
            {
                return this.TotalDebit - this.TotalCredit;
            }
        }
        public void AddBalance(AccountBalance bal)
        {
            TotalDebit += bal.TotalDebit;
            TotalCredit += bal.TotalCredit;
        }

        public double GetBalance(bool creditAccount)
        {
            if (creditAccount)
                return TotalCredit - TotalDebit;
            return TotalDebit - TotalCredit;
        }

        public void multiplyBy(double k)
        {
            TotalDebit *= k;
            TotalCredit *= k;
        }
    }

    [Serializable]
    
    public class AccountBase : ClonableBase,IIDObject
    {
        [IDField]
        public int id=-1;
        [DataField(false)]
        public int PID=-1;
        [DataField]
        public string Code;
        [DataField]
        public string Name;
        [DataField]
        public string Description;
        [DataField]
        public AccountStatus Status;
        [DataField]
        public DateTime CreationDate=DateTime.Now;
        [DataField]
        public DateTime ActivateDate = DateTime.Now;
        [DataField]
        public DateTime DeactivateDate = DateTime.Now;
        [DataField]
        public int childCount=0;
        [DataField]
        public AccountProtection protection;
        public bool isControl;
        public AccountBase()
        {
            id = -1;
            PID = -1;
            Status = AccountStatus.Pending;
            CreationDate = DateTime.Now;    
            ActivateDate = DateTime.Now;
            DeactivateDate = DateTime.Now;
            protection = AccountProtection.None;
        }
        public string CodeName
        {
            get
            {
                return Code + "-" + Name;
            }
        }
        public string NameCode
        {
            get
            {
                return Name+ " (" + Code+")";
            }
        }
        

        #region IIDObject Members

        public int ObjectIDVal
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        #endregion
        public const double ACCOUNT_PRECISION=0.0001;
        public const double DOCUMENT_CHAIN_MINIMUM_SEPARATION_SECONDS = 1;
        public static bool AmountEqual(double a1, double a2,double precision)
        {
            return Math.Abs(a1 - a2) < precision;
        }
        public static bool AmountEqual(double a1, double a2)
        {
            return AmountEqual(a1, a2, ACCOUNT_PRECISION);
        } 
        public static string FormatBalance(double amount)
        {
            if (AmountLess(amount, 0))
                return "(" + (-amount).ToString("#,#0.00") + ")";
            if (AmountEqual(amount, 0))
                return "-";
            return amount.ToString("#,#0.00");
        }
        public static string FormatAmount(double amount)
        {
            if (AmountEqual(amount, 0))
                return "-";
            return amount.ToString("#,#0.00");
        }
        public static bool AmountLess(double a1, double a2,double precision)
        {
            return Math.Abs(a1 - a2) >= precision && a1 < a2;
        }
        public static bool AmountGreater(double a1, double a2, double precision)
        {
            return Math.Abs(a1 - a2) >= precision && a1 > a2;
        }
        public static bool AmountLess(double a1, double a2)
        {
            return AmountLess(a1, a2, ACCOUNT_PRECISION);
        }
        public static bool AmountGreater(double a1, double a2)
        {
            return AmountGreater(a1, a2, ACCOUNT_PRECISION);
        }
        public static string GetCostCenterAccountIDField<AccountType>()
        {
            if (typeof(AccountType) == typeof(Account))
                return "accountID";
            return "costCenterID";
        }
        public static string GetCostCenterAccountComplementIDField<AccountType>()
        {
            if (typeof(AccountType) == typeof(Account))
                return "costCenterID";
            return "accountID";
        }
        public static string GetCostCenterAccountPIDField<AccountType>()
        {
            if (typeof(AccountType) == typeof(Account))
                return "accountPID";
            return "costCenterPID";
        }
        public static string GetAccountTypeDescription<AccountType>(bool fistLeterCap)
        {
            if (typeof(AccountType) == typeof(Account))
                return fistLeterCap ? "Account" : "account";
            return fistLeterCap ? "Cost center" : "cost center";
        }


        public static string FormatTime(DateTime dateTime)
        {
            return dateTime.ToString("MMM dd,yy hh:mm tt");
        }
        public static string FormatDate(DateTime dateTime)
        {
            return dateTime.ToString("MMM dd,yy");
        }
        public static DateTime getClosingTime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 30);
        }
        public static bool IsValidNoneClosingTransactionTime(DateTime dateTime)
        {
            return dateTime.Hour != 23 || dateTime.Minute != 59;
        }

        public static string FormatUnitPrice(double begQunatity, double begCost)
        {
            if (AmountEqual(begQunatity, 0) && AmountEqual(begCost, 0))
                return "-";
            if(AmountEqual(begCost,0))
                return "0";
            if(AmountEqual(begQunatity,0))
                return "Data Error";
            return (begCost/begQunatity).ToString("#,#0.00");
        }

        public static string FormatQuantity(double quantity)
        {
            if (AmountEqual(quantity, 0))
                return "-";
            if (AmountEqual(Math.Floor(quantity), quantity))
                return quantity.ToString("0");
            return quantity.ToString("0.00");
        }
        public override string ToString()
        {
            return Name;
        }
    }

    [Serializable]
    [SingleTableObject(orderBy="code")]
    [STOInclude(typeof(AccountBase))]
    public class Account:AccountBase
    {
        [DataField]
        public bool CreditAccount;
        [DataField]
        public StandardAccountType accountType = StandardAccountType.Undefined;
        public string DebitCreditName
        {
            get
            {
                return CreditAccount ? "Credit" : "Debit";
            }
        }
        public Account()
        {
            CreditAccount = true;
        }

        public static String getAccountTypeName(StandardAccountType type)
        {
            switch (type)
            {
                case StandardAccountType.Undefined:
                    return "Undefined";
                case StandardAccountType.Cash:
                    return "Cash on Hand";
                case StandardAccountType.Bank:
                    return "Cash at Bank";
                case StandardAccountType.FixedAsset:
                    return "Fixed Asset";
                case StandardAccountType.Inventory:
                    return "Inventory";
                case StandardAccountType.Stock:
                    return "Stock Holding";
                case StandardAccountType.Receivable:
                    return "Recievable";
                case StandardAccountType.Payable:
                    return "Payable";
                case StandardAccountType.Cost:
                    return "Cost";
                case StandardAccountType.Expense:
                    return "Expense";
                default:
                    return null;
            }
        }
    }
    
    [Serializable]
    [SingleTableObject(orderBy="code")]
    [STOInclude(typeof(AccountBase))]
    public class CostCenter:AccountBase
    {
        public const int ROOT_COST_CENTER = 1;
    }

    [Serializable]
    [SingleTableObject]
    public class CostCenterAccount : IEquatable<CostCenterAccount>
    {
        [IDField]
        public int id;
        [DataField]
        public int accountID;
        [DataField]
        public int accountPID;
        [DataField]
        public int costCenterID;
        [DataField]
        public int costCenterPID;
        [DataField]
        public bool creditAccount;
        [DataField]
        public int childAccountCount;
        [DataField]
        public int childCostCenterCount;
        [DataField]
        public DateTime activateDate = DateTime.Now;
        [DataField]
        public DateTime deactivateDate = DateTime.Now;
        [DataField]
        public AccountStatus status = AccountStatus.Activated;

        public bool isControlAccount
        {
            get
            {
                return childAccountCount > 0 || childCostCenterCount > 0;
            }
        }
        public CostCenterAccount()
        {
            creditAccount = false;
        }

        public static string CreateCode(string costCenterCode, string accountCode)
        {
            return costCenterCode + ":" + accountCode;
        }


        public bool Equals(CostCenterAccount other)
        {
            return other.id == this.id;
        }

        public static string CreateName(string accountName, string costCenterName)
        {
            return CreateCode(accountName, costCenterName);
        }

        public ulong longID
        {
            get 
            { 
                ulong liid = (ulong)accountID;
                return (ulong)((ulong)costCenterID<< 32) | liid;
            }
        }
    }
    [Serializable]
    public class CostCenterAccountWithDescription:CostCenterAccount
    {
        public CostCenter costCenter;
        public Account account;
        public string code;
        public string name;
        private CostCenterAccount cs;
        public string CodeName
        {
            get
            {
                return code + "-" + name;
            }
        }
        public string NameCode
        {
            get
            {
                return name + " (" + code + ")";
            }
        }
        public CostCenterAccountWithDescription(CostCenterAccount cs)
        {
            base.accountID = cs.accountID;
            base.accountPID = cs.accountPID;
            base.childAccountCount = cs.childAccountCount;
            base.childCostCenterCount = cs.childCostCenterCount;
            base.costCenterID = cs.costCenterID;
            base.costCenterPID = cs.costCenterPID;
            base.creditAccount = cs.creditAccount;
            base.id = cs.id;
        }
    }
    [Serializable]
    public class TransactionOfBatch
    {
        public int ID;
        public TransactionType TransactionType;
        public int AccountID;
        public int ItemID;
        public ulong ItemAccountID
        {
            get
            {
                ulong liid = (ulong)ItemID;
                return (ulong)((ulong)AccountID << 32) | liid;
            }
        }
        public double Amount;
        public double TotalCrBefore;
        public double TotalDbBefore;
        public string Note;
        public int OrderN;
        public TransactionOfBatch()
        {
            ID = -1;
            ItemID=TransactionItem.DEFAULT_CURRENCY;
            TransactionType = TransactionType.DebitCredit;
            AccountID = -1;
            Amount = 0;
            TotalDbBefore= 0;
            TotalCrBefore = 0;
            Note = null;
        }
        public TransactionOfBatch(AccountTransaction t)
        {
            ID = t.ID;
            ItemID = t.ItemID;
            TransactionType = t.TransactionType;
            AccountID = t.AccountID;
            Amount = t.Amount;
            TotalDbBefore = t.TotalDbBefore;
            TotalCrBefore = t.TotalCrBefore;
            Note = t.Note;
        }

        public TransactionOfBatch(int Account,int ItemID, double Amount)
        {
            this.ID = -1;
            this.TransactionType = TransactionType.DebitCredit;
            this.AccountID = Account;
            this.ItemID = ItemID;
            this.Amount = Amount;
            this.TotalDbBefore = 0;
            this.TotalCrBefore = 0;
            this.Note = null;
        }
        public TransactionOfBatch(int Account, double Amount)
            : this(Account, TransactionItem.DEFAULT_CURRENCY, Amount)
        {
        }
        public TransactionOfBatch(int Account, int ItemID, double Amount, string Note)
        {
            this.ID = -1;
            this.AccountID = Account;
            this.ItemID = ItemID;
            this.TransactionType = TransactionType.DebitCredit;
            this.Amount = Amount;
            this.TotalDbBefore = 0;
            this.TotalCrBefore = 0;
            this.Note = Note;
        }
        public TransactionOfBatch(int Account, double Amount, string Note)
            : this(Account, TransactionItem.DEFAULT_CURRENCY, Amount, Note)
        {
        }
        public double NewCredit
        {
            get
            {
                switch (TransactionType)
                {
                    case TransactionType.DebitCredit:
                        if (Amount < 0)
                            return TotalCrBefore - Amount;
                        return TotalCrBefore;
                    case TransactionType.Opening:
                        if (Amount < 0)
                            return -Amount;
                        return 0;
                }
                return 0;
            }
        }
        public double NewDebit
        {
            get
            {
                switch (TransactionType)
                {
                    case TransactionType.DebitCredit:
                        if (Amount > 0)
                            return TotalDbBefore + Amount;
                        return TotalDbBefore;
                    case TransactionType.Opening:
                        if (Amount > 0)
                            return Amount;
                        return 0;
                }
                return 0;
            }
        }
        public double GetNewBalance(bool creditAccount)
        {
            if (creditAccount)
                return NewCredit - NewDebit;
            return NewDebit - NewCredit;
        }
        public static double GetNewBalance(double prevBalance,double amount,bool creditAccount)
        {
            if (creditAccount)
                return prevBalance - amount;
            return prevBalance + amount;
        }
        public static TransactionOfBatch[] fromAccountTransaction(AccountTransaction[] trans)
        {
            if (trans == null)
                return null;
            TransactionOfBatch[] ret = new TransactionOfBatch[trans.Length];
            for (int i = 0; i < ret.Length; i++)
                ret[i] = new TransactionOfBatch(trans[i]);
            return ret;
        }
        public TransactionOfBatch clone()
        {
            return base.MemberwiseClone() as TransactionOfBatch;
        }
    }
    [Serializable]
    public class AccountTransaction : TransactionOfBatch
    {
        public int documentID;
        public long tranTicks=-1;
        public DateTime execTime
        {

            get
            {
                return new DateTime(tranTicks);
            }
            set
            {
                tranTicks = value.Ticks;
            }
        }


        public static TransactionOfBatch[] filterNoneZero(TransactionOfBatch[] trans)
        {
            List<TransactionOfBatch> filtered = new List<TransactionOfBatch>();
            foreach (TransactionOfBatch ti in trans)
            {
                if (AccountBase.AmountEqual(ti.Amount, 0))
                    continue;
                filtered.Add(ti);
            }
            return filtered.ToArray();
        }

        public new AccountTransaction clone()
        {
            return base.MemberwiseClone() as AccountTransaction;
        }
    }
    [Serializable]
    public class DocumentSearchPar
    {
        public string query; 
        public int type=-1; 
        public bool date; 
        public DateTime from; 
        public DateTime to;
        public int[] includeType = null;
    }
}
