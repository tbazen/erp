using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Web;
using BIZNET.iERP;

namespace INTAPS.Accounting
{
    [Serializable]
    [XmlInclude(typeof(TransactionOfBatch))]
    public class DirecLedgerEntryDocument : AccountDocument
    {
        public bool OpeningAccount;
        public TransactionOfBatch[] Transactions;
        public string[] AccountCodes;
        public string[] AccountNames;
        public override string BuildHTML()
        {
            string html = "<b>Paper Ref:</b>" + HttpUtility.HtmlEncode(base.PaperRef);
            html += "<b>Date:</b>" + HttpUtility.HtmlEncode(DocumentDate.ToLongDateString());
            if (!string.IsNullOrEmpty(base.ShortDescription))
                html += "<br/><b>Note:</b>" + HttpUtility.HtmlEncode(base.ShortDescription);
            if (!string.IsNullOrEmpty(base.LongDescription))
                if (string.IsNullOrEmpty(base.ShortDescription))
                    html += "<br/><b>Note:</b>" + HttpUtility.HtmlEncode(base.LongDescription);
                else
                    html += "<br/>" + HttpUtility.HtmlEncode(base.LongDescription);
            html += "<br/><b>Transactions</b>";
            var table = new bERPHtmlTable();
            //table.CellSpacing = 0;
            bERPHtmlTableRowGroup group = table.createBodyGroup();
            bERPHtmlBuilder.htmlAddRow(group
            , new bERPHtmlTableCell("Account", "LedgerGridHeadCell")
            , new bERPHtmlTableCell("Debit", "LedgerGridHeadCell")
            , new bERPHtmlTableCell("Credit", "LedgerGridHeadCell"));
            bERPHtmlTableCell cell1, cell2;
            int k = 0;
            foreach (TransactionOfBatch t in Transactions)
            {
                var row = bERPHtmlBuilder.htmlAddRow(group,
                                         new bERPHtmlTableCell(AccountCodes == null || k >= AccountCodes.Length ? "Account system ID:" + t.AccountID.ToString() : AccountCodes[k])
                                        , cell1 = new bERPHtmlTableCell(t.Amount > 0 ? t.Amount.ToString("0,0.00") : "")
                                        , cell2 = new bERPHtmlTableCell(t.Amount < 0 ? (-t.Amount).ToString("0,0.00") : "")
                );
                row.css = k % 2 == 0 ? "LedgerGridOddRowCell" : "LedgerGridEvenRowCell";
                cell1.styles.Add("text-align:right");
                cell2.styles.Add("text-align:right");
                k++;
            }
            StringBuilder sb = new StringBuilder();
            table.build(sb);
            html += sb;
            return html;
        }
    }
}
