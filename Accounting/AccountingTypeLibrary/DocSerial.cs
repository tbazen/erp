﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting
{
    public class DocumentTypedReferenceFieldAttribute : System.Xml.Serialization.XmlIgnoreAttribute
    {
        public string[] types;
        public DocumentTypedReferenceFieldAttribute(params string[] types)
        {
            this.types = types;
        }
        public DocumentTypedReferenceFieldAttribute()
        {
            this.types = null;
        }
    }
    public enum DocumentSerializationMode
    {
        Serialized,
        Manual
    }
    [Serializable]
    [INTAPS.RDBMS.SingleTableObject]
    public class DocumentSerialType
    {
        [INTAPS.RDBMS.IDField]
        public int id;
        [INTAPS.RDBMS.DataField]
        public string name;
        [INTAPS.RDBMS.DataField]
        public string code;
        [INTAPS.RDBMS.DataField]
        public DocumentSerializationMode serialType;
        [INTAPS.RDBMS.DataField]
        public string format="";
        [INTAPS.RDBMS.DataField]
        public string description;
        [INTAPS.RDBMS.DataField]
        public string prefix;


        public override string ToString()
        {
            return name;
        }
        public string FormatSerial(int serial)
        {
            return serial.ToString(format);
        }
    }

    [Serializable]
    public class DocumentTypedReference:IEquatable<DocumentTypedReference>
    {
        public int typeID = -1;
        public string reference = "";
        public bool primary=false;
        public DocumentTypedReference()
        {
        }
        public DocumentTypedReference(int typeID, string reference,bool primary)
        {
            this.typeID = typeID;
            this.reference = reference;
            this.primary = primary;
        }

        public int serial
        {
            get
            {
                int ret;
                if (int.TryParse(reference, out ret))
                    return ret;
                return -1;
            }
        }

        public bool Equals(DocumentTypedReference other)
        {
            if (other == null)
                return false;
            return other.typeID == this.typeID && other.reference.Equals(this.reference);
        }
        public static DocumentTypedReference[] createArray(params DocumentTypedReference[] refs)
        {
            List<DocumentTypedReference> ret = new List<DocumentTypedReference>();
            if (refs != null) 
            for (int i = 0; i < refs.Length; i++)
                if (refs[i] != null)
                {
                    if (!ret.Contains(refs[i]))
                        ret.Add(refs[i]);
                }
            return ret.ToArray();
        }
        public static DocumentTypedReference[] createArray(params DocumentSerial[] refs)
        {
            List<DocumentTypedReference> ret = new List<DocumentTypedReference>();
            if (refs != null)
                for (int i = 0; i < refs.Length; i++)
                    if (refs[i] != null)
                    {
                        if (!ret.Contains(refs[i].typedReference))
                            ret.Add(refs[i].typedReference);
                    }
            return ret.ToArray();
        }
        public static DocumentTypedReference[] createArray(DocumentTypedReference[] parent, params DocumentTypedReference[] refs)
        {
            List<DocumentTypedReference> ret = new List<DocumentTypedReference>();
            if (refs != null)
                for (int i = 0; i < refs.Length; i++)
                    if (refs[i] != null)
                    {
                        if (!ret.Contains(refs[i]))
                        {
                            refs[i].primary = false;
                            ret.Add(refs[i]);
                        }
                    }
            if (parent != null)
                for (int i = 0; i < parent.Length; i++)
                    if (parent[i] != null)
                    {
                        if (!ret.Contains(parent[i]))
                        {
                            parent[i].primary = false;
                            ret.Add(parent[i]);
                        }
                    }

            return ret.ToArray();
        }

        public static DocumentTypedReference[] cloneArray(bool primary,params DocumentTypedReference[] refs)
        {
            DocumentTypedReference[] ret = new DocumentTypedReference[refs.Length];
            for (int i = 0; i < refs.Length; i++)
            {
                ret[i] = refs[i].MemberwiseClone() as DocumentTypedReference;
                ret[i].primary = primary;
            }
            return ret;
        }
        public static DocumentTypedReference[] cloneArray(bool primary, params DocumentSerial[] refs)
        {
            DocumentTypedReference[] ret = new DocumentTypedReference[refs.Length];
            for (int i = 0; i < refs.Length; i++)
            {
                ret[i] = refs[i].typedReference;
                ret[i].primary = primary;
            }
            return ret;
        }
    }
    [Serializable]
    [INTAPS.RDBMS.SingleTableObject]
    public class DocumentSerialBatch
    {
        [INTAPS.RDBMS.IDField]
        public int id;
        [INTAPS.RDBMS.DataField]
        public int typeID;
        [INTAPS.RDBMS.DataField]
        public int costCenterID;
        [INTAPS.RDBMS.DataField]
        public int fromSer;
        [INTAPS.RDBMS.DataField]
        public int toSer;
        [INTAPS.RDBMS.DataField]
        public DateTime fromDate;
        [INTAPS.RDBMS.DataField]
        public bool timeUpperBound;
        [INTAPS.RDBMS.DataField]
        public DateTime toDate;
        [INTAPS.RDBMS.DataField]
        public string displayFormat;

        public void assertValidBatch()
        {
            if (fromSer > toSer && toSer != -1)
                throw new ClientServer.ServerUserMessage("Serial lower bound can't be higher than upper bound");
            if (fromDate >= toDate && timeUpperBound)
                throw new ClientServer.ServerUserMessage("Serial lower time bound can't be later than upper time bound");
        }
        public static void assertSerialRange(int fromSer,int toSer)
        {
            if (fromSer > toSer)
                throw new ClientServer.ServerUserMessage("Serial lower bound can't be higher than upper bound");
        }

        public bool inRange(int serialNo)
        {
            return serialNo >= fromSer && (toSer == -1 || (toSer != -1 && serialNo <= toSer));
        }

        public string rangeText
        {
            get
            {
                if (toSer == -1)
                    return fromSer + " - infinity";
                return fromSer + " - " + toSer;
            }
        }

        public string FormatSerial(int serial)
        {
            return serial.ToString( displayFormat);
        }
    }
    public enum TransactionJournalField
    {
        Date,
        Reference
    }
    public class JournalPrimaryEntry
    {
        public int documentID = -1;
        public int typeID = -1;
        public string reference = "";
        public int serialNo = -1;
        public long tranTicks=-1;
        public DateTime primaryDate
        {
            get
            {
                return new DateTime(tranTicks);
            }
            set
            {
                tranTicks = value.Ticks;
            }
        }
        public string primaryShortDescription;
        public int instanceCount;
        public JournalPrimaryEntry()
        {
        }
        public JournalPrimaryEntry(DocumentSerial documentSerial)
        {
            this.documentID = documentSerial.documentID;
            this.typeID = documentSerial.typeID;
            this.reference = documentSerial.reference;
            this.serialNo = documentSerial.serialNo;
            this.primaryDate = documentSerial.primaryDoc.DocumentDate;
            this.primaryShortDescription = documentSerial.primaryDoc.ShortDescription;
        }
    }
    [Serializable]
    [INTAPS.RDBMS.SingleTableObject]
    public class DocumentSerial:IEquatable<DocumentSerial>
    {
        [INTAPS.RDBMS.IDField]
        public int documentID=-1;
        [INTAPS.RDBMS.IDField]
        public int typeID = -1;
        [INTAPS.RDBMS.IDField]
        public string reference = "";
        [INTAPS.RDBMS.DataField]
        public int serialNo = -1;
        [INTAPS.RDBMS.DataField]
        public bool primaryDocument = false;
        public bool isVoid=false;
        public string voidReason=null;
        public AccountDocument primaryDoc;
        public DocumentTypedReference typedReference
        {
            get
            {
                return new DocumentTypedReference(typeID, reference,primaryDocument);
            }
            set
            {
                this.typeID = value.typeID;
                this.reference = value.reference;
                this.primaryDocument = value.primary;
            }
        }





        public bool Equals(DocumentSerial other)
        {
            if (other == null)
                return false;
            return this.typeID == other.typeID && this.reference.Equals(other.reference);
        }
    }

    [Serializable]
    [INTAPS.RDBMS.SingleTableObject]
    public class DocumentSerialVoided
    {

        [INTAPS.RDBMS.IDField]
        public int typeID = -1;
        [INTAPS.RDBMS.IDField]
        public string reference = "";
        [INTAPS.RDBMS.DataField]
        public int serialNo = -1;

        [INTAPS.RDBMS.DataField]
        public string voidReason;

        public DocumentTypedReference typedReference
        {
            get
            {
                return new DocumentTypedReference(typeID, reference,false);
            }
            set
            {
                this.typeID = value.typeID;
                this.reference = value.reference;
            }
        }
    }
}
