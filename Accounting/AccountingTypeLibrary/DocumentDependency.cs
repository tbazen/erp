﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.Accounting
{
    [SingleTableObject]
    public class AccountDependancy
    {
        [IDField]
        public int documentID;
        [IDField]
        public int csaID;
        [IDField]
        public int itemID;
        [IDField]
        public long tranTicks;
        [IDField]
        public bool exclusive; 
    }
    [SingleTableObject]
    public class DocumentDependancy
    {
        [IDField]
        public int documentID;
        [IDField]
        public int dependancyDocumentID;
    }
}
