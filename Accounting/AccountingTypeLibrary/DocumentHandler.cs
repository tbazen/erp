using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.Accounting
{
    [Serializable]
    [SingleTableObject]
    public class DocumentHandler
    {
        [IDField]
        public int documentTypeID;
        [DataField]
        public string name;
        [DataField]
        public string serverClass;
        [DataField]
        public string clientClass;

    }
}
