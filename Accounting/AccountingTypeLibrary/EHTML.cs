using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
using System.Xml.Serialization;
using INTAPS.RDBMS;

namespace INTAPS.Accounting
{
    [Serializable]
    [XmlInclude(typeof(EData))]
    [XmlInclude(typeof(ListData))]
    [XmlInclude(typeof(Vector1D))]
    public class EHTMLData
    {
        public string[] variables;
        public EData[] values;
        public string[] expressions;
        public string mainFormula;
        public EHTMLData()
            :this(0)
        {
        }
        public EHTMLData(int nElements)
        {
            variables = new string[nElements];
            values = new EData[nElements];
            expressions = new string[nElements];
            mainFormula = "";
        }
        public void SetItem(int i, string var, EData val, string exp)
        {
            variables[i] = var;
            values[i] = val;
            expressions[i] = exp;
        }
        public bool Valid()
        {
            //any of the arrays nulll
            if (variables == null || expressions == null || values == null)
                return false;
            //arrays of not equal length
            if (values.Length != expressions.Length || variables.Length != expressions.Length)
                return false;
            //repeated variale
            for (int i = 0; i < variables.Length - 1; i++)
                for (int j = i + 1; j < variables.Length; j++)
                    if (variables[i].ToUpper() == variables[j].ToUpper())
                        return false;
            return true;
        }
    }
    [Serializable]
    [INTAPS.RDBMS.SingleTableObject(orderBy="name")]
    public class ReportDefination
    {
        [IDField]
        public int id;
        [DataField]
        public int categoryID;
        [DataField]
        public string code;
        [DataField]
        public string name;
        [DataField]
        public string description;
        [DataField]
        public int reportTypeID;
        [XMLField]
        public EHTMLData formula;
        [DataField]
        public string styleSheet = "berp.css";
        public ReportDefination()
        {
            id = -1;
            name = "";
            description = "";
            reportTypeID = -1;
            formula = new EHTMLData();
        }
        public ReportDefination(int id, int typeID, int categoryID,string code,string name, string description)
        {
            this.id = id;
            this.reportTypeID = typeID;
            this.name = name;
            this.description = description;
            this.categoryID = categoryID;
            this.code = code;
            formula = new EHTMLData();
        }
    }
    
    [Serializable]
    [INTAPS.RDBMS.SingleTableObject(orderBy="name")]
    public class ReportCategory
    {
        [IDField]
        public int id;
        [DataField]
        public int pid;
        [DataField]
        public string name;
    }

    [Serializable]
    [INTAPS.RDBMS.SingleTableObject(orderBy="id")]
    public class ReportType
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        [DataField]
        public string description;
        [DataField]
        public string serverClass;
        [DataField]
        public string clientClass;

        public ReportType()
        {
            id = -1;
            name = "";
            serverClass = "";
        }
        public ReportType(int id, string name, string serverClass,string clientClass)
        {
            this.id = id;
            this.name = name;
            this.serverClass = serverClass;
            this.clientClass = clientClass;
        }
    }
}
