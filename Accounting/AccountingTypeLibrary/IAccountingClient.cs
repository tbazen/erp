﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting
{
    public interface IAccountingClient
    {
        int PostGenericDocument(AccountDocument doc);
    }
}
