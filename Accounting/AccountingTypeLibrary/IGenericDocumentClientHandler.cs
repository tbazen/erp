using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;

namespace INTAPS.Accounting
{
    public interface IGenericDocumentClientHandler
    {
        object CreateEditor(bool embeded);
        void SetEditorDocument(object editor, AccountDocument doc);
        AccountDocument GetEditorDocument(object editor);
        bool VerifyUserEntry(object editor,out string error);
        bool CanEdit { get;}
        void setAccountingClient(IAccountingClient client);
    }
}
