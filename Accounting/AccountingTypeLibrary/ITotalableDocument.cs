﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting
{
    public interface IStandardAttributeDocument
    {
        bool ammountApplies { get; }
        double documentAmount { get; }
        bool hasInstrument { get; }
        string getInstrument(bool includeInstrucmentType);
    }
}
