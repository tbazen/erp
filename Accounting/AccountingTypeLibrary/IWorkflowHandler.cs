﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Accounting
{
    public interface IWorkflowHandler
    {
        int initiateWorkFlow(AccountDocument workflowDocument);
    }
}
