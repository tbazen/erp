using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.Accounting
{
    [Serializable]
    public class BatchError
    {
        public int[] ObjectID;
        public string[] ErrorDesc;
        public Exception[] SystemException;
        
    }
    
    public enum VoidReasonType
    {
        PrinterMalfunction,
        CasheirError,
        Other
    }
    public enum AccountExplorerType
    {
        Main,
        ITEM,
        CUST,
        BUDGET,
        ALL
    }
    [SingleTableObject]
    [Serializable]
    public class UsedSerial
    {
        [IDField]
        public int batchID;
        [IDField]
        public int val;
        [DataField]
        public DateTime date;
        [DataField]
        public bool isVoid;
        [DataField]
        public VoidReasonType voidReason;
        [DataField]
        public string note;
        public void UseSerial(string DB, int AID,INTAPS.RDBMS.SQLHelper dsp, SerialBatch batch)
        {
            UsedSerial[] used = dsp.GetSTRArrayByFilter<UsedSerial>("batchID=" + this.batchID + " and val=" + this.val);
            if (used.Length == 1)
            {
                if (!this.isVoid) // if we are not trying to void this serial, it is not allowed to use it again
                    throw new Exception("The serial already used.");
                if (used[0].isVoid) //it is not allowed to void a serial that is already void
                    throw new Exception("The serial already voided.");
                
            }
            int nserial = (int)dsp.ExecuteScalar("Select count(*) from "+DB+".dbo.UsedSerial where batchID=" + this.batchID);
            if (nserial >= batch.serialTo - batch.serialFrom)
            {
                batch.active = false;
                dsp.UpdateSingleTableRecord(DB,batch);
            }
            if (used.Length == 0)
                dsp.InsertSingleTableRecord(DB,this,new string[]{"__AID"},new object[]{AID});
            else
                dsp.UpdateSingleTableRecord(DB, this, new string[] { "__AID" }, new object[] { AID });
        }
        public UsedSerial()
        {
        }
        public UsedSerial(int batchID, int serialNo, string note)
        {
            this.batchID = batchID;
            this.date = DateTime.Now;
            this.isVoid = false;
            this.note = note;
            this.val = serialNo;
        }
    }


    [SingleTableObject]
    [Serializable]
    public class SerialBatch
    {
        [IDField]
        public int id;
        [DataField(false)]
        public DateTime batchDate;
        [DataField(false)]
        public DateTime expiryDate;
        [DataField(false)]
        public int serialFrom;
        [DataField(false)]
        public int serialTo;
        [DataField]
        public string note;
        [DataField]
        public bool active;
        public SerialBatch()
        {
            id = -1;
        }
        public override string ToString()
        {
            if(string.IsNullOrEmpty(note))
                return  serialFrom + " to " + serialTo;
            return note+"(" +serialFrom + " to " + serialTo+")";
        }
        public UsedSerial createUsedSerial(int serialNo, string note)
        {
            return new UsedSerial()
            { 
                batchID=this.id,
              date=DateTime.Now,
              isVoid=false,
              note=note,
              val=serialNo
            };
        }
    }
}
