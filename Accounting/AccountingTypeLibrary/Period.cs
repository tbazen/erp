using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.Accounting
{
    [Serializable]
    public class InternationalDayPeriod : AccountingPeriod
    {
        public const string NAME = "AP_International_Day";
        static DateTime dayRefernce = new DateTime(1700, 1, 1);
        public InternationalDayPeriod(DateTime date)
        {
            fromDate = date.Date;
            toDate = fromDate.AddDays(1);
            name = Accounting.AccountBase.FormatDate(date);
            id = (int)date.Subtract(dayRefernce).TotalDays;
            days = 1;
            hours = 24;
        }
        public InternationalDayPeriod(int id)
        {
            fromDate = dayRefernce.AddDays(id);
            toDate = fromDate.AddDays(1);
            name = Accounting.AccountBase.FormatDate(fromDate);
            this.id = id;
            days = 1;
            hours = 24;
        }
    }
    [Serializable]
    [LoadFromSetting]
    public class AccountingPeriod
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        [DataField]
        public DateTime fromDate;
        [DataField]
        public DateTime toDate;
        [DataField]
        public double days;
        [DataField]
        public double hours;
        public AccountingPeriod()
        {
            days = 21;
            hours = 171;
            fromDate = DateTime.Now;
            toDate = DateTime.Now;
            id = -1;
            name = null;
        }
        public AccountingPeriod(DateTime from,DateTime to)
        {
            days = 21;
            hours = 171;
            fromDate = from;
            toDate = to;
            id = -1;
            name = null;
        }
        public bool InPeriod(DateTime date)
        {
            return fromDate <= date && date < toDate;
        }
        public override bool Equals(object obj)
        {
            if (obj is AccountingPeriod)
                return ((AccountingPeriod)obj).id == this.id;
            return false;
        }
        public override int GetHashCode()
        {
            return id;
        }
        public override string ToString()
        {
            return name;
        }
        public string DateRangeString(string sep,string dateRormat)
        {
            return fromDate.ToString(dateRormat) + sep + toDate.AddDays(-1).ToString(dateRormat);
        }
        public DateTime midTime
        {
            get
            {
                return fromDate.AddSeconds(toDate.Subtract(fromDate).TotalSeconds / 2);
            }
        }
    }
    [Serializable]
    public struct PayPeriodRange
    {
        public PayPeriodRange(int f, int t)
        {
            PeriodFrom = f;
            PeriodTo = t;
        }
        public int PeriodFrom;
        public int PeriodTo;
        public bool InPeriod(int p)
        {
            if (this.PeriodFrom == -1)
                return true;
            if (this.PeriodTo == -1)
                return p >= this.PeriodFrom;
            return p >= this.PeriodFrom && p < this.PeriodTo;
        }
        public bool FiniteRange
        {
            get
            {
                return this.PeriodFrom != -1 && this.PeriodTo != -1;
            }
        }
        public bool AllPeriod
        {
            get
            {
                return PeriodFrom == -1;
            }
        }
        public bool SinglePeriod
        {
            get
            {
                return PeriodFrom != -1 && PeriodTo == PeriodFrom;
            }
        }
        public bool FromNowOn
        {
            get
            {
                return PeriodFrom != -1 && PeriodTo == -1;
            }
        }
    }
}
