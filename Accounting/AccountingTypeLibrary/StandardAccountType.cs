﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTAPS.Accounting
{
    public enum StandardAccountType
    {
        Undefined=0,
        Cash=100100,
        Bank=100200,
        CashAndBank=100210,
        FixedAsset=100300,
        Inventory=100400,
        Stock=100500,
        Receivable=100600,
        Payable=200100,
        Income = 400000,
        Cost =500100,
        Expense=500200,
        
    }
}
