using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting.BDE;
namespace INTAPS.Payroll.BDE
{
    public class DHPayrollSet : DeleteReverseDocumentHandler, IDocumentServerHandler
    {
        public DHPayrollSet(INTAPS.Accounting.BDE.AccountingBDE bdeAccounting)
            : base(bdeAccounting)
        {
        }
        public override void DeleteDocument(int AID, int docID)
        {
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            PayrollSetDocument oldSet;
            deleteExisting(AID, bdePayroll, docID, false,out oldSet);
        }
        #region IDocumentServerHandler Members

        public bool CheckPostPermission(int docID, AccountDocument doc, UserSessionData userSession)
        {
            return true;
        }

        public override string GetHTML(AccountDocument doc)
        {
            PayrollSetDocument set = (PayrollSetDocument)doc;
            return "Payroll of " + set.PaperRef;
        }


        public int Post(int AID, AccountDocument doc)
        {
            PayrollSetDocument set = (PayrollSetDocument)doc;
            PayrollBDE bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");

            try
            {
                bdeAccounting.WriterHelper.BeginTransaction();
                TransactionOfBatch[] transactions = CollectTransactionsOfBatch(AID, bdePayroll, set);
                DocumentTypedReference tref=null;
                DocumentSerialType payrollRefType = bdeAccounting.getDocumentSerialType("Payroll");
                
                if (payrollRefType == null)
                    throw new ServerUserMessage("Please configure Payroll document reference type and create a range");

                if (set.AccountDocumentID != -1)
                {
                    PayrollSetDocument oldSet;
                    DocumentSerial[] ser= bdeAccounting.getDocumentSerials(set.AccountDocumentID);
                    deleteExisting(AID, bdePayroll, set.AccountDocumentID, true,out oldSet);
                    foreach (DocumentSerial ds in ser)
                    {
                        if (ds.typeID == payrollRefType.id)
                        {
                            tref = ds.typedReference;
                            break;
                        }
                    }
                }
                if (tref == null)
                {
                    tref = bdeAccounting.getNextTypedReference(payrollRefType.id, set.DocumentDate);
                    if (tref == null)
                        throw new ServerUserMessage("Unable to find a payroll reference range");
                }
                set.AccountDocumentID = bdeAccounting.RecordTransaction(AID, set, transactions, tref);
                int n = -1;
                bdePayroll.WriterHelper.Insert(bdePayroll.DBName, "PayrollSetDocument", new string[] { "accountDocumentID", "periodID", "title", "__AID" }
                    , new object[] { set.AccountDocumentID, set.payPeriod.id, set.ShortDescription, AID });
                foreach (PayrollDocument d in set.payrolls)
                {
                    string err = null;
                    if ((int)bdePayroll.WriterHelper.ExecuteScalar(
                        string.Format("Select count(*) from {0}.dbo.Payroll where employeeID={1} and periodID={2}"
                        , bdePayroll.DBName, d.employee.id, set.payPeriod.id)) > 0)
                    {
                        string msg = string.Format("Payroll already generated from employee {0}", d.employee.EmployeeNameID);
                        if (err == null)
                            err = msg;
                        else
                            err += "\n" + msg;
                    }
                    if (err != null)
                        throw new ServerUserMessage(err);
                    d.employeeID = d.employee.id;
                    d.periodID = d.payPeriod.id;
                    d.accountDocumentID = set.AccountDocumentID;
                    if (n != -1 && n != d.periodID)
                        throw new ServerUserMessage("All payrolls in a set should be from the same period");
                    n = d.periodID;
                    bdePayroll.WriterHelper.InsertSingleTableRecord(bdePayroll.DBName, d, new string[] { "__AID" }, new object[] { AID });
                    foreach (PayrollComponent c in d.components)
                    {
                        c.employeeID = d.employee.id;
                        c.periodID = d.payPeriod.id;
                        bdePayroll.WriterHelper.InsertSingleTableRecord(bdePayroll.DBName, c, new string[] { "__AID" }, new object[] { AID });
                        foreach (int id in c.dataID)
                        {
                            bdePayroll.WriterHelper.Insert(bdePayroll.DBName, "PayrollComponentData"
                                , new string[] { "employeeID", "periodID", "PCDID", "FormulaID", "dataID" }
                                , new object[] { d.employee.id, d.payPeriod.id, c.PCDID, c.FormulaID, id }
                                );
                        }
                    }
                }

                bdeAccounting.WriterHelper.CommitTransaction();
                return set.AccountDocumentID;
            }
            catch
            {
                bdeAccounting.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        private void deleteExisting(int AID, PayrollBDE bdePayroll, int docID, bool forUpdate,out PayrollSetDocument oldSet)
        {
            string sql;
            sql = string.Format(@"Select count(*) from {0}.dbo.PayrollPayment
                                where exists (Select * from {0}.dbo.Payroll where accountDocumentID={1}
                                and employeeiD=PayrollPayment.employeeid and periodid=PayrollPayment.periodID)", bdePayroll.DBName, docID);
            if ((int)bdePayroll.WriterHelper.ExecuteScalar(sql) > 0)
                throw new ServerUserMessage("It is not allowed to modify the payroll because some or all of the payroll is paid");

            oldSet = bdeAccounting.GetAccountDocument(docID, true) as PayrollSetDocument;
            if (oldSet != null)
            {
                foreach (PayrollDocument p in oldSet.payrolls)
                {
                    foreach (PayrollComponent c in p.components)
                    {


                        sql = string.Format("employeeID={0} and periodID={1} and PCDID={2} and FormulaID={3}"
                            , p.employee.id, p.payPeriod.id, c.PCDID, c.FormulaID);
                        bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.PayrollComponentData", sql);
                        bdeAccounting.WriterHelper.Delete(bdePayroll.DBName, "PayrollComponentData", sql);

                        sql = string.Format("employeeID={0} and periodID={1} and PCDID={2} and FormulaID={3}"
                        , p.employee.id, p.payPeriod.id, c.PCDID, c.FormulaID);
                        bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.PayrollComponent", sql);
                        bdeAccounting.WriterHelper.Delete(bdePayroll.DBName, "PayrollComponent", sql);

                    }
                    sql = string.Format("employeeID={0} and periodID={1}", p.employee.id, oldSet.payPeriod.id);
                    bdeAccounting.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.Payroll", sql);
                    bdeAccounting.WriterHelper.Delete(bdePayroll.DBName, "Payroll", sql);
                }
                bdeAccounting.DeleteAccountDocument(AID, docID, forUpdate);
                bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.PayrollSetDocument", "accountDocumentID=" + docID);
                bdePayroll.WriterHelper.Delete(bdePayroll.DBName, "PayrollSetDocument", "accountDocumentID=" + docID);
            }
        }
        private TransactionOfBatch[] CollectTransactionsOfBatch(int AID, PayrollBDE bdePayroll, PayrollSetDocument set)
        {
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            int companyCostCenterID = bdePayroll.SysPars.mainCostCenterID;
            foreach (PayrollDocument doc in set.payrolls)
            {
                int expense = bdePayroll.GetEmployeePayrollExpenseAcount(doc.employee);
                foreach (PayrollComponent com in doc.components)
                {
                    if (AccountBase.AmountEqual(com.Amount, 0))
                        continue;
                    foreach (TransactionOfBatch t in com.Transactions)
                    {
                        /*CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID,t.AccountID == expense ? doc.employee.costCenterID : companyCostCenterID, t.AccountID);*/
                        if(bdeAccounting.isControlAccount<Account>(t.AccountID))
                        {
                            t.AccountID = bdePayroll.addEmployeeParentAccount(AID,doc.employee.id,t.AccountID);
                        }
                        CostCenterAccount csa = bdeAccounting.GetOrCreateCostCenterAccount(AID, doc.employee.costCenterID, t.AccountID);
                        if (csa == null)
                            throw new ServerUserMessage("Invalid account found in the transactions of " + doc.employee.EmployeeNameID);
                        t.AccountID = csa.id;
                    }
                    trans.AddRange(com.Transactions);
                }
            }
            return bdeAccounting.summerizeTransactionOfBatch(trans.ToArray());
        }
        #endregion
    }
}
