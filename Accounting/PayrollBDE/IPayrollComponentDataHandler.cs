using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Payroll.BDE;
namespace INTAPS.Payroll
{
    public interface IPayrollComponentDataHandler
    {
        object GetData(int DataID);
        void CreateData(int AID,PayrollComponentData dataInfo, object Data);
        void DeleteData(int AID,int DataID);
        PayrollComponentFormulaSpec GetFormulaSpecification();
        bool CanFormulaApplyTo(AppliesToEmployees a);
        bool CanDataApplyTo(AppliesToEmployees a);
        PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp);
    }
}
