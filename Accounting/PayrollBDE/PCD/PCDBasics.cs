using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{
    public class BDCBase:IPayrollComponentDataHandler
    {
        protected PayrollBDE bdePayroll; 
        public BDCBase(PayrollBDE bde)
        {
            bdePayroll = bde;
        }
        public virtual EData GetSystemVariable(Employee e, PayrollPeriod pp, PayrollComponentData d, string var)
        {
            return EData.Empty;
        }
        #region IPayrollComponentDataHandler Members

        public virtual object GetData(int DataID)
        {
            return null;
        }

        public virtual void CreateData(int AID,PayrollComponentData dataInfo, object Data)
        {
            
        }

        public virtual void DeleteData(int AID, int DataID)
        {
            
        }

        public virtual PayrollComponentFormulaSpec GetFormulaSpecification()
        {
            return null;            
        }

        public virtual bool CanFormulaApplyTo(AppliesToEmployees a)
        {
            return true;
        }

        public virtual bool CanDataApplyTo(AppliesToEmployees a)
        {
            return true;
        }

        public virtual PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            return null;
        }
        
        #endregion
    }
    public class PCDGrossSalary : BDCBase
    {
        public PCDGrossSalary(PayrollBDE bde)
            : base(bde)
        {
        }
        public override PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            PayrollComponent com = new PayrollComponent();
            com.Name = "Gross salary";
            com.Description = "Gross salary";
            com.Deduct = false;
            com.Amount = e.grossSalary;
            com.Transactions = new TransactionOfBatch[]
            {
                new TransactionOfBatch(bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id),-com.Amount)
                ,new TransactionOfBatch(bdePayroll.GetPayrollExpenseAcount(e),com.Amount)
            };
            foreach (TransactionOfBatch t in com.Transactions)
            {
                t.Note = "Gross salary for " + e.EmployeeNameID + " for the period of " + pp.name;
            }

            return com;
        }
    }

}

