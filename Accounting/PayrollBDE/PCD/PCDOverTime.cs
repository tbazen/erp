using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{
    public class PCDOverTime : BDCBase
    {
        public PCDOverTime(PayrollBDE bde)
            : base(bde)
        {
        }
        public override PayrollComponentFormulaSpec GetFormulaSpecification()
        {
            PayrollComponentFormulaSpec spec = new PayrollComponentFormulaSpec();
            spec.VarsReadonly=false;
            spec.Vars = new string[] { "OH","LH", "WE", "HOL","EmpID","PeriodID","hSalary" };
            spec.Vals = new string[] { "", "","", "","","","" };
            spec.SystemVar = new bool[] { true,true, true, true,true,true,true };            
            spec.VarDescription = new string[]{"Off hours overtime hours"
                ,"Late hours"
                ,"Weekend overtime hours"
                ,"Holiday overtime hours"
            ,"Employee ID"
            ,"System identifier of payroll period."
            ,"Hourly Salary"
            };
            
            spec.AccountFormulaReadOnly=false;
            spec.AccountFormula = new string[] { @"EmpAccount(EmpID,""Payroll Liability"")" 
            ,@"EmpAccount(EmpID,""Payroll Expense"")"
            };
            spec.AmountFormulaReadonly = false;
            spec.AmountFormula = new string[] { "" ,""};
            
            return spec;
        }
        public override object GetData(int DataID)
        {
            INTAPS.RDBMS.SQLHelper dspReader= bdePayroll.GetReaderHelper();
            try
            {
                DataTable dat = dspReader.GetDataTable("Select OffHours,WeekEnds,Holidays,LateHours from " + bdePayroll.DBName + ".dbo.Data_OverTime where DataID=" + DataID);
                if (dat.Rows.Count == 0)
                    return null;
                OverTimeData ret = new OverTimeData();
                ret.offHours = (double)dat.Rows[0][0];
                ret.weekendHours = (double)dat.Rows[0][1];
                ret.holidayHours = (double)dat.Rows[0][2];
                ret.lateHours = (double)dat.Rows[0][3];
                return ret;
            }
            finally
            {
                bdePayroll.ReleaseHelper(dspReader);
            }
            
        }
        public override void CreateData(int AID, PayrollComponentData dataInfo, object Data)
        {
            OverTimeData ot=(OverTimeData)Data;
            INTAPS.RDBMS.SQLHelper dspWriter=bdePayroll.WriterHelper;
            try
            {
                dspWriter.BeginTransaction();
                logOldData(AID, dataInfo);
                dspWriter.ExecuteNonQuery("Delete from " + bdePayroll.DBName + ".dbo.Data_OverTime where DataID=" + dataInfo.ID);
                dspWriter.Insert(bdePayroll.DBName, "Data_OverTime"
                , new string[] { "DataID", "OffHours", "LateHours", "WeekEnds", "Holidays" }
            , new object[] { dataInfo.ID, ot.offHours, ot.lateHours, ot.weekendHours, ot.holidayHours });
                dspWriter.CommitTransaction();
            }
            catch (Exception ex)
            {
                dspWriter.RollBackTransaction();
                throw ex;
            }
        }

        private void logOldData(int AID, PayrollComponentData dataInfo)
        {
            bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.Data_OverTime", "DataID=" + dataInfo.ID);
        }
        public override void DeleteData(int AID,int DataID)
        {
            object additionalData;
            logOldData(AID,bdePayroll.GetPCData(DataID,out additionalData));
            bdePayroll.WriterHelper.Delete(bdePayroll.DBName, "Data_OverTime", "DataID=" + DataID);
        }
        public override PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            double Amount=0;
            PayrollComponentData[] data = bdePayroll.GetPCData(f.PCDID, f.ID, AppliesToObjectType.Employee, e.id, pp.id, false);
            PayrollComponent ret = new PayrollComponent();
            ret.Name = f.Name;
            ret.Description = f.Description;
            ret.dataID = new int[data.Length];
            int EmployeeLiabilityAccount=bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id);
            int i = 0;
            foreach (PayrollComponentData dat in data)
            {
                PCFSymbolProvider p = new PCFSymbolProvider(e, pp, bdePayroll,gen, this, this.GetFormulaSpecification(), f, dat);
                TransactionOfBatch[] trans = p.GenerateTransactions();
                ret.dataID[i++] = dat.ID;
                foreach (TransactionOfBatch t in trans)
                {
                    if (t.AccountID == EmployeeLiabilityAccount)
                    {
                        Amount -= t.Amount;
                    }
                }
                ret.UnionTransaction(p.GenerateTransactions());
            }
            foreach (TransactionOfBatch t in ret.Transactions)
            {
                t.Note = "Overtime payment for " +e.EmployeeNameID+" for the period of "+ pp.name;
            }
            ret.Deduct = f.Deduct;
            if(ret.Deduct)
                ret.Amount = -Amount;
            else
                ret.Amount = Amount;
            return ret;
        }
        public override EData GetSystemVariable(Employee e, PayrollPeriod pp, PayrollComponentData d, string var)
        {

            OverTimeData ot= d==null?null:this.GetData(d.ID) as OverTimeData;
            switch (var)
            {
                case "OH":
                    return new EData(DataType.Float, ot==null?0.0d:ot.offHours);
                case "LH":
                    return new EData(DataType.Float, ot == null ? 0.0d : ot.lateHours);
                case "WE":
                    return new EData(DataType.Float, ot == null ? 0.0d : ot.weekendHours);
                case "HOL":
                    return new EData(DataType.Float, ot == null ? 0.0d : ot.holidayHours);
                case "EMPID":
                    return new EData(DataType.Text, e.id);
                case "PERIODID":
                    return new EData(DataType.Int, pp.id);
                case "HSALARY":
                    double hsalary=0;
                    switch (e.salaryKind)
                    {
                        case SalaryKind.Monthly:
                            if (Account.AmountEqual(bdePayroll.SysPars.EmployeeMonthlyFullWorkingHours, 0))
                                throw new ClientServer.ServerUserMessage("Employee Monthly Full Working Hours not configured");
                            hsalary = e.grossSalary / bdePayroll.SysPars.EmployeeMonthlyFullWorkingHours;
                            break;
                        case SalaryKind.Daily:
                            if (Account.AmountEqual(bdePayroll.SysPars.EmployeeDailyFullWorkingHours, 0))
                                throw new ClientServer.ServerUserMessage("Employee Daily Full Working Hours not configured");
                            hsalary = e.grossSalary / bdePayroll.SysPars.EmployeeDailyFullWorkingHours;
                            break;
                    }
                    return new EData(DataType.Float, hsalary);
            }
            return new EData(DataType.Error, "Undefined system variable " + var);
        }        
    }
}


