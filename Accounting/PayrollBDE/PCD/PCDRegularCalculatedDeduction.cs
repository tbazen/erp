using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{
    public class PCDRegularCalculatedDeduction: BDCBase
    {
        public PCDRegularCalculatedDeduction(PayrollBDE bde)
            : base(bde)
        {
        }
        public override PayrollComponentFormulaSpec GetFormulaSpecification()
        {
            PayrollComponentFormulaSpec spec = new PayrollComponentFormulaSpec();
            spec.VarsReadonly = false;
            spec.Vars = new string[] { "EmpID", "PeriodID","Amount", "CrAccount" };
            spec.Vals = new string[] {"0", "0","","-----" };
            spec.SystemVar = new bool[] { true, true,false, false };
            spec.VarDescription = new string[]{
                
                "Employee System ID"
                ,"Payroll Period ID"
                ,"Deduction amount formula"
                ,@"Credit account, enter in double quations eg. ""0102-2200"""};

            spec.AccountFormulaReadOnly = false;
            spec.AccountFormula = new string[] { @"EmpAccount(EmpID,""Payroll Liability"")" 
            ,@"Account(CrAccount)"
            };
            spec.AmountFormulaReadonly = false;
            spec.AmountFormula = new string[] { "Amount", "-Amount" };

            return spec;
        }
        public override PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            double Amount = 0;
            PayrollComponent ret = new PayrollComponent();
            ret.Name = f.Name;
            ret.Description = f.Description;
            int EmployeeLiabilityAccount = bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id);
            if (bdePayroll.AppliesToEmployee(f.at, e.id))
            {
                PCFSymbolProvider p = new PCFSymbolProvider(e, pp, bdePayroll,gen, this, this.GetFormulaSpecification(), f, null);
                TransactionOfBatch[] trans = p.GenerateTransactions();
                foreach (TransactionOfBatch t in trans)
                {
                    if (t.AccountID == EmployeeLiabilityAccount)
                    {
                        Amount -= t.Amount;
                    }
                }
                ret.UnionTransaction(p.GenerateTransactions());
            }
            
            foreach (TransactionOfBatch t in ret.Transactions)
            {
                t.Note = f.Name +" for " + e.EmployeeNameID + " for the period of " + pp.name;
            }
            ret.Deduct = f.Deduct;
            if (ret.Deduct)
                ret.Amount = -Amount;
            else
                ret.Amount = Amount;
            return ret;
        }
        public override EData GetSystemVariable(Employee e, PayrollPeriod pp, PayrollComponentData d, string var)
        {
            switch (var)
            {
                case "EMPID":
                    return new EData(DataType.Int, e.id);
                case "PERIODID":
                    return new EData(DataType.Int, pp.id);
            }
            return new EData(DataType.Error,"Undefined system variable " + var);
        }

    }
}

