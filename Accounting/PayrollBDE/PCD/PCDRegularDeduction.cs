using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{
    public class PCDRegularDeduction : PCDSingleValueComponent
    {
        public PCDRegularDeduction(PayrollBDE bde)
            : base(bde)
        {
        }
        public override string TypeName
        {
            get
            {
                return "RegularDeduction";
            }
        }        
    }

}

