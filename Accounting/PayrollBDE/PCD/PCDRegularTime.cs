using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{
    public class PCDRegularTime : BDCBase
    {
        public PCDRegularTime(PayrollBDE bde)
            : base(bde)
        {
        }
        public override PayrollComponentFormulaSpec GetFormulaSpecification()
        {
            PayrollComponentFormulaSpec spec = new PayrollComponentFormulaSpec();
            spec.VarsReadonly = false;
            spec.Vars = new string[] { "WD", "WHPDAY", "PWH", "Salary", "EmpID", "PeriodID" };
            spec.Vals = new string[] { "", "", "", "", "", "" };
            spec.SystemVar = new bool[] { true, true, true, true, true, true };
            spec.VarDescription = new string[]{"Working Days"
                ,"Working hours per day"
                ,"Partial working hours"
                ,"Calculated Salary"
            ,"Employee ID"
            ,"System identifier of payroll period."};

            spec.AccountFormulaReadOnly = false;
            spec.AccountFormula = new string[] { @"EmpAccount(EmpID,""Payroll Liability"")"
            ,@"EmpAccount(EmpID,""Payroll Expense"")"
            };
            spec.AmountFormulaReadonly = false;
            spec.AmountFormula = new string[] { "", "" };

            return spec;
        }
        public override object GetData(int DataID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = bdePayroll.GetReaderHelper();
            try
            {
                Data_RegularTime[] data = dspReader.GetSTRArrayByFilter<Data_RegularTime>("DataID=" + DataID);
                if (data.Length == 0)
                    return null;
                return data[0];
            }
            finally
            {
                bdePayroll.ReleaseHelper(dspReader);
            }

        }
        private void logOldData(int AID, PayrollComponentData dataInfo)
        {
            bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.Data_RegularTime", "DataID=" + dataInfo.ID);
        }
        public override void CreateData(int AID, PayrollComponentData dataInfo, object Data)
        {
            Data_RegularTime regularTime = (Data_RegularTime)Data;
            INTAPS.RDBMS.SQLHelper dspWriter = bdePayroll.WriterHelper;
            try
            {
                dspWriter.BeginTransaction();
                logOldData(AID, dataInfo);
                bdePayroll.WriterHelper.Delete(bdePayroll.DBName, "Data_RegularTime", "DataID=" + dataInfo.ID);
                dspWriter.InsertSingleTableRecord<Data_RegularTime>(bdePayroll.DBName, regularTime, new string[] { "DataID" }, new object[] { dataInfo.ID });
                dspWriter.CommitTransaction();
            }
            catch (Exception ex)
            {
                dspWriter.RollBackTransaction();
                throw ex;
            }
        }
        public override void DeleteData(int AID, int DataID)
        {
            object additionalData;
            logOldData(AID, bdePayroll.GetPCData(DataID, out additionalData));
            bdePayroll.WriterHelper.Delete(bdePayroll.DBName, "Data_RegularTime", "DataID=" + DataID);
        }
        public override PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            double Amount = 0;
            PayrollComponentData[] data = bdePayroll.GetPCData(f.PCDID, f.ID, AppliesToObjectType.Employee, e.id, pp.id, false);
            if (data.Length == 0)
                data = new PayrollComponentData[] { null };
            PayrollComponent ret = new PayrollComponent();
            ret.Name = f.Name;
            ret.Description = f.Description;
            int EmployeeLiabilityAccount = bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id);

            foreach (PayrollComponentData dat in data)
            {
                PCFSymbolProvider p = new PCFSymbolProvider(e, pp, bdePayroll, gen, this, this.GetFormulaSpecification(), f, dat);
                TransactionOfBatch[] trans = p.GenerateTransactions();
                foreach (TransactionOfBatch t in trans)
                {
                    if (t.AccountID == EmployeeLiabilityAccount)
                    {
                        Amount -= t.Amount;
                    }
                }
                ret.UnionTransaction(p.GenerateTransactions());
            }
            foreach (TransactionOfBatch t in ret.Transactions)
            {
                t.Note = "Regular time wage for " + e.EmployeeNameID + " for the period of " + pp.name;
            }
            ret.Deduct = f.Deduct;
            if (ret.Deduct)
                ret.Amount = -Amount;
            else
                ret.Amount = Amount;
            return ret;
        }

        public override EData GetSystemVariable(Employee e, PayrollPeriod pp, PayrollComponentData d, string var)
        {
            Data_RegularTime regularTime;
            if (d == null)
                regularTime = new Data_RegularTime();
            else
                regularTime = this.GetData(d.ID) as Data_RegularTime;
            switch (var)
            {
                case "WD":
                    return new EData(DataType.Float, regularTime.WorkingDays);
                case "WHPDAY":
                    return new EData(DataType.Float, regularTime.WorkingHours);
                case "PWH":
                    return new EData(DataType.Float, regularTime.PartialWorkingHour);
                case "EMPID":
                    return new EData(DataType.Text, e.id);
                case "SALARY":
                    return new EData(DataType.Float, calculateSalary(e, regularTime, bdePayroll.SysPars.EmployeeMonthlyFullWorkingHours, bdePayroll.SysPars.EmployeeDailyFullWorkingHours));
                case "PERIODID":
                    return new EData(DataType.Int, pp.id);
            }
            return new EData(DataType.Error, "Undefined system variable " + var);
        }
        public double calculateSalary(Employee emp, Data_RegularTime data, double fullMonthlyWorkingHour, double fullDailyWorkingHour)
        {
            double salary = 0;

            if (emp.salaryKind == SalaryKind.Monthly) //monthly selected
            {
                if (data.WorkingDays == 0 && data.WorkingHours == 0 && data.PartialWorkingHour == 0)
                {
                    salary = emp.grossSalary;
                }
                else
                {
                    if (data.WorkingHours == 0)
                        data.WorkingHours = fullDailyWorkingHour;

                    if (data.PartialWorkingHour != 0)
                    {
                        double totalHour = data.WorkingDays * data.WorkingHours + data.PartialWorkingHour;
                        salary = (emp.grossSalary * totalHour) / fullMonthlyWorkingHour;
                    }
                    else
                    {
                        double totalHour = data.WorkingDays * data.WorkingHours;
                        salary = (emp.grossSalary * totalHour) / fullMonthlyWorkingHour;
                    }
                }

            }
            else //Daily Selected
            {
                if (data.WorkingHours == 0)
                    data.WorkingHours = fullDailyWorkingHour;

                if (data.PartialWorkingHour != 0)
                {
                    double totalHour = data.WorkingDays * data.WorkingHours + data.PartialWorkingHour;
                    salary = (emp.grossSalary * totalHour) / fullDailyWorkingHour;
                }
                else
                {
                    double totalHour = data.WorkingDays * data.WorkingHours;
                    salary = (emp.grossSalary * totalHour) / fullDailyWorkingHour;
                }

            }
            return salary;
        }
    }
}

