using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{

    public class PCDSingleValueComponent: BDCBase
    {
        public PCDSingleValueComponent(PayrollBDE bde)
            : base(bde)
        {
        }
        public virtual string ValueName
        {
            get
            {
                return "Amount";
            }
        }
        public virtual string TypeName
        {
            get
            {
                return "";
            }
        }
        protected string FieldName
        {
            get
            {
                return this.ValueName;
            }
        }
        protected string TableName
        {
            get
            {
                return "Data_" + this.TypeName;
            }
        }
        protected virtual string ValueVarDescription
        {
            get
            {
                return this.ValueName;
            }
        }
        protected virtual Dictionary<string, string> GetDefaultSystemVariables()
        {
            Dictionary<string, string> vars = new Dictionary<string, string>();
            vars.Add("EmpID", "Employee ID");
            vars.Add("PeriodID", "Period ID");
            return vars;
        }
        public override PayrollComponentFormulaSpec GetFormulaSpecification()
        {
            PayrollComponentFormulaSpec spec = new PayrollComponentFormulaSpec();
            spec.VarsReadonly = false;
            Dictionary<string, string> sysvars = GetDefaultSystemVariables();
            spec.Vars = new string[sysvars.Count + 4];
            spec.Vals = new string[sysvars.Count + 4];
            spec.VarDescription = new string[sysvars.Count + 4];
            spec.SystemVar = new bool[sysvars.Count + 4];

            spec.Vars[0] = this.ValueName;
            spec.Vals[0] = "";
            spec.VarDescription[0] = ValueVarDescription;
            spec.SystemVar[0] = true;

            spec.Vars[1] = "Deduct";
            spec.Vals[1] = this.ValueName;
            spec.VarDescription[1] = "Deduction ammount, negative value if addition";
            spec.SystemVar[1] = false;

            spec.Vars[2] = "DbAccount";
            spec.Vals[2] = @"EmpAccount(EmpID,""Payroll Liability"")";
            spec.VarDescription[2] = "Debit Account";
            spec.SystemVar[2] = false;

            spec.Vars[3] = "CrACcount";
            spec.Vals[3] = "Account(CrAccount)";
            spec.VarDescription[3] = "Credit account";
            spec.SystemVar[3] = false;
            int i = 4;
            foreach (KeyValuePair<string, string> v in sysvars)
            {
                spec.Vars[i] = v.Key;
                spec.Vals[i] = "";
                spec.VarDescription[i] = v.Value;
                spec.SystemVar[i] = true;
                i++;
            }


            spec.AccountFormulaReadOnly = true;
            spec.AccountFormula = new string[] { 
                @"DbAccount"
                ,@"CrAccount"};
            spec.AmountFormulaReadonly = false;
            spec.AmountFormula = new string[] { 
                "Deduct"
                , "-Deduct" };
            return spec;
        }
        public override object GetData(int DataID)
        {
            INTAPS.RDBMS.SQLHelper dsp = bdePayroll.GetReaderHelper();
            try
            {
                DataTable dat = dsp.GetDataTable("Select [" + this.FieldName + "] from " + bdePayroll.DBName + ".dbo.[" + this.TableName + "] where DataID=" + DataID);
                if (dat.Rows.Count == 0)
                    return null;
                SingleValueData ret = new SingleValueData();
                ret.value= (double)dat.Rows[0][0];
                return ret;
            }
            finally
            {
                bdePayroll.ReleaseHelper(dsp);
            }

        }
        private void logOldData(int AID, PayrollComponentData dataInfo)
        {
            bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo." + this.TableName, "DataID=" + dataInfo.ID);
        }
        public override void CreateData(int AID,PayrollComponentData dataInfo, object Data)
        {
            SingleValueData dat = (SingleValueData)Data;
            INTAPS.RDBMS.SQLHelper dspWriter = bdePayroll.WriterHelper;
            try
            {
                dspWriter.BeginTransaction();
                logOldData(AID, dataInfo);
                dspWriter.ExecuteNonQuery("Delete from " + bdePayroll.DBName + ".dbo.[" + this.TableName + "] where DataID=" + dataInfo.ID);
                dspWriter.Insert(bdePayroll.DBName, TableName
                , new string[] { "DataID", this.FieldName}
            , new object[] { dataInfo.ID, dat.value});
                dspWriter.CommitTransaction();
            }
            catch (Exception ex)
            {
                dspWriter.RollBackTransaction();
                throw ex; 
            }
        }
        public override void DeleteData(int AID,int DataID)
        {
            object additionalData;
            logOldData(AID, bdePayroll.GetPCData(DataID, out additionalData));
            bdePayroll.WriterHelper.Delete(bdePayroll.DBName, TableName, "DataID=" + DataID);
        }
        public override PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            double Amount = 0;
            PayrollComponentData[] data = bdePayroll.GetPCData(f.PCDID, f.ID, AppliesToObjectType.Employee, e.id, pp.id, true);
            PayrollComponent ret = new PayrollComponent();
            ret.Name = f.Name;
            ret.Description = f.Description;
            int EmployeeLiabilityAccount = bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id);
            foreach (PayrollComponentData dat in data)
            {
                PCFSymbolProvider p = new PCFSymbolProvider(e,pp, bdePayroll,gen, this, this.GetFormulaSpecification(), f, dat);
                TransactionOfBatch[] trans = p.GenerateTransactions();
                foreach (TransactionOfBatch t in trans)
                {
                    if (t.AccountID == EmployeeLiabilityAccount)
                    {
                        Amount -= t.Amount;
                    }
                }
                ret.UnionTransaction(p.GenerateTransactions());
            }
            foreach (TransactionOfBatch t in ret.Transactions)
            {
                t.Note = f.Name +" for " + e.EmployeeNameID + " for the period of " + pp.name;
            }
            ret.Deduct = f.Deduct;
            if (ret.Deduct)
                ret.Amount = -Amount;
            else
                ret.Amount = Amount;
            return ret;
        }
        public override EData GetSystemVariable(Employee e,PayrollPeriod pp, PayrollComponentData d, string var)
        {
            if (var == this.ValueName.ToUpper())
            {
                if (d == null)
                    return new EData(DataType.Empty, null);
                SingleValueData ot = this.GetData(d.ID) as SingleValueData;
                return new EData(DataType.Float, ot.value);

            }
            switch (var)
            {
                case "EMPID":
                    return new EData(DataType.Text, e.id);
                case "PERIODID":
                    return new EData(DataType.Int, pp.id);

            }
            return new EData(DataType.Error,"Undefined system variable " + var);
        }

    }
}

