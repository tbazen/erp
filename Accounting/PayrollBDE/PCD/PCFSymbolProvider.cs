using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.BDE.PCD
{
    public class PCFSymbolProvider:ISymbolProvider,IFunction
    {
        List<string> m_systemVars;
        PayrollComponentFormula m_formula;
        BDCBase m_component;
        PayrollComponentData m_data;
        PayrollBDE m_payroll;
        Employee m_employee;
        PayrollPeriod m_period;
        PayrollBDE.PayrollGenerator m_generator;
        public PCFSymbolProvider(Employee e,PayrollPeriod p, PayrollBDE bde,PayrollBDE.PayrollGenerator gen, BDCBase comp,PayrollComponentFormulaSpec spec, PayrollComponentFormula f,PayrollComponentData data)
        {
            m_generator=gen;
            m_payroll = bde;
            m_data = data;
            m_formula = f;
            m_component = comp;
            m_systemVars = new List<string>();
            m_employee = e;
            m_period = p;
            for (int i = 0; i < spec.Vars.Length;i++ )
            {
                if (spec.SystemVar[i])
                    m_systemVars.Add(spec.Vars[i].ToUpper());
            }
            m_getVarStatck = new List<string>();
        }

        Dictionary<string, EData> m_evaluatedVariables;
        Dictionary<string, bool> m_vars;
        List<string> m_getVarStatck;
        public EData[] getEvaluatedVariable(out string[] names)
        {
            EData[] ret = new EData[m_evaluatedVariables.Count];
            m_evaluatedVariables.Values.CopyTo(ret,0);
            names = new string[m_evaluatedVariables.Count];
            m_evaluatedVariables.Keys.CopyTo(names,0);
            return ret;
        }
        EData GetVariableValue(string var)
        {
            if (m_getVarStatck.Contains(var))
                return new EData(DataType.Error, "Circular reference of " + var);
            if (!m_vars.ContainsKey(var))
                return new EData(DataType.Error, "Undefined variable " + var);

            try
            {
                m_getVarStatck.Add(var);
                if (!m_vars[var])
                {
                    Symbolic s = new Symbolic();
                    s.SetSymbolProvider(this);
                    s.Expression = m_formula.GetVarFormula(var);
                    EData ret = s.Evaluate();
                    m_evaluatedVariables[var] = ret;
                    m_vars[var] = true;
                    return ret;
                }
                return m_evaluatedVariables[var];
            }
            finally
            {
                m_getVarStatck.RemoveAt(m_getVarStatck.Count - 1);
            }
        }
        public TransactionOfBatch[] GenerateTransactions()
        {
            m_evaluatedVariables = new Dictionary<string, EData>();
            m_vars = new Dictionary<string, bool>();
            
            foreach (string sv in m_systemVars)
            {
                m_evaluatedVariables.Add(sv, m_component.GetSystemVariable(m_employee,m_period, m_data, sv));
                m_vars.Add(sv, true);
            }

            foreach (string fv in m_formula.Vars)
            {
                if(m_vars.ContainsKey(fv.ToUpper()))
                    continue;
                m_evaluatedVariables.Add(fv.ToUpper(), EData.Empty);
                m_vars.Add(fv.ToUpper(), false);
            }

            TransactionOfBatch[] ret = new TransactionOfBatch[m_formula.AmountFormula.Length];
            for (int i = 0; i < m_formula.AmountFormula.Length; i++)
            {
                Symbolic ex = new Symbolic();
                ex.SetSymbolProvider(this);
                ex.Expression = m_formula.AccountFormula[i];
                EData eval= ex.Evaluate();
                if (eval.Type == DataType.Error)
                    throw new Exception(((FSError)eval.Value).Message);
                int AccountID = (int)eval.Value;

                ex = new Symbolic();
                ex.SetSymbolProvider(this);
                ex.Expression = m_formula.AmountFormula[i];
                eval = ex.Evaluate();
                if (eval.Type == DataType.Error)
                    throw new Exception(((FSError)eval.Value).Message);
                
                double Amount = (double)eval.Value;
                ret[i] = new TransactionOfBatch();
                ret[i].AccountID = AccountID;
                ret[i].Amount = Amount;
            }
            return ret;
        }

        #region ISymbolProvider Members

        public FunctionDocumentation[] GetAvialableFunctions()
        {
            return new FunctionDocumentation[0];
        }

        public EData GetData(URLIden iden)
        {
            return GetVariableValue(iden.Element.ToUpper());
        }

        public EData GetData(string symbol)
        {
            return GetVariableValue(symbol.ToUpper());
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            return null;
        }

        public IFunction GetFunction(URLIden iden)
        {
            return GetFunction(iden.Element);
        }

        public IFunction GetFunction(string symbol)
        {
            IFunction func = m_payroll.GetEvaluatorFunction(symbol.ToUpper());
            if (func != null)
                return func;
            if (symbol.ToUpper() == this.Symbol.ToUpper())
                return this;
            if(CalcGlobal.Functions.Contains(symbol.ToUpper()))
                return (IFunction)CalcGlobal.Functions[symbol.ToUpper()];
            return null;
        }

        public bool SymbolDefined(string Name)
        {
            return m_vars.ContainsKey(Name.ToUpper());
        }

        #endregion

        #region IFunction Members

        public string Name
        {
            get { return "Get component amount."; }
        }

        public string Symbol
        {
            get { return "Component"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public int ParCount
        {
            get { return 2; }
        }

        public EData Evaluate(EData[] Pars)
        {
            string PCD = (string)Pars[0].Value;
            string F = null;
            if (Pars.Length > 1)
                if (!string.IsNullOrEmpty(Pars[1].Value as string))
                    F = Pars[1].Value as string;
            PayrollComponent c = m_generator.GetComponent(PCD, F);
            if(c==null)
                return new EData(DataType.Float, (double)0);
            return new EData(DataType.Float, c.Amount);
        }

        #endregion
    }
}

