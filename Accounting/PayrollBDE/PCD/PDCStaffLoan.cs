using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using INTAPS.Accounting;
using INTAPS.Evaluator;
using INTAPS.ClientServer;

namespace INTAPS.Payroll.BDE.PCD
{
    public class PDCStaffLoan : BDCBase
    {
        public PDCStaffLoan(PayrollBDE bde)
            : base(bde)
        {
        }
        public override object GetData(int DataID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = bdePayroll.GetReaderHelper();
            try
            {
                DataTable dat = dspReader.GetDataTable("Select ReturnAmount,Exception from "+bdePayroll.DBName+".dbo.Data_LoanReturn where DataID=" + DataID);
                if (dat.Rows.Count == 0)
                    return null;
                StaffLoanPaymentData ret = new StaffLoanPaymentData();
                ret.Amount = (double)dat.Rows[0][0];
                ret.Exception = (bool)dat.Rows[0][1];
                return ret;
            }
            finally
            {
                bdePayroll.ReleaseHelper(dspReader);
            }

        }
        private void logOldData(int AID, PayrollComponentData dataInfo)
        {
            bdePayroll.WriterHelper.logDeletedData(AID, bdePayroll.DBName + ".dbo.Data_LoanReturn", "DataID=" + dataInfo.ID);
        }
        public override void CreateData(int AID,PayrollComponentData dataInfo, object Data)
        {
            if (!dataInfo.at.ASpecificEmployee)
                throw new Exception("Staff loan can be registered only for a specific employee.");

            StaffLoanPaymentData loan = (StaffLoanPaymentData)Data;
            if (loan.Exception)
            {
                if (!dataInfo.periodRange.SinglePeriod)
                    throw new Exception("Exception can only specified for a single month.");
                PayrollComponentData[] testdat = bdePayroll.GetPCData(dataInfo.PCDID, -1, AppliesToObjectType.Employee, dataInfo.at.Employees[0], dataInfo.periodRange.PeriodFrom, false);
                foreach (PayrollComponentData td in testdat)
                {
                    StaffLoanPaymentData tl = GetData(td.ID) as StaffLoanPaymentData;
                    if (tl == null)
                        continue;
                    if (tl.Exception)
                        throw new Exception("Only one exception payment can be registered for a single employee.");
                }
            }
            try
            {
                bdePayroll.WriterHelper.BeginTransaction();
                logOldData(AID, dataInfo);
                bdePayroll.WriterHelper.ExecuteNonQuery("Delete from " + bdePayroll.DBName + ".dbo.Data_LoanReturn where DataID=" + dataInfo.ID);
                bdePayroll.WriterHelper.Insert(bdePayroll.DBName, "Data_LoanReturn"
                , new string[] { "DataID", "ReturnAmount", "Exception" }
            , new object[] { dataInfo.ID, loan.Amount, loan.Exception });
                bdePayroll.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdePayroll.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        public override void DeleteData(int AID,int DataID)
        {
            object additionalData;
            logOldData(AID, bdePayroll.GetPCData(DataID, out additionalData));
            bdePayroll.WriterHelper.Delete(bdePayroll.DBName, "Data_LoanReturn", "DataID=" + DataID);
        }
        public override PayrollComponent GenerateTransactions(PayrollBDE.PayrollGenerator gen, Employee e, PayrollComponentFormula f, PayrollPeriod pp)
        {
            PayrollComponentData[] data = bdePayroll.GetPCData(PayrollBDE.PCD_STAFF_LOAN,f.ID, AppliesToObjectType.Employee, e.id, pp.id, false);
            PayrollComponent ret = new PayrollComponent();
            ret.Name = f.Name;
            ret.Description = "";
            ret.Deduct = true;

            double LongTerm = 0;
            double Advance = 0;
            bool longTerm = f.Vals[0] == "1";
            foreach (PayrollComponentData d in data)
            {
                StaffLoanPaymentData tl = GetData(d.ID) as StaffLoanPaymentData;
                if (tl == null)
                    continue;
                if (tl.Exception)
                {
                    if (longTerm)
                    {
                        LongTerm = tl.Amount;
                        Advance = 0;
                    }
                    else
                    {
                        LongTerm = 0;
                        Advance = tl.Amount;
                    }
                    break;
                }
                if (longTerm)
                    LongTerm += tl.Amount;
                else
                    Advance += tl.Amount;
            }
            //AUDIT for COMMENTED CODE below:- why use 'staff loan payable account id' for long term loan account?
            //int longTermAc = bdePayroll.GetStaffLoanPayableAccount(e.id);
            if (bdePayroll.SysPars.mainCostCenterID == 0)
            {
                throw new ServerUserMessage("Configuration inconsistency: Staff Head Quarter cost center is not set under 'Staff account configuration page' in settings!");
            }
            int longTermAc = bdePayroll.GetLongTermLoanAccount(e.id);
            if (longTermAc == -1)
                throw new ServerUserMessage("Long term staff loan account not set on the system configuration.");
            CostCenterAccount csa = bdePayroll.Accounting.GetOrCreateCostCenterAccount(-1, bdePayroll.SysPars.mainCostCenterID, longTermAc);
            if(csa==null)
                throw new ServerUserMessage("Configuration inconsistency: long term account not added to the cost center the employee currently belongs to.");
            double balLongTerm = bdePayroll.Accounting.GetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY,gen.generateDate).DebitBalance;
            
            int advanceAc = bdePayroll.GetSalaryAdvanceAccount(e.id);
            if (advanceAc == -1)
                throw new ServerUserMessage("Staff salary advance account not set");
            csa = bdePayroll.Accounting.GetOrCreateCostCenterAccount(-1, bdePayroll.SysPars.mainCostCenterID, advanceAc);
            if (csa == null)
                throw new ServerUserMessage("Configuration inconsistency: short term account not added to the cost center the employee currently belongs to.");
            double balAdvance = bdePayroll.Accounting.GetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, gen.generateDate).DebitBalance;

           // string accName = bdePayroll.Accounting.GetAccount<Account>(csa.accountID).Name;
          //  string costCenter = bdePayroll.Accounting.GetAccount<CostCenter>(csa.costCenterID).Name;
            if (balLongTerm < LongTerm)
                LongTerm = balLongTerm;
            if (balAdvance < Advance)
                Advance = balAdvance;
            ret.Amount = LongTerm + Advance;
            if (ret.Amount == 0)
                ret.Transactions = new TransactionOfBatch[0];
            else
            {
                if (Advance != 0)
                {
                    ret.Transactions = new TransactionOfBatch[]
                                        {
                                            new TransactionOfBatch(advanceAc,-Advance,"Salary advance return by "+e.EmployeeNameID)
                                            ,new TransactionOfBatch(bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id),ret.Amount,"Staff loan return")
                                        };
                }
                else
                {
                    ret.Transactions = new TransactionOfBatch[]
                                        {
                                            new TransactionOfBatch(longTermAc,-LongTerm,"Salary advance return by "+e.EmployeeNameID)
                                            ,new TransactionOfBatch(bdePayroll.GetEmployeeUncollectedPaymentAccount(e.id),LongTerm,"Staff loan return")
                                        };
                }
            }
            return ret;
        }
        public override PayrollComponentFormulaSpec GetFormulaSpecification()
        {
            PayrollComponentFormulaSpec spec = new PayrollComponentFormulaSpec();
            spec.VarsReadonly = false;
            spec.Vars = new string[] { "LongTerm" };
            spec.Vals = new string[] { "0"};
            spec.SystemVar = new bool[] { false};
            spec.VarDescription = new string[]{"Set to 1 for longterm"};

            spec.AccountFormulaReadOnly = true;
            spec.AccountFormula = new string[] {};
            spec.AmountFormulaReadonly = true;
            spec.AmountFormula = new string[] { };

            return spec;
        }
    }
}
