using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using System.Collections;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        public int GetEmployeeSubAccount(int EmployeeID, int ControlAccount)
        {
            Employee e = GetEmployee(EmployeeID);
            Account[] cchs = m_Accounting.GetChildAllAcccount<Account>(ControlAccount);
            foreach (Account a in cchs)
            {
                foreach (int eaID in e.accounts)
                {
                    if (eaID == a.id)
                        return a.id;
                }
            }
            return ControlAccount;
        }
        public int addEmployeeParentAccount(int AID, int employeeID, int accountID)
        {
            Employee e = this.GetEmployee(employeeID);
            foreach (int a in e.accounts)
            {
                if (Accounting.IsControlOf<Account>(a, accountID))
                    return a;
            }
            Account parent = Accounting.GetAccount<Account>(accountID);
            string accountCode = parent.Code + "-" + e.employeeID;
            Account account = Accounting.GetAccount<Account>(accountCode);
            if (account == null)
            {
                account = new Account();
                account.id = -1;
            }
            account.Name = parent.Name + "-" + e.employeeName;
            account.Code = parent.Code + "-" + e.employeeID;
            account.ActivateDate = DateTime.Now.Date;
            account.CreationDate = DateTime.Now.Date;
            account.CreditAccount = parent.CreditAccount;
            account.Description = "Automatically Created Employee Account - don't modify or delete";
            account.protection = AccountProtection.SystemAccount;
            account.Status = AccountStatus.Activated;
            account.PID = accountID;
            if(account.id==-1)
                account.id=Accounting.CreateAccount(AID, account);
            else
                Accounting.UpdateAccount(AID, account);
            Accounting.CreateCostCenterAccount(AID, e.costCenterID, account.id);
            dspWriter.Insert(this.DBName,"EmployeeAccount",new string[]{"EmployeeID","AccountID","__AID"},
            new object[]{employeeID,account.id,AID});
            return account.id;
        }

        public AccountBase[] GetParentAcccountsByEmployee(int employeeId)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string sql = "select a.ID, a.Code,a.Name,a.CreditAccount from {0}.dbo.EmployeeAccount as e  INNER JOIN Accounting_2006.dbo.Account as a on e.AccountID = a.Id where e.EmployeeID={1}";
                sql = string.Format(sql, this.DBName, employeeId);
                DataTable dataTable = dspReader.GetDataTable(sql);
                List<AccountBase> accounts = new List<AccountBase>();
                foreach (DataRow row in dataTable.Rows)
                {
                    accounts.Add(new Account()
                    {
                        id = (int)row.ItemArray[0],
                        Code = (string)row.ItemArray[1],
                        Name = (string)row.ItemArray[2],
                        CreditAccount = (bool)row.ItemArray[3]
                    });
                }
                AccountBase[] accountsArr = accounts.ToArray();
                return accountsArr;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public Account GetEmployeeSubAccount(Employee employee, int ControlAccount)
        {
            Account[] cchs = m_Accounting.GetChildAllAcccount<Account>(ControlAccount);
            foreach (Account a in cchs)
            {
                foreach (int eaID in employee.accounts)
                {
                    if (eaID == a.id)
                        return a;
                }
            }
            return null;
        }
        public Account GetEmployeeSubAccount(Employee employee, string ControlAccountCode)
        {
            int controlAccount = m_Accounting.GetAccount<Account>(ControlAccountCode).id;
            Account[] cchs = m_Accounting.GetChildAllAcccount<Account>(controlAccount);
            foreach (Account a in cchs)
            {
                foreach (int eaID in employee.accounts)
                {
                    if (eaID == a.id)
                        return a;
                }
            }
            return null;
        }
        public int GetEmployeeUncollectedPaymentAccount(int EmployeeID)
        {
            return GetEmployeeSubAccount(EmployeeID,m_sysPars.UnclaimedSalaryAccount);
        }
        public int GetPayrollExpenseAcount(Employee employee)
        {
            if (employee.expenseParentAccountID > 0)
                return employee.expenseParentAccountID;
            if (employee.accountAsCost)
            {
                if (employee.employmentType == EmploymentType.Permanent)
                    return m_sysPars.PermanentPayrollCostAccount;
                return m_sysPars.TemporaryPayrollCostAccount;
            }
            else
            {
                if (employee.employmentType == EmploymentType.Permanent)
                    return m_sysPars.PermanentPayrollExpenseAccount;
                return m_sysPars.TemporaryPayrollExpenseAccount;
            }
        }
        public int GetEmployeePayrollExpenseAcount(Employee employee)
        {
            if (employee.accountAsCost)
            {
                if (employee.employmentType == EmploymentType.Permanent)
                    return GetEmployeeSubAccount(employee.id, m_sysPars.PermanentPayrollCostAccount);
                return GetEmployeeSubAccount(employee.id, m_sysPars.TemporaryPayrollCostAccount);

            }
            else
            {
                if (employee.employmentType == EmploymentType.Permanent)
                    return GetEmployeeSubAccount(employee.id, m_sysPars.PermanentPayrollExpenseAccount);
                return GetEmployeeSubAccount(employee.id, m_sysPars.TemporaryPayrollExpenseAccount);
            }
        }
        public int GetEmployeelPensionContributionExpenseAcount(Employee employee)
        {
            if (employee.accountAsCost)
            {
                if (employee.employmentType == EmploymentType.Permanent)
                    return GetEmployeeSubAccount(employee.id, m_sysPars.PermanentPayrollPensionContributionCostAccount);
                return -1;

            }
            else
            {
                if (employee.employmentType == EmploymentType.Permanent)
                    return GetEmployeeSubAccount(employee.id, m_sysPars.PermanentPayrollPensionContributionExpenseAccount);
                return -1;
            }
        }
        public int GetPerdiemExpenseAccountID(Employee employee)
        {
            if (employee.accountAsCost)
            {
                return m_sysPars.staffPerDiemCostAccountID;
            }
            else
            {
                return m_sysPars.staffPerDiemAccountID;
            }
        }
        
        public int GetIncomeTaxAccountID(Employee employee) //AUDIT: hard coded rule
        {
            return m_sysPars.staffIncomeTaxAccountID;
            //if (AccountBase.AmountGreater(employee.grossSalary,150))
            //{
            //    return m_sysPars.staffIncomeTaxAccountID;
            //}
            //else
            //{
            //    return -1;
            //}
        }

        public int GetPensionLiabilityAccountID(Employee employee)
        {
            if(employee.employmentType == EmploymentType.Permanent && employee.calculateAge()<=60)
            //if (employee.employmentType == EmploymentType.Permanent)
            {
                return m_sysPars.staffPensionPayableAccountID;
            }
            else
            {
                return -1;
            }
        }

        public int GetCostSharingLiabilityAccountID(Employee employee)
        {
            if (employee.costSharingStatus != CostSharingStatus.NotApplicable)
            {
                return m_sysPars.staffCostSharingPayableAccountID;
            }
            else
            {
                return -1;
            }
        }

        public int GetShareHolderLoanAccountID(Employee employee)
        {
            if (employee.shareHolder)
            {
                return m_sysPars.ShareHoldersLoanAccountID;
            }
            else
            {
                return -1;
            }
        }
        public int GetOwnersEquityAccountID(Employee employee)
        {
            if (employee.shareHolder)
            {
                return m_sysPars.OwnersEquityAccount;
            }
            else
            {
                return -1;
            }
        }
       
        public int GetCashAccount()
        {
            return m_sysPars.PayrollCashierAccountID;
        }
        public int GetIncomeTaxLiablilityAccount(int EmployeeID)
        {
            return m_sysPars.IncomeTaxLiabilityAccount;
        }
        public int GetStaffLoanPayableAccount(int EmployeeID)
        {
            return GetEmployeeSubAccount(EmployeeID, m_sysPars.StaffLoanAccount);
        }
        public int GetSalaryAdvanceAccount(int EmployeeID)
        {
            return GetEmployeeSubAccount(EmployeeID, m_sysPars.SalaryAdvanceAccount);
        }
        public int GetLongTermLoanAccount(int employeeID)
        {
            return GetEmployeeSubAccount(employeeID, m_sysPars.staffLongTermLoanAccountID);
        }
        public Employee[] SearchEmployee(long time, int orgUnitID, string Query, bool includeDismissed, EmployeeSearchType SearchType, int index, int PageSize, out int NResult)
        {
            INTAPS.RDBMS.SQLHelper healper = base.GetReaderHelper();
            IDataReader reader = null;
            try
            {
                string sql = "Select ID,EmployeeID,EmployeeName,orgUnitID,status from {0}.dbo.Employee where ticksFrom<={1} and (ticksTo=-1 or ticksTo>{1}) order by EmployeeID";
                sql=string.Format(sql, this.DBName,time);
                reader = healper.ExecuteReader(sql);
                List<Employee> _ret = new List<Employee>();
                NResult = 0;
                while (reader.Read())
                {

                    int ID = (int)reader[0];
                    string EmpID = (string)reader[1];
                    string EmpName = (string)reader[2];
                    int thisOrgUnitID = (int)reader[3];
                    EmployeeStatus status=(EmployeeStatus)(int)reader[4];
                    if (!includeDismissed && status == EmployeeStatus.Fired)
                        continue;
                    if (orgUnitID != -1)
                        if (thisOrgUnitID != orgUnitID)
                            continue;
                    if (!string.IsNullOrEmpty(Query))
                    {
                        if( !(
                            ( ((SearchType&EmployeeSearchType.Name)==EmployeeSearchType.Name) && INTAPS.AmharicText.Contains(EmpName, Query))
                            || (((SearchType & EmployeeSearchType.ID) == EmployeeSearchType.ID) && INTAPS.AmharicText.Contains(EmpID, Query))
                            ))
                            continue;
                    }
                    NResult++;
                    if (NResult <= index || NResult > (index + PageSize))
                        continue;
                    Employee e = this.GetEmployee(ID,time);
                    _ret.Add(e);
                }
                return _ret.ToArray();
            }
            finally
            {
                if (reader != null)
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
                base.ReleaseHelper(healper);
            }
        }
        public void ChangeEmployeeOrgUnit(int AID, int ID, int NewOrgUnitID, DateTime date)
        {
            long ticks = DateTime.Now.Ticks;
            Employee emp = GetEmployee(ID, ticks);
            emp.ticksFrom = date.Ticks;
            emp.orgUnitID = NewOrgUnitID;
            Update(AID, emp, false);
        }

        public PayrollPeriod getPreviosPeriod(int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPreviousPeriod<PayrollPeriod>(dspReader, "PayPeriod", GetPayPeriod(periodID));
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }

        }
    }

}
