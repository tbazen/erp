using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using INTAPS.ClientServer;
using INTAPS;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        void ValidAccounts(Employee emp)
        {
            foreach (int a in emp.accounts)
            {
                Account ac = m_Accounting.GetAccount<Account>(a);
                if (ac == null)
                    //continue;
                    throw new ServerUserMessage("Invalid account ID:" + a);
                if (ac.isControl)
                    throw new ServerUserMessage(ac.Code + " is a control account hence it can't be used as an employee account");
            }
        }
        void ValidateLoginName(INTAPS.RDBMS.SQLHelper helper, Employee emp)
        {
            if (string.IsNullOrEmpty(emp.loginName))
                return;
            int uid=ApplicationServer.SecurityBDE.GetUIDForName(emp.loginName);
            if (uid < 1)
                throw new ServerUserMessage("Invalid login name:" + emp.loginName);
            object usedID;
            if ((usedID = helper.ExecuteScalar(string.Format("Select id from {0}.dbo.Employee where loginName='{1}' and id<>{2}", this.DBName, emp.loginName, emp.id))) is int)
            {
                Employee e = GetEmployee((int)usedID);
                throw new ServerUserMessage("Login name already used for employee:" + e.EmployeeNameID);
            }
        }
        private bool TINUsed(INTAPS.RDBMS.SQLHelper dspWriter, string TIN, int exclude)
        {
            int c = 0;
            if (TIN != String.Empty)
            {
                c = (int)dspWriter.ExecuteScalar(String.Format("Select count(*) from {0}.dbo.Employee where TIN='{1}' and status={2} and ID<>{3}", DBName, TIN, ((int)EmployeeStatus.Enrolled), exclude));
            }
            return c > 0;

        }
        public int RegisterEmployee(int AID,int companyCostCenterID,Employee emp)
        {
            if (GetEmployeeByLoginName(emp.loginName) != null)
                throw new ServerUserMessage("Login name already used");
            lock (dspWriter)
            {
                if (emp.ticksFrom == -1)
                    emp.ticksFrom = emp.enrollmentDate.Ticks;
                emp.ticksTo = -1;
                if (string.IsNullOrEmpty(emp.employeeID))
                    throw new Exception("Employee ID must be provided");
                if (string.IsNullOrEmpty(emp.employeeName))
                    throw new Exception("Employee name must be provided");
                ValidAccounts(emp);
                
                dspWriter.setReadDB(this.DBName);
                try
                {

                    dspWriter.BeginTransaction();
                    ValidateLoginName(dspWriter, emp);
                    if (emp.status == EmployeeStatus.Enrolled)
                    {
                        if (!AccountingBDE.IsWithinValidRange(emp.enrollmentDate))
                            throw new Exception("Invalid enrollment date.");
                        if (AccountingBDE.IsFutureDate(emp.enrollmentDate))
                            throw new Exception("Enrollment date can't be in the future.");
                        if (IDUsed(dspWriter, emp.employeeID, emp.id))
                            throw new Exception(String.Format("EmployeeID:{0} already used.", emp.employeeID));
                        if (TINUsed(dspWriter, emp.TIN, emp.id))
                            throw new Exception(String.Format("Employee TIN: {0} already used.", emp.TIN));
                    }
                    else if (emp.status == EmployeeStatus.Fired)
                    {

                        if (!AccountingBDE.IsWithinValidRange(emp.enrollmentDate))
                            throw new Exception("Invalid enrollment date.");

                        if (!AccountingBDE.IsWithinValidRange(emp.dismissDate))
                            throw new Exception("Invalid date of firing.");

                        if (AccountingBDE.IsFutureDate(emp.dismissDate))
                            throw new Exception("Date of firing can't be in the future.");

                        if (AccountingBDE.IsFutureDate(emp.enrollmentDate))
                            throw new Exception("Enrollment date can't be in the future.");
                        if (emp.dismissDate < emp.enrollmentDate)
                            throw new Exception("Date of firing can't be before enorllment date.");
                    }
                    if (emp.grossSalary < 0)
                        throw new Exception("Negative salary not supported");
                    if (emp.costCenterID != -1)
                    {
                        CostCenter cs = m_Accounting.GetAccount<CostCenter>(emp.costCenterID);
                        if (cs == null)
                            throw new Exception("Cost center doesn't exist");
                    }
                    emp.id = INTAPS.ClientServer.AutoIncrement.GetKey("Payroll.EmpID");
                    dspWriter.InsertSingleTableRecord<Employee>(m_DBName, emp, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.InsertColumn<int>(m_DBName, "EmployeeAccount", new string[] { "EmployeeID", "__AID", "AccountID" }, new object[] { emp.id, AID }
                    , emp.accounts);
                    createEmpleeCostCenterAccounts(AID, emp);
                    dspWriter.CommitTransaction();
                    return emp.id;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        private void createEmpleeCostCenterAccounts(int AID, Employee emp)
        {
            foreach (int ac in emp.accounts)
            {
                CostCenterAccount csAccount = m_Accounting.GetCostCenterAccount(emp.costCenterID, ac);
                if (csAccount == null)
                {
                    m_Accounting.CreateCostCenterAccount(AID, emp.costCenterID, ac);
                }
            }
        }
        public void Enroll(int AID, DateTime date, int ID)
        {
            lock (dspWriter)
            {

                if (AccountingBDE.IsFutureDate(date))
                    throw new Exception("Enollment date can't be in the future");
                
                Employee employee = GetEmployee(ID);
                if (employee == null)
                    throw new Exception("Employee doesn't exist in database ID:" + ID);
                if (employee.status != EmployeeStatus.Fired)
                    throw new Exception("Employee allready enrolled.");
                employee.ticksFrom = date.Ticks;
                employee.status = EmployeeStatus.Enrolled;
                Update(AID, employee, false);
            }
        }
        public void Fire(int AID,DateTime date, int ID)
        {
            lock (dspWriter)
            {
                if (AccountingBDE.IsFutureDate(date))
                    throw new Exception("Date of firing can't be in the future");
                Employee emp = GetEmployee(ID);
                if (emp==null)
                    throw new Exception("Employee doesn't exist in database ID:" + ID);
                if (emp.status == EmployeeStatus.Fired)
                {
                    throw new Exception("Employee allready fired.");
                }
                emp.dismissDate = date;
                emp.ticksFrom = date.Ticks;
                emp.status = EmployeeStatus.Fired;
                Update(AID, emp, false);
            }
        }

        // BDE exposed
        public void deleteEmployeeVersion(int AID, int ID, long ticks)
        {
            Employee emp = GetEmployee(ID, ticks);
            if (emp == null)
                throw new ServerUserMessage("Invalid employee ID and version");
            if (this.isEmployeeUsedInPayroll(ID, ticks))
                throw new ServerUserMessage("This employee has payrolls. It can't be deleted");
            long[] versions = this.getEmployeVersions(ID);
            if (versions.Length == 1)
            {
                DeleteEmployee(AID, ID);
                return;
            }

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    logOldEmployee(AID, ID, ticks, false);
                    dspWriter.Delete(m_DBName, "Employee", "ID=" + ID + " and ticksFrom=" + ticks);
                    dspWriter.Update(m_DBName, "Employee", new string[] { "ticksTo" }, new object[] { emp.ticksTo }, 
                        "id={0} and ticksTo={1}".format(ID,emp.ticksFrom));
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void DeleteEmployee(int AID,int ID)
        {
            
            lock (dspWriter)
            {
                dspWriter.setReadDB (this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    long[] versions = this.getEmployeVersions(ID);
                    foreach (long v in versions)
                        if (this.isEmployeeUsedInPayroll(ID, v))
                            throw new ServerUserMessage("This employee has payrolls. It can't be deleted");
                    logOldEmployee(AID, ID,-1,true);
                    int[] acnt = getEmployeeAccounts(ID);
                    foreach (int a in acnt)
                    {
                        if(m_Accounting.GetAccount<Account>(a)!=null)
                            m_Accounting.DeleteAccount<Account>(AID, a, true);
                    }
                    PayrollComponentData[] data= GetPCData(ID);
                    foreach (PayrollComponentData pd in data)
                    {
                        if (pd.at.ASpecificEmployee)
                            DeleteData(AID, pd.ID);
                    }
                    
                    dspWriter.Delete(m_DBName, "Employee", "ID=" + ID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }                    
            }
        }
        public void Update(int AID, Employee emp, bool logAccounts)
        {
            lock (dspWriter)
            {
                
                if (string.IsNullOrEmpty(emp.employeeID))
                    throw new Exception("Employee ID must be provided");
                if (string.IsNullOrEmpty(emp.employeeName))
                    throw new Exception("Employee name must be provided");


                Employee OldData=GetEmployee(emp.id,emp.ticksFrom);
                if (OldData==null || string.IsNullOrEmpty(OldData.loginName))
                {
                    if (GetEmployeeByLoginName(emp.loginName) != null)
                        throw new ServerUserMessage("Login name already used");
                }
                else
                {
                    if(!OldData.loginName.Equals(emp.loginName,StringComparison.CurrentCultureIgnoreCase))
                        throw new ServerUserMessage("Login name can't be changed once set");
                }
                ValidAccounts(emp);
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    ValidateLoginName(dspWriter, emp);
                    //if (OldData.status == EmployeeStatus.Enrolled)
                        if (IDUsed(dspWriter, emp.employeeID, emp.id))
                            throw new Exception("EmployeeID:" + emp.employeeID + " already used.");
                    
                    

                    if (emp.status == EmployeeStatus.Enrolled)
                    {
                        if (!AccountingBDE.IsWithinValidRange(emp.enrollmentDate))
                            throw new Exception("Invalid enrollment date.");
                        if (AccountingBDE.IsFutureDate(emp.enrollmentDate))
                            throw new Exception("Enrollment date can't be in the future.");
                        if (IDUsed(dspWriter, emp.employeeID, emp.id))
                            throw new Exception("EmployeeID:" + emp.employeeID + " already used.");
                    }
                    else if (emp.status == EmployeeStatus.Fired)
                    {

                        if (!AccountingBDE.IsWithinValidRange(emp.enrollmentDate))
                            throw new Exception("Invalid enrollment date.");

                        if (!AccountingBDE.IsWithinValidRange(emp.dismissDate))
                            throw new Exception("Invalid date of firing.");

                        if (AccountingBDE.IsFutureDate(emp.dismissDate))
                            throw new Exception("Date of firing can't be in the future.");

                        if (AccountingBDE.IsFutureDate(emp.enrollmentDate))
                            throw new Exception("Enrollment date can't be in the future.");
                        if (emp.dismissDate < emp.enrollmentDate)
                            throw new Exception("Date of firing can't be before enorllment date.");
                    }
                    
                    if (OldData != null && OldData.ticksTo == -1 && OldData.status != emp.status)//latest version
                    {
                        foreach (int account in emp.accounts)
                        {
                            Account acc = this.Accounting.GetAccount<Account>(account);
                            if (emp.status == EmployeeStatus.Enrolled)
                                this.Accounting.ActivateAcount<Account>(AID, account, new DateTime(emp.ticksFrom));
                            else
                                this.Accounting.DeactivateAccount<Account>(AID, account, new DateTime(emp.ticksFrom));
                        }
                    }
                    
                    if (emp.grossSalary < 0)
                        throw new Exception("Negative salary not supported");
                    
                    if(OldData!=null && OldData.ticksFrom==emp.ticksFrom)
                    {
                        if (isEmployeeUsedInPayroll(emp.id, emp.ticksFrom))
                            throw new ServerUserMessage("The employee profile can't be updated because it is used in a payroll, create and save new version instead");
                        logOldEmployee(AID, emp.id,emp.ticksFrom, logAccounts);
                        emp.ticksTo = OldData.ticksTo;
                        dspWriter.UpdateSingleTableRecord<Employee>(m_DBName, emp, new string[] { "__AID" }, new object[] { AID });
                    }
                    else
                    {
                        if(OldData==null)
                        {
                            object tf=dspWriter.ExecuteScalar(string.Format("Select min(ticksFrom) from {0}.dbo.Employee where id={1}",this.DBName,emp.id));
                            if(tf is long)
                                emp.ticksTo=(long)tf;
                            else
                                throw new ServerUserMessage("BUG: first version not found for employe ID: {0}",emp.id);
                        }
                        else
                        {
                            emp.ticksTo = OldData.ticksTo;
                            OldData.ticksTo=emp.ticksFrom;
                            dspWriter.Update(this.DBName, "Employee", new string[] { "ticksTo" }, new object[] { OldData.ticksTo }, string.Format("id={0} and ticksFrom={1}", OldData.id, OldData.ticksFrom));
                        }
                        dspWriter.InsertSingleTableRecord<Employee>(m_DBName,emp, new string[] { "__AID" }, new object[] { AID });
                    }
                    if (logAccounts)
                    {
                        dspWriter.Delete(m_DBName, "EmployeeAccount", "EmployeeID=" + emp.id);
                        dspWriter.InsertColumn<int>(m_DBName, "EmployeeAccount", new string[] { "EmployeeID", "__AID", "AccountID" }, new object[] { emp.id, AID }
                        , emp.accounts);
                    }
                    
                    createEmpleeCostCenterAccounts(AID, emp);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        
        private void logOldEmployee(int AID, int employeeID,long ticks,bool logAccountsToo)
        {
            if (ticks == -1)
            {
                WriterHelper.logDeletedData(AID, this.DBName + ".dbo.Employee", "id=" + employeeID);
                if (logAccountsToo)
                    WriterHelper.logDeletedData(AID, this.DBName + ".dbo.EmployeeAccount", "employeeID=" + employeeID);
            }
            else
                WriterHelper.logDeletedData(AID, this.DBName + ".dbo.Employee", "id=" + employeeID + " and ticksFrom=" + ticks);
        }
    }
}
