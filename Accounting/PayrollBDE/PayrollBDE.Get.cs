using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        int[] getEmployeeAccounts(int employeeID)
        {
            INTAPS.RDBMS.SQLHelper helper = base.GetReaderHelper();
            try
            {
                return helper.GetColumnArray<int>("Select AccountID from " + this.DBName + ".dbo.EmployeeAccount where EmployeeID=" + employeeID, 0);
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
        }
        Employee[] GetEmployeeByFilter(INTAPS.RDBMS.SQLHelper dsp, string Filter)
        {
            Employee[] ret = dsp.GetSTRArrayByFilter<Employee>(Filter);
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i].accounts = dsp.GetColumnArray<int>("Select AccountID from " + this.DBName + ".dbo.EmployeeAccount where EmployeeID=" + ret[i].id, 0);
            }
            return ret;
        }
        OrgUnit[] GetOrgUnitByFilter(INTAPS.RDBMS.SQLHelper dsp, string Filter)
        {
            DataTable t = dsp.GetDataTable("Select ID,PID,Name,Description from " + this.DBName + ".dbo.OrgUnit where " + Filter + " order by Name");
            OrgUnit[] ret = new OrgUnit[t.Rows.Count];
            int i = 0;
            foreach (DataRow row in t.Rows)
            {
                ret[i] = new OrgUnit();
                ret[i].id = (int)row[0];
                ret[i].PID = (int)row[1];
                ret[i].Name = (string)row[2];
                ret[i].Description = row[3] is string ? (string)row[3] : null;
                i++;
            }
            return ret;
        }
        public Employee GetEmployeeByID(string EmpID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                Employee[] es = GetEmployeeByFilter(dspReader, "EmployeeID='" + EmpID + "' and status=" + ((int)EmployeeStatus.Enrolled));
                if (es.Length == 0)
                    throw new Exception("Employee not in database EmployeeID:" + EmpID);
                if (es.Length > 1)
                    throw new Exception("Database inconsistency more than one employees with EmployeeID:" + EmpID);
                return es[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public Employee GetEmployee(int ID)
        {
            return this.GetEmployee(ID, DateTime.Now.Ticks);
        }
        public Employee GetEmployeeLastVersion(int ID)
        {
            return this.GetEmployee(ID, new DateTime(2099,1,1).Ticks);
        }
        // BDE exposed
        public Employee GetEmployee(int ID, long ticks)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                Employee[] es = GetEmployeeByFilter(dspReader, INTAPS.RDBMS.SQLHelper.getVersionDataFilter(ticks, "ID=" + ID));
                if (es.Length == 0)
                    return null;
                return es[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public Employee GetEmployeeByLoginName(string loginName)
        {
            return GetEmployeeByLoginName(loginName, DateTime.Now.Ticks);
        }

        public Employee GetEmployeeByLoginName(string loginName,long ticks)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                if (string.IsNullOrEmpty(loginName))
                    return null;
                Employee[] es = GetEmployeeByFilter(dspReader, INTAPS.RDBMS.SQLHelper.getVersionDataFilter(ticks, "loginName='" + loginName + "'"));
                if (es.Length == 0)
                    return null;
                return es[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void GetEmployessRecursive(INTAPS.RDBMS.SQLHelper dsp, int OrgID, List<Employee> data, long ticks)
        {
            Employee[] es = GetEmployeeByFilter(dsp, INTAPS.RDBMS.SQLHelper.getVersionDataFilter(ticks, "OrgUnitID=" + OrgID));
            data.AddRange(es);
            foreach (OrgUnit o in GetOrgUnitByFilter(dsp, "PID=" + OrgID))
                GetEmployessRecursive(dsp, o.id, data, ticks);
        }
        public Employee[] GetEmployees(int OrgID, bool IncludeSubOrg)
        {
            return GetEmployees(OrgID, IncludeSubOrg, DateTime.Now.Ticks);
        }
        public Employee[] GetEmployees(int OrgID, bool IncludeSubOrg, long ticks)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                Employee[] ret;
                if (IncludeSubOrg)
                {
                    List<Employee> _ret = new List<Employee>();
                    GetEmployessRecursive(dspReader, OrgID, _ret, ticks);
                    ret = _ret.ToArray();
                }
                else
                    ret = GetEmployeeByFilter(dspReader, INTAPS.RDBMS.SQLHelper.getVersionDataFilter(ticks, "OrgUnitID=" + OrgID));
                Array.Sort(ret, new EmployeeComparer(EmployeeField.Name));

                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public Employee[] GetEmployeesByRoll(int OrgID, EmployeeRoll Role)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                Employee[] es = GetEmployeeByFilter(dspReader, "OrgUnitID=" + OrgID + " and Roll=" + (int)Role);
                return es;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public OrgUnit GetOrgUnit(int ID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                OrgUnit[] os = GetOrgUnitByFilter(dspReader, "ID=" + ID);
                if (os.Length == 0)
                    throw new Exception("Oranization unit not found in the datbase ID:" + ID);
                return os[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public OrgUnit[] GetOrgUnits(int PID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                OrgUnit[] os = GetOrgUnitByFilter(dspReader, "PID=" + PID);
                return os;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public Employee[] GetEmployees(AppliesToEmployees at)
        {
            return GetEmployees(at, DateTime.Now.Ticks);
        }
        public Employee[] GetEmployees(AppliesToEmployees at,long ticks)
        {
            List<Employee> _ret = new List<Employee>();
            if (at.All)
                _ret.AddRange(GetEmployees(-1, true,ticks));
            else
            {
                foreach (int oid in at.OrgUnits)
                {
                    Employee[] es = GetEmployees(oid, true,ticks);
                    foreach (Employee e in es)
                    {
                        if (_ret.Contains(e))
                            continue;
                        _ret.Add(e);
                    }

                }
                foreach (int eid in at.Employees)
                {
                    Employee e = GetEmployee(eid,ticks);
                    if (_ret.Contains(e))
                        continue;
                    _ret.Add(e);
                }
            }
            return _ret.ToArray();
        }
        public bool AppliesToEmployee(AppliesToEmployees at, int EmployeeID)
        {

            if (at.All)
                return true;
            if (at.Employees != null)
                foreach (int ei in at.Employees)
                    if (ei == EmployeeID)
                        return true;
            List<int> pars = new List<int>();
            int ParOrg = GetEmployee(EmployeeID).orgUnitID;
            while (ParOrg != -1)
            {
                pars.Add(ParOrg);
                ParOrg = GetOrgUnit(ParOrg).PID;
            }
            if (at.OrgUnits != null)
                foreach (int oid in at.OrgUnits)
                    if (pars.Contains(oid))
                        return true;
            return false;
        }
        public double GetPension(int employeeID, out double employerContributionAmount)
        {
            Employee employee = GetEmployee(employeeID);
            employerContributionAmount = SysPars.EmployerPensionRate * employee.grossSalary;
            return SysPars.EmployeePensionRate * employee.grossSalary;
        }
        // BDE exposed
        public long[] getEmployeVersions(int employeeID)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetColumnArray<long>(string.Format("Select ticksFrom from {0}.dbo.Employee where id={1} order by ticksFrom", this.DBName, employeeID));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        bool isEmployeeUsedInPayroll(INTAPS.RDBMS.SQLHelper reader, int employeeID, long ver)
        {
            long ticksTo = (long)reader.ExecuteScalar(string.Format("Select ticksTo from {0}.dbo.Employee where id={1} and ticksFrom={2}", this.DBName, employeeID, ver));
            string sql;
            if (ticksTo == -1)
                sql = string.Format(@"SELECT     COUNT(*) FROM      {0}.dbo.Payroll INNER JOIN
                      {0}.dbo.PayPeriod ON Payroll.periodID = PayPeriod.ID
                WHERE     (Payroll.employeeID = {1}) and {2}.dbo.dateToTicks(ToDate)>{3}", this.DBName, employeeID, m_Accounting.DBName, ver);
            else
                sql = string.Format(@"SELECT     COUNT(*) FROM      {0}.dbo.Payroll INNER JOIN
                      {0}.dbo.PayPeriod ON Payroll.periodID = PayPeriod.ID
                WHERE     (Payroll.employeeID = {1}) and {2}.dbo.dateToTicks(ToDate)>{3} and {2}.dbo.dateToTicks(toDate)<={4}", this.DBName, employeeID, m_Accounting.DBName, ver, ticksTo);

            int count = (int)reader.ExecuteScalar(sql);
            return count > 0;
        }
        // BDE exposed
        public bool isEmployeeUsedInPayroll(int employeeID, long ver)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return isEmployeeUsedInPayroll(reader, employeeID, ver);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public Employee[] getEmployessByCostCenter(int costCenterID,long ticks,bool onlyEnrolled)
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                if(onlyEnrolled)
                    return GetEmployeeByFilter(reader, INTAPS.RDBMS.SQLHelper.getVersionDataFilter(ticks, string.Format("status={0} and  costCenterID={1}",(int)EmployeeStatus.Enrolled,costCenterID)));
                return GetEmployeeByFilter(reader, INTAPS.RDBMS.SQLHelper.getVersionDataFilter(ticks, string.Format("costCenterID={0}", costCenterID)));
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
        public double getEmployeeSalaryPerHours(Employee e)
        {
            if (e.salaryKind == SalaryKind.Daily)
                return e.grossSalary / m_sysPars.EmployeeDailyFullWorkingHours;
            return e.grossSalary / m_sysPars.EmployeeMonthlyFullWorkingHours;
        }
    }
}