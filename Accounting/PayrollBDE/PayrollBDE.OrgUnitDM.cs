using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        public int RegisterOrgUnit(int AID,OrgUnit ou)
        {
            lock (dspWriter)
            {
                

                
                if (string.IsNullOrEmpty(ou.Name))
                    throw new Exception("Organizational unit must be named.");
                dspWriter.BeginTransaction();
                dspWriter.setReadDB(this.DBName);
                try
                {
                    ou.id = INTAPS.ClientServer.AutoIncrement.GetKey("Payroll.OrgUnitID");
                    if (ou.PID != -1 && GetOrgUnit(ou.PID) == null)
                        throw new ClientServer.ServerUserMessage("Invalid parent ID");
                    dspWriter.Insert(m_DBName, "OrgUnit"
                    , new string[] { "ID", "PID", "Name", "Description" }
                , new object[] { ou.id, ou.PID, ou.Name, ou.Description });
                    dspWriter.CommitTransaction();
                    return ou.id;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Failed to register ougunit:" + ou, ex);
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
                
            }
        }
        public void DeleteOrgUnit(int ID)
        {
            lock (dspWriter)
            {                
                if (GetOrgUnits(ID).Length > 0)
                    throw new Exception("Delete child organizational units first.");
                if (GetEmployees(ID,false).Length>0)
                    throw new Exception("Delete employees unders this organizational unit first.");
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.Delete(m_DBName, "OrgUnit", "ID=" + ID);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void Update(int AID,OrgUnit ou)
        {
            lock (dspWriter)
            {
                
                if (string.IsNullOrEmpty(ou.Name))
                    throw new Exception("Organizational unit must be named.");
                int test=ou.PID;
                while (test != -1)
                {
                    if (ou.ObjectIDVal == test)
                        throw new ClientServer.ServerUserMessage("Invalid parent ID");
                    test = GetOrgUnit(test).PID;
                }
                if(ou.PID!=-1 && GetOrgUnit(ou.PID)==null)
                    throw new ClientServer.ServerUserMessage("Invalid parent ID");
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.Update(m_DBName, "OrgUnit"
                    , new string[] { "PID", "Name", "Description" }
                , new object[] { ou.PID, ou.Name, ou.Description }, "ID=" + ou.id);
                }
                catch (Exception ex)
                {
                    INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Failed to register ougunit:" + ou, ex);
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
    }
}
