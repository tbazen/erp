using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        public PayrollComponentFormulaSpec GetPCDefaultFormula(int PCDID)
        {
            return dataHandlers[PCDID].GetFormulaSpecification();
        }
        void setAppliesTo(int AID, string Table, string IDField, int IDValue, AppliesToEmployees at,bool tableAIDField)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();
                    logOldAppliesTo(AID, Table, IDField, IDValue);
                    dspWriter.Delete(m_DBName, Table, IDField + "=" + IDValue);
                    string []fields = tableAIDField ? new string[] { IDField, "ObjectType", "ObjectID", "__AID" } : new string[] { IDField, "ObjectType", "ObjectID" };
                    if (at.All)
                    {
                        dspWriter.Insert(m_DBName, Table, fields
                            , tableAIDField?new object[] { IDValue, (int)AppliesToObjectType.All, 0,AID }: new object[] { IDValue, (int)AppliesToObjectType.All, 0 });
                    }
                    else
                    {
                        foreach (int OID in at.OrgUnits)
                        {
                            dspWriter.Insert(m_DBName, Table, fields
                            , tableAIDField ? new object[] { IDValue, (int)AppliesToObjectType.OrgUnit, OID,AID } : new object[] { IDValue, (int)AppliesToObjectType.OrgUnit, OID });
                        }
                        foreach (int EID in at.Employees)
                        {
                            dspWriter.Insert(m_DBName, Table, fields
                            , tableAIDField ? new object[] { IDValue, (int)AppliesToObjectType.Employee, EID, AID } : new object[] { IDValue, (int)AppliesToObjectType.Employee, EID });
                        }
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        private void logOldAppliesTo(int AID, string Table, string IDField, int IDValue)
        {
            WriterHelper.logDeletedData(AID, this.DBName + ".dbo." + Table, IDField + "=" + IDValue);
        }
        void setTranFormula(int AID, int FormulaID, string[] AccountFormula, string[] AmountFormula)
        {
            lock (dspWriter)
            {
                try
                {
                    dspWriter.BeginTransaction();

                    logOldTranFormula(AID, FormulaID);

                    dspWriter.Delete(m_DBName, "PCDTranFormula", "FormulaID=" + FormulaID);
                    for (int i = 0; i < AccountFormula.Length; i++)
                    {
                        dspWriter.Insert(m_DBName, "PCDTranFormula", new string[] { "FormulaID", "OrderN", "AccountFormula", "TransactionFormula" }
                    , new object[] { FormulaID, (i + 1), AccountFormula[i], AmountFormula[i] });
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        private void logOldTranFormula(int AID, int FormulaID)
        {
            WriterHelper.logDeletedData(AID, this.DBName + ".dbo.PCDTranFormula", "FormulaID=" + FormulaID);
        }

        
        public int CreatePCNewFormula(int AID,PayrollComponentFormula formula)
        {
            lock (dspWriter)
            {
                IPayrollComponentDataHandler h = dataHandlers[formula.PCDID];
                PayrollComponentFormulaSpec spec = h.GetFormulaSpecification();
                if (spec.VarsReadonly)
                    formula.Vars = spec.Vars;
                if (spec.AccountFormulaReadOnly)
                    formula.AccountFormula = spec.AccountFormula;
                if (spec.AmountFormulaReadonly)
                    formula.AmountFormula = spec.AmountFormula;
                if (!h.CanFormulaApplyTo(formula.at))
                    throw new Exception("Invalid formula applies to domain.");
                
                dspWriter.setReadDB(this.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    int ID = INTAPS.ClientServer.AutoIncrement.GetKey("Payroll.FormulaID");
                    dspWriter.Insert(m_DBName, "PCDFormula", new string[]
                    {"ID"
                        ,"Name"
                        ,"Description"
                        ,"PCDID"
                        ,"FormulaDate"
                        ,"PeriodFrom"
                        ,"PeriodTo"
                    ,"Deduct"
                    ,"__AID"
                    }
                , new object[]
                {ID
                    ,formula.Name
                    ,formula.Description
                    ,formula.PCDID
                    ,formula.FormulaDate
                    ,formula.periodRange.PeriodFrom==-1?DBNull.Value:(object)formula.periodRange.PeriodFrom
                    ,formula.periodRange.PeriodTo==-1?DBNull.Value:(object)formula.periodRange.PeriodTo
                ,formula.Deduct
                ,AID
                });
                    setAppliesTo(AID,"FormulaAppliesTo", "FormulaID", ID, formula.at,false);
                    int i;
                    for (i = 0; i < formula.Vars.Length; i++)
                    {
                        dspWriter.Insert(m_DBName, "PCDFormulaVariable", new string[] { "FormulaID", "OrderN", "VarName", "VarFormula" }
                    , new object[] { ID, (i + 1), formula.Vars[i], formula.Vals[i] });
                    }
                    setTranFormula(AID, ID, formula.AccountFormula, formula.AmountFormula);
                    dspWriter.CommitTransaction();
                    return ID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }

            }
        }
        public void UpdatePCFormula(int AID,PayrollComponentFormula formula)
        {
            lock (dspWriter)
            {
                
                IPayrollComponentDataHandler h = dataHandlers[formula.PCDID];
                PayrollComponentFormulaSpec spec = h.GetFormulaSpecification();
                if (spec.VarsReadonly)
                    formula.Vars = spec.Vars;
                if (spec.AccountFormulaReadOnly)
                    formula.AccountFormula = spec.AccountFormula;
                if (spec.AmountFormulaReadonly)
                    formula.AmountFormula = spec.AmountFormula;
                if (!h.CanFormulaApplyTo(formula.at))
                    throw new Exception("Invalid formula applies to domain.");
                dspWriter.BeginTransaction();
                dspWriter.setReadDB(this.DBName);
                try
                {
                    PayrollComponentFormula oldF = GetPCFormula(formula.ID);
                    logOldFormula(AID, oldF);

                    dspWriter.Update(m_DBName, "PCDFormula", new string[]
                    {"Name"
                        ,"Description"
                        ,"PCDID"
                        ,"FormulaDate"
                        ,"PeriodFrom"
                        ,"PeriodTo"
                    ,"Deduct"
                    ,"__AID"
                    }
                , new object[]
                {formula.Name
                    ,formula.Description
                    ,formula.PCDID
                    ,formula.FormulaDate
                    ,formula.periodRange.PeriodFrom==-1?DBNull.Value:(object)formula.periodRange.PeriodFrom
                    ,formula.periodRange.PeriodTo==-1?DBNull.Value:(object)formula.periodRange.PeriodTo
                ,formula.Deduct
                ,AID
                }, "ID=" + formula.ID);
                    setAppliesTo(AID,"FormulaAppliesTo", "FormulaID", formula.ID, formula.at,false);
                    int i;

                    logOldFormulaVariable(AID, formula.ID);

                    dspWriter.Delete(m_DBName, "PCDFormulaVariable", "FormulaID=" + formula.ID);
                    for (i = 0; i < formula.Vars.Length; i++)
                    {
                        dspWriter.Insert(m_DBName, "PCDFormulaVariable", new string[] { "FormulaID", "OrderN", "VarName", "VarFormula" }
                    , new object[] { formula.ID, (i + 1), formula.Vars[i], formula.Vals[i] });
                    }
                    setTranFormula(AID,formula.ID, formula.AccountFormula, formula.AmountFormula);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
 
        }

        private void logOldFormula(int AID, PayrollComponentFormula oldF)
        {
            WriterHelper.logDeletedData(AID, this.DBName + ".dbo.PCDFormula", "id=" + oldF.ID);
        }

        private void logOldFormulaVariable(int AID, int formulaID)
        {
            WriterHelper.logDeletedData(AID, this.DBName + ".dbo.PCDFormulaVariable", "FormulaID=" + formulaID);
        }
        public void DeletePCForumla(int AID,int FormulaID)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB (this.DBName);
                try
                {
                    if ((int)dspWriter.ExecuteScalar("Select count(*) from " + this.DBName + ".dbo.PCDData where FormulaID=" + FormulaID) > 0)
                        throw new Exception("You can't delte a formula that is already attached with data.");
                    dspWriter.BeginTransaction();
                    try
                    {
                        PayrollComponentFormula oldF = GetPCFormula(FormulaID);
                        logOldFormula(AID, oldF);
                        logOldTranFormula(AID, oldF.ID);
                        logOldFormulaVariable(AID, oldF.ID);
                        logOldAppliesTo(AID, "FormulaAppliesTo", "FormulaID", FormulaID);
                        dspWriter.Delete(m_DBName, "PCDTranFormula", "FormulaID=" + FormulaID);
                        dspWriter.Delete(m_DBName, "PCDFormulaVariable", "FormulaID=" + FormulaID);
                        dspWriter.Delete(m_DBName, "FormulaAppliesTo", "FormulaID=" + FormulaID);
                        dspWriter.Delete(m_DBName, "PCDFormula", "ID=" + FormulaID);
                        dspWriter.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        dspWriter.RollBackTransaction();
                        throw ex;
                    }
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }        
        public int CreatePCData(int AID,PayrollComponentData data, object additionaldata)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    if (data.FormulaID == -1)
                    {
                        PayrollComponentDefination def = m_pcDefs[data.PCDID];
                        if (!ValidatePeriod(dspWriter, def.periodRange.PeriodFrom, def.periodRange.PeriodTo, data.periodRange.PeriodFrom, data.periodRange.PeriodTo))
                            throw new Exception("Invalid payment pariod.");
                    }
                    else
                    {
                        PayrollComponentFormula f = GetPCFormulaByFilter(dspWriter, "ID=" + data.FormulaID)[0];
                        if (!ValidatePeriod(dspWriter, f.periodRange.PeriodFrom, f.periodRange.PeriodTo, data.periodRange.PeriodFrom, data.periodRange.PeriodTo))
                            throw new Exception("Invalid payment pariod.");
                    }
                    if (data.ID > 0)
                    {
                        DeleteData(AID, data.ID);
                    }
                    else
                    {
                        int ID = INTAPS.ClientServer.AutoIncrement.GetKey("Payroll.DataID");
                        data.ID = ID;
                    }
                    dataHandlers[data.PCDID].CreateData(AID,data, additionaldata);


                    dspWriter.Insert(m_DBName, "PCDData"
                    , new string[] { "ID", "PCDID", "FormulaID", "DataDate", "PeriodFrom", "PeriodTo","__AID" }
                    , new object[]{data.ID,data.PCDID
                    ,data.FormulaID==-1?DBNull.Value:(object)data.FormulaID
                    ,data.DataDate
                    ,data.periodRange.PeriodFrom==-1?DBNull.Value:(object)data.periodRange.PeriodFrom
                    ,data.periodRange.PeriodTo==-1?DBNull.Value:(object)data.periodRange.PeriodTo
                    ,AID
                    });
                    setAppliesTo(AID,"DataAppliesTo", "DataID", data.ID, data.at,false);
                    dspWriter.CommitTransaction();
                    return data.ID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }        
        public void UpdatePCData(int AID,PayrollComponentData data, object additionaldata)
        {
            lock (dspWriter)
            {
                dataHandlers[data.PCDID].CreateData(AID,data, additionaldata);
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.Update(m_DBName, "PCDData"
                    , new string[] { "DataDate", "PeriodFrom", "PeriodTo","__AID" }
                    , new object[]{data.DataDate
                    ,data.periodRange.PeriodFrom==-1?DBNull.Value:(object)data.periodRange.PeriodFrom
                    ,data.periodRange.PeriodTo==-1?DBNull.Value:(object)data.periodRange.PeriodTo
                    ,AID
                    }
                        , "ID=" + data.ID);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void UpdatePCData(int AID, PayrollComponentData data)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.Update(m_DBName, "PCDData"
                    , new string[] { "DataDate", "PeriodFrom", "PeriodTo","__AID"}
                    , new object[]{data.DataDate
                    ,data.periodRange.PeriodFrom==-1?DBNull.Value:(object)data.periodRange.PeriodFrom
                    ,data.periodRange.PeriodTo==-1?DBNull.Value:(object)data.periodRange.PeriodTo
                    ,AID}
                        , "ID=" + data.ID
                        );
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void DeleteData(int AID, int DataID)
        {
            PayrollComponentData pd;
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    PayrollComponentData[] pds = GetPCDataByFilter(dspWriter, "ID=" + DataID);
                    if (pds.Length != 0)
                    {
                        pd = pds[0];
                        dataHandlers[pd.PCDID].DeleteData(AID, DataID);
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.DataAppliesTo", "DataID=" + DataID);
                        dspWriter.Delete(m_DBName, "DataAppliesTo", "DataID=" + DataID);
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.PCDData", "ID=" + DataID);
                        dspWriter.Delete(m_DBName, "PCDData", "ID=" + DataID);
                    }
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception(ex.Message,ex);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void SetPCDDomain(int AID,int PCDID, PayPeriodRange prange, AppliesToEmployees at)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                dspWriter.setReadDB (this.DBName);
                try
                {
                    PayrollComponentDefination def = m_pcDefs[PCDID];
                    dspWriter.Update(m_DBName, "PayrollComponentDef", new string[]
                    {"PeriodFrom"
                        ,"PeriodTo"}
                    , new object[]
                {prange.PeriodFrom==-1?DBNull.Value:(object)prange.PeriodFrom
                    ,prange.PeriodTo==-1?DBNull.Value:(object)prange.PeriodTo}, "ID=" + PCDID);
                    setAppliesTo(AID,"PCDAppliesTo", "PCDID", PCDID, at,true);
                    def.at = at;
                    def.periodRange = prange;
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
    }
}
