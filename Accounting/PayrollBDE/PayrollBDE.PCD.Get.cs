using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        PayrollComponentData[] GetPCDataByFilter(INTAPS.RDBMS.SQLHelper dsp, string filter)
        {
            DataTable dat = dsp.GetDataTable("Select ID,PCDID,FormulaID,DataDate,PeriodFrom,PeriodTo from " + this.DBName + ".dbo.PCDData where " + filter + " order by FormulaID,PeriodFrom");
            PayrollComponentData[] ret = new PayrollComponentData[dat.Rows.Count];
            int i = 0;
            foreach (DataRow row in dat.Rows)
            {
                ret[i] = new PayrollComponentData();
                ret[i].ID = (int)row[0];
                ret[i].PCDID = (int)row[1];
                ret[i].FormulaID = row[2] is int?(int)row[2]:-1;
                ret[i].DataDate = (DateTime)row[3];
                ret[i].periodRange.PeriodFrom =row[4] is int? (int)row[4]:-1;
                ret[i].periodRange.PeriodTo = row[5] is int?(int)row[5]:-1;
                ret[i].at = RetrieveAt(dsp, "DataAppliesTo", "DataID", ret[i].ID);
                i++;
            }
            return ret;
        }
        PayrollComponentFormula[] GetPCFormulaByFilter(INTAPS.RDBMS.SQLHelper dsp, string Filter)
        {
            DataTable dat = dsp.GetDataTable(
                    "Select ID,PCDID,Name,Description,FormulaDate,PeriodFrom,PeriodTo,Deduct from " + base.DBName + ".dbo.PCDFormula where " + Filter + " order by Name");
            PayrollComponentFormula[] ret = new PayrollComponentFormula[dat.Rows.Count];
            int i = 0;
            foreach (DataRow row in dat.Rows)
            {
                ret[i] = new PayrollComponentFormula();
                ret[i].ID = (int)row[0];
                ret[i].PCDID = (int)row[1];
                ret[i].Name = (string)row[2];
                ret[i].Description = row[3] as string;
                ret[i].FormulaDate = (DateTime)row[4];
                ret[i].periodRange.PeriodFrom = row[5] is int ? (int)row[5] : -1;
                ret[i].periodRange.PeriodTo = row[6] is int ? (int)row[6] : -1;
                ret[i].at = RetrieveAt(dsp, "FormulaAppliesTo", "FormulaID", ret[i].ID);
                ret[i].Deduct = (bool)row[7];
                RetrieveFromula(dsp, "PCDTranFormula", ret[i].ID, out ret[i].AccountFormula, out ret[i].AmountFormula);
                DataTable fdata = dsp.GetDataTable("Select VarName,VarFormula from " + base.DBName + ".dbo.PCDFormulaVariable where FormulaID=" + ret[i].ID + " order by OrderN");
                int j = 0;
                ret[i].Vars = new string[fdata.Rows.Count];
                ret[i].Vals = new string[fdata.Rows.Count];
                foreach (DataRow frow in fdata.Rows)
                {
                    ret[i].Vars[j] = (string)frow[0];
                    ret[i].Vals[j] = (string)frow[1];
                    j++;
                }
                i++;
            }
            return ret;
        }
        AppliesToEmployees RetrieveAt(INTAPS.RDBMS.SQLHelper dsp, string Table, string IDField, int ID)
        {
            DataTable dat = dsp.GetDataTable(
    "Select ObjectType,ObjectID from "+base.DBName+".dbo."+ Table + " where " + IDField + "=" + ID + " order by ObjectType");
            bool All = false;
            List<AppliesToObjectType> ot = new List<AppliesToObjectType>();
            List<int> OrgUnitID = new List<int>();
            List<int> EmpID = new List<int>();
            foreach (DataRow row in dat.Rows)
            {
                AppliesToObjectType thisos = (AppliesToObjectType)(int)row[0];
                int thisoid = (int)row[1];
                if (thisos == AppliesToObjectType.All)
                {
                    All = true;
                    break;
                }
                else if (thisos == AppliesToObjectType.OrgUnit)
                {
                    OrgUnitID.Add(thisoid);
                }
                else if (thisos == AppliesToObjectType.Employee)
                {
                    EmpID.Add(thisoid);
                }
            }
            AppliesToEmployees ret = new AppliesToEmployees();
            ret.All = All;
            ret.OrgUnits = OrgUnitID.ToArray();
            ret.Employees = EmpID.ToArray();
            return ret;
        }
        void RetrieveFromula(INTAPS.RDBMS.SQLHelper dsp, string Table, int FormulaID, out string[] AccountFormula, out string[] TransactionFormula)
        {
            DataTable dat = dsp.GetDataTable("Select AccountFormula,TransactionFormula from " + base.DBName + ".dbo." + Table + " where FormulaID=" + FormulaID + " order by OrderN");
            AccountFormula = new string[dat.Rows.Count];
            TransactionFormula = new string[dat.Rows.Count];
            int i = 0;
            foreach (DataRow row in dat.Rows)
            {
                AccountFormula[i] = (string)row[0];
                TransactionFormula[i] = (string)row[1];
                i++;
            }
        }        
        public PayrollComponentFormula[] GetPCFormulae(int PCDID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                return GetPCFormulaByFilter(dspReader, "PCDID=" + PCDID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public PayrollComponentDefination[] GetPCDefinations()
        {
            PayrollComponentDefination[] ret = new PayrollComponentDefination[m_pcDefs.Count];
            m_pcDefs.Values.CopyTo(ret, 0);
            return ret;
        }
        public PayrollComponentData[] GetPCData(int EmployeeID)
        {
            List<PayrollComponentData> _ret = new List<PayrollComponentData>();
            Employee e = this.GetEmployee(EmployeeID);
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); 
            try
            {
                _ret.AddRange(GetPCDataByFilter(dspReader,
                    "exists(Select * from " + this.DBName + ".dbo.DataAppliesTo where ObjectType="
                    + (int)AppliesToObjectType.Employee
                    + " and ObjectID=" + EmployeeID + " and DataID=PCDData.ID)"));

                _ret.AddRange(GetPCDataByFilter(dspReader,
                    "exists(Select * from " + this.DBName + ".dbo. DataAppliesTo where ObjectType="
                    + (int)AppliesToObjectType.OrgUnit
                    + " and ObjectID=" + e.orgUnitID + " and DataID=PCDData.ID)"));
                _ret.AddRange(GetPCDataByFilter(dspReader,
                    "exists(Select * from "+ this.DBName + ".dbo. DataAppliesTo where ObjectType="
                    + (int)AppliesToObjectType.All
                    + " and DataID=PCDData.ID)"));
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            return _ret.ToArray();
        }
        public PayrollComponentData[] GetPCData(int PCDID, int FormulaID, AppliesToObjectType objectType, int ObjectID, int Period, bool IncludeHeirarchy)
        {
            List<PayrollComponentData> _ret = new List<PayrollComponentData>();
            string AppliesToFilter;
            if (IncludeHeirarchy)
            {
                if (objectType == AppliesToObjectType.Employee)
                {
                    Employee e = GetEmployee(ObjectID);
                    AppliesToFilter = "(DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.All + ")";
                    int OID = e.orgUnitID;
                    while (OID != -1)
                    {
                        AppliesToFilter += " or (DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.OrgUnit + " and ObjectID=" + OID + ")";
                        OID = GetOrgUnit(OID).PID;
                    }
                    AppliesToFilter += " or (DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.Employee + " and ObjectID=" + e.id + ")";
                }
                else if (objectType == AppliesToObjectType.OrgUnit)
                {
                    AppliesToFilter = "(DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.All + ")";
                    int OID = ObjectID;
                    while (OID != -1)
                    {
                        AppliesToFilter += " or (DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.OrgUnit + " and ObjectID=" + OID + ")";
                        OID = GetOrgUnit(OID).PID;
                    }
                }
                else
                {
                    AppliesToFilter = "(DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.All + ")";
                }
            }
            else
            {
                if (objectType == AppliesToObjectType.All)
                    AppliesToFilter = "(DataAppliesTo.ObjectType=" + (int)AppliesToObjectType.All + ")";
                else
                {
                    if(ObjectID==-1)
                        AppliesToFilter = "(DataAppliesTo.ObjectType=" + (int)objectType+")";
                    else
                        AppliesToFilter = "(DataAppliesTo.ObjectType=" + (int)objectType + " and ObjectID=" + ObjectID + ")";
                }
            }
            string PCDFilter=null;
            if (FormulaID != -1)
                PCDFilter= "FormulaID=" + FormulaID;
            else if(PCDID !=-1)
                PCDFilter = "PCDID=" + PCDID;
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); 
            try
            {
                string sql = @"SELECT     PCDData.ID, PCDData.PCDID, PCDData.FormulaID, PCDData.DataDate, PCDData.PeriodFrom, PCDData.PeriodTo
                            FROM  {0}.dbo.PCDData INNER JOIN
                                    {0}.dbo.DataAppliesTo ON {0}.dbo.PCDData.ID = {0}.dbo.DataAppliesTo.DataID
                            WHERE  (" + AppliesToFilter + ")" + (PCDFilter != null ? " and (" + PCDFilter + ")" : "");
                sql = string.Format(sql, this.DBName);
                PayrollPeriod pp = GetPayPeriod(Period);
                DataTable data = dspReader.GetDataTable(sql);
                try
                {
                    foreach(DataRow reader in data.Rows)
                    {
                        PayrollComponentData pd = new PayrollComponentData();
                        pd.ID = (int)reader[0];
                        pd.PCDID = (int)reader[1];
                        pd.FormulaID = reader[2] is int ? (int)reader[2] : -1;
                        pd.DataDate = (DateTime)reader[3];
                        pd.periodRange.PeriodFrom = reader[4] is int ? (int)reader[4] : -1;
                        pd.periodRange.PeriodTo = reader[5] is int ? (int)reader[5] : -1;
                        pd.at = RetrieveAt(dspReader, "DataAppliesTo", "DataID", pd.ID);
                        if (pd.periodRange.AllPeriod)
                        {
                            _ret.Add(pd);
                            continue;
                        }
                        if (pd.periodRange.SinglePeriod)
                        {
                            if (Period==-1 || pd.periodRange.PeriodFrom == Period)
                                _ret.Add(pd);
                            continue;
                        }
                        PayrollPeriod start = this.GetPayPeriod(pd.periodRange.PeriodFrom);
                        if (pp!=null && pd.periodRange.FromNowOn)
                        {
                            if (start.fromDate <= pp.fromDate )
                                _ret.Add(pd);
                            continue;
                        }
                        PayrollPeriod end = this.GetPayPeriod(pd.periodRange.PeriodTo);
                        if (pp==null || start.fromDate <= pp.fromDate && end.toDate >= pp.toDate)
                        {
                            _ret.Add(pd);
                        }
                    }
                    return _ret.ToArray();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public PayrollComponentData GetPCData(int dataID, out object additionalData)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                PayrollComponentData[] _ret = GetPCDataByFilter(dspReader, "id=" + dataID);
                if (_ret.Length == 0)
                {
                    additionalData = null;
                    return null;
                }
                additionalData = GetPCAdditionalData(dataID);
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public object GetPCAdditionalData(int DataID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); 
            try
            {
                PayrollComponentData[] dat = GetPCDataByFilter(dspReader, "ID=" + DataID);
                if (dat.Length == 0)
                    throw new Exception("Payroll component data not found in database.");
                if (!dataHandlers.ContainsKey(dat[0].PCDID))
                    throw new Exception("Couldn't find the related payroll component handler.");
                IPayrollComponentDataHandler h = dataHandlers[dat[0].PCDID];
                return h.GetData(DataID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public PayrollComponentFormula GetPCFormula(int FormulaID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                PayrollComponentFormula[] _ret = GetPCFormulaByFilter(dspReader, "ID=" + FormulaID);
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public PayrollComponentDefination GetDefByType(Type t)
        {
            foreach (KeyValuePair<int,PayrollComponentDefination> kv in m_pcDefs)
            {
                if (Type.GetType(kv.Value.DataHandlerCass) == t)
                    return kv.Value;
            }
            return null;
        }

    }
}
