using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {

        public const int PCD_STAFF_LOAN = 4;

        Dictionary<int,PayrollComponentDefination> _m_pcDefs=null;
        Dictionary<int, IPayrollComponentDataHandler> _dataHandlers=null;
        Dictionary<int, PayrollComponentDefination> m_pcDefs
        {
            get
            {
                if(_m_pcDefs==null)
                    InitPayrollComponent();
                return _m_pcDefs;
            }
        }
        Dictionary<int, IPayrollComponentDataHandler> dataHandlers
        {
            get
            {
                if (_dataHandlers == null)
                    InitPayrollComponent();
                lock (_dataHandlers)
                {
                    return _dataHandlers;
                }
            }
        }


        void InitPayrollComponent()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.Log("Loading payroll components..");
                _m_pcDefs = new Dictionary<int, PayrollComponentDefination>();
                PayrollBDE bdePayroll = (PayrollBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("Payroll");
                _dataHandlers = new Dictionary<int, IPayrollComponentDataHandler>();
                lock (_dataHandlers)
                {
                    try
                    {
                        DataTable dat = dspReader.GetDataTable(string.Format("Select ID,Name,Description,DataHandlerClass,UIHandlerClass,SupportFormula,PeriodFrom,PeriodTo,Deduct from {0}.dbo.PayrollComponentDef order by Name", bdePayroll.DBName));
                        Console.WriteLine("{0} definations found ", dat.Rows.Count);
                        foreach (DataRow row in dat.Rows)
                        {
                            PayrollComponentDefination pcdef = new PayrollComponentDefination();
                            pcdef.ID = (int)row[0];
                            pcdef.Name = (string)row[1];
                            pcdef.Description = row[2] as string;
                            pcdef.DataHandlerCass = (string)row[3];
                            pcdef.UIHandlerClass = (string)row[4];
                            pcdef.SupportFormula = (bool)row[5];
                            pcdef.periodRange.PeriodFrom = row[6] is int ? (int)row[6] : -1;
                            pcdef.periodRange.PeriodTo = row[7] is int ? (int)row[7] : -1;
                            pcdef.at = RetrieveAt(dspReader, "PCDAppliesTo", "PCDID", pcdef.ID);
                            pcdef.Deduct = (bool)row[8];
                            try
                            {
                                _dataHandlers.Add(pcdef.ID, (IPayrollComponentDataHandler)pcdef.GetDataHandlerInstance(typeof(PayrollBDE), this));
                                _m_pcDefs.Add(pcdef.ID, pcdef);
                            }
                            catch (Exception ex)
                            {
                                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Failed to load PDC data handler type:" + pcdef.DataHandlerCass, ex);
                            }
                            finally
                            {
                                base.ReleaseHelper(dspReader);
                            }
                        }
                        INTAPS.ClientServer.ApplicationServer.EventLoger.Log("Done..Loading payroll components");
                    }
                    catch (Exception ex)
                    {
                        INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Failed..Loading payroll components", ex);
                    }
                    finally
                    {
                        base.ReleaseHelper(dspReader);
                    }
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

    }
}
