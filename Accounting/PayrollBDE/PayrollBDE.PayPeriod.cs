using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        static PayrollPeriod GetEthiopianMonthPayPeriod(int EthiopianYear, int Month)
        {
            PayrollPeriod ret= new PayrollPeriod();
            ret.fromDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, Month, EthiopianYear)).GridDate;
            if(Month<12)
                ret.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, Month + 1, EthiopianYear)).GridDate;
            else
                ret.toDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, 1, EthiopianYear + 1)).GridDate;
            ret.name = EthiopianYear.ToString() + " " + INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(Month);
            return ret;
        }
        static PayrollPeriod GetGregorianMotnPayPeriod(int Year, int Month)
        {
            PayrollPeriod ret = new PayrollPeriod();
            ret.fromDate = new DateTime(Year, Month, 1);
            if (Month < 12)
                ret.toDate = new DateTime(Year, Month + 1, 1);
            else
                ret.toDate = new DateTime(Year+1, 1, 1);
            ret.name = INTAPS.Ethiopic.EtGrDate.GetGrigMonthName(Month) + " " + Year.ToString();
            return ret;
        }
        
        bool ValidatePeriod(INTAPS.RDBMS.SQLHelper dspReader, int ParentFrom, int ParentTo, int ThisPeriodFrom, int ThisPeriodTo)
        {
            return INTAPS.Accounting.BDE.AccountingPeriodHelper.ValidatePeriod<PayrollPeriod>(dspReader,"PayPeriod", ParentFrom, ParentTo, ThisPeriodFrom, ThisPeriodTo);
        }
        PayrollPeriod[] GetPayPeriodByFilter(INTAPS.RDBMS.SQLHelper dsp, string Filter)
        {
            return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPayPeriodByFilter<PayrollPeriod>(dsp, "PayPeriod", Filter);
        }
        PayrollPeriod GetPayPeriodInternal(INTAPS.RDBMS.SQLHelper dsp, int PID)
        {
            return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPayPeriod<PayrollPeriod>(dsp, "PayPeriod", PID);
        }
        public void GeneratePayrollPeriods(int year, bool ethiopian)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();

                    PayrollPeriod[] ret = new PayrollPeriod[12];
                    for (int m = 1; m <= 12; m++)
                    {
                        PayrollPeriod epp;
                        if (ethiopian)
                            epp = PayrollBDE.GetEthiopianMonthPayPeriod(year, m);
                        else
                            epp = PayrollBDE.GetGregorianMotnPayPeriod(year, m);
                        PayrollPeriod[] ps = GetPayPeriodByFilter(dspWriter, " FromDate='" + epp.fromDate + "' and ToDate='" + epp.toDate + "'");
                        if (ps.Length == 0)
                            epp.id = CreatePayPeriod(epp);
                        else
                            epp = ps[0];
                        ret[m - 1] = epp;
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public PayrollPeriod[] GetPayPeriods(int Year, bool Ethiopian)
        {

            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    PayrollPeriod[] ret = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPayPeriods<PayrollPeriod>(this.DBName, dspWriter, dspWriter, "PayPeriod", Year, Ethiopian);
                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public PayrollPeriod GetPayPeriod(int PID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                PayrollPeriod[] _ret = GetPayPeriodByFilter(dspReader, "ID=" + PID);
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public PayrollPeriod getNextPeriod(int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetNextPeriod<PayrollPeriod>(dspReader, "PayPeriod", GetPayPeriod(periodID));
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public PayrollPeriod GetPayPeriod(DateTime date)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                return INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPeriodForDate<PayrollPeriod>(dspReader, "PayPeriod", date);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
            
        }
        public bool WithinValidPariod(int PeriodFrom, int PeriodTo, int Period)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper(); try
            {
                return ValidatePeriod(dspReader, PeriodFrom, PeriodTo, Period, Period);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
    }
}
