using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using System.Threading;
using INTAPS.Evaluator;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        public int ProgressStage;
        public int Progress;
        public int ProgressMax=-1;
        public bool StopProgress;
        public string SystemBusyMessage=null;
        
        public List<PayrollDocument> GeneratePayrollPreview(DateTime date, AppliesToEmployees at, int periodID, out BatchError error)
        {
            List<int> FailedEmpID = new List<int>();
            List<string> FailedDescription = new List<string>();
            List<Exception> SystemException = new List<Exception>();

            Dictionary<int, PayrollComponentFormula[]> formulae = new Dictionary<int, PayrollComponentFormula[]>();
            foreach (PayrollComponentDefination pcdef in m_pcDefs.Values)
            {
                if (pcdef.SupportFormula)
                {
                    formulae.Add(pcdef.ID, this.GetPCFormulae(pcdef.ID));
                }
            }
            Employee[] es = this.GetEmployees(at,date.Ticks);
            Array.Sort(es, new EmployeeComparer(EmployeeField.Name));
            List<PayrollDocument> ret = new List<PayrollDocument>();
            PayrollPeriod p = GetPayPeriod(periodID);
            foreach (Employee e in es)
            {
                PayrollGenerator gen = new PayrollGenerator(formulae, this, date, e,p);
                PayrollDocument doc = gen.Generate(false);
                if (doc == null)
                {
                    FailedEmpID.Add(e.id);
                    FailedDescription.Add(gen.errorDesc);
                    SystemException.Add(gen.ex);
                }
                else
                    ret.Add(doc);
            }
            error = new BatchError();
            error.ObjectID = FailedEmpID.ToArray();
            error.ErrorDesc = FailedDescription.ToArray();
            error.SystemException = SystemException.ToArray();
            return ret;
        }
        // BDE exposed
        public TransactionOfBatch[] testComponent(DateTime date, int PCDID, int formulaID, int employeeID, int periodID
            , out string[] vars, out EData[] vals
            )
        {
            PayrollPeriod p = GetPayPeriod(periodID);
            Employee e = GetEmployee(employeeID,date.Ticks);
            Dictionary<int, PayrollComponentFormula[]> formulae = new Dictionary<int, PayrollComponentFormula[]>();
            
            foreach (PayrollComponentDefination pcdef in m_pcDefs.Values)
            {
                if (pcdef.SupportFormula)
                {
                    formulae.Add(pcdef.ID, this.GetPCFormulae(pcdef.ID));
                    
                }
            }
            PayrollComponentFormula f=null;
            foreach(PayrollComponentFormula pf in formulae[PCDID])
                if(pf.ID==formulaID)
                {
                    f=pf;
                    break;
                }
            PayrollGenerator gen = new PayrollGenerator(formulae, this, date, e, p);
            INTAPS.Payroll.BDE.PCD.BDCBase def=dataHandlers[PCDID] as INTAPS.Payroll.BDE.PCD.BDCBase;
            PayrollComponentData [] data=GetPCData(PCDID,formulaID,AppliesToObjectType.Employee,e.id,p.id,false);
            PayrollComponentData aData;
            if(data.Length==0)
                aData=null;
            else if(data.Length>1)
                throw new ClientServer.ServerUserMessage("Can't run test with multiple data");
            else
                aData=data[0];
            INTAPS.Payroll.BDE.PCD.PCFSymbolProvider evaluator = new PCD.PCFSymbolProvider(e, p, this, gen, def
                , def.GetFormulaSpecification(), f, aData);
            TransactionOfBatch[] ret= evaluator.GenerateTransactions();
            vals = evaluator.getEvaluatedVariable(out vars);
            return ret;
        }
        // BDE exposed
        public PayrollSetDocument GeneratePayrollPreview2(DateTime date, string title, AppliesToEmployees at, int periodID, out BatchError error)
        {
            List<int> FailedEmpID = new List<int>();
            List<string> FailedDescription = new List<string>();
            List<Exception> SystemException = new List<Exception>();

            Dictionary<int, PayrollComponentFormula[]> formulae = new Dictionary<int, PayrollComponentFormula[]>();
            foreach (PayrollComponentDefination pcdef in m_pcDefs.Values)
            {
                if (pcdef.SupportFormula)
                {
                    formulae.Add(pcdef.ID, this.GetPCFormulae(pcdef.ID));
                }
            }
            PayrollPeriod p = GetPayPeriod(periodID);
            Employee[] es = this.GetEmployees(at,p.toDate.Ticks);
            Array.Sort(es, new EmployeeComparer(EmployeeField.Name));
            List<PayrollDocument> list = new List<PayrollDocument>();
            
            foreach (Employee e in es)
            {
                PayrollGenerator gen = new PayrollGenerator(formulae, this, date, e, p);
                PayrollDocument doc = gen.Generate(false);
                if (doc == null)
                {
                    FailedEmpID.Add(e.id);
                    FailedDescription.Add(gen.errorDesc);
                    SystemException.Add(gen.ex);
                }
                else
                    list.Add(doc);
            }
            error = new BatchError();
            error.ObjectID = FailedEmpID.ToArray();
            error.ErrorDesc = FailedDescription.ToArray();
            error.SystemException = SystemException.ToArray();
            PayrollSetDocument set = new PayrollSetDocument();
            set.DocumentDate = date;
            set.at = at;
            set.payPeriod = p;
            set.payrolls = list.ToArray();
            set.LongDescription = title;
            set.ShortDescription=p.name+" "+title;
            set.PaperRef = set.ShortDescription;
            return set;
        }
        public int PostPayroll2(int AID,DateTime date, string title, AppliesToEmployees at, int periodID, out BatchError error)
        {
            PayrollSetDocument doc= GeneratePayrollPreview2(date, title, at, periodID, out error);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    int ret = m_Accounting.PostGenericDocument(AID, doc);
                    dspWriter.CommitTransaction();
                    return ret;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        // BDE exposed
        public string getAppliesToString(AppliesToEmployees at)
        {
            if (at.All)
                return "All Employees";
            int no = at.OrgUnits == null ? 0 : at.OrgUnits.Length;
            int ne = at.Employees == null ? 0 : at.Employees.Length;
            
            List<string> items = new List<string>();
            bool tooMany=false;
            foreach (int ou in at.OrgUnits)
            {
                if (items.Count == 5)
                {
                    tooMany = true;
                    break;
                }
                items.Add(GetOrgUnit(ou).Name);
            }
            foreach (int e in at.Employees)
            {
                if (items.Count == 5)
                {
                    tooMany = true;
                    break;
                }
                items.Add(GetEmployee(e).EmployeeNameID);
            }
            string orgString = null;
            if (items.Count == 1)
                orgString = items[0];
            else if (items.Count> 1)
            {
                orgString = items[0];
                int i;
                for (i = 1; i < items.Count; i++)
                {
                    if(i==items.Count-1 && !tooMany)
                        orgString += " and " + items[i];
                    else
                        orgString += ", " + items[i];
                }
                if (tooMany) 
                    orgString += ", ...";
            }
            if (orgString == null)
                return "";
            return orgString;
        }
        public BatchError GeneratePayroll(DateTime date, AppliesToEmployees at, int PeriodID)
        {
            SystemBusyMessage = "Generating payrolls.";
            Progress = 0;
            ProgressMax = 0;
            StopProgress = false;

            try
            {
                List<int> FailedEmpID = new List<int>();
                List<string> FailedDescription = new List<string>();
                List<Exception> SystemException = new List<Exception>();
                PayrollPeriod pp = GetPayPeriod(PeriodID);
                Employee[] es = this.GetEmployees(at,pp.toDate.Ticks);
                Array.Sort(es, new EmployeeComparer(EmployeeField.Name));
                ProgressStage = 0;
                ProgressMax = es.Length;
                Progress = 0;
                
                Dictionary<int, PayrollComponentFormula[]> formulae = new Dictionary<int, PayrollComponentFormula[]>();
                foreach (PayrollComponentDefination pcdef in m_pcDefs.Values)
                {
                    if (pcdef.SupportFormula)
                    {
                        formulae.Add(pcdef.ID, this.GetPCFormulae(pcdef.ID));
                    }
                }
                foreach (Employee e in es)
                {
                    if (StopProgress)
                        break;
                    lock (dspWriter)
                    {
                        dspWriter.setReadDB(this.DBName);
                        try
                        {
                            PayrollGenerator pg = new PayrollGenerator(formulae, this, date, e, pp);



                            if (pg.Generate(true) == null)
                            {
                                FailedEmpID.Add(e.id);
                                FailedDescription.Add(pg.errorDesc);
                                SystemException.Add(pg.ex);
                            }

                        }
                        catch (Exception ex)
                        {
                            FailedEmpID.Add(e.id);
                            FailedDescription.Add("System error.");
                            SystemException.Add(ex);
                        }
                        finally
                        {
                            dspWriter.restoreReadDB();
                        }
                    }
                    Progress++;
                }
                BatchError ret = new BatchError();
                ret.ObjectID = FailedEmpID.ToArray();
                ret.ErrorDesc = FailedDescription.ToArray();
                ret.SystemException = SystemException.ToArray();
                return ret;
            }
            finally
            {
                ProgressMax = -1;
            }
        }

        public BatchError GeneratePayroll2(DateTime date, string title,AppliesToEmployees at, int PeriodID)
        {
            SystemBusyMessage = "Generating payrolls.";
            Progress = 0;
            ProgressMax = 0;
            StopProgress = false;

            try
            {
                List<int> FailedEmpID = new List<int>();
                List<string> FailedDescription = new List<string>();
                List<Exception> SystemException = new List<Exception>();
                PayrollPeriod pp = GetPayPeriod(PeriodID);
                Employee[] es = this.GetEmployees(at,pp.toDate.Ticks);
                Array.Sort(es, new EmployeeComparer(EmployeeField.Name));
                ProgressStage = 0;
                ProgressMax = es.Length;
                Progress = 0;
                
                Dictionary<int, PayrollComponentFormula[]> formulae = new Dictionary<int, PayrollComponentFormula[]>();
                foreach (PayrollComponentDefination pcdef in m_pcDefs.Values)
                {
                    if (pcdef.SupportFormula)
                    {
                        formulae.Add(pcdef.ID, this.GetPCFormulae(pcdef.ID));
                    }
                }
                foreach (Employee e in es)
                {
                    if (StopProgress)
                        break;
                    lock (dspWriter)
                    {
                        dspWriter.setReadDB(this.DBName);
                        try
                        {
                            PayrollGenerator pg = new PayrollGenerator(formulae, this, date, e, pp);

                            if (pg.Generate(true) == null)
                            {
                                FailedEmpID.Add(e.id);
                                FailedDescription.Add(pg.errorDesc);
                                SystemException.Add(pg.ex);
                            }

                        }
                        catch (Exception ex)
                        {
                            FailedEmpID.Add(e.id);
                            FailedDescription.Add("System error.");
                            SystemException.Add(ex);
                        }
                        finally
                        {
                            dspWriter.restoreReadDB();
                        }
                    }
                    Progress++;
                }
                BatchError ret = new BatchError();
                ret.ObjectID = FailedEmpID.ToArray();
                ret.ErrorDesc = FailedDescription.ToArray();
                ret.SystemException = SystemException.ToArray();
                return ret;
            }
            finally
            {
                ProgressMax = -1;
            }
        }
        // BDE exposed
        public int getEmployeePayrollDocumentID(int employeeID, int periodID)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string sql = string.Format("Select accountDocumentID from {0}.dbo.Payroll where employeeID={1} and periodID={2}"
                        , this.DBName, employeeID, periodID);
                object ret = dspReader.ExecuteScalar(sql);
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public double[] getPaymetAmounts(int p, int[] empID, out double[] payroll)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                double[] ret = new double[empID.Length];
                payroll = new double[empID.Length];
                for (int i = 0; i < empID.Length; i++)
                {
                    object v = dspReader.ExecuteScalar(string.Format("SELECT sum([amount]) FROM {0}.[dbo].[PayrollPayment] where periodID={1} and employeeID={2}", this.DBName, p, empID[i]));
                    if (v is double)
                        ret[i] = (double)v;
                    else
                        ret[i] = 0;
                    v = dspReader.ExecuteScalar(string.Format("SELECT  sum((case when deduct=0 then 1 else -1 end)* [amount]) FROM {0}.[dbo].[PayrollComponent] where periodID={1} and employeeID={2}", this.DBName, p, empID[i]));
                    if (v is double)
                        payroll[i] = (double)v;
                    else
                        payroll[i] = 0;

                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public class PayrollGenerator
        {
            Employee m_employee;
            PayrollPeriod m_period;
            DateTime m_date;
            PayrollBDE m_parent;
            Dictionary<int, Dictionary<int, PayrollComponent>> m_evaluated;
            Dictionary<int, PayrollComponentFormula[]> m_formulae;
            public string errorDesc;
            public Exception ex;
            public PayrollDocument result;
            public DateTime generateDate
            {
                get
                {
                    return m_date;
                }
            }
            public PayrollGenerator(Dictionary<int, PayrollComponentFormula[]> f, PayrollBDE parent, DateTime date, Employee e, PayrollPeriod p)
            {
                m_date = date;
                m_employee = e;
                m_period = p;
                m_parent = parent;
                m_evaluated = new Dictionary<int, Dictionary<int, PayrollComponent>>();
                m_formulae = f;
            }
            public PayrollComponent GetComponent(string def, string f)
            {
                PayrollComponentDefination defobj = null;
                foreach (KeyValuePair<int, PayrollComponentDefination> d in m_parent.m_pcDefs)
                {
                    if (d.Value.Name.ToUpper() == def.ToUpper())
                    {
                        defobj = d.Value;
                        break;
                    }
                }
                if (defobj == null)
                    throw new Exception("Unknown payroll component defination:" + def);
                PayrollComponentFormula fobj = null;
                if (!string.IsNullOrEmpty(f))
                {
                    PayrollComponentFormula[] fs = m_formulae[defobj.ID];
                    foreach(PayrollComponentFormula af in fs)
                        if (af.Name.ToUpper() == f.ToUpper())
                        {
                            fobj = af;
                            break;
                        }
                    if (fobj==null)
                        throw new Exception("Unknown payroll component formulat def:" + def + " f:" + f);
                }
                return GetComponent(defobj, fobj);
            }
            public PayrollComponent GetComponent(PayrollComponentDefination def, PayrollComponentFormula f)
            {
                Dictionary<int, PayrollComponent> fs=null;
                int PDCID = def.ID;
                int FormulaID = f == null ? -1 : f.ID;
                if (m_evaluated.ContainsKey(PDCID))
                {
                    fs = m_evaluated[PDCID];
                    if (fs.ContainsKey(FormulaID))
                        return fs[FormulaID];
                }
                PayrollComponent ret=EvaluateComponent(def, f);
                if (m_evaluated.ContainsKey(PDCID))
                {
                    fs = m_evaluated[PDCID];
                }
                else
                {
                    fs = new Dictionary<int, PayrollComponent>();
                    m_evaluated.Add(PDCID, fs);
                }
                fs.Add(FormulaID, ret);
                return ret;
            }

            private PayrollComponent EvaluateComponent(PayrollComponentDefination pcdef, PayrollComponentFormula f)
            {
                if (f == null)
                {
                    if (!m_parent.AppliesToEmployee(pcdef.at, m_employee.id))
                        return null;
                    if (!m_parent.WithinValidPariod(pcdef.periodRange.PeriodFrom, pcdef.periodRange.PeriodTo, m_period.id))
                        return null;
                }
                else
                {
                    if (!m_parent.AppliesToEmployee(f.at, m_employee.id))
                        return null;
                    if (!m_parent.WithinValidPariod(f.periodRange.PeriodFrom, f.periodRange.PeriodTo, m_period.id))
                        return null;
                }
                lock (m_parent.dataHandlers[pcdef.ID])
                {
                    PayrollComponent comp = m_parent.dataHandlers[pcdef.ID].GenerateTransactions(this, m_employee, f, m_period);
                    comp.PCDID = pcdef.ID;
                    comp.FormulaID = f == null ? -1 : f.ID;
                    comp.employeeID = m_employee.id;
                    comp.periodID = m_period.id;
                    return comp;
                }
            }
            public PayrollDocument Generate(bool save)
            {
                if (m_employee.status != EmployeeStatus.Enrolled)
                {
                    errorDesc = "Employee " + m_employee.EmployeeNameID+" is not enrolled";
                    ex = null;
                    return null;
                }
                if (m_employee.status == EmployeeStatus.Fired)
                {
                    errorDesc = "Employee " + m_employee.EmployeeNameID + " is dismissed";
                    ex = null;
                    return null;
                }
                string efilter = "EmployeeID=" + m_employee.id + " and PeriodID=" + m_period.id;
                PayrollDocument[] pd;
                
                
                pd = m_parent.GetPayrollByFilter(m_parent.dspWriter, efilter);
                
                if (pd.Length > 0)
                {
                    errorDesc="Payroll allready generated for " + efilter;
                    ex = null;
                    return null;
                }
                PayrollDocument pdoc = new PayrollDocument();
                pdoc.DocumentDate = m_date;
                pdoc.employee = m_employee;
                pdoc.payPeriod = m_period;
                List<PayrollComponent> comps = new List<PayrollComponent>();
                int i = 0;
                foreach (PayrollComponentDefination pcdef in m_parent.m_pcDefs.Values)
                {
                    if (pcdef.SupportFormula)
                    {
                        PayrollComponentFormula[] fs = m_formulae[pcdef.ID];
                        foreach (PayrollComponentFormula f in fs)
                        {
                            try
                            {
                                PayrollComponent c = GetComponent(pcdef, f);
                                if(c!=null)
                                    comps.Add(c);
                            }
                            catch (Exception innerex)
                            {
                                errorDesc = "Error trying to generate payroll component:" + f.Name + " for employee ID:" + m_employee.employeeID;
                                ex = innerex;
                                return null;
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            PayrollComponent c = GetComponent(pcdef, null);
                            if (c != null)
                                comps.Add(c);
                        }
                        catch (Exception innerex)
                        {
                            errorDesc = "Error trying to generate payroll component:" + pcdef.Name + " for employee ID:" + m_employee.employeeID;
                            ex = innerex;
                            return null;
                        }
                    }
                    i++;
                }
                pdoc.components = comps.ToArray();
                if (pdoc.NetPay < 0)
                {
                    errorDesc = "Negative net payment is not allowed for employee ID:" + m_employee.employeeID;
                    ex = null;
                    return null;
                }
                if (save)
                {
                    lock (m_parent.dspWriter)
                    {
                        m_parent.dspWriter.Insert(m_parent.DBName, "PayrollDocument", new string[] { "EmployeeID", "PeriodID", "XmlPayrollData", "AccountDocumentID" }
                        , new object[] { m_employee.id, m_period.id, pdoc.ToXML(), DBNull.Value });
                    }
                }
                result = pdoc;
                return result;
            }
        }
        public PayrollSystemParameters SysPars
        {
            get
            {
                return m_sysPars;
            }
        }
    }
}
