using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using System.Threading;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        public PayrollDocument[] GetPayrollByFilter(INTAPS.RDBMS.SQLHelper dsp, string Filter)
        {
            DataTable dat = dsp.GetDataTable("Select EmployeeID,PeriodID,XmlPayrollData,AccountDocumentID,PostDate,PayDocumentID,PayDocumentDate from "+this.DBName+".dbo.PayrollDocument where " + Filter);
            PayrollDocument[] ret = new PayrollDocument[dat.Rows.Count];
            int i = 0;
            foreach (DataRow row in dat.Rows)
            {
                ret[i]= (PayrollDocument)PayrollDocument.DeserializeXML((string)row[2], typeof(PayrollDocument));
                ret[i].AccountDocumentID = row[3] is int ? (int)row[3] : -1;
                ret[i].DocumentDate = row[4] is DateTime ? (DateTime)row[4] : DateTime.Now;
                ret[i].payDocumentID = row[5] is int ? (int)row[5] : -1;
                ret[i].payDocumentDate = row[6] is DateTime ? (DateTime)row[6] : DateTime.Now;
                i++;  
            }
            return ret;
        }
        
        PayrollComponent ConvertToCostCenterAccount(Employee emp,PayrollComponent comp)
        {
            foreach (TransactionOfBatch t in comp.Transactions)
            {
                CostCenterAccount acnt = m_Accounting.GetCostCenterAccount(emp.costCenterID, t.AccountID);
                if (acnt == null)
                    throw new Exception("Account not found CSID:" + acnt.id);
                t.AccountID = acnt.id;
            }
            return comp;
        }
        public PayrollDocument GetPayroll(int EmpID, int PeriodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                PayrollDocument[] _ret = GetPayrollByFilter(dspReader, "EmployeeID=" + EmpID + " and PeriodID=" + PeriodID);
                if (_ret.Length == 0)
                    return null;
                return _ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public PayrollDocument[] GetPayrolls(AppliesToEmployees at, int PeriodID)
        {
            Employee [] es=GetEmployees(at);
            List<PayrollDocument> _ret=new List<PayrollDocument>();
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                foreach (Employee e in es)
                {
                    _ret.AddRange(GetPayrollByFilter(dspReader, "EmployeeID=" + e.id + " and PeriodID=" + PeriodID));
                }
            }
            finally
            {
               base.ReleaseHelper(dspReader);
            }
            return _ret.ToArray();
        }
        int compareParyollSets(PayrollSetDocument doc1, PayrollSetDocument doc2)
        {
            if (doc1 == null && doc2 == null)
                return 0;
            if (doc1 == null && doc2 != null)
                return 1;
            if (doc1 != null && doc2 == null)
                return -1;
            return doc2.DocumentDate.CompareTo(doc2.DocumentDate);
        }
        // BDE exposed
        public PayrollSetDocument[] getPayrollSetsOfAPeriod(int periodID)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                int[] docIDs = dspReader.GetColumnArray<int>(string.Format("Select accountDocumentID from {0}.dbo.PayrollSetDocument where periodID={1}", this.DBName, periodID), 0);
                PayrollSetDocument[] ret = new PayrollSetDocument[docIDs.Length];
                for (int i = 0; i < docIDs.Length; i++)
                    ret[i] = m_Accounting.GetAccountDocument(docIDs[i], true) as PayrollSetDocument;
                Array.Sort<PayrollSetDocument>(ret, new Comparison<PayrollSetDocument>(compareParyollSets));
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public PayrollSetDocument getPayrollSetByEmployee(int periodID, int employeeID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {

                object docID = dspReader.ExecuteScalar(string.Format("Select accountDocumentID from {0}.dbo.Payroll where periodID={1} and employeeID={2}", this.DBName, periodID,employeeID));
                if(!(docID is int))
                    return null;
                return m_Accounting.GetAccountDocument((int)docID, true) as PayrollSetDocument;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
    }
}
