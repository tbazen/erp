using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using System.Threading;
using INTAPS.ClientServer;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        private void deletePayrollFromPayrollDatabase(PayrollDocument doc)
        {
            dspWriter.Delete(m_DBName, "PayrollDocument", "EmployeeID=" + doc.employee.id + " and PeriodID=" + doc.payPeriod.id);
        }
        public void DeletePayroll(int AID, int EmployeeID, int PeriodID)
        {
            PayrollDocument doc = GetPayroll(EmployeeID, PeriodID);
            if (doc.Posted)
                m_Accounting.DeleteGenericDocument(AID, doc.AccountDocumentID);
            else
            {
                lock (dspWriter)
                {
                    try
                    {
                        dspWriter.BeginTransaction();
                        deletePayrollFromPayrollDatabase(doc);
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
            }
        }

        
        public void DeletePayrollPosted(int AID,PayrollDocument doc)
        {
            if (doc.Paid)
                throw new Exception("It is not possible to delete a payroll that is paid to the account system.");
            lock (dspWriter)
            {
                dspWriter.setReadDB (this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    if (doc.Posted)
                    {
                        m_Accounting.DeleteAccountDocument(AID, doc.AccountDocumentID,false);
                    }
                    deletePayrollFromPayrollDatabase(doc);
                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }

        public void PostPayroll(int AID,int companyCostCenterID,int EmployeeID, int PeriodID, DateTime postDate)
        {
            PayrollDocument pr = GetPayroll(EmployeeID, PeriodID);
            if (pr.Posted)
                throw new Exception("Payroll allready posed.");
            pr.DocumentDate = postDate;
            pr.PaperRef = pr.payPeriod.name + "-" + pr.employee.employeeID;
            pr.ShortDescription = "Payroll of " + pr.employee.EmployeeNameID + " for " + pr.payPeriod.name;
            List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
            int unclaimed = GetEmployeeUncollectedPaymentAccount(pr.employee.id);
            //int expense = GetPayrollExpenseAcount(pr.employee);
            int expense = GetEmployeePayrollExpenseAcount(pr.employee);

            TransactionOfBatch net = null, exp = null;
            foreach (PayrollComponent pc in pr.components)
            {
                foreach (TransactionOfBatch t in pc.Transactions)
                {
                    if (AccountBase.AmountEqual(t.Amount, 0))
                        continue;
                    if (t.AccountID == unclaimed)
                    {
                        if (net == null)
                        {
                            net = t;
                            net.Note = "Net Salary for employee " +pr.employee.EmployeeNameID+" period "+ pr.payPeriod.name;
                        }
                        else
                            net.Amount += t.Amount;
                    }
                    else if (t.AccountID == expense)
                    {
                        if (exp== null)
                        {
                            exp = t;
                            exp.Note = "Salary expense for employee " + pr.employee.EmployeeNameID + " period " + pr.payPeriod.name;
                        }
                        else
                            exp.Amount += t.Amount;
                    }
                    else
                        trans.Add(t);
                }
                
            }
            if (exp != null)
                trans.Insert(0, exp);
            if (net != null)
                trans.Add(net);
            foreach (TransactionOfBatch t in trans)
            {
                CostCenterAccount csa = m_Accounting.GetCostCenterAccount(t.AccountID==expense? pr.employee.costCenterID:companyCostCenterID, t.AccountID);
                if (csa == null)
                    throw new ServerUserMessage("Invalid account found in the transactions of " + pr.employee.EmployeeNameID);
                t.AccountID = csa.id;
            }

            lock (dspWriter)
            {
                dspWriter.setReadDB (this.DBName);
                try
                {
                    dspWriter.BeginTransaction();
                    int AccountDocumentID = m_Accounting.RecordTransaction(AID, pr, trans.ToArray(), true, false);
                    dspWriter.Update(m_DBName, "PayrollDocument", new string[] { "AccountDocumentID", "PostDate" }
                    , new object[] { AccountDocumentID, postDate }
                    , "EmployeeID=" + EmployeeID + " and PeriodID=" + PeriodID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally

                {
                    dspWriter.restoreReadDB();
                }
            }
        }

    }
}
