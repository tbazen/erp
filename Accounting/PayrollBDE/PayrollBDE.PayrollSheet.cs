﻿using System;
using System.Collections.Generic;
using INTAPS.Evaluator;
using System.Web;

namespace INTAPS.Payroll.BDE
{
    
    partial class PayrollBDE
    {
        public class PayrollSheetRowEvaluator : ISymbolProvider
        {
            PayrollDocument m_doc;
            public EData[] Values;
            Dictionary<string, EData> m_vars;
            Dictionary<string, bool> m_evaluted;
            Dictionary<string, string> m_varformula;
            Dictionary<int, PayrollComponentFormula[]> _formulae;
            ComponentFunction m_func;
            Dictionary<int, PayrollComponentDefination> _defs;
            PayrollBDE _parent;
            public PayrollSheetRowEvaluator(PayrollBDE parent, Dictionary<int, PayrollComponentDefination> defs, Dictionary<int, PayrollComponentFormula[]> f, PayrollDocument doc, PayrollSheetColumn[] names)
            {
                
                _parent = parent;
                _formulae = f;
                m_doc = doc;
                m_vars = new Dictionary<string, EData>();
                m_evaluted = new Dictionary<string, bool>();
                m_varformula = new Dictionary<string, string>();
                _defs = defs;
                for (int i = 0; i < names.Length; i++)
                {
                    m_vars.Add(names[i].name.ToUpper(), EData.Empty);
                    m_evaluted.Add(names[i].name.ToUpper(), false);
                    m_varformula.Add(names[i].name.ToUpper(),  names[i].formula);
                }
                m_vars.Add("_NET", new EData(DataType.Float, doc.NetPay));
                m_evaluted.Add("_NET", true);
                m_varformula.Add("_NET", doc.NetPay.ToString());

                m_vars.Add("EMPID", new EData(DataType.Int, doc.employee.id));
                m_evaluted.Add("EMPID", true);
                m_varformula.Add("EMPID", doc.employee.id.ToString());

                m_vars.Add("PERIODID", new EData(DataType.Int, doc.payPeriod.id));
                m_evaluted.Add("PERIODID", true);
                m_varformula.Add("PERIODID", doc.payPeriod.id.ToString());

                m_func = new ComponentFunction(this);
                Values = new EData[names.Length];
                for (int j = 0; j < names.Length; j++)
                {
                    Values[j] = GetVariableValue(names[j].name.ToUpper());
                }
            }
            List<string> refs = new List<string>();
            private EData GetVariableValue(string p)
            {
                if (refs.Contains(p))
                    throw new INTAPS.ClientServer.ServerUserMessage("Circular reference " + p);
                if (m_evaluted[p])
                    return m_vars[p];
                refs.Add(p);
                try
                {
                Symbolic e = new Symbolic();
                e.SetSymbolProvider(this);
                e.Expression = m_varformula[p];
                EData ret = e.Evaluate();
                m_evaluted[p] = true;
                m_vars[p] = ret;
                return ret;
                }
                finally
                {
                    refs.Remove(p);
                }
            }

            #region ISymbolProvider Members

            public FunctionDocumentation[] GetAvialableFunctions()
            {
                return new FunctionDocumentation[0];
            }

            public EData GetData(URLIden iden)
            {
                return GetData(iden.Element.ToUpper());
            }

            public EData GetData(string symbol)
            {
                return GetVariableValue(symbol.ToUpper());

            }

            public FunctionDocumentation GetDocumentation(IFunction f)
            {
                return null;
            }

            public FunctionDocumentation GetDocumentation(URLIden iden)
            {
                return null;
            }

            public FunctionDocumentation GetDocumentation(string Symbol)
            {
                return null;
            }

            public IFunction GetFunction(URLIden iden)
            {
                return GetFunction(iden.Element);
            }

            public IFunction GetFunction(string symbol)
            {
                IFunction func = _parent.GetEvaluatorFunction(symbol.ToUpper());
                if (func != null)
                    return func;
                if (symbol.ToUpper() == m_func.Symbol.ToUpper())
                    return m_func;
                if (CalcGlobal.Functions.Contains(symbol.ToUpper()))
                    return (IFunction)CalcGlobal.Functions[symbol.ToUpper()];
                return null;
            }

            public bool SymbolDefined(string Name)
            {
                return m_evaluted.ContainsKey(Name.ToUpper());
            }

            #endregion
            public class ComponentFunction : IVarParamCountFunction
            {
                PayrollSheetRowEvaluator _parent;
                int nPars = 2;
                public ComponentFunction(PayrollSheetRowEvaluator re)
                {
                    _parent = re;
                }

                #region IFunction Members

                public string Name
                {
                    get { return "Get component amount."; }
                }

                public string Symbol
                {
                    get { return "Component"; }
                }

                public FunctionType Type
                {
                    get { return FunctionType.PreFix; }
                }

                public int ParCount
                {
                    get { return nPars; }
                }

                public EData Evaluate(EData[] Pars)
                {
                    string PCD = (string)Pars[0].Value;
                    string F = null;
                    if (Pars.Length > 1)
                        if (!string.IsNullOrEmpty(Pars[1].Value as string))
                            F = Pars[1].Value as string;
                    PayrollComponent _ret = null;
                    foreach (PayrollComponent comp in _parent.m_doc.components)
                    {
                        if (PCD.ToUpper() == _parent._defs[comp.PCDID].Name.ToUpper())
                        {
                            if (F == null)
                            {
                                _ret = comp;
                                break;
                            }
                            else
                            {
                                foreach (PayrollComponentFormula f in _parent._formulae[comp.PCDID])
                                {
                                    if (f.ID == comp.FormulaID)
                                    {
                                        if (f.Name.ToUpper() == F.ToUpper())
                                        {
                                            _ret = comp;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }
                    if (_ret == null)
                        return new EData(DataType.Float, (double)0);
                    return new EData(DataType.Float, _ret.Amount);
                }
                #endregion

                #region IVarParamCountFunction Members

                public IVarParamCountFunction Clone()
                {
                    return new ComponentFunction(_parent);
                }
                public bool SetParCount(int n)
                {
                    if (n == 1 || n == 2)
                    {
                        nPars = n;
                        return true;
                    }
                    return false;
                }
                #endregion
            }
        }

        static void SetRowSpan(PayrollSheetColumn[] cols, BIZNET.iERP.bERPHtmlTableRowGroup table, int[] index, int level)
        {
            BIZNET.iERP.bERPHtmlTableRow row;
            row = table.rows[level];
            int startindex = index[level];
            int j = 0;
            for (int k = 0; k < cols.Length; k++)
            {
                if (!cols[k].visisble)
                    continue;
                PayrollSheetColumn[] ch = cols[k].childs;

                if (ch.Length == 0)
                {
                    row.cells[startindex + j].rowSpan = table.rows.Count - level;
                }
                else
                {
                    SetRowSpan(ch, table, index, level + 1);
                }
                index[level]++;

                j++;
            }
        }
    
        public static int GetRows(PayrollSheetColumn[] cols , BIZNET.iERP.bERPHtmlTableRowGroup tableRowGroup, ref int level)
        {
            int ret = 0;
            BIZNET.iERP.bERPHtmlTableRow row = null;
            int maxlevel = level;
            int rowindexStart = 0;
            for (int k = 0; k < cols.Length; k++)
            {
                if (!cols[k].visisble)
                    continue;
                PayrollSheetColumn[] ch = cols[k].childs;
                if (row == null)
                {
                    if (tableRowGroup.rows.Count == level)
                    {
                        row = new BIZNET.iERP.bERPHtmlTableRow();
                        tableRowGroup.rows.Add(row);
                    }
                    else
                    {
                        row = tableRowGroup.rows[level];
                        rowindexStart = row.cells.Count;
                    }
                }
                var cell = new BIZNET.iERP.bERPHtmlTableCell(cols[k].header, cols[k].headerCss);
                int l = level + 1;
                int ncols;
                if (ch.Length>0)
                {
                    ncols = GetRows(ch, tableRowGroup, ref l);
                }
                else
                    ncols = 1;
                if (l > maxlevel)
                    maxlevel = l;
                cell.colSpan = ncols;
                ret += ncols;
                row.cells.Add(cell);
            }
            level = maxlevel;
            return ret;
        }

        // BDE exposed
        public string GetPayrollTableHTML(PayrollSetDocument set, int periodID, int formatID)
        {
            PayrollPeriod pp = GetPayPeriod(periodID);
            PayrollSheetFormat format = m_sysPars.getColumnFormula(formatID);
            PayrollDocument[] prdocs = set.payrolls;

            Dictionary<int, PayrollComponentFormula[]> fs = new Dictionary<int, PayrollComponentFormula[]>();

            foreach (KeyValuePair<int,PayrollComponentDefination> def in this.m_pcDefs)
            {
                fs.Add(def.Key, GetPCFormulae(def.Key));
            }

            var tables = new List<BIZNET.iERP.bERPHtmlTable>();
            var bs = new List<BIZNET.iERP.bERPHtmlTable>();
            if (format.breakNumberOfRows < 1)
            {
                var htable = new BIZNET.iERP.bERPHtmlTable();
                htable.createBodyGroup();
                htable.styles.Add("border:2px solid black");
                addHeaderRows(format, htable.groups[0]);
                tables.Add(htable);
            }
            BIZNET.iERP.bERPHtmlTableRow row;
            BIZNET.iERP.bERPHtmlTableCell cell;

            BIZNET.iERP.bERPHtmlTable btable =null;
            if (format.breakNumberOfRows < 1)
            {
                btable = new BIZNET.iERP.bERPHtmlTable();
                btable.styles.Add("border:2px solid black");
                bs.Add(btable);
            }
            int k = 0;
            
            PayrollSheetColumn[] colVariable=format.getFormulaSet();
            double[] colTotals = new double[colVariable.Length];
            for (int i = 0; i < colTotals.Length; i++)
                colTotals[i] = 0;
            List<BIZNET.iERP.bERPHtmlTable> tabelRows = new List<BIZNET.iERP.bERPHtmlTable>();
            List<BIZNET.iERP.bERPHtmlTable> rowTables = new List<BIZNET.iERP.bERPHtmlTable>();
            
            foreach (PayrollDocument doc in prdocs)
            {
                if (format.breakNumberOfRows > 0)
                {
                    if (k % format.breakNumberOfRows == 0)
                    {
                        BIZNET.iERP.bERPHtmlTable htable = new BIZNET.iERP.bERPHtmlTable();
                        htable.createBodyGroup();
                        htable.styles.Add("border:2px solid black");
                        addHeaderRows(format, htable.groups[0]);
                        tables.Add(htable);

                        btable = new BIZNET.iERP.bERPHtmlTable();
                        btable.createBodyGroup();
                        btable.styles.Add("border:2px solid black");
                        bs.Add(btable);
                    }
                }

                string css = k % 2 == 0 ? "LedgerGridOddRowCell" : "LedgerGridEvenRowCell";

                row = BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(btable.groups[0]
                    , new BIZNET.iERP.bERPHtmlTableCell((k + 1).ToString(), css)
                    , new BIZNET.iERP.bERPHtmlTableCell(doc.employee.employeeID, css)
                    , new BIZNET.iERP.bERPHtmlTableCell(doc.employee.employeeName, css));

               
                try
                {

                    PayrollSheetRowEvaluator re = new PayrollSheetRowEvaluator(this, m_pcDefs, fs, doc, colVariable);

                    for (int i = 0; i < colVariable.Length; i++)
                    {
                        if (!colVariable[i].visisble)
                            continue;
                        EData eval = re.Values[i];
                        string txt="";
                        if (eval.Value is double)
                        {
                            double val = (double)eval.Value;
                            if (val != 0)
                                txt = val.ToString(colVariable[i].numberFormat);
                            if (colVariable[i].total)
                                colTotals[i] += val;
                        }
                        else
                        {
                            if (eval.Value == null)
                                txt = "";
                            else

                                txt = eval.ToString();
                        }
                        string [] cssArr=colVariable[i].bodyCss.Split(new char[]{','},StringSplitOptions.RemoveEmptyEntries);
                        css = cssArr[k % cssArr.Length];
                        cell = new BIZNET.iERP.bERPHtmlTableCell(txt, css);
                        cell.styles.Add("text-align:right");
                        row.cells.Add(cell);                        
                    }
                }
                catch (Exception ex)
                {
                    row.cells.Add(new BIZNET.iERP.bERPHtmlTableCell(ex.Message, colVariable.Length));
                }
                k++;
            }

            /*//sort rows
            List<int> sortCols = new List<int>();
            int colIndex = 0;
            foreach (PayrollSheetColumn c in colVariable)//collect sort columns
            {
                if (c.sortOrder != -1)
                    sortCols.Add(colIndex);
                colIndex++;
            }
            if (sortCols.Count > 0)
            {

                sortCols.Sort(delegate(int c1, int c2)  //sort sort columns
                {
                    return format.columns[c1].sortOrder.CompareTo(format.columns[c2].sortOrder);
                }
                );
                tabelRows.Sort(delegate(HtmlTableRow row1,HtmlTableRow row2)
                    {
                        foreach (int col in sortCols)
                        {

                        }

                        return -1;
                    }
                    );

            }*/

            BIZNET.iERP.bERPHtmlTable ftable = new BIZNET.iERP.bERPHtmlTable();
            ftable.createBodyGroup();
            ftable.styles.Add("border:2px solid black");

            row =  BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(ftable.groups[0],
                new BIZNET.iERP.bERPHtmlTableCell(BIZNET.iERP.TDType.body, "Total", "LedgerGridFooterCell", 1,3));

            for (int i = 0; i < colVariable.Length; i++)
            {
                if (!colVariable[i].visisble)
                    continue;
                string txt = "";
                double val = colTotals[i];
                if (val != 0)
                    txt = val.ToString(colVariable[i].numberFormat);
                cell = new BIZNET.iERP.bERPHtmlTableCell(txt, colVariable[i].footerCss);
                cell.styles.Add("text-align:right");
                row.cells.Add(cell);
            }
            string ret="";

            int m = 0;
            foreach (var htable in tables)
            {
                var b = bs[m];
                //if (m > 0)
                //    ret += @"<span class=""pagebreak"">Page</span>";
                if(m>0)
                ret += "<span>" + format.header + "</span>";
                else
                    ret += format.header;
                ret+= "<span class='payrollTitle'>" + set.LongDescription + "</span>"
                + "<br/><b>Period:</b>" + HttpUtility.HtmlEncode(pp.name)
                + "<br/><b>Date:</b>" + HttpUtility.HtmlEncode(DateTime.Now.ToLongDateString()) + "</b>"
                + "<table><thead>";
                foreach (var r in htable.groups[0].rows)
                {
                    ret += BIZNET.iERP.bERPHtmlBuilder.ToString(r);
                }
                ret += "</thead><tbody>";
                foreach (var r in b.groups[0].rows)
                {
                    ret += BIZNET.iERP.bERPHtmlBuilder.ToString(r);
                }
                ret += "</tbody>";
                if (m == bs.Count - 1)
                {
                    ret += "<tfoot>";
                    foreach (var r in ftable.groups[0].rows)
                    {
                        ret += BIZNET.iERP.bERPHtmlBuilder.ToString(r);
                    }

                    ret += "</tfoot>";
                }
                ret += "</table>";

                ret += format.footer;
                    
                m++;
            }
            return ret;
        }

        private static BIZNET.iERP.bERPHtmlTableRow addHeaderRows(PayrollSheetFormat format, BIZNET.iERP.bERPHtmlTableRowGroup htable)
        {
            BIZNET.iERP.bERPHtmlTableRow row;
            int level = 0;
            int ncols = GetRows(format.columns, htable, ref level);
            SetRowSpan(format.columns, htable, new int[htable.rows.Count], 0);
            row = htable.rows[0];
            row.cells.Insert(0, new BIZNET.iERP.bERPHtmlTableCell(BIZNET.iERP.TDType.body, "NO", "LedgerGridHeadCell", htable.rows.Count, 1));
            row.cells.Insert(1, new BIZNET.iERP.bERPHtmlTableCell(BIZNET.iERP.TDType.body, "ID", "LedgerGridHeadCell", htable.rows.Count, 1));
            row.cells.Insert(2, new BIZNET.iERP.bERPHtmlTableCell(BIZNET.iERP.TDType.body, "Name", "LedgerGridHeadCell", htable.rows.Count, 1));

            return row;
        }
        // BDE exposed
        public string GetPayrollTableHTML(int setDocumentID, int periodID, int formatID)
        {
            return GetPayrollTableHTML(m_Accounting.GetAccountDocument(setDocumentID, true) as PayrollSetDocument, periodID, formatID);
        }
        // BDE exposed
        public EData[] testSheetFormat(PayrollSheetFormat format, int employeeID, int periodID, out string[] varNames)
        {
            PayrollPeriod pp = GetPayPeriod(periodID);
            Employee e=GetEmployee(employeeID);
            Dictionary<int, PayrollComponentFormula[]> fs = new Dictionary<int, PayrollComponentFormula[]>();

            foreach (KeyValuePair<int,PayrollComponentDefination> def in this.m_pcDefs)
            {
                fs.Add(def.Key, GetPCFormulae(def.Key));
            }
            INTAPS.Accounting.BatchError err;
            PayrollSetDocument set= this.GeneratePayrollPreview2(DateTime.Now,"test",new AppliesToEmployees(employeeID),periodID,out err);
            if(err.SystemException.Length>0)
                if(err.SystemException[0] is Exception)
                    throw err.SystemException[0];

            PayrollSheetColumn[] formulae=format.getFormulaSet();
            PayrollSheetRowEvaluator evaluator = new PayrollSheetRowEvaluator(this, m_pcDefs, fs, set.payrolls[0], formulae);
            varNames = new string[formulae.Length];
            for (int i = 0; i < varNames.Length; i++)
                varNames[i] = formulae[i].name;
            return evaluator.Values;
        }
    }
}
