using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using INTAPS.ClientServer;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE : BDEBase
    {
        public void SetSystemParameters(int AID,string[] fields, object[] vals)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.SetSystemParameters<PayrollSystemParameters>(m_DBName, m_sysPars, fields, vals,true,AID);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }

            }
        }
        public int CreatePayPeriod(PayrollPeriod pp)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                dspWriter.setReadDB(this.DBName);
                try
                {
                    int ID = INTAPS.ClientServer.AutoIncrement.GetKey("Payroll.PeriodID");
                    dspWriter.Insert(m_DBName, "PayPeriod", new string[] { "ID", "Name", "FromDate", "ToDate", "Days", "Hours" }
                    , new object[] { ID, pp.name, pp.fromDate, pp.toDate, pp.days, pp.hours });
                    dspWriter.CommitTransaction();
                    return ID;
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception(ex.Message, ex);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
        public void UpdatePayPeriod(PayrollPeriod pp)
        {
            lock (dspWriter)
            {
                dspWriter.setReadDB(this.DBName);
                try
                {
                    dspWriter.Update(m_DBName, "PayPeriod", new string[] { "Days", "Hours" }
                    , new object[] { pp.days, pp.hours }, "ID=" + pp.id);
                }
                finally
                {
                    dspWriter.restoreReadDB();
                }
            }
        }
    }
}
