using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using INTAPS.ClientServer;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE : BDEBase
    {
        //System paramters
        PayrollSystemParameters _m_sysPars=null;
        PayrollSystemParameters m_sysPars
        {
            get
            {
                if (_m_sysPars == null)
                    LoadSystemParameters();
                return _m_sysPars;
            }
        }
        
        //backend connection
        AccountingBDE m_Accounting;
        void LoadSystemParameters()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                _m_sysPars = dspReader.LoadSystemParameters<PayrollSystemParameters>();
            }
            catch (Exception e)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("Error loading system parameters.", e);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public object[] GetSystemParameters(string[] fields)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSystemParameters<PayrollSystemParameters>(m_sysPars, fields);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public PayrollBDE(string bdeName, string DB, INTAPS.RDBMS.SQLHelper writer, string ReadOnlyConnectionString)
            : base(bdeName, DB, writer, ReadOnlyConnectionString)
        {
            //connect to backend
            m_Accounting = (AccountingBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("Accounting");
            InitEvaluator();
        }

        bool IDUsed(INTAPS.RDBMS.SQLHelper dsp, string IDstring, int Exculde)
        {
            int c = (int)dsp.ExecuteScalar("Select count(*) from " + this.DBName + ".dbo.Employee where employeeID='" + IDstring + "' and ticksTo=-1 and status="+((int)EmployeeStatus.Enrolled) +" and ID<>" + Exculde);
            return c > 0;
        }

        public INTAPS.RDBMS.SQLHelper CreateConnection(bool ReadOnly)
        {
            if (!ReadOnly)
                throw new Exception("Only readonly connection are supported.");
            return base.GetReaderHelper();
        }

        public AccountingBDE Accounting
        {
            get
            {
                return m_Accounting;
            }
        }
        public void SetEmployeeImage(int employeeID, long ticks,byte[] image)
        {
            lock (dspWriter)
            {
                Employee emp = GetEmployee(employeeID,ticks);
                if (emp == null)
                    throw new Exception("Empolyee not found in the database:" + employeeID);
                dspWriter.BeginTransaction();
                try
                {
                    if (emp.picture == -1)
                    {
                        if (image != null)
                        {
                            emp.picture = base.AddImage(-1, 1, "Employee " + emp.EmployeeNameID, image);
                            dspWriter.Update(base.DBName, "Employee", new string[] { "picture" }, new object[] { emp.picture }, "id=" + employeeID+" and ticksFrom="+ticks);
                        }
                    }
                    else
                    {
                        ImageItem img = new ImageItem();
                        img.imageID = emp.picture;
                        img.itemIndex = 1;
                        img.imageData = image;
                        img.title = "Employee " + emp.EmployeeNameID;
                        base.UpateImage(img);
                    }

                    dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    throw new Exception("Error saving employee picture", ex);
                }
            }
        }

    }
}