using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using System.Data;
using INTAPS.Evaluator;
namespace INTAPS.Payroll.BDE
{
    public partial class PayrollBDE
    {
        Dictionary<string,IFunction> m_payrollFunctions;
        public IFunction GetEvaluatorFunction(string symb)
        {
            if (m_payrollFunctions.ContainsKey(symb))
                return m_payrollFunctions[symb];
            return null;
        }
        public void AddEvaluatorFucntion(Type t)
        {
            IFunction func = (IFunction)t.GetConstructor(new Type[] { typeof(PayrollBDE) })
                .Invoke(new object[] { this });
            m_payrollFunctions.Add(func.Symbol.ToUpper(),func);
        }
        void InitEvaluator()
        {
            m_payrollFunctions=new Dictionary<string,IFunction>();
            AddEvaluatorFucntion(typeof(EFEmpAccount));
            AddEvaluatorFucntion(typeof(EFEmpSubAC));
            AddEvaluatorFucntion(typeof(EFSystemAccount));
            AddEvaluatorFucntion(typeof(EFGrossSalary));
            AddEvaluatorFucntion(typeof(EFIsPerm));
            AddEvaluatorFucntion(typeof(EFWorkingDays));
            AddEvaluatorFucntion(typeof(EFWorkingHours));
            AddEvaluatorFucntion(typeof(EFGetSystemParameter));
            AddEvaluatorFucntion(typeof(EFGetEmployeeDataField));
            AddEvaluatorFucntion(typeof(EFGetPCDDataField));
            INTAPS.Accounting.BDE.AFAccount f= new INTAPS.Accounting.BDE.AFAccount(m_Accounting);
            m_payrollFunctions.Add(f.Symbol.ToUpper(), f);
        }
    }
    public abstract class EFBase:IFunction
    {
        protected PayrollBDE m_payroll;
        public EFBase(PayrollBDE bde)
        {
            m_payroll = bde;
        }
        #region IFunction Members

        public virtual string Name
        {
            get { return this.Symbol;}
        }

        public abstract string Symbol { get;}

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public abstract int ParCount{get;}

        public abstract EData Evaluate(EData[] Pars);

        #endregion
    }
    public class EFEmpAccount:EFBase
    {
        
        public EFEmpAccount(PayrollBDE bde):base(bde)
        {
        }


        public override string Symbol
        {
            get { return "EmpAccount"; }
        }

        public override int ParCount
        {
            get { return 2; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            try
            {
                int eid = Convert.ToInt32(Pars[0].Value);
                string type = (string)Pars[1].Value;
                Employee e = m_payroll.GetEmployee(eid);
                int empControlAccountCode;
                switch (type.ToUpper())
                {
                    case "PAYROLL LIABILITY":
                        return new EData(DataType.Int,  m_payroll.GetEmployeeUncollectedPaymentAccount(eid));
                    case "PAYROLL EXPENSE":
                        
                        return new EData(DataType.Int, m_payroll.GetEmployeeSubAccount(e,m_payroll.GetPayrollExpenseAcount(e)).id);
                }
                if (int.TryParse(type, out empControlAccountCode))
                    return new EData(DataType.Int, m_payroll.GetEmployeeSubAccount(e, empControlAccountCode.ToString()).id);

                return new EData(DataType.Error, "Undefined acount.");
            }
            catch (Exception ex)
            {
                return new EData(DataType.Error, ex.Message);
            }
        }
    }
    public class EFSystemAccount : EFBase
    {

        public EFSystemAccount(PayrollBDE bde)
            : base(bde)
        {
        }


        public override string Symbol
        {
            get { return "SystemAccount"; }
        }

        public override int ParCount
        {
            get { return 1; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            int employeID = (int)Pars[0].Value;
            return new EData(DataType.Int, m_payroll.GetPayrollExpenseAcount(m_payroll.GetEmployee(employeID)));
        }
    }
    public class EFIsPerm : EFBase,IFormulaFunction
    {
        
        public EFIsPerm(PayrollBDE bde)
            : base(bde)
        {
        }

        public override string Symbol
        {
            get { return "IsEmpPerm"; }
        }

        public override int ParCount
        {
            get { return 1; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            int EmpID = Convert.ToInt32(Pars[0].Value);//int EmpID = m_payroll.GetEmployeeByID((string)Pars[0].Value).id;
            int prdID=(int)provider.GetData("PERIODID").Value;
            PayrollPeriod prd = m_payroll.GetPayPeriod(prdID);
            Employee emp = m_payroll.GetEmployee(EmpID, prd.toDate.Ticks);
            return new EData(DataType.Bool,emp.employmentType == EmploymentType.Permanent);
        }
        ISymbolProvider provider;
        public string[] Dependencies
        {
            get { return new string[] { "PERIODID" }; }
        }

        public ISymbolProvider ISymbolProvider
        {
            set { this.provider = value; }
        }

        public IVarParamCountFunction Clone()
        {
            return new EFIsPerm(base.m_payroll) { provider = this.provider };
        }

        public bool SetParCount(int n)
        {
            return n == this.ParCount;
        }
    }
    public class EFGrossSalary : EFBase,IFormulaFunction
    {

        public EFGrossSalary(PayrollBDE bde)
            : base(bde)
        {
        }


        public override string Symbol
        {
            get { return "EmpGross"; }
        }

        public override int ParCount
        {
            get { return 1; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            int EmpID = Convert.ToInt32(Pars[0].Value); //int EmpID = m_payroll.GetEmployeeByID((string)Pars[0].Value).id;
            return new EData(DataType.Float,
                m_payroll.GetEmployee(EmpID).grossSalary);
        }
        ISymbolProvider provider;
        public string[] Dependencies
        {
            get { return new string[] { "PERIODID" }; }
        }

        public ISymbolProvider ISymbolProvider
        {
            set { this.provider = value; }
        }

        public IVarParamCountFunction Clone()
        {
            return new EFGrossSalary(base.m_payroll) { provider = this.provider };
        }

        public bool SetParCount(int n)
        {
            return n == this.ParCount;
        }
    }
    public class EFWorkingDays:EFBase
    {

        public EFWorkingDays(PayrollBDE bde)
            : base(bde)
        {
        }


        public override string Symbol
        {
            get { return "WorkingDays"; }
        }

        public override int ParCount
        {
            get { return 1; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            int pid = (int)Pars[0].Value;
            PayrollPeriod pp = m_payroll.GetPayPeriod(pid);
            return new EData(DataType.Float,pp.days);
        }
    }
    public class EFWorkingHours : EFBase
    {

        public EFWorkingHours(PayrollBDE bde)
            : base(bde)
        {
        }


        public override string Symbol
        {
            get { return "WorkingHours"; }
        }

        public override int ParCount
        {
            get { return 1; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            int pid= (int)Pars[0].Value;
            PayrollPeriod pp = m_payroll.GetPayPeriod(pid);
            return new EData(DataType.Float, pp.hours);
        }
    }
    public class EFEmpSubAC : EFBase
    {

        public EFEmpSubAC(PayrollBDE bde)
            : base(bde)
        {
        }


        public override string Symbol
        {
            get { return "EmpSubAC"; }
        }

        public override int ParCount
        {
            get { return 2; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            try
            {
                int eid = Convert.ToInt32(Pars[0].Value);// m_payroll.GetEmployeeByID((string)Pars[0].Value).id;
                int control;
                if(Pars[1].Value is int)
                    control = (int)Pars[1].Value;
                else
                    control= m_payroll.Accounting.GetAccountID<Account>((string)Pars[1].Value);
                int acid = m_payroll.GetEmployeeSubAccount(eid, control);
                
                if(acid!=-1)
                    return new EData(DataType.Int, acid);
                return new EData(DataType.Error, "Undefined acount.");
            }
            catch (Exception ex)
            {
                return new EData(DataType.Error, ex.Message);
            }
        }
    }

    public class EFGetSystemParameter : EFBase
    {
        public EFGetSystemParameter(PayrollBDE bde)
            : base(bde)
        {
        }
        public override string Symbol
        {
            get { return "SysPar"; }
        }

        public override int ParCount
        {
            get { return 1; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            string parName = Pars[0].Value.ToString();
            object val=base.m_payroll.GetSystemParameters(new string[]{parName})[0];
            if(val is int)
                return new EData(DataType.Int,val);
            if (val is double)
                return new EData(DataType.Float, val);

            if(val is string)
                return new EData(DataType.Text,val);
            return new FSError("Invalid type").ToEData();
        }

    }

    public class EFGetEmployeeDataField : EFBase,IFormulaFunction
    {
        public EFGetEmployeeDataField(PayrollBDE bde)
            : base(bde)
        {
        }
        public  override string Symbol
        {
            get { return "EmpField"; }
        }

        public  override int ParCount
        {
            get { return 2; }
        }

        public override EData Evaluate( EData[] Pars)
        {
            int empID;
            if(Pars[0].Value is int)
                empID=(int)Pars[0].Value;
            else if(Pars[0].Value is double)
                empID=(int)(double)Pars[0].Value;
            else 
                return new FSError("First parameter for EmpField should be numeric employee ID").ToEData();
            string field = Pars[1].Value as string;
            if(string.IsNullOrEmpty(field))
                return new FSError("Field name is not provided for EmpField").ToEData();
            try
            {
                Employee emp = base.m_payroll.GetEmployee(empID);
                System.Reflection.FieldInfo fi=typeof(Employee).GetField(field);
                object obj;
                if (fi == null)
                {
                    System.Reflection.PropertyInfo pi = typeof(Employee).GetProperty(field);
                    if (pi == null)
                    {
                        return new FSError("Invalid field name-" + field).ToEData();
                    }
                    else
                        obj = pi.GetValue(emp,new object[0]);

                }
                else
                    obj=fi.GetValue(emp);
                return EDataUtility.createEData(obj);
            }
            catch (Exception ex)
            {
                return new FSError("Error evaluating EmpField-" + ex.Message).ToEData();
            }
        }
        #region IFunction Members


        public string Name
        {
             get { return "Employee Field"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion

        ISymbolProvider provider;
        public string[] Dependencies
        {
            get { return new string[] { "PERIODID" }; }
        }

        public ISymbolProvider ISymbolProvider
        {
            set { this.provider = value; }
        }

        public IVarParamCountFunction Clone()
        {
            return new EFGetEmployeeDataField(base.m_payroll) { provider = this.provider };
        }

        public bool SetParCount(int n)
        {
            return n == this.ParCount;
        }
    }

    public class EFGetPCDDataField : EFBase
    {
        public EFGetPCDDataField(PayrollBDE bde)
            : base(bde)
        {
        }
        public override string Symbol
        {
            get { return "PCDField"; }
        }

        public override int ParCount
        {
            get { return 5; }
        }

        public override EData Evaluate(EData[] Pars)
        {
            int empID;
            if (Pars[0].Value is int)
                empID = (int)Pars[0].Value;
            else if (Pars[0].Value is double)
                empID = (int)(double)Pars[0].Value;
            else
                return new FSError("First parameter for EmpField should be numeric employee ID").ToEData();

            int periodID;
            if (Pars[1].Value is int)
                periodID = (int)Pars[1].Value;
            else if (Pars[1].Value is double)
                periodID = (int)(double)Pars[1].Value;
            else
                return new FSError("Second parameter for periodID should be numeric employee ID").ToEData();

            int compID;
            if (Pars[2].Value is int)
                compID = (int)Pars[2].Value;
            else if (Pars[2].Value is double)
                compID = (int)(double)Pars[2].Value;
            else
                return new FSError("Second parameter for component ID should be numeric employee ID").ToEData();

            int formulaID;
            if (Pars[3].Value is int)
                formulaID = (int)Pars[3].Value;
            else if (Pars[3].Value is double)
                formulaID = (int)(double)Pars[3].Value;
            else
                return new FSError("Second parameter for formula ID should be numeric employee ID").ToEData();
            
            string field = Pars[4].Value as string;

            if (string.IsNullOrEmpty(field))
                return new FSError("Field name is not provided for EmpField").ToEData();
            try
            {
                PayrollComponentData[] data= base.m_payroll.GetPCData(compID,formulaID,AppliesToObjectType.Employee,empID,periodID,true);
                if (data.Length == 0)
                    return new EData(DataType.Empty, null);
                object o;
                base.m_payroll.GetPCData(data[0].ID, out o);
                System.Reflection.FieldInfo fi = o.GetType().GetField(field);
                object obj;
                if (fi == null)
                {
                    System.Reflection.PropertyInfo pi= o.GetType().GetProperty(field);
                    if(pi==null)
                        return new FSError("Invalid field name-" + field).ToEData();
                    obj = pi.GetValue(o,new object[0]);
                }
                else
                    obj = fi.GetValue(o);
                if (obj is int)
                    return new EData(DataType.Int, obj);
                if (obj is double)
                    return new EData(DataType.Float, obj);
                return new EData(DataType.Text, obj.ToString());
            }
            catch (Exception ex)
            {
                return new FSError("Error evaluating EmpField-" + ex.Message).ToEData();
            }
        }
        #region IFunction Members


        public string Name
        {
            get { return "Payroll Data Field"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion
    }
}


