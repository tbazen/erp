using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login l = new Login();
            l.TryLogin();
            if (l.logedin)
            {
                MainForm f = new MainForm();
                new INTAPS.UI.UIFormApplicationBase(f);
               Application.Run(f);
            }
        }

    }
}