﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client
{
    public class DefaultPayrollDocumentHandlerClient:INTAPS.Accounting.IGenericDocumentClientHandler
    {
        public bool CanEdit
        {
            get { return false; }
        }

        public object CreateEditor(bool embeded)
        {
            throw new NotImplementedException();
        }

        public Accounting.AccountDocument GetEditorDocument(object editor)
        {
            throw new NotImplementedException();
        }

        public void SetEditorDocument(object editor, Accounting.AccountDocument doc)
        {
            throw new NotImplementedException();
        }

        public bool VerifyUserEntry(object editor, out string error)
        {
            throw new NotImplementedException();
        }

        public void setAccountingClient(Accounting.IAccountingClient client)
        {
        }
    }
}
