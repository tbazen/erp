namespace INTAPS.Payroll.Client
{
    partial class EmplolyeeProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bowserController1 = new INTAPS.UI.HTML.BowserController();
            this.wbEmployee = new INTAPS.UI.HTML.ControlBrowser();
            this.SuspendLayout();
            // 
            // bowserController1
            // 
            this.bowserController1.Location = new System.Drawing.Point(0, 0);
            this.bowserController1.Name = "bowserController1";
            this.bowserController1.ShowBackForward = true;
            this.bowserController1.ShowRefresh = true;
            this.bowserController1.Size = new System.Drawing.Size(693, 25);
            this.bowserController1.TabIndex = 0;
            this.bowserController1.Text = "bowserController1";
            // 
            // wbEmployee
            // 
            this.wbEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbEmployee.Location = new System.Drawing.Point(0, 25);
            this.wbEmployee.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbEmployee.Name = "wbEmployee";
            this.wbEmployee.Size = new System.Drawing.Size(693, 377);
            this.wbEmployee.StyleSheetFile = null;
            this.wbEmployee.TabIndex = 1;
            // 
            // EmplolyeeProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 402);
            this.Controls.Add(this.wbEmployee);
            this.Controls.Add(this.bowserController1);
            this.Name = "EmplolyeeProfile";
            this.Text = "Emplolyee Profile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private INTAPS.UI.HTML.BowserController bowserController1;
        private INTAPS.UI.HTML.ControlBrowser wbEmployee;
    }
}