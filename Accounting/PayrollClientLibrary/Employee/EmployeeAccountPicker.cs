using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace INTAPS.Payroll.Client
{
    public partial class EmployeeAccountPicker : Form
    {
        public Account account;
        Employee m_emp;
        public EmployeeAccountPicker(Employee e)
        {
            InitializeComponent();
            m_emp = e;
            this.Text = "Pick account for " + m_emp.EmployeeNameID;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Account ac;
            if ((ac=accountPlaceHolder.account) == null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select an account");
                return;
            }
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Update employee through the main client");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}