namespace INTAPS.Payroll.Client
{
    partial class EmployeeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmployeeEditor));
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGrossSalary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblEnrolled = new System.Windows.Forms.Label();
            this.dtpEnrolled = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.lblDeactivated = new System.Windows.Forms.Label();
            this.dtpFiredOn = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.txtParentOrg = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.pageGeneral = new System.Windows.Forms.TabPage();
            this.chkPermanent = new System.Windows.Forms.CheckBox();
            this.pageAccount = new System.Windows.Forms.TabPage();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.lvAccounts = new System.Windows.Forms.ListView();
            this.colAcount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCredit = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colBalance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label5 = new System.Windows.Forms.Label();
            this.comboLoginName = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            this.pageAccount.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(87, 44);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(368, 20);
            this.txtName.TabIndex = 1;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(87, 70);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(367, 20);
            this.txtID.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ID#";
            // 
            // txtGrossSalary
            // 
            this.txtGrossSalary.Location = new System.Drawing.Point(89, 128);
            this.txtGrossSalary.Name = "txtGrossSalary";
            this.txtGrossSalary.Size = new System.Drawing.Size(366, 20);
            this.txtGrossSalary.TabIndex = 5;
            this.txtGrossSalary.TextChanged += new System.EventHandler(this.txtGrossSalary_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gross Salary";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(610, 322);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblEnrolled
            // 
            this.lblEnrolled.AutoSize = true;
            this.lblEnrolled.Location = new System.Drawing.Point(9, 187);
            this.lblEnrolled.Name = "lblEnrolled";
            this.lblEnrolled.Size = new System.Drawing.Size(72, 13);
            this.lblEnrolled.TabIndex = 8;
            this.lblEnrolled.Text = "Activated On:";
            // 
            // dtpEnrolled
            // 
            this.dtpEnrolled.Location = new System.Drawing.Point(89, 181);
            this.dtpEnrolled.Name = "dtpEnrolled";
            this.dtpEnrolled.Size = new System.Drawing.Size(182, 20);
            this.dtpEnrolled.Style = INTAPS.Ethiopic.ETDateStyle.Ethiopian;
            this.dtpEnrolled.TabIndex = 9;
            // 
            // lblDeactivated
            // 
            this.lblDeactivated.AutoSize = true;
            this.lblDeactivated.Location = new System.Drawing.Point(9, 213);
            this.lblDeactivated.Name = "lblDeactivated";
            this.lblDeactivated.Size = new System.Drawing.Size(85, 13);
            this.lblDeactivated.TabIndex = 8;
            this.lblDeactivated.Text = "Deactivated On:";
            // 
            // dtpFiredOn
            // 
            this.dtpFiredOn.Location = new System.Drawing.Point(100, 213);
            this.dtpFiredOn.Name = "dtpFiredOn";
            this.dtpFiredOn.Size = new System.Drawing.Size(182, 20);
            this.dtpFiredOn.Style = INTAPS.Ethiopic.ETDateStyle.Ethiopian;
            this.dtpFiredOn.TabIndex = 9;
            this.dtpFiredOn.Load += new System.EventHandler(this.dtpFiredOn_Load);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 157);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Status";
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "Not Activated",
            "Activated",
            "Deactivated"});
            this.cmbStatus.Location = new System.Drawing.Point(89, 154);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(214, 21);
            this.cmbStatus.TabIndex = 11;
            this.cmbStatus.SelectedIndexChanged += new System.EventHandler(this.cmbStatus_SelectedIndexChanged);
            // 
            // txtParentOrg
            // 
            this.txtParentOrg.AcceptsReturn = true;
            this.txtParentOrg.Location = new System.Drawing.Point(124, 11);
            this.txtParentOrg.Name = "txtParentOrg";
            this.txtParentOrg.ReadOnly = true;
            this.txtParentOrg.Size = new System.Drawing.Size(330, 20);
            this.txtParentOrg.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Organizational Unit";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(681, 322);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.pageGeneral);
            this.tabControl1.Controls.Add(this.pageAccount);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(750, 320);
            this.tabControl1.TabIndex = 14;
            // 
            // pageGeneral
            // 
            this.pageGeneral.Controls.Add(this.chkPermanent);
            this.pageGeneral.Controls.Add(this.label4);
            this.pageGeneral.Controls.Add(this.txtParentOrg);
            this.pageGeneral.Controls.Add(this.label1);
            this.pageGeneral.Controls.Add(this.txtName);
            this.pageGeneral.Controls.Add(this.comboLoginName);
            this.pageGeneral.Controls.Add(this.cmbStatus);
            this.pageGeneral.Controls.Add(this.label5);
            this.pageGeneral.Controls.Add(this.label2);
            this.pageGeneral.Controls.Add(this.label6);
            this.pageGeneral.Controls.Add(this.txtID);
            this.pageGeneral.Controls.Add(this.dtpFiredOn);
            this.pageGeneral.Controls.Add(this.label3);
            this.pageGeneral.Controls.Add(this.lblDeactivated);
            this.pageGeneral.Controls.Add(this.txtGrossSalary);
            this.pageGeneral.Controls.Add(this.dtpEnrolled);
            this.pageGeneral.Controls.Add(this.lblEnrolled);
            this.pageGeneral.Location = new System.Drawing.Point(4, 22);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(742, 294);
            this.pageGeneral.TabIndex = 0;
            this.pageGeneral.Text = "General";
            this.pageGeneral.UseVisualStyleBackColor = true;
            // 
            // chkPermanent
            // 
            this.chkPermanent.AutoSize = true;
            this.chkPermanent.Checked = true;
            this.chkPermanent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPermanent.Location = new System.Drawing.Point(11, 245);
            this.chkPermanent.Name = "chkPermanent";
            this.chkPermanent.Size = new System.Drawing.Size(126, 17);
            this.chkPermanent.TabIndex = 14;
            this.chkPermanent.Text = "Permanent Employee";
            this.chkPermanent.UseVisualStyleBackColor = true;
            // 
            // pageAccount
            // 
            this.pageAccount.Controls.Add(this.btnRemove);
            this.pageAccount.Controls.Add(this.btnRefresh);
            this.pageAccount.Controls.Add(this.btnAddEmployee);
            this.pageAccount.Controls.Add(this.lvAccounts);
            this.pageAccount.Location = new System.Drawing.Point(4, 22);
            this.pageAccount.Name = "pageAccount";
            this.pageAccount.Padding = new System.Windows.Forms.Padding(3);
            this.pageAccount.Size = new System.Drawing.Size(742, 294);
            this.pageAccount.TabIndex = 1;
            this.pageAccount.Text = "Accounts";
            this.pageAccount.UseVisualStyleBackColor = true;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(7, 13);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(41, 23);
            this.btnRemove.TabIndex = 16;
            this.btnRemove.Text = "-";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(681, 13);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(55, 23);
            this.btnRefresh.TabIndex = 17;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Location = new System.Drawing.Point(54, 13);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(41, 23);
            this.btnAddEmployee.TabIndex = 17;
            this.btnAddEmployee.Text = "+";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // lvAccounts
            // 
            this.lvAccounts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colAcount,
            this.colCredit,
            this.colBalance});
            this.lvAccounts.Location = new System.Drawing.Point(6, 42);
            this.lvAccounts.Name = "lvAccounts";
            this.lvAccounts.Size = new System.Drawing.Size(661, 246);
            this.lvAccounts.TabIndex = 2;
            this.lvAccounts.UseCompatibleStateImageBehavior = false;
            this.lvAccounts.View = System.Windows.Forms.View.Details;
            this.lvAccounts.SelectedIndexChanged += new System.EventHandler(this.lvAccounts_SelectedIndexChanged);
            // 
            // colAcount
            // 
            this.colAcount.Text = "Account";
            this.colAcount.Width = 191;
            // 
            // colCredit
            // 
            this.colCredit.Text = "Type";
            this.colCredit.Width = 218;
            // 
            // colBalance
            // 
            this.colBalance.Text = "Balance";
            this.colBalance.Width = 236;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Login Name:";
            // 
            // comboLoginName
            // 
            this.comboLoginName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboLoginName.FormattingEnabled = true;
            this.comboLoginName.Location = new System.Drawing.Point(87, 101);
            this.comboLoginName.Name = "comboLoginName";
            this.comboLoginName.Size = new System.Drawing.Size(214, 21);
            this.comboLoginName.TabIndex = 11;
            this.comboLoginName.SelectedIndexChanged += new System.EventHandler(this.cmbStatus_SelectedIndexChanged);
            // 
            // EmployeeEditor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(750, 355);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmployeeEditor";
            this.Text = "Employee Basic Information";
            this.tabControl1.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.pageGeneral.PerformLayout();
            this.pageAccount.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGrossSalary;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblEnrolled;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpEnrolled;
        private System.Windows.Forms.Label lblDeactivated;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpFiredOn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.TextBox txtParentOrg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage pageGeneral;
        private System.Windows.Forms.TabPage pageAccount;
        private System.Windows.Forms.ListView lvAccounts;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.ColumnHeader colAcount;
        private System.Windows.Forms.ColumnHeader colCredit;
        private System.Windows.Forms.ColumnHeader colBalance;
        private System.Windows.Forms.CheckBox chkPermanent;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ComboBox comboLoginName;
        private System.Windows.Forms.Label label5;
    }
}