using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Client;

namespace INTAPS.Payroll.Client
{
    public partial class EmployeeEditor : Form
    {
        public Employee FormData;
        bool New=false;
        INTAPS.UI.EnterKeyNavigation m_nav;
        private INTAPS.Accounting.Client.AccountContextMenu accountContextMenu;

        public EmployeeEditor()
        {
            InitializeComponent();
            lvAccounts.Columns[2].TextAlign = HorizontalAlignment.Right;
            m_nav = new INTAPS.UI.EnterKeyNavigation();
            m_nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtName));
            m_nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtID));
            m_nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtGrossSalary));
            m_nav.AddNavigableItem(new INTAPS.UI.ComboNavigator(cmbStatus));
            m_nav.AddNavigableItem(new INTAPS.UI.ButtonNavigator(btnOk));
            tabControl1.TabPages.Remove(pageAccount);
            comboLoginName.Items.Add("");
            comboLoginName.Items.AddRange(SecurityClient.GetAllUsers());
            InitConextMenu();
            SetPicture();
        }

        private void InitConextMenu()
        {
            this.accountContextMenu = new INTAPS.Accounting.Client.AccountContextMenu();

            this.lvAccounts.ContextMenuStrip = this.accountContextMenu;
            accountContextMenu.SetStyle(AccountContextMenuStyle.Edit | AccountContextMenuStyle.Ledger);

        }

        void SetAccount(int [] accounts)
        {
            lvAccounts.Items.Clear();
            foreach (int a in accounts)
            {
                try
                {
                    Account ac = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(a);
                    AddAccountToList(ac);
                }
                catch (Exception ex)
                {
                    ListViewItem li = new ListViewItem("Error: AccountID:"+a);
                    li.Tag = ex;
                    lvAccounts.Items.Add(li);
                }
            }
        }

        private void AddAccountToList(Account ac)
        {
            ListViewItem li = new ListViewItem(ac.CodeName);
            li.SubItems.Add(ac.DebitCreditName);
            li.SubItems.Add(AccountingClient.GetNetBalanceAsOf(ac.id,TransactionItem.DEFAULT_CURRENCY,DateTime.Now) .ToString("0,0.00"));
            lvAccounts.Items.Add(li);
            li.Tag = ac;
        }
        void SetParent(int PID)
        {
            if (PID == -1)
                txtParentOrg.Text = "";
            else
                txtParentOrg.Text = PayrollClient.GetOrgUnit(PID).Name;
        }
        public EmployeeEditor(Employee e)
            : this()
        {
            FormData = e;
            txtName.Text = e.employeeName;
            txtID.Text = e.employeeID;
            txtGrossSalary.Text = e.grossSalary == 0 ? "" : e.grossSalary.ToString("0,0.00");
            cmbStatus.SelectedIndex = (int)e.status;
            if(e.status!=EmployeeStatus.Pending)
                dtpEnrolled.Value = e.enrollmentDate;
            if(e.status==EmployeeStatus.Fired)
                dtpFiredOn.Value = e.dismissDate;
            chkPermanent.Checked = e.employmentType == EmploymentType.Permanent;
            SetParent(e.orgUnitID);
            SetAccount(e.accounts);
            SetPicture();
            tabControl1.TabPages.Add(pageAccount);
            if (string.IsNullOrEmpty(e.loginName))
                comboLoginName.SelectedIndex = 0;
            else
                comboLoginName.Text = e.loginName;
            New = false;
        }
        void SetPicture()
        {
           
        }
        public EmployeeEditor(int OrgID)
            : this()
        {
            FormData = new Employee();
            FormData.orgUnitID = OrgID;
            cmbStatus.SelectedIndex = 0;
            SetParent(OrgID);
            SetAccount(new int[0]);
            SetPicture();
            New = true;
        }
        void UpdateStatusDatePickers()
        {
            EmployeeStatus s = (EmployeeStatus)cmbStatus.SelectedIndex;
            lblEnrolled.Visible= dtpEnrolled.Visible = s != EmployeeStatus.Pending;
            lblDeactivated.Visible= dtpFiredOn.Visible = s == EmployeeStatus.Fired;

        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            string EmpName = txtName.Text.Trim();
            if (string.IsNullOrEmpty(EmpName))
            {
                txtName.SelectAll();
                txtName.Focus();
                UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter employee name.");
                return;
            }
            string EmpID= txtID.Text.Trim();
            if (string.IsNullOrEmpty(EmpID))
            {
                txtID.SelectAll();
                txtID.Focus();
                UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter employee ID.");
                return;
            }
            double s;
            if (txtGrossSalary.Text.Trim() == "")
                s = 0;
            else
            {
                if (!double.TryParse(txtGrossSalary.Text.Trim(), out s) || s < 0)
                {
                    txtGrossSalary.SelectAll();
                    txtGrossSalary.Focus();
                    UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Plase enter valid gross salary.");
                    return;
                }
            }
            Employee emp = this.FormData.Clone() as Employee;
            emp.employeeName = EmpName;
            emp.employeeID = EmpID;
            emp.grossSalary = s;
            emp.status = (EmployeeStatus)cmbStatus.SelectedIndex;
            emp.enrollmentDate = dtpEnrolled.Value;
            emp.dismissDate = dtpFiredOn.Value;
            emp.employmentType = chkPermanent.Checked ? EmploymentType.Permanent : EmploymentType.Temporary;
            emp.accounts=new int[lvAccounts.Items.Count];
            emp.loginName = comboLoginName.Text;
            for(int i=0;i<emp.accounts.Length;i++)
            {
                Account ac = lvAccounts.Items[i].Tag as Account;
                if (ac == null)
                {
                    lvAccounts.Items[i].Selected = true;
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Can't save with error account data.");
                    return;
                }
                emp.accounts[i] = ac.ObjectIDVal;
            }
            try
            {
                if (New)
                {
                    emp.ObjectIDVal = PayrollClient.SaveEmployeeData(emp);
                    New = false;
                }
                else
                {
                    PayrollClient.UpdateEmployeeData(emp);
                    this.FormData = emp;
                }
                
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Employee data saved successfully.");
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save organizational unit.", ex);
            }

        }

        private void dtpFiredOn_Load(object sender, EventArgs e)
        {

        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateStatusDatePickers();   
        }

        private void txtGrossSalary_TextChanged(object sender, EventArgs e)
        {

        }


        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            EmployeeAccountPicker ap = new EmployeeAccountPicker(this.FormData);
            //ChartOfAccount<INTAPS.Accounting.IAccountingService> ap = new ChartOfAccount<INTAPS.Accounting.IAccountingService>();
            if (ap.ShowDialog() == DialogResult.OK)
            {
                foreach (ListViewItem li in lvAccounts.Items)
                {
                    Account ac = li.Tag as Account;
                    if (ac == null || ac.ObjectIDVal == ap.account.ObjectIDVal)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Account allready added.");
                        return;
                    }
                }
                AddAccountToList(ap.account);
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int c = lvAccounts.SelectedItems.Count;
            for (int i = c - 1; i >= 0; i--)
            {
                lvAccounts.SelectedItems[i].Remove();
            }
        }

        private void lvAccounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvAccounts.SelectedItems.Count == 0)
                accountContextMenu.SetAccount(null);
            else
                accountContextMenu.SetAccount(lvAccounts.SelectedItems[0].Tag as Account);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //SetAccount(FormData.Accounts);
            foreach (ListViewItem li in lvAccounts.Items)
            {
                Account ac = (Account)li.Tag;
                ac = AccountingClient.GetAccount<Account>(ac.ObjectIDVal);
                li.Text=ac.CodeName;
                li.SubItems[1].Text=ac.DebitCreditName;
                li.SubItems[2].Text = INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(ac.ObjectIDVal, TransactionItem.DEFAULT_CURRENCY,DateTime.Now).ToString("0,0.00");
                li.Tag = ac;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}