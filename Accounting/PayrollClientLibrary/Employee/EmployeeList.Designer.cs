namespace INTAPS.Payroll.Client
{
    partial class EmployeeList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvEmployees = new System.Windows.Forms.ListView();
            this.colID = new System.Windows.Forms.ColumnHeader();
            this.colName = new System.Windows.Forms.ColumnHeader();
            this.colSalary = new System.Windows.Forms.ColumnHeader();
            this.colUnit = new System.Windows.Forms.ColumnHeader();
            this.colType = new System.Windows.Forms.ColumnHeader();
            this.colLoan = new System.Windows.Forms.ColumnHeader();
            this.employeeContextMenu = new INTAPS.Payroll.Client.Utility.EmployeeContextMenu();
            this.SuspendLayout();
            // 
            // lvEmployees
            // 
            this.lvEmployees.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colName,
            this.colSalary,
            this.colUnit,
            this.colType,
            this.colLoan});
            this.lvEmployees.ContextMenuStrip = this.employeeContextMenu;
            this.lvEmployees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvEmployees.FullRowSelect = true;
            this.lvEmployees.Location = new System.Drawing.Point(0, 0);
            this.lvEmployees.MultiSelect = false;
            this.lvEmployees.Name = "lvEmployees";
            this.lvEmployees.Size = new System.Drawing.Size(559, 347);
            this.lvEmployees.TabIndex = 0;
            this.lvEmployees.UseCompatibleStateImageBehavior = false;
            this.lvEmployees.View = System.Windows.Forms.View.Details;
            this.lvEmployees.SelectedIndexChanged += new System.EventHandler(this.lvEmployees_SelectedIndexChanged);
            // 
            // colID
            // 
            this.colID.Text = "ID";
            this.colID.Width = 163;
            // 
            // colName
            // 
            this.colName.Text = "Name";
            this.colName.Width = 139;
            // 
            // colSalary
            // 
            this.colSalary.Text = "Gross salary";
            this.colSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colSalary.Width = 114;
            // 
            // colUnit
            // 
            this.colUnit.Text = "Unit";
            this.colUnit.Width = 109;
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 111;
            // 
            // colLoan
            // 
            this.colLoan.Text = "Loan";
            this.colLoan.Width = 214;
            // 
            // employeeContextMenu
            // 
            this.employeeContextMenu.Name = "employeeContextMenu";
            this.employeeContextMenu.Size = new System.Drawing.Size(290, 114);
            // 
            // EmployeeList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvEmployees);
            this.Name = "EmployeeList";
            this.Size = new System.Drawing.Size(559, 347);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvEmployees;
        private System.Windows.Forms.ColumnHeader colID;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colLoan;
        private System.Windows.Forms.ColumnHeader colSalary;
        private INTAPS.Payroll.Client.Utility.EmployeeContextMenu employeeContextMenu;
        private System.Windows.Forms.ColumnHeader colUnit;

    }
}