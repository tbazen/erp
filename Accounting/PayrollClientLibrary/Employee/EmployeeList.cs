using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace INTAPS.Payroll.Client
{
    public partial class EmployeeList : UserControl,IPayrollContentForm
    {
        public event EventHandler EmployeeSelected;
        Employee []m_emps=null;
        OrgUnit m_org = null;
        bool m_ExInfo = true;
        DateTime _dataDate = DateTime.Now;

        public DateTime DataDate
        {
            get { return _dataDate; }
            set { _dataDate = value; }
        }
        public bool ExInfo
        {
            get 
            { 
                return m_ExInfo; 
            }
            set 
            { 
                m_ExInfo = value; 
            }
        }
        public EmployeeList()
        {
            InitializeComponent();
        }
        private void LoadEmployees()
        {
            Employee[] es;
            if (m_emps == null)
                es = PayrollClient.GetEmployees(m_org.ObjectIDVal, true);
            else
                es = m_emps;
            Array.Sort(es, new EmployeeComparer(EmployeeField.EmployeeID));

            lvEmployees.Items.Clear();
            foreach (Employee e in es)
            {
                ListViewItem li = new ListViewItem();
                SetEmployeeItem(e, li);
                lvEmployees.Items.Add(li);
            }
            MainForm.SetStatus(es.Length + " employees listed.");
            PayrollClient.EmployeeCreated += new EmployeeCreatedHandler(PayrollClient_EmployeeCreated);
            PayrollClient.EmployeeDeleted += new EmployeeDeletedHandler(PayrollClient_EmployeeDeleted);
            PayrollClient.EmployeeUpdated += new EmployeeUpdatedHandler(PayrollClient_EmployeeUpdated);
        }

        private void SetEmployeeItem(Employee e, ListViewItem li)
        {
            li.Text = e.employeeID;
            li.Tag = e;
            li.SubItems.Add(e.employeeName);
            li.SubItems.Add(e.grossSalary.ToString("0,0.00"));
            if (m_ExInfo)
            {
                if (e.orgUnitID != -1)

                    try
                    {
                        li.SubItems.Add(PayrollClient.GetOrgUnit(e.orgUnitID).Name);
                    }
                    catch
                    {
                        li.SubItems.Add("Error");
                    }
                else
                {
                    li.SubItems.Add("");
                }
            }
            else
                li.SubItems.Add("");
            li.SubItems.Add(e.employmentType == EmploymentType.Permanent ? "Permanent" : "Temporary");
            if (m_ExInfo)
            {
                li.SubItems.Add("error");
            }
            else
                li.SubItems.Add("");
        }

        void PayrollClient_EmployeeUpdated(Employee e)
        {
            foreach (ListViewItem li in lvEmployees.Items)
            {
                Employee emp = (Employee)li.Tag;
                if (emp.ObjectIDVal == e.ObjectIDVal)
                {
                    li.SubItems.Clear();
                    SetEmployeeItem(e, li);
                    if (li.Selected)
                        lvEmployees_SelectedIndexChanged(null, null);
                    break;
                }
            }
        }

        void PayrollClient_EmployeeDeleted(int ID)
        {
            foreach (ListViewItem li in lvEmployees.Items)
            {
                Employee emp = (Employee)li.Tag;
                if (emp.ObjectIDVal == ID)
                {
                    li.Remove();
                    break;
                }
            }
        }

        void PayrollClient_EmployeeCreated(Employee e)
        {
            if(m_org==null)
                return;
            if (PayrollClient.EmployeeUnder(e.ObjectIDVal, m_org.ObjectIDVal))
            {
                ListViewItem li = new ListViewItem();
                SetEmployeeItem(e, li);
                lvEmployees.Items.Add(li);
            }
        }
        #region ISpreadContentForm Members

        public string GetStandardCommandText(MainFormStandardButtons cmd)
        {
            switch (cmd)
            {
                case MainFormStandardButtons.Refresh:
                    return "Refresh";
            }
            return "";
        }

        public bool SupportStandardCommand(MainFormStandardButtons cmd)
        {
            switch (cmd)
            {
                case MainFormStandardButtons.Refresh:
                    return true;
            }
            return false;
        }

        public void DoStandardCommand(MainFormStandardButtons cmd)
        {
            switch (cmd)
            {
                case MainFormStandardButtons.Refresh:
                    LoadEmployees();
                    break;
            }
        }



        public bool ShowEmbeded
        {
            get { return true; }
        }

        public void FilterByEmp(Employee e, OrgUnit parent)
        {
            
        }
        public void ShowList(Employee[] res)
        {
            m_emps = res;
            LoadEmployees();
        }

        public void FilterByOrg(OrgUnit o)
        {
            m_org = o;
            m_emps = null;
            LoadEmployees();
        }

        public void FilterByPCD(PayrollComponentDefination pcd)
        {
        }

        public void FilterByFormula(PayrollComponentFormula formula, PayrollComponentDefination parent)
        {
        }

        public void FilterByPayPeriod(PayrollPeriod pp)
        {
        }

        public Control GetControl()
        {
            return this;
        }

        #endregion

        private void lvEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvEmployees.SelectedItems.Count == 0)
                employeeContextMenu.SetEmployee(null,this._dataDate);
            else
            {
                employeeContextMenu.SetEmployee((Employee)lvEmployees.SelectedItems[0].Tag,this._dataDate);
                if (EmployeeSelected != null)
                    EmployeeSelected(this, null);
            }
        }
        public Employee SelectedEmployee
        {
            get
            {
                if (lvEmployees.SelectedItems.Count == 0)
                    return null;
                else
                    return (Employee)lvEmployees.SelectedItems[0].Tag;
            }
        }               
                

        #region ISpreadContentForm Members


        public void Close()
        {
        }

        #endregion
    }
}