using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class EmployeePicker : Form
    {
        public Employee PickedEmployee = null;
        public OrgUnit PickedOrgUnit = null;
        bool OnlyEmployee;
        bool OnlyOrg;
        public EmployeePicker()
        {
            InitializeComponent();
            OnlyEmployee = true;
            OnlyOrg = true;
        }
        public DialogResult PickOrgUnitItem(bool emp,bool org)
        {
            orgTree.IncludeEmployees = emp;
            OnlyEmployee = emp;
            OnlyOrg = org;
            this.LoadData();
            return this.ShowDialog();
        }

        public void LoadData()
        {
            orgTree.LoadData(DateTime.Now);
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            PickedEmployee = null;
            PickedOrgUnit = null;
            if (orgTree.SelectedEmployee != null && OnlyEmployee)
                PickedEmployee = orgTree.SelectedEmployee;
            else if (orgTree.SelectedOrg != null && OnlyOrg)
                PickedOrgUnit = orgTree.SelectedOrg;
            else
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select an item.");
                return;
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}