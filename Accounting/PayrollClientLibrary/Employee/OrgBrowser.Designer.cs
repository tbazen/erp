namespace INTAPS.Payroll.Client
{
    partial class OrgBrowser
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ctxtOrgUnit = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miRootOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miChildOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditOrg = new System.Windows.Forms.ToolStripMenuItem();
            this.miRegEmp = new System.Windows.Forms.ToolStripMenuItem();
            this.miORefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxtEmployee = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miEditEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miEnrollEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miFireEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miEGroupUnder = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSeach = new System.Windows.Forms.Panel();
            this.btnClearSearch = new System.Windows.Forms.Button();
            this.textSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tvOrganization = new INTAPS.Payroll.Client.OrgTree();
            this.employeeList = new INTAPS.Payroll.Client.EmployeeList();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctxtOrgUnit.SuspendLayout();
            this.ctxtEmployee.SuspendLayout();
            this.panelSeach.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctxtOrgUnit
            // 
            this.ctxtOrgUnit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miRootOrg,
            this.miChildOrg,
            this.miDelOrg,
            this.miEditOrg,
            this.miRegEmp,
            this.miORefresh,
            this.refreshToolStripMenuItem});
            this.ctxtOrgUnit.Name = "ctxtTree";
            this.ctxtOrgUnit.Size = new System.Drawing.Size(290, 158);
            this.ctxtOrgUnit.Opening += new System.ComponentModel.CancelEventHandler(this.ctxtTree_Opening);
            // 
            // miRootOrg
            // 
            this.miRootOrg.Name = "miRootOrg";
            this.miRootOrg.Size = new System.Drawing.Size(289, 22);
            this.miRootOrg.Text = "New Root Organizational Unit";
            this.miRootOrg.Click += new System.EventHandler(this.miRootOrg_Click);
            // 
            // miChildOrg
            // 
            this.miChildOrg.Name = "miChildOrg";
            this.miChildOrg.Size = new System.Drawing.Size(289, 22);
            this.miChildOrg.Text = "Create Child Organizational Unit";
            this.miChildOrg.Click += new System.EventHandler(this.miChildOrg_Click);
            // 
            // miDelOrg
            // 
            this.miDelOrg.Name = "miDelOrg";
            this.miDelOrg.Size = new System.Drawing.Size(289, 22);
            this.miDelOrg.Text = "Delete Organizational Unit";
            this.miDelOrg.Click += new System.EventHandler(this.miDelOrg_Click);
            // 
            // miEditOrg
            // 
            this.miEditOrg.Name = "miEditOrg";
            this.miEditOrg.Size = new System.Drawing.Size(289, 22);
            this.miEditOrg.Text = "Edit Organiational Unit Information";
            this.miEditOrg.Click += new System.EventHandler(this.miEditOrg_Click);
            // 
            // miRegEmp
            // 
            this.miRegEmp.Name = "miRegEmp";
            this.miRegEmp.Size = new System.Drawing.Size(289, 22);
            this.miRegEmp.Text = "Register Employee";
            this.miRegEmp.Click += new System.EventHandler(this.miRegEmp_Click);
            // 
            // miORefresh
            // 
            this.miORefresh.Name = "miORefresh";
            this.miORefresh.Size = new System.Drawing.Size(289, 22);
            this.miORefresh.Text = "Group Under Different Organizatinal Unit";
            this.miORefresh.Click += new System.EventHandler(this.miOGroupUnder_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // ctxtEmployee
            // 
            this.ctxtEmployee.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEditEmployee,
            this.miEnrollEmployee,
            this.miFireEmployee,
            this.miDeleteEmployee,
            this.miEGroupUnder});
            this.ctxtEmployee.Name = "ctxtTree";
            this.ctxtEmployee.Size = new System.Drawing.Size(290, 114);
            this.ctxtEmployee.Opening += new System.ComponentModel.CancelEventHandler(this.ctxtEmployee_Opening);
            // 
            // miEditEmployee
            // 
            this.miEditEmployee.Name = "miEditEmployee";
            this.miEditEmployee.Size = new System.Drawing.Size(289, 22);
            this.miEditEmployee.Text = "Edit Employee Profile";
            this.miEditEmployee.Click += new System.EventHandler(this.miEditEmployee_Click);
            // 
            // miEnrollEmployee
            // 
            this.miEnrollEmployee.Name = "miEnrollEmployee";
            this.miEnrollEmployee.Size = new System.Drawing.Size(289, 22);
            this.miEnrollEmployee.Text = "Enroll Employee";
            this.miEnrollEmployee.Click += new System.EventHandler(this.miEnrollEmployee_Click);
            // 
            // miFireEmployee
            // 
            this.miFireEmployee.Name = "miFireEmployee";
            this.miFireEmployee.Size = new System.Drawing.Size(289, 22);
            this.miFireEmployee.Text = "Fire Employee";
            this.miFireEmployee.Click += new System.EventHandler(this.miFireEmployee_Click);
            // 
            // miDeleteEmployee
            // 
            this.miDeleteEmployee.Name = "miDeleteEmployee";
            this.miDeleteEmployee.Size = new System.Drawing.Size(289, 22);
            this.miDeleteEmployee.Text = "Delete Employee";
            this.miDeleteEmployee.Click += new System.EventHandler(this.miDeleteEmployee_Click);
            // 
            // miEGroupUnder
            // 
            this.miEGroupUnder.Name = "miEGroupUnder";
            this.miEGroupUnder.Size = new System.Drawing.Size(289, 22);
            this.miEGroupUnder.Text = "Group Under Different Organizatinal Unit";
            this.miEGroupUnder.Click += new System.EventHandler(this.miEGroupUnder_Click);
            // 
            // panelSeach
            // 
            this.panelSeach.Controls.Add(this.btnClearSearch);
            this.panelSeach.Controls.Add(this.textSearch);
            this.panelSeach.Controls.Add(this.label1);
            this.panelSeach.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSeach.Location = new System.Drawing.Point(0, 0);
            this.panelSeach.Name = "panelSeach";
            this.panelSeach.Size = new System.Drawing.Size(531, 36);
            this.panelSeach.TabIndex = 2;
            // 
            // btnClearSearch
            // 
            this.btnClearSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnClearSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearSearch.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearSearch.ForeColor = System.Drawing.Color.White;
            this.btnClearSearch.Location = new System.Drawing.Point(460, 3);
            this.btnClearSearch.Name = "btnClearSearch";
            this.btnClearSearch.Size = new System.Drawing.Size(68, 28);
            this.btnClearSearch.TabIndex = 2;
            this.btnClearSearch.Text = "Clear ";
            this.btnClearSearch.UseVisualStyleBackColor = false;
            this.btnClearSearch.Click += new System.EventHandler(this.button1_Click);
            // 
            // textSearch
            // 
            this.textSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textSearch.Location = new System.Drawing.Point(54, 11);
            this.textSearch.Name = "textSearch";
            this.textSearch.Size = new System.Drawing.Size(401, 20);
            this.textSearch.TabIndex = 1;
            this.textSearch.TextChanged += new System.EventHandler(this.textSearch_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Search";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tvOrganization
            // 
            this.tvOrganization.HideSelection = false;
            this.tvOrganization.ImageIndex = 0;
            this.tvOrganization.IncludeEmployees = true;
            this.tvOrganization.Location = new System.Drawing.Point(273, 82);
            this.tvOrganization.Name = "tvOrganization";
            this.tvOrganization.SelectedImageIndex = 0;
            this.tvOrganization.Size = new System.Drawing.Size(225, 211);
            this.tvOrganization.TabIndex = 1;
            this.tvOrganization.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvEmployee_AfterSelect);
            this.tvOrganization.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvOrganization_KeyDown);
            this.tvOrganization.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvOrganization_MouseDown);
            // 
            // employeeList
            // 
            this.employeeList.ExInfo = false;
            this.employeeList.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.employeeList.Location = new System.Drawing.Point(16, 82);
            this.employeeList.Name = "employeeList";
            this.employeeList.Size = new System.Drawing.Size(251, 211);
            this.employeeList.TabIndex = 3;
            this.employeeList.EmployeeSelected += new System.EventHandler(this.employeeList_EmployeeSelected);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ID#";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Salary";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // OrgBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tvOrganization);
            this.Controls.Add(this.employeeList);
            this.Controls.Add(this.panelSeach);
            this.Name = "OrgBrowser";
            this.Size = new System.Drawing.Size(531, 350);
            this.ctxtOrgUnit.ResumeLayout(false);
            this.ctxtEmployee.ResumeLayout(false);
            this.panelSeach.ResumeLayout(false);
            this.panelSeach.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private INTAPS.Payroll.Client.OrgTree tvOrganization;
        private System.Windows.Forms.ContextMenuStrip ctxtOrgUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.ToolStripMenuItem miRootOrg;
        private System.Windows.Forms.ToolStripMenuItem miChildOrg;
        private System.Windows.Forms.ToolStripMenuItem miDelOrg;
        private System.Windows.Forms.ToolStripMenuItem miEditOrg;
        private System.Windows.Forms.ToolStripMenuItem miRegEmp;
        private System.Windows.Forms.ContextMenuStrip ctxtEmployee;
        private System.Windows.Forms.ToolStripMenuItem miEditEmployee;
        private System.Windows.Forms.ToolStripMenuItem miEnrollEmployee;
        private System.Windows.Forms.ToolStripMenuItem miFireEmployee;
        private System.Windows.Forms.ToolStripMenuItem miDeleteEmployee;
        private System.Windows.Forms.ToolStripMenuItem miORefresh;
        private System.Windows.Forms.ToolStripMenuItem miEGroupUnder;
        private System.Windows.Forms.Panel panelSeach;
        private System.Windows.Forms.TextBox textSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClearSearch;
        private System.Windows.Forms.Timer timer1;
        private EmployeeList employeeList;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
    }
}
