using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace INTAPS.Payroll.Client
{
    public enum OrgTreeMode
    {
        Tree,
        List
    }
    public delegate void EmployeeSelectedHandler(object source,Employee Selection);
    public delegate void OrgUnitSelectedHandler(object source, OrgUnit Selection);
    public partial class OrgBrowser : UserControl
    {
        public event EmployeeSelectedHandler EmployeeSelected;
        public event OrgUnitSelectedHandler OrgUnitSelected;
        Thread m_thread=null;
        OrgTreeMode m_Mode;
        Employee [] m_result=null;
        public bool showFired { get; set; }
        DateTime _dateDate=DateTime.Now;
        public DateTime dateDate
        {
            get
            {
                return _dateDate;
            }
            set
            {
                this._dateDate = value;
            }
        }
        void Search(object q)
        {
            int N;
            m_result = PayrollClient.SearchEmployee(_dateDate.Ticks, -1, (string)q, true, EmployeeSearchType.All, 0, 20, out N);
        }  
        public OrgTreeMode ViewMode
        {
            get 
            { 
                return m_Mode; 
            }
        }
        public OrgBrowser()
        {
            InitializeComponent();

            tvOrganization.Dock = DockStyle.Fill;
            employeeList.Dock = DockStyle.Fill;
            employeeList.Visible = false;
            m_Mode = OrgTreeMode.Tree;
            textSearch.Modified = false;
            btnClearSearch.Enabled = false;
        }
        public OrgTree Tree
        {
            get
            {
                return tvOrganization;
            }
        }
        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tvEmployee_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tvOrganization.SelectedEmployee != null)
            {
                if (EmployeeSelected != null)
                    EmployeeSelected(this, tvOrganization.SelectedEmployee);
            }
            else if (tvOrganization.SelectedOrg != null)
            {
                if (OrgUnitSelected != null)
                    OrgUnitSelected(this, tvOrganization.SelectedOrg);
            }
        }

        private void lvEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
        public void LoadData(DateTime date)
        {
            this._dateDate = date;
            tvOrganization.LoadData(date);
            employeeList.DataDate = date;
        }

        private void miRegEmp_Click(object sender, EventArgs e)
        {
            EmployeeEditor ep = new EmployeeEditor(tvOrganization.SelectedOrg.ObjectIDVal);
            if (ep.ShowDialog() == DialogResult.OK)
            {
                tvOrganization.AddEmployee(ep.FormData);
            }
        }

        private void ctxtTree_Opening(object sender, CancelEventArgs e)
        {
            bool ItemSelected = tvOrganization.SelectedNode != null;
            miChildOrg.Enabled = ItemSelected ;
            miDelOrg.Enabled = ItemSelected;
            miEditOrg.Enabled = ItemSelected;
            miRegEmp.Enabled = ItemSelected;
            miORefresh.Enabled = ItemSelected;
        }

        private void miRootOrg_Click(object sender, EventArgs e)
        {
            OrgUnitProfile o = new OrgUnitProfile(-1);
            if (o.ShowDialog() == DialogResult.OK)
            {
                tvOrganization.AddOrgUnit(o.FormData);
            }
        }

        private void miEditOrg_Click(object sender, EventArgs e)
        {
            OrgUnitProfile o = new OrgUnitProfile((OrgUnit)tvOrganization.SelectedOrg.Clone());
            if (o.ShowDialog() == DialogResult.OK)
            {
                tvOrganization.UpdateOrgUnit(o.FormData);
            }
        }

        private void miChildOrg_Click(object sender, EventArgs e)
        {
            OrgUnitProfile o = new OrgUnitProfile(tvOrganization.SelectedOrg.ObjectIDVal);
            if (o.ShowDialog() == DialogResult.OK)
            {
                tvOrganization.AddOrgUnit(o.FormData);
            }
        }

        private void miDelOrg_Click(object sender, EventArgs e)
        {
            if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this organizational unit from database?"))
            {
                try
                {
                    PayrollClient.DeleteOrgUnit(tvOrganization.SelectedOrg.ObjectIDVal);
                    tvOrganization.DeleteOrgUnit(tvOrganization.SelectedOrg.ObjectIDVal);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete organizational unit.", ex);
                }
            }

        }

        private void tvOrganization_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                tvOrganization.SelectedNode=tvOrganization.HitTest(tvOrganization.PointToClient(Cursor.Position)).Node;
                ShowContextMenu();
            }
        }

        private void ShowContextMenu()
        {
            Point p;
            if (tvOrganization.SelectedNode == null)
                p = tvOrganization.PointToClient(Cursor.Position);
            else
                p = new Point(tvOrganization.SelectedNode.Bounds.Left, tvOrganization.SelectedNode.Bounds.Bottom);
            if (tvOrganization.SelectedEmployee != null)
                ctxtEmployee.Show(tvOrganization, p);
            else
                ctxtOrgUnit.Show(tvOrganization, p);
        }

        private void tvOrganization_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Apps)
            {
                ShowContextMenu();
            }
        }

        private void ctxtEmployee_Opening(object sender, CancelEventArgs e)
        {
            miFireEmployee.Enabled = tvOrganization.SelectedEmployee.status == EmployeeStatus.Enrolled;
            miEnrollEmployee.Enabled = tvOrganization.SelectedEmployee.status != EmployeeStatus.Enrolled;
        }

        private void miEditEmployee_Click(object sender, EventArgs e)
        {
            EmployeeEditor ep = new EmployeeEditor((Employee)tvOrganization.SelectedEmployee.Clone());
            if (ep.ShowDialog() == DialogResult.OK)
            {
                tvOrganization.UpdateEmployee(ep.FormData);
            }
        }

        private void miDeleteEmployee_Click(object sender, EventArgs e)
        {
            if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this employee from database?"))
            {
                try
                {
                    PayrollClient.DeleteEmployee(tvOrganization.SelectedEmployee.ObjectIDVal);
                    tvOrganization.DeleteEmployee(tvOrganization.SelectedEmployee.ObjectIDVal);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete employee from database.", ex);
                }
            }


        }
        public Employee SelectedEmployee
        {
            get
            {
                if (m_Mode == OrgTreeMode.Tree)
                    return tvOrganization.SelectedEmployee;
                return employeeList.SelectedEmployee;
            }
        }
        public OrgUnit SelectedOrg
        {
            get
            {
                if (m_Mode == OrgTreeMode.List)
                    return null;
                return tvOrganization.SelectedOrg;
            }
        }
        string ToNodeKey(TreeNode node)
        {
            if (node.Tag is Employee)
                return "E_" + ((Employee)node.Tag).ObjectIDVal;
            return "O_" + ((OrgUnit)node.Tag).ObjectIDVal;

        }  
        public void LoadState()
        {
                        INTAPS.UI.UIStateHandler.LoadTreeState(
INTAPS.UI.UIFormApplicationBase.CurrentAppliation.GetSetting("Payroll.OrgTree", null)
, tvOrganization,new INTAPS.UI.TreeNodeToString(ToNodeKey));
        }
        public void SaveState()
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.SetSetting("Payroll.OrgTree"
, INTAPS.UI.UIStateHandler.GetTreeState(tvOrganization, new INTAPS.UI.TreeNodeToString(ToNodeKey)));

        }

        private void miEGroupUnder_Click(object sender, EventArgs e)
        {
            EmployeePicker picker = new EmployeePicker();
            if (picker.PickOrgUnitItem(false, true) == DialogResult.OK)
            {
                try
                {
                    Employee emp = tvOrganization.SelectedEmployee;
                    PayrollClient.ChangeEmployeeOrgUnit(emp.ObjectIDVal, picker.PickedOrgUnit.ObjectIDVal,_dateDate);
                    emp.orgUnitID = picker.PickedOrgUnit.ObjectIDVal;
                    tvOrganization.DeleteEmployee(emp.ObjectIDVal);
                    tvOrganization.AddEmployee(emp);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Operation not succesfull.", ex);
                }
            }
        }

        private void miOGroupUnder_Click(object sender, EventArgs e)
        {
            EmployeePicker picker = new EmployeePicker();
            if (picker.PickOrgUnitItem(false,true) == DialogResult.OK)
            {
                try
                {
                    OrgUnit org= tvOrganization.SelectedOrg;
                    org.PID = picker.PickedOrgUnit.ObjectIDVal;
                    PayrollClient.UpdateOrgUnit(org);
                    tvOrganization.DeleteOrgUnit(org.ObjectIDVal);
                    tvOrganization.AddOrgUnit(org);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Operation not succesfull.", ex);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (m_thread != null && !m_thread.IsAlive)
            {
                LoadSearchResult();
                if (textSearch.Modified)
                {
                    UpdateSearchResult();
                }
                else
                    timer1.Enabled = false;
            }
        }

        private void LoadSearchResult()
        {
            if(m_result!=null)
                employeeList.ShowList(m_result);
        }

        private void textSearch_TextChanged(object sender, EventArgs e)
        {
            UpdateSearchResult();
        }

        private void UpdateSearchResult()
        {
            if (textSearch.Modified)
            {
                textSearch.Modified = false;
                string q = textSearch.Text.Trim();
                if (q == "")
                {
                    SetViewMode(OrgTreeMode.Tree);
                    btnClearSearch.Enabled = false;
                }
                else
                {
                    btnClearSearch.Enabled = true;
                    SetViewMode(OrgTreeMode.List);
                    if (m_thread == null || !m_thread.IsAlive)
                    {
                        m_thread = new Thread(new ParameterizedThreadStart(Search));
                        m_thread.Start(textSearch.Text);
                        timer1.Enabled = true;
                    }
                }
            }
        }

        private void SetViewMode(OrgTreeMode orgTreeMode)
        {
            if (m_Mode == orgTreeMode)
                return;
            m_Mode = orgTreeMode;
            if (m_Mode == OrgTreeMode.Tree)
            {
                employeeList.Visible = false;
                tvOrganization.Visible = true;
            }
            else
            {
                employeeList.Visible = true;
                tvOrganization.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textSearch.Text = "";
            textSearch.Modified = true;
            UpdateSearchResult();
        }

        private void employeeList_EmployeeSelected(object sender, EventArgs e)
        {
            if (EmployeeSelected != null)
                EmployeeSelected(this, employeeList.SelectedEmployee);
        }


        internal bool IncludeEmployees
        {
            get
            {
                return this.tvOrganization.IncludeEmployees;
            }
            set
            {
                if (this.tvOrganization.IncludeEmployees != value)
                {
                    panelSeach.Visible = value;
                    this.tvOrganization.IncludeEmployees = value;   
                }
            }
        }

        private void miEnrollEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                PayrollClient.EnrollEmployee(DateTime.Now, tvOrganization.SelectedEmployee.ObjectIDVal);
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Employee enrolled.");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Faild to enroll the employee", ex);
            }
        }

        private void miFireEmployee_Click(object sender, EventArgs e)
        {
            try
            {
                PayrollClient.FireEmployee(DateTime.Now, SelectedEmployee.ObjectIDVal);
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Employee disabled.");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Faild to disable the employee", ex);
            }

        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadData(_dateDate);
        }
    }
}
