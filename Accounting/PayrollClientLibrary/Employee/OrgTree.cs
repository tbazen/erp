using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
namespace INTAPS.Payroll.Client
{
    public class OrgTree:TreeView
    {
        List<int> LoadedNodes;
        bool m_IncludeEmployees = false;
        ImageList m_Icons;
        public bool includeDismissed{ get; set; }
        public OrgTree()
        {
            m_Icons = new ImageList();
            m_Icons.Images.Add(Properties.Resources.OrgUnit);
            m_Icons.Images.Add(Properties.Resources.Employee);
            LoadedNodes = new List<int>();
            this.ImageList = m_Icons;
        }
        public bool IncludeEmployees
        {
            get 
            {
                return m_IncludeEmployees; 
            }
            set 
            {
                if (m_IncludeEmployees == value) 
                    return;
                m_IncludeEmployees = value;
                AddRemoveEmployeeNodes(this.Nodes,value);
            }
        }
        void AddRemoveEmployeeNodes(TreeNodeCollection nodes,bool Add)
        {
            foreach (TreeNode n in nodes)
            {
                if (Add)
                    AddEmployeeNodes(n.Nodes,((OrgUnit)n.Tag).ObjectIDVal);
                else
                {
                    if (n.Tag is Employee)
                        n.Remove();
                }
                AddRemoveEmployeeNodes(n.Nodes, Add);
            }
        }
        void AddEmployeeNode(TreeNodeCollection nodes, Employee e)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = 1;
            n.SelectedImageIndex = 1;
            SetEmployeeNode(n, e);
            nodes.Add(n);
        }
        private void SetEmployeeNode(TreeNode n, Employee e)
        {
            n.Text = e.EmployeeNameID;
            n.Tag = e;
        }
        private void AddEmployeeNodes(TreeNodeCollection nodes,int OrgID)
        {
            Employee[] es = PayrollClient.GetEmployees(OrgID, false);
            foreach (Employee e in es)
            {
                if (e.status != EmployeeStatus.Enrolled && !includeDismissed)
                    continue;
                AddEmployeeNode(nodes, e);
            }
            
        }
        TreeNode AddOrgUnitNode(TreeNodeCollection parent, OrgUnit unit)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = 0;
            n.SelectedImageIndex = 0;
            SetNode(n,unit);
            parent.Add(n);
            return n;
        }
        private static void SetNode(TreeNode n, OrgUnit unit)
        {
            n.Text = unit.Name;
            n.ToolTipText = string.IsNullOrEmpty(unit.Description) ? "" : unit.Description;
            n.Tag = unit;
        }
        void LoadChilds(TreeNodeCollection nodes, int PID)
        {
            if (LoadedNodes.Contains(PID))
                return;
            LoadedNodes.Add(PID);
            OrgUnit[] orgs = PayrollClient.GetOrgUnits(PID);
            foreach (OrgUnit o in orgs)
            {
                AddOrgUnitNode(nodes, o);
            }
            if (m_IncludeEmployees)
                AddEmployeeNodes(nodes, PID);
        }
        void LoadChildOfChilds(TreeNodeCollection nodes)
        {
            foreach (TreeNode n in nodes)
            {
                if(n.Tag is OrgUnit)
                    LoadChilds(n.Nodes, ((OrgUnit)n.Tag).ObjectIDVal);
            }
        }
        DateTime dataDate = DateTime.Now;
        public void LoadData(DateTime dateDate)
        {
            this.dataDate = dateDate;
            LoadedNodes.Clear();
            this.Nodes.Clear();
            LoadChilds(this.Nodes, -1);
            LoadChildOfChilds(this.Nodes);
            foreach (TreeNode n in this.Nodes)
                n.Expand();
        }
        protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
        {
            LoadChildOfChilds(e.Node.Nodes);
        }
        protected TreeNode SearchTree(TreeNodeCollection nodes, int ID,bool Org)
        {
            foreach(TreeNode n in nodes)
            {
                if (Org)
                {
                    if (!(n.Tag is OrgUnit))
                        continue;
                    if (((OrgUnit)n.Tag).ObjectIDVal == ID)
                        return n;
                    TreeNode sn = SearchTree(n.Nodes, ID,Org);
                    if (sn != null)
                        return sn;
                }
                else
                {
                    if (n.Tag is Employee)
                    {
                        if (((Employee)n.Tag).ObjectIDVal == ID)
                            return n;
                    }
                    TreeNode sn = SearchTree(n.Nodes, ID,Org);
                    if (sn != null)
                        return sn;
                }
            }
            return null;
        }
        public TreeNode AddOrgUnit(OrgUnit orgUnit)
        {
            TreeNode ret;
            if (orgUnit.PID == -1)
                ret=AddOrgUnitNode(this.Nodes, orgUnit);
            else
            {
                TreeNode n = SearchTree(this.Nodes, orgUnit.PID,true);
                if (n == null)
                    return null;
                ret=AddOrgUnitNode(n.Nodes, orgUnit);
            }
            if (ret.Parent == null || ret.Parent.IsExpanded)
                LoadChilds(ret.Nodes, orgUnit.ObjectIDVal);
            return ret;
        }
        public void UpdateOrgUnit(OrgUnit orgUnit)
        {
            TreeNode n = SearchTree(this.Nodes, orgUnit.ObjectIDVal,true);
            if (n == null)
                return;
            SetNode(n, orgUnit);
        }
        public OrgUnit SelectedOrg
        {
            get
            {
                if(this.SelectedNode==null)
                    return null;
                return this.SelectedNode.Tag as OrgUnit;
            }
        }
        public Employee SelectedEmployee
        {
            get
            {
                if (this.SelectedNode == null)
                    return null;
                return this.SelectedNode.Tag as Employee;
            }
        }
        public void DeleteOrgUnit(int ID)
        {
            TreeNode n = SearchTree(this.Nodes, ID,true);
            if (n == null)
                return;
            n.Remove();
        }
        internal void AddEmployee(Employee employee)
        {
            if (!m_IncludeEmployees)
                return;
            TreeNode n = SearchTree(this.Nodes, employee.orgUnitID, true);
            if (n == null)
                return;
            AddEmployeeNode(n.Nodes, employee);
        }
        internal void UpdateEmployee(Employee employee)
        {
            if (!m_IncludeEmployees)
                return;
            TreeNode n = SearchTree(this.Nodes, employee.ObjectIDVal, false);
            if (n == null)
                return;
            SetEmployeeNode(n, employee);
        }
        internal void DeleteEmployee(int ID)
        {
            if (!m_IncludeEmployees)
                return;
            TreeNode n = SearchTree(this.Nodes, ID, false);
            if (n == null)
                return;
            n.Remove();
        }
    }
}
