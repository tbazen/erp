using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class OrgUnitProfile : Form
    {
        public OrgUnit FormData;
        INTAPS.UI.EnterKeyNavigation m_nav;
        bool New = false;
        public OrgUnitProfile()
        {
            InitializeComponent();
            m_nav = new INTAPS.UI.EnterKeyNavigation();
            m_nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtName));
            m_nav.AddNavigableItem(new INTAPS.UI.TextNavigator(txtMoreInfo));
            m_nav.AddNavigableItem(new INTAPS.UI.ButtonNavigator(btnOk));
        }
        void SetParent(int PID)
        {
            if (PID == -1)
                txtParentOrg.Text= "";
            else
                txtParentOrg.Text = PayrollClient.GetOrgUnit(PID).Name;
        }
        public OrgUnitProfile(int PID)
            :this()
        {
            FormData = new OrgUnit();
            FormData.PID = PID;
            SetParent(PID);
            New = true;
        }
        public OrgUnitProfile(OrgUnit edit)
            : this()
        {
            FormData = edit;
            txtName.Text = edit.Name;
            txtMoreInfo.Text = edit.Description;
            SetParent(edit.PID);
            New = false;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Trim() == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter organization name.");
                txtName.SelectAll();
                txtName.Focus();
                return;
            }
            FormData.Name = txtName.Text;
            FormData.Description = txtMoreInfo.Text;
            try
            {
                if (New)
                {
                    FormData.ObjectIDVal = PayrollClient.SaveOrgUnit(FormData);
                    New = false;
                }
                else
                {
                    PayrollClient.UpdateOrgUnit(FormData);
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't seave organizatinal unit data.",ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}