﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.Client.Evaluator
{
    public class EFGetComponentDataField : IFunction
    {
        public EFGetComponentDataField()
        {
        }
        public string Symbol
        {
            get { return "CompField"; }
        }

        public  int ParCount
        {
            get { return 5; }
        }

        public EData Evaluate(EData[] Pars)
        {
            int empID;
            if (Pars[0].Value is int)
                empID = (int)Pars[0].Value;
            else if (Pars[0].Value is double)
                empID = (int)(double)Pars[0].Value;
            else
                return new FSError("First parameter for CompField should be numeric employee ID").ToEData();
            int periodID;
            if (Pars[1].Value is int)
                periodID = (int)Pars[1].Value;
            else if (Pars[1].Value is double)
                periodID = (int)(double)Pars[1].Value;
            else
                return new FSError("First parameter for CompField should be numeric period ID").ToEData();
            string component = Pars[2].Value as string;
            string formula = Pars[3].Value as string;
            string field = Pars[4].Value as string;
            if (string.IsNullOrEmpty(field))
                return new FSError("Field name is not provided for CompField").ToEData();
            if (string.IsNullOrEmpty(component))
                return new FSError("Component name is not provided for CompField").ToEData();

            if (string.IsNullOrEmpty(formula))
                return new FSError("Formula name is not provided for CompField").ToEData();

            try
            {
                PayrollComponentDefination pdef = null;
                foreach (PayrollComponentDefination def in PayrollClient.PCDDefsArr)
                {
                    if (def.Name.ToUpper() == component.ToUpper())
                    {
                        pdef = def;
                        break;
                    }
                }
                if (pdef == null)
                    return new FSError("Invalid component name:" + component).ToEData();
                PayrollComponentFormula pf = null;
                foreach (PayrollComponentFormula f in PayrollClient.GetPCFormulae(pdef.ID))
                {
                    if (f.Name.ToUpper() == formula.ToUpper())
                    {
                        pf = f;
                        break;
                    }
                }
                if (pf == null)
                    return new FSError("Invalid formula name:" + formula).ToEData();
                PayrollComponentData[] ret = PayrollClient.GetPCData(pf.PCDID, pf.ID, AppliesToObjectType.Employee, empID, periodID, false);
                if (ret.Length == 0)
                    return new EData(DataType.Empty, null);
                object addData = PayrollClient.GetPCAdditionalData(ret[0].ID);
                if (addData == null)
                    return new EData(DataType.Empty, null);

                System.Reflection.FieldInfo fi = addData.GetType().GetField(field);
                object obj;
                if (fi == null)
                {
                    System.Reflection.PropertyInfo pi = typeof(Employee).GetProperty(field);
                    if (pi == null)
                        return new FSError("Invalid field name-" + field).ToEData();
                    else
                        obj = pi.GetValue(addData, new object[] { });
                }
                else
                    obj = fi.GetValue(addData);
                if (obj is int)
                {
                    return new EData(DataType.Int, obj);
                }

                if (obj is double)
                {
                    return new EData(DataType.Float, obj);
                }

                return new EData(DataType.Text, obj.ToString());
            }
            catch (Exception ex)
            {
                return new FSError("Error evaluating EmpField-" + ex.Message).ToEData();
            }
        }
        #region IFunction Members


        public string Name
        {
            get { return "Employee Field"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion
    }
}
