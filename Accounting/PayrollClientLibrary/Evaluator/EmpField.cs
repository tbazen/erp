﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.Client.Evaluator
{
    public class EFGetEmployeeDataField : IFunction
    {
        public EFGetEmployeeDataField()
        {
        }
        public string Symbol
        {
            get { return "EmpField"; }
        }

        public  int ParCount
        {
            get { return 2; }
        }

        public  EData Evaluate(EData[] Pars)
        {
            int empID;
            if (Pars[0].Value is int)
                empID = (int)Pars[0].Value;
            else if (Pars[0].Value is double)
                empID = (int)(double)Pars[0].Value;
            else
                return new FSError("First parameter for EmpField should be numeric employee ID").ToEData();
            string field = Pars[1].Value as string;
            if (string.IsNullOrEmpty(field))
                return new FSError("Field name is not provided for EmpField").ToEData();
            try
            {
                Employee emp = PayrollClient.GetEmployee(empID);
                System.Reflection.FieldInfo fi = typeof(Employee).GetField(field);
                object obj;
                if (fi == null)
                {
                    System.Reflection.PropertyInfo pi=typeof(Employee).GetProperty(field);
                    if (pi == null)
                        return new FSError("Invalid field name-" + field).ToEData();
                    else
                        obj = pi.GetValue(emp, new object[] { });
                }
                else 
                    obj = fi.GetValue(emp);
                if (obj is int)
                {
                    if (field.Equals("WorkingHours") && (int)obj == 0)
                        obj = PayrollClient.GetSystemParameters(new string[] { "EmployeeDailyFullWorkingHours" })[0];
                    return new EData(DataType.Int, obj);
                }
                if (obj is double)
                {
                    return new EData(DataType.Float, obj);
                }
                return new EData(DataType.Text, obj.ToString());
            }
            catch (Exception ex)
            {
                return new FSError("Error evaluating EmpField-" + ex.Message).ToEData();
            }
        }
        #region IFunction Members


        public string Name
        {
            get { return "Employee Field"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        #endregion
    }
}
