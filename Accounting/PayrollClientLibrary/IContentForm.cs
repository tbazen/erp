using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public enum MainFormStandardButtons
    {
        Save,
        Refresh
    }
    public interface IPayrollContentForm
    {
        string GetStandardCommandText(MainFormStandardButtons cmd);
        bool SupportStandardCommand(MainFormStandardButtons cmd);
        void DoStandardCommand(MainFormStandardButtons cmd);
        bool ShowEmbeded { get;}
        void FilterByEmp(Employee e, OrgUnit parent);
        void FilterByOrg(OrgUnit o);
        void FilterByPCD(PayrollComponentDefination pcd);
        void FilterByFormula(PayrollComponentFormula formula, PayrollComponentDefination parent);
        void FilterByPayPeriod(PayrollPeriod pp);
        void Close();
        Control GetControl();
    }

}
