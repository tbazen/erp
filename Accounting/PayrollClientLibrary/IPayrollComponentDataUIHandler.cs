using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public interface IPayrollComponentUIHandler
    {
        Control CreateEditor(IPayrollComponentHanlderHost h);
        bool ConfigureEditor(Control editor, AppliesToEmployees at);
        bool SetEditorData(Control editor, PayrollComponentData dataInfo, object data);
        object GetEditorData(Control editor);
        bool ValidateEditorData(Control editor);
        bool HaveEditor();
        string GetHTML(PayrollComponentData dataInfo, object aditionaldata);
        string GetDefaultHTML(AppliesToEmployees at);
    }
    public interface IPayrollComponentHanlderHost
    {

    }
}
