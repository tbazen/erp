namespace INTAPS.Payroll.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tsMainForm = new System.Windows.Forms.ToolStrip();
            this.miShow = new System.Windows.Forms.ToolStripDropDownButton();
            this.miShowInputData = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowPayrolld = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowLoans = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.miShowChartOfAccounts = new System.Windows.Forms.ToolStripMenuItem();
            this.miShowSystemConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.miPayrollSheetFormat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.btnPrevYear = new System.Windows.Forms.ToolStripButton();
            this.btnPrevMonth = new System.Windows.Forms.ToolStripButton();
            this.lblPeriod = new System.Windows.Forms.ToolStripLabel();
            this.btnNextMonth = new System.Windows.Forms.ToolStripButton();
            this.btnNextYear = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnGenerateParyoll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.lblStatus = new System.Windows.Forms.ToolStripLabel();
            this.spltMain = new System.Windows.Forms.SplitContainer();
            this.orgBrowser = new INTAPS.Payroll.Client.OrgBrowser();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pcdTree = new INTAPS.Payroll.Client.PCDTreeEditor();
            this.tsMainForm.SuspendLayout();
            this.spltMain.Panel1.SuspendLayout();
            this.spltMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsMainForm
            // 
            this.tsMainForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miShow,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.btnPrevYear,
            this.btnPrevMonth,
            this.lblPeriod,
            this.btnNextMonth,
            this.btnNextYear,
            this.toolStripSeparator2,
            this.btnGenerateParyoll,
            this.toolStripSeparator4,
            this.btnSave,
            this.btnRefresh,
            this.toolStripSeparator5,
            this.lblStatus});
            this.tsMainForm.Location = new System.Drawing.Point(0, 0);
            this.tsMainForm.Name = "tsMainForm";
            this.tsMainForm.Size = new System.Drawing.Size(742, 25);
            this.tsMainForm.TabIndex = 1;
            this.tsMainForm.Text = "toolStrip1";
            // 
            // miShow
            // 
            this.miShow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.miShow.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miShowInputData,
            this.miShowPayrolld,
            this.miShowLoans,
            this.miShowEmployee,
            this.toolStripSeparator3,
            this.miShowChartOfAccounts,
            this.miShowSystemConfiguration,
            this.miPayrollSheetFormat});
            this.miShow.Image = ((System.Drawing.Image)(resources.GetObject("miShow.Image")));
            this.miShow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.miShow.Name = "miShow";
            this.miShow.Size = new System.Drawing.Size(49, 22);
            this.miShow.Text = "Show";
            // 
            // miShowInputData
            // 
            this.miShowInputData.Name = "miShowInputData";
            this.miShowInputData.Size = new System.Drawing.Size(194, 22);
            this.miShowInputData.Tag = "INTAPS.Payroll.Client.DataBrowser,PayrollClientLibrary";
            this.miShowInputData.Text = "Input Data";
            this.miShowInputData.Click += new System.EventHandler(this.ShowMenuItemClicked);
            // 
            // miShowPayrolld
            // 
            this.miShowPayrolld.Name = "miShowPayrolld";
            this.miShowPayrolld.Size = new System.Drawing.Size(194, 22);
            this.miShowPayrolld.Tag = "INTAPS.Payroll.Client.PayrollBrowser,PayrollClientLibrary";
            this.miShowPayrolld.Text = "Payroll";
            this.miShowPayrolld.Click += new System.EventHandler(this.ShowMenuItemClicked);
            // 
            // miShowLoans
            // 
            this.miShowLoans.Name = "miShowLoans";
            this.miShowLoans.Size = new System.Drawing.Size(194, 22);
            this.miShowLoans.Tag = "INTAPS.Payroll.Client.StaffLoanBrowser,PayrollClientLibrary";
            this.miShowLoans.Text = "Staff Loans";
            this.miShowLoans.Click += new System.EventHandler(this.ShowMenuItemClicked);
            // 
            // miShowEmployee
            // 
            this.miShowEmployee.Name = "miShowEmployee";
            this.miShowEmployee.Size = new System.Drawing.Size(194, 22);
            this.miShowEmployee.Tag = "INTAPS.Payroll.Client.EmployeeList,PayrollClientLibrary";
            this.miShowEmployee.Text = "Employees";
            this.miShowEmployee.Click += new System.EventHandler(this.ShowMenuItemClicked);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(191, 6);
            // 
            // miShowChartOfAccounts
            // 
            this.miShowChartOfAccounts.Name = "miShowChartOfAccounts";
            this.miShowChartOfAccounts.Size = new System.Drawing.Size(194, 22);
            this.miShowChartOfAccounts.Text = "Chart of Accounts";
            this.miShowChartOfAccounts.Click += new System.EventHandler(this.miShowChartOfAccounts_Click);
            // 
            // miShowSystemConfiguration
            // 
            this.miShowSystemConfiguration.Name = "miShowSystemConfiguration";
            this.miShowSystemConfiguration.Size = new System.Drawing.Size(194, 22);
            this.miShowSystemConfiguration.Text = "System Configurations";
            this.miShowSystemConfiguration.Click += new System.EventHandler(this.miShowSystemConfiguration_Click);
            // 
            // miPayrollSheetFormat
            // 
            this.miPayrollSheetFormat.Name = "miPayrollSheetFormat";
            this.miPayrollSheetFormat.Size = new System.Drawing.Size(194, 22);
            this.miPayrollSheetFormat.Text = "Payroll Sheet Format";
            this.miPayrollSheetFormat.Click += new System.EventHandler(this.miPayrollSheetFormat_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(44, 22);
            this.toolStripLabel1.Text = "Period:";
            // 
            // btnPrevYear
            // 
            this.btnPrevYear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPrevYear.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevYear.Image")));
            this.btnPrevYear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrevYear.Name = "btnPrevYear";
            this.btnPrevYear.Size = new System.Drawing.Size(27, 22);
            this.btnPrevYear.Text = "<<";
            this.btnPrevYear.ToolTipText = "Previous Year";
            // 
            // btnPrevMonth
            // 
            this.btnPrevMonth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnPrevMonth.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevMonth.Image")));
            this.btnPrevMonth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPrevMonth.Name = "btnPrevMonth";
            this.btnPrevMonth.Size = new System.Drawing.Size(23, 22);
            this.btnPrevMonth.Text = "<";
            this.btnPrevMonth.ToolTipText = "Previous Month";
            // 
            // lblPeriod
            // 
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(48, 22);
            this.lblPeriod.Text = "2003/10";
            // 
            // btnNextMonth
            // 
            this.btnNextMonth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNextMonth.Image = ((System.Drawing.Image)(resources.GetObject("btnNextMonth.Image")));
            this.btnNextMonth.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNextMonth.Name = "btnNextMonth";
            this.btnNextMonth.Size = new System.Drawing.Size(23, 22);
            this.btnNextMonth.Text = ">";
            this.btnNextMonth.ToolTipText = "Next Month";
            // 
            // btnNextYear
            // 
            this.btnNextYear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnNextYear.Image = ((System.Drawing.Image)(resources.GetObject("btnNextYear.Image")));
            this.btnNextYear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNextYear.Name = "btnNextYear";
            this.btnNextYear.Size = new System.Drawing.Size(27, 22);
            this.btnNextYear.Text = ">>";
            this.btnNextYear.ToolTipText = "Next Year";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnGenerateParyoll
            // 
            this.btnGenerateParyoll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnGenerateParyoll.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerateParyoll.Image")));
            this.btnGenerateParyoll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGenerateParyoll.Name = "btnGenerateParyoll";
            this.btnGenerateParyoll.Size = new System.Drawing.Size(97, 22);
            this.btnGenerateParyoll.Text = "Generate Paryoll";
            this.btnGenerateParyoll.Click += new System.EventHandler(this.btnGenerateParyoll_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btnSave
            // 
            this.btnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(35, 22);
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Image")));
            this.btnRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(50, 22);
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 22);
            this.lblStatus.Text = "Ready";
            // 
            // spltMain
            // 
            this.spltMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spltMain.Location = new System.Drawing.Point(0, 25);
            this.spltMain.Name = "spltMain";
            // 
            // spltMain.Panel1
            // 
            this.spltMain.Panel1.Controls.Add(this.orgBrowser);
            this.spltMain.Size = new System.Drawing.Size(505, 527);
            this.spltMain.SplitterDistance = 222;
            this.spltMain.TabIndex = 4;
            // 
            // orgBrowser
            // 
            this.orgBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orgBrowser.Location = new System.Drawing.Point(0, 0);
            this.orgBrowser.Name = "orgBrowser";
            this.orgBrowser.Size = new System.Drawing.Size(222, 527);
            this.orgBrowser.TabIndex = 0;
            this.orgBrowser.EmployeeSelected += new INTAPS.Payroll.Client.EmployeeSelectedHandler(this.orgBrowser_EmployeeSelected);
            this.orgBrowser.OrgUnitSelected += new INTAPS.Payroll.Client.OrgUnitSelectedHandler(this.orgBrowser_OrgUnitSelected);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(505, 25);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 527);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // pcdTree
            // 
            this.pcdTree.Dock = System.Windows.Forms.DockStyle.Right;
            this.pcdTree.Location = new System.Drawing.Point(508, 25);
            this.pcdTree.Name = "pcdTree";
            this.pcdTree.Size = new System.Drawing.Size(234, 527);
            this.pcdTree.TabIndex = 3;
            this.pcdTree.FormulaSelected += new INTAPS.Payroll.Client.FormulaSelectedHandler(this.pcdTree_FormulaSelected);
            this.pcdTree.PayrollComponentSelected += new INTAPS.Payroll.Client.PayrollComponentedSelectedHandler(this.pcdTree_PayrollComponentSelected);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 552);
            this.Controls.Add(this.spltMain);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pcdTree);
            this.Controls.Add(this.tsMainForm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tsMainForm.ResumeLayout(false);
            this.tsMainForm.PerformLayout();
            this.spltMain.Panel1.ResumeLayout(false);
            this.spltMain.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tsMainForm;
        private System.Windows.Forms.ToolStripDropDownButton miShow;
        private System.Windows.Forms.ToolStripMenuItem miShowInputData;
        private System.Windows.Forms.ToolStripMenuItem miShowPayrolld;
        private System.Windows.Forms.ToolStripButton btnPrevYear;
        private System.Windows.Forms.ToolStripButton btnPrevMonth;
        private System.Windows.Forms.ToolStripLabel lblPeriod;
        private System.Windows.Forms.ToolStripButton btnNextMonth;
        private System.Windows.Forms.ToolStripButton btnNextYear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.SplitContainer spltMain;
        private System.Windows.Forms.Splitter splitter1;
        private PCDTreeEditor pcdTree;
        private OrgBrowser orgBrowser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnGenerateParyoll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem miShowChartOfAccounts;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnRefresh;
        private System.Windows.Forms.ToolStripMenuItem miShowLoans;
        private System.Windows.Forms.ToolStripMenuItem miShowEmployee;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel lblStatus;
        private System.Windows.Forms.ToolStripMenuItem miShowSystemConfiguration;
        private System.Windows.Forms.ToolStripMenuItem miPayrollSheetFormat;
    }
}