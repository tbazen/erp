using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI;

namespace INTAPS.Payroll.Client
{
   
   
    public partial class MainForm : Form
    {

        IPayrollContentForm m_contentForm;
        PayrollPeriodNavigator m_periodNavigator;
        public static MainForm TheInstance;
        public PayrollPeriod SelectedPeriodInternal
        {
            get
            {
                return m_periodNavigator.SelectedPeriod;
            }
        }
        public MainForm()
        {
            InitializeComponent();
            orgBrowser.LoadData(DateTime.Now);
            pcdTree.LoadData();
            
            m_periodNavigator = new PayrollPeriodNavigator(btnPrevYear, btnPrevMonth, btnNextMonth, btnNextYear, lblPeriod,PayrollClient.CurrentYear);
            m_periodNavigator.PeriodSelectionChanged += new EventHandler(m_periodNavigator_PeriodSelectionChanged);
            TheInstance = this;
            ShowContentForm(typeof(PayrollBrowser));
        }
        public void SetSaveCommandTextInternal(string text)
        {
            btnSave.Text = text;
        }
        void m_periodNavigator_PeriodSelectionChanged(object sender, EventArgs e)
        {
            if (m_contentForm != null && m_periodNavigator.SelectedPeriod != null)
                m_contentForm.FilterByPayPeriod(m_periodNavigator.SelectedPeriod);
        }
        void ShowContentForm(Type t)
        {
            try
            {
                System.Reflection.ConstructorInfo ci = t.GetConstructor(new Type[0]);
                IPayrollContentForm sc = (IPayrollContentForm)ci.Invoke(new object[0]);
                Control c = sc.GetControl();
                if (c is Form)
                {
                    Form f = (Form)c;
                    f.TopLevel = false;
                    f.FormBorderStyle = FormBorderStyle.None;
                    f.Show();
                }
                c.Dock = DockStyle.Fill;
                spltMain.Panel2.Controls.Clear();
                spltMain.Panel2.Controls.Add(c);
                this.Text = c.Text;
                m_contentForm = sc;

                if (orgBrowser.SelectedEmployee != null)
                    m_contentForm.FilterByEmp(orgBrowser.SelectedEmployee, orgBrowser.SelectedOrg);
                else if (orgBrowser.SelectedOrg != null)
                    m_contentForm.FilterByOrg(orgBrowser.SelectedOrg);

                if (pcdTree.SelectedFormula != null)
                    m_contentForm.FilterByFormula(pcdTree.SelectedFormula, pcdTree.SelectedPCD);
                else if (pcdTree.SelectedPCD != null)
                    m_contentForm.FilterByPCD(pcdTree.SelectedPCD);

                m_contentForm.FilterByPayPeriod(m_periodNavigator.SelectedPeriod);



                if (m_contentForm.SupportStandardCommand(MainFormStandardButtons.Save))
                {
                    btnSave.Visible = true;
                    btnSave.Text = m_contentForm.GetStandardCommandText(MainFormStandardButtons.Save);
                }
                else
                    btnSave.Visible = false;

                if (m_contentForm.SupportStandardCommand(MainFormStandardButtons.Refresh))
                {
                    btnRefresh.Visible = true;
                    btnRefresh.Text = m_contentForm.GetStandardCommandText(MainFormStandardButtons.Refresh);
                }
                else
                    btnRefresh.Visible = false;

            }
            catch (Exception ex)
            {
                UI.UIFormApplicationBase.CurrentAppliation.HandleException("Unknown error.", ex);
            }
        }
        private void ShowMenuItemClicked(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            string tstr=mi.Tag as string;
            if (string.IsNullOrEmpty(tstr))
                return;
            Type t = Type.GetType(tstr);
            ShowContentForm(t);            
        }

        private void pcdTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (m_contentForm == null)
                return;
            /*if (pcdTree.SelectedForumla != null)
                m_contentForm.FilterByFormula(pcdTree.SelectedForumla);
            else if (pcdTree.SelectedPCD != null)
                m_contentForm.FilterByPCD(pcdTree.SelectedPCD);*/

        }

        private void orgBrowser_EmployeeSelected(object source, Employee Selection)
        {
            if(Selection!=null && m_contentForm!=null)
                m_contentForm.FilterByEmp(Selection,orgBrowser.SelectedOrg);
        }

        private void orgBrowser_OrgUnitSelected(object source, OrgUnit Selection)
        {
            if (Selection != null && m_contentForm != null)
                m_contentForm.FilterByOrg(Selection);
        }

        private void btnGenerateParyoll_Click(object sender, EventArgs e)
        {
            GeneratePayroll gp = new GeneratePayroll(m_periodNavigator.SelectedPeriod);
            gp.Show(this);
        }
        private void miShowChartOfAccounts_Click(object sender, EventArgs e)
        {
            INTAPS.Accounting.Client.AccountStructureBrowser ch = new INTAPS.Accounting.Client.AccountStructureBrowser(false);
            ch.Show(this);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            m_contentForm.DoStandardCommand(MainFormStandardButtons.Save);
        }

        internal void SetSaveCommandEnabledInternal(bool p)
        {
            btnSave.Enabled=p;
        }

        private void pcdTree_FormulaSelected(PCDTreeEditor source, PayrollComponentFormula selection, PayrollComponentDefination parent)
        {
            if (m_contentForm != null)
                m_contentForm.FilterByFormula(selection, parent);
        }

        private void pcdTree_PayrollComponentSelected(PCDTreeEditor source, PayrollComponentDefination selection)
        {
            if (m_contentForm != null)
                m_contentForm.FilterByPCD(selection);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            m_contentForm.DoStandardCommand(MainFormStandardButtons.Refresh);
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            INTAPS.UI.UIStateHandler.LoadFormState(
             INTAPS.UI.UIFormApplicationBase.CurrentAppliation.GetSetting("MainForm.Form", null)
            , this);
            INTAPS.UI.UIStateHandler.LoadControlSizeState(
             INTAPS.UI.UIFormApplicationBase.CurrentAppliation.GetSetting("MainForm.RightSize",null)
            , pcdTree);
            INTAPS.UI.UIStateHandler.LoadSpliterState(
             INTAPS.UI.UIFormApplicationBase.CurrentAppliation.GetSetting("MainForm.Spliter", null)
            , spltMain);
            orgBrowser.LoadState();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.SetSetting("MainForm.Form",
            INTAPS.UI.UIStateHandler.GetFormState(this));
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.SetSetting("MainForm.RightSize",
            INTAPS.UI.UIStateHandler.GetControlSizeState(pcdTree));
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.SetSetting("MainForm.Spliter",
            INTAPS.UI.UIStateHandler.GetSpliterState(spltMain));
            orgBrowser.SaveState();
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.SaveSettings();
        }
        public void SetStatusInternal(string status)
        {
            lblStatus.Text = status;
        }

        private void miShowSystemConfiguration_Click(object sender, EventArgs e)
        {
            PayrollConfiguration conf = new PayrollConfiguration();
            conf.ShowDialog(this);
        }


        public void AttachToBrowser(INTAPS.UI.HTML.ControlBrowser b)
        {
        }

        private void miPayrollSheetFormat_Click(object sender, EventArgs e)
        {
            PayrollSheetFormatEditor p = new PayrollSheetFormatEditor();
            p.Show();
        }


        internal static void SetStatus(string p)
        {
            if (TheInstance != null)
                TheInstance.SetStatusInternal(p);
        }

        internal static void SetSaveCommandEnabled(bool p)
        {
            if (TheInstance != null)
                TheInstance.SetSaveCommandEnabledInternal(p);
        }
        public static PayrollPeriod SelectedPeriod
        {
            get
            {
                if (TheInstance != null)
                    return TheInstance.SelectedPeriodInternal;
                return null;
            }
        }

        internal static void SetSaveCommandText(string p)
        {
            if(TheInstance!=null)
                TheInstance.SetSaveCommandTextInternal(p);
        }
    }
    
}