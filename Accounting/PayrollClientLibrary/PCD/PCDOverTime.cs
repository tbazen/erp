using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    class PCDOverTime : IPayrollComponentUIHandler
    {

        #region IPayrollComponentUIHandler Members

        public System.Windows.Forms.Control CreateEditor(IPayrollComponentHanlderHost h)
        {
            return new PCDOverTimeEditor();
        }

        public bool ConfigureEditor(System.Windows.Forms.Control editor, AppliesToEmployees at)
        {
            if (!at.ASpecificEmployee)
                return false;
            PCDOverTimeEditor e = (PCDOverTimeEditor)editor;
            return true;
        }

        public bool SetEditorData(System.Windows.Forms.Control editor, PayrollComponentData dataInfo, object data)
        {
            OverTimeData ot = data as OverTimeData;
            PCDOverTimeEditor e = (PCDOverTimeEditor)editor;
            if (ot == null)
                e.ClearData();
            else
            {
                e.textOffhours.Text = ot.offHours == 0 ? "" : ot.offHours.ToString("0.00");
                e.textWeekend.Text = ot.weekendHours == 0 ? "" : ot.weekendHours.ToString("0.00");
                e.textHoliday.Text = ot.holidayHours == 0 ? "" : ot.holidayHours.ToString("0.00");
                e.textLateHours.Text = ot.lateHours == 0 ? "" : ot.lateHours.ToString("0.00");
            }
            return true;
        }

        public object GetEditorData(System.Windows.Forms.Control editor)
        {
            OverTimeData ot = new OverTimeData();
            PCDOverTimeEditor e = (PCDOverTimeEditor)editor;
            if (!double.TryParse(e.textOffhours.Text, out ot.offHours))
                ot.offHours = 0;
            if (!double.TryParse(e.textWeekend.Text, out ot.weekendHours))
                ot.weekendHours = 0;
            if (!double.TryParse(e.textHoliday.Text, out ot.holidayHours))
                ot.holidayHours = 0;
            if (!double.TryParse(e.textLateHours.Text, out ot.lateHours))
                ot.lateHours = 0;
            return ot;
        }

        public bool ValidateEditorData(System.Windows.Forms.Control editor)
        {
            PCDOverTimeEditor e = (PCDOverTimeEditor)editor;
            double v;
            if (e.textOffhours.Text.Trim() != "" && !double.TryParse(e.textOffhours.Text, out v))
                return false;
            if (e.textWeekend.Text.Trim() != "" && !double.TryParse(e.textWeekend.Text, out v))
                return false;
            if (e.textHoliday.Text.Trim() != "" && !double.TryParse(e.textHoliday.Text, out v))
                return false;
            return true;
        }


        public bool HaveEditor()
        {
            return true;
        }

        public string GetHTML(PayrollComponentData dataInfo, object aditionaldata)
        {
            if (aditionaldata == null)
                return "Null data";
            OverTimeData ot = (OverTimeData)aditionaldata;
            string html;
            html = "<b>Off office hours:</b>" + (ot.offHours==0?"none":(ot.offHours.ToString("0.00") + " hours."))
                + "<br/><b>Late hours:</b>" + (ot.lateHours == 0 ? "none" : (ot.lateHours.ToString("0.00") + " hours"))
                +"<br/><b>Weekend hours:</b>"+(ot.weekendHours==0?"none":(ot.weekendHours.ToString("0.00")+" hours"))
                +"<br/><b>Holiday hours:</b>"+(ot.holidayHours==0?"none":(ot.holidayHours.ToString("0.00")+" hours"));
            return html;
        }


        public string GetDefaultHTML(AppliesToEmployees at)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
