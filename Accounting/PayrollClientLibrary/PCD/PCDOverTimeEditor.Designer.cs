namespace INTAPS.Payroll.Client.PCD
{
    partial class PCDOverTimeEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textOffhours = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textWeekend = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textHoliday = new System.Windows.Forms.TextBox();
            this.textLateHours = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Off hours:";
            // 
            // textOffhours
            // 
            this.textOffhours.Location = new System.Drawing.Point(118, 14);
            this.textOffhours.Name = "textOffhours";
            this.textOffhours.Size = new System.Drawing.Size(295, 20);
            this.textOffhours.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Weekend hours";
            // 
            // textWeekend
            // 
            this.textWeekend.Location = new System.Drawing.Point(118, 63);
            this.textWeekend.Name = "textWeekend";
            this.textWeekend.Size = new System.Drawing.Size(294, 20);
            this.textWeekend.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Holiday hours";
            // 
            // textHoliday
            // 
            this.textHoliday.Location = new System.Drawing.Point(118, 89);
            this.textHoliday.Name = "textHoliday";
            this.textHoliday.Size = new System.Drawing.Size(293, 20);
            this.textHoliday.TabIndex = 11;
            // 
            // textLateHours
            // 
            this.textLateHours.Location = new System.Drawing.Point(118, 40);
            this.textLateHours.Name = "textLateHours";
            this.textLateHours.Size = new System.Drawing.Size(295, 20);
            this.textLateHours.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Late hours:";
            // 
            // PCDOverTimeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textLateHours);
            this.Controls.Add(this.textOffhours);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textWeekend);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textHoliday);
            this.Name = "PCDOverTimeEditor";
            this.Size = new System.Drawing.Size(420, 115);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textOffhours;
        public System.Windows.Forms.TextBox textWeekend;
        public System.Windows.Forms.TextBox textHoliday;
        public System.Windows.Forms.TextBox textLateHours;
        private System.Windows.Forms.Label label4;
    }
}
