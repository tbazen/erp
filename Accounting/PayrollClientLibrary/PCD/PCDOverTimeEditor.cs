using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client.PCD
{
    public partial class PCDOverTimeEditor : UserControl
    {
        public PCDOverTimeEditor()
        {
            InitializeComponent();
        }

        internal void ClearData()
        {
            textOffhours.Text = "";
            textWeekend.Text = "";
            textHoliday.Text = "";
            textLateHours.Text = "";
        }
    }
}
