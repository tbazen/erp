using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    class PCDRegularTime : IPayrollComponentUIHandler
    {

        #region IPayrollComponentUIHandler Members

        public System.Windows.Forms.Control CreateEditor(IPayrollComponentHanlderHost h)
        {
            return new PCDRegularTimeEditor();
        }

        public bool ConfigureEditor(System.Windows.Forms.Control editor, AppliesToEmployees at)
        {
            if (!at.ASpecificEmployee)
                return false;
            return true;
        }

        public bool SetEditorData(System.Windows.Forms.Control editor, PayrollComponentData dataInfo, object data)
        {
            Data_RegularTime ot = data as Data_RegularTime;
            PCDRegularTimeEditor e = (PCDRegularTimeEditor)editor;
            if (ot == null)
                e.ClearData();
            else
            {
                e.textDays.Text = ot.WorkingDays == 0 ? "" : ot.WorkingDays.ToString();
                e.textHoursPerDay.Text = ot.WorkingHours == 0 ? "" : ot.WorkingHours.ToString();
                e.textPartial.Text = ot.PartialWorkingHour == 0 ? "" : ot.PartialWorkingHour.ToString();
            }
            return true;
        }

        public object GetEditorData(System.Windows.Forms.Control editor)
        {
            Data_RegularTime ot = new Data_RegularTime();
            PCDRegularTimeEditor e = (PCDRegularTimeEditor)editor;
            if (!double.TryParse(e.textDays.Text, out ot.WorkingDays))
                ot.WorkingDays = 0;
            if (!double.TryParse(e.textHoursPerDay.Text, out ot.WorkingHours))
                ot.WorkingHours = 0;
            if (!double.TryParse(e.textPartial.Text, out ot.PartialWorkingHour))
                ot.PartialWorkingHour = 0;
            return ot;
        }

        public bool ValidateEditorData(System.Windows.Forms.Control editor)
        {
            PCDRegularTimeEditor e = (PCDRegularTimeEditor)editor;
            int v;
            if (e.textDays.Text.Trim() != "" && !int.TryParse(e.textDays.Text, out v))
                return false;
            if (e.textHoursPerDay.Text.Trim() != "" && !int.TryParse(e.textHoursPerDay.Text, out v))
                return false;
            if (e.textPartial.Text.Trim() != "" && !int.TryParse(e.textPartial.Text, out v))
                return false;
            return true;
        }


        public bool HaveEditor()
        {
            return true;
        }

        public string GetHTML(PayrollComponentData dataInfo, object aditionaldata)
        {
            if (aditionaldata == null)
                return "Null data";
            Data_RegularTime ot = (Data_RegularTime)aditionaldata;
            string html;
            html = "<b>Off office hours:</b>" + (ot.WorkingDays == 0 ? "none" : (ot.WorkingDays.ToString() + " days."))
                + "<br/><b>Late hours:</b>" + (ot.WorkingHours == 0 ? "none" : (ot.WorkingHours.ToString() + " hour per day"))
                + "<br/><b>Weekend hours:</b>" + (ot.PartialWorkingHour == 0 ? "none" : (ot.PartialWorkingHour.ToString() + " hours"))
                ;
            return html;
        }


        public string GetDefaultHTML(AppliesToEmployees at)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
