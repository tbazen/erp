namespace INTAPS.Payroll.Client.PCD
{
    partial class PCDRegularTimeEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textDays = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textPartial = new System.Windows.Forms.TextBox();
            this.textHoursPerDay = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Days Worked:";
            // 
            // textDays
            // 
            this.textDays.Location = new System.Drawing.Point(118, 14);
            this.textDays.Name = "textDays";
            this.textDays.Size = new System.Drawing.Size(295, 20);
            this.textDays.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Partial days in hours:";
            // 
            // textPartial
            // 
            this.textPartial.Location = new System.Drawing.Point(118, 63);
            this.textPartial.Name = "textPartial";
            this.textPartial.Size = new System.Drawing.Size(294, 20);
            this.textPartial.TabIndex = 9;
            // 
            // textHoursPerDay
            // 
            this.textHoursPerDay.Location = new System.Drawing.Point(118, 40);
            this.textHoursPerDay.Name = "textHoursPerDay";
            this.textHoursPerDay.Size = new System.Drawing.Size(295, 20);
            this.textHoursPerDay.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Hours per Day:";
            // 
            // PCDRegularTimeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textHoursPerDay);
            this.Controls.Add(this.textDays);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textPartial);
            this.Name = "PCDRegularTimeEditor";
            this.Size = new System.Drawing.Size(420, 90);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textDays;
        public System.Windows.Forms.TextBox textPartial;
        public System.Windows.Forms.TextBox textHoursPerDay;
        private System.Windows.Forms.Label label4;
    }
}
