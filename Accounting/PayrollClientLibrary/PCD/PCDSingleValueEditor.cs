using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client.PCD
{
    public partial class PCDSingleValueEditor : UserControl
    {
        public PCDSingleValueEditor()
        {
            InitializeComponent();
        }
        internal void ClearData()
        {
            txtValue.Text = "";
        }
    }
}
