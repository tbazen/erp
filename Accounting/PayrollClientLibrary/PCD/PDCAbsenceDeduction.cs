using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    class PDCAbsenceDeduction : PDCSingleValueComponent
    {
        public override string TypeName
        {
            get
            {
                return "Absence";
            }
        }
        public override string ValueName
        {
            get
            {
                return "No of days";
            }
        }
        public override string FormatValue(double val)
        {
            if (val == 0)
                return "";
            if (Math.Floor(val) == val)
                return val.ToString() + " days absent";
            return val.ToString("0,0") + " days absent"; 
        }

        public override bool TryConvert(string txt, out double val)
        {
            if (txt.Trim() == "")
            {
                val = 0;
                return true;
            }
            if (!double.TryParse(txt, out val))
                return false;
            if (val < 0)
                return false;
            return true;
        }
    }
}
