using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    public class PDCRegularCalculatedDeduction : PCDUINoUI
    {
        public override string GetDefaultHTML(AppliesToEmployees at)
        {
            return "Deduction/addition will be calculated when paryoll is generated.";
        }
    }
}
