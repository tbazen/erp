using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    class PDCRegularDeduction : PDCSingleValueComponent
    {
        public override string TypeName
        {
            get
            {
                return "RegularDeduction";
            }
        }
    }
}
