using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    class PDCSingleValueComponent : IPayrollComponentUIHandler
    {
        public virtual string ValueName
        {
            get
            {
                return "Amount";
            }
        }
        public virtual string TypeName
        {
            get
            {
                return this.GetType().ToString();
            }
        }
        public virtual bool TryConvert(string txt, out double val)
        {
            if (txt.Trim() == "")
            {
                val = 0;
                return false;
            }
            if (!double.TryParse(txt, out val))
                return false;
            return true;
        }
        #region IPayrollComponentUIHandler Members

        public System.Windows.Forms.Control CreateEditor(IPayrollComponentHanlderHost h)
        {
            PCDSingleValueEditor e=new PCDSingleValueEditor();
            e.lblValue.Text = this.ValueName;
            return e;
        }

        public bool ConfigureEditor(System.Windows.Forms.Control editor, AppliesToEmployees at)
        {
            if (!at.ASpecificEmployee)
                return false;
            PCDSingleValueEditor e = (PCDSingleValueEditor)editor;
            return true;
        }
        public virtual string FormatValue(double val)
        {
            if (val == 0)
                return "";
            return val.ToString("0,0.00") + " Birrs";
        }
        public bool SetEditorData(System.Windows.Forms.Control editor, PayrollComponentData dataInfo, object data)
        {
            SingleValueData ded = data as SingleValueData;
            PCDSingleValueEditor e = (PCDSingleValueEditor)editor;
            if (ded == null)
                e.ClearData();
            else
            {
                e.txtValue.Text = ded.value == 0 ? "" : ded.value.ToString("0,0.00");
            }
            return true;
        }

        public object GetEditorData(System.Windows.Forms.Control editor)
        {
            SingleValueData ded = new SingleValueData();
            PCDSingleValueEditor e = (PCDSingleValueEditor)editor;
            this.TryConvert(e.txtValue.Text, out ded.value);
            return ded;
        }

        public bool ValidateEditorData(System.Windows.Forms.Control editor)
        {
            PCDSingleValueEditor e = (PCDSingleValueEditor)editor;
            double v;
            return TryConvert(e.txtValue.Text, out v);
        }


        public bool HaveEditor()
        {
            return true;
        }

        public string GetHTML(PayrollComponentData dataInfo, object aditionaldata)
        {
            if (aditionaldata == null)
                return "Null data";
            SingleValueData dat = (SingleValueData)aditionaldata;
            string html;
            html = "<b>"+System.Web.HttpUtility.HtmlEncode(this.ValueName) +":</b>" + System.Web.HttpUtility.HtmlEncode(this.FormatValue(dat.value));
            return html;
        }


        public string GetDefaultHTML(AppliesToEmployees at)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
