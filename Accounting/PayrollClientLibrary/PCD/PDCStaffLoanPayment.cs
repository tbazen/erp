using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;

namespace INTAPS.Payroll.Client.PCD
{
    class PDCStaffLoanPayment : IPayrollComponentUIHandler
    {

        #region IPayrollComponentUIHandler Members

        public System.Windows.Forms.Control CreateEditor(IPayrollComponentHanlderHost h)
        {
            PDCStaffLoanPaymentEditor ret= new PDCStaffLoanPaymentEditor();
            SetupEditor(ret, null);
            return ret;
        }

        public bool ConfigureEditor(System.Windows.Forms.Control editor, AppliesToEmployees at)
        {
            if (!at.ASpecificEmployee)
                return false;
            PDCStaffLoanPaymentEditor e = editor as PDCStaffLoanPaymentEditor;
            e.Configure(0, PayrollClient.GetCreditBalance(at.Employees[0], true), PayrollClient.GetCreditBalance(at.Employees[0], false));
            return true;
        }
        void SetupEditor(PDCStaffLoanPaymentEditor e, StaffLoanPaymentData loanData)
        {
            
        }
        public bool SetEditorData(System.Windows.Forms.Control editor, PayrollComponentData dataInfo, object data)
        {
            if(!dataInfo.at.ASpecificEmployee)
                return false;
            StaffLoanPaymentData loanData = data as StaffLoanPaymentData;
            PDCStaffLoanPaymentEditor e = (PDCStaffLoanPaymentEditor)editor;
            if (loanData == null)
                e.ClearData();
            else
            {
                e.SetStaffLoan(loanData);
            }
            return true;
        }

        public object GetEditorData(System.Windows.Forms.Control editor)
        {
            StaffLoanPaymentData loanData = new StaffLoanPaymentData();
            PDCStaffLoanPaymentEditor e = (PDCStaffLoanPaymentEditor)editor;
            e.GetStaffLoan(loanData);
            return loanData;
        }

        public bool ValidateEditorData(System.Windows.Forms.Control editor)
        {
            StaffLoanPaymentData loanData = new StaffLoanPaymentData();
            PDCStaffLoanPaymentEditor e = (PDCStaffLoanPaymentEditor)editor;
            return e.GetStaffLoan(loanData);
        }


        public bool HaveEditor()
        {
            return true;
        }

        public string GetHTML(PayrollComponentData dataInfo, object aditionaldata)
        {
            if (aditionaldata == null)
                return "Null data";
            StaffLoanPaymentData loanData = (StaffLoanPaymentData)aditionaldata;
            string html;
            if (loanData.Exception)
                html = "<b>Exceptional return of amount:" + loanData.Amount.ToString("0,0.00");
            else
                html = "<b>Regular return of amount:" + loanData.Amount.ToString("0,0.00");
            return html;
        }


        public string GetDefaultHTML(AppliesToEmployees at)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
