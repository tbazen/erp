namespace INTAPS.Payroll.Client.PCD
{
    partial class PDCStaffLoanPaymentEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtLoan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtReturn = new System.Windows.Forms.TextBox();
            this.chkExceptionalReturn = new System.Windows.Forms.CheckBox();
            this.chkNoPay = new System.Windows.Forms.CheckBox();
            this.txtExceptionalReturn = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Loan Balance";
            // 
            // txtLoan
            // 
            this.txtLoan.BackColor = System.Drawing.Color.White;
            this.txtLoan.Location = new System.Drawing.Point(89, 4);
            this.txtLoan.Name = "txtLoan";
            this.txtLoan.ReadOnly = true;
            this.txtLoan.Size = new System.Drawing.Size(257, 20);
            this.txtLoan.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Monthly Return";
            // 
            // txtReturn
            // 
            this.txtReturn.BackColor = System.Drawing.Color.White;
            this.txtReturn.Location = new System.Drawing.Point(89, 30);
            this.txtReturn.Name = "txtReturn";
            this.txtReturn.Size = new System.Drawing.Size(257, 20);
            this.txtReturn.TabIndex = 2;
            // 
            // chkExceptionalReturn
            // 
            this.chkExceptionalReturn.AutoSize = true;
            this.chkExceptionalReturn.Location = new System.Drawing.Point(7, 71);
            this.chkExceptionalReturn.Name = "chkExceptionalReturn";
            this.chkExceptionalReturn.Size = new System.Drawing.Size(73, 17);
            this.chkExceptionalReturn.TabIndex = 3;
            this.chkExceptionalReturn.Text = "Exception";
            this.chkExceptionalReturn.UseVisualStyleBackColor = true;
            this.chkExceptionalReturn.CheckedChanged += new System.EventHandler(this.chkExceptionalReturn_CheckedChanged);
            // 
            // chkNoPay
            // 
            this.chkNoPay.AutoSize = true;
            this.chkNoPay.Location = new System.Drawing.Point(27, 94);
            this.chkNoPay.Name = "chkNoPay";
            this.chkNoPay.Size = new System.Drawing.Size(71, 17);
            this.chkNoPay.TabIndex = 3;
            this.chkNoPay.Text = "Don\'t pay";
            this.chkNoPay.UseVisualStyleBackColor = true;
            this.chkNoPay.CheckedChanged += new System.EventHandler(this.chkExceptionalReturn_CheckedChanged);
            // 
            // txtExceptionalReturn
            // 
            this.txtExceptionalReturn.BackColor = System.Drawing.Color.White;
            this.txtExceptionalReturn.Location = new System.Drawing.Point(89, 71);
            this.txtExceptionalReturn.Name = "txtExceptionalReturn";
            this.txtExceptionalReturn.Size = new System.Drawing.Size(257, 20);
            this.txtExceptionalReturn.TabIndex = 2;
            // 
            // PDCStaffLoanPaymentEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkNoPay);
            this.Controls.Add(this.chkExceptionalReturn);
            this.Controls.Add(this.txtExceptionalReturn);
            this.Controls.Add(this.txtReturn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLoan);
            this.Controls.Add(this.label1);
            this.Name = "PDCStaffLoanPaymentEditor";
            this.Size = new System.Drawing.Size(357, 124);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLoan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtReturn;
        private System.Windows.Forms.CheckBox chkExceptionalReturn;
        private System.Windows.Forms.CheckBox chkNoPay;
        private System.Windows.Forms.TextBox txtExceptionalReturn;
    }
}
