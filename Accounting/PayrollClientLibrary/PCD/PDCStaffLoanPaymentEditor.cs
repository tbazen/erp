using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client.PCD
{
    public partial class PDCStaffLoanPaymentEditor : UserControl
    {
        bool NoLoan;
        public PDCStaffLoanPaymentEditor()
        {
            InitializeComponent();
            chkExceptionalReturn_CheckedChanged(null, null);
        }

        private void chkExceptionalReturn_CheckedChanged(object sender, EventArgs e)
        {
            txtExceptionalReturn.Visible = chkExceptionalReturn.Checked && !chkNoPay.Checked;
            chkNoPay.Visible = chkExceptionalReturn.Checked;
        }
        public void Configure(double defaultReturn, double longTerm,double advance)
        {
            NoLoan = longTerm== 0 && advance==0;
            txtLoan.Text = "Long term:" + longTerm.ToString("0,0.00") + " Advance:" + advance.ToString("0,0.00");
            txtReturn.Text = defaultReturn.ToString("0,0.00");
        }
        public void SetStaffLoan(StaffLoanPaymentData d)
        {
            if (d == null)
            {
                chkExceptionalReturn.Checked = false;
            }
            else
            {
                txtReturn.Text = d.Amount.ToString();
                chkExceptionalReturn.Checked = d.Exception;
                chkNoPay.Checked = d.Amount == 0;
                if (d.Exception && d.Amount > 0)
                    txtExceptionalReturn.Text = d.Amount.ToString("0,0.00");
            }
        }
        public bool GetStaffLoan(StaffLoanPaymentData dref)
        {
            if (NoLoan)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("There is no active loan.");
                return false;
            }
            double val;
            if (chkExceptionalReturn.Checked)
            {
                dref.Exception = true;
                if (chkNoPay.Checked)
                    dref.Amount = 0;
                else
                {
                    if (!double.TryParse(txtExceptionalReturn.Text, out val) || val<=0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter valid return amount.");
                        return false;
                    }
                    dref.Amount = val;
                }
                
            }
            else
            {
                dref.Amount = double.Parse(txtReturn.Text);
                dref.Exception = false;
            }
            return true;
        }

        internal void ClearData()
        {
            NoLoan = false;
            txtLoan.Text = "";
            chkExceptionalReturn.Checked = false;
        }
    }
}
