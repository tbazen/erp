namespace INTAPS.Payroll.Client
{
    partial class GeneratePayroll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GeneratePayroll));
            this.txtErrorInfo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.pbGenerate = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPeriod = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDate = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.lvErrors = new System.Windows.Forms.ListView();
            this.colName = new System.Windows.Forms.ColumnHeader();
            this.colError = new System.Windows.Forms.ColumnHeader();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.employeeDomain = new INTAPS.Payroll.Client.EmployeeDomainControl();
            this.SuspendLayout();
            // 
            // txtErrorInfo
            // 
            this.txtErrorInfo.BackColor = System.Drawing.Color.White;
            this.txtErrorInfo.ForeColor = System.Drawing.Color.Red;
            this.txtErrorInfo.Location = new System.Drawing.Point(282, 207);
            this.txtErrorInfo.Multiline = true;
            this.txtErrorInfo.Name = "txtErrorInfo";
            this.txtErrorInfo.ReadOnly = true;
            this.txtErrorInfo.Size = new System.Drawing.Size(415, 137);
            this.txtErrorInfo.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(276, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Errors";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(622, 350);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(75, 23);
            this.btnGenerate.TabIndex = 7;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // pbGenerate
            // 
            this.pbGenerate.Location = new System.Drawing.Point(10, 350);
            this.pbGenerate.Name = "pbGenerate";
            this.pbGenerate.Size = new System.Drawing.Size(606, 23);
            this.pbGenerate.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Error Detail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Period:";
            // 
            // txtPeriod
            // 
            this.txtPeriod.BackColor = System.Drawing.Color.White;
            this.txtPeriod.ForeColor = System.Drawing.Color.Black;
            this.txtPeriod.Location = new System.Drawing.Point(61, 49);
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.ReadOnly = true;
            this.txtPeriod.Size = new System.Drawing.Size(212, 20);
            this.txtPeriod.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date:";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(61, 4);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(168, 20);
            this.dtpDate.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpDate.TabIndex = 10;
            // 
            // lvErrors
            // 
            this.lvErrors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colError});
            this.lvErrors.ForeColor = System.Drawing.Color.Red;
            this.lvErrors.FullRowSelect = true;
            this.lvErrors.Location = new System.Drawing.Point(279, 20);
            this.lvErrors.Name = "lvErrors";
            this.lvErrors.Size = new System.Drawing.Size(418, 168);
            this.lvErrors.TabIndex = 11;
            this.lvErrors.UseCompatibleStateImageBehavior = false;
            this.lvErrors.View = System.Windows.Forms.View.Details;
            this.lvErrors.SelectedIndexChanged += new System.EventHandler(this.lvErrors_SelectedIndexChanged);
            // 
            // colName
            // 
            this.colName.Text = "Employee";
            this.colName.Width = 159;
            // 
            // colError
            // 
            this.colError.Text = "Error";
            this.colError.Width = 152;
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // employeeDomain
            // 
            this.employeeDomain.Location = new System.Drawing.Point(-2, 75);
            this.employeeDomain.Name = "employeeDomain";
            this.employeeDomain.Size = new System.Drawing.Size(275, 269);
            this.employeeDomain.TabIndex = 3;
            // 
            // GeneratePayroll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 377);
            this.Controls.Add(this.lvErrors);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pbGenerate);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPeriod);
            this.Controls.Add(this.txtErrorInfo);
            this.Controls.Add(this.employeeDomain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GeneratePayroll";
            this.Text = "Generate Payroll";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private EmployeeDomainControl employeeDomain;
        private System.Windows.Forms.TextBox txtErrorInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.ProgressBar pbGenerate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPeriod;
        private System.Windows.Forms.Label label4;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpDate;
        private System.Windows.Forms.ListView lvErrors;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colError;
        private System.Windows.Forms.Timer timer1;

    }
}