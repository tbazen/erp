using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace INTAPS.Payroll.Client
{
    public partial class GeneratePayroll : Form
    {
        PayrollPeriod m_pp;
        BatchError m_err = null;
        bool m_generating = false;
        public GeneratePayroll()
        {
            InitializeComponent();
            dtpDate.Value = DateTime.Now;
            employeeDomain.LoadData();
        }
        public GeneratePayroll(PayrollPeriod pp)
            : this()
        {
            m_pp=pp;
            txtPeriod.Text = m_pp.name;
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (m_generating)
            {
                try
                {
                    timer1.Enabled = false;
                    btnGenerate.Text = "Generate";
                    PayrollClient.StopGenerating();
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to stop payroll generation.", ex);
                }
                m_generating = false;
            }
            else
            {
                m_generating = true;
                try
                {
                    btnGenerate.Text = "Stop";
                    PayrollClient.GeneratePayroll(dtpDate.Value, employeeDomain.GetData(), m_pp.id);
                    pbGenerate.Value = 100;
                    timer1.Enabled = true;
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to generate payroll.", ex);
                }
            }

        }

        private void lvErrors_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvErrors.SelectedItems.Count == 0)
                txtErrorInfo.Text = "";
            else
            {
                Exception ex = lvErrors.SelectedItems[0].Tag as Exception;
                if (ex == null)
                    txtErrorInfo.Text = "";
                else
                {
                    txtErrorInfo.Text = ex.Message + "\n" + ex.StackTrace;
                }
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (!PayrollClient.IsBusy())
                {
                    timer1.Enabled = false;
                    btnGenerate.Text = "Generate";
                    pbGenerate.Value = 0;
                    m_err = PayrollClient.GetLastError();
                    lvErrors.Items.Clear();
                    for (int i = 0; i < m_err.ObjectID.Length; i++)
                    {
                        Employee emp = PayrollClient.GetEmployee(m_err.ObjectID[i]);
                        ListViewItem li = new ListViewItem(emp.employeeName + " - " + emp.employeeID);
                        li.SubItems.Add(m_err.ErrorDesc[i]);
                        li.Tag = m_err.SystemException[i];
                        lvErrors.Items.Add(li);
                    }
                    if (m_err.ObjectID.Length == 0)
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Payroll generated without error.");
                    else
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Payroll generated with " + m_err.ObjectID.Length + " errors.");

                }
                else
                {
                    int m = PayrollClient.GetProgressMax();
                    int c = PayrollClient.GetProgress();
                    if (m > 0)
                        pbGenerate.Value = c * 100 / m;
                }
            }
            catch (Exception ex)
            {
                timer1.Enabled = false;
                btnGenerate.Text = "Generate";
                pbGenerate.Value = 0;
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error waiting for payroll generation to finish", ex);
            }
            
        }
    }
}