namespace INTAPS.Payroll.Client
{
    partial class PayrollBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Not posed", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Posted", System.Windows.Forms.HorizontalAlignment.Left);
            this.spltPayrolld = new System.Windows.Forms.SplitContainer();
            this.lvPayrolls = new System.Windows.Forms.ListView();
            this.colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colAmount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cxtPayroll = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miPostPayroll = new System.Windows.Forms.ToolStripMenuItem();
            this.miSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.miInvertSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.wbPayroll = new INTAPS.UI.HTML.ControlBrowser();
            this.bowserController = new INTAPS.UI.HTML.BowserController();
            this.cmbViewMode = new System.Windows.Forms.ToolStripComboBox();
            this.cmbSheetFormat = new System.Windows.Forms.ToolStripComboBox();
            this.spltPayrolld.Panel1.SuspendLayout();
            this.spltPayrolld.Panel2.SuspendLayout();
            this.spltPayrolld.SuspendLayout();
            this.cxtPayroll.SuspendLayout();
            this.bowserController.SuspendLayout();
            this.SuspendLayout();
            // 
            // spltPayrolld
            // 
            this.spltPayrolld.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltPayrolld.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spltPayrolld.Location = new System.Drawing.Point(0, 0);
            this.spltPayrolld.Name = "spltPayrolld";
            this.spltPayrolld.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltPayrolld.Panel1
            // 
            this.spltPayrolld.Panel1.Controls.Add(this.lvPayrolls);
            // 
            // spltPayrolld.Panel2
            // 
            this.spltPayrolld.Panel2.Controls.Add(this.wbPayroll);
            this.spltPayrolld.Panel2.Controls.Add(this.bowserController);
            this.spltPayrolld.Size = new System.Drawing.Size(600, 422);
            this.spltPayrolld.SplitterDistance = 235;
            this.spltPayrolld.TabIndex = 1;
            // 
            // lvPayrolls
            // 
            this.lvPayrolls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colID,
            this.colName,
            this.colAmount});
            this.lvPayrolls.ContextMenuStrip = this.cxtPayroll;
            this.lvPayrolls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPayrolls.FullRowSelect = true;
            listViewGroup1.Header = "Not posed";
            listViewGroup1.Name = "grUnposted";
            listViewGroup2.Header = "Posted";
            listViewGroup2.Name = "grPosted";
            this.lvPayrolls.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2});
            this.lvPayrolls.HideSelection = false;
            this.lvPayrolls.Location = new System.Drawing.Point(0, 0);
            this.lvPayrolls.Name = "lvPayrolls";
            this.lvPayrolls.Size = new System.Drawing.Size(600, 235);
            this.lvPayrolls.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvPayrolls.TabIndex = 0;
            this.lvPayrolls.UseCompatibleStateImageBehavior = false;
            this.lvPayrolls.View = System.Windows.Forms.View.Details;
            this.lvPayrolls.SelectedIndexChanged += new System.EventHandler(this.lvPayrolls_SelectedIndexChanged);
            // 
            // colID
            // 
            this.colID.Text = "ID";
            this.colID.Width = 96;
            // 
            // colName
            // 
            this.colName.Text = "Employee";
            this.colName.Width = 206;
            // 
            // colAmount
            // 
            this.colAmount.Text = "Net Pay";
            this.colAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.colAmount.Width = 88;
            // 
            // cxtPayroll
            // 
            this.cxtPayroll.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miDelete,
            this.miPostPayroll,
            this.miSelectAll,
            this.miInvertSelection});
            this.cxtPayroll.Name = "cxtPayroll";
            this.cxtPayroll.Size = new System.Drawing.Size(156, 92);
            this.cxtPayroll.Opening += new System.ComponentModel.CancelEventHandler(this.cxtPayroll_Opening);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(155, 22);
            this.miDelete.Text = "Delete Payrolls";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // miPostPayroll
            // 
            this.miPostPayroll.Name = "miPostPayroll";
            this.miPostPayroll.Size = new System.Drawing.Size(155, 22);
            this.miPostPayroll.Text = "Post Payrolls";
            this.miPostPayroll.Click += new System.EventHandler(this.miPostPayroll_Click);
            // 
            // miSelectAll
            // 
            this.miSelectAll.Name = "miSelectAll";
            this.miSelectAll.Size = new System.Drawing.Size(155, 22);
            this.miSelectAll.Text = "Select All";
            this.miSelectAll.Click += new System.EventHandler(this.miSelectAll_Click);
            // 
            // miInvertSelection
            // 
            this.miInvertSelection.Name = "miInvertSelection";
            this.miInvertSelection.Size = new System.Drawing.Size(155, 22);
            this.miInvertSelection.Text = "Invert Selection";
            this.miInvertSelection.Click += new System.EventHandler(this.miInvertSelection_Click);
            // 
            // wbPayroll
            // 
            this.wbPayroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbPayroll.Location = new System.Drawing.Point(0, 25);
            this.wbPayroll.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbPayroll.Name = "wbPayroll";
            this.wbPayroll.Size = new System.Drawing.Size(600, 158);
            this.wbPayroll.StyleSheetFile = "berp.css";
            this.wbPayroll.TabIndex = 0;
            // 
            // bowserController
            // 
            this.bowserController.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmbViewMode,
            this.cmbSheetFormat});
            this.bowserController.Location = new System.Drawing.Point(0, 0);
            this.bowserController.Name = "bowserController";
            this.bowserController.ShowBackForward = false;
            this.bowserController.ShowRefresh = false;
            this.bowserController.Size = new System.Drawing.Size(600, 25);
            this.bowserController.TabIndex = 3;
            this.bowserController.Text = "bowserController1";
            // 
            // cmbViewMode
            // 
            this.cmbViewMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbViewMode.Items.AddRange(new object[] {
            "Payroll Sheet View",
            "Payroll Slip View"});
            this.cmbViewMode.Name = "cmbViewMode";
            this.cmbViewMode.Size = new System.Drawing.Size(121, 25);
            this.cmbViewMode.SelectedIndexChanged += new System.EventHandler(this.tsViewMode_SelectedIndexChanged);
            this.cmbViewMode.Click += new System.EventHandler(this.tsViewMode_Click);
            // 
            // cmbSheetFormat
            // 
            this.cmbSheetFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSheetFormat.Items.AddRange(new object[] {
            "Format 1",
            "Format 2",
            "Format 3"});
            this.cmbSheetFormat.Name = "cmbSheetFormat";
            this.cmbSheetFormat.Size = new System.Drawing.Size(121, 25);
            this.cmbSheetFormat.SelectedIndexChanged += new System.EventHandler(this.cmbSheetFormat_SelectedIndexChanged);
            // 
            // PayrollBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 422);
            this.Controls.Add(this.spltPayrolld);
            this.Name = "PayrollBrowser";
            this.Text = "Payrolls";
            this.spltPayrolld.Panel1.ResumeLayout(false);
            this.spltPayrolld.Panel2.ResumeLayout(false);
            this.spltPayrolld.Panel2.PerformLayout();
            this.spltPayrolld.ResumeLayout(false);
            this.cxtPayroll.ResumeLayout(false);
            this.bowserController.ResumeLayout(false);
            this.bowserController.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spltPayrolld;
        private System.Windows.Forms.ListView lvPayrolls;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colAmount;
        private System.Windows.Forms.ContextMenuStrip cxtPayroll;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.ToolStripMenuItem miSelectAll;
        private System.Windows.Forms.ToolStripMenuItem miPostPayroll;
        private System.Windows.Forms.ToolStripMenuItem miInvertSelection;
        private INTAPS.UI.HTML.ControlBrowser wbPayroll;
        private System.Windows.Forms.ColumnHeader colID;
        private INTAPS.UI.HTML.BowserController bowserController;
        private System.Windows.Forms.ToolStripComboBox cmbViewMode;
        private System.Windows.Forms.ToolStripComboBox cmbSheetFormat;
    }
}