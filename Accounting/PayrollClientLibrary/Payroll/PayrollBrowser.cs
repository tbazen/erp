using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class PayrollBrowser : Form, IPayrollContentForm
    {
        PayrollPeriod m_pp = null;
        Employee m_emp = null;
        OrgUnit m_orgUnit = null;
        int m_format;
        public PayrollBrowser()
        {
            InitializeComponent();
            bowserController.SetBrowser(wbPayroll);
            cmbViewMode.SelectedIndex = 0;
            cmbSheetFormat.SelectedIndex = 0;
        }

        #region ISpreadContentForm Members

        public bool ShowEmbeded
        {
            get { return true; }
        }

        public bool SupportStandardCommand(MainFormStandardButtons cmd)
        {
            return cmd == MainFormStandardButtons.Refresh;
        }
        bool m_ignoreEvents = false;
        void LoadPayroll()
        {
            m_ignoreEvents = true;
            try
            {
            lvPayrolls.Items.Clear();
            MainForm.SetStatus("");
            if(m_emp==null && m_orgUnit==null)
            {
                wbPayroll.DocumentText="Select an employee or an organizational unit.";
                return;
            }
            if(m_pp==null)
            {
                wbPayroll.DocumentText="Select period.";
                return;
            }

            
                PayrollDocument[] prs;
                if (m_emp != null)
                {
                    prs = PayrollClient.GetPayrolls(new AppliesToEmployees(m_emp.ObjectIDVal, true), m_pp.id);
                }
                else
                    prs = PayrollClient.GetPayrolls(new AppliesToEmployees(m_orgUnit.ObjectIDVal, false), m_pp.id);
                if (prs.Length == 0)
                    wbPayroll.DocumentText = "No payroll found.";
                foreach (PayrollDocument pd in prs)
                {
                    ListViewItem li = new ListViewItem(pd.employee.employeeID);
                    li.SubItems.Add(pd.employee.employeeName);
                    double NetPay = pd.NetPay;
                    li.SubItems.Add(NetPay == 0 ? "-" : NetPay.ToString("0,0.00"));
                    li.Tag = pd;
                    if (pd.Posted)
                        li.Group = lvPayrolls.Groups[1];
                    else
                        li.Group = lvPayrolls.Groups[0];
                    lvPayrolls.Items.Add(li);
                }
                if (lvPayrolls.Items.Count > 0)
                {
                    lvPayrolls.Items[0].Selected = true;
                }
                BuildSheet();
                MainForm.SetStatus(lvPayrolls.Items.Count + " Payrolls (" + lvPayrolls.Groups[1].Items.Count + " posted)");
            }
            catch (Exception ex)
            {
                wbPayroll.DocumentText = System.Web.HttpUtility.HtmlEncode(ex.Message);
            }
            finally
            {
                m_ignoreEvents = false;
            }
            lvPayrolls_SelectedIndexChanged(null, null);
        }

        private void BuildSheet()
        {
            if (cmbViewMode.SelectedIndex == 0)
            {
                try
                {
                    PayrollDocument[] docs = new PayrollDocument[lvPayrolls.Items.Count];
                    int i = 0;
                    foreach (ListViewItem li in lvPayrolls.Items)
                    {
                        docs[i++] = (PayrollDocument)li.Tag;
                    }
                    wbPayroll.LoadTextPage("Payroll List", PayrollClient.GetPayrollTableHTML(new PayrollSetDocument(docs, ""), m_pp.id, m_format));

                }
                catch (Exception ex)
                {
                    wbPayroll.DocumentText = System.Web.HttpUtility.HtmlEncode(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        public void FilterByEmp(Employee e, OrgUnit parent)
        {
            m_emp = e;
            LoadPayroll();
        }

        public void FilterByOrg(OrgUnit o)
        {
            m_orgUnit = o;
            m_emp = null;
            LoadPayroll();
        }

        public void FilterByPCD(PayrollComponentDefination pcd)
        {
            
        }

        public void FilterByFormula(PayrollComponentFormula formula, PayrollComponentDefination parent)
        {
            
        }

        public void FilterByPayPeriod(PayrollPeriod pp)
        {
            m_pp = pp;
            LoadPayroll();
        }

        public void DoStandardCommand(MainFormStandardButtons cmd)
        {
            if (cmd == MainFormStandardButtons.Refresh)
            {
                m_format = 0;
                LoadPayroll();
            }
        }

        #endregion

        #region ISpreadContentForm Members


        public Control GetControl()
        {
            return this;
        }


        public string GetStandardCommandText(MainFormStandardButtons cmd)
        {
            if (MainFormStandardButtons.Refresh == cmd)
                return "Refresh";
            return "";
        }

       #endregion
        private void lvPayrolls_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_ignoreEvents)
                return;
            
            try
            {
                if (cmbViewMode.SelectedIndex == 1)
                {
                    if (lvPayrolls.SelectedItems.Count == 0)
                    {
                        wbPayroll.DocumentText = "";
                        return;
                    }

                    PayrollDocument[] docs = new PayrollDocument[lvPayrolls.SelectedItems.Count];
                    int i = 0;
                    foreach (ListViewItem li in lvPayrolls.SelectedItems)
                    {
                        docs[i++] = (PayrollDocument)li.Tag;
                    }
                    wbPayroll.LoadTextPage("Payroll List", PayrollClient.GetPayrollSlipTableHTML(docs));
                }
            }
            catch (Exception ex)
            {
                wbPayroll.DocumentText = System.Web.HttpUtility.HtmlEncode(ex.Message + "\n" + ex.StackTrace);
            }
        }

        private void cxtPayroll_Opening(object sender, CancelEventArgs e)
        {
            miDelete.Enabled = lvPayrolls.SelectedItems.Count > 0;
        }

        private void miSelectAll_Click(object sender, EventArgs e)
        {
            m_ignoreEvents = true;
            try
            {
                foreach (ListViewItem li in lvPayrolls.Items)
                    li.Selected = true;
            }
            finally
            {
                m_ignoreEvents = false;
            }
            lvPayrolls_SelectedIndexChanged(null, null);
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected payrolls?"))
            {
                m_ignoreEvents = true;
                try
                {
                    ListView.SelectedListViewItemCollection selected = lvPayrolls.SelectedItems;
                    for (int i = selected.Count - 1; i >= 0; i--)
                    {
                        ListViewItem li = selected[i];
                        PayrollDocument d = (PayrollDocument)li.Tag;
                        PayrollClient.DeletePayroll(d.employee.ObjectIDVal, d.payPeriod.id);
                        li.Remove();
                    }
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException
                        ("Error deleting paryolls", ex);
                }
                finally
                {
                    m_ignoreEvents = false;
                }
                lvPayrolls_SelectedIndexChanged(null, null);
            }
        }

        private void miPostPayroll_Click(object sender, EventArgs e)
        {
            if (lvPayrolls.SelectedItems.Count == 0)
                return;
            PayrollDocument[] pd = new PayrollDocument[lvPayrolls.SelectedItems.Count];
            int i = 0;
            foreach (ListViewItem li in lvPayrolls.SelectedItems)
            {
                pd[i] = li.Tag as PayrollDocument;
                i++;
            }
            PostPayroll pp = new PostPayroll(pd);
            if (pp.ShowDialog() == DialogResult.OK)
            {
                LoadPayroll();
            }
            
        }

        private void miInvertSelection_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in lvPayrolls.Items)
                li.Selected = !li.Selected;
        }

      

        private void tsViewMode_Click(object sender, EventArgs e)
        {

        }

        private void tsViewMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbViewMode.SelectedIndex == 0)
            {
                BuildSheet();
                cmbSheetFormat.Visible = true;
            }
            else
            {
                lvPayrolls_SelectedIndexChanged(null, null);
                cmbSheetFormat.Visible = false;
            }
        }

        private void cmbSheetFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_format = cmbSheetFormat.SelectedIndex;
            BuildSheet();

        }

    }
}