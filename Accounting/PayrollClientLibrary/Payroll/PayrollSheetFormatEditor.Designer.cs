namespace INTAPS.Payroll.Client
{
    partial class PayrollSheetFormatEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Columns");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayrollSheetFormatEditor));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.splitColumns = new System.Windows.Forms.SplitContainer();
            this.btnAutoVar = new System.Windows.Forms.Button();
            this.treeColumn = new System.Windows.Forms.TreeView();
            this.btnDown = new System.Windows.Forms.Button();
            this.buttonAddRoot = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnRomove = new System.Windows.Forms.Button();
            this.btnAddPCDCol = new System.Windows.Forms.Button();
            this.pcdTree = new INTAPS.Payroll.Client.PCDTree();
            this.checkRepeatHeaders = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.numSortOrder = new System.Windows.Forms.NumericUpDown();
            this.comboSort = new System.Windows.Forms.ComboBox();
            this.checkCalculateTotal = new System.Windows.Forms.CheckBox();
            this.checkVisible = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textFooterStyle = new System.Windows.Forms.TextBox();
            this.textBodyStyle = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textHeaderStyle = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textFormula = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textColumnName = new System.Windows.Forms.TextBox();
            this.textNumberFormat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textColumn = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.payrollPeriodSelector = new INTAPS.Payroll.Client.PayrollPeriodSelector();
            this.buttonEvalue = new System.Windows.Forms.Button();
            this.listValues = new System.Windows.Forms.ListView();
            this.VariableName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.employeePlaceholder = new INTAPS.Payroll.Client.EmployeePlaceHolder();
            this.textNColPageBreak = new System.Windows.Forms.TextBox();
            this.textSheetName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textFooter = new System.Windows.Forms.TextBox();
            this.textHeader = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlDialog = new System.Windows.Forms.Panel();
            this.cmbFormat = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitColumns)).BeginInit();
            this.splitColumns.Panel1.SuspendLayout();
            this.splitColumns.Panel2.SuspendLayout();
            this.splitColumns.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSortOrder)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.pnlDialog.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.splitColumns);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.checkRepeatHeaders);
            this.splitContainer.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer.Panel2.Controls.Add(this.textNColPageBreak);
            this.splitContainer.Panel2.Controls.Add(this.textSheetName);
            this.splitContainer.Panel2.Controls.Add(this.label13);
            this.splitContainer.Panel2.Controls.Add(this.label6);
            this.splitContainer.Panel2.Controls.Add(this.textFooter);
            this.splitContainer.Panel2.Controls.Add(this.textHeader);
            this.splitContainer.Panel2.Controls.Add(this.label14);
            this.splitContainer.Panel2.Controls.Add(this.label5);
            this.splitContainer.Panel2.Controls.Add(this.label4);
            this.splitContainer.Size = new System.Drawing.Size(983, 542);
            this.splitContainer.SplitterDistance = 321;
            this.splitContainer.TabIndex = 0;
            // 
            // splitColumns
            // 
            this.splitColumns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitColumns.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitColumns.Location = new System.Drawing.Point(0, 0);
            this.splitColumns.Name = "splitColumns";
            this.splitColumns.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitColumns.Panel1
            // 
            this.splitColumns.Panel1.Controls.Add(this.btnAutoVar);
            this.splitColumns.Panel1.Controls.Add(this.treeColumn);
            this.splitColumns.Panel1.Controls.Add(this.btnDown);
            this.splitColumns.Panel1.Controls.Add(this.buttonAddRoot);
            this.splitColumns.Panel1.Controls.Add(this.btnAdd);
            this.splitColumns.Panel1.Controls.Add(this.btnUp);
            this.splitColumns.Panel1.Controls.Add(this.btnRomove);
            // 
            // splitColumns.Panel2
            // 
            this.splitColumns.Panel2.Controls.Add(this.btnAddPCDCol);
            this.splitColumns.Panel2.Controls.Add(this.pcdTree);
            this.splitColumns.Size = new System.Drawing.Size(321, 542);
            this.splitColumns.SplitterDistance = 219;
            this.splitColumns.TabIndex = 4;
            // 
            // btnAutoVar
            // 
            this.btnAutoVar.Location = new System.Drawing.Point(281, 154);
            this.btnAutoVar.Name = "btnAutoVar";
            this.btnAutoVar.Size = new System.Drawing.Size(36, 23);
            this.btnAutoVar.TabIndex = 4;
            this.btnAutoVar.Text = "A";
            this.btnAutoVar.UseVisualStyleBackColor = true;
            this.btnAutoVar.Click += new System.EventHandler(this.btnAutoVar_Click);
            // 
            // treeColumn
            // 
            this.treeColumn.AccessibleRole = System.Windows.Forms.AccessibleRole.Alert;
            this.treeColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeColumn.HideSelection = false;
            this.treeColumn.Location = new System.Drawing.Point(0, 25);
            this.treeColumn.Name = "treeColumn";
            treeNode1.Name = "nodeRoot";
            treeNode1.Text = "Columns";
            this.treeColumn.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.treeColumn.Size = new System.Drawing.Size(275, 191);
            this.treeColumn.TabIndex = 0;
            this.treeColumn.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeColumn_AfterSelect);
            // 
            // btnDown
            // 
            this.btnDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDown.Location = new System.Drawing.Point(281, 67);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(37, 23);
            this.btnDown.TabIndex = 3;
            this.btnDown.Text = "v";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // buttonAddRoot
            // 
            this.buttonAddRoot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddRoot.Location = new System.Drawing.Point(3, 3);
            this.buttonAddRoot.Name = "buttonAddRoot";
            this.buttonAddRoot.Size = new System.Drawing.Size(75, 23);
            this.buttonAddRoot.TabIndex = 3;
            this.buttonAddRoot.Text = "Unselect";
            this.buttonAddRoot.UseVisualStyleBackColor = true;
            this.buttonAddRoot.Click += new System.EventHandler(this.buttonAddRoot_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(281, 96);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(37, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "+";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUp
            // 
            this.btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUp.Location = new System.Drawing.Point(281, 43);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(37, 23);
            this.btnUp.TabIndex = 3;
            this.btnUp.Text = "^";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnRomove
            // 
            this.btnRomove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRomove.Location = new System.Drawing.Point(281, 14);
            this.btnRomove.Name = "btnRomove";
            this.btnRomove.Size = new System.Drawing.Size(37, 23);
            this.btnRomove.TabIndex = 3;
            this.btnRomove.Text = "X";
            this.btnRomove.UseVisualStyleBackColor = true;
            this.btnRomove.Click += new System.EventHandler(this.btnRomove_Click);
            // 
            // btnAddPCDCol
            // 
            this.btnAddPCDCol.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnAddPCDCol.Location = new System.Drawing.Point(96, 3);
            this.btnAddPCDCol.Name = "btnAddPCDCol";
            this.btnAddPCDCol.Size = new System.Drawing.Size(131, 23);
            this.btnAddPCDCol.TabIndex = 4;
            this.btnAddPCDCol.Text = "^";
            this.btnAddPCDCol.UseVisualStyleBackColor = true;
            this.btnAddPCDCol.Click += new System.EventHandler(this.btnAddPCDCol_Click);
            // 
            // pcdTree
            // 
            this.pcdTree.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pcdTree.HideSelection = false;
            this.pcdTree.Location = new System.Drawing.Point(3, 32);
            this.pcdTree.Name = "pcdTree";
            this.pcdTree.Size = new System.Drawing.Size(318, 287);
            this.pcdTree.TabIndex = 0;
            this.pcdTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.pcdTree_AfterSelect);
            // 
            // checkRepeatHeaders
            // 
            this.checkRepeatHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkRepeatHeaders.AutoSize = true;
            this.checkRepeatHeaders.Location = new System.Drawing.Point(24, 369);
            this.checkRepeatHeaders.Name = "checkRepeatHeaders";
            this.checkRepeatHeaders.Size = new System.Drawing.Size(131, 17);
            this.checkRepeatHeaders.TabIndex = 4;
            this.checkRepeatHeaders.Text = "checkRepeatHeaders";
            this.checkRepeatHeaders.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(20, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(626, 326);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.numSortOrder);
            this.tabPage1.Controls.Add(this.comboSort);
            this.tabPage1.Controls.Add(this.checkCalculateTotal);
            this.tabPage1.Controls.Add(this.checkVisible);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.textFooterStyle);
            this.tabPage1.Controls.Add(this.textBodyStyle);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.textHeaderStyle);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.textFormula);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.textColumnName);
            this.tabPage1.Controls.Add(this.textNumberFormat);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.textColumn);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(618, 300);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Formual";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // numSortOrder
            // 
            this.numSortOrder.Location = new System.Drawing.Point(381, 125);
            this.numSortOrder.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSortOrder.Name = "numSortOrder";
            this.numSortOrder.Size = new System.Drawing.Size(120, 20);
            this.numSortOrder.TabIndex = 5;
            this.numSortOrder.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSortOrder.ValueChanged += new System.EventHandler(this.numSortOrder_ValueChanged);
            // 
            // comboSort
            // 
            this.comboSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSort.FormattingEnabled = true;
            this.comboSort.Items.AddRange(new object[] {
            "Not Sorted",
            "Ascending",
            "Descending"});
            this.comboSort.Location = new System.Drawing.Point(107, 124);
            this.comboSort.Name = "comboSort";
            this.comboSort.Size = new System.Drawing.Size(168, 21);
            this.comboSort.TabIndex = 4;
            this.comboSort.SelectedIndexChanged += new System.EventHandler(this.comboSort_SelectedIndexChanged);
            // 
            // checkCalculateTotal
            // 
            this.checkCalculateTotal.AutoSize = true;
            this.checkCalculateTotal.Checked = true;
            this.checkCalculateTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkCalculateTotal.Location = new System.Drawing.Point(107, 88);
            this.checkCalculateTotal.Name = "checkCalculateTotal";
            this.checkCalculateTotal.Size = new System.Drawing.Size(97, 17);
            this.checkCalculateTotal.TabIndex = 3;
            this.checkCalculateTotal.Text = "Calculate Total";
            this.checkCalculateTotal.UseVisualStyleBackColor = true;
            this.checkCalculateTotal.CheckedChanged += new System.EventHandler(this.checkCalculateTotal_CheckedChanged);
            // 
            // checkVisible
            // 
            this.checkVisible.AutoSize = true;
            this.checkVisible.Checked = true;
            this.checkVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkVisible.Location = new System.Drawing.Point(7, 88);
            this.checkVisible.Name = "checkVisible";
            this.checkVisible.Size = new System.Drawing.Size(56, 17);
            this.checkVisible.TabIndex = 3;
            this.checkVisible.Text = "Visible";
            this.checkVisible.UseVisualStyleBackColor = true;
            this.checkVisible.CheckedChanged += new System.EventHandler(this.checkVisible_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Sort Mode";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Number Format";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Column Header:";
            // 
            // textFooterStyle
            // 
            this.textFooterStyle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFooterStyle.Location = new System.Drawing.Point(381, 94);
            this.textFooterStyle.Name = "textFooterStyle";
            this.textFooterStyle.Size = new System.Drawing.Size(231, 20);
            this.textFooterStyle.TabIndex = 2;
            this.textFooterStyle.TextChanged += new System.EventHandler(this.textFooterStyle_TextChanged);
            // 
            // textBodyStyle
            // 
            this.textBodyStyle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBodyStyle.Location = new System.Drawing.Point(381, 68);
            this.textBodyStyle.Name = "textBodyStyle";
            this.textBodyStyle.Size = new System.Drawing.Size(231, 20);
            this.textBodyStyle.TabIndex = 2;
            this.textBodyStyle.TextChanged += new System.EventHandler(this.textBodyStyle_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(281, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Sort Order:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(281, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Footer Style:";
            // 
            // textHeaderStyle
            // 
            this.textHeaderStyle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textHeaderStyle.Location = new System.Drawing.Point(381, 42);
            this.textHeaderStyle.Name = "textHeaderStyle";
            this.textHeaderStyle.Size = new System.Drawing.Size(231, 20);
            this.textHeaderStyle.TabIndex = 2;
            this.textHeaderStyle.TextChanged += new System.EventHandler(this.textHeaderStyle_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(281, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Body Style:";
            // 
            // textFormula
            // 
            this.textFormula.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFormula.Location = new System.Drawing.Point(6, 178);
            this.textFormula.Multiline = true;
            this.textFormula.Name = "textFormula";
            this.textFormula.Size = new System.Drawing.Size(606, 116);
            this.textFormula.TabIndex = 1;
            this.textFormula.TextChanged += new System.EventHandler(this.textFormula_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(281, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Header Style:";
            // 
            // textColumnName
            // 
            this.textColumnName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textColumnName.Location = new System.Drawing.Point(381, 14);
            this.textColumnName.Name = "textColumnName";
            this.textColumnName.Size = new System.Drawing.Size(231, 20);
            this.textColumnName.TabIndex = 2;
            this.textColumnName.TextChanged += new System.EventHandler(this.textColumnName_TextChanged);
            // 
            // textNumberFormat
            // 
            this.textNumberFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNumberFormat.Location = new System.Drawing.Point(107, 42);
            this.textNumberFormat.Name = "textNumberFormat";
            this.textNumberFormat.Size = new System.Drawing.Size(168, 20);
            this.textNumberFormat.TabIndex = 2;
            this.textNumberFormat.TextChanged += new System.EventHandler(this.textNumberFormat_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Variable Name:";
            // 
            // textColumn
            // 
            this.textColumn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textColumn.Location = new System.Drawing.Point(107, 14);
            this.textColumn.Name = "textColumn";
            this.textColumn.Size = new System.Drawing.Size(168, 20);
            this.textColumn.TabIndex = 2;
            this.textColumn.TextChanged += new System.EventHandler(this.textColumn_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Formula";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.payrollPeriodSelector);
            this.tabPage2.Controls.Add(this.buttonEvalue);
            this.tabPage2.Controls.Add(this.listValues);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.employeePlaceholder);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(618, 300);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Test";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // payrollPeriodSelector
            // 
            this.payrollPeriodSelector.Location = new System.Drawing.Point(130, 44);
            this.payrollPeriodSelector.Name = "payrollPeriodSelector";
            this.payrollPeriodSelector.NullSelection = null;
            this.payrollPeriodSelector.SelectedPeriod = null;
            this.payrollPeriodSelector.Size = new System.Drawing.Size(279, 24);
            this.payrollPeriodSelector.TabIndex = 5;
            // 
            // buttonEvalue
            // 
            this.buttonEvalue.Enabled = false;
            this.buttonEvalue.Location = new System.Drawing.Point(463, 16);
            this.buttonEvalue.Name = "buttonEvalue";
            this.buttonEvalue.Size = new System.Drawing.Size(69, 23);
            this.buttonEvalue.TabIndex = 4;
            this.buttonEvalue.Text = "Evaluate";
            this.buttonEvalue.UseVisualStyleBackColor = true;
            this.buttonEvalue.Click += new System.EventHandler(this.buttonEvalue_Click);
            // 
            // listValues
            // 
            this.listValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listValues.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.VariableName,
            this.columnHeader2,
            this.columnHeader3});
            this.listValues.FullRowSelect = true;
            this.listValues.Location = new System.Drawing.Point(9, 71);
            this.listValues.Name = "listValues";
            this.listValues.Size = new System.Drawing.Size(594, 223);
            this.listValues.TabIndex = 3;
            this.listValues.UseCompatibleStateImageBehavior = false;
            this.listValues.View = System.Windows.Forms.View.Details;
            // 
            // VariableName
            // 
            this.VariableName.Text = "Variable";
            this.VariableName.Width = 165;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 318;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Type";
            this.columnHeader3.Width = 86;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Period:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Test Employee:";
            // 
            // employeePlaceholder
            // 
            this.employeePlaceholder.Category = -1;
            this.employeePlaceholder.employee = null;
            this.employeePlaceholder.Location = new System.Drawing.Point(130, 18);
            this.employeePlaceholder.Name = "employeePlaceholder";
            this.employeePlaceholder.Size = new System.Drawing.Size(310, 20);
            this.employeePlaceholder.TabIndex = 2;
            this.employeePlaceholder.EmployeeChanged += new System.EventHandler(this.employeePlaceholder_EmployeeChanged);
            // 
            // textNColPageBreak
            // 
            this.textNColPageBreak.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textNColPageBreak.Location = new System.Drawing.Point(473, 342);
            this.textNColPageBreak.Name = "textNColPageBreak";
            this.textNColPageBreak.Size = new System.Drawing.Size(156, 20);
            this.textNColPageBreak.TabIndex = 2;
            // 
            // textSheetName
            // 
            this.textSheetName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textSheetName.Location = new System.Drawing.Point(143, 342);
            this.textSheetName.Name = "textSheetName";
            this.textSheetName.Size = new System.Drawing.Size(156, 20);
            this.textSheetName.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(316, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(135, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Number of Rows per Page:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 346);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Sheet Format Name:";
            // 
            // textFooter
            // 
            this.textFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textFooter.Location = new System.Drawing.Point(96, 482);
            this.textFooter.Multiline = true;
            this.textFooter.Name = "textFooter";
            this.textFooter.Size = new System.Drawing.Size(550, 54);
            this.textFooter.TabIndex = 1;
            // 
            // textHeader
            // 
            this.textHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textHeader.Location = new System.Drawing.Point(96, 409);
            this.textHeader.Multiline = true;
            this.textHeader.Name = "textHeader";
            this.textHeader.Size = new System.Drawing.Size(550, 57);
            this.textHeader.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 409);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Header Text:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 482);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Footer Text:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Header Text";
            // 
            // pnlDialog
            // 
            this.pnlDialog.Controls.Add(this.cmbFormat);
            this.pnlDialog.Controls.Add(this.btnOk);
            this.pnlDialog.Controls.Add(this.btnCancel);
            this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDialog.Location = new System.Drawing.Point(0, 542);
            this.pnlDialog.Name = "pnlDialog";
            this.pnlDialog.Size = new System.Drawing.Size(983, 38);
            this.pnlDialog.TabIndex = 4;
            // 
            // cmbFormat
            // 
            this.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFormat.FormattingEnabled = true;
            this.cmbFormat.Items.AddRange(new object[] {
            "Format 1",
            "Format 2",
            "Format 3"});
            this.cmbFormat.Location = new System.Drawing.Point(12, 7);
            this.cmbFormat.Name = "cmbFormat";
            this.cmbFormat.Size = new System.Drawing.Size(191, 21);
            this.cmbFormat.TabIndex = 6;
            this.cmbFormat.SelectedIndexChanged += new System.EventHandler(this.cmbFormat_SelectedIndexChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(835, 7);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(906, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // PayrollSheetFormatEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 580);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.pnlDialog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PayrollSheetFormatEditor";
            this.Text = "Payroll Sheet Format";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.splitColumns.Panel1.ResumeLayout(false);
            this.splitColumns.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitColumns)).EndInit();
            this.splitColumns.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSortOrder)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.pnlDialog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TreeView treeColumn;
        private System.Windows.Forms.TextBox textFormula;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textColumn;
        private System.Windows.Forms.Panel pnlDialog;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private PCDTree pcdTree;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.SplitContainer splitColumns;
        private System.Windows.Forms.Button btnRomove;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnAddPCDCol;
        private System.Windows.Forms.TextBox textColumnName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAutoVar;
        private System.Windows.Forms.TextBox textFooter;
        private System.Windows.Forms.TextBox textHeader;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbFormat;
        private System.Windows.Forms.TextBox textSheetName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private EmployeePlaceHolder employeePlaceholder;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListView listValues;
        private System.Windows.Forms.ColumnHeader VariableName;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button buttonEvalue;
        private PayrollPeriodSelector payrollPeriodSelector;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonAddRoot;
        private System.Windows.Forms.CheckBox checkCalculateTotal;
        private System.Windows.Forms.CheckBox checkVisible;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textFooterStyle;
        private System.Windows.Forms.TextBox textBodyStyle;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textHeaderStyle;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textNumberFormat;
        private System.Windows.Forms.TextBox textNColPageBreak;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkRepeatHeaders;
        private System.Windows.Forms.ComboBox comboSort;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numSortOrder;

    }
}