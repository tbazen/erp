using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Evaluator;

namespace INTAPS.Payroll.Client
{    
    public partial class PayrollSheetFormatEditor : Form
    {
        public static string[] Formats = new string[] { "PayrollColumnFormula", "PayrollColumnFormulaTwo", "PayrollColumnFormulaThree" };
        public PayrollSheetFormatEditor()
        {
            InitializeComponent();

            pcdTree.LoadData();
            pcdTree_AfterSelect(null, null);
            cmbFormat.SelectedIndex = 0;
            payrollPeriodSelector.LoadData(PayrollClient.CurrentYear);
            
        }
        void LoadFormat()
        {
            
            try
            {
                PayrollSheetFormat f = (PayrollSheetFormat)PayrollClient.GetSystemParameters(new string[] { Formats[cmbFormat.SelectedIndex] })[0];
                treeColumn.Nodes.Clear();
                if(f!=null)
                    LoadData(treeColumn.Nodes,f,f.columns);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't load payrollsheet formula.", ex);
            }
        }
        private void LoadData(TreeNodeCollection parent,PayrollSheetFormat f,  PayrollSheetColumn[] cols)
        {
            for (int i = 0; i < cols.Length; i++) 
            {

               if (cols[i].childs.Length==0)
                    AddColumn(parent, new ColumnDefination(cols[i]));
                else
                {
                    TreeNode n = AddColumn(parent, new ColumnDefination(cols[i]));
                    LoadData(n.Nodes, f,cols[i].childs);
                }
            }
            textHeader.Text = f.header;
            textFooter.Text = f.footer;
            textSheetName.Text = f.name == null ? "" : f.name;
            textNColPageBreak.Text = f.breakNumberOfRows < 1 ? "" : f.breakNumberOfRows.ToString();
        }
        TreeNode getNodeByName(TreeNodeCollection nodes, string name)
        {
            foreach (TreeNode n in nodes)
            {
                if (string.Compare(((ColumnDefination)n.Tag).name, name, true) == 0)
                    return n;
                TreeNode ch = getNodeByName(n.Nodes, name);
                if (ch != null)
                    return ch;
            }
            return null;
        }
        string GetNodeVar()
        {
            int i=treeColumn.GetNodeCount(true);
            TreeNode test;
            string varName;
            while( (test=getNodeByName(treeColumn.Nodes,varName=((char)('A'+i)).ToString()))!=null)
            {
                i++;
            }
            return varName;
        }
        TreeNode AddColumn(TreeNodeCollection parent, ColumnDefination def)
        {
            TreeNode n = new TreeNode();
            parent.Add(n);
            SetNode(def, n);
            return n;
        }
        private void SetNode(ColumnDefination def, TreeNode n)
        {
            n.Tag = def;
            n.Text = def.name + ":" + def.header;
        }
        private void textFormula_TextChanged(object sender, EventArgs e)
        {
            if (m_IgnoreEvents) return;
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.formula = textFormula.Text;
        }
        private void button2_Click(object sender, EventArgs e)
        {

        }
        bool m_IgnoreEvents = false;
        private void treeColumn_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                m_IgnoreEvents = true;
                TreeNode n = treeColumn.SelectedNode;
                TreeNodeCollection par = n == null ? null : (n.Parent==null?treeColumn.Nodes:n.Parent.Nodes);
                bool ColSelected = n != null;
                btnAdd.Enabled = treeColumn.SelectedNode != null;
                btnRomove.Enabled = ColSelected;
                btnUp.Enabled = ColSelected && (n.Index > 0);
                btnDown.Enabled = ColSelected && (n.Index < par.Count - 1);
                textColumn.Enabled = ColSelected;
                textFormula.Enabled = ColSelected && n.Nodes.Count == 0;
                textColumnName.Enabled = ColSelected;

                if (ColSelected)
                {
                    ColumnDefination col = (ColumnDefination)n.Tag;
                    textColumn.Text = col.header;
                    textFormula.Text = col.formula;
                    textColumnName.Text = col.name;
                    checkVisible.Checked = col.visible;
                    checkCalculateTotal.Checked = col.total;
                    textNumberFormat.Text = col.numberFormat;
                    textHeaderStyle.Text = col.headerCss;
                    textBodyStyle.Text = col.bodyCss;
                    textFooterStyle.Text = col.footerCss;
                    if (col.sortOrder == -1)
                    {
                        comboSort.SelectedIndex = 0;
                    }
                    else
                    {
                        comboSort.SelectedIndex = col.sortAscending ? 1 : 2;
                        numSortOrder.Value = col.sortOrder;
                    }
                }
                else
                {
                    textColumn.Text = "";
                    textFormula.Text = "";
                    textColumnName.Text = "";
                }
            }
            finally
            {
                m_IgnoreEvents = false;
            }
        }
        private void btnAddPCDCol_Click(object sender, EventArgs e)
        {
            TreeNodeCollection par;
            if (treeColumn.SelectedNode == null)
                par = treeColumn.Nodes;
            else
                par = treeColumn.SelectedNode.Nodes;
            ColumnDefination def = new ColumnDefination();
            if (pcdTree.SelectedFormula == null)
            {
                def.header = pcdTree.SelectedPCD.Name;
                def.formula = @"Component(""" + pcdTree.SelectedPCD.Name + @""")";
            }
            else
            {
                def.header = pcdTree.SelectedFormula.Name;
                def.formula = @"Component(""" + pcdTree.SelectedPCD.Name + @""",""" + pcdTree.SelectedFormula.Name + @""")";
            }
            def.name = GetNodeVar();
            AddColumn(par, def);
        }
        private void pcdTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            btnAddPCDCol.Enabled = pcdTree.SelectedPCD != null && (!pcdTree.SelectedPCD.SupportFormula || pcdTree.SelectedFormula != null);
        }
        private void btnRomove_Click(object sender, EventArgs e)
        {
            treeColumn.SelectedNode.Remove();
            treeColumn_AfterSelect(null, null);
        }
        private void btnUp_Click(object sender, EventArgs e)
        {
            TreeNode n=treeColumn.SelectedNode;
            if (n == null)
                return;
            TreeNodeCollection par = n.Parent==null?treeColumn.Nodes:n.Parent.Nodes;
            int index = n.Index;
            if (index == 0)
            {
                //int parindex = par.Index;
                //n.Remove();
                //par.Insert(parindex, n);
            }
            else
            {
                par.Remove(n);
                par.Insert(index - 1,n);
            }
            treeColumn.SelectedNode = n;
        }
        private void btnDown_Click(object sender, EventArgs e)
        {
            TreeNode n = treeColumn.SelectedNode;
            TreeNodeCollection par = n.Parent==null?treeColumn.Nodes:n.Parent.Nodes;
            int index = n.Index;
            if (index == par.Count-1)
            {
                //int parindex = par.Index;
                //n.Remove();
                //if(parindex==par.Parent.Nodes.Count-1)
                //    par.Parent.Nodes.Add( n);
                //else
                //    par.Parent.Nodes.Insert(parindex+1, n);
            }
            else
            {
                par.Remove(n);
                if(index==par.Count-1)
                    par.Add( n);
                else
                    par.Insert(index+1, n);
            }
            treeColumn.SelectedNode = n;
        }
        void GetSheet(TreeNodeCollection parent, out PayrollSheetColumn[] cols )
        {
            cols = new PayrollSheetColumn[parent.Count];
            int i = 0;
            foreach (TreeNode node in parent)
            {
                ColumnDefination col=(ColumnDefination)node.Tag;
                PayrollSheetColumn colData = new PayrollSheetColumn();
                colData.header = col.header;
                colData.formula = col.formula;
                colData.name = col.name;
                colData.visisble = col.visible;
                colData.total = col.total;
                colData.numberFormat = col.numberFormat;
                colData.headerCss = col.headerCss;
                colData.bodyCss = col.bodyCss;
                colData.footerCss = col.footerCss;
                colData.sortOrder = col.sortOrder;
                colData.sortAscending = col.sortAscending;
                cols[i] = colData;
                if (node.Nodes.Count >0)
                    GetSheet(node.Nodes,out colData.childs);
                i++;
            }
        }
        private void btnOk_Click(object sender, EventArgs e)
        {

            PayrollSheetFormat f= buildFormat();
            try
            {
                PayrollClient.SetSystemParameters(new string[] { Formats[cmbFormat.SelectedIndex] }, new object[] { f });
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Payrollsheet format saved succesfully.");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save payrollsheet formula", ex);
            }
        }

        private PayrollSheetFormat buildFormat()
        {
            PayrollSheetFormat f;
            f = new PayrollSheetFormat();
            GetSheet(treeColumn.Nodes, out f.columns);
            f.footer = textFooter.Text;
            f.header = textHeader.Text;
            f.name = textSheetName.Text;
            if (!int.TryParse(textNColPageBreak.Text, out  f.breakNumberOfRows))
                f.breakNumberOfRows = -1;
            return f;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            TreeNodeCollection par;
            if (treeColumn.SelectedNode == null || sender==buttonAddRoot)
                par = treeColumn.Nodes;
            else
            {
                par = treeColumn.SelectedNode.Nodes;
            }
            ColumnDefination def = new ColumnDefination();
            def.header = "Untitled";
            def.formula = "";
            def.name = GetNodeVar();
            AddColumn(par, def);
        }
        private void textColumn_TextChanged(object sender, EventArgs e)
        {
            if (m_IgnoreEvents) return;
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.header = textColumn.Text;
            SetNode(def, treeColumn.SelectedNode);
        }
        private void textColumnName_TextChanged(object sender, EventArgs e)
        {
            if (m_IgnoreEvents) return;
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.name= textColumnName.Text;
            SetNode(def, treeColumn.SelectedNode);
        }
        void UpdateVariableName(TreeNode par)
        {
            string var;
            var = ((ColumnDefination)par.Tag).name;
            foreach (TreeNode n in par.Nodes)
            {
                ((ColumnDefination)n.Tag).name = var + ((char)('A' + (char)n.Index)).ToString();
                SetNode((ColumnDefination)n.Tag, n);
            }
        }
        private void btnAutoVar_Click(object sender, EventArgs e)
        {
            //UpdateVariableName(treeColumn.Nodes[0]);
        }
        private void cmbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadFormat();
        }

        private void employeePlaceholder_EmployeeChanged(object sender, EventArgs e)
        {
            buttonEvalue.Enabled = employeePlaceholder.GetEmployeeID() > 0;
        }

        private void buttonEvalue_Click(object sender, EventArgs e)
        {
            listValues.Items.Clear();
            PayrollSheetFormat f = buildFormat();
            try
            {
                string[] col;
                EData[] vals=PayrollClient.testSheetFormat(f, employeePlaceholder.GetEmployeeID(), payrollPeriodSelector.SelectedPeriod.id, out col);
                for (int i = 0; i < col.Length; i++)
                {
                    ListViewItem li = new ListViewItem(col[i]);
                    li.SubItems.Add(vals[i].ToString());
                    li.SubItems.Add(vals[i].EDataType.Name);
                    listValues.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to evaluate formula", ex);
            }
        }

        private void checkVisible_CheckedChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.visible = checkVisible.Checked;
        }

        private void checkCalculateTotal_CheckedChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.total = checkCalculateTotal.Checked;
        }

        private void buttonAddRoot_Click(object sender, EventArgs e)
        {
            treeColumn.SelectedNode = null;
        }

        private void textNumberFormat_TextChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.numberFormat= textNumberFormat.Text;
        }

        private void textHeaderStyle_TextChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.headerCss = textHeaderStyle.Text;

        }

        private void textBodyStyle_TextChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.bodyCss= textBodyStyle.Text;

        }

        private void textFooterStyle_TextChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.footerCss = textFooterStyle.Text;

        }

        private void comboSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            switch (comboSort.SelectedIndex)
            {
                case 0:
                    def.sortOrder = -1;
                    break;
                case 1:
                    def.sortOrder = (int)numSortOrder.Value;
                    def.sortAscending= true;
                    break;
                case 2:
                    def.sortOrder = (int)numSortOrder.Value;
                    def.sortAscending = false;
                    break;
            }   
        }

        private void numSortOrder_ValueChanged(object sender, EventArgs e)
        {
            ColumnDefination def = (ColumnDefination)treeColumn.SelectedNode.Tag;
            def.sortOrder = (int)numSortOrder.Value;
        }
    }
    public class ColumnDefination
    {
        public string header;
        public string formula;
        public string name;
        public bool visible=true;
        public bool total=true;
        public string numberFormat = "#,#0.00";
        public string headerCss = "LedgerGridHeadCell";
        public string bodyCss = "LedgerGridOddRowCell,LedgerGridEvenRowCell";
        public string footerCss = "LedgerGridFooterCell";
        public int sortOrder=-1;
        public bool sortAscending;
        public ColumnDefination()
        {
            
        }
        public ColumnDefination(PayrollSheetColumn col)
        {
            header = col.header;
            formula=col.formula;
            this.name = col.name;
            this.visible = col.visisble;
            this.total = col.total;
            this.numberFormat = col.numberFormat;
            this.headerCss = col.headerCss;
            this.bodyCss = col.bodyCss;
            this.footerCss = col.footerCss;
            this.sortOrder = col.sortOrder;
            this.sortAscending = col.sortAscending;
        }
    }
}