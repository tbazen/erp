using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class PostPayroll : Form
    {
        PayrollDocument[] m_payrolls;
        public PostPayroll()
        {
            InitializeComponent();
        }
        public PostPayroll(PayrollDocument[] ps)
            : this()
        {
            m_payrolls = ps;
            foreach (PayrollDocument doc in ps)
            {
                ListViewItem li = new ListViewItem(doc.employee.employeeName + " - " + doc.employee.employeeID);
                li.SubItems.Add("Pending");
                lvPayrolls.Items.Add(li);
                li.Tag = doc;
            }
        }

        private void lvErrors_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvPayrolls.SelectedItems.Count == 0)
            {
                txtErrorInfo.Text = "";
                return;
            }
            Exception ex = lvPayrolls.SelectedItems[0].Tag as Exception;
            if (ex == null)
                txtErrorInfo.Text = "";
            else
                txtErrorInfo.Text = ex.Message + "\n" + ex.StackTrace;
        }

        private void btnPost_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            foreach (ListViewItem li in lvPayrolls.Items)
            {
                PayrollDocument doc = li.Tag as PayrollDocument;
                try
                {
                    li.SubItems[1].Text = "Posting..";
                    Application.DoEvents();
                    PayrollClient.PostPayroll(doc.employee.ObjectIDVal, doc.payPeriod.id,dtpDate.Value);
                    li.SubItems[1].Text = "Done";
                }
                catch (Exception ex)
                {
                    li.Tag = ex;
                    li.SubItems[1].Text = "Error";
                }
                Application.DoEvents();
            }
            for (int i = lvPayrolls.Items.Count - 1; i >= 0; i--)
            {
                if (lvPayrolls.Items[i].Tag is Exception)
                    continue;
                lvPayrolls.Items[i].Remove();
            }
            this.Enabled = true;
            btnPost.Enabled = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (btnPost.Enabled)
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.Cancel;
        }
    }
}