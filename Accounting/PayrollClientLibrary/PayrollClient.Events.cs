using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Payroll;
using System.Web;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
namespace INTAPS.Payroll.Client
{
    public delegate void EmployeeUpdatedHandler(Employee e);
    public delegate void EmployeeCreatedHandler(Employee e);
    public delegate void EmployeeDeletedHandler(int ID);
    public delegate void OrgUnitUpdatedHandler(OrgUnit o);
    public delegate void OrgUnitCreatedHandler(OrgUnit o);
    public delegate void OrgUnitDeletedHandler(int ID);

    public partial class PayrollClient
    {
        public static event EmployeeUpdatedHandler  EmployeeUpdated;
        public static event EmployeeCreatedHandler  EmployeeCreated;
        public static event EmployeeDeletedHandler  EmployeeDeleted;
        public static event OrgUnitUpdatedHandler    OrgUnitUpdated;
        public static event OrgUnitCreatedHandler   OrgUnitCreated;
        public static event OrgUnitDeletedHandler OrgUnitDeleted;
        public static void OnEmployeeUpdated(Employee e)
        {
            if (EmployeeUpdated != null)
                EmployeeUpdated(e);
        }
        public static void OnEmployeeCreated(Employee e)
        {
            if (EmployeeCreated != null)
                EmployeeCreated(e);
        }
        public static void OnEmployeeDeleted(int ID)
        {
            if (EmployeeDeleted != null)
                EmployeeDeleted(ID);
        }
        public static void OnOrgUnitUpdated(OrgUnit o)
        {
            if (OrgUnitUpdated != null)
                OrgUnitUpdated(o);
        }
        public static void OnOrgUnitCreated(OrgUnit o)
        {
            if (OrgUnitCreated != null)
                OrgUnitCreated(o);
        }
        public static void OnOrgUnitDeleted(int ID)
        {
            if(OrgUnitDeleted!=null)
                OrgUnitDeleted(ID);
        }
    }
}
