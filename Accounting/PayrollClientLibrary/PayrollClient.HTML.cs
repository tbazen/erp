using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Payroll;
using System.Web;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.Evaluator;
namespace INTAPS.Payroll.Client
{
    public partial class PayrollClient
    {
        public static bool IsEqual(double va1l,double val2)
        {
            return Math.Abs(va1l - val2) < 0.0001;
        }

        static string FormatPayroll(PayrollDocument doc)
        {
            return "<div style='border-right: black solid; border-top: black solid; border-left: black solid; border-bottom: black solid'> <h3>Payroll Slip</h3>" + doc.BuildHTML() + "<br/><i style='font-size: 0.8em'><b >Dire Dawa Water Supply and Sewerage Authority</b><br/><b>Computerized by INTAPS</b></i></div>";
        }
        public static string GetPayrollSlipTableHTML(PayrollDocument[] prdocs)
        {
            string html="";
            for (int i = 0; i < prdocs.Length; i++)
            {
                html+=FormatPayroll(prdocs[i]);
                html += "<br clear=all style='page-break-before:always'>";

            }
            return html;
        }


        
    }
}

