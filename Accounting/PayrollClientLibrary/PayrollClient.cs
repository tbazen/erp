using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.ClientServer.Client;
namespace INTAPS.Payroll.Client
{
    public partial class PayrollClient
    {
        static Dictionary<int, IPayrollComponentUIHandler> _PCDUIHandlers = null;
        public static Dictionary<int, PayrollComponentDefination> _PCDDefs = null;
        static string path;
       // public static INTAPS.Payroll.IPayrollService PayrollServer;
        public static Dictionary<int, IPayrollComponentUIHandler> PCDUIHandlers
        {
            get
            {
                if (_PCDUIHandlers == null)
                    LoadPUD();
                return _PCDUIHandlers;
            }
        }
        public static Dictionary<int, PayrollComponentDefination> PCDDefs
        {
            get
            {
                if (_PCDDefs == null)
                    LoadPUD();
                return _PCDDefs;
            }
        }

        public static PayrollComponentDefination[] PCDDefsArr;
        public static int CurrentYear;
        public static bool Ethiopian;
        static void InitCalendar()
        {
            try
            {
                Ethiopian = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["EthiopianCalendar"]);
            }
            catch
            {
                Ethiopian = true;
            }
            if (Ethiopian)
                CurrentYear = INTAPS.Ethiopic.EtGrDate.ToEth(DateTime.Now).Year;
            else
                CurrentYear = DateTime.Now.Year;

        }
        static void ConnectToServer(string url)
        {
            path = url + "/api/erp/payroll/";
            //PayrollServer = (INTAPS.Payroll.IPayrollService)System.Runtime.Remoting.RemotingServices.Connect(typeof(INTAPS.Payroll.IPayrollService), url + "/PayrollServer");
            INTAPS.Evaluator.CalcGlobal.Initialize();
            INTAPS.Evaluator.CalcGlobal.AddFunction(new Evaluator.EFGetEmployeeDataField());
            INTAPS.Evaluator.CalcGlobal.AddFunction(new Evaluator.EFGetComponentDataField());
        }
        static void LoadPUD()
        {
            _PCDUIHandlers = new Dictionary<int, IPayrollComponentUIHandler>();
            string error = null;
            _PCDDefs = new Dictionary<int, PayrollComponentDefination>();
            PayrollClient.GetPCDefinations();
            foreach (PayrollComponentDefination pd in PCDDefsArr)
            {
                _PCDDefs.Add(pd.ID, pd);
                string thise = null;
                try
                {
                    Type t = Type.GetType(pd.UIHandlerClass);
                    if (t == null)
                        thise = pd.UIHandlerClass + " couldn't be loaded";
                    else
                    {
                        System.Reflection.ConstructorInfo c = t.GetConstructor(new Type[] { });
                        if (c == null)
                            thise = pd.UIHandlerClass + " constructor not found.";
                        else
                        {
                            _PCDUIHandlers.Add(pd.ID, (IPayrollComponentUIHandler)c.Invoke(new object[] { }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    thise = ex.Message + "\n" + ex.StackTrace;
                }
                if (thise != null)
                {
                    if (error != null)
                        error += "\n" + thise;
                    else
                        error = thise;
                }
            }
            if (error != null)
                Console.WriteLine(error);
        }
        public static void Connect(string url)
        {
            ConnectToServer(url);

            InitCalendar();

        }


        public static void ChangeEmployeeOrgUnit(int ID, int NewOrgUnitID,DateTime date)
        {
            //PayrollServer.ChangeEmployeeOrgUnit(ApplicationClient.SessionID, ID, NewOrgUnitID,date);
            RESTAPIClient.Call<VoidRet>(path + "ChangeEmployeeOrgUnit", new { SessionID= ApplicationClient.SessionID, ID= ID, NewOrgUnitID= NewOrgUnitID, date= date });
            OnEmployeeUpdated(PayrollClient.GetEmployee(ID));
        }

        public static void ChangeEmployeeSalary(int ID, double NewSalary)
        {
            //PayrollServer.ChangeEmployeeSalary(ApplicationClient.SessionID, ID, NewSalary);
            RESTAPIClient.Call<VoidRet>(path + "ChangeEmployeeSalary", new { SessionID= ApplicationClient.SessionID, ID= ID, NewSalary= NewSalary });
            OnEmployeeUpdated(PayrollClient.GetEmployee(ID));
        }

        public static void DeleteEmployee(int ID)
        {
           // PayrollServer.DeleteEmployee(ApplicationClient.SessionID, ID);
           RESTAPIClient.Call<VoidRet>(path + "DeleteEmployee", new { SessionID= ApplicationClient.SessionID, ID= ID });
            OnEmployeeDeleted(ID);
        }

        public static void DeleteOrgUnit(int ID)
        {
           // PayrollServer.DeleteOrgUnit(ApplicationClient.SessionID, ID);
           RESTAPIClient.Call<VoidRet>(path + "DeleteOrgUnit", new { SessionID= ApplicationClient.SessionID, ID= ID });
            OnOrgUnitDeleted(ID);
        }

        public static void EnrollEmployee(DateTime date, int ID)
        {
           // PayrollServer.EnrollEmployee(ApplicationClient.SessionID, date, ID);
           RESTAPIClient.Call<VoidRet>(path + "EnrollEmployee", new { SessionID= ApplicationClient.SessionID, date= date, ID= ID });
            OnEmployeeUpdated(PayrollClient.GetEmployee(ID));
        }

        public static void FireEmployee(DateTime date, int ID)
        {
            //PayrollServer.FireEmployee(ApplicationClient.SessionID, date, ID);
            RESTAPIClient.Call<VoidRet>(path + "FireEmployee", new { SessionID= ApplicationClient.SessionID, date= date, ID= ID });
            OnEmployeeUpdated(PayrollClient.GetEmployee(ID));
        }


        public static INTAPS.Payroll.Employee GetEmployee(int ID)
        {
            //   return PayrollServer.GetEmployee(ApplicationClient.SessionID, ID);
            return RESTAPIClient.Call<Employee>(path + "GetEmployee", new { SessionID= ApplicationClient.SessionID, ID= ID });
        }

        public static INTAPS.Payroll.Employee GetEmployeeByID(string EmpID)
        {
            //return PayrollServer.GetEmployeeByID(ApplicationClient.SessionID, EmpID);
            return RESTAPIClient.Call<Employee>(path + "GetEmployeeByID", new { SessionID= ApplicationClient.SessionID, EmpID= EmpID });
        }

        public static INTAPS.Payroll.Employee[] GetEmployees(int OrgID, bool IncludeSubOrg)
        {
            //return PayrollServer.GetEmployees(ApplicationClient.SessionID, OrgID, IncludeSubOrg);
            return RESTAPIClient.Call<Employee[]>(path + "GetEmployees", new { SessionID= ApplicationClient.SessionID, OrgID= OrgID , IncludeSubOrg = IncludeSubOrg });
        }

        public static INTAPS.Payroll.Employee[] GetEmployeesByRoll(int OrgID, INTAPS.Payroll.EmployeeRoll Role)
        {
            //return PayrollServer.GetEmployeesByRoll(ApplicationClient.SessionID, OrgID, Role);
            return RESTAPIClient.Call<Employee[]>(path + "GetEmployeesByRoll", new { SessionID= ApplicationClient.SessionID , OrgID = OrgID , Role = Role });
        }

        public static INTAPS.Payroll.OrgUnit[] GetOrgUnits(int PID)
        {
            //   return PayrollServer.GetOrgUnits(ApplicationClient.SessionID, PID);
            return RESTAPIClient.Call<OrgUnit[]>(path + "GetOrgUnits", new { SessionID= ApplicationClient.SessionID , PID = PID });
        }

        public static int SaveEmployeeData(INTAPS.Payroll.Employee e)
        {
            //int id = PayrollServer.SaveEmployeeData(ApplicationClient.SessionID, e);
            int id =RESTAPIClient.Call<int>(path + "SaveEmployeeData", new { SessionID=ApplicationClient.SessionID,e=e });
            e.ObjectIDVal = id;
            OnEmployeeCreated(e);
            return id;
        }

        public static int SaveOrgUnit(INTAPS.Payroll.OrgUnit o)
        {
            //int id = PayrollServer.SaveOrgUnit(ApplicationClient.SessionID, o);
            int id =RESTAPIClient.Call<int>(path + "SaveOrgUnit", new { SessionID=ApplicationClient.SessionID,o=o });
            o.ObjectIDVal = id;
            OnOrgUnitCreated(o);
            return id;
        }
        public class SearchEmployeeOut
        {
            public int NResult;
            public Employee[] _ret;
        }
        public static INTAPS.Payroll.Employee[] SearchEmployee(long ticks, int orgUnitID, string Query, bool includeDismissed, INTAPS.Payroll.EmployeeSearchType SearchType, int index, int PageSize, out int NResult)
        {
            //return PayrollServer.SearchEmployee(ApplicationClient.SessionID, ticks,orgUnitID, Query, includeDismissed, SearchType, index, PageSize, out NResult);
            var _ret=RESTAPIClient.Call<SearchEmployeeOut>(path + "SearchEmployee", new { SessionID=ApplicationClient.SessionID, ticks= ticks, orgUnitID= orgUnitID, Query= Query, includeDismissed= includeDismissed, SearchType= SearchType, index= index, PageSize= PageSize });
            NResult = _ret.NResult;
            return _ret._ret;
        }

        public static void UpdateEmployeeData(INTAPS.Payroll.Employee e)
        {
           // PayrollServer.UpdateEmployeeData(ApplicationClient.SessionID, e);
           RESTAPIClient.Call<VoidRet>(path + "UpdateEmployeeData", new { SessionID=ApplicationClient.SessionID,e=e });
            OnEmployeeUpdated(e);
        }

        public static void UpdateOrgUnit(INTAPS.Payroll.OrgUnit o)
        {
           // PayrollServer.UpdateOrgUnit(ApplicationClient.SessionID, o);
           RESTAPIClient.Call<VoidRet>(path + "UpdateOrgUnit", new { SessionID=ApplicationClient.SessionID,o=o });
            OnOrgUnitUpdated(o);
        }


        public static INTAPS.Payroll.OrgUnit GetOrgUnit(int ID)
        {
            //return PayrollServer.GetOrgUnit(ApplicationClient.SessionID, ID);
            return RESTAPIClient.Call<OrgUnit>(path + "GetOrgUnit", new { SessionID=ApplicationClient.SessionID, ID= ID });
        }
        public static PayrollPeriod[] GetPayPeriods(int Year, bool Ethiopian)
        {
            //return PayrollServer.GetPayPeriods(ApplicationClient.SessionID, Year, Ethiopian);
            return RESTAPIClient.Call<PayrollPeriod[]>(path + "GetPayPeriods", new { SessionID= ApplicationClient.SessionID, Year= Year, Ethiopian= Ethiopian });
        }
        public static int CreatePCNewFormula(PayrollComponentFormula formula)
        {
            // return PayrollServer.CreatePCNewFormula(ApplicationClient.SessionID, formula);
            return RESTAPIClient.Call<int>(path + "CreatePCNewFormula", new { SessionID= ApplicationClient.SessionID, formula= formula });
        }
        public static void UpdatePCFormula(PayrollComponentFormula formula)
        {
            //PayrollServer.UpdatePCFormula(ApplicationClient.SessionID, formula);
            RESTAPIClient.Call<VoidRet>(path + "UpdatePCFormula", new { SessionID=ApplicationClient.SessionID, formula= formula });
        }
        public static void DeletePCForumla(int FormulaID)
        {
            //PayrollServer.DeletePCForumla(ApplicationClient.SessionID, FormulaID);
            RESTAPIClient.Call<VoidRet>(path + "DeletePCForumla", new { SessionID=ApplicationClient.SessionID, FormulaID= FormulaID });
        }
        public static int CreatePCData(PayrollComponentData data, object additionaldata)
        {
            //return PayrollServer.CreatePCData(ApplicationClient.SessionID, data, additionaldata);
            return RESTAPIClient.Call<int>(path + "CreatePCData", new { SessionID= ApplicationClient.SessionID, data= data, additionaldata= new BinObject(additionaldata) });
        }
        public static void UpdatePCData(PayrollComponentData data, object additionaldata)
        {
            //PayrollServer.UpdatePCData(ApplicationClient.SessionID, data, additionaldata);
            RESTAPIClient.Call<VoidRet>(path + "UpdatePCData", new { SessionID=ApplicationClient.SessionID, data= data, additionaldata= new BinObject(additionaldata) });
        }
        public static void DeleteData(int DataID)
        {
           // PayrollServer.DeleteData(ApplicationClient.SessionID, DataID);
           RESTAPIClient.Call<VoidRet>(path + "DeleteData", new { SessionID= ApplicationClient.SessionID, DataID= DataID });
        }
        public static PayrollComponentFormula[] GetPCFormulae(int PCDID)
        {
            //return PayrollServer.GetPCFormulae(ApplicationClient.SessionID, PCDID);
            return RESTAPIClient.Call<PayrollComponentFormula[]>(path + "GetPCFormulae", new { SessionID= ApplicationClient.SessionID, PCDID= PCDID });
        }
        public static PayrollComponentDefination[] GetPCDefinations()
        {
            if (PCDDefsArr == null)
                // PCDDefsArr = PayrollServer.GetPCDefinations(ApplicationClient.SessionID);
                PCDDefsArr = RESTAPIClient.Call<PayrollComponentDefination[]>(path + "GetPCDefinations", new { SessionID=ApplicationClient.SessionID });
            return PCDDefsArr;
        }
        public static PayrollComponentData[] GetPCData(int EmployeeID)
        {
            //  return PayrollServer.GetPCData(ApplicationClient.SessionID, EmployeeID);
            return RESTAPIClient.Call<PayrollComponentData[]>(path + "GetPCData", new { SessionID= ApplicationClient.SessionID , EmployeeID = EmployeeID });
        }
        public static object GetPCAdditionalData(int DataID)
        {
            //return PayrollServer.GetPCAdditionalData(ApplicationClient.SessionID, DataID);
            return RESTAPIClient.Call<TypeObject>(path + "GetPCAdditionalData", new { SessionID= ApplicationClient.SessionID, DataID= DataID }).GetConverted();
        }

        public static PayrollComponentFormulaSpec GetPCDefaultFormula(int PCDID)
        {
            // return PayrollServer.GetPCDefaultFormula(ApplicationClient.SessionID, PCDID);
            return RESTAPIClient.Call<PayrollComponentFormulaSpec>(path + "GetPCDefaultFormula", new { SessionID=ApplicationClient.SessionID, PCDID= PCDID });
        }

        public static PayrollComponentDefination GetPCDefination(int PCDID)
        {
            return PCDDefs[PCDID];
        }

        public static IPayrollComponentUIHandler GetPCDUIHandler(int PCDID)
        {
            return PCDUIHandlers[PCDID];
        }

        public static PayrollPeriod GetPayPeriod(int PID)
        {
            //  PayrollPeriod ret = PayrollServer.GetPayPeriod(ApplicationClient.SessionID, PID);
            PayrollPeriod ret = RESTAPIClient.Call<PayrollPeriod>(path + "GetPayPeriod", new { SessionID=ApplicationClient.SessionID, PID= PID });
            return ret;
        }
        public static void SetPCDDomain(int PCDID, PayPeriodRange pr, AppliesToEmployees at)
        {
        //    PayrollServer.SetPCDDomain(ApplicationClient.SessionID, PCDID, pr, at);
        RESTAPIClient.Call<VoidRet>(path + "SetPCDDomain", new { SessionID=ApplicationClient.SessionID, PCDID= PCDID, prange= pr, at= at });
            PayrollComponentDefination def = PCDDefs[PCDID];
            def.periodRange = pr;
            def.at = at;
        }
        public static PayrollComponentData[] GetPCData(int PCDID, int FormulaID, AppliesToObjectType objectType, int ObjectID, int Period, bool IncludeHeirarchy)
        {
            //  return PayrollServer.GetPCData(ApplicationClient.SessionID, PCDID, FormulaID, objectType, ObjectID, Period, IncludeHeirarchy);
            return RESTAPIClient.Call<PayrollComponentData[]>(path + "GetPCData2", new { SessionID= ApplicationClient.SessionID, PCDID=PCDID, FormulaID= FormulaID, objectType= objectType, ObjectID= ObjectID, Period=Period, IncludeHeirarchy= IncludeHeirarchy });
        }
        public static int GetProgressMax()
        {
            //return PayrollServer.GetProgressMax(ApplicationClient.SessionID);
            return RESTAPIClient.Call<int>(path + "GetProgressMax", new { SessionID=ApplicationClient.SessionID });
        }
        public static int GetProgress()
        {
            //return PayrollServer.GetProgress(ApplicationClient.SessionID);
            return RESTAPIClient.Call<int>(path + "GetProgress", new { SessionID= ApplicationClient.SessionID });
        }
        public static bool IsBusy()
        {
            //return PayrollServer.IsBusy(ApplicationClient.SessionID);
            return RESTAPIClient.Call<bool>(path + "IsBusy", new { SessionID= ApplicationClient.SessionID });
        }
        public static BatchError GetLastError()
        {
            //return PayrollServer.GetLastError(ApplicationClient.SessionID);
            return RESTAPIClient.Call<BatchError>(path + "GetLastError", new { SessionID=ApplicationClient.SessionID });
        }

        public static void GeneratePayroll(DateTime date, AppliesToEmployees at, int PeriodID)
        {
           // PayrollServer.GeneratePayroll(ApplicationClient.SessionID, date, at, PeriodID);
           RESTAPIClient.Call<VoidRet>(path + "GeneratePayroll", new { SessionID=ApplicationClient.SessionID, date= date, at= at, PeriodID= PeriodID });
        }
        public static void StopGenerating()
        {
           // PayrollServer.StopGenerating(ApplicationClient.SessionID);
           RESTAPIClient.Call<VoidRet>(path + "StopGenerating", new { SessionID=ApplicationClient.SessionID });
        }
        public static PayrollDocument GetPayroll(int EmpID, int PeriodID)
        {
            // return PayrollServer.GetPayroll(ApplicationClient.SessionID, EmpID, PeriodID);
            return RESTAPIClient.Call<PayrollDocument>(path + "GetPayroll", new { SessionID=ApplicationClient.SessionID, EmpID= EmpID, PeriodID= PeriodID });
        }
        public static PayrollDocument[] GetPayrolls(AppliesToEmployees at, int PeriodID)
        {
            // return PayrollServer.GetPayrolls(ApplicationClient.SessionID, at, PeriodID);
            return RESTAPIClient.Call<PayrollDocument[]>(path + "GetPayrolls", new { SessionID=ApplicationClient.SessionID, at= at, PeriodID= PeriodID });
        }

        public static void DeletePayroll(int EmployeeID, int PeriodID)
        {
            //PayrollServer.DeletePayroll(ApplicationClient.SessionID, EmployeeID, PeriodID);
            RESTAPIClient.Call<VoidRet>(path + "DeletePayroll", new { SessionID=ApplicationClient.SessionID, EmployeeID= EmployeeID, PeriodID= PeriodID });
        }



        public static void PostPayroll(int employeeID, int periodID, DateTime postDate)
        {
           // PayrollServer.PostPayroll(ApplicationClient.SessionID, employeeID, periodID, postDate);
           RESTAPIClient.Call<VoidRet>(path + "PostPayroll", new { SessionID=ApplicationClient.SessionID, EmployeeID= employeeID, PeriodID= periodID, postDate= postDate });
        }
        public static bool EmployeeUnder(int EID, int OID)
        {
            //return PayrollServer.EmployeeUnder(ApplicationClient.SessionID, EID, OID);
            return RESTAPIClient.Call<bool>(path + "EmployeeUnder", new { SessionID=ApplicationClient.SessionID, EID= EID, OID= OID });
        }

        public static object[] GetSystemParameters(string[] fields)
        {
            return ApplicationClient.GetSystemParameters(path, fields);
        }
        public static void SetSystemParameters(string[] fields, object[] vals)
        {
            ApplicationClient.SetSystemParameters(path,fields, vals);
        }

        public static void UpdatePayPeriod(PayrollPeriod pp)
        {
           // PayrollServer.UpdatePayPeriod(ApplicationClient.SessionID, pp);
           RESTAPIClient.Call<VoidRet>(path + "UpdatePayPeriod", new { SessionID=ApplicationClient.SessionID, pp= pp });
        }

        public static double GetCreditBalance(int employeeID, bool longTerm)
        {
            int parentAccountID;
            if (longTerm)
                parentAccountID = (int)GetSystemParameters(new string[] { "StaffLoanAccount" })[0];
            else
                parentAccountID = (int)GetSystemParameters(new string[] { "SalaryAdvanceAccount" })[0];
            Employee emp = PayrollClient.GetEmployee(employeeID);
            double bal = 0;
            foreach (int acid in emp.accounts)
            {
                Account ac = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(acid);
                if (ac.PID == parentAccountID)
                    bal += INTAPS.Accounting.Client.AccountingClient.GetNetBalanceAsOf(INTAPS.Accounting.Client.AccountingClient.GetCostCenterAccount(emp.costCenterID, ac.id).id, TransactionItem.DEFAULT_CURRENCY, DateTime.Now);
            }
            return bal;
        }
        static public void SetEmployeeImage(int employeeID, byte[] image)
        {
            //PayrollServer.SetEmployeeImage(ApplicationClient.SessionID, employeeID, image);
            RESTAPIClient.Call<VoidRet>(path + "SetEmployeeImage", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID, image= image });
        }
        public class GeneratePayrollPreviewOut
        {
            public BatchError error;
            public List<PayrollDocument> _ret;
        }
        static public List<PayrollDocument> GeneratePayrollPreview(DateTime date, AppliesToEmployees at, int periodID, out BatchError error)
        {
            // return PayrollServer.GeneratePayrollPreview(ApplicationClient.SessionID, date, at, periodID, out error);
            var _ret = RESTAPIClient.Call<GeneratePayrollPreviewOut>(path + "GeneratePayrollPreview", new { sessionID=ApplicationClient.SessionID, date= date, at= at , periodID = periodID });
            error = _ret.error;
            return _ret._ret;
        }
        public static int GetStaffLoanPayableAccount(int employeeID)
        {
            //return PayrollServer.GetStaffLoanPayableAccount(ApplicationClient.SessionID, employeeID);
            return RESTAPIClient.Call<int>(path + "GetStaffLoanPayableAccount", new { SessionID=ApplicationClient.SessionID, EmployeeID= employeeID });
        }
        static public void GeneratePayrollPeriods(int year, bool ethiopian)
        {
           // PayrollServer.GeneratePayrollPeriods(ApplicationClient.SessionID, year, ethiopian);
           RESTAPIClient.Call<VoidRet>(path + "GeneratePayrollPeriods", new { sessionID=ApplicationClient.SessionID, year= year, ethiopian= ethiopian });
        }

        public static INTAPS.Payroll.PayrollComponentFormula GetPCFormula(int FormulaID)
        {
            // return PayrollServer.GetPCFormula(ApplicationClient.SessionID, FormulaID);
            return RESTAPIClient.Call<INTAPS.Payroll.PayrollComponentFormula>(path + "GetPCFormula", new { sessionID=ApplicationClient.SessionID, FormulaID= FormulaID });
        }

        public static INTAPS.Payroll.PayrollPeriod GetPayPeriod(System.DateTime date)
        {
            //  return PayrollServer.GetPayPeriod(ApplicationClient.SessionID, date);
            return RESTAPIClient.Call<INTAPS.Payroll.PayrollPeriod>(path + "GetPayPeriod2", new { SessionID=ApplicationClient.SessionID, date = date });
        }
        public class GeneratePayrollPreview2Out
        {
            public BatchError error;
            public INTAPS.Payroll.PayrollSetDocument _ret;
        }
        public static INTAPS.Payroll.PayrollSetDocument GeneratePayrollPreview2(System.DateTime date, string title, INTAPS.Payroll.AppliesToEmployees at, int periodID, out INTAPS.Accounting.BatchError error)
        {
            //return PayrollServer.GeneratePayrollPreview2(ApplicationClient.SessionID, date, title, at, periodID, out error);
            var _ret = RESTAPIClient.Call<GeneratePayrollPreview2Out>(path + "GeneratePayrollPreview2", new { sessionID=ApplicationClient.SessionID, date= date, title= title, at= at, periodID= periodID });
            error = _ret.error;
            return _ret._ret;
        }

        public static INTAPS.Payroll.PayrollSetDocument[] getPayrollSetsOfAPeriod(int periodID)
        {
            // return PayrollServer.getPayrollSetsOfAPeriod(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<INTAPS.Payroll.PayrollSetDocument[]>(path + "getPayrollSetsOfAPeriod", new { sessionID =ApplicationClient.SessionID, periodID = periodID });
        }
        public class GetPaymetAmountsOut
        {
            public double[] payroll;
            public double[] _ret;
        }
        public static double[] getPaymetAmounts(int p, int[] empID, out double[] payroll)
        {
            //return PayrollServer.getPaymetAmounts(ApplicationClient.SessionID, p, empID, out payroll);
            var _ret = RESTAPIClient.Call<GetPaymetAmountsOut>(path + "getPaymetAmounts", new { sessionID= ApplicationClient.SessionID, p= p, empID= empID });
            payroll = _ret.payroll;
            return _ret._ret;
        }

        public static string getAppliesToString(INTAPS.Payroll.AppliesToEmployees at)
        {
            //return PayrollServer.getAppliesToString(ApplicationClient.SessionID, at);
            return RESTAPIClient.Call<string>(path + "getAppliesToString", new { sessionID= ApplicationClient.SessionID, at= at });
        }

        public static string GetPayrollTableHTML(INTAPS.Payroll.PayrollSetDocument set, int periodID, int formatID)
        {
            //return PayrollServer.GetPayrollTableHTML(ApplicationClient.SessionID, set, periodID, formatID);
            return RESTAPIClient.Call<string>(path + "GetPayrollTableHTML", new { sessionID= ApplicationClient.SessionID, set= set, periodID= periodID, formatID= formatID });
        }
                
        public static string GetPayrollTableHTML(int setDocumentID, int periodID, int formatID)
        {
            //return PayrollServer.GetPayrollTableHTML(ApplicationClient.SessionID, setDocumentID, periodID, formatID);
            return RESTAPIClient.Call<string>(path + "GetPayrollTableHTML2", new { sessionID= ApplicationClient.SessionID, setDocumentID= setDocumentID, periodID= periodID, formatID= formatID });
        }
        public class TestSheetFormatOut
        {
            public string[] varNames;
            public INTAPS.Evaluator.EData[] _ret;
        }
        public static INTAPS.Evaluator.EData[] testSheetFormat(INTAPS.Payroll.PayrollSheetFormat format, int employeeID, int periodID, out string[] varNames)
        {
            //return PayrollServer.testSheetFormat(ApplicationClient.SessionID, format, employeeID, periodID, out varNames);
            var _ret =RESTAPIClient.Call<TestSheetFormatOut>(path + "testSheetFormat", new { sessionID= ApplicationClient.SessionID, format= format, employeeID= employeeID, periodID = periodID });
            varNames = _ret.varNames;
            return _ret._ret;
        }
        public class TestComponentOut
        {
            public string[] vars; public INTAPS.Evaluator.EData[] vals;
            public INTAPS.Accounting.TransactionOfBatch[] _ret;
        }
        public static INTAPS.Accounting.TransactionOfBatch[] testComponent(System.DateTime date, int PCDID, int formulaID, int employeeID, int periodID, out string[] vars, out INTAPS.Evaluator.EData[] vals)
        {
            // return PayrollServer.testComponent(ApplicationClient.SessionID, date, PCDID, formulaID, employeeID, periodID, out vars, out vals);
            var _ret =RESTAPIClient.Call<TestComponentOut>(path + "testComponent", new { sessionID= ApplicationClient.SessionID, date= date, PCDID= PCDID, formulaID= formulaID, employeeID= employeeID, periodID= periodID });
            vars = _ret.vars;
            vals = _ret.vals;
            return _ret._ret;
        }

        public static int getEmployeePayrollDocumentID(int employeeID, int periodID)
        {
            // return PayrollServer.getEmployeePayrollDocumentID(ApplicationClient.SessionID, employeeID, periodID);
            return RESTAPIClient.Call<int>(path + "getEmployeePayrollDocumentID", new { sessionID= ApplicationClient.SessionID, employeeID= employeeID , periodID = periodID });
        }


        public static object GetSystemParameter(string key)
        {
            return GetSystemParameters(new string[] { key })[0];
        }

        public static INTAPS.Payroll.Employee GetEmployeeByLoginName(string loginName)
        {
            // return PayrollServer.GetEmployeeByLoginName(ApplicationClient.SessionID, loginName);
            return RESTAPIClient.Call<INTAPS.Payroll.Employee>(path + "GetEmployeeByLoginName", new { sessionID= ApplicationClient.SessionID, loginName= loginName });
        }

        public static INTAPS.Payroll.PayrollPeriod getNextPeriod(int periodID)
        {
            //return PayrollServer.getNextPeriod(ApplicationClient.SessionID, periodID);
            return RESTAPIClient.Call<INTAPS.Payroll.PayrollPeriod>(path + "getNextPeriod", new { sessionID= ApplicationClient.SessionID, periodID= periodID });
        }

        public static INTAPS.Payroll.PayrollSetDocument getPayrollSetByEmployee(int periodID, int employeeID)
        {
            //return PayrollServer.getPayrollSetByEmployee(ApplicationClient.SessionID, periodID, employeeID);
            return RESTAPIClient.Call<INTAPS.Payroll.PayrollSetDocument>(path + "getPayrollSetByEmployee", new { sessionID=ApplicationClient.SessionID, periodID= periodID, employeeID= employeeID });
        }

        public static long[] getEmployeVersions(int employeeID)
        {
            //return PayrollServer.getEmployeVersions(ApplicationClient.SessionID, employeeID);
            return RESTAPIClient.Call<long[]>(path + "getEmployeVersions", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID });
        }

        public static INTAPS.Payroll.Employee GetEmployee(int ID, long ticks)
        {
            // return PayrollServer.GetEmployee(ApplicationClient.SessionID, ID, ticks);
            return RESTAPIClient.Call<INTAPS.Payroll.Employee>(path + "GetEmployee2", new { sessionID=ApplicationClient.SessionID, ID= ID, ticks= ticks });
        }

        public static bool isEmployeeUsedInPayroll(int employeeID, long ver)
        {
            // return PayrollServer.isEmployeeUsedInPayroll(ApplicationClient.SessionID, employeeID, ver);
            return RESTAPIClient.Call<bool>(path + "isEmployeeUsedInPayroll", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID, ver= ver });
        }

        public static void deleteEmployeeVersion(int ID, long ticks)
        {
           // PayrollServer.deleteEmployeeVersion(ApplicationClient.SessionID, ID, ticks);
           RESTAPIClient.Call<VoidRet>(path + "deleteEmployeeVersion", new { sessionID=ApplicationClient.SessionID, ID= ID, ticks= ticks });
        }

        public static AccountBase[] GetParentAcccountsByEmployee(int id)
        {
            // AccountBase[] acccountsByEmployee = PayrollServer.GetParentAcccountsByEmployee(ApplicationClient.SessionID, id);
            AccountBase[] acccountsByEmployee =RESTAPIClient.Call<AccountBase[]>(path + "GetParentAcccountsByEmployee", new { sessionID=ApplicationClient.SessionID, employeeID= id });
            return acccountsByEmployee;
        }
        public static int GetEmployeeSubAccount(int employeeID,int accountID)
        {
            // int acocunt= PayrollServer.GetEmployeeSubAccount(ApplicationClient.SessionID, employeeID,accountID);
            int acocunt =RESTAPIClient.Call<int>(path + "GetEmployeeSubAccount", new { sessionID=ApplicationClient.SessionID, employeeID= employeeID, accountID= accountID });
            return accountID;
        }
    }
}
