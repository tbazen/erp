namespace INTAPS.Payroll.Client
{
    partial class DataBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.wbData = new INTAPS.UI.HTML.ControlBrowser();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // wbData
            // 
            this.wbData.AllowWebBrowserDrop = false;
            this.wbData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbData.Location = new System.Drawing.Point(0, 0);
            this.wbData.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbData.Name = "wbData";
            this.wbData.ScriptErrorsSuppressed = true;
            this.wbData.Size = new System.Drawing.Size(594, 362);
            this.wbData.StyleSheetFile = "berp.css";
            this.wbData.TabIndex = 4;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DataBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 362);
            this.Controls.Add(this.wbData);
            this.Name = "DataBrowser";
            this.Text = "Input Data";
            this.ResumeLayout(false);

        }

        #endregion

        private INTAPS.UI.HTML.ControlBrowser wbData;
        private System.Windows.Forms.Timer timer1;
    }
}