using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web.UI.HtmlControls;

namespace INTAPS.Payroll.Client
{
    public partial class DataBrowser : Form, IPayrollContentForm, INTAPS.UI.HTML.IHTMLBuilder
    {
        PayrollPeriod m_pp;
        Employee m_emp;
        OrgUnit m_org;
        PayrollComponentDefination m_def;
        PayrollComponentFormula m_f;

        List<PayrollComponentData> m_LoadedDataInfo;
        List<object> m_LoadedData;
        HtmlTable m_tableOuter = null;
        public DataBrowser()
        {
            InitializeComponent();
            wbData.StyleSheetFile = "berp.css";
        }
        bool NotShown = true;
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            NotShown = false;
            LoadData();
        }
        public string DocumentHeader
        {
            get
            {
                string h;
                if(m_def==null)
                    h="Select payment component.";
                else
                    if(m_f==null)
                        h=m_def.Name;
                    else
                        h=m_f.Name;
                return @"<h1 style=""border-right: black solid; border-top: black solid; border-left: black solid; color: white; border-bottom: black solid; background-color: dimgray"">"
                + System.Web.HttpUtility.HtmlEncode(h) + "</h1>";
            }
        }
        void LoadData()
        {
            MainForm.SetSaveCommandEnabled(false);
            if (m_pp == null)
            {
                wbData.DocumentText = DocumentHeader+ "<i>Select period.</i>";
                return;
            }
            if (m_emp == null && m_org == null)
            {
                wbData.DocumentText = DocumentHeader + "<i>Select an organizational unit or employee.</i>";
                return;
            }
            if (m_def == null && m_f == null)
            {
                wbData.DocumentText = DocumentHeader + "<i>Select a payroll component</i>";
                return;
            }
            if (NotShown)
                return;
            m_LoadedData = null;
            m_LoadedDataInfo = null;

            if (m_def.SupportFormula)
            {
                if (m_f == null)
                {
                    wbData.DocumentText = DocumentHeader + "<i>Select a formula under " + System.Web.HttpUtility.HtmlEncode(m_def.Name) + "</i>";
                    return;
                }
                PayrollComponentData[] data;
                if (m_emp == null)
                    data = PayrollClient.GetPCData(m_def.ID, m_f.ID, AppliesToObjectType.OrgUnit, m_org.ObjectIDVal, m_pp.id, false);
                else
                    data = PayrollClient.GetPCData(m_def.ID, m_f.ID, AppliesToObjectType.Employee, m_emp.ObjectIDVal, m_pp.id, false);
                PopulateData(data);
            }
            else
            {
                PayrollComponentData[] data;
                if (m_emp == null)
                    data = PayrollClient.GetPCData(m_def.ID, -1, AppliesToObjectType.OrgUnit, m_org.ObjectIDVal, m_pp.id, false);
                else
                    data = PayrollClient.GetPCData(m_def.ID, -1, AppliesToObjectType.Employee, m_emp.ObjectIDVal, m_pp.id, false);
                PopulateData(data);
            }
            IPayrollComponentUIHandler comp = PayrollClient.GetPCDUIHandler(m_def.ID);
            if (!comp.HaveEditor())
            {
                MainForm.SetSaveCommandEnabled(false);
                MainForm.SetSaveCommandText("N/A");
                if (m_emp == null)
                    wbData.DocumentText = comp.GetDefaultHTML(new AppliesToEmployees(m_org.ObjectIDVal, false));
                else
                    wbData.DocumentText = comp.GetDefaultHTML(new AppliesToEmployees(m_emp.ObjectIDVal));
            }
        }
        private void PopulateData(PayrollComponentData[] data)
        {
            m_LoadedData = new List<object>();
            (m_LoadedDataInfo = new List<PayrollComponentData>()).AddRange(data);
            if (data.Length == 0)
            {
                MainForm.SetSaveCommandText("Set Data");
                MainForm.SetSaveCommandEnabled(true);
                wbData.DocumentText = DocumentHeader + "<i>Data not set</i>";
            }
            else
            {
                try
                {
                    m_tableOuter = new HtmlTable();
                    m_tableOuter.CellSpacing = 0;
                    HtmlTableCell cell;
                    HtmlTableRow row;
                    m_tableOuter.ID = "tab";
                    m_tableOuter.Width = "100%";
                    int i = 0;
                    foreach (PayrollComponentData d in data)
                    {
                        object aditionaldata = PayrollClient.GetPCAdditionalData(d.ID);

                        row=INTAPS.UI.HTML.HTMLBuilder.CreateHtmlRow(m_tableOuter
    , INTAPS.UI.HTML.HTMLBuilder.CreateHtmlCell("<a href=Edit?id=" + d.ID + "> Edit </a>")
, cell = INTAPS.UI.HTML.HTMLBuilder.CreateHtmlCell("<a href=Delete?id=" + d.ID + "> X </a>")
);
                        row.ID = "r" + (2 * i + 1);
                        row.BgColor = "lightgray";

                        INTAPS.UI.HTML.HTMLBuilder.CreateHtmlRow(m_tableOuter
                        , INTAPS.UI.HTML.HTMLBuilder.CreateHtmlCell(PayrollClient.GetPCDUIHandler(d.PCDID).GetHTML(d, aditionaldata)
                        , "", "d" + i, 1, 2)).ID = "r" + (2 * i);
                        
                        cell.Align = "right";
                        m_LoadedData.Add(aditionaldata);
                        i++;
                    }
                    MainForm.SetSaveCommandText("Add Data");
                    MainForm.SetSaveCommandEnabled(true);
                    wbData.LoadControl(this);
                }
                catch (Exception ex)
                {
                    wbData.DocumentText = "DocumentHeader+<i>Error loading data</i><br/>" + System.Web.HttpUtility.HtmlEncode(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        #region ISpreadContentForm Members

        public bool ShowEmbeded
        {
            get { return true; }
        }

        public bool SupportStandardCommand(MainFormStandardButtons cmd)
        {
            return true;
        }

        public void FilterByEmp(Employee e,OrgUnit parent)
        {
            m_emp = e;
            m_org = parent;
            LoadData();
        }

        public void FilterByOrg(OrgUnit o)
        {
            m_org = o;
            m_emp = null;
            LoadData();
        }

        public void FilterByPCD(PayrollComponentDefination pcd)
        {
            m_def = pcd;
            m_f = null;
            LoadData();
        }

        public void FilterByFormula(PayrollComponentFormula formula, PayrollComponentDefination parent)
        {
            m_f = formula;
            m_def = parent;
            LoadData();
        }

        public void FilterByPayPeriod(PayrollPeriod pp)
        {
            m_pp = pp;
            LoadData();
        }

        public void DoStandardCommand(MainFormStandardButtons cmd)
        {
            switch (cmd)
            {
                case MainFormStandardButtons.Save:
                    PayrollComponentData datainfo = new PayrollComponentData();
                    datainfo.DataDate = DateTime.Now;
                    if (m_emp == null)
                        datainfo.at = new AppliesToEmployees(m_org.ObjectIDVal, false);
                    else
                        datainfo.at = new AppliesToEmployees(m_emp.ObjectIDVal, true);

                    datainfo.periodRange.PeriodFrom = m_pp.id;
                    datainfo.periodRange.PeriodTo = m_pp.id;
                    datainfo.PCDID = m_def.ID;
                    datainfo.FormulaID = m_f == null ? -1 : m_f.ID;
                    PCDEditorHost host = new PCDEditorHost(datainfo);
                    if (host.ShowDialog() == DialogResult.OK)
                    {
                        LoadData();
                    }
                    break;
                case MainFormStandardButtons.Refresh:
                    LoadData();
                    break;
            }
        }

        public Control GetControl()
        {
            return this;
        }

        public string GetStandardCommandText(MainFormStandardButtons cmd)
        {
            switch (cmd)
            {
                case MainFormStandardButtons.Refresh:
                    return "Refresh";
                case MainFormStandardButtons.Save:
                    return "Save";
            }
            return "";
        }

        #endregion

        #region IHTMLBuilder Members

        public string GetOuterHtml()
        {
            if (m_tableOuter == null)
                return DocumentHeader + "<b> Not set</b>";
            return DocumentHeader + INTAPS.UI.HTML.HTMLBuilder.ControlToString(m_tableOuter);
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            int index=-1;
            int id;
            int i;
            switch (path)
            {
                case "Edit":
                    id= int.Parse((string)query["id"]);
                    for(i=0;i<m_LoadedDataInfo.Count;i++)
                        if(m_LoadedDataInfo[i].ID==id)
                        {
                            index=i;
                            break;
                        }
                    PayrollComponentData d = m_LoadedDataInfo[index];
                    object aditionaldata = m_LoadedData[index];
                    PCDEditorHost host = new PCDEditorHost(d, aditionaldata);
                    if (host.ShowDialog() == DialogResult.OK)
                    {
                        d = host.FormDataInfo;
                        aditionaldata = host.FormData;
                        m_LoadedData[index] = aditionaldata;
                        m_LoadedDataInfo[index] = d;
                        wbData.UpdateElementInnerHtml("d" + index, PayrollClient.GetPCDUIHandler(d.PCDID).GetHTML(d, aditionaldata));
                    }
                    return true;
                case "Delete":
                    if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this data?"))
                        return true;
                                        id= int.Parse((string)query["id"]);
                    for(i=0;i<m_LoadedDataInfo.Count;i++)
                        if(m_LoadedDataInfo[i].ID==id)
                        {
                            index=i;
                            break;
                        }

                    try
                    {
                        PayrollClient.DeleteData(m_LoadedDataInfo[index].ID);
                        m_LoadedData.RemoveAt(index);
                        m_LoadedDataInfo.RemoveAt(index);
                        m_tableOuter.Rows.RemoveAt(2 * index);
                        m_tableOuter.Rows.RemoveAt(2 * index);
                        wbData.UpdateElement("tab", m_tableOuter);
                    }
                    catch (Exception ex)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete data.", ex);
                    }
                    return true;

            }
            return false;
        }

        public void SetHost(INTAPS.UI.HTML.IHTMLDocumentHost host)
        {

        }

        public void Build()
        {

        }

        public string WindowCaption
        {
            get
            {
                return "Payroll data";
            }
        }

        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
            LoadData();
            timer1.Enabled = false;
        }

    }
}