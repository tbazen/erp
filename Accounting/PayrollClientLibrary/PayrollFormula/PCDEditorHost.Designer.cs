namespace INTAPS.Payroll.Client
{
    partial class PCDEditorHost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PCDEditorHost));
            this.pnlDialog = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.pnlDomain = new System.Windows.Forms.Panel();
            this.periodRange = new INTAPS.Payroll.Client.PayrollPeriodRangeSelector();
            this.pnlDialog.SuspendLayout();
            this.pnlDomain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlDialog
            // 
            this.pnlDialog.Controls.Add(this.btnOk);
            this.pnlDialog.Controls.Add(this.btnCancel);
            this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDialog.Location = new System.Drawing.Point(0, 293);
            this.pnlDialog.Name = "pnlDialog";
            this.pnlDialog.Size = new System.Drawing.Size(312, 38);
            this.pnlDialog.TabIndex = 4;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(164, 7);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(235, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlDomain
            // 
            this.pnlDomain.Controls.Add(this.periodRange);
            this.pnlDomain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDomain.Location = new System.Drawing.Point(0, 0);
            this.pnlDomain.Name = "pnlDomain";
            this.pnlDomain.Size = new System.Drawing.Size(312, 94);
            this.pnlDomain.TabIndex = 5;
            // 
            // periodRange
            // 
            this.periodRange.Location = new System.Drawing.Point(0, 3);
            this.periodRange.Name = "periodRange";
            this.periodRange.Size = new System.Drawing.Size(311, 87);
            this.periodRange.TabIndex = 0;
            // 
            // PCDEditorHost
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(312, 331);
            this.Controls.Add(this.pnlDomain);
            this.Controls.Add(this.pnlDialog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PCDEditorHost";
            this.Text = "PCDEditorHost";
            this.Load += new System.EventHandler(this.PCDEditorHost_Load);
            this.pnlDialog.ResumeLayout(false);
            this.pnlDomain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlDialog;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlDomain;
        private PayrollPeriodRangeSelector periodRange;
    }
}