using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class PCDEditorHost : Form
    {
        public object FormData;
        public PayrollComponentData FormDataInfo;
        IPayrollComponentUIHandler m_uihandler;
        
        Control m_UIControl;
        bool Add;
        public PCDEditorHost()
        {
            InitializeComponent();
        }
        void SetUpForm(int PCDID)
        {
            PayrollComponentDefination pcdef = PayrollClient.GetPCDefination(PCDID);
            this.Text = pcdef.Name + " data";

            m_uihandler = PayrollClient.GetPCDUIHandler(PCDID);
            m_UIControl = m_uihandler.CreateEditor(null);
            if (m_UIControl == null)
            {
                this.ClientSize = new Size(
   pnlDomain.Width
    , pnlDialog.Height + pnlDomain.Height);

            }
            {
                this.ClientSize = new Size(
                    m_UIControl.Width > pnlDomain.Width ? m_UIControl.Width : pnlDomain.Width
                    , m_UIControl.Height + pnlDialog.Height + pnlDomain.Height);
                m_UIControl.Dock = DockStyle.Fill;
                this.Controls.Add(m_UIControl);
                m_UIControl.BringToFront();
                m_uihandler.ConfigureEditor(m_UIControl, FormDataInfo.at);
            }
            periodRange.LoadData(PayrollClient.CurrentYear);
            periodRange.SetData(FormDataInfo.periodRange);
            
        }
        public PCDEditorHost(PayrollComponentData dataInfo)
            : this()
        {
            FormDataInfo = dataInfo;
            FormData = null;
            SetUpForm(dataInfo.PCDID);
            Add = true;
        }
        public PCDEditorHost(PayrollComponentData datainfo, object data)
            : this()
        {
            FormData = data;
            FormDataInfo = datainfo;
            SetUpForm(datainfo.PCDID);
            m_uihandler.SetEditorData(m_UIControl, datainfo, data);
            Add = false;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (!m_uihandler.ValidateEditorData(m_UIControl))
                    return;
                FormData = m_uihandler.GetEditorData(m_UIControl);
                if(!periodRange.IsDataValid())
                    return;
                FormDataInfo.periodRange= periodRange.GetData();
                if (Add)
                {
                    FormDataInfo.ID=PayrollClient.CreatePCData(FormDataInfo, FormData);
                    Add = false;
                }
                else
                    PayrollClient.UpdatePCData(FormDataInfo, FormData);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't not save data", ex);
            }
        }

        private void PCDEditorHost_Load(object sender, EventArgs e)
        {

        }
    }
}