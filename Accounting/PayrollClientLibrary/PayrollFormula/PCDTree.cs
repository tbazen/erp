using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    class PCDTree:TreeView
    {
        public void LoadData()
        {
            this.Nodes.Clear();
            foreach (PayrollComponentDefination pcd in PayrollClient.GetPCDefinations())
            {
                PayrollComponentFormula[] fs = PayrollClient.GetPCFormulae(pcd.ID);
                //if (fs.Length == 1)
                //{
                //    AddFormulaNode(this.Nodes, fs[0]);
                //}
                //else
                //{
                    TreeNode n = new TreeNode();
                    n.Text = pcd.Name+"-"+pcd.ID;
                    n.Tag = pcd;
                    this.Nodes.Add(n);
                    foreach (PayrollComponentFormula f in fs)
                    {
                        AddFormulaNode(n.Nodes, f);
                    }
                //}
            }
            this.ExpandAll();
        }

        private void AddFormulaNode(TreeNodeCollection nodes, PayrollComponentFormula f)
        {
            TreeNode fn = new TreeNode();
            SetFormulaNode( fn,f);
            nodes.Add(fn);
        }
        private void SetFormulaNode(TreeNode fn, PayrollComponentFormula f)
        {
            fn.Text = f.Name+"-"+f.ID;
            fn.Tag = f;
        }
        public PayrollComponentDefination SelectedPCD
        {
            get
            {
                if(this.SelectedNode==null)
                    return null;
                if (this.SelectedNode.Tag is PayrollComponentDefination)
                    return (PayrollComponentDefination)this.SelectedNode.Tag;
                if (this.SelectedNode.Tag is PayrollComponentFormula)
                {
                    PayrollComponentFormula f = (PayrollComponentFormula)this.SelectedNode.Tag;
                    return PayrollClient.GetPCDefination(f.PCDID);

                }
                return (PayrollComponentDefination)this.SelectedNode.Parent.Tag;
            }
        }
        public PayrollComponentFormula SelectedFormula
        {
            get
            {
                if (this.SelectedNode == null)
                    return null;
                return this.SelectedNode.Tag as PayrollComponentFormula;
            }
        }
        public void AddFormula(PayrollComponentFormula payrollComponentFormula)
        {
            foreach (TreeNode n in this.Nodes)
            {
                if (((PayrollComponentDefination)n.Tag).ID == payrollComponentFormula.PCDID)
                {
                    AddFormulaNode(n.Nodes, payrollComponentFormula);
                    return;
                }
            }
        }
        internal void UpdateFormula(PayrollComponentFormula payrollComponentFormula)
        {
            foreach (TreeNode n in this.Nodes)
            {
                foreach (TreeNode fn in n.Nodes)
                {
                    if (((PayrollComponentFormula)fn.Tag).ID == payrollComponentFormula.ID)
                    {
                        SetFormulaNode(fn, payrollComponentFormula);
                        return;
                    }
                }
            }
        }
        public void DeleteFormula(int FID)
        {
            foreach (TreeNode n in this.Nodes)
            {
                foreach (TreeNode fn in n.Nodes)
                {
                    if (((PayrollComponentFormula)fn.Tag).ID == FID)
                    {
                        fn.Remove();
                        return;
                    }
                }
            }
        }
        public void UpdatePCD(PayrollComponentDefination payrollComponentDefination)
        {
            foreach (TreeNode n in this.Nodes)
            {
                if (n.Tag is PayrollComponentDefination)
                {
                    if (((PayrollComponentDefination)n.Tag).ID == payrollComponentDefination.ID)
                    {

                        n.Tag = payrollComponentDefination;
                        return;
                    }
                }
            }
        }
    }
}
