namespace INTAPS.Payroll.Client
{
    partial class PCDTreeEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pcdTree = new INTAPS.Payroll.Client.PCDTree();
            this.ctxtEditor = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.miEditPCD = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxtEditor.SuspendLayout();
            this.SuspendLayout();
            // 
            // pcdTree
            // 
            this.pcdTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcdTree.HideSelection = false;
            this.pcdTree.Location = new System.Drawing.Point(0, 0);
            this.pcdTree.Name = "pcdTree";
            this.pcdTree.Size = new System.Drawing.Size(289, 342);
            this.pcdTree.TabIndex = 0;
            this.pcdTree.DoubleClick += new System.EventHandler(this.pcdTree_DoubleClick);
            this.pcdTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.pcdTree_AfterSelect);
            this.pcdTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pcdTree_MouseDown);
            this.pcdTree.KeyDown += new System.Windows.Forms.KeyEventHandler(this.pcdTree_KeyDown);
            // 
            // ctxtEditor
            // 
            this.ctxtEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAdd,
            this.miEdit,
            this.miDelete,
            this.miEditPCD,
            this.refreshToolStripMenuItem});
            this.ctxtEditor.Name = "ctxtEditor";
            this.ctxtEditor.Size = new System.Drawing.Size(212, 136);
            this.ctxtEditor.Opening += new System.ComponentModel.CancelEventHandler(this.ctxtEditor_Opening);
            // 
            // miAdd
            // 
            this.miAdd.Name = "miAdd";
            this.miAdd.Size = new System.Drawing.Size(211, 22);
            this.miAdd.Text = "Add Formula";
            this.miAdd.Click += new System.EventHandler(this.miAdd_Click);
            // 
            // miEdit
            // 
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(211, 22);
            this.miEdit.Text = "Edit Formula";
            this.miEdit.Click += new System.EventHandler(this.miEdit_Click);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(211, 22);
            this.miDelete.Text = "Delete Formula";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // miEditPCD
            // 
            this.miEditPCD.Name = "miEditPCD";
            this.miEditPCD.Size = new System.Drawing.Size(211, 22);
            this.miEditPCD.Text = "Edit Payment Component";
            this.miEditPCD.Click += new System.EventHandler(this.miEditPCD_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // PCDTreeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pcdTree);
            this.Name = "PCDTreeEditor";
            this.Size = new System.Drawing.Size(289, 342);
            this.ctxtEditor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PCDTree pcdTree;
        private System.Windows.Forms.ContextMenuStrip ctxtEditor;
        private System.Windows.Forms.ToolStripMenuItem miAdd;
        private System.Windows.Forms.ToolStripMenuItem miEdit;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.ToolStripMenuItem miEditPCD;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
    }
}
