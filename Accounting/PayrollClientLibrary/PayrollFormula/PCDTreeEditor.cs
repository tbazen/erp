using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public delegate void FormulaSelectedHandler(PCDTreeEditor source,PayrollComponentFormula selection,PayrollComponentDefination parent);
    public delegate void PayrollComponentedSelectedHandler(PCDTreeEditor source,PayrollComponentDefination selection);
    public partial class PCDTreeEditor : UserControl
    {
        public event FormulaSelectedHandler FormulaSelected;
        public event PayrollComponentedSelectedHandler PayrollComponentSelected;
        public PCDTreeEditor()
        {
            InitializeComponent();
        }
        public void LoadData()
        {
            pcdTree.LoadData();
        }
        private void pcdTree_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pcdTree.SelectedNode = pcdTree.HitTest(pcdTree.PointToClient(Cursor.Position)).Node;
                if (pcdTree.SelectedNode != null)
                {
                    ShowContextMenu();
                }
            }
        }

        private void ShowContextMenu()
        {
            Point p;
            p = new Point(pcdTree.SelectedNode.Bounds.Left, pcdTree.SelectedNode.Bounds.Bottom);
            miAdd.Enabled = pcdTree.SelectedPCD.SupportFormula;
            miEdit.Enabled = pcdTree.SelectedFormula != null;
            miDelete.Enabled = pcdTree.SelectedFormula != null;
            miEditPCD.Enabled = pcdTree.SelectedPCD != null && pcdTree.SelectedFormula == null;
            ctxtEditor.Show(pcdTree, p);
        }

        private void pcdTree_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Apps)
            {
                if(pcdTree.SelectedNode!=null)
                    ShowContextMenu();
            }
        }

        private void miAdd_Click(object sender, EventArgs e)
        {
            PayrollComponentEditor pe = new PayrollComponentEditor(pcdTree.SelectedPCD.ID);
            pe.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }

        private void miEdit_Click(object sender, EventArgs e)
        {
            PayrollComponentEditor pe = new PayrollComponentEditor(pcdTree.SelectedFormula);
            pe.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }

        private void miDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected formula from the database?"))
                {
                    PayrollClient.DeletePCForumla(pcdTree.SelectedFormula.ID);
                    pcdTree.DeleteFormula(pcdTree.SelectedFormula.ID);
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete the formula.", ex);
            }
        }

        private void pcdTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (pcdTree.SelectedFormula != null)
            {
                if (FormulaSelected != null)
                    FormulaSelected(this, pcdTree.SelectedFormula, pcdTree.SelectedPCD);
            }
            else if (pcdTree.SelectedPCD != null)
            {
                if (PayrollComponentSelected != null)
                    PayrollComponentSelected(this, pcdTree.SelectedPCD);
            }
        }

        private void pcdTree_DoubleClick(object sender, EventArgs e)
        {
            if (pcdTree.SelectedFormula != null)
            {
            }
        }

        private void miEditPCD_Click(object sender, EventArgs e)
        {
            SetPCDDomain p = new SetPCDDomain(pcdTree.SelectedPCD);
            if (p.ShowDialog() == DialogResult.OK)
            {
                pcdTree.UpdatePCD(p.FormData);
            }
        }
        public PayrollComponentFormula SelectedFormula
        {
            get
            {
                return pcdTree.SelectedFormula;
            }
        }
        public PayrollComponentDefination SelectedPCD
        {
            get
            {
                return pcdTree.SelectedPCD;
            }
        }

        private void ctxtEditor_Opening(object sender, CancelEventArgs e)
        {

        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LoadData();
        }
    }
}
