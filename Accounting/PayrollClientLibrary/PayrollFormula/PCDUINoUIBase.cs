using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll.Client.PCD
{
    public class PCDUINoUI : IPayrollComponentUIHandler
    {

        #region IPayrollComponentUIHandler Members
        public System.Windows.Forms.Control CreateEditor(IPayrollComponentHanlderHost h)
        {
            return null;
        }
        public bool ConfigureEditor(System.Windows.Forms.Control editor, AppliesToEmployees at)
        {
            return false;
        }
        public bool SetEditorData(System.Windows.Forms.Control editor, PayrollComponentData dataInfo, object data)
        {
            return false;
        }
        public object GetEditorData(System.Windows.Forms.Control editor)
        {
            return null;
        }
        public bool ValidateEditorData(System.Windows.Forms.Control editor)
        {
            return false;
        }
        public bool HaveEditor()
        {
            return false;
        }
        public virtual string GetHTML(PayrollComponentData dataInfo, object aditionaldata)
        {
            return "";
        }
        public virtual string GetDefaultHTML(AppliesToEmployees at)
        {
            return this.ToString();
        }
        #endregion
    }
    public class PCDUIGrossSalary : PCDUINoUI
    {
        public override string GetDefaultHTML(AppliesToEmployees at)
        {
            if (!at.All)
            {
                if (at.Employees!=null && at.Employees.Length == 1)
                {
                    return System.Web.HttpUtility.HtmlEncode("Gross salary:"+ PayrollClient.GetEmployee(at.Employees[0]).grossSalary.ToString("0,0.00"));
                }
            }
            return "Gross Salary";
        }
    }
    public class PCDUIIncomeTax : PCDUINoUI
    {
        public override string GetDefaultHTML(AppliesToEmployees at)
        {
            return "Income tax will be calculated when payroll is generated";
        }
    }
    
}
