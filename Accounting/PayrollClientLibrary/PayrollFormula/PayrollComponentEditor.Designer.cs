namespace INTAPS.Payroll.Client
{
    partial class PayrollComponentEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PayrollComponentEditor));
            this.tabPayroll = new System.Windows.Forms.TabControl();
            this.pageGeneral = new System.Windows.Forms.TabPage();
            this.chkDeduct = new System.Windows.Forms.CheckBox();
            this.periodRange = new INTAPS.Payroll.Client.PayrollPeriodRangeSelector();
            this.dtpDate = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.lblEnrolled = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.employeeDomain = new INTAPS.Payroll.Client.EmployeeDomainControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMoreInfo = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.pageCalc = new System.Windows.Forms.TabPage();
            this.spltFormula = new System.Windows.Forms.SplitContainer();
            this.dgVars = new INTAPS.UI.EDataGridView();
            this.Variable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Formula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVarDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblVarDesc = new System.Windows.Forms.Label();
            this.lblVariable = new System.Windows.Forms.Label();
            this.dgTransactions = new INTAPS.UI.EDataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.pnlDialog = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.payrollPeriodSelector = new INTAPS.Payroll.Client.PayrollPeriodSelector();
            this.buttonEvalue = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.employeePlaceholder = new INTAPS.Payroll.Client.EmployeePlaceHolder();
            this.listVariables = new System.Windows.Forms.ListView();
            this.listTransactions = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPayroll.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pageCalc.SuspendLayout();
            this.spltFormula.Panel1.SuspendLayout();
            this.spltFormula.Panel2.SuspendLayout();
            this.spltFormula.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTransactions)).BeginInit();
            this.pnlDialog.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPayroll
            // 
            this.tabPayroll.Controls.Add(this.pageGeneral);
            this.tabPayroll.Controls.Add(this.pageCalc);
            this.tabPayroll.Controls.Add(this.tabPage1);
            this.tabPayroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPayroll.Location = new System.Drawing.Point(0, 0);
            this.tabPayroll.Name = "tabPayroll";
            this.tabPayroll.SelectedIndex = 0;
            this.tabPayroll.Size = new System.Drawing.Size(673, 480);
            this.tabPayroll.TabIndex = 1;
            // 
            // pageGeneral
            // 
            this.pageGeneral.Controls.Add(this.chkDeduct);
            this.pageGeneral.Controls.Add(this.periodRange);
            this.pageGeneral.Controls.Add(this.dtpDate);
            this.pageGeneral.Controls.Add(this.lblEnrolled);
            this.pageGeneral.Controls.Add(this.groupBox1);
            this.pageGeneral.Controls.Add(this.label2);
            this.pageGeneral.Controls.Add(this.label5);
            this.pageGeneral.Controls.Add(this.txtMoreInfo);
            this.pageGeneral.Controls.Add(this.txtName);
            this.pageGeneral.Location = new System.Drawing.Point(4, 22);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(637, 445);
            this.pageGeneral.TabIndex = 0;
            this.pageGeneral.Text = "General Informaton";
            this.pageGeneral.UseVisualStyleBackColor = true;
            // 
            // chkDeduct
            // 
            this.chkDeduct.AutoSize = true;
            this.chkDeduct.Checked = true;
            this.chkDeduct.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDeduct.Location = new System.Drawing.Point(8, 77);
            this.chkDeduct.Name = "chkDeduct";
            this.chkDeduct.Size = new System.Drawing.Size(75, 17);
            this.chkDeduct.TabIndex = 26;
            this.chkDeduct.Text = "Deduction";
            this.chkDeduct.UseVisualStyleBackColor = true;
            // 
            // periodRange
            // 
            this.periodRange.Location = new System.Drawing.Point(9, 261);
            this.periodRange.Name = "periodRange";
            this.periodRange.Size = new System.Drawing.Size(311, 87);
            this.periodRange.TabIndex = 25;
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(94, 21);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(182, 20);
            this.dtpDate.Style = INTAPS.Ethiopic.ETDateStyle.Gregorian;
            this.dtpDate.TabIndex = 24;
            // 
            // lblEnrolled
            // 
            this.lblEnrolled.AutoSize = true;
            this.lblEnrolled.Location = new System.Drawing.Point(4, 21);
            this.lblEnrolled.Name = "lblEnrolled";
            this.lblEnrolled.Size = new System.Drawing.Size(33, 13);
            this.lblEnrolled.TabIndex = 23;
            this.lblEnrolled.Text = "Date:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.employeeDomain);
            this.groupBox1.Location = new System.Drawing.Point(338, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 429);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Formula Applies To";
            // 
            // employeeDomain
            // 
            this.employeeDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employeeDomain.Location = new System.Drawing.Point(3, 16);
            this.employeeDomain.Name = "employeeDomain";
            this.employeeDomain.Size = new System.Drawing.Size(290, 410);
            this.employeeDomain.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Additional Information";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Formula Name";
            // 
            // txtMoreInfo
            // 
            this.txtMoreInfo.AcceptsReturn = true;
            this.txtMoreInfo.Location = new System.Drawing.Point(7, 129);
            this.txtMoreInfo.Multiline = true;
            this.txtMoreInfo.Name = "txtMoreInfo";
            this.txtMoreInfo.Size = new System.Drawing.Size(303, 104);
            this.txtMoreInfo.TabIndex = 18;
            // 
            // txtName
            // 
            this.txtName.AcceptsReturn = true;
            this.txtName.Location = new System.Drawing.Point(94, 50);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(216, 20);
            this.txtName.TabIndex = 19;
            // 
            // pageCalc
            // 
            this.pageCalc.Controls.Add(this.spltFormula);
            this.pageCalc.Location = new System.Drawing.Point(4, 22);
            this.pageCalc.Name = "pageCalc";
            this.pageCalc.Padding = new System.Windows.Forms.Padding(3);
            this.pageCalc.Size = new System.Drawing.Size(637, 445);
            this.pageCalc.TabIndex = 1;
            this.pageCalc.Text = "Calculation";
            this.pageCalc.UseVisualStyleBackColor = true;
            // 
            // spltFormula
            // 
            this.spltFormula.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spltFormula.Location = new System.Drawing.Point(3, 3);
            this.spltFormula.Name = "spltFormula";
            this.spltFormula.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spltFormula.Panel1
            // 
            this.spltFormula.Panel1.Controls.Add(this.dgVars);
            this.spltFormula.Panel1.Controls.Add(this.lblVarDesc);
            this.spltFormula.Panel1.Controls.Add(this.lblVariable);
            // 
            // spltFormula.Panel2
            // 
            this.spltFormula.Panel2.Controls.Add(this.dgTransactions);
            this.spltFormula.Panel2.Controls.Add(this.label4);
            this.spltFormula.Size = new System.Drawing.Size(631, 439);
            this.spltFormula.SplitterDistance = 263;
            this.spltFormula.TabIndex = 3;
            // 
            // dgVars
            // 
            this.dgVars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVars.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Variable,
            this.Formula,
            this.colVarDesc});
            this.dgVars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgVars.HorizontalEnterNavigationMode = true;
            this.dgVars.Location = new System.Drawing.Point(0, 28);
            this.dgVars.Name = "dgVars";
            this.dgVars.Size = new System.Drawing.Size(631, 183);
            this.dgVars.SuppessEnterKey = false;
            this.dgVars.TabIndex = 2;
            this.dgVars.SelectionChanged += new System.EventHandler(this.dgVars_SelectionChanged);
            // 
            // Variable
            // 
            this.Variable.HeaderText = "Variable";
            this.Variable.Name = "Variable";
            this.Variable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Formula
            // 
            this.Formula.HeaderText = "Formula/Value";
            this.Formula.Name = "Formula";
            this.Formula.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Formula.Width = 200;
            // 
            // colVarDesc
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.colVarDesc.DefaultCellStyle = dataGridViewCellStyle1;
            this.colVarDesc.HeaderText = "Description";
            this.colVarDesc.Name = "colVarDesc";
            this.colVarDesc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colVarDesc.Width = 200;
            // 
            // lblVarDesc
            // 
            this.lblVarDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVarDesc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblVarDesc.Location = new System.Drawing.Point(0, 211);
            this.lblVarDesc.Name = "lblVarDesc";
            this.lblVarDesc.Size = new System.Drawing.Size(631, 52);
            this.lblVarDesc.TabIndex = 3;
            // 
            // lblVariable
            // 
            this.lblVariable.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblVariable.Location = new System.Drawing.Point(0, 0);
            this.lblVariable.Name = "lblVariable";
            this.lblVariable.Size = new System.Drawing.Size(631, 28);
            this.lblVariable.TabIndex = 1;
            this.lblVariable.Text = "Variables";
            this.lblVariable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgTransactions
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTransactions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTransactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTransactions.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgTransactions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTransactions.HorizontalEnterNavigationMode = true;
            this.dgTransactions.Location = new System.Drawing.Point(0, 28);
            this.dgTransactions.Name = "dgTransactions";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgTransactions.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgTransactions.Size = new System.Drawing.Size(631, 144);
            this.dgTransactions.SuppessEnterKey = false;
            this.dgTransactions.TabIndex = 3;
            this.dgTransactions.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgTransactions_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Account";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Formula/Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(631, 28);
            this.label4.TabIndex = 2;
            this.label4.Text = "Transactions";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlDialog
            // 
            this.pnlDialog.Controls.Add(this.btnOk);
            this.pnlDialog.Controls.Add(this.btnCancel);
            this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDialog.Location = new System.Drawing.Point(0, 480);
            this.pnlDialog.Name = "pnlDialog";
            this.pnlDialog.Size = new System.Drawing.Size(673, 38);
            this.pnlDialog.TabIndex = 3;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(525, 7);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(596, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listTransactions);
            this.tabPage1.Controls.Add(this.listVariables);
            this.tabPage1.Controls.Add(this.payrollPeriodSelector);
            this.tabPage1.Controls.Add(this.buttonEvalue);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.employeePlaceholder);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(665, 454);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Test";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // payrollPeriodSelector
            // 
            this.payrollPeriodSelector.Location = new System.Drawing.Point(131, 32);
            this.payrollPeriodSelector.Name = "payrollPeriodSelector";
            this.payrollPeriodSelector.NullSelection = null;
            this.payrollPeriodSelector.SelectedPeriod = null;
            this.payrollPeriodSelector.Size = new System.Drawing.Size(279, 24);
            this.payrollPeriodSelector.TabIndex = 10;
            // 
            // buttonEvalue
            // 
            this.buttonEvalue.Location = new System.Drawing.Point(464, 4);
            this.buttonEvalue.Name = "buttonEvalue";
            this.buttonEvalue.Size = new System.Drawing.Size(69, 23);
            this.buttonEvalue.TabIndex = 9;
            this.buttonEvalue.Text = "Evaluate";
            this.buttonEvalue.UseVisualStyleBackColor = true;
            this.buttonEvalue.Click += new System.EventHandler(this.buttonEvalue_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Period:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Test Employee:";
            // 
            // employeePlaceholder
            // 
            this.employeePlaceholder.Category = -1;
            this.employeePlaceholder.employee = null;
            this.employeePlaceholder.Location = new System.Drawing.Point(131, 6);
            this.employeePlaceholder.Name = "employeePlaceholder";
            this.employeePlaceholder.Size = new System.Drawing.Size(310, 20);
            this.employeePlaceholder.TabIndex = 8;
            // 
            // listVariables
            // 
            this.listVariables.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listVariables.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listVariables.Location = new System.Drawing.Point(10, 87);
            this.listVariables.Name = "listVariables";
            this.listVariables.Size = new System.Drawing.Size(647, 168);
            this.listVariables.TabIndex = 11;
            this.listVariables.UseCompatibleStateImageBehavior = false;
            this.listVariables.View = System.Windows.Forms.View.Details;
            this.listVariables.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // listTransactions
            // 
            this.listTransactions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listTransactions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5});
            this.listTransactions.Location = new System.Drawing.Point(10, 277);
            this.listTransactions.Name = "listTransactions";
            this.listTransactions.Size = new System.Drawing.Size(647, 159);
            this.listTransactions.TabIndex = 11;
            this.listTransactions.UseCompatibleStateImageBehavior = false;
            this.listTransactions.View = System.Windows.Forms.View.Details;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Variables:";
            this.label1.Click += new System.EventHandler(this.label8_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Transactions:";
            this.label3.Click += new System.EventHandler(this.label8_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Variable";
            this.columnHeader1.Width = 114;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Value";
            this.columnHeader2.Width = 375;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Type";
            this.columnHeader3.Width = 136;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Account";
            this.columnHeader4.Width = 231;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Amount";
            this.columnHeader5.Width = 367;
            // 
            // PayrollComponentEditor
            // 
            this.ClientSize = new System.Drawing.Size(673, 518);
            this.Controls.Add(this.tabPayroll);
            this.Controls.Add(this.pnlDialog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PayrollComponentEditor";
            this.Text = "Payroll Formula";
            this.tabPayroll.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.pageGeneral.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.pageCalc.ResumeLayout(false);
            this.spltFormula.Panel1.ResumeLayout(false);
            this.spltFormula.Panel2.ResumeLayout(false);
            this.spltFormula.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTransactions)).EndInit();
            this.pnlDialog.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPayroll;
        private System.Windows.Forms.TabPage pageGeneral;
        private System.Windows.Forms.TabPage pageCalc;
        private System.Windows.Forms.Label lblVariable;
        private System.Windows.Forms.SplitContainer spltFormula;
        private INTAPS.UI.EDataGridView dgVars;
        private INTAPS.UI.EDataGridView dgTransactions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMoreInfo;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel pnlDialog;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpDate;
        private System.Windows.Forms.Label lblEnrolled;
        private EmployeeDomainControl employeeDomain;
        private PayrollPeriodRangeSelector periodRange;
        private System.Windows.Forms.Label lblVarDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Variable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Formula;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVarDesc;
        private System.Windows.Forms.CheckBox chkDeduct;
        private System.Windows.Forms.TabPage tabPage1;
        private PayrollPeriodSelector payrollPeriodSelector;
        private System.Windows.Forms.Button buttonEvalue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private EmployeePlaceHolder employeePlaceholder;
        private System.Windows.Forms.ListView listTransactions;
        private System.Windows.Forms.ListView listVariables;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
    }
}
