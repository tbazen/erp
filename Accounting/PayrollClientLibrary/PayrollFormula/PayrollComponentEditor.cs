using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;

namespace INTAPS.Payroll.Client
{
    public partial class PayrollComponentEditor : Form
    {
        static Color ReadOnlyBackColor = System.Drawing.Color.FromArgb(224,224,224);
        public PayrollComponentFormula FormData;

        bool m_AllowEditVariableFormula;
        bool m_AllowEditAccountFormula;
        IPayrollComponentUIHandler m_pcdui;
        PayrollComponentDefination m_pcd;
        PayrollComponentFormulaSpec m_spec;
        bool Add;
        public PayrollComponentEditor()
        {
            InitializeComponent();
            payrollPeriodSelector.LoadData(PayrollClient.CurrentYear);

            dtpDate.Value = DateTime.Now;
            employeeDomain.LoadData();
            periodRange.LoadData(PayrollClient.CurrentYear);
        }
        void SetUpFormlaGrid(PayrollComponentFormulaBase f)
        {
            int i;
            dgVars.Rows.Clear();
            for (i = 0; i < f.Vars.Length; i++)
            {
                dgVars.Rows.Add(f.Vars[i], f.Vals[i]);
            }
            dgTransactions.Rows.Clear();
            for(i=0;i<f.AmountFormula.Length;i++)
            {
                dgTransactions.Rows.Add(f.AccountFormula[i], f.AmountFormula[i]);
            }
        }
        void SetPCD(int PCDID, PayrollComponentFormula f)
        {
            m_pcd= PayrollClient.GetPCDefination(PCDID);
            m_pcdui = PayrollClient.GetPCDUIHandler(PCDID);
            m_spec = PayrollClient.GetPCDefaultFormula(PCDID);
            
            if (f == null)
                SetUpFormlaGrid(m_spec);
            else
                SetUpFormlaGrid(f);
            if (m_spec.VarsReadonly)
            {
                dgVars.ReadOnly = true;
                dgVars.DefaultCellStyle.BackColor = ReadOnlyBackColor;
            }
            else
            {
                dgVars.Columns[2].DefaultCellStyle.BackColor = ReadOnlyBackColor;
                foreach (DataGridViewRow row in dgVars.Rows)
                {
                    if (row.IsNewRow)
                        continue;
                    string var = (string)row.Cells[0].Value;
                    if (m_spec.IsVarSystem(var))
                    {
                        row.ReadOnly = true;
                        row.DefaultCellStyle.BackColor = ReadOnlyBackColor;
                    }
                    else
                    {
                        row.Cells[0].ReadOnly = false;
                        row.Cells[1].ReadOnly = false;
                        row.Cells[2].ReadOnly = true;
                    }
                    int index = m_spec.IndexOf(var);
                    if (index != -1)
                        row.Cells[2].Value = m_spec.VarDescription[index];

                }
            }
            dgTransactions.Columns[0].ReadOnly = m_spec.AccountFormulaReadOnly;
            dgTransactions.Columns[1].ReadOnly = m_spec.AmountFormulaReadonly;
            if (m_spec.AccountFormulaReadOnly)
                dgTransactions.Columns[0].DefaultCellStyle.BackColor = ReadOnlyBackColor;
            if(m_spec.AmountFormulaReadonly)
                dgTransactions.Columns[1].DefaultCellStyle.BackColor = ReadOnlyBackColor;
        }
        public PayrollComponentEditor(int PCDID)
            : this()
        {
            SetPCD(PCDID,null);
            FormData = new PayrollComponentFormula();
            FormData.PCDID = PCDID;
            FormData.Name = m_pcd.Name + " formula";
            txtName.Text = FormData.Name;
            Add = true;
 
        }
        public PayrollComponentEditor(PayrollComponentFormula formula)
            : this()
        {
            SetPCD(formula.PCDID,formula);
            FormData = formula;
            txtName.Text = formula.Name;
            txtMoreInfo.Text = formula.Description == null ? "" : formula.Description;
            employeeDomain.SetData(formula.at);
            dtpDate.Value = formula.FormulaDate;
            periodRange.SetData(formula.periodRange);
            chkDeduct.Checked = formula.Deduct;
            Add = false;
        }



        private void dgTransactions_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

        }
        
        public bool AllowEditVariableFormula
        {
            get { return m_AllowEditVariableFormula; }
            set
            {
                if (m_AllowEditVariableFormula == value)
                    return;
                dgVars.ReadOnly = !value;
                m_AllowEditVariableFormula = value;
            }
        }

        public bool AllowEditAccountFormula
        {
            get { return m_AllowEditAccountFormula; }
            set
            {
                if (m_AllowEditAccountFormula == value)
                    return;
                dgTransactions.ReadOnly = !value;
                m_AllowEditAccountFormula = value;
            }
        }



        private void btnOk_Click(object sender, EventArgs e)
        {
            FormData.FormulaDate = dtpDate.Value;
            FormData.Name = txtName.Text.Trim();
            FormData.Description = txtMoreInfo.Text.Trim();
            if(FormData.Name=="")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter formula name.");
                txtName.SelectAll();
                txtName.Focus();
                return;
            }
            //Period
            if (!periodRange.IsDataValid())
                return;
            FormData.periodRange=periodRange.GetData();
            if (!employeeDomain.IsDataValid())
                return;
            FormData.at = employeeDomain.GetData();

            FormData.Vars = new string[dgVars.Rows.Count-(dgVars.AllowUserToAddRows?1:0)];
            FormData.Vals = new string[FormData.Vars.Length];
            int i = 0;
            foreach (DataGridViewRow vrow in dgVars.Rows)
            {
                if (vrow.IsNewRow)
                    continue;
                FormData.Vars[i] = (string)vrow.Cells[0].Value;
                FormData.Vals[i] = (string)vrow.Cells[1].Value;
                i++;
            }
            FormData.AccountFormula = new string[dgTransactions.Rows.Count
                -(dgTransactions.AllowUserToAddRows?1:0)];
            FormData.AmountFormula = new string[FormData.AccountFormula.Length];
            i = 0;
            foreach (DataGridViewRow trow in dgTransactions.Rows)
            {
                if (trow.IsNewRow)
                    continue;
                FormData.AccountFormula[i] = (string)trow.Cells[0].Value;
                FormData.AmountFormula[i] = (string)trow.Cells[1].Value;
                i++;
            }
            FormData.Deduct = chkDeduct.Checked;
            try
            {
                if (Add)
                {
                    FormData.ID = PayrollClient.CreatePCNewFormula(FormData);
                    Add = false;
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    PayrollClient.UpdatePCFormula(FormData);
                    this.DialogResult = DialogResult.OK;
                }
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Formula saved successfully.");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save formula.", ex);
            }
        }

        private void dgVars_SelectionChanged(object sender, EventArgs e)
        {
            if (dgVars.SelectedRows.Count != 0)
            {
                DataGridViewRow row = dgVars.SelectedRows[0];
                string var = (string)row.Cells[0].Value;
                int index = m_spec.IndexOf(var);
                if (index == -1)
                    lblVarDesc.Text = var;
                else
                    lblVarDesc.Text = (string)row.Cells[2].Value;
                    
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonEvalue_Click(object sender, EventArgs e)
        {
            listVariables.Items.Clear();
            listTransactions.Items.Clear();
            try
            {
                string[] vars;
                INTAPS.Evaluator.EData [] vals;
                TransactionOfBatch[] tran = PayrollClient.testComponent(DateTime.Now, m_pcd.ID, FormData.ID, employeePlaceholder.GetEmployeeID(), payrollPeriodSelector.SelectedPeriod.id, out vars, out vals);
                for (int i = 0; i < vars.Length; i++)
                {
                    ListViewItem li = new ListViewItem(vars[i]);
                    li.SubItems.Add(vals[i].ToString());
                    li.SubItems.Add(vals[i].EDataType.Name);
                    listVariables.Items.Add(li);
                }
                foreach (TransactionOfBatch t in tran)
                {
                    Account ac = INTAPS.Accounting.Client.AccountingClient.GetAccount<Account>(t.AccountID);
                    ListViewItem li=new ListViewItem(ac==null?"[Invalid Account ID:"+t.AccountID+"]":ac.NameCode);
                    li.SubItems.Add(t.Amount.ToString());
                    listTransactions.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to evaluate formula", ex);
            }

        }
    }
}

