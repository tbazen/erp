namespace INTAPS.Payroll.Client
{
    partial class SetPCDDomain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetPCDDomain));
            this.periodRange = new INTAPS.Payroll.Client.PayrollPeriodRangeSelector();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.employeeDomain = new INTAPS.Payroll.Client.EmployeeDomainControl();
            this.pnlDialog = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.pnlDialog.SuspendLayout();
            this.SuspendLayout();
            // 
            // periodRange
            // 
            this.periodRange.Location = new System.Drawing.Point(12, 27);
            this.periodRange.Name = "periodRange";
            this.periodRange.Size = new System.Drawing.Size(315, 87);
            this.periodRange.TabIndex = 27;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.employeeDomain);
            this.groupBox1.Location = new System.Drawing.Point(12, 120);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 291);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Formula Applies To";
            // 
            // employeeDomain
            // 
            this.employeeDomain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.employeeDomain.Location = new System.Drawing.Point(3, 16);
            this.employeeDomain.Name = "employeeDomain";
            this.employeeDomain.Size = new System.Drawing.Size(309, 272);
            this.employeeDomain.TabIndex = 0;
            // 
            // pnlDialog
            // 
            this.pnlDialog.Controls.Add(this.btnOk);
            this.pnlDialog.Controls.Add(this.btnCancel);
            this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDialog.Location = new System.Drawing.Point(0, 432);
            this.pnlDialog.Name = "pnlDialog";
            this.pnlDialog.Size = new System.Drawing.Size(339, 40);
            this.pnlDialog.TabIndex = 28;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(191, 9);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "&Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(262, 9);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // SetPCDDomain
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(339, 472);
            this.Controls.Add(this.pnlDialog);
            this.Controls.Add(this.periodRange);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetPCDDomain";
            this.Text = "SetPCDDomain";
            this.groupBox1.ResumeLayout(false);
            this.pnlDialog.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PayrollPeriodRangeSelector periodRange;
        private System.Windows.Forms.GroupBox groupBox1;
        private EmployeeDomainControl employeeDomain;
        private System.Windows.Forms.Panel pnlDialog;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}