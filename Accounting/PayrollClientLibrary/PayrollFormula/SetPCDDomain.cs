using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class SetPCDDomain : Form
    {
        public PayrollComponentDefination FormData;
        public SetPCDDomain()
        {
            InitializeComponent();
            periodRange.LoadData(PayrollClient.CurrentYear);
            employeeDomain.LoadData();
        }
        public SetPCDDomain(PayrollComponentDefination def):this()
        {
            FormData=def;
            periodRange.SetData(def.periodRange);
            employeeDomain.SetData(FormData.at);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if(!periodRange.IsDataValid())
                    return;
                if(!employeeDomain.IsDataValid())
                    return;
                FormData.periodRange =periodRange.GetData();
                FormData.at=employeeDomain.GetData();
                PayrollClient.SetPCDDomain(FormData.ID, FormData.periodRange, FormData.at);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't save data.", ex);
            }

        }
    }
}