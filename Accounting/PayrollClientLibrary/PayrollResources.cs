using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace INTAPS.Payroll.Client
{
    public class PayrollResources
    {
        public static Image OrgUnitImage
        {
            get
            {
                return Properties.Resources.OrgUnit;
            }
        }
        public static Image EmployeeImage
        {
            get
            {
                return Properties.Resources.Employee;
            }
        }
    }
}
