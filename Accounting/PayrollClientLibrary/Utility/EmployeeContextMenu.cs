using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
namespace INTAPS.Payroll.Client.Utility
{
    class EmployeeContextMenu:ContextMenuStrip
    {
        private System.Windows.Forms.ToolStripMenuItem miEditEmployee;
        private System.Windows.Forms.ToolStripMenuItem miEnrollEmployee;
        private System.Windows.Forms.ToolStripMenuItem miFireEmployee;
        private System.Windows.Forms.ToolStripMenuItem miDeleteEmployee;
        private System.Windows.Forms.ToolStripMenuItem miEGroupUnder;
        Employee m_emp = null;
        public EmployeeContextMenu()
        {
            this.miEditEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miEnrollEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miFireEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miDeleteEmployee = new System.Windows.Forms.ToolStripMenuItem();
            this.miEGroupUnder = new System.Windows.Forms.ToolStripMenuItem();


            // 
            // ctxtEmployee
            // 
            this.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEditEmployee,
            this.miEnrollEmployee,
            this.miFireEmployee,
            this.miDeleteEmployee,
            this.miEGroupUnder});
            // 
            // miEditEmployee
            // 
            this.miEditEmployee.Name = "miEditEmployee";
            this.miEditEmployee.Size = new System.Drawing.Size(289, 22);
            this.miEditEmployee.Text = "Edit Employee Profile";
            this.miEditEmployee.Click += new System.EventHandler(this.miEditEmployee_Click);
            // 
            // miEnrollEmployee
            // 
            this.miEnrollEmployee.Name = "miEnrollEmployee";
            this.miEnrollEmployee.Size = new System.Drawing.Size(289, 22);
            this.miEnrollEmployee.Text = "Enroll Employee";
            // 
            // miFireEmployee
            // 
            this.miFireEmployee.Name = "miFireEmployee";
            this.miFireEmployee.Size = new System.Drawing.Size(289, 22);
            this.miFireEmployee.Text = "Fire Employee";
            // 
            // miDeleteEmployee
            // 
            this.miDeleteEmployee.Name = "miDeleteEmployee";
            this.miDeleteEmployee.Size = new System.Drawing.Size(289, 22);
            this.miDeleteEmployee.Text = "Delete Employee";
            this.miDeleteEmployee.Click += new System.EventHandler(this.miDeleteEmployee_Click);
            // 
            // miEGroupUnder
            // 
            this.miEGroupUnder.Name = "miEGroupUnder";
            this.miEGroupUnder.Size = new System.Drawing.Size(289, 22);
            this.miEGroupUnder.Text = "Group Under Different Organizatinal Unit";
            this.miEGroupUnder.Click += new System.EventHandler(this.miEGroupUnder_Click);


        }
        DateTime _dataDate;
        public void SetEmployee(Employee emp,DateTime dataDate)
        {
            m_emp = emp;
            _dataDate = dataDate;
        }
        private void miDeleteEmployee_Click(object sender, EventArgs e)
        {
            if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this employee from database?"))
            {
                try
                {
                    PayrollClient.DeleteEmployee(m_emp.ObjectIDVal);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't delete employee from database.", ex);
                }
            }
        }
        private void miEditEmployee_Click(object sender, EventArgs e)
        {
            EmployeeEditor ep = new EmployeeEditor((Employee)m_emp.Clone());
            ep.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
        private void miEGroupUnder_Click(object sender, EventArgs e)
        {
            EmployeePicker picker = new EmployeePicker();
            if (picker.PickOrgUnitItem(false, true) == DialogResult.OK)
            {
                try
                {
                    PayrollClient.ChangeEmployeeOrgUnit(m_emp.ObjectIDVal, picker.PickedOrgUnit.ObjectIDVal,_dataDate);
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Operation not succesfull.", ex);
                }
            }
        }
        protected override void OnOpening(System.ComponentModel.CancelEventArgs e)
        {
            bool EmpSet = m_emp != null;
            miEditEmployee.Enabled = EmpSet;
            miEGroupUnder.Enabled = EmpSet;
            miDeleteEmployee.Enabled = EmpSet;
            miFireEmployee.Enabled = EmpSet &&  m_emp.status == EmployeeStatus.Enrolled;
            miEnrollEmployee.Enabled = EmpSet && m_emp.status != EmployeeStatus.Enrolled;
        }

    }
}
