namespace INTAPS.Payroll.Client
{
    partial class EmployeeDomainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.optSelectedEmployees = new System.Windows.Forms.RadioButton();
            this.optAllEmployees = new System.Windows.Forms.RadioButton();
            this.lvAppliesTo = new System.Windows.Forms.ListView();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.ilOrg = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // optSelectedEmployees
            // 
            this.optSelectedEmployees.AutoSize = true;
            this.optSelectedEmployees.Location = new System.Drawing.Point(3, 23);
            this.optSelectedEmployees.Name = "optSelectedEmployees";
            this.optSelectedEmployees.Size = new System.Drawing.Size(120, 17);
            this.optSelectedEmployees.TabIndex = 17;
            this.optSelectedEmployees.TabStop = true;
            this.optSelectedEmployees.Text = "Selected employees";
            this.optSelectedEmployees.UseVisualStyleBackColor = true;
            this.optSelectedEmployees.CheckedChanged += new System.EventHandler(this.optAllEmployees_CheckedChanged);
            // 
            // optAllEmployees
            // 
            this.optAllEmployees.AutoSize = true;
            this.optAllEmployees.Location = new System.Drawing.Point(3, 0);
            this.optAllEmployees.Name = "optAllEmployees";
            this.optAllEmployees.Size = new System.Drawing.Size(90, 17);
            this.optAllEmployees.TabIndex = 18;
            this.optAllEmployees.TabStop = true;
            this.optAllEmployees.Text = "All Employees";
            this.optAllEmployees.UseVisualStyleBackColor = true;
            this.optAllEmployees.CheckedChanged += new System.EventHandler(this.optAllEmployees_CheckedChanged);
            // 
            // lvAppliesTo
            // 
            this.lvAppliesTo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAppliesTo.Location = new System.Drawing.Point(3, 58);
            this.lvAppliesTo.Name = "lvAppliesTo";
            this.lvAppliesTo.Size = new System.Drawing.Size(324, 228);
            this.lvAppliesTo.SmallImageList = this.ilOrg;
            this.lvAppliesTo.TabIndex = 16;
            this.lvAppliesTo.UseCompatibleStateImageBehavior = false;
            this.lvAppliesTo.View = System.Windows.Forms.View.List;
            this.lvAppliesTo.SelectedIndexChanged += new System.EventHandler(this.lvAppliesTo_SelectedIndexChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Location = new System.Drawing.Point(239, 29);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(41, 23);
            this.btnRemove.TabIndex = 14;
            this.btnRemove.Text = "-";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddEmployee.Location = new System.Drawing.Point(286, 29);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(41, 23);
            this.btnAddEmployee.TabIndex = 15;
            this.btnAddEmployee.Text = "+";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // ilOrg
            // 
            this.ilOrg.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.ilOrg.ImageSize = new System.Drawing.Size(16, 16);
            this.ilOrg.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // EmployeeDomainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.optSelectedEmployees);
            this.Controls.Add(this.optAllEmployees);
            this.Controls.Add(this.lvAppliesTo);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAddEmployee);
            this.Name = "EmployeeDomainControl";
            this.Size = new System.Drawing.Size(330, 291);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton optSelectedEmployees;
        private System.Windows.Forms.RadioButton optAllEmployees;
        private System.Windows.Forms.ListView lvAppliesTo;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.ImageList ilOrg;
    }
}
