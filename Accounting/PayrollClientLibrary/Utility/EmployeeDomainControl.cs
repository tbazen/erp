using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public partial class EmployeeDomainControl : UserControl
    {
        public event EventHandler DomainChanged;
        EmployeePicker epicker = new EmployeePicker();

        public EmployeeDomainControl()
        {
            InitializeComponent();
            ilOrg.Images.Add(Properties.Resources.OrgUnit);
            ilOrg.Images.Add(Properties.Resources.Employee);
            optAllEmployees.Checked = true;
            optAllEmployees_CheckedChanged(null, null);
            lvAppliesTo_SelectedIndexChanged(null, null);
        }
        public void LoadData()
        {
            epicker.LoadData();
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            lvAppliesTo.SelectedItems[0].Remove();
            raiseChangedEvent();
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            if (epicker.ShowDialog() == DialogResult.OK)
            {
                optSelectedEmployees.Checked = true;
                if (epicker.PickedEmployee != null)
                {
                    AddAppliesToEmployee(epicker.PickedEmployee);
                    raiseChangedEvent();
                }
                else if (epicker.PickedOrgUnit != null)
                {
                    
                    AddAppliesToOrgUnit(epicker.PickedOrgUnit);
                    raiseChangedEvent();
                }
            }
        }

        private void raiseChangedEvent()
        {
            if (DomainChanged != null)
                DomainChanged(this, null);
        }

        private void optAllEmployees_CheckedChanged(object sender, EventArgs e)
        {
            lvAppliesTo.Enabled = !optAllEmployees.Checked;
            raiseChangedEvent();
        }
        private void AddAppliesToEmployee(Employee e)
        {
            ListViewItem li = lvAppliesTo.Items.Add(e.employeeName);
            li.Tag = e;
            li.ImageIndex = 1;
        }

        private void AddAppliesToOrgUnit(OrgUnit u)
        {
            ListViewItem li = lvAppliesTo.Items.Add(u.Name);
            li.Tag = u;
            li.ImageIndex = 0;
        }
        public void SetData(AppliesToEmployees at)
        {
            optAllEmployees.Checked = at.All;
            optSelectedEmployees.Checked = !at.All;
            if (!at.All)
            {
                foreach (int OID in at.OrgUnits)
                {
                    OrgUnit u = PayrollClient.GetOrgUnit(OID);
                    AddAppliesToOrgUnit(u);
                }
                foreach (int EID in at.Employees)
                {
                    Employee e = PayrollClient.GetEmployee(EID);
                    AddAppliesToEmployee(e);
                }
            }
        }
        public bool IsDataValid()
        {
            if (!optAllEmployees.Checked && lvAppliesTo.Items.Count == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please select at least on employee or organizational unit.");
                return false;
            }
            return true;
        }
        public AppliesToEmployees GetData()
        {
            AppliesToEmployees ret= new AppliesToEmployees();
            //Applies to
            if (optAllEmployees.Checked)
                ret.All = true;
            else
            {
                ret.All = false;
                List<int> Orgs = new List<int>();
                List<int> Emps = new List<int>();
                foreach (ListViewItem li in lvAppliesTo.Items)
                {
                    if (li.Tag is OrgUnit)
                    {
                        Orgs.Add(((OrgUnit)li.Tag).ObjectIDVal);
                    }
                    if (li.Tag is Employee)
                    {
                        Emps.Add(((Employee)li.Tag).ObjectIDVal);
                    }
                }
                ret.OrgUnits = Orgs.ToArray();
                ret.Employees = Emps.ToArray();
            }
            return ret;
        }

        private void lvAppliesTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = lvAppliesTo.SelectedItems.Count > 0;
        }

 
    }
}
