using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    public class EmployeePlaceHolder : TextBox
    {
        public event EventHandler EmployeeChanged;
        
        EmployeePicker m_picker;
        Employee m_picked;
        Button m_btnPick;
        bool m_changed;
        int m_category=-1;
        public int Category
        {
            get { return m_category; }
            set { m_category = value; }
        }
        public EmployeePlaceHolder()
        {
            m_btnPick = new Button();
            m_btnPick.Text = "...";
            m_btnPick.Dock = DockStyle.Right;
            m_btnPick.Width = 20;
            m_btnPick.Click += new EventHandler(m_btnPick_Click);
            m_changed = false;
            this.Controls.Add(m_btnPick);
        }
        public bool Changed
        {
            get
            {
                return m_changed;
            }
        }
        void m_btnPick_Click(object sender, EventArgs e)
        {
            if (m_picker == null)
                m_picker = new EmployeePicker();
            if (m_picker.PickOrgUnitItem(true,false) == DialogResult.OK)
            {
                this.employee = m_picker.PickedEmployee;
                OnEmployeeChanged();
            }
        }
        [Browsable(false)]
        public Employee employee
        {
            get
            {
                return m_picked;
            }
            set
            {
                m_picked = value;
                if (m_picked == null)
                    this.Text = "";
                else
                    this.Text = value.EmployeeNameID;
            }
        }
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            this.SelectAll();
        }
        protected void OnEmployeeChanged()
        {
            if (EmployeeChanged != null)
                EmployeeChanged(this, null);
            m_changed = true;
        }
        protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string txt = this.Text.Trim();
                if (txt == "")
                {
                    this.employee = null;
                    OnEmployeeChanged();
                }
                else
                {
                    try
                    {
                        int N;
                        Employee[] es = PayrollClient.SearchEmployee(DateTime.Now.Ticks, -1,txt,false, EmployeeSearchType.All,  0, 10, out N);
                        if (es.Length == 0)
                            throw new Exception("Employee not found.");
                        if (es.Length > 1)
                            throw new Exception("More than one employee found please try to be more specific.");
                        this.employee = es[0];
                        OnEmployeeChanged();
                    }
                    catch (Exception ex)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(ex.Message);
                        this.employee = m_picked;
                        this.SelectAll();
                    }
                }
                
            }
        }

        public void SetByID(int ID)
        {
            if (ID == -1)
                this.employee = null;
            else
            {
                try
                {
                    this.employee= PayrollClient.GetEmployee(ID);
                }
                catch
                {
                    m_picked = null;
                    this.Text = "error";
                }
            }
        }

        public int GetEmployeeID()
        {
            if (m_picked == null)
                return -1;
            return m_picked.ObjectIDVal;
        }
    }
    public class EmployeeDataGridViewCell : DataGridViewTextBoxCell
    {
        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
        {
            Employee employee = value as Employee;
            if (employee == null)
                return "";
            return employee.EmployeeNameID;
        }
    }
}
