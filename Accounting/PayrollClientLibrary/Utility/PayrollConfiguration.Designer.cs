namespace INTAPS.Payroll.Client
{
    partial class PayrollConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.acUnclaimed = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label2 = new System.Windows.Forms.Label();
            this.acStaffLoan = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label3 = new System.Windows.Forms.Label();
            this.acSalaryAdvance = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label4 = new System.Windows.Forms.Label();
            this.acExpensePerm = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label5 = new System.Windows.Forms.Label();
            this.acCashier = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.acLoanIncome = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label8 = new System.Windows.Forms.Label();
            this.txtServiceChargeRate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.acExpenseTemp = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label6 = new System.Windows.Forms.Label();
            this.textPeriodDays = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textPeriodHours = new System.Windows.Forms.TextBox();
            this.grpPeriod = new System.Windows.Forms.GroupBox();
            this.periodSelector = new INTAPS.Payroll.Client.PayrollPeriodSelector();
            this.label11 = new System.Windows.Forms.Label();
            this.acExpenseAdvance = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label12 = new System.Windows.Forms.Label();
            this.acShareHoldersLoan = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.label13 = new System.Windows.Forms.Label();
            this.acOwnersEquity = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.grpPeriod.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Unclaimed Salary";
            // 
            // acUnclaimed
            // 
            this.acUnclaimed.account = null;
            this.acUnclaimed.AllowAdd = false;
            this.acUnclaimed.Location = new System.Drawing.Point(114, 105);
            this.acUnclaimed.Name = "acUnclaimed";
            this.acUnclaimed.OnlyLeafAccount = false;
            this.acUnclaimed.Size = new System.Drawing.Size(284, 20);
            this.acUnclaimed.TabIndex = 1;
            this.acUnclaimed.Tag = "UnclaimedSalaryAccount";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Staff Loan";
            // 
            // acStaffLoan
            // 
            this.acStaffLoan.account = null;
            this.acStaffLoan.AllowAdd = false;
            this.acStaffLoan.Location = new System.Drawing.Point(114, 131);
            this.acStaffLoan.Name = "acStaffLoan";
            this.acStaffLoan.OnlyLeafAccount = false;
            this.acStaffLoan.Size = new System.Drawing.Size(284, 20);
            this.acStaffLoan.TabIndex = 2;
            this.acStaffLoan.Tag = "StaffLoanAccount";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Salary Advance";
            // 
            // acSalaryAdvance
            // 
            this.acSalaryAdvance.account = null;
            this.acSalaryAdvance.AllowAdd = false;
            this.acSalaryAdvance.Location = new System.Drawing.Point(114, 157);
            this.acSalaryAdvance.Name = "acSalaryAdvance";
            this.acSalaryAdvance.OnlyLeafAccount = false;
            this.acSalaryAdvance.Size = new System.Drawing.Size(284, 20);
            this.acSalaryAdvance.TabIndex = 3;
            this.acSalaryAdvance.Tag = "SalaryAdvanceAccount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Paryoll Expense for Permanent Staff";
            // 
            // acExpensePerm
            // 
            this.acExpensePerm.account = null;
            this.acExpensePerm.AllowAdd = false;
            this.acExpensePerm.Location = new System.Drawing.Point(114, 31);
            this.acExpensePerm.Name = "acExpensePerm";
            this.acExpensePerm.OnlyLeafAccount = false;
            this.acExpensePerm.Size = new System.Drawing.Size(284, 20);
            this.acExpensePerm.TabIndex = 0;
            this.acExpensePerm.Tag = "PermanentPayrollExpenseAccount";
            this.acExpensePerm.TextChanged += new System.EventHandler(this.acExpensePerm_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Paryoll Cashier";
            // 
            // acCashier
            // 
            this.acCashier.account = null;
            this.acCashier.AllowAdd = false;
            this.acCashier.Location = new System.Drawing.Point(114, 287);
            this.acCashier.Name = "acCashier";
            this.acCashier.OnlyLeafAccount = false;
            this.acCashier.Size = new System.Drawing.Size(284, 20);
            this.acCashier.TabIndex = 4;
            this.acCashier.Tag = "PayrollCashierAccountID";
            this.acCashier.TextChanged += new System.EventHandler(this.accountPlaceHolder5_TextChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(256, 491);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "Save";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(327, 491);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(207, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Staff loan service charge income account:";
            // 
            // acLoanIncome
            // 
            this.acLoanIncome.account = null;
            this.acLoanIncome.AllowAdd = false;
            this.acLoanIncome.Location = new System.Drawing.Point(114, 326);
            this.acLoanIncome.Name = "acLoanIncome";
            this.acLoanIncome.OnlyLeafAccount = false;
            this.acLoanIncome.Size = new System.Drawing.Size(284, 20);
            this.acLoanIncome.TabIndex = 5;
            this.acLoanIncome.Tag = "StaffLoanServiceChargeIncomeAccount";
            this.acLoanIncome.TextChanged += new System.EventHandler(this.accountPlaceHolder5_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 349);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Staff loan service charge(%):";
            // 
            // txtServiceChargeRate
            // 
            this.txtServiceChargeRate.Location = new System.Drawing.Point(119, 366);
            this.txtServiceChargeRate.Name = "txtServiceChargeRate";
            this.txtServiceChargeRate.Size = new System.Drawing.Size(279, 20);
            this.txtServiceChargeRate.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(175, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Paryoll Expense for Temporary Staff";
            // 
            // acExpenseTemp
            // 
            this.acExpenseTemp.account = null;
            this.acExpenseTemp.AllowAdd = false;
            this.acExpenseTemp.Location = new System.Drawing.Point(114, 70);
            this.acExpenseTemp.Name = "acExpenseTemp";
            this.acExpenseTemp.OnlyLeafAccount = false;
            this.acExpenseTemp.Size = new System.Drawing.Size(284, 20);
            this.acExpenseTemp.TabIndex = 0;
            this.acExpenseTemp.Tag = "TemporaryPayrollExpenseAccount";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Days";
            // 
            // textPeriodDays
            // 
            this.textPeriodDays.Location = new System.Drawing.Point(88, 49);
            this.textPeriodDays.Name = "textPeriodDays";
            this.textPeriodDays.Size = new System.Drawing.Size(153, 20);
            this.textPeriodDays.TabIndex = 12;
            this.textPeriodDays.TextChanged += new System.EventHandler(this.textPeriodDays_TextChanged);
            this.textPeriodDays.Validating += new System.ComponentModel.CancelEventHandler(this.textPeriodDays_Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Hours";
            // 
            // textPeriodHours
            // 
            this.textPeriodHours.Location = new System.Drawing.Point(88, 75);
            this.textPeriodHours.Name = "textPeriodHours";
            this.textPeriodHours.Size = new System.Drawing.Size(153, 20);
            this.textPeriodHours.TabIndex = 12;
            this.textPeriodHours.Validating += new System.ComponentModel.CancelEventHandler(this.textPeriodHours_Validating);
            // 
            // grpPeriod
            // 
            this.grpPeriod.Controls.Add(this.periodSelector);
            this.grpPeriod.Controls.Add(this.textPeriodHours);
            this.grpPeriod.Controls.Add(this.label6);
            this.grpPeriod.Controls.Add(this.label10);
            this.grpPeriod.Controls.Add(this.textPeriodDays);
            this.grpPeriod.Location = new System.Drawing.Point(11, 385);
            this.grpPeriod.Name = "grpPeriod";
            this.grpPeriod.Size = new System.Drawing.Size(376, 100);
            this.grpPeriod.TabIndex = 13;
            this.grpPeriod.TabStop = false;
            this.grpPeriod.Text = "Working days and hours";
            // 
            // periodSelector
            // 
            this.periodSelector.Location = new System.Drawing.Point(15, 19);
            this.periodSelector.Name = "periodSelector";
            this.periodSelector.NullSelection = null;
            this.periodSelector.SelectedPeriod = null;
            this.periodSelector.Size = new System.Drawing.Size(279, 24);
            this.periodSelector.TabIndex = 10;
            this.periodSelector.SelectionChanged += new System.EventHandler(this.periodSelector_SelectionChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Purchase Advance";
            // 
            // acExpenseAdvance
            // 
            this.acExpenseAdvance.account = null;
            this.acExpenseAdvance.AllowAdd = false;
            this.acExpenseAdvance.Location = new System.Drawing.Point(114, 183);
            this.acExpenseAdvance.Name = "acExpenseAdvance";
            this.acExpenseAdvance.OnlyLeafAccount = false;
            this.acExpenseAdvance.Size = new System.Drawing.Size(284, 20);
            this.acExpenseAdvance.TabIndex = 3;
            this.acExpenseAdvance.Tag = "StaffExpenseAdvanceAccount";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 212);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Share Holders Loan";
            // 
            // acShareHoldersLoan
            // 
            this.acShareHoldersLoan.account = null;
            this.acShareHoldersLoan.AllowAdd = false;
            this.acShareHoldersLoan.Location = new System.Drawing.Point(114, 209);
            this.acShareHoldersLoan.Name = "acShareHoldersLoan";
            this.acShareHoldersLoan.OnlyLeafAccount = false;
            this.acShareHoldersLoan.Size = new System.Drawing.Size(284, 20);
            this.acShareHoldersLoan.TabIndex = 3;
            this.acShareHoldersLoan.Tag = "ShareHolderLoanAccount";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 238);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Owner\'s Equity";
            // 
            // acOwnersEquity
            // 
            this.acOwnersEquity.account = null;
            this.acOwnersEquity.AllowAdd = false;
            this.acOwnersEquity.Location = new System.Drawing.Point(114, 235);
            this.acOwnersEquity.Name = "acOwnersEquity";
            this.acOwnersEquity.OnlyLeafAccount = false;
            this.acOwnersEquity.Size = new System.Drawing.Size(284, 20);
            this.acOwnersEquity.TabIndex = 3;
            this.acOwnersEquity.Tag = "OwnersEquityAccount";
            // 
            // PayrollConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(399, 530);
            this.Controls.Add(this.grpPeriod);
            this.Controls.Add(this.txtServiceChargeRate);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.acLoanIncome);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.acCashier);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.acOwnersEquity);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.acShareHoldersLoan);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.acExpenseAdvance);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.acSalaryAdvance);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.acStaffLoan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.acExpenseTemp);
            this.Controls.Add(this.acExpensePerm);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.acUnclaimed);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PayrollConfiguration";
            this.Text = "Payroll System Configuration";
            this.grpPeriod.ResumeLayout(false);
            this.grpPeriod.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private INTAPS.Accounting.Client.AccountPlaceHolder acUnclaimed;
        private System.Windows.Forms.Label label2;
        private INTAPS.Accounting.Client.AccountPlaceHolder acStaffLoan;
        private System.Windows.Forms.Label label3;
        private INTAPS.Accounting.Client.AccountPlaceHolder acSalaryAdvance;
        private System.Windows.Forms.Label label4;
        private INTAPS.Accounting.Client.AccountPlaceHolder acExpensePerm;
        private System.Windows.Forms.Label label5;
        private INTAPS.Accounting.Client.AccountPlaceHolder acCashier;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label7;
        private INTAPS.Accounting.Client.AccountPlaceHolder acLoanIncome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtServiceChargeRate;
        private System.Windows.Forms.Label label9;
        private INTAPS.Accounting.Client.AccountPlaceHolder acExpenseTemp;
        private PayrollPeriodSelector periodSelector;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textPeriodDays;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textPeriodHours;
        private System.Windows.Forms.GroupBox grpPeriod;
        private System.Windows.Forms.Label label11;
        private INTAPS.Accounting.Client.AccountPlaceHolder acExpenseAdvance;
        private System.Windows.Forms.Label label12;
        private INTAPS.Accounting.Client.AccountPlaceHolder acShareHoldersLoan;
        private System.Windows.Forms.Label label13;
        private INTAPS.Accounting.Client.AccountPlaceHolder acOwnersEquity;
    }
}