using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;

namespace INTAPS.Payroll.Client
{
    public partial class PayrollConfiguration : Form
    {
        Dictionary<int, PayrollPeriod> m_updated;
        public PayrollConfiguration()
        {
            InitializeComponent();
            List<string> parKeys = new List<string>();
            parKeys.Add("LoanSericeChargeRate");
            foreach (Control c in this.Controls)
            {
                if (c is AccountPlaceHolder)
                    parKeys.Add((string)c.Tag);
            }
            object[] pars = PayrollClient.GetSystemParameters(parKeys.ToArray());
            txtServiceChargeRate.Text = ((double)pars[0] * 100).ToString("0.00");
            
            int i = 1;
            foreach (Control c in this.Controls)
            {
                if (c is AccountPlaceHolder)
                {
                    ((AccountPlaceHolder)c).SetByID((int)pars[i]);
                    i++;
                }
            }
            
            m_updated = new Dictionary<int, PayrollPeriod>();
            periodSelector.LoadData(PayrollClient.CurrentYear);
            periodSelector.SelectedPeriod = MainForm.SelectedPeriod;
            UpdatePeriodControls();
        }
        void UpdatePeriodControls()
        {
            PayrollPeriod pp;
            if (m_updated.ContainsKey(periodSelector.SelectedPeriod.id))
                pp = m_updated[periodSelector.SelectedPeriod.id];
            else
                pp= periodSelector.SelectedPeriod;
            if (pp == null)
            {
                textPeriodDays.Text = "";
                textPeriodHours.Text = "";
            }
            else
            {
                textPeriodHours.Text = pp.hours.ToString("0.0");
                textPeriodDays.Text = pp.days.ToString("0.0");
            }
        }
        private void accountPlaceHolder5_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            double charge;
            if (!double.TryParse(txtServiceChargeRate.Text, out charge) || charge<0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter valid loan service charge rate.");
                return;
            }
            charge = charge / 100.0;
  

            try
            {
                List<string> names = new List<string>();
                List<object> values = new List<object>();
                names.AddRange(new string[]{"LoanSericeChargeRate"});
                values.AddRange(new object[]{charge});
                foreach (Control c in this.Controls)
                {
                    if (c is AccountPlaceHolder)
                    {
                        names.Add((string)c.Tag);
                        values.Add(((AccountPlaceHolder)c).GetAccountID());
                    }
                }
                PayrollClient.SetSystemParameters(names.ToArray(), values.ToArray());   
                foreach (PayrollPeriod pp in m_updated.Values)
                {
                    PayrollClient.UpdatePayPeriod(pp);

                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to save system parameters.", ex);
            }
        }

        private void periodSelector_SelectionChanged(object sender, EventArgs e)
        {
            UpdatePeriodControls();
        }

        private void textPeriodDays_TextChanged(object sender, EventArgs e)
        {
            
            
        }

        private void textPeriodDays_Validating(object sender, CancelEventArgs e)
        {
            PayrollPeriod pp;
            if (m_updated.ContainsKey(periodSelector.SelectedPeriod.id))
                pp = m_updated[periodSelector.SelectedPeriod.id];
            else
            {
                pp = periodSelector.SelectedPeriod;
                m_updated.Add(pp.id, pp);
            }
            if(!double.TryParse(textPeriodDays.Text,out pp.days)|| pp.days<0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter valid working days.");
                e.Cancel=true;
            }
        }

        private void textPeriodHours_Validating(object sender, CancelEventArgs e)
        {
            PayrollPeriod pp;
            if (m_updated.ContainsKey(periodSelector.SelectedPeriod.id))
                pp = m_updated[periodSelector.SelectedPeriod.id];
            else
            {
                pp = periodSelector.SelectedPeriod;
                m_updated.Add(pp.id, pp);
            }
            if (!double.TryParse(textPeriodHours.Text, out pp.hours) || pp.hours < 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter valid working hours.");
                e.Cancel = true;
            }

        }

        private void acExpensePerm_TextChanged(object sender, EventArgs e)
        {

        }
    }
}