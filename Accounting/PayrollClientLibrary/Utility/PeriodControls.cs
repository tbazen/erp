using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.Payroll.Client
{
    class PayrollPeriodRangeSelector : INTAPS.Accounting.Client.AccountingPeriodRangeSelector<PayrollPeriod, PayrollPeriodSelector>
    {
        protected override PayrollPeriod[] GetAccountingPeriods(int Year)
        {
            return PayrollClient.GetPayPeriods(Year, PayrollClient.Ethiopian);
        }

        protected override PayrollPeriod GetAccountingPeriod(int periodID)
        {
            return PayrollClient.GetPayPeriod(periodID);
        }
    }
    class PayrollPeriodSelector : INTAPS.Accounting.Client.AccountingPeriodSelector<PayrollPeriod>
    {
        protected override PayrollPeriod[] GetPayPeriods(int Year)
        {
            return PayrollClient.GetPayPeriods(Year, PayrollClient.Ethiopian);
        }

        protected override int GetAccountPeriodYear(int periodID)
        {
            PayrollPeriod prd = PayrollClient.GetPayPeriod(periodID);
            if (PayrollClient.Ethiopian)
                return INTAPS.Ethiopic.EtGrDate.ToEth(prd.fromDate).Year;
            return prd.fromDate.Year;
        }
    }
    class PayrollPeriodNavigator : INTAPS.Accounting.Client.AccountingPeriodNavigator<PayrollPeriod>
    {
        public PayrollPeriodNavigator(ToolStripButton prevYear, ToolStripButton prevMonth, ToolStripButton nextMonth, ToolStripButton nextYear, ToolStripLabel display,int year)
        :base(prevYear,prevMonth,nextMonth,nextYear,display,year)
        {
        }
        protected override PayrollPeriod[] GetPayPeriods(int Year)
        {
            return PayrollClient.GetPayPeriods(Year, PayrollClient.Ethiopian);
        }
    }
}
