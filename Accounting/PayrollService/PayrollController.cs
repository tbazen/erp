using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Payroll.Service;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Mvc;
using INTAPS.ClientServer;
using System.Linq;

namespace INTAPS.Payroll.Service
{
    [Route("api/erp/[controller]/[action]")]
    [ApiController]
    public class PayrollController : RESTServerControllerBase<PayrollService>
    {
        #region GetEmployeeByID
        public class GetEmployeeByIDPar
        {
            public string SessionID; public string EmpID;
        }

        [HttpPost]
        public ActionResult GetEmployeeByID(GetEmployeeByIDPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetEmployeeByID(par.EmpID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployee
        public class GetEmployeePar
        {
            public string SessionID; public int ID;
        }

        [HttpPost]
        public ActionResult GetEmployee(GetEmployeePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetEmployee(par.ID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployees
        public class GetEmployeesPar
        {
            public string SessionID; public int OrgID; public bool IncludeSubOrg;
        }

        [HttpPost]
        public ActionResult GetEmployees(GetEmployeesPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetEmployees(par.OrgID, par.IncludeSubOrg));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeesByRoll
        public class GetEmployeesByRollPar
        {
            public string SessionID; public int OrgID; public EmployeeRoll Role;
        }

        [HttpPost]
        public ActionResult GetEmployeesByRoll(GetEmployeesByRollPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetEmployeesByRoll(par.OrgID, par.Role));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetOrgUnit
        public class GetOrgUnitPar
        {
            public string SessionID; public int ID;
        }

        [HttpPost]
        public ActionResult GetOrgUnit(GetOrgUnitPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetOrgUnit(par.ID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetOrgUnits
        public class GetOrgUnitsPar
        {
            public string SessionID; public int PID;
        }

        [HttpPost]
        public ActionResult GetOrgUnits(GetOrgUnitsPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetOrgUnits(par.PID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchEmployee
        public class SearchEmployeePar
        {
            public string SessionID; public long ticks; public int orgUnitID; public string Query; public bool includeDismissed; public EmployeeSearchType SearchType; public int index; public int PageSize;
        }
        public class SearchEmployeeOut
        {
            public int NResult;
            public Employee[] _ret;
        }

        [HttpPost]
        public ActionResult SearchEmployee(SearchEmployeePar par)
        {
            try
            {
                var _ret = new SearchEmployeeOut();
                SetSessionObject(par.SessionID);
                _ret._ret = ((PayrollService)SessionObject).SearchEmployee(par.ticks, par.orgUnitID, par.Query, par.includeDismissed, par.SearchType, par.index, par.PageSize, out _ret.NResult);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region SaveEmployeeData
        public class SaveEmployeeDataPar
        {
            public string SessionID; public Employee e;
        }

        [HttpPost]
        public ActionResult SaveEmployeeData(SaveEmployeeDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).SaveEmployeeData(par.e));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateEmployeeData
        public class UpdateEmployeeDataPar
        {
            public string SessionID; public Employee e;
        }

        [HttpPost]
        public ActionResult UpdateEmployeeData(UpdateEmployeeDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).UpdateEmployeeData(par.e);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeEmployeeOrgUnit
        public class ChangeEmployeeOrgUnitPar
        {
            public string SessionID; public int ID; public int NewOrgUnitID; public DateTime date;
        }

        [HttpPost]
        public ActionResult ChangeEmployeeOrgUnit(ChangeEmployeeOrgUnitPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).ChangeEmployeeOrgUnit(par.ID, par.NewOrgUnitID, par.date);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeEmployeeSalary
        public class ChangeEmployeeSalaryPar
        {
            public string SessionID; public int ID; public double NewSalary;
        }

        [HttpPost]
        public ActionResult ChangeEmployeeSalary(ChangeEmployeeSalaryPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).ChangeEmployeeSalary(par.ID, par.NewSalary);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteEmployee
        public class DeleteEmployeePar
        {
            public string SessionID; public int ID;
        }

        [HttpPost]
        public ActionResult DeleteEmployee(DeleteEmployeePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).DeleteEmployee(par.ID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EnrollEmployee
        public class EnrollEmployeePar
        {
            public string SessionID; public DateTime date; public int ID;
        }

        [HttpPost]
        public ActionResult EnrollEmployee(EnrollEmployeePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).EnrollEmployee(par.date, par.ID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region FireEmployee
        public class FireEmployeePar
        {
            public string SessionID; public DateTime date; public int ID;
        }

        [HttpPost]
        public ActionResult FireEmployee(FireEmployeePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).FireEmployee(par.date, par.ID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveOrgUnit
        public class SaveOrgUnitPar
        {
            public string SessionID; public OrgUnit o;
        }

        [HttpPost]
        public ActionResult SaveOrgUnit(SaveOrgUnitPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).SaveOrgUnit(par.o));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateOrgUnit 
        public class UpdateOrgUnitpar
        {
            public string SessionID; public OrgUnit o;
        }

        [HttpPost]
        public ActionResult UpdateOrgUnit(UpdateOrgUnitpar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).UpdateOrgUnit(par.o);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteOrgUnit
        public class DeleteOrgUnitPar
        {
            public string SessionID; public int ID;
        }

        [HttpPost]
        public ActionResult DeleteOrgUnit(DeleteOrgUnitPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).DeleteOrgUnit(par.ID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayPeriods
        public class GetPayPeriodsPar
        {
            public string SessionID; public int Year; public bool Ethiopian;
        }

        [HttpPost]
        public ActionResult GetPayPeriods(GetPayPeriodsPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPayPeriods(par.Year, par.Ethiopian));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region CreatePCNewFormula
        public class CreatePCNewFormulaPar
        {
            public string SessionID; public PayrollComponentFormula formula;
        }

        [HttpPost]
        public ActionResult CreatePCNewFormula(CreatePCNewFormulaPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).CreatePCNewFormula(par.formula));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdatePCFormula
        public class UpdatePCFormulaPar
        {
            public string SessionID; public PayrollComponentFormula formula;
        }

        [HttpPost]
        public ActionResult UpdatePCFormula(UpdatePCFormulaPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).UpdatePCFormula(par.formula);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeletePCForumla
        public class DeletePCForumlaPar
        {
            public string SessionID; public int FormulaID;
        }

        [HttpPost]
        public ActionResult DeletePCForumla(DeletePCForumlaPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).DeletePCForumla(par.FormulaID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreatePCData
        public class CreatePCDataPar
        {
            public string SessionID; public PayrollComponentData data; public BinObject additionaldata;
        }

        [HttpPost]
        public ActionResult CreatePCData(CreatePCDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).CreatePCData(par.data, par.additionaldata.Deserialized()));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdatePCData
        public class UpdatePCDataPar
        {
            public string SessionID; public PayrollComponentData data; public BinObject additionaldata;
        }

        [HttpPost]
        public ActionResult UpdatePCData(UpdatePCDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).UpdatePCData(par.data, par.additionaldata.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteData
        public class DeleteDataPar
        {
            public string SessionID; public int DataID;
        }

        [HttpPost]
        public ActionResult DeleteData(DeleteDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).DeleteData(par.DataID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCFormulae
        public class GetPCFormulaePar
        {
            public string SessionID; public int PCDID;
        }

        [HttpPost]
        public ActionResult GetPCFormulae(GetPCFormulaePar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPCFormulae(par.PCDID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCDefinations
        public class GetPCDefinationsPar
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult GetPCDefinations(GetPCDefinationsPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPCDefinations());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCData
        public class GetPCDataPar
        {
            public string SessionID; public int EmployeeID;
        }

        [HttpPost]
        public ActionResult GetPCData(GetPCDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPCData(par.EmployeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCAdditionalData
        public class GetPCAdditionalDataPar
        {
            public string SessionID; public int DataID;
        }

        [HttpPost]
        public ActionResult GetPCAdditionalData(GetPCAdditionalDataPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                var data = ((PayrollService)SessionObject).GetPCAdditionalData(par.DataID);
                return Json(new TypeObject(data));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCDefaultFormula
        public class GetPCDefaultFormulaPar
        {
            public string SessionID; public int PCDID;
        }

        [HttpPost]
        public ActionResult GetPCDefaultFormula(GetPCDefaultFormulaPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPCDefaultFormula(par.PCDID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayPeriod
        public class GetPayPeriodPar
        {
            public string SessionID; public int PID;
        }

        [HttpPost]
        public ActionResult GetPayPeriod(GetPayPeriodPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPayPeriod(par.PID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetPCDDomain
        public class SetPCDDomainPar
        {
            public string SessionID; public int PCDID; public PayPeriodRange prange; public AppliesToEmployees at;
        }

        [HttpPost]
        public ActionResult SetPCDDomain(SetPCDDomainPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).SetPCDDomain(par.PCDID, par.prange, par.at);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCData2
        public class GetPCData2Par
        {
            public string SessionID; public int PCDID; public int FormulaID; public AppliesToObjectType objectType; public int ObjectID; public int Period; public bool IncludeHeirarchy;
        }

        [HttpPost]
        public ActionResult GetPCData2(GetPCData2Par par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetPCData(par.PCDID, par.FormulaID, par.objectType, par.ObjectID, par.Period, par.IncludeHeirarchy));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GeneratePayroll
        public class GeneratePayrollPal
        {
            public string SessionID; public DateTime date; public AppliesToEmployees at; public int PeriodID;
        }

        [HttpPost]
        public ActionResult GeneratePayroll(GeneratePayrollPal par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).GeneratePayroll(par.date, par.at, par.PeriodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLastError
        public class GetLastErrorPar
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult GetLastError(GetLastErrorPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).GetLastError());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetProgressMax
        public class GetProgressMaxPal
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult GetProgressMax(GetProgressMaxPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                return Json(((PayrollService)SessionObject).GetProgressMax());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetProgress
        public class GetProgressPal
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult GetProgress(GetProgressPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                return Json(((PayrollService)SessionObject).GetProgress());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsBusy
        public class IsBusyPal
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult IsBusy(IsBusyPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                return Json(((PayrollService)SessionObject).IsBusy());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region StopGenerating
        public class StopGeneratingPal
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult StopGenerating(StopGeneratingPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                ((PayrollService)SessionObject).StopGenerating();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayroll
        public class GetPayrollPal
        {
            public string SessionID; public int EmpID; public int PeriodID;
        }

        [HttpPost]
        public ActionResult GetPayroll(GetPayrollPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                return Json(((PayrollService)SessionObject).GetPayroll(pal.EmpID, pal.PeriodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayrolls
        public class GetPayrollsPal
        {
            public string SessionID; public AppliesToEmployees at; public int PeriodID;
        }

        [HttpPost]
        public ActionResult GetPayrolls(GetPayrollsPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                return Json(((PayrollService)SessionObject).GetPayrolls(pal.at, pal.PeriodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeletePayroll
        public class DeletePayrollPal
        {
            public string SessionID; public int EmployeeID; public int PeriodID;
        }

        [HttpPost]
        public ActionResult DeletePayroll(DeletePayrollPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                ((PayrollService)SessionObject).DeletePayroll(pal.EmployeeID, pal.PeriodID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetStaffLoanPayableAccount   
        public class GetStaffLoanPayableAccountPal
        {
            public string SessionID; public int EmployeeID;
        }

        [HttpPost]
        public ActionResult GetStaffLoanPayableAccount(GetStaffLoanPayableAccountPal pal)
        {
            try
            {
                SetSessionObject(pal.SessionID);
                return Json(((PayrollService)SessionObject).GetStaffLoanPayableAccount(pal.EmployeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region PostPayroll
        public class PostPayrollPar
        {
            public string SessionID; public int EmployeeID; public int PeriodID; public DateTime postDate;
        }

        [HttpPost]
        public ActionResult PostPayroll(PostPayrollPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).PostPayroll(par.EmployeeID, par.PeriodID, par.postDate);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region EmployeeUnder
        public class EmployeeUnderPar
        {
            public string SessionID; public int EID; public int OID;
        }

        [HttpPost]
        public ActionResult EmployeeUnder(EmployeeUnderPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(((PayrollService)SessionObject).EmployeeUnder(par.EID, par.OID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region SetSystemParameters
        public class SetSystemParametersPar
        {
            public string SessionID; public string[] fields; public BinObject vals;
        }

        [HttpPost]
        public ActionResult SetSystemParameters(SetSystemParametersPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).SetSystemParameters(par.fields, (object[])par.vals.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSystemParameters
        public class GetSystemParametersPar
        {
            public string SessionID; public string[] names;
        }

        [HttpPost]
        public ActionResult GetSystemParameters(GetSystemParametersPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                return Json(new BinObject(((PayrollService)SessionObject).GetSystemParameters(par.names)));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region UpdatePayPeriod
        public class UpdatePayPeriodPar
        {
            public string SessionID; public PayrollPeriod pp;
        }

        [HttpPost]
        public ActionResult UpdatePayPeriod(UpdatePayPeriodPar par)
        {
            try
            {
                SetSessionObject(par.SessionID);
                ((PayrollService)SessionObject).UpdatePayPeriod(par.pp);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region SetEmployeeImage 
        public class SetEmployeeImagePar
        {
            public string sessionID; public int employeeID; public byte[] image;
        }

        [HttpPost]
        public ActionResult SetEmployeeImage(SetEmployeeImagePar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((PayrollService)SessionObject).SetEmployeeImage(par.employeeID, par.image);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GeneratePayrollPreview
        public class GeneratePayrollPreviewPar
        {
            public string sessionID; public DateTime date; public AppliesToEmployees at; public int periodID;
        }
        public class GeneratePayrollPreviewOut
        {
            public BatchError error;
            public List<PayrollDocument> _ret;
        }


        [HttpPost]
        public ActionResult GeneratePayrollPreview(GeneratePayrollPreviewPar par)
        {
            try
            {
                var _ret = new GeneratePayrollPreviewOut();
                SetSessionObject(par.sessionID);
                _ret._ret = ((PayrollService)SessionObject).GeneratePayrollPreview(par.date, par.at, par.periodID, out _ret.error);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GeneratePayrollPeriods
        public class GeneratePayrollPeriodsPar
        {
            public string sessionID; public int year; public bool ethiopian;
        }

        [HttpPost]
        public ActionResult GeneratePayrollPeriods(GeneratePayrollPeriodsPar par)
        {
            try
            {
                SetSessionObject(par.sessionID);
                ((PayrollService)SessionObject).GeneratePayrollPeriods(par.year, par.ethiopian);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPCFormula
        public class GetPCFormulaPar
        {
            public string sessionID; public int FormulaID;
        }

        [HttpPost]
        public ActionResult GetPCFormula(GetPCFormulaPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetPCFormula(par.FormulaID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayPeriod2
        public class GetPayPeriod2Par
        {
            public string sessionID; public System.DateTime date;
        }

        [HttpPost]
        public ActionResult GetPayPeriod2(GetPayPeriod2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetPayPeriod(par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GeneratePayrollPreview2
        public class GeneratePayrollPreview2Par
        {
            public string sessionID; public System.DateTime date; public string title; public INTAPS.Payroll.AppliesToEmployees at; public int periodID;
        }
        public class GeneratePayrollPreview2Out
        {
            public BatchError error;
            public INTAPS.Payroll.PayrollSetDocument _ret;
        }

        [HttpPost]
        public ActionResult GeneratePayrollPreview2(GeneratePayrollPreview2Par par)
        {
            try
            {
                var _ret = new GeneratePayrollPreview2Out();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((PayrollService)base.SessionObject).GeneratePayrollPreview2(par.date, par.title, par.at, par.periodID, out _ret.error);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayrollSetsOfAPeriod
        public class GetPayrollSetsOfAPeriodPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getPayrollSetsOfAPeriod(GetPayrollSetsOfAPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).getPayrollSetsOfAPeriod(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaymetAmounts
        public class GetPaymetAmountsPar
        {
            public string sessionID; public int p; public int[] empID;
        }
        public class GetPaymetAmountsOut
        {
            public double[] payroll;
            public double[] _ret;
        }

        [HttpPost]
        public ActionResult getPaymetAmounts(GetPaymetAmountsPar par)
        {
            try
            {
                var _ret = new GetPaymetAmountsOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((PayrollService)base.SessionObject).getPaymetAmounts(par.p, par.empID, out _ret.payroll);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAppliesToString
        public class GetAppliesToStringPar
        {
            public string sessionID; public INTAPS.Payroll.AppliesToEmployees at;
        }

        [HttpPost]
        public ActionResult getAppliesToString(GetAppliesToStringPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).getAppliesToString(par.at));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayrollTableHTML
        public class GetPayrollTableHTMLPar
        {
            public string sessionID; public INTAPS.Payroll.PayrollSetDocument set; public int periodID; public int formatID;
        }

        [HttpPost]
        public ActionResult GetPayrollTableHTML(GetPayrollTableHTMLPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetPayrollTableHTML(par.set, par.periodID, par.formatID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region GetPayrollTableHTML2
        public class GetPayrollTableHTML2Par
        {
            public string sessionID; public int setDocumentID; public int periodID; public int formatID;
        }

        [HttpPost]
        public ActionResult GetPayrollTableHTML2(GetPayrollTableHTML2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetPayrollTableHTML(par.setDocumentID, par.periodID, par.formatID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region TestSheetFormat
        public class TestSheetFormatPar
        {
            public string sessionID; public INTAPS.Payroll.PayrollSheetFormat format; public int employeeID; public int periodID;
        }
        public class TestSheetFormatOut
        {
            public string[] varNames;
            public INTAPS.Evaluator.EData[] _ret;
        }

        [HttpPost]
        public ActionResult testSheetFormat(TestSheetFormatPar par)
        {
            try
            {
                var _ret = new TestSheetFormatOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((PayrollService)base.SessionObject).testSheetFormat(par.format, par.employeeID, par.periodID, out _ret.varNames);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region TestComponent
        public class TestComponentPar
        {
            public string sessionID; public System.DateTime date; public int PCDID; public int formulaID; public int employeeID; public int periodID;
        }
        public class TestComponentOut
        {
            public string[] vars; public INTAPS.Evaluator.EData[] vals;
            public INTAPS.Accounting.TransactionOfBatch[] _ret;
        }

        [HttpPost]
        public ActionResult testComponent(TestComponentPar par)
        {
            try
            {
                var _ret = new TestComponentOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((PayrollService)base.SessionObject).testComponent(par.date, par.PCDID, par.formulaID, par.employeeID, par.periodID, out _ret.vars, out _ret.vals);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeePayrollDocumentID
        public class GetEmployeePayrollDocumentIDPar
        {
            public string sessionID; public int employeeID; public int periodID;
        }

        [HttpPost]
        public ActionResult getEmployeePayrollDocumentID(GetEmployeePayrollDocumentIDPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).getEmployeePayrollDocumentID(par.employeeID, par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeeByLoginName
        public class GetEmployeeByLoginNamePar
        {
            public string sessionID; public string loginName;
        }

        [HttpPost]
        public ActionResult GetEmployeeByLoginName(GetEmployeeByLoginNamePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetEmployeeByLoginName(par.loginName));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetNextPeriod
        public class GetNextPeriodPar
        {
            public string sessionID; public int periodID;
        }

        [HttpPost]
        public ActionResult getNextPeriod(GetNextPeriodPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).getNextPeriod(par.periodID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPayrollSetByEmployee
        public class GetPayrollSetByEmployeePar
        {
            public string sessionID; public int periodID; public int employeeID;
        }

        [HttpPost]
        public ActionResult getPayrollSetByEmployee(GetPayrollSetByEmployeePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).getPayrollSetByEmployee(par.periodID, par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeVersions
        public class GetEmployeVersionsPar
        {
            public string sessionID; public int employeeID;
        }

        [HttpPost]
        public ActionResult getEmployeVersions(GetEmployeVersionsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).getEmployeVersions(par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployee
        public class GetEmployee2Par
        {
            public string sessionID; public int ID; public long ticks;
        }

        [HttpPost]
        public ActionResult GetEmployee2(GetEmployee2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetEmployee(par.ID, par.ticks));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsEmployeeUsedInPayroll
        public class IsEmployeeUsedInPayrollPar
        {
            public string sessionID; public int employeeID; public long ver;
        }

        [HttpPost]
        public ActionResult isEmployeeUsedInPayroll(IsEmployeeUsedInPayrollPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).isEmployeeUsedInPayroll(par.employeeID, par.ver));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteEmployeeVersion
        public class DeleteEmployeeVersionPar
        {
            public string sessionID; public int ID; public long ticks;
        }

        [HttpPost]
        public ActionResult deleteEmployeeVersion(DeleteEmployeeVersionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((PayrollService)base.SessionObject).deleteEmployeeVersion(par.ID, par.ticks);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetParentAcccountsByEmployee
        public class GetParentAcccountsByEmployeePar
        {
            public string sessionID; public int employeeID;
        }

        [HttpPost]
        public ActionResult GetParentAcccountsByEmployee(GetParentAcccountsByEmployeePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetParentAcccountsByEmployee(par.employeeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetEmployeeSubAccount
        public class GetEmployeeSubAccountPar
        {
            public string sessionID; public int employeeID; public int accountID;
        }

        [HttpPost]
        public ActionResult GetEmployeeSubAccount(GetEmployeeSubAccountPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((PayrollService)base.SessionObject).GetEmployeeSubAccount(par.employeeID, par.accountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}
