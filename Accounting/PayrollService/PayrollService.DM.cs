using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using INTAPS.Accounting;
using System.Threading;
namespace INTAPS.Payroll.Service
{
    public partial class PayrollService 
    {
        [ServiceMethod]
        public int SaveEmployeeData(Employee e)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("SaveEmployeeData Attempt", e.employeeID, e, -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int ret = bdePayroll.RegisterEmployee(AID,CostCenter.ROOT_COST_CENTER,e);
                    base.WriteAddAudit("SaveEmployeeData Success", ret.ToString(), AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SaveEmployeeData Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }


        }
        private void CheckPayrollEmployeeDataPermission()
        {
            if (!m_Session.Permissions.IsPermited("root/Payroll/EmployeeData"))
                throw new AccessDeniedException("You are not authroized to edit employee data system");
        }
        private void CheckPayrollFormulaPermission()
        {
            if (!m_Session.Permissions.IsPermited("root/Payroll/Formula"))
                throw new AccessDeniedException("You are not authroized to change payroll formula");
        }
        private void checkPayrollAdminPermission()
        {
            if (!m_Session.Permissions.IsPermited("root/Payroll/admin"))
                throw new AccessDeniedException("You are not authroized to change payroll configurations");
        }
        [ServiceMethod]
        public void UpdateEmployeeData(Employee e)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdateEmployeeData Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.Update(AID, e, false);
                    base.WriteAddAudit("UpdateEmployeeData Success", "", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateEmployeeData Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        [ServiceMethod]
        public void ChangeEmployeeOrgUnit(int ID, int NewOrgUnitID, DateTime date)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("ChangeEmployeeOrgUnit Attempt",  -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.ChangeEmployeeOrgUnit(AID, ID,NewOrgUnitID,date);
                    base.WriteAddAudit("ChangeEmployeeOrgUnit Success", ID.ToString(), AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ChangeEmployeeOrgUnit Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        [ServiceMethod]
        public void ChangeEmployeeSalary(int ID, double NewSalary)
        {
            throw new ServerUserMessage("This method is deprciated");
        }

        [ServiceMethod]
        public void DeleteEmployee(int ID)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeleteEmployee Attempt",-1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.DeleteEmployee(AID,ID);
                    base.WriteExecuteAudit("DeleteEmployee Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteEmployee Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }

        }

        [ServiceMethod]
        public void EnrollEmployee(DateTime date, int ID)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("EnrollEmployee Attempt", ID.ToString(), -1, date, ID);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.Enroll(AID,date, ID);
                    base.WriteExecuteAudit("EnrollEmployee Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("EnrollEmployee Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        [ServiceMethod]
        public void FireEmployee(DateTime date, int ID)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteMethodCallAudit("FireEmployee Attempt", ID.ToString(), -1, date, ID);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.Fire(AID,date, ID);
                    base.WriteExecuteAudit("FireEmployee Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("FireEmployee Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        [ServiceMethod]
        public int SaveOrgUnit(OrgUnit o)
        {
            checkPayrollAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("SaveOrgUnit Attempt", o.Name, o, -1);
                try
                {
                    int ret = bdePayroll.RegisterOrgUnit(AID,o);
                    base.WriteAddAudit("SaveOrgUnit Success", ret.ToString(), AID);
                    return ret;
                }
                catch (Exception ex)
                {
                    base.WriteAudit("SaveOrgUnit Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        [ServiceMethod]
        public void UpdateOrgUnit(OrgUnit o)
        {
            checkPayrollAdminPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteObjectAudit("UpdateOrgUnit Attempt", o.id.ToString(), o, -1);
                try
                {
                    bdePayroll.Update(AID,o);
                    base.WriteExecuteAudit("UpdateOrgUnit Success", AID);
                }
                catch (Exception ex)
                {
                    base.WriteAudit("UpdateOrgUnit Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        [ServiceMethod]
        public void DeleteOrgUnit(int ID)
        {
            checkPayrollAdminPermission();
            OrgUnit o = bdePayroll.GetOrgUnit(ID);
            int AID = base.WriteObjectAudit("DeleteOrgUnit Attempt", o.id.ToString(), o, -1);
            try
            {
                bdePayroll.DeleteOrgUnit(ID);
                base.WriteExecuteAudit("DeleteOrgUnit Success", AID);
            }
            catch (Exception ex)
            {
                base.WriteAudit("DeleteOrgUnit Failure", ex.Message, ex.StackTrace, AID);
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw ex;
            }
            finally
            {
                bdeAccounting.CheckTransactionIntegrity();
            }
        }
        public int CreatePCNewFormula(PayrollComponentFormula formula)
        {
            CheckPayrollFormulaPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("CreatePCNewFormula Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int ret = bdePayroll.CreatePCNewFormula(AID,formula);
                    base.WriteExecuteAudit("CreatePCNewFormula Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreatePCNewFormula Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void UpdatePCFormula(PayrollComponentFormula formula)
        {
            CheckPayrollFormulaPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdatePCFormula Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.UpdatePCFormula(AID,formula);
                    base.WriteExecuteAudit("UpdatePCFormula Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdatePCFormula Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void DeletePCForumla(int FormulaID)
        {
            CheckPayrollFormulaPermission();
            PayrollComponentFormula f = bdePayroll.GetPCFormula(FormulaID);
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeletePCForumla Attempt",  -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.DeletePCForumla(AID,FormulaID);
                    base.WriteExecuteAudit("DeletePCForumla Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeletePCForumla Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public int CreatePCData(PayrollComponentData data, object additionaldata)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("CreatePCData Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    int ret = bdePayroll.CreatePCData(AID,data, additionaldata);
                    base.WriteExecuteAudit("CreatePCData Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreatePCData Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void UpdatePCData(PayrollComponentData data, object additionaldata)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdatePCData Attempt",-1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.UpdatePCData(AID,data, additionaldata);
                    base.WriteExecuteAudit("UpdatePCData Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdatePCData Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void DeleteData(int DataID)
        {
            CheckPayrollEmployeeDataPermission();
            PayrollComponentData data;
            object aditional;
            data = bdePayroll.GetPCData(DataID, out aditional);
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeleteData Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.DeleteData(AID,DataID);
                    base.WriteExecuteAudit("DeleteData Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteData Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void SetPCDDomain(int PCDID, PayPeriodRange prange, AppliesToEmployees at)
        {
            CheckPayrollEmployeeDataPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("SetPCDDomain Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    bdePayroll.SetPCDDomain(AID,PCDID, prange, at);
                    base.WriteExecuteAudit("SetPCDDomain Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SetPCDDomain Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void DeletePayroll(int EmployeeID, int PeriodID)
        {
            int AID = base.WriteAddAudit("Delete Payroll Attempt", EmployeeID + "_" + PeriodID,-1);
            lock (bdePayroll.WriterHelper)
            {
                bdePayroll.WriterHelper.BeginTransaction();
                try
                {
                    bdePayroll.DeletePayroll(AID, EmployeeID, PeriodID);
                    base.WriteAddAudit("Delete Payroll Success", EmployeeID + "_" + PeriodID, AID);
                    bdePayroll.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdePayroll.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Delete Payroll Failure", EmployeeID + "_" + PeriodID, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }

        
        public void PostPayroll(int EmployeeID, int PeriodID, DateTime postDate)
        {
            int AID = base.WriteAddAudit("Post Payroll Attempt", EmployeeID + "_" + PeriodID,-1);
            lock (bdePayroll.WriterHelper)
            {
                bdePayroll.WriterHelper.BeginTransaction();
                try
                {
                    bdePayroll.PostPayroll(AID,CostCenter.ROOT_COST_CENTER, EmployeeID, PeriodID, postDate);
                    base.WriteAddAudit("Post Payroll Success", EmployeeID + "_" + PeriodID, -1);
                    bdePayroll.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdePayroll.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Post Payroll Failure", EmployeeID + "_" + PeriodID, ex.Message + "\n" + ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    bdeAccounting.CheckTransactionIntegrity();
                }
            }
        }
        public void SetSystemParameters(string[] fields, object[] vals)
        {
            CheckPayrollFormulaPermission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("Set System Parameters Attempt", -1);
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.SetSystemParameters(AID, fields, vals);
                    base.WriteExecuteAudit("Set System Parameters Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Set System Parameters Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw ex;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }
        public void UpdatePayPeriod(PayrollPeriod pp)
        {
            try
            {
                bdePayroll.UpdatePayPeriod(pp);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw ex;
            }
            finally
            {
                bdeAccounting.CheckTransactionIntegrity();
            }
        }
        public void GeneratePayrollPeriods(int year, bool ethiopian)
        {
            _bde.GeneratePayrollPeriods(year, ethiopian);
        }
        
    }
}
