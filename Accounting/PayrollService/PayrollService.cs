using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using INTAPS.Accounting;
using System.Threading;
using INTAPS.Payroll.BDE;
namespace INTAPS.Payroll.Service
{
    public partial class PayrollService : SessionObjectBase<PayrollBDE>
    {
        Accounting.BDE.AccountingBDE bdeAccounting;
        BDE.PayrollBDE bdePayroll;
        public PayrollService(UserSessionData session)
            : base(session, (BDE.PayrollBDE)ApplicationServer.GetBDE("Payroll"))
        {
            bdePayroll = _bde;
            bdeAccounting = (Accounting.BDE.AccountingBDE)ApplicationServer.GetBDE("Accounting");
        }
        [ServiceMethod]
        public Employee GetEmployeeByID(string EmpID)
        {
            return bdePayroll.GetEmployeeByID(EmpID);
        }
        [ServiceMethod]
        public Employee GetEmployee(int ID)
        {
            return bdePayroll.GetEmployee(ID);
        }
        [ServiceMethod]
        public Employee[] GetEmployees(int OrgID, bool IncludeSubOrg)
        {
            return bdePayroll.GetEmployees(OrgID, IncludeSubOrg);
        }
        [ServiceMethod]
        public Employee[] GetEmployeesByRoll(int OrgID, EmployeeRoll Role)
        {
            return bdePayroll.GetEmployeesByRoll(OrgID, Role);
        }
        [ServiceMethod]
        public OrgUnit GetOrgUnit(int ID)
        {
            return bdePayroll.GetOrgUnit(ID);
        }
        [ServiceMethod]
        public OrgUnit[] GetOrgUnits(int PID)
        {
            return bdePayroll.GetOrgUnits(PID);
        }
        [ServiceMethod]
        public Employee[] SearchEmployee(long ticks,int orgUnitID, string Query, bool includeDismissed, EmployeeSearchType SearchType, int index, int PageSize, out int NResult)
        {
            return bdePayroll.SearchEmployee(ticks,orgUnitID, Query, includeDismissed, SearchType, index, PageSize, out NResult);
        }


        [ServiceMethod]
        public PayrollPeriod[] GetPayPeriods(int Year, bool Ethiopian)
        {
            return bdePayroll.GetPayPeriods(Year, Ethiopian);
        }
        public PayrollComponentFormula[] GetPCFormulae(int PCDID)
        {
            return bdePayroll.GetPCFormulae(PCDID);
        }
        public PayrollComponentDefination[] GetPCDefinations()
        {
            return bdePayroll.GetPCDefinations();
        }
        public PayrollComponentData[] GetPCData(int EmployeeID)
        {
            return bdePayroll.GetPCData(EmployeeID);
        }
        public object GetPCAdditionalData(int DataID)
        {
            return bdePayroll.GetPCAdditionalData(DataID);
        }
        public PayrollComponentFormulaSpec GetPCDefaultFormula(int PCDID)
        {
            return bdePayroll.GetPCDefaultFormula(PCDID);
        }

        public PayrollPeriod GetPayPeriod(int PID)
        {
            return bdePayroll.GetPayPeriod(PID);
        }
        public PayrollComponentData[] GetPCData(int PCDID, int FormulaID, AppliesToObjectType objectType, int ObjectID, int Period, bool IncludeHeirarchy)
        {
            return bdePayroll.GetPCData(PCDID, FormulaID, objectType, ObjectID, Period, IncludeHeirarchy);
        }
        public PayrollDocument GetPayroll(int EmpID, int PeriodID)
        {
            return bdePayroll.GetPayroll(EmpID, PeriodID);
        }
        public PayrollDocument[] GetPayrolls(AppliesToEmployees at, int PeriodID)
        {
            return bdePayroll.GetPayrolls(at, PeriodID);
        }


        public int GetStaffLoanPayableAccount(int EmployeeID)
        {
            return bdePayroll.GetStaffLoanPayableAccount(EmployeeID);
        }


        public bool EmployeeUnder(int EID, int OID)
        {
            Employee e = GetEmployee(EID);
            int PID = e.orgUnitID;
            while (PID != OID && PID != -1)
                PID = GetOrgUnit(PID).PID;
            return PID != -1;
        }

        public object[] GetSystemParameters(string[] fields)
        {
            return bdePayroll.GetSystemParameters(fields);
        }

        public void StopGenerating()
        {
            lock (bdePayroll)
            {
                bdePayroll.StopProgress = true;
            }
        }



        public void SetEmployeeImage(int employeeID, byte[] image)
        {
            bdePayroll.SetEmployeeImage(employeeID, DateTime.Now.Ticks, image);
        }

        public INTAPS.Payroll.PayrollComponentFormula GetPCFormula(int FormulaID)
        {
            return _bde.GetPCFormula(FormulaID);
        }

        public INTAPS.Payroll.PayrollPeriod GetPayPeriod(System.DateTime date)
        {
            return _bde.GetPayPeriod(date);
        }

        public INTAPS.Payroll.PayrollSetDocument GeneratePayrollPreview2(System.DateTime date, string title, INTAPS.Payroll.AppliesToEmployees at, int periodID, out INTAPS.Accounting.BatchError error)
        {
            return _bde.GeneratePayrollPreview2(date, title, at, periodID, out error);
        }

        public INTAPS.Payroll.PayrollSetDocument[] getPayrollSetsOfAPeriod(int periodID)
        {
            return _bde.getPayrollSetsOfAPeriod(periodID);
        }

        public double[] getPaymetAmounts(int p, int[] empID, out double[] payroll)
        {
            return _bde.getPaymetAmounts(p, empID, out payroll);
        }

        public string getAppliesToString(INTAPS.Payroll.AppliesToEmployees at)
        {
            return _bde.getAppliesToString(at);
        }

        public string GetPayrollTableHTML(INTAPS.Payroll.PayrollSetDocument set, int periodID, int formatID)
        {
            return _bde.GetPayrollTableHTML(set, periodID, formatID);
        }


        public string GetPayrollTableHTML(int setDocumentID, int periodID, int formatID)
        {
            return _bde.GetPayrollTableHTML(setDocumentID, periodID, formatID);
        }

        public INTAPS.Evaluator.EData[] testSheetFormat(INTAPS.Payroll.PayrollSheetFormat format, int employeeID, int periodID, out string[] varNames)
        {
            return _bde.testSheetFormat(format, employeeID, periodID, out varNames);
        }

        public INTAPS.Accounting.TransactionOfBatch[] testComponent(System.DateTime date, int PCDID, int formulaID, int employeeID, int periodID, out string[] vars, out INTAPS.Evaluator.EData[] vals)
        {
            return _bde.testComponent(date, PCDID, formulaID, employeeID, periodID, out vars, out vals);
        }

        public int getEmployeePayrollDocumentID(int employeeID, int periodID)
        {
            return _bde.getEmployeePayrollDocumentID(employeeID, periodID);
        }

        public INTAPS.Payroll.Employee GetEmployeeByLoginName(string loginName)
        {
            return _bde.GetEmployeeByLoginName(loginName);
        }

        public INTAPS.Payroll.PayrollPeriod getNextPeriod(int periodID)
        {
            return _bde.getNextPeriod(periodID);
        }

        public INTAPS.Payroll.PayrollSetDocument getPayrollSetByEmployee(int periodID, int employeeID)
        {
            return _bde.getPayrollSetByEmployee(periodID, employeeID);
        }

        public long[] getEmployeVersions(int employeeID)
        {
            return _bde.getEmployeVersions(employeeID);
        }

        public INTAPS.Payroll.Employee GetEmployee(int ID, long ticks)
        {
            return _bde.GetEmployee(ID, ticks);
        }

        public bool isEmployeeUsedInPayroll(int employeeID, long ver)
        {
            return _bde.isEmployeeUsedInPayroll(employeeID, ver);
        }

        public void deleteEmployeeVersion(int ID, long ticks)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteEmployeeVersion Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteEmployeeVersion(AID, ID, ticks);
                    base.WriteExecuteAudit("deleteEmployeeVersion Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteEmployeeVersion Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }


        public PayrollPeriod getPreviosPeriod(int periodID)
        {
            return _bde.getPreviosPeriod(periodID);
        }

        public void addEmployeeParentAccount(int employeeID, int accountID)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("addEmployeeParentAccount Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.addEmployeeParentAccount(AID, employeeID, accountID);
                    base.WriteExecuteAudit("addEmployeeParentAccount Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("addEmployeeParentAccount Failure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }

        }

        public AccountBase[] GetParentAcccountsByEmployee(int employeeID)
        {
            return bdePayroll.GetParentAcccountsByEmployee(employeeID);
        }

        internal int GetEmployeeSubAccount(int employeeID, int accountID)
        {
            return bdePayroll.GetEmployeeSubAccount(employeeID,accountID);
        }
    }
}
