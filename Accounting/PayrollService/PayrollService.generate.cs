using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using System.Data;
using INTAPS.Accounting;
using System.Threading;
namespace INTAPS.Payroll.Service
{
    public partial class PayrollService 
    {
        PayrollGeneratorThread m_payrollThread = null;
        public BatchError GetLastError()
        {
            if (m_payrollThread == null)
                return null;
            return m_payrollThread.error;
        }
        public int GetProgressMax()
        {
            lock (bdePayroll)
            {
                return bdePayroll.ProgressMax;
            }
        }
        public int GetProgress()
        {
            lock (bdePayroll)
            {
                return bdePayroll.Progress;
            }
        }
        public bool IsBusy()
        {
            lock (bdePayroll)
            {
                return bdePayroll.ProgressMax != -1;
            }
        }
        public void GeneratePayroll(DateTime date,AppliesToEmployees at, int PeriodID)
        {
            lock (bdePayroll)
            {
                bdePayroll.Progress = 0;
                bdePayroll.ProgressMax = 0;
                m_payrollThread = new PayrollGeneratorThread(bdePayroll, date, at, PeriodID);
                Thread t = new Thread(new ThreadStart(m_payrollThread.Run));
                t.Start();
            }
        }
        public List<PayrollDocument> GeneratePayrollPreview(DateTime date, AppliesToEmployees at, int periodID,out BatchError error)
        {
            return bdePayroll.GeneratePayrollPreview(date, at, periodID,out error);
        }
    }
    class PayrollGeneratorThread
    {
        DateTime m_date;
        AppliesToEmployees m_at;
        int m_periodID;
        public BatchError error;
        INTAPS.Payroll.BDE.PayrollBDE bdePayroll;
        public PayrollGeneratorThread(INTAPS.Payroll.BDE.PayrollBDE bde,DateTime date, AppliesToEmployees at, int PeriodID)
        {
            m_date = date;
            m_at = at;
            m_periodID = PeriodID;
            bdePayroll = bde;
        }
        public void Run()
        {
            error = bdePayroll.GeneratePayroll(m_date, m_at, m_periodID);
        }
    }
}
