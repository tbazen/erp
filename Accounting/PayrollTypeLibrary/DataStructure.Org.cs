using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.RDBMS;
namespace INTAPS.Payroll
{
    [Serializable]
    public class OrgUnit : ClonableBase,INTAPS.ClientServer.IIDObject
    {
        public string Name;
        public string Description;
        public int id;
        public int PID;
        
        //public int temporaryExpenseAccountPID;
        //public int temporaryExpenseSummaryAccountPID;
        //public int temporaryCostAccountPID;
        //public int temporaryCostSummaryAccountPID;

        //public int permanentExpenseAccountPID;
        //public int permanentExpenseSummaryAccountPID;
        //public int permanentCostAccountPID;
        //public int permanantCostSummaryAccountPID;
        public OrgUnit()
        {
            id = -1;
            PID = -1;
        }
        public override string ToString()
        {
            return "Name:" + (string.IsNullOrEmpty(Name) ? "" : Name)
            + "\nDescription:" + (string.IsNullOrEmpty(Description) ? "" : Description)
            + "\nID:" + id
            + "\nPID:" + PID;
        }

        #region IIDObject Members

        public int ObjectIDVal
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        #endregion
    }
    public enum EmployeeRoll
    {
        Manager,
        AssistantManager,
        Clerk,
        Expert,
        Consultant
    }
    public enum EmployeeStatus
    {
        Pending = 0,
        Enrolled = 1,
        Fired = 2
    }
    public enum EmploymentType
    {
        Permanent = 0,
        Temporary = 1
    }
    public enum Sex
    {
        Male,
        Female,
        None
    }
    public enum CostSharingStatus
    {
        EvidenceProvided,
        EvidenceNotProvided,
        NotApplicable
    }
    [Serializable]
    [SingleTableObject(orderBy="employeeID")]
    public class Employee : ClonableBase,INTAPS.ClientServer.IIDObject
    {
        [IDField]
        public int id=-1;
        [DataField]
        public EmployeeRoll roll;
        [DataField]
        public string employeeID;
        [DataField]
        public string employeeName;
        [DataField]
        public int orgUnitID;
        [DataField]
        public string BankAccountNo = "";
        [DataField]
        public double grossSalary;
        [DataField]
        public EmployeeStatus status;
        [DataField]
        public DateTime enrollmentDate;
        [DataField]
        public DateTime dismissDate;
        [DataField]
        public EmploymentType employmentType;
        [DataField]
        public bool shareHolder;
        public int[] accounts;
        [DataField]
        public int picture;

        [DataField]
        public DateTime birthDate; //Used to validate pension
        [DataField]
        public Sex sex;
        [DataField]
        public string telephone;
        [DataField]
        public string address;
        [DataField]
        public string TIN; //Tax Identification Number of the employee 

        [DataField]
        public double transportAllowance;
        [DataField]
        public double medicalBenefit;
        [DataField]
        public double otherBenefits; //value of other taxable benefits
        [DataField]
        public double totalCostSharingDebt = 0d; //Has zero default value meaning has no debt
        [DataField]
        public double costSharingPayableAmount;
        [DataField]
        public CostSharingStatus costSharingStatus;

        [DataField]
        public int costCenterID=-1;

        [DataField]
        public SalaryKind salaryKind;
        [DataField]
        public int  TaxCenter;
        [DataField]
        public bool accountAsCost=false;
        [DataField]
        public string loginName = "";
        [DataField]
        public string title = "";

        [IDField]
        public long ticksFrom = -1;
        [DataField]
        public long ticksTo = -1;

        [DataField]
        public int expenseParentAccountID=-1;
        public Employee()
        {
            id = -1;
            picture = -1;
            accounts = new int[0];
            TIN = "";
            address = "";
            telephone = "";
            sex = Sex.Male;
            birthDate = DateTime.Now;
        }
        public string EmployeeNameID
        {
            get
            {
                return employeeName + " - " + employeeID;
            }
        }
        public double age
        {
            get
            {
                int year;
                DateTime now = DateTime.Now;
                if(now.Month>birthDate.Month || (now.Month==birthDate.Month && now.Day>birthDate.Day))
                    year = now.Year - birthDate.Year;
                else
                    year = now.Year - birthDate.Year-1;
                return (double)year;
            }
        }
        

        public override string ToString()
        {
            return "ID:" + id
                + "\nStatus:" + status
                + "\nEmployeeID:" + (string.IsNullOrEmpty(employeeID) ? "" : employeeID)
                + "\nEmployeeName:" + (string.IsNullOrEmpty(employeeName) ? "" : employeeName)
                + "\nOrgUnitID:" + orgUnitID
                + "\nGrossSalary:" + grossSalary
                + "\nStatus:" + status
                + "\nEnrollDate:" + enrollmentDate
                + "\nFireDate:" + dismissDate
            ;
        }
        public override bool Equals(object obj)
        {
            if (obj is Employee)
            {
                return ((Employee)obj).id == this.id;
            }
            return false;
        }
        public override int GetHashCode()
        {
            return id;
        }
        public int ObjectIDVal
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public double TaxableTransportAllowance
        {
            get
            {
               // return SimplePayrollTaxRules.GetTaxableTransportAllowance(grossSalary, transportAllowance);
                //NOTE: Currently taxable transport allowance is being ignored for payroll tax rules
                return 0;
            }
        }

        public int calculateAge()
        {
            DateTime day1 = birthDate;
            DateTime day2 = DateTime.Today; 
            if (day1.Equals(day2))
                return 0;
            int sign=1;
            if (day1 > day2)
            {
                sign = -1;
                DateTime temp = day1;
                day1 = day2;
                day2 = temp;
            }
            if (new DateTime(day2.Year, day1.Month, day1.Day) > day2)
                return sign*(day2.Year - day1.Year - 1);
            return sign*(day2.Year - day1.Year);
        }


        public static string getStatusString(EmployeeStatus employeeStatus)
        {
            switch (employeeStatus)
            {
                case EmployeeStatus.Pending:
                    return "Pending";
                case EmployeeStatus.Enrolled:
                    return "Enrolled";
                case EmployeeStatus.Fired:
                    return "Dismissed";
                default:
                    return "";
            }
        }
    }   
    public enum EmployeeField
    {
        ID,
        Name,
        OrgUnit,
        GrossSalary,
        EmployeeID
    }
    public enum SalaryKind
    {
        Monthly = 0,
        Daily = 1
    }
    public class EmployeeComparer:IComparer<Employee>
    {
        EmployeeField m_field;
        public EmployeeComparer(EmployeeField f)
        {
            m_field = f;
        }
        #region IComparer<Employee> Members

        public int Compare(Employee x, Employee y)
        {
            if (x == null && y == null)
                return 0;
            if (x != null && y == null)
                return 1;
            if (x == null && y != null)
                return -1;
            if (x.id == y.id)
                return 0;
            switch (m_field)
            {
                case EmployeeField.ID:
                    return x.id.CompareTo(y.id);
                case EmployeeField.Name:
                    return x.employeeName.CompareTo(y.employeeName);
                case EmployeeField.EmployeeID:
                    return x.employeeID.CompareTo(y.employeeID);
                case EmployeeField.OrgUnit:
                    return x.orgUnitID.CompareTo(y.orgUnitID);
                case EmployeeField.GrossSalary:
                    return x.grossSalary.CompareTo(y.grossSalary);
            }
            return 1;
        }

        #endregion
    }
}
