using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
namespace INTAPS.Payroll
{
    public enum AppliesToObjectType
    {
        All = 1,
        OrgUnit = 2,
        Employee = 3
    }

    [Serializable]
    public struct AppliesToEmployees
    {
        public bool All;
        public int[] OrgUnits;
        public int[] Employees;
        public AppliesToEmployees(bool All)
        {
            this.All = All;
            OrgUnits = new int[0];
            Employees = new int[0];
        }
        public AppliesToEmployees(int ObjectID, bool Employee)
        {
            All = false;
            if (Employee)
            {
                OrgUnits = new int[] { };
                Employees = new int[] { ObjectID };
            }
            else
            {
                OrgUnits = new int[] { ObjectID };
                Employees = new int[0];
            }
        }

        public AppliesToEmployees(int Employee)
        {
            All = false;
            OrgUnits = new int[] { };
            Employees = new int[] { Employee };
        }
        public bool ASpecificEmployee
        {
            get
            {
                if (All)
                    return false;
                if (OrgUnits != null && OrgUnits.Length > 0)
                    return false;
                if (Employees == null)
                    return false;
                return Employees.Length == 1;
            }
        }
        public bool ASepcificOrgUnit
        {
            get
            {
                if (All)
                    return false;
                if (Employees != null && Employees.Length > 0)
                    return false;
                if (OrgUnits == null)
                    return false;
                return OrgUnits.Length == 1;
            }
        }
    }

    [Serializable]
    public class PayrollComponentDefination : DataWithAppliesTo
    {
        public int ID;
        public string Name;
        public string Description;
        public string DataHandlerCass;
        public string UIHandlerClass;
        public bool SupportFormula;
        public bool Deduct;
        public object GetDataHandlerInstance(Type t, object par)
        {
            Type dht = Type.GetType(DataHandlerCass);
            return dht.GetConstructor(new Type[] { t }).Invoke(new object[] { par });
        }

    }

    [Serializable]
    public class PayrollComponentFormulaBase
    {
        public string[] Vars;
        public string[] Vals;
        public string[] AccountFormula;
        public string[] AmountFormula;
    }

    [Serializable]
    public class PayrollComponentFormulaSpec : PayrollComponentFormulaBase
    {
        public bool VarsReadonly;
        public bool AccountFormulaReadOnly;
        public bool AmountFormulaReadonly;
        public string[] VarDescription;
        public bool[] SystemVar;
        public bool IsVarSystem(string var)
        {
            if (string.IsNullOrEmpty(var))
                return false;
            for (int i = 0; i < Vars.Length; i++)
                if (var.ToUpper() == Vars[i].ToUpper())
                {
                    return SystemVar[i];
                }
            return false;
        }
        public int IndexOf(string var)
        {
            if (string.IsNullOrEmpty(var))
                return -1;
            for (int i = 0; i < Vars.Length; i++)
                if (var.ToUpper() == Vars[i].ToUpper())
                    return i;
            return -1;
        }

    }

    

    [Serializable]
    public class PayrollComponentData : DataWithAppliesTo
    {
        public int ID=-1;
        public int FormulaID;
        public int PCDID;
        public DateTime DataDate;
    }

    [Serializable]
    public class PayrollComponentFormula : PayrollComponentFormulaBase
    {
        public int ID;
        public string Name;
        public string Description;
        public int PCDID;
        public DateTime FormulaDate;
        public AppliesToEmployees at;
        public PayPeriodRange periodRange;
        public bool Deduct;
        public string GetVarFormula(string var)
        {
            string uvar = var.ToUpper();
            int i = 0;
            foreach (string v in Vars)
            {
                if (v.ToUpper() == uvar)
                    return Vals[i];
                i++;
            }
            throw new Exception("Undefined variable " + var);
        }
    }

}
