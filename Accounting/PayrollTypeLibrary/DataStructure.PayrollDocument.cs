using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
using INTAPS.RDBMS;
namespace INTAPS.Payroll
{
    [XmlInclude(typeof(PayrollComponent))]
    [Serializable]
    [SingleTableObject("Payroll")]
    public class PayrollDocument : AccountDocument
    {
        [IDField]
        public int employeeID;
        [IDField]
        public int periodID;
        [DataField]
        public int accountDocumentID = -1;
        public Employee employee;
        public PayrollPeriod payPeriod;
        public PayrollComponent[] components;
        public int payDocumentID;
        public DateTime payDocumentDate;

        public double NetPay
        {
            get
            {
                if (components == null)
                    return 0;
                double ret = 0;
                foreach (PayrollComponent c in components)
                {
                    if (c == null)
                        continue;
                    ret += c.Deduct ? -c.Amount : c.Amount;
                }
                return ret;
            }
        }
        public override string BuildHTML()
        {
            
            var table = new BIZNET.iERP.bERPHtmlTable();
            var group = table.createBodyGroup();
            BIZNET.iERP.bERPHtmlTableCell cell;
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell("<b>Name: </b>" + this.employee.employeeName, 2));
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell("<b>ID: </b>" + this.employee.employeeID, 2));

            double np = this.NetPay;

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell("<b>Period: </b>" + this.payPeriod.name, 2));

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell("<b>Payroll Posed On: </b>" + this.DocumentDate, 2));


            var rowadd = new List<BIZNET.iERP.bERPHtmlTableRow>();
            var rowded = new List<BIZNET.iERP.bERPHtmlTableRow>();
            PayrollComponent[] pcs = (PayrollComponent[])components.Clone();
            Array.Sort(pcs, new PCComparerforHTML());
            foreach (PayrollComponent comp in pcs)
            {
                if (comp == null || Account.AmountEqual(comp.Amount, 0))
                {
                    /*HtmlTableRow row = HTMLBuilder.CreateHtmlRow(
                        HTMLBuilder.CreateTextCell("null value", 2));
                    rowded.Add(row);*/
                    continue;
                }
                else
                {

                    var row = new BIZNET.iERP.bERPHtmlTableRow();
                    row.cells.AddRange(new BIZNET.iERP.bERPHtmlTableCell[] {
                        new BIZNET.iERP.bERPHtmlTableCell("<i>" + System.Web.HttpUtility.HtmlEncode(comp.Name + ":") + "</i>")
                        , cell = new BIZNET.iERP.bERPHtmlTableCell(comp.Amount == 0 ? "" : comp.Amount.ToString("0,0.00"))
                    });
                    cell.styles.Add("text-alight:right");
                    if (comp.Deduct)
                        rowded.Add(row);
                    else
                        rowadd.Add(row);
                }
            }
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group, new BIZNET.iERP.bERPHtmlTableCell("<b>Additions</b>", 2));
            foreach (var radd in rowadd)
                group.rows.Add(radd);

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell("<b>Deductions</b>", 2));

            foreach (var rded in rowded)
                group.rows.Add(rded);

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group, new BIZNET.iERP.bERPHtmlTableCell("<b>Net Pay: </b>")
                , cell = new BIZNET.iERP.bERPHtmlTableCell((np == 0 ? "-" : np.ToString("0,0.00")))
            );
            cell.styles.Add("text-align:right");

            var sb = new StringBuilder();
            table.build(sb);
            return sb.ToString();
        }
        public bool Paid
        {
            get
            {
                return payDocumentID != -1;
            }
        }

        public override string GetDocumentHashCode()
        {
            return employee.id + "-" + payPeriod.id;
        }

        public PayrollComponent GetComponent(int pcidID, int fid)
        {
            PayrollComponent ret = null;
            foreach (PayrollComponent comp in this.components)
            {
                if (comp.PCDID == pcidID && comp.FormulaID == fid)
                {
                    ret = comp;
                    break;
                }
            }
            return ret;
        }

        internal double grossEarning()
        {
            double g = 0;
            foreach (PayrollComponent c in components)
            {
                if (c.Amount > 0 && !c.Deduct)
                    g += c.Amount;
            }
            return g;
        }

        internal PayrollDocument clone()
        {
            PayrollDocument ret = new PayrollDocument();
            ret.CoypFrom(this);
            ret.employeeID = this.employeeID;
            ret.periodID = this.periodID;
            ret.accountDocumentID = this.accountDocumentID;
            ret.employee = (Employee)this.employee.Clone();
            ret.payPeriod = new PayrollPeriod(this.payPeriod);
            ret.payDocumentID = this.payDocumentID;
            ret.payDocumentDate = this.payDocumentDate;
            ret.components = new PayrollComponent[this.components.Length];
            for (int i = 0; i < ret.components.Length; i++)
                ret.components[i] = this.components[i].clone();
            return ret;
        }
    }
}
