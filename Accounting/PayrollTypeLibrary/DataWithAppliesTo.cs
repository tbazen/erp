using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
namespace INTAPS.Payroll
{
    [Serializable]
    public class DataWithAppliesTo : ClonableBase
    {
        public AppliesToEmployees at;
        public PayPeriodRange periodRange;
    }
}
