using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;


namespace INTAPS.Payroll.Depricated
{
    [Serializable]
    [XmlInclude(typeof(Employee))]
    [XmlInclude(typeof(PayrollPeriod))]
    [XmlInclude(typeof(AccountDocument))]
    public class StaffLoan : Accounting.AccountDocument
    {
        public int loanID;
        public int employeeID;
        public Employee employee;
        public double amount;
        public int periodID;
        public PayrollPeriod payStart;
        public double regularPayment;
        public bool closed;
        public DateTime closedDate;
        public bool longTerm;

        public StaffLoan()
        {
            loanID = -1;
            employeeID = -1;
            employee = null;
            periodID = -1;
            payStart = null;
            closed = false;
        }
        public bool EmployeeObjectSet
        {
            get
            {
                return (employeeID == -1 && employee == null) || (employeeID != -1 && employee != null);
            }
        }
        public bool PeriodObjectSet
        {
            get
            {
                return (periodID == -1 && payStart == null) || (periodID != -1 && payStart != null);
            }
        }
        public override string BuildHTML()
        {
            var table = new BIZNET.iERP.bERPHtmlTable();
            var group = table.createBodyGroup();
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
            "<b>Date: </b>"
            + System.Web.HttpUtility.HtmlEncode(
            INTAPS.Ethiopic.EtGrDate.ToEth(this.DocumentDate).ToString()
            )));


            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
            "<b>Paper Ref: </b>"
            + System.Web.HttpUtility.HtmlEncode(
            this.PaperRef == null ? "" : this.PaperRef
            )));

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
"<b>Employee name: </b>"
+ System.Web.HttpUtility.HtmlEncode(
this.employee.employeeName
)));

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
"<b>Employee ID: </b>"
+ System.Web.HttpUtility.HtmlEncode(
this.employee.employeeID
)));

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
"<b>Loan amount: </b>"
+ System.Web.HttpUtility.HtmlEncode(
this.amount.ToString("0,0.00")
)));

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
"<b>Regular monthly payment: </b>"
+ System.Web.HttpUtility.HtmlEncode(
this.regularPayment.ToString("0,0.00")
)));
            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
            "<b>Payment starts on payroll of: </b>"
            + System.Web.HttpUtility.HtmlEncode(
            this.payStart.ToString()
            )));

            BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
            , new BIZNET.iERP.bERPHtmlTableCell(
            "<b>Status: </b>"
            + System.Web.HttpUtility.HtmlEncode(
            this.closed ? "Closed on " + INTAPS.Ethiopic.EtGrDate.ToEth(this.closedDate) : (Posted ? "Posted on " + INTAPS.Ethiopic.EtGrDate.ToEth(this.DocumentDate) : " Draft")
            )));
            if (!string.IsNullOrEmpty(this.LongDescription))
            {
                BIZNET.iERP.bERPHtmlBuilder.htmlAddRow(group
                        , new BIZNET.iERP.bERPHtmlTableCell(
                        System.Web.HttpUtility.HtmlEncode(
                        this.LongDescription
                        )));
            }
            var sb = new StringBuilder();
            table.build(sb);
            return sb.ToString();
        }
    }
}
