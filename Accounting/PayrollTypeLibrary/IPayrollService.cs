using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace INTAPS.Payroll
{



    public partial interface IPayrollService
    {
        //Query
        Employee GetEmployeeByID(string SessionID, string EmpID);
        Employee GetEmployee(string SessionID, int ID);
        Employee[] GetEmployees(string SessionID, int OrgID, bool IncludeSubOrg);
        Employee[] GetEmployeesByRoll(string SessionID, int OrgID, EmployeeRoll Role);
        OrgUnit GetOrgUnit(string SessionID, int ID);
        OrgUnit[] GetOrgUnits(string SessionID, int PID);
        Employee[] SearchEmployee(string SessionID, long ticks, int orgUnitID, string Query, bool includeDismissed, EmployeeSearchType SearchType, int index, int PageSize, out int NResult);

        //Mainpulate employee
        int SaveEmployeeData(string SessionID, Employee e);
        void UpdateEmployeeData(string SessionID, Employee e);
        void ChangeEmployeeOrgUnit(string SessionID, int ID, int NewOrgUnitID, DateTime date);
        void ChangeEmployeeSalary(string SessionID, int ID, double NewSalary);
        void DeleteEmployee(string SessionID, int ID);
        void EnrollEmployee(string SessionID, DateTime date, int ID);
        void FireEmployee(string SessionID, DateTime date, int ID);

        //Manipulate organization
        int SaveOrgUnit(string SessionID, OrgUnit o);
        void UpdateOrgUnit(string SessionID, OrgUnit o);
        void DeleteOrgUnit(string SessionID, int ID);

        //Pay periods
        PayrollPeriod[] GetPayPeriods(string SessionID, int Year, bool Ethiopian);
        void UpdatePayPeriod(string SessionID, PayrollPeriod pp);

        //Payment component defination
        int CreatePCNewFormula(string SessionID, PayrollComponentFormula formula);
        void UpdatePCFormula(string SessionID, PayrollComponentFormula formula);
        void DeletePCForumla(string SessionID, int FormulaID);
        int CreatePCData(string SessionID, PayrollComponentData data, object additionaldata);
        void UpdatePCData(string SessionID, PayrollComponentData data, object additionaldata);
        void DeleteData(string SessionID, int DataID);
        PayrollComponentFormula[] GetPCFormulae(string SessionID, int PCDID);
        PayrollComponentDefination[] GetPCDefinations(string SessionID);
        PayrollComponentData[] GetPCData(string SessionID, int EmployeeID);
        object GetPCAdditionalData(string SessionID, int DataID);
        PayrollComponentFormulaSpec GetPCDefaultFormula(string SessionID, int PCDID);
        PayrollPeriod GetPayPeriod(string SessionID, int PID);
        void SetPCDDomain(string SessionID, int PCDID, PayPeriodRange prange, AppliesToEmployees at);
        PayrollComponentData[] GetPCData(string SessionID, int PCDID, int FormulaID, AppliesToObjectType objectType, int ObjectID, int Period, bool IncludeHeirarchy);

        //generate
        int GetProgressMax(string SessionID);
        int GetProgress(string SessionID);
        bool IsBusy(string SessionID);
        BatchError GetLastError(string SessionID);
        void StopGenerating(string SessionID);
        void GeneratePayroll(string SessionID, DateTime date, AppliesToEmployees at, int PeriodID);
        PayrollDocument GetPayroll(string SessionID, int EmpID, int PeriodID);
        PayrollDocument[] GetPayrolls(string SessionID, AppliesToEmployees at, int PeriodID);
        void DeletePayroll(string SessionID, int EmployeeID, int PeriodID);
        void PostPayroll(string SessionID, int EmployeeID, int PeriodID, DateTime postDate);
        bool EmployeeUnder(string SessionID, int EID, int OID);
        void SetSystemParameters(string SessionID, string[] fields, object[] vals);
        object[] GetSystemParameters(string SessionID, string[] fields);
        void SetEmployeeImage(string sessionID, int employeeID, byte[] image);
        List<PayrollDocument> GeneratePayrollPreview(string sessionID, DateTime date, AppliesToEmployees at, int periodID, out BatchError error);
        int GetStaffLoanPayableAccount(string SessionID, int EmployeeID);
        void GeneratePayrollPeriods(string sessionID, int year, bool ethiopian);

        INTAPS.Payroll.PayrollComponentFormula GetPCFormula(string sessionID, int FormulaID);

        INTAPS.Payroll.PayrollPeriod GetPayPeriod(string sessionID, System.DateTime date);

        INTAPS.Payroll.PayrollSetDocument GeneratePayrollPreview2(string sessionID, System.DateTime date, string title, INTAPS.Payroll.AppliesToEmployees at, int periodID, out INTAPS.Accounting.BatchError error);

        INTAPS.Payroll.PayrollSetDocument[] getPayrollSetsOfAPeriod(string sessionID, int periodID);

        double[] getPaymetAmounts(string sessionID, int p, int[] empID, out double[] payroll);

        string getAppliesToString(string sessionID, INTAPS.Payroll.AppliesToEmployees at);

        string GetPayrollTableHTML(string sessionID, INTAPS.Payroll.PayrollSetDocument set, int periodID, int formatID);


        string GetPayrollTableHTML(string sessionID, int setDocumentID, int periodID, int formatID);

        INTAPS.Evaluator.EData[] testSheetFormat(string sessionID, INTAPS.Payroll.PayrollSheetFormat format, int employeeID, int periodID, out string[] varNames);

        INTAPS.Accounting.TransactionOfBatch[] testComponent(string sessionID, System.DateTime date, int PCDID, int formulaID, int employeeID, int periodID, out string[] vars, out INTAPS.Evaluator.EData[] vals);

        int getEmployeePayrollDocumentID(string sessionID, int employeeID, int periodID);

        INTAPS.Payroll.Employee GetEmployeeByLoginName(string sessionID, string loginName);

        INTAPS.Payroll.PayrollPeriod getNextPeriod(string sessionID, int periodID);

        INTAPS.Payroll.PayrollSetDocument getPayrollSetByEmployee(string sessionID, int periodID, int employeeID);

        long[] getEmployeVersions(string sessionID, int employeeID);

        INTAPS.Payroll.Employee GetEmployee(string sessionID, int ID, long ticks);

        bool isEmployeeUsedInPayroll(string sessionID, int employeeID, long ver);

        void deleteEmployeeVersion(string sessionID, int ID, long ticks);

        AccountBase[] GetParentAcccountsByEmployee(string sessionID, int employeeID);
        int GetEmployeeSubAccount(string sessionID, int employeeID, int accountID);
    }
}
