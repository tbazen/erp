using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;

namespace INTAPS.Payroll
{
    class PCComparerforHTML : IComparer<PayrollComponent>
    {

        #region IComparer<PayrollComponent> Members

        public int Compare(PayrollComponent x, PayrollComponent y)
        {
            if (x == null || y == null)
            {
                if (x == null && y == null)
                    return 0;
                if (x == null)
                    return -1;
                return 1;
            }
            return x.PCDID.CompareTo(y.PCDID);
        }

        #endregion
    }
}
