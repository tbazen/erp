using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
using INTAPS.RDBMS;

namespace INTAPS.Payroll
{
    [Serializable]
    public class OverTimeData
    {
        public double offHours;
        public double lateHours;
        public double weekendHours;
        public double holidayHours;
        public bool isZero()
        {
            return AccountBase.AmountEqual(offHours, 0) && AccountBase.AmountEqual(lateHours, 0) && AccountBase.AmountEqual(weekendHours,0)
                && AccountBase.AmountEqual(holidayHours,0)
                ;
        }
    }
    [Serializable]
    [SingleTableObject]
    public class Data_RegularTime
    {
        [DataField]
        public double WorkingDays = 0; //Number of days worked in a month
        [DataField]
        public double WorkingHours = 0; //Number of hours worked in a month
        [DataField]
        public double PartialWorkingHour = 0;

        public bool isZero()
        {
            return AccountBase.AmountEqual(WorkingDays, 0) && AccountBase.AmountEqual(WorkingHours, 0) && AccountBase.AmountEqual(PartialWorkingHour,0);
        }
    }
    [Serializable]
    public class StaffLoanPaymentData
    {
        public double Amount;
        public bool Exception;
    }
    [Serializable]
    public class RegularDeductionData
    {
        public double Amount;
    }
    [Serializable]
    public class SingleValueData
    {
        public double value;
    }
}

