using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
using INTAPS.RDBMS;
namespace INTAPS.Payroll
{
    [XmlInclude(typeof(AccountTransaction))]
    [Serializable]
    [SingleTableObject]
    public class PayrollComponent
    {
        [XmlIgnore]
        [IDField]
        public int employeeID;
        [XmlIgnore]
        [IDField]
        public int periodID;
        [IDField]
        public int PCDID;
        [IDField]
        public int FormulaID;
        [DataField]
        public string Name;
        [DataField]
        public string Description;
        [DataField]
        public double Amount;
        [DataField]
        public bool Deduct;
        [XmlIgnore]
        public int[] dataID=new int[0];
        public TransactionOfBatch[] Transactions;
        public PayrollComponent()
        {
            this.Name = null;
            this.Description = null;
            this.Amount = 0;
            this.Deduct = false;
            Transactions = new TransactionOfBatch[0];
        }
        public void UnionTransaction(TransactionOfBatch[] newTrnsactions)
        {
            List<int> AccountID = new List<int>();
            List<TransactionOfBatch> Trans = new List<TransactionOfBatch>();
            Trans.AddRange(Transactions);
            foreach (TransactionOfBatch t in Transactions)
            {
                AccountID.Add(t.AccountID);
            }
            foreach (TransactionOfBatch t in newTrnsactions)
            {
                int index = AccountID.IndexOf(t.AccountID);

                if (index == -1)
                {
                    AccountID.Add(t.AccountID);
                    Trans.Add(t);
                }
                else
                {
                    Trans[index].Amount += t.Amount;
                }
            }
            this.Transactions = Trans.ToArray();
        }
        public PayrollComponent clone()
        {
            PayrollComponent ret = (PayrollComponent)this.MemberwiseClone();
            ret.dataID = new int[dataID.Length];
            Array.Copy(ret.dataID, dataID, dataID.Length);
            ret.Transactions = new TransactionOfBatch[Transactions.Length];
            for (int i = 0; i < ret.Transactions.Length; i++)
            {
                ret.Transactions[i] = this.Transactions[i].clone();
            }
            return ret;
        }
    }
}
