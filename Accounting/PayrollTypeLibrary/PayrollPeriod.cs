using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
namespace INTAPS.Payroll
{
    [Serializable]
    public class PayrollPeriod : AccountingPeriod
    {
        public PayrollPeriod()
        {
        }

        public PayrollPeriod(AccountingPeriod payrollPeriod)
        {
            if (payrollPeriod == null)
                return;
            this.id = payrollPeriod.id;
            this.name = payrollPeriod.name;
            this.fromDate = payrollPeriod.fromDate;
            this.toDate = payrollPeriod.toDate;
            this.days = payrollPeriod.days;
            this.hours = payrollPeriod.hours;
        }
    }
}
