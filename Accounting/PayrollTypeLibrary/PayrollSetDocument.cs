﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.RDBMS;
using INTAPS.Evaluator;
using System.Xml.Serialization;

namespace INTAPS.Payroll
{
    [Serializable]
    [SingleTableObject]
    public class PayrollPayment
    {
        [IDField]
        public int employeeID;
        [IDField]
        public int periodID;
        [DataField]
        public double amount;
        [IDField]
        public int accountDocumentID;
    }
    [Serializable]
    public partial class PayrollSetDocument : AccountDocument
    {

        public AppliesToEmployees at;
        public PayrollPeriod payPeriod;
        public PayrollDocument[] payrolls;
        public PayrollSetDocument()
        {
        }
        public PayrollSetDocument(PayrollDocument[] docs, string title)
        {
            this.payrolls = docs;
            this.ShortDescription = title;
        }

        public double grossEarning()
        {
            double g = 0;
            foreach (PayrollDocument doc in payrolls)
            {
                g += doc.grossEarning();
            }
            return g;
        }

        public double netPayment()
        {
            double g = 0;
            foreach (PayrollDocument doc in payrolls)
            {
                g += doc.NetPay;
            }
            return g;
        }
        public Employee[] Employees
        {
            get
            {
                List<Employee> employees = new List<Employee>();
                foreach (PayrollDocument doc in payrolls)
                {
                    employees.Add(doc.employee);
                }
                return employees.ToArray();
            }
        }
        [XmlIgnore]
        public string title
        {
            get
            {
                return LongDescription;
            }
            set
            {
                LongDescription = value;
            }
        }

        public PayrollSetDocument clone()
        {
            PayrollSetDocument ret = new PayrollSetDocument();
            ret.CoypFrom(this);
            ret.at = at;
            ret.payPeriod = new PayrollPeriod(this.payPeriod);
            ret.payrolls = new PayrollDocument[payrolls.Length];
            for (int i = 0; i < ret.payrolls.Length; i++)
            {
                ret.payrolls[i] = this.payrolls[i].clone();
            }
            return ret;
        }
    }
    
}
