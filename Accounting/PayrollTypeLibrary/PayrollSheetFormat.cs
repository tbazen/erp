using System;
using System.Collections.Generic;
using INTAPS.RDBMS;

namespace INTAPS.Payroll
{
    [Serializable]
    public class PayrollSheetColumn
    {
        public string header;
        public PayrollSheetColumn[] childs=new PayrollSheetColumn[0];
        public string formula;
        public string name;
        public bool visisble=true;
        public bool total = true;
        public string numberFormat = "#,#0.00";
        public string headerCss = "LedgerGridHeadCell";
        public string bodyCss = "LedgerGridOddRowCell,LedgerGridEvenRowCell";
        public string footerCss = "LedgerGridFooterCell";
        public int sortOrder = -1;
        public bool sortAscending = true;
    }
    [Serializable]
    [LoadFromSetting]
    public class PayrollSheetFormat
    {
        public PayrollSheetColumn[] columns=new PayrollSheetColumn[0];
        public string header;
        public string footer;
        public string name;
        public int breakNumberOfRows = -1;
        public override string ToString()
        {
            return name;
        }
        void getFormulaSet(PayrollSheetColumn[] cols, List<PayrollSheetColumn> listCols)
        {
            foreach (PayrollSheetColumn col in cols)
            {
                if (col.childs.Length == 0)
                {
                    listCols.Add(col);
                }
                else
                {
                    getFormulaSet(col.childs, listCols);
                }
            }
        }
        public PayrollSheetColumn[] getFormulaSet()
        {
            List<PayrollSheetColumn> listCols = new List<PayrollSheetColumn>();
            getFormulaSet(columns, listCols);
            return listCols.ToArray();
        }

        
    }
}
