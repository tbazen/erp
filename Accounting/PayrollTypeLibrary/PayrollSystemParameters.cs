using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using System.Xml.Serialization;
namespace INTAPS.Payroll
{
    [Serializable]
    public class PayrollSystemParameters
    {
        public int PermanentPayrollExpenseAccount;
        public int PermanentPayrollPensionContributionExpenseAccount;
        public int TemporaryPayrollExpenseAccount;
        public int PermanentPayrollCostAccount;
        public int PermanentPayrollPensionContributionCostAccount;
        public int TemporaryPayrollCostAccount;

        public int PayrollCashierAccountID;
        public int IncomeTaxLiabilityAccount;
        public int StaffLoanAccount;
        public int UnclaimedSalaryAccount;
        public int SalaryAdvanceAccount;
        public int StaffExpenseAdvanceAccount;
        public int ShareHolderLoanAccount;
        public int OwnersEquityAccount;
        public string EmployeeIDFormat;
        public double LoanSericeChargeRate;
        public int StaffLoanServiceChargeIncomeAccount;
        public PayrollSheetFormat PayrollColumnFormula;
        public PayrollSheetFormat PayrollColumnFormulaTwo;
        public PayrollSheetFormat PayrollColumnFormulaThree;
        public double EmployeePensionRate;
        public double EmployerPensionRate;
        public double EmployeeMonthlyFullWorkingHours;
        public double EmployeeDailyFullWorkingHours;

        public int staffExpenseAdvanceAccountID;
        public int staffSalaryAdvanceAccountID;
        public int staffLongTermLoanAccountID;
        public int staffPensionPayableAccountID;
        public int staffCostSharingPayableAccountID;
        public int staffIncomeTaxAccountID;
        public int staffLoanPayrollComponentID; //payroll component def
        public int staffShortTermLoanPayrollFormulaID;
        public int staffLongTermLoanPayrollFormulaID;
        public int shareHoldersLoanPayrollFormulaID;
        public int staffPerDiemAccountID;
        public int staffPerDiemCostAccountID;
        public int LoanFromStaffAccountID;
        public int ShareHoldersLoanAccountID;

        public int groupPayrollExpenseAccount;
        public int groupPayrollCostAccount;
        public int groupPayrollIncomeTaxAccount;
        public int groupPayrollPensionPayableAccount;
        public int groupPayrollUnclaimedSalaryAccount;
        public int groupPayrollOverTimeExpenseAccount;
        public int groupPayrollOverTimeCostAccount;

        public int mainCostCenterID;
        public string additionalImportFields=null;
        public string additionalImportFormulae = null;


        public PayrollSheetFormat getColumnFormula(int formatID)
        {
            List<PayrollSheetFormat> fs = new List<PayrollSheetFormat>();
            if (PayrollColumnFormula != null)
                fs.Add(PayrollColumnFormula);
            if (PayrollColumnFormulaTwo != null)
                fs.Add(PayrollColumnFormulaTwo);
            if (PayrollColumnFormulaThree != null)
                fs.Add(PayrollColumnFormulaThree);
            if (formatID < fs.Count)
                return fs[formatID];
            return null;
        }
    }
}
