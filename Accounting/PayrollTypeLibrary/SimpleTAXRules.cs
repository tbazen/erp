using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Payroll
{
    public static class SimplePayrollTaxRules
    {
        public static double GetTaxableTransportAllowance(double grossSalary, double transportAllowance)
        {
            const double rate = 0.25; //rate used to calculate taxable allowance
            const double taxFreeCieling = 1000;

            double taxFree = Math.Min(rate * grossSalary, taxFreeCieling);
            if(INTAPS.Accounting.Account.AmountLess(taxFree,transportAllowance))
                return transportAllowance-taxFree;
            return 0;
           
        }
        
    }
}
