namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer.Client;
    using INTAPS.Ethiopic;
    using System;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class AuditViewer : Form
    {
        
        private IContainer components = null;
        private int index;
        private const int PAGE_SIZE = 100;
        public AuditViewer()
        {
            this.InitializeComponent();
            string[] allUsers = SecurityClient.GetAllUsers();
            Array.Sort<string>(allUsers);
            this.lstUsers.Items.AddRange(allUsers);
            this.lstAction.Items.AddRange(SecurityClient.GetAllAuditOperations());
            this.index = 0;
        }

        private void AuditViewer_Load(object sender, EventArgs e)
        {
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            int[] dest = new int[this.lstAction.CheckedIndices.Count];
            this.lstAction.CheckedIndices.CopyTo(dest, 0);
            foreach (int num in dest)
            {
                this.lstAction.SetItemChecked(num, false);
            }
            dest = new int[this.lstUsers.CheckedIndices.Count];
            this.lstUsers.CheckedIndices.CopyTo(dest, 0);
            foreach (int num in dest)
            {
                this.lstUsers.SetItemChecked(num, false);
            }
            this.chkByDate.Checked = false;
            this.chkTo.Checked = false;
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            this.index = 0;
            this.LoadPage();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void LoadPage()
        {
            int num;
            string[] dest = new string[this.lstAction.CheckedItems.Count];
            this.lstAction.CheckedItems.CopyTo(dest, 0);
            string[] strArray2 = new string[this.lstUsers.CheckedItems.Count];
            this.lstUsers.CheckedItems.CopyTo(strArray2, 0);
            DataTable table = SecurityClient.GetAudit(dest, strArray2, this.chkByDate.Checked, this.dtpFrom.Value, this.chkTo.Checked, this.dtpTo.Value, null, this.index, 100, out num);
            this.tsbPrev.Enabled = this.index > 0;
            this.tsbNext.Enabled = (this.index + 100) < num;
            this.dgAudit.DataSource = table;
            this.dgAudit.Columns["DateTime"].CellTemplate = new EthiopianDateCell();
            this.lblPage.Text = string.Concat(new object[] { (this.index / 100) + 1, " of ", (num / 100) + 1, ", Total Number of Records:", num });
        }

        private void tsbNext_Click(object sender, EventArgs e)
        {
            this.index += 100;
            this.LoadPage();
        }

        private void tsbPrev_Click(object sender, EventArgs e)
        {
            this.index -= 100;
            this.LoadPage();
        }
    }
}

