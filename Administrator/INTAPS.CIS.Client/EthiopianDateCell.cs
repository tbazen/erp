namespace INTAPS.CIS.Client
{
    using INTAPS.Ethiopic;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    internal class EthiopianDateCell : DataGridViewTextBoxCell
    {
        protected override object GetFormattedValue(object value, int rowIndex, ref DataGridViewCellStyle cellStyle, TypeConverter valueTypeConverter, TypeConverter formattedValueTypeConverter, DataGridViewDataErrorContexts context)
        {
            if (!(value is DateTime))
            {
                return "";
            }
            DateTime dt = (DateTime) value;
            return (EtGrDate.ToEth(dt).ToString() + " " + dt.AddHours(-6.0).ToString("HH:mm"));
        }
    }
}

