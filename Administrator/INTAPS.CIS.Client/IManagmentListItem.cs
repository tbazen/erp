namespace INTAPS.CIS.Client
{
    using System;
    using System.Windows.Forms;

    public interface IManagmentListItem
    {
        void Open(ListViewItem item);
        void Select(ListViewItem item);
        void ShowContext(ListViewItem item);
    }
}

