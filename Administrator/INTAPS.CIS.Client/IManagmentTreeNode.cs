namespace INTAPS.CIS.Client
{
    using System;
    using System.Windows.Forms;

    public interface IManagmentTreeNode
    {
        void PopulateChilds(TreeNodeCollection nodes);
        void Select(TreeNode node, ListView lv);
        void ShowContext(TreeNode node);
    }
}

