namespace INTAPS.CIS.Client
{
    using System;

    public interface IManagmentWindow
    {
        void DeleteItem(IManagmentListItem item);
        void DeleteNode(IManagmentTreeNode node);
    }
}

