﻿namespace INTAPS.CIS.Client
{
    public partial class MainForm : System.Windows.Forms.Form, IManagmentWindow
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.spnlMain = new System.Windows.Forms.SplitContainer();
            this.tvTree = new System.Windows.Forms.TreeView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.auditViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lvList = new System.Windows.Forms.ListView();
            this.menLicense = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.spnlMain)).BeginInit();
            this.spnlMain.Panel1.SuspendLayout();
            this.spnlMain.Panel2.SuspendLayout();
            this.spnlMain.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // spnlMain
            // 
            this.spnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spnlMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spnlMain.Location = new System.Drawing.Point(0, 0);
            this.spnlMain.Name = "spnlMain";
            // 
            // spnlMain.Panel1
            // 
            this.spnlMain.Panel1.Controls.Add(this.tvTree);
            this.spnlMain.Panel1.Controls.Add(this.menuStrip1);
            // 
            // spnlMain.Panel2
            // 
            this.spnlMain.Panel2.Controls.Add(this.lvList);
            this.spnlMain.Size = new System.Drawing.Size(661, 493);
            this.spnlMain.SplitterDistance = 243;
            this.spnlMain.TabIndex = 0;
            // 
            // tvTree
            // 
            this.tvTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvTree.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvTree.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.tvTree.Location = new System.Drawing.Point(0, 24);
            this.tvTree.Name = "tvTree";
            this.tvTree.Size = new System.Drawing.Size(243, 469);
            this.tvTree.TabIndex = 0;
            this.tvTree.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.tvTree_AfterExpand);
            this.tvTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvTree_AfterSelect);
            this.tvTree.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvTree_KeyDown);
            this.tvTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvTree_MouseDown);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.auditViewerToolStripMenuItem,
            this.menLicense});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(243, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // auditViewerToolStripMenuItem
            // 
            this.auditViewerToolStripMenuItem.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.auditViewerToolStripMenuItem.Name = "auditViewerToolStripMenuItem";
            this.auditViewerToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.auditViewerToolStripMenuItem.Text = "Audit Viewer";
            this.auditViewerToolStripMenuItem.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // lvList
            // 
            this.lvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvList.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lvList.FullRowSelect = true;
            this.lvList.Location = new System.Drawing.Point(0, 0);
            this.lvList.Name = "lvList";
            this.lvList.Size = new System.Drawing.Size(414, 493);
            this.lvList.TabIndex = 0;
            this.lvList.UseCompatibleStateImageBehavior = false;
            this.lvList.View = System.Windows.Forms.View.Details;
            this.lvList.SelectedIndexChanged += new System.EventHandler(this.lvList_SelectedIndexChanged);
            this.lvList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvList_MouseDown);
            // 
            // menLicense
            // 
            this.menLicense.Name = "menLicense";
            this.menLicense.Size = new System.Drawing.Size(58, 20);
            this.menLicense.Text = "License";
            this.menLicense.Click += new System.EventHandler(this.menLicense_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 493);
            this.Controls.Add(this.spnlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Security Managment";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.spnlMain.Panel1.ResumeLayout(false);
            this.spnlMain.Panel1.PerformLayout();
            this.spnlMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spnlMain)).EndInit();
            this.spnlMain.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.SplitContainer spnlMain;
        public System.Windows.Forms.TreeView tvTree;
        private System.Windows.Forms.MenuStrip menuStrip1;
        public System.Windows.Forms.ListView lvList;
        private System.Windows.Forms.ToolStripMenuItem auditViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menLicense;

    }
}