namespace INTAPS.CIS.Client
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class MainForm : Form, IManagmentWindow
    {
        
        private IContainer components = null;
        
        private Hashtable m_ExpandedNodes;
        private Hashtable m_ManagmentNodes;
        
        
        public static MainForm TheInstance;
        

        public MainForm()
        {
            this.InitializeComponent();
            try
            {
                this.PopulateTree();
            }
            catch (Exception exception)
            {
                if (TheInstance == null)
                    MessageBox.Show(exception.Message);
                else
                    TheInstance.ShowError(exception);
            }
            TheInstance = this;
            this.m_ExpandedNodes = new Hashtable();
            this.m_ManagmentNodes = new Hashtable();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            INTAPS.ClientServer.Client.ApplicationClient.Disconnect();
            base.OnClosing(e);
        }
        public void DeleteItem(IManagmentListItem item)
        {
            foreach (ListViewItem item2 in this.lvList.Items)
            {
                if (item2.Tag == item)
                {
                    item2.Remove();
                    break;
                }
            }
        }

        public void DeleteNode(IManagmentTreeNode node)
        {
            TreeNode node2 = this.FindNode(this.tvTree.Nodes, node);
            if (node2 != null)
            {
                node2.Remove();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private TreeNode FindNode(TreeNodeCollection nodes, IManagmentTreeNode node)
        {
            foreach (TreeNode node2 in nodes)
            {
                if (node2.Tag == node)
                {
                    return node2;
                }
                TreeNode node3 = this.FindNode(node2.Nodes, node);
                if (node3 != null)
                {
                    return node3;
                }
            }
            return null;
        }

        public TreeNode GetNode(IManagmentTreeNode node)
        {
            return this.FindNode(this.tvTree.Nodes, node);
        }


        private void lvList_MouseDown(object sender, MouseEventArgs e)
        {
            if ((e.Button == MouseButtons.Right) && (this.lvList.SelectedItems.Count > 0))
            {
                this.ShowListContext(this.lvList.SelectedItems[0]);
            }
        }

        private void lvList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lvList.SelectedItems.Count > 0)
            {
                ListViewItem item = this.lvList.SelectedItems[0];
                if (item.Tag is IManagmentListItem)
                {
                    try
                    {
                        ((IManagmentListItem) item.Tag).Select(item);
                    }
                    catch (Exception exception)
                    {
                        TheInstance.ShowError(exception);
                    }
                }
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
            }
            catch (Exception exception)
            {
                TheInstance.ShowError(exception);
            }
        }

        public void PopulateTree()
        {
            UsersNode.Initialize();
            SessionListItem.Initialize();
            PermissionNode.Initialize();
            TreeNode node = new TreeNode();
            SecurityNode node2 = new SecurityNode();
            node.Tag = node2;
            node.Text = "Security";
            this.tvTree.Nodes.Add(node);
            node2.PopulateChilds(node.Nodes);
            TreeNode node3 = new TreeNode();
            SessionsNode node4 = new SessionsNode();
            node3.Tag = node4;
            node3.Text = "Sessions";
            this.tvTree.Nodes.Add(node3);
            node4.PopulateChilds(node3.Nodes);
        }

        public DialogResult ShowConformation(string Msg)
        {
            return MessageBox.Show(Msg, "CIS", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void ShowContext(TreeNode n)
        {
            if (n.Tag is IManagmentTreeNode)
            {
                try
                {
                    ((IManagmentTreeNode) n.Tag).ShowContext(n);
                }
                catch (Exception exception)
                {
                    TheInstance.ShowError(exception);
                }
            }
        }

        public DialogResult ShowDialogBox(Form f)
        {
            f.Owner = this;
            return f.ShowDialog();
        }

        public DialogResult ShowError(Exception ex)
        {
            return MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "CIS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        public DialogResult ShowError(string Msg)
        {
            return MessageBox.Show(Msg, "CIS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        private void ShowListContext(ListViewItem li)
        {
            if (li.Tag is IManagmentListItem)
            {
                try
                {
                    ((IManagmentListItem) li.Tag).ShowContext(li);
                }
                catch (Exception exception)
                {
                    TheInstance.ShowError(exception);
                }
            }
        }

        public DialogResult ShowMessage(string Msg)
        {
            return MessageBox.Show(Msg, "CIS", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public void ShowPopupForm(Form f)
        {
            f.Owner = this;
            f.Show();
        }

        public DialogResult ShowWarning(string Msg)
        {
            return MessageBox.Show(Msg, "CIS", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AuditViewer viewer = new AuditViewer();
            viewer.Owner = this;
            viewer.Show();
        }

        private void tvTree_AfterExpand(object sender, TreeViewEventArgs e)
        {
            foreach (TreeNode node in e.Node.Nodes)
            {
                if ((node.Tag is IManagmentTreeNode) && (node.Nodes.Count == 0))
                {
                    try
                    {
                        ((IManagmentTreeNode) node.Tag).PopulateChilds(node.Nodes);
                    }
                    catch (Exception exception)
                    {
                        TheInstance.ShowError(exception);
                    }
                }
            }
        }

        private void tvTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag is IManagmentTreeNode)
            {
                try
                {
                    ((IManagmentTreeNode) e.Node.Tag).Select(e.Node, this.lvList);
                }
                catch (Exception exception)
                {
                    TheInstance.ShowError(exception);
                }
            }
        }

        private void tvTree_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Apps) && (this.tvTree.SelectedNode != null))
            {
                this.ShowContext(this.tvTree.SelectedNode);
            }
        }

        private void tvTree_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                TreeViewHitTestInfo info = this.tvTree.HitTest(this.tvTree.PointToClient(Cursor.Position));
                if (info.Node != null)
                {
                    this.tvTree.SelectedNode = info.Node;
                    this.ShowContext(info.Node);
                }
            }
        }

        private void menLicense_Click(object sender, EventArgs e)
        {
            new LicenseViewer().ShowDialog(this);
        }
    }
}

