namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Client;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class ObjectPicker : Form
    {
       
        
        private IContainer components = null;
        private ArrayList Loaded = new ArrayList();
        private ArrayList m_NodeSearch;
        private SecurityObjectData[] m_PickedObjects;
        
        private static ObjectPicker TheInstance;
        

        public ObjectPicker()
        {
            this.InitializeComponent();
            this.m_PickedObjects = new SecurityObjectData[0];
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.m_PickedObjects = new SecurityObjectData[0];
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.m_NodeSearch = new ArrayList();
            this.Search(this.tvObjects.Nodes);
            this.m_PickedObjects = new SecurityObjectData[this.m_NodeSearch.Count];
            this.m_NodeSearch.CopyTo(this.m_PickedObjects);
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void LoadNodeChilds(TreeNodeCollection nodes, SecurityObjectData Root, int level)
        {
            if (level == 1)
            {
                this.Loaded.Add(Root.ID);
            }
            foreach (SecurityObjectData data in SecurityClient.GetChildList(Root.ID))
            {
                TreeNode node = new TreeNode(data.Name);
                node.Tag = data;
                nodes.Add(node);
                if (level > 0)
                {
                    this.LoadNodeChilds(node.Nodes, data, level - 1);
                }
            }
        }

        public static SecurityObjectData[] PickObject(string Title, int Root)
        {
            if (TheInstance == null)
            {
                TheInstance = new ObjectPicker();
            }
            TheInstance.Text = Title;
            TheInstance.Reload(-1);
            TheInstance.ShowDialog();
            return TheInstance.m_PickedObjects;
        }

        public void Reload(int Root)
        {
            this.tvObjects.Nodes.Clear();
            this.LoadNodeChilds(this.tvObjects.Nodes, SecurityClient.GetObject(Root), 1);
        }

        private void Search(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Checked)
                {
                    this.m_NodeSearch.Add(node.Tag);
                }
                this.Search(node.Nodes);
            }
        }

        private void tvObjects_AfterExpand(object sender, TreeViewEventArgs e)
        {
            foreach (TreeNode node in e.Node.Nodes)
            {
                if (!this.Loaded.Contains(((SecurityObjectData) node.Tag).ID))
                {
                    this.LoadNodeChilds(node.Nodes, (SecurityObjectData) node.Tag, 0);
                }
            }
        }
    }
}

