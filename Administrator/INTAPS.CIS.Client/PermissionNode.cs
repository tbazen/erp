namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Client;
    using System;
    using System.Windows.Forms;

    public class PermissionNode : IManagmentTreeNode
    {
        private SecurityObjectData m_Obj;
        private static ContextMenu Menu;
        private static PermissionNode SelectedObject;

        public PermissionNode(SecurityObjectData SObject)
        {
            this.m_Obj = SObject;
        }

        private static void AddObject(object sender, EventArgs e)
        {
            try
            {
                new SecurityObjectProperties(SelectedObject, null).ShowDialog();
            }
            catch (Exception exception)
            { 
                MainForm.TheInstance.ShowError(exception);
            }
        }

        private static void DeleteObject(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you sure you want to delete this object?", "CIS Security", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    SecurityClient.DeleteObject(SelectedObject.m_Obj.ID);
                    MainForm.TheInstance.DeleteNode(SelectedObject);
                }
            }
            catch (Exception exception)
            {
               
                MainForm.TheInstance.ShowError(exception);
            }
        }

        public static void Initialize()
        {
            Menu = new ContextMenu();
            MenuItem item = new MenuItem("Delete");
            item.Click += new EventHandler(PermissionNode.DeleteObject);
            Menu.MenuItems.Add(item);
            item = new MenuItem("Add");
            item.Click += new EventHandler(PermissionNode.AddObject);
            Menu.MenuItems.Add(item);
            item = new MenuItem("Properties");
            item.Click += new EventHandler(PermissionNode.ObjectProperties);
            Menu.MenuItems.Add(item);
        }

        private static void ObjectProperties(object sender, EventArgs e)
        {
            try
            {
                new SecurityObjectProperties(SelectedObject, SelectedObject.m_Obj).ShowDialog();
            }
            catch (Exception exception)
            {
                MainForm.TheInstance.ShowError(exception);
            }
        }

        public void PopulateChilds(TreeNodeCollection nodes)
        {
            foreach (SecurityObjectData data in SecurityClient.GetChildList(this.m_Obj.ID))
            {
                TreeNode node = new TreeNode(data.Name);
                node.Tag = new PermissionNode(data);
                nodes.Add(node);
            }
        }

        public void Select(TreeNode node, ListView lv)
        {
            ListViewItem item;
            lv.Clear();
            lv.Groups.Clear();
            lv.View = View.Details;
            ColumnHeader header = new ColumnHeader("User Name");
            header.Width = 200;
            lv.Columns.Add(header);
            ListViewGroup group = new ListViewGroup("Permitted Users");
            lv.Groups.Add(group);
            ListViewGroup group2 = new ListViewGroup("Denied Users");
            lv.Groups.Add(group2);
            SelectedObject = this;
            string[] permittedEntries = SecurityClient.GetPermittedEntries(this.m_Obj.ID);
            foreach (string str in permittedEntries)
            {
                item = new ListViewItem(str);
                item.Group = group;
                item.Tag = new PermissionUserItems(str, true);
                lv.Items.Add(item);
            }
            permittedEntries = SecurityClient.GetDeniedEntries(this.m_Obj.ID);
            foreach (string str in permittedEntries)
            {
                item = new ListViewItem(str);
                item.Group = group2;
                item.Tag = new PermissionUserItems(str, true);
                lv.Items.Add(item);
            }
        }

        public void ShowContext(TreeNode node)
        {
            Menu.Show(node.TreeView, node.TreeView.PointToClient(Cursor.Position));
        }

        public SecurityObjectData SecurityObject
        {
            get
            {
                return this.m_Obj;
            }
        }
    }
}

