namespace INTAPS.CIS.Client
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class PropertyWindow : Form
    {
        
        
        
        private IContainer components = null;
        
        
        

        public PropertyWindow()
        {
            this.InitializeComponent();
            this.btnApply.Enabled = false;
        }

        protected virtual void ApplyAndClose()
        {
            this.ApplyChanges();
            base.DialogResult = DialogResult.OK;
            base.Close();
        }

        protected virtual void ApplyChanges()
        {
            this.btnApply.Enabled = false;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.ApplyChanges();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.CloseWindow();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.ApplyAndClose();
        }

        protected virtual void CloseWindow()
        {
            base.DialogResult = DialogResult.Cancel;
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected virtual void FieldChanged()
        {
            this.btnApply.Enabled = true;
        }


        private void PropertyWindow_Load(object sender, EventArgs e)
        {
        }
    }
}

