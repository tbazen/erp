namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer.Client;
    using System;
    using System.Windows.Forms;

    public class SecurityNode : IManagmentTreeNode
    {
        public void PopulateChilds(TreeNodeCollection nodes)
        {
            TreeNode node = new TreeNode();
            node.Text = "Users";
            node.Tag = new UsersNode("");
            nodes.Add(node);
            node = new TreeNode();
            node.Text = "Objects";
            node.Tag = new PermissionNode(SecurityClient.GetObject(-1));
            nodes.Add(node);
        }

        public void Select(TreeNode node, ListView lv)
        {
            lv.Clear();
            lv.Groups.Clear();
        }

        public void ShowContext(TreeNode node)
        {
        }
    }
}

