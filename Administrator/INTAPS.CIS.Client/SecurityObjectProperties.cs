namespace INTAPS.CIS.Client
{
    using INTAPS.CIS;
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Client;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class SecurityObjectProperties : Form
    {
        
        
        
        
        
        private Container components = null;
        
        private PermissionNode m_Node;
        private SecurityObjectData m_so;
        private UpdateBuffer m_ub;
        
        

        public SecurityObjectProperties(PermissionNode node, SecurityObjectData so)
        {
            this.InitializeComponent();
            this.m_Node = node;
            string[] allUsers = SecurityClient.GetAllUsers();
            Array.Sort<string>(allUsers);
            foreach (string str in allUsers)
            {
                this.cmbOwner.Items.Add(str);
            }
            this.m_ub = new UpdateBuffer();
            if (so != null)
            {
                this.m_so = so;
                this.Text = so.Name + " Properties";
                this.txtName.Text = so.Name;
                this.txtFullName.Text = so.FullName;
                this.chkInherit.Checked = so.Class == SecurityObjectClass.soInherit;
                string ownerName = so.OwnerName;
                this.cmbOwner.SelectedItem = ownerName;
                this.cmdApply.Enabled = false;
                base.Tag = false;
            }
            else
            {
                this.Text = "New Object";
                this.txtName.Text = "New Object";
                this.txtFullName.Text = "";
                this.chkInherit.Checked = true;
                this.cmbOwner.SelectedIndex = 0;
                this.cmdApply.Enabled = true;
                base.Tag = true;
                this.m_ub.Update(0, "Name", "New Object");
            }
        }

        private void chkInherit_CheckedChanged(object sender, EventArgs e)
        {
            this.m_ub.Update(0, "Class", 0);
            this.cmdApply.Enabled = true;
        }

        private void cmbOwner_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.m_ub.Update(0, "Owner", 0);
            this.cmdApply.Enabled = true;
        }

        private void cmdApply_Click(object sender, EventArgs e)
        {
            this.SaveChanges();
            this.cmdApply.Enabled = false;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
            base.Close();
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            try
            {
                this.SaveChanges();
                base.DialogResult = DialogResult.OK;
                base.Close();
            }
            catch(Exception ex)
            {
                MainForm.TheInstance.ShowError(ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void SaveChanges()
        {
            if (this.m_ub.changed)
            {
                UpdatedRow row = this.m_ub.UpdatedRows[0];
                UserPermissionsData data = SecurityClient.User(this.cmbOwner.Text);
                if ((bool) base.Tag)
                {
                    int objectID = SecurityClient.CreateObject(this.m_Node.SecurityObject.ID, this.txtName.Text);
                    SecurityObjectData sObject = SecurityClient.GetObject(objectID);
                    SecurityClient.ChangeOwner(objectID, data.UserID);
                    SecurityClient.ChangeClass(objectID, this.chkInherit.Checked ? SecurityObjectClass.soInherit : SecurityObjectClass.soNoInherit);
                    TreeNode node = new TreeNode();
                    node.Text = sObject.Name;
                    node.Tag = new PermissionNode(sObject);
                    MainForm.TheInstance.GetNode(this.m_Node).Nodes.Add(node);
                    base.Tag = false;
                    this.txtFullName.Text = sObject.FullName;
                }
                else
                {
                    string text = null;
                    foreach (string str2 in row.Fields)
                    {
                        string str3 = str2;
                        if (str3 != null)
                        {
                            if (!(str3 == "Name"))
                            {
                                if (str3 == "Class")
                                {
                                    goto Label_018D;
                                }
                                if (str3 == "Owner")
                                {
                                    goto Label_01B1;
                                }
                            }
                            else
                            {
                                SecurityClient.Rename(this.m_so.ID, this.txtName.Text);
                                text = this.txtName.Text;
                            }
                        }
                        continue;
                    Label_018D:
                        SecurityClient.ChangeClass(this.m_so.ID, this.chkInherit.Checked ? SecurityObjectClass.soInherit : SecurityObjectClass.soNoInherit);
                        continue;
                    Label_01B1:
                        SecurityClient.ChangeOwner(this.m_so.ID, data.UserID);
                    }
                    if (text != null)
                    {
                        MainForm.TheInstance.GetNode(this.m_Node).Text = text;
                    }
                }
                this.cmdApply.Enabled = false;
                this.m_ub = new UpdateBuffer();
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            this.m_ub.Update(0, "Name", 0);
            this.cmdApply.Enabled = true;
        }

        private void txtFullName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

