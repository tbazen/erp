namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer;
    using System;
    using System.Windows.Forms;

    internal class SessionListItem : IManagmentListItem
    {
        private UserSessionInfo m_Session;
        private static SessionListItem Selected = null;
        private static ContextMenu SessionMenu;

        public SessionListItem(UserSessionInfo session)
        {
            this.m_Session = session;
        }

        public static void Initialize()
        {
            SessionMenu = new ContextMenu();
            MenuItem item = new MenuItem("Kill");
            item.Click += new EventHandler(SessionListItem.KillSession);
            SessionMenu.MenuItems.Add(item);
        }

        private static void KillSession(object sender, EventArgs e)
        {
            if (Selected != null)
            {
                try
                {
                    MainForm.TheInstance.DeleteItem(Selected);
                }
                catch (Exception exception)
                {
                    MainForm.TheInstance.ShowError(exception);
                }
            }
        }

        public void Open(ListViewItem item)
        {
        }

        public void Select(ListViewItem item)
        {
            Selected = this;
        }

        public void ShowContext(ListViewItem item)
        {
            SessionMenu.Show(item.ListView, item.ListView.PointToClient(Cursor.Position));
        }
    }
}

