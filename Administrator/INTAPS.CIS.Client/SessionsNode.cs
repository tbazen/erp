namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Client;
    using System;
    using System.Windows.Forms;

    internal class SessionsNode : IManagmentTreeNode
    {
        public void PopulateChilds(TreeNodeCollection nodes)
        {
        }

        public void Select(TreeNode node, ListView lv)
        {
            lv.Clear();
            lv.Groups.Clear();
            ColumnHeader header = new ColumnHeader();
            header.Text = "User Name";
            header.Width = 200;
            lv.Columns.Add(header);
            header = new ColumnHeader();
            header.Text = "Idle Time";
            header.Width = 200;
            lv.Columns.Add(header);
            header = new ColumnHeader();
            header.Text = "Source";
            header.Width = 200;
            lv.Columns.Add(header);

            UserSessionInfo[] activeSessions = ApplicationClient.GetActiveSessions();
            foreach (UserSessionInfo info in activeSessions)
            {
                ListViewItem item = new ListViewItem(info.UserName);
                item.Tag = new SessionListItem(info);
                item.SubItems.Add(info.IdleSeconds.ToString());
                item.SubItems.Add(info.Source);
                lv.Items.Add(item);
            }
        }

        public void ShowContext(TreeNode node)
        {
        }
    }
}

