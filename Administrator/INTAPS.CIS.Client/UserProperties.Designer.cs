﻿namespace INTAPS.CIS.Client
{
    public partial class UserProperties : PropertyWindow
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lstPermited = new System.Windows.Forms.ListBox();
            this.lstDenied = new System.Windows.Forms.ListBox();
            this.btnAddAccess = new System.Windows.Forms.Button();
            this.btnDeny = new System.Windows.Forms.Button();
            this.btnRemoveAccess = new System.Windows.Forms.Button();
            this.btnRemoveDeny = new System.Windows.Forms.Button();
            this.btnPermit = new System.Windows.Forms.Button();
            this.btnAddDeny = new System.Windows.Forms.Button();
            this.grpPermited = new System.Windows.Forms.GroupBox();
            this.grpDenied = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAmharicName = new System.Windows.Forms.TextBox();
            this.tabPropPages.SuspendLayout();
            this.tabPageGeneral.SuspendLayout();
            this.grpPermited.SuspendLayout();
            this.grpDenied.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPropPages
            // 
            this.tabPropPages.Size = new System.Drawing.Size(428, 510);
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.Controls.Add(this.grpDenied);
            this.tabPageGeneral.Controls.Add(this.grpPermited);
            this.tabPageGeneral.Controls.Add(this.txtPassword2);
            this.tabPageGeneral.Controls.Add(this.label3);
            this.tabPageGeneral.Controls.Add(this.txtPassword1);
            this.tabPageGeneral.Controls.Add(this.label2);
            this.tabPageGeneral.Controls.Add(this.txtAmharicName);
            this.tabPageGeneral.Controls.Add(this.label4);
            this.tabPageGeneral.Controls.Add(this.txtUserName);
            this.tabPageGeneral.Controls.Add(this.label1);
            this.tabPageGeneral.Size = new System.Drawing.Size(420, 482);
            this.tabPageGeneral.Click += new System.EventHandler(this.tabPageGeneral_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(11, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserName.Location = new System.Drawing.Point(152, 22);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(258, 23);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // lstPermited
            // 
            this.lstPermited.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstPermited.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstPermited.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lstPermited.FormattingEnabled = true;
            this.lstPermited.ItemHeight = 15;
            this.lstPermited.Location = new System.Drawing.Point(6, 19);
            this.lstPermited.Name = "lstPermited";
            this.lstPermited.Size = new System.Drawing.Size(367, 94);
            this.lstPermited.TabIndex = 2;
            this.lstPermited.SelectedIndexChanged += new System.EventHandler(this.lstPermited_SelectedIndexChanged);
            // 
            // lstDenied
            // 
            this.lstDenied.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDenied.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDenied.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.lstDenied.FormattingEnabled = true;
            this.lstDenied.ItemHeight = 15;
            this.lstDenied.Location = new System.Drawing.Point(6, 19);
            this.lstDenied.Name = "lstDenied";
            this.lstDenied.Size = new System.Drawing.Size(363, 94);
            this.lstDenied.TabIndex = 2;
            this.lstDenied.SelectedIndexChanged += new System.EventHandler(this.lstDenied_SelectedIndexChanged);
            // 
            // btnAddAccess
            // 
            this.btnAddAccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnAddAccess.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAccess.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnAddAccess.ForeColor = System.Drawing.Color.White;
            this.btnAddAccess.Location = new System.Drawing.Point(7, 116);
            this.btnAddAccess.Name = "btnAddAccess";
            this.btnAddAccess.Size = new System.Drawing.Size(58, 28);
            this.btnAddAccess.TabIndex = 0;
            this.btnAddAccess.Text = "&Add";
            this.btnAddAccess.UseVisualStyleBackColor = false;
            this.btnAddAccess.Click += new System.EventHandler(this.AddObject);
            // 
            // btnDeny
            // 
            this.btnDeny.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeny.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnDeny.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeny.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnDeny.ForeColor = System.Drawing.Color.White;
            this.btnDeny.Location = new System.Drawing.Point(188, 116);
            this.btnDeny.Name = "btnDeny";
            this.btnDeny.Size = new System.Drawing.Size(68, 28);
            this.btnDeny.TabIndex = 1;
            this.btnDeny.Text = "&Deny";
            this.btnDeny.UseVisualStyleBackColor = false;
            this.btnDeny.Click += new System.EventHandler(this.SwapState);
            // 
            // btnRemoveAccess
            // 
            this.btnRemoveAccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveAccess.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnRemoveAccess.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemoveAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveAccess.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnRemoveAccess.ForeColor = System.Drawing.Color.White;
            this.btnRemoveAccess.Location = new System.Drawing.Point(305, 116);
            this.btnRemoveAccess.Name = "btnRemoveAccess";
            this.btnRemoveAccess.Size = new System.Drawing.Size(68, 28);
            this.btnRemoveAccess.TabIndex = 2;
            this.btnRemoveAccess.Text = "&Remove";
            this.btnRemoveAccess.UseVisualStyleBackColor = false;
            this.btnRemoveAccess.Click += new System.EventHandler(this.RemoveObject);
            // 
            // btnRemoveDeny
            // 
            this.btnRemoveDeny.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveDeny.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnRemoveDeny.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemoveDeny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveDeny.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnRemoveDeny.ForeColor = System.Drawing.Color.White;
            this.btnRemoveDeny.Location = new System.Drawing.Point(301, 117);
            this.btnRemoveDeny.Name = "btnRemoveDeny";
            this.btnRemoveDeny.Size = new System.Drawing.Size(66, 28);
            this.btnRemoveDeny.TabIndex = 2;
            this.btnRemoveDeny.Text = "&Remove";
            this.btnRemoveDeny.UseVisualStyleBackColor = false;
            this.btnRemoveDeny.Click += new System.EventHandler(this.RemoveObject);
            // 
            // btnPermit
            // 
            this.btnPermit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPermit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnPermit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPermit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPermit.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnPermit.ForeColor = System.Drawing.Color.White;
            this.btnPermit.Location = new System.Drawing.Point(184, 117);
            this.btnPermit.Name = "btnPermit";
            this.btnPermit.Size = new System.Drawing.Size(68, 28);
            this.btnPermit.TabIndex = 1;
            this.btnPermit.Text = "&Permit";
            this.btnPermit.UseVisualStyleBackColor = false;
            this.btnPermit.Click += new System.EventHandler(this.SwapState);
            // 
            // btnAddDeny
            // 
            this.btnAddDeny.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnAddDeny.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddDeny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddDeny.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnAddDeny.ForeColor = System.Drawing.Color.White;
            this.btnAddDeny.Location = new System.Drawing.Point(9, 117);
            this.btnAddDeny.Name = "btnAddDeny";
            this.btnAddDeny.Size = new System.Drawing.Size(58, 28);
            this.btnAddDeny.TabIndex = 0;
            this.btnAddDeny.Text = "&Add";
            this.btnAddDeny.UseVisualStyleBackColor = false;
            this.btnAddDeny.Click += new System.EventHandler(this.AddObject);
            // 
            // grpPermited
            // 
            this.grpPermited.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPermited.Controls.Add(this.lstPermited);
            this.grpPermited.Controls.Add(this.btnAddAccess);
            this.grpPermited.Controls.Add(this.btnDeny);
            this.grpPermited.Controls.Add(this.btnRemoveAccess);
            this.grpPermited.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpPermited.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.grpPermited.Location = new System.Drawing.Point(14, 145);
            this.grpPermited.Name = "grpPermited";
            this.grpPermited.Size = new System.Drawing.Size(396, 155);
            this.grpPermited.TabIndex = 8;
            this.grpPermited.TabStop = false;
            this.grpPermited.Text = "Permitted Accesses";
            this.grpPermited.Enter += new System.EventHandler(this.grpPermited_Enter);
            // 
            // grpDenied
            // 
            this.grpDenied.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDenied.Controls.Add(this.lstDenied);
            this.grpDenied.Controls.Add(this.btnAddDeny);
            this.grpDenied.Controls.Add(this.btnRemoveDeny);
            this.grpDenied.Controls.Add(this.btnPermit);
            this.grpDenied.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDenied.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.grpDenied.Location = new System.Drawing.Point(14, 314);
            this.grpDenied.Name = "grpDenied";
            this.grpDenied.Size = new System.Drawing.Size(396, 152);
            this.grpDenied.TabIndex = 9;
            this.grpDenied.TabStop = false;
            this.grpDenied.Text = "Denied Accesses";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(11, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password:";
            this.label2.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtPassword1
            // 
            this.txtPassword1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword1.Location = new System.Drawing.Point(152, 73);
            this.txtPassword1.Name = "txtPassword1";
            this.txtPassword1.PasswordChar = '*';
            this.txtPassword1.Size = new System.Drawing.Size(258, 23);
            this.txtPassword1.TabIndex = 5;
            this.txtPassword1.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(11, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(398, 34);
            this.label3.TabIndex = 6;
            this.label3.Text = "Password confirmation:";
            this.label3.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtPassword2
            // 
            this.txtPassword2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword2.Location = new System.Drawing.Point(152, 98);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(258, 23);
            this.txtPassword2.TabIndex = 7;
            this.txtPassword2.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(11, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Amharic Name:";
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtAmharicName
            // 
            this.txtAmharicName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAmharicName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmharicName.Location = new System.Drawing.Point(152, 47);
            this.txtAmharicName.Name = "txtAmharicName";
            this.txtAmharicName.Size = new System.Drawing.Size(258, 24);
            this.txtAmharicName.TabIndex = 3;
            this.txtAmharicName.Visible = false;
            this.txtAmharicName.TextChanged += new System.EventHandler(this.txtUserName_TextChanged);
            // 
            // UserProperties
            // 
            this.ClientSize = new System.Drawing.Size(428, 555);
            this.Name = "UserProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User Properties";
            this.Load += new System.EventHandler(this.UserProperties_Load);
            this.tabPropPages.ResumeLayout(false);
            this.tabPageGeneral.ResumeLayout(false);
            this.tabPageGeneral.PerformLayout();
            this.grpPermited.ResumeLayout(false);
            this.grpDenied.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.ListBox lstPermited;
        private System.Windows.Forms.ListBox lstDenied;
        private System.Windows.Forms.Button btnAddAccess;
        private System.Windows.Forms.Button btnDeny;
        private System.Windows.Forms.Button btnRemoveAccess;
        private System.Windows.Forms.Button btnRemoveDeny;
        private System.Windows.Forms.Button btnPermit;
        private System.Windows.Forms.Button btnAddDeny;
        private System.Windows.Forms.GroupBox grpPermited;
        private System.Windows.Forms.GroupBox grpDenied;
        private System.Windows.Forms.TextBox txtPassword1;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.TextBox txtAmharicName;

    }
}
