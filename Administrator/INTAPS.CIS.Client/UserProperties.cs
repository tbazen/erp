namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Client;
    using INTAPS.Ethiopic;
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class UserProperties : PropertyWindow
    {
        
        
        
        
        
        
        private IContainer components = null;
        
        
        
        private Hashtable LoadedPermissions;
        
        
        private bool m_NewUser;
        private string m_ParentUser;
        private UsersNode.UserListItem m_updateitem;
        private UsersNode m_updatenode;
        public static PowerGeezMapper PowerGeez = new PowerGeezMapper();
        private Hashtable SetPermissions;
       
        
        

        public UserProperties(UsersNode updatenode, UsersNode.UserListItem updateitem, string UserName, bool New)
        {
            this.InitializeComponent();
            this.m_updatenode = updatenode;
            this.m_updateitem = updateitem;
            PowerGeez.AddControl(this.txtAmharicName);
            this.m_NewUser = New;
            this.btnAddAccess.Tag = this.lstPermited;
            this.btnDeny.Tag = this.lstPermited;
            this.btnRemoveAccess.Tag = this.lstPermited;
            this.btnAddDeny.Tag = this.lstDenied;
            this.btnPermit.Tag = this.lstDenied;
            this.btnRemoveDeny.Tag = this.lstDenied;
            if (this.m_NewUser)
            {
                this.m_ParentUser = UserName;
                this.txtUserName.Text = "New User";
                this.txtUserName.ReadOnly = false;
                this.grpDenied.Enabled = false;
                this.grpPermited.Enabled = false;
                this.FieldChanged();
            }
            else
            {
                this.SetUserProperties(UserName);
            }
            this.SetPermissions = new Hashtable();
        }

        private void AddObject(object sender, EventArgs e)
        {
            ListBox tag = (ListBox) ((Button) sender).Tag;
            foreach (SecurityObjectData data in ObjectPicker.PickObject("Select objects", -1))
            {
                if (this.LoadedPermissions.Contains(data.FullName))
                {
                    MainForm.TheInstance.ShowError(data.FullName + " already exists. Skipped");
                }
                else
                {
                    if (tag == this.lstPermited)
                    {
                        this.SetPermission(data, PermissionState.Permit);
                    }
                    else
                    {
                        this.SetPermission(data, PermissionState.Deny);
                    }
                    tag.Items.Add(data.FullName);
                    this.LoadedPermissions.Add(data.FullName, data);
                    base.FieldChanged();
                }
            }
        }

        protected override void ApplyChanges()
        {
            try
            {
                if (this.m_NewUser)
                {
                    if (this.txtPassword1.Text != this.txtPassword2.Text)
                    {
                        MainForm.TheInstance.ShowMessage("Password not confirmed correctly");
                        return;
                    }
                    SecurityClient.CreateUser(this.m_ParentUser, this.txtUserName.Text, this.txtPassword1.Text);
                    this.SetUserProperties(this.txtUserName.Text);
                    this.m_NewUser = false;
                    if (this.m_updatenode != null)
                    {
                        TreeNode node = MainForm.TheInstance.GetNode(this.m_updatenode);
                        TreeNode node2 = new TreeNode();
                        node2.Text = this.txtUserName.Text;
                        node2.Tag = new UsersNode(this.txtUserName.Text);
                        node.Nodes.Add(node2);
                    }
                }
                else
                {
                    foreach (DictionaryEntry entry in this.SetPermissions)
                    {
                        int key = (int) entry.Key;
                        PermissionState state = (PermissionState) entry.Value;
                        SecurityClient.SetPermission(key, this.txtUserName.Text, state);
                    }
                }
                base.ApplyChanges();
            }
            catch (Exception exception)
            {
                MainForm.TheInstance.ShowError(exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void grpPermited_Enter(object sender, EventArgs e)
        {
        }


        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void lstDenied_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateButtonState();
        }

        private void lstPermited_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateButtonState();
        }

        private void RemoveObject(object sender, EventArgs e)
        {
            ListBox tag = (ListBox) ((Button) sender).Tag;
            int num = 0;
            int count = tag.SelectedItems.Count;
            for (num = 0; num < count; num++)
            {
                SecurityObjectData o = (SecurityObjectData) this.LoadedPermissions[tag.SelectedItems[0]];
                this.SetPermission(o, PermissionState.Undefined);
                tag.Items.Remove(o.FullName);
                this.LoadedPermissions.Remove(o.FullName);
                base.FieldChanged();
            }
        }

        private void SetPermission(SecurityObjectData o, PermissionState state)
        {
            if (this.SetPermissions.Contains(o.ID))
            {
                this.SetPermissions[o.ID] = state;
            }
            else
            {
                this.SetPermissions.Add(o.ID, state);
            }
        }

        private void SetUserProperties(string UserName)
        {
            SecurityObjectData data;
            this.grpDenied.Enabled = true;
            this.grpPermited.Enabled = true;
            this.txtPassword1.Text = "******************";
            this.txtPassword2.Text = "******************";
            this.txtPassword1.Enabled = false;
            this.txtPassword2.Enabled = false;
            this.txtUserName.Text = UserName;
            this.txtUserName.ReadOnly = true;
            int[] permissions = SecurityClient.User(UserName).Permissions;
            int[] denied = SecurityClient.User(UserName).Denied;
            this.LoadedPermissions = new Hashtable();
            foreach (int num in permissions)
            {
                data = SecurityClient.GetObject(num);
                this.lstPermited.Items.Add(data.FullName);
                this.LoadedPermissions.Add(data.FullName, data);
            }
            foreach (int num in denied)
            {
                data = SecurityClient.GetObject(num);
                this.lstDenied.Items.Add(data.FullName);
                this.LoadedPermissions.Add(data.FullName, data);
            }
        }

        private void SwapState(object sender, EventArgs e)
        {
            ListBox tag = (ListBox) ((Button) sender).Tag;
            ListBox box2 = (tag == this.lstPermited) ? this.lstDenied : this.lstPermited;
            int num = 0;
            int count = tag.SelectedItems.Count;
            for (num = 0; num < count; num++)
            {
                SecurityObjectData o = (SecurityObjectData) this.LoadedPermissions[tag.SelectedItems[0]];
                if (tag == this.lstPermited)
                {
                    this.SetPermission(o, PermissionState.Deny);
                }
                else
                {
                    this.SetPermission(o, PermissionState.Permit);
                }
                tag.Items.Remove(o.FullName);
                box2.Items.Add(o.FullName);
                base.FieldChanged();
            }
        }

        private void tabPageGeneral_Click(object sender, EventArgs e)
        {
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
        }

        private void UpdateButtonState()
        {
            bool flag = this.lstDenied.SelectedItems.Count > 0;
            this.btnRemoveDeny.Enabled = flag;
            this.btnPermit.Enabled = flag;
            flag = this.lstPermited.SelectedItems.Count > 0;
            this.btnRemoveAccess.Enabled = flag;
            this.btnDeny.Enabled = flag;
        }

        private void UserProperties_Load(object sender, EventArgs e)
        {
        }
    }
}

