namespace INTAPS.CIS.Client
{
    using INTAPS.ClientServer.Client;
    using System;
    using System.Windows.Forms;

    public class UsersNode : IManagmentTreeNode
    {
        private string m_UserName;
        private static MenuItem miAddUser;
        private static MenuItem miChangePassword;
        private static MenuItem miDelete;
        private static MenuItem miProperties;
        private static UserListItem SelectedItem;
        private static UsersNode SelectedNode;
        private static string SelectedUser;
        private static ContextMenu UserMenu;

        public UsersNode(string UserName)
        {
            this.m_UserName = UserName;
        }

        private static void AddUser(object sender, EventArgs e)
        {
            MainForm.TheInstance.ShowDialogBox(new UserProperties(SelectedNode, null, SelectedUser, true));
        }

        private static void ChangePassword(object sender, EventArgs e)
        {
            MainForm.TheInstance.ShowDialogBox(new INTAPS.CIS.Client.ChangePassword(SelectedUser));
        }

        public static void DeleteUser(object source, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you sure you want to delete this user?", "CIS Security", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    SecurityClient.DeleteUser(SelectedUser);
                    if (SelectedNode != null)
                    {
                        MainForm.TheInstance.DeleteNode(SelectedNode);
                    }
                    if (SelectedItem != null)
                    {
                        MainForm.TheInstance.DeleteItem(SelectedItem);
                    }
                }
            }
            catch (Exception exception)
            {
                MainForm.TheInstance.ShowError(exception);
            }
        }

        public static void Initialize()
        {
            UserMenu = new ContextMenu();
            miDelete = UserMenu.MenuItems.Add("Delete");
            miDelete.Click += new EventHandler(UsersNode.DeleteUser);
            UserMenu.MenuItems.Add(miDelete);
            miProperties = UserMenu.MenuItems.Add("Properties");
            miProperties.Click += new EventHandler(UsersNode.ShowProperties);
            UserMenu.MenuItems.Add(miProperties);
            miAddUser = UserMenu.MenuItems.Add("Add User");
            miAddUser.Click += new EventHandler(UsersNode.AddUser);
            UserMenu.MenuItems.Add(miAddUser);
            miChangePassword = UserMenu.MenuItems.Add("ChangePassword");
            miChangePassword.Click += new EventHandler(UsersNode.ChangePassword);
            UserMenu.MenuItems.Add(miChangePassword);
            UserMenu.Popup += new EventHandler(UsersNode.UserMenuPopup);
        }

        public void PopulateChilds(TreeNodeCollection nodes)
        {
            string[] childUsers = SecurityClient.GetChildUsers(this.m_UserName);
            Array.Sort<string>(childUsers);
            foreach (string str in childUsers)
            {
                TreeNode node = new TreeNode();
                node.Tag = new UsersNode(str);
                node.Text = str;
                nodes.Add(node);
            }
        }

        private void PupulateChilds(TreeNodeCollection nodes, string Parent)
        {
            string[] childUsers = SecurityClient.GetChildUsers(Parent);
            Array.Sort<string>(childUsers);
            foreach (string str in childUsers)
            {
                TreeNode node = new TreeNode();
                node.Text = str;
                nodes.Add(node);
                this.PupulateChilds(node.Nodes, str);
            }
        }

        public void Select(TreeNode node, ListView lv)
        {
            SelectedNode = this;
            SelectedItem = null;
            SelectedUser = this.m_UserName;
            lv.Clear();
            lv.Groups.Clear();
            ColumnHeader header = new ColumnHeader();
            header.Text = "User Name";
            header.Width = 200;
            lv.Columns.Add(header);
            header = new ColumnHeader();
            header.Text = "Group";
            header.Width = 200;
            lv.Columns.Add(header);
            string[] childUsers = SecurityClient.GetChildUsers(this.m_UserName);
            Array.Sort<string>(childUsers);
            foreach (string str in childUsers)
            {
                ListViewItem item = new ListViewItem(str);
                string parentUser = SecurityClient.GetParentUser(str);
                if (parentUser == null)
                {
                    parentUser = "<No Group>";
                }
                item.SubItems.Add(parentUser);
                item.Tag = new UserListItem(str);
                lv.Items.Add(item);
            }
        }

        public void ShowContext(TreeNode node)
        {
            UserMenu.Show(node.TreeView, node.TreeView.PointToClient(Cursor.Position));
            node.TreeView.Focus();
        }

        public static void ShowProperties(object source, EventArgs e)
        {
            MainForm.TheInstance.ShowPopupForm(new UserProperties(SelectedNode, SelectedItem, SelectedUser, false));
        }

        private static void UserMenuPopup(object sender, EventArgs e)
        {
            bool flag = SelectedUser != "";
            miDelete.Enabled = flag;
            miProperties.Enabled = flag;
            miChangePassword.Enabled = flag;
            if (flag)
            {
                bool flag2 = SecurityClient.GetParentUser(SelectedUser) == null;
                miAddUser.Enabled = flag2;
            }
            else
            {
                miAddUser.Enabled = true;
            }
        }

        public class UserListItem : IManagmentListItem
        {
            private string m_UserName;

            public UserListItem(string UserName)
            {
                this.m_UserName = UserName;
            }

            public void Open(ListViewItem item)
            {
                UsersNode.ShowProperties(null, null);
            }

            public void Select(ListViewItem item)
            {
                UsersNode.SelectedUser = this.m_UserName;
                UsersNode.SelectedItem = this;
                UsersNode.SelectedNode = null;
            }

            public void ShowContext(ListViewItem item)
            {
                UsersNode.UserMenu.Show(item.ListView, item.ListView.PointToClient(Cursor.Position));
            }
        }
    }
}

