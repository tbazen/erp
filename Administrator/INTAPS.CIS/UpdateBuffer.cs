namespace INTAPS.CIS
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml.Serialization;

    [Serializable, XmlInclude(typeof(ArrayList))]
    public class UpdateBuffer
    {
        public ArrayList Deleted;
        public ArrayList NewData;
        public ArrayList NewKey;
        public ArrayList UpdatedData;
        public ArrayList UpdatedKey;

        public UpdateBuffer()
        {
            this.Initialize();
        }

        public void AddNew(object RowKey)
        {
            this.NewKey.Add(RowKey);
            this.NewData.Add(new Hashtable());
        }

        public void Delete(object RowKey)
        {
            int index = this.UpdatedKey.IndexOf(RowKey);
            if (index != -1)
            {
                this.UpdatedKey.RemoveAt(index);
                this.UpdatedData.RemoveAt(index);
            }
            else
            {
                index = this.NewKey.IndexOf(RowKey);
                if (index != -1)
                {
                    this.NewKey.RemoveAt(index);
                    this.NewData.RemoveAt(index);
                }
                else if (this.Deleted.Contains(RowKey))
                {
                    this.Deleted.Remove(this.Deleted.IndexOf(RowKey));
                }
                else
                {
                    this.Deleted.Add(RowKey);
                }
            }
        }

        private UpdatedRow[] GetUpdatedRows(ArrayList key, ArrayList data)
        {
            UpdatedRow[] rowArray = new UpdatedRow[data.Count];
            IEnumerator enumerator = key.GetEnumerator();
            IEnumerator enumerator2 = data.GetEnumerator();
            for (int i = 0; enumerator.MoveNext(); i++)
            {
                enumerator2.MoveNext();
                rowArray[i].RowKey = enumerator.Current;
                Hashtable current = (Hashtable) enumerator2.Current;
                rowArray[i].Fields = new string[current.Count];
                current.Keys.CopyTo(rowArray[i].Fields, 0);
                rowArray[i].Values = new object[current.Count];
                current.Values.CopyTo(rowArray[i].Values, 0);
            }
            return rowArray;
        }

        private void Initialize()
        {
            this.Deleted = new ArrayList();
            this.UpdatedKey = new ArrayList();
            this.UpdatedData = new ArrayList();
            this.NewKey = new ArrayList();
            this.NewData = new ArrayList();
        }

        public void Update(object RowKey, string Field, object value)
        {
            Hashtable hashtable;
            if (this.Deleted.Contains(RowKey))
            {
                this.Deleted.Remove(RowKey);
            }
            int index = this.NewKey.IndexOf(RowKey);
            if (index != -1)
            {
                hashtable = (Hashtable) this.NewData[index];
            }
            else
            {
                index = this.UpdatedKey.IndexOf(RowKey);
                if (index == -1)
                {
                    this.UpdatedKey.Add(RowKey);
                    this.UpdatedData.Add(new Hashtable());
                    index = this.UpdatedKey.Count - 1;
                }
                hashtable = (Hashtable) this.UpdatedData[index];
            }
            if (!hashtable.ContainsKey(Field))
            {
                hashtable.Add(Field, value);
            }
            else
            {
                hashtable[Field] = value;
            }
        }

        public bool changed
        {
            get
            {
                return (((this.NewData.Count > 0) || (this.UpdatedData.Count > 0)) || (this.Deleted.Count > 0));
            }
        }

        public object[] DeletedRows
        {
            get
            {
                object[] array = new object[this.Deleted.Count];
                this.Deleted.CopyTo(array);
                return array;
            }
        }

        private object this[object rowkey, string field, bool UpdatedRow]
        {
            get
            {
                if (UpdatedRow)
                {
                    return ((Hashtable) this.UpdatedData[this.UpdatedKey.IndexOf(rowkey)])[field];
                }
                return ((Hashtable) this.NewData[this.NewKey.IndexOf(rowkey)])[field];
            }
        }

        public UpdatedRow[] NewRows
        {
            get
            {
                return this.GetUpdatedRows(this.NewKey, this.NewData);
            }
        }

        public UpdatedRow[] UpdatedRows
        {
            get
            {
                return this.GetUpdatedRows(this.UpdatedKey, this.UpdatedData);
            }
        }
    }
}

