namespace INTAPS.CIS
{
    using System;
    using System.Reflection;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct UpdatedRow
    {
        public object RowKey;
        public string[] Fields;
        public object[] Values;
        public object this[string field]
        {
            get
            {
                for (int i = 0; i < this.Fields.Length; i++)
                {
                    if (this.Fields[i].ToUpper() == field.ToUpper())
                    {
                        return this.Values[i];
                    }
                }
                return null;
            }
        }
    }
}

