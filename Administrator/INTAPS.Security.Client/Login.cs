namespace INTAPS.Security.Client
{
    using INTAPS.ClientServer.Client;
    using INTAPS.ClientServer.Client;
    using System;
    using System.ComponentModel;
    using System.Configuration;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class Login : Form
    {
        
        
        private Container components = null;
       
        public bool logedin;
        private bool m_boolValidUser = false;
        
        

        public Login()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["Server"];
                ApplicationClient.Connect(url, this.txtUserName.Text, this.txtPassword.Text);
                SecurityClient.Connect(url);
                ApplicationClient.runKeepAliveThread(10000);
                this.logedin = true;
                base.Close();
            }
            catch (Exception exception)
            {
                String msg= exception.Message;
                while(exception.InnerException!=null)
                {
                    exception = exception.InnerException;
                    msg  +="\n->"+exception.Message;
                }
                MessageBox.Show(this, msg, "Could not login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.txtUserName.Focus();
                this.txtUserName.Text = "";
                this.txtPassword.Text = "";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public void TryLogin()
        {
            this.logedin = false;
            base.ShowDialog();
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
        }

        public bool ValidUser
        {
            get
            {
                return this.m_boolValidUser;
            }
        }
    }
}

