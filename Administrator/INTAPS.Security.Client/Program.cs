namespace INTAPS.Security.Client
{
    using INTAPS.CIS.Client;
    using System;
    using System.Windows.Forms;

    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login login = new Login();
            login.TryLogin();
            if (login.logedin)
            {
                INTAPS.ClientServer.Client.ApplicationClient.runKeepAliveThread(10000);
                Application.Run(new MainForm());
            }
        }
    }
}

