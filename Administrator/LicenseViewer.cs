﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.CIS.Client
{
    public partial class LicenseViewer : Form
    {
        public LicenseViewer()
        {
            InitializeComponent();
            TLicense.LicenseData data = INTAPS.ClientServer.Client.ApplicationClient.getLicenseData();
            if (data != null)
            {
                textID.Text = data.id;
                textName.Text = data.licensee;
                textDescription.Text = data.description;
                foreach (TLicense.LicenseValue val in data.data)
                {
                    grid.Rows.Add(
                        val.key+" ("+val.description+")",
                        string.IsNullOrEmpty(val.value)?"":val.value
                        +(val.expiryTime>-1?" (Expires on "+new DateTime(val.expiryTime)+")":"")
                        );
                }
            }
        }
    }
}
