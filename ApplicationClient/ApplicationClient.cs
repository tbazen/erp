namespace INTAPS.ClientServer.Client
{
    using INTAPS.ClientServer;
    using INTAPS.Ethiopic;
    using System;
    using System.Linq;
    public class ApplicationClient
    {
        static String path;
        //public static IApplicationServer ApplicationServer=null;
        public static string Password;
        public static PowerGeezMapper PowerGeez;
        public static string Server;
        public static string SessionID;
        public static string UserName;
        public static void CloseSession()
        {
            //ApplicationServer.CloseSession(SessionID);
            RESTAPIClient.Call<VoidRet>(path + "CloseSession", new { SessionID= SessionID});
        }

        public static void CloseSession(string sid)
        {
            //ApplicationServer.CloseSession(sid);
            RESTAPIClient.Call<VoidRet>(path + "CloseSession", new { SessionID = sid });
        }
        static System.Threading.Thread keepAliveThread = null;
        static bool enableKeepAliveThread=false;
        public static void runKeepAliveThread(int mil)
        {
            enableKeepAliveThread = true;
            keepAliveThread=new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate()
                {
                    while (enableKeepAliveThread)
                    {
                        System.Threading.Thread.Sleep(mil);
                        //ApplicationServer.keepAlive(ApplicationClient.SessionID);
                        RESTAPIClient.Call<VoidRet>(path + "keepAlive", new { sessionID=ApplicationClient.SessionID });
                    }
                }
                ));
            keepAliveThread.Start();
        }

        public static object[] GetSystemParameters(string path, string[] names)
        {
            return RESTAPIClient.Call<BinObject>(path + "GetSystemParameters", new { sessionID = ApplicationClient.SessionID, names = names }).Deserialized() as object[];

        }
        public static void SetSystemParameters(string path, string[] fields, object[] vals)
        {
            RESTAPIClient.Call<VoidRet>(path + "SetSystemParameters", new { sessionID = ApplicationClient.SessionID, fields = fields, vals = new BinObject(vals)});
        }

        public static void Connect(string url, string sessionID)
        {
            ApplicationClient.SessionID = sessionID;
            //ApplicationServer = (IApplicationServer)RemotingServices.Connect(typeof(IApplicationServer), url + "/AppServer");
            Server = url;
            //UserName = ApplicationServer.getSessionInfo(sessionID).UserName;
            UserName =RESTAPIClient.Call<UserSessionInfo>(path + "getSessionInfo", new { sessionID =sessionID}).UserName;
        }

       

        public static void Connect(string url, string UserName, string Password)
        {
            PowerGeez = new PowerGeezMapper();
            //ApplicationServer = (IApplicationServer) RemotingServices.Connect(typeof(IApplicationServer), url + "/AppServer");
            try
            {
                path = url + "/api/app/server/";
                //SessionID = ApplicationServer.CreateUserSession(UserName, Password, Environment.MachineName);
                SessionID=RESTAPIClient.Call<String>(path+ "CreateUserSession", new { UserName, Password, Source = Environment.MachineName });
                Server = url;
                ApplicationClient.UserName = UserName;
                ApplicationClient.Password = Password;
            }
            catch (Exception exception)
            {
                //ApplicationServer = null;
                SessionID = null;
                throw exception;
            }
        }

        

        public static void Disconnect()
        {
            if(keepAliveThread!=null && keepAliveThread.IsAlive)
            {
                enableKeepAliveThread=false;
                keepAliveThread.Join();
            }

            //ApplicationServer.CloseSession(SessionID);
            RESTAPIClient.Call<VoidRet>(path + "CloseSession", new { SessionID=SessionID });
        }
        public static void ChangePasssword(string userName, string oldPassword, string newPassword)
        {
            
        }
        public static UserSessionInfo[] GetActiveSessions()
        {
            //return ApplicationServer.GetActiveSessions();
            return RESTAPIClient.Call<UserSessionInfo[] >(path + "GetActiveSessions", new {  });
        }

        public static bool isConnected()
        {
            return SessionID != null;
        }
        public static void pingServer(string url)
        {
            RESTAPIClient.Call<VoidRet>(path + "pingServer", new { });
        }
        public static void reConnect()
        {
            Connect(ApplicationClient.Server, ApplicationClient.UserName, ApplicationClient.Password);
        }
        static InstantMessageForm msgForm;
        public static void startInstantMessageClient()
        {
            msgForm = new InstantMessageForm();
            new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate()
                {
                    while (true)
                    {
                        //InstantMessageData[] data = ApplicationServer.getInstantMessages(ApplicationClient.SessionID);
                        InstantMessageData[] data = RESTAPIClient.Call<InstantMessageData[]>(path + "getInstantMessages", new { sessionID=ApplicationClient.SessionID });
                        if (data.Length != 0)
                        {
                            INTAPS.UI.UIFormApplicationBase.runInUIThread
                                (new INTAPS.UI.HTML.ProcessParameterless(
                                    delegate()
                                    {
                                        msgForm.showMessages(data);
                                    }));
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                )).Start();
        }
        public static void showMessageSender()
        {
            if(msgForm!=null)
                msgForm.showSendMessage();
        }
        public static string sendInstantMessage(InstantMessageData msg)
        {
            msg.dateSent = DateTime.Now;
            msg.from = ApplicationClient.UserName;
            //return ApplicationServer.sendMessage(ApplicationClient.SessionID, msg);
            return RESTAPIClient.Call<string>(path + "sendMessage", new { sessionID=ApplicationClient.SessionID, msg=msg });
        }
        public static void setMessageRead(string msgID)
        {
            //ApplicationServer.setRead(ApplicationClient.SessionID,new string[]{msgID});
            RESTAPIClient.Call<VoidRet>(path + "setRead", new { sessionID=ApplicationClient.SessionID, messageIDs=new string[]{msgID} });
        }


        public static TLicense.LicenseData getLicenseData()
        {
            //return ApplicationServer.getLicenseData();
            return RESTAPIClient.Call<TLicense.LicenseData>(path + "getLicenseData", new {  });
        }
    }
}

