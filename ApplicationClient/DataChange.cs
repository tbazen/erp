namespace INTAPS.ClientServer.Client
{
    using System;

    public enum DataChange
    {
        Create,
        Delete,
        Update
    }
}

