namespace INTAPS.ClientServer.Client
{

    using INTAPS.ClientServer;
    using System;
    using System.Data;
    using System.Runtime.InteropServices;
    using System.Runtime.Remoting;

    public class SecurityClient
    {
        //private static ISecurityService SecurityServer;
        static String path;

        public static void ChangeClass(int ObjectID, SecurityObjectClass Class)
        {
            //SecurityServer.ChangeClass(ApplicationClient.SessionID, ObjectID, Class);
            RESTAPIClient.Call<VoidRet>(path + "ChangeClass", new { SessionID=ApplicationClient.SessionID, ObjectID=ObjectID, Class=Class });
        }

        public static void ChangeOwner(int ObjectID, int NewOwner)
        {
            //SecurityServer.ChangeOwner(ApplicationClient.SessionID, ObjectID, NewOwner);
            RESTAPIClient.Call<VoidRet>(path + "ChangeOwner", new { SessionID=ApplicationClient.SessionID, ObjectID=ObjectID, NewOwner=NewOwner });
        }

        public static void ChangePassword(string newpassword)
        {
            //SecurityServer.ChangePassword(ApplicationClient.SessionID, newpassword);
            RESTAPIClient.Call<VoidRet>(path + "ChangePassword", new { SessionID=ApplicationClient.SessionID, newpassword=newpassword });
        }

        public static void ChangePassword(string UserName, string newpassword)
        {
            //SecurityServer.ChangePassword(ApplicationClient.SessionID, UserName, newpassword);
            RESTAPIClient.Call<VoidRet>(path + "ChangePassword2", new { SessionID=ApplicationClient.SessionID, UserName=UserName, newpassword=newpassword });
        }

        public static void Connect(string url)
        {
            path = url + "/api/app/security/";
            //SecurityServer = (ISecurityService) RemotingServices.Connect(typeof(ISecurityService), url + "/SecurityServer");
        }

        public static int CreateObject(int ParentOID, string Name)
        {
            //return SecurityServer.CreateObject(ApplicationClient.SessionID, ParentOID, Name);
            return RESTAPIClient.Call<int>(path + "CreateObject", new { SessionID=ApplicationClient.SessionID, ParentOID=ParentOID, Name=Name });
        }

        public static void CreateUser(string ParentUser, string UserName, string Password)
        {
            //SecurityServer.CreateUser(ApplicationClient.SessionID, ParentUser, UserName, );
            RESTAPIClient.Call<VoidRet>(path + "CreateUser", new { SessionID=ApplicationClient.SessionID, ParentUser=ParentUser, UserName=UserName, Password=Password });
        }

        public static void DeleteObject(int ObjectID)
        {
            //SecurityServer.DeleteObject(ApplicationClient.SessionID, ObjectID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteObject", new { SessionID=ApplicationClient.SessionID, ObjectID=ObjectID });
        }

        public static void DeleteUser(string UserName)
        {
            //SecurityServer.DeleteUser(ApplicationClient.SessionID, UserName);
            RESTAPIClient.Call<VoidRet>(path + "DeleteUser", new { SessionID=ApplicationClient.SessionID, UserName=UserName });
        }

        public static string[] GetAllAuditOperations()
        {
            //return SecurityServer.GetAllAuditOperations(ApplicationClient.SessionID);
            return RESTAPIClient.Call<string[]>(path + "GetAllAuditOperations", new { SessionID= ApplicationClient.SessionID });
        }

        public static string[] GetAllUsers()
        {
            //return SecurityServer.GetAllUsers(ApplicationClient.SessionID);
            return RESTAPIClient.Call<string[]>(path + "GetAllUsers", new { SessionID=ApplicationClient.SessionID });
        }
        public class GetAuditOut
        {
            public int NRecords;
            public DataTable _ret;
        }
        public static DataTable GetAudit(string[] AuditType, string[] Users, bool Date, DateTime from, bool DateTo, DateTime to, string[] Data1, int index, int PageSize, out int NRecords)
        {
            //return SecurityServer.GetAudit(ApplicationClient.SessionID, AuditType, Users, Date, from, DateTo, to, Data1, index, PageSize, out NRecords);
            var _ret= RESTAPIClient.Call<GetAuditOut>(path + "GetAudit", new { SessionID= ApplicationClient.SessionID, AuditType=AuditType, Users=Users, Date=Date, from=from, DateTo=DateTo, to=to, Data1=Data1,index=index, PageSize=PageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static string GetAuditData2(int AuditID)
        {
            //return SecurityServer.GetAuditData2(ApplicationClient.SessionID, AuditID);
            return RESTAPIClient.Call<string>(path + "GetAuditData2", new { SessionID=ApplicationClient.SessionID, AuditID=AuditID });
        }

        public static SecurityObjectData[] GetChildList(int ObjectID)
        {
            //return SecurityServer.GetChildList(ApplicationClient.SessionID, ObjectID);
            return RESTAPIClient.Call<SecurityObjectData[]>(path + "GetChildList", new { SessionID= ApplicationClient.SessionID, ObjectID=ObjectID });
        }

        public static string[] GetChildUsers(string User)
        {
            //return SecurityServer.GetChildUsers(ApplicationClient.SessionID, User);
            return RESTAPIClient.Call<string[]>(path + "GetChildUsers", new { SessionID=ApplicationClient.SessionID, User= User});
        }

        public static string[] GetDeniedEntries(int ObjectID)
        {
            //return SecurityServer.GetDeniedEntries(ApplicationClient.SessionID, ObjectID);
            return RESTAPIClient.Call<string[]>(path + "GetDeniedEntries", new { SessionID= ApplicationClient.SessionID, ObjectID=ObjectID });
        }

        public static string[] GetDeniedUsers(int ObjectID)
        {
            //return SecurityServer.GetDeniedUsers(ApplicationClient.SessionID, );
            return RESTAPIClient.Call<string[]>(path + "GetDeniedUsers", new { SessionID=ApplicationClient.SessionID, ObjectID=ObjectID });
        }

        public static string GetNameForID(int UserID)
        {
            //return SecurityServer.GetNameForID(ApplicationClient.SessionID, UserID);
            return RESTAPIClient.Call<string>(path + "GetNameForID", new { SessionID=ApplicationClient.SessionID, UserID= UserID });
        }

        public static SecurityObjectData GetObject(int ObjectID)
        {
            //return SecurityServer.GetObject(ApplicationClient.SessionID, ObjectID);
            return RESTAPIClient.Call<SecurityObjectData>(path + "GetObject", new { SessionID=ApplicationClient.SessionID, ObjectID=ObjectID });
        }

        public static string GetObjectFullName(int OID)
        {
            //return SecurityServer.GetObjectFullName(ApplicationClient.SessionID, OID);
            return RESTAPIClient.Call<string>(path + "GetObjectFullName", new { SessionID=ApplicationClient.SessionID, OID=OID });
        }

        public static int GetObjectID(string Name)
        {
            //return SecurityServer.GetObjectID(ApplicationClient.SessionID, Name);
            return RESTAPIClient.Call<int>(path + "GetObjectID", new { SessionID= ApplicationClient.SessionID, Name=Name });
        }

        public static string GetParentUser(string UserName)
        {
            //return SecurityServer.GetParentUser(ApplicationClient.SessionID, UserName);
            return RESTAPIClient.Call<string>(path + "GetParentUser", new { SessionID= ApplicationClient.SessionID, UserName= UserName });
        }

        public static string[] GetPermitedUsers(int ObjectID)
        {
            //return SecurityServer.GetPermitedUsers(ApplicationClient.SessionID, ObjectID);
            return RESTAPIClient.Call<string[]>(path + "GetPermitedUsers", new { SessionID=ApplicationClient.SessionID, ObjectID= ObjectID });
        }

        public static string[] GetPermittedEntries(int ObjectID)
        {
            //return SecurityServer.GetPermittedEntries(ApplicationClient.SessionID, ObjectID);
            return RESTAPIClient.Call<string[]>(path + "GetPermittedEntries", new { SessionID=ApplicationClient.SessionID, ObjectID= ObjectID });
        }

        public static int GetUIDForName(string Name)
        {
            //return SecurityServer.GetUIDForName(ApplicationClient.SessionID, Name);
            return RESTAPIClient.Call<int>(path + "GetUIDForName", new { SessionID=ApplicationClient.SessionID, Name= Name });
        }

        public static UserPermissionsData GetUserPermissions(int UserID)
        {
            //return SecurityServer.GetUserPermissions(ApplicationClient.SessionID, UserID);
            return RESTAPIClient.Call<UserPermissionsData>(path + "GetUserPermissions", new { SessionID=ApplicationClient.SessionID, UserID= UserID });
        }

        public static UserPermissionsData GetUserPermissions(string userName, string password)
        {
            //return SecurityServer.GetUserPermissions(ApplicationClient.SessionID, userName, password);
            return RESTAPIClient.Call<UserPermissionsData>(path + "GetUserPermissions2", new { SessionID= ApplicationClient.SessionID, userName= userName, password= password });
        }

        public static void Rename(int ObjectID, string NewName)
        {
            //SecurityServer.Rename(ApplicationClient.SessionID, ObjectID, );
            RESTAPIClient.Call<VoidRet>(path + "Rename", new { SessionID= ApplicationClient.SessionID, ObjectID=ObjectID, NewName=NewName });
        }

        public static void SetPermission(int ObjectID, string UserName, PermissionState state)
        {
            //SecurityServer.SetPermission(ApplicationClient.SessionID, ObjectID, UserName, state);
            RESTAPIClient.Call<VoidRet>(path + "SetPermission", new { SessionID=ApplicationClient.SessionID, ObjectID=ObjectID, UserName= UserName, state=state });
        }

        public static UserPermissionsData User(string user)
        {
            //return SecurityServer.User(ApplicationClient.SessionID, user);
            return RESTAPIClient.Call<UserPermissionsData>(path + "User", new { SessionID=ApplicationClient.SessionID, user=user });
        }
        public static bool IsPermited(string obj)
        {
            //return SecurityServer.IsPermited(ApplicationClient.SessionID, obj);
            return RESTAPIClient.Call<bool>(path + "IsPermited", new { SessionID=ApplicationClient.SessionID, obj= obj });
        }
    }
}

