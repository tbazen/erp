namespace INTAPS.ClientServer
{
    using INTAPS.RDBMS;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Reflection;

    public class ApplicationServer
    {
        class BDERecord
        {
            public String name;
            public object bde = null;
            public String readDB;
            public String connectionStr;
            internal string typeName;
        }
        private static Dictionary<string, BDERecord> BDEs;
        private static Dictionary<Type, BDERecord> BDEByType;
        private static SecurityProvider bdeSecurity;
        public static IEventLoger EventLoger;
        private static Dictionary<string, UserSessionData> Sessions;
        public static string TempPathLocal = @"c:\";
        public static string TempPathWeb = @"c:\";
        private static SQLHelper writerHelper;
        public static int languageID = 0;

        public static void CloseSession(string SessionID)
        {
            lock (Sessions)
            {
                if (!Sessions.ContainsKey(SessionID))
                    throw new ServerUserMessage("Session doesn't exist or expired");
                try
                {
                    UserSessionData sd = Sessions[SessionID];
                    string UserName = sd.Permissions.Name;
                    lock (Sessions)
                    {
                        Sessions.Remove(SessionID);
                        foreach (KeyValuePair<Type, object> kv in sd.SessionObjects)
                        {
                            MethodInfo mi = kv.Value.GetType().GetMethod("closeSession");
                            if (mi != null)
                                mi.Invoke(kv.Value, new object[0]);
                        }
                    }
                    EventLoger.Log(UserName + " disconnected");
                }
                catch (Exception ex)
                {
                    EventLoger.LogException("Failed.." + SessionID + " disconnecting", ex);
                    throw ex;
                }
            }
        }
        public static Dictionary<string, List<DateTime>> failedLoginAttempts = new Dictionary<string, List<DateTime>>();
        public static string CreateUserSession(string UserName, string Password, string Source)
        {
            DateTime attemptTime = DateTime.Now;
            string UserNameUpper = UserName.ToUpper();
            if (failedLoginAttempts.ContainsKey(UserNameUpper))
            {
                List<DateTime> times = failedLoginAttempts[UserNameUpper];
                if (times.Count / 3 > 0)
                {
                    int n = times.Count / 3;
                    if (attemptTime.Subtract(times[times.Count - 1]).TotalSeconds < 30 * n)
                    {
                        throw new ServerUserMessage("You will need to wait for {0} seconds because you have made three attempts to login with wrong password", Math.Round(30 * n - attemptTime.Subtract(times[times.Count - 1]).TotalSeconds));
                    }
                }
            }

            try
            {

                EventLoger.Log(UserName + " connecting..");
                SecurityProvider.UserPermissions perm = bdeSecurity.GetUserPermmissions(UserName, Password);
                UserSessionData d = new UserSessionData
                {
                    CreateTime = DateTime.Now,
                    LastAction = DateTime.Now,
                    Permissions = perm,
                    SessionObjects = new Dictionary<Type, object>(),
                    Source = Source
                };
                if (failedLoginAttempts.ContainsKey(UserNameUpper))
                    failedLoginAttempts.Remove(UserNameUpper);
                if (singleLogin)
                {
                    foreach (UserSessionData usd in Sessions.Values)
                    {
                        if (usd.Permissions.UserName.Equals(UserName, StringComparison.CurrentCultureIgnoreCase))
                            throw new ServerUserMessage("You have already loged in on another computer");
                    }
                }

                lock (Sessions)
                {
                    string SessionID = Guid.NewGuid().ToString();
                    Sessions.Add(SessionID, d);
                    EventLoger.Log("Done.." + UserName + " connecting sid:" + SessionID);
                    return SessionID;
                }
            }
            catch (Exception ex)
            {
                if (failedLoginAttempts.ContainsKey(UserNameUpper))
                {
                    failedLoginAttempts[UserNameUpper].Add(attemptTime);
                }
                else
                {
                    failedLoginAttempts.Add(UserNameUpper, new List<DateTime>(new DateTime[] { attemptTime }));
                }
                EventLoger.LogException("Failed.." + UserName + " connecting", ex);
                throw;
            }
        }
        public static UserSessionInfo[] GetActiveSessions()
        {
            UserSessionInfo[] ret = new UserSessionInfo[Sessions.Count];
            int i = 0;
            foreach (KeyValuePair<string, UserSessionData> kv in Sessions)
            {
                UserSessionInfo si = new UserSessionInfo
                {
                    SessionID = kv.Key,
                    IdleSeconds = kv.Value.IdleTime,
                    LogedInTime = kv.Value.CreateTime,
                    Source = kv.Value.Source,
                    UserName = kv.Value.Permissions.UserName,
                    UserID = kv.Value.Permissions.UserID,
                };
                ret[i++] = si;
            }
            return ret;
        }
        public static object GetBDE(string Name)
        {
            lock (BDEs)
            {
                var rec = BDEs[Name];
                if (rec.bde == null)
                {
                    var t = Type.GetType(rec.typeName);
                    if (t == null)
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, "Failed to load BDE " + rec.typeName);
                        return null;
                    }
                    instantiateBDE(t, rec);
                }
                return rec.bde;
            }
        }
        public static T GetBDE<T>()
        {
            lock (BDEs)
            {
                var found = false;
                if (!BDEByType.ContainsKey(typeof(T)))
                {
                    foreach (var r in BDEs.Values)
                    {
                        var t = Type.GetType(r.typeName);
                        if (t == typeof(T))
                        {
                            instantiateBDE(t, r);
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, $"Failed to load BDE of type {typeof(T)} not found");
                        return default(T);
                    }
                }
                return (T)BDEByType[typeof(T)].bde;
            }
        }


        static void sessionExpiryCheck()
        {
            while (!shuttingDown)
            {
                System.Threading.Thread.Sleep(sessionExpirySeconds * 1000);
                lock (Sessions)
                {
                    List<string> expired = null;
                    foreach (KeyValuePair<string, UserSessionData> kv in Sessions)
                    {
                        if (kv.Value.IdleTime > sessionExpirySeconds)
                        {
                            if (expired == null)
                                expired = new List<string>();
                            expired.Add(kv.Key);
                        }
                    }
                    if (expired != null)
                        foreach (string e in expired)
                        {
                            Console.WriteLine("Session {0} expired", e);
                            Sessions.Remove(e);
                        }
                }
            }
            Console.WriteLine("Session expiry loop exited");
        }
        public static void RenewSession(string SessionID)
        {
            if (SessionID == null || !Sessions.ContainsKey(SessionID))
            {
                throw new SessionExpiredException("GetSessionObject: session doesn't exist, it might have expired.");
            }
            lock (Sessions)
            {
                UserSessionData usd = Sessions[SessionID];
                usd.LastAction = DateTime.Now;
            }
        }
        public static object GetSessionObject(string SessionID, Type ServiceObjectType)
        {
            if (SessionID == null || !Sessions.ContainsKey(SessionID))
            {
                throw new SessionExpiredException("GetSessionObject: session doesn't exist, it might have expired.");
            }
            lock (Sessions)
            {
                object ret;
                UserSessionData usd = Sessions[SessionID];
                usd.LastAction = DateTime.Now;
                if (!usd.SessionObjects.ContainsKey(ServiceObjectType))
                {
                    EventLoger.Log(EventLogType.ServerActions, "Creating session " + ServiceObjectType);
                    try
                    {
                        ConstructorInfo ci = ServiceObjectType.GetConstructor(new Type[] { typeof(UserSessionData) });
                        if (ci == null)
                        {
                            throw new Exception("Constructor for server type " + ServiceObjectType + " not found");
                        }
                        ret = ci.Invoke(new object[] { usd });
                        usd.SessionObjects.Add(ServiceObjectType, ret);
                        EventLoger.Log(EventLogType.ServerActions, "Creating session " + ServiceObjectType + " done.");
                    }
                    catch (Exception ex)
                    {
                        EventLoger.LogException("Failed to create session " + ServiceObjectType, ex);
                        throw;
                    }
                }
                else
                {
                    ret = usd.SessionObjects[ServiceObjectType];
                }
                return ret;
            }
        }
        public static void testConnetionString(string conStr, string name)
        {

            INTAPS.RDBMS.SQLHelper h = new SQLHelper(conStr);
            int retry = 0;
            do
            {
                try
                {
                    ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Testing connection " + name
                        + (retry > 0 ? " retry " + retry : ""));
                    h.getOpenConnection();
                    ApplicationServer.EventLoger.Log(EventLogType.ServerActions, name + " connection ok");
                    h.CloseConnection();
                    return;
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.Log(EventLogType.Errors, "Test connection " + name + " failed with error\n" + ex.Message + "\nRetrying in 5 seconds");
                    Console.WriteLine();
                    int timeLeft = 5;
                    while (timeLeft > 0)
                    {
                        //Console.CursorTop--;
                        Console.WriteLine(timeLeft + "              ");
                        System.Threading.Thread.Sleep(1000);
                        timeLeft--;
                    }
                    //Console.CursorTop--;
                    retry++;
                }
            } while (!shuttingDown);

        }
        static int sessionExpirySeconds;
        static bool singleLogin;
        public static void Initialize(string[][] BDETypes, IEventLoger el, int sessionExpirySeconds, bool singleLogin)
        {
            ApplicationServer.sessionExpirySeconds = sessionExpirySeconds;
            ApplicationServer.singleLogin = singleLogin;
            if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["languageID"], out languageID))
                languageID = 0;

            EventLoger = el;
            EventLoger.Log(EventLogType.ServerActions, "Initializing autoincrement list..");
            string mainConStr = ConfigurationManager.ConnectionStrings["App"].ConnectionString;
            string mainDB = MainDB;
            testConnetionString(mainConStr, "Main");
            writerHelper = new SQLHelper(mainConStr);
            try
            {
                AutoIncrement.Initialize(writerHelper);
                EventLoger.Log(EventLogType.ServerActions, "Initializing autincrement list completed successfully.");
            }
            catch (Exception ex)
            {
                EventLoger.LogException("Failed to initialize autoincrement list.", ex);
            }
            EventLoger.Log(EventLogType.ServerActions, "Initializing change filters..");
            try
            {
                ObjectFilters.Initialize();
                EventLoger.Log(EventLogType.ServerActions, "Initializing change filters completed successfully.");
            }
            catch (Exception ex)
            {
                EventLoger.LogException("Failed to Initializing change filters.", ex);
            }
            EventLoger.Log(EventLogType.ServerActions, "Initializing security..");
            try
            {
                bdeSecurity = new SecurityProvider("Security", mainDB, writerHelper, mainConStr);
                EventLoger.Log(EventLogType.ServerActions, "Initializing security completed successfully.");
            }
            catch (Exception ex)
            {
                EventLoger.LogException("Failed to initialize security.", ex);
            }
            EventLoger.Log("Loading BDEs..");
            try
            {
                LoadBDE(BDETypes);
                EventLoger.Log("Done Loading BDEs..");
            }
            catch (Exception ex)
            {
                EventLoger.LogException("Failed..Loading BDEs.", ex);
            }

            EventLoger.Log("Loading Offline Sync Framework..");
            try
            {
                initMessageRepo();
                EventLoger.Log("Done Loading Offline Sync Framework");
            }
            catch (Exception ex)
            {
                EventLoger.LogException("Failed..Loading Offline Sync Framework.", ex);
            }
            EventLoger.Log("Initializing session framework..");
            try
            {
                InitSessionFramework();
                EventLoger.Log("Done..Initializing session framework..");
            }
            catch (Exception ex)
            {
                EventLoger.LogException("Failed..Initializing session framework.", ex);
            }
        }


        private static void InitSessionFramework()
        {
            Sessions = new Dictionary<string, UserSessionData>();
            new System.Threading.Thread(new System.Threading.ThreadStart(sessionExpiryCheck)).Start();
        }

        private static void LoadBDE(string[][] BDETypes)
        {
            BDEs = new Dictionary<string, BDERecord>();
            BDEByType = new Dictionary<Type, BDERecord>();
            for (int i = 0; i < BDETypes.Length; i++)
            {

                ConnectionStringSettings conStg = ConfigurationManager.ConnectionStrings[BDETypes[i][0] + "DB"];
                if (conStg == null)
                    throw new ServerUserMessage("Connection setting with key {0} expected but not found", BDETypes[i][0] + "DB");
                string db = conStg.ConnectionString;
                conStg = ConfigurationManager.ConnectionStrings[BDETypes[i][0] + "RO"];
                if (conStg == null)
                    throw new ServerUserMessage("Connection setting with key {0} expected but not found", BDETypes[i][0] + "RO");
                string cro = conStg.ConnectionString;


                var rec = new BDERecord() { bde = null, name = BDETypes[i][0], readDB = db, connectionStr = cro, typeName = BDETypes[i][1] };
                BDEs.Add(BDETypes[i][0], rec);


            }
        }
        static void instantiateBDE(Type t, BDERecord rec)
        {
            EventLoger.Log("Loading BDE " + t.Name);
            ConstructorInfo ci = t.GetConstructor(new Type[] { typeof(string), typeof(string), typeof(SQLHelper), typeof(string) });
            if (ci == null)
            {
                ConstructorInfo[] allcs = t.GetConstructors();
                throw new Exception("Couldn't find constructor for " + t.Name);
            }
            EventLoger.Log("Loading BDE " + t.Name + " .. Done");
            object bde = ci.Invoke(new object[] { rec.name, rec.readDB, writerHelper, rec.connectionStr });
            ObjectFilters.RegisterFilters(t.Assembly);
            rec.bde = bde;
            BDEByType.Add(t, rec);
        }
        private static void loadObjectFilters()
        {
        }
        static bool shuttingDown = false;
        public static void ShutDown()
        {
            ApplicationServer.shuttingDown = true;
            //Process.GetCurrentProcess().Kill();
        }

        public static string MainDB
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["AppDB"].ConnectionString;
            }
        }

        public static SecurityProvider SecurityBDE
        {
            get
            {
                return bdeSecurity;
            }
        }

        public static bool isAlive(string sessionID)
        {
            lock (Sessions)
            {
                return Sessions.ContainsKey(sessionID);
            }
        }

        static MessageRepository _msgRepo;
        static void initMessageRepo()
        {
            _msgRepo = new MessageRepository(SecurityBDE, false);
        }
        public static void sendDiffMsg(int AID, GenericDiff diff)
        {
            OfflineMessage msg = new OfflineMessage();
            msg.messageData = diff;
            msg.sentDate = DateTime.Now;
            msg.delivered = false;
            _msgRepo.addMessage(AID, msg);
        }
        public static MessageRepository MsgRepo
        {
            get
            {
                return _msgRepo;
            }
        }
        public static UserSessionInfo getSessionInfo(string sessionID)
        {
            if (Sessions.ContainsKey(sessionID))
            {
                UserSessionData d = Sessions[sessionID];
                return new UserSessionInfo
                {
                    SessionID = sessionID,
                    IdleSeconds = d.IdleTime,
                    LogedInTime = d.CreateTime,
                    Source = d.Source,
                    UserName = d.Permissions.UserName,
                    UserID = d.Permissions.UserID,
                };
            }
            return null;
        }
        public static List<UserSessionInfo> getSessionInfoByUserID(string userID)
        {
            List<UserSessionInfo> ret = new List<UserSessionInfo>();
            foreach (KeyValuePair<string, UserSessionData> kv in Sessions)
            {
                if (kv.Value.Permissions.UserName.Equals(userID, StringComparison.CurrentCultureIgnoreCase))
                    ret.Add(new UserSessionInfo
                    {
                        SessionID = kv.Key,
                        IdleSeconds = kv.Value.IdleTime,
                        LogedInTime = kv.Value.CreateTime,
                        Source = kv.Value.Source,
                        UserName = kv.Value.Permissions.UserName,
                        UserID = kv.Value.Permissions.UserID,
                    });
            }
            return ret;
        }
        public static string getSessionID(UserSessionData user)
        {
            foreach (KeyValuePair<string, UserSessionData> kv in Sessions)
            {
                if (kv.Value == user)
                {
                    return kv.Key;
                }
            }
            return null;
        }
        static Dictionary<string, Dictionary<string, InstantMessageData>> messageData = new Dictionary<string, Dictionary<string, InstantMessageData>>();
        public static InstantMessageData[] getInstantMessages(string sessionID)
        {
            lock (messageData)
            {
                if (messageData.ContainsKey(sessionID))
                {
                    Dictionary<string, InstantMessageData> messages = messageData[sessionID];
                    InstantMessageData[] ret = new InstantMessageData[messages.Count];
                    messages.Values.CopyTo(ret, 0);
                    Array.Sort(ret, (x, y) => x.dateSent.CompareTo(y.dateSent));
                    return ret;
                }
                return new InstantMessageData[0];
            }
        }
        public static void setRead(string sessionID, string[] messageIDs)
        {
            lock (messageData)
            {
                if (messageData.ContainsKey(sessionID))
                {
                    Dictionary<string, InstantMessageData> messages = messageData[sessionID];
                    foreach (string msgID in messageIDs)
                        messages.Remove(msgID);
                }
            }
        }
        public static string sendMessage(string sessionID, InstantMessageData msg)
        {
            lock (messageData)
            {
                msg.from = getSessionInfo(sessionID).UserName;
                msg.messageID = Guid.NewGuid().ToString();
                List<UserSessionInfo> sessions = getSessionInfoByUserID(msg.to);
                if (sessions.Count == 0)
                    throw new ServerUserMessage("User {0} not logedin", msg.to);
                foreach (UserSessionInfo s in sessions)
                {
                    Dictionary<string, InstantMessageData> messages;
                    if (!messageData.TryGetValue(s.SessionID, out messages))
                    {
                        messageData.Add(s.SessionID, messages = new Dictionary<string, InstantMessageData>());
                    }
                    messages.Add(msg.messageID, msg);
                }
                return msg.messageID;
            }
        }
    }
}