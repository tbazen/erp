namespace INTAPS.ClientServer
{
    using System;
    using System.Collections;
    using System.Data.SqlClient;

    public class AuditBase
    {
        protected SqlConnection m_con;
        private SqlTransaction m_Transaction;
        protected SecurityProvider.UserPermissions m_user;

        public AuditBase(SecurityProvider.UserPermissions user, SqlConnection con)
        {
            this.m_con = con;
            this.m_user = user;
        }

        public long Audit()
        {
            long AuditID = AutoIncrement.GetKey("AuditID");
            new SqlCommand(string.Concat(new object[] { "Insert into AuditBase (ID,TimeStamp,UserID,UserName) Values (", AuditID, ",'", DateTime.Now, "',", this.m_user.UID, ",'", this.m_user.Name, "')" }), this.m_con).ExecuteNonQuery();
            return AuditID;
        }

        public void BeginTransaction()
        {
            this.m_Transaction = this.m_con.BeginTransaction();
        }

        public void CommitTranscation()
        {
            this.m_Transaction.Commit();
        }

        protected AuditLogItem[] GetBaseLog(string Criteria)
        {
            ArrayList ret = new ArrayList();
            SqlDataReader reader = new SqlCommand("Select ID,TimeStamp,UserID,UserName from AuditBase where " + Criteria).ExecuteReader();
            while (reader.Read())
            {
                AuditLogItem li = new AuditLogItem {
                    ID = (long) reader["ID"],
                    UID = (long) reader["UID"],
                    UserName = (string) reader["UID"],
                    TimeStamp = (DateTime) reader["TimeStamp"]
                };
                ret.Add(li);
            }
            AuditLogItem[] r = new AuditLogItem[ret.Count];
            ret.CopyTo(r);
            return r;
        }

        public void Rollback()
        {
            this.m_Transaction.Rollback();
        }
    }
}

