namespace INTAPS.ClientServer
{
    using System;
    using System.Data.SqlClient;

    public class AuditCommand : AuditBase
    {
        public AuditCommand(SecurityProvider.UserPermissions user, SqlConnection con) : base(user, con)
        {
        }

        public void Audit(string CommandName, string Data1, string Data2)
        {
            long AuditID = base.Audit();
            new SqlCommand(string.Concat(new object[] { "Insert into AuditCommand (AuditID,Command,Data1,Data2) Values (", AuditID, ",'", CommandName, "','", Data1, "','", Data2, "')" }), base.m_con).ExecuteNonQuery();
        }
    }
}

