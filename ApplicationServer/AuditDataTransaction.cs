namespace INTAPS.ClientServer
{
    using System;
    using System.Data.SqlClient;

    public class AuditDataTransaction : AuditBase
    {
        private AuditCommand m_ac;
        private SqlConnection m_DataCon;

        public AuditDataTransaction(SqlConnection DataCon, SecurityProvider.UserPermissions user, SqlConnection con) : base(user, con)
        {
            this.m_DataCon = DataCon;
            this.m_ac = new AuditCommand(user, con);
        }

        public long AuditAddNew(string TableName, string RowKey)
        {
            long AuditID = base.Audit();
            this.m_ac.Audit("AddNewRow", TableName, RowKey);
            return AuditID;
        }

        public long AuditDelete(string TableName, string RowKey)
        {
            long AuditID = base.Audit();
            this.m_ac.Audit("DeleteRow", TableName, RowKey);
            return AuditID;
        }

        public long AuditUpdate(string SelectSql, string RowKey)
        {
            long AuditID = base.Audit();
            SqlDataReader reader = new SqlCommand(SelectSql, this.m_DataCon).ExecuteReader();
            bool empty = reader.Read();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string OV;
                string Field = reader.GetName(i);
                if (!empty)
                {
                    OV = "NULL";
                }
                else if (reader.IsDBNull(i))
                {
                    OV = "NULL";
                }
                else
                {
                    OV = "''" + reader[i].ToString() + "''";
                }
                new SqlCommand(string.Concat(new object[] { "Insert into AuditFieldUpdate (AuditID,RowKey ,FieldName,OldValue) Values (", AuditID, ",'", RowKey, "','", Field, "','", OV, "')" }), base.m_con).ExecuteNonQuery();
            }
            reader.Close();
            return AuditID;
        }

        public AuditLogItem[] GetLog(string TableName)
        {
            return null;
        }

        public SqlConnection DataConnection
        {
            set
            {
                this.m_DataCon = value;
            }
        }
    }
}

