namespace INTAPS.ClientServer
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct AuditLogItem
    {
        public long ID;
        public DateTime TimeStamp;
        public long UID;
        public string UserName;
        public string Data;
    }
}

