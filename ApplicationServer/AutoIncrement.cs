namespace INTAPS.ClientServer
{
    using INTAPS.RDBMS;
    using System;
    using System.Collections;
    using System.Data.SqlClient;

    public class AutoIncrement
    {
        private static SQLHelper m_helper;
        private static Hashtable m_ht = new Hashtable();

        public static int GetCurrentValue(string Name)
        {
            if (!m_ht.Contains(Name.ToUpper()))
            {
                return 0;
            }
            return (int) m_ht[Name.ToUpper()];
        }

        public static int GetKey(string Name)
        {
            if (!m_ht.Contains(Name.ToUpper()))
            {
                SetSeed(Name, 0, false);
            }
            lock (m_helper)
            {
                m_ht[Name.ToUpper()] = ((int) m_ht[Name.ToUpper()]) + 1;
                int ret = (int) m_ht[Name.ToUpper()];
                m_helper.ExecuteNonQuery(string.Concat(new object[] { "Update AutoIncrementFields  Set LastValue=", ret, " where Name='", Name, "'" }));
                return ret;
            }
        }

        public static void Initialize(SQLHelper helper)
        {
            m_helper = helper;
            SqlDataReader reader = helper.ExecuteReader("Select Name,LastValue from AutoIncrementFields order by Name");
            while (reader.Read())
            {
                m_ht.Add(((string) reader["Name"]).ToUpper(), (int) reader["LastValue"]);
            }
            reader.Close();
        }

        public void LoadSeed(SqlConnection con, string Name, string table, string field, string criteria, bool overide)
        {
            lock (m_helper)
            {
                object res = m_helper.ExecuteScalar("Select Max(" + field + ") from " + table + ((criteria == "") ? "" : (" where " + criteria)));
                if ((res == DBNull.Value) || (res == null))
                {
                    SetSeed(Name, 1, overide);
                }
                else
                {
                    SetSeed(Name, ((int) res) + 1, overide);
                }
            }
        }

        public static void SetSeed(string Name, int seed, bool Overide)
        {
            lock (m_helper)
            {
                if (m_ht.Contains(Name.ToUpper()))
                {
                    if ((((int) m_ht[Name.ToUpper()]) <= seed) || Overide)
                    {
                        m_ht[Name.ToUpper()] = seed;
                    }
                }
                else
                {
                    string sql = string.Concat(new object[] { "Insert into  AutoIncrementFields (Name,LastValue) Values('", Name, "',", seed, ")" });
                    m_helper.ExecuteNonQuery(sql);
                    m_ht.Add(Name.ToUpper(), seed);
                }
            }
        }
    }
}

