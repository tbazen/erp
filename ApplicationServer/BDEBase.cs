namespace INTAPS.ClientServer
{
    using System;
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.Threading;

    public class BDEBase
    {
        protected INTAPS.RDBMS.SQLHelper dspWriter;
        protected string m_BDEName;
        protected string m_DBName;
        protected string readerConStr;

        public BDEBase(string BDEName, string DBName, INTAPS.RDBMS.SQLHelper writer, string ReadOnlyConnectionString)
            : this(BDEName, DBName, writer, ReadOnlyConnectionString, true)
        {
        }
        public BDEBase(string BDEName, string DBName, INTAPS.RDBMS.SQLHelper writer, string ReadOnlyConnectionString,bool testConnection)
        {
            this.m_BDEName = BDEName;
            this.m_DBName = DBName;
            this.readerConStr = ReadOnlyConnectionString;
            this.dspWriter = writer;
            if(testConnection)
                ApplicationServer.testConnetionString(ReadOnlyConnectionString, BDEName);
        }

        public int AddImage(int imageID, int index, string title, byte[] image)
        {
            int var_1_0000;
            lock (this.dspWriter)
            {
                ImageItem img = new ImageItem();
                this.dspWriter.BeginTransaction();
                try
                {
                    if (imageID == -1)
                    {
                        img.imageID = AutoIncrement.GetKey("App.ImageID");
                        img.itemIndex = 1;
                    }
                    else
                    {
                        int count = (int) this.dspWriter.ExecuteScalar(string.Concat(new object[] { "Select count(*) from ", ApplicationServer.MainDB, ".dbo.ImageItem where imageID=", imageID }));
                        if (count == 0)
                        {
                            throw new Exception("Image set doesn't exist in the database:" + imageID);
                        }
                        if ((index > count) || (index < 1))
                        {
                            img.itemIndex = count + 1;
                        }
                        else
                        {
                            img.itemIndex = index;
                            this.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Update ", ApplicationServer.MainDB, ".dbo.ImageItem set itemIndex=itemIndex+1 where itemIndex>=", index }));
                        }
                    }
                    img.imageData = image;
                    img.title = title;
                    this.dspWriter.InsertSingleTableRecord<ImageItem>(ApplicationServer.MainDB, img, new string[] { "data" }, new object[] { img.imageData });
                    this.dspWriter.CommitTransaction();
                    var_1_0000 = img.imageID;
                }
                catch (Exception ex)
                {
                    this.dspWriter.RollBackTransaction();
                    throw new Exception("Error inserting image", ex);
                }
            }
            return var_1_0000;
        }


        public void CheckTransactionIntegrity()
        {
            try
            {
                this.dspWriter.CheckTransactionIntegrity();
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Transaction integrity failure", ex);
                throw ex;
            }
        }


        public static ImageCodecInfo GetencoderInfo(string str)
        {
            ImageCodecInfo[] imgCodecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo imgCodec in imgCodecs)
            {
                if (str == imgCodec.MimeType)
                {
                    return imgCodec;
                }
            }
            return null;
        }

        public INTAPS.RDBMS.SQLHelper GetReaderHelper()
        {
            lock (this.dspWriter)
            {
                if (this.dspWriter.InTransaction)
                {
                    this.dspWriter.setReadDB(this.m_DBName);
                    return this.dspWriter;
                }
            }
            return new INTAPS.RDBMS.SQLHelper(this.readerConStr);
        }

        

        public void ReleaseHelper(INTAPS.RDBMS.SQLHelper h)
        {
            if (h == this.dspWriter)
            {
                h.restoreReadDB();
            }
            else
            {
                h.CloseConnection();
            }
        }


        public void UpateImage(ImageItem item)
        {
            lock (this.dspWriter)
            {
                this.dspWriter.setReadDB(this.DBName);
                try
                {
                    if (this.dspWriter.GetSTRArrayByFilter<ImageItem>(string.Concat(new object[] { "imageID=", item.imageID, " and itemIndex=", item.itemIndex })).Length == 0)
                    {
                        throw new Exception("The image you are trying to update is not in the database");
                    }
                    this.dspWriter.UpdateSingleTableRecord<ImageItem>(ApplicationServer.MainDB, item, new string[] { "data" }, new object[] { item.imageData });
                }
                finally
                {
                    this.dspWriter.restoreReadDB();
                }
            }
        }

        public string BDEName
        {
            get
            {
                return this.m_BDEName;
            }
        }

        public string DBName
        {
            get
            {
                return this.m_DBName;
            }
        }

        public INTAPS.RDBMS.SQLHelper ReaderDSP
        {
            get
            {
                return this.GetReaderHelper();
            }
        }

        public INTAPS.RDBMS.SQLHelper WriterHelper
        {
            get
            {
                return this.dspWriter;
            }
        }
        protected T getSingleObjectIntID<T>(int id) where T : class,new()
        {
            INTAPS.RDBMS.SQLHelper reader = GetReaderHelper();
            try
            {
                T[] ret = reader.GetSTRArrayByFilter<T>("id=" + id);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                ReleaseHelper(reader);
            }
        }
        protected int createSingleOjectIntID<T>(int AID,T o) where T:class,new()
        {
            int id = AutoIncrement.GetKey(this.DBName + "." + typeof(T).Name + "ID");
            typeof(T).GetField("id").SetValue(o, id);
            dspWriter.InsertSingleTableRecord(this.DBName, o, new string[] { "__AID" }, new object[] { AID });
            return id;
        }
        protected void updateSingleOjectIntID<T>(int AID, T o) where T : class,new()
        {
            int id = (int)typeof(T).GetField("id").GetValue(o);
            T old = getSingleObjectIntID<T>(id);
            if (old == null)
                throw new ServerUserMessage("{0} with id {1} doesn't exist", typeof(T).Name, id);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(T).Name,"id="+id);
            dspWriter.UpdateSingleTableRecord<T>(this.DBName, o, new string[] { "__AID" }, new object[] { AID });
        }
        protected void deleteSingleOjectIntID<T>(int AID, int id) where T : class,new()
        {
            T old = getSingleObjectIntID<T>(id);
            if (old == null)
                throw new ServerUserMessage("{0} with id {1} doesn't exist", typeof(T).Name, id);
            dspWriter.logDeletedData(AID, this.DBName + ".dbo." + typeof(T).Name, "id=" + id);
            dspWriter.DeleteSingleTableRecrod<T>(this.DBName, id);
        }
    }
}

