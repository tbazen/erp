namespace INTAPS.ClientServer
{
    using System;

    public enum EventLogType
    {
        ClientActions = 2,
        DBConnections = 16,
        Debug = 8,
        Errors = 4,
        ServerActions = 1,
        SystemInconsistency=32

    }
}

