namespace INTAPS.ClientServer
{
    using System;

    public interface IEventLoger
    {
        void Log(string message);
        void Log(EventLogType lt, string message);
        void LogException(string message, Exception ex);
    }
}

