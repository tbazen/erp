﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace INTAPS.ClientServer
{
    public delegate bool MessageRepositoryParser(OfflineMessage msg);
    public class MessageRepository
    {
        Dictionary<int, OfflineMessage> _cashe;
        public static bool dontDoDiff = false;
        BDEBase _bde;
        public MessageRepository(BDEBase bde,bool cached)
        {
            _bde = bde;
            if (cached)
            {
                _cashe = new Dictionary<int, OfflineMessage>();
                INTAPS.RDBMS.SQLObjectReader<OfflineMessage> reader = null;
                INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
                try
                {
                    reader = dspReader.GetObjectReader<OfflineMessage>(null, new string[] { "messageData" });
                    while (reader.Read())
                    {
                        reader.Current.messageData = INTAPS.RDBMS.SQLHelper.BinaryToObject(reader.CurrentAdditionalData[0] as byte[]);
                        _cashe.Add(reader.Current.id, reader.Current);
                    }
                }
                finally
                {
                    if (reader != null && !reader.IsClosed)
                        reader.Close();
                    _bde.ReleaseHelper(dspReader);
                }
            }
            else
                _cashe = null;
        }
        public int getLastMessageNumber()
        {
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {
                object ret = dspReader.ExecuteScalar(string.Format("Select max(id) from {0}.dbo.OfflineMessage", _bde.DBName));
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }
        public int getLastUndeliveredMessageNumber()
        {
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {
                object ret = dspReader.ExecuteScalar(string.Format("Select max(id) from {0}.dbo.OfflineMessage where delivered=0", _bde.DBName));
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }
        public int getFirstMessageNumber()
        {
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {
                object ret = dspReader.ExecuteScalar(string.Format("Select min(id) from {0}.dbo.OfflineMessage", _bde.DBName));
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }
        public int getFirstUndeliveredMessageNumber()
        {
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {
                object ret = dspReader.ExecuteScalar(string.Format("Select min(id) from {0}.dbo.OfflineMessage where delivered=0", _bde.DBName));
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }
        public int addMessage(int AID,OfflineMessage msg)
        {
            if (dontDoDiff)
                return -1;
            msg.previousMessageID = getLastMessageNumber();
            lock (_bde.WriterHelper)
            {
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    int id = AutoIncrement.GetKey("MessageID");
                    msg.id = id;
                    _bde.WriterHelper.InsertSingleTableRecord(_bde.DBName, msg, new string[] { "messageData", "__AID" }, new object[] {INTAPS.RDBMS.SQLHelper.ObjectToBinary(msg.messageData), AID });
                    if(_cashe!=null)
                        _cashe.Add(msg.id, msg);
                    _bde.WriterHelper.CommitTransaction();
                    return msg.id;
                }
                catch
                {
                    _bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public void acknowledgeMessage(int AID,int messageID, DateTime ackDate)
        {
            lock (_bde.WriterHelper)
            {
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.WriterHelper.Update(_bde.DBName,"OfflineMessage", new string[] { "delivered", "deliveryDate", "__AID" }, new object[] { true, ackDate, AID },"id="+messageID);
                    if (_cashe != null)
                    {
                        if (_cashe.ContainsKey(messageID))
                        {
                            OfflineMessage msg = _cashe[messageID];
                            msg.delivered = true;
                            msg.deliveryDate = ackDate;
                        }
                        else
                            Console.WriteLine("Message Cache: Acknowledge called for not existent message ID: " + messageID);
                    }
                    _bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    _bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public MessageList getMessageList(int from, int to)
        {
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {
                object[][] data;
                OfflineMessage[] ret = dspReader.GetSTRArrayByFilter<OfflineMessage>(string.Format("id>={0} and id<={1}", from, to),new string[]{"messageData"},out data);
                for (int i = 0; i < ret.Length; i++)
                    ret[i].messageData = INTAPS.RDBMS.SQLHelper.BinaryToObject(data[i][0] as byte[]);
                return new MessageList(ret);
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }
        public MessageList getUndeliveredMessageList(int from, int to)
        {
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {
                object[][] data;
                OfflineMessage[] ret = dspReader.GetSTRArrayByFilter<OfflineMessage>(string.Format("id>={0} and id<={1} and delivered=0", from, to), new string[] { "messageData" }, out data);
                for (int i = 0; i < ret.Length; i++)
                    ret[i].messageData = INTAPS.RDBMS.SQLHelper.BinaryToObject(data[i][0] as byte[]);
                return new MessageList(ret);
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }
        public int countMessages(int after)
        {
            if(_cashe!=null)
                return _cashe.Count;
            INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
            try
            {

                object ret = dspReader.ExecuteScalar(string.Format("Select count(id) from {0}.dbo.OfflineMessage where id>{1}", _bde.DBName, after));
                if (ret is int)
                    return (int)ret;
                return -1;
            }
            finally
            {
                _bde.ReleaseHelper(dspReader);
            }
        }

        public void parseRepo(MessageRepositoryParser pareser)
        {
            if (_cashe != null)
            {
                lock (_cashe)
                {
                    foreach (KeyValuePair<int, OfflineMessage> msg in _cashe)
                    {
                        //reader.Current.messageData = INTAPS.RDBMS.SQLHelper.BinaryToObject(reader.CurrentAdditionalData[0] as byte[]);
                        if (!pareser(msg.Value))
                            return;
                    }
                }
            }
            else
            {
                INTAPS.RDBMS.SQLObjectReader<OfflineMessage> reader = null;
                INTAPS.RDBMS.SQLHelper dspReader = _bde.GetReaderHelper();
                try
                {
                    reader = dspReader.GetObjectReader<OfflineMessage>(null, new string[] { "messageData" });
                    while (reader.Read())
                    {
                        reader.Current.messageData = INTAPS.RDBMS.SQLHelper.BinaryToObject(reader.CurrentAdditionalData[0] as byte[]);
                        if (!pareser(reader.Current))
                            return;
                    }
                }
                finally
                {
                    if (reader != null && !reader.IsClosed)
                        reader.Close();
                    _bde.ReleaseHelper(dspReader);
                }
            }
        }

        public void deleteMessage(int id)
        {
            lock (_bde.WriterHelper)
            {
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.WriterHelper.DeleteSingleTableRecrod<OfflineMessage>(_bde.DBName, id);
                    if (_cashe != null)
                        _cashe.Remove(id);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    _bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
    }
}
