namespace INTAPS.ClientServer
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    public class ObjectFilters
    {
        private static Dictionary<Type, List<BDEChangeFilterAttribute>> _cachedFilterTypes = new Dictionary<Type, List<BDEChangeFilterAttribute>>();
        private static List<BDEChangeFilterAttribute> _filters;
        private static List<Assembly> _loadedAssembly = new List<Assembly>();

        public static void filterAdd(int AID, object obj)
        {
            foreach (BDEChangeFilterAttribute fi in GetFilter(obj.GetType()))
            {
                string msg;
                if (!fi.instance.CanCreate(AID, obj, out msg))
                {
                    throw new ServerUserMessage(msg);
                }
            }
        }

        public static void filterDelete(int AID, Type t, params object[] keyValues)
        {
            foreach (BDEChangeFilterAttribute fi in GetFilter(t))
            {
                string msg;
                if (!fi.instance.CanDelete(AID, t, out msg, keyValues))
                {
                    throw new ServerUserMessage(msg);
                }
            }
        }

        public static void filterUpdate(int AID, object obj)
        {
            foreach (BDEChangeFilterAttribute fi in GetFilter(obj.GetType()))
            {
                string msg;
                if (!fi.instance.CanUpdate(AID, obj, out msg))
                {
                    throw new ServerUserMessage(msg);
                }
            }
        }

        public static List<BDEChangeFilterAttribute> GetFilter(Type filterType)
        {
            if (filterType == null)
            {
                return _filters;
            }
            if (_cachedFilterTypes.ContainsKey(filterType))
            {
                return _cachedFilterTypes[filterType];
            }
            List<BDEChangeFilterAttribute> col = new List<BDEChangeFilterAttribute>();
            foreach (BDEChangeFilterAttribute f in _filters)
            {
                if (f.filterType.Length == 0)
                {
                    col.Add(f);
                }
                else
                {
                    foreach (Type ft in f.filterType)
                    {
                        if (filterType.Equals(ft) || filterType.IsSubclassOf(ft))
                        {
                            col.Add(f);
                            break;
                        }
                    }
                }
            }
            return col;
        }

        public static FilterType GetFilterInstance<FilterType>() where FilterType: class
        {
            foreach (BDEChangeFilterAttribute f in _filters)
            {
                if (f.instance is FilterType)
                {
                    return (f.instance as FilterType);
                }
            }
            return default(FilterType);
        }

        public static void Initialize()
        {
            _filters = new List<BDEChangeFilterAttribute>();
        }

        internal static void RegisterFilters(Assembly a)
        {
            if (!_loadedAssembly.Contains(a))
            {
                int count = 0;
                foreach (Type t in a.GetTypes())
                {
                    object[] atrs = t.GetCustomAttributes(typeof(BDEChangeFilterAttribute), false);
                    foreach (BDEChangeFilterAttribute atr in atrs)
                    {
                        _filters.Add(atr);
                        atr.instantiate(t);
                        count++;
                    }
                }
                if (count > 0)
                {
                    ApplicationServer.EventLoger.Log(EventLogType.ServerActions, count + " change filters loaded");
                }
            }
        }
    }
}

