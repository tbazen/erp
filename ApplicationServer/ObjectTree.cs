namespace INTAPS.ClientServer
{
    using INTAPS.RDBMS;
    using System;
    using System.Collections;
    using System.Data.SqlClient;
    using System.Reflection;

    internal class ObjectTree
    {
        public Hashtable m_Objects;
        private SecurityObject m_Root;
        private SecurityProvider Security;

        public ObjectTree(SQLHelper helper, SecurityProvider Secuirty)
        {
            this.Security = Secuirty;
            this.Load(helper);
        }

        public void AddObject(int OID, SecurityObject securityObject)
        {
            this.m_Objects.Add(OID, securityObject);
        }

        public SecurityObject[] GetAllObjects(string Name)
        {
            string[] Names = this.GetTokens(Name);
            SecurityObject[] ret = new SecurityObject[Names.Length];
            SecurityObject o = this.m_Root;
            ret[0] = this.m_Root;
            for (int i = 1; i < Names.Length; i++)
            {
                bool f = false;
                foreach (SecurityObject ch in ret[i - 1].m_childs)
                {
                    if (ch.Name == Names[i])
                    {
                        ret[i] = ch;
                        f = true;
                        break;
                    }
                }
                if (!f)
                {
                    return ret;
                }
            }
            return ret;
        }

        private string[] GetTokens(string path)
        {
            int lp = 0;
            int c = 0;
            while (true)
            {
                int p = path.IndexOf("/", lp);
                if (p == -1)
                {
                    lp = 0;
                    string[] ret = new string[c + 1];
                    for (int i = 0; i < c; i++)
                    {
                        p = path.IndexOf("/", lp);
                        ret[i] = path.Substring(lp, p - lp);
                        lp = p + 1;
                    }
                    ret[c] = path.Substring(lp);
                    return ret;
                }
                c++;
                lp = p + 1;
            }
        }

        private void Load(SQLHelper helper)
        {
            this.m_Root = new SecurityObject();
            this.m_Root.ID = -1;
            this.m_Root.Security = this.Security;
            this.m_Root.Owner = 1;
            this.m_Root.m_Class = SecurityObjectClass.soInherit;
            this.m_Root.Name = "Root";
            this.m_Objects = new Hashtable();
            this.m_Objects.Add(-1, this.m_Root);
            this.LoadChilds(helper, this.m_Root);
        }

        private void LoadChilds(SQLHelper helper, SecurityObject P)
        {
            SqlDataReader r = helper.ExecuteReader("Select Class,Owner,Name,ID from SecObjects where PID=" + P.ID + " order by ID");
            ArrayList chlds = new ArrayList();
            try
            {
                while (r.Read())
                {
                    SecurityObject obj = new SecurityObject {
                        Security = this.Security,
                        Name = (string) r["Name"],
                        ID = (int) r["ID"],
                        m_Parent = P,
                        Owner = (int) r["Owner"],
                        m_Class = (SecurityObjectClass) r["Class"]
                    };
                    chlds.Add(obj);
                    this.m_Objects.Add(obj.ID, obj);
                }
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error loading object tree", ex);
                if (!r.IsClosed)
                {
                    r.Close();
                }
            }
            r.Close();
            P.m_childs = new SecurityObject[chlds.Count];
            chlds.CopyTo(P.m_childs);
            foreach (SecurityObject seco in P.m_childs)
            {
                this.LoadChilds(helper, seco);
            }
        }

        public SecurityObject this[string path]
        {
            get
            {
                SecurityObject obj = this.m_Root;
                string[] names = this.GetTokens(path);
                for (int i = 1; i < names.Length; i++)
                {
                    int j = 0;
                    while (j < obj.m_childs.Length)
                    {
                        if (obj.m_childs[j].Name.ToUpper() == names[i].ToUpper())
                        {
                            break;
                        }
                        j++;
                    }
                    if (j == obj.m_childs.Length)
                    {
                        return null;
                    }
                    obj = obj.m_childs[j];
                }
                return obj;
            }
        }

        public SecurityObject this[int ObjectID]
        {
            get
            {
                return (SecurityObject) this.m_Objects[ObjectID];
            }
        }

        public SecurityObject Root
        {
            get
            {
                return this.m_Root;
            }
        }
    }
}

