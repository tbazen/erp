namespace INTAPS.ClientServer
{
    using System;

    public class SecurityObject
    {
        public int ID;
        internal SecurityObject[] m_childs;
        internal SecurityObjectClass m_Class;
        internal SecurityObject m_Parent;
        public string Name;
        public int Owner;
        internal SecurityProvider Security;

        public SecurityObject Find(int ObjectID)
        {
            for (SecurityObject s = this; s != null; s = s.m_Parent)
            {
                if (s.ID == ObjectID)
                {
                    return s;
                }
                if (s.m_Class == SecurityObjectClass.soNoInherit)
                {
                    break;
                }
            }
            return null;
        }

        public SecurityObject[] Childs
        {
            get
            {
                return this.m_childs;
            }
        }

        public SecurityObjectClass Class
        {
            get
            {
                return this.m_Class;
            }
        }

        public string FullName
        {
            get
            {
                string ret = this.Name;
                for (SecurityObject o = this.m_Parent; o != null; o = o.m_Parent)
                {
                    ret = o.Name + "/" + ret;
                }
                return ret;
            }
        }

        public string OwnerName
        {
            get
            {
                return this.Security.GetNameForID(this.Owner);
            }
        }

        public SecurityObject Parent
        {
            get
            {
                return this.m_Parent;
            }
        }
    }
}

