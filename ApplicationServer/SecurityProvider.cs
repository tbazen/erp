namespace INTAPS.ClientServer
{
    using INTAPS.RDBMS;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;
    using System.Text;

    public class UserPassword
    {
        public String userName;
        public String passwordHash;
        public int id;
        public int parentId;
    }
    public interface IUserListProvider
    {
        int saveUser(int ParentUser, string UserName, string Password);
        IEnumerable<UserPassword> getAllUsers();
        string hashPassword(string pw);
        void setPassword(String userName, String password);
    }
    public class SecurityProvider : BDEBase, IUserListProvider
    {

        public string AdminAccount;
        private INTAPS.ClientServer.ObjectTree m_ot;
        private Hashtable m_Users;
        private RSACryptoServiceProvider rsa;
        private const string SOBJ_CREATE_CHILD = "CREATE_CHILD";
        private const string SOBJ_SECURITY = "SECURITY";

        IUserListProvider userListProvider;
        public SecurityProvider(string bdeName, string DB, SQLHelper helper, string conStr) : base(bdeName, DB, helper, conStr)
        {
            this.AdminAccount = "CISAdmin";
            this.rsa = new RSACryptoServiceProvider();
            initUserListProvider();
            SQLHelper h = base.GetReaderHelper();
            try
            {

                this.m_ot = new INTAPS.ClientServer.ObjectTree(h, this);
                base.ReleaseHelper(h);
                this.m_Users = new Hashtable();
                this.LoadUsers(h);
            }
            finally
            {
                base.ReleaseHelper(h);
            }
        }

        public IEnumerable<SecurityObject> GetObjectTree()
        {
            var oTree = this.m_ot.m_Objects;
            return (oTree.Values).Cast<SecurityObject>();
        }

        private void initUserListProvider()
        {
            try
            {
                String ulp = System.Configuration.ConfigurationManager.AppSettings["user_list_provider"];
                if (String.IsNullOrEmpty(ulp))
                {
                    ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "No user list provider plugin provided. Using default user list provider.");
                }
                else
                {
                    ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Loading user list provider:" + ulp);
                    Type t = Type.GetType(ulp);
                    userListProvider = Activator.CreateInstance(t) as IUserListProvider;
                    if (userListProvider == null)
                        ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "User list provider plugin provided but it doesn't expose IUserListProvider interface. Using default user list provider.");

                }
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("Error trying to load user list provider, using default user list provider.", ex);
            }
            finally
            {
                if (userListProvider == null)
                    userListProvider = this;
            }
        }

        public void ChangeClass(int ObjectID, SecurityObjectClass Class)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Update SecObjects set Class=", (int)Class, " where ID=", ObjectID }));
                this.m_ot[ObjectID].m_Class = Class;
            }
        }

        public void ChangeOwner(int ObjectID, int NewOwner)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Update SecObjects set Owner=", NewOwner, " where ID=", ObjectID }));
                this.m_ot[ObjectID].Owner = NewOwner;
            }
        }

        public void ChangePassword(string UserName, string newpassword)
        {
            userListProvider.setPassword(UserName, newpassword);
            UserPermissions up = (UserPermissions)this.m_Users[UserName.ToUpper()];
            up.PassWordHash = this.Encrypt(newpassword);
        }
        public void setPassword(string UserName, string newpassword)
        {
            if (!this.m_Users.Contains(UserName.ToUpper()))
            {
                throw new AccessDeniedException("Unknown user.");
            }
            lock (base.dspWriter)
            {
                UserPermissions up = (UserPermissions)this.m_Users[UserName.ToUpper()];
                base.dspWriter.Update(base.m_DBName, "SecUsers", new string[] { "password" }, new object[] { this.Encrypt(newpassword) }, "Name='" + up.Name + "'");
            }
        }

        private void CheckOwnerShip(string UserName, int ObjectID)
        {
            if (!this.m_Users.Contains(UserName.ToUpper()))
            {
                throw new AccessDeniedException("Unkown user.");
            }
            if (!this.IsOwner(UserName, ObjectID))
            {
                throw new AccessDeniedException("You are not the owner of this object");
            }
        }

        private int ChildCount(int UID)
        {
            int ret = 0;
            foreach (DictionaryEntry de in this.m_Users)
            {
                if (((UserPermissions)de.Value).ParentUserID == UID)
                {
                    ret++;
                }
            }
            return ret;
        }

        public int CreateObject(int ParentOID, string Name)
        {
            lock (base.dspWriter)
            {
                int OID = AutoIncrement.GetKey("ObjectID");
                SecurityObject ob = this.ObjectTree[ParentOID];
                foreach (SecurityObject child in ob.m_childs)
                {
                    if (child.Name.ToUpper() == Name.ToUpper())
                    {
                        throw new Exception("An object with the same name allready exists.");
                    }
                }
                base.dspWriter.Insert(base.m_DBName, "SecObjects", new string[] { "ID", "PID", "Name", "Owner", "Class" }, new object[] { OID, ParentOID, Name, ob.Owner, 1 });
                SecurityObject[] childs = new SecurityObject[ob.m_childs.Length + 1];
                ob.m_childs.CopyTo(childs, 0);
                childs[childs.Length - 1] = new SecurityObject();
                childs[childs.Length - 1].m_Parent = ob;
                childs[childs.Length - 1].Security = this;
                childs[childs.Length - 1].m_childs = new SecurityObject[0];
                childs[childs.Length - 1].Name = Name;
                childs[childs.Length - 1].Owner = ob.Owner;
                childs[childs.Length - 1].ID = OID;
                ob.m_childs = childs;
                this.m_ot.AddObject(OID, childs[childs.Length - 1]);
                return OID;
            }
        }

        public void CreateUser(string ParentUser, string UserName, string Password)
        {
            lock (base.dspWriter)
            {
                int PID;
                if (this.m_Users.Contains(UserName.ToUpper()))
                {
                    throw new Exception("User allready exists");
                }
                if (UserName.Trim() == "")
                {
                    throw new Exception("Invalid user name");
                }
                if ((ParentUser == null) || (ParentUser.Trim() == ""))
                {
                    PID = -1;
                }
                else
                {
                    if (!this.m_Users.Contains(ParentUser.ToUpper()))
                    {
                        throw new Exception("Parent user doesn't exist");
                    }
                    PID = this.User(ParentUser).UID;
                }
                int UID = userListProvider.saveUser(PID, UserName, Password);
                UserPermissions up = new UserPermissions
                {
                    Permissions = new int[0],
                    Denied = new int[0],
                    Parent = this,
                    PassWordHash = this.Encrypt(Password),
                    UserID = UID,
                    UserName = UserName,
                    ParentUserID = PID
                };
                this.m_Users.Add(UserName.ToUpper(), up);
            }
        }

        private string Decrypt(string pw)
        {
            return pw;
        }

        public void DeleteObject(int ObjectID)
        {
            if (ObjectID == -1)
            {
                throw new Exception("You can not delete the root object.");
            }
            lock (base.dspWriter)
            {
                SecurityObject o = this.ObjectTree[ObjectID];
                if (o.Childs.Length > 0)
                    throw new ServerUserMessage("You can't delete this object because it has childs");
                if (this.GetPermitedUsers(ObjectID).Length > 0)
                    throw new ServerUserMessage("You can't delete this object because there are users with permission to it");

                this.DeleteRecursive(o);
                SecurityObject[] childs = new SecurityObject[o.m_Parent.m_childs.Length - 1];
                int i = 0;
                foreach (SecurityObject ch in o.m_Parent.m_childs)
                {
                    if (ch.ID != ObjectID)
                    {
                        childs[i++] = ch;
                    }
                }
                o.m_childs = childs;
            }
        }

        private void DeletePermission(int ObjectID, UserPermissions up)
        {
            lock (this.m_Users)
            {
                int[] NewPerm;
                int k;
                int[] perms = up.Permissions;
                bool found = false;
                int i = 0;
                foreach (int OID in perms)
                {
                    if (found)
                    {
                        up.Permissions[i - 1] = up.Permissions[i];
                    }
                    if (OID == ObjectID)
                    {
                        found = true;
                    }
                    i++;
                }
                if (found)
                {
                    NewPerm = new int[perms.Length - 1];
                    for (k = 0; k < NewPerm.Length; k++)
                    {
                        NewPerm[k] = up.Permissions[k];
                    }
                    up.Permissions = NewPerm;
                }
                perms = up.Denied;
                found = false;
                i = 0;
                foreach (int OID in perms)
                {
                    if (found)
                    {
                        up.Denied[i - 1] = up.Denied[i];
                    }
                    if (OID == ObjectID)
                    {
                        found = true;
                    }
                    i++;
                }
                if (found)
                {
                    NewPerm = new int[perms.Length - 1];
                    for (k = 0; k < NewPerm.Length; k++)
                    {
                        NewPerm[k] = up.Denied[k];
                    }
                    up.Denied = NewPerm;
                }
            }
        }

        private void DeleteRecursive(SecurityObject so)
        {
            foreach (SecurityObject child in so.m_childs)
            {
                this.DeleteRecursive(child);
            }
            string[] Users = this.GetPermitedUsers(so.ID);
            foreach (string user in Users)
            {
                this.SetPermission(so.ID, user, PermissionState.Undefined);
            }
            lock (base.dspWriter)
            {
                base.dspWriter.ExecuteNonQuery("Delete from SecObjects where ID=" + so.ID);
            }
        }

        public void DeleteUser(string UserName)
        {
            lock (base.dspWriter)
            {
                UserPermissions up = this.User(UserName);
                if (up.UID == 1)
                {
                    throw new Exception("You can not delete the root owner.");
                }
                if (this.ChildCount(up.UID) > 0)
                {
                    throw new Exception("Remove the child users first.");
                }
                base.dspWriter.BeginTransaction();
                try
                {
                    int[] perms = up.Permissions;
                    foreach (int OID in perms)
                    {
                        this.SetPermission(OID, up.Name, PermissionState.Undefined);
                    }
                    base.dspWriter.ExecuteNonQuery("Delete from SecUsers where ID=" + up.UID);
                    this.m_Users.Remove(up.Name.ToUpper());
                    base.dspWriter.CommitTransaction();
                }
                catch (Exception ex)
                {
                    base.dspWriter.RollBackTransaction();
                    throw new Exception("Error trying to delete user", ex);
                }
            }
        }

        private string Encrypt(string pw)
        {
            return userListProvider.hashPassword(pw);
        }

        public string[] GetAllAuditOperations()
        {
            string[] var_1_0000;
            SQLHelper h = base.GetReaderHelper();
            try
            {
                DataTable data = h.GetDataTable("Select Operation from AllOperations order by Operation");
                string[] ret = new string[data.Rows.Count];
                int i = 0;
                foreach (DataRow row in data.Rows)
                {
                    ret[i++] = (string)row[0];
                }
                var_1_0000 = ret;
            }
            finally
            {
                base.ReleaseHelper(h);
            }
            return var_1_0000;
        }

        public string[] GetAllUsers()
        {
            string[] ret = new string[this.m_Users.Count];
            int i = 0;
            foreach (DictionaryEntry de in this.m_Users)
            {
                ret[i] = ((UserPermissions)de.Value).Name;
                i++;
            }
            Array.Sort<string>(ret);
            return ret;
        }

        public DataTable GetAudit(string[] AuditType, string[] Users, bool Date, DateTime from, bool DateTo, DateTime to, string[] Data1, int index, int PageSize, out int NRecords)
        {
            bool FirstItem;
            DataTable var_1_0000;
            string cr = "";
            if ((AuditType != null) && (AuditType.Length > 0))
            {
                FirstItem = true;
                foreach (string at in AuditType)
                {
                    if (FirstItem)
                    {
                        FirstItem = false;
                        cr = cr + " Operation in ('" + at + "'";
                    }
                    else
                    {
                        cr = cr + ",'" + at + "'";
                    }
                }
                cr = cr + ")";
            }
            if ((Users != null) && (Users.Length > 0))
            {
                FirstItem = true;
                foreach (string User in Users)
                {
                    if (FirstItem)
                    {
                        FirstItem = false;
                        if (cr != "")
                        {
                            cr = cr + " AND ";
                        }
                        cr = cr + " UserName in ('" + User + "'";
                    }
                    else
                    {
                        cr = cr + ",'" + User + "'";
                    }
                }
                cr = cr + ")";
            }
            if (Date)
            {
                if (!DateTo)
                {
                    to = from.AddDays(1.0);
                }
                if (cr != "")
                {
                    cr = cr + " AND ";
                }
                object var_0_0004 = cr;
                cr = string.Concat(new object[] { var_0_0004, " ([DateTime] BETWEEN '", from, "' AND '", to, "')" });
            }
            if ((Data1 != null) && (Data1.Length > 0))
            {
                if (cr != "")
                {
                    cr = cr + " AND ";
                }
                cr = cr + " (Data1 in (";
                for (int j = 0; j < Data1.Length; j++)
                {
                    if (j == 0)
                    {
                        cr = cr + "'" + Data1[j] + "'";
                    }
                    else
                    {
                        cr = cr + ",'" + Data1[j] + "'";
                    }
                }
                cr = cr + "))";
            }
            if (cr != "")
            {
                cr = " WHERE " + cr;
            }
            SQLHelper h = base.GetReaderHelper();
            try
            {
                int i;
                NRecords = (int)h.ExecuteScalar("Select count(*) from Audit " + cr);
                IDataReader reader = h.ExecuteReader("Select ID,DateTime,UserName,UserID,Operation,Data1 from Audit " + cr + " order by ID desc");
                DataTable ret = new DataTable("Audit");
                ret.Columns.AddRange(new DataColumn[] { new DataColumn("ID", typeof(int)), new DataColumn("DateTime", typeof(DateTime)), new DataColumn("User", typeof(string)), new DataColumn("User ID", typeof(int)), new DataColumn("Operation", typeof(string)), new DataColumn("Remark", typeof(string)) });
                for (i = 0; (PageSize != -1) && (i < index); i++)
                {
                    reader.Read();
                }
                for (i = 0; reader.Read() && ((PageSize == -1) || (i < PageSize)); i++)
                {
                    object[] vals = new object[reader.FieldCount];
                    reader.GetValues(vals);
                    ret.Rows.Add(vals);
                }
                reader.Close();
                var_1_0000 = ret;
            }
            finally
            {
                base.ReleaseHelper(h);
            }
            return var_1_0000;
        }
        public AuditInfo getAuditInfo(int id)
        {
            SQLHelper h = base.GetReaderHelper();
            try
            {
                return getAuditInfoInternal(h, id);
            }
            finally
            {
                base.ReleaseHelper(h);
            }
        }

        private static AuditInfo getAuditInfoInternal(SQLHelper h, int id)
        {
            AuditInfo[] ret = h.GetSTRArrayByFilter<AuditInfo>("ID=" + id);
            if (ret.Length == 0)
                return null;
            return ret[0];
        }
        public string GetAuditData2(int AuditID)
        {
            string var_1_0000;
            SQLHelper h = base.GetReaderHelper();
            try
            {
                var_1_0000 = (string)h.ExecuteScalar("Select Data2 from Audit where ID=" + AuditID);
            }
            finally
            {
                base.ReleaseHelper(h);
            }
            return var_1_0000;
        }

        public string GetAuditData3(int AuditID)
        {
            string var_1_0000;
            SQLHelper h = base.GetReaderHelper();
            try
            {
                var_1_0000 = h.ExecuteScalar("Select Data3 from Audit where ID=" + AuditID) as string;
            }
            finally
            {
                base.ReleaseHelper(h);
            }
            return var_1_0000;
        }

        public SecurityObject[] GetChildList(int ObjectID)
        {
            if (ObjectID == -1)
            {
                return this.m_ot.Root.m_childs;
            }
            return this.m_ot[ObjectID].m_childs;
        }

        public string[] GetChildUsers(string User)
        {
            lock (base.dspWriter)
            {
                int UserID = (User == "") ? -1 : this.GetUIDForName(User);
                ArrayList list = new ArrayList();
                foreach (DictionaryEntry de in this.m_Users)
                {
                    if (((UserPermissions)de.Value).ParentUserID == UserID)
                    {
                        list.Add(((UserPermissions)de.Value).Name);
                    }
                }
                string[] ret = new string[list.Count];
                list.CopyTo(ret);
                return ret;
            }
        }

        public string[] GetDeniedEntries(int ObjectID)
        {
            ArrayList retb = new ArrayList();
            foreach (DictionaryEntry de in this.m_Users)
            {
                UserPermissions up = (UserPermissions)de.Value;
                foreach (int obj in up.Denied)
                {
                    if (obj == ObjectID)
                    {
                        retb.Add(up.Name);
                        break;
                    }
                }
            }
            string[] ret = new string[retb.Count];
            retb.CopyTo(ret);
            return ret;
        }

        public string[] GetDeniedUsers(int ObjectID)
        {
            ArrayList retb = new ArrayList();
            foreach (DictionaryEntry de in this.m_Users)
            {
                if (((UserPermissions)de.Value).IsDenied(ObjectID))
                {
                    retb.Add(((UserPermissions)de.Value).Name);
                }
            }
            string[] ret = new string[retb.Count];
            retb.CopyTo(ret);
            return ret;
        }

        public string GetNameForID(int UserID)
        {
            foreach (DictionaryEntry user in this.m_Users)
            {
                if (((UserPermissions)user.Value).UID == UserID)
                {
                    return ((UserPermissions)user.Value).Name;
                }
            }
            throw new IndexOutOfRangeException("Invalid UserID.");
        }

        public SecurityObject GetObject(int ObjectID)
        {
            return this.ObjectTree[ObjectID];
        }

        public int GetObjectID(string Name)
        {
            SecurityObject obj = this.m_ot[Name];
            if (obj == null)
            {
                return -1;
            }
            return obj.ID;
        }

        public string GetParentUser(string UserName)
        {
            int PID = this.User(UserName).ParentUserID;
            if (PID != -1)
            {
                return this.GetNameForID(PID);
            }
            return null;
        }

        public string[] GetPermitedUsers(int ObjectID)
        {
            ArrayList retb = new ArrayList();
            foreach (DictionaryEntry de in this.m_Users)
            {
                if (((UserPermissions)de.Value).IsPermited(ObjectID))
                {
                    retb.Add(((UserPermissions)de.Value).Name);
                }
            }
            string[] ret = new string[retb.Count];
            retb.CopyTo(ret);
            return ret;
        }

        public string[] GetPermittedEntries(int ObjectID)
        {
            ArrayList retb = new ArrayList();
            foreach (DictionaryEntry de in this.m_Users)
            {
                UserPermissions up = (UserPermissions)de.Value;
                foreach (int obj in up.Permissions)
                {
                    if (obj == ObjectID)
                    {
                        retb.Add(up.Name);
                        break;
                    }
                }
            }
            string[] ret = new string[retb.Count];
            retb.CopyTo(ret);
            return ret;
        }

        private SecurityObject GetSecurityObject(int ObjectID)
        {
            return this.m_ot[ObjectID];
        }

        public int GetUIDForName(string Name)
        {
            if (!this.m_Users.Contains(Name.ToUpper()))
            {
                throw new IndexOutOfRangeException("Invalid User Name.");
            }
            return ((UserPermissions)this.m_Users[Name.ToUpper()]).UID;
        }

        public UserPermissions GetUserPermissions(int UserID)
        {
            foreach (DictionaryEntry user in this.m_Users)
            {
                if (((UserPermissions)user.Value).UID == UserID)
                {
                    return (UserPermissions)user.Value;
                }
            }
            throw new IndexOutOfRangeException("Invalid UserID.");
        }

        public UserPermissions GetUserPermmissions(string username, string password)
        {
            if (this.m_Users.Contains(username.ToUpper()))
            {
                UserPermissions ss = (UserPermissions)this.m_Users[username.ToUpper()];
                if (ss.PassWordHash.Equals(this.Encrypt(password)))
                {
                    return ss;
                }
            }
            throw new AccessDeniedException("Invalid password or username");
        }

        private bool IsOwner(string UserName, int ObjectID)
        {
            if (!this.m_Users.Contains(UserName.ToUpper()))
            {
                throw new AccessDeniedException("Unkown user.");
            }
            UserPermissions up = (UserPermissions)this.m_Users[UserName.ToUpper()];
            SecurityObject so = this.GetSecurityObject(ObjectID);
            while (so.Owner != up.UID)
            {
                so = so.Parent;
                if (so == null)
                {
                    return false;
                }
            }
            return true;
        }

        private void LoadUsers(SQLHelper h)
        {
            UserPermissions ss;
            foreach (UserPassword p in userListProvider.getAllUsers())
            {
                ss = new UserPermissions
                {
                    PassWordHash = p.passwordHash
                };
                string username = p.userName;
                ss.UserName = username;
                ss.UserID = p.id;
                ss.ParentUserID = p.parentId;
                ss.Parent = this;
                this.m_Users.Add(username.ToUpper(), ss);
            }

            foreach (DictionaryEntry de in this.m_Users)
            {
                ss = (UserPermissions)de.Value;
                ArrayList perms = new ArrayList();
                ArrayList denys = new ArrayList();
                SqlDataReader reader = h.ExecuteReader("Select ObjectID,Permit from Permissions where UserID='" + ss.UID + "'");
                while (reader.Read())
                {
                    if (((byte)reader[1]) == 0)
                    {
                        perms.Add((int)reader["ObjectID"]);
                    }
                    else if (((byte)reader[1]) == 1)
                    {
                        denys.Add((int)reader["ObjectID"]);
                    }
                    else
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, "Invalid permission state value:" + reader[1].ToString());
                    }
                }
                ss.Permissions = new int[perms.Count];
                perms.CopyTo(ss.Permissions);
                ss.Denied = new int[denys.Count];
                denys.CopyTo(ss.Denied);
                reader.Close();
            }
        }

        public void Rename(int ObjectID, string NewName)
        {
            lock (base.dspWriter)
            {
                this.ObjectTree[ObjectID].Name = NewName;
                base.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Update SecObjects Set Name='", NewName, "' where ID=", ObjectID }));
            }
        }

        public void SetPermission(int ObjectID, string UserName, PermissionState state)
        {
            lock (base.dspWriter)
            {
                UserPermissions up = (UserPermissions)this.m_Users[UserName.ToUpper()];
                SecurityObject obj = this.ObjectTree[ObjectID];
                this.DeletePermission(ObjectID, up);
                if (state == PermissionState.Undefined)
                {
                    base.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Delete Permissions where ObjectID=", ObjectID, " and UserID=", up.UID }));
                }
                else
                {
                    int[] np;
                    SQLHelper h = base.GetReaderHelper();
                    int count = 0;
                    try
                    {
                        count = (int)h.ExecuteScalar(string.Concat(new object[] { "Select count(*) from Permissions where ObjectID=", ObjectID, " and UserID=", up.UID }));
                    }
                    finally
                    {
                        base.ReleaseHelper(h);
                    }
                    if (count == 0)
                    {
                        base.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Insert Into Permissions (ObjectID,UserID,permit) VALUES (", ObjectID, ",", up.UID, ",", (byte)state, ")" }));
                    }
                    else
                    {
                        base.dspWriter.ExecuteNonQuery(string.Concat(new object[] { "Update Permissions Set permit=", (byte)state, " where ObjectID=", ObjectID, " and UserID=", up.UID }));
                    }
                    if (state == PermissionState.Permit)
                    {
                        np = new int[up.Permissions.Length + 1];
                    }
                    else
                    {
                        np = new int[up.Denied.Length + 1];
                    }
                    ((state == PermissionState.Permit) ? up.Permissions : up.Denied).CopyTo(np, 0);
                    np[np.Length - 1] = ObjectID;
                    if (state == PermissionState.Permit)
                    {
                        up.Permissions = np;
                    }
                    else
                    {
                        up.Denied = np;
                    }
                }
            }
        }

        public UserPermissions User(string UserName)
        {
            return (UserPermissions)this.m_Users[UserName.ToUpper()];
        }

        public int WriteAddAudit(string userName, int userID, string AddType, string RowKey, int PID)
        {
            return this.WriteAudit(userName, userID, AddType, RowKey, null, PID);
        }

        public int WriteAudit(string userName, int userID, string Operation, string Data1, string Data2, int PID)
        {
            lock (base.dspWriter)
            {
                int ID = AutoIncrement.GetKey("AuditID");
                base.dspWriter.Insert(base.m_DBName, "Audit", new string[] { "ID", "UserName", "UserID", "Operation", "Data1", "Data2", "PID" }, new object[] { ID, userName, userID, Operation, (Data1 == null) ? ((object)DBNull.Value) : ((object)Data1), (Data2 == null) ? ((object)DBNull.Value) : ((object)Data2), PID });
                return ID;
            }
        }

        public int WriteAudit(string userName, int userID, string Operation, string Data1, string Data2, byte[] Data3, int PID)
        {
            lock (base.dspWriter)
            {
                int ID = AutoIncrement.GetKey("AuditID");
                base.dspWriter.Insert(base.m_DBName, "Audit", new string[] { "ID", "UserName", "UserID", "Operation", "Data1", "Data2", "Data3", "PID" }, new object[] { ID, userName, userID, Operation, (Data1 == null) ? ((object)DBNull.Value) : ((object)Data1), (Data2 == null) ? ((object)DBNull.Value) : ((object)Data2), (Data3 == null) ? ((object)DBNull.Value) : ((object)Data3), PID });
                return ID;
            }
        }

        public int WriteDataUpdateAudit(string userName, int userID, string UpdateType, string RowKey, DataTable Data, int PID)
        {
            StringWriter sw = new StringWriter();
            Data.WriteXml(sw, XmlWriteMode.WriteSchema);
            int ret = this.WriteAudit(userName, userID, UpdateType, RowKey, sw.ToString(), PID);
            sw.Dispose();
            return ret;
        }

        public int WriteExecuteAudit(string userName, int userID, string CommandName, int PID)
        {
            return this.WriteAudit(userName, userID, CommandName, null, null, PID);
        }

        public int WriteObjectAudit(string userName, int userID, string UpdateType, string RowKey, object obj, int PID)
        {
            byte[] objStr;
            if (obj == null)
            {
                objStr = null;
            }
            else
            {
                MemoryStream ms = new MemoryStream();
                new BinaryFormatter().Serialize(ms, obj);
                objStr = new byte[ms.Length];
                ms.Seek(0L, SeekOrigin.Begin);
                ms.Read(objStr, 0, objStr.Length);
            }
            return this.WriteAudit(userName, userID, UpdateType, RowKey, null, objStr, PID);
        }

        internal INTAPS.ClientServer.ObjectTree ObjectTree
        {
            get
            {
                return this.m_ot;
            }
        }

        public AuditInfo getAuditInfoFromTable(string DB, string tableName, string cr)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object aid = dspReader.ExecuteScalar(String.Format("Select {0} from {1}.dbo.{2} {3}"
                    , "__AID", DB, tableName, string.IsNullOrEmpty(cr) ? "" : " where " + cr));
                if (aid is int)
                    return getAuditInfoInternal(dspReader, (int)aid);
                return null;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public int saveUser(int PID, string UserName, string Password)
        {
            int UID = AutoIncrement.GetKey("UserID");
            string PW = this.Encrypt(Password);
            base.dspWriter.Insert(base.m_DBName, "SecUsers", new string[] { "ID", "Name", "PassWord", "PID" }, new object[] { UID, UserName, PW, PID });
            return UID;
        }

        public IEnumerable<UserPassword> getAllUsers()
        {
            List<UserPassword> ret = new List<UserPassword>();
            SQLHelper h = base.GetReaderHelper();
            try
            {
                SqlDataReader reader = h.ExecuteReader("Select Name,Password,ID,PID from SecUsers order by Name");
                while (reader.Read())
                {
                    UserPassword user = new UserPassword
                    {
                        passwordHash = (string)reader["Password"],
                        userName = (string)reader["Name"],
                        id = (int)reader["ID"],
                        parentId = -1
                    };
                    ret.Add(user);
                }
                reader.Close();
                return ret;
            }
            finally
            {
                this.ReleaseHelper(h);
            }
        }

        public string hashPassword(string pw)
        {
            byte[] hash = new SHA1CryptoServiceProvider().ComputeHash(Encoding.ASCII.GetBytes(pw));
            return Encoding.ASCII.GetString(hash);
        }

        public class UserPermissions : UserPermissionsData
        {
            public SecurityProvider Parent;
            public string PassWordHash;

            public void AddPermission(int ObjectID)
            {
            }

            public int[] GetAllDenied()
            {
                return (int[])base.Denied.Clone();
            }

            public int[] GetAllPermissions()
            {
                return (int[])base.Permissions.Clone();
            }

            public int[] GetDenied()
            {
                return (int[])base.Denied.Clone();
            }

            public bool IsDenied(int ObjectID)
            {
                return this.IsDeniedInternal(ObjectID);
            }

            public bool IsDenied(string ObjectName)
            {
                SecurityObject ob = this.Parent.ObjectTree[ObjectName];
                if (ob == null)
                {
                    return false;
                }
                return this.IsDenied(ob.ID);
            }

            public bool IsDeniedInternal(int ObjectID)
            {
                SecurityObject ob = this.Parent.m_ot[ObjectID];
                if (ob == null)
                {
                    return false;
                }
                for (int i = 0; i < base.Denied.Length; i++)
                {
                    if (ob.Find(base.Denied[i]) != null)
                    {
                        return true;
                    }
                }
                return ((base.ParentUserID != -1) && this.Parent.GetUserPermissions(base.ParentUserID).IsDeniedInternal(ObjectID));
            }

            public bool IsPermited(int ObjectID)
            {
                if (this.IsDeniedInternal(ObjectID))
                {
                    return false;
                }
                return this.IsPermitedInternal(ObjectID);
            }

            public bool IsPermited(string ObjectName)
            {
                SecurityObject ob = this.Parent.ObjectTree[ObjectName];
                if (ob == null)
                {
                    return false;
                }
                return this.IsPermited(ob.ID);
            }

            private bool IsPermitedInternal(int ObjectID)
            {
                SecurityObject ob = this.Parent.m_ot[ObjectID];
                if (ob == null)
                {
                    return false;
                }
                for (int i = 0; i < base.Permissions.Length; i++)
                {
                    if (ob.Find(base.Permissions[i]) != null)
                    {
                        return true;
                    }
                }
                return ((base.ParentUserID != -1) && this.Parent.GetUserPermissions(base.ParentUserID).IsPermitedInternal(ObjectID));
            }

            public override string ToString()
            {
                return base.UserName;
            }

            public string Name
            {
                get
                {
                    return base.UserName;
                }
            }

            public int UID
            {
                get
                {
                    return base.UserID;
                }
            }

         
        }
    }
}

