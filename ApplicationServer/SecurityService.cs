namespace INTAPS.ClientServer
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Runtime.InteropServices;

    public class SecurityService : SessionObjectBase<SecurityProvider>
    {
        private SecurityProvider bdeSecurity;

        public SecurityService(UserSessionData Session) : base(Session, ApplicationServer.SecurityBDE)
        {
            this.bdeSecurity = base._bde;
        }

        public void ChangeClass(int ObjectID, SecurityObjectClass Class)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.ChangeClass(ObjectID, Class);
        }

        public void ChangeOwner(int ObjectID, int NewOwner)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.ChangeOwner(ObjectID, NewOwner);
        }

        public void ChangePassword(string newpassword)
        {
            //this.CheckAdminPermission();
            this.bdeSecurity.ChangePassword(base.m_Session.Permissions.UserName, newpassword);
        }
        public bool IsPermited(string obj)
        {
            return base.m_Session.Permissions.IsPermited(obj);
        }
        public void ChangePassword(string UserName, string newpassword)
        {
            if (base.m_Session.Permissions.UserName.ToUpper() != UserName.ToUpper())
            {
                this.CheckAdminPermission();
            }
            this.bdeSecurity.ChangePassword(UserName, newpassword);
        }

        private void CheckAdminPermission()
        {
            this.CheckAdminPermission("Access denied.");
        }

        private void CheckAdminPermission(string msg)
        {
            if (!isAdmin() && !base.m_Session.Permissions.IsPermited("Root/Security/Admin"))
            {
                throw new AccessDeniedException(msg);
            }
        }

        private void CheckAuditViewPermission()
        {
            this.CheckAuditViewPermission("Access denied.");
        }

        private void CheckAuditViewPermission(string msg)
        {
            if (!isAdmin() && !base.m_Session.Permissions.IsPermited("Root/Security/Audit"))
            {
                throw new AccessDeniedException(msg);
            }
        }

        private void CheckSecViewPermission()
        {
            this.CheckSecViewPermission("Access denied.");
        }

        bool isAdmin()
        {
            return string.Compare(m_Session.Permissions.UserName, "Admin", true)==0;
        }

        private void CheckSecViewPermission(string msg)
        {
            if (!isAdmin() && !base.m_Session.Permissions.IsPermited("Root/Security/View"))
            {
                throw new AccessDeniedException(msg);
            }
        }

        public bool IsAdminUser(string username, string password)
        {
            var sessionData = GetUserPermissions(username, password);
            var admin = string.Compare(m_Session.Permissions.UserName, "Admin", true) == 0;
            var s = this.bdeSecurity.GetUserPermissions(sessionData.UserID);
            var permitted = s.IsPermited("Root/Security/View");
            if(!admin && !permitted)
            {
                return false;
            }
            return false;
        }

        public IEnumerable<UserPassword> GetUsers()
        {
            this.CheckAdminPermission();
            return this.bdeSecurity.getAllUsers();
        }

        public IEnumerable<SecurityObject> GetObjectTree()
        {
            this.CheckAdminPermission();
            return this.bdeSecurity.GetObjectTree();
        }



        public int CreateObject(int ParentOID, string Name)
        {
            this.CheckAdminPermission();
            return this.bdeSecurity.CreateObject(ParentOID, Name);
        }

        public void CreateUser(string ParentUser, string UserName, string Password)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.CreateUser(ParentUser, UserName, Password);
        }

        public void DeleteObject(int ObjectID)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.WriterHelper.BeginTransaction();
            try
            {
                int AID=this.WriteExecuteAudit("SecurityService.DeleteObject", -1);
                this.bdeSecurity.DeleteObject(ObjectID);
                this.bdeSecurity.WriterHelper.CommitTransaction();
            }
            catch
            {
                this.bdeSecurity.WriterHelper.RollBackTransaction();
                throw;
            }
        }

        public void DeleteUser(string UserName)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.DeleteUser(UserName);
        }

        public string[] GetAllAuditOperations()
        {
            this.CheckAuditViewPermission();
            return this.bdeSecurity.GetAllAuditOperations();
        }

        public string[] GetAllUsers()
        {
            this.CheckAuditViewPermission();
            return this.bdeSecurity.GetAllUsers();
        }

        public DataTable GetAudit(string[] AuditType, string[] Users, bool Date, DateTime from, bool DateTo, DateTime to, string[] Data1, int index, int PageSize, out int NRecords)
        {
            this.CheckAuditViewPermission();
            return this.bdeSecurity.GetAudit(AuditType, Users, Date, from, DateTo, to, Data1, index, PageSize, out NRecords);
        }

        public string GetAuditData2(int AuditID)
        {
            this.CheckAuditViewPermission();
            return this.bdeSecurity.GetAuditData2(AuditID);
        }

        public SecurityObjectData[] GetChildList(int ObjectID)
        {
            this.CheckSecViewPermission();
            SecurityObject[] chs = this.bdeSecurity.GetChildList(ObjectID);
            SecurityObjectData[] ret = new SecurityObjectData[chs.Length];
            for (int i = 0; i < ret.Length; i++)
            {
                SecurityObjectData d = new SecurityObjectData {
                    ID = chs[i].ID
                };
                if (chs[i].m_Parent == null)
                {
                    d.PID = -1;
                }
                else
                {
                    d.PID = chs[i].m_Parent.ID;
                }
                d.Name = chs[i].Name;
                d.FullName = chs[i].FullName;
                d.Class = chs[i].Class;
                d.OwnerName = chs[i].OwnerName;
                d.ChildCount = chs[i].Childs.Length;
                ret[i] = d;
            }
            return ret;
        }

        public string[] GetChildUsers(string User)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetChildUsers(User);
        }

        public string[] GetDeniedEntries(int ObjectID)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetDeniedEntries(ObjectID);
        }

        public string[] GetDeniedUsers(int ObjectID)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetDeniedUsers(ObjectID);
        }

        public string GetNameForID(int UserID)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetNameForID(UserID);
        }

        public SecurityObjectData GetObject(int ObjectID)
        {
            this.CheckSecViewPermission();
            SecurityObject so = this.bdeSecurity.GetObject(ObjectID);
            SecurityObjectData d = new SecurityObjectData {
                ID = so.ID
            };
            if (so.m_Parent == null)
            {
                d.PID = -1;
            }
            else
            {
                d.PID = so.m_Parent.ID;
            }
            d.Name = so.Name;
            d.FullName = so.FullName;
            d.Class = so.Class;
            d.OwnerName = so.OwnerName;
            return d;
        }

        public string GetObjectFullName(int OID)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.ObjectTree[OID].FullName;
        }

        public int GetObjectID(string Name)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetObjectID(Name);
        }

        public string GetParentUser(string UserName)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetParentUser(UserName);
        }

        public string[] GetPermitedUsers(int ObjectID)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetPermitedUsers(ObjectID);
        }

        public string[] GetPermittedEntries(int ObjectID)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetPermittedEntries(ObjectID);
        }

        public int GetUIDForName(string Name)
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetUIDForName(Name);
        }

        public UserPermissionsData GetUserPermissions(int UserID)
        {
            this.CheckSecViewPermission();
            SecurityProvider.UserPermissions perm = this.bdeSecurity.GetUserPermissions(UserID);
            return new UserPermissionsData { Denied = perm.Denied, ParentUserID = perm.ParentUserID, Permissions = perm.Permissions, UserID = perm.UserID, UserName = perm.UserName };
        }

        public UserPermissionsData GetUserPermissions(string userName, string password)
        {
            SecurityProvider.UserPermissions perm = this.bdeSecurity.GetUserPermmissions(userName, password);
            return new UserPermissionsData { Denied = perm.Denied, ParentUserID = perm.ParentUserID, Permissions = perm.Permissions, UserID = perm.UserID, UserName = perm.UserName };
        }

        public void Rename(int ObjectID, string NewName)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.Rename(ObjectID, NewName);
        }

        public void SetPermission(int ObjectID, string UserName, PermissionState state)
        {
            this.CheckAdminPermission();
            this.bdeSecurity.SetPermission(ObjectID, UserName, state);
        }

        public UserPermissionsData User(string user)
        {
            this.CheckSecViewPermission();
            SecurityProvider.UserPermissions perm = this.bdeSecurity.User(user);
            return new UserPermissionsData { Denied = perm.Denied, ParentUserID = perm.ParentUserID, Permissions = perm.Permissions, UserID = perm.UserID, UserName = perm.UserName };
        }

        public IEnumerable<UserPassword> getAllUsers()
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.getAllUsers();
        }

        public IEnumerable<SecurityObject> GetAllObjects()
        {
            this.CheckSecViewPermission();
            return this.bdeSecurity.GetObjectTree();
        }
    }
}

