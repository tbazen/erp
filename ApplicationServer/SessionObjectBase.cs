namespace INTAPS.ClientServer
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class SessionObjectBase<BDEType> where BDEType: BDEBase
    {
        protected BDEType _bde;
        protected UserSessionData m_Session;
        public string sessionID
        {
            get
            {
                return ApplicationServer.getSessionID(m_Session);
            }
        }
        public SessionObjectBase(UserSessionData Session, BDEType bde)
        {
            this.m_Session = Session;
            this._bde = bde;
        }


        public int WriteAddAudit(string AddType, string RowKey, int PID)
        {
            return ApplicationServer.SecurityBDE.WriteAddAudit(this.m_Session.Permissions.UserName, this.m_Session.Permissions.UserID, base.GetType().Name + "." + AddType, RowKey, PID);
        }

        public int WriteAudit(string Operation, string Data1, string Data2, int PID)
        {
            return ApplicationServer.SecurityBDE.WriteAudit(this.m_Session.Permissions.UserName, this.m_Session.Permissions.UserID, base.GetType().Name + "." + Operation, Data1, Data2, PID);
        }

        public int WriteDataUpdateAudit(string UpdateType, string RowKey, DataTable Data, int PID)
        {
            return ApplicationServer.SecurityBDE.WriteDataUpdateAudit(this.m_Session.Permissions.UserName, this.m_Session.Permissions.UserID, base.GetType().Name + "." + UpdateType, RowKey, Data, PID);
        }

        public int WriteExecuteAudit(string CommandName, int PID)
        {
            return ApplicationServer.SecurityBDE.WriteExecuteAudit(this.m_Session.Permissions.UserName, this.m_Session.Permissions.UserID, base.GetType().Name + "." + CommandName, PID);
        }

        public int WriteMethodCallAudit(string MethodName, string note, int PID, params object[] MethodPars)
        {
            return ApplicationServer.SecurityBDE.WriteObjectAudit(this.m_Session.Permissions.UserName, this.m_Session.Permissions.UserID, base.GetType().Name + "." + MethodName, note, MethodPars, PID);
        }

        public int WriteObjectAudit(string UpdateType, string RowKey, object obj, int PID)
        {
            return ApplicationServer.SecurityBDE.WriteObjectAudit(this.m_Session.Permissions.UserName, this.m_Session.Permissions.UserID, base.GetType().Name + "." + UpdateType, RowKey, obj, PID);
        }
        public virtual void closeSession()
        {
        }

    }
}