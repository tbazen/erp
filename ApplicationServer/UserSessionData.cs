namespace INTAPS.ClientServer
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class UserSessionData
    {
        public DateTime CreateTime;
        public DateTime LastAction;
        public SecurityProvider.UserPermissions Permissions;
        public Dictionary<Type, object> SessionObjects;
        public string Source;

        public double IdleTime
        {
            get
            {
                return DateTime.Now.Subtract(this.LastAction).TotalSeconds;
            }
        }

        public double TotalTime
        {
            get
            {
                return DateTime.Now.Subtract(this.CreateTime).TotalSeconds;
            }
        }
    }
}

