namespace INTAPS.ClientServer
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class AccessDeniedException : Exception, ISerializable
    {
        public AccessDeniedException() : base("Access denied")
        {
        }

        public AccessDeniedException(string msg) : base(msg)
        {
        }

        public AccessDeniedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}

