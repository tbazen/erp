﻿    using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;

namespace INTAPS.ClientServer
{
    
    [Serializable]
    [SingleTableObject(tableName="Audit")]
    public class AuditInfo
    {
        [IDField]
        public long ID;
        [DataField("DateTime")]
        public DateTime dateTime;
        [DataField("UserName")]
        public string userName;
    }
}
