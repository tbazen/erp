namespace INTAPS.ClientServer
{
    using INTAPS.RDBMS;
    using System;
    using System.Reflection;

    public class BDEChangeFilterAttribute : Attribute
    {
        [DataField]
        public Type[] filterType;
        public IBDEChangeFilter instance = null;

        public BDEChangeFilterAttribute(params Type[] filterObject)
        {
            this.filterType = filterObject;
        }

        public void instantiate(Type t)
        {
            ConstructorInfo constructor = t.GetConstructor(new Type[0]);
            if (constructor == null)
            {
                throw new Exception("Couldn't load change filter class constructor for " + t);
            }
            this.instance = constructor.Invoke(new object[0]) as IBDEChangeFilter;
            if (this.instance == null)
            {
                throw new Exception("Filter class doesn't implement IBDEChangeFilter interface. class name:" + t);
            }
        }
    }
}

