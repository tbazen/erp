﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.ClientServer
{
    public class BinObject
    {
        public byte[] data;
        public BinObject(object val)
        {
            if (val == null)
                data = null;
            else
            {
                using (var m = new System.IO.MemoryStream())
                {
                    new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Serialize(m, val);
                    m.Seek(0, System.IO.SeekOrigin.Begin);
                    data = new byte[m.Length];
                    m.Read(data, 0, data.Length);
                }
            }
        }
        public object Deserialized()
        {
            if (data == null)
                return null;
            using (var m = new System.IO.MemoryStream(data))
            {
                return new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter().Deserialize(m);
            }
        }
    }
}
