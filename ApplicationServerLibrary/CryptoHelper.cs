namespace INTAPS.ClientServer
{
    using System;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Security.Cryptography;

    public class CryptoHelper
    {
        public static bool CheckHash(object obj, byte[] hash)
        {
            byte[] buffer = ComputeHash(obj);
            if (buffer.Length == hash.Length)
            {
                for (int i = 0; i < buffer.Length; i++)
                {
                    if (buffer[i] != hash[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public static byte[] ComputeHash(object obj)
        {
            if (obj == null)
            {
                return new byte[0];
            }
            MemoryStream serializationStream = new MemoryStream();
            new BinaryFormatter().Serialize(serializationStream, obj);
            serializationStream.Close();
            return MD5.Create().ComputeHash(serializationStream.ToArray());
        }
    }
}

