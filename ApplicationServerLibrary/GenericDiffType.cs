using System;
using System.Collections.Generic;
using System.Xml.Serialization;
namespace INTAPS.ClientServer
{
    public enum GenericDiffType
    {
        Add = 1,
        Delete = 2,
        Replace = 3,
        NoChange=0
    }
    [Serializable]
    public class DiffObject<ObjectType> where ObjectType:class
    {
        public GenericDiffType diffType;
        public ObjectType data;
        public DiffObject()
        {
            diffType = GenericDiffType.NoChange;
            data = null;
        }
        public DiffObject(ObjectType data,GenericDiffType type)
        {
            this.diffType = type;
            this.data = data;
        }

    }
    public interface IMergable<T>
    {
        void merge(T data);
    }
    [Serializable] public class DiffEntry<T, IDType> : DiffObject<T>
        where T : class,IIDObject<IDType>
        where IDType : IComparable<IDType>
    {
        public IDType objectID;
        public DiffEntry()
        {
            
        }
        public static DiffEntry<T,IDType>[] createNoChangeArray(T[] list)
        {
            if (list == null)
                return null;
            DiffEntry<T, IDType>[] ret = new DiffEntry<T, IDType>[list.Length];
            for (int i = 0; i < list.Length; i++)
                ret[i] = new DiffEntry<T, IDType>(list[i], GenericDiffType.NoChange);
            return ret;
        }
        public DiffEntry(T d,GenericDiffType dt)
        {
            this.data = d;
            this.objectID = d.ObjectIDVal;
            this.diffType = dt;
        }
        public DiffEntry(IDType id)
        {
            this.objectID = id;
            this.diffType = GenericDiffType.Delete;
            this.data = null;
        }
        public void upate(DiffEntry<T,IDType> diff)
        {
            switch (diff.diffType)
            {
                case GenericDiffType.Add:
                    if(this.diffType!=GenericDiffType.NoChange)
                        throw new ServerUserMessage("Invalid merge operation");
                    break;
                case GenericDiffType.NoChange:
                    return;
                default:
                    break;
            }
            this.data = diff.data;
            this.diffType = diff.diffType;
        }
    }

    [Serializable]
    public class DiffTableSerializationSerogate<T, IDType>
        where T : class,IIDObject<IDType>
        where IDType : IComparable<IDType>
    {
        public T[] _dbData;
        public DiffEntry<T, IDType>[] _diffs;
        public void loadForm(DiffTable<T, IDType> data)
        {
            _dbData = new T[data._dbData.Count];
            int i = 0;
            foreach (KeyValuePair<IDType, T> kv in data._dbData)
                _dbData[i++] = kv.Value;
            _diffs = new DiffEntry<T, IDType>[data._diffs.Count];
            data._diffs.CopyTo(_diffs);
        }
        public void saveTo(DiffTable<T, IDType> data)
        {
            data._dbData = new Dictionary<IDType, T>();
            data.setDBData(_dbData);
            data.setDiffs(_diffs);
            
        }
    }
    [Serializable]
    public class DiffTable<T> : DiffTable<T, int>, IXmlSerializable, IEnumerable<DiffEntry<T, int>>
        where T : class,IIDObject
    {

    }
    [Serializable] public class DiffTable<T, IDType> :IEnumerable<DiffEntry<T, IDType>>
        where T : class,IIDObject<IDType>
        where IDType : IComparable<IDType>
       
    {
        public Dictionary<IDType, T> _dbData = new Dictionary<IDType, T>();
        public Dictionary<IDType, DiffEntry<T, IDType>> _diffKeys = new Dictionary<IDType, DiffEntry<T, IDType>>();
        public List<DiffEntry<T, IDType>> _diffs=new List<DiffEntry<T,IDType>>();
       public void setDiffs(IEnumerable<DiffEntry<T, IDType>> diffs)
        {
            this._diffs=new List<DiffEntry<T,IDType>>();
            this._diffKeys = new Dictionary<IDType, DiffEntry<T, IDType>>();
            foreach(DiffEntry<T, IDType> dd in diffs)
            {
                this._diffs.Add(dd);
                this._diffKeys.Add(dd.objectID, dd);
            }
        }
       [Serializable] class DiffSourceEnum : IEnumerator<DiffEntry<T, IDType>>
        {
            public DiffTable<T, IDType> parent;
            bool currentDiffs = false;
            IEnumerator<DiffEntry<T, IDType>> _diffEnum = null;
            IEnumerator<KeyValuePair<IDType,T>> _dbEnum = null;
            public DiffEntry<T, IDType> Current
            {
                get
                {
                    if (currentDiffs)
                        return _diffEnum.Current;
                    return new DiffEntry<T, IDType>() { data = _dbEnum.Current.Value, diffType = GenericDiffType.NoChange };
                }
            }

            public void Dispose()
            {
                if (_diffEnum != null)
                    _diffEnum.Dispose();
                if (_dbEnum != null)
                    _dbEnum.Dispose();
            }

            object System.Collections.IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public bool MoveNext()
            {
                if (!currentDiffs)
                {
                    if (parent._dbData != null)
                    {
                        if (_dbEnum == null)
                            _dbEnum = parent._dbData.GetEnumerator();
                        while (_dbEnum.MoveNext())
                        {
                            if (parent._diffKeys.ContainsKey(_dbEnum.Current.Key))
                            {
                                DiffEntry<T, IDType> d = parent._diffKeys[_dbEnum.Current.Key];
                                if (d.diffType != GenericDiffType.Delete && d.diffType != GenericDiffType.Replace)
                                    return true;
                            }
                            else
                                return true;
                        }
                    }
                    currentDiffs = true;
                }
                if (_diffEnum == null)
                    _diffEnum = parent._diffs.GetEnumerator();
                while (_diffEnum.MoveNext())
                {
                    if (_diffEnum.Current.diffType == GenericDiffType.Add || _diffEnum.Current.diffType == GenericDiffType.Replace)
                        return true;
                }
                return false;
            }

            public void Reset()
            {
                currentDiffs = false;
                if (_dbEnum != null)
                    _dbEnum.Reset();
            }
        }

        public void merge(DiffTable<T, IDType> other)
        {
            if (other == null)
                return;
            List<KeyValuePair<IDType, T>> newKv = new List<KeyValuePair<IDType, T>>();
            foreach(KeyValuePair<IDType,T> kv in other._dbData)
            {
                if (!_dbData.ContainsKey(kv.Key))
                    newKv.Add(kv);
            }
            foreach (KeyValuePair<IDType, T> kv in newKv)
                _dbData.Add(kv.Key,kv.Value);
            foreach(DiffEntry<T, IDType> d in other._diffs)
            {
                addUpate(d);
            }
        }

        public void addUpate(DiffEntry<T, IDType> d)
        {
            DiffEntry<T, IDType> thisD = _diffKeys.ContainsKey(d.objectID) ? _diffKeys[d.objectID] : null;
            bool merged = false;
            switch (d.diffType)
            {
                case GenericDiffType.Add:
                case GenericDiffType.Replace:
                    if (thisD != null)
                    {
                        if (thisD.diffType == GenericDiffType.Add || thisD.diffType == GenericDiffType.Replace)
                        {
                            if (d.data is IMergable<T> && thisD.data is IMergable<T>)
                            {
                                merged = true;
                                ((IMergable<T>)thisD.data).merge(d.data);
                            }
                            else
                            {
                                _diffs.Remove(thisD);
                                _diffKeys.Remove(thisD.objectID);
                                d.diffType = thisD.diffType;
                            }
                        }
                        else
                        {
                            _diffs.Remove(thisD);
                            _diffKeys.Remove(thisD.objectID);
                        }
                    }
                    break;
                case GenericDiffType.Delete:
                    if (thisD != null )
                    {
                        _diffs.Remove(thisD);
                        _diffKeys.Remove(thisD.objectID);
                        merged = thisD.diffType != GenericDiffType.Delete;
                    }
                    break;
            }
            if (!merged)
            {
                _diffs.Add(d);
                _diffKeys.Add(d.objectID, d);
            }
        }
        public void setDBData(IEnumerable<T> dbData)
        {
            foreach (T x in dbData)
            {
                _dbData.Add(x.ObjectIDVal, x);
            }
        }




        public IEnumerator<DiffEntry<T, IDType>> GetEnumerator()
        {
            return new DiffSourceEnum() { parent = this };
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }


        public System.Xml.Schema.XmlSchema GetSchema()
        {
            //XmlSerializer s = INTAPS.SerializationExtensions.getSerializer(typeof(T));
            throw new ServerUserMessage("Get schema not expected");
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {
            object obj = reader.ReadElementContentAs(typeof(byte[]), null);
            //int len = reader.ReadElementContentAsInt();
            //byte[] data = new byte[len];
            //reader.ReadElementContentAsBase64(data, 0, len);
            byte[] data = null;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter b = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream(data))
            {
                DiffTableSerializationSerogate<T, IDType> srg = b.Deserialize(ms) as DiffTableSerializationSerogate<T, IDType>;
                srg.saveTo(this);
            }

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            DiffTableSerializationSerogate<T, IDType> srg = new DiffTableSerializationSerogate<T, IDType>();
            srg.loadForm(this);
            
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter b = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] data;
            using(System.IO.MemoryStream ms=new System.IO.MemoryStream())
            {
                b.Serialize(ms, srg);
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                data=new byte[ms.Length];
                ms.Read(data, 0, data.Length);
            }
            writer.WriteValue(data);
        }
    }
}
     

