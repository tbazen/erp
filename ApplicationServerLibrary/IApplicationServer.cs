namespace INTAPS.ClientServer
{
    using System;

    public interface IApplicationServer
    {
        bool isAlive(string sessionID);
        void CloseSession(string SessionID);
        string CreateUserSession(string UserName, string Password, string Source);
        UserSessionInfo[] GetActiveSessions();
        UserSessionInfo getSessionInfo(string sessionID);
        void keepAlive(string sessionID);
        void pingServer();

        InstantMessageData[] getInstantMessages(string sessionID);
        void setRead(string sessionID, string[] messageIDs);
        string sendMessage(string sessionID, InstantMessageData msg);
        TLicense.LicenseData getLicenseData();
    }
}

