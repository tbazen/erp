namespace INTAPS.ClientServer
{
    using System;
    using System.Runtime.InteropServices;

    public interface IBDEChangeFilter
    {
        bool CanCreate(int AID, object objectData, out string userMessage);
        bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues);
        bool CanUpdate(int AID, object objectData, out string userMessage);
    }
}

