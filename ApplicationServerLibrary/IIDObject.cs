namespace INTAPS.ClientServer
{
    using System;

    public interface IIDObject<IDType> where IDType:IComparable<IDType>
    {
        IDType ObjectIDVal { get; set; }
    }
    public interface IIDObject:IIDObject<int>
    {
    }     
    [Serializable]
    public class VersionedID
    {
        public int id;
        public long version;
    

        public VersionedID(int id, long version)
        {
            this.id = id;
            this.version= version;
        }
        public override string ToString()
        {
            return id + ", " + version;
        }
        public VersionedID()
        {
            id = -1;
            version = -1;
        }
        public static int compareVersion(long v1, long v2)
        {
            if (v1 == v2)
                return 0;
            if (v1 == -1)
                return 1;
            if (v2 == -1)
                return -1;
            return v1.CompareTo(v2);
        }
           
    }
}

