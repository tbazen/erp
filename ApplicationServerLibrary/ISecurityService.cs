namespace INTAPS.ClientServer
{
    using System;
    using System.Data;
    using System.Runtime.InteropServices;

    public interface ISecurityService
    {
        void ChangeClass(string SessionID, int ObjectID, SecurityObjectClass Class);
        void ChangeOwner(string SessionID, int ObjectID, int NewOwner);
        void ChangePassword(string SessionID, string newpassword);
        void ChangePassword(string SessionID, string UserName, string newpassword);
        int CreateObject(string SessionID, int ParentOID, string Name);
        void CreateUser(string SessionID, string ParentUser, string UserName, string Password);
        void DeleteObject(string SessionID, int ObjectID);
        void DeleteUser(string SessionID, string UserName);
        string[] GetAllAuditOperations(string SessionID);
        string[] GetAllUsers(string SessionID);
        DataTable GetAudit(string SessionID, string[] AuditType, string[] Users, bool Date, DateTime from, bool DateTo, DateTime to, string[] Data1, int index, int PageSize, out int NRecords);
        string GetAuditData2(string SessionID, int AuditID);
        SecurityObjectData[] GetChildList(string SessionID, int ObjectID);
        string[] GetChildUsers(string SessionID, string User);
        string[] GetDeniedEntries(string SessionID, int ObjectID);
        string[] GetDeniedUsers(string SessionID, int ObjectID);
        string GetNameForID(string SessionID, int UserID);
        SecurityObjectData GetObject(string SessionID, int ObjectID);
        string GetObjectFullName(string SessionID, int OID);
        int GetObjectID(string SessionID, string Name);
        string GetParentUser(string SessionID, string UserName);
        string[] GetPermitedUsers(string SessionID, int ObjectID);
        string[] GetPermittedEntries(string SessionID, int ObjectID);
        int GetUIDForName(string SessionID, string Name);
        UserPermissionsData GetUserPermissions(string SessionID, int UserID);
        UserPermissionsData GetUserPermissions(string SessionID, string userName, string password);
        void Rename(string SessionID, int ObjectID, string NewName);
        void SetPermission(string SessionID, int ObjectID, string UserName, PermissionState state);
        UserPermissionsData User(string SessionID, string user);

        bool IsPermited(string SessionID, string obj);
    }
}

