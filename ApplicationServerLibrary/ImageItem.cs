namespace INTAPS.ClientServer
{
    using INTAPS.RDBMS;
    using System;

    [Serializable, SingleTableObject]
    public class ImageItem
    {
        public byte[] imageData;
        [IDField]
        public int imageID;
        [IDField]
        public int itemIndex;
        [DataField]
        public string title;

        public ImageItem()
        {
            this.imageID = -1;
            this.itemIndex = -1;
            this.title = null;
            this.imageData = null;
        }

        public ImageItem(int id, int index, string title, byte[] data)
        {
            this.imageID = id;
            this.itemIndex = index;
            this.title = title;
            this.imageData = data;
        }
    }
}

