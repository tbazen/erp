﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS.ClientServer
{
    [Serializable]
    public class InstantMessageData
    {
        public string messageID;
        public string from;
        public string to;
        public DateTime dateSent;
        public string messages;
        public int status;
    }
}
