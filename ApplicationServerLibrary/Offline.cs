﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using INTAPS.ClientServer;

namespace INTAPS.ClientServer
{
    [Serializable]
    public class GenericDiff
    {
        [DataField]
        public string objectID;
        [DataField]
        public DateTime changeDate;
        [DataField]
        public GenericDiffType changeType;
        [DataField]
        public int typeID;

        public object diffData;

        public GenericDiff()
        {
        }
        
        public static GenericDiff getUpdateDiff(string objectID, object data)
        {
            GenericDiff ret = new GenericDiff();
            ret.objectID = objectID;
            ret.changeDate = DateTime.Now;
            if (data == null)
                throw new Exception("update with null data not supported");
            ret.changeType = GenericDiffType.Replace;
            ret.typeID = ObjectTable.getTypeID(data.GetType(), true);
            ret.diffData = data;
            return ret;
        }
        public static GenericDiff getAddDiff(int objectID, object data)
        {
            return getAddDiff(objectID.ToString(), data);
        }
        public static GenericDiff getAddDiff(string objectID, object data)
        {
            GenericDiff ret = new GenericDiff();
            ret.objectID = objectID;
            ret.changeDate = DateTime.Now;
            if (data == null)
                throw new Exception("Adding null data not supported");
            ret.changeType = GenericDiffType.Add;
            ret.typeID = ObjectTable.getTypeID(data.GetType(), true);
            ret.diffData = data;
            return ret;
        }
        public static GenericDiff getDeleteDiff(Type t, string objectID)
        {
            GenericDiff ret = new GenericDiff();
            ret.objectID = objectID;
            ret.changeDate = DateTime.Now;
            ret.changeType = GenericDiffType.Delete;
            ret.typeID = ObjectTable.getTypeID(t, true);
            ret.diffData = null;
            return ret;
        }

        public static GenericDiff getAddDiff(int key1, int key2, object data)
        {
            return getAddDiff(key1 + "~" + key2, data);
        }
        public static GenericDiff getAddDiff(int key1, long key2, object data)
        {
            return getAddDiff(key1 + "~" + key2, data);
        }
        public static GenericDiff getUpdateDiff(int key1, int key2, object data)
        {
            return getUpdateDiff(key1 + "~" + key2, data);
        }
        public static GenericDiff getUpdateDiff(int key1, long key2, object data)
        {
            return getUpdateDiff(key1 + "~" + key2, data);
        }
        public static GenericDiff getDeleteDiff(Type type, int objectID)
        {
            return getDeleteDiff(type, objectID.ToString());
        }

        public static GenericDiff getDeleteDiff(Type type, int key1, int key2)
        {
            return getDeleteDiff(type, key1 + "~" + key2);
        }
        public static GenericDiff getDeleteDiff(Type type, int key1, long key2)
        {
            return getDeleteDiff(type, key1 + "~" + key2);
        }
        public static GenericDiff getUpdateDiff(int objectID, object data)
        {
            return getUpdateDiff(objectID.ToString(), data);
        }

    }
    [Serializable]

    [SingleTableObject]
    public class OfflineMessage
    {
        [DataField]
        public int id;
        [DataField]
        public DateTime sentDate = DateTime.Now;
        public object messageData;
        [DataField]
        public bool delivered = false;
        [DataField]
        public DateTime deliveryDate = DateTime.Now;
        [DataField]
        public int previousMessageID = -1;
    }


    [Serializable]
    public class MessageAcknowledgeMessage
    {
        public int paymentCenterID;
        public int messageID;
    }
    [Serializable]
    public class MessageList : INTAPS.ClientServer.SignedData<OfflineMessage[]>
    {
        public MessageList()
        {
        }
        public MessageList(OfflineMessage[] message)
            : base(message)
        {
        }
    }

    [Serializable]
    [SingleTableObject]
    public class ObjectInstance
    {
        [IDField]
        public string id;
        [DataField]
        public int typeID;
        public object data;
    }
}
