namespace INTAPS.ClientServer
{
    using System;

    public enum PermissionState
    {
        Permit,
        Deny,
        Undefined
    }
}

