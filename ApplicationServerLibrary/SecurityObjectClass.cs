namespace INTAPS.ClientServer
{
    using System;

    public enum SecurityObjectClass
    {
        soInherit = 1,
        soNoInherit = 2
    }
}

