namespace INTAPS.ClientServer
{
    using System;

    [Serializable]
    public class SecurityObjectData
    {
        public SecurityObjectClass Class;
        public string FullName;
        public int ID;
        public string Name;
        public int Owner;
        public string OwnerName;
        public int PID;
        public int ChildCount;
    }
}

