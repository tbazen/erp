namespace INTAPS.ClientServer
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ServerConfigurationError : Exception, ISerializable
    {
        public ServerConfigurationError() : base("Incorrect system configuration")
        {
        }

        public ServerConfigurationError(string msg) : base(msg)
        {
        }

        public ServerConfigurationError(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

