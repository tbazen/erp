namespace INTAPS.ClientServer
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ServerUserMessage : Exception, ISerializable
    {
        public ServerUserMessage() : base("Incorrect request")
        {
        }

        public ServerUserMessage(string msg) : base(msg)
        {
        }

        public ServerUserMessage(string msg,params object[] pars)
            : base(string.Format(msg,pars))
        {
        }
        public ServerUserMessage(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
    [Serializable]
    public class SessionExpiredException : Exception, ISerializable
    {
        public SessionExpiredException()
            : base("Incorrect request")
        {
        }

        public SessionExpiredException(string msg)
            : base(msg)
        {
        }

        public SessionExpiredException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

