namespace INTAPS.ClientServer
{
    using System;

    [Serializable]
    public class SignedData<ObjectType> where ObjectType: class
    {
        public ObjectType data;
        public byte[] signature;

        public SignedData()
        {
        }

        public SignedData(ObjectType data)
        {
            this.data = data;
            this.signature = CryptoHelper.ComputeHash(data);
        }

        public bool Verify()
        {
            return CryptoHelper.CheckHash(this.data, this.signature);
        }
    }
}

