﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.ClientServer
{
    public class TypeObject
    {
        public String TypeName { get; set; }
        public Object Val { get; set; }
        public TypeObject(object val)
        {
            this.Val = val;
            if (val == null)
                this.TypeName = null;
            else
            {
                var t = val.GetType();
                var assemblyName = t.Assembly.GetName();
                if (assemblyName.Name.StartsWith("System"))
                    this.TypeName = t.FullName;
                else
                    this.TypeName= t.FullName + "," + t.Assembly.GetName().Name;
            }
        }
        public Object GetConverted()
        {
            if (string.IsNullOrEmpty(TypeName))
                return null;
            var t =  Type.GetType(TypeName);
            if (t == null)
                throw new ArgumentException($"{TypeName} couldn't be loaded");
            if (Val is JObject)
            {
                return ((JObject)Val).ToObject(t);
            }
            if (Val is JArray)
            {
                return ((JArray)Val).ToObject(t);
            }
            if (Val == null)
                return null;
            if (!Val.GetType().Equals(t))
            {
                return Convert.ChangeType(Val, t);
            }
            return Val;
        }

    }
}
