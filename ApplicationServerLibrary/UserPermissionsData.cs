namespace INTAPS.ClientServer
{
    using System;

    [Serializable]
    public class UserPermissionsData
    {
        public int[] Denied;
        public int ParentUserID;
        public int[] Permissions;
        public int UserID;
        public string UserName;
    }
}

