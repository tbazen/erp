namespace INTAPS.ClientServer
{
    using System;

    [Serializable]
    public class UserSessionInfo
    {
        public double IdleSeconds;
        public DateTime LogedInTime;
        public string SessionID;
        public string Source;
        public string UserName;
        public int UserID;
    }
}

