using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class ButtonGrid : Panel
    {
        #region events

        public event GenericEventHandler<ButtonGrid, ButtonGridItem> ActiveButtonChanging;
        public event GenericEventHandler<ButtonGrid, ButtonGridItem> AcitveButtonChanged;

        bool _suspenPaint = false;
        public void SuspendPaint()
        {
            _suspenPaint = true;
        }

        public event GenericEventHandler<ButtonGrid, ButtonGridPage> AcitvePageChanging;
        public event GenericEventHandler<ButtonGrid, ButtonGridPage> ActivePageChanged;
        public event GenericEventHandler<ButtonGrid, ButtonGridItem> LeafButtonClicked;

        void raiseActvieButtonChanging()
        {
            GenericEventHandler<ButtonGrid, ButtonGridItem> eventCopy = ActiveButtonChanging;
            if (eventCopy != null)
                eventCopy(this, _activeButtonItem);
        }

        void raiseAcitveButtonChanged()
        {
            GenericEventHandler<ButtonGrid, ButtonGridItem> eventCopy = AcitveButtonChanged;
            if (eventCopy != null)
                eventCopy(this, _activeButtonItem);
        }

        public void ResumePaint()
        {
            _suspenPaint = false;
        }

        void raiseAcitvePageChanging()
        {
            GenericEventHandler<ButtonGrid, ButtonGridPage> eventCopy = AcitvePageChanging;
            if (eventCopy != null)
                eventCopy(this, _activePage);
        }

        void raiseActivePageChanged()
        {
            GenericEventHandler<ButtonGrid, ButtonGridPage> eventCopy = ActivePageChanged;
            if (eventCopy != null)
                eventCopy(this, _activePage);
        }

        void raiseLeafButtonClicked(ButtonGridItem leafButton)
        {
            GenericEventHandler<ButtonGrid, ButtonGridItem> eventCopy = LeafButtonClicked;
            if (eventCopy != null)
                eventCopy(this, leafButton);
        }

        #endregion

        private ButtonGridBodyButtonItem _RootButtonItem;
        ButtonGridPage _HeaderPage;
        List<ButtonGridPage> _bodyPages;
        ButtonGridPage _pageNavigator;
        ButtonGridPage _activePage = null;
        ButtonGridItem _activeButtonItem;
        ItemView _activeItemView;
        ItemView _snapedItemView;
        ItemView _selectedHeaderButtonView;
        bool _showLeafNode = true;
        GroupButtonView _groupButtonView;
        BodyButtonView _buttonBodyView;
        HeaderButtonView _buttonHeaderView;
        PageNavigatorButtonView _pageNavigatorView;
        MyGraphics _buttonGridGraphics;
        bool _zoomAnimate = true;
        DateTime _prevTime;
        Timer _timer;
        TextBox _filterBox;
        Label _filterLabel;
        ButtonGridStyles _rootStyle;
        ButtonGridStyles _rootStyleButtonBody, _rootStyleHeaderButton, _rootStyleNavigatorButton, _rootStyleGroupButton;
        StylesCollection _RootStylesCollection;
        StylesCollection _RootStylesCollectionBodyButton;
        StylesCollection _RootStylesCollectionGroupButton;
        StylesCollection _RootStylesCollectionHeaderButton;
        StylesCollection _RootStylesCollectionNavigatorButton;

        bool _visibleHeader;


        #region Button Width and Heights

        //public int GroupWidth
        //{
        //    get
        //    {
        //        return groupButtonView.DefaultButtonWidth;
        //    }
        //    set
        //    {
        //        groupButtonView.DefaultButtonWidth = value;
        //    }
        //}

        //public int GroupHeight
        //{
        //    get
        //    {
        //        return groupButtonView.DefaultButtonHeight;
        //    }
        //    set
        //    {
        //        groupButtonView.DefaultButtonHeight = value;
        //    }
        //}

        //public int ButtonWidth
        //{
        //    get
        //    {
        //        return buttonBodyView.DefaultButtonWidth;
        //    }
        //    set
        //    {
        //        buttonBodyView.DefaultButtonWidth = value;
        //    }
        //}

        //public int ButtonHeight
        //{
        //    get
        //    {
        //        return buttonBodyView.DefaultButtonHeight;
        //    }
        //    set
        //    {
        //        buttonBodyView.DefaultButtonHeight = value;
        //    }
        //}

        //public int ButtonWidth
        //{
        //    get
        //    {
        //        return buttonHeaderView.DefaultButtonWidth;
        //    }
        //    set
        //    {
        //        buttonHeaderView.DefaultButtonWidth = value;
        //    }
        //}

        //public int ButtonHeight
        //{
        //    get
        //    {
        //        return buttonHeaderView.DefaultButtonHeight;
        //    }
        //    set
        //    {
        //        buttonHeaderView.DefaultButtonHeight = value;
        //    }
        //}

        //public int ButtonWidth
        //{
        //    get
        //    {
        //        return pageNavigatorView.DefaultButtonWidth;
        //    }
        //    set
        //    {
        //        pageNavigatorView.DefaultButtonWidth = value;
        //    }
        //}

        //public int ButtonHeight
        //{
        //    get
        //    {
        //        return pageNavigatorView.DefaultButtonHeight;
        //    }
        //    set
        //    {
        //        pageNavigatorView.DefaultButtonHeight = value;

        //    }
        //}

        #endregion

        static ButtonGrid()
        {
        }

        public ButtonGrid()
        {
            base.SetStyle(ControlStyles.Selectable, true);
            this.TabStop = true;
            _buttonGridGraphics = new MyGraphics(this);

            _visibleHeader = true;

            InitializeApperance();

             _RootButtonItem = new ButtonGridBodyButtonItem(false);
            _RootButtonItem.ApperanceBody.StylesCollection.setParentStyleCollection(_RootStylesCollectionBodyButton );
            _RootButtonItem.ApperanceHeader.StylesCollection.setParentStyleCollection(_RootStylesCollectionHeaderButton );
            _RootButtonItem.ApperanceBody.AddText("Home");
            _RootButtonItem.ApperanceHeader.AddText("Home");
            _HeaderPage = new ButtonGridPage();
            _bodyPages = new List<ButtonGridPage>();

            //ItemRelationShips.AddRelationShip(new ParentChildRelationShip<ButtonGridItem>(null, rootButtonItem, null ));
            _activeButtonItem = _RootButtonItem;

            ButtonGridPage rootPage = new ButtonGridPage();
            _bodyPages.Add(rootPage);
            _activePage = rootPage;

            _pageNavigator = new ButtonGridPage();
            _pageNavigator.Height = 40;

            _filterBox = new TextBox();
            _filterBox.Font = new Font("Arial", 12);
            _filterBox.BorderStyle = BorderStyle.None;
            _filterBox.BackColor = this.BackColor;
            _filterLabel = new Label();
            _filterLabel.Width = 150;
            _filterBox.Height = _filterLabel.Height;
            _filterLabel.Text = "Search";
            _filterLabel.Font = new Font("Arial", 12);
            //filterBox.Dock = DockStyle.Top;
            _filterBox.TextChanged += new EventHandler(filterBox_TextChanged);
            _filterBox.KeyDown += new KeyEventHandler(filterBox_KeyDown);
            _timer = new Timer();
            _timer.Interval = 1000;
            _timer.Tick += new EventHandler(timer_Tick);

        }

        public TextBox FilterBox
        {
            get
            {
                return _filterBox;
            }
        }

        public Label FilterLabel
        {
            get
            {
                return _filterLabel;
            }
        }

        public Color FilterBoxBackColor
        {
            get
            {
                return _filterBox.BackColor;
            }
            set
            {
                _filterBox.BackColor = value;
            }
        }

        public Font FilterBoxFont
        {
            get
            {
                return _filterBox.Font;
            }
            set
            {
                _filterBox.Font = value;
            }
        }

        public ButtonGridBodyButtonItem rootButtonItem
        {
            get
            {
                return _RootButtonItem;
            }
        }

        public ButtonGridPage ActivePage
        {
            get { return _activePage; }
            set
            {
                if (value == null) return;
                changeActivePage(value, true);
            }
        }

        public ButtonGridItem ActiveButtonItem
        {
            get { return _activeButtonItem; }

        }

        public bool VisibleHeader
        {
            get
            {
                return _visibleHeader;
            }
            set
            {
                _visibleHeader = value;
            }
        }
 
        #region Styles Properties

        public ButtonGridStyles RootStyle
        {
            get
            {
                return _rootStyle;
            }
        }

        public ButtonGridStyles RootStyleButtonBody
        {
            get
            {
                return _rootStyleButtonBody;
            }
        }

        public ButtonGridStyles RootStyleHeaderButton
        {
            get
            {
                return _rootStyleHeaderButton;
            }
        }

        public ButtonGridStyles RootStyleNavigatorButton
        {
            get
            {
                return _rootStyleNavigatorButton;
            }
        }

        public ButtonGridStyles RootStyleGroupButton
        {
            get
            {
                return _rootStyleGroupButton;
            }
        }

        public StylesCollection RootStylesCollection
        {
            get
            {
                return _RootStylesCollection;
            }
        }

        public StylesCollection RootStylesCollectionBodyButton
        {
            get
            {
                return _RootStylesCollectionBodyButton;
            }
        }

        public StylesCollection RootStylesCollectionGroupButton
        {
            get
            {
                return _RootStylesCollectionGroupButton;
            }
        }

        public StylesCollection RootStylesCollectionHeaderButton
        {
            get
            {
                return _RootStylesCollectionHeaderButton;
            }
        }

        public StylesCollection RootStylesCollectionNavigatorButton
        {
            get
            {
                return _RootStylesCollectionNavigatorButton;
            }
        }

        #endregion

        public bool ShowLeafNode
        {
            get { return _showLeafNode; }
            set
            {
                _showLeafNode = value;
                if (rootButtonItem == null) return;
                setActiveButtonView(rootButtonItem);
            }
        }

        public List<ButtonGridPage> Pages
        {
            get { return _bodyPages; }
        }


        public void InitializeApperance()
        {
            _rootStyle = new ButtonGridStyles();
            _rootStyle.Add(StyleType.CornerRadius, 15);
            _rootStyle.Add(StyleType.Font, new Font("Arial", 12));
            _rootStyle.Add(StyleType.Position, Point.Empty);

            Pen DefaultPen = new Pen(Color.Silver);
            DefaultPen.Width = 2;
            DefaultPen.Alignment = System.Drawing.Drawing2D.PenAlignment.Center;
            DefaultPen.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;
            _rootStyle.Add(StyleType.BroderPen, DefaultPen);

            Brush fillBrush = (Brush)new SolidBrush(Color.FromArgb(120, Color.LightBlue));
            _rootStyle.Add(StyleType.FillBrush, fillBrush);

            Brush fontBrush = (Brush)new SolidBrush(Color.Black);
            _rootStyle.Add(StyleType.FontBrush, fontBrush);

            StringFormat textFormat = new StringFormat();
            textFormat.Alignment = StringAlignment.Center;
            textFormat.LineAlignment = StringAlignment.Center;
            textFormat.FormatFlags = StringFormatFlags.FitBlackBox;
            textFormat.Trimming = StringTrimming.EllipsisWord;
            _rootStyle.Add(StyleType.StringFormat, textFormat);

            _rootStyle.Add(StyleType.ImageAlignment, ImageAlignment.Center);

            _rootStyleButtonBody = new ButtonGridStyles();
            _rootStyleButtonBody.SetParentStyle(_rootStyle);
            _rootStyleButtonBody.Add(StyleType.ButtonWidth, 120);
            _rootStyleButtonBody.Add(StyleType.ButtonHeight, 150);

            _rootStyleGroupButton  = new ButtonGridStyles();
            _rootStyleGroupButton.SetParentStyle(_rootStyle);
            _rootStyleGroupButton.Add(StyleType.ButtonWidth, 30);
            _rootStyleGroupButton.Add(StyleType.ButtonHeight, 30);

            _rootStyleHeaderButton  = new ButtonGridStyles();
            _rootStyleHeaderButton.SetParentStyle(_rootStyle);
            _rootStyleHeaderButton.Add(StyleType.ButtonWidth, 100);
            _rootStyleHeaderButton.Add(StyleType.ButtonHeight, 30);

            _rootStyleNavigatorButton = new ButtonGridStyles();
            _rootStyleNavigatorButton.SetParentStyle(_rootStyle);
            _rootStyleNavigatorButton.Add(StyleType.ButtonWidth, 20);
            _rootStyleNavigatorButton.Add(StyleType.ButtonHeight, 20);


            _RootStylesCollection = new StylesCollection();
            _RootStylesCollection.DefaultStyle.SetParentStyle(_rootStyle);
            _RootStylesCollection.SnapStyle.SetParentStyle(_rootStyle);
            _RootStylesCollection.SelectStyle.SetParentStyle(_rootStyle);

            _RootStylesCollectionBodyButton  = new StylesCollection();
            _RootStylesCollectionBodyButton.DefaultStyle.SetParentStyle(_rootStyleButtonBody);
            _RootStylesCollectionBodyButton.SnapStyle.SetParentStyle(_rootStyleButtonBody);
            _RootStylesCollectionBodyButton.SelectStyle.SetParentStyle(_rootStyleButtonBody);

            _RootStylesCollectionGroupButton   = new StylesCollection();
            _RootStylesCollectionGroupButton.DefaultStyle.SetParentStyle(_rootStyleGroupButton);
            _RootStylesCollectionGroupButton.SnapStyle.SetParentStyle(_rootStyleGroupButton);
            _RootStylesCollectionGroupButton.SelectStyle.SetParentStyle(_rootStyleGroupButton);

            _RootStylesCollectionHeaderButton = new StylesCollection();
            _RootStylesCollectionHeaderButton.DefaultStyle.SetParentStyle(_rootStyleHeaderButton);
            _RootStylesCollectionHeaderButton.SnapStyle.SetParentStyle(_rootStyleHeaderButton);
            _RootStylesCollectionHeaderButton.SelectStyle.SetParentStyle(_rootStyleHeaderButton);

            _RootStylesCollectionNavigatorButton  = new StylesCollection();
            _RootStylesCollectionNavigatorButton.DefaultStyle.SetParentStyle(_rootStyleNavigatorButton);
            _RootStylesCollectionNavigatorButton.SnapStyle.SetParentStyle(_rootStyleNavigatorButton);
            _RootStylesCollectionNavigatorButton.SelectStyle.SetParentStyle(_rootStyleNavigatorButton);

        }

        void filterBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                timer_Tick(null, null);
                if (_bodyPages.Count == 1 && _bodyPages[0].PageItems.Count == 1)
                {
                    if (((ItemView)_bodyPages[0].PageItems[0]).ButtonGridItem.HasChildern)
                        setActiveButtonView((ItemView)_bodyPages[0].PageItems[0]);
                    else
                        raiseLeafButtonClicked(((ItemView)_bodyPages[0].PageItems[0]).ButtonGridItem);

                }
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            ((IButtonChildSource)_activeButtonItem.Childs).SetQuery(_filterBox.Text);
            createLayout();
            reDraw();
            _timer.Stop();
        }

        void filterBox_TextChanged(object sender, EventArgs e)
        {
            _prevTime = DateTime.Now;
            _timer.Start();
        }


        public void AddButtonGridItem(ButtonGridItem parentItem, IButtonChildSource childItems)
        {
            if (parentItem == null)
                throw new ArgumentNullException("parentItem", "parentItem is null.");
            if (childItems == null)
                throw new ArgumentNullException("childItems", "childItems is null.");
            parentItem.Childs = childItems;
        }


        public void RelateWithParent(ButtonGridItem parentItem, ButtonGridItem item)
        {
            item.Parent = parentItem;
            if (item is ButtonGridBodyButtonItem)
            {
                ((ButtonGridBodyButtonItem)item).ApperanceBody.StylesCollection.setParentStyleCollection(_RootStylesCollectionBodyButton );
                ((ButtonGridBodyButtonItem)item).ApperanceHeader .StylesCollection.setParentStyleCollection(_RootStylesCollectionHeaderButton );
            }
            else if (item is ButtonGridGroupButtonItem)
                ((ButtonGridGroupButtonItem)item).Apperance.StylesCollection.setParentStyleCollection(_RootStylesCollectionGroupButton );
            else if (item is ButtonGridNavigtorButtonItem)
                ((ButtonGridGroupButtonItem)item).Apperance.StylesCollection.setParentStyleCollection(_RootStylesCollectionNavigatorButton );



        }
        public void RemoveButton(ButtonGridBodyButtonItem button)
        {
        }

        public void RemoveGroup(ButtonGridGroupButtonItem group, bool withButton)
        {
        }

        public void createLayout()
        {
            this.SuspendPaint();
            try{
                // use the active button item to create the active page
                //also create the depth area

                //first create the headerpage
                List<ButtonGridItem> parentButtons = _activeButtonItem.getAncestors();
                parentButtons.Reverse();
                if (!parentButtons.Contains(_activeButtonItem))
                    parentButtons.Add(_activeButtonItem);
                List<ItemView> headerPageItemViews = new List<ItemView>();
                foreach (ButtonGridItem ancestorButton in parentButtons)
                    if (ancestorButton is ButtonGridBodyButtonItem)
                    {
                        ItemView buttonView = new HeaderButtonView((ButtonGridBodyButtonItem)ancestorButton);
                        buttonView.ButtonGridItem.NewLine = false;
                        headerPageItemViews.Add(buttonView);
                    }
                _selectedHeaderButtonView = headerPageItemViews[headerPageItemViews.Count - 1];
                _HeaderPage.createLayout(headerPageItemViews, Point.Empty, ButtonGridPage.PageFit.height, this.Width, ButtonGridPageRowAlignment.Left);
                if (!_visibleHeader)
                {
                    _HeaderPage.Width = 0;
                    _HeaderPage.Height = 0;
                }
                //add the filterBox if it exists
                TextBox filterBox = null;
                if (this.Controls.Count != 0)
                {
                    _filterLabel = (Label)this.Controls[1];
                    _filterLabel.Location = new Point(10, _HeaderPage.Height + 5);
                    filterBox = (TextBox)this.Controls[0];
                    filterBox.Width = 250;
                    filterBox.Location = new Point(_filterLabel.Width + 10, _HeaderPage.Height + 5);
                }

                //calculate the gridPage properties
                int gridPageWidth = this.Width, gridPageHeight = this.Height - _HeaderPage.Height - _pageNavigator.Height - (filterBox != null ? filterBox.Height - 5 : 0);
                Point gridPagePosition;
                if (filterBox != null)
                    gridPagePosition = new Point(0, filterBox.Location.Y + filterBox.Height - 5);
                else
                    gridPagePosition = new Point(0, _HeaderPage.Height);


                //create and populate the grid pages
                if (gridPageHeight < 0)
                {
                    //throw new Exception("");
                }
                else
                {
                    _bodyPages.Clear();
                    ButtonGridPage gridPage = null;

                    //List<ButtonGridItem> activeButtonChildren = activeButtonItem.getChildren();
                    List<ItemView> activeButtonChildrenViews = new List<ItemView>();

                    IButtonChildSource childs = _activeButtonItem.getChildren();
                    if (childs != null)
                    {


                        foreach (ButtonGridItem child in _activeButtonItem.getChildren())
                        {
                            if (child is ButtonGridBodyButtonItem)
                                activeButtonChildrenViews.Add(new BodyButtonView((ButtonGridBodyButtonItem)child));
                            else if (child is ButtonGridGroupButtonItem)
                                activeButtonChildrenViews.Add(new GroupButtonView((ButtonGridGroupButtonItem)child));

                        }

                    }
                    int i = 0;
                    List<ItemView> processedItemViews = new List<ItemView>();
                    while (i < activeButtonChildrenViews.Count)
                    {
                        gridPage = new ButtonGridPage(gridPagePosition, gridPageWidth, gridPageHeight);
                        _bodyPages.Add(gridPage);
                        processedItemViews.Clear();
                        processedItemViews.AddRange(activeButtonChildrenViews.GetRange(i, activeButtonChildrenViews.Count - i));
                        if (processedItemViews[0] is BodyButtonView)
                        {
                            for (int j = i - 1; j >= 0; j--)
                            {
                                if (activeButtonChildrenViews[j] is GroupButtonView)
                                {
                                    GroupButtonView groupButtonView = (GroupButtonView)activeButtonChildrenViews[j];
                                    GroupButtonView continueGroup = new GroupButtonView(((ButtonGridGroupButtonItem)groupButtonView.ButtonGridItem));
                                    //continueGroup.GroupItem.AddText(" Con.");
                                    processedItemViews.Insert(0, continueGroup);
                                    break;
                                }
                            }
                        }
                        i += gridPage.createLayout(processedItemViews, gridPagePosition, gridPageWidth, gridPageHeight, ButtonGridPage.PageFit.None, ButtonGridPageRowAlignment.Left);

                    }
                }
                if (_bodyPages.Count > 0)
                    _activePage = _bodyPages[0];
                else
                {
                    _activePage = new ButtonGridPage(gridPagePosition, gridPageWidth, gridPageHeight);
                    _bodyPages.Add(_activePage);
                }
                List<ItemView> pageNavigatorViews = new List<ItemView>();
                for (int j = 0; j < _bodyPages.Count; j++)
                {
                    ButtonGridNavigtorButtonItem pageNavigtorItem = new ButtonGridNavigtorButtonItem();
                    pageNavigtorItem.Apperance.StylesCollection.setParentStyleCollection(_RootStylesCollectionNavigatorButton);
                    ButtonElement<string> text = pageNavigtorItem.Apperance.AddText(Convert.ToString(j + 1));
                    text.DefaultStyle.Add(StyleType.FontBrush, Brushes.Black);
                    pageNavigatorViews.Add(new PageNavigatorButtonView(pageNavigtorItem));
                }

                _pageNavigator.Position = new Point(0, this.Height - _pageNavigator.Height);
                _pageNavigator.Width = this.Width;
                _pageNavigator.PageItems.Clear();
                _pageNavigator.createLayout(pageNavigatorViews, new Point(0, this.Height - _pageNavigator.Height), _pageNavigator.Width, _pageNavigator.Height, ButtonGridPage.PageFit.None, ButtonGridPageRowAlignment.Center);
            }
            finally
            {

                this.ResumePaint();
            }
        }

            public void ShowLastPage()
        {
            ButtonGridPage lastPage = _bodyPages[_bodyPages.Count - 1];
            if (_activePage != lastPage)
                ActivePage = lastPage;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (this._suspenPaint)
                return;
            base.OnPaint(e);
            reDraw();
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {

        }
        public void reDraw()
        {
            _buttonGridGraphics.bitmapGraphics.Clear(this.BackColor);
            if (_visibleHeader && _HeaderPage != null)
                _HeaderPage.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Default, _selectedHeaderButtonView);
            if (_activePage != null)
            {
                _activePage.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Default, _snapedItemView);
            }
            if (_pageNavigator != null)
            {
                _pageNavigator.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Default);
                _pageNavigator.PageItems[_bodyPages.IndexOf(_activePage)].Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Select);

            }
            if (_snapedItemView != null)
                _snapedItemView.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Snap);
            if (_visibleHeader && _selectedHeaderButtonView != null)
                _selectedHeaderButtonView.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Select);

            foreach (ItemView view in _activePage.PageItems)
                if (view.ButtonGridItem.HasChildern)
                {
                    int hotKeyLength = 14;
                    int offsetFromCorner = -1 * hotKeyLength / 2; ;
                    Rectangle imageRect = new Rectangle(view.Position.X + view.Apperance.StylesCollection.DefaultStyle.ButtonWidth + offsetFromCorner, view.Position.Y + view.Apperance.StylesCollection.DefaultStyle.ButtonHeight + offsetFromCorner, hotKeyLength, hotKeyLength);
                    _buttonGridGraphics.bitmapGraphics.DrawImage(Resources.pgcollpasedbtn1, imageRect);
                }
            //_activePage.DrawHotKeys(_buttonGridGraphics.bitmapGraphics);
            //_buttonGridGraphics.canvasGraphics.DrawImageUnscaled(_buttonGridGraphics.bitmapBuffer, Point.Empty);
            _buttonGridGraphics.canvasGraphics.DrawImage(_buttonGridGraphics.bitmapBuffer, new Rectangle(0,0,_buttonGridGraphics.bitmapBuffer.Width,_buttonGridGraphics.bitmapBuffer.Height)
                , 0, 0, _buttonGridGraphics.bitmapBuffer.Width, _buttonGridGraphics.bitmapBuffer.Height, GraphicsUnit.Pixel);

        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            ItemView newSnapedItemView = null;
            if (_visibleHeader && _HeaderPage.ContainsPoint(e.Location))
            {
                foreach (ItemView item in _HeaderPage.PageItems)
                    if (item.ContainPoint(e.Location))
                    {
                        newSnapedItemView = item;
                        break;
                    }
            }
            else if (_activePage.ContainsPoint(e.Location))
            {
                foreach (ItemView item in _activePage.PageItems)
                    if (item.ContainPoint(e.Location))
                    {
                        newSnapedItemView = item;
                        break;
                    }
            }
            else if (_pageNavigator.ContainsPoint(e.Location))
            {
                foreach (PageNavigatorButtonView item in _pageNavigator.PageItems)
                    if (item.ContainPoint(e.Location))
                    {
                        newSnapedItemView = item;
                        break;
                    }
                if (newSnapedItemView != null)
                {
                    PageNavigatorButtonView snapedPageNavBtton = (PageNavigatorButtonView)newSnapedItemView;
                    if (_activePage == _bodyPages[int.Parse(snapedPageNavBtton.ButtonGridNavigtorButtonItem.Apperance.Texts[0].Value) - 1])
                        newSnapedItemView = null;
                }

            }
            else
            {
                return;
                throw new Exception("the point should be in one of the above partitions.");
            }
            if (newSnapedItemView != null
                && newSnapedItemView.ButtonGridItem == _selectedHeaderButtonView.ButtonGridItem
                || (!_showLeafNode && newSnapedItemView is GroupButtonView))
                newSnapedItemView = null;
            if ((newSnapedItemView == null ^ _snapedItemView == null)
                || (newSnapedItemView != null && _snapedItemView != null && newSnapedItemView.ButtonGridItem != this._snapedItemView.ButtonGridItem))
            {
                this._snapedItemView = newSnapedItemView;
                reDraw();
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (_snapedItemView != null)
            {
                _snapedItemView.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Select);
                _buttonGridGraphics.canvasGraphics.DrawImageUnscaled(_buttonGridGraphics.bitmapBuffer, Point.Empty);
            }

        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            if (_snapedItemView != null)
            {
                if (!(_snapedItemView is PageNavigatorButtonView))
                    setActiveButtonView((ItemView)_snapedItemView); //ActiveButtonItem = snapedItemView.ButtonGridItem;
                else
                {
                    PageNavigatorButtonView snapedPageNavBtton = (PageNavigatorButtonView)_snapedItemView;
                    ActivePage = _bodyPages[int.Parse(snapedPageNavBtton.ButtonGridNavigtorButtonItem.Apperance.Texts[0].Value) - 1];
                }
            }
        }

        public void setFilterBox()
        {
            if (_activeButtonItem.Childs is IButtonChildSource && ((IButtonChildSource)_activeButtonItem.Childs).Searchable)
            {
                _filterLabel.Text = ((IButtonChildSource)_activeButtonItem.Childs).searchLabel;
                this.Controls.Add(_filterBox);
                this.Controls.Add(_filterLabel);
                _filterBox.Text = null;
                ((IButtonChildSource)_activeButtonItem.Childs).SetQuery(null);
            }
            else
            {
                this.Controls.Remove(_filterBox);
                this.Controls.Remove(_filterLabel);
                this.Focus();
            }

        }

        public void setActiveButtonView(ButtonGridItem buttonItem)
        {
            if (buttonItem == null) return;
            if (!buttonItem.HasChildern && !_showLeafNode && buttonItem != rootButtonItem)
            {
                raiseLeafButtonClicked(buttonItem);
            }
            else
            {
                _activeButtonItem = buttonItem;
                this.createLayout();
                this.Invalidate();
                raiseAcitveButtonChanged();
                if (!_activeButtonItem.HasChildern)
                    raiseLeafButtonClicked(buttonItem);
            }
        }

        public void setActiveButtonView(ItemView zoomedItemView)
        {
            ButtonGridItem prevActiveItem = _activeButtonItem;
            _activeItemView = zoomedItemView;
            float ZoomSpeed = 4;
            Brush backColorBrush = new SolidBrush(this.BackColor);


            if (!zoomedItemView.ButtonGridItem.HasChildern && !_showLeafNode && zoomedItemView.ButtonGridItem != rootButtonItem)
            {
                raiseLeafButtonClicked(zoomedItemView.ButtonGridItem);
                setFilterBox();
            }
            else
            {

                //drill down
                if (zoomedItemView.ButtonGridItem.getAncestors().Contains(_activeButtonItem))
                {
                    Bitmap buttonParentBitmap = new Bitmap(this.Width, this.Height);
                    Graphics intialImageGraphics = Graphics.FromImage(buttonParentBitmap);
                    _activePage.Draw(intialImageGraphics, ShadeTypes.Default);
                    //buttonParentBitmap.Save(@"C:\buttonParentBitmap.bmp");

                    _activeButtonItem = zoomedItemView.ButtonGridItem;
                    setFilterBox();
                    this.createLayout();
                    Bitmap buttonBitmap = new Bitmap(this.Width, this.Height);
                    Graphics finalImageGraphics = Graphics.FromImage(buttonBitmap);
                    _activePage.Draw(finalImageGraphics, ShadeTypes.Default);
                    _HeaderPage.Draw(finalImageGraphics, ShadeTypes.Default);
                    // buttonBitmap.Save(@"C:\buttonBitmap.bmp" );

                    RectangleF ButtonInitialRect = new RectangleF(zoomedItemView.Position.X, zoomedItemView.Position.Y
                        , zoomedItemView.Apperance.StylesCollection.DefaultStyle.ButtonWidth
                        , zoomedItemView.Apperance.StylesCollection.DefaultStyle.ButtonHeight );
                    RectangleF buttonFinalRect = new RectangleF(0, 0, this.Width, this.Height);

                    GraphicsExtension.FillRoundedRectangle(intialImageGraphics, backColorBrush, Rectangle.Round(ButtonInitialRect)
                        , zoomedItemView.Apperance.StylesCollection.DefaultStyle.CornerRadius);

                    float fFinX = 0, fFinY = 0;
                    fFinX = buttonFinalRect.Width / ButtonInitialRect.Width;
                    fFinY = buttonFinalRect.Height / ButtonInitialRect.Height;
                    PointF newMp = new PointF();
                    newMp.X = -1 * (fFinX * ButtonInitialRect.X) + buttonFinalRect.X;
                    newMp.Y = -1 * (fFinY * ButtonInitialRect.Y) + buttonFinalRect.Y;

                    List<float> zFactorsX = new List<float>();
                    List<float> zFactorsY = new List<float>();
                    List<PointF> mPs = new List<PointF>();

                    if (_zoomAnimate)
                    {
                        PointF deltaMp = PointF.Empty;
                        float alp;
                        for (int i = 1; i <= ZoomSpeed; i++)
                        {
                            alp = ((float)i) / ZoomSpeed;
                            zFactorsX.Add((alp * fFinX) + ((1f - alp) * 1));
                            zFactorsY.Add((alp * fFinY) + ((1f - alp) * 1));
                            deltaMp.X = (alp * newMp.X) + ((1f - alp) * 1);
                            deltaMp.Y = (alp * newMp.Y) + ((1f - alp) * 1);
                            mPs.Add(deltaMp);
                        }
                    }

                    PointF interpolatePosition;
                    float interpolateFactorX, interpolateFactorY;
                    RectangleF interplolatedRect = new RectangleF();
                    RectangleF interplolatedParentRect = new RectangleF();
                    for (int i = 0; i < zFactorsX.Count; i++)
                    {
                        interpolatePosition = mPs[i];
                        interpolateFactorX = zFactorsX[i];
                        interpolateFactorY = zFactorsY[i];

                        interplolatedRect.X = (interpolateFactorX * ButtonInitialRect.X) + interpolatePosition.X;
                        interplolatedRect.Y = (interpolateFactorY * ButtonInitialRect.Y) + interpolatePosition.Y;
                        interplolatedRect.Width = ButtonInitialRect.Width * interpolateFactorX;
                        interplolatedRect.Height = ButtonInitialRect.Height * interpolateFactorY;

                        interplolatedParentRect.X = (interpolateFactorX * buttonFinalRect.X) + interpolatePosition.X;
                        interplolatedParentRect.Y = (interpolateFactorY * buttonFinalRect.Y) + interpolatePosition.Y;
                        interplolatedParentRect.Width = buttonFinalRect.Width * interpolateFactorX;
                        interplolatedParentRect.Height = buttonFinalRect.Height * interpolateFactorY;

                        _buttonGridGraphics.bitmapGraphics.Clear(this.BackColor);
                        _buttonGridGraphics.bitmapGraphics.DrawImage(buttonParentBitmap, interplolatedParentRect);
                        _buttonGridGraphics.bitmapGraphics.DrawImage(buttonBitmap, interplolatedRect);
                        //HeaderPage.Draw(buttonGridGraphics.bitmapGraphics, ShadeTypes.Default);
                        _buttonGridGraphics.canvasGraphics.DrawImageUnscaled(_buttonGridGraphics.bitmapBuffer, Point.Empty);
                        //buttonGridGraphics.bitmapBuffer.Save(@"C:\interPolateBitmap"+i+".bmp");
                    }

                }
                else //drill up
                {
                    //the intial drawing
                    Bitmap intialImage = new Bitmap(this.Width, this.Height);
                    Graphics intialImageGraphics = Graphics.FromImage(intialImage);
                    _activePage.Draw(intialImageGraphics, ShadeTypes.Default);
                    //intialImage.Save(@"C:\intialImage.bmp");

                    //change the active button to the new one
                    _activeButtonItem = zoomedItemView.ButtonGridItem;
                    setFilterBox();
                    this.createLayout();

                    //the final drawing
                    Bitmap finalImage = new Bitmap(this.Width, this.Height);
                    Graphics finalImageGraphics = Graphics.FromImage(finalImage);
                    _activePage.Draw(finalImageGraphics, ShadeTypes.Default);

                    //finalImage.Save(@"C:\finalImage.bmp");

                    //get the buttonItem in the newly created pages that is the ancestor of the previous active item
                    ButtonGridItem ancestorButtonItem = null;
                    IButtonChildSource childs = _activeButtonItem.getChildren();
                    if (childs != null)
                    {
                        foreach (ButtonGridItem child in zoomedItemView.ButtonGridItem.getChildren())
                            if (child.Equals(prevActiveItem) || prevActiveItem.getAncestors().Contains(child))
                            {
                                ancestorButtonItem = child;
                                break;
                            }
                    }
                    if (ancestorButtonItem == null) throw new Exception("ancestorButtonItem Cannot be null .");
                    //get the page and view of the ancestor buttonItem
                    ButtonGridPage ancestorBtnPage = getParentPage(ancestorButtonItem);
                    ItemView ancestorButtonView = ancestorBtnPage.getView(ancestorButtonItem);

                    RectangleF ButtonInitialRect = new RectangleF(0, 0, this.Width, this.Height);
                    RectangleF buttonFinalRect = new RectangleF(ancestorButtonView.Position.X, ancestorButtonView.Position.Y
                        , ancestorButtonView.Apperance.StylesCollection.DefaultStyle.ButtonWidth
                        , ancestorButtonView.Apperance.StylesCollection.DefaultStyle.ButtonHeight );

                    GraphicsExtension.FillRoundedRectangle(finalImageGraphics, backColorBrush, Rectangle.Round(buttonFinalRect)
                        , ancestorButtonView.Apperance.StylesCollection.DefaultStyle.CornerRadius);

                    float fFinX = 0, fFinY = 0;
                    fFinX = buttonFinalRect.Width / ButtonInitialRect.Width;
                    fFinY = buttonFinalRect.Height / ButtonInitialRect.Height;
                    PointF newMp = new PointF();
                    newMp.X = -1 * (fFinX * ButtonInitialRect.X) + buttonFinalRect.X;
                    newMp.Y = -1 * (fFinY * ButtonInitialRect.Y) + buttonFinalRect.Y;

                    List<float> zFactorsX = new List<float>();
                    List<float> zFactorsY = new List<float>();
                    List<PointF> mPs = new List<PointF>();

                    if (_zoomAnimate)
                    {
                        //ZoomSpeed = ZoomSpeed - 5;
                        PointF deltaMp = PointF.Empty;
                        float alp;
                        for (int i = 1; i <= ZoomSpeed; i++)
                        {
                            alp = ((float)i) / ZoomSpeed;
                            zFactorsX.Add((alp * fFinX) + ((1f - alp) * 1));
                            zFactorsY.Add((alp * fFinY) + ((1f - alp) * 1));
                            deltaMp.X = (alp * newMp.X) + ((1f - alp) * 0);
                            deltaMp.Y = (alp * newMp.Y) + ((1f - alp) * 0);
                            mPs.Add(deltaMp);
                        }
                    }

                    RectangleF parentIntialRect = new RectangleF();
                    RectangleF parentFinalRect = ButtonInitialRect;
                    parentIntialRect.X = (parentFinalRect.X - newMp.X) / fFinX;
                    parentIntialRect.Y = (parentFinalRect.Y - newMp.Y) / fFinY;
                    parentIntialRect.Width = parentFinalRect.Width / fFinX;
                    parentIntialRect.Height = parentFinalRect.Height / fFinY;


                    PointF interpolatePosition;
                    float interpolateFactorX, interpolateFactorY;
                    RectangleF interplolatedRect = new RectangleF();
                    RectangleF interplolatedParentRect = new RectangleF();
                    for (int i = 0; i < zFactorsX.Count; i++)
                    {
                        interpolatePosition = mPs[i];
                        interpolateFactorX = zFactorsX[i];
                        interpolateFactorY = zFactorsY[i];

                        interplolatedRect.X = (interpolateFactorX * ButtonInitialRect.X) + interpolatePosition.X;
                        interplolatedRect.Y = (interpolateFactorY * ButtonInitialRect.Y) + interpolatePosition.Y;
                        interplolatedRect.Width = ButtonInitialRect.Width * interpolateFactorX;
                        interplolatedRect.Height = ButtonInitialRect.Height * interpolateFactorY;

                        interplolatedParentRect.X = (interpolateFactorX * parentIntialRect.X) + interpolatePosition.X;
                        interplolatedParentRect.Y = (interpolateFactorY * parentIntialRect.Y) + interpolatePosition.Y;
                        interplolatedParentRect.Width = parentIntialRect.Width * interpolateFactorX;
                        interplolatedParentRect.Height = parentIntialRect.Height * interpolateFactorY;

                        _buttonGridGraphics.bitmapGraphics.Clear(this.BackColor);
                        _buttonGridGraphics.bitmapGraphics.DrawImage(finalImage, interplolatedParentRect);
                        _buttonGridGraphics.bitmapGraphics.DrawImage(intialImage, interplolatedRect);
                        //buttonGridGraphics.bitmapGraphics.DrawRectangle(Pens.White, Rectangle.Round(ButtonInitialRect));
                        _buttonGridGraphics.canvasGraphics.DrawImageUnscaled(_buttonGridGraphics.bitmapBuffer, Point.Empty);
                        //buttonGridGraphics.bitmapBuffer.Save(@"C:\interPolateBitmap" + i + ".bmp");
                    }
                    changeActivePage(ancestorBtnPage, false);

                }



                raiseAcitveButtonChanged();
                if (!_activeButtonItem.HasChildern)
                    raiseLeafButtonClicked(_activeButtonItem);
                //Debug.Assert(ItemRelationShips.NodeExists(rootButtonItem));

                _snapedItemView = null;
                reDraw();
            }
        }

        public void changeActivePage(ButtonGridPage value, bool animate)
        {
            if (_bodyPages.Contains(value))
            {
                //save the previou page to a bitmap
                //save the new page to a bitmap
                //move the previous page from page  p(position.x,position.y) to p(position.x-width,position.y)
                //move the new page from page p(position.x + width,position.y ) to p(positon.x,positon.y )
                if (_activePage == value) return;
                if (_activePage != null && animate)
                {
                    int oldPageNo = _bodyPages.IndexOf(_activePage) + 1;
                    ButtonGridPage previousPage = _activePage;
                    ButtonGridPage newPage = value;
                    if (_activePage != newPage)
                    {
                        int transitionFactor = _bodyPages.IndexOf(newPage) > _bodyPages.IndexOf(_activePage) ? -1 : 1;
                        Bitmap prevPageBtimap = new Bitmap(this.Width, this.Height);
                        Graphics g = Graphics.FromImage(prevPageBtimap);
                        previousPage.Draw(g, ShadeTypes.Default);
                        Bitmap newPageBtimap = new Bitmap(this.Width, this.Height);
                        g = Graphics.FromImage(newPageBtimap);
                        newPage.Draw(g, ShadeTypes.Default);

                        Point prevPageP1 = new Point(previousPage.Position.X, 0);
                        Point prevPageP2 = new Point(previousPage.Position.X + (transitionFactor * Width), 0);
                        Point newPageP1 = new Point(previousPage.Position.X - (transitionFactor * Width), 0);
                        Point newPageP2 = new Point(previousPage.Position.X, 0);
                        //newPageP2.X += newPage.Width;
                        float offset;
                        int speed = 10;
                        DateTime t1 = DateTime.Now;
                        double total = 200;
                        DateTime t2 = t1.AddMilliseconds(total);
                        DateTime t;
                        //for (int i = 0; i <= speed; i++)
                        while ((t = DateTime.Now) < t2)
                        {
                            //offset = ((float)transitionFactor) * ((float)i / speed) * newPage.Width;
                            offset = (float)transitionFactor * (float)(((TimeSpan)t.Subtract(t1)).TotalMilliseconds / total) * newPage.Width;
                            prevPageP2.X = (int)offset + prevPageP1.X;
                            newPageP2.X = (int)offset + newPageP1.X;
                            _buttonGridGraphics.bitmapGraphics.Clear(this.BackColor);
                            _buttonGridGraphics.bitmapGraphics.DrawImageUnscaled(prevPageBtimap, prevPageP2);
                            _buttonGridGraphics.bitmapGraphics.DrawImageUnscaled(newPageBtimap, newPageP2);
                            _HeaderPage.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Default);
                            _pageNavigator.Draw(_buttonGridGraphics.bitmapGraphics, ShadeTypes.Default);
                            _buttonGridGraphics.canvasGraphics.DrawImageUnscaled(_buttonGridGraphics.bitmapBuffer, Point.Empty);
                        }
                    }
                }

                _activePage = value;
                reDraw();
            }
            //else
            //    MessageBox.Show("Page doesn't exist");
        }

        public void zoomOut()
        {
        }

        public void UpdateLayout()
        {
            createLayout();
            reDraw();
        }
        public void UpdateLayoutInPlace()
        {
            ItemView firstItemView = null;
            if (_activePage.PageItems.Count != 0)
            {
                firstItemView = _activePage.PageItems[0];

            }
            createLayout();
            if (firstItemView != null)
            {
                ButtonGridPage firstItemPage = getParentPage(firstItemView.ButtonGridItem);
                if (_pageNavigator.PageItems.Count > _bodyPages.IndexOf(firstItemPage))
                    changeActivePage(firstItemPage, false);
            }
            reDraw();
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            _buttonGridGraphics.updateGraphics();
            UpdateLayoutInPlace();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ButtonGrid
            // 
            this.Size = new System.Drawing.Size(200, 200);
            this.ResumeLayout(false);

        }

        public static bool PointOnControl(Control control, Point p)
        {
            return PointOnRectangle(control.Location, control.Width, control.Height, p);
        }

        public static bool PointOnRectangle(Point Location, int Width, int Height, Point p)
        {
            return Location.X <= p.X && p.X <= Location.X + Width
                && Location.Y <= p.Y && p.Y <= Location.Y + Height;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (_filterBox.Focused) return;
            base.OnKeyDown(e);
            //handleKeyDown(e);
        }

        public void handleKeyDown(KeyEventArgs e)
        {
            if (_filterBox.Focused) return;
            if (e.KeyCode == Keys.Home)
            {
                if (_HeaderPage.PageItems.Count != 1)
                    setActiveButtonView((ItemView)_HeaderPage.PageItems[0]);
            }
            else if (e.KeyCode == Keys.Back)
            {
                List<ButtonGridItem> parentButtons = _activeButtonItem.getAncestors();
                if (_HeaderPage.PageItems.Count > 1)
                    setActiveButtonView((ItemView)_HeaderPage.PageItems[_HeaderPage.PageItems.Count - 2]);
            }
            else if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Up)
            {
                int currentPageIndex = _bodyPages.IndexOf(_activePage);
                if (currentPageIndex > 0)
                    ActivePage = _bodyPages[currentPageIndex - 1];
            }
            else if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Down)
            {
                int currentPageIndex = _bodyPages.IndexOf(_activePage);
                if (currentPageIndex < _bodyPages.Count - 1)
                    ActivePage = _bodyPages[currentPageIndex + 1];
            }


            else
            {
                if (_activePage != null)
                {
                    ItemView pressedView = _activePage.getView(e.KeyCode);
                    if (pressedView != null && !(pressedView is PageNavigatorButtonView))
                        setActiveButtonView(pressedView);
                }
            }
        }

        public ButtonGridPage getParentPage(ButtonGridItem gridItem)
        {
            foreach (ButtonGridPage page in _bodyPages)
                if (page.getView(gridItem) != null)
                    return page;
            return null;
        }

        public ButtonGridPage getParentPage(ItemView view)
        {
            foreach (ButtonGridPage page in _bodyPages)
                if (page.PageItems.Contains(view))
                    return page;
            return null;
        }

        ButtonGridItem getItemByText(string itemText)
        {
            foreach (ButtonGridItem buttonItem in rootButtonItem.getAllButtons(true))
                if (buttonItem.HasText(itemText))
                    return buttonItem;
            throw new Exception("There is not item with text" + itemText + " in the relationShip .");
        }

        public byte[] getBytes()
        {
            return new byte[] { };
        }

        //public static TreeRelationShipManager<ButtonGridItem> loadFromBytes(byte[] bytes)
        //{
        //    return Helpers.ByteArrayToObject<TreeRelationShipManager<ButtonGridItem>>(bytes);
        //}
    }

    public class ButtonGridSerializer
    {
        // TreeRelationShipManager<ButtonGridItem> ItemRelationShips;
        GroupButtonView groupButtonView;
        BodyButtonView buttonBodyView;
        HeaderButtonView buttonHeaderView;

        public ButtonGridSerializer(GroupButtonView groupButtonView, BodyButtonView buttonBodyView, HeaderButtonView buttonHeaderView)
        {

        }
    }

    public static class Helpers
    {
        public static string getXml(object obj)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                MemoryStream memoryStream = new MemoryStream();
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                serializer.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                return UTF8ByteArrayToString(memoryStream.ToArray());
            }
            catch (Exception err)
            {
                return null;
            }
        }

        public static T loadFromXml<T>(string xmaliziedString)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                MemoryStream memoryStream = new MemoryStream(StringToUTF8ByteArray(xmaliziedString));
                return (T)serializer.Deserialize(memoryStream);
            }
            catch (Exception err)
            {
                return default(T);
            }
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null) return null;
            IFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, obj);
            return stream.ToArray();
        }

        public static T ByteArrayToObject<T>(byte[] byteArray)
            where T : class
        {
            if (byteArray == null) return null;
            try
            {
                IFormatter formatter = new BinaryFormatter();
                MemoryStream ms = new MemoryStream(byteArray);
                Object obj = formatter.Deserialize(ms);
                ms.Close();
                return (T)obj;
            }
            catch
            {
                return null;
            }

        }

        public static byte[] getBytes(object obj)
        {
            try
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter serializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream();
                serializer.Serialize(memoryStream, obj);
                byte[] ret = new byte[memoryStream.Length];
                memoryStream.Seek(0, SeekOrigin.Begin);
                memoryStream.Read(ret, 0, ret.Length);
                return ret;
            }
            catch (Exception err)
            {
                return null;
            }
        }

        public static T loadFromBytes<T>(byte[] bytes)
        {
            try
            {
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter serializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream(bytes);
                return (T)serializer.Deserialize(memoryStream);
            }
            catch (Exception err)
            {
                return default(T);
            }
        }

        static string UTF8ByteArrayToString(Byte[] character)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetString(character);
        }

        static Byte[] StringToUTF8ByteArray(string xmlizedString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            return encoding.GetBytes(xmlizedString);
        }

        public static PointF PointAdd(PointF p1, PointF p2)
        {
            return new PointF(p2.X + p1.X, p2.Y + p1.Y);
        }

        public static PointF PointSubtract(PointF p1, PointF p2)
        {
            return new PointF(p2.X - p1.X, p2.Y - p1.Y);
        }

        public static PointF PointMultiply(PointF p2, float divider)
        {
            return new PointF(p2.X * divider, p2.Y * divider);
        }

        public static float PointMangnitude(PointF p2)
        {
            return (float)Math.Sqrt((p2.X * p2.X) + (p2.Y * p2.Y));
        }

    }

    public interface IButtonInteractive
    {
    }

    public enum ShadeTypes
    {
        Default,
        Snap,
        Select
    }

    public delegate void GenericEventHandler();
    public delegate void GenericEventHandler<T>(T t);
    public delegate void GenericEventHandler<T, Z>(T t, Z z);

    public class MyGraphics
    {
        public Control control;
        public Bitmap bitmapBuffer;
        public Graphics bitmapGraphics, canvasGraphics;

        public MyGraphics(Control control)
        {
            this.control = control;
            updateGraphics();
        }

        public void updateGraphics()
        {
            canvasGraphics = control.CreateGraphics();
            if (control.Width == 0 || control.Height == 0)
            {
                bitmapBuffer = new Bitmap(1, 1);
            }
            else
            {
                bitmapBuffer = new Bitmap(control.Width, control.Height);
            }
            bitmapGraphics = Graphics.FromImage(bitmapBuffer);
            bitmapGraphics.SmoothingMode = SmoothingMode.HighQuality;
        }
    }

}
