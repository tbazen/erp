using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{
    public class ButtonGridPage
    {
        List<ItemView> _pageItemViews = new List<ItemView>();
        int _width;
        int _height;
        Point _position;
        int _spacing = 20;
        int _hotKeyMargin = 4;

        public static Pen circlePen;
        public static Font font;
        public static StringFormat textFormat;

        static ButtonGridPage()
        {
            font = new Font("Courier New", 8, FontStyle.Bold);

            circlePen = new Pen(Color.Green);
            circlePen.Width = 3;
            circlePen.Alignment = System.Drawing.Drawing2D.PenAlignment.Inset;
            circlePen.LineJoin = System.Drawing.Drawing2D.LineJoin.Round;

            textFormat = new StringFormat();
            textFormat.Alignment = StringAlignment.Center;
            textFormat.LineAlignment = StringAlignment.Center;
            //textFormat.FormatFlags = StringFormatFlags.FitBlackBox;
            //textFormat.Trimming = StringTrimming.EllipsisWord;

        }

        public ButtonGridPage()
        {
        }

        public ButtonGridPage(Point position, int width, int height)
        {
            this._position = position;
            this._height = height;
            this._width = width;


        }

        public List<ItemView> PageItems
        {
            get { return _pageItemViews; }
        }

        public int Width
        {
            get
            {
                return _width;
            }
            set
            {
                this._width = value;
            }
        }

        public int Height
        {
            get
            {
                return _height;
            }
            set
            {
                this._height = value;
            }
        }

        public Point Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public bool ContainsPoint(Point p)
        {
            return ButtonGrid.PointOnRectangle(_position, _width, _height, p);
        }

        public void arrangeButtons()
        {
        }

        public int createLayout(List<ItemView> itemViews, Point position, int width, int height, PageFit pageFit, ButtonGridPageRowAlignment rowAlignment)
        {
            this._position = position;
            this._height = height;
            this._width = width;
            Point cursorPosition = position;
            cursorPosition.Y += _spacing;
            cursorPosition.X += _spacing;
            int rowMaxHeight = 0;
            this._pageItemViews.Clear();
            GroupButtonView parentGroup = null;
            List<ItemView> rowViews = new List<ItemView>();
            for (int i = 0; i < itemViews.Count; i++)
            {
                ItemView itemView = itemViews[i];
                float DefaultButtonWidth;
                if (i > 0 && (itemView.ButtonGridItem.NewLine 
                    || itemView .Apperance .StylesCollection .DefaultStyle.ButtonWidth
                    + cursorPosition.X > _width - (2 * _spacing)))
                {
                    if (itemView is GroupButtonView)
                        cursorPosition.Y += rowMaxHeight + (2 * _spacing);
                    else
                        cursorPosition.Y += rowMaxHeight + _spacing;

                    //center the row items
                    if (rowAlignment == ButtonGridPageRowAlignment.Left)
                    {
                        //the default calculaton is the same as left alignement
                    }
                    else if (rowAlignment == ButtonGridPageRowAlignment.Center)
                    {
                        int offset = (int)((this._width - cursorPosition.X) / 2f);
                        if (offset > 0)
                            foreach (ItemView view in rowViews)
                            {
                                view.Position = new Point(view.Position.X + offset, view.Position.Y);
                                //view.create();
                            }
                    }
                    else
                    {
                    }

                    if (itemView is GroupButtonView)
                        cursorPosition.X = position.X + _spacing;
                    else
                        cursorPosition.X = position.X + (2 * _spacing);


                    
                    //move down the short buttons
                    foreach (ItemView rowView in rowViews)
                    {
                        int buttonHeight = rowView.Apperance.StylesCollection.DefaultStyle.ButtonHeight;
                        if (buttonHeight < rowMaxHeight)
                        {
                            rowView.Position = new Point(rowView.Position.X, rowView.Position.Y + rowMaxHeight - buttonHeight);
                        }
                    }

                    rowMaxHeight = 0;
                    rowViews.Clear();
                }
                if (i == 0)
                {
                    if (itemView is GroupButtonView)
                        cursorPosition.X = position.X + _spacing;
                    else
                        cursorPosition.X = position.X + (2 * _spacing);

                }
                //if (itemView.ButtonGridItem.Texts[0].Value == "Users")
                //{

                //}
                if (i > 0 &&  itemView.Apperance.StylesCollection.DefaultStyle.ButtonHeight + cursorPosition.Y > height + position.Y)
                    break;
                itemView.Position = new Point(cursorPosition.X, cursorPosition.Y);
                this._pageItemViews.Add(itemView);
                rowMaxHeight = Math.Max(rowMaxHeight, itemView.Apperance.StylesCollection.DefaultStyle.ButtonHeight);
                if (itemView is GroupButtonView)
                    itemView.Apperance.StylesCollection.DefaultStyle.Add(StyleType.ButtonWidth, this._width - (2 * _spacing));
                cursorPosition.X +=itemView.Apperance.StylesCollection.DefaultStyle.ButtonWidth   + _spacing;


                //itemView.create();

                if (itemView is GroupButtonView)
                {
                    //updates the previous group brush
                    if (parentGroup != null)
                        parentGroup.UpdateBrush();
                    //start a new parent group
                    parentGroup = (GroupButtonView)itemView;
                    parentGroup.GroupBodyHeight = 0;
                    parentGroup.GroupBodyWidth = parentGroup.Apperance.StylesCollection.DefaultStyle.ButtonWidth;
                }
                else
                {
                    if (parentGroup != null)
                    {
                        parentGroup.GroupBodyHeight = cursorPosition.Y + rowMaxHeight + _spacing - (parentGroup.Position.Y + parentGroup.Apperance.StylesCollection.DefaultStyle.ButtonHeight);
                    }
                }


                rowViews.Add(itemView);
            }

            //center the row items
            if (rowAlignment == ButtonGridPageRowAlignment.Left)
            {
                //the default calculaton is the same as left alignement
            }
            else if (rowAlignment == ButtonGridPageRowAlignment.Center)
            {
                int offset = (int)((this._width - cursorPosition.X) / 2f);
                if (offset > 0)
                    foreach (ItemView view in rowViews)
                    {
                        view.Position = new Point(view.Position.X + offset, view.Position.Y);
                        //view.create();
                    }
            }
            else
            {
            }

            foreach (ItemView rowView in rowViews)
            {
                int buttonHeight = rowView.Apperance.StylesCollection.DefaultStyle.ButtonHeight ;
                if (buttonHeight < rowMaxHeight)
                {
                    rowView.Position = new Point(rowView.Position.X, rowView.Position.Y + rowMaxHeight - buttonHeight);
                }
            }

            if (parentGroup != null)
                parentGroup.UpdateBrush();

            if (pageFit == PageFit.height)
                this._height = cursorPosition.Y - position.Y + rowMaxHeight;
            else if (pageFit == PageFit.width)
                throw new Exception("Not implimented");
            else if (pageFit == PageFit.Both)
                throw new Exception("Not implimented");

            //this removes the last item view if it is groupbuttonview
            if (this._pageItemViews.Count > 1 && this._pageItemViews[this._pageItemViews.Count - 1] is GroupButtonView && itemViews.Count != this._pageItemViews.Count)
                this._pageItemViews.RemoveAt(this._pageItemViews.Count - 1);
            return this._pageItemViews.Count;
        }

        public int createLayout(List<ItemView> pageItems, Point position, PageFit pageFit, int value, ButtonGridPageRowAlignment rowAlignment)
        {
            switch (pageFit)
            {
                case PageFit.width:
                    return createLayout(pageItems, position, 10000, value, pageFit, rowAlignment);
                case PageFit.height:
                    return createLayout(pageItems, position, value, 10000, pageFit, rowAlignment);
                case PageFit.None:
                    return createLayout(pageItems, position, value, value, pageFit, rowAlignment);
                case PageFit.Both:
                    return createLayout(pageItems, position, value, value, pageFit, rowAlignment);
            }
            return -1;
        }

        public void Draw(Graphics g, ShadeTypes shadeType, params ItemView[] Exclude)
        {
            List<ItemView> ExcludeList = new List<ItemView>();
            ExcludeList.AddRange(Exclude);
            foreach (ItemView itemView in _pageItemViews)
                if (!ExcludeList.Contains(itemView))
                    itemView.Draw(g, shadeType);
        }

        internal void DrawHotKeys(Graphics g)
        {
            int i = 1;
            Rectangle rect = new Rectangle();
            int hotkeyLength = 24;
            rect.Height = rect.Width = hotkeyLength;
            _hotKeyMargin = -1 * hotkeyLength/2 ;
            GraphicsPath path = new GraphicsPath();
            foreach (ItemView itemView in _pageItemViews)
                if (itemView is BodyButtonView)
                {

                    rect.Location = itemView.Position;
                    rect.X += _hotKeyMargin;
                    rect.Y += _hotKeyMargin;
                    path.Reset();
                    path.AddArc(rect, 0, 360);
                    g.FillPath(Brushes.Green, path);
                    g.DrawString("F" + i, font, Brushes.White, rect, textFormat);
                    i++;
                    if (i > 12)
                        break;
                }
            path.Dispose();
        }

        public ItemView getView(Keys pressedKey)
        {
            string pressedKeyValue = pressedKey.ToString();
            if (pressedKeyValue.StartsWith("F"))
            {
                string endString = pressedKeyValue.Substring(1);
                int itemViewIndex = 0;
                if (int.TryParse(endString, out itemViewIndex))
                    if (itemViewIndex <= _pageItemViews.Count)
                    {
                        int i = -1;
                        foreach (ItemView view in _pageItemViews)
                            if (view is BodyButtonView)
                            {
                                i++;
                                if (i == itemViewIndex - 1)
                                    return view;
                            }
                        return null;
                    }
            }
            return null;
        }

        public ItemView getView(ButtonGridItem gridItem)
        {
            foreach (ItemView view in _pageItemViews)
                if (view.ButtonGridItem.Equals(gridItem))
                    return view;
            return null;
        }

        public enum PageFit
        {
            width, height, Both, None
        }

    }


    public enum ButtonGridPageRowAlignment
    {
        Left, Right, Center
    }


}
