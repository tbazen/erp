using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.UI.ButtonGrid
{
    public class ButtonGridTabbed : TabControl
    {
        ButtonGrid CurrentButtonGrid
        {
            get
            {
                return this.SelectedTab != null ? (ButtonGrid)SelectedTab.Controls[0] : null;
            }
        }

        public ButtonGridTabbed()
        {
        }


        public void AddTab(string buttonName, IButtonChildSource childItems)
        {
            ButtonGrid buttonGrid = new ButtonGrid();
            buttonGrid.AddButtonGridItem(buttonGrid.rootButtonItem, childItems);
            buttonGrid.setFilterBox();
            buttonGrid.Dock = DockStyle.Fill;

            TabPage newPage = new TabPage(buttonName);
            this.TabPages.Add(newPage);
            newPage.Controls.Add(buttonGrid);
        }
    }
}
