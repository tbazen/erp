
using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{
    static class GraphicsExtension
    {
        private static GraphicsPath GenerateRoundedRectangle(Graphics graphics, RectangleF rectangle, float radius)
        {
            float diameter;
            GraphicsPath path = new GraphicsPath();
            if (radius <= 0.0F)
            {
                path.AddRectangle(rectangle);
                path.CloseFigure();
                return path;
            }
            else
            {
                if (radius >= (Math.Min(rectangle.Width, rectangle.Height)) / 2.0)
                    return GenerateCapsule(graphics,rectangle);
                diameter = radius * 2.0F;
                SizeF sizeF = new SizeF(diameter, diameter);
                RectangleF arc = new RectangleF(rectangle.Location, sizeF);
                path.AddArc(arc, 180, 90);
                arc.X = rectangle.Right - diameter;
                path.AddArc(arc, 270, 90);
                arc.Y = rectangle.Bottom - diameter;
                path.AddArc(arc, 0, 90);
                arc.X = rectangle.Left;
                path.AddArc(arc, 90, 90);
                path.CloseFigure();
            }
            return path;
        }
   
        private static GraphicsPath GenerateCapsule(Graphics graphics, RectangleF baseRect)
        {
            float diameter;
            RectangleF arc;
            GraphicsPath path = new GraphicsPath();
            try
            {
                if (baseRect.Width > baseRect.Height)
                {
                    diameter = baseRect.Height;
                    SizeF sizeF = new SizeF(diameter, diameter);
                    arc = new RectangleF(baseRect.Location, sizeF);
                    path.AddArc(arc, 90, 180);
                    arc.X = baseRect.Right - diameter;
                    path.AddArc(arc, 270, 180);
                }
                else if (baseRect.Width < baseRect.Height)
                {
                    diameter = baseRect.Width;
                    SizeF sizeF = new SizeF(diameter, diameter);
                    arc = new RectangleF(baseRect.Location, sizeF);
                    path.AddArc(arc, 180, 180);
                    arc.Y = baseRect.Bottom - diameter;
                    path.AddArc(arc, 0, 180);
                }
                else path.AddEllipse(baseRect);
            }
            catch { path.AddEllipse(baseRect); }
            finally { path.CloseFigure(); }
            return path;
        }

        public static void DrawRoundedRectangle(Graphics graphics, Pen pen, float x,
            float y, float width, float height, float radius)
        {
            RectangleF rectangle = new RectangleF(x, y, width, height);
            GraphicsPath path = GenerateRoundedRectangle(graphics,rectangle, radius);
            graphics.DrawPath(pen, path);
        }

        public static void DrawRoundedRectangle(Graphics graphics, Pen pen, RectangleF rect, float radius)
        {
             DrawRoundedRectangle(graphics, pen, rect.X, rect.Y, rect.Width, rect.Height, radius);
        }

        public static void FillRoundedRectangle(Graphics graphics, Brush brush,
            float x, float y, float width, float height, float radius)
        {
            RectangleF rectangle = new RectangleF(x, y, width, height);
            GraphicsPath path = GenerateRoundedRectangle(graphics , rectangle, radius);
            graphics.FillPath(brush, path);
        }

        public static void FillRoundedRectangle(Graphics graphics, Brush brush, RectangleF rect, float radius)
        {
            FillRoundedRectangle(graphics, brush, rect.X, rect.Y, rect.Width, rect.Height, radius);
        }
    }

}