using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    public class ButtonApperance
    {
        List<ButtonElement<string>> _Texts;
        private List<ButtonElement<Image>> _Images;
        protected StylesCollection _StylesCollection;

        public ButtonApperance()
        {
            _Texts = new List<ButtonElement<string>>();
            _Images = new List<ButtonElement<Image>>();
            _StylesCollection = new StylesCollection();
        }

        public StylesCollection StylesCollection
        {
            get
            {
                return _StylesCollection;
            }
        }

        public List<ButtonElement<string>> Texts
        {
            get
            {
                return _Texts;
            }
        }

        public List<ButtonElement<Image>> Images
        {
            get
            {
                return _Images;
            }
        }

        public ButtonElement<string> AddText(string text)
        {
            ButtonElement<string> textElement = new ButtonElement<string>(text);
            _Texts.Add(textElement);
            textElement.StylesCollection.setParentStyleCollection(_StylesCollection);
            return textElement;
        }

        public ButtonElement<Image> AddImage(Image Image)
        {
            ButtonElement<Image> imageElement = new ButtonElement<Image>(Image);
            _Images.Add(imageElement);
            imageElement.StylesCollection.setParentStyleCollection(_StylesCollection);
            return imageElement;
        }


    }
}
