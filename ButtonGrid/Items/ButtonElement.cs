using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    public interface IButtonElement
    {
        ButtonGridStyles SnapStyle
        {
            get;
        }

        ButtonGridStyles DefaultStyle
        {
            get;
        }

        ButtonGridStyles SelectStyle
        {
            get;
        }
    }

    public class ButtonElement<T> : IButtonElement
    {
        T _Value;
        private StylesCollection _StylesCollection;

        public ButtonElement(T Value)
        {
            this._Value = Value;
            _StylesCollection = new StylesCollection();
        }

        public T Value
        {
            get
            {
                return _Value;
            }
            set
            {
                //if (value == null)
                //    throw new ArgumentException("value is null or empty.", "value");
                _Value = value;
            }
        }

        public StylesCollection StylesCollection
        {
            get
            {
                return _StylesCollection;
            }
        }

        #region IButtonElement Members

        public ButtonGridStyles SnapStyle
        {
            get { return _StylesCollection.SnapStyle; }
        }

        public ButtonGridStyles DefaultStyle
        {
            get { return _StylesCollection.DefaultStyle; }
        }

        public ButtonGridStyles SelectStyle
        {
            get { return _StylesCollection.SelectStyle; }
        }

        #endregion
    }
}
