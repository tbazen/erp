using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class ButtonGridBodyButtonItem : ButtonGridItem
    {
        protected object _tag;
        protected ButtonApperance _ApperanceBody, _ApperanceHeader;

        public ButtonGridBodyButtonItem(bool newLine)
            : base(newLine)
        {
            _ApperanceBody = new ButtonApperance();
            _ApperanceHeader = new ButtonApperance();
        }

        public object Tag
        {
            get
            {
                return _tag;
            }
            set
            {
                _tag = value;
            }
        }

        public ButtonApperance ApperanceBody
        {
            get
            {
                return _ApperanceBody;
            }
        }

        public ButtonApperance ApperanceHeader
        {
            get
            {
                return _ApperanceHeader;
            }
        }



        public override bool HasText(string Text)
        {
            foreach (ButtonElement<string>  textElement in _ApperanceBody .Texts )
            {
                if (textElement.Value == Text)
                    return true;
            }
            foreach (ButtonElement<string> textElement in _ApperanceHeader .Texts)
            {
                if (textElement.Value == Text)
                    return true;
            }
            return false;
        }
    }

}