using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class ButtonGridGroupButtonItem : ButtonGridItem
    {
        ButtonApperance _Apperance;

        public ButtonGridGroupButtonItem()
            : base(true)
        {
            _Apperance = new ButtonApperance();
        }

        public ButtonApperance Apperance
        {
            get
            {
                return _Apperance;
            }
        }


        public override bool HasText(string Text)
        {
            foreach (ButtonElement<string>  textElement in _Apperance .Texts )
            {
                if (textElement.Value == Text)
                    return true;
            }
            return false;
        }

    }
}