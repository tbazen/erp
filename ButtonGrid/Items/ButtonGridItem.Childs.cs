using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.UI.ButtonGrid
{
    public interface IButtonChildSource:IEnumerable<ButtonGridItem>
    {
        bool Searchable{get;}
        void SetQuery(string query);
        string searchLabel { get; }
    }
 
    
    public abstract partial class ButtonGridItem
    {
        IButtonChildSource _childs;
        ButtonGridItem parent;

        public IButtonChildSource Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
 
        public ButtonGridItem Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        List<ButtonGridItem> ChildList
        {
            get
            {
                List<ButtonGridItem> ret = _childs as List<ButtonGridItem>;
                if (ret == null)
                {
                    ret = new List<ButtonGridItem>();
                    foreach (ButtonGridItem itm in _childs)
                    {
                        itm.parent = this;
                        ret.Add(itm);
                    }
                }
                return ret;
            }
        }

        public bool ButtonExists(ButtonGridItem child)
        {
            foreach (ButtonGridItem bgi in _childs)
            {
                if (bgi == child)
                    return true;
            }
            return false;
        }
        
        public IEnumerable<ButtonGridItem> getEnumerator()
        {
            foreach (ButtonGridItem node in getAllButtons(true))
                yield return node;
        }

        public List<ButtonGridItem> getAllButtons(bool topBottom)
        {
            List<ButtonGridItem> buttons = new List<ButtonGridItem>();
            ButtonGridItem rootButton = getRootButton();
            if (rootButton != null)
            {
                buttons.Add(rootButton);
                PopulateButtonList(rootButton, buttons);
            }
            if (!topBottom)
                buttons.Reverse();
            return buttons;
        }

        void PopulateButtonList(ButtonGridItem node, List<ButtonGridItem> nodes)
        {
            IButtonChildSource nodeChildren = node.getChildren();
            if (nodeChildren != null)
            {
                nodes.AddRange(nodeChildren);
                foreach (ButtonGridItem childNode in nodeChildren)
                    PopulateButtonList(childNode, nodes);
            }
        }

        public ButtonGridItem getRootButton()
        {
            ButtonGridItem par = this.parent;
            while (par.parent != null)
            {
                par = par.parent;
            }
            return par;
        }

        public List<ButtonGridItem> getAncestors()
        {
            List<ButtonGridItem> ancestors = new List<ButtonGridItem>();
            ButtonGridItem parent = this.parent;
            while (parent != null)
            {
                ancestors.Add(parent);
                parent = parent.parent;
            }

            return ancestors;
        }
    
        public bool HasChildern
        {
            get
            {
                return _childs != null;
                //if(_childs is List<ButtonGridItem>)
                //    return ChildList.Count>0;
                //return true;
            }
        }
        
        public IButtonChildSource getChildren()
        {
            return _childs;
        }
        
        internal List<ButtonGridItem> getDecendents()
        {
            List<ButtonGridItem> decendents = new List<ButtonGridItem>();
            AddChildren(this, decendents);
            return decendents;
        }

        void AddChildren(ButtonGridItem parentButton, List<ButtonGridItem> decendents)
        {
            foreach (ButtonGridItem child in parent.getChildren())
            {
                decendents.Add(child);
                AddChildren(child, decendents);
            }
        }

    }
}
