using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public abstract partial class ButtonGridItem
    {
        protected bool _newLine = true;

        public ButtonGridItem()
        {
        }

        public ButtonGridItem(bool newLine)
            : this()
        {
            this._newLine = newLine;
        }

        public bool NewLine
        {
            get { return _newLine; }
            set
            {
                _newLine = value;
            }
        }

        public abstract bool HasText(string Text);
            
    }

    public class StylesCollection
    {
        protected ButtonGridStyles _SnapStyle, _DefaultStyle, _SelectStyle;


        public StylesCollection()
        {
            _SnapStyle = new ButtonGridStyles();
            _DefaultStyle = new ButtonGridStyles();
            _SelectStyle = new ButtonGridStyles();
        }

        public StylesCollection(StylesCollection ParentStylesCollection)
            : this()
        {
            setParentStyleCollection(ParentStylesCollection);
        }

        public void setParentStyleCollection(StylesCollection ParentStylesCollection)
        {
            _DefaultStyle.SetParentStyle(ParentStylesCollection.DefaultStyle);
            _SnapStyle.SetParentStyle(ParentStylesCollection.SnapStyle);
            _SelectStyle.SetParentStyle(ParentStylesCollection.SelectStyle);
        }

        public ButtonGridStyles GetStyle(ShadeTypes shadeType)
        {
            if (shadeType == ShadeTypes.Default)
            {
                return _DefaultStyle;
            }
            else if (shadeType == ShadeTypes.Snap)
            {
                return _SnapStyle;
            }
            else if (shadeType == ShadeTypes.Select)
            {
                return _SelectStyle;
            }
            throw new NotImplementedException();
        }

        public ButtonGridStyles SnapStyle
        {
            get
            {
                return _SnapStyle;
            }
            set
            {
                _SnapStyle = value;
            }
        }

        public ButtonGridStyles DefaultStyle
        {
            get
            {
                return _DefaultStyle;
            }
            set
            {
                _DefaultStyle = value;
            }
        }

        public ButtonGridStyles SelectStyle
        {
            get
            {
                return _SelectStyle;
            }
            set
            {
                _SelectStyle = value;
            }
        }


    }
}