using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{

    public class ButtonGridNavigtorButtonItem : ButtonGridItem
    {
        ButtonApperance _Apperance;

        public ButtonGridNavigtorButtonItem()
            : base(false)
        {
            _Apperance = new ButtonApperance();
        }

        public ButtonApperance Apperance
        {
            get
            {
                return _Apperance;
            }
        }

        public override bool HasText(string Text)
        {
            foreach (ButtonElement<string> textElement in _Apperance.Texts)
            {
                if (textElement.Value == Text)
                    return true;
            }
            return false;
        }

    }

}
