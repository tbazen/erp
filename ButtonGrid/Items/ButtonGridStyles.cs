using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class ButtonGridStyles
    {
        Dictionary<StyleType, object> _StylesList;
        ButtonGridStyles _ParentStyle;

        public ButtonGridStyles()
        {
            _StylesList = new Dictionary<StyleType, object>();
        }

        public ButtonGridStyles(ButtonGridStyles ParentStyle)
            : this()
        {
            _ParentStyle = ParentStyle;
        }

        public void Add(StyleType styleType, object value)
        {
            if (_StylesList.ContainsKey(styleType))
                _StylesList.Remove(styleType);
            _StylesList.Add(styleType, value);


        }

        public void Remove(StyleType styleType)
        {
            _StylesList.Remove(styleType);
        }

        public object GetValue(StyleType styleType)
        {
            if (_StylesList.ContainsKey(styleType))
                return _StylesList[styleType];
            if (_ParentStyle == null)
                return null;
            return _ParentStyle.GetValue(styleType);
        }

        public void SetParentStyle(ButtonGridStyles parentStyle)
        {
            _ParentStyle = parentStyle;
        }


        public int ButtonWidth
        {
            get
            {
                return (int)GetValue(StyleType.ButtonWidth);
            }
        }

        public int ButtonHeight
        {
            get
            {
                return (int)GetValue(StyleType.ButtonHeight);
            }
        }

        public int CornerRadius
        {
            get
            {
                return (int)GetValue(StyleType.CornerRadius);
            }
        }

        public Point Position
        {
            get
            {
                return (Point)GetValue(StyleType.Position);
            }
        }

        public ImageAlignment ImageAlignment
        {
            get
            {
                return (ImageAlignment)GetValue(StyleType.ImageAlignment);
            }
        }

        public StringFormat TextFormat
        {
            get
            {
                return (StringFormat)GetValue(StyleType.StringFormat);
            }
        }

        public Font Font
        {
            get
            {
                return (Font)GetValue(StyleType.Font);
            }
        }

        public Brush FontBrush
        {
            get
            {
                return (Brush)GetValue(StyleType.FontBrush );
            }
        }

        public Brush FillBrush
        {
            get
            {
                return (Brush)GetValue(StyleType.FillBrush);
            }
        }

        public Pen BorderPen
        {
            get
            {
                return (Pen)GetValue(StyleType.BroderPen);
            }
        }
    }

}