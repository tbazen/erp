using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{

    class FileButtongGridItem : ButtonGridBodyButtonItem,IEquatable<FileButtongGridItem>   
    {
        private string _Path;
        public FileButtongGridItem(bool folder)
            :base(false)
        {
        }

        public string Path
        {
            get
            {
                return _Path;
            }
            set
            {
            	_Path  = value;
            }
        }

        #region IEquatable<FileButtongGridItem> Members

        public bool Equals(FileButtongGridItem other)
        {
            if (other == null)
                return false;
            return _Path == other._Path;
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as FileButtongGridItem);
        }
        
        #endregion
    }
 
    public class FolderEnumerator :IButtonChildSource,IEnumerator<ButtonGridItem>
    {
        private string _Path;
        string[] files;
        string[] folders;
        int index;
        ButtonGrid container;
        ButtonGridItem _parentItem;
        public FolderEnumerator(string path, ButtonGrid container,ButtonGridItem parentItem)
        {
            _Path = path;
            _parentItem = parentItem;
            this.container = container;
        }

        #region IEnumerable<ButtonGridItem> Members
        string _query=null;
        public IEnumerator<ButtonGridItem> GetEnumerator()
        {
            try
            {
                if (string.IsNullOrEmpty(_query))
                    folders = Directory.GetDirectories(_Path);
                else
                    folders = Directory.GetDirectories(_Path,_query);
            }
            catch
            {
                folders = new string[] { };
            }
            try
            {
                if (string.IsNullOrEmpty(_query))
                    files = Directory.GetFiles(_Path);
                else
                    files = Directory.GetFiles(_Path,_query);
            }
            catch
            {
                files = new string[0];
            }
            index = -1;
            return this;
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion


        #region IEnumerator<ButtonGridItem> Members

        public ButtonGridItem Current
        {
            get
            {
                if (index < folders.Length)
                {
                    FileButtongGridItem bg = new FileButtongGridItem(true);
                    bg.Path = folders[index];
                    bg.ApperanceBody .AddText(new System.IO.DirectoryInfo(folders[index]).Name);
                    //bg.DefaultStyle.Add( StyleType.ButtonHeight, 80);
                    container.AddButtonGridItem(bg, new FolderEnumerator(folders[index], container, bg));
                    //container.AddText(bg, "Test");
                    container.RelateWithParent(_parentItem, bg);
                    return bg;
                }
                else
                {
                    FileButtongGridItem item = new FileButtongGridItem(false);
                    item.Path = files[index - folders.Length];
                    item.ApperanceBody .AddText(new System.IO.FileInfo(files[index - folders.Length]).Name);
                    item.ApperanceBody .StylesCollection.DefaultStyle.Add( StyleType.ButtonHeight, 80);
                    item.ApperanceBody .Texts[0].DefaultStyle.Add(StyleType.StringFormat,
                        StringFormatExtensions.GetStringFormatVert(StringAlignment.Far)
                        );
                    container.RelateWithParent(_parentItem, item);
                    //container.AddButtonGridItem(this, new FolderEnumerator(folders[index], container));

                    //ButtonElement<string> ButtonElement = new ButtonElement<string>("Test");
                    //bg.AddText(ButtonElement); 
                    return item;
                }
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        #region IEnumerator Members


        public bool MoveNext()
        {
            index++;
            return index < files.Length + folders.Length;
        }

        public void Reset()
        {
            index = 0;
        }

        #endregion

        #region IButtonChildSource Members

        public bool Searchable
        {
            get { return true; }
        }

        public void SetQuery(string query)
        {
            _query = query;
        }

        #endregion

        #region IEnumerator Members

        object IEnumerator.Current
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        #endregion


        public string searchLabel
        {
            get { return "Folder Name:"; }
        }
    }
}
