using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    public static class GraphicsExtensions
    {
        public static void DrawImage(Graphics g, Image image, Rectangle rect, ImageAlignment imageAlignment)
        {
            Point position = Point.Empty;
            //postion x
            if (imageAlignment == ImageAlignment.LeftTop || imageAlignment == ImageAlignment.LeftMiddle || imageAlignment == ImageAlignment.LeftBottom)
            {
                position.X = 0;
            }
            else if (imageAlignment == ImageAlignment.CenterTop || imageAlignment == ImageAlignment.Center || imageAlignment == ImageAlignment.CenterBottom)
            {
                position.X = (rect.Width - image.Width) / 2;
            }
            else
            {
                position.X = rect.Width - image.Width;
            }
            //position y
            if (imageAlignment == ImageAlignment.LeftTop || imageAlignment == ImageAlignment.CenterTop  || imageAlignment == ImageAlignment.RightTop )
            {
                position.Y = 0;
            }
            else if (imageAlignment == ImageAlignment.LeftMiddle  || imageAlignment == ImageAlignment.Center || imageAlignment == ImageAlignment.RightMiddle )
            {
                position.Y = (rect.Height - image.Height) / 2;
            }
            else
            {
                position.Y = rect.Height - image.Height;
            }

            //g.DrawImage(image, rect, position.X, position.Y, image.Width, image.Height, GraphicsUnit.Pixel);
            g.DrawImage(ImageExtensions.ClippedImage(image, rect, position), rect.Location);
        }
    }
}
