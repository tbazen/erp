using System;
using System.Collections.Generic;

namespace INTAPS.UI.ButtonGrid
{
    public enum ImageAlignment
    {
        LeftTop, LeftMiddle, LeftBottom, RightTop, RightMiddle, RightBottom, CenterTop, Center, CenterBottom
    }
}
