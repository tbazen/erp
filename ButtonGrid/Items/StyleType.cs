using System;
using System.Collections.Generic;

namespace INTAPS.UI.ButtonGrid
{
    [Flags]
    public enum StyleType
    {
        //NavigatorButton = 2,
        //HeaderButton = 4,
        //GroupButton = 8,
        //BodyButton = 16,
        BroderPen = 32,
        FillBrush = 64,
        CornerRadius = 128,
        Font = 256,
        StringFormat = 512,
        Margin = 1024,
        ImageAlignment = 2048,
        ButtonWidth = 4096,
        ButtonHeight = 8192,
        Position = 16384,
        FontBrush = 32768,
    }
}
