using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.UI.ButtonGrid
{

    //[Serializable ]
    //public class TreeRelationShipManager<T>
    //    where T : class 
    //{

    //    List<ParentChildRelationShip<T>> relationShips = new List<ParentChildRelationShip<T>>();
        
    //    public List<ParentChildRelationShip<T>> RelationShips
    //    {
    //        get { return relationShips; }
    //    }

    //    public void AddRelationShip(ParentChildRelationShip<T> relationShip)
    //    {
    //        List<ParentChildRelationShip<T>> ChildObjPreRelationShips = getRelationShips(relationShip.ChildObj);
    //        List<ParentChildRelationShip<T>> ParentObjPreRelationShips = getRelationShips(relationShip.ParentObj);
    //        /*if (relationShip.ParentObj != null && ParentObjPreRelationShips.Count == 0) 
    //            throw new Exception("Parentt object doesn't exist on the tree");
    //         */ 
    //        if (ChildObjPreRelationShips.Count != 0) throw new Exception("Child object is already assigned to a parent");
    //        relationShips.Add(relationShip);
    //    }

    //    public void RemoveRelationShip(T nodeObj)
    //    {
    //        foreach (ParentChildRelationShip<T> relationShip in getRelationShips(nodeObj))
    //            relationShips.Remove(relationShip);
    //    }

    //    public bool NodeExists(T nodeObj)
    //    {
    //        foreach (ParentChildRelationShip<T> relationShip in relationShips)
    //            if (relationShip.Contains(nodeObj))
    //                return true;
    //        return false;
    //    }

    //    public List<ParentChildRelationShip<T>> getRelationShips(T nodeObj)
    //    {
    //        List<ParentChildRelationShip<T>> nodeObjRelationShips = new List<ParentChildRelationShip<T>>();
    //        if (nodeObj != null)
    //            foreach (ParentChildRelationShip<T> relationShip in relationShips)
    //                if (relationShip.Contains(nodeObj))
    //                    nodeObjRelationShips.Add(relationShip);
    //        return nodeObjRelationShips;
    //    }

    //    public IEnumerable<T> getEnumerator()
    //    {
    //        foreach (T node in getAllNodes(true))
    //            yield return node;
    //    }

    //    public List<T> getAllNodes(bool topBottom)
    //    {
    //        List<T> nodes = new List<T>();
    //        T rootNode = getRootNode();
    //        if (rootNode != null)
    //        {
    //            nodes.Add(rootNode);
    //            PopulateNodeList(rootNode, nodes);
    //        }
    //        if (!topBottom)
    //            nodes.Reverse();
    //        return nodes;
    //    }

    //    void PopulateNodeList(T node, List<T> nodes)
    //    {
    //        List<T> nodeChildren=getChildren(node);
    //        nodes.AddRange(nodeChildren);
    //        foreach (T childNode in nodeChildren)
    //            PopulateNodeList(childNode, nodes);
    //    }

    //    public T getRootNode()
    //    {
    //        foreach (ParentChildRelationShip<T> rs in relationShips)
    //            if (rs.ParentObj  == null )
    //                return rs.ChildObj ;
    //        return null;
    //    }

    //    internal List<T> getAncestors(T nodeObj)
    //    {
    //        List<T> ancestors = new List<T>();
    //        T parent = getParent(nodeObj);
    //        while (parent != null)
    //        {
    //            ancestors.Add(parent);
    //            parent = getParent(parent);
    //        }

    //        return ancestors;
    //    }

    //    internal T getParent(T nodeObj)
    //    {
    //        foreach (ParentChildRelationShip<T> rs in relationShips)
    //            if (rs.ChildObj == nodeObj)
    //                return rs.ParentObj;
    //        return null;
    //    }

    //    internal List<T> getChildren(T nodeObj)
    //    {
    //        List<T> children = new List<T>();
    //        foreach (ParentChildRelationShip<T> rs in relationShips)
    //            if (rs.ParentObj == nodeObj)
    //            {
    //                if (rs.ChildObj != null)
    //                    children.Add(rs.ChildObj);
    //                {
    //                    foreach (T ch in rs.childList)
    //                    {
    //                        children.Add(ch);
    //                    }
    //                    break;
    //                }

    //            }
    //        return children;
    //    }

    //    internal List<T> getDecendents(T nodeObj)
    //    {
    //        List<T> decendents = new List<T>();
    //        AddChildren(nodeObj, decendents);
    //        return decendents;
    //    }

    //    void AddChildren(T parentNode, List<T> decendents)
    //    {
    //        foreach (T child in getChildren(parentNode))
    //        {
    //            decendents.Add(child);
    //            AddChildren(child, decendents);
    //        }
    //    }
    //}

    [Serializable]
    public class ParentChildRelationShip<T> 
        where T : class
    {
        T parentObj;
        T childObj;
        public IEnumerable<T> childList;

        public T ParentObj
        {
            get { return parentObj; }
            set { parentObj = value; }
        }

        public T ChildObj
        {
            get { return childObj; }
            set { childObj = value; }
        }


        public ParentChildRelationShip(T parentObj, T childObj)
            : this(parentObj, childObj, null)
        {

        }

        public ParentChildRelationShip(T parentObj, T childObj, IEnumerable<T> childList)
        {
            this.childObj = childObj;
            this.parentObj = parentObj;
            this.childList = childList;
        }

        public bool Contains(T obj)
        {
            return childObj == obj || parentObj == obj;
        }
    }
}
