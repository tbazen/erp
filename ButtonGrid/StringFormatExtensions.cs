using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    public static class StringFormatExtensions
    {
        public static  readonly StringFormat DefaultTextFormat;

        static StringFormatExtensions()
        {
            DefaultTextFormat = GetStringFormat(StringAlignment.Far  , StringAlignment.Center , StringFormatFlags.FitBlackBox, StringTrimming.EllipsisWord);
        }
        public static StringFormat GetStringFormatHor(StringAlignment HorizontalAlignment)
        {
            return GetStringFormat(StringAlignment.Center, HorizontalAlignment, StringFormatFlags.FitBlackBox, StringTrimming.None);
        }
        public static StringFormat GetStringFormatVert(StringAlignment VerticalAlignment)
        {
            return GetStringFormat(VerticalAlignment, StringAlignment.Center, StringFormatFlags.FitBlackBox, StringTrimming.None);
        }
        public static StringFormat GetStringFormat(StringAlignment VerticalAlignment, StringAlignment HorizontalAlignment)
        {
            return GetStringFormat(VerticalAlignment, HorizontalAlignment, StringFormatFlags.FitBlackBox, StringTrimming.None);
        }
        public static StringFormat GetStringFormat(StringAlignment VerticalAlignment, StringAlignment HorizontalAlignment, StringFormatFlags FormatFlags, StringTrimming Trimming)
        {
            StringFormat DefaultTextFormat = new StringFormat();
            DefaultTextFormat.Alignment = HorizontalAlignment ;
            DefaultTextFormat.LineAlignment = VerticalAlignment ;
            DefaultTextFormat.FormatFlags = FormatFlags;
            DefaultTextFormat.Trimming = Trimming;
            return DefaultTextFormat;
        }
    }
}
