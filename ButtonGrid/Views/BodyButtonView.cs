using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class BodyButtonView : ItemView
    {
        protected ButtonGridBodyButtonItem _ButtonItem;

        public BodyButtonView(ButtonGridBodyButtonItem buttonItem)
            : base()
        {
            this._ButtonItem = buttonItem;


        }

        public override ButtonGridItem ButtonGridItem
        {
            get { return _ButtonItem; }
        }


        public override ButtonApperance Apperance
        {
            get { return _ButtonItem.ApperanceBody; }
        }

        public override bool ContainPoint(Point p)
        {
            return _position.X <= p.X && p.X <= _position.X + _ButtonItem.ApperanceBody.StylesCollection .DefaultStyle .ButtonWidth
                && _position.Y <= p.Y && p.Y <= _position.Y + _ButtonItem.ApperanceBody.StylesCollection.DefaultStyle.ButtonHeight;
        }

        public override void Draw(Graphics g, ShadeTypes shadeType)
        {
            ButtonGridStyles style = _ButtonItem.ApperanceBody.StylesCollection.GetStyle(shadeType);
            _buttonRect = new Rectangle(_position.X, _position.Y
                , (int)style.GetValue( StyleType.ButtonWidth)
                , (int)style.GetValue( StyleType.ButtonHeight)
                );
            setImageRectAndTextRectSizeToTotalButtonSize(_buttonRect.Width, _buttonRect.Height);
            DrawButtonBoundRoundedRectangle(g
                , (Pen)style.GetValue( StyleType.BroderPen)
                , (Brush)style.GetValue( StyleType.FillBrush)
                , (int)style.GetValue( StyleType.CornerRadius));

            DrawImages(g, shadeType, _ButtonItem.ApperanceBody.Images, _imageRect);
            DrawTexts(g, shadeType, _ButtonItem.ApperanceBody.Texts, _textRect);
        }

    }
}