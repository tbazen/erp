using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class GroupButtonView : ItemView
    {
        ButtonGridGroupButtonItem _ButtonItem;
        int _groupBodyWidth;
        int _groupBodyHeight;

        public GroupButtonView(ButtonGridGroupButtonItem buttonItem)
            : base()
        {
            this._ButtonItem = buttonItem;
        }

        public int GroupBodyWidth
        {
            get { return _groupBodyWidth; }
            set { _groupBodyWidth = value; }
        }

        public int GroupBodyHeight
        {
            get { return _groupBodyHeight; }
            set { _groupBodyHeight = value; }
        }

        public override ButtonApperance Apperance
        {
            get { return _ButtonItem.Apperance ; }
        }

        public void UpdateBrush()
        {
            Brush brush = (Brush)_ButtonItem.Apperance.StylesCollection.DefaultStyle.GetValue(StyleType.FillBrush);
            if (brush is LinearGradientBrush)
                if (this._groupBodyWidth > 0 || this._groupBodyHeight > 0)
                {
                    LinearGradientBrush gradientBrush = (LinearGradientBrush)brush;
                    _ButtonItem.Apperance.StylesCollection.DefaultStyle.Add(StyleType.FillBrush, new LinearGradientBrush(new Rectangle(this._position.X, this._position.Y, this._groupBodyWidth, this._groupBodyHeight + _ButtonItem.Apperance.StylesCollection.DefaultStyle.ButtonHeight), gradientBrush.LinearColors[0], gradientBrush.LinearColors[1], 45));
                }
        }

        public override ButtonGridItem ButtonGridItem
        {
            get { return _ButtonItem; }
        }

        public override bool ContainPoint(Point p)
        {
            return _position.X <= p.X && p.X <= _position.X + _ButtonItem.Apperance.StylesCollection.DefaultStyle.ButtonWidth
                && _position.Y <= p.Y && p.Y <= _position.Y + _ButtonItem.Apperance.StylesCollection.DefaultStyle.ButtonHeight;
        }


        public override void Draw(Graphics g, ShadeTypes shadeType)
        {
            _buttonRect = new Rectangle(_position.X, _position.Y
                , _groupBodyWidth
                , _groupBodyHeight + this.Apperance.StylesCollection.DefaultStyle.ButtonHeight);
            _imageRect.Location = _position;
            _imageRect.Width = _buttonRect.Width;
            _imageRect.Height = _buttonRect.Height;
            _textRect.Location = _imageRect.Location;
            _textRect.Width = _imageRect.Width;
            _textRect.Height = _imageRect.Height;
            ButtonGridStyles style = _ButtonItem.Apperance.StylesCollection.GetStyle(shadeType);
            DrawButtonBoundRoundedRectangle(g
                , (Pen)style.GetValue( StyleType.BroderPen)
                , (Brush)style.GetValue( StyleType.FillBrush)
                , (int)style.GetValue( StyleType.CornerRadius));
            DrawImages(g, shadeType, _ButtonItem.Apperance.Images, _imageRect);
            DrawTexts(g, shadeType, _ButtonItem.Apperance.Texts, _textRect);
        }
    }
}
