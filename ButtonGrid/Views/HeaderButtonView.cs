using System;
using System.Collections.Generic;
using System.Drawing;

namespace INTAPS.UI.ButtonGrid
{
    [Serializable]
    public class HeaderButtonView : ItemView
    {
        protected ButtonGridBodyButtonItem _ButtonItem;

        public HeaderButtonView(ButtonGridBodyButtonItem groupItem)
            : base()
        {
            this._ButtonItem = groupItem;
        }


        public override ButtonGridItem ButtonGridItem
        {
            get { return _ButtonItem; }
        }

        public override ButtonApperance Apperance
        {
            get { return _ButtonItem.ApperanceHeader ; }
        }

        public override void Draw(Graphics g, ShadeTypes shadeType)
        {
            ButtonGridStyles style = _ButtonItem.ApperanceHeader.StylesCollection.GetStyle(shadeType);
            _buttonRect = new Rectangle(_position.X, _position.Y
                , (int)style.GetValue( StyleType.ButtonWidth)
                , (int)style.GetValue( StyleType.ButtonHeight)
                );
            //setImageRectAndTextRectSizeToSplittedButtonSize(_buttonRect.Width, _buttonRect.Height);
            setImageRectAndTextRectSizeToTotalButtonSize(_buttonRect.Width, _buttonRect.Height);
            DrawButtonBoundRoundedRectangle (g
                , (Pen)style.GetValue( StyleType.BroderPen)
                , (Brush)style.GetValue( StyleType.FillBrush)
                , (int)style.GetValue( StyleType.CornerRadius));
            DrawImages(g, shadeType, _ButtonItem.ApperanceHeader.Images, _imageRect);
            DrawTexts(g, shadeType, _ButtonItem.ApperanceHeader .Texts, _textRect);
        }

        public override bool ContainPoint(Point p)
        {
            return _position.X <= p.X && p.X <= _position.X + _ButtonItem.ApperanceHeader .StylesCollection.DefaultStyle.ButtonWidth
                && _position.Y <= p.Y && p.Y <= _position.Y + _ButtonItem.ApperanceHeader.StylesCollection.DefaultStyle.ButtonHeight;
        }
    }
}
