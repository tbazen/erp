using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{

    public abstract class ItemView
    {
        protected Point _position;
        protected Rectangle _imageRect;
        protected Rectangle _textRect;
        protected Rectangle _buttonRect;

        public ItemView()
        {

        }

        public abstract bool ContainPoint(Point p);
        public abstract void Draw(Graphics g, ShadeTypes shadeType);
        public abstract ButtonGridItem ButtonGridItem
        {
            get;
        }

        public abstract ButtonApperance Apperance
        {
            get;
        }

        public Point Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }

        private void getButtonWidthAndHeight(ShadeTypes shadeType, out int buttonWidth, out int buttonHeight)
        {
            buttonWidth = 0;
            buttonHeight = 0;
            _imageRect.X = _position.X;
            _imageRect.Y = _position.Y;
            if (shadeType == ShadeTypes.Default)
            {
                buttonWidth = this.Apperance.StylesCollection.DefaultStyle .ButtonWidth; 
                buttonHeight = this.Apperance.StylesCollection.DefaultStyle.ButtonHeight; 
            }
            else if (shadeType == ShadeTypes.Snap)
            {
                buttonWidth = this.Apperance.StylesCollection.SnapStyle .ButtonWidth ; 
                buttonHeight = this.Apperance.StylesCollection.SnapStyle .ButtonHeight; 
            }
            else if (shadeType == ShadeTypes.Select)
            {
                buttonWidth = this.Apperance.StylesCollection.SelectStyle .ButtonWidth;
                buttonHeight = this.Apperance.StylesCollection.SelectStyle.ButtonHeight;
            }
        }

        public void setImageRectAndTextRectSizeToTotalButtonSize(int buttonWidth, int buttonHeight)
        {
            _imageRect.X = _position.X;
            _imageRect.Y = _position.Y;
            _imageRect.Height = buttonHeight;
            _imageRect.Width = buttonWidth;
            _textRect.X = _imageRect.X;
            _textRect.Y = _imageRect.Y;
            _textRect.Height = _imageRect.Height;
            _textRect.Width = _imageRect.Width;
        }

        public void setImageRectAndTextRectSizeToSplittedButtonSize(int buttonWidth, int buttonHeight)
        {
            _imageRect.X = _position.X;
            _imageRect.Y = _position.Y;
            _imageRect.Height = buttonHeight;
            _imageRect.Width = _imageRect.Height;
            _textRect.X = _position.X + _imageRect.Width;
            _textRect.Y = _position.Y;
            _textRect.Height = buttonHeight;
            _textRect.Width = buttonWidth - _textRect.Height;

            //int margin = 0;
            //if (this is GroupButtonView)
            //    margin = GroupButtonView.margin;
            //else if (this is ButtonView)
            //    margin = ButtonView.margin;
            //else if (this is PageNavigatorButtonView)
            //    margin = PageNavigatorButtonView.margin;
            //else
            //    throw new Exception("not implimented.");

            //imageRect.X += margin;
            //imageRect.Y += margin;
            //imageRect.Width -= (2 * margin);
            //imageRect.Height -= margin + (margin / 4);

            //textRect.X += margin;
            //textRect.Y += margin / 4;
            //textRect.Width -= (2 * margin);
            //textRect.Height -= margin + (margin / 4);


        }

        public void SetButtonRect(Graphics g, ShadeTypes shadeType)
        {
            if (shadeType == ShadeTypes.Default)
            {
                _buttonRect = new Rectangle(_position.X, _position.Y, this.Apperance.StylesCollection.DefaultStyle.ButtonWidth , this.Apperance.StylesCollection.DefaultStyle.ButtonHeight);
            }
            else if (shadeType == ShadeTypes.Snap)
            {
                _buttonRect = new Rectangle(_position.X, _position.Y, this.Apperance.StylesCollection.SnapStyle.ButtonWidth, this.Apperance.StylesCollection.SnapStyle.ButtonHeight);
            }
            else if (shadeType == ShadeTypes.Select)
            {
                _buttonRect = new Rectangle(_position.X, _position.Y, this.Apperance.StylesCollection.SelectStyle.ButtonWidth, this.Apperance.StylesCollection.SelectStyle.ButtonHeight);
            }
        }

        public static void DrawImages(Graphics g, ShadeTypes shadeType, List<ButtonElement<Image>> Images, Rectangle ImageRect)
        {
            ButtonGridStyles imageStyle;
            for (int i = 0; i < Images.Count; i++)
            {
                ButtonElement<Image> buttonImage = Images[i];
                imageStyle = buttonImage.StylesCollection.GetStyle(shadeType);
                GraphicsExtensions.DrawImage(g
                    , ImageExtensions.RoundCornerImage(buttonImage.Value, imageStyle.CornerRadius, Color.Transparent)
                    , ImageRect
                    , imageStyle.ImageAlignment);
            }
        }

        public static void DrawTexts(Graphics g, ShadeTypes shadeType, List<ButtonElement<string>> Texts, Rectangle _textRect)
        {
            for (int i = 0; i < Texts.Count; i++)
            {
                ButtonElement<string> buttonText = Texts[i];
                ButtonGridStyles textStyle;
                if (buttonText == null) return;
                textStyle = buttonText.StylesCollection.GetStyle(shadeType);
                g.DrawString(buttonText.Value
                                    , (Font)textStyle.GetValue(StyleType.Font)
                                    , (Brush)textStyle.GetValue(StyleType.FontBrush)
                                    , _textRect
                                    , (StringFormat)textStyle.GetValue(StyleType.StringFormat)
                                    );
            }
        }

        public void DrawButtonBoundRoundedRectangle(Graphics g, Pen BorderPen, Brush FillBrush, int CornerRadius)
        {
            GraphicsExtension.DrawRoundedRectangle(g, BorderPen, _buttonRect, CornerRadius);
            GraphicsExtension.FillRoundedRectangle(g, FillBrush, _buttonRect, CornerRadius);
        }

        public void DrawButtonBoundCircle(Graphics g, Pen BorderPen, Brush FillBrush)
        {
            GraphicsPath path = new GraphicsPath();
            path.Reset();
            path.AddArc(_buttonRect, 0, 360);
            g.FillPath(FillBrush, path);
            g.DrawPath(BorderPen, path);
            path.Dispose();

        }
    }

}