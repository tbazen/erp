using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace INTAPS.UI.ButtonGrid
{
    public class PageNavigatorButtonView : ItemView
    {
        protected ButtonGridNavigtorButtonItem _ButtonItem;

        public PageNavigatorButtonView(ButtonGridNavigtorButtonItem buttonItem)
            : base()
        {
            this._ButtonItem = buttonItem;
        }

        public ButtonGridNavigtorButtonItem ButtonGridNavigtorButtonItem
        {
            get { return _ButtonItem; }
        }

        public override ButtonGridItem ButtonGridItem
        {
            get { return _ButtonItem; }
        }

        public override ButtonApperance Apperance
        {
            get { return _ButtonItem.Apperance; }
        }


        public override void Draw(Graphics g, ShadeTypes shadeType)
        {
            ButtonGridStyles style = _ButtonItem.Apperance.StylesCollection.GetStyle(shadeType);
            _buttonRect = new Rectangle(_position.X, _position.Y
                , (int)style.GetValue( StyleType.ButtonWidth)
                , (int)style.GetValue( StyleType.ButtonHeight)
                );
            setImageRectAndTextRectSizeToTotalButtonSize(_buttonRect.Width, _buttonRect.Height);
            DrawButtonBoundCircle(g
                , (Pen)style.GetValue( StyleType.BroderPen)
                , (Brush)style.GetValue( StyleType.FillBrush));
            DrawImages(g, shadeType, _ButtonItem.Apperance.Images, _imageRect);
            DrawTexts(g, shadeType, _ButtonItem.Apperance.Texts, _textRect);
        }

        public override bool ContainPoint(Point p)
        {
            return _position.X <= p.X && p.X <= _position.X + _ButtonItem.Apperance .StylesCollection.DefaultStyle.ButtonWidth
                && _position.Y <= p.Y && p.Y <= _position.Y + _ButtonItem.Apperance.StylesCollection.DefaultStyle.ButtonHeight;
        }

    }
}
