﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace BIZNET.iERP.Client.DeclarationDesign
{
    public partial class IncomeTaxSummary : DevExpress.XtraReports.UI.XtraReport
    {
        public IncomeTaxSummary()
        {
            InitializeComponent();
            //TypedDataSets.IncomeTaxDeclarationData data=new BIZNET.iERP.TypedDataSets.IncomeTaxDeclarationData();
            //data.Summary.AddSummaryRow("","","","","","","","","","","",0,0,0,0,0,0,0,0,0,0,0,"");
            //data.ResignedEmployees.AddResignedEmployeesRow("1","0000234","Abebe",data.Summary[0]);
            //subResigned.ReportSource.DataSource = data;
        }
        protected override void OnDataSourceDemanded(EventArgs e)
        {
            base.OnDataSourceDemanded(e);
            subResigned.ReportSource.DataSource = this.DataSource;
        }
        private void GroupFooter1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }
        
        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //((IncomeTaxResignedEmployees)((XRSubreport)sender).ReportSource).DataSource = this.DataSource;
        }

    }
}
