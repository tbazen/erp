using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.Evaluator
{
    public partial class ExpressionTextBuilder : Form
    {

        public string Expression;
        
        private Symbolic m_Parent;

        public ExpressionTextBuilder()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Expression = this.Formula.Formula;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.manager != null))
            {
                this.manager.Dispose();
            }
            base.Dispose(disposing);
        }

       

        public DialogResult ShowExpression(string exp, Symbolic Parent)
        {
            this.m_Parent = Parent;
            this.Formula.m_Parent = this.m_Parent;
            this.Formula.Formula = exp;
            return base.ShowDialog();
        }
    }
}

