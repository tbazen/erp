using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
namespace INTAPS.Evaluator
{
    public class FormulaEditor : RichTextBox
    {
        private ContextMenu ctxtTextBox;
        private Font FormulaSelectionFont;
        private int m_FocusPos;
        private bool m_Formating = false;
        private int m_FoucsLen = -1;
        public Symbolic m_Parent = null;
        private int m_SL;
        private int m_SP;
        private MenuItem mebBuildPlainText;
        private MenuItem menBuild;
        private MenuItem menBuildExpressionText;
        private MenuItem menBuildFuncation;
        private MenuItem menBuildList;
        private MenuItem menBuildText;
        private MenuItem menCopy;
        private MenuItem menCut;
        private MenuItem menPaste;
        private MenuItem menSubExpression;

        public event EventHandler FormulaChanged;

        public FormulaEditor()
        {
            this.InitializeMenu();
            this.FormulaSelectionFont = new Font(this.Font.FontFamily, this.Font.Size, this.Font.Style | FontStyle.Underline);
        }

        private void Build()
        {
            this.Build(false);
        }

        private void Build(bool ShowSubExpression)
        {
            if (this.m_FoucsLen != -1)
            {
                Symbolic s = new Symbolic();
                s.m_ISymbolProvider = this.m_Parent.m_ISymbolProvider;
                s.Expression = this.Text;
                SubExpression subExpressionSpan = s.GetSubExpressionSpan(base.SelectionStart);
                s.Expression = s.Expression.Substring(this.m_FocusPos, this.m_FoucsLen);
                if (ShowSubExpression)
                {
                    this.BuildFormula(s, true);
                }
                else
                {
                    switch (subExpressionSpan.Type)
                    {
                        case SubExpressionType.FunctionCall:
                            this.BuildFunction(s, true);
                            return;

                        case SubExpressionType.Text:
                            this.BuildText(s, true);
                            return;

                        case SubExpressionType.List:
                            this.BuildList(s, true);
                            return;
                    }
                    this.BuildFormula(s, true);
                }
            }
        }

        private void BuildExpression(Symbolic exp, bool Replace)
        {
            string expression = exp.Expression;
            ExpressionTextBuilder builder = new ExpressionTextBuilder();
            if (builder.ShowExpression(TextBuilder.UnBuildText(expression), this.m_Parent) == DialogResult.OK)
            {
                if (Replace)
                {
                    this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                }
                this.SelectedText = TextBuilder.BuildText(builder.Expression);
                if (Replace)
                {
                    this.EndFormat();
                }
                this.OnSelectionChanged(null);
            }
        }

        private void BuildFormula(Symbolic s, bool Replace)
        {
            ExpressionTextBuilder builder = new ExpressionTextBuilder();
            if (builder.ShowExpression(s.Expression, s) == DialogResult.OK)
            {
                if (Replace)
                {
                    this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                }
                this.SelectedText = builder.Expression;
                if (Replace)
                {
                    this.EndFormat();
                }
                this.OnSelectionChanged(null);
            }
        }

        private void BuildFunction(Symbolic exp, bool Replace)
        {
            FunctionsList list = new FunctionsList();
            if (list.ShowFunctioList(exp) == DialogResult.OK)
            {
                if (Replace)
                {
                    this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                }
                this.SelectedText = list.Expression;
                if (Replace)
                {
                    this.EndFormat();
                }
                this.OnSelectionChanged(null);
            }
        }

        private void BuildList(Symbolic exp, bool Replace)
        {
            ListBuilder builder = new ListBuilder();
            if (builder.ShowList(exp.GetSubExpressions(1), exp) == DialogResult.OK)
            {
                if (Replace)
                {
                    this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                }
                this.SelectedText = builder.Expression;
                if (Replace)
                {
                    this.EndFormat();
                }
                this.OnSelectionChanged(null);
            }
        }

        private void BuildText(Symbolic exp, bool Replace)
        {
            string expression = exp.Expression;
            TextEditor editor = new TextEditor();
            if (editor.ShowText(exp, expression) == DialogResult.OK)
            {
                if (Replace)
                {
                    this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                }
                this.SelectedText = editor.BuiltText;
                if (Replace)
                {
                    this.EndFormat();
                }
                this.OnSelectionChanged(null);
            }
        }

        private void ctxtTextBox_Popup(object sender, EventArgs e)
        {
            this.menPaste.Enabled = Clipboard.ContainsText();
            this.menCopy.Enabled = this.SelectionLength > 0;
            this.menCut.Enabled = this.SelectionLength > 0;
        }

        private void DoContext()
        {
        }

        private void DoDoubleClick()
        {
            this.Build();
        }

        private void EndFormat()
        {
            base.SelectionStart = this.m_SP;
            this.SelectionLength = this.m_SL;
            this.m_Formating = false;
        }

        private Symbolic GetEmptySymbolic()
        {
            Symbolic symbolic = new Symbolic();
            symbolic.m_ISymbolProvider = this.m_Parent.m_ISymbolProvider;
            symbolic.Expression = "";
            return symbolic;
        }

        private void InitializeMenu()
        {
            this.ctxtTextBox = new ContextMenu();
            this.menBuild = new MenuItem("&Build");
            this.menBuildFuncation = new MenuItem("Insert &Function");
            this.menBuildText = new MenuItem("&Insert Text");
            this.mebBuildPlainText = new MenuItem("Insert &Text");
            this.menBuildExpressionText = new MenuItem("Insert &Expression");
            this.menBuildList = new MenuItem("Insert &List");
            this.menCopy = new MenuItem("&Copy");
            this.menCut = new MenuItem("&Cut");
            this.menPaste = new MenuItem("&Paste");
            this.menSubExpression = new MenuItem("&Sub Expression");
            base.SuspendLayout();
            this.ctxtTextBox.MenuItems.AddRange(new MenuItem[] { this.menBuild, this.menSubExpression, new MenuItem("-"), this.menBuildFuncation, this.menBuildText, this.menBuildList, new MenuItem("-"), this.menCopy, this.menCut, this.menPaste });
            this.menBuild.Click += new EventHandler(this.itemBuild_Click);
            this.menBuildFuncation.Click += new EventHandler(this.menBuildFuncation_Click_1);
            this.menBuildText.MenuItems.AddRange(new MenuItem[] { this.mebBuildPlainText, this.menBuildExpressionText });
            this.mebBuildPlainText.Click += new EventHandler(this.mebBuildPlainText_Click);
            this.menBuildExpressionText.Click += new EventHandler(this.menBuildExpressionText_Click);
            this.menBuildList.Click += new EventHandler(this.InsertList);
            this.menCopy.Click += new EventHandler(this.menCopy_Click);
            this.menCut.Click += new EventHandler(this.menCut_Click);
            this.menPaste.Click += new EventHandler(this.menPaste_Click);
            this.menSubExpression.Click += new EventHandler(this.menSubExpression_Click);
            this.Font = new Font("Batang", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.ctxtTextBox.Popup += new EventHandler(this.ctxtTextBox_Popup);
            this.ContextMenu = this.ctxtTextBox;
        }

        private void InsertList(object sender, EventArgs e)
        {
            this.BuildList(this.GetEmptySymbolic(), false);
        }

        private void itemBuild_Click(object sender, EventArgs e)
        {
            this.Build();
        }

        private void mebBuildPlainText_Click(object sender, EventArgs e)
        {
            this.BuildText(this.GetEmptySymbolic(), false);
        }

        private void menBuildExpressionText_Click(object sender, EventArgs e)
        {
            this.BuildExpression(this.GetEmptySymbolic(), false);
        }

        private void menBuildFuncation_Click_1(object sender, EventArgs e)
        {
            this.BuildFunction(this.GetEmptySymbolic(), false);
        }

        private void menCopy_Click(object sender, EventArgs e)
        {
            base.Copy();
        }

        private void menCut_Click(object sender, EventArgs e)
        {
            base.Cut();
        }

        private void menPaste_Click(object sender, EventArgs e)
        {
            base.Paste();
        }

        private void menSubExpression_Click(object sender, EventArgs e)
        {
            this.Build(true);
        }

        protected override void OnEnter(EventArgs e)
        {
            this.OnSelectionChanged(null);
            base.OnEnter(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Apps)
            {
                this.ShowContextMenu();
            }
            else
            {
                if ((e.KeyCode == Keys.B) && (e.Modifiers == Keys.Control))
                {
                    this.Build();
                }
                base.OnKeyDown(e);
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            if (this.m_FoucsLen != -1)
            {
                this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                base.SelectionFont = this.Font;
                this.EndFormat();
                this.m_FoucsLen = -1;
            }
            base.OnLeave(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point pt = new Point(e.X, e.Y);
                int charIndexFromPosition = this.GetCharIndexFromPosition(pt);
                if (charIndexFromPosition == (this.TextLength - 1))
                {
                    Point positionFromCharIndex = this.GetPositionFromCharIndex(charIndexFromPosition + 1);
                    if (pt.X > positionFromCharIndex.X)
                    {
                        charIndexFromPosition++;
                    }
                }
                base.SelectionStart = charIndexFromPosition;
                this.ShowContextMenu();
            }
            base.OnMouseDown(e);
        }

        protected override void OnSelectionChanged(EventArgs e)
        {
            if (!this.m_Formating)
            {
                Symbolic symbolic = new Symbolic();
                symbolic.m_ISymbolProvider = this.m_Parent.m_ISymbolProvider;
                symbolic.Expression = this.Text;
                SubExpression subExpressionSpan = symbolic.GetSubExpressionSpan(base.SelectionStart);
                if ((subExpressionSpan.Index != this.m_FocusPos) || (subExpressionSpan.Length != this.m_FoucsLen))
                {
                    if (this.m_FoucsLen != -1)
                    {
                        this.StartFormat(this.m_FocusPos, this.m_FoucsLen);
                        base.SelectionFont = this.Font;
                        this.EndFormat();
                    }
                    if (subExpressionSpan.Length != -1)
                    {
                        this.StartFormat(subExpressionSpan.Index, subExpressionSpan.Length);
                        base.SelectionFont = this.FormulaSelectionFont;
                        this.EndFormat();
                    }
                    this.m_FocusPos = subExpressionSpan.Index;
                    this.m_FoucsLen = subExpressionSpan.Length;
                    base.OnSelectionChanged(e);
                }
            }
        }

        protected override void OnTextChanged(EventArgs e)
        {
            if (this.FormulaChanged != null)
            {
                this.FormulaChanged(this, null);
            }
            base.OnTextChanged(e);
        }

        private void ShowContextMenu()
        {
            Point positionFromCharIndex = this.GetPositionFromCharIndex(base.SelectionStart);
            positionFromCharIndex.Y += 20;
            this.ctxtTextBox.Show(this, positionFromCharIndex);
        }

        private void StartFormat(int i, int l)
        {
            this.m_Formating = true;
            this.m_SP = base.SelectionStart;
            this.m_SL = this.SelectionLength;
            base.SelectionStart = i;
            this.SelectionLength = l;
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x203)
            {
                this.DoDoubleClick();
            }
            else if (m.Msg == 0x204)
            {
                this.DoContext();
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        public string Formula
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
                if ((this.m_Parent != null) && this.Focused)
                {
                    this.OnSelectionChanged(null);
                }
            }
        }

        public bool MultiLine
        {
            get
            {
                return (base.ScrollBars == RichTextBoxScrollBars.Both);
            }
            set
            {
                if (value)
                {
                    base.ScrollBars = RichTextBoxScrollBars.Both;
                }
                else
                {
                    base.ScrollBars = RichTextBoxScrollBars.None;
                }
            }
        }
    }
}

