﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Evaluator
{
    partial class FunctionBuilder
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FunctionBuilder));
            this.pnlPars = new System.Windows.Forms.Panel();
            this.lblDocumentation = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lblDesc = new System.Windows.Forms.Label();
            base.SuspendLayout();
            this.pnlPars.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Top;
            this.pnlPars.Location = new System.Drawing.Point(0, 0x38);
            this.pnlPars.Name = "pnlPars";
            this.pnlPars.Size = new System.Drawing.Size(0x250, 240);
            this.pnlPars.TabIndex = 0;
            this.lblDocumentation.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top;
            this.lblDocumentation.Font = new System.Drawing.Font("Arial", 14.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            this.lblDocumentation.Location = new System.Drawing.Point(0, 8);
            this.lblDocumentation.Name = "lblDocumentation";
            this.lblDocumentation.Size = new System.Drawing.Size(0x250, 40);
            this.lblDocumentation.TabIndex = 1;
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(0x1e8, 360);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(0x58, 0x18);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(0x188, 360);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(0x58, 0x18);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "&Ok";
            this.btnOk.Click += new EventHandler(this.btnOk_Click);
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.Location = new System.Drawing.Point(8, 360);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 0x18);
            this.button1.TabIndex = 3;
            this.button1.Text = "&Add Parameter";
            this.button1.Click += new EventHandler(this.button1_Click);
            this.lblDesc.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom;
            this.lblDesc.Location = new System.Drawing.Point(0, 0x130);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(0x250, 0x30);
            this.lblDesc.TabIndex = 1;
            base.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            base.CancelButton = this.btnCancel;
            base.ClientSize = new System.Drawing.Size(0x250, 390);
            base.Controls.Add(this.button1);
            base.Controls.Add(this.btnCancel);
            base.Controls.Add(this.pnlPars);
            base.Controls.Add(this.btnOk);
            base.Controls.Add(this.lblDocumentation);
            base.Controls.Add(this.lblDesc);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            base.Name = "FunctionBuilder";
            base.ShowInTaskbar = false;
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormulaBuilder";
            base.ResumeLayout(false);
        }
        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlPars;
        private System.ComponentModel.Container resources = null;
    }
}
