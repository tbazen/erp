using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.Evaluator
{
    public partial class FunctionBuilder : Form
    {
        private static int ButtonWidth = 30;
        public string Expression;
        private static int LabelHeight = 0x19;
        private static int LabelWidth = 100;


        

        private Label lblDesc;
        private Label lblDocumentation;
        private FunctionDocumentation m_doc;
        private FormulaEditor m_Focus = null;
        private Symbolic m_Parent;
        private int nPar = 0;
        

        public FunctionBuilder()
        {
            this.InitializeComponent();
        }

        private void AddParamter(int i, string Name, string par)
        {
            Label label = new Label();
            label.AutoSize = false;
            label.Text = Name;
            label.Width = LabelWidth;
            label.Height = LabelHeight;
            label.Top = this.nPar * LabelHeight;
            label.Left = 0;
            label.TextAlign = ContentAlignment.MiddleLeft;
            this.pnlPars.Controls.Add(label);
            FormulaEditor editor = new FormulaEditor();
            editor.Width = (this.pnlPars.Width - LabelWidth) - ButtonWidth;
            editor.Height = label.Height;
            editor.Top = this.nPar * LabelHeight;
            editor.Left = LabelWidth;
            editor.Formula = par;
            editor.m_Parent = this.m_Parent;
            editor.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            this.pnlPars.Controls.Add(editor);
            editor.GotFocus += new EventHandler(this.ParFocused);
            this.pnlPars.Controls.Add(editor);
            Button button = new Button();
            button.Width = ButtonWidth;
            button.Height = label.Height;
            button.Top = editor.Top;
            button.Left = editor.Left + editor.Width;
            button.Text = "X";
            button.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            button.Click += new EventHandler(this.RemoveItem);
            this.pnlPars.Controls.Add(button);
            this.nPar++;
            editor.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Expression = this.m_doc.FunctionName + "(";
            for (int i = 0; i < this.nPar; i++)
            {
                if (((FormulaEditor) this.pnlPars.Controls[(3 * i) + 1]).Formula.Trim() == "")
                {
                    break;
                }
                if (i > 0)
                {
                    this.Expression = this.Expression + ",";
                }
                this.Expression = this.Expression + ((FormulaEditor) this.pnlPars.Controls[(3 * i) + 1]).Formula;
            }
            this.Expression = this.Expression + ")";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.AddParamter(this.nPar, this.m_doc.GetParName(this.nPar), "");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.resources != null))
            {
                this.resources.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FormulaChanged(object sender, EventArgs e)
        {
        }

        

        private void ParFocused(object sender, EventArgs e)
        {
            this.m_Focus = (FormulaEditor) sender;
        }

        private void RemoveItem(object sender, EventArgs e)
        {
            int index = this.pnlPars.Controls.IndexOf((Control) sender) - 2;
            this.pnlPars.Controls.RemoveAt(index);
            this.pnlPars.Controls.RemoveAt(index);
            this.pnlPars.Controls.RemoveAt(index);
            int count = this.pnlPars.Controls.Count;
            while (index < count)
            {
                if ((index % 3) == 0)
                {
                    this.pnlPars.Controls[index].Text = this.m_doc.GetParName(index / 3);
                }
                Control control1 = this.pnlPars.Controls[index];
                control1.Top -= LabelHeight;
                index++;
            }
            this.nPar--;
        }

        public DialogResult ShowFunction(IFunction f, FunctionDocumentation doc, string[] Pars, Symbolic parent)
        {
            return this.ShowFunction(f, doc, Pars, parent, null);
        }

        public DialogResult ShowFunction(IFunction f, FunctionDocumentation doc, string[] Pars, Symbolic parent, IWin32Window win)
        {
            this.m_Parent = parent;
            int i = 0;
            this.Text = "Function builder:" + doc.FunctionName;
            this.lblDocumentation.Text = doc.FunctionName;
            int num2 = (doc.NPars > Pars.Length) ? doc.NPars : Pars.Length;
            for (i = 0; i < num2; i++)
            {
                this.AddParamter(i, doc.GetParName(i), (i < Pars.Length) ? Pars[i] : "");
            }
            this.m_doc = doc;
            this.lblDesc.Text = doc.Description;
            if (win != null)
            {
                return base.ShowDialog(win);
            }
            return base.ShowDialog();
        }
    }
}

