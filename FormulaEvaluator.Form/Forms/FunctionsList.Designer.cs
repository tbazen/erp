﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Evaluator
{
    partial class FunctionsList
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FunctionsList));
            this.lstFunctions = new System.Windows.Forms.ListBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cmbCat = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            base.SuspendLayout();
            this.lstFunctions.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Top;
            this.lstFunctions.Location = new System.Drawing.Point(0, 0x20);
            this.lstFunctions.Name = "lstFunctions";
            this.lstFunctions.Size = new System.Drawing.Size(560, 0x108);
            this.lstFunctions.Sorted = true;
            this.lstFunctions.TabIndex = 0;
            this.lstFunctions.DoubleClick += new EventHandler(this.lstFunctions_DoubleClick);
            this.lstFunctions.SelectedIndexChanged += new EventHandler(this.lstFunctions_SelectedIndexChanged);
            this.lblDescription.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom;
            this.lblDescription.Location = new System.Drawing.Point(0, 0x138);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(560, 40);
            this.lblDescription.TabIndex = 3;
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(0x178, 360);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 0x18);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new EventHandler(this.btnOk_Click);
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(0x1d0, 360);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 0x18);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.cmbCat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCat.Location = new System.Drawing.Point(0x90, 0);
            this.cmbCat.Name = "cmbCat";
            this.cmbCat.Size = new System.Drawing.Size(0x1a0, 0x15);
            this.cmbCat.TabIndex = 4;
            this.cmbCat.SelectedIndexChanged += new EventHandler(this.cmbCat_SelectedIndexChanged);
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0x80, 0x18);
            this.label1.TabIndex = 5;
            this.label1.Text = "Category";
            base.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            base.CancelButton = this.btnCancel;
            base.ClientSize = new System.Drawing.Size(560, 390);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.cmbCat);
            base.Controls.Add(this.btnOk);
            base.Controls.Add(this.lblDescription);
            base.Controls.Add(this.lstFunctions);
            base.Controls.Add(this.btnCancel);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            base.Name = "FunctionsList";
            base.ShowInTaskbar = false;
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FunctionsList";
            base.Load += new EventHandler(this.FunctionsList_Load);
            base.ResumeLayout(false);
        }
        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ComboBox cmbCat;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.ListBox lstFunctions;

    }
}
