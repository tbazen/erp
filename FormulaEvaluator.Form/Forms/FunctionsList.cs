using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.Evaluator
{

    public partial class FunctionsList : Form
    {
        private static string Category = "ALL";
        public string Expression;

        
        private FunctionDocumentation[] m_Docs;
        public Symbolic m_Parent;

        public FunctionsList()
        {
            this.InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.lstFunctions.SelectedIndex != -1)
            {
                FunctionDocumentation selectedDoc = this.SelectedDoc;
                FunctionBuilder builder = new FunctionBuilder();
                base.Close();
                base.DialogResult = builder.ShowFunction(this.m_Parent.m_ISymbolProvider.GetFunction(selectedDoc.FunctionName), selectedDoc, new string[0], this.m_Parent);
                this.Expression = builder.Expression;
            }
        }

        private void cmbCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lstFunctions.Items.Clear();
            foreach (FunctionDocumentation documentation in this.m_Docs)
            {
                if (this.cmbCat.SelectedIndex == 0)
                {
                    this.lstFunctions.Items.Add(documentation.FunctionName);
                }
                else if (this.cmbCat.Text == documentation.Category)
                {
                    this.lstFunctions.Items.Add(documentation.FunctionName);
                }
            }
            Category = this.cmbCat.Text;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FunctionsList_Load(object sender, EventArgs e)
        {
            this.m_Docs = this.m_Parent.m_ISymbolProvider.GetAvialableFunctions();
            ArrayList list = new ArrayList();
            foreach (FunctionDocumentation documentation in this.m_Docs)
            {
                if (!list.Contains(documentation.Category))
                {
                    list.Add(documentation.Category);
                }
            }
            this.cmbCat.Items.Add("ALL");
            foreach (string str in list)
            {
                this.cmbCat.Items.Add(str);
            }
            this.cmbCat.Text = Category;
            this.lstFunctions.SelectedItem = 0;
            this.lstFunctions.Focus();
        }

    

        private void lstFunctions_DoubleClick(object sender, EventArgs e)
        {
            this.btnOk_Click(null, null);
        }

        private void lstFunctions_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.lblDescription.Text = this.SelectedDoc.Description;
        }

        public DialogResult ShowFunctioList(Symbolic s)
        {
            this.m_Parent = s;
            if (s.m_evaluator.ProgramSize == 0)
            {
                return base.ShowDialog();
            }
            ProgramEntry entry = s.m_evaluator.ReturnTopProgramEntry();
            if (!(entry.Entry is IFunction))
            {
                return base.ShowDialog();
            }
            FunctionDocumentation doc = this.m_Parent.m_ISymbolProvider.GetDocumentation((IFunction) entry.Entry);
            FunctionBuilder builder = new FunctionBuilder();
            base.Close();
            string[] subExpressions = s.GetSubExpressions(1);
            base.DialogResult = builder.ShowFunction((IFunction) entry.Entry, doc, subExpressions, s);
            this.Expression = builder.Expression;
            return base.DialogResult;
        }

        private FunctionDocumentation SelectedDoc
        {
            get
            {
                foreach (FunctionDocumentation documentation in this.m_Docs)
                {
                    if (documentation.FunctionName == this.lstFunctions.Text)
                    {
                        return documentation;
                    }
                }
                return null;
            }
        }
    }
}

