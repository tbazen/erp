using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.Evaluator
{
    public partial class ListBuilder : Form
    {

        private static int ButtonWidth = 30;
        
        public string Expression;
        private static int LabelHeight = 0x19;
        private static int LabelWidth = 100;
        private FormulaEditor m_Focus = null;
        private Symbolic m_Parent;
        private int nPar = 0;
        private Panel pnlPars;

        public ListBuilder()
        {
            this.InitializeComponent();
        }
        FormulaEditor focusedTextBox = null;
        private void AddParamter(int i, string Name, string par)
        {
            Label label = new Label();
            label.AutoSize = false;
            label.Text = Name;
            label.Width = LabelWidth;
            label.Height = LabelHeight;
            label.Top = this.nPar * LabelHeight;
            label.Left = 0;
            label.TextAlign = ContentAlignment.MiddleLeft;
            this.pnlPars.Controls.Add(label);
            FormulaEditor editor = new FormulaEditor();
            editor.Width = (this.pnlPars.Width - LabelWidth) - ButtonWidth;
            editor.Height = label.Height;
            editor.Top = this.nPar * LabelHeight;
            editor.Left = LabelWidth;
            editor.Formula = par;
            editor.m_Parent = this.m_Parent;
            editor.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top;
            editor.Enter += new EventHandler(delegate(object s, EventArgs e) {
                focusedTextBox = s as FormulaEditor; 
            });
            this.pnlPars.Controls.Add(editor);
            editor.GotFocus += new EventHandler(this.ParFocused);
            this.pnlPars.Controls.Add(editor);
            Button button = new Button();
            button.Width = ButtonWidth;
            button.Height = label.Height;
            button.Top = editor.Top;
            button.Left = editor.Left + editor.Width;
            button.Text = "X";
            button.Anchor = AnchorStyles.Right | AnchorStyles.Top;
            button.Click += new EventHandler(this.RemoveItem);
            this.pnlPars.Controls.Add(button);
            this.nPar++;
            editor.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Expression = "{";
            for (int i = 0; i < this.nPar; i++)
            {
                if (i > 0)
                {
                    this.Expression = this.Expression + ",";
                }
                this.Expression = this.Expression + ((FormulaEditor) this.pnlPars.Controls[(3 * i) + 1]).Formula;
            }
            this.Expression = this.Expression + "}";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.AddParamter(this.nPar, "Element " + (this.nPar + 1), "");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        

        private void ParFocused(object sender, EventArgs e)
        {
            this.m_Focus = (FormulaEditor) sender;
        }

        private void RemoveItem(object sender, EventArgs e)
        {
            int index = this.pnlPars.Controls.IndexOf((Control) sender) - 2;
            this.pnlPars.Controls.RemoveAt(index);
            this.pnlPars.Controls.RemoveAt(index);
            this.pnlPars.Controls.RemoveAt(index);
            int count = this.pnlPars.Controls.Count;
            while (index < count)
            {
                if ((index % 3) == 0)
                {
                    this.pnlPars.Controls[index].Text = "Element " + ((index / 3) + 1);
                }
                Control control1 = this.pnlPars.Controls[index];
                control1.Top -= LabelHeight;
                index++;
            }
            this.nPar--;
        }

        public DialogResult ShowList(string[] Elements, Symbolic parent)
        {
            this.m_Parent = parent;
            int i = 0;
            for (i = 0; i < Elements.Length; i++)
            {
                this.AddParamter(i, "Element " + (i + 1), Elements[i]);
            }
            return base.ShowDialog();
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            FormulaEditor prevTextBox = null;
            foreach (Control c in pnlPars.Controls)
            {
                FormulaEditor tb = c as FormulaEditor;
                if (tb == null)
                    continue;
                if (tb == focusedTextBox)
                {
                    if (prevTextBox == null)
                        return;
                    string text = tb.Text;
                    tb.Text = prevTextBox.Text;
                    prevTextBox.Text = text;
                    prevTextBox.Focus();
                    prevTextBox.SelectAll();
                    return;
                }
                prevTextBox = tb;
            }
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            FormulaEditor prevTextBox = null;
            foreach (Control c in pnlPars.Controls)
            {
                FormulaEditor tb = c as FormulaEditor;
                if (tb == null)
                    continue;
                if (tb == focusedTextBox)
                {
                    prevTextBox = focusedTextBox;
                    continue;
                }
                if (prevTextBox!=null)
                {
                    string text = tb.Text;
                    tb.Text = prevTextBox.Text;
                    prevTextBox.Text = text;
                    tb.Focus();
                    tb.SelectAll();
                    return;
                }
            }
        }
    }
}

