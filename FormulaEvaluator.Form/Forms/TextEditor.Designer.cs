﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.Evaluator
{
    partial class TextEditor
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextEditor));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtText = new System.Windows.Forms.RichTextBox();
            base.SuspendLayout();
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(0x1b0, 0x108);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 0x18);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new EventHandler(this.btnOk_Click);
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(520, 0x108);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 0x18);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.Location = new System.Drawing.Point(8, 0x108);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 0x18);
            this.button1.TabIndex = 3;
            this.button1.Text = "&Build as Expression";
            this.button1.Click += new EventHandler(this.button1_Click);
            this.txtText.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Top;
            this.txtText.Location = new System.Drawing.Point(0, 0);
            this.txtText.Name = "txtText";
            this.txtText.Size = new System.Drawing.Size(0x268, 0x100);
            this.txtText.TabIndex = 0;
            this.txtText.Text = "";
            base.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            base.CancelButton = this.btnCancel;
            base.ClientSize = new System.Drawing.Size(0x268, 0x126);
            base.Controls.Add(this.txtText);
            base.Controls.Add(this.button1);
            base.Controls.Add(this.btnOk);
            base.Controls.Add(this.btnCancel);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            base.Location = new System.Drawing.Point(300, 2);
            base.Name = "TextEditor";
            base.ShowInTaskbar = false;
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Text";
            base.ResumeLayout(false);
        }
        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button1;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.RichTextBox txtText;
    }
}
