using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.Evaluator
{
    public partial class TextEditor : Form
    {


        public string BuiltText;
        public Symbolic m_Parent;

        public TextEditor()
        {
            this.InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.BuiltText = TextBuilder.BuildText(this.txtText.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            base.Close();
            ExpressionTextBuilder builder = new ExpressionTextBuilder();
            base.DialogResult = builder.ShowExpression(this.txtText.Text, this.m_Parent);
            if (base.DialogResult == DialogResult.OK)
            {
                this.BuiltText = TextBuilder.BuildText(builder.Expression);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        

        public DialogResult ShowText(Symbolic parent, string txt)
        {
            this.m_Parent = parent;
            this.txtText.Text = TextBuilder.UnBuildText(txt);
            return base.ShowDialog();
        }
    }
}

