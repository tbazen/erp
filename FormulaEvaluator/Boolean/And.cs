using System;

namespace INTAPS.Evaluator
{

    internal class And : IInfixFunction, IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Bool)
                {
                    FSError error = new FSError("Parameter should be boolean");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Bool;
            data2.Value = !((bool) Pars[0].Value) ? ((object) (false)) : ((object) ((bool) Pars[1].Value));
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Evaluates the conjugation of two logical values", new string[] { "value1", "value2" }, new string[] { "Logical Value 1", "Logical Value 2" }, new string[] { "Mand", "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "And";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "And";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

