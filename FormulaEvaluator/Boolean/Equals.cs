using System;

namespace INTAPS.Evaluator
{

    public class Equals : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (!(data.Value is IComparable) && (data.Type != DataType.Float))
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Bool;
            data2.Value = Pars[0].ToString() == Pars[1].ToString();
            return data2;
        }

        public string Name
        {
            get
            {
                return "Equals";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 5;
            }
        }

        public string Symbol
        {
            get
            {
                return "=";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

