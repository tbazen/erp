using System;
namespace INTAPS.Evaluator
{

    public class LessOrEqualTo : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (!(data.Value is IComparable) && (data.Type != DataType.Float))
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Bool;
            if (Pars[0].Type == DataType.Float)
            {
                data2.Value = ((double) Pars[0].Value) <= ((double) Pars[1].Value);
            }
            else
            {
                data2.Value = ((IComparable) Pars[0].Value).CompareTo(Pars[1].Value) <= 0;
            }
            return data2;
        }

        public string Name
        {
            get
            {
                return "Less or equal";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 5;
            }
        }

        public string Symbol
        {
            get
            {
                return "<=";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

