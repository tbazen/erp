using System;
namespace INTAPS.Evaluator
{

    internal class Not : IInfixFunction, IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Bool)
                {
                    FSError error = new FSError("Parameter should be boolean");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Bool;
            data2.Value = !((bool) Pars[0].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Negate logical value", new string[] { "value" }, new string[] { "Logical value" }, new string[] { "Mand" }, "Logic function");
            }
        }

        public string Name
        {
            get
            {
                return "Not";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public int Precidence
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "Not";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

