using System;
namespace INTAPS.Evaluator
{

    internal class Or : IInfixFunction, IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Bool)
                {
                    FSError error = new FSError("Parameter should be boolean");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Bool;
            data2.Value = ((bool) Pars[0].Value) ? ((object) (true)) : ((object) ((bool) Pars[1].Value));
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Evaluates the disjugation of two logical values", new string[] { "value", "value" }, new string[] { "logical value", "logical value" }, new string[] { "Mand", "Mand" }, "Logic function");
            }
        }

        public string Name
        {
            get
            {
                return "Or";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "Or";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

