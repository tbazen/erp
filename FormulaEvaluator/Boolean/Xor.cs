using System;
namespace INTAPS.Evaluator
{

    internal class Xor : IInfixFunction, IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Bool)
                {
                    FSError error = new FSError("Parameter should be boolean");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Bool;
            bool flag = (bool) Pars[0].Value;
            bool flag2 = (bool) Pars[1].Value;
            data2.Value = (flag && !flag2) ? ((object) 1) : (flag ? ((object) 0) : ((object) flag2));
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Evaluates the exclusiveOr of two logical values", new string[] { "value", "value" }, new string[] { "logical value", "logical value" }, new string[] { "Mand", "Mand" }, "Logic function");
            }
        }

        public string Name
        {
            get
            {
                return "Xor";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "Xor";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

