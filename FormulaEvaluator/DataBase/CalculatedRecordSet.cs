using System;
using System.Collections;
using System.Reflection;
namespace INTAPS.Evaluator
{
    internal class CalculatedRecordSet : IRecordset
    {
        private ArrayList m_Columns;
        private Symbolic m_exp;
        private IRecordset m_Internal;
        private ISymbolProvider m_symb;

        internal CalculatedRecordSet(IRecordset rs, Symbolic exp, ISymbolProvider symb)
        {
            this.m_Internal = rs;
            this.m_exp = exp;
            this.m_symb = symb;
            this.m_Columns = new ArrayList();
            int colCount = rs.ColCount;
            for (int i = 0; i < colCount; i++)
            {
                this.m_Columns.Add(rs.ColName(i).ToUpper());
            }
        }

        public void Close()
        {
            this.m_Internal.Close();
        }

        public string ColName(int ColIndex)
        {
            if (ColIndex == this.m_Internal.ColCount)
            {
                return "Calculated";
            }
            return this.m_Internal.ColName(ColIndex);
        }

        public bool Read()
        {
            return this.m_Internal.Read();
        }

        public bool Requery()
        {
            return this.m_Internal.Requery();
        }

        public int ColCount
        {
            get
            {
                return (this.m_Internal.ColCount + 1);
            }
        }

        public int CurrentRow
        {
            get
            {
                return this.m_Internal.CurrentRow;
            }
            set
            {
                this.m_Internal.CurrentRow = value;
            }
        }

        public bool IsClosed
        {
            get
            {
                return this.m_Internal.IsClosed;
            }
        }

        public EData this[string colname]
        {
            get
            {
                int colIndex = 0;
                for (colIndex = 0; colIndex < this.ColCount; colIndex++)
                {
                    if (this.m_Internal.ColName(colIndex).ToUpper() == colname.ToUpper())
                    {
                        return this[colIndex];
                    }
                }
                return EData.Empty;
            }
        }

        public EData this[int ColIndex]
        {
            get
            {
                if (ColIndex == this.m_Internal.ColCount)
                {
                    int varCount = this.m_exp.VarCount;
                    for (int i = 0; i < varCount; i++)
                    {
                        string simpleVarName = this.m_exp.GetSimpleVarName(i);
                        if (this.m_Columns.Contains(simpleVarName.ToUpper()))
                        {
                            this.m_exp[i] = this.m_Internal[this.m_Columns.IndexOf(simpleVarName.ToUpper())];
                        }
                        else
                        {
                            this.m_exp[i] = this.m_symb.GetData(simpleVarName);
                        }
                    }
                    return this.m_exp.Evaluate();
                }
                return this.m_Internal[ColIndex];
            }
        }

        public int RowCount
        {
            get
            {
                return this.m_Internal.RowCount;
            }
        }
    }
}

