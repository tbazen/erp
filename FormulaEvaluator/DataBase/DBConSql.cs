using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
namespace INTAPS.Evaluator
{
    [Serializable]
    public class DBConSql : IDBConnection
    {
        [NonSerialized]
        internal SqlConnection m_con;
        internal string m_conStr;
        internal string m_Database;
        internal string m_Password;
        private int m_Recon;
        internal string m_Server;
        internal string m_UserName;
        static void assertNODMLSQL(string sql)
        {
            if (sql.IndexOf(" DELETE ", StringComparison.CurrentCultureIgnoreCase) != -1
                || sql.IndexOf(" INSERT ", StringComparison.CurrentCultureIgnoreCase) != -1
                || sql.IndexOf(" UPDATE ", StringComparison.CurrentCultureIgnoreCase) != -1
                || sql.IndexOf(" DROP ", StringComparison.CurrentCultureIgnoreCase) != -1
                )
                throw new System.InvalidOperationException("It is not allowed to run DML query through the formula evaluator");
        }
        internal DBConSql()
        {
            this.m_Recon = 0;
            this.m_con = new SqlConnection();
        }

        public DBConSql(string constr)
        {
            this.m_Recon = 0;
            this.m_Server = "";
            this.m_UserName = "";
            this.m_Password = "";
            this.m_Database = "";
            this.m_conStr = constr;
            this.InitCon();
        }

        public DBConSql(string server, string db, string user, string pwd)
        {
            this.m_Recon = 0;
            this.m_Server = server;
            this.m_UserName = user;
            this.m_Password = pwd;
            this.m_Database = db;
            this.m_conStr = BuildConnStr(this.m_Server, this.m_Database, this.m_UserName, this.m_Password);
            this.InitCon();
        }

        public static string BuildConnStr(string server, string database, string username, string password)
        {
            return ("data source=" + server + ";initial catalog=" + database + ";persist security info=False;user id=" + username + ";password=" + password + ";workstation id=" + server + ";packet size=4096");
        }

        internal void CheckCon()
        {
            if (this.m_con.State != ConnectionState.Open)
            {
                this.m_con.Open();
                this.m_Recon++;
            }
        }

        private void Connect()
        {
            try
            {
                this.m_con.Open();
            }
            catch
            {
            }
        }

        public IDisconnectedRecordSet GetDisconnectedRecordSet(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            return new SqlDisconnectRecordset(SqlCmd, this);
        }

        public ListData GetList1D(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            lock (this.m_con)
            {
                this.CheckCon();
                ArrayList list = new ArrayList();
                this.ShowSQL(SqlCmd);
                var cmd = new SqlCommand(SqlCmd, this.m_con);
                cmd.CommandTimeout = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        object sql = reader[0];
                        list.Add(SqlToEData(sql));
                    }
                }
                ListData data = new ListData(list.Count);
                for (int i = 0; i < list.Count; i++)
                {
                    data.elements[i] = (EData)list[i];
                }
                return data;
            }
        }

        public ListData GetList2D(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            lock (this.m_con)
            {
                int num;
                this.CheckCon();
                ArrayList list = new ArrayList();
                this.ShowSQL(SqlCmd);
                var cmd = new SqlCommand(SqlCmd, this.m_con);
                cmd.CommandTimeout = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    int fieldCount = 0;
                    while (reader.Read())
                    {
                        ListData data = new ListData(reader.FieldCount);
                        fieldCount = reader.FieldCount;
                        num = 0;
                        while (num < fieldCount)
                        {
                            object sql = reader[num];
                            data[num] = SqlToEData(sql);
                            num++;
                        }
                        list.Add(data);
                    }
                }

                ListData data2 = new ListData(list.Count);
                for (num = 0; num < list.Count; num++)
                {
                    EData data3 = new EData();
                    data3.Type = DataType.ListData;
                    data3.Value = list[num];
                    data2[num] = data3;
                }
                return data2;
            }
        }

        public IRecordset GetRecordset(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            lock (this.m_con)
            {
                this.CheckCon();
                string sql = "SELECT COUNT(*) AS COLEX FROM (";
                sql = sql + SqlCmd + ")DERIVEDTBL";
                this.ShowSQL(sql);
                var cmd = new SqlCommand(SqlCmd, this.m_con);
                cmd.CommandTimeout = 0;
                int rowCount = (int)cmd.ExecuteScalar();
                SqlRecordSet set = new SqlRecordSet(SqlCmd, this, rowCount);
                set.Close();
                return set;
            }
        }

        public EData GetSingleValue(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            lock (this.m_con)
            {
                this.CheckCon();
                this.ShowSQL(SqlCmd);
                var cmd = new SqlCommand(SqlCmd, this.m_con);
                cmd.CommandTimeout = 0;
                return SqlToEData(cmd.ExecuteScalar());
            }
        }

        public Vector1D GetVector1D(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            lock (this.m_con)
            {
                this.CheckCon();
                ArrayList list = new ArrayList();
                var cmd = new SqlCommand(SqlCmd, this.m_con);
                cmd.CommandTimeout = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        double num;
                        object obj2 = reader[0];
                        if (obj2.GetType() == typeof(int))
                        {
                            num = (int)obj2;
                        }
                        else if (obj2.GetType() == typeof(float))
                        {
                            num = (float)obj2;
                        }
                        else if (obj2.GetType() == typeof(double))
                        {
                            num = (double)obj2;
                        }
                        else if (obj2.GetType() == typeof(decimal))
                        {
                            num = (double)((decimal)obj2);
                        }
                        else if (obj2.GetType() == typeof(string))
                        {
                            num = double.Parse((string)obj2);
                        }
                        else
                        {
                            num = 0.0;
                        }
                        list.Add(num);
                    }
                }
                Vector1D vectord = new Vector1D(list.Count);
                list.CopyTo(vectord.VectorElements, 0);
                return vectord;
            }
        }

        public Vector2D GetVector2D(string SqlCmd)
        {
            assertNODMLSQL(SqlCmd);
            lock (this.m_con)
            {
                int num2;
                this.CheckCon();
                ArrayList list = new ArrayList();
                this.ShowSQL(SqlCmd);
                int r = 0;
                var cmd = new SqlCommand(SqlCmd, this.m_con);
                cmd.CommandTimeout = 0;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        double[] numArray = new double[reader.FieldCount];
                        r = numArray.Length;
                        num2 = 0;
                        while (num2 < numArray.Length)
                        {
                            double num;
                            object obj2 = reader[num2];
                            if (obj2.GetType() == typeof(int))
                            {
                                num = (int)obj2;
                            }
                            else if (obj2.GetType() == typeof(float))
                            {
                                num = (float)obj2;
                            }
                            else if (obj2.GetType() == typeof(double))
                            {
                                num = (double)obj2;
                            }
                            else if (obj2.GetType() == typeof(decimal))
                            {
                                num = (double)((decimal)obj2);
                            }
                            else if (obj2.GetType() == typeof(string))
                            {
                                try
                                {
                                    num = double.Parse((string)obj2);
                                }
                                catch
                                {
                                    num = 0.0;
                                }
                            }
                            else
                            {
                                num = 0.0;
                            }
                            numArray[num2] = num;
                            num2++;
                        }
                        list.Add(numArray);
                    }
                }
                Vector2D vectord = new Vector2D(r, list.Count);
                for (num2 = 0; num2 < list.Count; num2++)
                {
                    for (int i = 0; i < r; i++)
                    {
                        vectord[i, num2] = ((double[])list[num2])[i];
                    }
                }
                return vectord;
            }
        }

        private void InitCon()
        {
            this.m_con = new SqlConnection(this.m_conStr);
        }

        public static void Initialize()
        {
            EData.RegisterDataType(new DBConSqlType());
        }

        public void ReConnect()
        {
            try
            {
                this.InitCon();
                this.m_con.Open();
            }
            catch
            {
            }
        }

        public string ShowConnectionUI()
        {
            return null;
        }

        private void ShowSQL(string sql)
        {
        }

        public static EData SqlToEData(object sql)
        {

            double doubleVal = 0;
            int intVal = 0;
            long longVal = 0;
            if (sql != DBNull.Value)
            {
                if (sql == null)
                {
                    return EData.Empty;
                }
                if (sql.GetType() == typeof(int))
                {
                    intVal = (int)sql;
                    goto ret_int;
                }

                if (sql.GetType() == typeof(short))
                {
                    intVal = (short)sql;
                    goto ret_int;
                }
                if (sql.GetType() == typeof(long))
                {
                    longVal = (long)sql;
                    goto ret_long;
                }
                if (sql.GetType() == typeof(float))
                {
                    doubleVal = (float)sql;
                    goto ret_double;
                }
                if (sql.GetType() == typeof(double))
                {
                    doubleVal = (double)sql;
                    goto ret_double;
                }
                if (sql.GetType() == typeof(decimal))
                {
                    doubleVal = (double)((decimal)sql);
                    goto ret_double;
                }
                if (sql.GetType() == typeof(string))
                {
                    return new EData(DataType.Text, (string)sql);
                }
                if (sql.GetType() == typeof(DateTime))
                {
                    return new EData(DataType.DateTime, (DateTime)sql);
                }
            }
            return EData.Empty;
        ret_int:
            return new EData(DataType.Int, intVal);
        ret_long:
            return new EData(DataType.LongInt, longVal);
        ret_double:
            return new EData(DataType.Float, doubleVal);
        }

        public override string ToString()
        {
            return this.m_con.ConnectionString;
        }

        public string Database
        {
            get
            {
                return this.m_Database;
            }
            set
            {
                this.m_Database = value;
                this.InitCon();
            }
        }

        public string Password
        {
            get
            {
                return this.m_Password;
            }
            set
            {
                this.m_Password = value;
                this.InitCon();
            }
        }

        public string Server
        {
            get
            {
                return this.m_Server;
            }
            set
            {
                this.m_Server = value;
                this.InitCon();
            }
        }

        public string UserName
        {
            get
            {
                return this.m_UserName;
            }
            set
            {
                this.m_UserName = value;
                this.InitCon();
            }
        }
    }
}

