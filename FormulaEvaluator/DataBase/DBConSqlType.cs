using System;
namespace INTAPS.Evaluator
{
    public class DBConSqlType : IEDataType
    {
        public object CreateObject()
        {
            return new DBConSql();
        }

        public string Name
        {
            get
            {
                return "Sql Server Connection";
            }
        }

        public DataType Type
        {
            get
            {
                return DataType.DBCon;
            }
        }
    }
}

