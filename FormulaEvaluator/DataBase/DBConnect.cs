using System;
namespace INTAPS.Evaluator
{

    internal class DBConnect : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            string str = Pars[0].Value as string;
            bool flag = (bool) Pars[1].Value;
            string str2 = Pars[2].Value as string;
            string str3 = Pars[3].Value as string;
            string str4 = Pars[4].Value as string;
            string format = "Data Source={0};Integrated Security={1};User ID={2};Password={3};Initial Catalog={4}";
            return new EData(DataType.DBCon, new DBConSql(string.Format(format, new object[] { str, flag.ToString(), str2, str3, str4 })));
        }

        public string Name
        {
            get
            {
                return "Connect to SQL";
            }
        }

        public int ParCount
        {
            get
            {
                return 5;
            }
        }

        public string Symbol
        {
            get
            {
                return "DBConnect";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

