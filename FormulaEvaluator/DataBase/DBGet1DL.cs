using System;
namespace INTAPS.Evaluator
{
    internal class DBGet1DL : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data2;
            FSError error;
            if (Pars.Length > 2)
            {
                error = new FSError(2, "Too many arguments.");
                return error.ToEData();
            }
            if (Pars.Length < 2)
            {
                error = new FSError(2, "Too few arguments.");
                return error.ToEData();
            }
            if (Pars[0].Type != DataType.DBCon)
            {
                error = new FSError(2, "First Argument should be database connection.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Text)
            {
                error = new FSError(2, "Second Argument should be text.");
                return error.ToEData();
            }
            string sqlCmd = (string) Pars[1].Value;
            ListData data = ((IDBConnection) Pars[0].Value).GetList1D(sqlCmd);
            data2.Type = DataType.ListData;
            data2.Value = data;
            return data2;
        }

        public string GetParName(int i)
        {
            switch (i)
            {
                case 0:
                    return "Sql connection";

                case 1:
                    return "Sql statment";
            }
            return null;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Gets a one dimentional list from a database", new string[] { "Connection", "Sql statment" }, new string[] { "An SQL Connection variable", "A text that is used as a query to get the list." }, new string[] { "Mand", "Mand" }, "Database");
            }
        }

        public string Name
        {
            get
            {
                return "DB1DL";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "DB1DL";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

