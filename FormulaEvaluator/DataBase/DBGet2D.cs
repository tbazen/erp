using System;
namespace INTAPS.Evaluator
{
    internal class DBGet2D : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data;
            FSError error;
            if (Pars.Length > 2)
            {
                error = new FSError(2, "Too many arguments.");
                return error.ToEData();
            }
            if (Pars.Length < 2)
            {
                error = new FSError(2, "Too few arguments.");
                return error.ToEData();
            }
            if (Pars[0].Type != DataType.DBCon)
            {
                error = new FSError(2, "First Argument should be database connection.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Text)
            {
                error = new FSError(2, "Second Argument should be text.");
                return error.ToEData();
            }
            string sqlCmd = (string) Pars[1].Value;
            Vector2D vectord = ((IDBConnection) Pars[0].Value).GetVector2D(sqlCmd);
            data.Type = DataType.Vector2D;
            data.Value = vectord;
            return data;
        }

        public string GetParName(int i)
        {
            switch (i)
            {
                case 0:
                    return "Sql connection";

                case 1:
                    return "Sql statment";
            }
            return null;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Gets a two dimentional vector from a database", new string[] { "Connection", "Sql statment" }, new string[] { "An SQL Connection variable", "A text that is used as a query to get the data." }, new string[] { "Mand", "Mand" }, "Database");
            }
        }

        public string Name
        {
            get
            {
                return "DB2D";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "DB2D";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

