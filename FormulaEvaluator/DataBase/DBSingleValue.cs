using System;
namespace INTAPS.Evaluator
{
    internal class DBSingleValue : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars.Length > 2)
            {
                error = new FSError(2, "Too many arguments.");
                return error.ToEData();
            }
            if (Pars.Length < 2)
            {
                error = new FSError(2, "Too few arguments.");
                return error.ToEData();
            }
            if (Pars[0].Type != DataType.DBCon)
            {
                error = new FSError(2, "First argument should be database connection.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Text)
            {
                error = new FSError(2, "Second Argument should be text.");
                return error.ToEData();
            }
            string sqlCmd = (string) Pars[1].Value;
            return ((IDBConnection) Pars[0].Value).GetSingleValue(sqlCmd);
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Gets a single value from a database", new string[] { "Connection", "Statement" }, new string[] { "Sql database connection", "sql statement" }, new string[] { "Mand", "Mand" }, "Data Retrieving function");
            }
        }

        public string Name
        {
            get
            {
                return "DBSV";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "DBSV";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

