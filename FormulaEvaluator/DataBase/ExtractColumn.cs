using System;
namespace INTAPS.Evaluator
{
    internal class ExtractColumn : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.RecordSet)
            {
                error = new FSError("First parameter should be a recordset");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Float)
            {
                error = new FSError("Second parameter should be a number designating an column.");
                return error.ToEData();
            }
            IRecordset recordset = (IRecordset) Pars[0].Value;
            int num = (int) ((double) Pars[1].Value);
            if ((num < 1) || (num > recordset.ColCount))
            {
                error = new FSError("Invalid column index");
                return error.ToEData();
            }
            recordset.Requery();
            ListData data = new ListData(recordset.RowCount);
            int num2 = 0;
            while (recordset.Read())
            {
                data[num2++] = recordset[num - 1];
            }
            EData data2 = new EData();
            data2.Type = DataType.ListData;
            data2.Value = data;
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Extracts a single column from a recordset", new string[] { "Recordset", "Index" }, new string[] { "a record set", "one based index of the column that will be extracted" }, new string[] { "Mand", "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "Extract column";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "ExtrCol";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

