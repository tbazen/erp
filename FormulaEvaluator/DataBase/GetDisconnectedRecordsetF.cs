using System;
namespace INTAPS.Evaluator
{
    internal class GetDisconnectedRecordsetF : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data;
            FSError error;
            if (Pars.Length > 2)
            {
                error = new FSError(2, "Too many arguments.");
                return error.ToEData();
            }
            if (Pars.Length < 2)
            {
                error = new FSError(2, "Too few arguments.");
                return error.ToEData();
            }
            if (Pars[0].Type != DataType.DBCon)
            {
                error = new FSError(2, "First argument should be database connection.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Text)
            {
                error = new FSError(2, "Second Argument should be text.");
                return error.ToEData();
            }
            string sqlCommand = (string) Pars[1].Value;
            data.Type = DataType.D_RecordSet;
            data.Value = ((IDBConnection) Pars[0].Value).GetDisconnectedRecordSet(sqlCommand);
            return data;
        }

        public string Name
        {
            get
            {
                return "GetDisconnectedRecordset";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "DBDRS";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

