using System;
namespace INTAPS.Evaluator
{
    internal class GetRecordSetF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data;
            FSError error;
            if (Pars.Length > 2)
            {
                error = new FSError(2, "Too many arguments.");
                return error.ToEData();
            }
            if (Pars.Length < 2)
            {
                error = new FSError(2, "Too few arguments.");
                return error.ToEData();
            }
            if (Pars[0].Type != DataType.DBCon)
            {
                error = new FSError(2, "First argument should be database connection.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Text)
            {
                error = new FSError(2, "Second Argument should be text.");
                return error.ToEData();
            }
            string sqlCmd = (string) Pars[1].Value;
            data.Type = DataType.RecordSet;
            data.Value = ((IDBConnection) Pars[0].Value).GetRecordset(sqlCmd);
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Gets recordset from a database", new string[] { "Connection", "Statement" }, new string[] { "sql database connection", "sql statement" }, new string[] { "Mand", "Mand" }, "Data Retrieving function");
            }
        }

        public string Name
        {
            get
            {
                return "GetRecordSet";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "DBRS";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

