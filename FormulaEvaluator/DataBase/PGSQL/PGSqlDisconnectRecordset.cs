using Npgsql;
using System;
using System.Data;
namespace INTAPS.Evaluator
{

    internal class PGSqlDisconnectRecordset : IDisconnectedRecordSet
    {
        private DBConPGSql m_con;
        private DataSet m_DataSet;
        private string m_sql;
        private string m_sqlCount;
        private NpgsqlDataAdapter m_NpgsqlDataAdapter;
        private int m_TotalNumberofRows;
        //NpgsqlCommand
        //NpgsqlDataReader
        public PGSqlDisconnectRecordset(string sql, DBConPGSql con)
        {
            this.m_sql = sql;
            this.m_con = con;
            this.m_DataSet = new DataSet();
            this.m_sqlCount = "SELECT COUNT(*) AS COLEX FROM (";
            this.m_sqlCount = this.m_sqlCount + sql;
            this.m_sqlCount = this.m_sqlCount + ")DERIVEDTBL";
            try
            {
                this.m_TotalNumberofRows = int.Parse(new NpgsqlCommand(this.m_sqlCount, this.m_con.m_con).ExecuteScalar().ToString());
            }
            catch
            {
                this.m_sql = "";
                this.m_TotalNumberofRows = 0;
            }
            this.m_NpgsqlDataAdapter = new NpgsqlDataAdapter(this.m_sql, this.m_con.m_con);
        }

        public string ColumnName(int ColIndex)
        {
            if (this.m_DataSet.Tables.Contains("TableOne"))
            {
                return this.m_DataSet.Tables["TableOne"].Columns[ColIndex].Caption;
            }
            return "";
        }

        public int Fill(int startindex, int maxrecords)
        {
            try
            {
                this.m_DataSet.Reset();
                return this.m_NpgsqlDataAdapter.Fill(this.m_DataSet, startindex, maxrecords, "TableOne");
            }
            catch
            {
                return 0;
            }
        }

        public int ColCount
        {
            get
            {
                if (this.m_DataSet.Tables.Contains("TableOne"))
                {
                    return this.m_DataSet.Tables["TableOne"].Columns.Count;
                }
                return 0;
            }
        }

        EData IDisconnectedRecordSet.this[int RowIndex, int ColIndex]
        {
            get
            {
                if (this.m_DataSet.Tables.Contains("TableOne"))
                {
                    return DBConSql.SqlToEData(this.m_DataSet.Tables["TableOne"].Rows[RowIndex][ColIndex]);
                }
                return EData.Empty;
            }
        }

        public int RowCount
        {
            get
            {
                if (this.m_DataSet.Tables.Contains("TableOne"))
                {
                    return this.m_DataSet.Tables["TableOne"].Rows.Count;
                }
                return 0;
            }
        }

        public int TotalRows
        {
            get
            {
                return this.m_TotalNumberofRows;
            }
        }
    }
}

