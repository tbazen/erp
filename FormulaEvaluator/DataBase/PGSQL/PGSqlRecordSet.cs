using Npgsql;
using System;
using System.Collections;
using System.Data;
using System.Reflection;
namespace INTAPS.Evaluator
{

    internal class PGSqlRecordSet : IRecordset
    {
        private ArrayList Cache = null;
        private string[] ColNames;
        private int m_ColumnCount;
        private DBConPGSql m_con;
        private int m_CurrentRow;
        private int m_RowCount;
        private string m_sql;
        private NpgsqlDataReader reader;

        public PGSqlRecordSet(string sql, DBConPGSql con, int RowCount)
        {
            this.reader = new NpgsqlCommand(sql, con.m_con).ExecuteReader();
            this.m_CurrentRow = -1;
            this.m_sql = sql;
            this.m_con = con;
            this.m_RowCount = RowCount;
            this.m_ColumnCount = this.reader.FieldCount;
            this.Cache = new ArrayList();
            this.ColNames = new string[this.m_ColumnCount];
            for (int i = 0; i < this.m_ColumnCount; i++)
            {
                this.ColNames[i] = this.reader.GetName(i);
            }
            while (this.reader.Read())
            {
                object[] values = new object[this.m_ColumnCount];
                this.reader.GetValues(values);
                this.Cache.Add(values);
            }
            this.reader.Close();
        }

        public void Close()
        {
            if (!this.reader.IsClosed)
            {
                this.reader.Close();
            }
        }

        public string ColName(int ColIndex)
        {
            return this.ColNames[ColIndex];
        }

        public bool Read()
        {
            if (this.Cache != null)
            {
                this.m_CurrentRow++;
                return (this.m_CurrentRow < this.m_RowCount);
            }
            if (this.reader.IsClosed)
            {
                this.Requery();
            }
            bool flag = this.reader.Read();
            if (flag)
            {
                this.m_CurrentRow++;
            }
            else
            {
                this.reader.Close();
            }
            return flag;
        }

        public bool Requery()
        {
            if (this.Cache != null)
            {
                this.m_CurrentRow = -1;
                return true;
            }
            if (!this.reader.IsClosed)
            {
                this.reader.Close();
            }
            this.m_con.CheckCon();
            this.reader = new NpgsqlCommand(this.m_sql, this.m_con.m_con).ExecuteReader();
            this.m_CurrentRow = -1;
            if ((this.m_con.m_con.State == ConnectionState.Closed) || (this.m_con.m_con.State == ConnectionState.Broken))
            {
                return false;
            }
            return true;
        }

        public int ColCount
        {
            get
            {
                return this.m_ColumnCount;
            }
        }

        public int CurrentRow
        {
            get
            {
                return this.m_CurrentRow;
            }
            set
            {
                this.m_CurrentRow = value;
            }
        }

        public bool IsClosed
        {
            get
            {
                return this.reader.IsClosed;
            }
        }

        public EData this[int ColIndex]
        {
            get
            {
                if (this.Cache != null)
                {
                    return DBConSql.SqlToEData(((object[]) this.Cache[this.m_CurrentRow])[ColIndex]);
                }
                if (this.reader.IsClosed)
                {
                    return EData.Empty;
                }
                return DBConSql.SqlToEData(this.reader[ColIndex]);
            }
        }

        public EData this[string colname]
        {
            get
            {
                int num = 0;
                foreach (string str in this.ColNames)
                {
                    if (str.ToUpper() == colname.ToUpper())
                    {
                        return this[num];
                    }
                    num++;
                }
                return EData.Empty;
            }
        }

        public int RowCount
        {
            get
            {
                return this.m_RowCount;
            }
        }
    }
}

