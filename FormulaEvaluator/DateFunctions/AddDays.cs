using System;
namespace INTAPS.Evaluator
{
    public class AddDays : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            DateTime time = (DateTime) Pars[0].Value;
            return new EData(DataType.DateTime, time.AddDays((double) Pars[1].Value));
        }

        public string Name
        {
            get
            {
                return "AddDays";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "AddDays";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

