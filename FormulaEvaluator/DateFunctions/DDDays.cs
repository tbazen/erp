using System;
namespace INTAPS.Evaluator
{
    public class DDDays : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            if ((Pars[0].Type != DataType.DateTime) || (Pars[1].Type != DataType.DateTime))
            {
                FSError error = new FSError("Parameters should be datetime");
                return error.ToEData();
            }
            DateTime time = (DateTime) Pars[0].Value;
            DateTime time2 = (DateTime) Pars[1].Value;
            TimeSpan span = time.Subtract(time2);
            return new EData(DataType.Float, (double) span.Days);
        }

        public string Name
        {
            get
            {
                return "DDDays";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "DDDays";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

