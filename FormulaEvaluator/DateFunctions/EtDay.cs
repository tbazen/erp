using INTAPS.Ethiopic;
using System;
namespace INTAPS.Evaluator
{
    public class EtDay : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.DateTime)
            {
                FSError error = new FSError("Type mismatch - ethiopian day function");
                return error.ToEData();
            }
            DateTime time = (DateTime) Pars[0].Value;
            EtGrDate date = EtGrDate.ToEth(new EtGrDate(time.Day, time.Month, time.Year));
            EData data = new EData();
            data.Type = DataType.Float;
            data.Value = date.Day;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Changes European date to Ethiopian date and returns the day part of the ethiopian date", new string[] { "string" }, new string[] { "text" }, new string[] { "Mand" }, "Date function");
            }
        }

        public string Name
        {
            get
            {
                return "EtDay";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "EtDay";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

