using INTAPS.Ethiopic;
using System;
namespace INTAPS.Evaluator
{
    public class EthiopiaLeapYear : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.Float)
            {
                FSError error = new FSError("Type mismatch - ethiopian leap year function");
                return error.ToEData();
            }
            int y = (int) ((double) Pars[0].Value);
            return new EData(DataType.Bool, EtGrDate.IsLeapYearEt(y));
        }

        public string Name
        {
            get
            {
                return "Ethiopian Leap Year";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "EtLeap";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

