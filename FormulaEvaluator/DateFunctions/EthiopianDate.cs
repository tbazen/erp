using INTAPS.Ethiopic;
using System;
namespace INTAPS.Evaluator
{
    public class EthiopianDate : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.Text)
            {
                FSError error = new FSError("Type mismatch - ethiopian date function");
                return error.ToEData();
            }
            EtGrDate date = EtGrDate.ToGrig(this.Parse(Pars[0].ToString()));
            EData data = new EData();
            data.Type = DataType.DateTime;
            data.Value = date.GridDate;
            return data;
        }

        private EtGrDate Parse(string strDate)
        {
            strDate = strDate + "#";
            int[] numArray = new int[3];
            int num = 0;
            int startIndex = -1;
            for (int i = 0; (num < 3) && (i < strDate.Length); i++)
            {
                if (char.IsDigit(strDate[i]))
                {
                    if (startIndex == -1)
                    {
                        startIndex = i;
                    }
                }
                else if (startIndex != -1)
                {
                    try
                    {
                        numArray[num++] = int.Parse(strDate.Substring(startIndex, i - startIndex));
                        startIndex = -1;
                    }
                    catch
                    {
                        return new EtGrDate(0, 0, 0);
                    }
                }
            }
            if (num != 3)
            {
                return new EtGrDate(0, 0, 0);
            }
            if (numArray[2] < 100)
            {
                numArray[2] += 0x76c;
            }
            return new EtGrDate(numArray[0], numArray[1], numArray[2]);
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Changes Ethiopian date to European date", new string[] { "#string#" }, new string[] { "#text#" }, new string[] { "Mand" }, "Date function");
            }
        }

        public string Name
        {
            get
            {
                return "EtDate";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "EtDate";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

