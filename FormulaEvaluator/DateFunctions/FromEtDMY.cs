using INTAPS.Ethiopic;
using System;
namespace INTAPS.Evaluator
{
    public class FromEtDMY : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.Float)
            {
                error = new FSError("Type mismatch - from ethiopian dmy function");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Float)
            {
                error = new FSError("Type mismatch - from ethiopian dmy function");
                return error.ToEData();
            }
            if (Pars[2].Type != DataType.Float)
            {
                error = new FSError("Type mismatch - from ethiopian dmy function");
                return error.ToEData();
            }
            EData data = new EData();
            data.Type = DataType.DateTime;
            data.Value = EtGrDate.ToGrig(new EtGrDate((int) ((double) Pars[0].Value), (int) ((double) Pars[1].Value), (int) ((double) Pars[2].Value))).GridDate;
            return data;
        }

        public string Name
        {
            get
            {
                return "EtDMY";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "EtDMY";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

