using System;
namespace INTAPS.Evaluator
{
    public class GrigorianLeapYear : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.Float)
            {
                FSError error = new FSError("Type mismatch - gregorian leap year function");
                return error.ToEData();
            }
            int year = (int) ((double) Pars[0].Value);
            return new EData(DataType.Bool, DateTime.IsLeapYear(year));
        }

        public string Name
        {
            get
            {
                return "GrigLeap";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "GrigLeap";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

