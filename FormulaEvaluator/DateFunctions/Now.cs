using System;
namespace INTAPS.Evaluator
{
    public class Now : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            return new EData(DataType.DateTime, DateTime.Now);
        }

        public string Name
        {
            get
            {
                return "Now";
            }
        }

        public int ParCount
        {
            get
            {
                return 0;
            }
        }

        public string Symbol
        {
            get
            {
                return "Now";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

