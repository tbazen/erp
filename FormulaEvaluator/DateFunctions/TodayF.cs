using System;
namespace INTAPS.Evaluator
{
    public class TodayF : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            return new EData(DataType.DateTime, DateTime.Today.Date);
        }

        public string Name
        {
            get
            {
                return "Today";
            }
        }

        public int ParCount
        {
            get
            {
                return 0;
            }
        }

        public string Symbol
        {
            get
            {
                return "Today";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

