using Microsoft.Win32;
using System;
using System.Collections;
using System.IO;
using System.Reflection;
namespace INTAPS.Evaluator
{
    public class CalcGlobal
    {
        public static Hashtable DocumentedFunctions;
        public static Hashtable Functions;
        public static bool Initialized = false;
        public static ArrayList PlugInFunctions;

        public static void AddFunction(IFunction f)
        {
            Functions.Add(f.Symbol.ToUpper(), f);
            if (f is IFunctionDocumentation)
            {
                DocumentedFunctions.Add(f.Symbol.ToUpper(), new FunctionDocumentation((IFunctionDocumentation) f));
            }
            else
            {
                DocumentedFunctions.Add(f.Symbol.ToUpper(), new FunctionDocumentation(f));
            }
        }

        public static void AddFunctionFromAssembly(Assembly a)
        {
            Type[] types = a.GetTypes();
            foreach (Type type in types)
            {
                if (IsIFunction(type))
                {
                    AddFunction((IFunction) a.CreateInstance(type.ToString()));
                }
            }
        }

        public static void CopyPlugInFunctionsToGlobalFunctionsList()
        {
            foreach (IFunction function in PlugInFunctions)
            {
                if (!Functions.ContainsKey(function.Symbol.ToUpper()))
                {
                    AddFunction(function);
                }
            }
        }

        private static bool FilterFunctions(Type t, object o)
        {
            return (t == typeof(IFunction));
        }

        public static void FindPlugins(string Path, ArrayList list)
        {
            Assembly assembly;
            FileInfo info;
            foreach (string str in Directory.GetFiles(Path))
            {
                info = new FileInfo(str);
                if (info.Extension.Equals(".dll"))
                {
                    assembly = Assembly.LoadFrom(str);
                    list.Add(assembly);
                }
            }
            foreach (string str2 in Directory.GetDirectories(Path))
            {
                foreach (string str in Directory.GetFiles(str2))
                {
                    info = new FileInfo(str);
                    if (info.Extension.Equals(".dll"))
                    {
                        assembly = Assembly.LoadFrom(str);
                        list.Add(assembly);
                    }
                }
            }
        }

        public static void Initialize()
        {
            EData.Initialize();
            InitializeFunctions();
            InitializeDataTypes();
            Initialized = true;
        }

        public static void InitializeDataTypes()
        {
            Vector1D.Initialize();
            Vector2D.Initialize();
        }

        public static void InitializeFunctions()
        {
            Functions = new Hashtable();
            DocumentedFunctions = new Hashtable();
            AddFunctionFromAssembly(Assembly.GetCallingAssembly());
        }

        public static void InitializePlugInFunctions()
        {
            PlugInFunctions = new ArrayList();
            ArrayList list = new ArrayList();
        }

        private static bool IsIFunction(Type t)
        {
            if (t.IsInterface)
            {
                return false;
            }
            if (t == typeof(ListData))
            {
                return false;
            }
            if (t == typeof(Vector1D))
            {
                return false;
            }
            if (t == typeof(Vector2D))
            {
                return false;
            }
            return (t.FindInterfaces(new TypeFilter(CalcGlobal.FilterFunctions), null).Length > 0);
        }
    }
}

