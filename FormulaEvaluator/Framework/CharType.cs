using System;

namespace INTAPS.Evaluator
{

    internal enum CharType
    {
        Digit,
        Dot,
        Comma,
        Alpha,
        Brace,
        SqrBrace,
        CurBrace,
        E,
        Minus,
        Op,
        Invalid,
        HashChar
    }
}

