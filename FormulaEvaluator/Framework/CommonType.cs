using System;

namespace INTAPS.Evaluator
{

    public class CommonType : IEDataType
    {
        private DataType m_Type;
        private byte[] SerialData;

        public CommonType(DataType t)
        {
            this.m_Type = t;
        }

        public object CreateObject()
        {
            switch (this.m_Type)
            {
                case DataType.Text:
                    return "";

                case DataType.Float:
                    return 0.0;

                case DataType.DateTime:
                    return new DateTime(0x76c, 1, 1);
            }
            return null;
        }

        public int GetByteCount()
        {
            return 0;
        }

        public int SerializeToBin(byte[] data, int index, int ofs)
        {
            int num = index + ofs;
            while (num < data.Length)
            {
                if ((num - index) >= this.SerialData.Length)
                {
                    break;
                }
                data[num] = this.SerialData[num - index];
                num++;
            }
            return (num - (index + ofs));
        }

        public void StartDeSerializeFromBin()
        {
            this.CreateObject();
            switch (this.m_Type)
            {
                case DataType.Text:
                    this.SerialData = new byte[4];
                    break;

                case DataType.Float:
                    this.SerialData = new byte[8];
                    break;

                case DataType.DateTime:
                    this.SerialData = new byte[4];
                    break;
            }
        }

        public string Name
        {
            get
            {
                switch (this.m_Type)
                {
                    case DataType.Text:
                        return "Text";

                    case DataType.Float:
                        return "Number";

                    case DataType.DateTime:
                        return "DateTime";
                }
                return this.m_Type.ToString();
            }
        }

        public DataType Type
        {
            get
            {
                return this.m_Type;
            }
        }
    }
}

