using System;
namespace INTAPS.Evaluator
{

    public enum DataType
    {
        Text = 0,
        Int = 1,
        Float = 2,
        Bool = 3,
        DateTime = 4,
        LongInt=5,
        AtomicMax = 0xff,
        Vector1D = 0x100,
        Vector2D = 0x101,
        Set = 0x102,
        ListData = 0x103,
        Expression = 0x104,
        ByteArray = 0x105,
        CompositeMax = 0x1ff,
        RecordSet = 0x200,
        DBCon = 0x201,
        D_RecordSet = 0x202,
        MapLayer = 0x300,
        GISConnection = 0x301,
        Object = 0x400,
        GISShapeSet = 0x500,
        Variant = 0xf000,
        Empty = 0xf001,
        Error = 0xf002,
        Calculating = 0xf003,
    }
}

