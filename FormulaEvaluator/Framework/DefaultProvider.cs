using System;
using System.Collections;
namespace INTAPS.Evaluator
{

    public class DefaultProvider : ISymbolProvider
    {
        internal Hashtable m_Variables = new Hashtable();

        public FunctionDocumentation[] GetAvialableFunctions()
        {
            FunctionDocumentation[] documentationArray = new FunctionDocumentation[CalcGlobal.Functions.Count];
            int index = 0;
            foreach (DictionaryEntry entry in CalcGlobal.Functions)
            {
                IFunction function = (IFunction) entry.Value;
                if (function is IFunctionDocumentation)
                {
                    documentationArray[index] = new FunctionDocumentation((IFunctionDocumentation) function);
                }
                else
                {
                    string[] pars = new string[function.ParCount];
                    string[] pardesc = new string[function.ParCount];
                    string[] types = new string[function.ParCount];
                    for (int i = 0; i < function.ParCount; i++)
                    {
                        pars[i] = "Parameter " + i;
                        pardesc[i] = "(Undocumented)";
                        types[i] = "Mand";
                    }
                    documentationArray[index] = new FunctionDocumentation(FunctionDocumentation.CreateDocumentation(function.Symbol, function.Symbol + "(Undocumented)", pars, pardesc, types));
                }
                index++;
            }
            return documentationArray;
        }

        public EData GetData(URLIden iden)
        {
            return this.GetData(iden.Element);
        }

        public EData GetData(string symbol)
        {
            if (!this.m_Variables.Contains(symbol.ToUpper()))
            {
                throw new Exception("Variable " + symbol + " doesn't exist");
            }
            return (EData) this.m_Variables[symbol.ToUpper()];
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            if (f is IFunctionDocumentation)
            {
                return new FunctionDocumentation((IFunctionDocumentation) f);
            }
            return new FunctionDocumentation(f);
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public IFunction GetFunction(URLIden iden)
        {
            return this.GetFunction(iden.Element);
        }

        public IFunction GetFunction(string symbol)
        {
            return (IFunction) CalcGlobal.Functions[symbol.ToUpper()];
        }

        public bool SymbolDefined(string Name)
        {
            return this.m_Variables.Contains(Name.ToUpper());
        }
    }
}

