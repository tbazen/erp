using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace INTAPS.Evaluator
{

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct EData : IComparable
    {
        public object Value;
        public DataType Type;
        public static EData Empty;
        public static EData Calculating;
        public static ArrayList DataTypes;
        private static Hashtable IndexDataTypes;
        public override string ToString()
        {
            if (this.Type == DataType.Empty)
            {
                return "";
            }
            if (this.Type == DataType.Calculating)
            {
                return "Calculating";
            }
           
            if (this.Value == null)
            {
                return "[Null]";
            }
            if (this.Type == DataType.Object)
            {
                
                if(this.Value.GetType().IsEnum)
                {
                    return Convert.ToInt32(this.Value).ToString();
                }
                string s = "{";
                bool first = true;
                foreach(System.Reflection.FieldInfo fi in Value.GetType().GetFields())
                {
                    if (first)
                        first = false;
                    else
                        s += ", ";
                    s += "\"" + fi.Name + "\":" + new EData(fi.GetValue(this.Value)).ToString();
                  
                }
                foreach (System.Reflection.PropertyInfo fi in Value.GetType().GetProperties())
                {
                    if (fi.GetIndexParameters().Length > 0)
                        continue;
                    if (first)
                        first = false;
                    else
                        s += ", ";
                    s += "\"" + fi.Name + "\":" + new EData(fi.GetValue(this.Value,new object[0])).ToString();

                }
                s += "}";
                return s;
            }
            return this.Value.ToString();
        }

        public IEDataType EDataType
        {
            get
            {
                if (IndexDataTypes.Contains(this.Type))
                {
                    return (IEDataType) DataTypes[(int) IndexDataTypes[this.Type]];
                }
                return new CommonType(this.Type);
            }
        }
        public EData(DataType t, object v)
        {
            this.Value = v;
            this.Type = t;
        }
        public EData(object v)
        {
            if (v == null)
            {
                this.Type = DataType.Empty;
                this.Value = null;
                return;
            }
            
            if (v is int)
            {
                this.Type = DataType.Int;
                this.Value = v;
                return;
            }
            if (v is uint)
            {
                this.Type = DataType.Int;
                this.Value = (int)v;
                return;
            }
            if (v is long)
            {
                this.Type = DataType.LongInt;
                this.Value = (long)v;
                return;
            }
            if (v is ulong)
            {
                this.Type = DataType.LongInt;
                this.Value = (long)(ulong)v;
                return;
            }
            if (v is short)
            {
                this.Type = DataType.Int;
                this.Value = (int)(short)v;
                return;
            }

            if (v is ushort)
            {
                this.Type = DataType.Int;
                this.Value = (int)(ushort)v;
                return;
            }
            if (v is double)
            {
                this.Type = DataType.Float;
                this.Value = v;
                return;
            }

            if (v is float)
            {
                this.Type = DataType.Float;
                this.Value = (double)(float)v;
                return;
            }
            if (v is DateTime)
            {
                this.Type = DataType.DateTime;
                this.Value = v;
                return;
            }
            if (v is string)
            {
                this.Type = DataType.Text;
                this.Value = v;
                return;
            }
            if (v is bool)
            {
                this.Type = DataType.Bool;
                this.Value = v;
                return;
            }
            if(v is byte[])
            {
                this.Type = DataType.ByteArray;
                this.Value = new ListData(v as byte[]);
                return;
            }
            if(v is IList)
            {
                this.Type = DataType.ListData;
                this.Value = new ListData(v as IList);
                return;
            }
            this.Type = DataType.Object;
            this.Value = v;
        }

        public static void Initialize()
        {
            Empty.Value = null;
            Empty.Type = DataType.Empty;
            Calculating.Value = null;
            Calculating.Type = DataType.Calculating;
            RegisterCommonTypes();
        }

        public void CreateObject()
        {
            if (DataTypes.Contains(this.Type))
            {
                this.Value = ((IEDataType) DataTypes[(int) IndexDataTypes[this.Type]]).CreateObject();
            }
        }

        private void WriteData(byte[] data)
        {
        }

        public static void RegisterDataType(IEDataType dt)
        {
            if (IndexDataTypes.Contains(dt.Type))
            {
                DataTypes[(int) IndexDataTypes[dt.Type]] = dt;
            }
            else
            {
                IndexDataTypes.Add(dt.Type, DataTypes.Count);
                DataTypes.Add(dt);
            }
        }

        private static void RegisterCommonTypes()
        {
            RegisterDataType(new CommonType(DataType.Float));
            RegisterDataType(new CommonType(DataType.Text));
        }

        private static EData CreateFloat(double value)
        {
            EData data;
            data.Type = DataType.Float;
            data.Value = value;
            return data;
        }

        public int CompareTo(object obj)
        {
            if (obj is EData)
            {
                EData data = (EData) obj;
                if (data.Type != this.Type)
                {
                    return -1;
                }
                switch (this.Type)
                {
                    case DataType.Text:
                        return ((string) this.Value).CompareTo((string) data.Value);

                    case DataType.Float:
                        if (((double) this.Value) >= ((double) data.Value))
                        {
                            if (((double) this.Value) > ((double) data.Value))
                            {
                                return 1;
                            }
                            return 0;
                        }
                        return -1;
                }
                if (this.Value is IComparable)
                {
                    return ((IComparable) this.Value).CompareTo(obj);
                }
            }
            return -1;
        }

        static EData()
        {
            Empty = new EData();
            Calculating = new EData();
            DataTypes = new ArrayList();
            IndexDataTypes = new Hashtable();
        }
    }
}

