using System;
namespace INTAPS.Evaluator
{

    public class EUtilities
    {
        public static bool CompareStringBin(string s1, string s2)
        {
            if (s1 == null)
            {
                return false;
            }
            if (s2 == null)
            {
                return false;
            }
            char[] chArray = s1.ToCharArray();
            char[] chArray2 = s2.ToCharArray();
            int length = chArray.Length;
            int num2 = chArray2.Length;
            if (length != num2)
            {
                return false;
            }
            for (int i = 0; i < length; i++)
            {
                if (chArray[i] != chArray2[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}

