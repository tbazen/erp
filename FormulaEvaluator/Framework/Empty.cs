using System;

namespace INTAPS.Evaluator
{

    public class Empty : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            return EData.Empty;
        }

        public string Name
        {
            get
            {
                return "Returns an empty data.";
            }
        }

        public int ParCount
        {
            get
            {
                return 0;
            }
        }

        public string Symbol
        {
            get
            {
                return "Empty";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

