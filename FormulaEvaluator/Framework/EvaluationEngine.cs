using System;
using System.Collections;
using System.Reflection;

namespace INTAPS.Evaluator
{

    public class EvaluationEngine
    {
        private INTAPS.Evaluator.Stack m_Stack;
        private ArrayList Program = new ArrayList();
        private ArrayList Vars = new ArrayList();

        public EvaluationEngine(INTAPS.Evaluator.Stack Stack)
        {
            this.m_Stack = Stack;
        }

        public void AddItem(ProgramEntry pe)
        {
            this.Program.Add(pe);
        }

        public int DefineVar()
        {
            this.Vars.Add(EData.Empty);
            return (this.Vars.Count - 1);
        }

        public EData Evaluate(Symbolic s)
        {
            foreach (ProgramEntry entry in this.Program)
            {
                IFunction function;
                EData[] dataArray;
                int num;
                EData data;
                switch (entry.Type)
                {
                    case ProgramEntryType.Data:
                    {
                        this.m_Stack.Push((EData) entry.Entry);
                        continue;
                    }
                    case ProgramEntryType.Var:
                    {
                        this.m_Stack.Push((EData) this.Vars[(int) entry.Entry]);
                        continue;
                    }
                    case ProgramEntryType.Function:
                        function = (IFunction) entry.Entry;
                        dataArray = new EData[function.ParCount];
                        num = dataArray.Length - 1;
                        goto Label_009E;

                    default:
                    {
                        continue;
                    }
                }
            Label_0081:
                dataArray[num] = this.m_Stack.Pop();
                num--;
            Label_009E:
                if (num >= 0)
                {
                    goto Label_0081;
                }
                try
                {
                    data = function.Evaluate(dataArray);
                }
                catch (Exception exception)
                {
                    data = new EData(DataType.Error, "Error evaluating " + function.Name + ". More Info: " + exception.Message);
                }
                this.m_Stack.Push(data);
            }
            if (this.m_Stack.Empty)
            {
                return EData.Empty;
            }
            return this.m_Stack.Pop();
        }

        public ProgramEntry ReturnTopProgramEntry()
        {
            return (ProgramEntry) this.Program[this.Program.Count - 1];
        }

        public bool IsAsync
        {
            get
            {
                return false;
            }
        }

        public bool IsComplete
        {
            get
            {
                return true;
            }
        }

        public EData this[int Var]
        {
            get
            {
                return (EData) this.Vars[Var];
            }
            set
            {
                this.Vars[Var] = value;
            }
        }

        public int ProgramCount
        {
            get
            {
                return this.Program.Count;
            }
        }

        public int ProgramSize
        {
            get
            {
                return this.Program.Count;
            }
        }
    }
}

