using System;
using System.Runtime.InteropServices;
namespace INTAPS.Evaluator
{
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct FSError
    {
        public int Number;
        public string Message;
        public FSError(string msg)
        {
            this.Number = 0;
            this.Message = msg;
        }

        public FSError(int n, string msg)
        {
            this.Number = n;
            this.Message = msg;
        }

        public EData ToEData()
        {
            EData data = new EData();
            data.Type = DataType.Error;
            data.Value = this;
            return data;
        }

        public override string ToString()
        {
            return this.Message;
        }
    }
}

