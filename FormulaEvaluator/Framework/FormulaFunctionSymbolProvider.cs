using System;
namespace INTAPS.Evaluator
{
    public class FormulaFunctionSymbolProvider : ISymbolProvider
    {
        private IDynamicFormulaFunction m_formulaFunc;
        private ISymbolProvider m_symbolProvider;

        public FormulaFunctionSymbolProvider(ISymbolProvider p, IDynamicFormulaFunction f)
        {
            this.m_symbolProvider = p;
            this.m_formulaFunc = f;
        }

        public FunctionDocumentation[] GetAvialableFunctions()
        {
            return this.m_symbolProvider.GetAvialableFunctions();
        }

        public EData GetData(URLIden iden)
        {
            return this.GetData(iden.Element);
            //return this.m_symbolProvider.GetData(iden);
        }

        public EData GetData(string symbol)
        {
            if (this.m_formulaFunc.Defines(symbol))
            {
                return this.m_formulaFunc.GetData(symbol);
            }
            return this.m_symbolProvider.GetData(symbol);
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return this.m_symbolProvider.GetDocumentation(f);
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return this.m_symbolProvider.GetDocumentation(iden);
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            return this.m_symbolProvider.GetDocumentation(Symbol);
        }

        public IFunction GetFunction(URLIden iden)
        {
            return this.m_symbolProvider.GetFunction(iden);
        }

        public IFunction GetFunction(string symbol)
        {
            return this.m_symbolProvider.GetFunction(symbol);
        }

        public bool SymbolDefined(string Name)
        {
            return (this.m_formulaFunc.Defines(Name) || this.m_symbolProvider.SymbolDefined(Name));
        }
    }
}

