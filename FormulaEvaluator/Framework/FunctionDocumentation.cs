using System;
using System.IO;
using System.Xml;
namespace INTAPS.Evaluator
{
    [Serializable]
    public class FunctionDocumentation
    {
        [NonSerialized]
        private XmlDocument m_x;
        public string XmlStr;
        public FunctionDocumentation()
        {

        }
        public FunctionDocumentation(IFunction f)
        {
            this.XmlStr = null;
            this.m_x = null;
            this.m_x = new XmlDocument();
            this.m_x.LoadXml("<Func/>");
            XmlElement newChild = this.m_x.CreateElement("Name");
            newChild.InnerText = f.Symbol;
            this.m_x.DocumentElement.AppendChild(newChild);
            newChild = this.m_x.CreateElement("Desc");
            newChild.InnerText = f.Name;
            this.m_x.DocumentElement.AppendChild(newChild);
            newChild = this.m_x.CreateElement("Pars");
            int parCount = f.ParCount;
            for (int i = 0; i < parCount; i++)
            {
                XmlElement element2 = this.m_x.CreateElement("p");
                XmlAttribute node = this.m_x.CreateAttribute("type");
                node.Value = "Mand";
                element2.Attributes.Append(node);
                newChild.AppendChild(element2);
            }
            this.m_x.DocumentElement.AppendChild(newChild);
            newChild = this.m_x.CreateElement("HLPURL");
            this.m_x.DocumentElement.AppendChild(newChild);
            this.FixString();
        }

        public FunctionDocumentation(IFunctionDocumentation f)
        {
            this.XmlStr = null;
            this.m_x = null;
            this.m_x = new XmlDocument();
            this.m_x.LoadXml(f.Documentation);
            this.FixString();
        }

        public FunctionDocumentation(string doc)
        {
            this.XmlStr = null;
            this.m_x = null;
            this.m_x = new XmlDocument();
            this.m_x.LoadXml(doc);
            this.FixString();
        }

        public static string CreateDocumentation(string Name, string Desc, string[] pars, string[] pardesc, string[] types)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml("<Func></Func>");
            XmlElement newChild = document.CreateElement("Name");
            newChild.InnerText = Name;
            XmlElement element2 = document.CreateElement("Desc");
            element2.InnerText = Desc;
            XmlElement element3 = document.CreateElement("Pars");
            for (int i = 0; i < pars.Length; i++)
            {
                XmlElement element4 = document.CreateElement("P");
                XmlElement element5 = document.CreateElement("Name");
                element5.InnerText = pars[i];
                XmlElement element6 = document.CreateElement("Desc");
                element6.InnerText = pardesc[i];
                XmlAttribute node = document.CreateAttribute("type");
                node.InnerText = types[i];
                element4.AppendChild(element5);
                element4.AppendChild(element6);
                element4.Attributes.Append(node);
                element3.AppendChild(element4);
            }
            document.DocumentElement.AppendChild(newChild);
            document.DocumentElement.AppendChild(element2);
            document.DocumentElement.AppendChild(element3);
            return document.InnerXml;
        }

        public static string CreateDocumentation(string Name, string Desc, string[] pars, string[] pardesc, string[] types, string Cat)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml("<Func></Func>");
            XmlElement newChild = document.CreateElement("Name");
            newChild.InnerText = Name;
            XmlElement element2 = document.CreateElement("Desc");
            element2.InnerText = Desc;
            XmlElement element3 = document.CreateElement("Pars");
            for (int i = 0; i < pars.Length; i++)
            {
                XmlElement element4 = document.CreateElement("P");
                XmlElement element5 = document.CreateElement("Name");
                element5.InnerText = pars[i];
                XmlElement element6 = document.CreateElement("Desc");
                element6.InnerText = pardesc[i];
                XmlAttribute node = document.CreateAttribute("type");
                node.InnerText = types[i];
                element4.Attributes.Append(node);
                element4.AppendChild(element5);
                element4.AppendChild(element6);
                element3.AppendChild(element4);
            }
            XmlElement element7 = document.CreateElement("CAT");
            element7.InnerText = Cat;
            document.DocumentElement.AppendChild(newChild);
            document.DocumentElement.AppendChild(element2);
            document.DocumentElement.AppendChild(element3);
            document.DocumentElement.AppendChild(element7);
            return document.InnerXml;
        }

        private void FixString()
        {
            if (this.m_x == null)
            {
                this.XmlStr = null;
            }
            else
            {
                StringWriter w = new StringWriter();
                this.m_x.WriteTo(new XmlTextWriter(w));
                this.XmlStr = w.ToString();
            }
        }

        private void FixXML()
        {
            if (!((this.m_x != null) || string.IsNullOrEmpty(this.XmlStr)))
            {
                this.m_x = new XmlDocument();
                this.m_x.LoadXml(this.XmlStr);
            }
        }

        public string GetParName(int i)
        {
            this.FixXML();
            if (this.m_x.DocumentElement.ChildNodes.Count >= 3)
            {
                XmlNode node = this.m_x.DocumentElement.ChildNodes[2];
                int count = node.ChildNodes.Count;
                if (i < count)
                {
                    if (node.ChildNodes[i].ChildNodes.Count > 0)
                    {
                        return node.ChildNodes[i].ChildNodes[0].InnerText;
                    }
                }
                else
                {
                    node = node.ChildNodes[node.ChildNodes.Count - 1];
                    if (node != null)
                    {
                        XmlAttribute namedItem = (XmlAttribute) node.Attributes.GetNamedItem("type");
                        if (((namedItem != null) && (namedItem.Value.ToUpper() == "VAR")) && (node.ChildNodes.Count > 0))
                        {
                            return (node.ChildNodes[0].InnerText + " " + ((i - count) + 2));
                        }
                    }
                }
            }
            return ("Parameter " + i);
        }

        public string Category
        {
            get
            {
                this.FixXML();
                if (this.m_x.DocumentElement.ChildNodes.Count > 3)
                {
                    return this.m_x.DocumentElement.ChildNodes[3].InnerText;
                }
                return "Misc";
            }
        }

        public string Description
        {
            get
            {
                this.FixXML();
                if (this.m_x.DocumentElement.ChildNodes.Count == 0)
                {
                    return this.m_x.DocumentElement.InnerText;
                }
                if (this.m_x.DocumentElement.ChildNodes.Count > 1)
                {
                    return this.m_x.DocumentElement.ChildNodes[1].InnerText;
                }
                return null;
            }
        }

        public string FunctionName
        {
            get
            {
                this.FixXML();
                if (this.m_x.DocumentElement.ChildNodes.Count > 0)
                {
                    return this.m_x.DocumentElement.ChildNodes[0].InnerText;
                }
                return null;
            }
            set
            {
                if (this.m_x.DocumentElement.ChildNodes.Count > 0)
                {
                    this.m_x.DocumentElement.ChildNodes[0].InnerText = value;
                    this.FixString();
                }
            }
        }

        public int NPars
        {
            get
            {
                this.FixXML();
                XmlNodeList elementsByTagName = this.m_x.DocumentElement.GetElementsByTagName("Pars");
                if (elementsByTagName.Count == 0)
                {
                    return 0;
                }
                return elementsByTagName[0].ChildNodes.Count;
            }
        }
    }
}

