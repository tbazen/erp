using System;
namespace INTAPS.Evaluator
{

    public interface IDBConnection
    {
        IDisconnectedRecordSet GetDisconnectedRecordSet(string SqlCommand);
        ListData GetList1D(string SqlCmd);
        ListData GetList2D(string SqlCmd);
        IRecordset GetRecordset(string SqlCmd);
        EData GetSingleValue(string SqlCmd);
        Vector1D GetVector1D(string SqlCmd);
        Vector2D GetVector2D(string SqlCmd);
        string ShowConnectionUI();
    }
}

