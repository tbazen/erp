using System;
using System.Reflection;
namespace INTAPS.Evaluator
{
    public interface IDisconnectedRecordSet
    {
        string ColumnName(int ColIndex);
        int Fill(int startindex, int maxrecords);

        int ColCount { get; }

        EData this[int RowIndex, int ColIndex] { get; }

        int RowCount { get; }

        int TotalRows { get; }
    }
}

