using System;
namespace INTAPS.Evaluator
{
    public interface IDynamicFormulaFunction : IFormulaFunction, IVarParamCountFunction, IFunction
    {
        bool Defines(string symbol);
        EData GetData(string symbol);
    }
}

