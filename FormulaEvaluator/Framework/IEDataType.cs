using System;
namespace INTAPS.Evaluator
{

    public interface IEDataType
    {
        object CreateObject();

        string Name { get; }

        DataType Type { get; }
    }
}

