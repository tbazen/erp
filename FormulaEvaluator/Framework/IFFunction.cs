using System;

namespace INTAPS.Evaluator
{

    internal class IFFunction : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.Bool)
            {
                FSError error = new FSError(100, "Type mismatch:first paramter must be boolean expression");
                return error.ToEData();
            }
            if ((bool) Pars[0].Value)
            {
                return Pars[1];
            }
            return Pars[2];
        }

        public string GetParName(int i)
        {
            switch (i)
            {
                case 0:
                    return "Condition";

                case 1:
                    return "True case";

                case 2:
                    return "False case";
            }
            return null;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Returns either of the second or the third parameter depending on whether the first parameter evaluates to true or false", new string[] { "Condition", "Ture case", "False case" }, new string[] { "An expression that evaluates to true of false", "the value that is returned if the condition is ture", "the value that is retured if the condition is false" }, new string[] { "Mand", "Mand", "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "If function";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "If";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

