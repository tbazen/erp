using System;
namespace INTAPS.Evaluator
{
    public interface IFormulaFunction : IVarParamCountFunction, IFunction
    {
        string[] Dependencies { get; }

        INTAPS.Evaluator.ISymbolProvider ISymbolProvider { set; }
    }
}

