using System;

namespace INTAPS.Evaluator
{

    public interface IFunction
    {
        EData Evaluate(EData[] Pars);

        string Name { get; }

        int ParCount { get; }

        string Symbol { get; }

        FunctionType Type { get; }
    }
}

