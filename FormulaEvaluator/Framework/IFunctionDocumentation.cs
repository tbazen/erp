using System;
namespace INTAPS.Evaluator
{
    public interface IFunctionDocumentation
    {
        string Documentation { get; }
    }
}

