using System;
namespace INTAPS.Evaluator
{
    public interface IInfixFunction : IFunction
    {
        int Precidence { get; }
    }
}

