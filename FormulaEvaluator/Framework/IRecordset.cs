using System;
using System.Reflection;

namespace INTAPS.Evaluator
{

    public interface IRecordset
    {
        void Close();
        string ColName(int ColIndex);
        bool Read();
        bool Requery();

        int ColCount { get; }

        int CurrentRow { get; set; }

        bool IsClosed { get; }

        EData this[int ColIndex] { get; }

        EData this[string colname] { get; }

        int RowCount { get; }
    }
}

