using System;
namespace INTAPS.Evaluator
{
    public interface ISymbolProvider
    {
        FunctionDocumentation[] GetAvialableFunctions();
        EData GetData(URLIden iden);
        EData GetData(string symbol);
        FunctionDocumentation GetDocumentation(IFunction f);
        FunctionDocumentation GetDocumentation(URLIden iden);
        FunctionDocumentation GetDocumentation(string Symbol);
        IFunction GetFunction(URLIden iden);
        IFunction GetFunction(string symbol);
        bool SymbolDefined(string Name);
    }
}

