using System;
namespace INTAPS.Evaluator
{

    public interface IVarParamCountFunction : IFunction
    {
        IVarParamCountFunction Clone();
        bool SetParCount(int n);
    }
}

