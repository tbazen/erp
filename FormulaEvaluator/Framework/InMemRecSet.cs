using System;
using System.Reflection;
namespace INTAPS.Evaluator
{

    internal class InMemRecSet : IRecordset
    {
        private ListData m_ColNames;
        private int m_Cur = -1;
        private ListData m_Data;

        public InMemRecSet(ListData data, ListData ColNames)
        {
            this.m_Data = data;
            this.m_ColNames = ColNames;
        }

        public void Close()
        {
            this.m_Cur = -1;
        }

        public string ColName(int ColIndex)
        {
            EData data = this.m_ColNames[ColIndex];
            return data.ToString();
        }

        public bool Read()
        {
            if (this.m_Cur == (this.m_Data.elements.Length - 1))
            {
                this.m_Cur = -1;
                return false;
            }
            this.m_Cur++;
            return true;
        }

        public bool Requery()
        {
            this.m_Cur = -1;
            return true;
        }

        public int ColCount
        {
            get
            {
                return this.m_ColNames.elements.Length;
            }
        }

        public int CurrentRow
        {
            get
            {
                return this.m_Cur;
            }
            set
            {
                this.m_Cur = value;
            }
        }

        public bool IsClosed
        {
            get
            {
                return false;
            }
        }

        public EData this[string colname]
        {
            get
            {
                int colIndex = 0;
                for (colIndex = 0; colIndex < this.ColCount; colIndex++)
                {
                    if (this.ColName(colIndex).ToUpper() == colname.ToUpper())
                    {
                        return this[colIndex];
                    }
                }
                return EData.Empty;
            }
        }

        public EData this[int ColIndex]
        {
            get
            {
                return ((ListData) this.m_Data[this.m_Cur].Value)[ColIndex];
            }
        }

        public int RowCount
        {
            get
            {
                return this.m_Data.elements.Length;
            }
        }
    }
}

