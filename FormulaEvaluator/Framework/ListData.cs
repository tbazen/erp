using System;
using System.Collections;
using System.Reflection;

namespace INTAPS.Evaluator
{

    [Serializable]
    public class ListData : IName, IFunction
    {
        public EData[] elements;
        public static ListData EmptyList = new ListData(0);
        public string m_Name;
        public int m_NumberElements;

        public ListData() : this(0)
        {
        }

        public ListData(int n)
        {
            this.m_Name = "";
            this.m_NumberElements = n;
            this.elements = new EData[this.m_NumberElements];
            for (int i = 0; i < this.m_NumberElements; i++)
            {
                this.elements[i] = EData.Empty;
            }
        }

        public ListData(EData[] data)
        {
            this.m_Name = "";
            this.m_NumberElements = data.Length;
            this.elements = data;
        }
        public ListData(IList list):this(list.Count)
        {
            int i=0;
            foreach(object e in list)
            {
                this.elements[i] = new EData(e);
                i++;
            }
        }
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.Float)
            {
                error = new FSError(2, "Type mismatch.");
                return error.ToEData();
            }
            EData data = new EData();
            int num = (int) ((double) Pars[0].Value);
            if ((num < 1) || (num > this.m_NumberElements))
            {
                error = new FSError(5, "List Index Out ofBound.");
                return error.ToEData();
            }
            return this.elements[num - 1];
        }

        public static void Initialize()
        {
            EData.RegisterDataType(new ListDataType());
        }

        public static ListData MakeList(EData[] ele)
        {
            ListData data = new ListData(ele.Length);
            for (int i = 0; i < ele.Length; i++)
            {
                data.elements[i] = ele[i];
            }
            return data;
        }

        public override string ToString()
        {
            if (this.elements.Length > 100)
            {
                return ("List(" + this.elements.Length + ")");
            }
            string str = " { ";
            for (int i = 0; i < this.m_NumberElements; i++)
            {
                str = str + this.elements[i].ToString();
                if (i != (this.m_NumberElements - 1))
                {
                    str = str + " , ";
                }
            }
            return (str + " } ");
        }

        string IFunction.Name
        {
            get
            {
                return this.m_Name.ToUpper();
            }
        }

        public EData this[int i]
        {
            get
            {
                return this.elements[i];
            }
            set
            {
                this.elements[i] = value;
            }
        }

        public string Name
        {
            get
            {
                return this.m_Name;
            }
            set
            {
                this.m_Name = value;
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return this.m_Name.ToUpper();
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

