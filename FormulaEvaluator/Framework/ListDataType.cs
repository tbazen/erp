using System;
namespace INTAPS.Evaluator
{
    internal class ListDataType : IEDataType
    {
        private ListData m_ToSerialze;

        public object CreateObject()
        {
            return new ListData(0);
        }

        public int EndDeSerializeFromBin()
        {
            return 0;
        }

        public int GetByteCount()
        {
            return 0;
        }

        public void StartDeSerializeFromBin()
        {
        }

        public void StartSerializeToBin(object Target)
        {
            this.m_ToSerialze = (ListData) Target;
        }

        public string Name
        {
            get
            {
                return "ListData";
            }
        }

        public object Result
        {
            get
            {
                return this.m_ToSerialze;
            }
        }

        public DataType Type
        {
            get
            {
                return DataType.ListData;
            }
        }
    }
}

