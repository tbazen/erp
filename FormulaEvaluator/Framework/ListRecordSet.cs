using System;
namespace INTAPS.Evaluator
{
    internal class ListRecordSet : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if ((Pars[0].Type != DataType.ListData) || (Pars[1].Type != DataType.ListData))
            {
                error = new FSError("Type mistmatch");
                return error.ToEData();
            }
            int length = -1;
            foreach (EData data in ((ListData) Pars[0].Value).elements)
            {
                if (data.Type != DataType.ListData)
                {
                    error = new FSError("The first list should be proper 2D list.");
                    return error.ToEData();
                }
                if (length == -1)
                {
                    length = ((ListData) Pars[0].Value).elements.Length;
                }
                else if (length != ((ListData) Pars[0].Value).elements.Length)
                {
                    error = new FSError("The first list should be proper 2D list.");
                    return error.ToEData();
                }
            }
            InMemRecSet set = new InMemRecSet((ListData) Pars[0].Value, (ListData) Pars[1].Value);
            EData data2 = new EData();
            data2.Type = DataType.RecordSet;
            data2.Value = set;
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Creates a recordset from list", new string[] { "Data list", "Name List" }, new string[] { "data list", "a column data list" }, new string[] { "Mand", "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "Create recordset from list";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "ListRS";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

