using System;
using System.Runtime.InteropServices;

namespace INTAPS.Evaluator
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ProgramEntry
    {
        public object Entry;
        public ProgramEntryType Type;
    }
}

