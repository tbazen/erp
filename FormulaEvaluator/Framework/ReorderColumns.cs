using System;
namespace INTAPS.Evaluator
{
    internal class ReorderColumns : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.RecordSet)
            {
                error = new FSError("First parameter should be a recordset");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Float)
            {
                error = new FSError("Second parameter should be a number designating a column.");
                return error.ToEData();
            }
            if (Pars[2].Type != DataType.Float)
            {
                error = new FSError("third parameter should be a number designating a column.");
                return error.ToEData();
            }
            IRecordset rs = (IRecordset) Pars[0].Value;
            int num = (int) ((double) Pars[1].Value);
            int num2 = (int) ((double) Pars[2].Value);
            if ((((num < 1) || (num > rs.ColCount)) || (num2 < 1)) || (num2 > rs.ColCount))
            {
                error = new FSError("Invalid column index");
                return error.ToEData();
            }
            rs.Requery();
            EData data = new EData();
            data.Type = DataType.RecordSet;
            data.Value = new ReorderedRecordSet(rs, num, num2);
            return data;
        }

        public string Name
        {
            get
            {
                return "Reorder columns";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "ReorderCols";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

