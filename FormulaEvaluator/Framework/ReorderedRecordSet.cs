using System;
using System.Reflection;
namespace INTAPS.Evaluator
{

    internal class ReorderedRecordSet : IRecordset
    {
        private int m_index1;
        private int m_index2;
        private IRecordset m_Internal;

        internal ReorderedRecordSet(IRecordset rs, int index1, int index2)
        {
            this.m_Internal = rs;
            this.m_index1 = index1;
            this.m_index2 = index2;
        }

        public void Close()
        {
            this.m_Internal.Close();
        }

        public string ColName(int ColIndex)
        {
            if (ColIndex == (this.m_index1 - 1))
            {
                return this.m_Internal.ColName(this.m_index2 - 1);
            }
            if (ColIndex == (this.m_index2 - 1))
            {
                return this.m_Internal.ColName(this.m_index1 - 1);
            }
            return this.m_Internal.ColName(ColIndex);
        }

        public bool Read()
        {
            return this.m_Internal.Read();
        }

        public bool Requery()
        {
            return this.m_Internal.Requery();
        }

        public int ColCount
        {
            get
            {
                return this.m_Internal.ColCount;
            }
        }

        public int CurrentRow
        {
            get
            {
                return this.m_Internal.CurrentRow;
            }
            set
            {
                this.m_Internal.CurrentRow = value;
            }
        }

        public bool IsClosed
        {
            get
            {
                return this.m_Internal.IsClosed;
            }
        }

        public EData this[string colname]
        {
            get
            {
                int colIndex = 0;
                for (colIndex = 0; colIndex < this.ColCount; colIndex++)
                {
                    if (this.m_Internal.ColName(colIndex).ToUpper() == colname.ToUpper())
                    {
                        return this[colIndex];
                    }
                }
                return EData.Empty;
            }
        }

        public EData this[int ColIndex]
        {
            get
            {
                if (ColIndex == (this.m_index1 - 1))
                {
                    return this.m_Internal[this.m_index2 - 1];
                }
                if (ColIndex == (this.m_index2 - 1))
                {
                    return this.m_Internal[this.m_index1 - 1];
                }
                return this.m_Internal[ColIndex];
            }
        }

        public int RowCount
        {
            get
            {
                return this.m_Internal.RowCount;
            }
        }
    }
}

