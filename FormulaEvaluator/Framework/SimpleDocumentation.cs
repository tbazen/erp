using System;
namespace INTAPS.Evaluator
{

    public class SimpleDocumentation : IFunctionDocumentation
    {
        private IFunction m_f;

        public SimpleDocumentation(IFunction f)
        {
            this.m_f = f;
        }

        public virtual string[] AddParameters(int n)
        {
            return new string[] { this.GetParName(n++) };
        }

        public virtual int DeleteParameters(int n)
        {
            return 1;
        }

        public virtual string GetParName(int i)
        {
            if (i < this.m_f.ParCount)
            {
                return ("Parameter " + (i + 1));
            }
            return null;
        }

        public string Documentation
        {
            get
            {
                return ("<Doc>" + this.m_f.Name + "(Not documented)</doc>");
            }
        }
    }
}

