using System;
namespace INTAPS.Evaluator
{
    public class Stack
    {
        private EData[] Data = new EData[10];
        private const int Delta = 10;
        private const int InitialSize = 10;
        private int Size = 10;
        private int SP = 0;

        public void Clear()
        {
            this.SP = 0;
        }

        public EData Peek()
        {
            return this.Data[this.SP];
        }

        public EData Pop()
        {
            return this.Data[--this.SP];
        }

        public void Push(EData d)
        {
            if (this.SP == this.Size)
            {
                this.Size += 10;
                EData[] array = new EData[this.Size];
                this.Data.CopyTo(array, 0);
                this.Data = array;
            }
            this.Data[this.SP++] = d;
        }

        public bool Empty
        {
            get
            {
                return (this.SP == 0);
            }
        }
    }
}

