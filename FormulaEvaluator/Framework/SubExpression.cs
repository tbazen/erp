using System;
using System.Runtime.InteropServices;
namespace INTAPS.Evaluator
{

    [StructLayout(LayoutKind.Sequential)]
    public struct SubExpression
    {
        public SubExpressionType Type;
        public int Index;
        public int Length;
        public SubExpression(SubExpressionType type, int index, int length)
        {
            this.Type = type;
            this.Index = index;
            this.Length = length;
        }

        public bool IsSub(int i)
        {
            return ((this.Index <= i) && (i < (this.Index + this.Length)));
        }

        public bool IsSub(int i, int l)
        {
            return (((this.Index <= i) && ((i + l) < (this.Index + this.Length))) || ((this.Index < i) && ((i + l) <= (this.Index + this.Length))));
        }
    }
}

