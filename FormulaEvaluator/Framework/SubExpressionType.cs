using System;

namespace INTAPS.Evaluator
{

    public enum SubExpressionType
    {
        FunctionCall,
        Infix,
        Text,
        List
    }
}

