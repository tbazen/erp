using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
namespace INTAPS.Evaluator
{
    public class Symbolic
    {
        public ArrayList Dependencies;
        public string m_Error;
        public EvaluationEngine m_evaluator;
        private string m_Exp;
        private Hashtable m_Handles;
        public ISymbolProvider m_ISymbolProvider;
        private string m_OrgExp;
        private ArrayList m_ParseStack;
        private bool m_SaveStructure;
        private INTAPS.Evaluator.Stack m_stack;
        private ArrayList m_Structure;
        private ArrayList m_Vars;
        public int StructureIndex;
        public int StructureLength;
        internal bool UnKnownFunctionPresent;

        public Symbolic() : this(new INTAPS.Evaluator.Stack())
        {
        }

        public Symbolic(INTAPS.Evaluator.Stack stack)
        {
            this.m_Handles = new Hashtable();
            this.m_Vars = new ArrayList();
            this.m_OrgExp = "";
            this.UnKnownFunctionPresent = false;
            this.m_ISymbolProvider = null;
            this.m_Structure = new ArrayList();
            this.m_SaveStructure = true;
            this.m_Error = "";
            this.m_ISymbolProvider = new DefaultProvider();
            this.m_stack = stack;
        }

        public Symbolic(string exp) : this(exp, new INTAPS.Evaluator.Stack())
        {
        }

        public Symbolic(string expression, INTAPS.Evaluator.Stack stack)
        {
            this.m_Handles = new Hashtable();
            this.m_Vars = new ArrayList();
            this.m_OrgExp = "";
            this.UnKnownFunctionPresent = false;
            this.m_ISymbolProvider = null;
            this.m_Structure = new ArrayList();
            this.m_SaveStructure = true;
            this.m_Error = "";
            this.Expression = expression;
            this.m_ISymbolProvider = new DefaultProvider();
            this.m_stack = stack;
        }

        public void AddDependency(string dep)
        {
            if (!this.Dependencies.Contains(dep.ToUpper()))
            {
                this.Dependencies.Add(dep.ToUpper());
            }
        }

        private void AddSubExpression(SubExpression se)
        {
            for (int i = 0; i < this.m_Structure.Count; i++)
            {
                SubExpression expression = (SubExpression) this.m_Structure[i];
                if ((expression.Index == se.Index) && (expression.Length == se.Length))
                {
                    return;
                }
            }
            this.m_Structure.Add(se);
        }

        public void AddVariable(string Name, EData value)
        {
            if (this.m_ISymbolProvider is DefaultProvider)
            {
                if (((DefaultProvider) this.m_ISymbolProvider).m_Variables.Contains(Name.ToUpper()))
                {
                    ((DefaultProvider) this.m_ISymbolProvider).m_Variables[Name.ToUpper()] = value;
                }
                else
                {
                    ((DefaultProvider) this.m_ISymbolProvider).m_Variables.Add(Name.ToUpper(), value);
                }
            }
        }

        public Symbolic Clone()
        {
            return (Symbolic) base.MemberwiseClone();
        }

        public bool ContainsVariable(string VarName)
        {
            return this.m_Handles.Contains(VarName.ToUpper());
        }

        public bool Depends(string VarName)
        {
            return this.Dependencies.Contains(VarName.ToUpper());
        }

        public EData Evaluate()
        {
            FSError error;
            if (this.UnKnownFunctionPresent)
            {
                error = new FSError(0x90, "Unknown Function Call.");
                return error.ToEData();
            }
            try
            {
                foreach (URLIden iden in this.m_Vars)
                {
                    if (this.m_ISymbolProvider.SymbolDefined(iden.Element))
                    {
                        this[iden.Element] = this.m_ISymbolProvider.GetData(iden.Element);
                    }
                }
                return this.m_evaluator.Evaluate(this);
            }
            catch (Exception exception)
            {
                error = new FSError(4, "System Error-" + exception.Message);
                return error.ToEData();
            }
        }

        private void FlushStack(int To)
        {
            int num = this.m_ParseStack.Count - 1;
            for (int i = num; i > To; i--)
            {
                ProgramEntry pe = new ProgramEntry();
                pe.Type = ProgramEntryType.Function;
                pe.Entry = this.PopInfixFunction();
                this.m_evaluator.AddItem(pe);
            }
        }

        private int GetAlpha(int i)
        {
            if (IsAlpha(this.m_Exp[i]))
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetAlphaNum(int i)
        {
            if (IsAlpha(this.m_Exp[i]) || IsNumeric(this.m_Exp[i]))
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetAlphaNumDot(int i)
        {
            if ((IsAlpha(this.m_Exp[i]) || IsNumeric(this.m_Exp[i])) || (this.m_Exp[i] == '.'))
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetCloseBrace(int i)
        {
            if (this.m_Exp[i] == ')')
            {
                return (i + 1);
            }
            return -1;
        }

        public int GetCloseBraceCurly(int i)
        {
            if (this.m_Exp[i] == '}')
            {
                return (i + 1);
            }
            return -1;
        }

        public int GetCloseBraceSqr(int i)
        {
            if (this.m_Exp[i] == ']')
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetComma(int i)
        {
            if (this.m_Exp[i] == ',')
            {
                return (i + 1);
            }
            return -1;
        }
        private int GetColon(int i)
        {
            if (this.m_Exp[i] == ':')
            {
                return (i + 1);
            }
            return -1;
        }
        private int GetDateTime(int i, out DateTime datetime)
        {
            datetime = new DateTime();
            int num = i;
            int hashChar = this.GetHashChar(i);
            if (hashChar == -1)
            {
                return -1;
            }
            i = hashChar;
            while (true)
            {
                hashChar = this.GetTextDateTime(i);
                if ((hashChar == -1) || (hashChar == i))
                {
                    break;
                }
                i = hashChar;
            }
            hashChar = this.GetHashChar(i);
            if (hashChar == -1)
            {
                return -1;
            }
            i = hashChar;
            string str = this.m_Exp.Substring(num + 1, (i - num) - 2);
            try
            {
                datetime = Convert.ToDateTime(str);
                return i;
            }
            catch
            {
                this.m_Error = "Invalid Date/time format";
                return -1;
            }
        }

        private int GetDot(int i)
        {
            if (this.m_Exp[i] == '.')
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetDouble(int i, out double d)
        {
            bool flag = false;
            d = 0.0;
            int startIndex = i;
            int minus = this.GetMinus(i);
            if (minus != -1)
            {
                i = minus;
            }
            minus = this.GetInteger(i);
            if (minus != -1)
            {
                i = minus;
                flag = true;
            }
            minus = this.GetDot(i);
            if (minus != -1)
            {
                i = minus;
                minus = this.GetInteger(i);
                if (minus == -1)
                {
                    return -1;
                }
                i = minus;
            }
            else if (!flag)
            {
                return -1;
            }
            minus = this.GetE(i);
            if (minus != -1)
            {
                i = minus;
                minus = this.GetMinus(i);
                if (minus != -1)
                {
                    i = minus;
                }
                if (this.GetInteger(i) == -1)
                {
                    return -1;
                }
            }
            d = double.Parse(this.m_Exp.Substring(startIndex, i - startIndex));
            return i;
        }

        private int GetDoubleQuot(int i)
        {
            if ((this.m_Exp[i] == '"') && (this.m_Exp[i + 1] == '"'))
            {
                return (i + 2);
            }
            return -1;
        }

        private int GetE(int i)
        {
            if (this.m_Exp[i] == 'E')
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetExp(int i)
        {
            int index = i;
            int to = this.m_ParseStack.Count - 1;
            i = this.GetWhiteSpace(i);
            int item = this.GetItem(i);
            if (item == -1)
            {
                return -1;
            }
            i = item;
            int num4 = 1;
            while (true)
            {
                IInfixFunction function;
                i = this.GetWhiteSpace(i);
                item = this.GetOp(i, out function);
                if (item == -1)
                {
                    this.FlushStack(to);
                    if (num4 > 1)
                    {
                        this.AddSubExpression(new SubExpression(SubExpressionType.Infix, index, i - index));
                    }
                    return i;
                }
                i = item;
                this.PushInfixFunction(function, to);
                i = this.GetWhiteSpace(i);
                item = this.GetItem(i);
                if (item == -1)
                {
                    return -1;
                }
                num4++;
                i = item;
            }
        }

        private int GetFuncCall(int i)
        {
            int num2;
            URLIden iden=new URLIden();
            iden.HName = new string[1];
            //int uRLItem = this.GetURLItem(i, out iden);
            int index = this.GetIden(i, out iden.HName[0]);
            if (index == -1)
            {
                return -1;
            }
            i = index;
            i = this.GetWhiteSpace(i);
            index = this.GetParList(i, out num2);
            if (index == -1)
            {
                return -1;
            }
            IFunction f = this.m_ISymbolProvider.GetFunction(iden);
            if (f is IVarParamCountFunction)
            {
                f = ((IVarParamCountFunction) f).Clone();
                if (!((IVarParamCountFunction) f).SetParCount(num2))
                {
                    this.m_Error = "Too many or too few parameters in function call " + this.m_Exp.Substring(i, index - i);
                    return -1;
                }
            }
            else if (f == null)
            {
                this.PushVariable(false, ref iden);
                f = this.m_ISymbolProvider.GetFunction("index2");
            }
            bool flag = false;
            if (f == null)
            {
                flag = true;
            }
            this.PushFunction(f);
            this.UnKnownFunctionPresent = this.UnKnownFunctionPresent || flag;
            return index;
        }

        private int GetHashChar(int i)
        {
            if (this.m_Exp[i] == '#')
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetHirarchicalIden(int i, out string[] Names)
        {
            string str;
            ArrayList list = new ArrayList();
            Names = null;
            int iden = this.GetIden(i, out str);
            if (iden == -1)
            {
                return -1;
            }
            i = iden;
            list.Add(str);
            while (true)
            {
                iden = this.GetDot(i);
                if (iden == -1)
                {
                    Names = new string[list.Count];
                    list.CopyTo(Names);
                    return i;
                }
                i = iden;
                iden = this.GetIden(i, out str);
                if (iden == -1)
                {
                    return -1;
                }
                i = iden;
                list.Add(str);
            }
        }

        private int GetHirarchicalIdenQuat(int i, out string[] Names)
        {
            string str;
            ArrayList list = new ArrayList();
            Names = null;
            int idenQuat = this.GetIdenQuat(i, out str);
            if (idenQuat == -1)
            {
                return -1;
            }
            i = idenQuat;
            list.Add(str);
            while (true)
            {
                idenQuat = this.GetDot(i);
                if (idenQuat == -1)
                {
                    Names = new string[list.Count];
                    list.CopyTo(Names);
                    return i;
                }
                i = idenQuat;
                idenQuat = this.GetIdenQuat(i, out str);
                if (idenQuat == -1)
                {
                    return -1;
                }
                i = idenQuat;
                list.Add(str);
            }
        }

        private int GetIden(int i, out string Name)
        {
            int startIndex = i;
            Name = "";
            if (this.GetAlpha(i) == -1)
            {
                return -1;
            }
            while (true)
            {
                int alphaNum = this.GetAlphaNum(i);
                if (alphaNum == -1)
                {
                    Name = this.m_Exp.Substring(startIndex, i - startIndex).ToUpper();
                    return i;
                }
                i = alphaNum;
            }
        }

        private int GetIdenDNS(int i, out string Name)
        {
            return GetIden(i, out Name);
            //int startIndex = i;
            //Name = "";
            //if (this.GetAlphaNum(i) == -1)
            //{
            //    return -1;
            //}
            //while (true)
            //{
            //    int alphaNumDot = this.GetAlphaNumDot(i);
            //    if (alphaNumDot == -1)
            //    {
            //        Name = this.m_Exp.Substring(startIndex, i - startIndex).ToUpper();
            //        return i;
            //    }
            //    i = alphaNumDot;
            //}
        }

        private int GetIdenQuat(int i, out string Name)
        {
            int startIndex = i;
            Name = "";
            if (this.GetWord(i) == -1)
            {
                return -1;
            }
            while (true)
            {
                i = this.GetWhiteSpace(i);
                int word = this.GetWord(i);
                if (word == -1)
                {
                    Name = this.m_Exp.Substring(startIndex, i - startIndex).ToUpper();
                    return i;
                }
                i = word;
            }
        }

        private int GetInteger(int i)
        {
            int num = this.GetNum(i);
            if (num == -1)
            {
                return -1;
            }
            i = num;
            while (true)
            {
                num = this.GetNum(i);
                if (num == -1)
                {
                    return i;
                }
                i = num;
            }
        }

        private int GetItem(int i)
        {
            double num3;
            ProgramEntry entry;
            EData data;
            int index = i;
            SubExpressionType infix = SubExpressionType.Infix;
            bool negItem = false;
            i = this.GetWhiteSpace(i);
            index = i;
            int neg = this.GetNeg(i);
            if (neg != i)
            {
                negItem = true;
            }
            i = neg;
            neg = this.GetDouble(i, out num3);
            if (neg != -1)
            {
                entry = new ProgramEntry();
                entry.Type = ProgramEntryType.Data;
                data = new EData();
                data.Type = DataType.Float;
                data.Value = num3;
                entry.Entry = data;
                this.m_evaluator.AddItem(entry);
                if (negItem)
                {
                    this.PushFunction("NEG");
                }
            }
            else
            {
                string str;
                neg = this.GetString(i, out str);
                if (neg != -1)
                {
                    entry = new ProgramEntry();
                    entry.Type = ProgramEntryType.Data;
                    data = new EData();
                    data.Type = DataType.Text;
                    data.Value = str;
                    entry.Entry = data;
                    this.m_evaluator.AddItem(entry);
                    if (negItem)
                    {
                        this.PushFunction("NEG");
                    }
                    infix = SubExpressionType.Text;
                }
                else
                {
                    DateTime time;
                    neg = this.GetDateTime(i, out time);
                    if (neg != -1)
                    {
                        entry = new ProgramEntry();
                        entry.Type = ProgramEntryType.Data;
                        data = new EData();
                        data.Type = DataType.DateTime;
                        data.Value = time;
                        entry.Entry = data;
                        this.m_evaluator.AddItem(entry);
                        if (negItem)
                        {
                            this.PushFunction("NEG");
                        }
                    }
                    else
                    {
                        Vector2D vectord2;
                        neg = this.GetVector2D(i, out vectord2);
                        if (neg != -1)
                        {
                            entry = new ProgramEntry();
                            entry.Type = ProgramEntryType.Data;
                            data = new EData();
                            data.Type = DataType.Vector2D;
                            data.Value = vectord2;
                            entry.Entry = data;
                            this.m_evaluator.AddItem(entry);
                        }
                        else
                        {
                            Vector1D vectord;
                            neg = this.GetVector1D(i, out vectord);
                            if (neg != -1)
                            {
                                entry = new ProgramEntry();
                                entry.Type = ProgramEntryType.Data;
                                data = new EData();
                                data.Type = DataType.Vector1D;
                                data.Value = vectord;
                                entry.Entry = data;
                                this.m_evaluator.AddItem(entry);
                            }
                            else
                            {
                                neg = this.GetList(i);
                                if (neg != -1)
                                {
                                    infix = SubExpressionType.List;
                                }
                                else
                                {
                                    neg = this.GetFuncCall(i);
                                    if (neg != -1)
                                    {
                                        if (negItem)
                                        {
                                            this.PushFunction("NEG");
                                        }
                                        infix = SubExpressionType.FunctionCall;
                                    }
                                    else
                                    {
                                        URLIden iden=new URLIden();
                                        iden.HName = new string[1];
                                        neg = this.GetIden(i, out iden.HName[0]);
                                        if (neg != -1)
                                        {
                                            entry = this.PushVariable(negItem, ref iden);
                                        }
                                        else
                                        {
                                            neg = this.GetPrentExp(i);
                                            if (neg != -1)
                                            {
                                                if (negItem)
                                                {
                                                    this.PushFunction("NEG");
                                                }
                                            }
                                            else
                                            {
                                                return -1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.AddSubExpression(new SubExpression(infix, index, neg - index));
            if (i != index)
            {
                this.AddSubExpression(new SubExpression(SubExpressionType.FunctionCall, i, neg - i));
            }
            return neg;
        }

        private int GetList(int i)
        {
            int n = 0;
            int openBraceCurly = this.GetOpenBraceCurly(i);
            if (openBraceCurly == -1)
            {
                return -1;
            }
            i = openBraceCurly;
            openBraceCurly = this.GetExp(i);
            if (openBraceCurly == -1)
            {
                ProgramEntry entry;
                openBraceCurly = this.GetCloseBraceCurly(i);
                if (openBraceCurly == -1)
                {
                    return -1;
                }
                entry.Type = ProgramEntryType.Data;
                entry.Entry = new EData(DataType.ListData, ListData.EmptyList);
                this.m_evaluator.AddItem(entry);
                return openBraceCurly;
            }
            n++;
            i = openBraceCurly;
            while (true)
            {
                openBraceCurly = this.GetComma(i);
                if (openBraceCurly == -1)
                {
                    break;
                }
                i = openBraceCurly;
                openBraceCurly = this.GetExp(i);
                if (openBraceCurly == -1)
                {
                    return -1;
                }
                i = openBraceCurly;
                n++;
            }
            openBraceCurly = this.GetCloseBraceCurly(i);
            if (openBraceCurly == -1)
            {
                this.m_Error = "Closing brace expected";
                return -1;
            }
            LISTF f = new LISTF();
            f.SetParCount(n);
            this.PushFunction(f);
            return openBraceCurly;
        }

        private int getObjectExp(int i)
        {
            int i2 = this.GetExp(i);
            if (i2 == -1)
                return -1;
            i=i2;
            i2 = this.GetColon(i);
            if (i2 == -1)
                return -1;
            i = i2;
            i2 = this.GetExp(i);
            if(i2==-1)
            {
                this.m_Error = "Object value expected";
                return -1;
            }
            return i2;
        }
        private int GetObject(int i)
        {
            int n = 0;
            int i2 = this.GetOpenBraceCurly(i);
            if (i2 == -1)
            {
                return -1;
            }
            i = i2;
            i2 = this.getObjectExp(i);
            if (i2 == -1)
                return -1;
            n++;
            i = i2;
            while (true)
            {
                i2 = this.GetComma(i);
                if (i2 == -1)
                {
                    break;
                }
                i = i2;
                i2 = this.getObjectExp(i);
                if (i2 == -1)
                {
                    return -1;
                }
                i = i2;
                n++;
            }
            i2 = this.GetCloseBraceCurly(i);
            if (i2 == -1)
            {
                this.m_Error = "Closing brace expected";
                return -1;
            }
            LISTF f = new LISTF();
            f.SetParCount(n);
            this.PushFunction(f);
            return i2;
        }
        private int GetMinus(int i)
        {
            if (this.m_Exp[i] == '-')
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetNeg(int i)
        {
            if (this.m_Exp[i] == '-')
            {
                i++;
            }
            return i;
        }

        private int GetNegExp(int i)
        {
            int neg = this.GetNeg(i);
            i = this.GetWhiteSpace(i);
            if (neg == -1)
            {
                return -1;
            }
            i = neg;
            neg = this.GetExp(i);
            if (neg == -1)
            {
                return -1;
            }
            i = neg;
            ProgramEntry pe = new ProgramEntry();
            pe.Type = ProgramEntryType.Function;
            pe.Entry = this.m_ISymbolProvider.GetFunction("NEG");
            this.m_evaluator.AddItem(pe);
            return i;
        }

        private int GetNonHashChar(int i)
        {
            if (this.m_Exp[i] == '#')
            {
                return -1;
            }
            return (i + 1);
        }

        private int GetNonQuat(int i)
        {
            if (this.m_Exp[i] == '"')
            {
                return -1;
            }
            return (i + 1);
        }

        private int GetNum(int i)
        {
            if (IsNumeric(this.m_Exp[i]))
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetOp(int i, out IInfixFunction op)
        {
            string str;
            int specialInfix = this.GetSpecialInfix(i, out str);
            if (specialInfix != -1)
            {
                op = (IInfixFunction) this.m_ISymbolProvider.GetFunction(str);
                return specialInfix;
            }
            i = this.GetWhiteSpace(i);
            specialInfix = this.GetIden(i, out str);
            op = null;
            if (specialInfix == -1)
            {
                return -1;
            }
            IFunction function = this.m_ISymbolProvider.GetFunction(str);
            if (function == null)
            {
                return -1;
            }
            if (!(function is IInfixFunction))
            {
                return -1;
            }
            op = (IInfixFunction) function;
            return specialInfix;
        }

        private int GetOpenBrace(int i)
        {
            if (this.m_Exp[i] == '(')
            {
                return (i + 1);
            }
            return -1;
        }

        public int GetOpenBraceCurly(int i)
        {
            if (this.m_Exp[i] == '{')
            {
                return (i + 1);
            }
            return -1;
        }

        public int GetOpenBraceSqr(int i)
        {
            if (this.m_Exp[i] == '[')
            {
                return (i + 1);
            }
            return -1;
        }

        private int GetParList(int i, out int ParCount)
        {
            ParCount = 0;
            int openBrace = this.GetOpenBrace(i);
            if (openBrace == -1)
            {
                return -1;
            }
            i = openBrace;
        Label_001E:
            if (ParCount > 0)
            {
                openBrace = this.GetComma(i);
                if (openBrace == -1)
                {
                    goto Label_007A;
                }
                i = openBrace;
            }
            openBrace = this.GetExp(i);
            if (openBrace == -1)
            {
                if (ParCount > 0)
                {
                    return -1;
                }
            }
            else
            {
                i = openBrace;
                ParCount++;
                goto Label_001E;
            }
        Label_007A:
            openBrace = this.GetCloseBrace(i);
            if (openBrace == -1)
            {
                return -1;
            }
            return openBrace;
        }

        private int GetPrentExp(int i)
        {
            int openBrace = this.GetOpenBrace(i);
            if (openBrace == -1)
            {
                return -1;
            }
            i = openBrace;
            openBrace = this.GetExp(i);
            if (openBrace == -1)
            {
                return -1;
            }
            i = openBrace;
            openBrace = this.GetCloseBrace(i);
            if (openBrace == -1)
            {
                return -1;
            }
            return openBrace;
        }

        private int GetQuot(int i)
        {
            if (this.m_Exp[i] == '"')
            {
                return (i + 1);
            }
            return -1;
        }

        public string GetSimpleVarName(int index)
        {
            URLIden iden = (URLIden) this.m_Vars[index];
            return iden.Element;
        }

        public int GetSpace(int i)
        {
            int num = i;
            while (((this.m_Exp[i] == ' ') || (this.m_Exp[i] == '\r')) || (this.m_Exp[i] == '\t'))
            {
                i++;
            }
            return ((i != num) ? i : -1);
        }

        private int GetSpecialInfix(int i, out string symb)
        {
            if (this.m_Exp[i] == '+')
            {
                symb = "+";
                return (i + 1);
            }
            if (this.m_Exp[i] == '-')
            {
                symb = "-";
                return (i + 1);
            }
            if (this.m_Exp[i] == '*')
            {
                symb = "*";
                return (i + 1);
            }
            if (this.m_Exp[i] == '/')
            {
                symb = "/";
                return (i + 1);
            }
            if (this.m_Exp[i] == '^')
            {
                symb = "^";
                return (i + 1);
            }
            if (this.m_Exp[i] == '=')
            {
                symb = "=";
                return (i + 1);
            }
            if (this.m_Exp[i] == '<')
            {
                if (this.m_Exp[i + 1] == '=')
                {
                    symb = "<=";
                    return (i + 2);
                }
                if (this.m_Exp[i + 1] == '>')
                {
                    symb = "<>";
                    return (i + 2);
                }
                symb = "<";
                return (i + 1);
            }
            if (this.m_Exp[i] == '>')
            {
                if (this.m_Exp[i + 1] != '=')
                {
                    symb = ">";
                    return (i + 1);
                }
                symb = ">=";
                return (i + 2);
            }
            if(this.m_Exp[i]=='.')
            {
                symb = ".";
                return i + 1;
            }
            symb = "";
            return -1;
        }

        private int GetString(int i, out string strText)
        {
            bool flag;
            strText = "";
            int num = i;
            int quot = this.GetQuot(i);
            if (quot == -1)
            {
                return -1;
            }
            i = quot;
            strText = "";
        Label_00C1:
            flag = true;
            quot = this.GetDoubleQuot(i);
            if (quot != -1)
            {
                i = quot;
                strText = strText + "\"";
            }
            quot = this.GetTextString(i);
            if ((quot == -1) || (quot == i))
            {
                if (quot == i)
                {
                    quot = this.GetDoubleQuot(i);
                    if (quot != -1)
                    {
                        i = quot;
                        strText = strText + "\"";
                        goto Label_00C1;
                    }
                }
                quot = this.GetQuot(i);
                if (quot == -1)
                {
                    return -1;
                }
                i = quot;
                return i;
            }
            strText = strText + this.m_Exp.Substring(i, quot - i);
            i = quot;
            goto Label_00C1;
        }

        public string GetSubExpression(int index)
        {
            int[] numArray = new int[this.m_Structure.Count];
            ArrayList list = new ArrayList();
            int num3 = -1;
            int num4 = -1;
            for (int i = 0; i < this.m_Structure.Count; i++)
            {
                numArray[i] = 0;
                SubExpression expression = (SubExpression) this.m_Structure[i];
                if (expression.IsSub(index))
                {
                    for (int j = 0; j < this.m_Structure.Count; j++)
                    {
                        SubExpression expression2 = (SubExpression) this.m_Structure[j];
                        if ((i != j) && expression2.IsSub(expression.Index))
                        {
                            numArray[i]++;
                        }
                    }
                    if (numArray[i] > num3)
                    {
                        num3 = numArray[i];
                        num4 = i;
                    }
                }
            }
            if (num4 == -1)
            {
                return "";
            }
            return this.m_OrgExp.Substring(((SubExpression) this.m_Structure[num4]).Index, ((SubExpression) this.m_Structure[num4]).Length);
        }

        public string[] GetSubExpressions(int level)
        {
            int[] numArray = new int[this.m_Structure.Count];
            ArrayList list = new ArrayList();
            for (int i = 0; i < this.m_Structure.Count; i++)
            {
                numArray[i] = 0;
                SubExpression expression = (SubExpression) this.m_Structure[i];
                for (int j = 0; j < this.m_Structure.Count; j++)
                {
                    SubExpression expression2 = (SubExpression) this.m_Structure[j];
                    if ((i != j) && expression2.IsSub(expression.Index, expression.Length))
                    {
                        numArray[i]++;
                    }
                }
                if (numArray[i] == level)
                {
                    list.Add(this.m_OrgExp.Substring(expression.Index, expression.Length));
                }
            }
            string[] array = new string[list.Count];
            list.CopyTo(array);
            return array;
        }

        public SubExpression GetSubExpressionSpan(int index)
        {
            int[] numArray = new int[this.m_Structure.Count];
            ArrayList list = new ArrayList();
            int num3 = -1;
            int num4 = -1;
            for (int i = 0; i < this.m_Structure.Count; i++)
            {
                numArray[i] = 0;
                SubExpression expression = (SubExpression) this.m_Structure[i];
                if (expression.IsSub(index))
                {
                    for (int j = 0; j < this.m_Structure.Count; j++)
                    {
                        SubExpression expression2 = (SubExpression) this.m_Structure[j];
                        if ((i != j) && expression2.IsSub(expression.Index))
                        {
                            numArray[i]++;
                        }
                    }
                    if (numArray[i] > num3)
                    {
                        num3 = numArray[i];
                        num4 = i;
                    }
                }
            }
            if (num4 == -1)
            {
                return new SubExpression(SubExpressionType.Infix, 0, -1);
            }
            return (SubExpression) this.m_Structure[num4];
        }

        private int GetTextDateTime(int i)
        {
            while (true)
            {
                int nonHashChar = this.GetNonHashChar(i);
                if (nonHashChar == -1)
                {
                    return i;
                }
                i = nonHashChar;
            }
        }

        private int GetTextString(int i)
        {
            while (true)
            {
                int nonQuat = this.GetNonQuat(i);
                if (nonQuat == -1)
                {
                    return i;
                }
                i = nonQuat;
            }
        }

        private int GetURLHName(int i, out URLIden iden)
        {
            int hirarchicalIden;
            iden = new URLIden();
            if (this.m_Exp[i] == '\'')
            {
                i++;
                hirarchicalIden = this.GetHirarchicalIdenQuat(i, out iden.HName);
                if (hirarchicalIden != -1)
                {
                    i = hirarchicalIden;
                    if (this.m_Exp[i] == '\'')
                    {
                        return (i + 1);
                    }
                }
                return -1;
            }
            hirarchicalIden = this.GetHirarchicalIden(i, out iden.HName);
            if (hirarchicalIden == -1)
            {
                return -1;
            }
            i = hirarchicalIden;
            return i;
        }

        public URLIden GetURLIden(int index)
        {
            return (URLIden) this.m_Vars[index];
        }

        private int GetURLIdenProt(int i, out URLIden iden)
        {
            int idenQuat;
            string str;
            string str2;
            iden = new URLIden();
            if (this.m_Exp[i] != '\'')
            {
                idenQuat = this.GetIden(i, out str);
                if (idenQuat != -1)
                {
                    i = idenQuat;
                    if (this.m_Exp[i] == ':')
                    {
                        i++;
                        if (this.m_Exp[i] == '/')
                        {
                            i++;
                        }
                        else
                        {
                            return -1;
                        }
                        if (this.m_Exp[i] == '/')
                        {
                            i++;
                        }
                        else
                        {
                            return -1;
                        }
                        iden.Protocol = str;
                        idenQuat = this.GetIdenDNS(i, out str);
                        if (idenQuat != -1)
                        {
                            iden.Server = str;
                            i = idenQuat;
                            if (this.m_Exp[i] == '/')
                            {
                                i++;
                                idenQuat = this.GetIden(i, out str);
                                if (idenQuat == -1)
                                {
                                    return -1;
                                }
                                i = idenQuat;
                                str2 = str.ToUpper();
                                if (str2 == null)
                                {
                                    goto Label_0359;
                                }
                                if (!(str2 == "DS"))
                                {
                                    if (str2 == "FS")
                                    {
                                        iden.Space = SpaceType.FS;
                                        goto Label_0373;
                                    }
                                    if (str2 == "DE")
                                    {
                                        iden.Space = SpaceType.DE;
                                        goto Label_0373;
                                    }
                                    goto Label_0359;
                                }
                                iden.Space = SpaceType.DS;
                                goto Label_0373;
                            }
                        }
                        return -1;
                    }
                }
                return -1;
            }
            i++;
            idenQuat = this.GetIdenQuat(i, out str);
            if (idenQuat != -1)
            {
                i = idenQuat;
                if (this.m_Exp[i] == ':')
                {
                    i++;
                    if (this.m_Exp[i] == '/')
                    {
                        i++;
                    }
                    else
                    {
                        return -1;
                    }
                    if (this.m_Exp[i] == '/')
                    {
                        i++;
                    }
                    else
                    {
                        return -1;
                    }
                    iden.Protocol = str;
                    idenQuat = this.GetIdenDNS(i, out str);
                    if (idenQuat != -1)
                    {
                        iden.Server = str;
                        i = idenQuat;
                        if (this.m_Exp[i] == '/')
                        {
                            i++;
                            idenQuat = this.GetIdenQuat(i, out str);
                            if (idenQuat == -1)
                            {
                                return -1;
                            }
                            i = idenQuat;
                            str2 = str.ToUpper();
                            if (str2 == null)
                            {
                                goto Label_017A;
                            }
                            if (!(str2 == "DS"))
                            {
                                if (str2 == "FS")
                                {
                                    iden.Space = SpaceType.FS;
                                    goto Label_0197;
                                }
                                if (str2 == "DE")
                                {
                                    iden.Space = SpaceType.DE;
                                    goto Label_0197;
                                }
                                goto Label_017A;
                            }
                            iden.Space = SpaceType.DS;
                            goto Label_0197;
                        }
                    }
                    return -1;
                }
            }
            return -1;
        Label_017A:
            this.m_Error = "Unknown space type-'" + str + ". Only DS,FS or DE are allowed.";
            return -1;
        Label_0197:
            if (this.m_Exp[i] == '/')
            {
                i++;
            }
            else
            {
                return -1;
            }
            idenQuat = this.GetHirarchicalIdenQuat(i, out iden.HName);
            if (idenQuat != -1)
            {
                i = idenQuat;
                if (this.m_Exp[i] == '\'')
                {
                    return (i + 1);
                }
            }
            return -1;
        Label_0359:
            this.m_Error = "Unknown space type-'" + str + ". Only DS,FS or DE are allowed.";
            return -1;
        Label_0373:
            if (this.m_Exp[i] == '/')
            {
                i++;
            }
            else
            {
                return -1;
            }
            idenQuat = this.GetHirarchicalIden(i, out iden.HName);
            if (idenQuat == -1)
            {
                return -1;
            }
            i = idenQuat;
            return i;
        }

        private int GetURLItem(int i, out URLIden iden)
        {
            int uRLIdenProt = this.GetURLIdenProt(i, out iden);
            if (uRLIdenProt != -1)
            {
                return uRLIdenProt;
            }
            uRLIdenProt = this.GetURLServer(i, out iden);
            if (uRLIdenProt != -1)
            {
                return uRLIdenProt;
            }
            uRLIdenProt = this.GetURLSpace(i, out iden);
            if (uRLIdenProt != -1)
            {
                return uRLIdenProt;
            }
            uRLIdenProt = this.GetURLHName(i, out iden);
            if (uRLIdenProt != -1)
            {
                return uRLIdenProt;
            }
            return -1;
        }

        private int GetURLServer(int i, out URLIden iden)
        {
            int idenDNS;
            string str;
            string str2;
            iden = new URLIden();
            if (this.m_Exp[i] != '\'')
            {
                idenDNS = this.GetIdenDNS(i, out str);
                if (idenDNS != -1)
                {
                    iden.Server = str;
                    i = idenDNS;
                    if (this.m_Exp[i] == '/')
                    {
                        i++;
                        idenDNS = this.GetIden(i, out str);
                        if (idenDNS == -1)
                        {
                            return -1;
                        }
                        i = idenDNS;
                        str2 = str.ToUpper();
                        if (str2 == null)
                        {
                            goto Label_022F;
                        }
                        if (!(str2 == "DS"))
                        {
                            if (str2 == "FS")
                            {
                                iden.Space = SpaceType.FS;
                                goto Label_0249;
                            }
                            if (str2 == "DE")
                            {
                                iden.Space = SpaceType.DE;
                                goto Label_0249;
                            }
                            goto Label_022F;
                        }
                        iden.Space = SpaceType.DS;
                        goto Label_0249;
                    }
                }
                return -1;
            }
            i++;
            idenDNS = this.GetIdenDNS(i, out str);
            if (idenDNS != -1)
            {
                iden.Server = str;
                i = idenDNS;
                if (this.m_Exp[i] == '/')
                {
                    i++;
                    idenDNS = this.GetIdenQuat(i, out str);
                    if (idenDNS == -1)
                    {
                        return -1;
                    }
                    i = idenDNS;
                    str2 = str.ToUpper();
                    if (str2 == null)
                    {
                        goto Label_00E5;
                    }
                    if (!(str2 == "DS"))
                    {
                        if (str2 == "FS")
                        {
                            iden.Space = SpaceType.FS;
                            goto Label_0102;
                        }
                        if (str2 == "DE")
                        {
                            iden.Space = SpaceType.DE;
                            goto Label_0102;
                        }
                        goto Label_00E5;
                    }
                    iden.Space = SpaceType.DS;
                    goto Label_0102;
                }
            }
            return -1;
        Label_00E5:
            this.m_Error = "Unknown space type-'" + str + ". Only DS,FS or DE are allowed.";
            return -1;
        Label_0102:
            if (this.m_Exp[i] == '/')
            {
                i++;
            }
            else
            {
                return -1;
            }
            idenDNS = this.GetHirarchicalIdenQuat(i, out iden.HName);
            if (idenDNS != -1)
            {
                i = idenDNS;
                if (this.m_Exp[i] == '\'')
                {
                    return (i + 1);
                }
            }
            return -1;
        Label_022F:
            this.m_Error = "Unknown space type-'" + str + ". Only DS,FS or DE are allowed.";
            return -1;
        Label_0249:
            if (this.m_Exp[i] == '/')
            {
                i++;
            }
            else
            {
                return -1;
            }
            idenDNS = this.GetHirarchicalIden(i, out iden.HName);
            if (idenDNS == -1)
            {
                return -1;
            }
            i = idenDNS;
            return i;
        }

        private int GetURLSpace(int i, out URLIden iden)
        {
            int idenQuat;
            string str;
            string str2;
            iden = new URLIden();
            if (this.m_Exp[i] != '\'')
            {
                idenQuat = this.GetIden(i, out str);
                if (idenQuat == -1)
                {
                    return -1;
                }
                i = idenQuat;
                str2 = str.ToUpper();
                if (str2 == null)
                {
                    goto Label_0199;
                }
                if (!(str2 == "DS"))
                {
                    if (str2 == "FS")
                    {
                        iden.Space = SpaceType.FS;
                        goto Label_01B3;
                    }
                    if (str2 == "DE")
                    {
                        iden.Space = SpaceType.DE;
                        goto Label_01B3;
                    }
                    goto Label_0199;
                }
                iden.Space = SpaceType.DS;
                goto Label_01B3;
            }
            i++;
            idenQuat = this.GetIdenQuat(i, out str);
            if (idenQuat != -1)
            {
                i = idenQuat;
                str2 = str.ToUpper();
                if (str2 != null)
                {
                    if (!(str2 == "DS"))
                    {
                        if (str2 == "FS")
                        {
                            iden.Space = SpaceType.FS;
                            goto Label_00B7;
                        }
                        if (str2 == "DE")
                        {
                            iden.Space = SpaceType.DE;
                            goto Label_00B7;
                        }
                    }
                    else
                    {
                        iden.Space = SpaceType.DS;
                        goto Label_00B7;
                    }
                }
                this.m_Error = "Unknown space type-'" + str + ". Only DS,FS or DE are allowed.";
            }
            return -1;
        Label_00B7:
            if (this.m_Exp[i] == '/')
            {
                i++;
            }
            else
            {
                return -1;
            }
            idenQuat = this.GetHirarchicalIdenQuat(i, out iden.HName);
            if (idenQuat != -1)
            {
                i = idenQuat;
                if (this.m_Exp[i] == '\'')
                {
                    return (i + 1);
                }
            }
            return -1;
        Label_0199:
            this.m_Error = "Unknown space type-'" + str + ". Only DS,FS or DE are allowed.";
            return -1;
        Label_01B3:
            if (this.m_Exp[i] == '/')
            {
                i++;
            }
            else
            {
                return -1;
            }
            idenQuat = this.GetHirarchicalIden(i, out iden.HName);
            if (idenQuat == -1)
            {
                return -1;
            }
            i = idenQuat;
            return i;
        }

        public int GetVector1D(int i, out Vector1D vector1D)
        {
            int n = 0;
            double d = 0.0;
            int num3 = 0;
            Vector1D vectord = new Vector1D(1);
            vector1D = vectord;
            ArrayList list = new ArrayList();
            i = this.GetOpenBraceSqr(i);
            if (i == -1)
            {
                return -1;
            }
            while (true)
            {
                i = this.GetWhiteSpace(i);
                num3 = this.GetDouble(i, out d);
                if (num3 == -1)
                {
                    i = this.GetWhiteSpace(i);
                    i = this.GetCloseBraceSqr(i);
                    if (i == -1)
                    {
                        return -1;
                    }
                    vectord = new Vector1D(n);
                    vector1D = new Vector1D(n);
                    for (int j = 0; j < n; j++)
                    {
                        vectord[j] = (double) list[j];
                    }
                    vector1D = vectord;
                    return i;
                }
                i = num3;
                list.Add(d);
                n++;
            }
        }

        public int GetVector2D(int i, out Vector2D vector2D)
        {
            int r = 0;
            int c = 0;
            int num3 = 0;
            vector2D = null;
            ArrayList list = new ArrayList();
            i = this.GetOpenBraceSqr(i);
            if (i == -1)
            {
                return -1;
            }
            i = this.GetWhiteSpace(i);
            c = -1;
            while (true)
            {
                Vector1D vectord;
                num3 = this.GetVector1D(i, out vectord);
                if (num3 == -1)
                {
                    i = this.GetCloseBraceSqr(i);
                    if (i == -1)
                    {
                        return -1;
                    }
                    Vector2D vectord2 = new Vector2D(r, c);
                    for (int j = 0; j < r; j++)
                    {
                        for (int k = 0; k < c; k++)
                        {
                            vectord2[j, k] = ((Vector1D) list[j])[k];
                        }
                    }
                    vector2D = vectord2;
                    return i;
                }
                if (c == -1)
                {
                    c = vectord.dimension;
                }
                else if (c != vectord.dimension)
                {
                    this.m_Error = "All rows should have equal number of elements.";
                    return -1;
                }
                i = num3;
                list.Add(vectord);
                r++;
                i = this.GetWhiteSpace(i);
            }
        }

        private int GetWhiteSpace(int i)
        {
            while ((((this.m_Exp[i] == ' ') || (this.m_Exp[i] == '\r')) || (this.m_Exp[i] == '\t')) || (this.m_Exp[i] == '\n'))
            {
                i++;
            }
            return i;
        }

        private int GetWord(int i)
        {
            int num2 = i;
            if (this.GetAlphaNum(i) == -1)
            {
                return -1;
            }
            while (true)
            {
                int alphaNum = this.GetAlphaNum(i);
                if (alphaNum == -1)
                {
                    return i;
                }
                i = alphaNum;
            }
        }

        public static bool IsAlpha(char c)
        {
            return ((((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'))) || (c == '_'));
        }

        public static bool IsNumeric(char c)
        {
            return ((c >= '0') && (c <= '9'));
        }

        public static bool IsOp(char c)
        {
            return ((((c == '+') || (c == '/')) || ((c == '*') || (c == '^'))) || (c == '-'));
        }

        public static bool IsWhiteSpace(char ch)
        {
            return (((ch == ' ') || (ch == '\r')) || (ch == '\t'));
        }

        private IFunction PopInfixFunction()
        {
            int index = this.m_ParseStack.Count - 1;
            IFunction function = (IFunction) this.m_ParseStack[index];
            this.m_ParseStack.RemoveAt(index);
            return function;
        }

        private void PushFunction(IFunction f)
        {
            ProgramEntry pe = new ProgramEntry();
            pe.Type = ProgramEntryType.Function;
            pe.Entry = f;
            if (f is IFormulaFunction)
            {
                ((IFormulaFunction) f).ISymbolProvider = this.m_ISymbolProvider;
            }
            this.m_evaluator.AddItem(pe);
        }

        private void PushFunction(string FName)
        {
            IFunction function = this.m_ISymbolProvider.GetFunction(FName);
            ProgramEntry pe = new ProgramEntry();
            pe.Type = ProgramEntryType.Function;
            pe.Entry = function;
            if (function is IFormulaFunction)
            {
                ((IFormulaFunction) function).ISymbolProvider = this.m_ISymbolProvider;
            }
            this.m_evaluator.AddItem(pe);
        }

        private void PushInfixFunction(IInfixFunction f, int InitialStackLevel)
        {
            int precidence = 0;
            precidence = f.Precidence;
            int num2 = this.m_ParseStack.Count - 1;
            for (int i = num2; i > InitialStackLevel; i--)
            {
                if (precidence > ((IInfixFunction) this.m_ParseStack[i]).Precidence)
                {
                    break;
                }
                ProgramEntry pe = new ProgramEntry();
                pe.Type = ProgramEntryType.Function;
                pe.Entry = this.PopInfixFunction();
                this.m_evaluator.AddItem(pe);
            }
            this.m_ParseStack.Add(f);
        }

        private ProgramEntry PushVariable(bool negItem, ref URLIden url)
        {
            int num;
            if (this.m_Handles.Contains(url.ToString().ToUpper()))
            {
                num = (int) this.m_Handles[url.ToString().ToUpper()];
            }
            else
            {
                num = this.m_evaluator.DefineVar();
                this.m_Vars.Add((URLIden) url);
                this.AddDependency(url.ToString());
                this.m_Handles.Add(url.ToString().ToUpper(), num);
            }
            ProgramEntry pe = new ProgramEntry();
            pe.Type = ProgramEntryType.Var;
            pe.Entry = num;
            this.m_evaluator.AddItem(pe);
            if (negItem)
            {
                this.PushFunction("NEG");
            }
            return pe;
        }

        public void SetSymbolProvider(ISymbolProvider provider)
        {
            this.m_ISymbolProvider = provider;
        }

        public ProgramEntry TopProgram()
        {
            return this.m_evaluator.ReturnTopProgramEntry();
        }

        public string Expression
        {
            get
            {
                return this.m_OrgExp;
            }
            set
            {
                if (value == null)
                {
                    value = "";
                }
                this.UnKnownFunctionPresent = false;
                this.m_ParseStack = new ArrayList();
                this.m_Handles = new Hashtable();
                this.m_Vars = new ArrayList();
                this.m_Structure.Clear();
                this.m_evaluator = new EvaluationEngine(this.m_stack);
                this.m_OrgExp = value;
                this.m_Exp = value + "$";
                this.Dependencies = new ArrayList();
                try
                {
                    this.m_Error = "";
                    int exp = this.GetExp(0);
                }
                catch (Exception exception)
                {
                    string message = exception.Message;
                }
            }
        }

        public EData this[string Name]
        {
            get
            {
                return this.m_evaluator[(int) this.m_Handles[Name.ToUpper()]];
            }
            set
            {
                this.m_evaluator[(int) this.m_Handles[Name.ToUpper()]] = value;
            }
        }

        public EData this[int i]
        {
            get
            {
                return this.m_evaluator[i];
            }
            set
            {
                this.m_evaluator[i] = value;
            }
        }

        public bool SaveStructure
        {
            get
            {
                return this.m_SaveStructure;
            }
            set
            {
                this.m_SaveStructure = value;
                this.m_Structure = new ArrayList();
            }
        }

        public int VarCount
        {
            get
            {
                return this.m_Vars.Count;
            }
        }
    }
}

