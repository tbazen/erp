using System;
namespace INTAPS.Evaluator
{

    public class TextBuilder
    {
        public static string BuildText(string bt)
        {
            return ("\"" + bt.Replace("\"", "\"\"") + "\"");
        }

        public static string UnBuildText(string ut)
        {
            if (ut.Length < 2)
            {
                return "";
            }
            ut = ut.Substring(1, ut.Length - 2);
            return ut.Replace("\"\"", "\"");
        }
    }
}

