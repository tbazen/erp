using System;
namespace INTAPS.Evaluator
{

    internal enum TokenType
    {
        Num,
        Symbol,
        Op,
        OpenBrace,
        CloseBrace,
        OpenBraceSqr,
        CloseBraceSqr,
        OpenBraceCur,
        CloseBraceCur,
        Comma,
        String,
        Empty,
        Hash
    }
}

