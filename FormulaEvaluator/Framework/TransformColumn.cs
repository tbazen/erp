using System;
using System.Collections;
namespace INTAPS.Evaluator
{

    internal class TransformColumn : IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private string[] m_Depedencies;
        private Symbolic m_Exp;
        private string m_ExpStr = "";
        private INTAPS.Evaluator.ISymbolProvider m_parent;

        public IVarParamCountFunction Clone()
        {
            IFormulaFunction function = new TransformColumn();
            function.ISymbolProvider = this.m_parent;
            return function;
        }

        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.RecordSet)
            {
                error = new FSError("First parameter should be a recordset");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.Text)
            {
                error = new FSError("Second parameter should be an expression text");
                return error.ToEData();
            }
            if (Pars[2].Type != DataType.Float)
            {
                error = new FSError("Third parameter should be a number designating an column.");
                return error.ToEData();
            }
            IRecordset rs = (IRecordset) Pars[0].Value;
            string str = (string) Pars[1].Value;
            if ((this.m_Exp == null) || (this.m_ExpStr != str))
            {
                Symbolic symbolic = new Symbolic();
                symbolic.m_ISymbolProvider = this.m_parent;
                symbolic.Expression = str;
                this.m_Exp = symbolic;
                int varCount = this.m_Exp.VarCount;
                ArrayList list = new ArrayList();
                for (int i = 0; i < varCount; i++)
                {
                    string simpleVarName = this.m_Exp.GetSimpleVarName(i);
                    if (simpleVarName.ToUpper() != "CV")
                    {
                        list.Add(simpleVarName);
                    }
                }
                this.m_Depedencies = new string[list.Count];
                list.CopyTo(this.m_Depedencies);
                this.m_ExpStr = str;
            }
            int index = (int) ((double) Pars[2].Value);
            if ((index < 1) || (index > rs.ColCount))
            {
                error = new FSError("Invalid column index");
                return error.ToEData();
            }
            EData data = new EData();
            data.Type = DataType.RecordSet;
            data.Value = new TransformedRecordSet(rs, index, this.m_Exp, this.m_parent);
            return data;
        }

        public bool SetParCount(int n)
        {
            return (n == 3);
        }

        public string[] Dependencies
        {
            get
            {
                return this.m_Depedencies;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Transforms a column in recordset using an expression", new string[] { "Recordeset", "Expression", "column index" }, new string[] { "a record set", "an expresion", "Column Index" }, new string[] { "Mand", "Mand", "Mand" }, "Composite Data function");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_parent = value;
            }
        }

        public string Name
        {
            get
            {
                return "Transform column";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "TranCol";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

