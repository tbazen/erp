using System;
using System.Reflection;
namespace INTAPS.Evaluator
{

    internal class TransformedRecordSet : IRecordset
    {
        private Symbolic m_exp;
        private int m_index;
        private IRecordset m_Internal;
        private ISymbolProvider m_symb;

        internal TransformedRecordSet(IRecordset rs, int index, Symbolic exp, ISymbolProvider symb)
        {
            this.m_Internal = rs;
            this.m_index = index;
            this.m_exp = exp;
            this.m_symb = symb;
        }

        public void Close()
        {
            this.m_Internal.Close();
        }

        public string ColName(int ColIndex)
        {
            return this.m_Internal.ColName(ColIndex);
        }

        public bool Read()
        {
            return this.m_Internal.Read();
        }

        public bool Requery()
        {
            return this.m_Internal.Requery();
        }

        public int ColCount
        {
            get
            {
                return this.m_Internal.ColCount;
            }
        }

        public int CurrentRow
        {
            get
            {
                return this.m_Internal.CurrentRow;
            }
            set
            {
                this.m_Internal.CurrentRow = value;
            }
        }

        public bool IsClosed
        {
            get
            {
                return this.m_Internal.IsClosed;
            }
        }

        public EData this[string colname]
        {
            get
            {
                int colIndex = 0;
                for (colIndex = 0; colIndex < this.ColCount; colIndex++)
                {
                    if (this.m_Internal.ColName(colIndex).ToUpper() == colname.ToUpper())
                    {
                        return this[colIndex];
                    }
                }
                return EData.Empty;
            }
        }

        public EData this[int ColIndex]
        {
            get
            {
                EData data = this.m_Internal[ColIndex];
                if (ColIndex != (this.m_index - 1))
                {
                    return data;
                }
                int varCount = this.m_exp.VarCount;
                for (int i = 0; i < varCount; i++)
                {
                    string simpleVarName = this.m_exp.GetSimpleVarName(i);
                    if (simpleVarName.ToUpper() != "CV")
                    {
                        int currentRow = this.m_Internal.CurrentRow;
                        this.m_exp[i] = this.m_symb.GetData(simpleVarName);
                        this.m_Internal.CurrentRow = currentRow;
                    }
                    else
                    {
                        this.m_exp[i] = data;
                    }
                }
                return this.m_exp.Evaluate();
            }
        }

        public int RowCount
        {
            get
            {
                return this.m_Internal.RowCount;
            }
        }
    }
}

