using System;
using System.Runtime.InteropServices;
namespace INTAPS.Evaluator
{

    [StructLayout(LayoutKind.Sequential)]
    public struct URLIden
    {
        public string Protocol;
        public string Server;
        public SpaceType Space;
        public string[] HName;
        public URLIden(string url)
        {
            URLIden uRLIden = new Symbolic("'" + url + "'").GetURLIden(0);
            this = uRLIden;
        }

        public URLIden LessElement
        {
            get
            {
                URLIden iden = new URLIden();
                iden.Protocol = this.Protocol;
                iden.Server = this.Server;
                iden.Space = this.Space;
                iden.HName = new string[this.HName.Length - 1];
                Array.Copy(this.HName, 0, iden.HName, 0, iden.HName.Length);
                return iden;
            }
        }
        public string Element
        {
            get
            {
                return this.HName[this.HName.Length - 1];
            }
            set
            {
                this.HName[this.HName.Length - 1] = value;
            }
        }
        public bool DefaultProtocol
        {
            get
            {
                return (this.Protocol == null);
            }
        }
        public bool DefaultServer
        {
            get
            {
                return (this.Server == null);
            }
        }
        public bool DefaultSpace
        {
            get
            {
                return (this.Space == SpaceType.Null);
            }
        }
        public override string ToString()
        {
            string str = "";
            if (this.Protocol != null)
            {
                str = str + this.Protocol + "://";
            }
            if (this.Server != null)
            {
                str = str + this.Server + "/";
            }
            if (this.Space != SpaceType.Null)
            {
                str = str + this.Space.ToString() + "/";
            }
            bool flag = true;
            foreach (string str2 in this.HName)
            {
                if (flag)
                {
                    flag = false;
                }
                else
                {
                    str = str + ".";
                }
                str = str + str2;
            }
            return str;
        }
    }
}

