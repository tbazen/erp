using System;
namespace INTAPS.Evaluator
{

    public class VarParDocumentation : SimpleDocumentation
    {
        private int m_minn;

        public VarParDocumentation(int minn, IVarParamCountFunction f) : base(f)
        {
            this.m_minn = minn;
        }

        public override string[] AddParameters(int n)
        {
            if (n == 0)
            {
                return new string[this.m_minn];
            }
            return null;
        }
    }
}

