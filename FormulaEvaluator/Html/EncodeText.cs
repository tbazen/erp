using INTAPS.Evaluator;
using System;
using System.Web;

namespace INTAPS.Evaluator.HTML
{

    public class EncodeText : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            return new EData(DataType.Text, HttpUtility.HtmlEncode((string) Pars[0].Value));
        }

        public string Name
        {
            get
            {
                return "Encode text to html.";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "EhEncode";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

