using INTAPS.Evaluator;
using System;

namespace INTAPS.Evaluator.HTML
{

    public class HtmlTableCellFunc : TableCellFunc
    {
        public override IVarParamCountFunction Clone()
        {
            HtmlTableCellFunc func = new HtmlTableCellFunc();
            func.m_nPar = base.m_nPar;
            return func;
        }

        protected override string EncodeText(string txt)
        {
            return txt;
        }

        public override string Name
        {
            get
            {
                return "Table cell with html text";
            }
        }

        public override string Symbol
        {
            get
            {
                return "EhTD2";
            }
        }
    }
}

