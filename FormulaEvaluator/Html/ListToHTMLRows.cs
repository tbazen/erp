using INTAPS.Evaluator;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Web;
namespace INTAPS.Evaluator.HTML
{

    public class ListToHTMLRows : IDynamicFormulaFunction, IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        protected EData m_colIndex;
        protected string m_colVar;
        protected int m_npars;
        protected INTAPS.Evaluator.ISymbolProvider m_provider;
        protected EData m_rowIndex;
        protected string m_rowVar;

        public virtual IVarParamCountFunction Clone()
        {
            ListToHTMLRows rows = new ListToHTMLRows();
            rows.m_npars = this.m_npars;
            return rows;
        }

        public bool Defines(string symbol)
        {
            return ((symbol == this.m_colVar) || (symbol == this.m_rowVar));
        }

        public virtual EData Evaluate(EData[] Pars)
        {
            string str3;
            FSError error;
            if (Pars[0].Type != DataType.ListData)
            {
                error = new FSError("First parameter should be list-l2h function");
                return error.ToEData();
            }
            ListData ld = (ListData) Pars[0].Value;
            string rowstyle = null;
            if (Pars.Length > 1)
            {
                if (Pars[1].Type != DataType.Text)
                {
                    error = new FSError("Second parameter should be expression text-l2h function");
                    return error.ToEData();
                }
                rowstyle = (string) Pars[1].Value;
            }
            string cellstyle = null;
            if (Pars.Length > 2)
            {
                if (Pars[2].Type != DataType.Text)
                {
                    error = new FSError("Third parameter should be expression text-l2h function");
                    return error.ToEData();
                }
                cellstyle = (string) Pars[2].Value;
            }
            this.m_rowVar = "rowi";
            if (Pars.Length > 3)
            {
                if (Pars[3].Type != DataType.Text)
                {
                    error = new FSError("Fourth parameter should be variable name text-l2h function");
                    return error.ToEData();
                }
                this.m_rowVar = (string) Pars[3].Value;
            }
            this.m_rowVar = this.m_rowVar.ToUpper();
            this.m_colVar = "coli";
            if (Pars.Length > 4)
            {
                if (Pars[4].Type != DataType.Text)
                {
                    error = new FSError("Fivth parameter should be variable name text-l2h function");
                    return error.ToEData();
                }
                this.m_colVar = (string) Pars[4].Value;
            }
            bool encode = true;
            if (Pars.Length > 5)
            {
                if (Pars[5].Type != DataType.Text)
                {
                    error = new FSError("Sixth parameter should be encode flag-l2h function");
                    return error.ToEData();
                }
                encode = (bool) Pars[5].Value;
            }
            this.m_colVar = this.m_colVar.ToUpper();
            List<string> rows = new List<string>();
            this.GetRows(ld, rowstyle, cellstyle, rows, encode, out str3);
            ListData v = new ListData(rows.Count);
            int index = 0;
            foreach (string str4 in rows)
            {
                v.elements[index] = new EData(DataType.Text, str4);
                index++;
            }
            if (!string.IsNullOrEmpty(str3))
            {
                error = new FSError(str3);
                return error.ToEData();
            }
            return new EData(DataType.ListData, v);
        }

        public EData GetData(string symbol)
        {
            if (symbol == this.m_colVar)
            {
                return this.m_colIndex;
            }
            if (symbol == this.m_rowVar)
            {
                return this.m_rowIndex;
            }
            FSError error = new FSError("Undefined expression symbol-l2d");
            return error.ToEData();
        }

        protected virtual string GetRows(ListData ld, string rowstyle, string cellstyle, List<string> rows, bool encode, out string err)
        {
            Symbolic symbolic = null;
            Symbolic symbolic2 = null;
            if (!string.IsNullOrEmpty(rowstyle))
            {
                symbolic = new Symbolic();
                symbolic.SetSymbolProvider(new FormulaFunctionSymbolProvider(this.m_provider, this));
                symbolic.Expression = rowstyle;
            }
            if (!string.IsNullOrEmpty(cellstyle))
            {
                symbolic2 = new Symbolic();
                symbolic2.SetSymbolProvider(new FormulaFunctionSymbolProvider(this.m_provider, this));
                symbolic2.Expression = cellstyle;
            }
            string str = "";
            int v = 0;
            int num2 = 0;
            foreach (EData data in ld.elements)
            {
                EData data3;
                string item = "";
                if (data.Type != DataType.ListData)
                {
                    err = "The table data list should be a two dimenstional list-l2h function";
                }
                ListData data2 = (ListData) data.Value;
                num2 = 0;
                this.m_rowIndex = new EData(DataType.Int, v);
                string str3 = "";
                if (symbolic != null)
                {
                    data3 = symbolic.Evaluate();
                    if (data3.Type == DataType.Text)
                    {
                        str3 = (string) data3.Value;
                    }
                }
                if (string.IsNullOrEmpty(str3))
                {
                    item = item + "<tr>";
                }
                else
                {
                    item = item + "<tr " + str3 + ">";
                }
                foreach (EData data4 in data2.elements)
                {
                    string str5;
                    this.m_colIndex = new EData(DataType.Int, num2);
                    string str4 = "";
                    if (symbolic2 != null)
                    {
                        data3 = symbolic2.Evaluate();
                        if (data3.Type == DataType.Text)
                        {
                            str4 = (string) data3.Value;
                        }
                    }
                    if (data4.Type == DataType.ListData)
                    {
                        ListData data5 = (ListData) data4.Value;
                        str5 = (data5[0].Value == null) ? "" : data5[0].Value.ToString();
                        if (data5.elements.Length > 1)
                        {
                            str4 = str4 + " colspan=" + data5[1].Value.ToString();
                        }
                        if (data5.elements.Length > 2)
                        {
                            str4 = str4 + " rowspan=" + data5[2].Value.ToString();
                        }
                    }
                    else
                    {
                        str5 = (data4.Value == null) ? "" : data4.Value.ToString();
                    }
                    if (encode)
                    {
                        str5 = HttpUtility.HtmlEncode(str5);
                    }
                    if (string.IsNullOrEmpty(str4))
                    {
                        item = item + "<td>" + str5 + "</td>";
                    }
                    else
                    {
                        string str7 = item;
                        item = str7 + "<td " + str4 + ">" + str5 + "</td>";
                    }
                    num2++;
                }
                item = item + "</tr>";
                if (rows == null)
                {
                    str = str + item;
                }
                else
                {
                    rows.Add(item);
                }
                v++;
            }
            err = null;
            return str;
        }

        public virtual bool SetParCount(int n)
        {
            this.m_npars = n;
            return ((n >= 1) && (n <= 6));
        }

        public string[] Dependencies
        {
            get
            {
                return new string[0];
            }
        }

        public virtual string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Converts a two dimensional list into an html table row list..", new string[] { "List", "Row Style", "Cell Style", "Row Var", "Col Var" }, new string[] { "A list", "expression that evaluates into attribute for the tr elements", "expression that evaluates into attribute for the td elements", "Row Variable Name", "Column Variable Name" }, new string[] { "Mand", "Opt", "Opt", "Opt", "Opt" }, "List processing");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_provider = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return "List to HTML Rows";
            }
        }

        public virtual int ParCount
        {
            get
            {
                return this.m_npars;
            }
        }

        public virtual string Symbol
        {
            get
            {
                return "l2hr";
            }
        }

        public virtual FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

