using INTAPS.Evaluator;
using System;
namespace INTAPS.Evaluator.HTML
{

    public class ListToHTMLTable : ListToHTMLRows, IDynamicFormulaFunction, IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        public override IVarParamCountFunction Clone()
        {
            ListToHTMLTable table = new ListToHTMLTable();
            table.m_npars = base.m_npars;
            return table;
        }

        public override EData Evaluate(EData[] Pars)
        {
            string str4;
            string str5;
            FSError error;
            if (Pars[0].Type != DataType.ListData)
            {
                error = new FSError("First parameter should be list-l2h function");
                return error.ToEData();
            }
            ListData ld = (ListData) Pars[0].Value;
            string rowstyle = null;
            if (Pars.Length > 1)
            {
                if (Pars[1].Type != DataType.Text)
                {
                    error = new FSError("Second parameter should be expression text-l2h function");
                    return error.ToEData();
                }
                rowstyle = (string) Pars[1].Value;
            }
            string cellstyle = null;
            if (Pars.Length > 2)
            {
                if (Pars[2].Type != DataType.Text)
                {
                    error = new FSError("Third parameter should be expression text-l2h function");
                    return error.ToEData();
                }
                cellstyle = (string) Pars[2].Value;
            }
            string str3 = null;
            if (Pars.Length > 3)
            {
                if (Pars[3].Type != DataType.Text)
                {
                    error = new FSError("Fourth parameter should be expression text-l2h function");
                    return error.ToEData();
                }
                str3 = (string) Pars[3].Value;
            }
            base.m_rowVar = "rowi";
            if (Pars.Length > 4)
            {
                if (Pars[4].Type != DataType.Text)
                {
                    error = new FSError("Fivth parameter should be variable name text-l2h function");
                    return error.ToEData();
                }
                base.m_rowVar = (string) Pars[4].Value;
            }
            base.m_rowVar = base.m_rowVar.ToUpper();
            base.m_colVar = "coli";
            if (Pars.Length > 5)
            {
                if (Pars[5].Type != DataType.Text)
                {
                    error = new FSError("Sixth parameter should be variable name text-l2h function");
                    return error.ToEData();
                }
                base.m_colVar = (string) Pars[5].Value;
            }
            bool encode = true;
            if (Pars.Length > 6)
            {
                if (Pars[6].Type != DataType.Bool)
                {
                    error = new FSError("Seventh parameter should be encode flag-l2h function");
                    return error.ToEData();
                }
                encode = (bool) Pars[6].Value;
            }
            base.m_colVar = base.m_colVar.ToUpper();
            if (!string.IsNullOrEmpty(str3))
            {
                str4 = "<table " + str3 + ">";
            }
            else
            {
                str4 = "<table>";
            }
            str4 = str4 + this.GetRows(ld, rowstyle, cellstyle, null, encode, out str5);
            if (!string.IsNullOrEmpty(str5))
            {
                error = new FSError(str5);
                return error.ToEData();
            }
            return new EData(DataType.Text, str4 + "</table>");
        }

        public override bool SetParCount(int n)
        {
            base.m_npars = n;
            return ((n >= 1) && (n <= 7));
        }

        public override string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Converts a two dimensional list into an html table..", new string[] { "List", "Table Style", "Row Style", "Cell Style", "Row Var", "Col Var" }, new string[] { "A list", "attributes for the table element", "expression that evaluates into attribute for the tr elements", "expression that evaluates into attribute for the td elements", "Row Variable Name", "Column Variable Name" }, new string[] { "Mand", "Opt", "Opt", "Opt", "Opt", "Opt" }, "List processing");
            }
        }

        public override string Name
        {
            get
            {
                return "List to HTML";
            }
        }

        public override string Symbol
        {
            get
            {
                return "l2h";
            }
        }
    }
}

