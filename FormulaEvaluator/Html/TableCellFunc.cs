using INTAPS.Evaluator;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
namespace INTAPS.Evaluator.HTML
{

    public class TableCellFunc : IVarParamCountFunction, IFunction
    {
        protected int m_nPar;

        public virtual IVarParamCountFunction Clone()
        {
            TableCellFunc func = new TableCellFunc();
            func.m_nPar = this.m_nPar;
            return func;
        }

        protected virtual string EncodeText(string txt)
        {
            return HttpUtility.HtmlEncode(txt);
        }

        public EData Evaluate(EData[] Pars)
        {
            string str;
            string str2;
            EData data;
            int extraParm = 0;
            int num2 = 1;
            int num3 = 1;
            if (Pars[Pars.Length - 1].Type == DataType.Float)
            {
                num2 = (int) ((double) Pars[Pars.Length - 1].Value);
                if (Pars[Pars.Length - 2].Type == DataType.Float)
                {
                    num3 = (int) ((double) Pars[Pars.Length - 2].Value);
                    extraParm = 2;
                }
                else
                {
                    extraParm = 1;
                }
            }
            else
            {
                extraParm = 0;
            }
            ParseHTMLParameters(Pars, out str, out str2, out data, extraParm);
            StringBuilder builder = new StringBuilder();
            builder.Append("<td");
            if (num2 > 1)
            {
                builder.Append(" colspan=" + num2);
            }
            if (num3 > 1)
            {
                builder.Append(" rowspan=" + num3);
            }
            if (!string.IsNullOrEmpty(str))
            {
                builder.Append(" class='" + str + "'");
            }
            if (!string.IsNullOrEmpty(str2))
            {
                builder.Append(" style='" + str2 + "'");
            }
            builder.Append(">");
            builder.Append(this.EncodeText(data.Value.ToString()));
            builder.Append("</td>");
            return new EData(DataType.Text, builder.ToString());
        }

        public static void ParseHTMLParameters(EData[] Pars, out string css, out string styles, out EData content, int extraParm)
        {
            ListData data;
            if (Pars.Length == 0)
            {
                throw new Exception("Too few parameters.");
            }
            if ((Pars.Length - extraParm) == 1)
            {
                css = null;
                data = null;
                content = Pars[0];
            }
            else if ((Pars.Length - extraParm) == 2)
            {
                if (Pars[0].Type == DataType.Text)
                {
                    css = (string) Pars[0].Value;
                    data = null;
                }
                else
                {
                    if (Pars[0].Type != DataType.ListData)
                    {
                        throw new Exception("Style should be given as list");
                    }
                    css = null;
                    data = (ListData) Pars[0].Value;
                }
                content = Pars[1];
            }
            else
            {
                if (Pars[0].Type != DataType.Text)
                {
                    throw new Exception("CSS should be given as text");
                }
                css = (string) Pars[0].Value;
                if (Pars[1].Type != DataType.ListData)
                {
                    throw new Exception("Style should be given as list");
                }
                data = (ListData) Pars[1].Value;
                content = Pars[2];
            }
            if (data == null)
            {
                styles = null;
            }
            else
            {
                styles = "";
                int num = 0;
                foreach (EData data2 in data.elements)
                {
                    styles = styles + data2.Value.ToString();
                    if ((num % 2) == 0)
                    {
                        styles = styles + ": ";
                    }
                    else
                    {
                        styles = styles + "; ";
                    }
                    num++;
                }
            }
        }

        public bool SetParCount(int n)
        {
            if (n < 1)
            {
                return false;
            }
            this.m_nPar = n;
            return true;
        }

        public virtual string Name
        {
            get
            {
                return "Table Cell";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_nPar;
            }
        }

        public virtual string Symbol
        {
            get
            {
                return "EhTD";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

