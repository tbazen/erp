using INTAPS.Evaluator;
using System;
using System.Text;
namespace INTAPS.Evaluator.HTML
{

    public class TableFunc : IVarParamCountFunction, IFunction
    {
        private int m_nPar;

        public IVarParamCountFunction Clone()
        {
            TableFunc func = new TableFunc();
            func.m_nPar = this.m_nPar;
            return func;
        }

        public EData Evaluate(EData[] Pars)
        {
            string str;
            string str2;
            EData data;
            TableCellFunc.ParseHTMLParameters(Pars, out str, out str2, out data, 0);
            StringBuilder builder = new StringBuilder();
            builder.Append("<table");
            if (!string.IsNullOrEmpty(str))
            {
                builder.Append(" class='" + str + "'");
            }
            if (!string.IsNullOrEmpty(str2))
            {
                builder.Append(" style='" + str2 + "'");
            }
            builder.Append(">");
            foreach (EData data2 in ((ListData) data.Value).elements)
            {
                builder.Append((string) data2.Value);
            }
            builder.Append("</table>");
            return new EData(DataType.Text, builder.ToString());
        }

        public bool SetParCount(int n)
        {
            if (n < 1)
            {
                return false;
            }
            this.m_nPar = n;
            return true;
        }

        public string Name
        {
            get
            {
                return "Table";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_nPar;
            }
        }

        public string Symbol
        {
            get
            {
                return "EhTable";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

