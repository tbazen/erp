using System;
namespace INTAPS.Evaluator
{

    internal class ChangeToList : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new ChangeToList();
        }

        public EData Evaluate(EData[] Pars)
        {
            EData data4;
            int n = 0;
            int index = 0;
            foreach (EData data in Pars)
            {
                if ((data.Type != DataType.Vector1D) && (data.Type != DataType.Vector2D))
                {
                    FSError error = new FSError(2, "INVALID mismatch.");
                    return error.ToEData();
                }
                if (data.Type == DataType.Vector1D)
                {
                    n += ((Vector1D) data.Value).dimension;
                }
                else
                {
                    n += ((Vector2D) data.Value).row * ((Vector2D) data.Value).column;
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            ListData data3 = new ListData(n);
            for (int i = 0; i < Pars.Length; i++)
            {
                int num4;
                if (Pars[i].Type == DataType.Vector1D)
                {
                    num4 = 0;
                    while (num4 < ((Vector1D) Pars[i].Value).dimension)
                    {
                        data2.Value = ((Vector1D) Pars[i].Value)[num4];
                        data3.elements[index] = data2;
                        index++;
                        num4++;
                    }
                }
                else
                {
                    for (num4 = 0; num4 < ((Vector2D) Pars[i].Value).row; num4++)
                    {
                        for (int j = 0; j < ((Vector2D) Pars[i].Value).column; j++)
                        {
                            data2.Value = ((Vector2D) Pars[i].Value)[num4, j];
                            data3.elements[index] = data2;
                            index++;
                        }
                    }
                }
            }
            data4.Type = DataType.ListData;
            data4.Value = data3;
            return data4;
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Converts a vector to List", new string[] { "vector" }, new string[] { "a one dimensional vector" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "CHANGETOLIST";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "ToList";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

