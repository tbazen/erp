using System;

namespace INTAPS.Evaluator
{

    internal class CrossList : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.ListData)
            {
                error = new FSError("First prameter should be list - CrossList function.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.ListData)
            {
                error = new FSError("Second prameter should be list - CrossList function.");
                return error.ToEData();
            }
            ListData data = (ListData) Pars[0].Value;
            ListData data2 = (ListData) Pars[1].Value;
            ListData v = new ListData(data.elements.Length * data2.elements.Length);
            int num = 0;
            for (int i = 0; i < data.elements.Length; i++)
            {
                for (int j = 0; j < data2.elements.Length; j++)
                {
                    ListData data4 = new ListData(2);
                    data4[0] = data[i];
                    data4[1] = data2[j];
                    v[num++] = new EData(DataType.ListData, data4);
                }
            }
            return new EData(DataType.ListData, v);
        }

        public string Name
        {
            get
            {
                return "Crosses two lists.";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "Cross";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

