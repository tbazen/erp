using System;
namespace INTAPS.Evaluator
{

    internal class CrossListToMatrix : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.ListData)
            {
                error = new FSError("First prameter should be list - CrossList function.");
                return error.ToEData();
            }
            if (Pars[1].Type != DataType.ListData)
            {
                error = new FSError("Second prameter should be list - CrossList function.");
                return error.ToEData();
            }
            ListData rowData = (ListData) Pars[0].Value;
            ListData colData = (ListData) Pars[1].Value;
            ListData v = new ListData(rowData.elements.Length);
            for (int i = 0; i < rowData.elements.Length; i++)
            {
                ListData row = new ListData(colData.elements.Length);
                for (int j = 0; j < colData.elements.Length; j++)
                {
                    ListData crossElement = new ListData(2);
                    crossElement[0] = rowData[i];
                    crossElement[1] = colData[j];
                    row[j] = new EData(DataType.ListData, crossElement);
                }
                v[i] = new EData(DataType.ListData, row);
            }
            return new EData(DataType.ListData, v);
        }

        public string Name
        {
            get
            {
                return "Crosses two lists.";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "ToMatrix";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

