using System;
namespace INTAPS.Evaluator
{

    internal class DeleteListElement : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new DeleteListElement();
        }

        public EData Evaluate(EData[] Pars)
        {
            int num4;
            EData data2;
            int num = 0;
            int n = 0;
            int index = 0;
            for (num4 = 0; num4 < Pars.Length; num4++)
            {
                FSError error;
                if (num4 == 0)
                {
                    if (Pars[num4].Type != DataType.ListData)
                    {
                        error = new FSError(2, "INVALID parameter in delete function");
                        return error.ToEData();
                    }
                }
                else
                {
                    if (Pars[num4].Type != DataType.Float)
                    {
                        error = new FSError(2, "INVALID parameter in delete function");
                        return error.ToEData();
                    }
                    num++;
                }
            }
            n = ((ListData) Pars[0].Value).m_NumberElements - num;
            ListData data = new ListData(n);
            for (num4 = 0; num4 < ((ListData) Pars[0].Value).m_NumberElements; num4++)
            {
                if (this.test(num4 + 1, Pars))
                {
                    data.elements[index] = ((ListData) Pars[0].Value)[num4];
                    index++;
                }
            }
            data2.Type = DataType.ListData;
            data2.Value = data;
            return data2;
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return true;
        }

        private bool test(int i, EData[] Pars)
        {
            bool flag = true;
            for (int j = 1; j < Pars.Length; j++)
            {
                if (i == ((double) Pars[j].Value))
                {
                    flag = false;
                }
            }
            return flag;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Deletes an element in a list", new string[] { "List", "Index" }, new string[] { "A list", "index of the element to delete" }, new string[] { "Mand", "Mand" }, "List processing");
            }
        }

        public string Name
        {
            get
            {
                return "DELETE";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "DELETE";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

