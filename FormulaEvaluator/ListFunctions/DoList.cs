using System;
using System.Collections;
namespace INTAPS.Evaluator
{
    internal class DoList : IDynamicFormulaFunction, IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private EData m_cv;
        private string m_cvVar;
        private ArrayList m_Dep = new ArrayList();
        private EData m_iv;
        private string m_ivVar;
        private int m_NPars = 2;
        private INTAPS.Evaluator.ISymbolProvider m_SymbolProvider;

        public IVarParamCountFunction Clone()
        {
            DoList list = new DoList();
            list.ISymbolProvider = this.m_SymbolProvider;
            return list;
        }

        public bool Defines(string symbol)
        {
            return ((symbol == this.m_cvVar) || (symbol == this.m_ivVar));
        }

        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if ((Pars[0].Type != DataType.ListData) || (Pars[1].Type != DataType.Text))
            {
                error = new FSError("Type mismatch - do list function");
                return error.ToEData();
            }
            string str = "cv";
            string str2 = "iv";
            if (Pars.Length > 2)
            {
                if (Pars[2].Type != DataType.Text)
                {
                    error = new FSError("Third parameter should be a variable name - Dolist function");
                    return error.ToEData();
                }
                str = (string) Pars[2].Value;
            }
            if (Pars.Length > 3)
            {
                if (Pars[3].Type != DataType.Text)
                {
                    error = new FSError("Fourth parameter should be a variable name - Dolist function");
                    return error.ToEData();
                }
                str2 = (string) Pars[3].Value;
            }
            str = str.ToUpper();
            str2 = str2.ToUpper();
            this.m_ivVar = str2;
            this.m_cvVar = str;
            ListData data = (ListData) Pars[0].Value;
            Symbolic symbolic = new Symbolic();
            symbolic.m_ISymbolProvider = new FormulaFunctionSymbolProvider(this.m_SymbolProvider, this);
            symbolic.Expression = Pars[1].Value.ToString();
            ListData data2 = new ListData(data.elements.Length);
            int index = 0;
            foreach (EData data3 in data.elements)
            {
                this.m_iv = new EData(DataType.Int, index);
                this.m_cv = data3;
                data2.elements[index] = symbolic.Evaluate();
                index++;
            }
            EData data4 = new EData();
            data4.Type = DataType.ListData;
            data4.Value = data2;
            return data4;
        }

        public EData GetData(string symbol)
        {
            if (symbol == this.m_cvVar)
            {
                return this.m_cv;
            }
            if (symbol == this.m_ivVar)
            {
                return this.m_iv;
            }
            FSError error = new FSError("Undefined symbol for DoList function." + symbol);
            return error.ToEData();
        }

        public bool SetParCount(int n)
        {
            if ((n < 2) || (n > 4))
            {
                return false;
            }
            this.m_NPars = n;
            return true;
        }

        public string[] Dependencies
        {
            get
            {
                string[] array = new string[this.m_Dep.Count];
                this.m_Dep.CopyTo(array);
                return array;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Applies an expression for each element of a list and replaces each element with the result of the expression.\n The variable CV is used to represent the value of the current element.", new string[] { "List", "Expression", "Expression", "Expression" }, new string[] { "A list", "The expression to apply on the list elements", "Current value variable", "Index variable" }, new string[] { "Mand", "Mand", "Opt", "Opt" }, "List processing");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_SymbolProvider = value;
            }
        }

        public string Name
        {
            get
            {
                return "DoList";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_NPars;
            }
        }

        public string Symbol
        {
            get
            {
                return "DoList";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

