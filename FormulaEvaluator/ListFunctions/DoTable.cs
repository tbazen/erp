using System;
using System.Collections;
namespace INTAPS.Evaluator
{
    internal class DoTable : IDynamicFormulaFunction, IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private EData m_cv;
        private string m_cvVar;
        private ArrayList m_Dep = new ArrayList();
        private EData m_rowIndexValue;
        private EData m_colIndexValue;
        private string m_rowiVar;
        private string m_coliVar;
        private int m_NPars = 2;
        private INTAPS.Evaluator.ISymbolProvider m_SymbolProvider;

        public IVarParamCountFunction Clone()
        {
            DoTable list = new DoTable();
            list.ISymbolProvider = this.m_SymbolProvider;
            return list;
        }

        public bool Defines(string symbol)
        {
            return ((symbol == this.m_cvVar) || (symbol == this.m_rowiVar))|| (symbol == this.m_coliVar);
        }

        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if ((Pars[0].Type != DataType.ListData) || (Pars[1].Type != DataType.Text))
            {
                error = new FSError("Type mismatch - do list function");
                return error.ToEData();
            }
            string rowVar = "cv";
            string rowIndexVar = "rowi";
            string colIndexVar = "coli";
            if (Pars.Length > 2)
            {
                if (Pars[2].Type != DataType.Text)
                {
                    error = new FSError("Third parameter should be a variable name - Dolist function");
                    return error.ToEData();
                }
                rowVar = (string) Pars[2].Value;
            }
            if (Pars.Length > 3)
            {
                if (Pars[3].Type != DataType.Text)
                {
                    error = new FSError("Fourth parameter should be a variable name - Dolist function");
                    return error.ToEData();
                }
                rowIndexVar = (string) Pars[3].Value;
            }
            if (Pars.Length > 4)
            {
                if (Pars[4].Type != DataType.Text)
                {
                    error = new FSError("Fivth parameter should be a variable name - Dolist function");
                    return error.ToEData();
                }
                colIndexVar = (string)Pars[4].Value;
            }
            
            rowVar = rowVar.ToUpper();
            rowIndexVar = rowIndexVar.ToUpper();
            colIndexVar = colIndexVar.ToUpper();
            this.m_rowiVar = rowIndexVar;
            this.m_coliVar = colIndexVar;
            this.m_cvVar = rowVar;
            
            ListData data = (ListData) Pars[0].Value;
            Symbolic symbolic = new Symbolic();
            symbolic.m_ISymbolProvider = new FormulaFunctionSymbolProvider(this.m_SymbolProvider, this);
            symbolic.Expression = Pars[1].Value.ToString();
            ListData result = new ListData(data.elements.Length);
            int index = 0;
            foreach (EData rowData in data.elements)
            {
                
                ListData row = rowData.Value as ListData;
                if (row == null)
                    continue;
                ListData resultRow = new ListData(row.elements.Length);
                int colIndex = 0;
                foreach (EData el in row.elements)
                {
                    this.m_rowIndexValue = new EData(DataType.Int, index);
                    this.m_colIndexValue = new EData(DataType.Int, colIndex);
                    this.m_cv = el;
                    resultRow.elements[colIndex] = symbolic.Evaluate();
                    colIndex++;
                }
                result.elements[index] = new EData(DataType.ListData, resultRow);
                index++;
            }
            EData ret = new EData();
            ret.Type = DataType.ListData;
            ret.Value = result;
            return ret;
        }

        public EData GetData(string symbol)
        {
            if (symbol == this.m_cvVar)
            {
                return this.m_cv;
            }
            if (symbol == this.m_rowiVar)
            {
                return this.m_rowIndexValue;
            }
            if (symbol == this.m_coliVar)
            {
                return this.m_colIndexValue;
            }
            FSError error = new FSError("Undefined symbol for DoList function." + symbol);
            return error.ToEData();
        }

        public bool SetParCount(int n)
        {
            if ((n < 2) || (n > 5))
            {
                return false;
            }
            this.m_NPars = n;
            return true;
        }

        public string[] Dependencies
        {
            get
            {
                string[] array = new string[this.m_Dep.Count];
                this.m_Dep.CopyTo(array);
                return array;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Applies an expression for each element of a list and replaces each element with the result of the expression.\n The variable CV is used to represent the value of the current element.", new string[] { "List", "Expression", "Expression", "Expression" }, new string[] { "A list", "The expression to apply on the list elements", "Current value variable", "Row Index variable","Column Index Variabe" }, new string[] { "Mand", "Mand", "Opt", "Opt","Opt" }, "List processing");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_SymbolProvider = value;
            }
        }

        public string Name
        {
            get
            {
                return "Transform Table";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_NPars;
            }
        }

        public string Symbol
        {
            get
            {
                return "DoTable";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

