using System;
using System.Collections;
namespace INTAPS.Evaluator
{
    public class GenerateSeriesList : IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private string[] m_Depedencies;
        private Symbolic m_Exp;
        private string m_ExpStr = "";
        private INTAPS.Evaluator.ISymbolProvider m_FP;
        private int m_ParCount = 4;

        public IVarParamCountFunction Clone()
        {
            GenerateSeriesList list = new GenerateSeriesList();
            list.ISymbolProvider = this.m_FP;
            return list;
        }

        public EData Evaluate(EData[] Pars)
        {
            int num5;
            string simpleVarName;
            EData data3;
            FSError error;
            double num = 1.0;
            if (this.m_ParCount > 4)
            {
                num = (double) Pars[4].Value;
            }
            double num2 = (double) Pars[2].Value;
            int n = (int) ((double) Pars[3].Value);
            string str = Pars[1].Value.ToString();
            string str2 = Pars[0].Value.ToString();
            if ((this.m_Exp == null) || (this.m_ExpStr != str2))
            {
                Symbolic symbolic = new Symbolic();
                symbolic.m_ISymbolProvider = this.m_FP;
                symbolic.Expression = str2;
                this.m_Exp = symbolic;
                ArrayList list = new ArrayList();
                for (num5 = 0; num5 < this.m_Exp.VarCount; num5++)
                {
                    simpleVarName = this.m_Exp.GetSimpleVarName(num5);
                    if (simpleVarName.ToUpper() != str.ToUpper())
                    {
                        list.Add(simpleVarName);
                    }
                }
                this.m_Depedencies = new string[list.Count];
                list.CopyTo(this.m_Depedencies);
                this.m_ExpStr = str2;
            }
            if (num == 0.0)
            {
                error = new FSError(0, "Step can't be 0.");
                return error.ToEData();
            }
            if (n < 0)
            {
                error = new FSError(0, "Size of series can't be negative.");
                return error.ToEData();
            }
            ListData data = new ListData(n);
            for (num5 = 0; num5 < this.m_Exp.VarCount; num5++)
            {
                simpleVarName = this.m_Exp.GetSimpleVarName(num5);
                if (simpleVarName.ToUpper() != str.ToUpper())
                {
                    this.m_Exp[simpleVarName] = this.m_FP.GetData(simpleVarName);
                }
            }
            if (num > 0.0)
            {
                double num4 = num2;
                for (num5 = 0; num5 < n; num5++)
                {
                    EData data2 = new EData();
                    data2.Type = DataType.Float;
                    data2.Value = num4;
                    this.m_Exp[str] = data2;
                    data[num5] = this.m_Exp.Evaluate();
                    num4 += num;
                }
            }
            data3.Type = DataType.ListData;
            data3.Value = data;
            return data3;
        }

        public string GetParName(int i)
        {
            switch (i)
            {
                case 0:
                    return "Expression(text)";

                case 1:
                    return "Counter variable(text)";

                case 2:
                    return "Counter Initial value";

                case 3:
                    return "Count";
            }
            return null;
        }

        public bool SetParCount(int n)
        {
            if (n < 4)
            {
                return false;
            }
            this.m_ParCount = n;
            return true;
        }

        public string[] Dependencies
        {
            get
            {
                return this.m_Depedencies;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Generate series of values and return them in a list", new string[] { "Expression", "Counter", "Initial Value", "Count" }, new string[] { "A text which is used as an expression for generating the series", "A text that is used as a counter variable", "Initial value for the counter variable", "The number of values generated" }, new string[] { "Mand", "Opt", "Opt", "Opt" }, "List processing");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_FP = value;
            }
        }

        public string Name
        {
            get
            {
                return "Generate list Series";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "SeriesL";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

