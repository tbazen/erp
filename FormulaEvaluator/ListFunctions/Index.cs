using System;
namespace INTAPS.Evaluator
{
    internal class Index : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_nPars = 2;

        public IVarParamCountFunction Clone()
        {
            return new Index();
        }

        public EData Evaluate(EData[] Pars)
        {
            int num;
            FSError error;
            if ((Pars[1].Type != DataType.Float) && (Pars[1].Type != DataType.Int))
            {
                error = new FSError("Index: Second parameter should be number. It is now " + Pars[1].Type);
                return error.ToEData();
            }
            if (Pars[1].Value is double)
            {
                num = (int) ((double) Pars[1].Value);
            }
            else
            {
                num = (int) Pars[1].Value;
            }
            switch (Pars[0].Type)
            {
                case DataType.Vector1D:
                {
                    if ((num < 1) || (num > ((Vector1D) Pars[0].Value).VectorElements.Length))
                    {
                        error = new FSError("Index out of bound.");
                        return error.ToEData();
                    }
                    EData data = new EData();
                    data.Value = ((Vector1D) Pars[0].Value).VectorElements[num - 1];
                    data.Type = DataType.Float;
                    return data;
                }
                case DataType.ListData:
                    if ((num < 1) || (num > ((ListData) Pars[0].Value).elements.Length))
                    {
                        error = new FSError("Index out of bound.");
                        return error.ToEData();
                    }
                    return ((ListData) Pars[0].Value).elements[num - 1];
            }
            error = new FSError("Can't index this data type");
            return error.ToEData();
        }

        public bool SetParCount(int n)
        {
            if (n < 2)
            {
                return false;
            }
            this.m_nPars = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Returns an element from a list", new string[] { "List", "Index" }, new string[] { "A list", "Index of the element to return" }, new string[] { "Mand", "Mand" }, "List processing");
            }
        }

        public string Name
        {
            get
            {
                return "Index";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_nPars;
            }
        }

        public string Symbol
        {
            get
            {
                return "Index";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

