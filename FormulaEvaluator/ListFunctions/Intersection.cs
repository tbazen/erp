using System;
using System.Runtime.InteropServices;
namespace INTAPS.Evaluator
{
    internal class Intersection : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_ParCount;

        private int CheckExistenceInAll(EData par, EData[] Pars, out bool[] inall, int total)
        {
            int num2;
            int num = 0;
            inall = new bool[((ListData) par.Value).m_NumberElements];
            for (num2 = 0; num2 < ((ListData) par.Value).m_NumberElements; num2++)
            {
                inall[num2] = false;
            }
            for (num2 = 0; num2 < ((ListData) par.Value).m_NumberElements; num2++)
            {
                if (this.ExistsInAll(((ListData) par.Value)[num2], Pars))
                {
                    inall[num2] = true;
                    num++;
                }
                else
                {
                    inall[num2] = false;
                }
            }
            return num;
        }

        public IVarParamCountFunction Clone()
        {
            return new Intersection();
        }

        public EData Evaluate(EData[] Pars)
        {
            bool[] flagArray;
            bool[] flagArray2;
            EData data3;
            int total = 0;
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.ListData)
                {
                    FSError error = new FSError(2, "INVALID mismatch.");
                    return error.ToEData();
                }
                total += ((ListData) data.Value).m_NumberElements;
            }
            total = this.CheckExistenceInAll(Pars[0], Pars, out flagArray, total);
            ListData data2 = new ListData(this.FilterRepeated(Pars[0], flagArray, out flagArray2));
            int index = 0;
            int num4 = 0;
            for (int i = 0; i < ((ListData) Pars[0].Value).m_NumberElements; i++)
            {
                if (flagArray2[num4])
                {
                    data2.elements[index] = ((ListData) Pars[0].Value)[i];
                    index++;
                }
                num4++;
            }
            data3.Type = DataType.ListData;
            data3.Value = data2;
            return data3;
        }

        private bool ExistsInAll(EData par, EData[] Pars)
        {
            bool flag = false;
            int num = 0;
            for (int i = 1; i < Pars.Length; i++)
            {
                for (int j = 0; j < ((ListData) Pars[i].Value).m_NumberElements; j++)
                {
                    if ((par.Type == ((ListData) Pars[i].Value)[j].Type) && ((ListData) Pars[i].Value)[j].Value.Equals(par.Value))
                    {
                        j = ((ListData) Pars[i].Value).m_NumberElements;
                        flag = true;
                        num = i;
                    }
                    else if ((j == (((ListData) Pars[i].Value).m_NumberElements - 1)) && (num != i))
                    {
                        flag = false;
                        i = Pars.Length - 1;
                        j = ((ListData) Pars[i].Value).m_NumberElements;
                    }
                }
            }
            return flag;
        }

        private int FilterRepeated(EData par, bool[] inall, out bool[] filtered)
        {
            int num2;
            int num = 0;
            filtered = new bool[((ListData) par.Value).m_NumberElements];
            for (num2 = 0; num2 < ((ListData) par.Value).m_NumberElements; num2++)
            {
                filtered[num2] = false;
            }
            for (num2 = 0; num2 < ((ListData) par.Value).m_NumberElements; num2++)
            {
                if (inall[num2])
                {
                    for (int i = 0; i < num2; i++)
                    {
                        EData data = ((ListData) par.Value)[num2];
                        if (data.Equals(((ListData) par.Value)[i]))
                        {
                            i = num2;
                        }
                        else if (i == (num2 - 1))
                        {
                            num++;
                            filtered[num2] = true;
                        }
                    }
                    if (num2 == 0)
                    {
                        filtered[0] = true;
                        num++;
                    }
                }
            }
            return num;
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Evaluates the intersection of lists", new string[] { "List", "List" }, new string[] { "a list", "a list" }, new string[] { "Mand", "Mand" }, "Logic function");
            }
        }

        public string Name
        {
            get
            {
                return "INTERSECTION";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "INTERSECTION";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

