using System;
namespace INTAPS.Evaluator
{
    internal class LISTF : IVarParamCountFunction, IFunction
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new LISTF();
        }

        public EData Evaluate(EData[] Pars)
        {
            EData data2;
            ListData data = ListData.MakeList(Pars);
            data2.Type = DataType.ListData;
            data2.Value = data;
            return data2;
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return true;
        }

        public string Name
        {
            get
            {
                return "LIST";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "LIST";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

