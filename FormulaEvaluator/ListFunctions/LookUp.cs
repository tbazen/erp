using System;
namespace INTAPS.Evaluator
{
    internal class LookUp : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            int num2;
            FSError error;
            if ((Pars[0].Type != DataType.ListData) && (Pars[0].Type != DataType.Vector1D))
            {
                error = new FSError("First parameter should be a list or a one dimentional data.");
                return error.ToEData();
            }
            int num = -1;
            switch (Pars[0].Type)
            {
                case DataType.Vector1D:
                {
                    Vector1D vectord = (Vector1D) Pars[0].Value;
                    num2 = 1;
                    if (Pars[1].Type != DataType.Float)
                    {
                        error = new FSError("Second parameter should be double when first parameter is a vector");
                        return error.ToEData();
                    }
                    double num3 = (double) Pars[1].Value;
                    foreach (double num4 in vectord.VectorElements)
                    {
                        if (num3 == num4)
                        {
                            num = num2;
                            break;
                        }
                        num2++;
                    }
                    break;
                }
                case DataType.ListData:
                {
                    ListData data = (ListData) Pars[0].Value;
                    num2 = 1;
                    EData data2 = Pars[1];
                    foreach (EData data3 in data.elements)
                    {
                        if (data3.Value is string)
                        {
                            if (((string) data3.Value) == ((string) Pars[1].Value))
                            {
                                num = num2;
                                break;
                            }
                        }
                        else if (data3.Value is double)
                        {
                            try
                            {
                                if (((double) data3.Value) == Convert.ToDouble(Pars[1].Value))
                                {
                                    num = num2;
                                    break;
                                }
                            }
                            catch (Exception exception)
                            {
                                throw exception;
                            }
                        }
                        else if ((data3.Value is IComparable) && (((IComparable) data3.Value).CompareTo(Pars[1].Value) == 0))
                        {
                            num = num2;
                            break;
                        }
                        num2++;
                    }
                    break;
                }
            }
            EData data4 = new EData();
            data4.Type = DataType.Float;
            data4.Value = num;
            return data4;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Searchs for the occurence of a value in a list", new string[] { "List", "Value" }, new string[] { "A list", "The value to search" }, new string[] { "Mand", "Mand" }, "List processing");
            }
        }

        public string Name
        {
            get
            {
                return "Lookup";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "lookup";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

