using System;
using System.Collections;
namespace INTAPS.Evaluator
{
    internal class RunList : IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private ArrayList m_Dep = new ArrayList();
        private int m_NPars = 3;
        private INTAPS.Evaluator.ISymbolProvider m_SymbolProvider;

        public IVarParamCountFunction Clone()
        {
            RunList list = new RunList();
            list.ISymbolProvider = this.m_SymbolProvider;
            return list;
        }

        public EData Evaluate(EData[] Pars)
        {
            if ((Pars[0].Type != DataType.ListData) || (Pars[1].Type != DataType.Text))
            {
                FSError error = new FSError("Type mismatch - run list function");
                return error.ToEData();
            }
            ListData data = (ListData) Pars[0].Value;
            Symbolic symbolic = new Symbolic();
            symbolic.m_ISymbolProvider = this.m_SymbolProvider;
            symbolic.Expression = Pars[1].Value.ToString();
            EData data2 = Pars[2];
            bool flag = false;
            bool flag2 = false;
            for (int i = 0; i < symbolic.VarCount; i++)
            {
                string simpleVarName = symbolic.GetSimpleVarName(i);
                if ("IV" == simpleVarName.ToUpper())
                {
                    flag = true;
                }
                else if ("CV" == simpleVarName.ToUpper())
                {
                    flag2 = true;
                }
                else
                {
                    symbolic[i] = this.m_SymbolProvider.GetData(symbolic.GetURLIden(i));
                    this.m_Dep.Add(symbolic.GetURLIden(i).ToString());
                }
            }
            foreach (EData data3 in data.elements)
            {
                if (flag)
                {
                    symbolic["IV"] = data2;
                }
                if (flag2)
                {
                    symbolic["CV"] = data3;
                }
                data2 = symbolic.Evaluate();
            }
            return data2;
        }

        public bool SetParCount(int n)
        {
            if (n != 3)
            {
                return false;
            }
            this.m_NPars = n;
            return true;
        }

        public string[] Dependencies
        {
            get
            {
                string[] array = new string[this.m_Dep.Count];
                this.m_Dep.CopyTo(array);
                return array;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Applies an expression for each element of a list and return a single value.\n The variables cv and IV are used to represent the value of the current element and the last result of the evaluation of the expression respectively.", new string[] { "List", "Expression", "Initial value" }, new string[] { "A list", "The expression to apply on the list elements", "The initial value of IV" }, new string[] { "Mand", "Mand", "Mand" }, "List processing");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_SymbolProvider = value;
            }
        }

        public string Name
        {
            get
            {
                return "RunList";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "RunList";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

