using System;
using System.Collections;
namespace INTAPS.Evaluator
{

    public class SortF : IVarParamCountFunction, IComparer,IFormulaFunction
    {
        Symbolic _compareExp = null;
        string _xVar = "x";
        string _yVar = "y";
        int _nPar = 0;
        protected INTAPS.Evaluator.ISymbolProvider m_provider;
        public IVarParamCountFunction Clone()
        {
            SortF s = new SortF();
            s._compareExp = _compareExp;
            s._nPar = _nPar;
            return s;
        }
        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_provider = value;
            }
        }
        public int Compare(object x, object y)
        {
            if (_compareExp == null)
            {
                if ((((EData)x).Type == DataType.ListData) || (((EData)y).Type == DataType.ListData))
                {
                    EData data = ((ListData)((EData)x).Value).elements[0];
                    EData data2 = ((ListData)((EData)y).Value).elements[0];
                    return data.CompareTo(data2);
                }
                EData data3 = (EData)x;
                return data3.CompareTo((EData)y);
            }
            else
            {
                _compareExp[_xVar] = (EData)x;
                _compareExp[_yVar] = (EData)y;
                EData ret=_compareExp.Evaluate();
                if (ret.Value is int)
                    return (int)ret.Value;
                if (ret.Value is double)
                {
                    if ((double)ret.Value > 0)
                        return 1;
                    if ((double)ret.Value < 0)
                        return -1;
                    return 0;
                }
                return 0;
            }
            
        }

        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.ListData)
            {
                FSError error = new FSError(2, "Type mismatch - sort function.");
                return error.ToEData();
            }
            if (Pars.Length > 1)
            {
                _compareExp = new Symbolic();
                _compareExp.SetSymbolProvider(this.m_provider);
                _compareExp.Expression = Pars[1].ToString();
            }
            ListData data = (ListData) Pars[0].Value;
            ListData data2 = new ListData(data.elements.Length);
            data.elements.CopyTo(data2.elements, 0);
            Array.Sort(data2.elements, 0, data2.elements.Length, this);
            EData data3 = new EData();
            data3.Value = data2;
            data3.Type = DataType.ListData;
            return data3;
        }

        public bool SetParCount(int n)
        {
            if (n == 0)
                return false;
            _nPar=n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Sort values of a list", new string[] { "List" }, new string[] { "a list" }, new string[] { "Mand" }, "Composite data function");
            }
        }

        string IFunction.Name
        {
            get
            {
                return "Sort";
            }
        }

        public int ParCount
        {
            get
            {
                return _nPar;
            }
        }

        public string Symbol
        {
            get
            {
                return "SORT";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }

        public string[] Dependencies
        {
            get
            {
                return new string[0];
            }
        }

    }
}

