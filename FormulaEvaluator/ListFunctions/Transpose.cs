using System;
using System.Collections;
namespace INTAPS.Evaluator
{
    internal class Transpose : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars.Length != 1)
            {
                error = new FSError(100, "Invalid parameter count - transpose function.");
                return error.ToEData();
            }
            if (Pars[0].Type != DataType.ListData)
            {
                error = new FSError(100, "Type mismatch - transpose function.");
                return error.ToEData();
            }
            ListData data = (ListData) Pars[0].Value;
            ListData emptyList = null;
            int length = data.elements.Length;
            int num2 = -1;
            if (data.elements.Length == 0)
            {
                emptyList = ListData.EmptyList;
            }
            else
            {
                int num3;
                ArrayList list = new ArrayList();
                EData[] elements = data.elements;
                for (int i = 0; i < elements.Length; i++)
                {
                    object obj2 = elements[i];
                    if (((EData) obj2).Type != DataType.ListData)
                    {
                        error = new FSError(100, "All elements should be list.");
                        return error.ToEData();
                    }
                    ListData data3 = (ListData) ((EData) obj2).Value;
                    if (num2 == -1)
                    {
                        num2 = data3.elements.Length;
                        num3 = 0;
                        foreach (EData data4 in data3.elements)
                        {
                            list.Add(new ArrayList());
                            ((ArrayList) list[num3++]).Add(data4);
                        }
                    }
                    else
                    {
                        if (num2 != data3.elements.Length)
                        {
                            error = new FSError(100, "All rows should have the same number of elements.");
                            return error.ToEData();
                        }
                        num3 = 0;
                        foreach (EData data4 in data3.elements)
                        {
                            ((ArrayList) list[num3++]).Add(data4);
                        }
                    }
                }
                emptyList = new ListData(list.Count);
                int num4 = 0;
                foreach (ArrayList list2 in list)
                {
                    ListData data5 = new ListData(list2.Count);
                    num3 = 0;
                    foreach (EData data7 in list2)
                    {
                        data5[num3++] = data7;
                    }
                    EData data6 = new EData();
                    data6.Type = DataType.ListData;
                    data6.Value = data5;
                    emptyList[num4++] = data6;
                }
            }
            EData data8 = new EData();
            data8.Type = DataType.ListData;
            data8.Value = emptyList;
            return data8;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Transposes a two dimensional list", new string[] { "List" }, new string[] { "a two dimensional list" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "Tran";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "Tran";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

