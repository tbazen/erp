using System;
using System.Runtime.InteropServices;
namespace INTAPS.Evaluator
{
    internal class Union : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_ParCount;

        private int CheckDoubleExistence(EData[] Pars, out bool[] onlyonce, int t)
        {
            int lastindex = 0;
            int num2 = 0;
            onlyonce = new bool[t];
            foreach (EData data in Pars)
            {
                for (int i = 0; i < ((ListData) data.Value).m_NumberElements; i++)
                {
                    if (this.CheckElement(((ListData) data.Value)[i], Pars, lastindex))
                    {
                        num2++;
                        onlyonce[lastindex] = true;
                        lastindex++;
                    }
                    else
                    {
                        onlyonce[lastindex] = false;
                        lastindex++;
                    }
                }
            }
            return num2;
        }

        private bool CheckElement(EData data, EData[] Pars, int lastindex)
        {
            int num = 0;
            int num2 = 0;
            foreach (EData data2 in Pars)
            {
                for (int i = 0; i < ((ListData) data2.Value).m_NumberElements; i++)
                {
                    if (num2 <= lastindex)
                    {
                        if ((data.Type == ((ListData) data2.Value)[i].Type) && ((ListData) data2.Value)[i].Value.Equals(data.Value))
                        {
                            num++;
                        }
                        num2++;
                    }
                }
            }
            return (num == 1);
        }

        public IVarParamCountFunction Clone()
        {
            return new Union();
        }

        public EData Evaluate(EData[] Pars)
        {
            EData data3;
            int n = 0;
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.ListData)
                {
                    FSError error = new FSError(2, "INVALID mismatch.");
                    return error.ToEData();
                }
                n += ((ListData) data.Value).m_NumberElements;
            }
            ListData data2 = new ListData(n);
            int index = 0;
            foreach (EData data in Pars)
            {
                for (int i = 0; i < ((ListData) data.Value).m_NumberElements; i++)
                {
                    data2.elements[index] = ((ListData) data.Value)[i];
                    index++;
                }
            }
            data3.Type = DataType.ListData;
            data3.Value = data2;
            return data3;
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the union of lists", new string[] { "List", "List" }, new string[] { "a list", "a list" }, new string[] { "Mand", "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "UNION";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "UNION";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

