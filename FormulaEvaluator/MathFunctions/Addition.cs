using System;
namespace INTAPS.Evaluator
{
    public class Addition : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            int j;
            FSError error;
            EData data = new EData();
            bool flag = false;
            foreach (EData data2 in Pars)
            {
                if (data2.Type == DataType.Text)
                {
                    flag = true;
                }
            }
            if (flag)
            {
                string str = "";
                foreach (EData data2 in Pars)
                {
                    str = str + data2.ToString();
                }
                data.Type = DataType.Text;
                data.Value = str;
                return data;
            }
            DataType[] typeArray = new DataType[] { Pars[0].Type, Pars[1].Type };
            if ((typeArray[0] == DataType.Float) && (typeArray[1] == DataType.Float))
            {
                data.Type = DataType.Float;
                data.Value = ((double) Pars[0].Value) + ((double) Pars[1].Value);
                return data;
            }
            if ((typeArray[0] == DataType.Int) && (typeArray[1] == DataType.Int))
            {
                data.Type = DataType.Int;
                data.Value = ((int) Pars[0].Value) + ((int) Pars[1].Value);
                return data;
            }
            if ((typeArray[0] == DataType.Float) && (typeArray[1] == DataType.Int))
            {
                data.Type = DataType.Float;
                data.Value = ((double)Pars[0].Value) + ((int)Pars[1].Value);
                return data;
            }
            if ((typeArray[0] == DataType.Int) && (typeArray[1] == DataType.Float))
            {
                data.Type = DataType.Float;
                data.Value = ((int)Pars[0].Value) + ((double)Pars[1].Value);
                return data;
            }
            if ((typeArray[0] == DataType.Text) && (typeArray[1] == DataType.Text))
            {
                data.Type = DataType.Text;
                data.Value = ((string) Pars[0].Value) + ((string) Pars[1].Value);
                return data;
            }
            if ((typeArray[0] == DataType.ListData) && (typeArray[1] == DataType.ListData))
            {
                ListData list1 = (ListData) Pars[0].Value;
                ListData list2 = (ListData) Pars[1].Value;
                ListData sumList = new ListData(list1.elements.Length + list2.elements.Length);
                list1.elements.CopyTo(sumList.elements, 0);
                list2.elements.CopyTo(sumList.elements, list1.elements.Length);
                data.Type = DataType.ListData;
                data.Value = sumList;
                return data;
            }
            if ((typeArray[0] == DataType.Vector1D) && (typeArray[1] == DataType.Vector1D))
            {
                data.Type = DataType.Vector1D;
                int n = 0;
                int dimension = 0;
                for (j = 0; j < Pars.Length; j++)
                {
                    if (j == 0)
                    {
                        dimension = ((Vector1D) Pars[j].Value).dimension;
                    }
                    else
                    {
                        n = ((Vector1D) Pars[j].Value).dimension;
                        if (dimension != n)
                        {
                            error = new FSError(2, "invalid dimension specification");
                            return error.ToEData();
                        }
                        dimension = n;
                    }
                }
                Vector1D vectord = new Vector1D(n);
                for (j = 0; j < n; j++)
                {
                    foreach (EData data6 in Pars)
                    {
                        vectord.VectorElements[j] += ((Vector1D) data6.Value).VectorElements[j];
                    }
                }
                data.Value = vectord;
                return data;
            }
            if ((typeArray[0] == DataType.Vector2D) && (typeArray[1] == DataType.Vector2D))
            {
                data.Type = DataType.Vector2D;
                int r = 0;
                int c = 0;
                int row = 0;
                int column = 0;
                for (j = 0; j < Pars.Length; j++)
                {
                    if (j == 0)
                    {
                        row = ((Vector2D) Pars[j].Value).row;
                        column = ((Vector2D) Pars[j].Value).column;
                    }
                    else
                    {
                        r = ((Vector2D) Pars[j].Value).row;
                        c = ((Vector2D) Pars[j].Value).column;
                        if ((row != r) || (column != c))
                        {
                            error = new FSError(2, "invalid 2D dimension specification");
                            return error.ToEData();
                        }
                        row = r;
                        column = c;
                    }
                }
                Vector2D vectord2 = new Vector2D(r, c);
                for (j = 0; j < r; j++)
                {
                    for (int i = 0; i < c; i++)
                    {
                        foreach (EData data6 in Pars)
                        {
                            vectord2.VectorElements[j, i] += ((Vector2D) data6.Value).VectorElements[j, i];
                        }
                    }
                }
                data.Value = vectord2;
                return data;
            }
            error = new FSError(2, "Type mismatch - addition function.");
            return error.ToEData();
        }

        public string Name
        {
            get
            {
                return "Addition";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 10;
            }
        }

        public string Symbol
        {
            get
            {
                return "+";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

