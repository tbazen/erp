using System;
namespace INTAPS.Evaluator
{
    public class Atan2F : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = Math.Atan2((double) Pars[0].Value, (double) Pars[1].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the trigonometric tangent accepting x and y", new string[] { "Number", "Number" }, new string[] { "value of X", "value of Y" }, new string[] { "Mand", "Mand" }, "Trigonometric function");
            }
        }

        public string Name
        {
            get
            {
                return "Atan2";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "ATAN2";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

