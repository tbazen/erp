using System;
namespace INTAPS.Evaluator
{
    public class BigInt
    {
        private const uint BASE10 = 10;
        private uint[] IntArray = new uint[NDIGITS];
        private static int NDIGITS = 200;

        public BigInt()
        {
            for (int i = 0; i < NDIGITS; i++)
            {
                this.IntArray[i] = 0;
            }
        }

        public static BigInt operator +(BigInt N1, BigInt N2)
        {
            int num4;
            int num = 0;
            uint num2 = 0;
            BigInt num3 = new BigInt();
            for (num4 = 0; num4 < NDIGITS; num4++)
            {
                num2 = (N1.IntArray[num4] + N2.IntArray[num4]) + ((uint) num);
                if (num2 >= 10)
                {
                    num = 1;
                    num2 -= 10;
                }
                else
                {
                    num = 0;
                }
                num3.IntArray[num4] = num2;
            }
            if (num != 0)
            {
                for (num4 = 0; num4 < NDIGITS; num4++)
                {
                    num3.IntArray[num4] = 0;
                }
            }
            return num3;
        }

        public void PutInt(int n)
        {
            int num = 0;
            do
            {
                this.IntArray[num++] = (uint) (((long) n) % 10L);
                n = (int) (((long) n) / 10L);
            }
            while (n != 0);
            do
            {
                this.IntArray[num++] = 0;
            }
            while (num < NDIGITS);
        }

        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < NDIGITS; i++)
            {
                str = this.IntArray[i].ToString() + str;
            }
            return str;
        }
    }
}

