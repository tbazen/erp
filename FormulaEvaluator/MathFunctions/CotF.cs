using System;
namespace INTAPS.Evaluator
{
    public class CotF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = 1.0 / Math.Tan((double) Pars[0].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the Trigonometric cotangent of a number", new string[] { "Number" }, new string[] { "a number" }, new string[] { "Mand" }, "Trigonometric function");
            }
        }

        public string Name
        {
            get
            {
                return "Cot";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "COT";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

