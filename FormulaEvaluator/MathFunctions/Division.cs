using System;
namespace INTAPS.Evaluator
{
    public class Division : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            object sum = null;
            foreach (var p in Pars)
            {
                if (sum == null)
                    sum = p.Value;
                else
                {
                    if (sum.GetType() == typeof(double) || p.Type == DataType.Float)
                        sum = Convert.ToDouble(sum)/ Convert.ToDouble(p.Value);
                    else if (sum.GetType() == typeof(long) || p.Type == DataType.LongInt)
                        sum = Convert.ToInt64(sum)/ Convert.ToInt64(p.Value);
                    else if (sum.GetType() == typeof(int) || p.Type == DataType.Int)
                        sum = Convert.ToInt32(sum)/ Convert.ToInt32(p.Value);
                    else
                        return new FSError("Division-Type mistmatch").ToEData();
                }
            }
            return new EData(sum);
            /*EData data;
            if ((Pars[0].Type == DataType.Float) && (Pars[1].Type == DataType.Float))
            {
                data = new EData();
                data.Value = ((double) Pars[0].Value) / ((double) Pars[1].Value);
                data.Type = DataType.Float;
                return data;
            }
            if ((Pars[0].Type == DataType.Int) && (Pars[1].Type == DataType.Int))
            {
                data = new EData();
                data.Value = ((double) ((int) Pars[0].Value)) / ((double) ((int) Pars[1].Value));
                data.Type = DataType.Float;
                return data;
            }
            if ((Pars[0].Type == DataType.Float) && (Pars[1].Type == DataType.Int))
            {
                data = new EData();
                data.Value = ((double) Pars[0].Value) / ((double) ((int) Pars[1].Value));
                data.Type = DataType.Float;
                return data;
            }
            if ((Pars[0].Type == DataType.Int) && (Pars[1].Type == DataType.Float))
            {
                data = new EData();
                data.Value = ((double) ((int) Pars[0].Value)) / ((double) Pars[1].Value);
                data.Type = DataType.Float;
                return data;
            }
            FSError error = new FSError(2, "Type mismatch - division function.");
            return error.ToEData();*/
        }

        public string Name
        {
            get
            {
                return "Division";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 20;
            }
        }

        public string Symbol
        {
            get
            {
                return "/";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

