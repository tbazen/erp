using System;
namespace INTAPS.Evaluator
{
    public class Exponentation : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch - exponentation function.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Value = Math.Pow((double) Pars[0].Value, (double) Pars[1].Value);
            data2.Type = DataType.Float;
            return data2;
        }

        public string Name
        {
            get
            {
                return "Addition";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 30;
            }
        }

        public string Symbol
        {
            get
            {
                return "^";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

