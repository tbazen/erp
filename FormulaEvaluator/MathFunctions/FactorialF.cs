using System;
namespace INTAPS.Evaluator
{
    public class FactorialF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Int;
            string str = Pars[0].Value.ToString();
            ulong n = Convert.ToUInt64(str);
            if (n <= 20L)
            {
                ulong num2 = this.fact(n);
                data2.Value = num2;
                return data2;
            }
            data2.Type = DataType.Float;
            uint num4 = Convert.ToUInt16(str);
            double num3 = this.factd(num4);
            data2.Value = num3;
            return data2;
        }

        public ulong fact(ulong n)
        {
            if (n == 1L)
            {
                return 1L;
            }
            if (n == 0L)
            {
                return 0L;
            }
            return (n * this.fact(n - ((ulong) 1L)));
        }

        private double factd(uint n)
        {
            double num2 = this.fact(20L);
            for (uint i = 0x15; i <= n; i++)
            {
                num2 *= i;
            }
            return num2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the factorial of anumber", new string[] { "Number" }, new string[] { "a posetive integer" }, new string[] { "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "Factorial";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "FACT";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

