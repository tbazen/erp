using System;
namespace INTAPS.Evaluator
{
    public class Floor : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = Math.Floor((double) Pars[0].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Rounds to the Lower integer", new string[] { "Decimal" }, new string[] { "Rational Number" }, new string[] { "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "Floor";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "Floor";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

