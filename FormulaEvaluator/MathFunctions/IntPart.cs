using System;
namespace INTAPS.Evaluator
{
    internal class IntPart : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            switch(Pars[0].Type)
            {
                case DataType.Float:
                    EData data = new EData();
                    data.Type = DataType.Int;
                    data.Value = (int) ((double) Pars[0].Value);
                    return data;
                case DataType.Int:
                    return Pars[0];
               default:
                FSError error = new FSError(100, "Type mismatch");
                return error.ToEData();
            }
            
            
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Extracts only the Integer part of a number", new string[] { "Number" }, new string[] { "a number" }, new string[] { "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Integer part";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "Int";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

