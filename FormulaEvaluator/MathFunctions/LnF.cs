using System;
namespace INTAPS.Evaluator
{
    public class LnF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            double newBase = 2.7182818;
            EData data = new EData();
            data.Type = DataType.Float;
            data.Value = Math.Log((double) Pars[0].Value, newBase);
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates Nutural logarithm of a number", new string[] { "Number" }, new string[] { "a Number" }, new string[] { "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Ln";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "LN";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

