using System;
namespace INTAPS.Evaluator
{

    public class Log10F : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            double newBase = 10.0;
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = Math.Log((double) Pars[0].Value, newBase);
            return data2;
        }

        public string Name
        {
            get
            {
                return "Log10";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "LOG10";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

