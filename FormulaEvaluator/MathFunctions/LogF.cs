using System;
namespace INTAPS.Evaluator
{

    public class LogF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = Math.Log((double) Pars[0].Value, (double) Pars[1].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Logarithm of a number", new string[] { "Number", "Number" }, new string[] { "a Number", "a Number" }, new string[] { "Mand", "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Log";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "LOG";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

