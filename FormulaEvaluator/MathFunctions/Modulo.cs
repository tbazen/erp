using System;
namespace INTAPS.Evaluator
{
    internal class Modulo : IInfixFunction, IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            bool flag = true;
            foreach (EData data in Pars)
            {
                if ((data.Type != DataType.Float) && (data.Type != DataType.Int))
                {
                    FSError error = new FSError(100, "Type mismatch-modulo");
                    return error.ToEData();
                }
                if (flag && (data.Type == DataType.Float))
                {
                    flag = false;
                }
            }
            EData data2 = new EData();
            if (flag)
            {
                data2.Type = DataType.Int;
                data2.Value = ((int) Pars[0].Value) % ((int) Pars[1].Value);
            }
            else
            {
                data2.Type = DataType.Float;
                data2.Value = ((double) ((double) Pars[0].Value)) % ((int) ((double) Pars[1].Value));
            }
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates Modulus", new string[] { "Number", "Number" }, new string[] { "a number", "a number" }, new string[] { "Mand", "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Module";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 20;
            }
        }

        public string Symbol
        {
            get
            {
                return "Mod";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

