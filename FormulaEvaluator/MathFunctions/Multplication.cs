using System;
namespace INTAPS.Evaluator
{
    public class Multplication : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data;
            Vector2D vectord;
            Vector1D vectord4;
            Vector1D vectord6;
            int num;
            int num2;
            int row;
            int column;
            double num6;
            FSError error;
            if ((Pars[0].Type == DataType.Float) && (Pars[1].Type == DataType.Float))
            {
                data = new EData();
                data.Value = ((double) Pars[0].Value) * ((double) Pars[1].Value);
                data.Type = DataType.Float;
                return data;
            }
            if ((Pars[0].Type == DataType.Int) && (Pars[1].Type == DataType.Int))
            {
                data = new EData();
                data.Value = ((int) Pars[0].Value) * ((int) Pars[1].Value);
                data.Type = DataType.Int;
                return data;
            }
            if ((Pars[0].Type == DataType.Vector2D) && (Pars[1].Type == DataType.Vector1D))
            {
                vectord = (Vector2D) Pars[0].Value;
                vectord4 = (Vector1D) Pars[1].Value;
                vectord6 = new Vector1D(vectord.row);
                row = vectord.row;
                column = vectord.column;
                if (vectord.column != vectord4.VectorElements.Length)
                {
                    error = new FSError(2, "Invalid dimension.");
                    return error.ToEData();
                }
                for (num = 0; num < row; num++)
                {
                    num6 = 0.0;
                    num2 = 0;
                    while (num2 < column)
                    {
                        num6 += vectord.VectorElements[num, num2] * vectord4.VectorElements[num2];
                        num2++;
                    }
                    vectord6.VectorElements[num] = num6;
                }
                data = new EData();
                data.Value = vectord6;
                data.Type = DataType.Vector1D;
                return data;
            }
            if ((Pars[0].Type == DataType.Vector1D) && (Pars[1].Type == DataType.Vector1D))
            {
                vectord4 = (Vector1D) Pars[0].Value;
                Vector1D vectord5 = (Vector1D) Pars[1].Value;
                row = vectord4.VectorElements.Length;
                if (row != vectord5.VectorElements.Length)
                {
                    error = new FSError(2, "Invalid dimension.");
                    return error.ToEData();
                }
                vectord6 = new Vector1D(row);
                for (num = 0; num < row; num++)
                {
                    vectord6.VectorElements[num] = vectord4.VectorElements[num] * vectord5.VectorElements[num];
                }
                data = new EData();
                data.Value = vectord6;
                data.Type = DataType.Vector1D;
                return data;
            }
            if ((Pars[0].Type == DataType.Vector2D) && (Pars[1].Type == DataType.Vector2D))
            {
                int num7;
                vectord = (Vector2D) Pars[0].Value;
                Vector2D vectord2 = (Vector2D) Pars[1].Value;
                row = vectord.row;
                column = vectord2.column;
                if ((num7 = vectord.column) != vectord2.row)
                {
                    error = new FSError(2, "Invalid dimension.");
                    return error.ToEData();
                }
                Vector2D vectord3 = new Vector2D(row, column);
                for (num = 0; num < row; num++)
                {
                    for (num2 = 0; num2 < column; num2++)
                    {
                        num6 = 0.0;
                        for (int i = 0; i < num7; i++)
                        {
                            num6 += vectord.VectorElements[num, i] * vectord2.VectorElements[i, num2];
                        }
                        vectord3.VectorElements[num, num2] = num6;
                    }
                }
                data = new EData();
                data.Value = vectord3;
                data.Type = DataType.Vector2D;
                return data;
            }
            error = new FSError(2, "Type mismatch - multiply function.");
            return error.ToEData();
        }

        public string Name
        {
            get
            {
                return "Multplication";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 20;
            }
        }

        public string Symbol
        {
            get
            {
                return "*";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

