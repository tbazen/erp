using System;
namespace INTAPS.Evaluator
{
    public class Neg : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data2;
            foreach (EData data in Pars)
            {
                if ((data.Type != DataType.Float) && (data.Type != DataType.Int))
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            if (Pars[0].Type == DataType.Float)
            {
                data2 = new EData();
                data2.Type = DataType.Float;
                data2.Value = -((double) Pars[0].Value);
                return data2;
            }
            if (Pars[0].Type == DataType.Int)
            {
                data2 = new EData();
                data2.Type = DataType.Int;
                data2.Value = -((int) Pars[0].Value);
                return data2;
            }
            return EData.Empty;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Negates a number", new string[] { "Number" }, new string[] { "a Number" }, new string[] { "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Neg";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "Neg";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

