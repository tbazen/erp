using System;
namespace INTAPS.Evaluator
{
    public class PercentileF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            EData data = new EData();
            data.Type = DataType.Text;
            double num = 0.0;
            int num2 = 0;
            if ((Pars[0].Type != DataType.Vector1D) || (Pars[1].Type != DataType.Float))
            {
                error = new FSError(2, "the data type should be a vector");
                return error.ToEData();
            }
            for (int i = 0; i < ((Vector1D) Pars[0].Value).dimension; i++)
            {
                if (((Vector1D) Pars[0].Value).VectorElements[i] < ((double) Pars[1].Value))
                {
                    num2++;
                }
            }
            if (((Vector1D) Pars[0].Value).dimension <= 0)
            {
                error = new FSError(2, "the dimension should be greater than zero");
                return error.ToEData();
            }
            num = (num2 * 100) / ((Vector1D) Pars[0].Value).dimension;
            data.Value = " " + num + "%";
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the percentile of a number in vector", new string[] { "Vector", "Number" }, new string[] { "a Vector", "a number" }, new string[] { "Mand", "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "PERCENTILE";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "PERCENTILE";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

