using System;
namespace INTAPS.Evaluator
{
    public class PowerF : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = Math.Pow((double) Pars[0].Value, (double) Pars[1].Value);
            return data2;
        }

        public string Name
        {
            get
            {
                return "Power";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "POWER";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

