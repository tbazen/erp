using System;
namespace INTAPS.Evaluator
{
    public class Round : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_NPar = 2;

        public IVarParamCountFunction Clone()
        {
            return new Round();
        }

        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            if (Pars.Length == 2)
            {
                data2.Value = Math.Round((double) Pars[0].Value, (int) ((double) Pars[1].Value));
            }
            else
            {
                data2.Value = Math.Round((double) Pars[0].Value);
            }
            return data2;
        }

        public bool SetParCount(int n)
        {
            if ((n < 1) && (n > 2))
            {
                return false;
            }
            this.m_NPar = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Rounds to the nearest Integer", new string[] { "Number" }, new string[] { "Number" }, new string[] { "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Round";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_NPar;
            }
        }

        public string Symbol
        {
            get
            {
                return "Round";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

