using System;
namespace INTAPS.Evaluator
{

    internal class SolvQuad : IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data;
            double num = (double) Pars[0].Value;
            double num2 = (double) Pars[1].Value;
            double num3 = (double) Pars[2].Value;
            double d = (num2 * num2) - ((4.0 * num) * num3);
            double num4 = (-num2 + Math.Sqrt(d)) / (2.0 * num);
            data.Value = num4;
            data.Type = DataType.Float;
            return data;
        }

        public string Name
        {
            get
            {
                return "Solv quadratic equation.";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "sq";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

