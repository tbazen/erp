using System;
namespace INTAPS.Evaluator
{

    public class SqrtF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            data2.Value = Math.Sqrt((double) Pars[0].Value);
            return data2;
        }

        public string GetParName(int i)
        {
            if (i == 0)
            {
                return "Number";
            }
            return null;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates squar root of a number", new string[] { "Number" }, new string[] { "The number of which squart root is to computed" }, new string[] { "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "Sqrt";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "SQRT";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

