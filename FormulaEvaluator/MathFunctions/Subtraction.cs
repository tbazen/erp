using System;
namespace INTAPS.Evaluator
{

    public class Subtraction : IInfixFunction, IFunction
    {
        public EData Evaluate(EData[] Pars)
        {
            int i;
            FSError error;
            EData ret = new EData();
            int operandType = -1;
            foreach (EData data2 in Pars)
            {
                if ((data2.Type == DataType.Float) || (data2.Type == DataType.Int)
                    || data2.Type== DataType.LongInt
                    )
                {
                    operandType = 0;
                }
                else
                {
                    operandType = -1;
                    break;
                }
            }
            switch (operandType)
            {
                case -1:
                    foreach (EData data2 in Pars)
                    {
                        if (data2.Type == DataType.Vector1D)
                        {
                            operandType = 1;
                        }
                        else
                        {
                            operandType = -2;
                            break;
                        }
                    }
                    break;
                case -2:
                    foreach (EData data2 in Pars)
                    {
                        if (data2.Type == DataType.Vector2D)
                        {
                            operandType = 2;
                        }
                        else
                        {
                            operandType = -1;
                            break;
                        }
                    }
                    break;
                case 0:
                    object sum = null;
                    foreach(var p in Pars)
                    {
                        if (sum == null)
                            sum = p.Value;
                        else
                        {
                            if (sum.GetType() == typeof(double) || p.Type==DataType.Float)
                                sum = Convert.ToDouble(sum) - Convert.ToDouble(p.Value);
                            else if (sum.GetType() == typeof(long) || p.Type == DataType.LongInt)
                                sum = Convert.ToInt64(sum) - Convert.ToInt64(p.Value);
                            else if (sum.GetType() == typeof(int) || p.Type == DataType.Int)
                                sum = Convert.ToInt32(sum) - Convert.ToInt32(p.Value);
                            else
                                return new FSError("Substraction-Type mistmatch").ToEData();
                        }
                    }
                    return new EData(sum);
                    /*if ((Pars[0].Type == DataType.Float) && (Pars[1].Type == DataType.Float))
                    {
                        ret.Type = DataType.Float;
                        ret.Value = ((double)Pars[0].Value) - ((double)Pars[1].Value);
                        return ret;
                    }
                    if ((Pars[0].Type == DataType.Int) && (Pars[1].Type == DataType.Int))
                    {
                        ret.Type = DataType.Int;
                        ret.Value = ((int)Pars[0].Value) - ((int)Pars[1].Value);
                        return ret;
                    }
                    if ((Pars[0].Type == DataType.Float) && (Pars[1].Type == DataType.Int))
                    {
                        ret.Type = DataType.Float;
                        ret.Value = ((double)Pars[0].Value) - ((int)Pars[1].Value);
                        return ret;
                    }
                    if ((Pars[0].Type == DataType.Int) && (Pars[1].Type == DataType.Float))
                    {
                        ret.Type = DataType.Float;
                        ret.Value = ((int)Pars[0].Value) - ((double)Pars[1].Value);
                        return ret;
                    }
                    return new FSError("Substraction-Type mistmatch").ToEData();*/
                case 1:
                    {
                        int n = 0;
                        int dimension = 0;
                        ret.Type = DataType.Vector1D;
                        for (i = 0; i < Pars.Length; i++)
                        {
                            if (i == 0)
                            {
                                dimension = ((Vector1D)Pars[i].Value).dimension;
                            }
                            else
                            {
                                n = ((Vector1D)Pars[i].Value).dimension;
                                if (dimension != n)
                                {
                                    error = new FSError(2, "invalid dimension specification");
                                    return error.ToEData();
                                }
                                dimension = n;
                            }
                        }
                        Vector1D vectord = new Vector1D(n);
                        for (i = 0; i < n; i++)
                        {
                            vectord.VectorElements[i] = ((Vector1D)Pars[0].Value).VectorElements[i] - ((Vector1D)Pars[1].Value).VectorElements[i];
                        }
                        ret.Value = vectord;
                        return ret;
                    }
                case 2:
                    {
                        int r = 0;
                        int c = 0;
                        int row = 0;
                        int column = 0;
                        ret.Type = DataType.Vector2D;
                        for (i = 0; i < Pars.Length; i++)
                        {
                            if (i == 0)
                            {
                                row = ((Vector2D)Pars[i].Value).row;
                                column = ((Vector2D)Pars[i].Value).column;
                            }
                            else
                            {
                                r = ((Vector2D)Pars[i].Value).row;
                                c = ((Vector2D)Pars[i].Value).column;
                                if ((row != r) || (column != c))
                                {
                                    error = new FSError(2, "invalid 2D dimension specification");
                                    return error.ToEData();
                                }
                                row = r;
                                column = c;
                            }
                        }
                        Vector2D vectord2 = new Vector2D(r, c);
                        for (i = 0; i < r; i++)
                        {
                            for (int j = 0; j < c; j++)
                            {
                                vectord2.VectorElements[i, j] = ((Vector2D)Pars[0].Value).VectorElements[i, j] - ((Vector2D)Pars[1].Value).VectorElements[i, j];
                            }
                        }
                        ret.Value = vectord2;
                        return ret;
                    }
            }
            error = new FSError(2, "Type mismatch - subtraction function.");
            return error.ToEData();
        }

        public string Name
        {
            get
            {
                return "Subtraction";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public int Precidence
        {
            get
            {
                return 10;
            }
        }

        public string Symbol
        {
            get
            {
                return "-";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

