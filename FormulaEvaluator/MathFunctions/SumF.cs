using System;
namespace INTAPS.Evaluator
{

    public class SumF : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int parCount = 2;

        public IVarParamCountFunction Clone()
        {
            return new SumF();
        }

        public EData Evaluate(EData[] Pars)
        {
            if ((Pars.Length == 1) && (Pars[0].Type == DataType.Vector1D))
            {
                return ((IFunction) CalcGlobal.Functions["SUMELEMENTS"]).Evaluate(Pars);
            }
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            double num = 0.0;
            foreach (EData data in Pars)
            {
                num += (double) data.Value;
            }
            data2.Type = DataType.Float;
            data2.Value = num;
            this.parCount = 2;
            return data2;
        }

        public bool SetParCount(int n)
        {
            this.parCount = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Add numbers", new string[] { "Number", "Number" }, new string[] { "Number", "Number" }, new string[] { "Mand", "var" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "Sum";
            }
        }

        public int ParCount
        {
            get
            {
                return this.parCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "SUM";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

