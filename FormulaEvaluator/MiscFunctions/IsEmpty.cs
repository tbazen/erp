using System;
namespace INTAPS.Evaluator
{
    public class IsEmpty : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            data.Type = DataType.Bool;
            data.Value = Pars[0].Type == DataType.Empty;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Checks if the value is empty", new string[] { "value" }, new string[] { "any value" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "Is empty?";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "IsEmpty";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

