using System;
namespace INTAPS.Evaluator
{

    public class IsError : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            data.Type = DataType.Bool;
            data.Value = Pars[0].Type == DataType.Error;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Checks if the expresion has error", new string[] { "expresion" }, new string[] { "any expresion" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "Is Error?";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "IsError";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

