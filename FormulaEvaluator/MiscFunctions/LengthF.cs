using System;
namespace INTAPS.Evaluator
{

    internal class LengthF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            int length;
            if (Pars[0].Type == DataType.ListData)
            {
                length = ((ListData) Pars[0].Value).elements.Length;
            }
            else if (Pars[0].Type == DataType.Vector1D)
            {
                length = ((Vector1D) Pars[0].Value).VectorElements.Length;
            }
            else if (Pars[0].Type == DataType.Vector2D)
            {
                length = ((Vector2D) Pars[0].Value).VectorElements.GetUpperBound(0) + 1;
            }
            else if (Pars[0].Type == DataType.Text)
            {
                length = ((string) Pars[0].Value).Length;
            }
            else
            {
                FSError error = new FSError("Type mismatch - length function.");
                return error.ToEData();
            }
            EData data = new EData();
            data.Type = DataType.Int;
            data.Value = length;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the length of a text", new string[] { "string" }, new string[] { "a text" }, new string[] { "Mand" }, "Text function");
            }
        }

        public string Name
        {
            get
            {
                return "Length of vector and list";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "Len";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

