using System;
namespace INTAPS.Evaluator
{

    public class Solve : IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private INTAPS.Evaluator.ISymbolProvider m_FP;
        private int m_ParCount = 1;

        public IVarParamCountFunction Clone()
        {
            Solve solve = new Solve();
            solve.ISymbolProvider = this.m_FP;
            return solve;
        }

        public EData Evaluate(EData[] Pars)
        {
            double num2;
            EData data7;
            string str = "";
            string str2 = "X";
            double num = -1.0;
            int num3 = 0x2710;
            int num4 = 6;
            int length = Pars.Length;
            if (length == 0)
            {
                FSError error = new FSError(1, "Too few argument");
                return error.ToEData();
            }
            if (length > 0)
            {
                str = Pars[0].Value.ToString();
            }
            if (length > 1)
            {
                str2 = Pars[1].Value.ToString();
            }
            if (length > 2)
            {
                num4 = (int) ((double) Pars[2].Value);
            }
            if (length > 3)
            {
                num = (double) Pars[3].Value;
            }
            if (length > 4)
            {
                num2 = (double) Pars[4].Value;
            }
            else
            {
                num2 = 1.0001 * num;
            }
            if (length > 5)
            {
                num3 = (int) ((double) Pars[5].Value);
            }
            Symbolic symbolic = new Symbolic();
            symbolic.m_ISymbolProvider = this.m_FP;
            symbolic.Expression = str;
            double num7 = 0.0;
            double num8 = 0.0;
            double num9 = 0.0;
            num7 = num;
            num8 = num2;
            double num13 = Math.Pow(10.0, (double) num4);
            double num6 = 1.0 / num13;
            int num14 = 0;
            Random random = new Random(0);
            double num15 = 1E-13;
            for (int i = 0; i < symbolic.VarCount; i++)
            {
                if (symbolic.GetSimpleVarName(i).ToUpper() != str2.ToUpper())
                {
                    symbolic[i] = this.m_FP.GetData(symbolic.GetSimpleVarName(i));
                }
            }
            while (true)
            {
                num14++;
                if (num14 > num3)
                {
                    break;
                }
                EData data = new EData();
                data.Type = DataType.Float;
                data.Value = num7;
                symbolic[str2] = data;
                double num10 = (double) symbolic.Evaluate().Value;
                EData data2 = new EData();
                data2.Type = DataType.Float;
                data2.Value = num8;
                symbolic[str2] = data2;
                double num11 = (double) symbolic.Evaluate().Value;
                if (Math.Abs((double) (num10 - num11)) < num15)
                {
                    num9 = num7;
                }
                else
                {
                    num9 = num7 - ((num10 * (num7 - num8)) / (num10 - num11));
                }
                EData data3 = new EData();
                data3.Type = DataType.Float;
                data3.Value = num9;
                symbolic[str2] = data3;
                double num12 = (double) symbolic.Evaluate().Value;
                if (Math.Abs(num12) < num6)
                {
                    break;
                }
                if (Math.Abs((double) (num10 - num11)) <= num15)
                {
                    num7 = random.Next(0x3e8);
                    num8 = random.Next(0x7d0);
                }
                else if (Math.Abs(num7) > Math.Abs(num8))
                {
                    num7 = num9;
                }
                else
                {
                    num8 = num9;
                }
            }
            if (num14 > num3)
            {
                data7 = new FSError(0, "Solution couldn't be found after " + num3 + " iterations").ToEData();
            }
            else
            {
                data7.Type = DataType.Vector1D;
                Vector1D vectord = new Vector1D(2);
                vectord[0] = num9;
                vectord[1] = num14;
                data7.Value = vectord;
            }
            return data7;
        }

        public bool SetParCount(int n)
        {
            if (n < 1)
            {
                return false;
            }
            if (n > 6)
            {
                return false;
            }
            this.m_ParCount = n;
            return true;
        }

        public string[] Dependencies
        {
            get
            {
                return null;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Solves any function", new string[] { "Function", "Variable", "Number", "Number" }, new string[] { "function", "variable", "significant Digit", "Iteration" }, new string[] { "Mand", "Opt", "Opt", "Opt" }, "Mathematical function");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_FP = value;
            }
        }

        public string Name
        {
            get
            {
                return "Solver";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "Solv";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

