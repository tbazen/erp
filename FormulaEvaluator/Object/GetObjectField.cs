using System;
using System.Collections.Generic;
using System.Dynamic;
namespace INTAPS.Evaluator
{    
    internal class GetObjectField : IInfixFunction, IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if ((Pars[1].Type != DataType.Text))
            {
                error = new FSError("GetObjectField: Second parameter should be text");
                return error.ToEData();
            }
            string field = Pars[1].Value as string;
            if (Pars[0].Value == null)
                return EData.Empty;
            object fv=null;
            System.Reflection.FieldInfo fieldInfo=Pars[0].Value.GetType().GetField(field);
            if (fieldInfo != null)
            {
                fv = fieldInfo.GetValue(Pars[0].Value);
            }
            else
            {
                System.Reflection.PropertyInfo prop = Pars[0].Value.GetType().GetProperty(field);
                if(prop!=null)
                    fv = prop.GetValue(Pars[0].Value,new object[]{});
            }
            return new EData(fv);
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Gets a field of an object", new string[] { "List", "Index" }, new string[] { "Object", "Field Name" }, new string[] { "Mand", "Mand" }, "Object");
            }
        }

        public string Name
        {
            get
            {
                return "Get Field Value";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return ".";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }

        public int Precidence
        {
            get { return 40; }
        }
    }
}

