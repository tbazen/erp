using System;
using System.Collections.Generic;
using System.Dynamic;
namespace INTAPS.Evaluator
{
    public class EDynaObject : DynamicObject
    {
        public Dictionary<string, EData> values;
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (values.ContainsKey(binder.Name))
                result = values[binder.Name];
            else
                result = new EData(DataType.Empty, null);
            return true;
        }
    }
    internal class OBJECTF : IVarParamCountFunction, IFunction
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new OBJECTF() { m_ParCount = this.m_ParCount };
        }

        public EData Evaluate(EData[] Pars)
        {
            
            Dictionary<string, EData> values = new Dictionary<string, EData>();
            for(int i=0;i<Pars.Length/2;i++)
            {
                EData key=Pars[2*i];
                EData value=Pars[2*i+1];
                if (key.Value == null)
                    return new FSError("Empty value can't be object property name").ToEData();
                string keyVal = key.Value.ToString();
                if(values.ContainsKey(keyVal))
                    return new FSError(keyVal + " property is repeated in the object").ToEData();
                values.Add(keyVal, value);
            }
            EData ret;
            EDynaObject data = new EDynaObject() { values = values };
            ret.Type = DataType.Object;
            ret.Value = data;
            return ret;
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return true;
        }

        public string Name
        {
            get
            {
                return "OBJECT";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "OBJECT";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
    
    
    public class ObjectToList:IFunction
    {
        EData objectToEdata(EData val)
        {
            if (val.Type == DataType.ListData)
            {
                ListData list = (ListData)val.Value;
                for (int i = 0; i < list.elements.Length; i++)
                {
                    list.elements[i] = objectToEdata(list.elements[i]);
                }
                return val;
            }
            else if (val.Type == DataType.Object)
            {
                if (val.Value == null)
                    return EData.Empty;
                List<EData> l = new List<EData>();
                foreach (System.Reflection.FieldInfo fi in val.Value.GetType().GetFields())
                {
                    EData data = new EData(fi.GetValue(val.Value));
                    l.Add(new EData(DataType.ListData,new ListData(new EData[]{
                        new EData(fi.Name),
                        objectToEdata(data)}
                        )));
                }
                foreach (System.Reflection.PropertyInfo fi in val.Value.GetType().GetProperties())
                {
                    if (fi.GetIndexParameters().Length > 0)
                        continue;
                    EData data = new EData(fi.GetValue(val.Value, new object[0]));
                    l.Add(new EData(DataType.ListData, new ListData(new EData[]{
                        new EData(fi.Name),
                        objectToEdata(data)}
                        )));
                }
                return new EData(DataType.ListData, new ListData(l.ToArray()));
            }
            else
                return val;
        }
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.Object)
                return new FSError("ObectToList: Parameter should be object").ToEData();
            return objectToEdata(Pars[0]);
        }

        public string Name
        {
            get { return "Object to List"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "O2L"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
}

