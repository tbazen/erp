namespace INTAPS.Evaluator
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Runtime.Serialization.Formatters.Binary;
    
    internal class XmlObject : IVarParamCountFunction, IFunction
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new XmlObject() { m_ParCount = this.m_ParCount };
        }

        public EData Evaluate(EData[] Pars)
        {
            string xml = Pars[0].Value as string;
            Type type = System.Type.GetType(Pars[1].Value as string);
            Type[] types = new Type[Pars.Length-2];
            for(int i=2;i<Pars.Length;i++)
            {
                types[i - 2] = System.Type.GetType(Pars[i].Value as string);
            }
            System.Xml.Serialization.XmlSerializer s =  types.Length==0?new System.Xml.Serialization.XmlSerializer(type):
                new System.Xml.Serialization.XmlSerializer(type,types);
            return new EData(s.Deserialize(new System.IO.StringReader(xml)));
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return n>1;
        }

        public string Name
        {
            get
            {
                return "XmlObject";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "XmlObject";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }

    internal class BinObject : IVarParamCountFunction, IFunction
    {
        private int m_ParCount;

        public IVarParamCountFunction Clone()
        {
            return new BinObject() { m_ParCount = this.m_ParCount };
        }

        public EData Evaluate(EData[] Pars)
        {
            byte[] bytes = Pars[0].Value as byte[];
            BinaryFormatter s = new BinaryFormatter();
            return new EData(s.Deserialize(new System.IO.MemoryStream(bytes)));
        }

        public bool SetParCount(int n)
        {
            this.m_ParCount = n;
            return n > 1;
        }

        public string Name
        {
            get
            {
                return "BinObject";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "BinObject";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

