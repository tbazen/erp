using System;
namespace INTAPS.Evaluator
{
    public class FReplaceF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            DataType[] typeArray = new DataType[] { Pars[0].Type, Pars[1].Type, Pars[1].Type };
            if (((typeArray[0] == DataType.Text) && (typeArray[1] == DataType.Text)) && (typeArray[2] == DataType.Text))
            {
                string[] strArray = new string[] { Pars[0].ToString(), Pars[1].ToString(), Pars[2].ToString() };
                data.Value = strArray[0].Replace(strArray[1], strArray[2]);
                data.Type = DataType.Text;
                return data;
            }
            FSError error = new FSError(2, "Type mismatch.");
            return error.ToEData();
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Replaces new text in a give text ", new string[] { "string", "string", "string" }, new string[] { "The given text", "The text to be replaced", "The new text" }, new string[] { "Mand", "Mand", "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "FReplace";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "FREPLACE";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

