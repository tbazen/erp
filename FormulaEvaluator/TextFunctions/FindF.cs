using System;
namespace INTAPS.Evaluator
{
    public class FindF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data;
            if ((Pars[0].Type != DataType.Text) || (Pars[1].Type != DataType.Text))
            {
                FSError error = new FSError("Type mistmatch");
                return error.ToEData();
            }
            string str = (string) Pars[0].Value;
            string str2 = (string) Pars[1].Value;
            data.Value = str.IndexOf(str2) + 1.0;
            data.Type = DataType.Float;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Finds the specified text in a given text", new string[] { "string", "string" }, new string[] { "Given Text", "Text to be Searched" }, new string[] { "Mand", "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "Find";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "Find";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

