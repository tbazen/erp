using System;
namespace INTAPS.Evaluator
{
    public class ReplaceF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            EData data = new EData();
            DataType[] typeArray = new DataType[] { Pars[0].Type, Pars[1].Type, Pars[2].Type, Pars[3].Type };
            try
            {
                if ((((typeArray[0] == DataType.Text) && (typeArray[1] == DataType.Float)) && (typeArray[2] == DataType.Float)) && (typeArray[3] == DataType.Text))
                {
                    string str = Pars[0].ToString();
                    int num = (int) ((double) Pars[1].Value);
                    int count = (int) ((double) Pars[2].Value);
                    string str2 = Pars[3].ToString();
                    data.Value = str.Remove(num - 1, count).Insert(num - 1, str2);
                    data.Type = DataType.Text;
                    return data;
                }
                error = new FSError(2, "Type mismatch.");
                return error.ToEData();
            }
            catch
            {
                error = new FSError(3, "Invalid start index or Count");
                return error.ToEData();
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Replaces new text in a give text ", new string[] { "string", "Number", "Number", "string" }, new string[] { "The given text", "Starting position", "Number of characters to be Replaced", "The new text" }, new string[] { "Mand", "Mand", "Mand", "Mand" }, "Text function");
            }
        }

        public string Name
        {
            get
            {
                return "Replace";
            }
        }

        public int ParCount
        {
            get
            {
                return 4;
            }
        }

        public string Symbol
        {
            get
            {
                return "REPLACE";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

