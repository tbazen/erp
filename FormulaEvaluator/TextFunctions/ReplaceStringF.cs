using System;
namespace INTAPS.Evaluator
{
    public class ReplaceStringF : IVarParamCountFunction, IFunction
    {
        private int _nPar;

        public IVarParamCountFunction Clone()
        {
            ReplaceStringF gf = new ReplaceStringF();
            gf._nPar = this._nPar;
            return gf;
        }

        public EData Evaluate(EData[] Pars)
        {
            string format = Pars[0].Value as string;
            object[] args = new object[Pars.Length - 1];
            for (int i = 1; i < Pars.Length; i++)
            {
                args[i - 1] = Pars[i].Value;
            }
            return new EData(DataType.Text, string.Format(format, args));
        }

        public bool SetParCount(int n)
        {
            this._nPar = n;
            return (n > 0);
        }

        public string Name
        {
            get
            {
                return "Format String by Parameter";
            }
        }

        public int ParCount
        {
            get
            {
                return this._nPar;
            }
        }

        public string Symbol
        {
            get
            {
                return "FormatByPar";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

