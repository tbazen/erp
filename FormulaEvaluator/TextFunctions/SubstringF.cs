using System;
namespace INTAPS.Evaluator
{
    public class SubstringF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            EData data = new EData();
            DataType[] typeArray = new DataType[] { Pars[0].Type, Pars[1].Type, Pars[1].Type };
            if ((typeArray[1] != DataType.Float) || (typeArray[2] != DataType.Float))
            {
                error = new FSError(2, "Type mismatch.");
                return error.ToEData();
            }
            int startIndex = Convert.ToInt32( Pars[1].Value) - 1;
            int length = Convert.ToInt32(Pars[2].Value);
            if (typeArray[0] == DataType.Text)
            {
                string str = Pars[0].ToString();
                if ((((startIndex + length) > str.Length) || (startIndex < 0)) || (length < 0))
                {
                    error = new FSError("Index out of bound");
                    return error.ToEData();
                }
                data.Value = str.Substring(startIndex, length);
                data.Type = DataType.Text;
                return data;
            }
            if (typeArray[0] == DataType.ListData)
            {
                ListData data2 = (ListData) Pars[0].Value;
                if ((((startIndex + length) > data2.elements.Length) || (startIndex < 0)) || (length < 0))
                {
                    error = new FSError("Index out of bound");
                    return error.ToEData();
                }
                ListData data3 = new ListData(length);
                Array.Copy(data2.elements, startIndex, data3.elements, 0, length);
                data.Value = data3;
                data.Type = DataType.ListData;
                return data;
            }
            error = new FSError(2, "Type mismatch.");
            return error.ToEData();
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Extracts a text", new string[] { "string", "Number", "Number" }, new string[] { "a text", "starting index", "length" }, new string[] { "Mand", "Mand", "Mand" }, "Text function");
            }
        }

        public string Name
        {
            get
            {
                return "Substring";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "sub";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

