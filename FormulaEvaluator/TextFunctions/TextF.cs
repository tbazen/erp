using System;
namespace INTAPS.Evaluator
{
    public class TextF : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_npar = 1;

        public IVarParamCountFunction Clone()
        {
            return new TextF();
        }

        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            data.Type = DataType.Text;
            if (Pars.Length == 1)
            {
                data.Value = Pars[0].ToString();
                return data;
            }
            string format = (string) Pars[1].Value;
            switch (Pars[0].Type)
            {
                case DataType.Int:
                    data.Value = ((int) Pars[0].Value).ToString(format);
                    return data;

                case DataType.Float:
                    data.Value = ((double) Pars[0].Value).ToString(format);
                    return data;

                case DataType.DateTime:
                    data.Value = ((DateTime) Pars[0].Value).ToString(format);
                    return data;
            }
            data.Value = Pars[0].ToString();
            return data;
        }

        public bool SetParCount(int n)
        {
            if (n < 1)
            {
                return false;
            }
            this.m_npar = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Converts the value to text ", new string[] { "Value" }, new string[] { "any value" }, new string[] { "Mand" }, "Text function");
            }
        }

        public string Name
        {
            get
            {
                return "Text";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_npar;
            }
        }

        public string Symbol
        {
            get
            {
                return "Text";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

