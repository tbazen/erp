using System;
namespace INTAPS.Evaluator
{
    public class UCaseF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Text)
                {
                    FSError error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            string str = Pars[0].ToString();
            data2.Type = DataType.Text;
            data2.Value = str.ToUpper();
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Convert text to uppercase ", new string[] { "string" }, new string[] { "text" }, new string[] { "Mand" }, "Text function");
            }
        }

        public string Name
        {
            get
            {
                return "UCase";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "UCASE";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

