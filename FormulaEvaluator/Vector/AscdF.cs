using System;
namespace INTAPS.Evaluator
{

    public class AscdF : IFunction, IFunctionDocumentation
    {
        public Vector1D ascending(Vector1D vect_1d)
        {
            int num2;
            Vector1D vectord = new Vector1D(vect_1d.dimension);
            for (num2 = 0; num2 < vectord.dimension; num2++)
            {
                vectord.VectorElements[num2] = vect_1d.VectorElements[num2];
            }
            for (num2 = 0; num2 < vectord.dimension; num2++)
            {
                for (int i = 0; i <= num2; i++)
                {
                    double num = vectord.VectorElements[num2];
                    if (num < vectord.VectorElements[i])
                    {
                        vectord.VectorElements[num2] = vectord.VectorElements[i];
                        vectord.VectorElements[i] = num;
                    }
                }
            }
            return vectord;
        }

        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Vector1D)
                {
                    FSError error = new FSError(2, "Type mismatch.-INVLID PARAMETER FOR ORDERING");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Vector1D;
            data2.Value = this.ascending((Vector1D) Pars[0].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Sorts one_dimentional_vector in ascending order", new string[] { "Vector" }, new string[] { "One Dimentional Vector" }, new string[] { "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "ASCENDING";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "ASCD";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

