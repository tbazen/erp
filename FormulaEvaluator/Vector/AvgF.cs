using System;
namespace INTAPS.Evaluator
{

    public class AvgF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            data.Type = DataType.Float;
            double num = 0.0;
            if (Pars[0].Type != DataType.Vector1D)
            {
                FSError error = new FSError(2, "the data type should be a vector");
                return error.ToEData();
            }
            for (int i = 0; i < ((Vector1D) Pars[0].Value).dimension; i++)
            {
                num += ((Vector1D) Pars[0].Value).VectorElements[i];
            }
            num /= (double) ((Vector1D) Pars[0].Value).dimension;
            data.Value = num;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the average of numbers in one dimentional vector", new string[] { "vector" }, new string[] { "vector" }, new string[] { "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "AVERAGE";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "AVG";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

