using System;
namespace INTAPS.Evaluator
{

    internal class CombineVec : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int n = 0;

        public IVarParamCountFunction Clone()
        {
            return new CombineVec();
        }

        public EData Evaluate(EData[] Pars)
        {
            int length = Pars.Length;
            int c = -1;
            foreach (EData data in Pars)
            {
                FSError error;
                if (data.Type != DataType.Vector1D)
                {
                    error = new FSError("Type mismatch");
                    return error.ToEData();
                }
                if (c == -1)
                {
                    c = ((Vector1D) data.Value).dimension;
                }
                else if (c != ((Vector1D) data.Value).dimension)
                {
                    error = new FSError("All elements must have the same dimention");
                    return error.ToEData();
                }
            }
            Vector2D vectord = new Vector2D(length, c);
            int num4 = 0;
            foreach (EData data in Pars)
            {
                Vector1D vectord2 = (Vector1D) data.Value;
                for (int i = 0; i < c; i++)
                {
                    vectord.VectorElements[num4, i] = vectord2[i];
                }
                num4++;
            }
            EData data2 = new EData();
            data2.Type = DataType.Vector2D;
            data2.Value = vectord;
            return data2;
        }

        public bool SetParCount(int n)
        {
            this.n = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Combine one_dimentional Vectors", new string[] { "vector", "vector" }, new string[] { "One dimensional vector", "One dimensional vector" }, new string[] { "Mand", "Mand" });
            }
        }

        public string Name
        {
            get
            {
                return "Combine";
            }
        }

        public int ParCount
        {
            get
            {
                return this.n;
            }
        }

        public string Symbol
        {
            get
            {
                return "CombVec";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

