using System;
namespace INTAPS.Evaluator
{

    public class DscdF : IFunction, IFunctionDocumentation
    {
        public Vector1D descending(Vector1D vect_1d)
        {
            int num2;
            Vector1D vectord = new Vector1D(vect_1d.dimension);
            for (num2 = 0; num2 < vectord.dimension; num2++)
            {
                vectord.VectorElements[num2] = vect_1d.VectorElements[num2];
            }
            for (num2 = 0; num2 < vectord.dimension; num2++)
            {
                for (int i = 0; i <= num2; i++)
                {
                    double num = vectord.VectorElements[num2];
                    if (num > vectord.VectorElements[i])
                    {
                        vectord.VectorElements[num2] = vectord.VectorElements[i];
                        vectord.VectorElements[i] = num;
                    }
                }
            }
            return vectord;
        }

        public EData Evaluate(EData[] Pars)
        {
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Vector1D)
                {
                    FSError error = new FSError(2, "Type mismatch.-INVLID PARAMETER FOR ORDERING");
                    return error.ToEData();
                }
            }
            EData data2 = new EData();
            data2.Type = DataType.Vector1D;
            data2.Value = this.descending((Vector1D) Pars[0].Value);
            return data2;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Sort values of one dimensional values in descending order", new string[] { "Vector" }, new string[] { "One dimentional vector" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "DESCENDING";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "DSCD";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

