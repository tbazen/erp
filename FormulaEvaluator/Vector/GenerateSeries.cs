using System;
using System.Collections;
namespace INTAPS.Evaluator
{

    public class GenerateSeries : IFormulaFunction, IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private string[] m_Depedencies;
        private Symbolic m_Exp;
        private string m_ExpStr = "";
        private INTAPS.Evaluator.ISymbolProvider m_FP;
        private int m_ParCount = 4;

        public IVarParamCountFunction Clone()
        {
            GenerateSeries series = new GenerateSeries();
            series.ISymbolProvider = this.m_FP;
            return series;
        }

        public EData Evaluate(EData[] Pars)
        {
            int num5;
            string simpleVarName;
            EData data2;
            FSError error;
            double num = 1.0;
            if (this.m_ParCount > 4)
            {
                num = (double) Pars[4].Value;
            }
            double num2 = (double) Pars[2].Value;
            int n = (int) ((double) Pars[3].Value);
            string str = Pars[1].Value.ToString();
            string str2 = Pars[0].Value.ToString();
            if ((this.m_Exp == null) || (this.m_ExpStr != str2))
            {
                Symbolic symbolic = new Symbolic();
                symbolic.m_ISymbolProvider = this.m_FP;
                symbolic.Expression = str2;
                this.m_Exp = symbolic;
                ArrayList list = new ArrayList();
                for (num5 = 0; num5 < this.m_Exp.VarCount; num5++)
                {
                    simpleVarName = this.m_Exp.GetSimpleVarName(num5);
                    if (simpleVarName.ToUpper() != str.ToUpper())
                    {
                        list.Add(simpleVarName);
                    }
                }
                this.m_Depedencies = new string[list.Count];
                list.CopyTo(this.m_Depedencies);
                this.m_ExpStr = str2;
            }
            if (num == 0.0)
            {
                error = new FSError(0, "Step can't be 0.");
                return error.ToEData();
            }
            if (n < 0)
            {
                error = new FSError(0, "Size of series can't be negative.");
                return error.ToEData();
            }
            Vector1D vectord = new Vector1D(n);
            for (num5 = 0; num5 < this.m_Exp.VarCount; num5++)
            {
                simpleVarName = this.m_Exp.GetSimpleVarName(num5);
                if (simpleVarName.ToUpper() != str.ToUpper())
                {
                    this.m_Exp[simpleVarName] = this.m_FP.GetData(simpleVarName);
                }
            }
            if (num > 0.0)
            {
                double num4 = num2;
                for (num5 = 0; num5 < n; num5++)
                {
                    EData data = new EData();
                    data.Type = DataType.Float;
                    data.Value = num4;
                    this.m_Exp[str] = data;
                    vectord.VectorElements[num5] = (double) this.m_Exp.Evaluate().Value;
                    num4 += num;
                }
            }
            data2.Type = DataType.Vector1D;
            data2.Value = vectord;
            return data2;
        }

        public bool SetParCount(int n)
        {
            if (n < 4)
            {
                return false;
            }
            this.m_ParCount = n;
            return true;
        }

        public string[] Dependencies
        {
            get
            {
                return this.m_Depedencies;
            }
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculate the series ", new string[] { "string", "string", "Number", "Number" }, new string[] { "expresion", "variable", "expresion", "sequence limit" }, new string[] { "Mand", "Mand", "Mand", "Mand" }, "Composite Data function");
            }
        }

        public INTAPS.Evaluator.ISymbolProvider ISymbolProvider
        {
            set
            {
                this.m_FP = value;
            }
        }

        public string Name
        {
            get
            {
                return "Generate Series";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_ParCount;
            }
        }

        public string Symbol
        {
            get
            {
                return "Series";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

