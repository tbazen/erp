using System;
namespace INTAPS.Evaluator
{
    internal class Index2 : IVarParamCountFunction, IFunction, IFunctionDocumentation
    {
        private int m_nPars = 2;

        public IVarParamCountFunction Clone()
        {
            return new Index();
        }

        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            if (Pars[0].Type != DataType.Float)
            {
                error = new FSError("First parameter should be number for index2.");
                return error.ToEData();
            }
            int num = (int) ((double) Pars[0].Value);
            int index = Pars.Length - 1;
            switch (Pars[index].Type)
            {
                case DataType.Vector1D:
                {
                    if ((num < 1) || (num > ((Vector1D) Pars[index].Value).VectorElements.Length))
                    {
                        error = new FSError("Index out of bound.");
                        return error.ToEData();
                    }
                    EData data = new EData();
                    data.Value = ((Vector1D) Pars[index].Value).VectorElements[num - 1];
                    data.Type = DataType.Float;
                    return data;
                }
                case DataType.ListData:
                    if ((num < 1) || (num > ((ListData) Pars[index].Value).elements.Length))
                    {
                        error = new FSError("Index out of bound.");
                        return error.ToEData();
                    }
                    return ((ListData) Pars[index].Value).elements[num - 1];
            }
            error = new FSError("Can't index this data type");
            return error.ToEData();
        }

        public bool SetParCount(int n)
        {
            if (n < 2)
            {
                return false;
            }
            this.m_nPars = n;
            return true;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Returns an element from a list; implicitly called through visual basic array indexing syntax.", new string[] { "Index", "List" }, new string[] { "Index of the element to return.", "A list" }, new string[] { "Mand", "Mand" }, "List processing");
            }
        }

        public string Name
        {
            get
            {
                return "Index2";
            }
        }

        public int ParCount
        {
            get
            {
                return this.m_nPars;
            }
        }

        public string Symbol
        {
            get
            {
                return "Index2";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.Infix;
            }
        }
    }
}

