using System;
namespace INTAPS.Evaluator
{
    public class MedianF : IFunction, IFunctionDocumentation
    {
        public Vector1D ascending(Vector1D vect_1d)
        {
            int num2;
            Vector1D vectord = new Vector1D(vect_1d.dimension);
            for (num2 = 0; num2 < vectord.dimension; num2++)
            {
                vectord.VectorElements[num2] = vect_1d.VectorElements[num2];
            }
            for (num2 = 0; num2 < vectord.dimension; num2++)
            {
                for (int i = 0; i <= num2; i++)
                {
                    double num = vectord.VectorElements[num2];
                    if (num < vectord.VectorElements[i])
                    {
                        vectord.VectorElements[num2] = vectord.VectorElements[i];
                        vectord.VectorElements[i] = num;
                    }
                }
            }
            return vectord;
        }

        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            data.Type = DataType.Float;
            double num = 0.0;
            int index = 0;
            if (((Pars[0].Type != DataType.Vector1D) || (((Vector1D) Pars[0].Value).dimension == 1)) || (((Vector1D) Pars[0].Value).dimension == 1))
            {
                FSError error = new FSError(2, "the data type should be a vector or the dimension needs to be > 1");
                return error.ToEData();
            }
            Vector1D vectord = new Vector1D(((Vector1D) Pars[0].Value).dimension);
            vectord = (Vector1D) Pars[0].Value;
            vectord = this.ascending(vectord);
            if ((((Vector1D) Pars[0].Value).dimension % 2) != 0)
            {
                index = ((Vector1D) Pars[0].Value).dimension / 2;
                num = vectord.VectorElements[index];
            }
            else
            {
                index = (((Vector1D) Pars[0].Value).dimension / 2) - 1;
                num += vectord.VectorElements[index];
                num += vectord.VectorElements[index + 1];
                num /= 2.0;
            }
            data.Value = num;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates median of a vector", new string[] { "Vector" }, new string[] { "a Vector" }, new string[] { "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "MEDIAN";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "MEDIAN";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

