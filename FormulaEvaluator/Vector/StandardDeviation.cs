using System;
namespace INTAPS.Evaluator
{
    public class StandardDeviation : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            int num4;
            EData data = new EData();
            data.Type = DataType.Float;
            double d = 0.0;
            double num2 = 0.0;
            double num3 = 0.0;
            if (((Pars[0].Type != DataType.Vector1D) || (((Vector1D) Pars[0].Value).dimension == 0)) || (((Vector1D) Pars[0].Value).dimension == 1))
            {
                FSError error = new FSError(2, "the data type should be a vector or the dimension should be greater than zero / one");
                return error.ToEData();
            }
            for (num4 = 0; num4 < ((Vector1D) Pars[0].Value).dimension; num4++)
            {
                num2 += ((Vector1D) Pars[0].Value).VectorElements[num4];
            }
            for (num4 = 0; num4 < ((Vector1D) Pars[0].Value).dimension; num4++)
            {
                num3 = ((Vector1D) Pars[0].Value).VectorElements[num4] * ((Vector1D) Pars[0].Value).VectorElements[num4];
                d += num3;
            }
            d *= ((Vector1D) Pars[0].Value).dimension;
            d -= num2 * num2;
            d /= (double) (((Vector1D) Pars[0].Value).dimension * (((Vector1D) Pars[0].Value).dimension - 1));
            d = Math.Sqrt(d);
            data.Value = d;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Calculates the standard deviation of a vector", new string[] { "Vector" }, new string[] { "a Vector" }, new string[] { "Mand" }, "Mathematical function");
            }
        }

        public string Name
        {
            get
            {
                return "STANDARD_DEVIATION";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "STDEV";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

