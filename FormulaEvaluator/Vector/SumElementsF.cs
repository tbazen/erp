using System;
namespace INTAPS.Evaluator
{

    public class SumElementsF : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            EData data = new EData();
            data.Type = DataType.Float;
            double num = 0.0;
            if (Pars[0].Type != DataType.Vector1D)
            {
                FSError error = new FSError(2, "the data type should be a vector");
                return error.ToEData();
            }
            for (int i = 0; i < ((Vector1D) Pars[0].Value).dimension; i++)
            {
                num += ((Vector1D) Pars[0].Value).VectorElements[i];
            }
            data.Value = num;
            return data;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Adds the elements of a vector.", new string[] { "Vector" }, new string[] { "A one dimensional vector(Vector1D)" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "SUMELEMENTS";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "SumEL";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

