using System;
namespace INTAPS.Evaluator
{
    internal class ToVector : IFunction, IFunctionDocumentation
    {
        public EData Evaluate(EData[] Pars)
        {
            if (Pars[0].Type != DataType.ListData)
            {
                FSError error = new FSError("Type mismatch - to vector function.");
                return error.ToEData();
            }
            ListData data = (ListData) Pars[0].Value;
            Vector1D vectord = new Vector1D(data.elements.Length);
            int num = 0;
            foreach (EData data2 in data.elements)
            {
                if (data2.Type == DataType.Float)
                {
                    vectord[num] = (double) data2.Value;
                }
                else
                {
                    try
                    {
                        vectord[num] = double.Parse(data2.Value.ToString());
                    }
                    catch
                    {
                        vectord[num] = 0.0;
                    }
                }
                num++;
            }
            EData data3 = new EData();
            data3.Type = DataType.Vector1D;
            data3.Value = vectord;
            return data3;
        }

        public string Documentation
        {
            get
            {
                return FunctionDocumentation.CreateDocumentation(this.Symbol, "Changes list data to vector", new string[] { "List" }, new string[] { "a list" }, new string[] { "Mand" }, "Composite Data function");
            }
        }

        public string Name
        {
            get
            {
                return "Convert to Vector";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "ToVec";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

