using System;
using System.Reflection;
namespace INTAPS.Evaluator
{
    [Serializable]
    public class Vector1D : IName, IFunction
    {
        public int dimension;
        public string name;
        public double[] VectorElements;

        public Vector1D() : this(0)
        {
        }

        public Vector1D(int n)
        {
            this.name = "";
            this.dimension = n;
            this.VectorElements = new double[n];
            for (int i = 0; i < n; i++)
            {
                this.VectorElements[i] = 0.0;
            }
        }

        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            if (Math.Floor((double) Pars[0].Value) != ((double) Pars[0].Value))
            {
                error = new FSError(2, "Type mismatch.");
                return error.ToEData();
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            int num = (int) ((double) Pars[0].Value);
            if ((num < 1) || (num > this.dimension))
            {
                error = new FSError(5, "Vector Index Out ofBound.");
                return error.ToEData();
            }
            data2.Value = this.VectorElements[num - 1];
            return data2;
        }

        public static void Initialize()
        {
            EData.RegisterDataType(new Vector1DType());
        }

        public override string ToString()
        {
            string str = "";
            str = str + "[ ";
            for (int i = 0; i < this.dimension; i++)
            {
                str = str + this.VectorElements[i] + " ";
            }
            return (str + "]");
        }

        string IFunction.Name
        {
            get
            {
                return this.name.ToUpper();
            }
        }

        public double this[int i]
        {
            get
            {
                return this.VectorElements[i];
            }
            set
            {
                this.VectorElements[i] = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return this.name.ToUpper();
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

