using System;
namespace INTAPS.Evaluator
{

    internal class Vector1DType : IEDataType
    {
        private Vector1D m_ToSerialze;

        public object CreateObject()
        {
            return new Vector1D(0);
        }

        public int EndDeSerializeFromBin()
        {
            return 0;
        }

        public int GetByteCount()
        {
            return 0;
        }

        public void StartDeSerializeFromBin()
        {
        }

        public void StartSerializeToBin(object Target)
        {
            this.m_ToSerialze = (Vector1D) Target;
        }

        public string Name
        {
            get
            {
                return "Vector (1D)";
            }
        }

        public object Result
        {
            get
            {
                return this.m_ToSerialze;
            }
        }

        public DataType Type
        {
            get
            {
                return DataType.Vector1D;
            }
        }
    }
}

