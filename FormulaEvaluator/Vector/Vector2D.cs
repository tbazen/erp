using System;
using System.Reflection;
namespace INTAPS.Evaluator
{

    [Serializable]
    public class Vector2D : IName, IFunction
    {
        public int column;
        public string name;
        public int row;
        public double[,] VectorElements;

        public Vector2D() : this(0, 0)
        {
        }

        public Vector2D(int r, int c)
        {
            this.row = r;
            this.column = c;
            this.VectorElements = new double[r, c];
            for (int i = 0; i < r; i++)
            {
                for (int j = 0; j < c; j++)
                {
                    this.VectorElements[i, j] = 0.0;
                }
            }
        }

        public EData Evaluate(EData[] Pars)
        {
            FSError error;
            foreach (EData data in Pars)
            {
                if (data.Type != DataType.Float)
                {
                    error = new FSError(2, "Type mismatch.");
                    return error.ToEData();
                }
            }
            if (Math.Floor((double) Pars[0].Value) != ((double) Pars[0].Value))
            {
                error = new FSError(2, "Type mismatch.");
                return error.ToEData();
            }
            EData data2 = new EData();
            data2.Type = DataType.Float;
            int num = (int) ((double) Pars[0].Value);
            int num2 = (int) ((double) Pars[1].Value);
            if ((((num < 1) || (num > this.row)) || (num2 < 1)) || (num2 > this.column))
            {
                error = new FSError(5, "Vector Index Out ofBound.");
                return error.ToEData();
            }
            data2.Value = this.VectorElements[num - 1, num2 - 1];
            return data2;
        }

        public static void Initialize()
        {
            EData.RegisterDataType(new Vector2DType());
        }

        public override string ToString()
        {
            string str = " [ [  ";
            for (int i = 0; i < this.row; i++)
            {
                if (i != 0)
                {
                    str = str + " [ ";
                }
                for (int j = 0; j < this.column; j++)
                {
                    str = str + this.VectorElements[i, j] + "  ";
                }
                if (i != (this.row - 1))
                {
                    str = str + " ] ";
                }
            }
            return (str + " ] ] ");
        }

        string IFunction.Name
        {
            get
            {
                return this.name.ToUpper();
            }
        }

        public double this[int i, int j]
        {
            get
            {
                return this.VectorElements[i, j];
            }
            set
            {
                this.VectorElements[i, j] = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return this.name.ToUpper();
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

