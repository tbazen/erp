using System;
namespace INTAPS.Evaluator
{
    public class Vector2DType : IEDataType
    {
        private Vector2D m_ToSerialize;

        public object CreateObject()
        {
            return new Vector2D(0, 0);
        }

        public int EndDeSerializeFromBin()
        {
            return 0;
        }

        public int GetByteCount()
        {
            return 0;
        }

        public void StartDeSerializeFromBin()
        {
        }

        public void StartSerializeToBin(object Target)
        {
            this.m_ToSerialize = (Vector2D) Target;
        }

        public string Name
        {
            get
            {
                return "Vector(2D)";
            }
        }

        public object Result
        {
            get
            {
                return this.m_ToSerialize;
            }
        }

        public DataType Type
        {
            get
            {
                return DataType.Vector2D;
            }
        }
    }
}

