﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS
{
    public class ArrayExtension
    {
        public static T[] mergeArray<T>(T[] a1, params T[] a2)
        {
            if (a1 == null && a2 == null)
                return null;
            if (a1 == null)
                return clone(a2);
            if (a1 == null)
                return clone(a1);
            T[] ret = new T[a1.Length + a2.Length];
            Array.Copy(a1, ret, a1.Length);
            Array.Copy(a2, 0, ret, a1.Length, a2.Length);
            return ret;
        }
        
        public static T[] appendToArray<T>(T[] a, T e)
        {
            if (a == null)
                return new T[]{e};
            T[] ret = new T[a.Length + 1];
            Array.Copy(a, ret, a.Length);
            ret[a.Length] = e;
            return ret;
        }
        public static bool contains<T>(T[] a, T e)
        {
            if (a== null)
                return false;
            foreach (T el in a)
            {
                if (el.Equals(e))
                    return true;
            }
            return false;
        }
        public static bool contains<T>(T e, params T[] a)
        {
            foreach (T el in a)
            {
                if (el.Equals(e))
                    return true;
            }
            return false;
        }
        public static bool isNullOrEmpty(Array a)
        {
            return a == null || a.Length == 0;
        }
        public static bool arrayEqual<T>(T[] a, T[] b)
        {
            if (a == null && b == null)
                return true;
            if (a == null || b == null)
                return false;
            if (a.Length != b.Length)
                return false;
            for(int i=0;i<a.Length;i++)
                if(!a[i].Equals(b[i]))
                    return false;
            return true;
        }
        public static int indexOf<T>(T[] a, T e)
        {
            if (a == null)
                return -1;
            for(int i=0;i<a.Length;i++)
            {
                if (a[i].Equals(e))
                    return i;
            }
            return -1;
        }
        public static T[] removeByIndex<T>(T[] a, int index)
        {
            T[] ret = new T[a.Length - 1];
            if (index > 0)
                Array.Copy(a, 0, ret, 0, index);
            if (index < a.Length - 1)
                Array.Copy(a, index + 1, ret, index, a.Length - 1 - index);
            return ret;
        }
        public static T[] clone<T>(T[] a)
        {
            if (a == null)
                return null;
            T[] ret = new T[a.Length];
            a.CopyTo(ret,0);
            return ret;
        }
        public static T[] removeItem<T>(T[] a, T e)
        {
            
            int index = indexOf<T>(a, e);
            if (index == -1)
                return clone(a);
            return removeByIndex<T>(a, index);    
        }
    }

    public class WeakDictionary<KT, VT> : IDictionary<KT, VT> where VT : class
    {
        Dictionary<KT, VT> data = new Dictionary<KT, VT>();
        public void Add(KT key, VT value)
        {
            data.Add(key, value);
        }

        public bool ContainsKey(KT key)
        {
            return data.ContainsKey(key);
        }

        public ICollection<KT> Keys
        {
            get { return data.Keys; }
        }

        public bool Remove(KT key)
        {
            return data.Remove(key);
        }

        public bool TryGetValue(KT key, out VT value)
        {
            return data.TryGetValue(key, out value);
        }

        public ICollection<VT> Values
        {
            get { return data.Values; }
        }

        public VT this[KT key]
        {
            get
            {
                if (data.ContainsKey(key))
                    return data[key];
                return null;
            }
            set
            {
                if (data.ContainsKey(key))
                    data[key] = value;
                else
                    data.Add(key, value);
            }
        }

        public void Add(KeyValuePair<KT, VT> item)
        {
            data.Add(item.Key, item.Value);
        }

        public void Clear()
        {
            data.Clear();
        }

        public bool Contains(KeyValuePair<KT, VT> item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<KT, VT>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return data.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(KeyValuePair<KT, VT> item)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<KeyValuePair<KT, VT>> GetEnumerator()
        {
            return data.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return data.GetEnumerator();
        }
    }
}
