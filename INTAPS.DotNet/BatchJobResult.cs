
    using System;
namespace INTAPS.UI
{
    public enum BatchJobResultType
    {
        Ok,
        Error,
        Warning,
    }

    [Serializable]
    public class BatchJobResult
    {
        public Exception ex;
        public string item;
        public string itemKey;
        public const int RESULT_CUSTOM_LEVEL_BASE = 0x10000000;
        public const int RESULT_ERROR = -1;
        public const int RESULT_INFORMATION = 1;
        public const int RESULT_NORMAL_WARNING = -2;
        public const int RESULT_OK = 0;
        public const int RESULT_SEVER_WARNNG = -3;
        public BatchJobResultType resultType;
        public string statusDescription;

        public BatchJobResult(string key, string item, string statusText, Exception ex) : this(key, item, statusText, ex, (ex == null) ? BatchJobResultType.Ok : BatchJobResultType.Error)
        {
        }

        public BatchJobResult(string key, string item, string statusText, Exception ex, BatchJobResultType resultType)
        {
            this.itemKey = key;
            this.item = item;
            this.statusDescription = statusText;
            this.ex = ex;
            this.resultType = resultType;
        }
    }
}

