
    using System;
namespace INTAPS.UI
{

    public enum BatchJobStage
    {
        Idle,
        Running,
        Suspended,
        Error,
        Done
    }
}

