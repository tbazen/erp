using System;
namespace INTAPS.UI
{

    [Serializable]
    public class BatchJobStatus
    {
        public BatchJobResult[] newResults;
        public double progress;
        public int resultIndex;
        public object retData;
        public BatchJobStage stage;
        public string statusText;
    }
}

