﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS
{
    public class CallTimer
    {
        int count = 0;
        public long totalTime = 0;
        long t1;
        public int nCall
        {
            get
            {
                return count / 2;
            }
        }
        public bool isBalanced
        {
            get
            {
                return count % 2 == 0;
            }
        }
        public void start()
        {
            count++;
            t1 = DateTime.Now.Ticks;
        }
        public void stop()
        {
            count++;
            totalTime += DateTime.Now.Ticks - t1;
        }
        public override string ToString()
        {
            return "{0} call {1} ticks".format(nCall, totalTime);
        }
    }
}
