using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace INTAPS.Ethiopic
{
    [StructLayout(LayoutKind.Sequential)]
    public struct EtGrDate
    {
        private const int c1 = 0x23ab1;
        private const int c2 = 0x8eac;
        private const int c3 = 0x5b5;
        private const int dd = 0x97e;
        public int Day;
        public int Month;
        public int Year;
        static Dictionary<String, String> englishMonthNameList = new Dictionary<string, string>();
        static Dictionary<String, String> afanoOromoMonthNameList = new Dictionary<string, string>();
        static void initEnglish()
        {
            englishMonthNameList.Add("ET_MONTH_1", "Meskerem");
            englishMonthNameList.Add("ET_MONTH_10", "Sene");
            englishMonthNameList.Add("ET_MONTH_11", "Hamle");
            englishMonthNameList.Add("ET_MONTH_12", "Nehase");
            englishMonthNameList.Add("ET_MONTH_13", "Pagume");
            englishMonthNameList.Add("ET_MONTH_2", "Tikimt");
            englishMonthNameList.Add("ET_MONTH_3", "Hidar");
            englishMonthNameList.Add("ET_MONTH_4", "Tahisas");
            englishMonthNameList.Add("ET_MONTH_5", "Tir");
            englishMonthNameList.Add("ET_MONTH_6", "Yekatit");
            englishMonthNameList.Add("ET_MONTH_7", "Megabit");
            englishMonthNameList.Add("ET_MONTH_8", "Miyazia");
            englishMonthNameList.Add("ET_MONTH_9", "Ginbot");
        }
        static void initAfanOromo()
        {
            afanoOromoMonthNameList.Add("ET_MONTH_1", "Fulb");
            afanoOromoMonthNameList.Add("ET_MONTH_10", "Waxa");
            afanoOromoMonthNameList.Add("ET_MONTH_11", "Adol");
            afanoOromoMonthNameList.Add("ET_MONTH_12", "Haga");
            afanoOromoMonthNameList.Add("ET_MONTH_13", "Pagume");
            afanoOromoMonthNameList.Add("ET_MONTH_2", "Onko");
            afanoOromoMonthNameList.Add("ET_MONTH_3", "Sada");
            afanoOromoMonthNameList.Add("ET_MONTH_4", "Mude");
            afanoOromoMonthNameList.Add("ET_MONTH_5", "Amaj");
            afanoOromoMonthNameList.Add("ET_MONTH_6", "Gura");
            afanoOromoMonthNameList.Add("ET_MONTH_7", "Bito");
            afanoOromoMonthNameList.Add("ET_MONTH_8", "Ebla");
            afanoOromoMonthNameList.Add("ET_MONTH_9", "Caam");
        }
        static Dictionary<String,String>[] monthNamesLists = new Dictionary<String, String>[]
        {
            englishMonthNameList
            ,afanoOromoMonthNameList
        };
        static EtGrDate()
        {
            initEnglish();
            initAfanOromo();
        }
            
        private static int Months(int index)
        {
            switch (index)
            {
                case 0:
                case 2:
                case 4:
                case 6:
                case 7:
                case 9:
                case 11:
                    return 0x1f;

                case 1:
                    return 0x1c;
            }
            return 30;
        }

        private static int AddMonths(int m, int y)
        {
            int num2 = 0;
            for (int i = 0; i < (m - 1); i++)
            {
                num2 += Months(i);
                if ((i == 1) && IsLeapYearGr(y))
                {
                    num2++;
                }
            }
            return num2;
        }

        private static void GetMonthAndDayGrig(int n, int y, ref int m, ref int d)
        {
            int num = 1;
            while (n >= GetMonthLengthGrig(num, y))
            {
                n -= GetMonthLengthGrig(num, y);
                num++;
            }
            m = num;
            d = n + 1;
        }

        public static int GetMonthLengthGrig(int m, int y)
        {
            if ((m == 2) && IsLeapYearGr(y))
            {
                return 0x1d;
            }
            return Months(m - 1);
        }

        public static int GetMonthLengthEt(int m, int y)
        {
            if (m == 13)
            {
                return (IsLeapYearEt(y) ? 6 : 5);
            }
            return 30;
        }

        public static bool IsLeapYearGr(int y)
        {
            return (((y % 4) == 0) && (((y % 100) != 0) || ((y % 400) == 0)));
        }

        public int DayNoEt
        {
            get
            {
                int num = this.Year / 4;
                int num2 = this.Year % 4;
                return (((((num * 0x5b5) + (num2 * 0x16d)) + ((this.Month - 1) * 30)) + this.Day) - 1);
            }
        }
        public int GetDayOfWeekEt
        {
            get
            {
                return (((this.DayNoEt + 1) % 7) + 1);
            }
        }
        public EtGrDate(int dn, bool Ethiopian)
        {
            int num;
            int num2;
            int num3;
            int num4;
            if (Ethiopian)
            {
                num = dn / 0x5b5;
                num2 = dn % 0x5b5;
                num3 = num2 / 0x16d;
                num4 = num2 % 0x16d;
                if (num2 != 0x5b4)
                {
                    this.Year = (num * 4) + num3;
                    this.Month = (num4 / 30) + 1;
                    this.Day = (num4 % 30) + 1;
                }
                else
                {
                    this.Year = ((num * 4) + num3) - 1;
                    this.Month = 13;
                    this.Day = 6;
                }
            }
            else
            {
                num = dn / 0x23ab1;
                num2 = dn % 0x23ab1;
                num3 = num2 / 0x8eac;
                num4 = num2 % 0x8eac;
                int num5 = num4 / 0x5b5;
                int num6 = num4 % 0x5b5;
                int num7 = num6 / 0x16d;
                int n = num6 % 0x16d;
                this.Year = ((((num * 400) + (num3 * 100)) + (num5 * 4)) + num7) + 1;
                this.Month = 0;
                this.Day = 0;
                GetMonthAndDayGrig(n, this.Year, ref this.Month, ref this.Day);
            }
        }

        public int DayNoGrig
        {
            get
            {
                int num = (this.Year - 1) / 400;
                int num2 = (this.Year - 1) % 400;
                int num3 = num2 / 100;
                int num4 = num2 % 100;
                int num5 = num4 / 4;
                int num6 = num4 % 4;
                int num7 = AddMonths(this.Month, this.Year);
                return (((((((num * 0x23ab1) + (num3 * 0x8eac)) + (num5 * 0x5b5)) + (num6 * 0x16d)) + num7) + this.Day) - 1);
            }
        }
        public int DayOfWeekGrig
        {
            get
            {
                return ((this.DayNoGrig % 7) + 1);
            }
        }
        public EtGrDate(int d, int m, int y)
        {
            this.Day = d;
            this.Month = m;
            this.Year = y;
        }

        public EtGrDate(DateTime dt)
        {
            this.Day = dt.Day;
            this.Month = dt.Month;
            this.Year = dt.Year;
        }

        public DateTime GridDate
        {
            get
            {
                return new DateTime(this.Year, this.Month, this.Day);
            }
        }
        public static EtGrDate ToEth(EtGrDate gr)
        {
            return new EtGrDate(gr.DayNoGrig - 0x97e, true);
        }

        public static EtGrDate ToEth(DateTime dt)
        {
            return ToEth(new EtGrDate(dt));
        }

        public static string ToEthWithTime(DateTime dt)
        {
            return (ToEth(dt).ToString() + " " + dt.ToShortTimeString());
        }

        public static EtGrDate ToGrig(EtGrDate et)
        {
            return new EtGrDate(et.DayNoEt + 0x97e, false);
        }

        public static string GetEtMonthName(int m)
        {
            switch (m)
            {
                case 1:
                    return "መስከረም";

                case 2:
                    return "ጥቅምት";

                case 3:
                    return "ህዳር";

                case 4:
                    return "ታህሳስ";

                case 5:
                    return "ጥር";

                case 6:
                    return "የካቲት";

                case 7:
                    return "መጋቢት";

                case 8:
                    return "ሚያዚያ";

                case 9:
                    return "ግንቦት";

                case 10:
                    return "ሰኔ";

                case 11:
                    return "ሐምሌ";

                case 12:
                    return "ነሀሴ";

                case 13:
                    return "ጳጉሜ";
            }
            return "";
        }

        public static string GetEtMonthNameEng(int m)
        {
            switch (m)
            {
                case 1:
                    return "Meskerem";

                case 2:
                    return "Tikimt";

                case 3:
                    return "Hidar";

                case 4:
                    return "Tahisas";

                case 5:
                    return "Tir";

                case 6:
                    return "Yekatit";

                case 7:
                    return "Megabit";

                case 8:
                    return "Miyazia";

                case 9:
                    return "Ginbot";

                case 10:
                    return "Sene";

                case 11:
                    return "Hamle";

                case 12:
                    return "Nehase";

                case 13:
                    return "Pagume";
            }
            return "";
        }
        public static string GetEtMonthNameEng(int m, int langagueID)
        {
            return monthNamesLists[langagueID]["ET_MONTH_" + m];
        }
        public static string GetDayOfWeekNameEt(int d)
        {
            switch (d)
            {
                case 1:
                    return "ሰኞ";

                case 2:
                    return "ማክሰኞ";

                case 3:
                    return "ረቡዕ";

                case 4:
                    return "ሀሙስ";

                case 5:
                    return "አርብ";

                case 6:
                    return "ቅዳሜ";

                case 7:
                    return "እሁድ";
            }
            return "";
        }

        public static string GetGrigMonthName(int m)
        {
            switch (m)
            {
                case 1:
                    return "January";

                case 2:
                    return "February";

                case 3:
                    return "March";

                case 4:
                    return "April";

                case 5:
                    return "May";

                case 6:
                    return "June";

                case 7:
                    return "July";

                case 8:
                    return "August";

                case 9:
                    return "September";

                case 10:
                    return "October";

                case 11:
                    return "Novomber";

                case 12:
                    return "December";
            }
            return "";
        }

        public static string GetDayOfWeekNameGrig(int d)
        {
            switch (d)
            {
                case 1:
                    return "Mon";

                case 2:
                    return "Tue";

                case 3:
                    return "Wed";

                case 4:
                    return "Thu";

                case 5:
                    return "Fri";

                case 6:
                    return "Sat";

                case 7:
                    return "Sun";
            }
            return "";
        }

        public static bool IsLeapYearEt(int y)
        {
            return ((y % 4) == 3);
        }

        public override string ToString()
        {
            return (this.Day.ToString("00") + "/" + this.Month.ToString("00") + "/" + this.Year.ToString("0000"));
        }

        public static EtGrDate Parse(string date)
        {
            string[] strArray = date.Split(new char[] { '\\', '/', ' ', ',' });
            if (strArray.Length < 3)
            {
                throw new Exception("Invalid date fromat");
            }
            int d = int.Parse(strArray[0]);
            int m = int.Parse(strArray[1]);
            return new EtGrDate(d, m, int.Parse(strArray[2]));
        }
        public static bool TryParse(string str, bool ethiopian, out EtGrDate date)
        {
            string[] strArray = str.Split(new char[] { '\\', '/', ' ', ',' });
            date = new EtGrDate();
            if (strArray.Length != 3)
            {
                return false;
            }

            int d;
            int m;
            int y;
            if (!int.TryParse(strArray[0], out d))
                return false;
            if (!int.TryParse(strArray[1], out m))
                return false;
            if (!int.TryParse(strArray[2], out y))
                return false;
            if (ethiopian)
            {

                if (m < 1 || d < 1 || y < 1800)
                    return false;
                if (m < 13)
                {
                    if (d < 1 || d > 30)
                        return false;
                }
                if (m == 13)
                {
                    if (IsLeapYearEt(y) && d > 6)
                        return false;
                    if (d > 5)
                        return false;
                }
                if (m > 13)
                    return false;
                if (y > 2100)
                    return false;
            }
            date = new EtGrDate(d, m, y);
            return true;
        }
        public static bool IsValidDMY(int day, int month, int year, bool _eth)
        {
            if ((year < 0x3e8) || (year > 0xbb8))
            {
                return false;
            }
            if (month < 1)
            {
                return false;
            }
            if (day < 1)
            {
                return false;
            }
            if (_eth)
            {
                if (month > 13)
                {
                    return false;
                }
                if (day > GetMonthLengthEt(month, year))
                {
                    return false;
                }
            }
            else
            {
                if (month > 12)
                {
                    return false;
                }
                if (day > GetMonthLengthGrig(month, year))
                {
                    return false;
                }
            }
            return true;
        }
    }
}

