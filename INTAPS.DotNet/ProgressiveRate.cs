using System;
using System.Collections.Generic;
using System.Text;
namespace INTAPS.UI
{
    [Serializable]
    public class ProgressiveRateItem
    {
        public RangeType type;
        public double v1, v2;
        public double rate;

        public bool overlapsWith(ProgressiveRateItem other)
        {
            if (other == null)
                return false;
            switch (type)
            {
                case RangeType.LowerBound:
                    switch (other.type)
                    {
                        case RangeType.LowerBound:
                            return true;
                        case RangeType.UpperBound:
                            return other.v2> this.v1;
                        case RangeType.UpperAndLowerBound:
                            return other.v1> this.v1 || other.v2> this.v1;
                    }
                    break;
                case RangeType.UpperBound:
                    switch (other.type)
                    {
                        case RangeType.LowerBound:
                            return this.v2>other.v1;
                        case RangeType.UpperBound:
                            return true;
                        case RangeType.UpperAndLowerBound:
                            return other.v1< this.v1 || other.v2<this.v1;
                    }
                    break;
                case RangeType.UpperAndLowerBound:
                    switch (other.type)
                    {
                        case RangeType.LowerBound:
                            return this.v1> other.v1 || this.v2>other.v1;
                        case RangeType.UpperBound:
                            return this.v1<other.v1 || this.v2< other.v1;
                        case RangeType.UpperAndLowerBound:
                            return (this.v1<=other.v1 && this.v2>other.v1)
                                || (other.v1 <= this.v1 && other.v2 > this.v1);
                    }
                    break;
            }
            return false;
        }
    }
    [Serializable]
    public class ProgressiveRate
    {
        public ProgressiveRateItem[] items = new ProgressiveRateItem[0];
        public ProgressiveRate()
            : this(0)
        {
        }
        public ProgressiveRate(int nitems)
        {
            items = new ProgressiveRateItem[nitems];
        }
        public double evaluate(double value)
        {
            double total = 0;
            for (int i = 0; i < items.Length; i++)
            {
                double v1 = items[i].v1;
                double v2 = items[i].v2;
                switch (items[i].type)
                {
                    case RangeType.UpperBound:
                        if (value < v2)
                        {
                            total += (v2 - value) * items[i].rate;
                        }
                        break;
                    case RangeType.LowerBound:
                        if (value > v1)
                        {
                            total += (value - v1) * items[i].rate;
                        }
                        break;
                    case RangeType.UpperAndLowerBound:
                        if (value >= v1 && value < v2)
                        {
                            total += (value - v1) * items[i].rate;
                        }
                        else if (value >= v1)
                        {
                            total += (v2- v1) * items[i].rate;
                        }

                            
                        break;
                    default:
                        break;
                }

            }
            return total;
        }
        public double findRate(double value,out bool found)
        {
            found = true;
            for (int i = 0; i < items.Length; i++)
            {
                double v1 = items[i].v1;
                double v2 = items[i].v2;
                switch (items[i].type)
                {
                    case RangeType.UpperBound:
                        if (value < v2)
                            return items[i].rate;
                        break;
                    case RangeType.LowerBound:
                        if (value >= v1)
                            return items[i].rate;
                        break;
                    case RangeType.UpperAndLowerBound:
                        if (value >= v1 && value < v2)
                           return items[i].rate;
                        break;
                }

            }
            found = false;
            return 0;
        }
    }
}
