using System;
using System.Collections.Generic;
using System.Text;
namespace INTAPS.UI
{
    public enum RangeType
    {
        LowerBound = 1,
        UpperBound = 2,
        UpperAndLowerBound = 3
    }
}
