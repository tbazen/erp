﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace INTAPS
{
    public class SerializationExtensions
    {
        public static T loadFromBytes<T>(byte[] bytes)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream serializationStream = new MemoryStream(bytes);
            return (T)formatter.Deserialize(serializationStream);
        }
        public static T loadFromXml<T>(string xmaliziedString)
        {
            return (T)loadFromXml(typeof(T), xmaliziedString);
        }

        class XmlSerializerCacheItem
        {
            public XmlSerializer serializser;
            public Type[] includeTypes = null;
        }
        static Dictionary<Type, XmlSerializerCacheItem> serializserCache = new Dictionary<Type, XmlSerializerCacheItem>();
        public static XmlSerializer getSerializer(Type t, params Type[] includeTypes)
        {
            lock (serializserCache)
            {
                if (serializserCache.ContainsKey(t))
                {
                    XmlSerializerCacheItem item = serializserCache[t];
                    bool subset=true;
                    if (includeTypes != null)
                    {
                        if (item.includeTypes == null)
                            subset = false;
                        else
                        {
                            subset = true;
                            foreach (Type it in includeTypes)
                            {
                                bool found = false;
                                foreach (Type st in item.includeTypes)
                                {
                                    if (st.Equals(it))
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    subset = false;
                                    break;
                                }
                            }
                        }
                    }
                    if (!subset)
                    {
                        item.serializser = includeTypes.Length==0? new XmlSerializer(t):new XmlSerializer(t, includeTypes);
                        item.includeTypes = includeTypes;
                        //Console.WriteLine("XML Serializer: replacing serializer because of extra type mismatch.");
                    }
                    return item.serializser;
                }
                else
                {
                    XmlSerializerCacheItem item = new XmlSerializerCacheItem();
                    item.serializser = includeTypes.Length == 0 ? new XmlSerializer(t) : new XmlSerializer(t, includeTypes);
                    item.includeTypes = includeTypes;
                    serializserCache.Add(t, item);
                    //Console.WriteLine("XML Serializer: serializser added Type :{0}", t);
                    return item.serializser;
                }
            }
        }
        public static object loadFromXml(Type t, string xmaliziedString,params Type [] includeTypes)
        {
            XmlSerializer serializer = getSerializer(t,includeTypes);
            using (StringReader reader = new StringReader(xmaliziedString))
            {
                object ret=serializer.Deserialize(reader);
                reader.Close();
                return ret;
            }
        }
        public static string getXml(object obj,params Type[] includeTypes)
        {
            if (obj == null)
                return null;
            XmlSerializer serializer = getSerializer(obj.GetType(), includeTypes);
            try
            {
                using (StringWriter writer = new StringWriter())
                {
                    serializer.Serialize(writer, obj);
                    writer.Close();
                    return writer.ToString();
                }

            }
            catch (Exception)
            {
                return null;
            }
        }
        public static byte[] getBytes(object obj)
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream serializationStream = new MemoryStream();
                formatter.Serialize(serializationStream, obj);
                byte[] buffer = new byte[serializationStream.Length];
                serializationStream.Seek(0L, SeekOrigin.Begin);
                serializationStream.Read(buffer, 0, buffer.Length);
                return buffer;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
