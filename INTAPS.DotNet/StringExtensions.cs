﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS
{
    public static class StringExtensions
    {
        public static string format(this string str,params object [] pars)
        {
            return string.Format(str, pars);
        }
        public static string AppendOperand(string exp, string op, string operand)
        {
            if (string.IsNullOrEmpty(exp))
                return operand;
            return string.Format("{0} {1} ({2})", exp, op, operand);
        }
    }
}
