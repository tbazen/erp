﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS
{
    public static class TypeTools
    {
        static object _castWithDefault<T>(object val)
        {
            if (val is T)
                return (T)val;
            if (typeof(T) == typeof(int))
                return (int)-1;
            if (typeof(T) == typeof(short))
                return (short)-1;
            if (typeof(T) == typeof(long))
                return (long)-1;
            if (typeof(T) == typeof(DateTime))
                return DateTime.Now;
            if (typeof(T) == typeof(double))
                return (double)-1;
            return default(T);
        }
        public static T castWithDefault<T>(object val)
        {
            return (T)_castWithDefault<T>(val);
        }
    }
}
