﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace BIZNET.iERP
{
    public enum TDType
    {
        body,
        ColHeader,
        RowHeader
    }
    public enum TableRowGroupType
    {
        body,
        header,
        footer
    }
    public class bERPHtmlElement
    {
        public string id = null;
        public string name = null;
        public string css=null;
        public List<string> styles = new List<string>();
        protected string appendStyles(string attributes)
        {
            if (!string.IsNullOrEmpty(css))
                attributes+= " class='" + css + "'";
            if (!string.IsNullOrEmpty(name))
                attributes += " name='" + name+ "'";
            if (!string.IsNullOrEmpty(id))
                attributes += " id='" + id+ "'";
            attributes += " style='";
            foreach (string style in styles)
            {
                attributes += style + ";";
            }
            attributes += "'";

            return attributes;
        }
    }

    public class bERPHtmlTableCell : bERPHtmlElement
    {
        public TDType type;
        public string innerHtml;
        public int rowSpan;
        public int colSpan;
        public string innerText
        {
            get
            {
                return HttpUtility.HtmlDecode(innerHtml);
            }
            set
            {
                innerHtml = bERPHtmlBuilder.HtmlEncode(innerHtml);
            }
        }
        public bERPHtmlTableCell(TDType type, string text, string css, int rowSpan, int colSpan)
        {
            this.type = type;
            this.css = css;
            this.innerHtml = bERPHtmlBuilder.HtmlEncode(text);
            this.rowSpan = rowSpan;
            this.colSpan = colSpan;
        }
        public bERPHtmlTableCell(string text)
            : this(TDType.body, text, null, 1, 1)
        {
        }
        public bERPHtmlTableCell(string text, int colSpan)
            : this(TDType.body, text, null, 1, colSpan)
        {
        }
        public bERPHtmlTableCell(string text, string css)
            : this(TDType.body, text, css, 1, 1)
        {
        }

        public bERPHtmlTableCell(TDType type, string text)
            : this(type, text, null, 1, 1)
        {
        }
        public bERPHtmlTableCell(TDType type, string text, int colSpan)
            : this(type, text, null, 1, colSpan)
        {
        }
        public bERPHtmlTableCell(TDType type, string text, string css)
            : this(type, text, css, 1, 1)
        {
        }

        public string getOuterHtml()
        {
            string attributes = "";

            if (rowSpan > 1)
                attributes += " rowspan='" + rowSpan + "'";
            if (colSpan > 1)
                attributes += " colspan='" + colSpan + "'";

                attributes = appendStyles(attributes);
            
            switch (type)
            {
                case TDType.body:
                    return string.Format("<td {0}>{1}</td>", attributes, innerHtml);
                case TDType.ColHeader:
                    attributes += " scope='col'";
                    return string.Format("<th {0}>{1}</th>", attributes, innerHtml);
                default:
                    attributes += " scope='row'";
                    return string.Format("<th {0}>{1}</th>", attributes, innerHtml);
            }

        }

        
    }
    public class bERPHtmlTableRow:bERPHtmlElement
    {
        public List<bERPHtmlTableCell> cells = new List<bERPHtmlTableCell>();
        public void build(StringBuilder builder)
        {
            string attribute = "";
            attribute = appendStyles(attribute);
            builder.Append(string.Format("<tr {0}>", attribute));
            foreach (bERPHtmlTableCell cell in cells)
                builder.Append(cell.getOuterHtml());
            builder.Append("</tr>");
        }
        public bERPHtmlTableRow(params bERPHtmlTableCell[] cells)
        {
            this.cells.AddRange(cells);
        }
    }
    public class bERPHtmlTableRowGroup
    {
        public TableRowGroupType type = TableRowGroupType.body;
        public bERPHtmlTableRowGroup(TableRowGroupType type)
        {
            this.type = type;
        }
        public bERPHtmlTableRow htmlAddRow(params bERPHtmlTableCell[] cells)
        {
            bERPHtmlTableRow ret = new bERPHtmlTableRow();
            ret.cells.AddRange(cells);
            this.rows.Add(ret);
            return ret;
        }
        public bERPHtmlTableRow htmlAddRow(int index,params bERPHtmlTableCell[] cells)
        {
            bERPHtmlTableRow ret = new bERPHtmlTableRow();
            ret.cells.AddRange(cells);
            this.rows.Insert(index,ret);
            return ret;
        }

        public List<bERPHtmlTableRow> rows = new List<bERPHtmlTableRow>();
        public void build(StringBuilder builder)
        {
            switch (type)
            {
                case TableRowGroupType.body:
                    builder.Append("<tbody>");
                    break;
                case TableRowGroupType.header:
                    builder.Append("<thead>");
                    break;
                case TableRowGroupType.footer:
                    builder.Append("<tfoot>");
                    break;
                default:
                    break;
            }

            foreach (bERPHtmlTableRow row in rows)
                row.build(builder);
            switch (type)
            {
                case TableRowGroupType.body:
                    builder.Append("</tbody>");
                    break;
                case TableRowGroupType.header:
                    builder.Append("</thead>");
                    break;
                case TableRowGroupType.footer:
                    builder.Append("</tfoot>");
                    break;
                default:
                    break;
            }
        }
    }
    public class bERPHtmlTable : bERPHtmlElement
    {

        public List<bERPHtmlTableRowGroup> groups = new List<bERPHtmlTableRowGroup>();
        public void build(StringBuilder builder)
        {
            string attributes = "";
            if (!string.IsNullOrEmpty(css))
                attributes = " class='" + css + "'";
            if (!string.IsNullOrEmpty(id))
                attributes += " id='" + id + "'";
            attributes = appendStyles(attributes);

            builder.Append("<table " + attributes + ">");
            foreach (bERPHtmlTableRowGroup g in groups)
                g.build(builder);
            builder.Append("</table>");
        }
        public bERPHtmlTableRowGroup createBodyGroup()
        {
            bERPHtmlTableRowGroup body = new bERPHtmlTableRowGroup(TableRowGroupType.body);
            groups.Add(body);
            return body;
        }
    }
    public  class bERPHtmlBuilder
    {
        public static bERPHtmlTableRow htmlAddRow(bERPHtmlTableRowGroup rows, params bERPHtmlTableCell[] cells)
        {
            bERPHtmlTableRow ret = new bERPHtmlTableRow();
            ret.cells.AddRange(cells);
            rows.rows.Add(ret);
            return ret;
        }
        public static bERPHtmlTableRow htmlAddRow(String css, params bERPHtmlTableCell[] cells)
        {
            bERPHtmlTableRow ret = new bERPHtmlTableRow();
            ret.css = css;
            ret.cells.AddRange(cells);
            return ret;
        }
        public static void htmlBuildTable(StringBuilder builder, List<List<string>> rows, int headerCount, int footerCount)
        {
            int i;
            builder.Append("<table>");
            if (headerCount > 0)
            {
                i = 0;
                builder.Append("<thead>");
                while (i < headerCount)
                {
                    builder.Append("<tr>");
                    foreach (string cell in rows[i])
                        builder.Append(cell);
                    builder.Append("</tr>");
                    i++;
                }
                builder.Append("</thead>");
            }
            if (footerCount > 0)
            {
                i = 0;
                builder.Append("<tfoot>");
                while (i < footerCount)
                {
                    builder.Append("<tr>");
                    foreach (string cell in rows[(rows.Count - headerCount - footerCount) + i])
                        builder.Append(cell);
                    builder.Append("</tr>");
                    i++;
                }
                builder.Append("</tfoot>");
            }
            if (rows.Count - headerCount - footerCount > 0)
            {
                i = 0;
                builder.Append("<tobdy>");
                while (i < rows.Count - headerCount - footerCount)
                {
                    builder.Append("<tr>");
                    foreach (string cell in rows[headerCount + i])
                        builder.Append(cell);
                    builder.Append("</tr>");
                    i++;
                }
                builder.Append("</tobdy>");
            }
            builder.Append("</table>");
        }

        public static string HtmlEncode(string text)
        {
            string html = HttpUtility.HtmlEncode(text);
            if(html!=null)
                return html.Replace("\n", "<br/>");
            return html;
        }

        public static string ToString(bERPHtmlTable tableFinancial)
        {
            var sb = new StringBuilder();
            tableFinancial.build(sb);
            return sb.ToString();
        }
        public static string ToString(bERPHtmlTableRow tableFinancial)
        {
            var sb = new StringBuilder();
            tableFinancial.build(sb);
            return sb.ToString();
        }
    }

}
