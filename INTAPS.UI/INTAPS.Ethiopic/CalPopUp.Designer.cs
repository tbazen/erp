﻿namespace INTAPS.Ethiopic
{
    public partial class CalPopUp : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CalPopUp));
            this.cmbMonth = new System.Windows.Forms.ComboBox();
            this.pnlDates = new System.Windows.Forms.Panel();
            this.pnlSelector = new System.Windows.Forms.Panel();
            this.radET = new System.Windows.Forms.RadioButton();
            this.txtYear = new System.Windows.Forms.NumericUpDown();
            this.radGr = new System.Windows.Forms.RadioButton();
            this.ImLst = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblGR = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlSelector.SuspendLayout();
            this.txtYear.BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.pictureBox1).BeginInit();
            base.SuspendLayout();
            this.cmbMonth.AllowDrop = true;
            this.cmbMonth.BackColor = System.Drawing.SystemColors.Control;
            this.cmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMonth.Font = new System.Drawing.Font("Nyala", 8.25f);
            this.cmbMonth.Location = new System.Drawing.Point(0x3a, 0);
            this.cmbMonth.MaxDropDownItems = 13;
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.Size = new System.Drawing.Size(0x69, 0x15);
            this.cmbMonth.TabIndex = 2;
            this.cmbMonth.SelectedIndexChanged += new System.EventHandler(this.cmbMonth_SelectedIndexChanged);
            this.pnlDates.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlDates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDates.Location = new System.Drawing.Point(0, 0x18);
            this.pnlDates.Name = "pnlDates";
            this.pnlDates.Size = new System.Drawing.Size(230, 0x7e);
            this.pnlDates.TabIndex = 4;
            this.pnlDates.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlDates_Paint);
            this.pnlSelector.Controls.Add(this.cmbMonth);
            this.pnlSelector.Controls.Add(this.radET);
            this.pnlSelector.Controls.Add(this.txtYear);
            this.pnlSelector.Controls.Add(this.radGr);
            this.pnlSelector.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSelector.Location = new System.Drawing.Point(0, 0);
            this.pnlSelector.Name = "pnlSelector";
            this.pnlSelector.Size = new System.Drawing.Size(230, 0x18);
            this.pnlSelector.TabIndex = 10;
            this.pnlSelector.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlSelector_Paint);
            this.radET.BackColor = System.Drawing.SystemColors.Control;
            this.radET.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radET.Font = new System.Drawing.Font("Tahoma", 6f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            this.radET.Location = new System.Drawing.Point(0, 1);
            this.radET.Name = "radET";
            this.radET.Size = new System.Drawing.Size(70, 10);
            this.radET.TabIndex = 0;
            this.radET.Text = "Ethiopian";
            this.radET.UseVisualStyleBackColor = false;
            this.radET.CheckedChanged += new System.EventHandler(this.radET_Clicked);
            this.txtYear.BackColor = System.Drawing.SystemColors.Control;
            this.txtYear.Location = new System.Drawing.Point(0xa3, 0);
            int[] bits = new int[4];
            bits[0] = 0x270f;
            this.txtYear.Maximum = new decimal(bits);
            bits = new int[4];
            bits[0] = 1;
            this.txtYear.Minimum = new decimal(bits);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(0x45, 20);
            this.txtYear.TabIndex = 3;
            bits = new int[4];
            bits[0] = 0x7d0;
            this.txtYear.Value = new decimal(bits);
            this.txtYear.ValueChanged += new System.EventHandler(this.txtYear_ValueChanged);
            this.radGr.BackColor = System.Drawing.SystemColors.Control;
            this.radGr.Checked = true;
            this.radGr.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.radGr.Font = new System.Drawing.Font("Tahoma", 6f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            this.radGr.Location = new System.Drawing.Point(0, 11);
            this.radGr.Name = "radGr";
            this.radGr.Size = new System.Drawing.Size(0x4e, 10);
            this.radGr.TabIndex = 1;
            this.radGr.TabStop = true;
            this.radGr.Text = "Grigorian";
            this.radGr.UseVisualStyleBackColor = false;
            this.radGr.CheckedChanged += new System.EventHandler(this.radGr_Clicked);
            this.ImLst.ImageStream = (System.Windows.Forms.ImageListStreamer)resources.GetObject("ImLst.ImageStream");
            this.ImLst.TransparentColor = System.Drawing.Color.Transparent;
            this.ImLst.Images.SetKeyName(0, "");
            this.ImLst.Images.SetKeyName(1, "");
            this.ImLst.Images.SetKeyName(2, "");
            this.ImLst.Images.SetKeyName(3, "");
            this.ImLst.Images.SetKeyName(4, "");
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblGR);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 150);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 0x18);
            this.panel1.TabIndex = 11;
            this.lblGR.BackColor = System.Drawing.Color.White;
            this.lblGR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGR.Location = new System.Drawing.Point(0x10, 0);
            this.lblGR.Name = "lblGR";
            this.lblGR.Size = new System.Drawing.Size(0xd6, 0x18);
            this.lblGR.TabIndex = 1;
            this.lblGR.Text = "Today :";
            this.lblGR.Click += new System.EventHandler(this.lblGR_Click);
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = (System.Drawing.Image)resources.GetObject("pictureBox1.Image");
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(0x10, 0x18);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.Control;
            base.ClientSize = new System.Drawing.Size(230, 0xae);
            base.ControlBox = false;
            base.Controls.Add(this.pnlDates);
            base.Controls.Add(this.panel1);
            base.Controls.Add(this.pnlSelector);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "CalPopUp";
            base.ShowInTaskbar = false;
            base.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            base.Deactivate += new System.EventHandler(this.CalPopUp_Deactivate);
            base.Load += new System.EventHandler(this.CalPopUp_Load);
            base.Leave += new System.EventHandler(this.CalPopUp_Leave);
            base.Closing += new System.ComponentModel.CancelEventHandler(this.CalPopUp_Closing);
            this.pnlSelector.ResumeLayout(false);
            this.txtYear.EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.pictureBox1).EndInit();
            base.ResumeLayout(false);
        }
        #endregion
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ComboBox cmbMonth;
        private System.Windows.Forms.Panel pnlDates;
        private System.Windows.Forms.Panel pnlSelector;
        private System.Windows.Forms.RadioButton radET;
        private System.Windows.Forms.NumericUpDown txtYear;
        private System.Windows.Forms.RadioButton radGr;
        private System.Windows.Forms.ImageList ImLst;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblGR;
        private System.Windows.Forms.PictureBox pictureBox1;

    }
}