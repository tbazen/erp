
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    public partial class CalPopUp : Form
    {
        internal Color m_ColumnCaptionColor = Color.Transparent;
        internal Color m_DayColors = Color.White;
        internal Font m_DayFont = new Font("Ethiopia Jiret", 8.249999f);
        internal int m_FirstDayOfWeek = 1;
        private bool m_IgnoreEvents = true;
        internal int m_MaxDay = 30;
        internal Color m_SelectedDayColor = Color.Red;
        private ETDateStyle m_style = ETDateStyle.Ethiopian;
        internal Color m_SundayColor = Color.Yellow;
        internal EtGrDate m_value = new EtGrDate(0x1c, 2, 0x7cd);
        private const int NCols = 7;
       
        public event EventHandler Changed;

        public CalPopUp()
        {
            this.InitializeComponent();
            this.InitializeLabels();
            this.FillMonth();
            this.RefreshValue();
            this.DrawCalendar();
            this.m_IgnoreEvents = false;
        }

        private void CalPopUp_Closing(object sender, CancelEventArgs e)
        {
            base.Hide();
            this.Compose();
            e.Cancel = true;
        }

        private void CalPopUp_Deactivate(object sender, EventArgs e)
        {
            base.Close();
        }

        private void CalPopUp_Leave(object sender, EventArgs e)
        {
            base.Close();
        }

        private void CalPopUp_Load(object sender, EventArgs e)
        {
        }

        private void cmbMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                int m = this.cmbMonth.SelectedIndex + 1;
                if (this.m_style == ETDateStyle.Ethiopian)
                {
                    if (EtGrDate.GetMonthLengthEt(m, this.m_value.Year) < this.m_value.Day)
                    {
                        this.m_value.Day = EtGrDate.GetMonthLengthEt(m, this.m_value.Year);
                    }
                }
                else if (EtGrDate.GetMonthLengthGrig(m, this.m_value.Year) < this.m_value.Day)
                {
                    this.m_value.Day = EtGrDate.GetMonthLengthGrig(m, this.m_value.Year);
                }
                this.m_value.Month = this.cmbMonth.SelectedIndex + 1;
                this.DrawCalendar();
                this.Compose();
            }
        }

        private void Compose()
        {
            if (this.Changed != null)
            {
                this.Changed(this, null);
            }
        }

        private void DateClicked(object source, EventArgs e)
        {
            this.m_value.Day = (int) ((Label) source).Tag;
            this.DrawCalendar();
            this.Compose();
            base.Hide();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        internal void DrawCalendar()
        {
            int monthLengthGrig;
            int getDayOfWeekEt;
            int num4;
            EtGrDate date;
            if (this.m_style == ETDateStyle.Ethiopian)
            {
                date = new EtGrDate(1, this.m_value.Month, this.m_value.Year);
                getDayOfWeekEt = date.GetDayOfWeekEt;
                if (this.m_value.Month == 13)
                {
                    monthLengthGrig = EtGrDate.IsLeapYearEt(this.m_value.Year) ? 6 : 5;
                }
                else
                {
                    monthLengthGrig = 30;
                }
                num4 = 0;
                while (num4 < 7)
                {
                    this.pnlDates.Controls[num4].Text = EtGrDate.GetDayOfWeekNameEt(num4 + 1);
                    num4++;
                }
            }
            else
            {
                date = new EtGrDate(1, this.m_value.Month, this.m_value.Year);
                getDayOfWeekEt = date.DayOfWeekGrig;
                monthLengthGrig = EtGrDate.GetMonthLengthGrig(this.m_value.Month, this.m_value.Year);
                for (num4 = 0; num4 < 7; num4++)
                {
                    this.pnlDates.Controls[num4].Text = EtGrDate.GetDayOfWeekNameGrig(num4 + 1);
                }
            }
            int day = this.m_value.Day;
            for (num4 = 7; num4 < this.pnlDates.Controls.Count; num4++)
            {
                Label label = (Label) this.pnlDates.Controls[num4];
                int num5 = ((num4 - 7) - getDayOfWeekEt) + 2;
                if (num5 == day)
                {
                    label.BackColor = this.m_SelectedDayColor;
                }
                else if ((num4 % 7) == 6)
                {
                    label.BackColor = this.m_SundayColor;
                }
                else
                {
                    label.BackColor = this.m_DayColors;
                }
                if ((num5 < 1) || (num5 > monthLengthGrig))
                {
                    label.Text = "";
                    label.Enabled = false;
                }
                else
                {
                    label.Text = num5.ToString();
                    label.Enabled = true;
                    label.Tag = num5;
                }
            }
        }

        private void FillMonth()
        {
            int num;
            this.cmbMonth.Items.Clear();
            if (this.m_style == ETDateStyle.Ethiopian)
            {
                for (num = 1; num <= 13; num++)
                {
                    this.cmbMonth.Items.Add(EtGrDate.GetEtMonthName(num));
                }
            }
            else
            {
                for (num = 1; num <= 12; num++)
                {
                    this.cmbMonth.Items.Add(EtGrDate.GetGrigMonthName(num));
                }
            }
        }


        private void InitializeLabels()
        {
            int width = this.pnlDates.Width / 7;
            int height = this.pnlDates.Height / 7;
            for (int i = 0; i < 7; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    Label label = new Label();
                    if (i == 0)
                    {
                        label.Font = new Font("Ethiopia Jiret", 10f);
                    }
                    label.Size = new Size(width, height);
                    label.Location = new Point(j * width, i * height);
                    label.BorderStyle = BorderStyle.None;
                    label.BackColor = this.m_ColumnCaptionColor;
                    label.TextAlign = ContentAlignment.MiddleCenter;
                    if (i > 0)
                    {
                        label.Click += new EventHandler(this.DateClicked);
                    }
                    this.pnlDates.Controls.Add(label);
                }
            }
        }

        private void lblGR_Click(object sender, EventArgs e)
        {
            this.pictureBox1_Click(sender, e);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (this.m_style == ETDateStyle.Ethiopian)
            {
                this.m_value.Day = DateTime.Now.Day;
                this.m_value.Month = DateTime.Now.Month;
                this.m_value.Year = DateTime.Now.Year;
                this.m_value = EtGrDate.ToEth(this.m_value);
                this.cmbMonth.SelectedIndex = this.m_value.Month - 1;
                this.txtYear.Text = Convert.ToString(this.m_value.Year);
                this.FillMonth();
                this.DrawCalendar();
                this.RefreshValue();
            }
            else if (this.m_style == ETDateStyle.Gregorian)
            {
                this.m_value.Day = DateTime.Now.Day;
                this.m_value.Month = DateTime.Now.Month;
                this.m_value.Year = DateTime.Now.Year;
                this.cmbMonth.SelectedIndex = this.m_value.Month - 1;
                this.txtYear.Text = Convert.ToString(this.m_value.Year);
                this.FillMonth();
                this.DrawCalendar();
                this.RefreshValue();
            }
        }

        private void pnlDates_Paint(object sender, PaintEventArgs e)
        {
        }

        private void pnlSelector_Paint(object sender, PaintEventArgs e)
        {
        }

        private void radET_Clicked(object sender, EventArgs e)
        {
            if (this.radET.Checked)
            {
                this.m_style = ETDateStyle.Ethiopian;
            }
        }

        private void radGr_Clicked(object sender, EventArgs e)
        {
            this.Style = this.radET.Checked ? ETDateStyle.Ethiopian : ETDateStyle.Gregorian;
        }

        internal void RefreshValue()
        {
            this.m_IgnoreEvents = true;
            this.txtYear.Text = this.m_value.Year.ToString();
            this.cmbMonth.SelectedIndex = this.m_value.Month - 1;
            this.radET.Checked = this.m_style == ETDateStyle.Ethiopian;
            this.radGr.Checked = this.m_style == ETDateStyle.Gregorian;
            this.Compose();
            this.m_IgnoreEvents = false;
        }

        private void txtYear_ValueChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvents)
            {
                try
                {
                    this.m_value.Year = int.Parse(Convert.ToString(this.txtYear.Value));
                    this.DrawCalendar();
                    this.Compose();
                }
                catch
                {
                }
            }
        }

        public ETDateStyle Style
        {
            get
            {
                return this.m_style;
            }
            set
            {
                if (this.m_style != value)
                {
                    this.m_style = value;
                    if (this.m_style == ETDateStyle.Ethiopian)
                    {
                        this.m_value = EtGrDate.ToEth(this.m_value);
                    }
                    else
                    {
                        this.m_value = EtGrDate.ToGrig(this.m_value);
                    }
                    this.FillMonth();
                    this.RefreshValue();
                    this.DrawCalendar();
                }
            }
        }

        public DateTime Value
        {
            get
            {
                DateTime time = new DateTime();
                if (this.m_style == ETDateStyle.Ethiopian)
                {
                    EtGrDate date = EtGrDate.ToGrig(this.m_value);
                    return new DateTime(date.Year, date.Month, date.Day);
                }
                if (this.m_style == ETDateStyle.Gregorian)
                {
                    time = new DateTime(this.m_value.Year, this.m_value.Month, this.m_value.Day);
                }
                return time;
            }
            set
            {
                if (this.m_style == ETDateStyle.Ethiopian)
                {
                    this.m_value = EtGrDate.ToEth(new EtGrDate(value.Day, value.Month, value.Year));
                    this.RefreshValue();
                    this.DrawCalendar();
                }
                else if (this.m_style == ETDateStyle.Gregorian)
                {
                    this.m_value.Day = value.Day;
                    this.m_value.Month = value.Month;
                    this.m_value.Year = value.Year;
                    this.RefreshValue();
                    this.DrawCalendar();
                }
            }
        }
    }
}

