﻿namespace INTAPS.Ethiopic
{
    internal partial class CalendarPupup : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.yearGregorian = new System.Windows.Forms.NumericUpDown();
            this.comboGregorian = new System.Windows.Forms.ComboBox();
            this.yearEthiopian = new System.Windows.Forms.NumericUpDown();
            this.comboEthiopian = new System.Windows.Forms.ComboBox();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.labelGregorianDate = new System.Windows.Forms.Label();
            this.labelEthiopianDate = new System.Windows.Forms.Label();
            this.panelGregorian = new INTAPS.Ethiopic.CalendarPanel();
            this.panelEthiopian = new INTAPS.Ethiopic.CalendarPanel();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearGregorian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearEthiopian)).BeginInit();
            this.panelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.yearGregorian);
            this.panelTop.Controls.Add(this.comboGregorian);
            this.panelTop.Controls.Add(this.yearEthiopian);
            this.panelTop.Controls.Add(this.comboEthiopian);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(583, 30);
            this.panelTop.TabIndex = 0;
            // 
            // yearGregorian
            // 
            this.yearGregorian.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yearGregorian.Location = new System.Drawing.Point(511, 5);
            this.yearGregorian.Name = "yearGregorian";
            this.yearGregorian.Size = new System.Drawing.Size(60, 20);
            this.yearGregorian.TabIndex = 1;
            this.yearGregorian.ValueChanged += new System.EventHandler(this.yearGregorian_ValueChanged);
            // 
            // comboGregorian
            // 
            this.comboGregorian.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboGregorian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboGregorian.FormattingEnabled = true;
            this.comboGregorian.Location = new System.Drawing.Point(373, 5);
            this.comboGregorian.Name = "comboGregorian";
            this.comboGregorian.Size = new System.Drawing.Size(131, 21);
            this.comboGregorian.TabIndex = 0;
            this.comboGregorian.SelectedIndexChanged += new System.EventHandler(this.comboGregorian_SelectedIndexChanged);
            // 
            // yearEthiopian
            // 
            this.yearEthiopian.Location = new System.Drawing.Point(148, 5);
            this.yearEthiopian.Name = "yearEthiopian";
            this.yearEthiopian.Size = new System.Drawing.Size(60, 20);
            this.yearEthiopian.TabIndex = 1;
            this.yearEthiopian.ValueChanged += new System.EventHandler(this.yearEthiopian_ValueChanged);
            // 
            // comboEthiopian
            // 
            this.comboEthiopian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboEthiopian.Font = new System.Drawing.Font("Nyala", 10F);
            this.comboEthiopian.FormattingEnabled = true;
            this.comboEthiopian.Location = new System.Drawing.Point(11, 3);
            this.comboEthiopian.Name = "comboEthiopian";
            this.comboEthiopian.Size = new System.Drawing.Size(131, 24);
            this.comboEthiopian.TabIndex = 0;
            this.comboEthiopian.SelectedIndexChanged += new System.EventHandler(this.comboEthiopian_SelectedIndexChanged);
            // 
            // panelBottom
            // 
            this.panelBottom.BackColor = System.Drawing.Color.DarkOrange;
            this.panelBottom.Controls.Add(this.button1);
            this.panelBottom.Controls.Add(this.labelGregorianDate);
            this.panelBottom.Controls.Add(this.labelEthiopianDate);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 272);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(583, 42);
            this.panelBottom.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(232, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "&Now";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelGregorianDate
            // 
            this.labelGregorianDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGregorianDate.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.labelGregorianDate.Location = new System.Drawing.Point(413, 7);
            this.labelGregorianDate.Name = "labelGregorianDate";
            this.labelGregorianDate.Size = new System.Drawing.Size(158, 28);
            this.labelGregorianDate.TabIndex = 1;
            this.labelGregorianDate.Text = "September 11,2007";
            this.labelGregorianDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelEthiopianDate
            // 
            this.labelEthiopianDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelEthiopianDate.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.labelEthiopianDate.Location = new System.Drawing.Point(12, 7);
            this.labelEthiopianDate.Name = "labelEthiopianDate";
            this.labelEthiopianDate.Size = new System.Drawing.Size(182, 28);
            this.labelEthiopianDate.TabIndex = 1;
            this.labelEthiopianDate.Text = "Meskerem 1, 2000";
            this.labelEthiopianDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panelGregorian
            // 
            this.panelGregorian.Location = new System.Drawing.Point(316, 70);
            this.panelGregorian.Name = "panelGregorian";
            this.panelGregorian.Size = new System.Drawing.Size(226, 165);
            this.panelGregorian.TabIndex = 3;
            this.panelGregorian.TabStop = true;
            // 
            // panelEthiopian
            // 
            this.panelEthiopian.Location = new System.Drawing.Point(52, 70);
            this.panelEthiopian.Name = "panelEthiopian";
            this.panelEthiopian.Size = new System.Drawing.Size(193, 165);
            this.panelEthiopian.TabIndex = 2;
            this.panelEthiopian.TabStop = true;
            // 
            // CalendarPupup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 314);
            this.Controls.Add(this.panelGregorian);
            this.Controls.Add(this.panelEthiopian);
            this.Controls.Add(this.panelBottom);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimizeBox = false;
            this.Name = "CalendarPupup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Dual Calendar";
            this.panelTop.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.yearGregorian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearEthiopian)).EndInit();
            this.panelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.NumericUpDown yearGregorian;
        private System.Windows.Forms.ComboBox comboGregorian;
        private System.Windows.Forms.NumericUpDown yearEthiopian;
        private System.Windows.Forms.ComboBox comboEthiopian;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelGregorianDate;
        private System.Windows.Forms.Label labelEthiopianDate;
        private CalendarPanel panelGregorian;
        private CalendarPanel panelEthiopian;

    }
}
