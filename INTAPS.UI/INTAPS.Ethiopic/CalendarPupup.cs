
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    internal partial class CalendarPupup : Form
    {
        private int _caledarBoarderSpace = 3;
        private bool _closeForm = false;
        private DateTime _currentDate = DateTime.Now;
        private EtGrDate _currentEthipianDate;
        private Label _currentEtLabel = null;
        private Label _currentGrLabel = null;
        private Color _dayBackColor = Color.White;
        private Color _dayColor = Color.Black;
        private Font _ethiopianFont = new Font("Nyala", 10f);
        private int _gridCellHeight = 0x19;
        private int _gridCellWidth = 30;
        private Color _gridLineColor = Color.Black;
        private Color _hoverColor = Color.Red;
        private bool _ignoreEvents = false;
        private int _nameCellHeight = 30;
        private int _nameGridSpace = 5;
        private Color _nameLineColor = Color.Black;
        private Color _selectedDayBackColor = Color.Cyan;
        private Color _selectedDayColor = Color.DarkBlue;
        private Color _weekEndBackColor = Color.Yellow;
        private Color _weekEndColor = Color.Red;
        private Color _weekNameBackColor = Color.DarkBlue;
        private Color _weekNameColor = Color.White;
        private Color _weekNameWeekEndBackColor = Color.Brown;
        private Color _weekNameWeekEndColor = Color.White;
        
        private IContainer components = null;
        
        private Label prevFocusLabel = null;
        
        public event EventHandler DateChanged;

        public CalendarPupup()
        {
            this.InitializeComponent();
            this.InitializeLabels(this.panelEthiopian, true);
            this.InitializeLabels(this.panelGregorian, false);
            base.ClientSize = new Size((this.panelEthiopian.Width + this.panelGregorian.Width) + (3 * this._caledarBoarderSpace), ((this.panelEthiopian.Height + this.panelTop.Height) + this.panelBottom.Height) + (2 * this._caledarBoarderSpace));
            this.panelEthiopian.Location = new Point(this._caledarBoarderSpace, this.panelTop.Height + this._caledarBoarderSpace);
            this.panelGregorian.Location = new Point((this._caledarBoarderSpace + this.panelEthiopian.Width) + (this._caledarBoarderSpace * 2), this.panelTop.Height + this._caledarBoarderSpace);
            this._ignoreEvents = true;
            try
            {
                this.populateMonths();
                this.yearEthiopian.Minimum = 1000M;
                this.yearEthiopian.Maximum = 3000M;
                this.yearGregorian.Minimum = 1000M;
                this.yearGregorian.Maximum = 3000M;
            }
            finally
            {
                this._ignoreEvents = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.CurrentDate = DateTime.Now;
            base.Hide();
        }

        public void CloseForm()
        {
            this._closeForm = true;
            base.Close();
        }

        private void comboEthiopian_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                int monthLengthEt = EtGrDate.GetMonthLengthEt(this.comboEthiopian.SelectedIndex + 1, this._currentEthipianDate.Year);
                if (this._currentEthipianDate.Day > monthLengthEt)
                {
                    this._currentEthipianDate.Day = monthLengthEt;
                }
                this._currentEthipianDate.Month = this.comboEthiopian.SelectedIndex + 1;
                this.CurrentDate = EtGrDate.ToGrig(this._currentEthipianDate).GridDate;
            }
        }

        private void comboGregorian_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                int monthLengthGrig = EtGrDate.GetMonthLengthGrig(this.comboGregorian.SelectedIndex + 1, this._currentDate.Year);
                if (this._currentDate.Day > monthLengthGrig)
                {
                    this.CurrentDate = new DateTime(this._currentDate.Year, this.comboGregorian.SelectedIndex + 1, monthLengthGrig);
                }
                else
                {
                    this.CurrentDate = new DateTime(this._currentDate.Year, this.comboGregorian.SelectedIndex + 1, this._currentDate.Day);
                }
            }
        }

        private static Label CreateDayLabel()
        {
            Label label = new Label();
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.AutoSize = false;
            label.BorderStyle = BorderStyle.None;
            return label;
        }

        private void DayLabelMouseEnter(object sender, EventArgs e)
        {
            if (this.prevFocusLabel != null)
            {
                this.prevFocusLabel.BorderStyle = BorderStyle.None;
            }
            Label label = (Label) sender;
            label.BorderStyle = BorderStyle.FixedSingle;
            this.prevFocusLabel = label;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void EthiopianDayClicked(object sender, EventArgs e)
        {
            Label label = (Label) sender;
            if (label.Tag is int)
            {
                this._currentEthipianDate.Day = (int) label.Tag;
                this.CurrentDate = EtGrDate.ToGrig(this._currentEthipianDate).GridDate;
            }
            this.panelEthiopian.Focus();
            base.Hide();
        }

        private void fixMonthAndYearControls()
        {
            this._ignoreEvents = true;
            try
            {
                this.yearEthiopian.Value = this._currentEthipianDate.Year;
                this.yearGregorian.Value = this._currentDate.Year;
                this.comboEthiopian.SelectedIndex = this._currentEthipianDate.Month - 1;
                this.comboGregorian.SelectedIndex = this._currentDate.Month - 1;
            }
            finally
            {
                this._ignoreEvents = false;
            }
        }

        private void GregorianDayClicked(object sender, EventArgs e)
        {
            Label label = (Label) sender;
            if (label.Tag is int)
            {
                this.CurrentDate = this.CurrentDate = new DateTime(this._currentDate.Year, this._currentDate.Month, (int) label.Tag);
            }
            this.panelGregorian.Focus();
            base.Hide();
        }


        private void InitializeLabels(Panel panel, bool eth)
        {
            Label label;
            for (int i = 0; i < 7; i++)
            {
                label = CreateDayLabel();
                label.Size = new Size(this._gridCellWidth, this._nameCellHeight);
                label.Location = new Point(i * this._gridCellWidth, 0);
                label.ForeColor = (i < 5) ? this._weekNameColor : this._weekNameWeekEndColor;
                label.BackColor = (i < 5) ? this._weekNameBackColor : this._weekNameWeekEndBackColor;
                if (eth)
                {
                    label.Font = this._ethiopianFont;
                    label.Text = EtGrDate.GetDayOfWeekNameEt(i + 1).Substring(0, 1);
                }
                else
                {
                    label.Text = EtGrDate.GetDayOfWeekNameGrig(i + 1).Substring(0, 1);
                }
                panel.Controls.Add(label);
            }
            for (int j = 0; j < 6; j++)
            {
                for (int k = 0; k < 7; k++)
                {
                    label = CreateDayLabel();
                    label.Size = new Size(this._gridCellWidth, this._gridCellHeight);
                    label.Location = new Point(k * this._gridCellWidth, (this._nameCellHeight + this._nameGridSpace) + (j * this._gridCellHeight));
                    label.ForeColor = (k < 5) ? this._dayColor : this._weekEndColor;
                    label.BackColor = (k < 5) ? this._dayBackColor : this._weekEndBackColor;
                    if (eth)
                    {
                        label.Click += new EventHandler(this.EthiopianDayClicked);
                    }
                    else
                    {
                        label.Click += new EventHandler(this.GregorianDayClicked);
                    }
                    label.MouseEnter += new EventHandler(this.DayLabelMouseEnter);
                    panel.Controls.Add(label);
                }
            }
            panel.Width = this._gridCellWidth * 7;
            panel.Height = ((this._gridCellHeight * 6) + this._nameCellHeight) + this._nameGridSpace;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (!this._closeForm)
            {
                e.Cancel = true;
                base.Hide();
            }
            base.OnClosing(e);
        }

        private void OnDateChanged()
        {
            if (this.DateChanged != null)
            {
                this.DateChanged(this, null);
            }
        }

        protected override void OnDeactivate(EventArgs e)
        {
            base.Hide();
            base.OnDeactivate(e);
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
        }

        private void populateMonths()
        {
            this.comboEthiopian.Items.Clear();
            int m = 1;
            for (m = 1; m <= 13; m++)
            {
                this.comboEthiopian.Items.Add(EtGrDate.GetEtMonthName(m));
            }
            for (m = 1; m <= 12; m++)
            {
                this.comboGregorian.Items.Add(EtGrDate.GetGrigMonthName(m));
            }
        }

        private void updateLabeles()
        {
            this.labelEthiopianDate.Text = string.Concat(new object[] { EtGrDate.GetEtMonthName(this._currentEthipianDate.Month), " ", this._currentEthipianDate.Day, ", ", this._currentEthipianDate.Year });
            this.labelGregorianDate.Text = string.Concat(new object[] { EtGrDate.GetGrigMonthName(this._currentDate.Month), " ", this._currentDate.Day, ", ", this._currentDate.Year });
        }

        private void updateValues()
        {
            int num4;
            int num = -1;
            int monthLengthEt = EtGrDate.GetMonthLengthEt(this._currentEthipianDate.Month, this._currentEthipianDate.Year);
            EtGrDate date = new EtGrDate(1, this._currentEthipianDate.Month, this._currentEthipianDate.Year);
            int getDayOfWeekEt = date.GetDayOfWeekEt;
            if (this._currentEtLabel != null)
            {
                if ((this.panelEthiopian.Controls.IndexOf(this._currentEtLabel) % 7) > 4)
                {
                    this._currentEtLabel.ForeColor = this._weekEndColor;
                    this._currentEtLabel.BackColor = this._weekEndBackColor;
                }
                else
                {
                    this._currentEtLabel.ForeColor = this._dayColor;
                    this._currentEtLabel.BackColor = this._dayBackColor;
                }
            }
            this._currentEtLabel = null;
            foreach (Label label in this.panelEthiopian.Controls)
            {
                num++;
                if (num >= 7)
                {
                    num4 = ((2 + num) - getDayOfWeekEt) - 7;
                    if ((num4 < 1) || (num4 > monthLengthEt))
                    {
                        label.Text = "";
                        label.Tag = null;
                    }
                    else
                    {
                        label.Text = num4.ToString();
                        label.Tag = num4;
                        if (num4 == this._currentEthipianDate.Day)
                        {
                            label.BackColor = this._selectedDayBackColor;
                            label.ForeColor = this._selectedDayColor;
                            this._currentEtLabel = label;
                        }
                    }
                }
            }
            num = -1;
            monthLengthEt = EtGrDate.GetMonthLengthGrig(this._currentDate.Month, this._currentDate.Year);
            date = new EtGrDate(1, this._currentDate.Month, this._currentDate.Year);
            getDayOfWeekEt = date.DayOfWeekGrig;
            if (this._currentGrLabel != null)
            {
                if ((this.panelGregorian.Controls.IndexOf(this._currentGrLabel) % 7) > 4)
                {
                    this._currentGrLabel.ForeColor = this._weekEndColor;
                    this._currentGrLabel.BackColor = this._weekEndBackColor;
                }
                else
                {
                    this._currentGrLabel.ForeColor = this._dayColor;
                    this._currentGrLabel.BackColor = this._dayBackColor;
                }
            }
            this._currentGrLabel = null;
            foreach (Label label in this.panelGregorian.Controls)
            {
                num++;
                if (num >= 7)
                {
                    num4 = ((2 + num) - getDayOfWeekEt) - 7;
                    if ((num4 < 1) || (num4 > monthLengthEt))
                    {
                        label.Text = "";
                        label.Tag = null;
                    }
                    else
                    {
                        label.Text = num4.ToString();
                        label.Tag = num4;
                        if (num4 == this._currentDate.Day)
                        {
                            label.BackColor = this._selectedDayBackColor;
                            label.ForeColor = this._selectedDayColor;
                            this._currentGrLabel = label;
                        }
                    }
                }
            }
        }

        private void yearEthiopian_ValueChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                int monthLengthEt = EtGrDate.GetMonthLengthEt(this._currentEthipianDate.Month, (int) this.yearEthiopian.Value);
                if (this._currentEthipianDate.Day > monthLengthEt)
                {
                    this._currentEthipianDate.Day = monthLengthEt;
                }
                this._currentEthipianDate.Year = (int) this.yearEthiopian.Value;
                this.CurrentDate = EtGrDate.ToGrig(this._currentEthipianDate).GridDate;
            }
        }

        private void yearGregorian_ValueChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                int monthLengthGrig = EtGrDate.GetMonthLengthGrig(this._currentDate.Month, (int) this.yearGregorian.Value);
                if (this._currentDate.Day > monthLengthGrig)
                {
                    this.CurrentDate = new DateTime((int) this.yearGregorian.Value, this._currentDate.Month, monthLengthGrig);
                }
                else
                {
                    this.CurrentDate = new DateTime((int) this.yearGregorian.Value, this._currentDate.Month, this._currentDate.Day);
                }
            }
        }

        public DateTime CurrentDate
        {
            get
            {
                return this._currentDate;
            }
            set
            {
                this._currentDate = value;
                this._currentEthipianDate = EtGrDate.ToEth(this._currentDate);
                this.updateValues();
                this.fixMonthAndYearControls();
                this.updateLabeles();
                this.OnDateChanged();
            }
        }
    }
}

