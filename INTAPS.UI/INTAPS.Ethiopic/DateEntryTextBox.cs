
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    internal class DateEntryTextBox : TextBox
    {
        private EtGrDate _currentDate;
        private bool _editing = false;
        private int _editingValue = 0;
        private EditPositionType _editPosition = EditPositionType.Day;
        private bool _eth;
        private bool _ignoreEvent = false;

        public event EventHandler DateChanged;

        public DateEntryTextBox()
        {
            base.ReadOnly = true;
        }

        private void commitEdit()
        {
            if (this._editing)
            {
                switch (this._editPosition)
                {
                    case EditPositionType.Day:
                        if (EtGrDate.IsValidDMY(this._editingValue, this._currentDate.Month, this._currentDate.Year, this._eth))
                        {
                            this._currentDate.Day = this._editingValue;
                            this.OnDateChanged();
                        }
                        break;

                    case EditPositionType.Month:
                        if (EtGrDate.IsValidDMY(this._currentDate.Day, this._editingValue, this._currentDate.Year, this._eth))
                        {
                            this._currentDate.Month = this._editingValue;
                            this.OnDateChanged();
                        }
                        break;

                    case EditPositionType.Year:
                        if (EtGrDate.IsValidDMY(this._currentDate.Day, this._currentDate.Month, this._editingValue, this._eth))
                        {
                            this._currentDate.Year = this._editingValue;
                            this.OnDateChanged();
                        }
                        break;
                }
                this._editing = false;
            }
        }

        private void FinishEdit()
        {
            if (!this._editing)
            {
            }
        }

        public DateTime GetDate()
        {
            if (this._eth)
            {
                return EtGrDate.ToGrig(this._currentDate).GridDate;
            }
            return new DateTime(this._currentDate.Year, this._currentDate.Month, this._currentDate.Day);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            if ((keyData != Keys.Right) && (keyData != Keys.Return))
            {
                if (keyData != Keys.Left)
                {
                    if (keyData == Keys.Up)
                    {
                        return false;
                    }
                    if (keyData == Keys.Down)
                    {
                        return false;
                    }
                    return base.IsInputKey(keyData);
                }
                switch (this._editPosition)
                {
                    case EditPositionType.Day:
                        this.commitEdit();
                        this.updateText();
                        break;

                    case EditPositionType.Month:
                        this.commitEdit();
                        this._editPosition = EditPositionType.Day;
                        this.updateText();
                        break;

                    case EditPositionType.Year:
                        this.commitEdit();
                        this._editPosition = EditPositionType.Month;
                        this.updateText();
                        break;
                }
                return false;
            }
            switch (this._editPosition)
            {
                case EditPositionType.Day:
                    this.commitEdit();
                    this._editPosition = EditPositionType.Month;
                    this.updateText();
                    break;

                case EditPositionType.Month:
                    this.commitEdit();
                    this._editPosition = EditPositionType.Year;
                    this.updateText();
                    break;

                case EditPositionType.Year:
                    this.commitEdit();
                    this.updateText();
                    break;
            }
            return false;
        }

        protected override void OnClick(EventArgs e)
        {
            if (!this._ignoreEvent)
            {
                this.SetEditPosition();
                base.OnClick(e);
            }
        }

        private void OnDateChanged()
        {
            if (this.DateChanged != null)
            {
                this.DateChanged(this, null);
            }
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9'))
            {
                if (!this._editing)
                {
                    this._editingValue = 0;
                    this._editing = true;
                }
                int num = (this._editingValue * 10) + (e.KeyChar - '0');
                this._editingValue = num;
                this.updateText();
                e.Handled = true;
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            this.commitEdit();
            this.updateText();
            base.OnLeave(e);
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            return (((keyData == Keys.Right) || (keyData == Keys.Left)) || base.ProcessDialogKey(keyData));
        }

        public void SetDate(DateTime date, bool ethiopian)
        {
            this._eth = ethiopian;
            if (this._eth)
            {
                this._currentDate = EtGrDate.ToEth(date);
            }
            else
            {
                this._currentDate = new EtGrDate(date.Day, date.Month, date.Year);
            }
            this.updateText();
        }

        private void SetEditPosition()
        {
            if (this._editing)
            {
                this.commitEdit();
            }
            if (base.SelectionStart <= this.Text.IndexOf('/'))
            {
                this._editPosition = EditPositionType.Day;
            }
            else if (base.SelectionStart <= this.Text.LastIndexOf('/'))
            {
                this._editPosition = EditPositionType.Month;
            }
            else
            {
                this._editPosition = EditPositionType.Year;
            }
            this.updateText();
        }

        private void updateText()
        {
            this._ignoreEvent = true;
            try
            {
                if (this._editing)
                {
                    switch (this._editPosition)
                    {
                        case EditPositionType.Day:
                            this.Text = this._editingValue.ToString() + "/" + this._currentDate.Month.ToString() + "/" + this._currentDate.Year.ToString();
                            break;

                        case EditPositionType.Month:
                            this.Text = this._currentDate.Day.ToString() + "/" + this._editingValue.ToString() + "/" + this._currentDate.Year.ToString();
                            break;

                        case EditPositionType.Year:
                            this.Text = this._currentDate.Day.ToString("") + "/" + this._currentDate.Month.ToString() + "/" + this._editingValue.ToString();
                            break;
                    }
                }
                else
                {
                    this.Text = this._currentDate.Day.ToString() + "/" + this._currentDate.Month.ToString() + "/" + this._currentDate.Year.ToString();
                }
                if (this.Focused)
                {
                    switch (this._editPosition)
                    {
                        case EditPositionType.Day:
                            base.Select(0, this.Text.IndexOf('/'));
                            return;

                        case EditPositionType.Month:
                            base.Select(this.Text.IndexOf('/') + 1, (this.Text.LastIndexOf('/') - this.Text.IndexOf('/')) - 1);
                            return;

                        case EditPositionType.Year:
                            base.Select(this.Text.LastIndexOf('/') + 1, this.Text.Length - this.Text.LastIndexOf('/'));
                            return;
                    }
                }
            }
            finally
            {
                this._ignoreEvent = false;
            }
        }

        private enum EditPositionType
        {
            Day,
            Month,
            Year
        }
    }
}

