
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    [ToolboxBitmap(@"D:\Projects\Hansa\INTAPS.Ethiopic\icon.bmp")]
    public partial class DualCalendar : UserControl
    {
        private DualCalendarController _cont;

        public event EventHandler DateTimeChanged;

        public DualCalendar()
        {
            this._cont = new DualCalendarController(this);
            this._cont.DateTimeChanged += new EventHandler(this._cont_DateTimeChanged);
        }

        private void _cont_DateTimeChanged(object sender, EventArgs e)
        {
            if (this.DateTimeChanged != null)
            {
                this.DateTimeChanged(this, e);
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public System.DateTime DateTime
        {
            get
            {
                return this._cont.DateTime;
            }
            set
            {
                this._cont.DateTime = value;
            }
        }

        public bool ShowEthiopian
        {
            get
            {
                return this._cont.ShowEthiopian;
            }
            set
            {
                this._cont.ShowEthiopian = value;
            }
        }

        public bool ShowGregorian
        {
            get
            {
                return this._cont.ShowGregorian;
            }
            set
            {
                this._cont.ShowGregorian = value;
            }
        }

        public bool ShowTime
        {
            get
            {
                return this._cont.ShowTime;
            }
            set
            {
                this._cont.ShowTime = value;
            }
        }

        public bool VerticalLayout
        {
            get
            {
                return this._cont.VerticalLayout;
            }
            set
            {
                this._cont.VerticalLayout = value;
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DualCalendar
            // 
            this.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DualCalendar";
            this.ResumeLayout(false);

        }
    }
}

