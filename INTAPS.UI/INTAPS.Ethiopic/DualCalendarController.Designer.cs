﻿namespace INTAPS.Ethiopic
{
    public partial class DualCalendarController
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonDropDown = new System.Windows.Forms.Button();
            this.timePicker = new System.Windows.Forms.DateTimePicker();
            this.labelEthiopian = new System.Windows.Forms.Label();
            this.labelGergorian = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.textGregorian = new DateEntryTextBox();
            this.textEthiopian = new DateEntryTextBox();
            this._container.SuspendLayout();
            this.buttonDropDown.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonDropDown.Image = INTAPS.UI.Properties.Resources.calendar;
            this.buttonDropDown.Location = new System.Drawing.Point(310, 0);
            this.buttonDropDown.Name = "buttonDropDown";
            this.buttonDropDown.Size = new System.Drawing.Size(0x29, 150);
            this.buttonDropDown.TabIndex = 6;
            this.buttonDropDown.UseVisualStyleBackColor = true;
            this.buttonDropDown.Click += new System.EventHandler(this.buttonDropDown_Click);
            this.timePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timePicker.Location = new System.Drawing.Point(160, 0x40);
            this.timePicker.Name = "timePicker";
            this.timePicker.ShowUpDown = true;
            this.timePicker.Size = new System.Drawing.Size(140, 20);
            this.timePicker.TabIndex = 5;
            this.labelEthiopian.Location = new System.Drawing.Point(9, 1);
            this.labelEthiopian.Name = "labelEthiopian";
            this.labelEthiopian.Size = new System.Drawing.Size(0x4a, 0x17);
            this.labelEthiopian.TabIndex = 0;
            this.labelEthiopian.Text = "Ethiopian:";
            this.labelEthiopian.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelGergorian.Location = new System.Drawing.Point(9, 0x26);
            this.labelGergorian.Name = "labelGergorian";
            this.labelGergorian.Size = new System.Drawing.Size(0x4a, 0x17);
            this.labelGergorian.TabIndex = 2;
            this.labelGergorian.Text = "Gregorian:";
            this.labelGergorian.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelTime.Location = new System.Drawing.Point(9, 0x41);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(0x4a, 0x17);
            this.labelTime.TabIndex = 4;
            this.labelTime.Text = "Time:";
            this.labelTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.textGregorian.BackColor = System.Drawing.Color.White;
            this.textGregorian.Location = new System.Drawing.Point(160, 0x26);
            this.textGregorian.Name = "textGregorian";
            this.textGregorian.ReadOnly = true;
            this.textGregorian.Size = new System.Drawing.Size(140, 20);
            this.textGregorian.TabIndex = 3;
            this.textEthiopian.BackColor = System.Drawing.Color.White;
            this.textEthiopian.Location = new System.Drawing.Point(160, 4);
            this.textEthiopian.Name = "textEthiopian";
            this.textEthiopian.ReadOnly = true;
            this.textEthiopian.Size = new System.Drawing.Size(0x90, 20);
            this.textEthiopian.TabIndex = 1;
            this._container.Controls.Add(this.labelTime);
            this._container.Controls.Add(this.labelGergorian);
            this._container.Controls.Add(this.labelEthiopian);
            this._container.Controls.Add(this.timePicker);
            this._container.Controls.Add(this.textGregorian);
            this._container.Controls.Add(this.textEthiopian);
            this._container.Controls.Add(this.buttonDropDown);
            this._container.Name = "DualCalendar";
            this._container.Size = new System.Drawing.Size(0x15f, 150);
            this._container.ResumeLayout(false);
            this._container.PerformLayout();
        }
        #endregion
        private System.Windows.Forms.Button buttonDropDown;
        private System.Windows.Forms.DateTimePicker timePicker;
        private System.Windows.Forms.Label labelEthiopian;
        private System.Windows.Forms.Label labelGergorian;
        private System.Windows.Forms.Label labelTime;
        private DateEntryTextBox textGregorian;
        private DateEntryTextBox textEthiopian;

    }

}