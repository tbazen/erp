
    using INTAPS.UI;
    using INTAPS.UI.Properties;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    public partial class DualCalendarController
    {
        private List<Control[]> _allControlPairs;
        private Control _container;
        private System.DateTime _currentDate = System.DateTime.Now;
        private EtGrDate _ethiopianCurrentDate;
        private bool _ignoreEvents = false;
        private bool _layoutVertical = true;
        private CalendarPupup _popup;
        private bool _showEthiopian = true;
        private bool _showGregorian = true;
        private bool _showTime = true;
       
        public event EventHandler DateTimeChanged;

        public DualCalendarController(Control container)
        {
            this._container = container;
            this._ignoreEvents = true;
            try
            {
                this.InitializeComponent();
                this._allControlPairs = new List<Control[]>();
                this._allControlPairs.Add(new Control[] { this.labelEthiopian, this.textEthiopian });
                this._allControlPairs.Add(new Control[] { this.labelGergorian, this.textGregorian });
                this._allControlPairs.Add(new Control[] { this.labelTime, this.timePicker });
                this.layoutControls();
                this._popup = new CalendarPupup();
                this.textEthiopian.SetDate(this._currentDate, true);
                this.textGregorian.SetDate(this._currentDate, false);
                this._popup.DateChanged += new EventHandler(this.PopupDateChanged);
                this.textEthiopian.DateChanged += new EventHandler(this.EthiopianDateChanged);
                this.textGregorian.DateChanged += new EventHandler(this.GregorianDateChanged);
                this.timePicker.ValueChanged += new EventHandler(this.timePicker_ValueChanged);
                this._container.Resize += new EventHandler(this._container_Resize);
                this._container.Disposed += new EventHandler(this._container_Disposed);
            }
            finally
            {
                this._ignoreEvents = false;
            }
        }

        private void _container_Disposed(object sender, EventArgs e)
        {
            this._popup.CloseForm();
        }

        private void _container_Resize(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                this.layoutControls();
            }
        }

        private void buttonDropDown_Click(object sender, EventArgs e)
        {
            Point bestPopupLocation = Helper.GetBestPopupLocation(this._popup.Size);
            this._popup.Location = bestPopupLocation;
            this._ignoreEvents = true;
            try
            {
                this._popup.CurrentDate = this._currentDate;
                this._popup.Show();
            }
            finally
            {
                this._ignoreEvents = false;
            }
        }

        private void CalculateEthiopianMonth()
        {
            this._ethiopianCurrentDate = EtGrDate.ToEth(this._currentDate);
        }

        private void EthiopianDateChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                this._ignoreEvents = true;
                try
                {
                    this.setDatePart(this.textEthiopian.GetDate().Date);
                    this._popup.CurrentDate = this._currentDate;
                    this.textGregorian.SetDate(this._currentDate, false);
                    this.timePicker.Value = this._currentDate;
                    this.OnDateChanged();
                }
                finally
                {
                    this._ignoreEvents = false;
                }
            }
        }

        private void GregorianDateChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                this._ignoreEvents = true;
                try
                {
                    this.setDatePart(this.textGregorian.GetDate());
                    this._popup.CurrentDate = this._currentDate;
                    this.textEthiopian.SetDate(this._currentDate, true);
                    this.timePicker.Value = this._currentDate;
                    this.OnDateChanged();
                }
                finally
                {
                    this._ignoreEvents = false;
                }
            }
        }


        private void layoutControls()
        {
            int width;
            int num4;
            int num = 0;
            foreach (bool flag in new bool[] { this._showEthiopian, this._showGregorian, this._showTime })
            {
                this._allControlPairs[num][0].Visible = flag;
                this._allControlPairs[num][1].Visible = flag;
                num++;
            }
            List<Control[]> list = new List<Control[]>();
            if (this._showEthiopian)
            {
                list.Add(new Control[] { this.labelEthiopian, this.textEthiopian });
            }
            if (this._showGregorian)
            {
                list.Add(new Control[] { this.labelGergorian, this.textGregorian });
            }
            if (this._showTime)
            {
                list.Add(new Control[] { this.labelTime, this.timePicker });
            }
            if (this._layoutVertical)
            {
                int num2 = 0;
                width = this.labelEthiopian.Width;
                num4 = (this._container.Width - this.buttonDropDown.Width) - width;
                foreach (Control[] controlArray in list)
                {
                    controlArray[0].Top = num2;
                    controlArray[0].Width = width;
                    controlArray[0].Height = controlArray[1].Height;
                    controlArray[0].Left = 0;
                    controlArray[1].Top = num2;
                    controlArray[1].Left = width;
                    controlArray[1].Width = num4;
                    num2 += controlArray[1].Height;
                }
                this._container.Height = num2;
            }
            else
            {
                int num5 = 0;
                width = this.labelEthiopian.Width;
                num4 = (this.labelEthiopian.Width * 3) / 2;
                int height = 0;
                foreach (Control[] controlArray in list)
                {
                    controlArray[0].Top = 0;
                    controlArray[0].Width = width;
                    controlArray[0].Height = controlArray[1].Height;
                    controlArray[0].Left = num5;
                    controlArray[1].Top = 0;
                    controlArray[1].Width = num4;
                    controlArray[1].Left = num5 + width;
                    num5 += width + num4;
                    if (controlArray[1].Height > height)
                    {
                        height = controlArray[1].Height;
                    }
                }
                this._container.Width = num5 + this.buttonDropDown.Width;
                this._container.Height = height;
            }
        }

        private void OnDateChanged()
        {
            if (this.DateTimeChanged != null)
            {
                this.DateTimeChanged(this, null);
            }
        }

        private void PopupDateChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                this._ignoreEvents = true;
                try
                {
                    this._currentDate = this._popup.CurrentDate;
                    this.textEthiopian.SetDate(this._currentDate, true);
                    this.textGregorian.SetDate(this._currentDate, false);
                    this.timePicker.Value = this._popup.CurrentDate;
                    this.OnDateChanged();
                }
                finally
                {
                    this._ignoreEvents = false;
                }
            }
        }

        private void setDatePart(System.DateTime date)
        {
            System.DateTime time = this.timePicker.Value;
            this._currentDate = new System.DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second, time.Millisecond);
        }

        private void timePicker_ValueChanged(object sender, EventArgs e)
        {
            if (!this._ignoreEvents)
            {
                this._ignoreEvents = true;
                try
                {
                    this._currentDate = this.timePicker.Value;
                    this._popup.CurrentDate = this._currentDate;
                    this.textEthiopian.SetDate(this._currentDate, true);
                    this.textGregorian.SetDate(this._currentDate, false);
                    this.OnDateChanged();
                }
                finally
                {
                    this._ignoreEvents = false;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(false)]
        public System.DateTime DateTime
        {
            get
            {
                return this._currentDate;
            }
            set
            {
                this._currentDate = value;
                this._ignoreEvents = true;
                try
                {
                    this._popup.CurrentDate = this._currentDate;
                    this.textEthiopian.SetDate(this._currentDate, true);
                    this.textGregorian.SetDate(this._currentDate, false);
                    this.timePicker.Value = this._currentDate;
                }
                finally
                {
                    this._ignoreEvents = false;
                }
            }
        }

        public bool ShowEthiopian
        {
            get
            {
                return this._showEthiopian;
            }
            set
            {
                if (this._showEthiopian != value)
                {
                    this._showEthiopian = value;
                    this.layoutControls();
                }
            }
        }

        public bool ShowGregorian
        {
            get
            {
                return this._showGregorian;
            }
            set
            {
                if (this._showGregorian != value)
                {
                    this._showGregorian = value;
                    this.layoutControls();
                }
            }
        }

        public bool ShowTime
        {
            get
            {
                return this._showTime;
            }
            set
            {
                if (this._showTime != value)
                {
                    this._showTime = value;
                    if (!this._showTime)
                    {
                        this.DateTime = this._currentDate.Date;
                    }
                    this.layoutControls();
                }
            }
        }

        public bool VerticalLayout
        {
            get
            {
                return this._layoutVertical;
            }
            set
            {
                if (this._layoutVertical != value)
                {
                    this._layoutVertical = value;
                    this.layoutControls();
                }
            }
        }
    }
}

