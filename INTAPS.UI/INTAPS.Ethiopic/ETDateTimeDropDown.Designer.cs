﻿namespace INTAPS.Ethiopic
{
    public partial class ETDateTimeDropDown : System.Windows.Forms.UserControl, INTAPS.UI.IEnterNavigable
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ETDateTimeDropDown));
            this.button1 = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.TextBox();
            base.SuspendLayout();
            this.button1.Image = (System.Drawing.Image)resources.GetObject("button1.Image");
            this.button1.Location = new System.Drawing.Point(0x58, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 20);
            this.button1.TabIndex = 0;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.txtDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtDate.Location = new System.Drawing.Point(0, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(0x58, 20);
            this.txtDate.TabIndex = 1;
            this.txtDate.Text = "";
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDate_KeyPress);
            this.txtDate.TextChanged += new System.EventHandler(this.txtDate_TextChanged);
            base.Controls.Add(this.button1);
            base.Controls.Add(this.txtDate);
            base.Name = "ETDateTimeDropDown";
            base.Size = new System.Drawing.Size(0x70, 0x18);
            base.Resize += new System.EventHandler(this.ETDateTimeDropDown_Resize);
            base.Load += new System.EventHandler(this.ETDateTimeDropDown_Load);
            base.Leave += new System.EventHandler(this.ETDateTimeDropDown_Leave);
            base.ResumeLayout(false);
        }
        #endregion
        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.TextBox txtDate;

    }
}