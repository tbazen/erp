
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Resources;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    public partial class ETDateTimeDropDown : UserControl, IEnterNavigable
    {
        
        private Container components = null;
        private bool DayInputFirst = true;
        private CalPopUp m_cp = new CalPopUp();
        private ETDateStyle m_Style = ETDateStyle.Ethiopian;
        private bool MonthInputFirst = true;
        
        private bool YearInputFirst = true;

        public event EventHandler Changed;

        public event EventHandler GoToNext;

        public ETDateTimeDropDown()
        {
            this.InitializeComponent();
            this.m_cp.Changed += new EventHandler(this.PopupChanged);
            this.Style = ETDateStyle.Ethiopian;
            this.m_cp.Value = DateTime.Today;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.m_cp.Location = Helper.GetBestPopupLocation(this.m_cp.Size);
            this.m_cp.Show();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void EnterNavigate()
        {
            if (this.GoToNext != null)
            {
                this.GoToNext(this, null);
            }
        }

        private void ETDateTimeDropDown_Leave(object sender, EventArgs e)
        {
            this.DayInputFirst = true;
            this.MonthInputFirst = true;
            this.YearInputFirst = true;
        }

        private void ETDateTimeDropDown_Load(object sender, EventArgs e)
        {
        }

        private void ETDateTimeDropDown_Resize(object sender, EventArgs e)
        {
            this.txtDate.Location = new Point(0, 0);
            this.txtDate.Width = base.Width;
            base.Height = this.txtDate.Height;
            this.button1.Left = (this.txtDate.Width - this.button1.Width) - 2;
            this.button1.Top = this.txtDate.Top + 2;
            this.button1.Height = this.txtDate.Height - 4;
        }

        internal void FireChangeEvent()
        {
            if (this.Changed != null)
            {
                this.Changed(this, null);
            }
        }

        private void Increase(int D)
        {
            int monthLengthEt;
            int selectionStart = this.txtDate.SelectionStart;
            EtGrDate date = this.m_cp.m_value;
            int num2 = (this.m_cp.Style == ETDateStyle.Ethiopian) ? date.DayNoEt : date.DayNoGrig;
            if (this.txtDate.SelectionStart < 3)
            {
                monthLengthEt = 1;
            }
            else if (this.txtDate.SelectionStart < 6)
            {
                if (this.m_cp.Style == ETDateStyle.Ethiopian)
                {
                    if (D == 1)
                    {
                        monthLengthEt = EtGrDate.GetMonthLengthEt(date.Month, date.Year);
                    }
                    else
                    {
                        monthLengthEt = EtGrDate.GetMonthLengthEt((date.Month == 1) ? 13 : (date.Month - 1), date.Year - 1);
                    }
                }
                else if (D == 1)
                {
                    monthLengthEt = EtGrDate.GetMonthLengthGrig(date.Month, date.Year);
                }
                else
                {
                    monthLengthEt = EtGrDate.GetMonthLengthGrig((date.Month == 1) ? 12 : (date.Month - 1), date.Year - 1);
                }
            }
            else if (this.m_cp.Style == ETDateStyle.Ethiopian)
            {
                if (D == 1)
                {
                    monthLengthEt = EtGrDate.IsLeapYearEt(date.Year) ? 0x16e : 0x16d;
                }
                else
                {
                    monthLengthEt = EtGrDate.IsLeapYearEt(date.Year - 1) ? 0x16e : 0x16d;
                }
            }
            else if (D == 1)
            {
                monthLengthEt = EtGrDate.IsLeapYearGr(date.Year) ? 0x16e : 0x16d;
            }
            else
            {
                monthLengthEt = EtGrDate.IsLeapYearGr(date.Year - 1) ? 0x16e : 0x16d;
            }
            monthLengthEt *= D;
            this.m_cp.m_value = new EtGrDate(num2 + monthLengthEt, this.m_cp.Style == ETDateStyle.Ethiopian);
            this.m_cp.DrawCalendar();
            this.m_cp.RefreshValue();
            this.PopupChanged(null, null);
            this.txtDate.SelectionStart = selectionStart;
        }


        private void PopupChanged(object sender, EventArgs e)
        {
            this.txtDate.Text = this.m_cp.m_value.ToString();
        }

        public void StartEdit()
        {
            this.txtDate.Focus();
        }

        private void txtDate_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    this.button1_Click(null, null);
                    break;

                case Keys.Left:
                    if (this.txtDate.SelectionStart < 6)
                    {
                        if (this.txtDate.SelectionStart >= 3)
                        {
                            this.txtDate.SelectionStart = 1;
                        }
                        break;
                    }
                    this.txtDate.SelectionStart = 4;
                    break;

                case Keys.Up:
                    this.Increase(1);
                    e.Handled = true;
                    break;

                case Keys.Right:
                    if (this.txtDate.SelectionStart >= 3)
                    {
                        if (this.txtDate.SelectionStart < 6)
                        {
                            this.txtDate.SelectionStart = 5;
                        }
                        break;
                    }
                    this.txtDate.SelectionStart = 2;
                    break;

                case Keys.Down:
                    this.Increase(-1);
                    e.Handled = true;
                    break;

                case Keys.Return:
                    this.EnterNavigate();
                    break;
            }
        }

        private void txtDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9'))
            {
                int num4;
                int selectionStart = this.txtDate.SelectionStart;
                int destinationIndex = 0;
                int num3 = e.KeyChar - '0';
                string str = "";
                EtGrDate date = this.m_cp.m_value;
                if (selectionStart < 3)
                {
                    destinationIndex = 0;
                    if (this.DayInputFirst)
                    {
                        num4 = 0;
                        this.DayInputFirst = false;
                    }
                    else
                    {
                        num4 = int.Parse(this.txtDate.Text.Substring(0, 2)) % 10;
                    }
                    if (this.Style == ETDateStyle.Ethiopian)
                    {
                        if (num4 > (EtGrDate.GetMonthLengthEt(date.Month, date.Year) / 10))
                        {
                            num4 = 0;
                        }
                        num4 = (num4 * 10) + num3;
                        if (num4 > EtGrDate.GetMonthLengthEt(date.Month, date.Year))
                        {
                            return;
                        }
                        str = num4.ToString("00");
                    }
                    else
                    {
                        if (num4 > (EtGrDate.GetMonthLengthGrig(date.Month, date.Year) / 10))
                        {
                            num4 = 0;
                        }
                        num4 = (num4 * 10) + num3;
                        if (num4 > EtGrDate.GetMonthLengthGrig(date.Month, date.Year))
                        {
                            return;
                        }
                        str = num4.ToString("00");
                    }
                    str = num4.ToString("00");
                    date.Day = num4;
                }
                else if (selectionStart < 6)
                {
                    destinationIndex = 3;
                    if (this.MonthInputFirst)
                    {
                        num4 = 0;
                        this.MonthInputFirst = false;
                    }
                    else
                    {
                        num4 = int.Parse(this.txtDate.Text.Substring(3, 2)) % 10;
                    }
                    if (num4 > 1)
                    {
                        num4 = 0;
                    }
                    if (num4 == 1)
                    {
                        if (this.Style == ETDateStyle.Ethiopian)
                        {
                            if (num3 > 3)
                            {
                                return;
                            }
                        }
                        else if (num3 > 2)
                        {
                            return;
                        }
                    }
                    str = ((num4 * 10) + num3).ToString("00");
                    date.Month = (num4 * 10) + num3;
                }
                else
                {
                    destinationIndex = 6;
                    if (this.YearInputFirst)
                    {
                        num4 = num3;
                        this.YearInputFirst = false;
                    }
                    else
                    {
                        num4 = (int.Parse(this.txtDate.Text.Substring(6, 4)) * 10) + num3;
                    }
                    num4 = num4 % 0x2710;
                    str = num4.ToString("0000");
                    date.Year = num4;
                }
                this.m_cp.m_value = date;
                this.m_cp.RefreshValue();
                char[] destination = this.txtDate.Text.ToCharArray();
                str.CopyTo(0, destination, destinationIndex, str.Length);
                string str2 = "";
                foreach (char ch in destination)
                {
                    str2 = str2 + ch;
                }
                this.txtDate.Text = str2;
                this.txtDate.SelectionStart = selectionStart;
            }
        }

        private void txtDate_TextChanged(object sender, EventArgs e)
        {
            this.FireChangeEvent();
        }

        public Control ControlObject
        {
            get
            {
                return this;
            }
        }

        public bool Skip
        {
            get
            {
                return (!base.Enabled || !base.Visible);
            }
        }

        public ETDateStyle Style
        {
            get
            {
                return this.m_Style;
            }
            set
            {
                this.m_Style = value;
                this.m_cp.Style = value;
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DateTime Value
        {
            get
            {
                return this.m_cp.Value;
            }
            set
            {
                this.m_cp.Value = value;
            }
        }
    }
}

