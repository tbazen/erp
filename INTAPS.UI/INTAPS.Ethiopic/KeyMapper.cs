
    using INTAPS.UI;
    using Microsoft.VisualBasic.Devices;
    using System;
    using System.Collections;
    using System.Windows.Forms;
namespace INTAPS.Ethiopic
{

    public class KeyMapper
    {
        private int Current = -1;
        private string CurrentKeys = "";
        private bool IgnoreKey = false;
        private Keyboard KeyBoard = new Keyboard();
        private bool m_Enabled = true;
        private SortedList m_Map = new SortedList();

        public KeyMapper()
        {
            this.LoadMap();
        }

        public void AddControl(Control c)
        {
            c.KeyDown += new KeyEventHandler(this.ControlKeyDown);
            c.KeyPress += new KeyPressEventHandler(this.ControlKeyPress);
            c.Disposed += new EventHandler(this.ControlDisposed);
        }

        private void ControlDisposed(object sender, EventArgs e)
        {
        }

        private void ControlKeyDown(object sender, KeyEventArgs e)
        {
            if (this.m_Enabled)
            {
                if (e.KeyCode == Keys.Control)
                {
                    this.Current = -1;
                }
                Control control = (Control) sender;
                char[] chArray = this.GetNextChar(e.KeyCode, this.KeyBoard.CapsLock, e.Shift);
                if (chArray.Length == 0)
                {
                    this.IgnoreKey = false;
                }
                foreach (char ch in chArray)
                {
                    this.IgnoreKey = false;
                    if (ch == '\b')
                    {
                        user32.SendMessageW(control.Handle, 0x100, 8, 1);
                    }
                    user32.SendMessageW(control.Handle, 0x102, ch, 1);
                }
            }
        }

        private void ControlKeyPress(object sender, KeyPressEventArgs e)
        {
            if (this.m_Enabled)
            {
                e.Handled = this.IgnoreKey;
                if (!this.IgnoreKey)
                {
                    this.IgnoreKey = true;
                }
            }
        }

        public virtual char[] GetNextChar(Keys ks, bool Caps, bool Shift)
        {
            int current;
            string str;
            int count = this.m_Map.Count;
            string str2 = this.ToChar(ks).ToString();
            if (Shift)
            {
                str2 = "^" + str2;
            }
            if (this.Current == -1)
            {
                current = 0;
                str = (Caps ? "C_" : "") + str2;
            }
            else
            {
                current = this.Current;
                str = this.CurrentKeys + str2;
            }
            while (current < count)
            {
                if (((string) this.m_Map.GetKey(current)).IndexOf(str) == 0)
                {
                    char[] chArray;
                    this.CurrentKeys = str;
                    char unicode = ((Map) this.m_Map.GetByIndex(current)).Unicode;
                    if (this.Current == -1)
                    {
                        chArray = new char[] { unicode };
                    }
                    else
                    {
                        chArray = new char[] { '\b', unicode };
                    }
                    this.Current = current;
                    return chArray;
                }
                current++;
            }
            if (this.Current == -1)
            {
                return new char[0];
            }
            this.Current = -1;
            return this.GetNextChar(ks, Caps, Shift);
        }

        protected void KeyStroke(char unicode, bool caps, string chars)
        {
            if (chars != null)
            {
                Map map = new Map();
                map.Unicode = unicode;
                map.Caps = caps;
                map.Chars = chars;
                this.m_Map.Add((caps ? "C_" : "") + map.Chars, map);
            }
        }

        protected virtual void LoadMap()
        {
        }

        public void RemoveControl(Control c)
        {
            c.KeyDown -= new KeyEventHandler(this.ControlKeyDown);
            c.KeyPress -= new KeyPressEventHandler(this.ControlKeyPress);
            c.Disposed -= new EventHandler(this.ControlDisposed);
        }

        public char ToChar(Keys ks)
        {
            switch (((int) ks))
            {
                case 0xdd:
                    return ']';

                case 0xde:
                    return '\'';

                case 0xd5:
                    return '[';

                case 0xbf:
                    return '/';

                case 0xba:
                    return ';';

                case 0xbc:
                    return ',';

                case 110:
                    return '.';
            }
            switch (ks)
            {
                case Keys.Prior:
                case Keys.Next:
                case Keys.Back:
                case Keys.Insert:
                case Keys.Delete:
                case Keys.NumPad0:
                case Keys.NumPad1:
                case Keys.NumPad2:
                case Keys.NumPad3:
                case Keys.NumPad4:
                case Keys.NumPad5:
                case Keys.NumPad6:
                case Keys.NumPad7:
                case Keys.NumPad8:
                case Keys.NumPad9:
                case Keys.F1:
                case Keys.F2:
                case Keys.F3:
                case Keys.F4:
                case Keys.F5:
                case Keys.F6:
                case Keys.F7:
                case Keys.F8:
                case Keys.F9:
                case Keys.F10:
                case Keys.F11:
                case Keys.F12:
                    return '\0';
            }
            return char.ToLower((char) ((ushort) ks));
        }

        public Keys ToKey(char ch)
        {
            return (Keys) char.ToUpper(ch);
        }

        public bool Enabled
        {
            get
            {
                return this.m_Enabled;
            }
            set
            {
                this.m_Enabled = value;
            }
        }
    }
}

