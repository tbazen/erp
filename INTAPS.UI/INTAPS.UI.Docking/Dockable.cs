
    using System;
    using System.Windows.Forms;
namespace INTAPS.UI.Docking
{

    public class Dockable
    {
        public Separator Bottom;
        public Control C;
        public Separator Left;
        public Separator Right;
        public Separator Top;

        public Dockable(Control theControl, Separator L, Separator R, Separator T, Separator B)
        {
            this.C = theControl;
            this.Left = L;
            this.Right = R;
            this.Top = T;
            this.Bottom = B;
            this.Left.Side2.Add(this);
            this.Right.Side1.Add(this);
            this.Top.Side2.Add(this);
            this.Bottom.Side1.Add(this);
            this.ReCalcBound();
        }

        public void ReCalcBound()
        {
            this.C.Left = this.Left.Pos + this.Left.Thickness;
            this.C.Width = (this.Right.Pos - this.Left.Pos) - this.Left.Thickness;
            this.C.Top = this.Top.Pos + this.Top.Thickness;
            this.C.Height = (this.Bottom.Pos - this.Top.Pos) - this.Top.Thickness;
        }
    }
}

