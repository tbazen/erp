namespace INTAPS.UI.Docking
{
    using Microsoft.Win32;
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public class DockingStructure
    {
        private int DragDel;
        private Separator m_Bottom;
        private Control m_Client;
        private ArrayList m_Dockables;
        private ArrayList m_HS;
        private Separator m_Left;
        private Separator m_Right;
        private Separator m_Top;
        private ArrayList m_VS;

        public DockingStructure()
        {
        }

        public DockingStructure(Control Client)
        {
            this.m_Client = Client;
            this.m_HS = new ArrayList();
            this.m_VS = new ArrayList();
            this.InitializeBoundaries();
        }

        public void AddDockable(Control C, Separator Left, Separator Right, Separator Top, Separator Bottom)
        {
            this.m_Client.Controls.Add(C);
            this.m_Dockables.Add(new Dockable(C, Left, Right, Top, Bottom));
        }

        public Separator AddHorizontal(Separator Left, Separator Right, int Y, int Thickness)
        {
            Panel c = this.CreateHPanel(Left, Right, Y, Thickness);
            Separator separator = new Separator(Y, Left, Right, Thickness, c);
            this.m_HS.Add(separator);
            c.Tag = separator;
            return separator;
        }

        public Separator AddVertical(Separator Top, Separator Bottom, int X, int Thickness)
        {
            Panel c = this.CreateVPanel(Top, Bottom, X, Thickness);
            Separator separator = new Separator(X, Top, Bottom, Thickness, c);
            this.m_VS.Add(separator);
            c.Tag = separator;
            return separator;
        }

        public void ClearSeparators()
        {
            this.m_HS = new ArrayList();
            this.m_VS = new ArrayList();
        }

        private void ClientResize(object sender, EventArgs e)
        {
            this.m_Right.Pos = this.m_Client.ClientSize.Width;
            this.m_Bottom.Pos = this.m_Client.ClientSize.Height;
            this.HorizontalMoved(this.m_Bottom);
            this.VerticalMoved(this.m_Right);
        }

        private void ConvertToSeparator(ArrayList a, ArrayList Dest)
        {
            foreach (SeparatorRegValue value2 in a)
            {
                Dest.Add(new Separator(value2.Pos, null, null, value2.t, null));
            }
        }

        private Panel CreateHPanel(Separator Left, Separator Right, int Y, int Thickness)
        {
            Panel panel = this.CreateSeparaterControl(Left.Pos + Left.Thickness, Y, (Right.Pos - Left.Pos) - Left.Thickness, Thickness);
            this.m_Client.Controls.Add(panel);
            panel.MouseDown += new MouseEventHandler(this.HMouseDown);
            panel.MouseMove += new MouseEventHandler(this.HMouseMove);
            panel.MouseUp += new MouseEventHandler(this.HMouseUp);
            panel.Cursor = Cursors.SizeNS;
            return panel;
        }

        private Panel CreateSeparaterControl(int Left, int Top, int Width, int Height)
        {
            Panel panel = new Panel();
            panel.Left = Left;
            panel.Width = Width;
            panel.Top = Top;
            panel.Height = Height;
            panel.BackColor = System.Drawing.Color.LightGray;
            return panel;
        }

        public Panel CreateVPanel(Separator Top, Separator Bottom, int X, int Thickness)
        {
            Panel panel = this.CreateSeparaterControl(X, Top.Pos + Top.Thickness, Thickness, (Bottom.Pos - Top.Pos) - Top.Thickness);
            this.m_Client.Controls.Add(panel);
            panel.MouseDown += new MouseEventHandler(this.VMouseDown);
            panel.MouseMove += new MouseEventHandler(this.VMouseMove);
            panel.MouseUp += new MouseEventHandler(this.VMouseUp);
            panel.Cursor = Cursors.SizeWE;
            return panel;
        }

        public Separator GetHorizontal(int index)
        {
            return (Separator) this.m_HS[index + 2];
        }

        public Separator GetVertical(int index)
        {
            return (Separator) this.m_VS[index + 2];
        }

        private void HFinalizeConversion(ArrayList a)
        {
            int num = 0;
            foreach (Separator separator in this.m_HS)
            {
                if (separator.End1 == null)
                {
                    SeparatorRegValue value2 = (SeparatorRegValue) a[num];
                    separator.End1 = (Separator) this.m_VS[value2.end1];
                    separator.End2 = (Separator) this.m_VS[value2.end2];
                    Panel panel = this.CreateHPanel(separator.End1, separator.End2, value2.Pos, value2.t);
                    panel.Tag = separator;
                    separator.Control = panel;
                    num++;
                }
            }
        }

        private Separator HitTest(int C1, int C2, ArrayList List)
        {
            foreach (Separator separator in List)
            {
                if (((separator.Pos <= C1) && (C1 < (separator.Pos + separator.Thickness))) && ((separator.End1.Pos <= C2) && (C2 < separator.End2.Pos)))
                {
                    return separator;
                }
            }
            return null;
        }

        private void HMouseDown(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            Point point = panel.PointToScreen(new Point(e.X, e.Y));
            panel.Capture = true;
            this.DragDel = e.Y;
        }

        private void HMouseMove(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            if (panel.Capture)
            {
                Point p = panel.PointToScreen(new Point(e.X, e.Y));
                Separator tag = (Separator) panel.Tag;
                p = this.m_Client.PointToClient(p);
                tag.Control.Top = p.Y - this.DragDel;
                tag.Pos = p.Y - this.DragDel;
            }
        }

        private void HMouseUp(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            Separator tag = (Separator) panel.Tag;
            if (panel.Capture)
            {
                this.HorizontalMoved(tag);
                panel.Capture = false;
            }
        }

        public Separator HorizonalHitTest(int x, int y)
        {
            return this.HitTest(y, x, this.m_HS);
        }

        private void HorizontalMoved(Separator s)
        {
            foreach (Dockable dockable in s.Side1)
            {
                dockable.ReCalcBound();
                dockable.Right.RecalcV();
                dockable.Left.RecalcV();
            }
            foreach (Dockable dockable in s.Side2)
            {
                dockable.ReCalcBound();
                dockable.Left.RecalcV();
                dockable.Right.RecalcV();
            }
        }

        private void InitializeBoundaries()
        {
            this.m_Client.Resize += new EventHandler(this.ClientResize);
            this.m_Dockables = new ArrayList();
            this.m_Left = new Separator(0, null, null, 0, null);
            this.m_Right = new Separator(this.Client.ClientRectangle.Width, null, null, 0, null);
            this.m_Top = new Separator(0, null, null, 0, null);
            this.m_Bottom = new Separator(this.Client.ClientRectangle.Height, null, null, 0, null);
            this.m_Left.End1 = this.m_Top;
            this.m_Left.End2 = this.m_Bottom;
            this.m_Right.End1 = this.m_Top;
            this.m_Right.End2 = this.m_Bottom;
            this.m_Top.End1 = this.m_Left;
            this.m_Top.End2 = this.m_Right;
            this.m_Bottom.End1 = this.m_Left;
            this.m_Bottom.End2 = this.m_Right;
            this.m_HS.Add(this.m_Top);
            this.m_HS.Add(this.m_Bottom);
            this.m_VS.Add(this.m_Left);
            this.m_VS.Add(this.m_Right);
        }

        private ArrayList LoadSeparators(RegistryKey Key)
        {
            RegistryKey key = Key.OpenSubKey("POS");
            if (key == null)
            {
                return null;
            }
            RegistryKey key2 = Key.OpenSubKey("End1");
            if (key2 == null)
            {
                return null;
            }
            RegistryKey key3 = Key.OpenSubKey("End2");
            if (key3 == null)
            {
                return null;
            }
            RegistryKey key4 = Key.OpenSubKey("T");
            if (key4 == null)
            {
                return null;
            }
            int num = (int) Key.GetValue("n", -1);
            if (num == -1)
            {
                return null;
            }
            int num2 = 0;
            ArrayList list = new ArrayList();
            for (num2 = 0; num2 < num; num2++)
            {
                object obj2 = key.GetValue(num2.ToString());
                if (obj2 == null)
                {
                    return null;
                }
                int p = (int) obj2;
                obj2 = key2.GetValue(num2.ToString());
                if (obj2 == null)
                {
                    return null;
                }
                int num4 = (int) obj2;
                obj2 = key3.GetValue(num2.ToString());
                if (obj2 == null)
                {
                    return null;
                }
                int num5 = (int) obj2;
                obj2 = key4.GetValue(num2.ToString());
                if (obj2 == null)
                {
                    return null;
                }
                int t = (int) obj2;
                list.Add(new SeparatorRegValue(p, num4, num5, t));
            }
            return list;
        }

        public bool LoadSeparators(RegistryKey Key, Control client)
        {
            this.m_Client = client;
            RegistryKey key = Key.OpenSubKey("VS");
            if (key == null)
            {
                return false;
            }
            RegistryKey key2 = Key.OpenSubKey("HS");
            if (key2 == null)
            {
                return false;
            }
            ArrayList a = this.LoadSeparators(key);
            if (a == null)
            {
                return false;
            }
            ArrayList list2 = this.LoadSeparators(key2);
            if (list2 == null)
            {
                return false;
            }
            this.m_VS = new ArrayList();
            this.m_HS = new ArrayList();
            this.InitializeBoundaries();
            this.ConvertToSeparator(a, this.m_VS);
            this.ConvertToSeparator(list2, this.m_HS);
            this.HFinalizeConversion(list2);
            this.VFinalizeConversion(a);
            return true;
        }

        private void MouseDown(object sender, MouseEventArgs e)
        {
        }

        public void SaveSeparators(RegistryKey Key)
        {
            RegistryKey key = Key.CreateSubKey("VS");
            RegistryKey key2 = Key.CreateSubKey("HS");
            this.SaveSeparators(this.m_VS, this.m_HS, key, this.m_Right, this.m_Left);
            this.SaveSeparators(this.m_HS, this.m_VS, key2, this.m_Top, this.m_Bottom);
        }

        private void SaveSeparators(ArrayList Sep, ArrayList CSep, RegistryKey Key, Separator e1, Separator e2)
        {
            RegistryKey key = Key.CreateSubKey("POS");
            RegistryKey key2 = Key.CreateSubKey("End1");
            RegistryKey key3 = Key.CreateSubKey("End2");
            RegistryKey key4 = Key.CreateSubKey("T");
            Key.SetValue("n", Sep.Count - 2);
            int num = 0;
            foreach (Separator separator in Sep)
            {
                if ((separator != e1) && (separator != e2))
                {
                    key.SetValue(num.ToString(), separator.Pos);
                    key2.SetValue(num.ToString(), CSep.IndexOf(separator.End1));
                    key3.SetValue(num.ToString(), CSep.IndexOf(separator.End2));
                    key4.SetValue(num.ToString(), separator.Thickness);
                    num++;
                }
            }
        }

        private void SetEventHandler(Control C)
        {
            C.MouseDown += new MouseEventHandler(this.MouseDown);
        }

        public Separator VerticalHitTest(int x, int y)
        {
            return this.HitTest(x, y, this.m_VS);
        }

        private void VerticalMoved(Separator s)
        {
            foreach (Dockable dockable in s.Side1)
            {
                dockable.ReCalcBound();
                dockable.Top.RecalcH();
                dockable.Bottom.RecalcH();
            }
            foreach (Dockable dockable in s.Side2)
            {
                dockable.ReCalcBound();
                dockable.Top.RecalcH();
                dockable.Bottom.RecalcH();
            }
        }

        private void VFinalizeConversion(ArrayList a)
        {
            int num = 0;
            foreach (Separator separator in this.m_VS)
            {
                if (separator.End1 == null)
                {
                    SeparatorRegValue value2 = (SeparatorRegValue) a[num];
                    separator.End1 = (Separator) this.m_HS[value2.end1];
                    separator.End2 = (Separator) this.m_HS[value2.end2];
                    Panel panel = this.CreateVPanel(separator.End1, separator.End2, value2.Pos, value2.t);
                    panel.Tag = separator;
                    separator.Control = panel;
                    num++;
                }
            }
        }

        private void VMouseDown(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            Point point = panel.PointToScreen(new Point(e.X, e.Y));
            panel.Capture = true;
            this.DragDel = e.X;
        }

        private void VMouseMove(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            if (panel.Capture)
            {
                Point p = panel.PointToScreen(new Point(e.X, e.Y));
                Separator tag = (Separator) panel.Tag;
                p = this.m_Client.PointToClient(p);
                tag.Control.Left = p.X - this.DragDel;
                tag.Pos = p.X - this.DragDel;
            }
        }

        private void VMouseUp(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            Separator tag = (Separator) panel.Tag;
            if (panel.Capture)
            {
                this.VerticalMoved(tag);
                panel.Capture = false;
            }
        }

        public Separator Bottom
        {
            get
            {
                return this.m_Bottom;
            }
        }

        public Control Client
        {
            get
            {
                return this.m_Client;
            }
            set
            {
                this.m_Client = value;
            }
        }

        public Separator Left
        {
            get
            {
                return this.m_Left;
            }
        }

        public Separator Right
        {
            get
            {
                return this.m_Right;
            }
        }

        public Separator Top
        {
            get
            {
                return this.m_Top;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SeparatorRegValue
        {
            public int Pos;
            public int end1;
            public int end2;
            public int t;
            public SeparatorRegValue(int p, int e1, int e2, int t)
            {
                this.Pos = p;
                this.end1 = e1;
                this.end2 = e2;
                this.t = t;
            }
        }
    }
}

