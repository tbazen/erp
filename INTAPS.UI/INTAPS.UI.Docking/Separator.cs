
    using System;
    using System.Collections;
    using System.Windows.Forms;
namespace INTAPS.UI.Docking
{

    public class Separator
    {
        public Panel Control;
        public Separator End1;
        public Separator End2;
        public int Pos;
        public ArrayList Side1;
        public ArrayList Side2;
        public int Thickness;

        public Separator(int P, Separator e1, Separator e2, int t, Panel C)
        {
            this.Pos = P;
            this.End1 = e1;
            this.End2 = e2;
            this.Thickness = t;
            this.Control = C;
            this.Side1 = new ArrayList();
            this.Side2 = new ArrayList();
        }

        public void RecalcH()
        {
            if (this.Control != null)
            {
                this.Control.Left = this.End1.Pos + this.End1.Thickness;
                this.Control.Width = (this.End2.Pos - this.End1.Pos) - this.End1.Thickness;
            }
        }

        public void RecalcV()
        {
            if (this.Control != null)
            {
                this.Control.Top = this.End1.Pos + this.End1.Thickness;
                this.Control.Height = (this.End2.Pos - this.End1.Pos) - this.End1.Thickness;
            }
        }
    }
}

