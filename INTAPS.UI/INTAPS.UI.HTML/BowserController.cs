
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI.HTML
{

    public class BowserController : ToolStrip
    {
        private ToolStripButton m_Backward;
        private ToolStripDropDownButton m_Export;
        private ToolStripButton m_Forward;
        private ToolStripButton m_Refresh = new ToolStripButton("Refresh");
        private ControlBrowser m_Target;

        public event HTMLLoadErrorHandler HtmlLoadError;

        public BowserController()
        {
            this.m_Refresh.ToolTipText = "Refresh";
            this.m_Refresh.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.m_Refresh.Click += new EventHandler(this.RefreshClick);
            this.Items.Add(this.m_Refresh);
            this.m_Backward = new ToolStripButton("<Back");
            this.m_Backward.ToolTipText = "Back";
            this.m_Backward.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.m_Backward.Click += new EventHandler(this.BackClicked);
            this.Items.Add(this.m_Backward);
            this.m_Forward = new ToolStripButton("Forward>");
            this.m_Forward.ToolTipText = "Back";
            this.m_Forward.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.m_Forward.Click += new EventHandler(this.ForwardClicked);
            this.Items.Add(this.m_Forward);
            this.m_Export = new ToolStripDropDownButton("Export");
            this.m_Export.ToolTipText = "Export";
            this.m_Export.DisplayStyle = ToolStripItemDisplayStyle.Text;
            this.Items.Add(this.m_Export);
            ToolStripMenuItem item = new ToolStripMenuItem("Excel");
            item.Tag = ApplicationType.Excel;
            item.Click += new EventHandler(this.ApplicationTypeClick);
            this.m_Export.DropDownItems.Add(item);
            item = new ToolStripMenuItem("Word");
            item.Tag = ApplicationType.Word;
            item.Click += new EventHandler(this.ApplicationTypeClick);
            this.m_Export.DropDownItems.Add(item);
            item = new ToolStripMenuItem("Internet Explorer");
            item.Tag = ApplicationType.IExplorer;
            item.Click += new EventHandler(this.ApplicationTypeClick);
            this.m_Export.DropDownItems.Add(item);
            this.SetBrowser(null);
        }

        private void ApplicationTypeClick(object sender, EventArgs e)
        {
            this.m_Target.ViewInApplication((ApplicationType) ((ToolStripMenuItem) sender).Tag);
        }

        private void BackClicked(object sender, EventArgs e)
        {
            this.m_Target.Backward();
        }

        private void ForwardClicked(object sender, EventArgs e)
        {
            this.m_Target.Forward();
        }

        private void PageShown(object sender, EventArgs e)
        {
            this.UpdateButtons();
        }

        private void RefreshClick(object sender, EventArgs e)
        {
            try
            {
                this.m_Target.RefreshPage();
            }
            catch (Exception exception)
            {
                if (this.HtmlLoadError != null)
                {
                    this.HtmlLoadError(this, exception);
                }
                else
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        public void SetBrowser(ControlBrowser b)
        {
            this.m_Target = b;
            this.UpdateButtons();
            if (b != null)
            {
                this.m_Target.PageShown += new EventHandler(this.PageShown);
            }
        }

        private void UpdateButtons()
        {
            if (this.m_Target == null)
            {
                this.m_Refresh.Enabled = false;
                this.m_Backward.Enabled = false;
                this.m_Forward.Enabled = false;
                this.m_Export.Enabled = false;
            }
            else
            {
                this.m_Refresh.Enabled = true;
                this.m_Forward.Enabled = this.m_Target.CanForward;
                this.m_Backward.Enabled = this.m_Target.CanBackward;
                this.m_Export.Enabled = this.m_Target.CanViewInApp();
            }
        }

        public bool ShowBackForward
        {
            get
            {
                return this.m_Backward.Visible;
            }
            set
            {
                this.m_Backward.Visible = value;
                this.m_Forward.Visible = value;
            }
        }

        public bool ShowRefresh
        {
            get
            {
                return this.m_Refresh.Visible;
            }
            set
            {
                this.m_Refresh.Visible = value;
            }
        }
    }
}

