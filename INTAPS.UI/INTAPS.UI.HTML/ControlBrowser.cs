
using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Windows.Forms;
namespace INTAPS.UI.HTML
{
    public delegate void ShowErrorMesssageDelegate(Exception ex);
    public delegate void ProcessParameterless();
    public delegate void ProcessSingleParameter<ParType>(ParType par);
    public delegate void ProcessTowParameter<ParType1, ParType2>(ParType1 par1, ParType2 par2);

    public class ControlBrowser : WebBrowser, IHTMLDocumentHost
    {
        private ArrayList m_History = new ArrayList();
        private object m_page = null;
        private string m_StyleSheetFile;

        public event EventHandler PageShown;

        public void AddNewPage(object page)
        {
            int index = this.m_History.IndexOf(this.m_page);
            if (index == -1)
            {
                this.m_History.Add(page);
            }
            else
            {
                for (int i = this.m_History.Count - 1; i > index; i--)
                {
                    this.m_History.RemoveAt(i);
                }
                this.m_History.Add(page);
            }
        }

        public void Backward()
        {
            int index = this.m_History.IndexOf(this.m_page);
            if ((index != -1) && (index != 0))
            {
                this.m_page = this.m_History[index - 1];
                this.LoadPage(null);
            }
        }

        public bool CanViewInApp()
        {
            return (this.m_page is IHTMLBuilder);
        }

        public void Forward()
        {
            int index = this.m_History.IndexOf(this.m_page);
            if ((index != -1) && (index != (this.m_History.Count - 1)))
            {
                this.m_page = this.m_History[index + 1];
                this.LoadPage(null);
            }
        }

        public System.Windows.Forms.HtmlElement GetElement(string id)
        {
            return base.Document.GetElementById(id);
        }

        public void LoadControl(IHTMLBuilder builder)
        {
            IHTMLBuilderWithHeader header = this.m_page as IHTMLBuilderWithHeader;
            this.LoadControl(builder, header==null? null:header.getHeaderItems());
        }

        public void LoadControl(IHTMLBuilder builder, string headerItems)
        {
            this.AddNewPage(builder);
            this.m_page = builder;
            
            this.LoadPage(headerItems);
        }

        private void LoadPage(string headerItems)
        {
            if (this.m_page == null)
            {
                base.DocumentText = "Nothing to show.";
            }
            else
            {
                if (this.m_page is IHTMLBuilder)
                {
                    string str = HTMLBuilder.AppendHeadBody(this.StyleSheetFile, ((IHTMLBuilder)this.m_page).GetOuterHtml(), headerItems);
                    base.DocumentText = str;
                    ((IHTMLBuilder)this.m_page).SetHost(this);
                }
                else if(this.m_page is Uri)
                {
                    base.Navigate((Uri)this.m_page);
                }
                else
                    base.Navigate((string)this.m_page);
                
                if (this.PageShown != null)
                {
                    this.PageShown(this, null);
                }
            }
        }

        public void LoadTextPage(string caption, string htmltext)
        {
            this.LoadControl(new HTMLTextBuilder(caption, htmltext));
        }

        public void LoadTextPage(string caption, string htmltext, string headerItems)
        {
            this.LoadControl(new HTMLTextBuilder(caption, htmltext), headerItems);
        }

        protected override void OnNavigated(WebBrowserNavigatedEventArgs e)
        {
            base.OnNavigated(e);
        }

        protected override void OnNavigating(WebBrowserNavigatingEventArgs e)
        {
            string str;
            Hashtable hashtable;
            ParseUrl(e.Url, out str, out hashtable);
            bool cancelNavigate = false;
            if (str == null)
            {
                cancelNavigate = true;
            }
            else if ((this.m_page is IHTMLBuilder) && ((IHTMLBuilder)this.m_page).ProcessUrl(str, hashtable))
            {
                cancelNavigate = true;
            }
            else if (this.ProcessUrl(str, hashtable))
            {
                cancelNavigate = true;
            }
            if (cancelNavigate)
            {
                e.Cancel = true;
            }
            
            base.OnNavigating(e);
            if(!cancelNavigate)
                AddNewPage(e.Url);
        }

        public static void ParseUrl(Uri url, out string Path, out Hashtable queries)
        {
            Path = null;
            queries = null;
            if (!string.IsNullOrEmpty(url.PathAndQuery))
            {
                string[] strArray = url.PathAndQuery.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length != 0)
                {
                    queries = new Hashtable();
                    Path = strArray[0];
                    if (strArray.Length > 1)
                    {
                        foreach (string str in strArray[1].Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            string[] strArray2 = str.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray2.Length != 0)
                            {
                                if (strArray2.Length == 1)
                                {
                                    queries.Add(strArray2[0], null);
                                }
                                else
                                    queries.Add(strArray2[0], strArray2[1]);
                            }
                        }
                    }
                }
            }
        }

        private bool ProcessUrl(string path, Hashtable queries)
        {
            try
            {
                return false;
            }
            catch (Exception exception)
            {
                base.DocumentText = exception.Message;
                return false;
            }
        }

        public void RefreshPage()
        {
            if (this.m_page is IHTMLBuilder)
            {
                ((IHTMLBuilder)this.m_page).Build();
            }
            this.LoadPage(null);
        }

        public System.Windows.Forms.HtmlElement UpdateElement(string id, HtmlControl Control)
        {
            System.Windows.Forms.HtmlElement elementById = base.Document.GetElementById(id);
            if (elementById != null)
            {
                StringWriter writer = new StringWriter();
                Control.RenderControl(new HtmlTextWriter(writer));
                writer.Close();
                string str = writer.ToString();
                elementById.OuterHtml = str;
            }
            return elementById;
        }

        public System.Windows.Forms.HtmlElement UpdateElementInnerHtml(string id, string InnerHtml)
        {
            System.Windows.Forms.HtmlElement elementById = base.Document.GetElementById(id);
            if (elementById != null)
            {
                elementById.InnerHtml = InnerHtml;
            }
            return elementById;
        }

        public void ViewInApplication(ApplicationType at)
        {
            string page;
            if (this.m_page is IHTMLBuilder)
            {
                IHTMLBuilderWithHeader header = this.m_page as IHTMLBuilderWithHeader;
                page = HTMLBuilder.AppendHeadBody(this.StyleSheetFile, ((IHTMLBuilder)this.m_page).GetOuterHtml(), 
                    header==null?null:header.getHeaderItems());
            }
            else
            {
                page = (string)this.m_page;
            }
            HTMLBuilder.ViewInApplication(at, ((IHTMLBuilder)this.m_page).WindowCaption, page);
        }

        public bool CanBackward
        {
            get
            {
                int index = this.m_History.IndexOf(this.m_page);
                return ((index != -1) && (index > 0));
            }
        }

        public bool CanForward
        {
            get
            {
                int index = this.m_History.IndexOf(this.m_page);
                return ((index != -1) && (index < (this.m_History.Count - 1)));
            }
        }

        public string StyleSheetFile
        {
            get
            {
                return this.m_StyleSheetFile;
            }
            set
            {
                this.m_StyleSheetFile = value;
            }
        }
    }
}

