
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Windows.Forms;
namespace INTAPS.UI.HTML
{

    public class HTMLBuilder
    {
        public static void AddAlternatingStyleRow(HtmlTable table, HtmlTableRow row)
        {
            string[] styles = new string[2];
            styles[1] = "AltVeryLight";
            AddAlternatingStyleRow(table, row, styles);
        }

        public static void AddAlternatingStyleRow(HtmlTable table, HtmlTableRow row, string[] styles)
        {
            string str = styles[table.Rows.Count % styles.Length];
            if (!string.IsNullOrEmpty(str))
            {
                row.Attributes.Add("class", str);
            }
            table.Rows.Add(row);
        }

        public static string AppendHeadBody(string cssFile, string html)
        {
            return AppendHeadBody(cssFile, html, null);
        }

        public static string AppendHeadBody(string cssFile, string html, string headerItems)
        {
            string str = "<html><head><link href=\"" + PathToUrl(GetAppHTMLRoot() + cssFile) + "\" rel=\"stylesheet\" type=\"text/css\"/>";
            if (!string.IsNullOrEmpty(headerItems))
            {
                str = str + headerItems;
            }
            return (str + "</head><body>" + html + "</body></html>");
        }

        public static string ControlToString(System.Web.UI.Control c)
        {
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            c.RenderControl(writer2);
            return writer.ToString();
        }

        public static string CreateAppImage(string name, string css, string id, string altext)
        {
            return CreateImage(PathToUrl(GetAppImageRoot() + name), css, id, altext);
        }

        public static string CreateBMPImage(Bitmap bmp, string altext)
        {
            string filename = Helpers.GetTempName() + ".bmp";
            bmp.Save(filename, ImageFormat.Bmp);
            return ("<img alt='" + HttpUtility.HtmlEncode(altext) + "' src='" + filename + "'/>");
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control)
        {
            return CreateControlCell(control, null, null, 1, 1);
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control, int ColSpan)
        {
            return CreateControlCell(control, null, null, 1, ColSpan);
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control, string css)
        {
            return CreateControlCell(control, css, null, 1, 1);
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control, int RowSpan, int ColSpan)
        {
            return CreateControlCell(control, null, null, RowSpan, ColSpan);
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control, string css, int ColSpan)
        {
            return CreateControlCell(control, css, null, 1, ColSpan);
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control, string css, int RowSpan, int ColSpan)
        {
            return CreateControlCell(control, css, null, RowSpan, ColSpan);
        }

        public static HtmlTableCell CreateControlCell(HtmlControl control, string css, string id, int RowSpan, int ColSpan)
        {
            HtmlTableCell cell = new HtmlTableCell();
            cell.Controls.Add(control);
            if (!string.IsNullOrEmpty(css))
            {
                cell.Attributes.Add("class", css);
            }
            if (!string.IsNullOrEmpty(id))
            {
                cell.ID = id;
            }
            cell.RowSpan = RowSpan;
            cell.ColSpan = ColSpan;
            return cell;
        }

        public static HtmlTableCell CreateControlCellNoCss(HtmlControl control, string id)
        {
            return CreateControlCell(control, null, id, 1, 1);
        }

        public static HtmlTableCell CreateControlCellNoCss(HtmlControl control, string id, int ColSpan)
        {
            return CreateControlCell(control, null, id, 1, ColSpan);
        }

        public static HtmlTableCell CreateControlCellNoCss(HtmlControl control, string id, int RowSpan, int ColSpan)
        {
            return CreateControlCell(control, null, id, RowSpan, 1);
        }

        public static HtmlTableCell CreateHtmlCell(string Html)
        {
            return CreateHtmlCell(Html, null, null, 1, 1);
        }

        public static HtmlTableCell CreateHtmlCell(string Html, int ColSpan)
        {
            return CreateHtmlCell(Html, null, null, 1, ColSpan);
        }

        public static HtmlTableCell CreateHtmlCell(string Html, string css)
        {
            return CreateHtmlCell(Html, css, null, 1, 1);
        }

        public static HtmlTableCell CreateHtmlCell(string Html, int RowSpan, int ColSpan)
        {
            return CreateHtmlCell(Html, null, null, RowSpan, ColSpan);
        }

        public static HtmlTableCell CreateHtmlCell(string Html, string css, int ColSpan)
        {
            return CreateHtmlCell(Html, css, null, 1, ColSpan);
        }

        public static HtmlTableCell CreateHtmlCell(string Html, string css, int RowSpan, int ColSpan)
        {
            return CreateHtmlCell(Html, css, null, RowSpan, ColSpan);
        }

        public static HtmlTableCell CreateHtmlCell(string Html, string css, string id, int RowSpan, int ColSpan)
        {
            HtmlTableCell cell = new HtmlTableCell();
            cell.InnerHtml = Html;
            if (!string.IsNullOrEmpty(css))
            {
                cell.Attributes.Add("class", css);
            }
            if (!string.IsNullOrEmpty(id))
            {
                cell.ID = id;
            }
            cell.RowSpan = RowSpan;
            cell.ColSpan = ColSpan;
            return cell;
        }

        public static HtmlTableCell CreateHtmlCellNoCss(string Html, string id)
        {
            return CreateHtmlCell(Html, null, id, 1, 1);
        }

        public static HtmlTableCell CreateHtmlCellNoCss(string Html, string id, int ColSpan)
        {
            return CreateHtmlCell(Html, null, id, 1, ColSpan);
        }

        public static HtmlTableCell CreateHtmlCellNoCss(string Html, string id, int RowSpan, int ColSpan)
        {
            return CreateHtmlCell(Html, null, id, RowSpan, 1);
        }

        public static HtmlTableRow CreateHtmlRow(params HtmlTableCell[] cells)
        {
            return CreateHtmlRow(null, null, cells);
        }

        public static HtmlTableRow CreateHtmlRow(string css)
        {
            HtmlTableRow row = new HtmlTableRow();
            if (!string.IsNullOrEmpty(css))
            {
                row.Attributes.Add("class", css);
            }
            return row;
        }

        public static HtmlTableRow CreateHtmlRow(string css, params HtmlTableCell[] cells)
        {
            return CreateHtmlRow(null, css, cells);
        }

        public static HtmlTableRow CreateHtmlRow(HtmlTable table, params HtmlTableCell[] cells)
        {
            return CreateHtmlRow(table, null, cells);
        }

        public static HtmlTableRow CreateHtmlRow(HtmlTable table, string css, params HtmlTableCell[] cells)
        {
            HtmlTableRow row = new HtmlTableRow();
            if (!string.IsNullOrEmpty(css))
            {
                row.Attributes.Add("class", css);
            }
            foreach (HtmlTableCell cell in cells)
            {
                row.Cells.Add(cell);
            }
            if (table != null)
            {
                table.Rows.Add(row);
            }
            return row;
        }

        public static string CreateImage(string srcurl, string css, string id, string altext)
        {
            return ("<img alt='" + HttpUtility.HtmlEncode(altext) + "' src='" + srcurl + "' class='" + css + "' id='" + id + "'/>");
        }

        public static HtmlTableCell CreateTextCell(string Text)
        {
            return CreateTextCell(Text, null, null, 1, 1);
        }

        public static HtmlTableCell CreateTextCell(string Text, int ColSpan)
        {
            return CreateTextCell(Text, null, null, 1, ColSpan);
        }

        public static HtmlTableCell CreateTextCell(string Text, string css)
        {
            return CreateTextCell(Text, css, null, 1, 1);
        }

        public static HtmlTableCell CreateTextCell(string Text, int RowSpan, int ColSpan)
        {
            return CreateTextCell(Text, null, null, RowSpan, ColSpan);
        }

        public static HtmlTableCell CreateTextCell(string Text, string css, int ColSpan)
        {
            return CreateTextCell(Text, css, null, 1, ColSpan);
        }

        public static HtmlTableCell CreateTextCell(string Text, string css, int RowSpan, int ColSpan)
        {
            return CreateTextCell(Text, css, null, RowSpan, ColSpan);
        }

        public static HtmlTableCell CreateTextCell(string Text, string css, string id, int rowspan, int colspan)
        {
            HtmlTableCell cell = new HtmlTableCell();
            string [] lines=Text==null?new string[]{""}: Text.Split('\n');
            string html=lines[0];
            for(int i=1;i<lines.Length;i++)
                html+="<br/>"+lines[i];
            cell.InnerHtml = html;
            if (!string.IsNullOrEmpty(css))
            {
                cell.Attributes.Add("class", css);
            }
            if (!string.IsNullOrEmpty(id))
            {
                cell.ID = id;
            }
            cell.ColSpan = colspan;
            cell.RowSpan = rowspan;
            return cell;
        }

        public static HtmlTableCell CreateTextCellNoCss(string Text, string id)
        {
            return CreateTextCell(Text, null, id, 1, 1);
        }

        public static HtmlTableCell CreateTextCellNoCss(string Text, string id, int ColSpan)
        {
            return CreateTextCell(Text, null, id, 1, ColSpan);
        }

        public static HtmlTableCell CreateTextCellNoCss(string Text, string id, int RowSpan, int ColSpan)
        {
            return CreateTextCell(Text, null, id, RowSpan, ColSpan);
        }

        public static string GetAppHTMLRoot()
        {
            return (Application.StartupPath + @"\HTMLTemplate\");
        }

        public static string GetAppImageRoot()
        {
            return (Application.StartupPath + @"\HTMLTemplate\Images\");
        }

        public static string PathToUrl(string path)
        {
            return ("file:///" + path.Replace('\\', '/'));
        }

        public static void ViewInApplication(ApplicationType at, string caption, string html)
        {
            string path = Helpers.GetTempName() + ".htm";
            File.WriteAllText(path, html, Encoding.Unicode);
            Helpers.ViewHTMLFileInAnApp(caption, path, at);
        }
        public static string HtmlEncode(string text)
        {
            string html = HttpUtility.HtmlEncode(text);
            if (html != null)
                return html.Replace("\r\n", "<br/>").Replace("\n","<br/");
            return html;
        }
    }
}

