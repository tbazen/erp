
    using System;
    using System.IO;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
namespace INTAPS.UI.HTML
{

    public class HTMLBuilderBase
    {
        protected IHTMLDocumentHost m_Host;

        public virtual void Build()
        {
        }

        public virtual HtmlControl GetControl()
        {
            return null;
        }

        public virtual string GetOuterHtml()
        {
            StringWriter writer = new StringWriter();
            this.GetControl().RenderControl(new HtmlTextWriter(writer));
            writer.Close();
            string str = writer.ToString();
            writer.Dispose();
            return str;
        }

        public virtual void SetHost(IHTMLDocumentHost host)
        {
            this.m_Host = host;
        }

        public virtual string WindowCaption
        {
            get
            {
                return "";
            }
        }
    }
}

