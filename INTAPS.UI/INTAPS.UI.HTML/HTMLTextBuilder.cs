
    using System;
    using System.Collections;
namespace INTAPS.UI.HTML
{

    public class HTMLTextBuilder : IHTMLBuilder
    {
        private string m_Caption;
        private string m_html;

        public HTMLTextBuilder(string caption, string txt)
        {
            this.m_html = txt;
            this.m_Caption = caption;
        }

        public void Build()
        {
        }

        public string GetOuterHtml()
        {
            return this.m_html;
        }

        public bool ProcessUrl(string path, Hashtable query)
        {
            return false;
        }

        public void SetHost(IHTMLDocumentHost host)
        {
        }

        public string WindowCaption
        {
            get
            {
                return this.m_Caption;
            }
        }
    }
}

