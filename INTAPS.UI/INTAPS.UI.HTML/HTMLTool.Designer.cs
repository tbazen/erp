﻿namespace INTAPS.UI.HTML
{
    partial class HTMLExportTool
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonExport = new System.Windows.Forms.Button();
            this.contexExport = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.woredToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internetExplorerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contexExport.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExport
            // 
            this.buttonExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonExport.Location = new System.Drawing.Point(0, 0);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(125, 27);
            this.buttonExport.TabIndex = 0;
            this.buttonExport.Text = "Export";
            this.buttonExport.Click += new System.EventHandler(this.simpleExport_Click);
            // 
            // contexExport
            // 
            this.contexExport.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excelToolStripMenuItem,
            this.woredToolStripMenuItem,
            this.internetExplorerToolStripMenuItem});
            this.contexExport.Name = "contexExport";
            this.contexExport.Size = new System.Drawing.Size(161, 92);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.excelToolStripMenuItem.Text = "Excel";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.excelToolStripMenuItem_Click);
            // 
            // woredToolStripMenuItem
            // 
            this.woredToolStripMenuItem.Name = "woredToolStripMenuItem";
            this.woredToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.woredToolStripMenuItem.Text = "Word";
            this.woredToolStripMenuItem.Click += new System.EventHandler(this.woredToolStripMenuItem_Click);
            // 
            // internetExplorerToolStripMenuItem
            // 
            this.internetExplorerToolStripMenuItem.Name = "internetExplorerToolStripMenuItem";
            this.internetExplorerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.internetExplorerToolStripMenuItem.Text = "Internet Explorer";
            this.internetExplorerToolStripMenuItem.Click += new System.EventHandler(this.internetExplorerToolStripMenuItem_Click);
            // 
            // HTMLTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonExport);
            this.Name = "HTMLTool";
            this.Size = new System.Drawing.Size(125, 27);
            this.contexExport.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonExport;
        private System.Windows.Forms.ContextMenuStrip contexExport;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem woredToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internetExplorerToolStripMenuItem;
    }
}
