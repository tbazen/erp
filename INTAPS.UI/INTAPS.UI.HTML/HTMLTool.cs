﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.UI.HTML
{
    public delegate string GenerateHTMLDelegate(out string headers);
    public partial class HTMLExportTool : UserControl
    {
        INTAPS.UI.HTML.ControlBrowser _browser;
        GenerateHTMLDelegate _delegate = null;
        public HTMLExportTool()
        {
            InitializeComponent();
            setBrowser(null);
        }

        public void setDelegate(GenerateHTMLDelegate genHtml)
        {
            _delegate = genHtml;
            buttonExport.Enabled = _browser != null || _delegate != null;
        }
        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                this.buttonExport.ForeColor = value;
                base.ForeColor = value;
            }
        }
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                this.buttonExport.BackColor = value;
                base.BackColor = value;
            }
        }
        public override Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                this.buttonExport.Font = value;
                base.Font = value;
            }
        }
        public System.Windows.Forms.FlatStyle FlatStyle
        {
            get
            {
                if (this.buttonExport == null)
                    return System.Windows.Forms.FlatStyle.Standard;

                return this.buttonExport.FlatStyle;
            }
            set
            {
                if (this.buttonExport == null)
                    return;
                
                this.buttonExport.FlatStyle = value;
            }
        }
        public void setBrowser(INTAPS.UI.HTML.ControlBrowser browser)
        {
            _browser = browser;
            buttonExport.Enabled = _browser != null || _delegate!=null;
        }
        private void simpleExport_Click(object sender, EventArgs e)
        {
            contexExport.Show(buttonExport.PointToScreen(new Point(0, buttonExport.Height + 3)));
        }
        private void exportThroughDelegate(ApplicationType applicationType)
        {
            try
            {
                string head;
                string html = _delegate(out head);
                INTAPS.UI.HTML.Helpers.ViewHTMLWithHeadInAnApp(html, head, applicationType);
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error generating export data", ex);
            }
        }

        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_delegate == null)
                this._browser.ViewInApplication(INTAPS.UI.HTML.ApplicationType.Excel);
            else
            {
                exportThroughDelegate(INTAPS.UI.HTML.ApplicationType.Excel);
            }
        }



        private void woredToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_delegate == null)
                this._browser.ViewInApplication(INTAPS.UI.HTML.ApplicationType.Word);
            else
            {
                exportThroughDelegate(INTAPS.UI.HTML.ApplicationType.Word);
            }
        }

        private void internetExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_delegate == null)
                this._browser.ViewInApplication(INTAPS.UI.HTML.ApplicationType.IExplorer);
            else
            {
                exportThroughDelegate(INTAPS.UI.HTML.ApplicationType.IExplorer);
            }
        }
    }
}
