
    using System;
    using System.Diagnostics;
    using System.IO;
namespace INTAPS.UI.HTML
{

    public class HTMLWriter : StreamWriter
    {
        private string m_AppName;
        private ApplicationType m_at;
        private string m_fn;
        private string m_Title;

        public HTMLWriter(string Title, ApplicationType at) : this(Title, at, Helpers.GetTempName() + ".htm")
        {
        }

        public HTMLWriter(string Title, ApplicationType at, string FileName) : base(FileName)
        {
            this.m_fn = "";
            this.m_Title = Title;
            this.m_at = at;
            string str = "";
            this.m_AppName = "";
            switch (at)
            {
                case ApplicationType.IExplorer:
                case ApplicationType.Integrated:
                    str = "";
                    break;

                case ApplicationType.Excel:
                    str = "<meta name=ProgId content=Excel.Sheet>";
                    this.m_AppName = "Excel.exe";
                    break;

                case ApplicationType.Word:
                    str = "<meta name=ProgId content=Word.Document>";
                    this.m_AppName = "Winword.exe";
                    break;
            }
            base.Write("<html><Title>" + Title + "</Title><head>" + str + "</head><body>");
            this.m_fn = FileName;
        }

        public override void Close()
        {
            base.Write("</body></html>");
            base.Close();
        }

        public void OpenApplication()
        {
            if (this.m_at == ApplicationType.IExplorer)
            {
                Process.Start(this.m_fn);
            }
            else
            {
                Process.Start(this.m_AppName, this.m_fn);
            }
        }
    }
}

