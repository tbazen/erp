
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Xml.Serialization;
namespace INTAPS.UI.HTML
{

    public class Helpers
    {
        public static string ControlToString(Control c)
        {
            StringWriter writer = new StringWriter();
            HtmlTextWriter writer2 = new HtmlTextWriter(writer);
            c.RenderControl(writer2);
            return writer.ToString();
        }

        public static string GetHTMLTableFromDataTable(DataTable Dt, int border)
        {
            HtmlTableCell cell;
            HtmlTable c = new HtmlTable();
            c.Border = border;
            c.CellPadding = 0;
            c.CellSpacing = 0;
            HtmlTableRow row = new HtmlTableRow();
            row.BgColor = "WhiteGray";
            row.Align = "Left";
            foreach (DataColumn column in Dt.Columns)
            {
                cell = new HtmlTableCell();
                cell.InnerText = column.ColumnName;
                row.Cells.Add(cell);
            }
            c.Rows.Add(row);
            foreach (DataRow row2 in Dt.Rows)
            {
                row = new HtmlTableRow();
                foreach (object obj2 in row2.ItemArray)
                {
                    string str;
                    if (obj2 == null)
                    {
                        str = "";
                    }
                    else if (obj2 == DBNull.Value)
                    {
                        str = "";
                    }
                    else
                    {
                        str = obj2.ToString();
                    }
                    cell = new HtmlTableCell();
                    cell.InnerText = str;
                    row.Cells.Add(cell);
                }
                c.Rows.Add(row);
            }
            return ControlToString(c);
        }

        public static void GetHTMLTableFromDataTable(DataTable dtca, TextWriter writer)
        {
            HtmlTableCell cell;
            HtmlTable table = new HtmlTable();
            table.Border = 1;
            HtmlTableRow row = new HtmlTableRow();
            row.BgColor = "LightGray";
            foreach (DataColumn column in dtca.Columns)
            {
                cell = new HtmlTableCell();
                cell.InnerText = column.ColumnName;
                row.Cells.Add(cell);
            }
            table.Rows.Add(row);
            foreach (DataRow row2 in dtca.Rows)
            {
                row = new HtmlTableRow();
                foreach (object obj2 in row2.ItemArray)
                {
                    string str;
                    if (obj2 == null)
                    {
                        str = "";
                    }
                    else if (obj2 == DBNull.Value)
                    {
                        str = "";
                    }
                    else
                    {
                        str = obj2.ToString();
                    }
                    cell = new HtmlTableCell();
                    cell.InnerText = str;
                    row.Cells.Add(cell);
                }
                table.Rows.Add(row);
            }
            table.RenderControl(new HtmlTextWriter(writer));
        }

        public static string GetTempName()
        {
            return (Environment.GetEnvironmentVariable("Temp") + Guid.NewGuid().ToString());
        }

        public static string ObjectToXml(object o)
        {
            XmlSerializer serializer = new XmlSerializer(o.GetType());
            StringWriter writer = new StringWriter();
            serializer.Serialize((TextWriter) writer, o);
            writer.Close();
            return writer.ToString();
        }

        public static void ViewHTMLFileInAnApp(string Title, string fn, ApplicationType at)
        {
            string fileName = "";
            switch (at)
            {
                case ApplicationType.Excel:
                    fileName = "Excel.exe";
                    break;

                case ApplicationType.Word:
                    fileName = "Winword.exe";
                    break;
            }
            if (at == ApplicationType.IExplorer)
            {
                Process.Start(fn);
            }
            else
            {
                Process.Start(fileName, fn);
            }
        }

        public static void ViewHTMLInAnApp(string Title, string html, ApplicationType at)
        {
            ViewHTMLInAnApp(Title, html, at, GetTempName() + ".htm");
        }

        public static void ViewHTMLInAnApp(string Title, string html, ApplicationType at, string fn)
        {
            ViewHTMLInAnApp(Title, html, at, fn, "");
        }
     
        public static void ViewHTMLInAnApp(string Title, string html, ApplicationType at, string fn, string head)
        {
            string str = "";
            string fileName = "";
            switch (at)
            {
                case ApplicationType.IExplorer:
                case ApplicationType.Integrated:
                    str = "";
                    break;

                case ApplicationType.Excel:
                    str = "<meta name=ProgId content=Excel.Sheet>";
                    fileName = "Excel.exe";
                    break;

                case ApplicationType.Word:
                    str = "<meta name=ProgId content=Word.Document>";
                    fileName = "Winword.exe";
                    break;
            }
            html = "<html><title>" + Title + "</title><head>" + str + head + "</head><body>" + html + "</body></html>";
            FileStream stream = new FileStream(fn, FileMode.CreateNew);
            stream.Write(new byte[] { 0xff, 0xfe }, 0, 2);
            stream.Write(Encoding.Unicode.GetBytes(html), 0, Encoding.Unicode.GetByteCount(html));
            stream.Close();
            if (at == ApplicationType.IExplorer)
            {
                Process.Start(fn);
            }
            else
            {
                Process.Start(fileName, fn);
            }
        }
        public static void ViewHTMLWithHeadInAnApp(string body, string head, ApplicationType at)
        {
            ViewHTMLWithHeadInAnApp(body, head, at, GetTempName() + ".htm");
        }
        public static void ViewHTMLWithHeadInAnApp(string body, string head, ApplicationType at, string fn)
        {
            string str = "";
            string fileName = "";
            switch (at)
            {
                case ApplicationType.IExplorer:
                case ApplicationType.Integrated:
                    str = "";
                    break;

                case ApplicationType.Excel:
                    str = "<meta name=ProgId content=Excel.Sheet>";
                    fileName = "Excel.exe";
                    break;

                case ApplicationType.Word:
                    str = "<meta name=ProgId content=Word.Document>";
                    fileName = "Winword.exe";
                    break;
            }
            string html = "<html><head>" + str + head + "</head><body>" + body+ "</body></html>";
            FileStream stream = new FileStream(fn, FileMode.CreateNew);
            stream.Write(new byte[] { 0xff, 0xfe }, 0, 2);
            stream.Write(Encoding.Unicode.GetBytes(html), 0, Encoding.Unicode.GetByteCount(html));
            stream.Close();
            if (at == ApplicationType.IExplorer)
            {
                Process.Start(fn);
            }
            else
            {
                Process.Start(fileName, fn);
            }
        }
        public static void ViewHTMLInIE(string Title, string html)
        {
            ViewHTMLInAnApp(Title, html, ApplicationType.IExplorer);
        }

        public static object XmlToObject(string xml, Type type)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }
            XmlSerializer serializer = new XmlSerializer(type);
            return serializer.Deserialize(new StringReader(xml));
        }
    }
}

