
    using System;
    using System.Collections;
namespace INTAPS.UI.HTML
{

    public interface IHTMLBuilder
    {
        void Build();
        string GetOuterHtml();
        bool ProcessUrl(string path, Hashtable query);
        void SetHost(IHTMLDocumentHost host);

        string WindowCaption { get; }
    }
    public interface IHTMLBuilderWithHeader
    {
        string getHeaderItems();
    }
}

