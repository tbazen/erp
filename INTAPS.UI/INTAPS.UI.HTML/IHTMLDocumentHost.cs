
    using System;
    using System.Web.UI.HtmlControls;
    using System.Windows.Forms;
namespace INTAPS.UI.HTML
{

    public interface IHTMLDocumentHost
    {
        System.Windows.Forms.HtmlElement GetElement(string id);
        System.Windows.Forms.HtmlElement UpdateElement(string id, HtmlControl Control);
        System.Windows.Forms.HtmlElement UpdateElementInnerHtml(string id, string InnerHtml);
    }
}

