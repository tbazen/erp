
    using System;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Xml.Serialization;
namespace INTAPS.UI.UIState
{

    [StructLayout(LayoutKind.Sequential), XmlInclude(typeof(Size))]
    public struct ControlSize
    {
        public Size sz;
    }
}

