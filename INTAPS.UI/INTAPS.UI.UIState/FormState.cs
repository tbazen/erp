
    using System;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using System.Xml.Serialization;
namespace INTAPS.UI.UIState
{

    [StructLayout(LayoutKind.Sequential), XmlInclude(typeof(Point))]
    public struct FormState
    {
        public FormWindowState windowState;
        public Point formPosition;
        public Size formSize;
    }
}

