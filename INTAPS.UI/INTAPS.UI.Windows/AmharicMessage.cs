
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public partial class AmharicMessage : Form
    {
        
        private IContainer components;
        
        private AmharicMessage()
        {
            this.components = null;
            this.InitializeComponent();
        }

        public AmharicMessage(string msg) : this(msg, "CIS Archive Message")
        {
        }

        public AmharicMessage(string msg, string title) : this()
        {
            this.Text = title;
            this.txtMessage.Text = msg;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.OK;
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

