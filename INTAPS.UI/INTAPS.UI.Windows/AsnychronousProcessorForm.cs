
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public class AsnychronousProcessorForm : BatchJobForm
    {
        private IContainer components;
        private AsynchronousProcessorBase m_processor;

        public AsnychronousProcessorForm() : this(null)
        {
        }

        public AsnychronousProcessorForm(AsynchronousProcessorBase p)
        {
            this.components = null;
            this.InitializeComponent();
            this.m_processor = p;
        }

        protected override BatchJobStatus CheckJob(int index)
        {
            return this.m_processor.GetStatus(index);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected virtual object GetStartParameter()
        {
            return null;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            base.AutoScaleMode = AutoScaleMode.Font;
            this.Text = "AsnychronousProcessorForm";
        }

        protected override void StartJob()
        {
            this.m_processor.Start(this.GetStartParameter());
        }

        protected override string JobCommandName
        {
            get
            {
                return "Start";
            }
        }
    }
}

