﻿namespace INTAPS.UI.Windows
{
    public partial class AsyncForm : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AsyncForm));
            this.pnlWaitPanel = new System.Windows.Forms.Panel();
            this.lblDesc = new System.Windows.Forms.Label();
            this.pbWaitAnimation = new System.Windows.Forms.PictureBox();
            this.timerWait = new System.Windows.Forms.Timer(this.components);
            this.pnlWaitPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlWaitPanel
            // 
            this.pnlWaitPanel.BackColor = System.Drawing.Color.White;
            this.pnlWaitPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlWaitPanel.Controls.Add(this.lblDesc);
            this.pnlWaitPanel.Controls.Add(this.pbWaitAnimation);
            this.pnlWaitPanel.Location = new System.Drawing.Point(103, 100);
            this.pnlWaitPanel.Name = "pnlWaitPanel";
            this.pnlWaitPanel.Size = new System.Drawing.Size(310, 45);
            this.pnlWaitPanel.TabIndex = 0;
            this.pnlWaitPanel.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.BackColor = System.Drawing.Color.White;
            this.lblDesc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesc.Location = new System.Drawing.Point(39, 0);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(269, 43);
            this.lblDesc.TabIndex = 1;
            this.lblDesc.Text = "##";
            this.lblDesc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbWaitAnimation
            // 
            this.pbWaitAnimation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbWaitAnimation.ErrorImage = null;
            this.pbWaitAnimation.Image = global::INTAPS.UI.Properties.Resources.WaitAnimation;
            this.pbWaitAnimation.Location = new System.Drawing.Point(0, 0);
            this.pbWaitAnimation.Name = "pbWaitAnimation";
            this.pbWaitAnimation.Size = new System.Drawing.Size(39, 43);
            this.pbWaitAnimation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWaitAnimation.TabIndex = 0;
            this.pbWaitAnimation.TabStop = false;
            this.pbWaitAnimation.Click += new System.EventHandler(this.pbWaitAnimation_Click);
            // 
            // timerWait
            // 
            this.timerWait.Interval = 500;
            this.timerWait.Tick += new System.EventHandler(this.timerWait_Tick);
            // 
            // AsyncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 281);
            this.Controls.Add(this.pnlWaitPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AsyncForm";
            this.Text = "Async Form";
            this.pnlWaitPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWaitAnimation)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Panel pnlWaitPanel;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.PictureBox pbWaitAnimation;
        private System.Windows.Forms.Timer timerWait;
    }
}