
    using INTAPS.UI;
    using INTAPS.UI.Properties;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public partial class AsyncForm : Form
    {
        
        
        private ITabMainWindow m_MainWindow;
        private ServerDownloaderState m_PrevState;
        protected bool m_SyncMode;
        private ServerDataDownloader m_WaitFor;
        private int N_Tick;
        
        
        private const int SHOW_AFTER = 2;
        

        public AsyncForm() : this(null)
        {
        }

        public AsyncForm(ITabMainWindow mainwindow)
        {
            this.components = null;
            this.m_PrevState = ServerDownloaderState.Unknown;
            this.m_SyncMode = false;
            this.InitializeComponent();
            this.m_MainWindow = mainwindow;
        }

        protected virtual void CheckProgress()
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void EnableForm(bool Enable)
        {
            foreach (Control control in base.Controls)
            {
                if (control != this.pnlWaitPanel)
                {
                    control.Enabled = Enable;
                }
            }
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.IsBusy)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please wait until the process finishes");
                e.Cancel = true;
            }
        }

        protected override void OnResize(EventArgs e)
        {
            if (this.ShowAnmiation)
            {
                int num = (base.Width - this.pnlWaitPanel.Width) / 2;
                if (num < 0)
                {
                    this.pnlWaitPanel.Left = 0;
                }
                else
                {
                    this.pnlWaitPanel.Left = num;
                }
                this.pnlWaitPanel.Top = (base.Height - this.pnlWaitPanel.Height) / 2;
            }
            base.OnResize(e);
        }

        private void pbWaitAnimation_Click(object sender, EventArgs e)
        {
        }

        private void timerWait_Tick(object sender, EventArgs e)
        {
            this.CheckProgress();
            if (this.m_WaitFor.State != this.m_PrevState)
            {
                if (this.m_WaitFor.State == ServerDownloaderState.Complete)
                {
                    this.timerWait.Enabled = false;
                    if (this.ShowAnmiation)
                    {
                        this.pnlWaitPanel.Visible = false;
                    }
                    this.EnableForm(true);
                    this.WaitIsOver(this.m_WaitFor);
                }
                else
                {
                    this.N_Tick++;
                    if ((this.N_Tick >= 2) && this.ShowAnmiation)
                    {
                        this.pnlWaitPanel.Visible = true;
                        this.pnlWaitPanel.BringToFront();
                    }
                }
            }
        }

        public void WaitFor(ServerDataDownloader d)
        {
            this.m_WaitFor = d;
            if (this.m_SyncMode)
            {
                if (!this.m_WaitFor.IsBusy)
                {
                    this.m_WaitFor.RunDownloader();
                }
                this.WaitIsOver(d);
            }
            else
            {
                if (!this.m_WaitFor.IsBusy)
                {
                    this.m_WaitFor.Start();
                }
                this.lblDesc.Text = d.Description;
                this.N_Tick = 0;
                this.EnableForm(false);
                this.timerWait.Enabled = true;
                this.timerWait_Tick(this.timerWait, null);
            }
        }

        protected virtual void WaitIsOver(ServerDataDownloader d)
        {
            if ((!base.Visible && (this.m_MainWindow != null)) && (this.m_MainWindow.ShowConformation(d.Description + " has finished. Do you want to see it?") == DialogResult.Yes))
            {
                this.m_MainWindow.SelectPage(this);
            }
        }

        public bool IsBusy
        {
            get
            {
                if (this.m_WaitFor == null)
                {
                    return false;
                }
                return this.m_WaitFor.IsBusy;
            }
        }

        protected virtual bool ShowAnmiation
        {
            get
            {
                return true;
            }
        }
    }
}

