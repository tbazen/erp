﻿namespace INTAPS.UI.Windows
{
    public partial class BatchJobForm : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager manager = new System.ComponentModel.ComponentResourceManager(typeof(BatchJobForm));
            this.txtErrorInfo = new System.Windows.Forms.TextBox();
            this.lblErrorLabel = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.pbGenerate = new System.Windows.Forms.ProgressBar();
            this.lblDetailLabel = new System.Windows.Forms.Label();
            this.lvErrors = new System.Windows.Forms.ListView();
            this.colName = new System.Windows.Forms.ColumnHeader();
            this.colInformation = new System.Windows.Forms.ColumnHeader();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.btnExport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblStatus = new System.Windows.Forms.Label();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            base.SuspendLayout();
            this.txtErrorInfo.BackColor = System.Drawing.Color.White;
            this.txtErrorInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtErrorInfo.ForeColor = System.Drawing.Color.Red;
            this.txtErrorInfo.Location = new System.Drawing.Point(0, 0xc5);
            this.txtErrorInfo.Multiline = true;
            this.txtErrorInfo.Name = "txtErrorInfo";
            this.txtErrorInfo.ReadOnly = true;
            this.txtErrorInfo.Size = new System.Drawing.Size(0x201, 0x75);
            this.txtErrorInfo.TabIndex = 5;
            this.txtErrorInfo.TextChanged += new System.EventHandler(this.txtErrorInfo_TextChanged);
            this.lblErrorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblErrorLabel.Location = new System.Drawing.Point(0, 0);
            this.lblErrorLabel.Name = "lblErrorLabel";
            this.lblErrorLabel.Size = new System.Drawing.Size(0x201, 0x1d);
            this.lblErrorLabel.TabIndex = 6;
            this.lblErrorLabel.Text = "Errors";
            this.lblErrorLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnGenerate.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top;
            this.btnGenerate.Location = new System.Drawing.Point(0x29b, 0x1d);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(0x4b, 0x17);
            this.btnGenerate.TabIndex = 7;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            this.pbGenerate.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top;
            this.pbGenerate.Location = new System.Drawing.Point(12, 0x1d);
            this.pbGenerate.Name = "pbGenerate";
            this.pbGenerate.Size = new System.Drawing.Size(0x289, 0x17);
            this.pbGenerate.TabIndex = 8;
            this.lblDetailLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblDetailLabel.Location = new System.Drawing.Point(0, 0xa6);
            this.lblDetailLabel.Name = "lblDetailLabel";
            this.lblDetailLabel.Size = new System.Drawing.Size(0x201, 0x1f);
            this.lblDetailLabel.TabIndex = 6;
            this.lblDetailLabel.Text = "Message";
            this.lblDetailLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lvErrors.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { this.colName, this.colInformation });
            this.lvErrors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvErrors.ForeColor = System.Drawing.Color.Red;
            this.lvErrors.FullRowSelect = true;
            this.lvErrors.Location = new System.Drawing.Point(0, 0x1d);
            this.lvErrors.Name = "lvErrors";
            this.lvErrors.Size = new System.Drawing.Size(0x201, 0x89);
            this.lvErrors.TabIndex = 11;
            this.lvErrors.UseCompatibleStateImageBehavior = false;
            this.lvErrors.View = System.Windows.Forms.View.Details;
            this.lvErrors.SelectedIndexChanged += new System.EventHandler(this.lvErrors_SelectedIndexChanged);
            this.colName.Text = "Item";
            this.colName.Width = 0x9f;
            this.colInformation.Text = "Message";
            this.colInformation.Width = 0x167;
            this.timer.Interval = 300;
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Panel2.Controls.Add(this.lvErrors);
            this.splitContainer.Panel2.Controls.Add(this.lblErrorLabel);
            this.splitContainer.Panel2.Controls.Add(this.lblDetailLabel);
            this.splitContainer.Panel2.Controls.Add(this.txtErrorInfo);
            this.splitContainer.Size = new System.Drawing.Size(0x336, 0x13a);
            this.splitContainer.SplitterDistance = 0x131;
            this.splitContainer.TabIndex = 12;
            this.btnExport.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Top;
            this.btnExport.Location = new System.Drawing.Point(0x2ec, 0x1d);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(0x41, 0x17);
            this.btnExport.TabIndex = 7;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            this.panel1.Controls.Add(this.lblStatus);
            this.panel1.Controls.Add(this.pbGenerate);
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.btnGenerate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 0x13a);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(0x336, 0x40);
            this.panel1.TabIndex = 13;
            this.lblStatus.Anchor = System.Windows.Forms.AnchorStyles.Right | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Top;
            this.lblStatus.Location = new System.Drawing.Point(12, 2);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0x327, 0x17);
            this.lblStatus.TabIndex = 9;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStatus.Click += new System.EventHandler(this.lblStatus_Click);
            base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            base.ClientSize = new System.Drawing.Size(0x336, 0x17a);
            base.Controls.Add(this.splitContainer);
            base.Controls.Add(this.panel1);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            base.Icon = (System.Drawing.Icon)manager.GetObject("$this.Icon");
            base.Name = "BatchJobForm";
            this.Text = "Generate Bills";
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            this.splitContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            base.ResumeLayout(false);
        }
        #endregion
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.TextBox txtErrorInfo;
        private System.Windows.Forms.Label lblErrorLabel;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.ProgressBar pbGenerate;
        private System.Windows.Forms.Label lblDetailLabel;
        private System.Windows.Forms.ListView lvErrors;
        private System.Windows.Forms.ColumnHeader colName;
        private System.Windows.Forms.ColumnHeader colInformation;
        private System.Windows.Forms.Timer timer;
        protected System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblStatus;

    }
}