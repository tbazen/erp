
    using INTAPS.UI;
    using INTAPS.UI.HTML;
    using System;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public partial class BatchJobForm : Form
    {
        private bool m_generating;
        private int m_index;
        protected bool m_unattended;
        private int n_errors;
        
        public BatchJobForm() : this(false)
        {
        }

        public BatchJobForm(bool unattedned)
        {
            this.components = null;
            this.m_generating = false;
            this.m_index = -1;
            this.n_errors = 0;
            this.m_unattended = false;
            this.InitializeComponent();
            this.btnGenerate.Text = this.JobCommandName;
            this.m_unattended = unattedned;
            if (this.m_unattended)
            {
                this.btnGenerate_Click(null, null);
            }
        }

        private void AddResults(BatchJobStatus status)
        {
            if (status.newResults != null)
            {
                foreach (BatchJobResult result in status.newResults)
                {
                    ListViewItem item = new ListViewItem(result.item);
                    item.SubItems.Add(result.statusDescription);
                    item.Tag = result.ex;
                    switch (result.resultType)
                    {
                        case BatchJobResultType.Warning:
                            item.ForeColor = Color.Orange;
                            break;
                        case BatchJobResultType.Error:
                            item.ForeColor = Color.Red;
                            this.n_errors++;
                            break;
                        case BatchJobResultType.Ok:
                            item.ForeColor = Color.Green;
                            break;

                        default:
                            item.ForeColor = Color.Black;
                            break;
                    }
                    this.lvErrors.Items.Add(item);
                }
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable("Errors");
            dt.Columns.Add("No");
            dt.Columns.Add("Item");
            dt.Columns.Add("Description");
            dt.Columns.Add("Exception");
            dt.Columns.Add("Stack");
            int num = 1;
            foreach (ListViewItem item in this.lvErrors.Items)
            {
                Exception tag = item.Tag as Exception;
                dt.Rows.Add(new object[] { num.ToString(), item.Text, item.SubItems[1].Text, (tag == null) ? "" : tag.Message, (tag == null) ? "" : tag.StackTrace });
                num++;
            }
            string hTMLTableFromDataTable = Helpers.GetHTMLTableFromDataTable(dt, 1);
            Helpers.ViewHTMLInAnApp("Error list", hTMLTableFromDataTable, ApplicationType.IExplorer);
        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            Exception exception;
            if (this.m_generating)
            {
                try
                {
                    this.StopJob();
                    this.TimerTick(this.timer, null);
                }
                catch (Exception exception1)
                {
                    exception = exception1;
                    this.panel1.Enabled = true;
                    this.timer.Enabled = false;
                    this.btnGenerate.Text = this.JobCommandName;
                    this.SetGenerating(false);
                    if (!this.m_unattended)
                    {
                        UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to stop job.", exception);
                    }
                }
            }
            else
            {
                try
                {
                    this.n_errors = 0;
                    this.m_index = -1;
                    this.lvErrors.Items.Clear();
                    this.StartJob();
                    this.btnGenerate.Text = "Stop";
                    this.SetGenerating(true);
                    this.pbGenerate.Maximum = 100;
                    this.pbGenerate.Value = 0;
                    this.timer.Enabled = true;
                    this.panel1.Enabled = true;
                }
                catch (Exception exception2)
                {
                    exception = exception2;
                    if (!this.m_unattended)
                    {
                        UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to generate payroll.", exception);
                    }
                }
            }
        }

        protected virtual BatchJobStatus CheckJob(int index)
        {
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected virtual void FinishedJob(BatchJobStatus stat)
        {
        }


        private void lblStatus_Click(object sender, EventArgs e)
        {
        }

        private void lvErrors_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lvErrors.SelectedItems.Count == 0)
            {
                this.txtErrorInfo.Text = "";
            }
            else
            {
                Exception tag = this.lvErrors.SelectedItems[0].Tag as Exception;
                if (tag == null)
                {
                    this.txtErrorInfo.Text = this.lvErrors.SelectedItems[0].SubItems[1].Text;
                }
                else
                {
                    this.txtErrorInfo.Text = tag.Message + "\r\n" + tag.StackTrace;
                }
                this.txtErrorInfo.ForeColor = this.lvErrors.SelectedItems[0].ForeColor;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = this.m_generating;
            base.OnClosing(e);
        }

        private void SetGenerating(bool gen)
        {
            this.m_generating = gen;
            this.splitContainer.Panel1.Enabled = !gen;
            this.btnGenerate.Enabled = !gen;
        }

        protected virtual void StartJob()
        {
        }

        protected virtual void StopJob()
        {
        }

        private void TimerTick(object sender, EventArgs e)
        {
            try
            {
                BatchJobStatus status = this.CheckJob(this.m_index);
                this.m_index = status.resultIndex;
                if (status.stage != BatchJobStage.Running)
                {
                    this.SetGenerating(false);
                    this.timer.Enabled = false;
                    this.btnGenerate.Text = this.JobCommandName;
                    this.pbGenerate.Value = 0;
                    this.AddResults(status);
                    if (!this.m_unattended)
                    {
                        if (this.n_errors == 0)
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job completed without errors.");
                        }
                        else
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job completed with " + this.n_errors + " errors.");
                        }
                    }
                    this.FinishedJob(status);
                }
                else
                {
                    this.AddResults(status);
                    this.lblStatus.Text = string.IsNullOrEmpty(status.statusText) ? "" : status.statusText;
                    if (status.progress < 0.0)
                    {
                        this.pbGenerate.Value = 0;
                    }
                    else
                    {
                        this.pbGenerate.Value = (int) (status.progress * 100.0);
                    }
                }
            }
            catch (Exception exception)
            {
                this.timer.Enabled = false;
                this.btnGenerate.Text = this.JobCommandName;
                this.pbGenerate.Value = 0;
                this.SetGenerating(false);
                if (!this.m_unattended)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error waiting for job to finish", exception);
                }
            }
            finally
            {
                Application.DoEvents();
            }
        }

        private void txtErrorInfo_TextChanged(object sender, EventArgs e)
        {
        }

        protected virtual string JobCommandName
        {
            get
            {
                return "Start";
            }
        }


    }
}

