
    using System;
namespace INTAPS.UI.Windows
{

    public interface IHostableForm
    {
        bool CanClose();
    }
}

