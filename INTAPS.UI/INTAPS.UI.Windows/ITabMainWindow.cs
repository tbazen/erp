
    using INTAPS.Ethiopic;
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public interface ITabMainWindow
    {
        void AddToKeyMapper(Control c);
        void ClosePage(Control c);
        void CloseSideBar(Control c);
        SideBarContainer GetSideBarContainer(Control c);
        int PickMainObject();
        void RemoveFromKeyMapper(Control c);
        void SelectPage(Control cont);
        void ShowAmharicMessage(string Msg);
        void ShowCadastreCodeContextMenu();
        DialogResult ShowConformation(string Msg);
        void ShowData(int p);
        void ShowError(string Msg);
        DialogResult ShowInputForm(string p, string p_2, out string text);
        void ShowMessage(string Msg);
        void ShowOwnedForm(System.Windows.Forms.Form f);
        void ShowPage(System.Windows.Forms.Form f);
        void ShowSideBar(Control c, string Title);
        void ShowWarning(string Msg);
        DialogResult ShowYesNoCancl(string Msg);

        System.Windows.Forms.Form Form { get; }

        INTAPS.Ethiopic.KeyMapper KeyMapper { get; }

        bool KMEnabled { get; set; }
    }
}

