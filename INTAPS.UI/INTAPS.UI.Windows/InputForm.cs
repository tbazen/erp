
    using INTAPS.Ethiopic;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public partial class InputForm : Form
    {
        
        
        private Container components;
        public string InputString;
        
        

        public InputForm() : this(null)
        {
        }

        public InputForm(KeyMapper mapper)
        {
            this.components = null;
            this.InitializeComponent();
            if (mapper != null)
            {
                mapper.AddControl(this.txtInput);
                this.txtInput.Font = new Font("Ethiopia Jiret", 14f);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.DialogResult = DialogResult.Cancel;
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.InputString = this.txtInput.Text;
            base.DialogResult = DialogResult.OK;
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public DialogResult Show(string Title, string Prompt, string Default)
        {
            this.Text = Title;
            this.lblPrompt.Text = Prompt;
            this.txtInput.Text = Default;
            this.txtInput.SelectAll();
            this.txtInput.Focus();
            return base.ShowDialog();
        }
    }
}

