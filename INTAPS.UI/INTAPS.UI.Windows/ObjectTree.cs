
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public abstract class ObjectTree : TreeView
    {
        private Dictionary<System.Type, List<int>> LoadedNodes = new Dictionary<System.Type, List<int>>();

        public TreeNode AddItem(object item, System.Type parentType)
        {
            return this.AddItem(item, parentType, true);
        }

        public TreeNode AddItem(object item, System.Type parentType, bool loadChilds)
        {
            TreeNode node;
            int itemPID = this.GetItemPID(item);
            if (itemPID == -1)
            {
                node = this.AddItemNode(base.Nodes, item);
            }
            else
            {
                TreeNode node2 = this.SearchTree(base.Nodes, itemPID, parentType);
                if (node2 == null)
                {
                    return null;
                }
                node = this.AddItemNode(node2.Nodes, item);
            }
            if (loadChilds && ((node.Parent == null) || node.Parent.IsExpanded))
            {
                this.LoadChilds(node.Nodes, item);
            }
            return node;
        }

        protected virtual TreeNode AddItemNode(TreeNodeCollection nodes, object item)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = 1;
            n.SelectedImageIndex = 1;
            this.SetItemNode(n, item);
            nodes.Add(n);
            return n;
        }

        private void AddItemNodes(TreeNodeCollection nodes, object parent)
        {
            object[] childItems = this.GetChildItems(parent);
            foreach (object obj2 in childItems)
            {
                this.AddItemNode(nodes, obj2);
            }
        }

        protected void DeleteItem(int ID, System.Type t)
        {
            TreeNode node = this.SearchTree(base.Nodes, ID, t);
            if (node != null)
            {
                this.RemoveNode(node);
            }
        }

        protected abstract object[] GetChildItems(object item);
        protected abstract int GetItemID(object item);
        protected abstract int GetItemPID(object item);
        protected abstract string GetItemText(object item);
        private void LoadChildOfChilds(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                this.LoadChilds(node.Nodes, node.Tag);
            }
        }

        private void LoadChilds(TreeNodeCollection nodes, object parent)
        {
            int itemID = this.GetItemID(parent);
            if (itemID != -1)
            {
                List<int> list;
                System.Type key = parent.GetType();
                if (this.LoadedNodes.ContainsKey(key))
                {
                    list = this.LoadedNodes[key];
                    if (list.Contains(itemID))
                    {
                        return;
                    }
                    list.Add(itemID);
                }
                else
                {
                    list = new List<int>();
                    list.Add(itemID);
                    this.LoadedNodes.Add(key, list);
                }
            }
            object[] childItems = this.GetChildItems(parent);
            foreach (object obj2 in childItems)
            {
                this.AddItemNode(nodes, obj2);
            }
        }

        public void LoadData()
        {
            base.Nodes.Clear();
            this.LoadedNodes.Clear();
            this.LoadChilds(base.Nodes, null);
            this.LoadChildOfChilds(base.Nodes);
            foreach (TreeNode node in base.Nodes)
            {
                node.Expand();
            }
        }

        protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
        {
            this.LoadChildOfChilds(e.Node.Nodes);
        }

        protected virtual void RemoveNode(TreeNode node)
        {
            node.Remove();
        }

        private TreeNode SearchTree(TreeNodeCollection nodes, int ID, System.Type t)
        {
            foreach (TreeNode node in nodes)
            {
                if ((node.Tag.GetType() == t) && (this.GetItemID(node.Tag) == ID))
                {
                    return node;
                }
                TreeNode node2 = this.SearchTree(node.Nodes, ID, t);
                if (node2 != null)
                {
                    return node2;
                }
            }
            return null;
        }

        protected virtual void SetItemNode(TreeNode n, object item)
        {
            n.Text = this.GetItemText(item);
            n.Tag = item;
        }

        public void UpdateItem(object Item)
        {
            TreeNode n = this.SearchTree(base.Nodes, this.GetItemID(Item), Item.GetType());
            if (n != null)
            {
                this.SetItemNode(n, Item);
            }
        }

        public object SelectedObject
        {
            get
            {
                if (base.SelectedNode == null)
                {
                    return null;
                }
                return base.SelectedNode.Tag;
            }
        }
    }

    public abstract class ObjectTreeObjectID : TreeView
    {

        private Dictionary<System.Type, List<object>> LoadedNodes = new Dictionary<System.Type, List<object>>();

        public TreeNode AddItem(object item, System.Type parentType)
        {
            return this.AddItem(item, parentType, true);
        }

        public TreeNode AddItem(object item, System.Type parentType, bool loadChilds)
        {
            TreeNode node;
            object itemPID = this.GetItemPID(item);
            if (itemPID == null)
            {
                node = this.AddItemNode(base.Nodes, item);
            }
            else
            {
                TreeNode node2 = this.SearchTree(base.Nodes, itemPID, parentType);
                if (node2 == null)
                {
                    return null;
                }
                node = this.AddItemNode(node2.Nodes, item);
            }
            if (loadChilds && ((node.Parent == null) || node.Parent.IsExpanded))
            {
                this.LoadChilds(node.Nodes, item);
            }
            return node;
        }

        protected virtual TreeNode AddItemNode(TreeNodeCollection nodes, object item)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = 1;
            n.SelectedImageIndex = 1;
            this.SetItemNode(n, item);
            nodes.Add(n);
            return n;
        }

        private void AddItemNodes(TreeNodeCollection nodes, object parent)
        {
            object[] childItems = this.GetChildItems(parent);
            foreach (object obj2 in childItems)
            {
                this.AddItemNode(nodes, obj2);
            }
        }

        protected void DeleteItem(object ID, System.Type t)
        {
            TreeNode node = this.SearchTree(base.Nodes, ID, t);
            if (node != null)
            {
                this.RemoveNode(node);
            }
        }

        protected abstract object[] GetChildItems(object item);
        protected abstract object GetItemID(object item);
        protected abstract object GetItemPID(object item);
        protected abstract string GetItemText(object item);
        private void LoadChildOfChilds(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                this.LoadChilds(node.Nodes, node.Tag);
            }
        }

        private void LoadChilds(TreeNodeCollection nodes, object parent)
        {
            object itemID = this.GetItemID(parent);
            if (itemID != null)
            {
                List<object> list;
                System.Type key = parent.GetType();
                if (this.LoadedNodes.ContainsKey(key))
                {
                    list = this.LoadedNodes[key];
                    if (list.Contains(itemID))
                    {
                        return;
                    }
                    list.Add(itemID);
                }
                else
                {
                    list = new List<object>();
                    list.Add(itemID);
                    this.LoadedNodes.Add(key, list);
                }
            }
            object[] childItems = this.GetChildItems(parent);
            foreach (object obj2 in childItems)
            {
                this.AddItemNode(nodes, obj2);
            }
        }

        public void LoadData()
        {
            base.Nodes.Clear();
            this.LoadedNodes.Clear();
            this.LoadChilds(base.Nodes, null);
            this.LoadChildOfChilds(base.Nodes);
            foreach (TreeNode node in base.Nodes)
            {
                node.Expand();
            }
        }

        protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
        {
            this.LoadChildOfChilds(e.Node.Nodes);
        }

        protected virtual void RemoveNode(TreeNode node)
        {
            node.Remove();
        }

        private TreeNode SearchTree(TreeNodeCollection nodes, object ID, System.Type t)
        {
            foreach (TreeNode node in nodes)
            {
                if ((node.Tag.GetType() == t) && (this.GetItemID(node.Tag) == ID))
                {
                    return node;
                }
                TreeNode node2 = this.SearchTree(node.Nodes, ID, t);
                if (node2 != null)
                {
                    return node2;
                }
            }
            return null;
        }

        protected virtual void SetItemNode(TreeNode n, object item)
        {
            n.Text = this.GetItemText(item);
            n.Tag = item;
        }

        public void UpdateItem(object Item)
        {
            TreeNode n = this.SearchTree(base.Nodes, this.GetItemID(Item), Item.GetType());
            if (n != null)
            {
                this.SetItemNode(n, Item);
            }
        }

        public object SelectedObject
        {
            get
            {
                if (base.SelectedNode == null)
                {
                    return null;
                }
                return base.SelectedNode.Tag;
            }
        }
    }
}

