
    using System;
namespace INTAPS.UI.Windows
{

    public abstract class ObjectTreeSimpleTyped<ObjectType> : ObjectTree where ObjectType: class
    {
        protected ObjectTreeSimpleTyped()
        {
        }

        protected override object[] GetChildItems(object item)
        {
            ObjectType local = item as ObjectType;
            return (object[]) this.GetObjectChilds(local);
        }

        protected override int GetItemID(object item)
        {
            ObjectType local = item as ObjectType;
            return this.GetObjectID(local);
        }

        protected override int GetItemPID(object item)
        {
            ObjectType local = item as ObjectType;
            return this.GetObjectPID(local);
        }

        protected override string GetItemText(object item)
        {
            ObjectType local = item as ObjectType;
            return this.GetObjectString(local);
        }

        protected abstract ObjectType[] GetObjectChilds(ObjectType obj);
        protected abstract int GetObjectID(ObjectType obj);
        protected abstract int GetObjectPID(ObjectType obj);
        protected abstract string GetObjectString(ObjectType obj);
    }
}

