
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public abstract class ObjectTreeTyped<FolderType, ItemType> : TreeView where FolderType: class where ItemType: class
    {
        private List<int> LoadedNodes;
        private bool m_IncludeItems;

        public ObjectTreeTyped()
        {
            this.m_IncludeItems = false;
            this.LoadedNodes = new List<int>();
        }

        public TreeNode AddFolder(FolderType folder)
        {
            TreeNode node;
            if (this.GetFolderPID(folder) == -1)
            {
                node = this.AddFolderNode(base.Nodes, folder);
            }
            else
            {
                TreeNode node2 = this.SearchTree(base.Nodes, this.GetFolderPID(folder), true);
                if (node2 == null)
                {
                    return null;
                }
                node = this.AddFolderNode(node2.Nodes, folder);
            }
            if ((node.Parent == null) || node.Parent.IsExpanded)
            {
                this.LoadChilds(node.Nodes, this.GetFolderID(folder));
            }
            return node;
        }

        private TreeNode AddFolderNode(TreeNodeCollection parent, FolderType unit)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = 0;
            n.SelectedImageIndex = 0;
            this.SetNode(n, unit);
            parent.Add(n);
            return n;
        }

        protected void AddItem(ItemType item)
        {
            if (this.m_IncludeItems)
            {
                TreeNode node = this.SearchTree(base.Nodes, this.GetItemFolderID(item), true);
                if (node != null)
                {
                    this.AddItemNode(node.Nodes, item);
                }
            }
        }

        protected virtual TreeNode AddItemNode(TreeNodeCollection nodes, ItemType e)
        {
            TreeNode n = new TreeNode();
            n.ImageIndex = 1;
            n.SelectedImageIndex = 1;
            this.SetItemNode(n, e);
            nodes.Add(n);
            return n;
        }

        private void AddItemNodes(TreeNodeCollection nodes, int OrgID)
        {
            ItemType[] folderItems = this.GetFolderItems(OrgID);
            foreach (ItemType local in folderItems)
            {
                this.AddItemNode(nodes, local);
            }
        }

        private void AddRemoveItemNodes(TreeNodeCollection nodes, bool Add)
        {
            foreach (TreeNode node in nodes)
            {
                if (Add)
                {
                    this.AddItemNodes(node.Nodes, this.GetFolderID((FolderType) node.Tag));
                }
                else if (node.Tag is ItemType)
                {
                    node.Remove();
                }
                this.AddRemoveItemNodes(node.Nodes, Add);
            }
        }

        protected void DeleteFolder(int ID)
        {
            TreeNode node = this.SearchTree(base.Nodes, ID, true);
            if (node != null)
            {
                node.Remove();
            }
        }

        protected void DeleteItem(int ID)
        {
            if (this.m_IncludeItems)
            {
                TreeNode node = this.SearchTree(base.Nodes, ID, false);
                if (node != null)
                {
                    node.Remove();
                }
            }
        }

        protected abstract FolderType[] GetChildFolders(int FolderID);
        protected abstract int GetFolderID(FolderType f);
        protected abstract ItemType[] GetFolderItems(int FolderID);
        protected abstract int GetFolderPID(FolderType f);
        protected abstract string GetFolderText(FolderType f);
        protected abstract int GetItemFolderID(ItemType item);
        protected abstract int GetItemID(ItemType item);
        protected abstract string GetItemText(ItemType item);
        private void LoadChildOfChilds(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.Tag is FolderType)
                {
                    this.LoadChilds(node.Nodes, this.GetFolderID((FolderType) node.Tag));
                }
            }
        }

        private void LoadChilds(TreeNodeCollection nodes, int PID)
        {
            if (!this.LoadedNodes.Contains(PID))
            {
                this.LoadedNodes.Add(PID);
                FolderType[] childFolders = this.GetChildFolders(PID);
                foreach (FolderType local in childFolders)
                {
                    this.AddFolderNode(nodes, local);
                }
                if (this.m_IncludeItems)
                {
                    this.AddItemNodes(nodes, PID);
                }
            }
        }

        public void LoadData()
        {
            base.Nodes.Clear();
            this.LoadChilds(base.Nodes, -1);
            this.LoadChildOfChilds(base.Nodes);
            foreach (TreeNode node in base.Nodes)
            {
                node.Expand();
            }
        }

        protected override void OnBeforeExpand(TreeViewCancelEventArgs e)
        {
            this.LoadChildOfChilds(e.Node.Nodes);
        }

        private TreeNode SearchTree(TreeNodeCollection nodes, int ID, bool Org)
        {
            foreach (TreeNode node in nodes)
            {
                TreeNode node2;
                if (Org)
                {
                    if (node.Tag is FolderType)
                    {
                        if (this.GetFolderID((FolderType) node.Tag) == ID)
                        {
                            return node;
                        }
                        node2 = this.SearchTree(node.Nodes, ID, Org);
                        if (node2 != null)
                        {
                            return node2;
                        }
                    }
                }
                else
                {
                    if ((node.Tag is ItemType) && (this.GetItemID((ItemType) node.Tag) == ID))
                    {
                        return node;
                    }
                    node2 = this.SearchTree(node.Nodes, ID, Org);
                    if (node2 != null)
                    {
                        return node2;
                    }
                }
            }
            return null;
        }

        protected void SetIncludeItems(bool value)
        {
            if (this.m_IncludeItems != value)
            {
                this.m_IncludeItems = value;
                this.AddRemoveItemNodes(base.Nodes, value);
            }
        }

        private void SetItemNode(TreeNode n, ItemType e)
        {
            n.Text = this.GetItemText(e);
            n.Tag = e;
        }

        private void SetNode(TreeNode n, FolderType unit)
        {
            n.Text = this.GetFolderText(unit);
            n.Tag = unit;
        }

        public void UpdateFolder(FolderType folder)
        {
            TreeNode n = this.SearchTree(base.Nodes, this.GetFolderID(folder), true);
            if (n != null)
            {
                this.SetNode(n, folder);
            }
        }

        protected void UpdateItem(ItemType item)
        {
            if (this.m_IncludeItems)
            {
                TreeNode n = this.SearchTree(base.Nodes, this.GetItemID(item), false);
                if (n != null)
                {
                    this.SetItemNode(n, item);
                }
            }
        }

        public FolderType SelectedFolder
        {
            get
            {
                if (base.SelectedNode == null)
                {
                    return default(FolderType);
                }
                return (base.SelectedNode.Tag as FolderType);
            }
        }

        public ItemType SelectedItem
        {
            get
            {
                if (base.SelectedNode == null)
                {
                    return default(ItemType);
                }
                return (base.SelectedNode.Tag as ItemType);
            }
        }
    }
}

