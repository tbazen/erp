
using System;
namespace INTAPS.UI.Windows
{

    public class OperationCanceledByUser : Exception
    {
        public OperationCanceledByUser() : base("You canceled the operation.")
        {
        }
    }
}

