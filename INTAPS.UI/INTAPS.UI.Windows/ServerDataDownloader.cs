
    using System;
namespace INTAPS.UI.Windows
{

    public abstract class ServerDataDownloader
    {
        private AsyncCallback m_AsyncCallBack;
        private AsyncServerDownloadDelegate m_AsyncCallDelegate;
        private IAsyncResult m_asyncRes;
        private Exception m_LastException;
        private ServerDownloaderState m_State = ServerDownloaderState.Idle;

        public ServerDataDownloader()
        {
            this.m_AsyncCallBack = new AsyncCallback(this.DownloadComplete);
            this.m_AsyncCallDelegate = null;
            this.m_asyncRes = null;
        }

        protected void CheckBusy()
        {
            if (this.m_State != ServerDownloaderState.Complete)
            {
                new WaitForm().WaitFor(true, true, new ServerDataDownloader[] { this });
            }
        }

        private void DownloadComplete(IAsyncResult res)
        {
            try
            {
                this.m_AsyncCallDelegate.EndInvoke(res);
                this.m_State = ServerDownloaderState.Complete;
            }
            catch (Exception exception)
            {
                this.m_State = ServerDownloaderState.Failed;
                this.m_LastException = exception;
            }
        }

        public abstract void RunDownloader();
        public void Start()
        {
            if (this.IsBusy)
            {
                throw new Exception("The downloader is already busy.");
            }
            this.m_AsyncCallDelegate = new AsyncServerDownloadDelegate(this.RunDownloader);
            try
            {
                this.m_State = ServerDownloaderState.Busy;
                this.m_asyncRes = this.m_AsyncCallDelegate.BeginInvoke(this.m_AsyncCallBack, null);
            }
            catch (Exception exception)
            {
                this.m_State = ServerDownloaderState.Failed;
                this.m_LastException = exception;
            }
        }

        public abstract string Description { get; }

        public bool IsBusy
        {
            get
            {
                return ((this.m_asyncRes != null) && !this.m_asyncRes.IsCompleted);
            }
        }

        public Exception LastException
        {
            get
            {
                return this.m_LastException;
            }
            set
            {
                this.m_LastException = value;
            }
        }

        public ServerDownloaderState State
        {
            get
            {
                return this.m_State;
            }
        }
    }
}

