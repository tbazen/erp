
    using System;
namespace INTAPS.UI.Windows
{

    public enum ServerDownloaderState
    {
        Unknown,
        Idle,
        Complete,
        Busy,
        Failed
    }
}

