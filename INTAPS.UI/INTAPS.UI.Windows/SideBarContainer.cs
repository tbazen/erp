
    using System;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public class SideBarContainer : Panel
    {
        private Label lblTitle;
        private Button m_btn;
        private Control m_Hosted;
        private const int TITLE_HEGHT = 20;

        public event FormClosedEventHandler Closed;

        public event FormClosingEventHandler Closing;

        public SideBarContainer(Control c, string Title)
        {
            this.m_Hosted = c;
            Panel panel = new Panel();
            panel.Dock = DockStyle.Top;
            panel.BackColor = Color.LightBlue;
            panel.Height = 20;
            this.lblTitle = new Label();
            this.lblTitle.AutoSize = false;
            this.lblTitle.TextAlign = ContentAlignment.MiddleLeft;
            this.lblTitle.Text = Title;
            this.lblTitle.Dock = DockStyle.Fill;
            this.m_btn = new Button();
            this.m_btn.Text = "X";
            this.m_btn.Width = 20;
            this.m_btn.FlatStyle = FlatStyle.Popup;
            this.m_btn.Dock = DockStyle.Right;
            this.m_btn.Click += new EventHandler(this.btn_Click);
            panel.Controls.Add(this.m_btn);
            panel.Controls.Add(this.lblTitle);
            c.Visible = true;
            c.Dock = DockStyle.Fill;
            if (c is Form)
            {
                Form form = (Form) c;
                form.TopLevel = false;
                form.FormBorderStyle = FormBorderStyle.None;
                base.Controls.Add(c);
                c.Show();
            }
            else
            {
                base.Controls.Add(c);
            }
            base.Controls.Add(panel);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            if (this.Closing != null)
            {
                FormClosingEventArgs args = new FormClosingEventArgs(CloseReason.UserClosing, false);
                this.Closing(this, args);
                if (args.Cancel)
                {
                    return;
                }
            }
            if (this.Closed != null)
            {
                base.Controls.Remove(this.m_Hosted);
                this.m_Hosted.Visible = false;
                if (this.m_Hosted is Form)
                {
                    ((Form) this.m_Hosted).Close();
                }
                FormClosedEventArgs args2 = new FormClosedEventArgs(CloseReason.UserClosing);
                this.Closed(this, args2);
            }
        }

        public bool CloseEnabled
        {
            get
            {
                return this.m_btn.Enabled;
            }
            set
            {
                this.m_btn.Enabled = value;
                this.m_btn.Visible = value;
            }
        }

        public Control HostedControl
        {
            get
            {
                return this.m_Hosted;
            }
        }

        public string Title
        {
            get
            {
                return this.lblTitle.Text;
            }
            set
            {
                this.lblTitle.Text = value;
            }
        }
    }
}

