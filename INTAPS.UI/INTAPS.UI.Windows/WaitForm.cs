
using INTAPS.UI.Properties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.UI.Windows
{

    public partial class WaitForm : Form
    {
        public WaitForm()
        {
            this.InitializeComponent();
        }

        private Panel CreateWaitPanel(ServerDataDownloader d)
        {
            Panel panel = new Panel();
            PictureBox box = new PictureBox();
            box.Tag = ServerDownloaderState.Unknown;
            box.Dock = DockStyle.Left;
            box.Size = new Size(0x30, 0x30);
            box.SizeMode = PictureBoxSizeMode.CenterImage;
            Label label = new Label();
            label.Text = d.Description;
            label.AutoSize = false;
            label.TextAlign = ContentAlignment.MiddleLeft;
            label.Dock = DockStyle.Fill;
            panel.Controls.Add(label);
            panel.Controls.Add(box);
            panel.Tag = d;
            panel.BackColor = Color.White;
            panel.BorderStyle = BorderStyle.FixedSingle;
            panel.Dock = DockStyle.Top;
            panel.Height = 0x30;
            return panel;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        protected override void OnClosed(EventArgs e)
        {
            this.timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                this.UpdateStatus();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void UpdateStatus()
        {
            bool flag = true;
            foreach (Panel panel in base.Controls)
            {
                PictureBox box = (PictureBox)panel.Controls[1];
                ServerDownloaderState tag = (ServerDownloaderState)box.Tag;
                Label label = (Label)panel.Controls[0];
                ServerDataDownloader downloader = (ServerDataDownloader)panel.Tag;
                flag &= downloader.State == ServerDownloaderState.Complete;
                if (downloader.State != tag)
                {
                    if (downloader.State == ServerDownloaderState.Busy)
                    {
                        box.Image = Resources.WaitAnimation;
                    }
                    else
                    {
                        if (downloader.State == ServerDownloaderState.Failed)
                        {
                            base.Close();
                            this.timer1.Stop();
                            throw downloader.LastException;
                        }
                        box.Image = null;
                    }
                    box.Tag = downloader.State;
                }
            }
            if (flag)
            {
                base.Close();
            }
        }

        public void WaitFor(bool Show, bool Modal, params ServerDataDownloader[] downloaders)
        {
            base.Controls.Clear();
            foreach (ServerDataDownloader downloader in downloaders)
            {
                base.Controls.Add(this.CreateWaitPanel(downloader));
            }
            this.UpdateStatus();
            if (Show)
            {
                if (Modal)
                {
                    base.ShowDialog();
                }
                else
                {
                    base.Show();
                }
            }
        }
    }
}

