
    using System;
    using System.Collections.Generic;
    using System.Threading;
namespace INTAPS.UI
{

    public class AsynchronousProcessorBase
    {
        private int m_maximum;
        private Thread m_processorThread;
        private int m_progress;
        private List<BatchJobResult> m_results = new List<BatchJobResult>();
        private BatchJobStage m_stage;
        private string m_statusText;

        public AsynchronousProcessorBase()
        {
            this.Reset();
        }

        private void AddResult(BatchJobResult r)
        {
            lock (this.m_results)
            {
                this.AddResult(r);
            }
        }

        protected void AddTextResult(string status, Exception ex)
        {
            this.AddTextResultWithProgress(status, -1, -1, BatchJobResultType.Ok, ex);
        }

        protected void AddTextResult(string status, BatchJobResultType type)
        {
            this.AddTextResultWithProgress(status, -1, -1, type, null);
        }

        protected void AddTextResult(string status, BatchJobResultType type, Exception ex)
        {
            this.AddTextResultWithProgress(status, -1, -1, type, ex);
        }

        protected void AddTextResultWithProgress(string status, int progress, int max, BatchJobResultType type, Exception ex)
        {
            BatchJobResult item = new BatchJobResult("", "", status, ex, type);
            this.m_maximum = max;
            this.m_progress = progress;
            this.m_results.Add(item);
        }

        public virtual BatchJobStatus GetStatus(int lastResultIndex)
        {
            lock (this.m_results)
            {
                BatchJobResult[] array = new BatchJobResult[this.m_results.Count - (lastResultIndex + 1)];
                this.m_results.CopyTo(lastResultIndex + 1, array, 0, array.Length);
                BatchJobStatus status = new BatchJobStatus();
                status.resultIndex = lastResultIndex + array.Length;
                status.newResults = array;
                status.progress = (this.m_maximum > 0) ? (((double) this.m_progress) / ((double) this.m_maximum)) : -1.0;
                status.retData = null;
                status.stage = this.m_stage;
                status.statusText = this.m_statusText;
                return status;
            }
        }

        private void Reset()
        {
            this.m_results.Clear();
            this.m_stage = BatchJobStage.Idle;
            this.m_statusText = "Ready";
            this.m_maximum = -1;
            this.m_progress = -1;
            this.m_processorThread = null;
        }

        public virtual void RunJob(object par)
        {
        }

        protected void SetProgress(int val, int max)
        {
            lock (this.m_results)
            {
                this.m_progress = val;
                this.m_maximum = max;
            }
        }

        protected void SetStage(BatchJobStage batchJobStage)
        {
            lock (this.m_results)
            {
                this.m_stage = batchJobStage;
            }
        }

        public void Start(object par)
        {
            lock (this.m_results)
            {
                if (((this.m_processorThread != null) && this.m_processorThread.IsAlive) || (this.m_stage == BatchJobStage.Running))
                {
                    throw new Exception(this.JobName + " is busy.");
                }
                this.Reset();
                this.m_processorThread = new Thread(new ParameterizedThreadStart(this.RunJob));
                this.m_stage = BatchJobStage.Running;
                this.m_processorThread.Start(par);
            }
        }

        public virtual string JobName
        {
            get
            {
                return this.ToString();
            }
        }
    }
}

