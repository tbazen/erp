
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class ButtonNavigator : IEnterNavigable
    {
        private Button m_but;

        public event EventHandler GoToNext;

        public ButtonNavigator(Button but)
        {
            this.m_but = but;
            this.m_but.Click += new EventHandler(this.OnClik);
        }

        private void OnClik(object sender, EventArgs e)
        {
            this.GoToNext(this, null);
        }

        public void StartEdit()
        {
            this.m_but.Focus();
        }

        public Control ControlObject
        {
            get
            {
                return this.m_but;
            }
        }

        public bool Skip
        {
            get
            {
                return (!this.m_but.Enabled || !this.m_but.Visible);
            }
        }
    }
}

