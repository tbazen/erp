
    using System;
    using System.Collections;
    using System.Collections.Generic;
namespace INTAPS.UI
{

    public class ChangeList<ObjectType> : IEnumerable<ObjectType>, IEnumerable
    {
        public List<ObjectType> deletedObjects;
        public List<ObjectType> newObjects;
        public List<ObjectType> updated;

        public ChangeList()
        {
            this.newObjects = new List<ObjectType>();
            this.updated = new List<ObjectType>();
            this.deletedObjects = new List<ObjectType>();
        }

        public void DeleteObject(ObjectType obj)
        {
            int index = this.updated.IndexOf(obj);
            if (index != -1)
            {
                this.updated.RemoveAt(index);
                this.deletedObjects.Add(obj);
            }
            else
            {
                index = this.newObjects.IndexOf(obj);
                if (index != -1)
                {
                    this.newObjects.RemoveAt(index);
                }
                else
                {
                    this.deletedObjects.Add(obj);
                }
            }
        }

        public IEnumerator<ObjectType> GetEnumerator()
        {
            return new ChangeListEnumeration<ObjectType>((ChangeList<ObjectType>) this);
        }

        public void NewObject(ObjectType obj)
        {
            if (this.newObjects.Contains(obj))
            {
                throw new Exception("Object allready exists");
            }
            this.newObjects.Add(obj);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void UpdateObject(ObjectType obj)
        {
            if (!this.updated.Contains(obj) && !this.newObjects.Contains(obj))
            {
                this.updated.Add(obj);
            }
        }

        public bool HasChanges
        {
            get
            {
                return (((this.newObjects.Count > 0) || (this.updated.Count > 0)) || (this.deletedObjects.Count > 0));
            }
        }
    }
}

