
    using System;
    using System.Collections;
    using System.Collections.Generic;
namespace INTAPS.UI
{

    public class ChangeListEnumeration<ObjectType> : IEnumerator<ObjectType>, IDisposable, IEnumerator
    {
        private IEnumerator<ObjectType> m_currentEnumerator;
        private List<ObjectType> m_currentList;
        private ChangeList<ObjectType> m_list;

        public ChangeListEnumeration(ChangeList<ObjectType> list)
        {
            this.m_list = list;
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            if (this.m_currentEnumerator == null)
            {
                this.Reset();
            }
            if (!this.m_currentEnumerator.MoveNext())
            {
                if (this.m_currentList == this.m_list.deletedObjects)
                {
                    return false;
                }
                if (this.m_currentList == this.m_list.newObjects)
                {
                    this.m_currentList = this.m_list.updated;
                }
                else
                {
                    this.m_currentList = this.m_list.deletedObjects;
                }
                this.m_currentEnumerator = this.m_currentList.GetEnumerator();
                return this.m_currentEnumerator.MoveNext();
            }
            return true;
        }

        public void Reset()
        {
            this.m_currentList = this.m_list.newObjects;
            this.m_currentEnumerator = this.m_currentList.GetEnumerator();
        }

        public ObjectType Current
        {
            get
            {
                return this.m_currentEnumerator.Current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                if (this.m_currentEnumerator == null)
                {
                    this.Reset();
                }
                return this.m_currentEnumerator.Current;
            }
        }
    }
}

