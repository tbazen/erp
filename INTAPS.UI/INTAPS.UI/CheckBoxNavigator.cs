
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class CheckBoxNavigator : IEnterNavigable
    {
        private CheckBox m_chk;

        public event EventHandler GoToNext;

        public CheckBoxNavigator(CheckBox chk)
        {
            this.m_chk = chk;
            this.m_chk.KeyDown += new KeyEventHandler(this.OnKeyDown);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.GoToNext(this, null);
            }
        }

        public void StartEdit()
        {
            this.m_chk.Focus();
        }

        public Control ControlObject
        {
            get
            {
                return this.m_chk;
            }
        }

        public bool Skip
        {
            get
            {
                return (!this.m_chk.Enabled || !this.m_chk.Visible);
            }
        }
    }
}

