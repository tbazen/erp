
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class ComboBoxLookupMapper : FieldControlMapper
    {
        private string[] _display;
        private object[] _vals;

        public ComboBoxLookupMapper(System.Type enumType)
        {
            Array values = Enum.GetValues(enumType);
            this._display = Enum.GetNames(enumType);
            this._vals = new object[values.Length];
            for (int i = 0; i < this._display.Length; i++)
            {
                this._display[i] = this._display[i].Replace('_', ' ');
                this._vals[i] = values.GetValue(i);
            }
        }

        public ComboBoxLookupMapper(object[] vals, GetObjectString method)
        {
            this._vals = vals;
            this._display = new string[this._vals.Length];
            for (int i = 0; i < this._display.Length; i++)
            {
                this._display[i] = method(this._vals[i]);
            }
        }

        public ComboBoxLookupMapper(string[] display, object[] vals)
        {
            this._display = display;
            this._vals = vals;
        }

        public override object GetData(object control, out bool valid)
        {
            valid = false;
            ComboBox box = (ComboBox) control;
            if (box.SelectedIndex == -1)
            {
                return null;
            }
            valid = true;
            return this._vals[box.SelectedIndex];
        }

        public override void InitializeControl(object control)
        {
            ComboBox box = (ComboBox) control;
            box.Items.Clear();
            box.Items.AddRange(this._display);
        }

        public override void SetData(object control, object value)
        {
            ComboBox box = (ComboBox) control;
            int num = 0;
            foreach (object obj2 in this._vals)
            {
                if (obj2.Equals(value))
                {
                    box.SelectedIndex = num;
                    return;
                }
                num++;
            }
            box.SelectedIndex = -1;
        }
    }
}

