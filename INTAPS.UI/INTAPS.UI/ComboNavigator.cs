
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class ComboNavigator : IEnterNavigable
    {
        private ComboBox m_cb;

        public event EventHandler GoToNext;

        public ComboNavigator(ComboBox cb)
        {
            this.m_cb = cb;
            this.m_cb.KeyPress += new KeyPressEventHandler(this.OnKeyPress);
            this.m_cb.KeyDown += new KeyEventHandler(this.OnKeyDown);
            this.m_cb.SelectedIndexChanged += new EventHandler(this.OnSelectedIndexChanged);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.GoToNext(this, null);
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
            }
        }

        private void OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.GoToNext(this, null);
        }

        public void StartEdit()
        {
            this.m_cb.Focus();
        }

        public Control ControlObject
        {
            get
            {
                return this.m_cb;
            }
        }

        public bool Skip
        {
            get
            {
                return (!this.m_cb.Enabled || !this.m_cb.Visible);
            }
        }
    }
}

