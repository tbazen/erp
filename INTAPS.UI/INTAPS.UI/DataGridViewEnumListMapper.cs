
    using System;
    using System.Collections;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class DataGridViewEnumListMapper : FieldControlMapper
    {
        private System.Type _enumType;

        public DataGridViewEnumListMapper(System.Type t)
        {
            this._enumType = t;
        }

        public override object GetData(object control, out bool valid)
        {
            valid = true;
            DataGridView view = (DataGridView) control;
            Array array = Array.CreateInstance(this._enumType, (int) (view.Rows.Count - 1));
            int index = 0;
            foreach (DataGridViewRow row in (IEnumerable) view.Rows)
            {
                if (index == array.Length)
                {
                    return array;
                }
                object obj2 = Enum.Parse(this._enumType, row.Cells[0].Value.ToString().Replace(' ', '_'));
                array.SetValue(obj2, index);
                index++;
            }
            return array;
        }

        public override void InitializeControl(object control)
        {
            DataGridView view = (DataGridView) control;
            foreach (string str in Enum.GetNames(this._enumType))
            {
                ((DataGridViewComboBoxColumn) view.Columns[0]).Items.Add(str.Replace('_', ' '));
            }
        }

        public override void SetData(object control, object value)
        {
            DataGridView view = (DataGridView) control;
            Array array = (Array) value;
            view.Rows.Clear();
            for (int i = 0; i < array.Length; i++)
            {
                view.Rows.Add(new object[] { array.GetValue(i).ToString().Replace('_', ' ') });
            }
        }
    }
}

