
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using System.Reflection;
namespace INTAPS.UI
{

    public class DataGridViewMapper<ObjectType> : FieldControlMapper where ObjectType: new()
    {
        private Pairs<FieldInfo, DataGridViewColumn>[] _bindings;

        public DataGridViewMapper(Pairs<string, DataGridViewColumn>[] bindings)
        {
            this._bindings = new Pairs<FieldInfo, DataGridViewColumn>[bindings.Length];
            for (int i = 0; i < this._bindings.Length; i++)
            {
                this._bindings[i] = new Pairs<FieldInfo, DataGridViewColumn>(typeof(ObjectType).GetField(bindings[i].first), bindings[i].second);
            }
        }

        public override object GetData(object control, out bool valid)
        {
            DataGridView view = (DataGridView) control;
            List<ObjectType> list = new List<ObjectType>();
            foreach (DataGridViewRow row in (IEnumerable) view.Rows)
            {
                if (!row.IsNewRow)
                {
                    ObjectType local2 = default(ObjectType);
                    ObjectType local = (local2 == null) ? Activator.CreateInstance<ObjectType>() : (local2 = default(ObjectType));
                    foreach (Pairs<FieldInfo, DataGridViewColumn> pairs in this._bindings)
                    {
                        pairs.first.SetValue(local, row.Cells[pairs.second.Index].Value);
                    }
                    list.Add(local);
                }
            }
            valid = true;
            return list.ToArray();
        }

        public override void InitializeControl(object control)
        {
            for (int i = 0; i < this._bindings.Length; i++)
            {
                Pairs<FieldInfo, DataGridViewColumn> pairs = this._bindings[i];
                pairs.second.ValueType = pairs.first.FieldType;
            }
        }

        public override void SetData(object control, object value)
        {
            DataGridView view = (DataGridView) control;
            ObjectType[] localArray = value as ObjectType[];
            view.Rows.Clear();
            if (localArray != null)
            {
                foreach (ObjectType local in localArray)
                {
                    DataGridViewRow row = view.Rows[view.Rows.Add()];
                    foreach (Pairs<FieldInfo, DataGridViewColumn> pairs in this._bindings)
                    {
                        row.Cells[pairs.second.Index].Value = pairs.first.GetValue(local);
                    }
                }
            }
        }
    }
}

