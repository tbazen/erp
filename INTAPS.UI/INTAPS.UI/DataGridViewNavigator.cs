
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class DataGridViewNavigator : IEnterNavigable
    {
        private DataGridView m_grid;

        public event EventHandler GoToNext;

        public DataGridViewNavigator(DataGridView tb)
        {
            this.m_grid = tb;
            this.m_grid.KeyDown += new KeyEventHandler(this.OnKeyDown);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                this.GoToNext(this, null);
            }
        }

        public void StartEdit()
        {
            this.m_grid.Focus();
        }

        public Control ControlObject
        {
            get
            {
                return this.m_grid;
            }
        }

        public bool Skip
        {
            get
            {
                return (!this.m_grid.Enabled || !this.m_grid.Visible);
            }
        }
    }
}

