
    using System;
    using System.Collections;
    using System.IO;
    using System.Threading;
    using System.Windows.Forms;
    using System.Xml;
namespace INTAPS.UI
{

    public class EDataGridView : DataGridView
    {
        private bool _showContextMenu = true;
        private const string CLIPBOARD_FORMAT = "XML Spreadsheet";
        private bool m_HorizontalEnterNavigationMode = true;
        protected MenuItem m_ItemCopy;
        protected MenuItem m_ItemInsert;
        protected MenuItem m_ItemPaste;
        protected ContextMenu m_Menu;
        private bool m_SuppessEnterKey = false;
        private const string XML_TEMPLATE = "<?xml version=\"1.0\"?>\r\n<?mso-application progid=\"Excel.Sheet\"?>\r\n<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\r\n  <Styles>\r\n    <Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n      <Alignment ss:Vertical=\"Bottom\" />\r\n      <Borders />\r\n      <Font />\r\n      <Interior />\r\n      <NumberFormat />\r\n      <Protection />\r\n    </Style>\r\n  </Styles>\r\n  <Worksheet ss:Name=\"Sheet1\">\r\n    <Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"3\"/>\r\n</Worksheet>\r\n</Workbook>\r\n            ";

        public event DataGridViewCellEventHandler EnterComit;

        public EDataGridView()
        {
            this.InitializeContextMenu();
            this.ContextMenu = this.m_Menu;
        }

        public void Copy()
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml("<?xml version=\"1.0\"?>\r\n<?mso-application progid=\"Excel.Sheet\"?>\r\n<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\r\n  <Styles>\r\n    <Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n      <Alignment ss:Vertical=\"Bottom\" />\r\n      <Borders />\r\n      <Font />\r\n      <Interior />\r\n      <NumberFormat />\r\n      <Protection />\r\n    </Style>\r\n  </Styles>\r\n  <Worksheet ss:Name=\"Sheet1\">\r\n    <Table ss:ExpandedColumnCount=\"2\" ss:ExpandedRowCount=\"3\"/>\r\n</Worksheet>\r\n</Workbook>\r\n            ");
            XmlElement element = (XmlElement) document.GetElementsByTagName("Table")[0];
            int num = 0;
            int num2 = 0;
            foreach (DataGridViewRow row in (IEnumerable) base.Rows)
            {
                XmlElement newChild = null;
                int num3 = 0;
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Selected)
                    {
                        if (newChild == null)
                        {
                            newChild = document.CreateElement("Row", element.NamespaceURI);
                        }
                        XmlElement element3 = document.CreateElement("Cell", element.NamespaceURI);
                        XmlElement element4 = document.CreateElement("Data", element.NamespaceURI);
                        XmlAttribute newAttr = document.CreateAttribute("ss:Type", element.NamespaceURI);
                        newAttr.Value = "String";
                        element4.InnerText = (cell.Value == null) ? "" : cell.Value.ToString();
                        element4.SetAttributeNode(newAttr);
                        element3.AppendChild(element4);
                        newChild.AppendChild(element3);
                        num3++;
                    }
                }
                if (num2 < num3)
                {
                    num2 = num3;
                }
                if (newChild != null)
                {
                    element.AppendChild(newChild);
                    num++;
                }
            }
            element.Attributes[0].Value = num2.ToString();
            element.Attributes[1].Value = num.ToString();
            MemoryStream outStream = new MemoryStream();
            document.Save(outStream);
            Clipboard.SetData("XML Spreadsheet", outStream);
        }

        protected void InitializeContextMenu()
        {
            this.m_Menu = new ContextMenu();
            this.m_Menu.Popup += new EventHandler(this.menu_Popup);
            this.m_ItemCopy = new MenuItem("Copy");
            this.m_ItemCopy.Click += new EventHandler(this.itemCopy_Click);
            this.m_Menu.MenuItems.Add(this.m_ItemCopy);
            this.m_ItemPaste = new MenuItem("Paste");
            this.m_ItemPaste.Click += new EventHandler(this.itemPaste_Click);
            this.m_Menu.MenuItems.Add(this.m_ItemPaste);
            this.m_ItemInsert = new MenuItem("Insert");
            this.m_ItemInsert.Click += new EventHandler(this.itemInsert_Click);
            this.m_Menu.MenuItems.Add(this.m_ItemInsert);
        }

        private void itemCopy_Click(object sender, EventArgs e)
        {
            this.Copy();
        }

        private void itemInsert_Click(object sender, EventArgs e)
        {
            base.Rows.Insert(base.CurrentRow.Index, new object[0]);
        }

        private void itemPaste_Click(object sender, EventArgs e)
        {
            this.Paste();
        }

        private void menu_Popup(object sender, EventArgs e)
        {
            this.m_ItemPaste.Enabled = Clipboard.GetDataObject().GetDataPresent("XML Spreadsheet");
            this.m_ItemCopy.Enabled = base.SelectedCells.Count > 0;
            if (base.AllowUserToAddRows)
            {
                this.m_ItemInsert.Enabled = base.CurrentRow.Index < (base.Rows.Count - 2);
            }
            else
            {
                this.m_ItemInsert.Enabled = base.CurrentRow.Index < (base.Rows.Count - 1);
            }
        }

        protected virtual void OnEnterComit(DataGridViewCellEventArgs e)
        {
            if (this.EnterComit != null)
            {
                this.EnterComit(this, e);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (this._showContextMenu && (e.Button == MouseButtons.Right))
            {
                this.m_Menu.Show(this, base.PointToClient(Cursor.Position));
            }
            base.OnMouseDown(e);
        }

        public void Paste()
        {
            IDataObject dataObject = Clipboard.GetDataObject();
            if (!dataObject.GetDataPresent("XML Spreadsheet"))
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No data on the clipboard");
            }
            else
            {
                XmlDocument document = new XmlDocument();
                MemoryStream data = (MemoryStream) dataObject.GetData("XML Spreadsheet");
                if (data != null)
                {
                    document.Load(data);
                    data.Dispose();
                    XmlNodeList elementsByTagName = document.GetElementsByTagName("Table");
                    if (elementsByTagName.Count == 0)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Clipboard do not contain any tabular data.");
                    }
                    else if (elementsByTagName.Count > 1)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Clipboard contains more than one tables.");
                    }
                    else
                    {
                        XmlNodeList list2 = ((XmlElement) elementsByTagName[0]).GetElementsByTagName("Row");
                        int result = 0;
                        int index = base.CurrentRow.Index;
                        int columnIndex = base.CurrentCell.ColumnIndex;
                        foreach (XmlElement element2 in list2)
                        {
                            XmlNode namedItem = element2.Attributes.GetNamedItem("ss:Index");
                            if ((namedItem != null) && int.TryParse(namedItem.InnerText, out result))
                            {
                                result--;
                            }
                            if (base.AllowUserToAddRows)
                            {
                                while ((result + index) >= (base.RowCount - 1))
                                {
                                    base.Rows.Add();
                                }
                            }
                            else if ((result + index) >= base.RowCount)
                            {
                                break;
                            }
                            int num4 = 0;
                            XmlNodeList list3 = element2.GetElementsByTagName("Cell");
                            foreach (XmlNode node2 in list3)
                            {
                                namedItem = node2.Attributes.GetNamedItem("ss:Index");
                                if ((namedItem != null) && int.TryParse(namedItem.InnerText, out num4))
                                {
                                    num4--;
                                }
                                if ((num4 + columnIndex) == base.ColumnCount)
                                {
                                    break;
                                }
                                DataGridViewCell cell = base[num4 + columnIndex, result + index];
                                if ((node2.InnerText != "") && !cell.ReadOnly)
                                {
                                    cell.Value = node2.InnerText;
                                }
                                num4++;
                            }
                            result++;
                        }
                    }
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (!(this.m_HorizontalEnterNavigationMode && (keyData == Keys.Return)))
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }
            if (this.m_SuppessEnterKey)
            {
                base.CommitEdit(DataGridViewDataErrorContexts.Commit);
                base.EndEdit();
                this.OnEnterComit(new DataGridViewCellEventArgs(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex));
                return true;
            }
            int rowIndex = base.CurrentCell.RowIndex;
            int columnIndex = base.CurrentCell.ColumnIndex;
            if (columnIndex < (base.Columns.Count - 1))
            {
                base.CurrentCell = base[columnIndex + 1, rowIndex];
            }
            else if (rowIndex < (base.Rows.Count - 1))
            {
                base.CurrentCell = base[0, rowIndex + 1];
            }
            return true;
        }

        public bool HorizontalEnterNavigationMode
        {
            get
            {
                return this.m_HorizontalEnterNavigationMode;
            }
            set
            {
                this.m_HorizontalEnterNavigationMode = value;
            }
        }

        protected bool ShowContextMenu
        {
            get
            {
                return this._showContextMenu;
            }
            set
            {
                this._showContextMenu = value;
            }
        }

        public bool SuppessEnterKey
        {
            get
            {
                return this.m_SuppessEnterKey;
            }
            set
            {
                this.m_SuppessEnterKey = value;
            }
        }
    }
}

