
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Reflection;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class EnterKeyNavigation : IEnterNavigable
    {
        private ArrayList m_Controls = new ArrayList();
        private bool m_Cycle = false;
        private int m_Tab = 0;
        private ArrayList m_TabPages = new ArrayList();
        private Color m_Temp = Color.Black;

        public event EventHandler GoToNext;

        public void AddNavigableItem(IEnterNavigable o)
        {
            this.m_Controls.Add(o);
            o.GoToNext += new EventHandler(this.OnGotToNext);
            o.ControlObject.Enter += new EventHandler(this.EnerControl);
            o.ControlObject.Leave += new EventHandler(this.OnLeave);
            o.ControlObject.TabIndex = this.m_Tab++;
        }

        public void AddTabControl(TabControl tab)
        {
            foreach (TabPage page in tab.TabPages)
            {
                this.m_TabPages.Add(page);
            }
        }

        public void Close()
        {
        }

        private void EnerControl(object sender, EventArgs e)
        {
            this.m_Temp = ((Control) sender).BackColor;
            ((Control) sender).BackColor = Color.LightBlue;
        }

        ~EnterKeyNavigation()
        {
            this.Close();
        }

        private void FocusControl(int i)
        {
            this[i].StartEdit();
        }

        private bool IsChild(Control par, Control child)
        {
            if (child != null)
            {
                if (par == null)
                {
                    return false;
                }
                if (par.Controls.IndexOf(child) != -1)
                {
                    return true;
                }
                foreach (Control control in par.Controls)
                {
                    if (this.IsChild(control, child))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void OnGotToNext(object sender, EventArgs e)
        {
            int num2;
            IEnterNavigable navigable = (IEnterNavigable) sender;
            int i = num2 = this.m_Controls.IndexOf(navigable);
        Label_0017:
            if (i == (this.m_Controls.Count - 1))
            {
                if (!this.m_Cycle)
                {
                    if (this.GoToNext != null)
                    {
                        this.GoToNext(this, null);
                    }
                    return;
                }
                i = 0;
            }
            else
            {
                i++;
            }
            if (i != num2)
            {
                this.ShowTab(i);
                if (this[i].Skip)
                {
                    goto Label_0017;
                }
                this.FocusControl(i);
            }
        }

        private void OnL(object sender, EventArgs e)
        {
        }

        private void OnLeave(object sender, EventArgs e)
        {
            ((Control) sender).BackColor = this.m_Temp;
        }

        private void ShowTab(int i)
        {
            IEnterNavigable navigable = this[i];
            Control controlObject = navigable.ControlObject;
            foreach (TabPage page in this.m_TabPages)
            {
                if (this.IsChild(page, controlObject))
                {
                    int index = ((TabControl) page.Parent).TabPages.IndexOf(page);
                    if (index != ((TabControl) page.Parent).SelectedIndex)
                    {
                        page.Show();
                        ((TabControl) page.Parent).SelectedIndex = index;
                    }
                    break;
                }
            }
        }

        public void StartEdit()
        {
            if (this.m_Controls.Count > 0)
            {
                this.ShowTab(0);
                this.FocusControl(0);
            }
        }

        public Control ControlObject
        {
            get
            {
                return null;
            }
        }

        public bool Cycle
        {
            get
            {
                return this.m_Cycle;
            }
            set
            {
                this.m_Cycle = value;
            }
        }

        private IEnterNavigable this[int i]
        {
            get
            {
                return (IEnterNavigable) this.m_Controls[i];
            }
        }

        public bool Skip
        {
            get
            {
                return false;
            }
        }
    }
}

