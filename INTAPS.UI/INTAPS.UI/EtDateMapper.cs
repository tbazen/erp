
    using INTAPS.Ethiopic;
    using System;
    using System.Runtime.InteropServices;
namespace INTAPS.UI
{

    public class EtDateMapper : FieldControlMapper
    {
        public override object GetData(object control, out bool valid)
        {
            ETDateTimeDropDown down = (ETDateTimeDropDown) control;
            valid = true;
            return down.Value;
        }

        public override void InitializeControl(object control)
        {
            ETDateTimeDropDown down = (ETDateTimeDropDown) control;
            down.Value = DateTime.Now;
        }

        public override void SetData(object control, object value)
        {
            ETDateTimeDropDown down = (ETDateTimeDropDown) control;
            down.Value = (DateTime) value;
        }
    }
}

