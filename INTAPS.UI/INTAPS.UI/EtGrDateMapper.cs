
    using System;
    using System.Runtime.InteropServices;
namespace INTAPS.UI
{

    public class EtGrDateMapper : FieldControlMapper
    {
        internal override void EnableDisable(object control, bool enable)
        {
            ((EtGrPickerPair) control).EnableDisable(enable);
        }

        public override object GetData(object control, out bool valid)
        {
            EtGrPickerPair pair = (EtGrPickerPair) control;
            valid = true;
            return pair.GetValue();
        }

        public override void InitializeControl(object control)
        {
            EtGrPickerPair pair = (EtGrPickerPair) control;
        }

        public override void SetData(object control, object value)
        {
            ((EtGrPickerPair) control).SetValue((DateTime) value);
        }
    }
}

