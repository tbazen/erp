
    using INTAPS.Ethiopic;
    using System;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class EtGrPickerPair
    {
        private ETDateTimeDropDown _ethiopic;
        private DateTimePicker _gr;
        private bool _ignore = false;

        public EtGrPickerPair(ETDateTimeDropDown et, DateTimePicker gr)
        {
            this._ethiopic = et;
            this._gr = gr;
            this._gr.Format = DateTimePickerFormat.Time;
            this._ethiopic.Changed += new EventHandler(this._ethiopic_Changed);
            this._gr.ValueChanged += new EventHandler(this._gr_ValueChanged);
        }

        private void _ethiopic_Changed(object sender, EventArgs e)
        {
            if (!this._ignore)
            {
                this._ignore = true;
                try
                {
                    DateTime time = this._ethiopic.Value;
                    DateTime time2 = this._gr.Value;
                    this._gr.Value = new DateTime(time.Year, time.Month, time.Day, time2.Hour, time2.Minute, time2.Second);
                }
                catch
                {
                }
                finally
                {
                    this._ignore = false;
                }
            }
        }

        private void _gr_ValueChanged(object sender, EventArgs e)
        {
            if (!this._ignore)
            {
                this._ignore = true;
                try
                {
                    this._ethiopic.Value = this._gr.Value;
                }
                catch
                {
                }
                finally
                {
                    this._ignore = false;
                }
            }
        }

        internal void EnableDisable(bool enable)
        {
            this._gr.Enabled = enable;
            this._ethiopic.Enabled = enable;
        }

        public DateTime GetValue()
        {
            return this._gr.Value;
        }

        public void SetValue(DateTime dateTime)
        {
            if (!this._ignore)
            {
                this._ignore = true;
                try
                {
                    this._gr.Value = dateTime;
                    this._ethiopic.Value = dateTime;
                }
                finally
                {
                    this._ignore = false;
                }
            }
        }
    }
}

