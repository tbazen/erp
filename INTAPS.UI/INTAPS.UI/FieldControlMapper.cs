
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public abstract class FieldControlMapper
    {
        protected FieldControlMapper()
        {
        }

        internal virtual void EnableDisable(object control, bool enable)
        {
            if (control is Control)
            {
                ((Control) control).Enabled = enable;
            }
        }

        public abstract object GetData(object control, out bool valid);
        public abstract void InitializeControl(object control);
        public abstract void SetData(object control, object value);
    }
}

