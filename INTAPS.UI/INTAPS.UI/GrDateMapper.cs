
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class GrDateMapper : FieldControlMapper
    {
        public override object GetData(object control, out bool valid)
        {
            DateTimePicker picker = (DateTimePicker) control;
            valid = true;
            return picker.Value;
        }

        public override void InitializeControl(object control)
        {
            DateTimePicker picker = (DateTimePicker) control;
            picker.Value = DateTime.Now;
        }

        public override void SetData(object control, object value)
        {
            DateTimePicker picker = (DateTimePicker) control;
            picker.Value = (DateTime) value;
        }
    }
}

