
    using System;
    using System.Drawing;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class Helper
    {
        private static Form PopUpForm = null;

        static Helper()
        {
            PopUpForm = new Form();
            PopUpForm.StartPosition = FormStartPosition.CenterParent;
        }

        public static Point GetBestPopupLocation(Size PopopSize)
        {
            int x;
            int y;
            Point position = Cursor.Position;
            Size size = Screen.PrimaryScreen.Bounds.Size;
            if ((position.X + PopopSize.Width) < size.Width)
            {
                x = position.X;
            }
            else
            {
                x = position.X - PopopSize.Width;
            }
            if ((position.Y + PopopSize.Height) < size.Height)
            {
                y = position.Y;
            }
            else
            {
                y = position.Y - PopopSize.Height;
            }
            return new Point(x, y);
        }

        public static Point GetBestPopupLocation(Rectangle parent, Size PopopSize)
        {
            int y;
            Point position = Cursor.Position;
            Size size = Screen.PrimaryScreen.Bounds.Size;
            int x = parent.X;
            if (((parent.Y + parent.Height) + PopopSize.Height) < size.Height)
            {
                y = position.Y;
            }
            else
            {
                y = position.Y - PopopSize.Height;
            }
            return new Point(x, y);
        }

        public static DialogResult ShowControlInPopup(Control c, bool Dialog)
        {
            if (c is Form)
            {
                if (Dialog)
                {
                    return ((Form) c).ShowDialog();
                }
                ((Form) c).Show();
                return DialogResult.None;
            }
            PopUpForm.Controls.Clear();
            PopUpForm.Controls.Add(c);
            PopUpForm.Size = c.Size;
            PopUpForm.Text = c.Text;
            c.Dock = DockStyle.Fill;
            if (Dialog)
            {
                return PopUpForm.ShowDialog();
            }
            PopUpForm.Show();
            return DialogResult.None;
        }

        public static bool ValidateDoubleTextBox(TextBox txtFrom, string message, out double dVal)
        {
            return ValidateDoubleTextBox(txtFrom, message, false, false, out dVal);
        }

        public static bool ValidateDoubleTextBox(TextBox txtFrom, string message, bool allNegative, bool allowZero, out double dVal)
        {
            double num;
            if (double.TryParse(txtFrom.Text, out num) && (((num > 0.0) || ((num == 0.0) && allowZero)) || ((num < 0.0) && allNegative)))
            {
                dVal = num;
                return true;
            }
            dVal = 0.0;
            UIFormApplicationBase.CurrentAppliation.ShowUserMessage(message);
            return false;
        }

        public static bool ValidateIntegerTextBox(TextBox txtFrom, string message, out int intVar)
        {
            return ValidateIntegerTextBox(txtFrom, message, false, false, out intVar);
        }

        public static bool ValidateIntegerTextBox(TextBox txtFrom, string message, bool allNegative, bool allowZero, out int intVar)
        {
            int num;
            if (int.TryParse(txtFrom.Text, out num) && (((num > 0) || ((num == 0) && allowZero)) || ((num < 0) && allNegative)))
            {
                intVar = num;
                return true;
            }
            intVar = 0;
            UIFormApplicationBase.CurrentAppliation.ShowUserMessage(message);
            return false;
        }

        public static bool ValidateNonEmptyTextBox(TextBox txtName, string message, out string txtVar)
        {
            if (txtName.Text.Trim() == "")
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage(message);
                txtVar = "";
                return false;
            }
            txtVar = txtName.Text.Trim();
            return true;
        }
    }
}

