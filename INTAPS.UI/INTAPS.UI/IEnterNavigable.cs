
    using System;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public interface IEnterNavigable
    {
        event EventHandler GoToNext;

        void StartEdit();

        Control ControlObject { get; }

        bool Skip { get; }
    }
}

