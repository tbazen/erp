
    using System;
namespace INTAPS.UI
{

    public interface IUIApplication
    {
        string GetSetting(string key, string def);
        void HandleException(string p, Exception ex);
        void SaveSettings();
        void SetSetting(string key, string value);
        void ShowUserError(string Message);
        void ShowUserInformation(string Message);
        void ShowUserMessage(string Message);
        void ShowUserWarning(string Message);
        bool UserConfirms(string p);
    }
}

