
    using System;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public partial class NoBeepTextBox : TextBox
    {
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            base.SelectAll();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Return)
            {
                this.OnKeyDown(new KeyEventArgs(Keys.Return));
                return true;
            }
            if (keyData == Keys.Escape)
            {
                this.OnKeyDown(new KeyEventArgs(Keys.Escape));
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
    }
}

