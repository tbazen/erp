
    using System;
namespace INTAPS.UI
{

    [Serializable]
    public class NullableDateTime
    {
        public DateTime DT;

        public NullableDateTime()
        {
            this.DT = DateTime.Now;
        }

        public NullableDateTime(DateTime dt)
        {
            this.DT = dt;
        }

        public static NullableDateTime FromDb(object dbval)
        {
            if (dbval is DateTime)
            {
                return new NullableDateTime((DateTime) dbval);
            }
            return null;
        }

        public override string ToString()
        {
            return this.DT.ToString();
        }

        public static NullableDateTime Now
        {
            get
            {
                return new NullableDateTime(DateTime.Now);
            }
        }
    }
}

