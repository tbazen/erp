﻿namespace INTAPS.UI
{
    public partial class NullableDateTimePicker : System.Windows.Forms.UserControl, IEnterNavigable
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.btnToggleNullable = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtDate);
            this.panel1.Controls.Add(this.btnToggleNullable);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(178, 24);
            this.panel1.TabIndex = 0;
            // 
            // txtDate
            // 
            this.txtDate.BackColor = System.Drawing.Color.White;
            this.txtDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDate.Location = new System.Drawing.Point(0, 0);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(160, 20);
            this.txtDate.TabIndex = 5;
            this.txtDate.Click += new System.EventHandler(this.txtDate_Click);
            this.txtDate.TextChanged += new System.EventHandler(this.txtDate_Changed);
            this.txtDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDate_KeyDown);
            this.txtDate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDate_KeyPress);
            // 
            // btnToggleNullable
            // 
            this.btnToggleNullable.BackColor = System.Drawing.SystemColors.Control;
            this.btnToggleNullable.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnToggleNullable.Location = new System.Drawing.Point(160, 0);
            this.btnToggleNullable.Name = "btnToggleNullable";
            this.btnToggleNullable.Size = new System.Drawing.Size(18, 24);
            this.btnToggleNullable.TabIndex = 4;
            this.btnToggleNullable.Text = "?";
            this.btnToggleNullable.UseVisualStyleBackColor = false;
            this.btnToggleNullable.Click += new System.EventHandler(this.btnToggleNullable_Click);
            // 
            // NullableDateTimePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "NullableDateTimePicker";
            this.Size = new System.Drawing.Size(178, 24);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Button btnToggleNullable;
    }
}
