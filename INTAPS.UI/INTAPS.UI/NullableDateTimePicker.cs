
    using INTAPS.Ethiopic;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public partial class NullableDateTimePicker : UserControl, IEnterNavigable
    {
        private IContainer components = null;
        private int Digits = -1;
        private bool m_SkipThisEvent = false;
        private static Color NotNullColor = Color.Black;
        private static Color NullColor = Color.Gray;
        private const string UNKNOWN = "Unknown";

        public event EventHandler Changed;

        public event EventHandler GoToNext;

        public NullableDateTimePicker()
        {
            this.InitializeComponent();
            this.txtDate.ForeColor = NullColor;
            this.SetDatTime(DateTime.Now);
        }

        private void btnToggleNullable_Click(object sender, EventArgs e)
        {
            if (this.Value == null)
            {
                this.Value = NullableDateTime.Now;
            }
            else
            {
                this.Value = null;
            }
            this.OnChanged();
            if (this.GoToNext != null)
            {
                this.GoToNext(this, null);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private CursorPostion GetCurrentCurrPosition()
        {
            int selectionStart = this.txtDate.SelectionStart;
            if (selectionStart < 3)
            {
                return CursorPostion.Day;
            }
            if (selectionStart < 6)
            {
                return CursorPostion.Month;
            }
            return CursorPostion.Year;
        }


        private void OnChanged()
        {
            if (this.Changed != null)
            {
                this.Changed(this, null);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            base.Height = this.txtDate.Height;
        }

        private void SetDatTime(DateTime dt)
        {
            this.m_SkipThisEvent = true;
            this.txtDate.Text = EtGrDate.ToEth(dt).ToString();
            this.m_SkipThisEvent = false;
        }

        public void StartEdit()
        {
            this.txtDate.Focus();
        }

        private void txtDate_Changed(object sender, EventArgs e)
        {
            if (!this.m_SkipThisEvent)
            {
                this.txtDate.ForeColor = NotNullColor;
                this.OnChanged();
            }
        }

        private void txtDate_Click(object sender, EventArgs e)
        {
            this.Digits = -1;
        }

        private void txtDate_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    switch (this.GetCurrentCurrPosition())
                    {
                        case CursorPostion.Month:
                            this.txtDate.SelectionStart = 2;
                            break;

                        case CursorPostion.Year:
                            this.txtDate.SelectionStart = 4;
                            break;
                    }
                    this.Digits = -1;
                    return;

                case Keys.Up:
                    return;

                case Keys.Right:
                    switch (this.GetCurrentCurrPosition())
                    {
                        case CursorPostion.Day:
                            this.txtDate.SelectionStart = 2;
                            break;

                        case CursorPostion.Month:
                            this.txtDate.SelectionStart = 5;
                            break;
                    }
                    break;

                case Keys.Return:
                    if (this.GoToNext != null)
                    {
                        this.GoToNext(this, null);
                    }
                    this.Digits = -1;
                    return;

                default:
                    return;
            }
            this.Digits = -1;
        }

        private void txtDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            EtGrDate etDate = this.etDate;
            if ((e.KeyChar >= '0') && (e.KeyChar <= '9'))
            {
                int selectionStart = this.txtDate.SelectionStart;
                int num2 = e.KeyChar - '0';
                if (this.Digits == -1)
                {
                    if (num2 == 0)
                    {
                        return;
                    }
                    this.Digits = num2;
                }
                else
                {
                    this.Digits = (this.Digits * 10) + num2;
                }
                this.Digits = this.Digits % 0x2710;
                switch (this.GetCurrentCurrPosition())
                {
                    case CursorPostion.Day:
                        etDate.Day = this.Digits;
                        etDate.Day = etDate.Day % 100;
                        if (etDate.Month >= 13)
                        {
                            if (EtGrDate.IsLeapYearEt(etDate.Year))
                            {
                                etDate.Day = ((etDate.Day - 1) % 6) + 1;
                            }
                            else
                            {
                                etDate.Day = ((etDate.Day - 1) % 5) + 1;
                            }
                            break;
                        }
                        etDate.Day = ((etDate.Day - 1) % 30) + 1;
                        break;

                    case CursorPostion.Month:
                        etDate.Month = this.Digits;
                        etDate.Month = etDate.Month % 100;
                        etDate.Month = ((etDate.Month - 1) % 13) + 1;
                        break;

                    case CursorPostion.Year:
                        etDate.Year = this.Digits;
                        etDate.Year = etDate.Year % 0x2710;
                        break;
                }
                this.m_SkipThisEvent = true;
                this.txtDate.Text = etDate.ToString();
                this.m_SkipThisEvent = false;
                this.txtDate.SelectionStart = selectionStart;
                this.txtDate.ForeColor = NotNullColor;
                this.OnChanged();
            }
        }

        public Control ControlObject
        {
            get
            {
                return this.txtDate;
            }
        }

        private EtGrDate etDate
        {
            get
            {
                if (this.txtDate.Text == "Unknown")
                {
                    return EtGrDate.ToEth(DateTime.Now);
                }
                return EtGrDate.Parse(this.txtDate.Text);
            }
        }

        public bool Skip
        {
            get
            {
                return false;
            }
        }

        public NullableDateTime Value
        {
            get
            {
                if (this.txtDate.ForeColor == NullColor)
                {
                    return null;
                }
                return new NullableDateTime(EtGrDate.ToGrig(EtGrDate.Parse(this.txtDate.Text)).GridDate);
            }
            set
            {
                if (value == null)
                {
                    this.txtDate.ForeColor = NullColor;
                    this.m_SkipThisEvent = true;
                    this.txtDate.Text = "Unknown";
                    this.m_SkipThisEvent = false;
                }
                else
                {
                    this.SetDatTime(value.DT);
                    this.txtDate.ForeColor = NotNullColor;
                }
            }
        }
    }
}

