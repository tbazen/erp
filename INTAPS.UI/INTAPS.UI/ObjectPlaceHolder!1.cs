
    using System;
    using System.ComponentModel;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{
    public abstract class ObjectPlaceHolder<ObjectType> : ObjectPlaceHolderGenID<ObjectType,int>where ObjectType: class
    {
        public ObjectPlaceHolder()
            : base(-1)
        {
        }
    }
    public abstract class ObjectPlaceHolderGenID<ObjectType,IDType> : TextBox where ObjectType: class
    {
        private Button m_btnPick;
        private bool m_changed;
        private ObjectType m_picked;
        IDType _nullID;
        public event EventHandler ObjectChanged;

        public ObjectPlaceHolderGenID(IDType nullID)
        {
            _nullID = nullID;
            if (hasPicker)
            {
                this.m_btnPick = new Button();
                this.m_btnPick.Text = "...";
                this.m_btnPick.Dock = DockStyle.Right;
                this.m_btnPick.Width = 20;
                this.m_btnPick.Click += new EventHandler(this.m_btnPick_Click);
                this.m_changed = false;
                base.Controls.Add(this.m_btnPick);
            }
        }
        protected virtual bool hasPicker
        {
            get
            {
                return true;
            }
        }
        protected abstract ObjectType GetObjectBYID(IDType id);
        public IDType GetObjectID()
        {
            if (this.m_picked == null)
            {
                return _nullID;
            }
            return this.GetObjectID(this.m_picked);
        }

        protected abstract IDType GetObjectID(ObjectType obj);
        protected abstract string GetObjectString(ObjectType obj);
        private void m_btnPick_Click(object sender, EventArgs e)
        {
            ObjectType local = this.PickObject();
            if (local != null)
            {
                this.anobject = local;
                this.OnObjectChanged();
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            base.SelectAll();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                string query = this.Text.Trim();
                if (query == "")
                {
                    this.anobject = default(ObjectType);
                    this.OnObjectChanged();
                }
                else
                {
                    try
                    {
                        ObjectType[] localArray = this.SearchObject(query);
                        if (localArray.Length == 0)
                        {
                            throw new Exception("Not found.");
                        }
                        if (localArray.Length > 1)
                        {
                            throw new Exception("More than one anobject found please be specific.");
                        }
                        this.anobject = localArray[0];
                        this.OnObjectChanged();
                    }
                    catch (Exception exception)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage(exception.Message);
                        this.anobject = this.m_picked;
                        base.SelectAll();
                    }
                }
            }
        }

        protected void OnObjectChanged()
        {
            if (this.ObjectChanged != null)
            {
                this.ObjectChanged(this, null);
            }
            this.m_changed = true;
        }

        protected abstract ObjectType PickObject();
        protected abstract ObjectType[] SearchObject(string query);
        public void SetByID(IDType ID)
        {
            if (ID.Equals(_nullID))
            {
                this.anobject = default(ObjectType);
            }
            else
            {
                try
                {
                    this.anobject = this.GetObjectBYID(ID);
                }
                catch
                {
                    this.m_picked = default(ObjectType);
                    this.Text = "error";
                }
            }
        }

        [Browsable(false)]
        public ObjectType anobject
        {
            get
            {
                return this.m_picked;
            }
            set
            {
                this.m_picked = value;
                if (this.m_picked == null)
                {
                    this.Text = "";
                }
                else
                {
                    this.Text = this.GetObjectString(this.m_picked);
                }
            }
        }

        public bool Changed
        {
            get
            {
                return this.m_changed;
            }
        }
    }


    public abstract class ObjectPlaceHolderTypedID<ObjectType,IDType> : TextBox where ObjectType : class
    {
        private Button m_btnPick;
        private bool m_changed;
        private ObjectType m_picked;

        public event EventHandler ObjectChanged;
        IDType nullValue;
        Button addButton = null;
        public ObjectPlaceHolderTypedID(IDType nullValue)
        {
            if (this.pickable)
            {
                this.m_btnPick = new Button();
                this.m_btnPick.Text = "...";
                this.m_btnPick.Dock = DockStyle.Right;
                this.m_btnPick.Width = 20;
                this.m_btnPick.Click += new EventHandler(this.m_btnPick_Click);
                this.m_changed = false;
                base.Controls.Add(this.m_btnPick);
            }
            this.nullValue = nullValue;
            if(this.showAddButton())
            {
                addButton = new Button();
                addButton.Dock = DockStyle.Right;
                addButton.Text = "+";
                addButton.Width = 40;
                addButton.Click += b_Click;
                this.Controls.Add(addButton);
            }
        }
        bool _showAdd = true;
        public bool showAdd
        {
            get
            {
                if (addButton == null)
                    return false;
                return  _showAdd;
            }
            set
            {
                if (addButton != null)
                {
                    addButton.Visible = value;
                    _showAdd= value;
                }
            }
        }
        void b_Click(object sender, EventArgs e)
        {
            ObjectType o;
            if(getNewItem(out o))
            {
                this.anobject = o;
            }
        }

        protected abstract ObjectType GetObjectBYID(IDType id);
        protected virtual bool showAddButton()
        {
            return false;
        }
        protected virtual bool getNewItem(out ObjectType val)
        {
            val = null;
            return false;
        }
        public IDType GetObjectID()
        {
            if (this.m_picked == null)
            {
                return nullValue;
            }
            return this.GetObjectID(this.m_picked);
        }

        protected abstract IDType GetObjectID(ObjectType obj);
        protected abstract string GetObjectString(ObjectType obj);
        private void m_btnPick_Click(object sender, EventArgs e)
        {
            ObjectType local = this.PickObject();
            if (local != null)
            {
                this.anobject = local;
                this.OnObjectChanged();
            }
        }

        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);
            base.SelectAll();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                string query = this.Text.Trim();
                if (query == "")
                {
                    this.anobject = default(ObjectType);
                    this.OnObjectChanged();
                }
                else
                {
                    try
                    {
                        ObjectType[] localArray = this.SearchObject(query);
                        if (localArray.Length == 0)
                        {
                            throw new Exception("Not found.");
                        }
                        if (localArray.Length > 1)
                        {
                            throw new Exception("More than one anobject found please be specific.");
                        }
                        this.anobject = localArray[0];
                        this.OnObjectChanged();
                    }
                    catch (Exception exception)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage(exception.Message);
                        this.anobject = this.m_picked;
                        base.SelectAll();
                    }
                }
            }
        }

        protected void OnObjectChanged()
        {
            if (this.ObjectChanged != null)
            {
                this.ObjectChanged(this, null);
            }
            this.m_changed = true;
        }

        protected abstract ObjectType PickObject();
        protected abstract ObjectType[] SearchObject(string query);
        public void SetByID(IDType ID)
        {
            if ((ID==null && nullValue==null) ||(ID!=null && ID.Equals(nullValue)))
            {
                this.anobject = default(ObjectType);
            }
            else
            {
                try
                {
                    this.anobject = this.GetObjectBYID(ID);
                }
                catch
                {
                    this.m_picked = default(ObjectType);
                    this.Text = "error";
                }
            }
        }

        [Browsable(false)]
        public ObjectType anobject
        {
            get
            {
                return this.m_picked;
            }
            set
            {
                this.m_picked = value;
                if (this.m_picked == null)
                {
                    this.Text = "";
                }
                else
                {
                    this.Text = this.GetObjectString(this.m_picked);
                }
            }
        }

        public bool Changed
        {
            get
            {
                return this.m_changed;
            }
        }

        public abstract bool pickable { get;}
    }
}

