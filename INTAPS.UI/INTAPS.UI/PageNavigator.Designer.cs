﻿namespace INTAPS.UI
{
    public partial class PageNavigator : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageNavigator));
            this.lblYear = new System.Windows.Forms.Label();
            this.btnPrevPage = new System.Windows.Forms.Button();
            this.btnNexPage = new System.Windows.Forms.Button();
            this.btnFirst = new System.Windows.Forms.Button();
            this.btnLast = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblYear
            // 
            this.lblYear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblYear.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.lblYear.Location = new System.Drawing.Point(95, 13);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(270, 23);
            this.lblYear.TabIndex = 26;
            this.lblYear.Text = "##";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPrevPage
            // 
            this.btnPrevPage.BackColor = System.Drawing.Color.Transparent;
            this.btnPrevPage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPrevPage.BackgroundImage")));
            this.btnPrevPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPrevPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrevPage.ForeColor = System.Drawing.Color.Transparent;
            this.btnPrevPage.Location = new System.Drawing.Point(49, 4);
            this.btnPrevPage.Name = "btnPrevPage";
            this.btnPrevPage.Size = new System.Drawing.Size(40, 40);
            this.btnPrevPage.TabIndex = 25;
            this.btnPrevPage.UseVisualStyleBackColor = false;
            this.btnPrevPage.Click += new System.EventHandler(this.btnPrevPage_Click);
            // 
            // btnNexPage
            // 
            this.btnNexPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNexPage.BackColor = System.Drawing.Color.Transparent;
            this.btnNexPage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNexPage.BackgroundImage")));
            this.btnNexPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnNexPage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNexPage.ForeColor = System.Drawing.Color.Transparent;
            this.btnNexPage.Location = new System.Drawing.Point(371, 4);
            this.btnNexPage.Name = "btnNexPage";
            this.btnNexPage.Size = new System.Drawing.Size(40, 40);
            this.btnNexPage.TabIndex = 24;
            this.btnNexPage.UseVisualStyleBackColor = false;
            this.btnNexPage.Click += new System.EventHandler(this.btnNexPage_Click);
            // 
            // btnFirst
            // 
            this.btnFirst.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFirst.BackgroundImage")));
            this.btnFirst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnFirst.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFirst.ForeColor = System.Drawing.Color.Transparent;
            this.btnFirst.Location = new System.Drawing.Point(3, 4);
            this.btnFirst.Name = "btnFirst";
            this.btnFirst.Size = new System.Drawing.Size(40, 40);
            this.btnFirst.TabIndex = 25;
            this.btnFirst.UseVisualStyleBackColor = true;
            this.btnFirst.Click += new System.EventHandler(this.btnFirst_Click);
            // 
            // btnLast
            // 
            this.btnLast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLast.BackColor = System.Drawing.Color.Transparent;
            this.btnLast.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLast.BackgroundImage")));
            this.btnLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnLast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLast.ForeColor = System.Drawing.Color.Transparent;
            this.btnLast.Location = new System.Drawing.Point(417, 4);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(40, 40);
            this.btnLast.TabIndex = 24;
            this.btnLast.UseVisualStyleBackColor = false;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // PageNavigator
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.btnFirst);
            this.Controls.Add(this.btnPrevPage);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.btnNexPage);
            this.Name = "PageNavigator";
            this.Size = new System.Drawing.Size(460, 47);
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Button btnPrevPage;
        private System.Windows.Forms.Button btnNexPage;
        private System.Windows.Forms.Button btnFirst;
        private System.Windows.Forms.Button btnLast;

    }
}