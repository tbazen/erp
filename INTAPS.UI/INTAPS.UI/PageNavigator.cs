
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public partial class PageNavigator : UserControl
    {
        private IContainer components = null;
        
        private int m_index;
        private int m_nRec;
        private int m_pageSize;

        public event EventHandler PageChanged;

        public PageNavigator()
        {
            this.InitializeComponent();
            this.m_nRec = 0;
            this.m_index = 0;
            this.m_pageSize = 0;
            this.UpdateControlSate();
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            this.m_index = 0;
            this.UpdateControlSate();
            this.OnPageChanged();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            if ((this.m_nRec % this.m_pageSize) == 0)
            {
                this.m_index = ((this.m_nRec / this.m_pageSize) - 1) * this.m_pageSize;
            }
            else
            {
                this.m_index = (this.m_nRec / this.m_pageSize) * this.m_pageSize;
            }
            this.UpdateControlSate();
            this.OnPageChanged();
        }

        private void btnNexPage_Click(object sender, EventArgs e)
        {
            this.m_index += this.m_pageSize;
            this.UpdateControlSate();
            this.OnPageChanged();
        }

        private void btnPrevPage_Click(object sender, EventArgs e)
        {
            this.m_index -= this.m_pageSize;
            this.UpdateControlSate();
            this.OnPageChanged();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public virtual void OnPageChanged()
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, null);
            }
        }

        private void UpdateControlSate()
        {
            this.btnFirst.Enabled = this.btnPrevPage.Enabled = this.m_index > 0;
            this.btnNexPage.Enabled = this.btnLast.Enabled = (this.m_index + this.m_pageSize) < this.m_nRec;
            if (this.m_nRec > 0)
            {
                this.lblYear.Text = string.Concat(new object[] { this.m_index + 1, " to ", (this.m_nRec < (this.m_index + this.m_pageSize)) ? this.m_nRec : (this.m_index + this.m_pageSize), " of ", this.m_nRec });
            }
            else
            {
                this.lblYear.Text = "no record";
            }
        }

        public int NRec
        {
            get
            {
                return this.m_nRec;
            }
            set
            {
                this.m_nRec = value;
                this.UpdateControlSate();
            }
        }

        public int PageSize
        {
            get
            {
                return this.m_pageSize;
            }
            set
            {
                this.m_pageSize = value;
                this.UpdateControlSate();
            }
        }

        public int RecordIndex
        {
            get
            {
                return this.m_index;
            }
            set
            {
                this.m_index = value;
                this.UpdateControlSate();
            }
        }
    }
}

