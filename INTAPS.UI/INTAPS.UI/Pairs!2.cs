
    using System;
namespace INTAPS.UI
{

    public class Pairs<First, Second>
    {
        public First first;
        public Second second;

        public Pairs(First first, Second second)
        {
            this.first = first;
            this.second = second;
        }
    }
}

