
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public abstract partial class SimpleObjectList<ObjectType> : Form where ObjectType: class, new()
    {
        private IContainer components;
        
        public SimpleObjectList()
        {
            this.components = null;
            this.InitializeComponent();
            if(loadInConstructor)
                this.ReloadList();
            this.Text = this.ObjectTypeNamePlural + ".";
        }
        protected virtual bool loadInConstructor
        {
            get
            {
                return true;
            }
        }
        private void AddObject(ObjectType obj)
        {
            Button button2;
            Panel panel = new Panel();
            TextBox box = new TextBox();
            box.ReadOnly = true;
            box.Dock = DockStyle.Fill;
            Button button = new Button();
            button.Height = box.Height;
            panel.Height = box.Height;
            button.Width = button.Height;
            button.BackgroundImage = INTAPS.UI.Properties.Resources.delete2;
            button.BackgroundImageLayout = ImageLayout.Zoom;
            //button.Text = "X";
           // button.BackColor = Color.Red;
            button.Dock = DockStyle.Left;
            panel.Controls.Add(box);
            panel.Controls.Add(button);
            if (this.AllowEdit)
            {
                button2 = new Button();
                button2.Height = box.Height;
                button2.Height = box.Height;
                button2.Width = button.Height;
                button2.Text = "...";
                button2.BackColor = Color.Green;
                button2.Dock = DockStyle.Right;
                panel.Controls.Add(button2);
                button2.Click += new EventHandler(this.EditButtonClick);
            }
            else
            {
                button2 = null;
            }
            panel.Dock = DockStyle.Top;
            this.panel.Controls.Add(panel);
            panel.BringToFront();
            button.Click += new EventHandler(this.DeleteButtonClick);
            panel.Tag = obj;
            button.Tag = obj;
            if (button2 != null)
            {
                button2.Tag = obj;
            }
            try
            {
                box.Text = this.GetObjectString(obj);
            }
            catch
            {
                box.Text = "error";
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ObjectType local = this.CreateObject();
                if (local != null)
                {
                    this.AddObject(local);
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't create meter reader employee.", exception);
            }
        }

        protected abstract ObjectType CreateObject();
        private void DeleteButtonClick(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            ObjectType tag = button.Tag as ObjectType;
            if ((tag != null) && UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure  you want to delete " + this.GetObjectString(tag) + "?"))
            {
                try
                {
                    this.DeleteObject(tag);
                    button.Click -= new EventHandler(this.DeleteButtonClick);
                    if (this.AllowEdit)
                    {
                        ((Button) button.Parent.Controls[2]).Click -= new EventHandler(this.EditButtonClick);
                    }
                    this.panel.Controls.Remove(button.Parent);
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Failed to delete the " + this.ObjectTypeName + ".", exception);
                }
            }
        }

        protected abstract void DeleteObject(ObjectType obj);
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void EditButtonClick(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            ObjectType tag = button.Tag as ObjectType;
            if (tag != null)
            {
                tag = this.EditObject(tag);
                if (tag != null)
                {
                    Panel parent = (Panel) button.Parent;
                    try
                    {
                        parent.Controls[0].Text = this.GetObjectString(tag);
                    }
                    catch
                    {
                        parent.Controls[0].Text = "error";
                    }
                    parent.Controls[1].Tag = tag;
                    parent.Controls[2].Tag = tag;
                }
            }
        }

        protected virtual ObjectType EditObject(ObjectType obj)
        {
            return obj;
        }

        protected abstract ObjectType[] GetObjects();
        protected abstract string GetObjectString(ObjectType obj);

        public void ReloadList()
        {
            this.panel.Controls.Clear();
            foreach (ObjectType local in this.GetObjects())
            {
                this.AddObject(local);
            }
        }

        protected virtual bool AllowEdit
        {
            get
            {
                return false;
            }
        }

        protected abstract string ObjectTypeName { get; }

        protected string ObjectTypeNamePlural
        {
            get
            {
                return (this.ObjectTypeName + " list");
            }
        }
    }


    
}

