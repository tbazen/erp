
    using System;
using System.Collections.Generic;
using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public abstract partial class SimpleObjectListPicker<ObjectType> : Form where ObjectType: class, new()
    {
        private IContainer components;
        
        public SimpleObjectListPicker()
        {
            this.components = null;
            this.InitializeComponent();
            if(loadInConstructor)
                this.ReloadList();
            this.Text = this.ObjectTypeNamePlural + ".";
        }
        protected virtual bool loadInConstructor
        {
            get
            {
                return true;
            }
        }
        const int BUTTON_HEIGHT = 50;
        int y = 0;
        private void AddObject(ObjectType obj)
        {
            
            Button button = new Button();
            button.Width= panel.Width;
            button.Height = BUTTON_HEIGHT;
            button.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            button.Width = this.ClientRectangle.Width;
            button.Dock = DockStyle.Top;
            button.Click += Button_Click;
            this.panel.Controls.Add(button);
            try
            {
                button.Tag = obj;
                button.Text = this.GetObjectString(obj);
            }
            catch
            {
                button.Tag = null;
                button.Text = "error";
            }
        }
        public ObjectType selectedObject = null;
        private void Button_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            selectedObject = ((Button)sender).Tag as ObjectType;
            this.Close();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }



        protected abstract IList<ObjectType> GetObjects();
        protected abstract string GetObjectString(ObjectType obj);

        public void ReloadList()
        {
            this.panel.Controls.Clear();
            foreach (ObjectType local in this.GetObjects())
            {
                this.AddObject(local);
            }
        }



        protected abstract string ObjectTypeName { get; }

        protected string ObjectTypeNamePlural
        {
            get
            {
                return (this.ObjectTypeName + " list");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}

