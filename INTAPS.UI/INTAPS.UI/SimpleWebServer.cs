using System;
using System.Net;
using System.Text;
namespace INTAPS.UI
{

    public class SimpleWebServer
    {
        private static string HTML;
        private static HttpListener http;
        private static string url;

        private static void ProcessWebRequest(IAsyncResult res)
        {
            HttpListenerContext context = http.EndGetContext(res);
            http.BeginGetContext(new AsyncCallback(SimpleWebServer.ProcessWebRequest), http);
            byte[] bytes = Encoding.ASCII.GetBytes(HTML);
            context.Response.OutputStream.Write(bytes, 0, bytes.Length);
            context.Response.OutputStream.Close();
        }

        public static void PutOnWebServer(int Port, string html)
        {
            HTML = html;
            if (http == null)
            {
                http = new HttpListener();
                url = "http://localhost:" + Port + "/";
                http.Prefixes.Add(url);
                http.Start();
                http.BeginGetContext(new AsyncCallback(SimpleWebServer.ProcessWebRequest), http);
            }
        }

        public static string URL
        {
            get
            {
                return url;
            }
        }
    }
}

