
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.InteropServices;
namespace INTAPS.UI
{

    public class SingleTableObjectFormHandler<ObjectType>
    {
        private Dictionary<string, Map<ObjectType>> _mappers;
        public ObjectType controlData;

        public SingleTableObjectFormHandler()
        {
            this._mappers = new Dictionary<string, Map<ObjectType>>();
        }

        public void Add(string key, object c, FieldControlMapper mapper)
        {
            this._mappers.Add(key, new Map<ObjectType>(key, c, mapper, true));
        }

        public void Add(string key, object c, FieldControlMapper mapper, bool allowToControl)
        {
            this._mappers.Add(key, new Map<ObjectType>(key, c, mapper, allowToControl));
        }

        public void EnableDisableAll(bool enable)
        {
            foreach (KeyValuePair<string, Map<ObjectType>> pair in this._mappers)
            {
                pair.Value.mapper.EnableDisable(pair.Value.editor, enable);
            }
        }

        public void InitializeControls()
        {
            foreach (KeyValuePair<string, Map<ObjectType>> pair in this._mappers)
            {
                pair.Value.mapper.InitializeControl(pair.Value.editor);
            }
        }

        public void ToControls()
        {
            foreach (KeyValuePair<string, Map<ObjectType>> pair in this._mappers)
            {
                foreach (FieldInfo info in typeof(ObjectType).GetFields())
                {
                    if (info.Name.ToUpper() == pair.Key.ToUpper())
                    {
                        pair.Value.mapper.SetData(pair.Value.editor, info.GetValue(this.controlData));
                        break;
                    }
                }
            }
        }

        public bool ToData()
        {
            foreach (KeyValuePair<string, Map<ObjectType>> pair in this._mappers)
            {
                if (pair.Value.allowToData)
                {
                    foreach (FieldInfo info in typeof(ObjectType).GetFields())
                    {
                        if (info.Name.ToUpper() == pair.Key.ToUpper())
                        {
                            bool flag;
                            object data = pair.Value.mapper.GetData(pair.Value.editor, out flag);
                            if (!flag)
                            {
                                return false;
                            }
                            info.SetValue(this.controlData, data);
                            break;
                        }
                    }
                }
            }
            return true;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct Map<ObjectType>
        {
            public string key;
            public object editor;
            public FieldControlMapper mapper;
            public bool allowToData;
            public Map(string k, object e, FieldControlMapper m, bool a)
            {
                this.key = k;
                this.editor = e;
                this.mapper = m;
                this.allowToData = a;
            }
        }
    }
}

