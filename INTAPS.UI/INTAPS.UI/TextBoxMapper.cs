
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class TextBoxMapper : FieldControlMapper
    {
        private bool _allowEmpty;
        private System.Type _dataType;
        private object _empty;
        private string _error;

        public TextBoxMapper(System.Type t, bool allowEmpty, object _emptyValue, string errMsg)
        {
            this._empty = _emptyValue;
            this._dataType = t;
            this._error = errMsg;
            this._allowEmpty = allowEmpty;
        }

        public override object GetData(object control, out bool valid)
        {
            TextBox box = (TextBox) control;
            if (box.Text.Trim() == "")
            {
                if (this._allowEmpty)
                {
                    valid = true;
                    return this._empty;
                }
                if (string.IsNullOrEmpty(this._error))
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter valid text");
                }
                else
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError(this._error);
                }
                valid = false;
                return null;
            }
            try
            {
                valid = true;
                return Convert.ChangeType(box.Text, this._dataType);
            }
            catch
            {
                if (string.IsNullOrEmpty(this._error))
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter valid text");
                }
                else
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError(this._error);
                }
                box.SelectAll();
                box.Focus();
                valid = false;
                return null;
            }
        }

        public override void InitializeControl(object control)
        {
            TextBox box = (TextBox) control;
        }

        public override void SetData(object control, object value)
        {
            TextBox box = (TextBox) control;
            if (value == null)
            {
                box.Text = "";
            }
            else
            {
                box.Text = value.ToString();
            }
        }
    }
}

