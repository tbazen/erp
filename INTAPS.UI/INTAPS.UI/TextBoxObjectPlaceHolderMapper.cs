
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class TextBoxObjectPlaceHolderMapper : FieldControlMapper
    {
        private bool _allowEmpty;
        private object _empty;
        private string _error;
        private GetStringDelegate _getString;

        public TextBoxObjectPlaceHolderMapper(GetStringDelegate getString, bool allowEmpty, object _emptyValue, string errMsg)
        {
            this._empty = _emptyValue;
            this._error = errMsg;
            this._allowEmpty = allowEmpty;
            this._getString = getString;
        }

        public override object GetData(object control, out bool valid)
        {
            TextBox box = (TextBox) control;
            valid = true;
            return box.Tag;
        }

        public override void InitializeControl(object control)
        {
            TextBox box = (TextBox) control;
            box.ReadOnly = true;
        }

        public override void SetData(object control, object value)
        {
            TextBox box = (TextBox) control;
            box.Tag = value;
            box.Text = (string) this._getString.Method.Invoke(value, new object[0]);
        }
    }
}

