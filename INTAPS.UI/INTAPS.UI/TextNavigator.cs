
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class TextNavigator : IEnterNavigable
    {
        private TextBox m_tb;

        public event EventHandler GoToNext;

        public TextNavigator(TextBox tb)
        {
            this.m_tb = tb;
            this.m_tb.KeyPress += new KeyPressEventHandler(this.OnKeyPress);
            this.m_tb.KeyDown += new KeyEventHandler(this.OnKeyDown);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.GoToNext(this, null);
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
            }
        }

        public void StartEdit()
        {
            this.m_tb.Focus();
            this.m_tb.SelectAll();
        }

        public Control ControlObject
        {
            get
            {
                return this.m_tb;
            }
        }

        public bool Skip
        {
            get
            {
                return (!this.m_tb.Enabled || !this.m_tb.Visible);
            }
        }
    }
}

