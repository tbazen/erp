namespace INTAPS.UI
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    public class TitleBarPanel : ToolStripPanel
    {
        private Brush m_BackBrush = Brushes.Blue;
        private Brush m_TextBursh = Brushes.White;
        public string TitleText = "";

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(this.m_BackBrush, e.ClipRectangle);
            e.Graphics.DrawString(this.TitleText, this.Font, this.m_TextBursh, (float) 0f, (float) 0f);
        }

        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
                this.m_TextBursh = new SolidBrush(value);
            }
        }
    }
}

