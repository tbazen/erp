
    using System;
    using System.Collections;
    using System.Drawing;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class UICommand
    {
        private ArrayList m_Buttons = new ArrayList();
        private ArrayList m_CBB = new ArrayList();
        private bool m_Enabled = true;
        private Icon m_Icon;
        private ArrayList m_MenuItem = new ArrayList();
        private string m_Name;

        public event EventHandler CommandShow;

        public event EventHandler Invoked;

        public UICommand(string Name, Icon icon)
        {
            this.m_Name = Name;
            this.m_Icon = icon;
        }

        public void AddButton(Button b)
        {
            b.Click += new EventHandler(this.CommandInvoked);
            this.m_Buttons.Add(b);
            b.Disposed += new EventHandler(this.ButtonDisposed);
        }

        public void AddMenuItem(MenuItem mi)
        {
            mi.Click += new EventHandler(this.CommandInvoked);
            this.m_MenuItem.Add(mi);
            if (mi.Parent is MenuItem)
            {
                ((MenuItem) mi.Parent).Popup += new EventHandler(this.OnShow);
            }
            else if (mi.Parent is ContextMenu)
            {
                ((ContextMenu) mi.Parent).Popup += new EventHandler(this.OnShow);
            }
        }

        private void ButtonDisposed(object sender, EventArgs e)
        {
        }

        private void CommandInvoked(object sender, EventArgs e)
        {
            if (this.Invoked != null)
            {
                this.Invoked(sender, e);
            }
        }

        public void Invoke()
        {
            if (this.m_Enabled && (this.Invoked != null))
            {
                this.Invoked(this, null);
            }
        }

        private void OnShow(object source, EventArgs e)
        {
            if (this.CommandShow != null)
            {
                this.CommandShow(this, null);
            }
        }

        public bool Enabled
        {
            get
            {
                return this.m_Enabled;
            }
            set
            {
                foreach (Button button in this.m_Buttons)
                {
                    button.Enabled = value;
                }
                foreach (MenuItem item in this.m_MenuItem)
                {
                    item.Enabled = value;
                }
                this.m_Enabled = value;
            }
        }
    }
}

