using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
namespace INTAPS.UI
{

    public class UIFormApplicationBase : IUIApplication
    {
        public static IUIApplication CurrentAppliation;
        protected string m_ApplicationName = "INTAPS Application";
        private Dictionary<string, string> m_localSettings;
        public static Form MainForm;

        public UIFormApplicationBase(Form main)
        {
            MainForm = main;
            CurrentAppliation = this;
            this.LoadSettings();
        }

        public string GetSetting(string key, string def)
        {
            if (this.m_localSettings.ContainsKey(key))
            {
                return this.m_localSettings[key];
            }
            return def;
        }

        public void HandleException(string m, Exception ex)
        {
            string str = "";
            while (ex != null)
            {
                str = ex.Message + "\n" + str;
                ex = ex.InnerException;
            }
            MessageBox.Show(string.IsNullOrEmpty(m)?str:m + "\n" + str, this.m_ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        public void LoadSettings()
        {
            this.m_localSettings = new Dictionary<string, string>();
            try
            {
                string path = Application.StartupPath + @"\LocalSettings.xml";
                if (File.Exists(path))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SettingTable));
                    StreamReader textReader = File.OpenText(path);
                    SettingTable table = (SettingTable) serializer.Deserialize(textReader);
                    textReader.Close();
                    for (int i = 0; i < table.keys.Length; i++)
                    {
                        this.m_localSettings.Add(table.keys[i], table.vals[i]);
                    }
                }
            }
            catch
            {
            }
        }

        public void SaveSettings()
        {
            try
            {
                SettingTable o = new SettingTable();
                o.keys = new string[this.m_localSettings.Count];
                o.vals = new string[this.m_localSettings.Count];
                this.m_localSettings.Keys.CopyTo(o.keys, 0);
                this.m_localSettings.Values.CopyTo(o.vals, 0);
                StreamWriter writer = File.CreateText(Application.StartupPath + @"\LocalSettings.xml");
                new XmlSerializer(typeof(SettingTable)).Serialize((TextWriter) writer, o);
                writer.Close();
            }
            catch
            {
            }
        }

        public void SetSetting(string key, string value)
        {
            if (this.m_localSettings.ContainsKey(key))
            {
                this.m_localSettings[key] = value;
            }
            else
            {
                this.m_localSettings.Add(key, value);
            }
        }

        public void ShowUserError(string Message)
        {
            MessageBox.Show(Message, this.m_ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        public void ShowUserInformation(string Message)
        {
            MessageBox.Show(Message, this.m_ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public void ShowUserMessage(string Message)
        {
            MessageBox.Show(Message, this.m_ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        public void ShowUserWarning(string Message)
        {
            MessageBox.Show(Message, this.m_ApplicationName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public bool UserConfirms(string p)
        {
            return (MessageBox.Show(p, this.m_ApplicationName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
        }

        public class SettingTable
        {
            public string[] keys;
            public string[] vals;
        }
        public static void runInUIThread(Delegate d, params object[] parms)
        {
            if (INTAPS.UI.UIFormApplicationBase.MainForm != null
                && !INTAPS.UI.UIFormApplicationBase.MainForm.Disposing
                && !INTAPS.UI.UIFormApplicationBase.MainForm.IsDisposed
            && INTAPS.UI.UIFormApplicationBase.MainForm.IsHandleCreated)
                INTAPS.UI.UIFormApplicationBase.MainForm.Invoke(d, parms);
        }
    }
}

