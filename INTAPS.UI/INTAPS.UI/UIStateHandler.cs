
    using INTAPS.UI.UIState;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Forms;
    using System.Xml.Serialization;
namespace INTAPS.UI
{

    public class UIStateHandler
    {
        private static void ExpandTreeNodes(TreeNodeCollection nodes, List<string> en, TreeNodeToString conv)
        {
            foreach (TreeNode node in nodes)
            {
                if ((node.Tag != null) && en.Contains(conv(node)))
                {
                    node.Expand();
                }
                ExpandTreeNodes(node.Nodes, en, conv);
            }
        }

        public static string GetControlSizeState(Control c)
        {
            ControlSize size = new ControlSize();
            size.sz = c.Size;
            return GetXMLString(size);
        }

        public static string GetFormState(Form f)
        {
            FormState state = new FormState();
            state.formPosition = f.Location;
            state.windowState = f.WindowState;
            state.formSize = f.Size;
            return GetXMLString(state);
        }

        private static T GetObjetFromXML<T>(string xml)
        {
            StringReader textReader = new StringReader(xml);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            return (T) serializer.Deserialize(textReader);
        }

        public static string GetSpliterState(SplitContainer sc)
        {
            SpliterSate sate = new SpliterSate();
            sate.spliterPosition = sc.SplitterDistance;
            return GetXMLString(sate);
        }

        public static string GetTreeState(TreeView t, TreeNodeToString conv)
        {
            List<string> expanded = new List<string>();
            ParseTree(t.Nodes, expanded, conv);
            TreeState state = new TreeState();
            state.ExpandedID = expanded.ToArray();
            return GetXMLString(state);
        }

        private static string GetXMLString(object obj)
        {
            StringWriter writer = new StringWriter();
            new XmlSerializer(obj.GetType()).Serialize((TextWriter) writer, obj);
            return writer.ToString();
        }

        public static bool LoadControlSizeState(string state, Control c)
        {
            try
            {
                ControlSize objetFromXML = GetObjetFromXML<ControlSize>(state);
                c.Size = objetFromXML.sz;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool LoadFormState(string state, Form f)
        {
            try
            {
                FormState objetFromXML = GetObjetFromXML<FormState>(state);
                f.WindowState = objetFromXML.windowState;
                f.Location = objetFromXML.formPosition;
                f.Size = objetFromXML.formSize;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool LoadSpliterState(string state, SplitContainer sc)
        {
            try
            {
                SpliterSate objetFromXML = GetObjetFromXML<SpliterSate>(state);
                sc.SplitterDistance = objetFromXML.spliterPosition;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool LoadTreeState(string state, TreeView t, TreeNodeToString conv)
        {
            try
            {
                TreeState objetFromXML = GetObjetFromXML<TreeState>(state);
                List<string> en = new List<string>();
                en.AddRange(objetFromXML.ExpandedID);
                ExpandTreeNodes(t.Nodes, en, conv);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void ParseTree(TreeNodeCollection nodes, List<string> expanded, TreeNodeToString conv)
        {
            foreach (TreeNode node in nodes)
            {
                if (node.IsExpanded && (conv(node) != null))
                {
                    expanded.Add(conv(node));
                    ParseTree(node.Nodes, expanded, conv);
                }
            }
        }
    }
}

