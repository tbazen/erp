
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
namespace INTAPS.UI
{

    public class user32
    {
        [DllImport("User32.dll")]
        public static extern short GetAsyncKeyState(int State);
        public static bool IsKeyDown(Keys key)
        {
            return ((GetAsyncKeyState((int) key) & 0x8000) == 0x8000);
        }

        [DllImport("User32.dll")]
        public static extern uint SendMessageW(IntPtr hWnd, uint Msg, uint wParam, uint lParam);
    }
}

