namespace INTAPS.Archive.UI
{
    using System;

    public enum DragTool
    {
        Zoom,
        Box,
        Pan,
        Rotate
    }
}

