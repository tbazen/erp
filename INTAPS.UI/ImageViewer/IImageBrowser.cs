namespace INTAPS.Archive.UI
{
    using System;
    using System.Drawing;

    public interface IImageBrowser
    {
        void MoveNext();
        void MovePrev();

        Image CurrentImage { get; }

        int CurrentPageNumber { get; }

        string CurrentTitle { get; }

        bool IsCurrnetImageBack { get; }

        int NPages { get; }
    }
}

