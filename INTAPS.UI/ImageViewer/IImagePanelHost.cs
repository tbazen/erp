namespace INTAPS.Archive.UI
{
    using System;
    using System.Drawing;

    public interface IImagePanelHost
    {
        void DragToolChanged(DragTool tool);
        void PanChanged(Point pan);
        void ZoomLevelChanged(float Level);
    }
}

