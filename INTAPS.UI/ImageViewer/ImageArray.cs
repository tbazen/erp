namespace INTAPS.Archive.UI
{
    using System;
    using System.Drawing;

    public class ImageArray : IImageBrowser
    {
        private Image[] m_Images;
        private int Page;

        public ImageArray(Image[] images)
        {
            this.m_Images = images;
            this.Page = 0;
        }

        public ImageArray(string[] Files)
        {
            this.m_Images = new Image[Files.Length];
            for (int i = 0; i < Files.Length; i++)
            {
                this.m_Images[i] = Image.FromFile(Files[i]);
            }
            this.Page = 0;
        }

        public void MoveNext()
        {
            if (this.Page == (this.m_Images.Length - 1))
            {
                throw new Exception("No image after the last image.");
            }
            this.Page++;
        }

        public void MovePrev()
        {
            if (this.Page == 0)
            {
                throw new Exception("No image before the first image");
            }
            this.Page--;
        }

        public Image CurrentImage
        {
            get
            {
                return this.m_Images[this.Page];
            }
        }

        public int CurrentPageNumber
        {
            get
            {
                return (this.Page + 1);
            }
        }

        public string CurrentTitle
        {
            get
            {
                return ("Page " + this.CurrentPageNumber.ToString());
            }
        }

        public bool IsCurrnetImageBack
        {
            get
            {
                return false;
            }
        }

        public int NPages
        {
            get
            {
                return this.m_Images.Length;
            }
        }
    }
}

