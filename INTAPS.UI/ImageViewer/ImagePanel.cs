namespace INTAPS.Archive.UI
{
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;

    public class ImagePanel : Panel
    {
        private Image m_CurrentImage = null;
        private PointF m_CurrentOffset = new PointF(0f, 0f);
        private float m_CurrentZoomLevel = 1f;
        private DragState m_DragState = DragState.None;
        private IImagePanelHost m_Host = null;
        private int m_LastX = -1;
        private int m_LastY = -1;
        private int m_OriginX = -1;
        private int m_OriginY = -1;
        private DragTool m_PrevTool = DragTool.Pan;
        private int m_RotRefX = -1;
        private int m_RotRefY = -1;
        private DragTool m_Tool;
        private Rectangle m_ZoomBox;
        private static Pen RotateLinePen = new Pen(Color.FromArgb(100, Color.Blue), 4f);
        private const double TOO_MUCH = 10.0;
        private const double TOO_SMALL = 0.0099999997764825821;
        private const double ZOOM_MAX = 100.0;
        private const double ZOOM_MIN = 0.01;
        private const double ZOOM_STEP = 0.1;

        public ImagePanel()
        {
            this.tool = DragTool.Zoom;
            base.SetStyle(ControlStyles.Selectable, true);
            base.Focus();
        }

        public void BestFit()
        {
            if (this.m_CurrentImage != null)
            {
                float num = ((float) base.Width) / ((float) this.m_CurrentImage.Width);
                float num2 = ((float) base.Height) / ((float) this.m_CurrentImage.Height);
                this.m_CurrentZoomLevel = (num < num2) ? num : num2;
                if (this.m_Host != null)
                {
                    this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
                }
                this.Center();
            }
        }

        internal void Center()
        {
            if (this.m_CurrentImage != null)
            {
                bool flag = false;
                if ((this.m_CurrentImage.Width * this.m_CurrentZoomLevel) < base.Width)
                {
                    this.m_CurrentOffset.X = (base.Width / 2) - ((this.m_CurrentImage.Width * this.m_CurrentZoomLevel) / 2f);
                    flag = true;
                }
                else if (this.m_CurrentOffset.X > 0f)
                {
                    this.m_CurrentOffset.X = 0f;
                    flag = true;
                }
                else if ((this.m_CurrentOffset.X + (this.m_CurrentImage.Width * this.m_CurrentZoomLevel)) < (base.Width - 1))
                {
                    this.m_CurrentOffset.X = base.Width - (this.m_CurrentImage.Width * this.m_CurrentZoomLevel);
                    flag = true;
                }
                if ((this.m_CurrentImage.Height * this.m_CurrentZoomLevel) < base.Height)
                {
                    this.m_CurrentOffset.Y = (base.Height / 2) - ((this.m_CurrentImage.Height * this.m_CurrentZoomLevel) / 2f);
                    flag = true;
                }
                else if (this.m_CurrentOffset.Y > 0f)
                {
                    this.m_CurrentOffset.Y = 0f;
                    flag = true;
                }
                else if ((this.m_CurrentOffset.Y + (this.m_CurrentImage.Height * this.m_CurrentZoomLevel)) < (base.Height - 1))
                {
                    this.m_CurrentOffset.Y = base.Height - (this.m_CurrentImage.Height * this.m_CurrentZoomLevel);
                    flag = true;
                }
                if (this.m_Host != null)
                {
                    this.m_Host.PanChanged(new Point((int) this.m_CurrentOffset.X, (int) this.m_CurrentOffset.Y));
                }
            }
        }

        public void FitHeight()
        {
            if (this.m_CurrentImage != null)
            {
                this.m_CurrentZoomLevel = ((float) base.Height) / ((float) this.m_CurrentImage.Height);
                if (this.m_Host != null)
                {
                    this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
                }
                this.Center();
            }
        }

        public void FitWidth()
        {
            if (this.m_CurrentImage != null)
            {
                this.m_CurrentZoomLevel = ((float) base.Width) / ((float) this.m_CurrentImage.Width);
                if (this.m_Host != null)
                {
                    this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
                }
                this.Center();
            }
        }

        protected override void OnClick(EventArgs e)
        {
            base.Focus();
            base.OnClick(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            MouseButtons button = e.Button;
            switch (button)
            {
                case MouseButtons.Left:
                    base.Capture = true;
                    switch (this.m_Tool)
                    {
                        case DragTool.Zoom:
                            this.m_DragState = DragState.Zooming;
                            goto Label_018D;

                        case DragTool.Box:
                            this.m_DragState = DragState.Boxing;
                            goto Label_018D;

                        case DragTool.Pan:
                            this.m_DragState = DragState.Panning;
                            goto Label_018D;

                        case DragTool.Rotate:
                            if (this.m_DragState != DragState.None)
                            {
                                if (this.m_DragState == DragState.RotateOrgin)
                                {
                                    this.m_DragState = DragState.Rotate;
                                    this.m_OriginX = (this.m_OriginX + e.X) / 2;
                                    this.m_OriginY = (this.m_OriginY + e.Y) / 2;
                                    this.m_RotRefX = e.X;
                                    this.m_RotRefY = e.Y;
                                    base.Invalidate();
                                }
                                break;
                            }
                            this.m_OriginX = e.X;
                            this.m_OriginY = e.Y;
                            this.m_DragState = DragState.RotateOrgin;
                            goto Label_018D;
                    }
                    break;

                case MouseButtons.Right:
                    switch (this.m_Tool)
                    {
                        case DragTool.Zoom:
                            this.tool = DragTool.Pan;
                            break;

                        case DragTool.Box:
                        case DragTool.Rotate:
                            this.tool = DragTool.Zoom;
                            break;

                        case DragTool.Pan:
                            this.tool = DragTool.Box;
                            break;
                    }
                    goto Label_01A7;

                default:
                    if ((button == MouseButtons.Middle) && (this.m_DragState == DragState.None))
                    {
                        this.m_DragState = DragState.Panning;
                        this.m_LastX = e.X;
                        this.m_LastY = e.Y;
                        base.Capture = true;
                    }
                    goto Label_01A7;
            }
        Label_018D:
            this.m_LastX = e.X;
            this.m_LastY = e.Y;
        Label_01A7:
            base.OnMouseDown(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            float num4;
            int num = this.m_LastX - e.X;
            int num2 = this.m_LastY - e.Y;
            switch (this.m_DragState)
            {
                case DragState.Panning:
                    if (((num * num) + (num2 * num2)) != 0)
                    {
                        this.m_CurrentOffset.X -= num;
                        this.m_CurrentOffset.Y -= num2;
                        this.m_LastX = e.X;
                        this.m_LastY = e.Y;
                        if (this.m_Host != null)
                        {
                            this.m_Host.PanChanged(new Point((int) this.m_CurrentOffset.X, (int) this.m_CurrentOffset.Y));
                        }
                        base.Invalidate();
                    }
                    goto Label_021B;

                case DragState.Boxing:
                    this.m_DragState = DragState.Boxing;
                    this.m_ZoomBox = new Rectangle(this.m_LastX, this.m_LastY, e.X - this.m_LastX, e.Y - this.m_LastY);
                    base.Invalidate();
                    goto Label_021B;

                case DragState.Zooming:
                {
                    double d = ((double) ((num * num) + (num2 * num2))) / ((double) ((base.Width * base.Width) + (base.Height * base.Height)));
                    float currentZoomLevel = this.m_CurrentZoomLevel;
                    if (num2 <= 0)
                    {
                        num4 = this.m_CurrentZoomLevel * ((float) (1.0 / (1.0 + Math.Sqrt(d))));
                        break;
                    }
                    num4 = this.m_CurrentZoomLevel * ((float) (1.0 + Math.Sqrt(d)));
                    break;
                }
                case DragState.RotateOrgin:
                    this.m_LastX = e.X;
                    this.m_LastY = e.Y;
                    base.Invalidate();
                    goto Label_021B;

                case DragState.Rotate:
                    this.m_LastX = e.X;
                    this.m_LastY = e.Y;
                    base.Invalidate();
                    goto Label_021B;

                default:
                    goto Label_021B;
            }
            this.ZoomAt(base.Width / 2, base.Height / 2, num4);
            this.m_LastX = e.X;
            this.m_LastY = e.Y;
            base.Invalidate();
        Label_021B:
            base.OnMouseMove(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            switch (this.m_DragState)
            {
                case DragState.Boxing:
                    if (e.Button == MouseButtons.Left)
                    {
                        if ((this.m_ZoomBox.Width > 5) && (this.m_ZoomBox.Height > 5))
                        {
                            this.Zoom(this.m_ZoomBox);
                        }
                        this.m_DragState = DragState.None;
                        base.Invalidate();
                    }
                    break;

                case DragState.RotateOrgin:
                    break;

                case DragState.Rotate:
                    if (e.Button == MouseButtons.Left)
                    {
                        this.m_DragState = DragState.None;
                        base.Invalidate();
                    }
                    break;

                default:
                    this.m_DragState = DragState.None;
                    break;
            }
            base.Capture = false;
            base.OnMouseUp(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            int originX;
            int originY;
            if (this.m_DragState == DragState.Rotate)
            {
                originX = this.m_OriginX;
                originY = this.m_OriginY;
            }
            else
            {
                originX = e.X;
                originY = e.Y;
            }
            float num3 = (float) ((e.Delta / 120) * 0.1);
            if ((Math.Abs(num3) <= 10.0) && (Math.Abs(num3) >= 0.0099999997764825821))
            {
                if (num3 > 0f)
                {
                    num3 = 1f + num3;
                }
                else if (num3 < 0f)
                {
                    num3 = 1f / (1f - num3);
                }
                this.ZoomAt(originX, originY, this.m_CurrentZoomLevel * num3);
                base.OnMouseWheel(e);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rectangle;
            Matrix matrix;
            if (this.m_CurrentImage == null)
            {
                rectangle = new Rectangle(0, 0, 0, 0);
            }
            else
            {
                rectangle = new Rectangle((int) this.m_CurrentOffset.X, (int) this.m_CurrentOffset.Y, (int) (this.m_CurrentZoomLevel * this.m_CurrentImage.Width), (int) (this.m_CurrentZoomLevel * this.m_CurrentImage.Height));
            }
            if (this.m_DragState == DragState.Rotate)
            {
                double num = Math.Atan2((double) (this.m_LastY - this.m_OriginY), (double) (this.m_LastX - this.m_OriginX)) - Math.Atan2((double) (this.m_RotRefY - this.m_OriginY), (double) (this.m_RotRefX - this.m_OriginX));
                double num2 = (num * 180.0) / 3.1415926535897931;
                double num3 = Math.Sqrt((double) ((this.m_OriginX * this.m_OriginX) + (this.m_OriginY * this.m_OriginY)));
                double num4 = Math.Atan2((double) this.m_OriginY, (double) this.m_OriginX);
                double num5 = this.m_OriginX - (num3 * Math.Cos(num4 + num));
                double num6 = this.m_OriginY - (num3 * Math.Sin(num4 + num));
                matrix = e.Graphics.Transform;
                matrix.Translate((float) num5, (float) num6);
                matrix.Rotate((float) num2);
                e.Graphics.Transform = matrix;
            }
            Brush black = Brushes.Black;
            Matrix transform = e.Graphics.Transform;
            Rectangle rectangle2 = new Rectangle(-1, -1, e.ClipRectangle.Width + 2, e.ClipRectangle.Height + 2);
            transform.Invert();
            Point[] pts = new Point[] { new Point(0, 0), new Point(rectangle2.Width, 0), new Point(base.Bounds.Width, rectangle2.Height), new Point(0, base.Bounds.Height) };
            transform.TransformPoints(pts);
            GraphicsPath path = new GraphicsPath(pts, new byte[] { 1, 1, 1, 1 });
            path.CloseFigure();
            Region region = new Region(path);
            region.Xor(rectangle);
            e.Graphics.FillRegion(black, region);
            if (this.m_CurrentImage != null)
            {
                e.Graphics.DrawImage(this.m_CurrentImage, rectangle);
            }
            if (this.m_DragState == DragState.Rotate)
            {
                matrix = e.Graphics.Transform;
                matrix.Reset();
                e.Graphics.Transform = matrix;
            }
            if (this.m_DragState == DragState.Boxing)
            {
                e.Graphics.DrawRectangle(Pens.Red, this.m_ZoomBox);
            }
            else if (this.m_DragState == DragState.RotateOrgin)
            {
                e.Graphics.DrawLine(RotateLinePen, this.m_OriginX, this.m_OriginY, this.m_LastX, this.m_LastY);
            }
            else if (this.m_DragState == DragState.Rotate)
            {
                e.Graphics.DrawLine(RotateLinePen, (2 * this.m_OriginX) - this.m_LastX, (2 * this.m_OriginY) - this.m_LastY, this.m_LastX, this.m_LastY);
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnResize(EventArgs eventargs)
        {
            this.Center();
            base.Invalidate();
        }

        public void SetImage(Image img)
        {
            this.m_CurrentImage = img;
            this.BestFit();
            base.Invalidate();
        }

        internal void Zoom(Rectangle rec)
        {
            float num = ((float) rec.Width) / ((float) rec.Height);
            float num2 = ((float) base.Width) / ((float) base.Height);
            if (num > num2)
            {
                rec.Y -= (int) (((((float) rec.Width) / num2) - rec.Height) / 2f);
                rec.Height = (int) (((float) rec.Width) / num2);
            }
            else if (num < num2)
            {
                rec.X -= (int) (((rec.Height * num2) - rec.Width) / 2f);
                rec.Width = (int) (rec.Height * num2);
            }
            float num3 = (this.m_CurrentZoomLevel * base.Width) / ((float) rec.Width);
            if ((num3 > 0.01) && (num3 < 100.0))
            {
                this.m_CurrentOffset.X = (((float) base.Width) / 2f) - ((num3 / this.m_CurrentZoomLevel) * ((-this.m_CurrentOffset.X + rec.X) + (((float) rec.Width) / 2f)));
                this.m_CurrentOffset.Y = (((float) base.Height) / 2f) - ((num3 / this.m_CurrentZoomLevel) * ((-this.m_CurrentOffset.Y + rec.Y) + (((float) rec.Height) / 2f)));
                this.m_CurrentZoomLevel = num3;
                if (this.m_Host != null)
                {
                    this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
                }
            }
        }

        internal void Zoom(float factor)
        {
            this.m_CurrentOffset = new PointF((this.m_CurrentOffset.X * factor) - ((factor - 1f) * (base.Width / 2)), (this.m_CurrentOffset.Y * factor) - ((factor - 1f) * (base.Height / 2)));
            this.m_CurrentZoomLevel *= factor;
            if (this.m_Host != null)
            {
                this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
            }
        }

        private void ZoomAt(int x, int y, float newzoomlevel)
        {
            float currentZoomLevel = this.m_CurrentZoomLevel;
            float num2 = newzoomlevel;
            if ((num2 > 0.01) && (num2 < 100.0))
            {
                this.m_CurrentOffset.X = x - (((x - this.m_CurrentOffset.X) * num2) / currentZoomLevel);
                this.m_CurrentOffset.Y = y - (((y - this.m_CurrentOffset.Y) * num2) / currentZoomLevel);
                this.m_CurrentZoomLevel = num2;
                if (this.m_Host != null)
                {
                    this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
                }
                base.Invalidate();
            }
        }

        public IImagePanelHost Host
        {
            get
            {
                return this.m_Host;
            }
            set
            {
                this.m_Host = value;
            }
        }

        public DragTool tool
        {
            get
            {
                return this.m_Tool;
            }
            set
            {
                switch (value)
                {
                    case DragTool.Zoom:
                        this.Cursor = Cursors.SizeNS;
                        break;

                    case DragTool.Box:
                        this.Cursor = Cursors.Cross;
                        break;

                    case DragTool.Pan:
                        this.Cursor = Cursors.SizeAll;
                        break;

                    case DragTool.Rotate:
                        this.Cursor = Cursors.HSplit;
                        this.m_DragState = DragState.None;
                        break;
                }
                this.m_Tool = value;
                if (this.m_Host != null)
                {
                    this.m_Host.DragToolChanged(value);
                }
            }
        }

        public float ZoomLevel
        {
            get
            {
                return this.m_CurrentZoomLevel;
            }
            set
            {
                if ((this.m_CurrentZoomLevel > 0.01) || (this.m_CurrentZoomLevel < 100.0))
                {
                    this.m_CurrentZoomLevel = value;
                    if (this.m_Host != null)
                    {
                        this.m_Host.ZoomLevelChanged(this.m_CurrentZoomLevel);
                    }
                    base.Invalidate();
                }
            }
        }

        private enum DragState
        {
            Panning,
            Boxing,
            Zooming,
            RotateOrgin,
            Rotate,
            None
        }
    }
}

