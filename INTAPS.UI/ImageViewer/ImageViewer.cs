namespace INTAPS.Archive.UI
{
    using INTAPS.UI.Properties;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Windows.Forms;

    public partial class ImageViewer : UserControl, IImagePanelHost
    {
        
        
              
        
        
        
       
        private IContainer components = null;
        
        
        
        private IImageBrowser m_Browser = null;
        internal bool m_FullScreenMode;
        internal int m_Page;
        private ImageViewer m_Parent;
        private bool m_ShowNext = true;
        private bool m_ShowPageNumber = true;
        private bool m_ShowPageTitle = true;
        private bool m_ShowPrev = true;
        
        
        
        

        public ImageViewer()
        {
            this.InitializeComponent();
            this.imgPanel.Host = this;
            this.SetFullSreenMode(false, null);
            this.SetState(true);
            this.ShowImage();
        }

        private void btnCloseFullScreen_Click(object sender, EventArgs e)
        {
            ((Form) base.Parent).Close();
        }

        private void btnFullScreen_Click(object sender, EventArgs e)
        {
            ViewerForm form = new ViewerForm();
            form.imageViewer1.SetFullSreenMode(true, this);
            form.imageViewer1.m_Browser = this.m_Browser;
            form.imageViewer1.m_Page = this.m_Page;
            form.imageViewer1.ShowImageSetState(true);
            form.ShowDialog();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            this.m_Browser.MoveNext();
            this.m_Page++;
            this.ShowImageSetState(false);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            this.m_Browser.MovePrev();
            this.m_Page--;
            this.ShowImageSetState(false);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (this.m_Browser.CurrentImage != null)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    new Bitmap(this.m_Browser.CurrentImage).Save(dialog.FileName + ".jpg", ImageFormat.Jpeg);
                }
            }
        }

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            this.imgPanel.Zoom((float) 1.5f);
            this.imgPanel.Center();
            this.imgPanel.Invalidate();
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            this.imgPanel.Zoom((float) 0.5f);
            this.imgPanel.Invalidate();
        }

        private void cmbZoom_Click(object sender, EventArgs e)
        {
        }

        private void cmbZoom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SetComboZoom();
            }
        }

        private void cmbZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((this.cmbZoom.Items.Count - this.cmbZoom.SelectedIndex))
            {
                case 1:
                    this.imgPanel.BestFit();
                    this.imgPanel.Invalidate();
                    break;

                case 2:
                    this.imgPanel.FitHeight();
                    this.imgPanel.Invalidate();
                    break;

                case 3:
                    this.imgPanel.FitWidth();
                    this.imgPanel.Invalidate();
                    break;

                default:
                    this.SetComboZoom();
                    break;
            }
        }

        private void cmbZoom_Validated(object sender, EventArgs e)
        {
            this.ZoomLevelChanged(this.imgPanel.ZoomLevel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void DragToolChanged(DragTool tool)
        {
            this.btnPan.Checked = tool == DragTool.Pan;
            this.btnZoomInteractive.Checked = tool == DragTool.Zoom;
            this.btnZoomBox.Checked = tool == DragTool.Box;
            this.tsbtnRotate.Checked = tool == DragTool.Rotate;
        }

        public void GoToFirst()
        {
            while (this.m_Page > 0)
            {
                this.m_Browser.MovePrev();
                this.m_Page--;
            }
            this.ShowImage();
            this.SetState(false);
        }

        public void GotoLast()
        {
            int nPages = this.m_Browser.NPages;
            while (this.m_Page < (nPages - 1))
            {
                this.m_Browser.MoveNext();
                this.m_Page++;
            }
            this.ShowImage();
            this.SetState(false);
        }

        public void ImageAdded(bool MoveLast)
        {
            this.SetState(this.m_Parent == null);
        }

        public void ImageDeleted(int Page)
        {
            if (Page == this.m_Page)
            {
                this.m_Page--;
                this.ShowImageSetState(this.m_Parent == null);
            }
        }

       
        public void PanChanged(Point pan)
        {
        }

        private void SetComboZoom()
        {
            string text = this.cmbZoom.Text;
            if (text.Length != 0)
            {
                text = text.TrimEnd(new char[] { '%' });
                try
                {
                    this.imgPanel.ZoomLevel = float.Parse(text) / 100f;
                }
                catch
                {
                    goto Label_0056;
                }
                return;
            }
        Label_0056:
            MessageBox.Show("Please enter valid zoom level");
            this.ZoomLevelChanged(this.imgPanel.ZoomLevel);
        }

        internal void SetFullSreenMode(bool fsm, ImageViewer parent)
        {
            this.m_FullScreenMode = fsm;
            this.btnFullScreen.Enabled = !fsm;
            this.btnCloseFullScreen.Enabled = fsm;
            if (fsm)
            {
                this.m_Parent = parent;
            }
            else
            {
                this.m_Parent = null;
            }
        }

        public void SetPage(Image img, int Page)
        {
            this.m_Page = Page;
            this.imgPanel.SetImage(img);
        }

        public void SetSource(IImageBrowser browser)
        {
            this.m_Browser = browser;
            this.m_Page = 0;
            this.ShowImageSetState(false);
        }

        private void SetState(bool IsParent)
        {
            if (this.m_Browser == null)
            {
                this.btnNext.Enabled = false;
                this.btnPrev.Enabled = false;
            }
            else
            {
                this.btnPrev.Enabled = this.m_Page > 0;
                this.btnNext.Enabled = (this.m_Browser.NPages - 1) > this.m_Page;
            }
            if (!((this.m_Parent == null) || IsParent))
            {
                this.m_Parent.m_Page = this.m_Page;
                this.m_Parent.SetState(false);
            }
        }

        public void ShowImage()
        {
            if ((this.m_Browser != null) && (this.m_Browser.NPages > 0))
            {
                this.imgPanel.SetImage(this.m_Browser.CurrentImage);
                this.lblPage.Text = "Page " + this.m_Browser.CurrentPageNumber.ToString() + "/" + this.m_Browser.NPages.ToString();
                this.lblTitle.Text = "(" + this.m_Browser.CurrentTitle + ")";
            }
            else
            {
                this.lblPage.Text = "";
                this.lblTitle.Text = "";
                this.imgPanel.SetImage(null);
            }
            if (this.m_Parent != null)
            {
                this.m_Parent.ShowImage();
            }
        }

        internal void ShowImageSetState(bool IsParent)
        {
            this.SetState(IsParent);
            this.ShowImage();
        }

        private void ZoomButtonClick(object sender, EventArgs e)
        {
            if (sender == this.btnZoomBox)
            {
                this.imgPanel.tool = DragTool.Box;
            }
            else if (sender == this.btnPan)
            {
                this.imgPanel.tool = DragTool.Pan;
            }
            else if (sender == this.btnZoomInteractive)
            {
                this.imgPanel.tool = DragTool.Zoom;
            }
            else if (sender == this.tsbtnRotate)
            {
                this.imgPanel.tool = DragTool.Rotate;
            }
        }

        public void ZoomLevelChanged(float Level)
        {
            this.cmbZoom.Text = (Level * 100f).ToString("0.0");
        }

        public bool ShowNext
        {
            get
            {
                return this.m_ShowNext;
            }
            set
            {
                this.btnNext.Visible = value;
                this.m_ShowNext = value;
            }
        }

        public bool ShowPageNumber
        {
            get
            {
                return this.m_ShowPageNumber;
            }
            set
            {
                this.lblPage.Visible = value;
                this.m_ShowPageNumber = value;
            }
        }

        public bool ShowPageTitle
        {
            get
            {
                return this.m_ShowPageTitle;
            }
            set
            {
                this.lblTitle.Visible = value;
                this.m_ShowPageTitle = value;
            }
        }

        public bool ShowPrev
        {
            get
            {
                return this.m_ShowPrev;
            }
            set
            {
                this.btnPrev.Visible = value;
                this.m_ShowPrev = value;
            }
        }
    }
}

