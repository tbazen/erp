namespace INTAPS.Archive.UI
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public partial class ViewerForm : Form
    {
        private IContainer components = null;

        public ViewerForm()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        

        private void ViewerForm_Load(object sender, EventArgs e)
        {
            this.imageViewer1.imgPanel.BestFit();
        }
    }
}

