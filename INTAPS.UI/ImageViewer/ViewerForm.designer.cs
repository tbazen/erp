﻿namespace INTAPS.Archive.UI
{
    public partial class ViewerForm : System.Windows.Forms.Form 
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.imageViewer1 = new ImageViewer();
            base.SuspendLayout();
            this.imageViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewer1.Location = new System.Drawing.Point(0, 0);
            this.imageViewer1.Name = "imageViewer1";
            this.imageViewer1.Size = new System.Drawing.Size(0x270, 0x1cf);
            this.imageViewer1.TabIndex = 0;
            base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            base.ClientSize = new System.Drawing.Size(0x270, 0x1cf);
            base.Controls.Add(this.imageViewer1);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            base.Name = "ViewerForm";
            this.Text = "Viewer";
            base.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            base.Load += new System.EventHandler(this.ViewerForm_Load);
            base.ResumeLayout(false);
        }
        #endregion
        internal ImageViewer imageViewer1;

    }
}