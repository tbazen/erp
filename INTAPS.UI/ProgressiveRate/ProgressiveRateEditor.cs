﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI;
using System.Windows.Forms;

namespace INTAPS.UI
{
    public class ProgressiveRateEditor:EDataGridView
    {
        ProgressiveRate _rate = new ProgressiveRate();
        string _fromColumnHeader = "From";
        string _toColumnHeader = "To";
        string _rateColumnHeader= "Rate";
        public ProgressiveRateEditor()
        {
        }
        public void setRate(ProgressiveRate rate)
        {
            this.Rows.Clear();
            _rate = rate;
            for (int i = 0; i < _rate.items.Length; i++)
            {
                switch (_rate.items[i].type)
                {

                    case RangeType.UpperBound:
                        Rows.Add("<" + _rate.items[i].v2.ToString(), null, _rate.items[i].rate.ToString());
                        break;
                    case RangeType.LowerBound:
                        Rows.Add(">" + _rate.items[i].v1.ToString(), null, _rate.items[i].rate.ToString());
                        break;
                    case RangeType.UpperAndLowerBound:
                        Rows.Add(_rate.items[i].v1.ToString(), _rate.items[i].v2.ToString(), _rate.items[i].rate.ToString());
                        break;
                    default:
                        break;
                }
            }
        }
        public bool validateInput(out ProgressiveRate prate, out string msg)
        {
            int index=0;
            msg = null;
            prate = new ProgressiveRate(this.Rows.Count-1);
            foreach (DataGridViewRow row in this.Rows)
            {
                if (row.IsNewRow)
                    break;
                string strV1 = row.Cells[0].Value as string;
                string strV2 = row.Cells[1].Value as string;
                string strRate = row.Cells[2].Value as string;
                if (string.IsNullOrEmpty(strV1))
                {
                    msg = "Data not complete for row " + (index + 1);
                    return false;
                }
                double rate;
                if (!double.TryParse(strRate, out rate))
                {
                    msg = "Invalid rate for row "+(index+1);
                    return false;
                }
                double value1=0,value2=0;
                RangeType type=RangeType.UpperAndLowerBound;
                switch (strV1[0])
                {
                    case '<':case '>':
                        if (!string.IsNullOrEmpty(strV2))
                        {
                            msg = "To column must be empty for row " + (index + 1);
                            return false;
                        }
                        double v;
                        if(!double.TryParse(strV1.Substring(1),out v))
                        {
                            msg = "Invalid from value for row " + (index + 1);
                            return false;
                        }
                        if (strV1[0] == '<')
                        {
                            value2 = v;
                            type = RangeType.UpperBound;
                        }
                        else
                        {
                            value1 = v;
                            type = RangeType.LowerBound;

                        }
                        break;
                    default:
                        type = RangeType.UpperAndLowerBound;
                        if(!double.TryParse(strV1,out value1))
                        {
                            msg = "Invalid from value for row " + (index + 1);
                            return false;
                        }
                        if (!double.TryParse(strV2, out value2))
                        {
                            msg = "Invalid to value for row " + (index + 1);
                            return false;
                        }
                        break;
                }
                switch (type)
                {
                    case RangeType.UpperBound:
                        if (index != 0)
                        {
                            msg = "< should be only on the first row";
                            return false;
                        }
                        break;
                    case RangeType.LowerBound:
                        if (index != this.Rows.Count - 2)
                        {
                            msg = "> should be only on the last row";
                            return false;
                        }
                        break;
                    case RangeType.UpperAndLowerBound:
                        break;
                    default:
                        break;
                }
                ProgressiveRateItem item = new ProgressiveRateItem();
                item.type = type;
                item.v1 = value1;
                item.v2 = value2;
                item.rate = rate;
                for(int i=0;i<index;i++)
                {
                    if (item.overlapsWith(prate.items[i]))
                    {
                        msg = string.Format("Row {0} overlaps with row {1}", i + 1, index + 1);
                        return false;
                    }
                }
                prate.items[index] = item;
                index++;
            }
            return true;
        }
        public void initGrid()
        {
            this.Columns.Add(_fromColumnHeader, _fromColumnHeader);
            this.Columns.Add(_toColumnHeader, _toColumnHeader);
            this.Columns.Add(_rateColumnHeader, _rateColumnHeader);
        }
    }
}
