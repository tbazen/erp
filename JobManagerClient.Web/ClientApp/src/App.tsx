import React from 'react'
import { Provider } from 'react-redux'
import configureStore from './_redux_setup/store/index'
import AppContainer from './AppContainer'
const store = configureStore

const App: React.FC = () => {
  return (
    <Provider store ={store}>
      <AppContainer/>
    </Provider>
  );
}

export default App;
