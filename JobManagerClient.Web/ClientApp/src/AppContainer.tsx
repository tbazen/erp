import React, {useEffect} from 'react'
import { ApplicationState } from './_model/state-model/application-state'
import {useDispatch, useSelector} from 'react-redux'
import LoginScreen from './shared/screens/auth/login-screen'
import LoadingPage from "./shared/components/loading/loading-page"
import {validateSessionId} from "./_redux_setup/actions/auth-actions"
import {BrowserRouter} from "react-router-dom"
import CLayout from './shared/screens/clayout/clayout';

const AppContainer = ()=>{
    const auth = useSelector((state:ApplicationState)=>state.auth)
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(validateSessionId())
    },[])

    return (
        <React.Fragment>
            {
                auth.isCheckingSession &&
                <LoadingPage/>
            }
            {
                auth.authenticated &&
                <BrowserRouter>
                    <CLayout/>
                </BrowserRouter>
            }
            {
                !auth.isCheckingSession&&
                !auth.authenticated &&
                <LoginScreen/>
            }
        </React.Fragment>
    )
}

export default AppContainer