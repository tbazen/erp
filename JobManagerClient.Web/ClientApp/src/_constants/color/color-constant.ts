export const PRIMARY_BACKGROUND_700 = '#536DFE'
export const PRIMARY_BACKGROUND_500 = ''
export const PRIMARY_BACKGROUND_300 = ''

export const PRIMARY_FOREGROUND = '#fff'



export const SECONDARY_BACKGROUND_700 = '#536DFE'
export const SECONDARY_BACKGROUND_500 = ''
export const SECONDARY_BACKGROUND_300 = ''
export const SECONDARY_FOREGROUND = ''


export const SUCCESS_BACKGROUND = '#00C851'
export const ERROR_BACKGROUND = '#FF4444'


export const DRAWER_BACKGROUND = '#2e3a4d'
export const DRAWER_FOREGROUND = '#fff'

export const DRAWER_ACTIVE_ITEM_FG = '#000'
export const DRAWER_ACTIVE_ITEM_BG = 'lightGray'

export const DRAWER_HOVER_ITEM_FG = '#fff'
export const DRAEWR_HOVER_ITEM_BG = 'gray'
