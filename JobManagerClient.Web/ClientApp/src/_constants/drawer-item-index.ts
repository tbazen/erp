export enum DrawerMenuIndex{
    HOME =1,
    HUMAN_RESOURCE=2,
    ITEM_CONFIGURATION =3,
    CONFIGURE_ESTIMATION = 4,

    //dummy index [index with no actionName]
    DUMMY_INDEX = 666,
    //Report items index
    REPORT = 5,
    REPORT_BILLING=6,
    REPORT_CUSTOMER_DATABASE = 7,
    REPORT_DASHBOARD = 8,
    REPORT_FINANCE = 9,
    REPORT_READING = 10,
    REPORT_SYSTEM = 11,
    REPORT_MANAGE =12,
    REPORT_REFRESH_MENU =13
}