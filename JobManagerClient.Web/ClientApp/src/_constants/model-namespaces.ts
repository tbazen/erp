export const NAME_SPACES = {
    WORKFLOW : {
        WORKFLOW_DATA : `INTAPS.WSIS.Job.WorkFlowData, JobManagerTypeLibrary`,
        BILL_CREDIT_DATA : `INTAPS.WSIS.Job.BillCreditData, JobManagerTypeLibrary`,
        BILL_DEPOSIT_DATA : `INTAPS.WSIS.Job.BillDepositData, JobManagerTypeLibrary`,
        CASH_HANDOVER_DATA : `INTAPS.WSIS.Job.CashHandoverData, JobManagerTypeLibrary`,
        CLOSE_CREDIT_SCHEME_DATA : `INTAPS.WSIS.Job.CloseCreditSchemeData, JobManagerTypeLibrary`,
        CONNECTION_MAINTENANCE_DATA : `INTAPS.WSIS.Job.ConntectionMaintenanceData, JobManagerTypeLibrary`,
        DELETE_BILLS_DATA: `INTAPS.WSIS.Job.DeleteBillsData, JobManagerTypeLibrary`,
        DISCONNECT_CONNECTION_DATA: `INTAPS.WSIS.Job.BatchDisconnectData, JobManagerTypeLibrary`,
        RECONNECT_CONNECTION_DATA: `INTAPS.WSIS.Job.BatchReconnectData, JobManagerTypeLibrary`,
        EDIT_CUSTOMER_DATA: `INTAPS.WSIS.Job.EditCustomerData, JobManagerTypeLibrary`,
        EXEMPT_BILL_ITEMS_DATA: `INTAPS.WSIS.Job.ExemptBillItemsData, JobManagerTypeLibrary`,
        NEW_LINE_DATA: `INTAPS.WSIS.Job.NewLineData, JobManagerTypeLibrary`,
        GENERAL_SELLS_DATA: `INTAPS.WSIS.Job.GeneralSellsData, JobManagerTypeLibrary`,
        OWNERSHIP_TRANSFER_DATA: `INTAPS.WSIS.Job.OwnerShipTransferData, JobManagerTypeLibrary`,
        READING_CORRECTION_DATA: `INTAPS.WSIS.Job.ReadingCorrectionData, JobManagerTypeLibrary`,
        RETURN_METER_DATA: `INTAPS.WSIS.Job.ReturnMeterData, JobManagerTypeLibrary`,
        VOID_RECEIPT_DATA: `INTAPS.WSIS.Job.VoidReceiptData, JobManagerTypeLibrary`,
        SHASHEMENE: {
            SHASHEMENE_CONNECTION_MAINTENANCE_DATA:'INTAPS.WSIS.Job.Shashemene.ConntectionMaintenanceShashemeneData, JobRuleEnglineTypeLibraryShashemene'
        },
        ASSELA:{
            ASSELA_CONNECTION_MAINTENACE_DATA: 'INTAPS.WSIS.Job.Assela.ConntectionMaintenanceAsselaData, JobRuleEnglineTypeLibraryAssela'
        },
        DIRE_DAWA:{
            HYDRANT_WATER_SUPPLY_DATA: 'INTAPS.WSIS.Job.DD.HydrantWaterSupplyData, DDJobRuleEnglineTypeLibrary',
            LIQUID_WASTE_DISPOSAL_DATA: 'INTAPS.WSIS.Job.DD.LiquidWasteDisposalData, DDJobRuleEnglineTypeLibrary',
        },
        DUKEM:{
            CONNTECTION_MAINTENANCE_DUKEM_DATA:"INTAPS.WSIS.Job.Dukem.ConntectionMaintenanceDukemData, JobRuleEnglineTypeLibraryDukem"
        },
        NEKEMTE:{
            NEKEMTE_CONNECTION_MAINTENANCE_DATA:'INTAPS.WSIS.Job.Nekemte.NekemteConnectionMaintenanceData, NekemteJobRuleEnglineTypeLibrary'
        },
        HARAR:{
            CONNTECTION_MAINTENANCE_HARAR_DATA:'INTAPS.WSIS.Job.Harar.ConntectionMaintenanceHararData, HararJobRuleEnglineTypeLibrary',
            LIQUID_WASTE_DISPOSAL_DATA:'INTAPS.WSIS.Job.Harar.LiquidWasteDisposalData, HararJobRuleEnglineTypeLibrary',
            HYDRANT_WATER_SUPPLY_DATA:'INTAPS.WSIS.Job.Harar.HydrantWaterSupplyData, HararJobRuleEnglineTypeLibrary',
            CONNECTION_PENALITY_DATA:'INTAPS.WSIS.Job.Harar.ConnectionPenalityData, HararJobRuleEnglineTypeLibrary',
        }
    },
    DOCUMENT_TYPE: {
      CUSTOMER_BILL_DOCUMENT:"INTAPS.SubscriberManagment.CustomerBillDocument, SubscriberManagmentTypeLibrary",
      CUSTOMER_PAYMENT_RECEIPT:"INTAPS.SubscriberManagment.CustomerPaymentReceipt, SubscriberManagmentTypeLibrary"
    },
   ESTIMATION_CONFIGURATION : {
        SHASHEMENE :`INTAPS.WSIS.Job.Shashemene.ShashemeneEstimationConfiguration, JobRuleEnglineTypeLibraryShashemene`,
        ADAMA: `INTAPS.WSIS.Job.AdamaBishoftu.AdamaEstimationConfiguration, JobRuleEnglineTypeLibraryAdamaBishoftu`
   },
    REPORT: {
        CLIENT:{
            ACCOUNTING_LIBRARY:{
                RF_DATE_RANGE_PARAMETER:'INTAPS.Accounting.Client.RFDateRangeParameter,AccountingClientLibarary',
                RCH_DATE_RANGE_PARAMETER:'INTAPS.Accounting.Client.RCHDateRangeParameter,AccountingClientLibarary',
                RCH_DATE_RANGE_PARAMETER_FINANCE:'INTAPS.Accounting.Client.RCHDateRangeParameterFinance,AccountingClientLibarary',

                RF_NO_PARAMETER:'INTAPS.Accounting.Client.RFNoParameter,AccountingClientLibarary',
                RCH_NO_PARAMETER:'INTAPS.Accounting.Client.RCHNoParameter,AccountingClientLibarary',
                RCH_NO_PARAMETER_FINANCE:'INTAPS.Accounting.Client.RCHNoParameterFinance,AccountingClientLibarary',

                RF_SINGLE_DATE_PARAMETER:'INTAPS.Accounting.Client.RFSingleDateParameter,AccountingClientLibarary',
                RCH_SINGLE_DATE_PARAMETER:'INTAPS.Accounting.Client.RCHSingleDateParameter,AccountingClientLibarary',
                RCH_SINGLE_DATE_PARAMETER_FINANCE:'INTAPS.Accounting.Client.RCHSingleDateParameterFinance,AccountingClientLibarary',
             },
            JOB_MANAGER_LIBRARY:{
                ECH_JOB_TYPE:'INTAPS.WSIS.Job.Client.ECHJobType,JobManagerClientLibrary',
                RF_SINGLE_PERIOD:'INTAPS.WSIS.Job.Client.RFSinglePeriod,JobManagerClientLibrary'
            },
            SUBSCRIBER_MANAGEMENT_LIBRARY:{
                ECH_SALE_POINT_SUMMERY: 'INTAPS.SubscriberManagment.Client.ECHSalePointSummary,SubscriberManagmentClientLibrary',
                ECH_SINGLE_PERIOD: 'INTAPS.SubscriberManagment.Client.ECHSinglePeriod,SubscriberManagmentClientLibrary',
                ECH_METER_READER_PERIOD_FORM:'INTAPS.SubscriberManagment.Client.ECHMeterReaderPeriodForm,SubscriberManagmentClientLibrary',
                ECH_SINGLE_PERIOD_DATE_RANGE:'INTAPS.SubscriberManagment.Client.ECHSinglePeriodDateRange,SubscriberManagmentClientLibrary',
                ECH_PERIOD_RANGE :'INTAPS.SubscriberManagment.Client.ECHPeriodRange,SubscriberManagmentClientLibrary',
                ECH_READER_PERIOD : 'INTAPS.SubscriberManagment.Client.ECHReaderPeriod,SubscriberManagmentClientLibrary',
                ECH_KEBELE_PERIOD : 'INTAPS.SubscriberManagment.Client.ECHKebelePeriod,SubscriberManagmentClientLibrary',
                ECH_PAYMENT_CENTER_PERIOD: 'INTAPS.SubscriberManagment.Client.ECHPaymentCenterPeriod,SubscriberManagmentClientLibrary',



               /* RF_PAYMENT_CENTER_PERIOD:'INTAPS.SubscriberManagment.Client.RFPaymentCenterPeriod,SubscriberManagmentClientLibrary',
                RF_PAYMENT_CENTER_DATE_RANGE_PERIOD:'INTAPS.SubscriberManagment.Client.RFPaymentCenterDateRangePeriod,SubscriberManagmentClientLibrary',

                RF_PERIOD_RANGE:'INTAPS.SubscriberManagment.Client.RFPeriodRange,SubscriberManagmentClientLibrary',
                RFP_KEBELE_PERIOD:'INTAPS.SubscriberManagment.Client.RFPKebelePeriod,SubscriberManagmentClientLibrary',

                RF_SINGLE_PERIOD:'INTAPS.SubscriberManagment.Client.RFSinglePeriod,SubscriberManagmentClientLibrary',
                RF_SINGLE_PERIOD_DATE_RANGE:'INTAPS.SubscriberManagment.Client.RFSinglePeriodDateRange,SubscriberManagmentClientLibrary',*/
            }
        }
    }
}