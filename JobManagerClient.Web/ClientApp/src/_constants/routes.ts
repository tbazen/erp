import {StandardJobTypes} from "../_model/view_model/mn-vm/standard-job-types";

type Dictionary = {[index : number]:{ INDEX:string , ABSOLUTE  : string}}
interface IRouteContainer{
    CONFIGURATION:{ NEW_LINE : string }
    JOB: {
        NEW_LINE: {
            INDEX: string,
            EDIT: string,
        },
        EXEMPT_BILL_ITEMS: string,
        CLOSE_CREDIT_SCHEME: string,
        BATCH_DELETE_BILLS: string,
        BILLING_CREDIT: string,
        BILLING_DEPOSIT: string,
        READING_CORRECTION: string,
        RETURN_METER: string,
        CONNECTION_MAINTENANCE: string,
        CONNECTION_OWNERSHIP_TRANSFER: string,
        LINE_MAINTENANCE: string,

        INTERNAL_SERVICE: {
            INDEX: string,
            CASH_HANDOVER: string,
            EDIT_CUSTOMER_DATA: string,
            VOID_RECEIPT: string,
            BATCH_DISCONNECT: string,
            BATCH_RECONNECT: string
        };

        OTHER_SERVICE:{
            INDEX:string,
            CUSTOMER_SPONSORED_NETWORK_WORK:string,
            LIQUID_WASTE_DISPOSAL:string,
            HYDRANT_SERVICE:string,
            CONNECTION_PENALITY:string,
            GENERAL_CELL:string,
        }
    },
    MAIN :{
        INDEX : string,
        CUSTOMER_LIST: string,
        WORKER_LIST: string,
    },
    ACTIVE_JOBS:string;
    ACTIVE_JOB : {
        EDIT : Dictionary
    };
    HUMAN_RESOURCE: string,
    ITEM_CONFIGURATION : string,
    REPORT_ABSOLUTE : string,
    REPORT_INDEX : string,
    R_MANAGE_REPORT : string
}

export const ROUTES :  IRouteContainer = {
    CONFIGURATION:{
      NEW_LINE : "/configuration/new line"
    },
    JOB :{
        NEW_LINE:{
          INDEX:"/job/new line",
          EDIT: "/job/new line/:jobId",
        },
        EXEMPT_BILL_ITEMS:"/job/exempt bill items",
        CLOSE_CREDIT_SCHEME:"/job/close credit scheme",
        BATCH_DELETE_BILLS:"/job/batch delete bills",
        BILLING_CREDIT:"/job/billing credit",
        BILLING_DEPOSIT:"/job/billing deposit",
        READING_CORRECTION:"/job/reading correction",
        RETURN_METER:"/job/return meter",
        CONNECTION_MAINTENANCE:"/job/connection maintenance",
        CONNECTION_OWNERSHIP_TRANSFER:"/job/connection ownership transfer",
        LINE_MAINTENANCE:"/job/line maintenance",

        INTERNAL_SERVICE:{
            INDEX:"/job/internal services",
            CASH_HANDOVER : "/job/internal services/cash handover",
            EDIT_CUSTOMER_DATA : "/job/internal services/edit customer data",
            VOID_RECEIPT:"/job/internal services/void receipt",
            BATCH_DISCONNECT:"/job/internal services/batch disconnect",
            BATCH_RECONNECT:"/job/internal services/batch reconnect"
        },

        OTHER_SERVICE:{
            INDEX:"/job/other services",
            CUSTOMER_SPONSORED_NETWORK_WORK:"/job/other services/customer sponsored network work",
            LIQUID_WASTE_DISPOSAL:"/job/other services/liquid waste disposal",
            HYDRANT_SERVICE:"/job/other services/hydrant services",
            CONNECTION_PENALITY:"/job/other services/connection penalty",
            GENERAL_CELL:"/job/other services/general cell",
        },
    },
    MAIN :{
        INDEX : "/main",
        CUSTOMER_LIST: "/main/customer list",
        WORKER_LIST: "/main/worker list",
    },
    ACTIVE_JOBS:'/active jobs',
    ACTIVE_JOB: {
      EDIT : {
          [StandardJobTypes.NEW_LINE]: {
              INDEX : '/active jobs/new line/edit',
              ABSOLUTE : '/active jobs/new line/edit/:jobId'
          },
          [StandardJobTypes.GENERAL_SELL]: {
              INDEX : '/active jobs/general sell/edit',
              ABSOLUTE : '/active jobs/general sell/edit/:jobId'
          },
          [StandardJobTypes.CONNECTION_PENALITY]: {
              INDEX : '/active jobs/connection penalty/edit',
              ABSOLUTE : '/active jobs/connection penalty/edit/:jobId'
          },
          [StandardJobTypes.EXEMPT_BILL_ITEMS]: {
              INDEX : '/active jobs/exempt bill item/edit',
              ABSOLUTE : '/active jobs/exempt bill item/edit/:jobId'
          },
          [StandardJobTypes.CLOSE_CREDIT_SCHEME]: {
              INDEX : '/active jobs/close credit scheme/edit',
              ABSOLUTE : '/active jobs/close credit scheme/edit/:jobId'
          },
          [StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK]: {
              INDEX : '/active jobs/customer sponsored network expansion/edit',
              ABSOLUTE : '/active jobs/customer sponsored network expansion/edit/:jobId'
          },
          [StandardJobTypes.HYDRANT_SERVICE]: {
              INDEX : '/active jobs/hydrant service/edit',
              ABSOLUTE : '/active jobs/hydrant service/edit/:jobId'
          },
          [StandardJobTypes.LIQUID_WASTE_DISPOSAL]: {
              INDEX : '/active jobs/liquid waste disposal/edit',
              ABSOLUTE : '/active jobs/liquid waste disposal/edit/:jobId'
          },
          [StandardJobTypes.BATCH_DELETE_BILLS]: {
              INDEX : '/active jobs/batch delete bills/edit',
              ABSOLUTE : '/active jobs/batch delete bills/edit/:jobId'
          },
          [StandardJobTypes.BATCH_RECONNECT]: {
              INDEX : '/active jobs/batch reconnect/edit',
              ABSOLUTE : '/active jobs/batch reconnect/edit/:jobId'
          },
          [StandardJobTypes.BATCH_DISCONNECT]: {
              INDEX : '/active jobs/batch disconnect/edit',
              ABSOLUTE : '/active jobs/batch disconnect/edit/:jobId'
          },
          [StandardJobTypes.VOID_RECEIPT]: {
              INDEX : '/active jobs/void receipt/edit',
              ABSOLUTE : '/active jobs/void receipt/edit/:jobId'
          },
          [StandardJobTypes.BILLING_CERDIT]: {
              INDEX : '/active jobs/billing credit/edit',
              ABSOLUTE : '/active jobs/billing credit/edit/:jobId'
          },
          [StandardJobTypes.BILLING_DEPOSIT]: {
              INDEX : '/active jobs/billing deposit/edit',
              ABSOLUTE : '/active jobs/billing deposit/edit/:jobId'
          },
          [StandardJobTypes.READING_CORRECTION]: {
              INDEX : '/active jobs/reading correction/edit',
              ABSOLUTE : '/active jobs/reading correction/edit/:jobId'
          },
          [StandardJobTypes.EDIT_CUTOMER_DATA]: {
              INDEX : '/active jobs/edit customer data/edit',
              ABSOLUTE : '/active jobs/edit customer data/edit/:jobId'
          },
          [StandardJobTypes.RETURN_METER]: {
              INDEX : '/active jobs/return meter/edit',
              ABSOLUTE : '/active jobs/return meter/edit/:jobId'
          },
          [StandardJobTypes.CONNECTION_MAINTENANCE]: {
              INDEX : '/active jobs/connection maintenance/edit',
              ABSOLUTE : '/active jobs/connection maintenance/edit/:jobId'
          },
          [StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER]: {
              INDEX : '/active jobs/connection ownership transfer/edit',
              ABSOLUTE : '/active jobs/connection ownership transfer/edit/:jobId'
          },
          [StandardJobTypes.CASH_HANDOVER]: {
              INDEX : '/active jobs/cash handover/edit',
              ABSOLUTE : '/active jobs/cash handover/edit/:jobId'
          },
      }
    },

    HUMAN_RESOURCE: '/human resource',
    ITEM_CONFIGURATION :'/item configuration',
    REPORT_ABSOLUTE : '/report/:reportTypeId/:reportDefinitionId',
    REPORT_INDEX : '/report',
    R_MANAGE_REPORT : '/manage report'
}