import { StandardJobTypes } from '../_model/view_model/mn-vm/standard-job-types';
import {IRoute} from "../_route/route-index";

export interface IJobRuleClientDecorator{
    jobType : StandardJobTypes ;
    jobName : string;
    route : string;
    priority? : number;
    hasConfiguration? : boolean;
    configurationName? : string;
    configurationRoute? : string;

    innerRoutes? : IRoute[];
}

export function JobRuleClientDecorator(jobRuleHandler : IJobRuleClientDecorator ){
    return function <TFunction extends Function>( target :  TFunction) : TFunction {
        const newJobConstructor : Function = function() {}
        
        newJobConstructor.prototype = Object.create(target.prototype);
        newJobConstructor.prototype.constructor = target;

        newJobConstructor.prototype.isStandardJobType = true;
        newJobConstructor.prototype.jobType = jobRuleHandler.jobType;
        newJobConstructor.prototype.route = jobRuleHandler.route;
        newJobConstructor.prototype.jobName = jobRuleHandler.jobName;
        newJobConstructor.prototype.priority = jobRuleHandler.priority ? jobRuleHandler.priority : 0;

        newJobConstructor.prototype.hasConfiguration = jobRuleHandler.hasConfiguration;
        newJobConstructor.prototype.configurationName = jobRuleHandler.configurationName;
        newJobConstructor.prototype.configurationRoute = jobRuleHandler.configurationRoute;
        newJobConstructor.prototype.innerRoutes = jobRuleHandler.innerRoutes;
        return <TFunction>newJobConstructor;
    }
}