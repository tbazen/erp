export enum CostSharingStatus {
    EvidenceProvided = 0,
    EvidenceNotProvided = 1,
    NotApplicable = 2
}