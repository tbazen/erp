export enum EmployeeRoll {
    Manager = 0,
    AssistantManager = 1,
    Clerk = 2,
    Expert = 3,
    Consultant = 4
}