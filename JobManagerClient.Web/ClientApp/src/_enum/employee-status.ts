export enum EmployeeStatus {
    Pending = 0,
    Enrolled = 1,
    Fired = 2
}