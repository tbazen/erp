export enum EmploymentType {
    Permanent = 0,
    Temporary = 1
}