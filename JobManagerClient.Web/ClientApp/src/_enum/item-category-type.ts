export enum ItemCategoryType{
    EXPENSE_ITEM = 1,
    SALES_ITEM = 2,
    FIXED_ASSET_ITEM = 3,
    INVENTORY_ITEM=4
}