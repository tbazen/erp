export enum ConnectionType {
    Unknown = 0,
    Yard_1 = 1,
    House_2 = 2,
    Shared_3 = 3
}