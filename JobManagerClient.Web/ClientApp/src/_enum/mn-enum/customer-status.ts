export enum CustomerStatus {
    None = 0,
    Active = 1,
    Inactive = 2
}