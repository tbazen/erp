export enum DataStatus {
    Unknown = 0,
    Complete_1 = 1,
    Critical_Complete_2 = 2,
    Partial_3 = 3,
    No_Meter_4 = 4
}