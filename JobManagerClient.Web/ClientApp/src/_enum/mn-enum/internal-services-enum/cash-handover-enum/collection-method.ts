export enum CollectionMethod {
    None = 0,
    Online = 1,
    Offline = 2,
    Mobile = 4
}