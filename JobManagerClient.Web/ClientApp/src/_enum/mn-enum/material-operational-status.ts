export enum MaterialOperationalStatus {
    Working = 0,
    NotWorking = 1,
    Defective = 2
}