export enum MeterPositioning {
    Unknown = 0,
    Good_For_Reading_1 = 1,
    Bad_For_Reading_2 = 2
}