export enum ServiceType {
    New_Line = 1,
    Leak_Diagnosis = 2,
    Connection_Upgrading = 3,
    Meter_Temporary_Return = 3,
    Ownership_Transfer = 4,
    Connection_Transfer = 5,
    Replacement_of_Brokendown_Meter = 6,
    Replacement_of_Meter = 7,
    Defective_Meter_Diagnosis = 8,
    Meter_Return = 9,
    Sewer_Service = 10,
    Contract_Renewal = 12,
    Hydrant_Service = 13
}