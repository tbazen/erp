export enum SubscriberType {
    Unknown = 0,
    Private = 1,
    GovernmentInstitution = 2,
    CommercialInstitution = 3,
    NGO = 4,
    RelegiousInstitution = 5,
    Community = 6,
    Industry = 7,
    Other = 8
}