export enum SubscriptionStatus {
    Unknown = 0,
    Pending = 1,
    Active = 2,
    Diconnected = 3,
    Discontinued = 4
}