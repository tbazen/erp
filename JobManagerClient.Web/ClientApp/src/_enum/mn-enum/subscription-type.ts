export enum SubscriptionType {
    Unknown = 0,
    Tap = 1,
    Shared = 2,
    Hydrant = 3,
    CattleDrink = 4,
    Well = 5
}