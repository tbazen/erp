export enum SupplyCondition {
    Unknown = 0,
    Very_Good_1 = 1,
    Good_2 = 2,
    Bad_3 = 3,
    Very_Bad_4 = 4
}