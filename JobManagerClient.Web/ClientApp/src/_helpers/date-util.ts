import moment from 'moment';

const EPOCH_MICRO_TIME_DIFF = Math.abs(new Date(0, 0, 1).setFullYear(1));


const getCurrentTicks = (): number => {
    let tick = new Date().getTime() + EPOCH_MICRO_TIME_DIFF
    tick*=10000
    return tick
}

export const DEFAULT_DATE_TIME = '1/1/0001 12:00:00 AM'

export const getTickFromDate = (date:Date): number => {
    let tick = date.getTime() + EPOCH_MICRO_TIME_DIFF
//    let tick = new Date(date).getTime() + EPOCH_MICRO_TIME_DIFF
    tick*=10000
    return tick
}

export const getDateDifference = (date1:string,date2:string) : string =>{
    const firstDate = new Date(date1),secondDate = new Date(date2);
    // @ts-ignore
    let millisecondDifference:number = firstDate - secondDate;

    function getYearDifference():string {
        const years = Math.floor((millisecondDifference) / (1000*60*60*24*365))
        if(years > 0){
            millisecondDifference-= (years*1000*60*60*24*365)
            return `${years} Years `
        }
        return ''
    }
    function getMonthsDifference():  string {
        const months = Math.floor((millisecondDifference) / (1000*60*60*24*30))
        if(months > 0){
            millisecondDifference-= (months*1000*60*60*24*30)
            return `${months} Months `
        }
        return ''
    }
    function getWeeksDifference(): string{
        const weeks = Math.floor((millisecondDifference) / (1000*60*60*24*7))
        if(weeks > 0){
            millisecondDifference-= (weeks*1000*60*60*24*7)
            return `${weeks} Weeks `
        }
        return ''
    }

    function getDaysDifference():string {
        const days = Math.floor((millisecondDifference) / (1000*60*60*24))
        if(days > 0){
            millisecondDifference-= (days*1000*60*60*24)
            return `${days} Days`
        }
        return ''
    }

    function getHoursDifference():string {
        const hours = Math.floor((millisecondDifference) / (1000*60*60))
        if(hours > 0){
            millisecondDifference-= (hours*1000*60*60)
            return `${hours} Hours `
        }
        return ''
    }

    function getMinutesDifference(): string {
        const minutes = Math.floor((millisecondDifference) / (1000*60))
        if(minutes > 0){
            millisecondDifference-= (minutes*1000*60)
            return `${minutes} Minutes `
        }
        return ''
    }
    function getSecondDifference(): string {
        const seconds = Math.floor((millisecondDifference) / (1000))
        if(seconds > 0){
            millisecondDifference-= (seconds*1000)
            return `${seconds} seconds`
        }
        return ''
    }

    return `${getYearDifference()} 
            ${getMonthsDifference()} 
            ${getWeeksDifference()} 
            ${getDaysDifference()} 
            ${getHoursDifference()} 
            ${getMinutesDifference()}
            ${getSecondDifference()}`;
}
export const getDateFromTick = (ticks : number): Date =>{
    let ticksToMicrotime = ticks / 10000;
    let tickDate = new Date(ticksToMicrotime - EPOCH_MICRO_TIME_DIFF);
    return tickDate
}
export const getDateStringFromTicks = (ticks : number): string =>{
    let ticksToMicrotime = ticks / 10000;
    let tickDate = new Date(ticksToMicrotime - EPOCH_MICRO_TIME_DIFF);
    return tickDate.toLocaleDateString()
}
export default getCurrentTicks
export const getLocalDateString = () :string => `${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}`

export const getFormattedDateTime = (dateTime: string) => new Date(dateTime).toLocaleString()
export const getFormattedLocalDate = (dateTime: string) => new Date(dateTime).toLocaleDateString()
export const getFormattedDate = (dateTime: string) => new Date(dateTime).toDateString()

export const years: number[] = [
    new Date().getFullYear()-12,
    new Date().getFullYear()-11,
    new Date().getFullYear()-10,
    new Date().getFullYear()-9,
    new Date().getFullYear()-8,
    new Date().getFullYear()-7,
    new Date().getFullYear()-6,
    new Date().getFullYear()-5,
    new Date().getFullYear()-4,
    new Date().getFullYear()-3,
    new Date().getFullYear()-2,
    new Date().getFullYear()-1,
    new Date().getFullYear(),
    new Date().getFullYear()+1,
    new Date().getFullYear()+2,
    new Date().getFullYear()+3,
    new Date().getFullYear()+4,
    new Date().getFullYear()+5,
]
export const currentYear = (new Date().getFullYear()-7 )


export class MomentUtil {
    public static getCurrentDateTime = ():moment.Moment => { return moment(new Date().toLocaleString()) }
    public static getCurrentDate = () => {}
    public static getCurrentTime = () => {}
    public static getCurrentDateTimeString = ():string => new Date().toLocaleString()
}
