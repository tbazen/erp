import {Ref, RefObject} from "react";

export class FileExtension {

    public static parseBlobToBase64String = async (file: Blob): Promise<any> => {
        return new Promise((resolve, reject) => {
            let reader = new FileReader()
            reader.readAsBinaryString(file)
            reader.onload = ev => {
                resolve(btoa((ev.target as any).result))
            }
            reader.onerror = function(error) {
                reject(error)
            }
        })
    }

    public static getUrlFromBlob(dataBlob: Blob){
        let urlObject = URL.createObjectURL(dataBlob);
        return urlObject;
    }

    public static startFileDownload = (dataBlob: Blob,reference: RefObject<HTMLAnchorElement>, fileName?: string) => {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(dataBlob, fileName);
        } else {
            let urlObject = URL.createObjectURL(dataBlob);

            if(reference && reference.current){
                reference.current.href = urlObject
                reference.current.setAttribute('download', fileName?`${fileName}.reportDef` : `Report Definition.reportDef`);
                reference.current.click()
            }
            URL.revokeObjectURL(urlObject);
        }
    }


}