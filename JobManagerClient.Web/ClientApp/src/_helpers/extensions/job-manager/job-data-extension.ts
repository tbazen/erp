import {JobData} from "../../../_services/job.manager.service";
import {SubscriberExtension} from "./subscriber-extension";

export class JobDataExtension{
    public static validate(jobData: JobData): string[] | undefined{
        let errorList:string[] = []
        if(jobData.customerID === -1){
            if(!jobData.newCustomer)
                errorList = [...errorList,'If existing customer is not selected you must add nuw customers information']
            else{
                const customerValidationError = SubscriberExtension.validate(jobData.newCustomer)
                if(customerValidationError && customerValidationError.length > 0)
                    errorList = [...errorList,...customerValidationError ]
            }

        }
        if(errorList.length> 0)
            return errorList
        return undefined
    }
}