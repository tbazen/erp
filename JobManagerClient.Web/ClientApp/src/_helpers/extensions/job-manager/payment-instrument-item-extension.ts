import {
    PaymentInstrumentItem,
    PaymentInstrumentType
} from "../../../_model/level0/iERP-transaction-model/payment-instrument-item";
import {parseNumberToMoney} from "../../numbers-util";

export interface PaymentInstrumentItemValidationParams{
    amount: number,
    paymentInstrumentType?: PaymentInstrumentType
    paymentInstrumentItem: PaymentInstrumentItem
}

export class PaymentInstrumentItemExtension{

    public static validate(params: PaymentInstrumentItemValidationParams){
        const { paymentInstrumentItem ,paymentInstrumentType,amount } = params
        let errorList:string[] = []

        if(!paymentInstrumentType){
            errorList = [...errorList,'Select payment instrument type']
            return errorList
        }

        if(paymentInstrumentType.isTransferFromAccount){
            if(paymentInstrumentItem.bankBranchID === 0)
                errorList = [...errorList,'Select bank and branch']
            if(paymentInstrumentItem.accountNumber.trim() === '')
                errorList = [...errorList,'Please Enter account number']
        }
        else{
            paymentInstrumentItem.bankBranchID = -1
            paymentInstrumentItem.accountNumber =''
        }
        if(paymentInstrumentType.isBankDocument && !paymentInstrumentType.isToken){
            if(paymentInstrumentItem.despositToBankAccountID === -1)
                errorList = [...errorList,'Select deposit bank account']
        }
        if(paymentInstrumentItem.documentReference.trim() === '')
            errorList = [...errorList,'Please enter reference number']
        if(new Date() < new Date(paymentInstrumentItem.documentDate))
            errorList = [ ...errorList,'Future dated document is not valid']

        if(isNaN(paymentInstrumentItem.amount) || paymentInstrumentItem.amount<= 0)
            errorList = [...errorList, 'Please enter valid amount']

        if(paymentInstrumentItem.amount > amount)
            errorList = [...errorList,`Please enter amount less than or equal to ${parseNumberToMoney(amount)}`]
        if(errorList.length > 0 )
            return errorList
        return undefined
    }
}