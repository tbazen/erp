import {Subscriber} from "../../../_model/view_model/mn-vm/subscriber-search-result";

export class SubscriberExtension{
    public static validate(subscriber: Subscriber):string [] | undefined {
        let errorList:string[] = []
        if(subscriber.name.trim()==='')
            errorList = [...errorList,'Name is required for customer']
        if(errorList.length > 0)
            return errorList
        return undefined
    }
}