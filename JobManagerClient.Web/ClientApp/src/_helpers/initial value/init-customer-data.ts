import { Subscriber } from '../../_model/view_model/mn-vm/subscriber-search-result';
import { CustomerStatus } from '../../_enum/mn-enum/customer-status';
import { SubscriberType } from '../../_enum/mn-enum/subscriber-type';
import { getLocalDateString } from '../date-util';
export const initialCustomerData : Subscriber={
    id: -1,
    subTypeID: -1,
    depositAccountID: 0,
    accountID: 0,
    statusDate: getLocalDateString(),
    status: CustomerStatus.Active,
    email: '',
    nameOfPlace: '',
    customerCode: '',
    amharicName: '',
    address: '',
    kebele: -1,
    subscriberType: SubscriberType.Unknown,
    name: '',
    phoneNo: '',
    objectIDVal: -1
}