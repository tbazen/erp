import { GetSubscriptions2Par, SubscriberSearchField } from '../../_services/subscribermanagment.service';

export const initializeSubscriberSearchParams: GetSubscriptions2Par = {
    sessionId: '',
    query: '',
    fields: SubscriberSearchField.CustomerName | SubscriberSearchField.CustomerCode,
    multipleVersion: false,
    kebele: -1,
    zone: -1,
    dma: -1,
    index: 0,
    page: 30
}