import { AddJobPar } from '../../_services/job.manager.service';
import {MomentUtil} from "../date-util";

export const initialJobData : AddJobPar = {
    sessionId:'',
    job:{
        id:-1,
        jobNo:null,
        newCustomer:null,
        description:'',
        serviceTypes:[],
        logDate:MomentUtil.getCurrentDateTimeString(),
        startDate:MomentUtil.getCurrentDateTimeString(),
        statusDate:MomentUtil.getCurrentDateTimeString(),
        status:0,
        applicationType:0,
        customerID:-1,
        completionDocumentID:-1
    },
    data :  {
        data : null
    }
}