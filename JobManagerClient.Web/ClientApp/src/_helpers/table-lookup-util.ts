import {SubscriptionType} from '../_enum/mn-enum/subscription-type'
import {CustomerStatus} from '../_enum/mn-enum/customer-status'
import {Kebele} from '../_model/view_model/mn-vm/kebele'
import {PressureZone} from '../_model/view_model/mn-vm/pressure-zone'
import {DistrictMeteringZone} from '../_model/view_model/mn-vm/district-metering-zone'
import {SubscriberType} from "../_enum/mn-enum/subscriber-type";

export const lookupSubscriptionType=(subscriptionType?: SubscriptionType ):string=>{
    switch(subscriptionType){
        case SubscriptionType.CattleDrink:
            return 'Cattle Drink'
        case SubscriptionType.Hydrant:
            return 'Hydrant'
        case SubscriptionType.Shared:
            return 'Shared'
        case SubscriptionType.Tap:
            return 'Tap'
        case SubscriptionType.Unknown:
            return 'Unknown'
        case SubscriptionType.Well:
            return 'Well'                 
    }
    return ''
}

export const lookupSubscriberType =( subscriberType ?: SubscriberType ):string=>{
    switch(subscriberType){
        case SubscriberType.CommercialInstitution:
            return 'Commercial Institution'
        case SubscriberType.Community:
            return 'Community'
        case SubscriberType.GovernmentInstitution:
            return 'Government Institution'
        case SubscriberType.Industry:
            return 'Industry'
        case SubscriberType.NGO:
            return 'NGO'
        case SubscriberType.Other:
            return 'Other'
        case SubscriberType.Private:
            return 'Private'
        case SubscriberType.RelegiousInstitution:
            return 'Religious Institution'
        case SubscriberType.Unknown:
            return 'Unknown'
    }
      return ''
}

export const lookupCustomerStatus =(status?: CustomerStatus )=>{
    switch(status){
        case CustomerStatus.Active:
            return 'Active'
        case CustomerStatus.Inactive:
            return 'Inactive'
        case CustomerStatus.None:
            return 'None'
    }
    return ''
}

export const lookupKebele = (kebeleList:Kebele[],kebeleId?: number):string=>{
    const find = kebeleList.find(kbl=>kebeleId==kbl.id)
    return find ? find.name : ''
}

export const lookupPressureZone = (pressureZoneList : PressureZone[],przId?: number):string =>{
    const find = pressureZoneList.find(prz => prz.id == przId)
    return find ? find.name : ''
}

export const lookupDMZ = (DMZList:DistrictMeteringZone[],dmzId?:number) : string=>{
    const find = DMZList.find(dmz=>dmz.id === dmzId)
    return find ? find.name : ''
}