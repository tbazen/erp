export enum ConnectionProfileOptions {
    ShowMeterInfo=1,
    ShowReadingChart=2,
    ShowVersionLink=4,
    ShowMapLink=8,
    ShowLedgerLink=16,
    ShowCustomerRef=32,
    All = ShowMeterInfo | ShowReadingChart | ShowVersionLink | ShowMapLink | ShowLedgerLink | ShowCustomerRef,
    Minimal = (((All ^ ShowReadingChart) ^ ShowVersionLink) ^ ShowMapLink)^ShowLedgerLink,
    NoLink = ShowMeterInfo | ShowReadingChart
}