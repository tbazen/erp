import {IJobRuleClientDecorator} from "../../_decorator/jobrule-client-handler.decorator";
import {JobRuleClientHandlerBase} from "./job-rule-client-handler-base";

export interface IClientHandler {
    meta : IJobRuleClientDecorator,
    handler : JobRuleClientHandlerBase,
}