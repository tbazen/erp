import React,{ Fragment } from "react";
import {RouteComponentProps} from "react-router";
import {JobData} from "../../_services/job.manager.service";

export abstract class JobRuleClientHandlerBase {
    public abstract getNewApplicationForm(): React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>
    public getConfigurationForm(): React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any> { return EmptyComponent }
    public getWorkFlowData(job: JobData):JSX.Element { return <EmptyComponent/> }
}

class EmptyComponent extends React.Component {
    render(){
        return <Fragment/>
    }
}
