import { JobStatusType } from '../view_model/active-jobs-vm/job-status-type';

export enum StandardJobStatus {
    NONE = 0,
    APPLICATION = 1,
    APPLICATION_APPROVAL = 2,
    WORK_APPROVAL = 3,
    SURVEY = 4,
    ANALYSIS = 5,
    APPLICATION_PAYMENT = 6,
    WORK_PAYMENT = 7,
    CANCELED = 8,
    FINISHED = 9,
    INACTIVE = 10,
    TECHNICAL_WORK = 11,
    WORK_COMPLETED = 12,
    CONTRACT = 13,
    FINALIZATION = 14,
    STORE_ISSUE = 15,
    STORE_ISSUED = 17,
}

export const StandardJobStatusTypes : { [ id : number ] : JobStatusType } = {
    [ StandardJobStatus.APPLICATION ]:{ id : StandardJobStatus.APPLICATION, name : "Application", commandName : "Apply" },
    [ StandardJobStatus.APPLICATION_APPROVAL ]: { id : StandardJobStatus.APPLICATION_APPROVAL, name : "Application Approval", commandName : "Send to Application Approval" },
    [ StandardJobStatus.APPLICATION_PAYMENT ] : { id : StandardJobStatus.APPLICATION_PAYMENT, name : "Application Payment", commandName : "Send to Application Payment" },
    [ StandardJobStatus.SURVEY ] : { id : StandardJobStatus.SURVEY, name : "Survey", commandName : "Send to Survey" },
    [ StandardJobStatus.ANALYSIS ] : { id : StandardJobStatus.ANALYSIS, name : "Analysis", commandName : "Send to Analysis" },
    [ StandardJobStatus.WORK_APPROVAL ] : { id : StandardJobStatus.WORK_APPROVAL, name : "Work Approval", commandName : "Send to Work Approval" },
    [ StandardJobStatus.WORK_PAYMENT ] : { id : StandardJobStatus.WORK_PAYMENT, name : "Work Payment", commandName : "Send to Work Payment" },
    [ StandardJobStatus.CONTRACT ] : { id : StandardJobStatus.CONTRACT, name : "Contract", commandName : "Send to Contract" },
    [ StandardJobStatus.TECHNICAL_WORK ] : { id : StandardJobStatus.TECHNICAL_WORK, name : "Technical Work", commandName : "Send to Technical Work" },
    [ StandardJobStatus.FINALIZATION ] : { id : StandardJobStatus.FINALIZATION, name : "Finalization", commandName : "Send to Finalization" },
    [ StandardJobStatus.CANCELED ] : { id : StandardJobStatus.CANCELED, name : "Canceled", commandName : "Cancel" },
    [ StandardJobStatus.FINISHED ] : { id : StandardJobStatus.FINISHED, name : "Finished", commandName : "Finish" },
    [ StandardJobStatus.STORE_ISSUE ] : { id : StandardJobStatus.STORE_ISSUE, name : "Store Issue", commandName : "Send to Store" },
    [ StandardJobStatus.STORE_ISSUED ] : { id : StandardJobStatus.STORE_ISSUED, name : "Stores Issued", commandName : "Store Issue" }
}