import getCurrentTicks, {getLocalDateString} from "../../../_helpers/date-util";

export class AccountDocument {
    public static FUTURE_TOLERANCE_SECONDS: number = 120;
    public accountDocumentID: number = -1;
    public documentTypeID: number = -1;
    public tranTicks: number = -1;

    public get documentDate(): number {
        return this.tranTicks;
    }

    public set documentDate(value: number) {
        this.tranTicks = value;
    }

    public paperRef: string;
    public shortDescription:string | null;
    public longDescription:string | null;
    public reversed:boolean = false;
    public scheduled: boolean = false;
    public materialized: boolean = false;
    public materializedOn:string = getLocalDateString();


    public constructor() {
        this.documentTypeID = -1;
        this.accountDocumentID = -1;
        this.documentDate = getCurrentTicks();
        this.paperRef = "";
        this.reversed = false;
        this.shortDescription = null;
        this.longDescription = null;
    }

    public get posted(): boolean {
        return this.accountDocumentID != -1;
    }
}
