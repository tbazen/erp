import {getLocalDateString} from "../../../_helpers/date-util";
import {AccountStatus} from "../../view_model/ic-vm/account-base";

export class DocumentType {
    public id: number = 0;
    public name: string = '';
    public description: string = '';
    public documentClass: string = '';
    public assemblyName: string = '';
    public system: boolean = false;
}


export class CostCenterAccount {
    public id: number = 0;
    public accountID: number = 0;
    public accountPID: number = 0;
    public costCenterID: number = 0;
    public costCenterPID: number = 0;
    public creditAccount: boolean = false;
    public childAccountCount: number = 0;
    public childCostCenterCount: number = 0;
    public activateDate: string = getLocalDateString();
    public deactivateDate = getLocalDateString();
    public status: AccountStatus = AccountStatus.Activated;

    public constructor() {
        this.creditAccount = false;
    }

    public get isControlAccount(): boolean {
        return this.childAccountCount > 0 || this.childCostCenterCount > 0;
    }

    public get longID(): number {
        let liid: number = this.accountID as number;
        return (this.costCenterID << 32) | liid;
    }
}


export class TransactionItem {
    public static readonly DEFAULT_CURRENCY: number = 0;
    public static readonly MATERIAL_QUANTITY: number = 1;
    public static readonly BUDGET:number = 2;
    public static DefaultCurrency: TransactionItem = {...new TransactionItem(),id:TransactionItem.DEFAULT_CURRENCY,name:'Birr'};
    public static  MaterialQuantity: TransactionItem = {...new TransactionItem(),id:TransactionItem.DEFAULT_CURRENCY,name:'pcs'};
/*
   TODO: OLD CODE REMOVEIT AFTER TEST
    static TransactionItem()
    {
        this.DefaultCurrency=new TransactionItem();
        this.DefaultCurrency.id = this.DEFAULT_CURRENCY;
        this.DefaultCurrency.name = "Birr";

        this.MaterialQuantity = new TransactionItem();
        this.MaterialQuantity.id = this.DEFAULT_CURRENCY;
        this.MaterialQuantity.name = "pcs";
    }*/
    public id:number = 0;
    public name?: string;
    public displayUnit:number = 0;
    public displayFormat?:string  ;
}
