export class DocumentTypedReference {
    public typeID: number = -1;
    public reference:string = "";
    public primary:boolean =false;
    public constructor(value?:{typeID:number,reference:string,primary:boolean }){
        if(value){
            this.typeID = value.typeID;
            this.reference = value.reference;
            this.primary = value.primary;
        }
    }
    public get serial():number{
            return (+this.reference) || -1;
    }
}

export enum DocumentSerializationMode {
    Serialized,
    Manual
}
export class DocumentSerialType {
    public id:number= 0;
    public name?:string;
    public code?:string;
    public serialType: DocumentSerializationMode= DocumentSerializationMode.Serialized;
    public format:string="";
    public description?: string;
    public prefix?: string;
}
