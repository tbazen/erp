import {EData} from "../formula-evaluator/edata";

export class EHTMLData {
    public variables: string[] = [];
    public values: EData[] = [];
    public expressions: string[] = [];
    public mainFormula?: string;

    public valid=():boolean => {
        //any of the arrays nulll
        if (this.variables == null || this.expressions == null || this.values == null)
            return false;
        //arrays of not equal length
        if (this.values.length != this.expressions.length || this.variables.length != this.expressions.length)
            return false;
        //repeated variale
        for (let i = 0; i < this.variables.length - 1; i++)
            for (let j = i + 1; j < this.variables.length; j++)
                if (this.variables[i].toUpperCase() === this.variables[j].toUpperCase())
                    return false;
        return true;
    }
}

export interface ReportDefination {
    id: number;
    categoryID: number;
    code: string;
    name: string;
    description: string;
    reportTypeID: number;
    formula: EHTMLData;
    styleSheet: string;
}

export interface ReportType {
    id: number;
    name: string;
    description: string;
    serverClass: string;
    clientClass: string;
}