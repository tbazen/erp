import {getLocalDateString} from "../../../_helpers/date-util";

export class AccountingPeriod {
    public id: number;
    public name: string | null;
    public fromDate: string;
    public toDate: string;
    public days: number;
    public hours: number;
    public constructor( value:{from:string, to:string}) {
        if(value){
            this.days = 21;
            this.hours = 171;
            this.fromDate = value.from;
            this.toDate = value.to;
            this.id = -1;
            this.name = null;
        } else{
            this.days = 21;
            this.hours = 171;
            this.fromDate = getLocalDateString();
            this.toDate = getLocalDateString();
            this.id = -1;
            this.name = null;
        }

    }
    public midTime: string = ''
}
