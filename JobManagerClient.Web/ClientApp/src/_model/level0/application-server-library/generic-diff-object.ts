export enum GenericDiffType {
    Add = 1,
    Delete = 2,
    Replace = 3,
    NoChange=0
}


export class DiffObject<ObjectType>{
    public diffType: GenericDiffType;
    public data: ObjectType | null ;
    public constructor( value?: {data:ObjectType,type: GenericDiffType } ) {
        if(value){
            this.diffType = value.type;
            this.data = value.data;
        } else {
            this.diffType = GenericDiffType.NoChange;
            this.data = null;
        }
    }
}