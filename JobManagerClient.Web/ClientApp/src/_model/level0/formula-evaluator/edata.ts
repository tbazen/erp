import {DataType} from "./data-type";

export interface EData {
    empty: EData
    calculating: EData
    dataTypes: []
    value: object
    type: DataType
}