export class FunctionDocumentation {
    public xmlStr?: string;
    public category?: String ;
    public description?: String;
    public nPars: number = 0;
    public functionName?: String;
}