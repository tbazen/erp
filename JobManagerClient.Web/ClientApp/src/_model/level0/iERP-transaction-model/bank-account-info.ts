export class BankAccountInfo {
    public mainCsAccount: number = 0;
    public bankServiceChargeCsAccountID: number = 0;
    public bankName?: string;
    public branchName?: string;
    public bankAccountNumber?: string;
    public bankAccountName?: string;

    public get bankBranchAccount():string {
        return `${this.bankName} ${this.branchName}-${this.bankAccountNumber}`;
    }
}