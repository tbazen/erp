export class ItemCategory {
    public id: number=0;
    public code: string='';
    public pid: number = -1;
    public description: string ='';
    public expenseAccountPID: number = -1;

    public directCostAccountPID: number = -1;

    public prePaidExpenseAccountPID: number = -1;

    public salesAccountPID: number = -1;

    public unearnedRevenueAccountPID: number = -1;

    public finishedWorkAccountPID: number = -1;

    // finished services
    public generalInventoryAccountPID: number = -1;

    public finishedGoodsAccountPID: number = -1;

    public originalFixedAssetAccountPID: number = -1;

    public depreciationAccountPID: number = -1;

    // Expense account
    public accumulatedDepreciationAccountPID: number = -1;

    // Asset account
    public pendingOrderAccountPID: number = -1;

    public pendingDeliveryAcountPID: number = -1;

    // summary accounts follow
    public expenseSummaryAccountID: number = -1;

    public directCostSummaryAccountID: number = -1;

    public prePaidExpenseSummaryAccountID: number = -1;

    public salesSummaryAccountID: number = -1;

    public unearnedSummaryAccountID: number = -1;

    public finishedWorkSummaryAccountID: number = -1;

    // finished services
    public generalInventorySummaryAccountID: number = -1;

    public finishedGoodsSummaryAccountID: number = -1;
    public originalFixedAssetSummaryAccountID: number = -1;

    public depreciationSummaryAccountID: number = -1;

    // Expense account
    public accumulatedDepreciationSummaryAccountID: number = -1;

    // Asset account
    public pendingOrderSummaryAccountID: number = -1;

   
    public pendingDeliverySummaryAccountID: number = -1;

}