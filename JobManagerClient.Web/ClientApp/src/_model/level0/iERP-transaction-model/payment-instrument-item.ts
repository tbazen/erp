import {DEFAULT_DATE_TIME} from "../../../_helpers/date-util";


export class PaymentInstrumentType {
    public itemCode?: string;
    public name?: string;
    public isBankDocument: boolean = false;
    public isCurreny: boolean = false;
    public isToken:boolean = false;
    public isTransferFromAccount: boolean = false;
}


export class PaymentInstrumentItem{
    public despositToBankAccountID=-1;
    public instrumentTypeItemCode:string = '';
    public bankBranchID:number = 0;
    public accountNumber:string = '';
    public documentReference : string='';
    public documentDate:string = DEFAULT_DATE_TIME;
    public remark:string = '';
    public amount:number = 0;

    public constructor(value? : {instrumentCode: string,amount: number}){
        if(value){
            this.instrumentTypeItemCode = value.instrumentCode;
            this.amount = value.amount;
        }
    }
}


export class BankInfo {
    public id: number = 0;
    public name?: string;
}


export class BankBranchInfo {
    public id: number = 0;
    public bankID: number = 0;
    public name?: string;
}