import {GoodOrService, GoodType, ItemTaxStatus, ServiceType} from "../../view_model/ic-vm/transaction-item";

export interface IAccountingItem {
    coaName:string
    coaCode :string
}


export class TransactionItems implements IAccountingItem {
    coaName:string='';
    coaCode :string='';

    public code?: string;

    public name?: string;
    public description?: string;
    public fixedUnitPrice: number=0;
    public isDirectCost?: boolean;
    public taxStatus?: ItemTaxStatus;
    public goodOrService?: GoodOrService;
    public serviceType?: ServiceType;
    public expenseType: ExpenseType = ExpenseType.GeneralExpense;
    public inventoryType?: InventoryType;
    public depreciationType: number =0;
    public goodType?: GoodType;
    public measureUnitID: number = 1;
    public activated: boolean = true;
    public isInventoryItem: boolean = false;
    public isExpenseItem: boolean = false;
    public isSalesItem: boolean = false;
    public isFixedAssetItem: boolean = false;
    // either director cost or expense account
    public prePaidExpenseAccountID: number = -1;
    public salesAccountID: number = -1;
    public unearnedRevenueAccountID: number = -1;
    public inventoryAccountID: number = -1;
    public finishedGoodAccountID: number = -1;
    public finishedWorkAccountID: number = -1;
    public originalFixedAssetAccountID: number = -1;
    public depreciationAccountID: number = -1;
    public accumulatedDepreciationAccountID: number = -1;

    public pendingOrderAccountID: number = -1;
    public pendingDeliveryAcountID: number = -1;
    public expenseSummaryAccountID: number = -1;
    public prePaidExpenseSummaryAccountID: number = -1;
    public salesSummaryAccountID: number = -1;
    public unearnedSummaryAccountID: number = -1;
    public finishedWorkSummaryAccountID: number = -1;
    public originalFixedAssetSummaryAccountID: number = -1;
    public depreciationSummaryAccountID: number = -1;
    public accumulatedDepreciationSummaryAccountID: number = -1;

    public generalInventorySummaryAccountID: number = -1;
    public finishedGoodsSummaryAccountID: number = -1;
    public pendingOrderSummaryAccountID: number = -1;
    public pendingDeliverySummaryAccountID: number = -1;
    public categoryID: number = 0 ;
}

export class MeasureUnit {
    public id: number=0;
    public name: string='';
}
export class UnitCoversionFactor {
    public itemScope: string = "";
    public categoryScope: number = -1;
    public baseUnitID: number =0;
    public conversionUnitID: number=0;
    public conversionFactor: number=0;
}
export class UnitConvertor {
}

export enum ExpenseType {

    PurchaseExpense = 1,

    GeneralExpense = 2,

    UnclaimedExpense = 3,

    TaxAuthorityUnclaimable = 4,
}

export enum InventoryType {

    GeneralInventory = 0,

    FinishedGoods = 1,
}

export enum DepreciationType {

    Building = 0,

    IntangibleAssets = 1,

    ComputersAndSoftwareProductsRelated = 2,

    OtherBusinessAssets = 3,

    None = 4,
}
