import {RangeType} from "./range-type";

export class ProgressiveRateItem {
    public type?:RangeType;
    public v1:number = 0
    public v2: number = 0;
    public rate: number = 0;
}

export class ProgressiveRate {
    public items:ProgressiveRateItem[] = [];
}