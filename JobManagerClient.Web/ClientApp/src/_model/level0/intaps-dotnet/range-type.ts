export enum RangeType {
    LowerBound = 1,
    UpperBound = 2,
    UpperAndLowerBound = 3
}