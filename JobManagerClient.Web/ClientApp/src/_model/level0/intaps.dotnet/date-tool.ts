export const NullDateValue = new Date(1800, 1, 1);
export const NullDateValueString = NullDateValue.toLocaleDateString();