import {AgentActionData} from "../view_model/active-jobs-vm/job-status-history";
export interface  JobAppointment extends AgentActionData {
    active:boolean;
    appointmentCloseDate:string;
    appointmentDate:string;
}