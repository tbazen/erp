import {ItemCategory} from "../iERP-transaction-model/item-category";
import {TransactionItems} from "../iERP-transaction-model/transaction-items";

export class ItemsListItem {
    public isCategory: boolean = false;
    public category?: ItemCategory;
    public  titem: TransactionItems = new TransactionItems();
}