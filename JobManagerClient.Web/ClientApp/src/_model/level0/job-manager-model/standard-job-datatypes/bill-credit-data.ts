import { WorkFlowData } from './job-item-setting';
import {CreditScheme} from "../../subscriber-managment-type-library/credit-scheme";
import {CustomerBillDocument} from "../../subscriber-managment-type-library/bill";


export class BillCreditData extends WorkFlowData {
    public schedule : CreditScheme = new CreditScheme();
    public creditedBills :CustomerBillDocument[] = [];
}