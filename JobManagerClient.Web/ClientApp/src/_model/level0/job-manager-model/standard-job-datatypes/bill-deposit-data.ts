import { WorkFlowData } from './job-item-setting';
export class BillDepositData extends WorkFlowData
{
    public deposit : number = 0;
}