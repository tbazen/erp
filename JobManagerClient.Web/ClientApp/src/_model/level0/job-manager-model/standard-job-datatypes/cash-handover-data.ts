import { WorkFlowData } from "./job-item-setting";
import {PaymentInstrumentItem} from "../../iERP-transaction-model/payment-instrument-item";
export class CashHandoverData extends WorkFlowData
{
    public receiptNumber:string | null = null ;
    public sourceCashAccountID: number =0  ;
    public bankDespoit: boolean =false;
    public bankDespositReference:string | null = null;
    public bankDespositDate : string | null = null ;
    public destinationAccountID:number = 0;
    public reportHTML:string | null = null;
    public instruments:PaymentInstrumentItem[] = [];
    public delta: number = 0;
    public handoverDocumentID:number=-1;
    public get total():number
    {
        let ret:number = 0;
        this.instruments.forEach(item=>{
            ret+=item.amount
        })
        return ret;
    }
}