import { WorkFlowData } from './job-item-setting';
import {CreditScheme} from "../../subscriber-managment-type-library/credit-scheme";
export class CloseCreditSchemeData extends WorkFlowData {
    public schemeIDs : number[] = [];
    public  schemes? : CreditScheme[];
    public billed? :number[];
}  