import { WorkFlowData } from './job-item-setting';
import { MeterData } from '../../../view_model/mn-vm/meter-data';
export class ConntectionMaintenanceData extends WorkFlowData {
    public connectionID:number = 0;
    public changeMeter : boolean = false;
    public meterData?: MeterData;
}