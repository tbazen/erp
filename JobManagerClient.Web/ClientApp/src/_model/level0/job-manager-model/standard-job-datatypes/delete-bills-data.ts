import { WorkFlowData } from "./job-item-setting";
import {CustomerBillDocument} from "../../subscriber-managment-type-library/bill";

export class DeleteBillsData extends WorkFlowData {
    public deletedBills: CustomerBillDocument[]=[];
}