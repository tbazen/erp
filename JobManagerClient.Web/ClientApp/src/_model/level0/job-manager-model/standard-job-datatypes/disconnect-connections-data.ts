import { WorkFlowData } from "./job-item-setting";
import { VersionedID } from "../../../view_model/mn-vm/subscriber-search-result";

export class BatchDisconnectData extends WorkFlowData
{
    public discontinue: boolean =false;
    public connections: VersionedID[] = [];
}

export class BatchReconnectData extends BatchDisconnectData
{
}

export class DeleteBillData extends WorkFlowData
{
    public billDocumentID : number = -1;
}