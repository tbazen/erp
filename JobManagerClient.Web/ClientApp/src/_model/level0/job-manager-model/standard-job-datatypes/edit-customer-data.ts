import { Subscriber } from "../../../view_model/mn-vm/subscriber-search-result";
import { WorkFlowData } from "./job-item-setting";
import {DiffObject} from "../../application-server-library/generic-diff-object";
import {Subscription} from "../../subscriber-managment-type-library/subscription";

export class EditCustomerData extends WorkFlowData {
    public oldVersion?: Subscriber;
    public customerDiff: DiffObject<Subscriber> | null =null;
    public oldVersions : Subscription []=[];
    public connections: DiffObject<Subscription>[]=[];
}