import { WorkFlowData } from "./job-item-setting";
import {BillItem, CustomerBillRecord} from "../../subscriber-managment-type-library/bill";

export class ExemptBillItemsData extends WorkFlowData {
    public exemptedBill: CustomerBillRecord = new CustomerBillRecord();
    public originalItems: BillItem[] = [];
    public exemptedItems: number[] = [];
    public exemptedAmounts: number[] = [];
}