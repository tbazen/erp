import { WorkFlowData } from "./job-item-setting";
import {Subscription} from "../../subscriber-managment-type-library/subscription";

export class NewLineData extends WorkFlowData
{
    constructor(subscription?:Subscription){
        super()
        if(subscription)
            this.connectionData = subscription;
    }
    public connectionData : Subscription = new Subscription();
}
export class GeneralSellsData extends WorkFlowData
{

}