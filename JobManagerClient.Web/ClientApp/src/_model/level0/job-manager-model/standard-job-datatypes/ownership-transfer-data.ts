import { WorkFlowData } from "./job-item-setting";
import { Subscriber } from '../../../view_model/mn-vm/subscriber-search-result';

export class OwnerShipTransferData extends WorkFlowData
{
    public connectionID : number = -1;
    public newConnectionID = -1;
    public oldCustomer?: Subscriber ;
}