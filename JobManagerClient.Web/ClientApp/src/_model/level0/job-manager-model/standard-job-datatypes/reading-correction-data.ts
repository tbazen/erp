import { WorkFlowData } from "./job-item-setting";
import {WaterBillDocument} from "../../subscriber-managment-type-library/bill";
import {BWFMeterReading} from "../../subscriber-managment-type-library/bwf-meter-reading";

export class ReadingCorrectionData extends WorkFlowData {
    public customerID: number = 0;
    public oldBill?: WaterBillDocument;
    public newBill?: WaterBillDocument;
    public oldReading?: BWFMeterReading;
    public newReading?: BWFMeterReading;
    public averageReadings: BWFMeterReading[] = [];
}