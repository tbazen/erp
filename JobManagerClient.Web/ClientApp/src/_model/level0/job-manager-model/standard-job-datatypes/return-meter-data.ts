import { WorkFlowData } from "./job-item-setting";
import { MaterialOperationalStatus } from '../../../../_enum/mn-enum/material-operational-status';

export class ReturnMeterData extends WorkFlowData
{
    public connectionID: number  =-1;
    public opStatus?: MaterialOperationalStatus;
    public permanent: boolean =false;
    public remark?: string;
}