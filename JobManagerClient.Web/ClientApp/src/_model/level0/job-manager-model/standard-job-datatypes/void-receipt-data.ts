import { WorkFlowData } from "./job-item-setting";

export class VoidReceiptData extends WorkFlowData
{
    public receitDocumentID: number =-1;
    public receiptHTML?: string ;
}