import {AccountDocument} from "../accounting-type-library/account-document";
import {DocumentTypedReference} from "../accounting-type-library/doc-serial";
import {CashHandoverData} from "../job-manager-model/standard-job-datatypes/cash-handover-data";

export class CashHandoverDocument extends AccountDocument{
    public voucher?: DocumentTypedReference;
    public previousHandoverDocumentID: number =-1;
    public data?: CashHandoverData;
}