import {getLocalDateString} from "../../../_helpers/date-util";
import {JobBOMItem} from "./job-bom-item";

export class JobBillOfMaterial {
    public id:number = -1;
    public jobID:number = 0;
    public items: JobBOMItem[] = [];
    public surveyCollectionTime:string = getLocalDateString();
    public analysisTime = getLocalDateString();
    public note? :string;
    public invoiceNo: number = -1;
    public invoiceDate: string =  getLocalDateString();
    public preparedBy?: string;
    public storeIssueID: number = -1;
}