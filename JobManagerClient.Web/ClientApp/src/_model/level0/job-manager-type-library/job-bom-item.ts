export class JobBOMItem {
    public itemID?: string;
    public inSourced?: boolean;
    public quantity: number = 0;
    public referenceUnitPrice: number =-1;
    public unitPrice: number = 0;
    public description?: string;
    public calculated: boolean = false;
    public tag?: string;
}