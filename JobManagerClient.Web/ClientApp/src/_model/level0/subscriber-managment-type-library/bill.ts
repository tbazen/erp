import {Subscriber} from "../../view_model/mn-vm/subscriber-search-result";
import {AccountDocument} from "../accounting-type-library/account-document";
import {DocumentTypedReference} from "../accounting-type-library/doc-serial";
import {getLocalDateString} from "../../../_helpers/date-util";
import {Subscription} from "./subscription";
import {BillPeriod} from "./bill-period";
import {BWFMeterReading} from "./bwf-meter-reading";
import {PaymentInstrumentItem} from "../iERP-transaction-model/payment-instrument-item";


export enum BillItemAccounting {
    Invoice,
    Advance,
    Cash
}

export class BillItem {
    public customerBillID: number = -1;
    public itemTypeID: number = 0;
    public incomeAccountID: number = -1;
    public description: string | null = null;
    public hasUnitPrice: boolean = false;
    public quantity: number = 0;
    public unit: string = "";
    public unitPrice: number = 0;
    public price: number = 0;
    public accounting: BillItemAccounting = BillItemAccounting.Invoice;
    public settledFromDepositAmount: number = 0;

    public get fullySettledByDebosit(): boolean {
        return this.settledFromDepositAmount === this.price;
    }

    public get partiallySettledByDebosit(): boolean {
        return this.settledFromDepositAmount > 0;
    }

    public get netPayment(): number {
        return this.price - this.settledFromDepositAmount;
    }
}

export abstract class CustomerBillDocument extends AccountDocument {
    public invoiceNumber: DocumentTypedReference = new DocumentTypedReference();
    public customer?: Subscriber;
    public items: BillItem[] = []
    public draft: boolean = true;
    public summerizeTransaction: boolean = false;
    public billBatchID: number = -1;
    //exemption
    public exemptedItems: number[] = new Array(0);
    public exemptedAmounts: number[] = new Array(0);
    public itemsLessExemption: BillItem[] = []

    public get total(): number {
        let ret: number = 0;
        this.items.forEach(item => {
            ret += item.price
        });
        this.exemptedItems.forEach(amount => {
            ret -= amount;
        })
        return ret;
    }
}


export class CustomerBillRecord {
    public id: number = -1;
    public billDate: string = getLocalDateString();
    public billDocumentTypeID: number = -1;
    public customerID: number = -1;
    public connectionID: number = -1;
    public periodID: number = -1;
    public postDate: string = getLocalDateString();
    public paymentDocumentID: number = -1;
    public paymentDate: string = getLocalDateString();
    public paymentDiffered: boolean = false;
    public draft: boolean = true;

    public get isPayedOrDiffered(): boolean {
        return this.paymentDocumentID != -1 || this.paymentDiffered;
    }
}


export abstract class PeriodicBill extends CustomerBillDocument {
    public period?: BillPeriod;
}

export class WaterBillDocument extends PeriodicBill {
    public subscription?: Subscription;
    public reading?: BWFMeterReading;
    public waterBillItems: BillItem[] = [];

    public get items(): BillItem[] {
        return this.waterBillItems;
    }

}

export class CustomerPaymentReceipt extends AccountDocument {
    public receiptNumber: DocumentTypedReference | null = null;
    public offline: boolean = false;
    public mobile: boolean = false;
    public notifyCustomer: boolean = false;

    public assetAccountID: number = 0;
    public paymentInstruments: PaymentInstrumentItem[] = [];
    public bankDepsitDocuments: number[] = [];

    public customer: Subscriber | null = null;
    public settledBills: number[] = [];
    public billBatchID: number = -1;
    public summerizeTransaction: boolean = false;
    public billItems: BillItem[] | null = null;

    public offlineBills: number[] = [];

    public get totalAmount(): number {
        if (this.billItems == null) {
            return -1;
        }
        let ret: number = 0;
        this.billItems.forEach(item => { ret += item.netPayment;})
        return ret;
    }

    public get totalInstrument(): number {
        let ret: number = 0;
        this.paymentInstruments.forEach(instrument => {ret += instrument.amount})
        return ret;
    }
}



