import {getLocalDateString} from "../../../_helpers/date-util";
import {BWFStatus} from "./bwf-status";
import {MeterReadingProblem, MeterReadingType, ReadingMethod} from "./mater-reading-type";

export enum ReadingExtraInfo {
    None = 0,
    Coordinate = 1,
    ReadingDate = 2,
    Remark = 4,
}

export class BWFMeterReading {

    public readingBlockID: number = -1;
    public subscriptionID: number = -1;

    public averageMonths: number = 0;
    public billDocumentID: number = -1;
    public bwfStatus: BWFStatus = BWFStatus.Unread;
    public consumption: number = 0.0;
    public entryDate: string = getLocalDateString();
    public orderN: number = -1;
    public periodID: number = 0;
    public reading: number = 0.0;
    public readingType: MeterReadingType = MeterReadingType.Normal;
    public readingProblem: MeterReadingProblem = MeterReadingProblem.NoProblem;

    public extraInfo: ReadingExtraInfo = ReadingExtraInfo.None;
    public readingRemark: string = "";
    public readingTime: string = getLocalDateString();
    public readingX: number = 0;
    public readingY: number = 0;
    public method: ReadingMethod = ReadingMethod.ReaderPaper;

    public get readingProblemString():string {
        switch (this.readingProblem)
        {
            case MeterReadingProblem.NoProblem:
                return "No Problem";
            case MeterReadingProblem.VisistedNoAttendant:
                return "Nobody Home";
            case MeterReadingProblem.VisistedNotCooperative:
                return "Household Not Cooperative";
            case MeterReadingProblem.VisistedDefectiveMeter:
                return "Defective Meter";
            case MeterReadingProblem.VisistedMeterPhysicallyInaccessible:
                return "Inaccessible Meter";
            case MeterReadingProblem.Other:
                return "General Problem";
            default:
                return "";
        }
    }
    public get hasCoordinate():boolean{ return this.readingX != 0 || this.readingY != 0; }
}


export class MeterReadingFilter {
    public periods:number[] | null = null;
    public customerTypes: number[] | null = null;
    public kebeles: number[] | null = null;
    public readingStatus:BWFStatus[] | null = null;
    public meterReaders:number[] | null = null;
    public subscriptions: number[] | null = null;
}