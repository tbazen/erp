export class CreditScheme {
    public id:number= 0;
    public customerID:number = 0;
    public creditedAmount:number = 0;
    public paymentAmount:number = 0;
    public issueDate:string= '1/1/0001 12:00:00 AM';
    public startPeriodID:number=-1;
    public active:boolean = true;
}