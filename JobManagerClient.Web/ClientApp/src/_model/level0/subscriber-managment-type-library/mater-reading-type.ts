export enum MeterReadingType {
    Normal=0,
    MeterReset=1,
    Average=4,
}
export enum MeterReadingProblem {
    NoProblem=0,
    VisistedNoAttendant=1,
    VisistedNotCooperative=2,
    VisistedDefectiveMeter=3,
    VisistedMeterPhysicallyInaccessible=4,
    Other=5
}
export enum ReadingMethod {
    Unknown=0,
    ReaderPaper=1,
    ReaderMobileOffline=2,
    ReaderMobileOnline=3,
    CustomerProvidedPaper=4,
    CustomerProvidedSMS=5,
    CustomerProvidedWeb=6,
    EstimationMethodAverage=7,
    EstimationMethodManual=8,
    EstimationMethodScaledAverage=9,
}