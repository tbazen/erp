import {Employee} from "../../view_model/IEmployee";

export class MeterReaderEmployee {
    public employeeID: number = 0;
    public periodID: number = 0;
    public emp?: Employee;

}