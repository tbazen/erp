import {CollectionMethod} from "../../../_enum/mn-enum/internal-services-enum/cash-handover-enum/collection-method";
import {PaymentCenterSerialBatch} from "../../view_model/mn-vm/internal-service-vm/cash-handover/payment-center";

export class PaymentCenter {
    public id:number = 0;
    public casheir: number = 0;
    public casherAccountID: number = 0;
    public centerName: string | null = null;
    public summaryAccountID: number = 0;
    public userName:string | null =null;
    public storeCostCenterID: number =-1;
    public collectionMethod: CollectionMethod = CollectionMethod.Online;
    public serialBatches: PaymentCenterSerialBatch[] =[];
}