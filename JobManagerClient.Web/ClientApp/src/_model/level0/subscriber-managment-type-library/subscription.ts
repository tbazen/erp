import {DataTimeBinding} from "../../../_enum/mn-enum/datetime-binding";
import {SubscriptionType} from "../../../_enum/mn-enum/subscription-type";
import {SubscriptionStatus} from "../../../_enum/mn-enum/subscription-status";
import getCurrentTicks from "../../../_helpers/date-util";
import {Subscriber, VersionedID} from "../../view_model/mn-vm/subscriber-search-result";
import {ConnectionType} from "../../../_enum/mn-enum/connection-type";
import {SupplyCondition} from "../../../_enum/mn-enum/supply-condition";
import {DataStatus} from "../../../_enum/mn-enum/data-status";
import {MeterPositioning} from "../../../_enum/mn-enum/meter-positioning";
import {MaterialOperationalStatus} from "../../../_enum/mn-enum/material-operational-status";
import {MeterData} from "../../view_model/mn-vm/meter-data";

export class Subscription{
    public id: number = -1;
    public pressureZone: number = -1;
    public dma: number = -1;
    public householdSize: number = 5;
    public opStatus?:MaterialOperationalStatus;
    public modelNo?: string;
    public serialNo?: string;
    public itemCode?: string;
    public remark: string = "";
    public houseConnectionY: number = 0;
    public houseConnectionX: number = 0;
    public waterMeterY: number = 0;
    public waterMeterX: number = 0;
    public dataStatus: DataStatus = DataStatus.Unknown;
    public supplyCondition: SupplyCondition = SupplyCondition.Unknown;
    public meterPosition: MeterPositioning = MeterPositioning.Unknown;
    public parcelNo: string = "";
    public ticksFrom: number = getCurrentTicks();
    public ticksTo: number = -1;
    public timeBinding: DataTimeBinding = DataTimeBinding.LowerBound;
    public subscriberID: number = -1;
    public connectionType: ConnectionType = ConnectionType.Unknown;
    public subscriptionType: SubscriptionType = SubscriptionType.Tap;
    public subscriptionStatus: SubscriptionStatus = SubscriptionStatus.Unknown;
    public contractNo: string | null = null;
    public address: string | null = null;
    public landCertificateNo: string | null = null;
    public subscriber?: Subscriber;
    public initialMeterReading: number = 0;
    public prePaid: boolean = false;
    public kebele: number = -1;

    public objectIDVal: number = 0;
    public versionedID: VersionedID = { id:-1,version :-1  };


    public set meterData(value : MeterData){
        this.itemCode = value.itemCode;
        this.serialNo = value.serialNo;
        this.modelNo = value.modelNo;
        this.opStatus = value.opStatus;
        this.initialMeterReading = value.initialMeterReading;
    }
    public get meterData():MeterData {
       return new MeterData({
           itemCode:this.itemCode || '',
           serialNo:this.serialNo || '',
           modelNo:this.modelNo || '',
           opStatus:this.opStatus || MaterialOperationalStatus.Working
       })
    }

    public get hasUpperBound(): boolean{ return this.ticksTo > 0}
    public get hasCoordinate(): boolean{ return this.waterMeterX != 0 || this.waterMeterY != 0 }
}

