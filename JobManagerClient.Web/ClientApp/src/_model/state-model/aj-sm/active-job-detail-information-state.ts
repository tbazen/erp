import {IStateMachineBase} from "../state-machine-base";
import {Subscriber} from "../../view_model/mn-vm/subscriber-search-result";
import {JobData} from "../../../_services/job.manager.service";
import {JobStatusType} from "../../view_model/active-jobs-vm/job-status-type";
import {JobTypeInfo} from "../../level0/job-type-info";
import {JobAppointment} from "../../level0/job-appointment";
import {JobStatusHistory} from "../../view_model/active-jobs-vm/job-status-history";

export interface IActiveJobDetailPayload{
    customer : Subscriber,
    job: JobData,
    nextStatus:number[],
    status : JobStatusType,
    jobType: JobTypeInfo,
    appointments: JobAppointment[],
    previousStatus: JobStatusHistory[]
}

export interface ActiveJobDetailInformationState extends IStateMachineBase{
    payload? : IActiveJobDetailPayload
}