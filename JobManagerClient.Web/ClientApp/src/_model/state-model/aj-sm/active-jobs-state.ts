import {IStateMachineBase} from "../state-machine-base";
import {JobData} from "../../../_services/job.manager.service";
import {Subscriber} from "../../view_model/mn-vm/subscriber-search-result";
import {JobStatusType} from "../../view_model/active-jobs-vm/job-status-type";

export interface IActiveJobPayload{
    job : JobData
    customer: Subscriber
    jobStatus: JobStatusType
}

export interface ActiveJobsState extends  IStateMachineBase{
    payload: IActiveJobPayload[]
}