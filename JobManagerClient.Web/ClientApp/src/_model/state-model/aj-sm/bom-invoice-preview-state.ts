import {IStateMachineBase} from "../state-machine-base";

export interface BomInvoicePreviewState extends IStateMachineBase{
    previewHtml : string
}