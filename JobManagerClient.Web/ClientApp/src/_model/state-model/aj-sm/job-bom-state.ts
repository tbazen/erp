import { IStateMachineBase } from '../state-machine-base';
import { JobBillOfMaterial } from '../../view_model/active-jobs-vm/job-bill-of-material';


export interface IJobBOMPayload{
    billOfMaterial: JobBillOfMaterial
    isPaid: boolean
    isPaymentStatus: boolean
    isStoreIssue: boolean
    total: number
    receiptNo:string
}

export interface JobBomState extends IStateMachineBase{
    payload? : IJobBOMPayload[]
}