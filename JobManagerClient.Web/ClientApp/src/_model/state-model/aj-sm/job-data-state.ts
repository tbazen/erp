import {IStateMachineBase} from "../state-machine-base";
import {JobData} from "../../../_services/job.manager.service";

export interface JobDataState extends  IStateMachineBase{
    jobs : JobData[]
}