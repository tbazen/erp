import { IStateMachineBase } from '../state-machine-base';
export interface JobHistory{
    date: string ;
    worker: string ;
    stage : string;
    forwardTo: string;
    note: string;
    duration:string;   
}


export interface JobHistoryState extends IStateMachineBase { 
    payload:JobHistory[]
}