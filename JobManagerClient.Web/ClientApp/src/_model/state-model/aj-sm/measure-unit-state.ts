import {IStateMachineBase} from "../state-machine-base";
import {MeasureUnit} from "../../level0/iERP-transaction-model/transaction-items";

export interface MeasureUnitState extends IStateMachineBase{
    payload : MeasureUnit[]
}