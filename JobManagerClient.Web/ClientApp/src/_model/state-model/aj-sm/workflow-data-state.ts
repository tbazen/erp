import {IStateMachineBase} from "../state-machine-base";

export interface WorkflowDataState extends IStateMachineBase {
    payload : any
}