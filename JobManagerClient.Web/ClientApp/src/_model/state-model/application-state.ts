import { AuthenticationState } from './auth-state'
import {EmployeeSearchState} from "./hr-sm/employee-search-state"
import {OrganizationalUnitState} from "./hr-sm/organizational-unit-state"
import {BreadcrumdState} from "./breadcrumd-state"
import {ReportCategoryState} from "./report-category-state"
import {ReportDefinitionState} from "./report-definition-state"
import {ItemCategoryState} from "./ic-sm/item-catagory-state"
import {TransactionItemsState} from "./ic-sm/transaction-items-state"
import {KebeleState} from "./mn-sm/kebele-state"
import {DmzState} from "./mn-sm/dmz-state"
import {PressureZoneState} from "./mn-sm/pressure-zone-state"
import {AccountBaseState} from "./ic-sm/account-base-state"
import { SubscriberSearchResultState } from './mn-sm/subscriber-search-result-state'
import { DrawerModalState } from './drawer-dialog-state'
import { GlobalSearchBarState } from './global-search-bar'
import {SingleSubscriberState} from './mn-sm/single-subscriber-state'
import {PaymentCenterState} from "./mn-sm/internal-service-sm/payment-center-state";
import {JobDataState} from "./aj-sm/job-data-state";
import {JobRuleStateModel} from "./job-rule-sm/job-rule-state-model";
import {JobWorkerState} from "./mn-sm/job-worker-state";
import { JobHistoryState } from './aj-sm/job-history-state';
import {JobConfigurationState} from "./job-configuration-sm/job-configuration-state";
import {MeasureUnitState} from "./aj-sm/measure-unit-state";
import {ActiveJobDetailInformationState} from "./aj-sm/active-job-detail-information-state";
import {WorkflowDataState} from "./aj-sm/workflow-data-state";
import { JobBomState } from './aj-sm/job-bom-state';
import {BomInvoicePreviewState} from "./aj-sm/bom-invoice-preview-state";
import {CreditSchemeState} from "./mn-sm/credit-scheme-state";
import {BillPeriodState} from "./mn-sm/bill-period-state";
import {BillDocumentsState} from "./mn-sm/bill-documents-state";
import {BWFMeterReadingState} from "./mn-sm/bwf-meter-reading-state";
import {BWFMeterReadingListState} from "./mn-sm/bwf-meter-reading-list-state";
import {EmployeeState} from "./mn-sm/employee-state";
import {SubscriptionState} from "./mn-sm/subscription-state";
import {ActiveJobsState} from "./aj-sm/active-jobs-state";
import {CustomerProfileState} from "./mn-sm/customer-profile-state";
import {ReportState} from "./r-sm/report-state";
import {ReportTypeState} from "./r-sm/report-types-state";
import {ReportHTMLState} from "./r-sm/report-html-state";
import {MeterReaderEmployeeState} from "./r-sm/meter-reader-employee-state";

export interface ApplicationState{
    auth : AuthenticationState,

    //UI state
    breadcrumb:BreadcrumdState,
    reportCategory:ReportCategoryState,
    reportDefinitions : ReportDefinitionState,
    drawerModalState : DrawerModalState,
    globalSearchbarState:GlobalSearchBarState,

    //application logic state
    employeeSearch : EmployeeSearchState
    orgUnits : OrganizationalUnitState
    item_category_state :ItemCategoryState
    transaction_items_state:TransactionItemsState


    //Main Module
    //Customer List
    kebele_state : KebeleState
    dmz_state : DmzState
    pressure_zone_state : PressureZoneState
    subscription_search_result_state:SubscriberSearchResultState
    subscriber_state : SingleSubscriberState
    worker_state : JobWorkerState

    //Item Configuration
    accountBaseState : AccountBaseState

    //Internal services  state

    //Cash handover state
    payment_center_state : PaymentCenterState

    //Active Jobs State
    job_data_state : JobDataState
    /**
     * job rule clients state
     */
    job_rule : JobRuleStateModel
    job_history: JobHistoryState
    configuration_state : JobConfigurationState

    measure_unit_state : MeasureUnitState

    active_job_detail_information_state : ActiveJobDetailInformationState

    workflow_data_state : WorkflowDataState

    job_bom_state : JobBomState
    bom_invoice_preview: BomInvoicePreviewState
    credit_scheme_state: CreditSchemeState
    bill_period_state: BillPeriodState
    bill_document_state: BillDocumentsState

    bwf_meter_reading_state: BWFMeterReadingState
    bwf_meter_reading_list_state: BWFMeterReadingListState
    employee_state:EmployeeState
    subscription_state: SubscriptionState
    active_jobs_state: ActiveJobsState
    customer_profile_state: CustomerProfileState
    report_state: ReportState
    report_type_state: ReportTypeState,
    report_html_state: ReportHTMLState,
    mater_reader_employee_state: MeterReaderEmployeeState
}