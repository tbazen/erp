export interface AuthenticationState{
    isCheckingSession: boolean
    loading : boolean
    authenticated : boolean 
    error: boolean
    message : string
    sessionId: string
    userName: string
}