export interface IBreadcrumb{
    path:string,
    breadcrumbName:string
}

export interface BreadcrumdState{
    paths:IBreadcrumb[]
}