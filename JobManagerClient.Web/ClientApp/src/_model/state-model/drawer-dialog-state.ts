import { ReactChildren } from "react";

export interface DrawerModalState{
    visible :boolean
    component:JSX.Element
}