export interface GlobalSearchBarState{
    visible:boolean
    placeholder:string
    changeHandler:(event:any)=>void
}