import {SearchEmployeeOut} from "../../view_model/employee-search-par";
import {IStateMachineBase} from "../state-machine-base";

export interface EmployeeSearchState extends IStateMachineBase{
    payload : SearchEmployeeOut | null
}
