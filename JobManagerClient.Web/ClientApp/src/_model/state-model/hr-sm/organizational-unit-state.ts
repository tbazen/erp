import {IStateMachineBase} from "../state-machine-base";
import {OrgUnit} from "../../view_model/hr-vm/orginanizational-unit";

export interface OrganizationalUnitState extends IStateMachineBase{
    payload : OrgUnit[]
}
