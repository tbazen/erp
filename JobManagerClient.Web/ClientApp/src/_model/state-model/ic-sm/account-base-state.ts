import {IStateMachineBase} from "../state-machine-base";
import {Account} from "../../view_model/ic-vm/account-base";

export interface AccountBaseState extends IStateMachineBase{
    accounts:Account[]
}