import {IStateMachineBase} from "../state-machine-base";
import {ItemCategory} from "../../view_model/ic-vm/item-category";

export interface ItemCategoryState extends IStateMachineBase{
    item_categories:ItemCategory[]
}