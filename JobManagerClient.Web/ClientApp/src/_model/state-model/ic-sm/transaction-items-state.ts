import {IStateMachineBase} from "../state-machine-base";
import { TransactionItems } from "../../view_model/ic-vm/transaction-item";

export interface TransactionItemsState extends IStateMachineBase{
    transactions_items:TransactionItems[]
}