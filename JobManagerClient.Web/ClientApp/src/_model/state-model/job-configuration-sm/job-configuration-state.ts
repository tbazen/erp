import {IStateMachineBase} from "../state-machine-base";

export interface JobConfigurationState extends IStateMachineBase{
    payload : any
}