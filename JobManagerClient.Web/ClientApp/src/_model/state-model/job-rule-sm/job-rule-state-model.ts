import {IStateMachineBase} from "../state-machine-base";
import {IClientHandler} from "../../job-rule-model/client-handler";

export interface JobRuleStateModel extends IStateMachineBase {
    clientHandlers : IClientHandler []
}