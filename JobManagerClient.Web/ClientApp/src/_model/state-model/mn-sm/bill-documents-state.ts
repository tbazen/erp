import {IStateMachineBase} from "../state-machine-base";
import {CustomerBillDocument} from "../../level0/subscriber-managment-type-library/bill";
import { DocumentType } from "../../level0/accounting-type-library/data-structure";
import {BillPeriod} from "../../level0/subscriber-managment-type-library/bill-period";

export interface IBillDocumentPayload{
    billDocument: CustomerBillDocument
    documentType: DocumentType
    billPeriod?: BillPeriod
}
export interface BillDocumentsState extends IStateMachineBase {
    payload : IBillDocumentPayload[]
}