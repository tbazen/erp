import {BillPeriod} from "../../level0/subscriber-managment-type-library/bill-period";
import {IStateMachineBase} from "../state-machine-base";

export interface BillPeriodState extends IStateMachineBase {
    payload : BillPeriod[]
}