import {IStateMachineBase} from "../state-machine-base";
import {IBWFMeterReadingPayload} from "./bwf-meter-reading-state";

export interface BWFMeterReadingListState extends IStateMachineBase{
    payload: IBWFMeterReadingPayload[]
}