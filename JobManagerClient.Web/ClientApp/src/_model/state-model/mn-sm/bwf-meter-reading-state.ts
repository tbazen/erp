import {IStateMachineBase} from "../state-machine-base";
import {BillPeriod} from "../../level0/subscriber-managment-type-library/bill-period";
import {BWFMeterReading} from "../../level0/subscriber-managment-type-library/bwf-meter-reading";

export interface IBWFMeterReadingPayload{
    period: BillPeriod,
    reading: BWFMeterReading
}
export interface  BWFMeterReadingState extends IStateMachineBase {
    payload?: IBWFMeterReadingPayload
}