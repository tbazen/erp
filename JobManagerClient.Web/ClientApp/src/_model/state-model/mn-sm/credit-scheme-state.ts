import {IStateMachineBase} from "../state-machine-base";
import {GetCustomerCreditSchemesOut} from "../../../_services/subscribermanagment.service";

export interface CreditSchemeState extends IStateMachineBase{
    payload : GetCustomerCreditSchemesOut
}