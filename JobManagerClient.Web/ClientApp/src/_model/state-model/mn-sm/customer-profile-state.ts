import {IStateMachineBase} from "../state-machine-base";
import {Subscriber} from "../../view_model/mn-vm/subscriber-search-result";
import {Subscription} from "../../level0/subscriber-managment-type-library/subscription";
import {CustomerBillDocument} from "../../level0/subscriber-managment-type-library/bill";
import {CreditScheme} from "../../level0/subscriber-managment-type-library/credit-scheme";
import {JobData} from "../../../_services/job.manager.service";


export interface CustomerProfilePayload{
    customer:  Subscriber,
    connections: Subscription[],
    bills: CustomerBillDocument[],
    schemes: CreditScheme[],
    settlement: number[],
    jobs: JobData[]
}


export interface CustomerProfileState extends IStateMachineBase {
    payload: CustomerProfilePayload | undefined
}