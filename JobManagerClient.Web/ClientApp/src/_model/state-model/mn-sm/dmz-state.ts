import {IStateMachineBase} from "../state-machine-base";
import {DistrictMeteringZone} from "../../view_model/mn-vm/district-metering-zone";

export interface DmzState extends IStateMachineBase{
    dmzs:DistrictMeteringZone[]
}