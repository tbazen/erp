import {IStateMachineBase} from "../state-machine-base";
import {Employee} from "../../view_model/IEmployee";

export interface EmployeeState extends IStateMachineBase{
    payload:Employee[]
}