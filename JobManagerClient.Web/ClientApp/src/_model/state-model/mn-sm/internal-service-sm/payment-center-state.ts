import {IStateMachineBase} from "../../state-machine-base";
import {PaymentCenter} from "../../../view_model/mn-vm/internal-service-vm/cash-handover/payment-center";

export interface PaymentCenterState extends IStateMachineBase{
    payment_centers : PaymentCenter[]
}