import {IStateMachineBase} from "../state-machine-base";
import {JobWorker} from "../../view_model/mn-vm/internal-service-vm/job-worker";

export interface JobWorkerState extends IStateMachineBase{
    workers : JobWorker[]
}