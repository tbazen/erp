import {IStateMachineBase} from "../state-machine-base";
import {Kebele} from "../../view_model/mn-vm/kebele";

export interface KebeleState extends IStateMachineBase{
    kebeles:Kebele[]
}