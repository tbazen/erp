import {IStateMachineBase} from "../state-machine-base";
import {PressureZone} from "../../view_model/mn-vm/pressure-zone";

export interface PressureZoneState extends IStateMachineBase{
    pressure_zones :PressureZone[]
}