import { IStateMachineBase } from '../state-machine-base';
import { GetSubscriptions2Out } from './subscriber-search-result-state';
export interface SingleSubscriberState extends IStateMachineBase{
    payload : GetSubscriptions2Out | undefined
}