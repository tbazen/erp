import { IStateMachineBase } from '../state-machine-base';
import { SubscriberSearchResult } from '../../view_model/mn-vm/subscriber-search-result';

export interface GetSubscriptions2Out {
    nRecord: number;
    _ret: SubscriberSearchResult[];
}
export interface SubscriberSearchResultState extends IStateMachineBase{
    payload:GetSubscriptions2Out
}