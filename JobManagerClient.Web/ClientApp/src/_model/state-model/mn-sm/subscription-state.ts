import {IStateMachineBase} from "../state-machine-base";
import {Subscription} from "../../level0/subscriber-managment-type-library/subscription";

export interface SubscriptionState extends IStateMachineBase{
    payload : Subscription[]
}