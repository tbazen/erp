import {IStateMachineBase} from "../state-machine-base";
import {MeterReaderEmployee} from "../../level0/subscriber-managment-type-library/meter-reader-employee";

export interface MeterReaderEmployeeState extends IStateMachineBase{
    payload: MeterReaderEmployee[]
}