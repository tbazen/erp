import {IStateMachineBase} from "../state-machine-base";
import {EData} from "../../level0/formula-evaluator/edata";

export interface ReportHTMLStatePayload{
    htmlRenderString: string
    htmlCodeString: string
    edata?: EData
}
export interface ReportHTMLState extends  IStateMachineBase{
    payload?: ReportHTMLStatePayload
}