import {IStateMachineBase} from "../state-machine-base";
import {FunctionDocumentation} from "../../level0/formula-evaluator/function-documentation";
import {ReportDefination} from "../../level0/accounting-type-library/ehtml";

export interface ReportStatePayload{
    definition: ReportDefination,
    functionDocumentations: FunctionDocumentation[]
}

export interface ReportState extends IStateMachineBase{
    payload?: ReportStatePayload
}