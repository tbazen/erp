import {IStateMachineBase} from "../state-machine-base";
import {ReportType} from "../../level0/accounting-type-library/ehtml";

export interface ReportTypeState extends IStateMachineBase{
    reportTypes: ReportType[]
}