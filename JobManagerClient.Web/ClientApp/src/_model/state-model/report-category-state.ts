import {IStateMachineBase} from "./state-machine-base";
import {ReportCategory} from "../view_model/report-category";

export interface ReportCategoryState extends IStateMachineBase{
    categories : ReportCategory[]
}