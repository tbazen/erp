import {IStateMachineBase} from "./state-machine-base";
import {ReportDefination} from "../level0/accounting-type-library/ehtml";

export interface ReportDefinitionState extends IStateMachineBase{
    definitions : ReportDefination[]
}