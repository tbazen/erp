export interface IStateMachineBase{
    loading : boolean
    error : boolean
    message : string
}