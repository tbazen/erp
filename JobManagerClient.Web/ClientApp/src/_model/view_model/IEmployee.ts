import {CostSharingStatus} from "../../_enum/cost-sharing-status";
import {SalaryKind} from "../../_enum/salary-kind";
import {Sex} from "../../_enum/sex";
import {EmployeeRoll} from "../../_enum/employee-roll";
import {EmployeeStatus} from "../../_enum/employee-status";
import {EmploymentType} from "../../_enum/employment-type";

export interface Employee {
    id: number;
    transportAllowance: number;
    medicalBenefit: number;
    totalCostSharingDebt: number;
    costSharingPayableAmount: number;
    costSharingStatus: CostSharingStatus;
    costCenterID: number;
    tin: string;
    salaryKind: SalaryKind;
    accountAsCost: boolean;
    loginName: string;
    title: string;
    ticksFrom: number;
    ticksTo: number;
    expenseParentAccountID: number;
    taxCenter: number;
    address: string;
    otherBenefits: number;
    sex: Sex;
    roll: EmployeeRoll;
    employeeID: string;
    employeeName: string;
    telephone: string;
    bankAccountNo: string;
    grossSalary: number;
    status: EmployeeStatus;
    orgUnitID: number;
    dismissDate: string;
    employmentType: EmploymentType;
    shareHolder: boolean;
    accounts: number[];
    picture: number;
    birthDate: string;
    enrollmentDate: string;

    age: number;
    employeeNameID: string;
    taxableTransportAllowance: number;
    objectIDVal: number;
}