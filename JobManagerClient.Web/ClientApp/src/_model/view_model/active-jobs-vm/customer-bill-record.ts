export interface CustomerBillRecord {
    id: number;
    billDate: string;
    billDocumentTypeID: number;
    customerID: number;
    connectionID: number;
    periodID: number;
    postDate: string;
    paymentDocumentID: number;
    paymentDate: string;
    paymentDiffered: boolean;
    draft: boolean;
    isPayedOrDiffered: boolean;
}