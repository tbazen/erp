export interface JobBOMItem {
    itemID: string;
    inSourced: boolean;
    quantity: number;
    referenceUnitPrice: number;
    unitPrice: number;
    description: string;
    calculated: boolean;
    tag: string;
}
export interface JobBillOfMaterial {
    id: number;
    jobID: number;
    items: JobBOMItem[];
    surveyCollectionTime: string;
    analysisTime: string;
    note: string;
    invoiceNo: number;
    invoiceDate: string;
    preparedBy: string;
    storeIssueID: number;
}