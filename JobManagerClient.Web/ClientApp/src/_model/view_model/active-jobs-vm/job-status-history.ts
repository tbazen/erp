export interface AgentActionData
{
    id: number;
    actionDate: string;
    agent?: string;
    jobID: string;
    note: string;
}

export interface JobStatusHistory extends AgentActionData {
    changeDate:string;
    newStatus : number;
    oldStatus : number;
}