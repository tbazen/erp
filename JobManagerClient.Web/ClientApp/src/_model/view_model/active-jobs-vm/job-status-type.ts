export interface JobStatusType {
    id: number;
    name: string;
    commandName: string;
}