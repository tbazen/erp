import {Employee} from "./IEmployee";

export interface SearchEmployeePar {
    sessionID: string;
    ticks: number;
    orgUnitID: number;
    query: string;
    includeDismissed: boolean;
    searchType: EmployeeSearchType;
    index: number;
    pageSize: number;
}

export enum EmployeeSearchType {
    Name = 1,
    All = 27,
    ID = 27
}


export interface SearchEmployeeOut {
    nResult: number;
    _ret: Employee[];
}