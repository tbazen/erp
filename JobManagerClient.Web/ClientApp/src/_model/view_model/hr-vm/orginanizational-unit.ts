export interface OrgUnit {
    name: string
    description: string
    id: number
    pid: number
    objectIDVal: number
}