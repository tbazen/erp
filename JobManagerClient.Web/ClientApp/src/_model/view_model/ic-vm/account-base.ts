export enum AccountProtection {
    None = 0,
    DenyUserUpdate = 1,
    DenyUserDelete = 2,
    DenyUserPost = 4,
    SystemAccount = 5
}

export enum StandardAccountType
{
    Undefined=0,
    Cash=100100,
    Bank=100200,
    CashAndBank=100210,
    FixedAsset=100300,
    Inventory=100400,
    Stock=100500,
    Receivable=100600,
    Payable=200100,
    Income = 400000,
    Cost =500100,
    Expense=500200,    
}

export enum AccountStatus {
    Pending = 0,
    Activated = 1,
    Deactivated = 2
}

export interface AccountBase {
    id: number;
    isControl: boolean;
    protection: AccountProtection;
    childCount: number;
    activateDate: string;
    deactivateDate: string;
    status: AccountStatus;
    description: string;
    name: string;
    code: string;
    pid: number;
    creationDate: string;

    codeName: string;
    nameCode: string;
    objectIDVal: number;
}

export interface Account extends AccountBase {
    creditAccount: boolean;
    debitCreditName: string;
    accountType:StandardAccountType;
}