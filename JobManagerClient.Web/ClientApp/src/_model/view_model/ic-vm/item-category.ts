export interface ItemCategory {
    id: number;
    pendingDeliverySummaryAccountID: number;
    pendingOrderSummaryAccountID: number;
    accumulatedDepreciationSummaryAccountID: number;
    depreciationSummaryAccountID: number;
    originalFixedAssetSummaryAccountID: number;
    finishedGoodsSummaryAccountID: number;
    generalInventorySummaryAccountID: number;
    finishedWorkSummaryAccountID: number;
    unearnedSummaryAccountID: number;
    salesSummaryAccountID: number;
    prePaidExpenseSummaryAccountID: number;
    directCostSummaryAccountID: number;
    expenseSummaryAccountID: number;
    pendingOrderAccountPID: number;
    pendingDeliveryAcountPID: number;
    prePaidExpenseAccountPID: number;
    depreciationAccountPID: number;
    originalFixedAssetAccountPID: number;
    finishedGoodsAccountPID: number;
    generalInventoryAccountPID: number;
    finishedWorkAccountPID: number;
    unearnedRevenueAccountPID: number;
    salesAccountPID: number;
    accumulatedDepreciationAccountPID: number;
    directCostAccountPID: number;
    expenseAccountPID: number;
    description: string;
    pid: number;
    code: string;
    nameCode: string;
}