export enum GoodOrService {
    Good = 1,
    Service = 2,
    Unknown = 3
}

export enum ServiceType {
    None = 0,
    Contractors = 1,
    MillServices = 2,
    TractorsAndCombinedHarvesters = 3
}

export enum ExpenseType {
    PurchaseExpense = 1,
    GeneralExpense = 2,
    UnclaimedExpense = 3,
    TaxAuthorityUnclaimable = 4
}

export enum InventoryType {
    GeneralInventory = 0,
    FinishedGoods = 1
}

export enum ItemTaxStatus {
    Taxable = 1,
    NonTaxable = 2
}

export enum GoodType {
    None = 0,
    RawHide = 1
}

export interface TransactionItems {
    code: string;
    finishedWorkAccountID: number;
    originalFixedAssetAccountID: number;
    depreciationAccountID: number;
    pendingOrderAccountID: number;
    pendingDeliveryAcountID: number;
    expenseSummaryAccountID: number;
    prePaidExpenseSummaryAccountID: number;
    salesSummaryAccountID: number;
    finishedGoodAccountID: number;
    unearnedSummaryAccountID: number;
    originalFixedAssetSummaryAccountID: number;
    depreciationSummaryAccountID: number;
    accumulatedDepreciationSummaryAccountID: number;
    generalInventorySummaryAccountID: number;
    finishedGoodsSummaryAccountID: number;
    pendingOrderSummaryAccountID: number;
    pendingDeliverySummaryAccountID: number;
    categoryID: number;
    finishedWorkSummaryAccountID: number;
    inventoryAccountID: number;
    accumulatedDepreciationAccountID: number;
    salesAccountID: number;
    name: string;
    description: string;
    fixedUnitPrice: number;
    isDirectCost: boolean;
    unearnedRevenueAccountID: number;
    goodOrService: GoodOrService;
    serviceType: ServiceType;
    expenseType: ExpenseType;
    inventoryType: InventoryType;
    taxStatus: ItemTaxStatus;
    goodType: GoodType;
    depreciationType: number;
    measureUnitID: number;
    activated: boolean;
    isInventoryItem: boolean;
    isExpenseItem: boolean;
    isSalesItem: boolean;
    isFixedAssetItem: boolean;
    expenseAccountID: number;
    prePaidExpenseAccountID: number;

    coaName: string;
    materialAssetReturnAccountID: number;
    nameCode: string;
    coaCode: string;
}