export interface CreateUserSessionPar {
    userName: string;
    password: string;
    source: string;
}