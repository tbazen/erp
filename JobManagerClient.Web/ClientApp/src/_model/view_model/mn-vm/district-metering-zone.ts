export interface DistrictMeteringZone {
    id: number;
    name: string;
}