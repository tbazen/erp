import { CollectionMethod } from "../../../../../_enum/mn-enum/internal-services-enum/cash-handover-enum/collection-method";

export interface PaymentCenterSerialBatch {
    paymentCenterID: number;
    documentTypeID: number;
    batchID: number;
}
export interface PaymentCenter {
    id: number;
    casheir: number;
    casherAccountID: number;
    centerName: string;
    summaryAccountID: number;
    userName: string;
    storeCostCenterID: number;
    collectionMethod: CollectionMethod;
    serialBatches: PaymentCenterSerialBatch[];
}