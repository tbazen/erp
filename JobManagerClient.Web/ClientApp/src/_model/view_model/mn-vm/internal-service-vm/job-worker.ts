import { JobWorkerRole } from "../../../../_enum/mn-enum/job-worker-role";

export interface JobWorker {
    userID: string;
    active: boolean;
    employeeID: number;
    roles: JobWorkerRole[];
    jobTypes: number[];
}