export interface Kebele {
    id: number;
    name: string;
}