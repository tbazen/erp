import { MaterialOperationalStatus } from '../../../_enum/mn-enum/material-operational-status';

export class MeterData {
    public itemCode?:string;
    public serialNo?:string;
    public modelNo?:string;
    public initialMeterReading:number= 0;
    public opStatus?:MaterialOperationalStatus;
    public detailDesc:string = this.modelNo||'';
    /*
    commented out because of the problem when used with react state hook
    public get detailDesc():string {
        return this.modelNo||'';
    }*/
    constructor(value?: {itemCode:string,serialNo:string,modelNo:string,opStatus:MaterialOperationalStatus}){
        if(value){
            this.itemCode = value.itemCode;
            this.serialNo = value.serialNo;
            this.modelNo = value.modelNo;
            this.opStatus = value.opStatus;
        }
    }
}