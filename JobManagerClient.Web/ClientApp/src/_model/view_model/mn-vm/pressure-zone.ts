export interface PressureZone {
    id: number;
    name: string;
}