import { SubscriptionType } from "../../../_enum/mn-enum/subscription-type";
import { ConnectionType } from "../../../_enum/mn-enum/connection-type";
import { MaterialOperationalStatus } from "../../../_enum/mn-enum/material-operational-status";
import { DataStatus } from "../../../_enum/mn-enum/data-status";
import { SupplyCondition } from "../../../_enum/mn-enum/supply-condition";
import { MeterPositioning } from "../../../_enum/mn-enum/meter-positioning";
import { DataTimeBinding } from "../../../_enum/mn-enum/datetime-binding";
import { CustomerStatus } from "../../../_enum/mn-enum/customer-status";
import { SubscriberType } from "../../../_enum/mn-enum/subscriber-type";
import { SubscriptionStatus } from '../../../_enum/mn-enum/subscription-status';
import { MeterData } from './meter-data';

export interface SubscriberSearchResult {
    subscriber: Subscriber;
    subscription: Subscription;
    rank: number;
}
export interface Subscriber {
    id: number;
    subTypeID: number;
    depositAccountID: number;
    accountID: number;
    statusDate: string;
    status: CustomerStatus;
    email: string;
    nameOfPlace: string;
    customerCode: string;
    amharicName: string;
    address: string;
    kebele: number;
    subscriberType: SubscriberType;
    name: string;
    phoneNo: string;
    objectIDVal: number;
}

export interface Subscription {
    id: number;
    pressureZone: number;
    dma: number;
    householdSize: number;
    opStatus: MaterialOperationalStatus;
    modelNo: string;
    serialNo: string;
    itemCode: string;
    remark: string;
    houseConnectionY: number;
    houseConnectionX: number;
    waterMeterY: number;
    waterMeterX: number;
    dataStatus: DataStatus;
    supplyCondition: SupplyCondition;
    meterPosition: MeterPositioning;
    parcelNo: string;
    ticksFrom: number;
    ticksTo: number;
    timeBinding: DataTimeBinding;
    subscriberID: number;
    connectionType: ConnectionType;
    subscriptionType: SubscriptionType;
    subscriptionStatus: SubscriptionStatus;
    contractNo: string;
    address: string;
    landCertificateNo: string;
    subscriber: Subscriber;
    initialMeterReading: number;
    prePaid: boolean;
    kebele: number;

    objectIDVal: number;
    versionedID: VersionedID;
    hasUpperBound: boolean;
    meterData: MeterData;
    hasCoordinate: boolean;
}

export interface VersionedID {
    id: number;
    version: number;
}