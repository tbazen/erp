import {ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import {IClientHandler} from "../../../_model/job-rule-model/client-handler";
import {JobRuleActions} from "../../../_constants/action-types";
import {IJobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";



const jobRuleLoading = (message : string) => ({type : JobRuleActions.LOADING , message})
const jobRulePush = (payload : IClientHandler) => ({type: JobRuleActions.PUSH , payload})
const jobRuleSuccess = (message : string) => ({type : JobRuleActions.SUCCESS,message})
const jobRuleError = (message : string) => ({type:JobRuleActions.ERROR,message})


export const constructClientMetaData = (target : Function ) : IJobRuleClientDecorator => {
    const clientMetaData : IJobRuleClientDecorator = {
        route : target.prototype.route,
        jobType : target.prototype.jobType,
        jobName : target.prototype.jobName,
        hasConfiguration : target.prototype.hasConfiguration,
        configurationName : target.prototype.configurationName,
        configurationRoute : target.prototype.configurationRoute,
        innerRoutes : target.prototype.innerRoutes,
        priority : target.prototype.priority
    }
    return clientMetaData
}

export const constructClientHandler = <TFunction extends Function>(target : TFunction ) : JobRuleClientHandlerBase => {
    const targetInstance = Object.create(target.prototype)
    if(! (targetInstance instanceof JobRuleClientHandlerBase) )
        throw new Error(`Target object is not instance of ${JobRuleClientHandlerBase.name}`)
    const handler : JobRuleClientHandlerBase = targetInstance
    return handler;
}


export const loadClientHandler: ActionCreator<ThunkAction<Promise<void>, any, void, any>> = () => {
    return async (dispatch: any) => {
        dispatch(jobRuleLoading("Constructing Customer services,please wait ..."))
        const wrapperModules = await import('../../../job-rule-configuration/job-rule-configuration')
        const moduleKeys = Object.keys(wrapperModules)
        try{
            for(const key of moduleKeys){
                // @ts-ignore
                const innerModules  = await wrapperModules[key]()
                const innerModuleKeys = Object.keys(innerModules)
                for(const subKey of innerModuleKeys){
                    // @ts-ignore
                    const module = innerModules[subKey]
                    if( module.prototype && module.prototype.isStandardJobType )
                    {
                        const clientHandler : IClientHandler = {
                            meta : constructClientMetaData(module),
                            handler : constructClientHandler(module)
                        }
                        dispatch(jobRulePush(clientHandler))
                    }
                }
            }
            dispatch(jobRuleSuccess("Job rules loaded successfully"))
        }
        catch(error){
            dispatch(jobRuleError(error.message)) }
    }
}
