import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import errorMessage from "../error.message";
import {
    ActiveJobDetailInformationActions, ActiveJobsActions,
    BOMInvoicePreviewActions,
    JobBOMActions,
    JobDataActions,
    JobHistoryActions,
    MeasureUnitActions,
    WorkflowDataActions
} from '../../../_constants/action-types';
import JobManagerService, {
    AddAppointmentPar,
    ChangeJobStatusPar,
    CloseAppointmentPar, DeleteBOMPar,
    GetFilteredJobsPar,
    GetInvoicePreviewPar,
    GetJobBOMPar,
    GetJobHistoryPar,
    GetJobPar,
    GetPossibleNextStatusPar,
    GetWorkFlowDataWebPar,
    JobData,
    RemoveJobPar,
    SearchJobPar, SetBillofMateialPar,
    UpdateAppointmentPar
} from "../../../_services/job.manager.service";
import {NotificationTypes, openNotification} from "../../../shared/components/notification/notification";
import {JobStatusHistory} from "../../../_model/view_model/active-jobs-vm/job-status-history";
import PayrollService from "../../../_services/payroll.service";
import {JobWorker} from "../../../_model/view_model/mn-vm/internal-service-vm/job-worker";
import {Employee} from "../../../_model/view_model/IEmployee";
import {JobHistory} from "../../../_model/state-model/aj-sm/job-history-state";
import {JobStatusType} from "../../../_model/view_model/active-jobs-vm/job-status-type";
import {getDateDifference, getLocalDateString} from "../../../_helpers/date-util";
import {MeasureUnit, TransactionItems} from "../../../_model/level0/iERP-transaction-model/transaction-items";
import BNFinanceService, {GetMeasureUnitsPar} from "../../../_services/bn.finance.service";
import SubscriberManagementService, {GetSystemParametersWebPar} from "../../../_services/subscribermanagment.service";
import {
    transactionItemsError,
    transactionItemsLoading,
    transactionItemsSuccess
} from "../ic-actions/item-configuration-actions";
import {IActiveJobDetailPayload} from "../../../_model/state-model/aj-sm/active-job-detail-information-state";
import {Subscriber} from "../../../_model/view_model/mn-vm/subscriber-search-result";
import {JobTypeInfo} from "../../../_model/level0/job-type-info";
import {JobAppointment} from "../../../_model/level0/job-appointment";
import {IJobBOMPayload} from '../../../_model/state-model/aj-sm/job-bom-state';
import {JobBillOfMaterial} from '../../../_model/view_model/active-jobs-vm/job-bill-of-material';
import {CustomerBillRecord} from '../../../_model/view_model/active-jobs-vm/customer-bill-record';
import AccountingService from '../../../_services/accounting.service';
import {StandardJobStatus} from "../../../_model/job-rule-model/standard-job-status";
import {IActiveJobPayload} from "../../../_model/state-model/aj-sm/active-jobs-state";

const job_mgr_services = new JobManagerService()
const payroll_service = new PayrollService()
const bn_finance_service = new BNFinanceService()
const subscriber_mgr_service = new SubscriberManagementService()
const accounting_service = new AccountingService()

const jobDataLoading = (message:string) => ({type:JobDataActions.LOADING,message})
const jobDataSuccess = (payload: JobData[])=> ({type:JobDataActions.SUCCESS,payload})
const jobDataError = (message:string)=>({type:JobDataActions.ERROR,message})
export const fetchFilteredJobs: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetFilteredJobsPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(jobDataLoading('Loading Jobs,Please wait...'))
        return await job_mgr_services.GetFilteredJobs(params)
            .then(res => {
                return dispatch(jobDataSuccess(res.data))
            })
            .catch(error => dispatch(jobDataError(errorMessage(error))))
    }
}
export const searchJobs: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :SearchJobPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(jobDataLoading('Loading Jobs,Please wait...'))
        return await job_mgr_services.SearchJob(params)
            .then(res => {
                return dispatch(jobDataSuccess(res.data._ret))
            })
            .catch(error => dispatch(jobDataError(errorMessage(error))))
    }
}





const activeJobsLoading = (message:string) => ({type:ActiveJobsActions.LOADING,message})
const activeJobsSuccess = (payload: IActiveJobPayload[])=> ({type:ActiveJobsActions.SUCCESS,payload})
const activeJobsError = (message:string)=>({type:ActiveJobsActions.ERROR,message})
export const fetchFilteredActiveJobs: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetFilteredJobsPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(activeJobsLoading('Loading Jobs,Please wait...'))
        try{
            const jobs:JobData[] = ( await job_mgr_services.GetFilteredJobs(params) ).data
            let activeJobs:IActiveJobPayload[] = []
            for(const job of jobs){
                let customer:Subscriber
                if(job.customerID === -1)
                  customer = job.newCustomer as Subscriber
                else
                  customer= (await subscriber_mgr_service.GetSubscriber2({sessionId:params.sessionId,id: job.customerID}) ).data;
                const jobStatus:JobStatusType = (await job_mgr_services.GetJobStatus({sessionId:params.sessionId,status: job.status}) ).data;
                const activeJob:IActiveJobPayload ={
                    job,
                    customer,
                    jobStatus
                }
                activeJobs = [...activeJobs,activeJob]
            }
            return dispatch(activeJobsSuccess(activeJobs))
        } catch(error){ return dispatch(activeJobsError(errorMessage(error))) }
    }
}
export const searchActiveJobs: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :SearchJobPar) => {
    return async (dispatch: any): Promise<Action> => {

        dispatch(activeJobsLoading('Loading Jobs,Please wait...'))
        try{
            const jobs:JobData[] = ( await job_mgr_services.SearchJob(params) ).data._ret
            let activeJobs:IActiveJobPayload[] = []
            for(const job of jobs){
                let customer:Subscriber
                if(job.customerID === -1)
                    customer = job.newCustomer as Subscriber
                else
                    customer= (await subscriber_mgr_service.GetSubscriber2({sessionId:params.sessionID,id: job.customerID}) ).data;
                const jobStatus:JobStatusType = (await job_mgr_services.GetJobStatus({sessionId:params.sessionID,status: job.status}) ).data;
                const activeJob:IActiveJobPayload ={
                    job,
                    customer,
                    jobStatus
                }
                activeJobs = [...activeJobs,activeJob]
            }
            return dispatch(activeJobsSuccess(activeJobs))
        } catch(error){ return dispatch(activeJobsError(errorMessage(error))) }
    }
}



export const removeJob: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :RemoveJobPar) => {
    return async (dispatch: any): Promise<Action> => {
        openNotification({message:'Deleting Job',description:'deleting job please wait,please wait',notificationType:NotificationTypes.LOADING})
        return await job_mgr_services.RemoveJob(params)
            .then(res => {
                openNotification({message:'Success',description:'job deleted successfully',notificationType:NotificationTypes.SUCCESS})
                const jobDetailParams : GetJobPar={
                    sessionId: params.sessionId,
                    jobID: params.jobHandle
                }
                /**
                 * Added to refresh the page
                 */
                dispatch(fetchActiveJobDetailInformation(jobDetailParams))
                return res
            })
            .catch(error => {
                openNotification({message:'Failed',description:errorMessage(error),notificationType:NotificationTypes.ERROR})
                return error
            })
    }
}


const jobHistoryLoading = (message:string) => ({type:JobHistoryActions.LOADING,message})
const jobHistorySuccess = (payload: JobHistory[])=> ({type:JobHistoryActions.SUCCESS,payload})
const jobHistoryError = (message:string)=>({type:JobHistoryActions.ERROR,message})
export const fetchJobHistory: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetJobHistoryPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            dispatch(jobHistoryLoading("Loading job History"))
            const response =  await job_mgr_services.GetJobHistory(params);
            const jobStatusHistory : JobStatusHistory[] = response.data
            let jobHistory : JobHistory[] = []
            for (const iterator of jobStatusHistory) {
                if(iterator === jobStatusHistory[0])
                    continue;
                const index = jobStatusHistory.indexOf(iterator);
                const worker : JobWorker = ( await job_mgr_services.GetWorker({sessionId:params.sessionId,userID:iterator.agent+''})).data;
                const employee : Employee =( await payroll_service.GetEmployee({sessionId:params.sessionId,ID:worker.employeeID})).data;
                const oldStatus : JobStatusType = ( await job_mgr_services.GetJobStatus({sessionId:params.sessionId,status:iterator.oldStatus}) ).data
                const newStatus : JobStatusType = ( await job_mgr_services.GetJobStatus({sessionId:params.sessionId,status:iterator.newStatus}) ).data
                const duration = getDateDifference( jobStatusHistory[index].changeDate,jobStatusHistory[index-1].changeDate);
                const history:JobHistory={
                    date:iterator.changeDate,
                    worker:employee.employeeName,
                    stage:oldStatus.name,
                    forwardTo:newStatus.name,
                    note:iterator.note,
                    duration
                };
                jobHistory = [...jobHistory,history];
            }
            return dispatch( jobHistorySuccess(jobHistory))
        } catch( error) { return dispatch( jobHistoryError(errorMessage(error))) }
    }
}


const measureUnitLoading = (message:string) => ({type:MeasureUnitActions.LOADING,message})
const measureUnitSuccess = (payload: MeasureUnit[])=> ({type:MeasureUnitActions.SUCCESS,payload})
const measureUnitError = (message:string)=>({type:MeasureUnitActions.ERROR,message})
export const fetchMeasureUnits: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetMeasureUnitsPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(measureUnitLoading('Loading measure units,please wait...'))
        try{
            const response  = await bn_finance_service.GetMeasureUnits(params)
            return dispatch(measureUnitSuccess(response.data))
        } catch(error){
            return dispatch(measureUnitError(errorMessage(error)))
        }
    }
}
export const fetchItemsInCategory: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetSystemParametersWebPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            dispatch(transactionItemsLoading('Loading transaction items'))
            let response = await subscriber_mgr_service.GetSystemParametersWeb(params);
            let systemParameter: number[] = response.data;
            if(systemParameter.length > 0){
                response  = await bn_finance_service.GetItemsInCategory({ sessionId:params.sessionId,categID:systemParameter[0]});
                const descriptionArray :TransactionItems[] = response.data
                return dispatch(transactionItemsSuccess(descriptionArray))
            }
            return dispatch(transactionItemsError('Item Category Not found'))
        } catch(error){
            console.log(error)
            return dispatch(transactionItemsError(errorMessage(error)))
        }
    }
}


const activeJobDetailInformationLoading = (message:string) => ({type:ActiveJobDetailInformationActions.LOADING,message})
const activeJobDetailInformationSuccess = (payload: IActiveJobDetailPayload)=> ({type:ActiveJobDetailInformationActions.SUCCESS,payload})
const activeJobDetailInformationError = (message:string)=>({type:ActiveJobDetailInformationActions.ERROR,message})
const activeJobDetailInformationUpdateAppointment = (payload:JobAppointment[])=>({type:ActiveJobDetailInformationActions.UPDATE_APPOINTMENT,payload})
const activeJobDetailInformationUpdateStatus = (status : JobStatusType , nextStatus:number[], previousStatus: JobStatusHistory[] )=>({type:ActiveJobDetailInformationActions.UPDATE_STATUS,status,nextStatus,previousStatus})


export const fetchActiveJobDetailInformation: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetJobPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(activeJobDetailInformationLoading('Loading Jobs,Please wait...'))
        try{

            const job : JobData = ( await job_mgr_services.GetJob(params)  ).data
            let customer : Subscriber;
            if(job.customerID === -1)
                customer = job.newCustomer as Subscriber
            else
                customer  = (await subscriber_mgr_service.GetSubscriber2({sessionId:params.sessionId,id:job.customerID}) ).data;
            const status: JobStatusType = (await job_mgr_services.GetJobStatus({sessionId:params.sessionId,status:job.status})).data;
            const nextStatusPar : GetPossibleNextStatusPar = {
                sessionId:params.sessionId,
                date:getLocalDateString(),
                jobID:job.id
            }
            const nextStatus : number[] = (await job_mgr_services.getPossibleNextStatus({...nextStatusPar})).data
            const jobType: JobTypeInfo = (await job_mgr_services.GetJobType({ sessionId:params.sessionId,typeID:job.applicationType }) ).data
            const appointments : JobAppointment[] = (await job_mgr_services.GetJobAppointments({ sessionId:params.sessionId,jobHandle:job.id}) ).data
            const previousStatusResponse : JobStatusHistory[] = ( await job_mgr_services.GetJobHistory({sessionId:params.sessionId,jobID:job.id}) ).data
            const previousStatus : JobStatusHistory[] = []
            for(let i = previousStatusResponse.length-1 ; i>=0 ; i--){
                const isAdded = previousStatus.find(state => state.newStatus === previousStatusResponse[i].newStatus)
                if(!isAdded && job.status !== previousStatusResponse[i].newStatus)
                    previousStatus.push(previousStatusResponse[i]);
            }

            const payload : IActiveJobDetailPayload = {
                job,
                status,
                customer,
                nextStatus,
                jobType,
                appointments,
                previousStatus
            }
            return dispatch(activeJobDetailInformationSuccess(payload))
        } catch(error){
            return  dispatch(activeJobDetailInformationError(errorMessage(error)))
        }
    }
}
export const updateActiveJobAppointment: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :UpdateAppointmentPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            openNotification({
                message:'Loading',
                description:'Registering Appointment please wait',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_services.UpdateAppointment( params);
            const appointments : JobAppointment[] = (await job_mgr_services.GetJobAppointments({ sessionId:params.sessionId,jobHandle:+params.appointment.jobID}) ).data
            openNotification({
                message:'Appointment Set',
                description:'Appointment set successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(activeJobDetailInformationUpdateAppointment(appointments))
        } catch(error){
            openNotification({
                message:'Failed',
                description:error.message,
                notificationType:NotificationTypes.ERROR
            })
            return  dispatch(activeJobDetailInformationError(errorMessage(error)))
        }
    }
}
export const addActiveJobAppointment: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :AddAppointmentPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            openNotification({
                message:'Loading',
                description:'Registering Appointment please wait',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_services.AddAppointment(params)
            const appointments : JobAppointment[] = (await job_mgr_services.GetJobAppointments({ sessionId:params.sessionId,jobHandle:+params.appointment.jobID}) ).data
            openNotification({
                message:'Appointment Set',
                description:'Appointment set successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(activeJobDetailInformationUpdateAppointment(appointments))
        } catch(error){
            openNotification({
                message:'Failed',
                description:error.message,
                notificationType:NotificationTypes.ERROR
            })
            return  dispatch(activeJobDetailInformationError(errorMessage(error)))
        }
    }
}
export const closeActiveJobAppointment: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :CloseAppointmentPar, jobID : number) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            openNotification({
                message:'Loading',
                description:'Closing Appointment please wait',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_services.CloseAppointment(params)
            const appointments : JobAppointment[] = (await job_mgr_services.GetJobAppointments({ sessionId:params.sessionId,jobHandle:jobID}) ).data
            openNotification({
                message:'Appointment Closed',
                description:'Appointment closed successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(activeJobDetailInformationUpdateAppointment(appointments))
        } catch(error){
            openNotification({
                message:'Failed',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return  dispatch(activeJobDetailInformationError(errorMessage(error)))
        }
    }
}
export const changeJobStatus: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :ChangeJobStatusPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            openNotification({
                message:'Loading',
                description:'Changing job status please wait',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_services.ChangeJobStatus(params)
            const job : JobData = ( await job_mgr_services.GetJob({sessionId:params.sessionId,jobID:params.jobID})  ).data
            const status: JobStatusType = (await job_mgr_services.GetJobStatus({sessionId:params.sessionId,status:job.status})).data;
            const nextStatusPar : GetPossibleNextStatusPar = {
                sessionId:params.sessionId,
                date:getLocalDateString(),
                jobID:params.jobID
            }
            const nextStatus : number[] = (await job_mgr_services.getPossibleNextStatus({...nextStatusPar})).data

            const previousStatusResponse : JobStatusHistory[] = ( await job_mgr_services.GetJobHistory({sessionId:params.sessionId,jobID:job.id}) ).data
            const previousStatus : JobStatusHistory[] = []
            for(let i = previousStatusResponse.length-1 ; i>=0 ; i--){
                const isAdded = previousStatus.find(state => state.newStatus === previousStatusResponse[i].newStatus)
                if(!isAdded && job.status !== previousStatusResponse[i].newStatus)
                    previousStatus.push(previousStatusResponse[i]);
            }

            openNotification({
                message:'Job status changed',
                description:'job status changed successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(activeJobDetailInformationUpdateStatus(status,nextStatus,previousStatus))
        } catch(error){
            openNotification({
                message:'Failed',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return  dispatch(()=>{})
        }
    }
}


const workflowDataLoading = (message:string) => ({type:WorkflowDataActions.LOADING,message})
const workflowDataSuccess = (payload: any)=> ({type:WorkflowDataActions.SUCCESS,payload})
const workflowDataError = (message:string)=>({type:WorkflowDataActions.ERROR,message})
export const fetchWorkflowData: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetWorkFlowDataWebPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(workflowDataLoading('Loading measure units,please wait...'))
        try{
            const response  = await job_mgr_services.GetWorkFlowDataWeb(params)
            return dispatch(workflowDataSuccess(response.data))
        } catch(error){
            return dispatch(workflowDataError(errorMessage(error)))
        }
    }
}




const jobBOMLoading = (message:string) => ({type:JobBOMActions.LOADING,message})
const jobBOMSuccess = (payload: IJobBOMPayload[])=> ({type:JobBOMActions.SUCCESS,payload})
const jobBOMError = (message:string)=>({type:JobBOMActions.ERROR,message})
export const fetchJobBOM: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetJobBOMPar , job :JobData ) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            dispatch(jobBOMLoading("Loading job History"))
            const billOfMaterials : JobBillOfMaterial[]  = (await job_mgr_services.GetJobBOM(params)).data 
            let payload : IJobBOMPayload[] = []
            for(const billOfMaterial of billOfMaterials){
                const billrec:CustomerBillRecord = (await subscriber_mgr_service.GetCustomerBillRecord({sessionId:params.sessionId,billRecordID: billOfMaterial.invoiceNo })).data;
                let receiptNo: string ="";
                let isPaid:boolean = billrec!=null && billrec.isPayedOrDiffered;
                if(isPaid)
                {
                    if (billrec.paymentDiffered)
                        receiptNo = "Differed";
                    else 
                        receiptNo = (await accounting_service.GetAccountDocument( { sessionId:params.sessionId, documentID: billrec.paymentDocumentID, fullData: false} )).data.paperRef;
                }
                let isPaymentStatus: boolean = false;
                if ( (!isPaid) && (job.status === StandardJobStatus.APPLICATION_PAYMENT || job.status === StandardJobStatus.WORK_PAYMENT) )
                    isPaymentStatus = true;
                let isStoreIssue = false;
                if(job.status === StandardJobStatus.STORE_ISSUE && billOfMaterial.storeIssueID!=-1)
                    isStoreIssue = true;

                let total: number
                try{
                    total = ( await job_mgr_services.GetBOMTotal({ sessionId:params.sessionId, bomID:billOfMaterial.id }) ).data
                } catch(err){
                    total = -1;
                }
                const payloadItem: IJobBOMPayload ={
                    total,
                    isPaid,
                    receiptNo,
                    isStoreIssue,
                    billOfMaterial,
                    isPaymentStatus,
                }
                payload = [...payload,payloadItem]; 
            }
            return dispatch( jobBOMSuccess(payload))
        } catch( error) { return dispatch( jobBOMError(errorMessage(error))) }
    }
}



const bomPreviewLoading = (message:string) => ({type:BOMInvoicePreviewActions.LOADING,message})
const bomPreviewSuccess = (payload: string)=> ({type:BOMInvoicePreviewActions.SUCCESS,payload})
const bomPreviewError = (message:string)=>({type:BOMInvoicePreviewActions.ERROR,message})
export const fetchBOMInvoicePreview: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetInvoicePreviewPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            dispatch(bomPreviewLoading("Loading bom preview"))
            const html :string = (await job_mgr_services.GetInvoicePreview(params)).data
            return dispatch( bomPreviewSuccess(html))
        } catch( error) { return dispatch( bomPreviewError(errorMessage(error))) }
    }
}

export const requestDeleteBOM: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :DeleteBOMPar , job:JobData) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            openNotification({
                message:'Deleting',
                description:'deleting bill of material please wait ',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_services.DeleteBOM(params)
            openNotification({
                message:'Delete complete',
                description:'bill of material deleted successfully! ',
                notificationType:NotificationTypes.SUCCESS
            })
            const jobBOMParams : GetJobBOMPar = {
                sessionId:params.sessionId,
                jobID:job.id
            }
            return dispatch(fetchJobBOM(jobBOMParams , job))
        } catch( error) {
            openNotification({
                message:'Failed',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return dispatch( bomPreviewError(errorMessage(error))) }
    }
}

export const requestSetBOM: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :SetBillofMateialPar , job:JobData) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            openNotification({
                message:'Saving',
                description:'saving bill of material please wait ',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_services.SetBillofMateial(params)
            openNotification({
                message:'Saved',
                description:'bill of material saved successfully! ',
                notificationType:NotificationTypes.SUCCESS
            })
            const jobBOMParams : GetJobBOMPar = {
                sessionId:params.sessionId,
                jobID:job.id
            }
            return dispatch(fetchJobBOM(jobBOMParams , job))
        } catch( error) {
            openNotification({
                message:'Failed',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return dispatch( bomPreviewError(errorMessage(error))) }
    }
}

