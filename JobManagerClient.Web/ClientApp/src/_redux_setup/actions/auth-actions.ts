import { AuthActions } from "../../_constants/action-types";
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import AuthService from "../../_services/auth.service";
import {CreateUserSessionPar} from "../../_model/view_model/login-view-model";
import {LOGGED_IN_USERNAME, SESSION_KEY} from "../../_constants/local-storage";
import {PERMISSION_OBJECT} from "../../_model/view_model/session-id";
import errorMessage from "./error.message";


const auth_service = new AuthService()

const authenticationRequest=(message:string) =>({type:AuthActions.AUTH_REQUEST,message})
const authenticationError = (message:string) => ({type:AuthActions.AUTH_REQUEST_ERROR,message})
const authenticationSuccess = (sessionId:string,userName: string) => ({type:AuthActions.AUTH_REQUEST_SUCCESS,sessionId,userName})
const logoutUser =()=>( {type:AuthActions.LOGOUT})
const checkSession= () => ( {type:AuthActions.SESSION_CHECKING})


export const requestLogin: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (cred : CreateUserSessionPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(authenticationRequest('validating Credential,Please wait'))
        return await auth_service.validateCredential(cred)
            .then(res => {
                localStorage.setItem(SESSION_KEY,res.data)
                localStorage.setItem(LOGGED_IN_USERNAME,cred.userName)
                return dispatch(authenticationSuccess(res.data,cred.userName))
            })
            .catch(error => {
                dispatch(authenticationError(errorMessage(error)))
            })
    }
}

export const requestLogout: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return (dispatch: any): Promise<Action> => {
        auth_service.destroySession()
        return dispatch(logoutUser())
    }
}

export const validateSessionId: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = () => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(checkSession())
        let cred = await  localStorage.getItem(SESSION_KEY)
        let userName = await localStorage.getItem(LOGGED_IN_USERNAME)
        if(cred==null)
            return dispatch(logoutUser())
        return await auth_service.validateSessionId({sessionId:cred,obj:PERMISSION_OBJECT})
            .then(res => {
                return dispatch(authenticationSuccess(cred as string,userName as string))
            })
            .catch(error => {
                dispatch(logoutUser())
            })
    }
}



