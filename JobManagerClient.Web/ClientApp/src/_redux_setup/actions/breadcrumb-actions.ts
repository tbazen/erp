import {BreadcrumbActions} from "../../_constants/action-types";
import {IBreadcrumb} from "../../_model/state-model/breadcrumd-state";


export default function pushPathToBC(payload:IBreadcrumb[]){
    return {type:BreadcrumbActions.PUSH_BC_PATHS,payload}
}