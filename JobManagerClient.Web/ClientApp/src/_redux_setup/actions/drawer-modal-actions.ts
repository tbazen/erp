import { DrawerModalActions } from '../../_constants/action-types';

export function openDrawerModal(payload:JSX.Element){
    return {type:DrawerModalActions.OPEN,payload}
}

export function closeDrawerModal(){
    return {type:DrawerModalActions.CLOSE}
}