import { SearchBarActions } from '../../_constants/action-types';

export function exposeSearchBar(placeholder:string,handler:(event:any)=>void){
    return {type:SearchBarActions.VISIBLE,handler,placeholder}
}

export function hideSearchBar(){
    return {type:SearchBarActions.HIDDEN}
}