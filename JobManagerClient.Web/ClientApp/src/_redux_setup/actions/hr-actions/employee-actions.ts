import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import {SearchEmployeeOut, SearchEmployeePar} from "../../../_model/view_model/employee-search-par";
import PayrollService, {ChangeEmployeeOrgUnitPar} from "../../../_services/payroll.service";
import errorMessage from "../error.message";
import {SearchEmployeeActions} from "../../../_constants/action-types";

export function searchEmployeeLoading(message:string){
    return {type:SearchEmployeeActions.SEARCH_EMPLOYEE_LOADING,message}
}
function searchEmployeeSuccess(payload: SearchEmployeeOut){
    return {type:SearchEmployeeActions.SEARCH_EMPLOYEE_SUCCESS,payload}
}
export function searchEmployeeError(message:string){
    return {type:SearchEmployeeActions.SEARCH_EMPLOYEE_ERROR,message}
}

export const searchEmployeeRemove = ( employeeID: string ) => ({type: SearchEmployeeActions.REMOVE, employeeID})

const payroll_service = new PayrollService()

export const requestEmployeeSearch: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (searchParams : SearchEmployeePar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(searchEmployeeLoading('Loading Employees,Please wait...'))
        return await payroll_service.SearchEmployee(searchParams)
            .then(res => {
                return dispatch(searchEmployeeSuccess(res.data))
            })
            .catch(error => dispatch(searchEmployeeError(errorMessage(error))))
    }
}


export const changeEmployeeOrganization: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params : ChangeEmployeeOrgUnitPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(searchEmployeeLoading('Loading Employees,Please wait...'))
        return await payroll_service.ChangeEmployeeOrgUnit(params)
            .then(res => {
                console.log('Empl ORG Changed ',res.data)
                return res.data
            })
            .catch(error => errorMessage(error))
    }
}