import PayrollService, {GetOrgUnitsPar} from "../../../_services/payroll.service";
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import {SearchEmployeeOut} from "../../../_model/view_model/employee-search-par";
import errorMessage from "../error.message";
import {OrgUnitsActions} from "../../../_constants/action-types";



function orgUnitsLoading(message:string){
    return {type:OrgUnitsActions.LOADING,message}
}
function orgUnitsSuccess(payload: SearchEmployeeOut){
    return {type:OrgUnitsActions.SUCCESS,payload}
}
function orgUnitsError(message:string){
    return {type:OrgUnitsActions.ERROR,message}
}

const payroll_service = new PayrollService()

export const fetchOrgUnits: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms : GetOrgUnitsPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(orgUnitsLoading('Loading Organizational Units,Please wait...'))
        return await payroll_service.GetOrgUnits(prms)
            .then(res => {
                return dispatch(orgUnitsSuccess(res.data))
            })
            .catch(error => dispatch(orgUnitsError(errorMessage(error))))
    }
}