import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import errorMessage from "../error.message";
import {AccountBaseActions, ItemCategoryActions, TransactionItemActions} from "../../../_constants/action-types";
import BNFinanceService, { SearchTransactionItemsPar } from "../../../_services/bn.finance.service";
import {ItemCategory} from "../../../_model/view_model/ic-vm/item-category";
import {AccountBase} from "../../../_model/view_model/ic-vm/account-base";
import AccountingService, {GetAccountPar, GetChildAcccountPar} from "../../../_services/accounting.service";
// import { TransactionItems } from "../../../_model/view_model/ic-vm/transaction-item";
import { GetTransactionItemsPar } from '../../../_services/bn.finance.service';
import {TransactionItems} from "../../../_model/level0/iERP-transaction-model/transaction-items";

const account_service = new AccountingService()

//#region  ITEM CATEGORY ACTION CREATOR AND ACTIONS

function itemCategoryLoading(message:string){
    return {type:ItemCategoryActions.ITEM_CATEGORIES_LOADING,message}
}
function itemCategorySuccess(payload: ItemCategory[]){
    return {type:ItemCategoryActions.ITEM_CATEGORIES_SUCCESS,payload}
}
function itemCategoryPush(payload: ItemCategory[]){
    return {type:ItemCategoryActions.ITEM_CATEGORIES_PUSH,payload}
}
function itemCategoryError(message:string){
    return {type:ItemCategoryActions.ITEM_CATEGORIES_ERROR,message}
}

const bnfinance_service = new BNFinanceService()

export const fetchItemCategories: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms : {sessionId:string,PID:number}) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(itemCategoryLoading('Loading Item Categories,Please wait...'))
        try{
            const response = await bnfinance_service.GetItemCategories2(prms);
            const rootCategories : ItemCategory[] = response.data
            let allCategories : ItemCategory[] = [...rootCategories];
            for(const category of rootCategories)
            {
                await (async function fetchChildCategories(itemCategory : ItemCategory){
                    const childResponse = await bnfinance_service.GetItemCategories2({sessionId:prms.sessionId,PID:itemCategory.id});
                    const childCategories:ItemCategory[] = childResponse.data;
                    allCategories= [...allCategories,...childCategories];
                    for(const childCategory of childCategories){ await fetchChildCategories(childCategory) }
                })(category)
            }
            return dispatch(itemCategorySuccess(allCategories))
        } catch(error){
            return dispatch(itemCategoryError(errorMessage(error)))
        }
    }
}

export const fetchChildItemCategories: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms : {sessionId:string,PID:number}) => {
    return async (dispatch: any): Promise<Action> => {
        return await bnfinance_service.GetItemCategories2(prms)
            .then(res => {
                const ctgr : ItemCategory[] = res.data
                dispatch(itemCategoryPush(ctgr))
                ctgr.forEach(ctr=>{
                    return dispatch(fetchChildItemCategories({sessionId:prms.sessionId,PID:ctr.id}))
                })
            })
            .catch(error => dispatch(itemCategoryError(errorMessage(error))))
    }
}


//#endregion


//#region TRANSACTION ITEMS ACTION AND CREATORS


export const transactionItemsLoading = (message:string)=>( {type:TransactionItemActions.LOADING,message}  )
export const transactionItemsSuccess = (payload: TransactionItems[])=>({type:TransactionItemActions.SUCCESS,payload})
export const transactionItemsError = (message:string) => ( {type:TransactionItemActions.ERROR,message} )

export const fetchTransactionItemsInCategory: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms : {sessionId:string,categID:number}) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(transactionItemsLoading('Loading Transaction Items,Please wait...'))
        return await bnfinance_service.GetItemsInCategory(prms)
            .then(res => {
                return dispatch(transactionItemsSuccess(res.data))
            })
            .catch(error => dispatch(transactionItemsError(errorMessage(error))))
    }
}


export const searchTransactionItems: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms :SearchTransactionItemsPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(transactionItemsLoading('Loading Transaction Items,Please wait...'))
        return await bnfinance_service.SearchTransactionItems(prms)
            .then(res => {
                return dispatch(transactionItemsSuccess(res.data._ret))
            })
            .catch(error => dispatch(transactionItemsError(errorMessage(error))))
    }
}

//#endregion


//region ACCOUNT BASE ACTIONS
function accountBaseLoading(message:string){
    return {type:AccountBaseActions.LOADING,message}
}
function accountBaseSuccess(payload: AccountBase[]){
    return {type:AccountBaseActions.SUCCESS,payload}
}
function accountBasePush(payload: Account[]){
    return {type:AccountBaseActions.PUSH,payload}
}
function accountBaseError(message:string){
    return {type:AccountBaseActions.ERROR,message}
}


const tempParam:GetAccountPar ={
    accountType:'CostCenter',
    accountID:1,
    sessionId:''
}

const tempChildPar:GetChildAcccountPar={
    index:0,
    sessionId:'',
    pageSize:50,
    parentAccount:-1,
    accountType:'Account'
}


export const fetchParentAccountBase: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (sessionId :string) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(accountBaseLoading('Loading Transaction Items,Please wait...'))
        return await account_service.GetAccount({...tempParam,sessionId})
            .then(res => {
                console.log('Parent Account ',res.data)
                return dispatch(fetchChildAccountBase({...tempChildPar,sessionId}))
                //TODO Figure out what to do with this comment
                //return dispatch(accountBaseSuccess(res.data))
            })
            .catch(error => dispatch(accountBaseError(errorMessage(error))))
    }
}

export const fetchChildAccountBase: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms : GetChildAcccountPar) => {
    return async (dispatch: any): Promise<Action> => {
        return await account_service.GetChildAcccount(prms)
            .then(res => {
                res.data._ret.forEach((itm:any)=>
                {
                    return dispatch(accountBasePush([itm.val]))
                   /*if(itm.val.childCount>0){
                       return dispatch(fetchChildAccountBase({...prms,parentAccount:itm.val.id}))}
                    */
                })
                return res.data
            })
            .catch(error => dispatch(transactionItemsError(errorMessage(error))))
    }
}
//endregion



//#region NEW VERSION OF CODE STARTS FROM HERE...



export const fetchTransactionItems: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms :GetTransactionItemsPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(transactionItemsLoading('Loading Transaction Items,Please wait...'))
        return await bnfinance_service.GetTransactionItems(prms)
            .then(res => {
                return dispatch(transactionItemsSuccess([res.data]))
            })
            .catch(error => dispatch(transactionItemsError(errorMessage(error))))
    }
}

//#endregion