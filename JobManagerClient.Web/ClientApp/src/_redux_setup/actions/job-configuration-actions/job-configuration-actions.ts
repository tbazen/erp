import {JobConfigurationActions} from "../../../_constants/action-types";
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import JobManagerService, {GetConfigurationWeb} from "../../../_services/job.manager.service";
import errorMessage from "../error.message";

export const configurationLoading = (message:string) => ( {type : JobConfigurationActions.LOADING,message } )
export const configurationError   = (message:string) => ( {type : JobConfigurationActions.ERROR,message } )
export const configurationSuccess = (payload:any) => ( {type : JobConfigurationActions.SUCCESS,payload } )


const job_mgr_service = new JobManagerService();
export const fetchJobConfiguration: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params :GetConfigurationWeb) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(configurationLoading('Loading Job Configuration,Please wait...'))
        try {
            const response = await job_mgr_service.GetConfigurationWeb(params)
            return dispatch(configurationSuccess((response.data)))
        } catch (error) {
            return dispatch(configurationError(errorMessage(error)))
        }
    }
}