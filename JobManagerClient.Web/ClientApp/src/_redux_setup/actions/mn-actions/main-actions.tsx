import React from 'react'
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import {
    BillDocumentActions,
    BillPeriodActions,
    BWFMeterReadingActions,
    BWFMeterReadingListActions,
    CreditSchemeActions,
    CustomerProfileActions,
    DMZActions,
    EmployeeActions,
    JobWorkerActions,
    KebeleActions,
    OrgUnitsActions,
    PaymentCenterActions,
    PressureZoneActions,
    SearchEmployeeActions,
    SingleSubscriberActions,
    SubscriberSearchResultActions,
    SubscriptionActions
} from '../../../_constants/action-types';
import {GetSubscriptions2Out} from '../../../_model/state-model/mn-sm/subscriber-search-result-state';
import {Employee} from '../../../_model/view_model/IEmployee';
import {DistrictMeteringZone} from "../../../_model/view_model/mn-vm/district-metering-zone";
import {JobWorker} from '../../../_model/view_model/mn-vm/internal-service-vm/job-worker';
import {Kebele} from "../../../_model/view_model/mn-vm/kebele";
import {PressureZone} from "../../../_model/view_model/mn-vm/pressure-zone";
import {Subscriber} from "../../../_model/view_model/mn-vm/subscriber-search-result";
import ISession from "../../../_model/view_model/session-id";
import JobManagerService, {
    AddJobPar,
    AddJobWebPar,
    GetCustomerJobsPar,
    JobData,
    SetWorkFlowDataWebPar,
    UpdateJobWebPar
} from '../../../_services/job.manager.service';
import PayrollService, {GetEmployeePar, GetEmployeesPar, GetOrgUnitsPar} from '../../../_services/payroll.service';
import SubscriberManagementService, {
    BWFGetMeterReadingByPeriodIDPar,
    BWFGetMeterReadings2Out,
    BWFGetMeterReadings2Par,
    GetBillDocumentsPar,
    GetBillPeriodPar,
    GetBillPeriodsPar,
    GetCustomerCreditSchemesOut,
    GetCustomerCreditSchemesPar,
    GetCustomerProfilePar,
    GetCustomersPar,
    GetSubscriber2Par,
    GetSubscriptions2Par,
    GetSubscriptionsPar
} from "../../../_services/subscribermanagment.service";
import errorMessage from "../error.message";
import {searchEmployeeError, searchEmployeeLoading} from "../hr-actions/employee-actions";
import {PaymentCenter} from "../../../_model/view_model/mn-vm/internal-service-vm/cash-handover/payment-center";
import {WorkFlowData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {NotificationTypes, openNotification} from "../../../shared/components/notification/notification";
import {BillPeriod} from "../../../_model/level0/subscriber-managment-type-library/bill-period";
import {IBillDocumentPayload} from "../../../_model/state-model/mn-sm/bill-documents-state";
import {CustomerBillDocument, PeriodicBill} from "../../../_model/level0/subscriber-managment-type-library/bill";
import AccountingService from "../../../_services/accounting.service";
import {DocumentType} from "../../../_model/level0/accounting-type-library/data-structure";
import {BWFMeterReading} from "../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import {IBWFMeterReadingPayload} from "../../../_model/state-model/mn-sm/bwf-meter-reading-state";
import {OrgUnit} from "../../../_model/view_model/hr-vm/orginanizational-unit";
import {Subscription} from "../../../_model/level0/subscriber-managment-type-library/subscription";
import getCurrentTicks from "../../../_helpers/date-util";
import {CustomerProfilePayload} from "../../../_model/state-model/mn-sm/customer-profile-state";

const subscriber_man_service = new SubscriberManagementService()
const job_mgr_service = new JobManagerService()
const payroll_service = new PayrollService()
const accounting_service = new AccountingService()


//#region  CUSTOMER LIST ACTION CREATOR AND ACTIONS

const loadingKebeleList = (message: string) => ({ type: KebeleActions.LOADING, message })
const loadingKebeleListSucccess = (payload: Kebele[]) => ({ type: KebeleActions.SUCCESS, payload })
const loadingKebeleListError = (message: string) => ({ type: KebeleActions.ERROR, message })

const loadingPressureZoneList = (message: string) => ({ type: PressureZoneActions.LOADING, message })
const loadingPressureZoneListSucccess = (payload: PressureZone[]) => ({ type: PressureZoneActions.SUCCESS, payload })
const loadingPressureZoneListError = (message: string) => ({ type: PressureZoneActions.ERROR, message })

const loadingDMZList = (message: string) => ({ type: DMZActions.LOADING, message })
const loadingDMZSucccess = (payload: DistrictMeteringZone[]) => ({ type: DMZActions.SUCCESS, payload })
const loadingDMZError = (message: string) => ({ type: DMZActions.ERROR, message })


const subscriberListLoading = (message: string) => ({ type: SubscriberSearchResultActions.LOADING, message })
const subscriberListSucccess = (payload: GetSubscriptions2Out) => ({ type: SubscriberSearchResultActions.SUCCESS, payload })
const subscriberListError = (message: string) => ({ type: SubscriberSearchResultActions.ERROR, message })
export const subscriberListClear = () => ({ type: SubscriberSearchResultActions.CLEAR })

export const fetchKebeleList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: ISession) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingKebeleList('Loading Kebele List,Please wait...'))
        return await subscriber_man_service.GetAllKebeles(prms)
            .then(res => {
                dispatch(loadingKebeleListSucccess(res.data))
            })
            .catch(error => dispatch(loadingKebeleListError(errorMessage(error))))
    }
}

export const fetchPressureZoneList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: ISession) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingPressureZoneList('Loading Pressure Zone List,Please wait...'))
        return await subscriber_man_service.GetAllDMAs(prms)
            .then(res => dispatch(loadingPressureZoneListSucccess(res.data)))
            .catch(error => dispatch(loadingPressureZoneListError(errorMessage(error))))
    }
}

export const fetchDMZList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: ISession) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingDMZList('Loading DMZ List,Please wait...'))
        return await subscriber_man_service.GetAllDMAs(prms)
            .then(res => dispatch(loadingDMZSucccess(res.data)))
            .catch(error => dispatch(loadingDMZError(errorMessage(error))))
    }
}

export const fetchSubscriberList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetSubscriptions2Par) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(subscriberListLoading('Loading Subscriber List,Please wait...'))
        return await subscriber_man_service.GetSubscriptions2(prms)
            .then(res => {
                dispatch(subscriberListSucccess(res.data))
            })
            .catch(error => dispatch(subscriberListError(errorMessage(error))))
    }
}


export const fetchCustomerList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetCustomersPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(subscriberListLoading('Loading Customer List,Please wait...'))
        return await subscriber_man_service.GetCustomers(prms)
            .then(res => {
                dispatch(subscriberListSucccess(res.data))
            })
            .catch(error => dispatch(subscriberListError(errorMessage(error))))
    }
}




const singleSubscriberLoading = (message: string = '') => ({ type: SingleSubscriberActions.LOADING, message })
const singleSubscriberError = (message: string) => ({ type: SingleSubscriberActions.ERROR, message })
const singleSubscriberSuccess = (payload: Subscriber) => ({ type: SingleSubscriberActions.SUCCESS, payload })
export const fetchSubscriberProfile: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetSubscriptions2Par) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(singleSubscriberLoading())
        return await subscriber_man_service.GetSubscriptions2(prms)
            .then(res => {
                return dispatch(singleSubscriberSuccess(res.data))
            })
            .catch(error => dispatch(singleSubscriberError(errorMessage(error))))
    }
}



const customerProfileLoading = (message: string = '') => ({ type: CustomerProfileActions.LOADING, message })
const customerProfileError = (message: string) => ({ type: CustomerProfileActions.ERROR, message })
const customerProfileSuccess = (payload: CustomerProfilePayload) => ({ type: CustomerProfileActions.SUCCESS, payload })

export const fetchCustomerProfile: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (param: GetCustomerProfilePar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(customerProfileLoading())
        try{
            const customerPar: GetSubscriber2Par = { sessionId: param.sessionId, id:param.customerId}
            const customer : Subscriber =(await subscriber_man_service.GetSubscriber2(customerPar)).data
            const connectionPar: GetSubscriptionsPar = { sessionId: param.sessionId, subscriberID:param.customerId,version:getCurrentTicks() }
            const connections: Subscription[] =(await subscriber_man_service.GetSubscriptions(connectionPar)).data
            const billsPar: GetBillDocumentsPar = { sessionId:param.sessionId,customerID:param.customerId ,connectionID:-1,excludePaid:true,mainTypeID:-1, periodID:-1 }
            const bills: CustomerBillDocument[] = (await subscriber_man_service.GetBillDocuments(billsPar)).data
            const schemesPar: GetCustomerCreditSchemesPar = {sessionId:param.sessionId,customerID:param.customerId }
            const schemesResponse:GetCustomerCreditSchemesOut = (await subscriber_man_service.GetCustomerCreditSchemes(schemesPar)).data
            const schemes = schemesResponse._ret
            const settlement: number[] = schemesResponse.settlement
            const jobsPar: GetCustomerJobsPar = { sessionId:param.sessionId, customerID:param.customerId, onlyAcitve: false }
            const jobs:JobData[] = (await job_mgr_service.GetCustomerJobs(jobsPar)).data
            const payload: CustomerProfilePayload = {
                customer,
                connections,
                bills,
                schemes,
                settlement,
                jobs
            }
            return dispatch(customerProfileSuccess(payload))
        } catch(error){
            return dispatch(customerProfileError(errorMessage(error)));
        }
    }
}







//#endregion

//#region internal services API request 
function pushEmployee(employee: Employee) {
    return { type: SearchEmployeeActions.PUSH, employee }
}


const jobWorkersLoading = (message: string = '') => ({ type: JobWorkerActions.LOADING, message })
const jobWorkersError = (message: string) => ({ type: JobWorkerActions.ERROR, message })
const jobWorkersSuccess = (payload: JobWorker[]) => ({ type: JobWorkerActions.SUCCESS, payload })
export const jobWorkerRemove = (workerId: string ) => ({ type: JobWorkerActions.REMOVE, workerId })
export const fetchAllWorkerList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: ISession) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(searchEmployeeLoading('Loading Workers,Please Wait...'))
        dispatch(jobWorkersLoading('Loading Workers,Please Wait...'))

        try{
            const workerResponse = await job_mgr_service.GetAllWorkers(prms);
            const workers: JobWorker[] = workerResponse.data
            for(const worker of workers){
                dispatch(fetchEmployee({ ID: worker.employeeID, sessionId: prms.sessionId }))
            }
            return dispatch(jobWorkersSuccess(workers));
        } catch (error) {
            dispatch(searchEmployeeError(errorMessage(error)))
            return dispatch(jobWorkersError(errorMessage(error)))
        }
    }
}
export const fetchEmployee: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetEmployeePar) => {
    return async (dispatch: any): Promise<Action> => {
        return await payroll_service.GetEmployee(prms)
            .then(res => {
                return dispatch(pushEmployee(res.data))
            })
            .catch(error => console.log(errorMessage(error)))
    }
}


//#endregion


export const requestAddJob: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: AddJobPar) => {
    openNotification({
        notificationType:NotificationTypes.LOADING,
        message:'Creating Job',
        description:'Registering job please wait...'
    })

    return async (dispatch: any): Promise<Action> => {
        try{
            const res = await job_mgr_service.AddJob(prms)
            openNotification({
                notificationType:NotificationTypes.SUCCESS,
                message:'Success',
                description:'Job Registered successfully'
            })
            return dispatch(pushEmployee(res.data))
        } catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Failed',
                description:errorMessage(error)
            })
            return dispatch(()=>{})
        }
    }
}


export function requestAddJobWeb<T extends WorkFlowData>(prms: AddJobWebPar<T>){
    openNotification({
        message:'Creating Job',
        description:'Registering Job Information please wait',
        notificationType:NotificationTypes.LOADING
    })
    return async (dispatch: any): Promise<Action> => {
        try{
            const response = await job_mgr_service.AddJobWeb(prms)
            openNotification({
                message:'Job Added',
                description:'Job information registered successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(pushEmployee(response.data))
        } catch(error){
            openNotification({
                message:'Failed To Register Job',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return dispatch(()=>{})
        }
    }
}

export function requestUpdateJobWeb<T extends WorkFlowData>(prms: UpdateJobWebPar<T>){
    return async (dispatch: any): Promise<Action | void> => {
        openNotification({
            message:'Updating Job',
            description:'Updating Job Information please wait',
            notificationType:NotificationTypes.LOADING
        })
        try{
            await job_mgr_service.UpdateJobWeb(prms)
            openNotification({
                message:'Job Updates',
                description:'Job information updated successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(()=>{})
        } catch(error){
            openNotification({
                message:'Update Failed',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return dispatch(()=>{})
        }
    }
}

export function requestSetWorkflowDataWeb<T extends WorkFlowData>(prms: SetWorkFlowDataWebPar<T>){
    return async (dispatch: any): Promise<Action | void> => {
        openNotification({
            message:'Updating Job',
            description:'Updating Job Information please wait',
            notificationType:NotificationTypes.LOADING
        })
        try{
            await job_mgr_service.SetWorkFlowDataWeb(prms)
            openNotification({
                message:'Job Updated',
                description:'Job information updated successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            return dispatch(()=>{})
        } catch(error){
            openNotification({
                message:'Update Failed',
                description:errorMessage(error),
                notificationType:NotificationTypes.ERROR
            })
            return dispatch(()=>{})
        }
    }
}



const customerCreditSchemeLoading = (message:string) => ({type:CreditSchemeActions.LOADING,message  })
const customerCreditSchemeError = (message: string) => ({ type:CreditSchemeActions.ERROR,message   })
const customerCreditSchemeSuccess = (payload: GetCustomerCreditSchemesOut) => ({ type:CreditSchemeActions.SUCCESS,payload   })
export const fetchCustomerCreditSchemes: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetCustomerCreditSchemesPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(customerCreditSchemeLoading('Fetching customer credit scheme, Please wait ... '))
        try{
            const responseScheme = ( await subscriber_man_service.GetCustomerCreditSchemes(prms) ).data
            return dispatch(customerCreditSchemeSuccess(responseScheme))
        } catch(error){
            return  dispatch(customerCreditSchemeError(errorMessage((error))));
        }
    }
}




//#region INTERNAL SERVICES ACTION CREATOR


const  loadPaymentCentersLoading =( message : string = 'loading')=>({ type:PaymentCenterActions.LOADING,message })
const  loadPaymentCentersError =(message: string )=>({ type: PaymentCenterActions.ERROR,message })
const  loadPaymentCentersSuccess =(payload:PaymentCenter[])=>({type:PaymentCenterActions.SUCCESS,payload})


export const fetchPaymentCenters: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: ISession) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadPaymentCentersLoading('Fetching payment centers, Please wait ... '))
        return await subscriber_man_service.GetAllPaymentCenters(prms)
            .then(res => {
                return dispatch(loadPaymentCentersSuccess(res.data))
            })
            .catch( error => dispatch(loadPaymentCentersError(errorMessage((error)))))
    }
}
//#endregion

//#region SUBSCRIBER MANAGEMENT SERVICES ACTION CREATOR

const  billPeriodsLoading =( message : string = 'loading')=>({ type:BillPeriodActions.LOADING,message })
const  billPeriodsError =(message: string )=>({ type: BillPeriodActions.ERROR,message })
const  billPeriodsSuccess =(payload:BillPeriod[])=>({type:BillPeriodActions.SUCCESS,payload})
export const fetchBillPeriods: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetBillPeriodsPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            dispatch(billPeriodsLoading('Fetching payment centers, Please wait ... '))
            const periods = (await subscriber_man_service.GetBillPeriods(prms)).data
            return dispatch(billPeriodsSuccess(periods))
        } catch(error){
            return dispatch(billPeriodsError(errorMessage((error))))
        }
    }
}



const  subscriptionsLoading =( message : string = 'loading')=>({ type:SubscriptionActions.LOADING,message })
const  subscriptionsError =(message: string )=>({ type: SubscriptionActions.ERROR,message })
const  subscriptionsSuccess =(payload:Subscription[])=>({type:SubscriptionActions.SUCCESS,payload})
export const fetchCustomerSubscriptions: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetSubscriptionsPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            dispatch(subscriptionsLoading('Fetching customer subscriptions, Please wait ... '))
            const subscriptions: Subscription[] = (await subscriber_man_service.GetSubscriptions(prms)).data
            return dispatch(subscriptionsSuccess(subscriptions))
        } catch(error){
            return dispatch(subscriptionsError(errorMessage((error))))
        }
    }
}




const  billDocumentsLoading =( message : string = 'loading')=>({ type:BillDocumentActions.LOADING,message })
const  billDocumentsError =(message: string )=>({ type: BillDocumentActions.ERROR,message })
const  billDocumentsSuccess =(payload:IBillDocumentPayload[])=>({type:BillDocumentActions.SUCCESS,payload})
export const fetchCustomerBillDocuments: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (prms: GetBillDocumentsPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            dispatch(billDocumentsLoading('Fetching payment centers, Please wait ... '))
            const billDocuments:CustomerBillDocument[] = (await subscriber_man_service.GetBillDocuments(prms)).data
            let customerBills:IBillDocumentPayload[] = []
            for( const billDocument of billDocuments )
            {
                const documentType:DocumentType = ( await accounting_service.GetDocumentTypeByID({ sessionId:prms.sessionId,typeID:billDocument.documentTypeID})).data
                const pariodicBill: PeriodicBill = billDocument as PeriodicBill
                let billPeriod:BillPeriod | undefined = undefined
                if( pariodicBill.period !== undefined && pariodicBill.period.id)
                billPeriod = ( await subscriber_man_service.GetBillPeriod({sessionId:prms.sessionId,periodID:pariodicBill.period.id })).data
                const customerBill: IBillDocumentPayload = { billDocument,documentType, billPeriod }
                customerBills = [...customerBills , customerBill ]
            }
            return dispatch(billDocumentsSuccess(customerBills))
        } catch(error){
            return dispatch(billDocumentsError(errorMessage((error))))
        }
    }
}



const  bwfMeterReadingLoading =( message : string = 'loading')=>({ type:BWFMeterReadingActions.LOADING,message })
const  bwfMeterReadingError =(message: string )=>({ type: BWFMeterReadingActions.ERROR,message })
const  bwfMeterReadingSuccess =(payload:IBWFMeterReadingPayload)=>({type:BWFMeterReadingActions.SUCCESS,payload})
export const fetchBWFMeterReading: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: BWFGetMeterReadingByPeriodIDPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            dispatch(bwfMeterReadingLoading('Fetching Meter Reading, Please wait ... '))
            const reading : BWFMeterReading = (await subscriber_man_service.BWFGetMeterReadingByPeriodID(params)).data
            if (reading === null) { throw new Error("Reading is empty") }
            const billPeriodparams : GetBillPeriodPar ={
                sessionId: params.sessionId,
                periodID: reading.periodID
            }
            const period : BillPeriod = ( await subscriber_man_service.GetBillPeriod(billPeriodparams)).data
            return dispatch(bwfMeterReadingSuccess({reading,period}))
        } catch(error){
            return dispatch(bwfMeterReadingError(errorMessage((error))))
        }
    }
}



const  bwfMeterReadingListLoading =( message : string = 'loading')=>({ type:BWFMeterReadingListActions.LOADING,message })
const  bwfMeterReadingListError =(message: string )=>({ type: BWFMeterReadingListActions.ERROR,message })
const  bwfMeterReadingListSuccess =(payload:IBWFMeterReadingPayload[])=>({type:BWFMeterReadingListActions.SUCCESS,payload})

export const fetchBWFMeterReadingList: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: BWFGetMeterReadings2Par) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            dispatch(bwfMeterReadingListLoading('Fetching Meter Reading List, Please wait ... '))
            let readingList:IBWFMeterReadingPayload[] = []
            const readingResponse : BWFGetMeterReadings2Out = (await subscriber_man_service.BWFGetMeterReadings2(params)).data
            for(const reading of readingResponse._ret){
                const billPeriodparams : GetBillPeriodPar ={
                    sessionId: params.sessionId,
                    periodID: reading.periodID
                }
                const period : BillPeriod = ( await subscriber_man_service.GetBillPeriod(billPeriodparams)).data
                const readingItem: IBWFMeterReadingPayload = { reading, period }
                readingList = [...readingList,readingItem]
            }
            return dispatch(bwfMeterReadingListSuccess(readingList))
        } catch(error){
            console.log('ERROR ',error)
            return dispatch(bwfMeterReadingListError(errorMessage((error))))
        }
    }
}



//#endregion

//#region PAYROLL ACTIONS
const  organizationUnitsLoading =( message : string = 'loading')=>({ type:OrgUnitsActions.LOADING,message })
const  organizationUnitsError =(message: string )=>({ type: OrgUnitsActions.ERROR,message })
const  organizationUnitsSuccess =(payload:OrgUnit[])=>({type:OrgUnitsActions.SUCCESS,payload})
const  organizationUnitsAppend =(payload:OrgUnit[])=>({type:OrgUnitsActions.APPEND,payload})
const  organizationUnitsDone =()=>({type:OrgUnitsActions.DONE})


const  employeesLoading =( message : string = 'loading')=>({ type: EmployeeActions.LOADING,message })
const  employeessError =(message: string )=>({ type: EmployeeActions.ERROR,message })
const  employeessSuccess =(payload:Employee[])=>({type: EmployeeActions.SUCCESS,payload})
const  employeessDone =()=>({type: EmployeeActions.DONE})
const  employeesAppend =(payload:Employee[])=>({type: EmployeeActions.APPEND,payload})

export const fetchOrganizationalUnitsAndEmployee: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params:GetOrgUnitsPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            dispatch(organizationUnitsLoading('Fetching organizational units, Please wait ... '))
            dispatch(employeesLoading('Fetching employees, Please wait ... '))
            const orgUnitsResponse:OrgUnit[] = (await payroll_service.GetOrgUnits(params)).data
            const recursiveApiCall = async (organizationUnits:OrgUnit[])=>{
                for(const unit of organizationUnits)
                {
                    dispatch(organizationUnitsAppend([unit]))

                    const employeeparams: GetEmployeesPar = {
                        sessionId: params.sessionId,
                        orgID: unit.id,
                        includeSubOrg: false
                    }
                    const employeeResponse: Employee[] = (await payroll_service.GetEmployees(employeeparams)).data
                    dispatch(employeesAppend(employeeResponse))

                    const unitParams : GetOrgUnitsPar ={
                        pid:unit.id,
                        sessionId:params.sessionId
                    }
                    const apiCallResponse:OrgUnit[] = (await payroll_service.GetOrgUnits(unitParams)).data
                    if(apiCallResponse.length > 0)
                       await recursiveApiCall(apiCallResponse)
                }
            }
            await recursiveApiCall(orgUnitsResponse)
            dispatch(employeessDone())
            return dispatch(organizationUnitsDone())
        } catch(error){
            console.log('ERROR ',error)
            return dispatch(organizationUnitsError(errorMessage((error))))
        }
    }
}

//#endregion
