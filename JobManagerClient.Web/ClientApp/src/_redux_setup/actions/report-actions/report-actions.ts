import {
    MeterReaderEmployeeActions,
    ReportActions,
    ReportHTMLActions,
    ReportTypeActions
} from "../../../_constants/action-types";
import {ReportStatePayload} from "../../../_model/state-model/r-sm/report-state";
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import errorMessage from "../error.message";
import ISession from "../../../_model/view_model/session-id";
import AccountingService, {
    EvaluateEHTML2Out,
    EvaluateEHTML2Par,
    EvaluateEHTML3WebOut,
    EvaluateEHTML3WebPar,
    GetAllReportTypesPar,
    GetFunctionDocumentationsPar,
    SaveEHTMLDataPar
} from "../../../_services/accounting.service";
import {FunctionDocumentation} from "../../../_model/level0/formula-evaluator/function-documentation";
import {ReportDefination, ReportType} from "../../../_model/level0/accounting-type-library/ehtml";
import {ReportHTMLStatePayload} from "../../../_model/state-model/r-sm/report-html-state";
import {DataType} from "../../../_model/level0/formula-evaluator/data-type";
import {NotificationTypes, openNotification} from "../../../shared/components/notification/notification";
import {pushReportDefinition} from "../report-definition-actions";
import SubscriberManagementService, {GetAllMeterReaderEmployeesPar} from "../../../_services/subscribermanagment.service";
import {MeterReaderEmployee} from "../../../_model/level0/subscriber-managment-type-library/meter-reader-employee";
import {
    ImportReportDefinitionPar,
    WrapperReportServices
} from "../../../_services/wrapper-api-services/wrapper-report-services";

const accounting_service = new AccountingService()
const subscriber_manager_actions = new SubscriberManagementService()
const wrapper_report_service = new WrapperReportServices()

export const loadingReport =(message:string)=>({ type:ReportActions.LOADING , message })
const errorReport =(message:string)=>({ type:ReportActions.ERROR , message })
const successReport =(payload:ReportStatePayload)=>({ type:ReportActions.SUCCESS , payload })
export interface FetchReportPar extends ISession { definition: ReportDefination}
export const fetchReport: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: FetchReportPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingReport('Loading report information,Please wait...'))
        try{
            const  functionDocsPar:GetFunctionDocumentationsPar= {
                sessionId:params.sessionId,
                reportTypeID: params.definition.reportTypeID
            }
            const functionDocumentations: FunctionDocumentation[] = ( await accounting_service.GetFunctionDocumentations(functionDocsPar) ).data
            return  dispatch(successReport({functionDocumentations,definition:params.definition}))
        }
        catch(error){
            return dispatch(errorReport(errorMessage(error)));
        }
    }
}
export const requestImportReportDefinition: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: ImportReportDefinitionPar & ISession) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingReport('Loading report information,Please wait...'))
        try{
            const  functionDocsPar:GetFunctionDocumentationsPar= {
                sessionId:params.sessionId,
                reportTypeID: params.defination.reportTypeID
            }
            const functionDocumentations: FunctionDocumentation[] = ( await accounting_service.GetFunctionDocumentations(functionDocsPar) ).data
            const extractedReportDefinition: ReportDefination = ( await wrapper_report_service.ImportReportDefinition({defination:params.defination,base64ReportFile:params.base64ReportFile}) ).data
            return  dispatch(successReport({functionDocumentations,definition:extractedReportDefinition}))
        }
        catch(error){
            return dispatch(errorReport(errorMessage(error)));
        }
    }
}





export const loadingReportTypes =(message:string)=>({ type:ReportTypeActions.LOADING , message })
const errorReportTypes =(message:string)=>({ type:ReportTypeActions.ERROR , message })
const successReportTypes =(payload:ReportType[])=>({ type:ReportTypeActions.SUCCESS , payload })
export const fetchReportTypes: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: GetAllReportTypesPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingReportTypes('Loading report types,Please wait...'))
        try{
            const reportTypes:ReportType[] = (await accounting_service.GetAllReportTypes(params)).data
            return  dispatch(successReportTypes(reportTypes))
        }
        catch(error){
            return dispatch(errorReportTypes(errorMessage(error)));
        }
    }
}


export const clearReportHTML =()=>({ type:ReportHTMLActions.CLEAR })
const loadingReportHTML =(message:string)=>({ type:ReportHTMLActions.LOADING , message })
const errorReportHTML =(message:string)=>({ type:ReportHTMLActions.ERROR , message })
const successReportHTML =(payload:ReportHTMLStatePayload)=>({ type:ReportHTMLActions.SUCCESS , payload })
export const requestEvaluateFormula3Web: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: EvaluateEHTML3WebPar) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingReportHTML('Loading report evaluation,Please wait...'))
        try{
            const ehtml3WebOut:EvaluateEHTML3WebOut = (await accounting_service.EvaluateEHTML3Web(params)).data
            const reportHtmlPayload: ReportHTMLStatePayload = {
                htmlRenderString:`<h3>${DataType[ehtml3WebOut.edata.type]}</h3>`,
                htmlCodeString: ehtml3WebOut.edataString,
                edata: ehtml3WebOut.edata
            }
            return  dispatch(successReportHTML(reportHtmlPayload))
        }
        catch(error){
            return dispatch(errorReportHTML(errorMessage(error)));
        }
    }
}
export const requestEvaluate2: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: EvaluateEHTML2Par) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(loadingReportHTML('Loading report,Please wait...'))
        try{
            const evaluate2Out:EvaluateEHTML2Out = (await accounting_service.EvaluateEHTML2(params)).data

            const reportHtmlPayload: ReportHTMLStatePayload = {
                htmlRenderString:`${evaluate2Out.headerItems && evaluate2Out.headerItems}${evaluate2Out._ret}`,
                htmlCodeString:'',
            }
             return  dispatch(successReportHTML(reportHtmlPayload))
        }
        catch(error){
            return dispatch(errorReportHTML(errorMessage(error)));
        }
    }
}



export const requestSaveReportDefinition: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: SaveEHTMLDataPar) => {
    return async (dispatch: any): Promise<Action> => {
        openNotification({
            notificationType:NotificationTypes.LOADING,
            message:'Loading',
            description:'Saving report definition changes,please wait...'
        })
        try{
            const reportDefinitionId:number = (await accounting_service.SaveEHTMLData(params)).data
            const  functionDocsPar:GetFunctionDocumentationsPar= {
                sessionId:params.sessionId,
                reportTypeID: params.r.reportTypeID
            }
            const functionDocumentations: FunctionDocumentation[] = ( await accounting_service.GetFunctionDocumentations(functionDocsPar) ).data
            openNotification({
                notificationType:NotificationTypes.SUCCESS,
                message:'Saved!',
                description:'Report definition changes saved successfully!'
            })

            /**
             * dispatching the successReport here will make is possible to keep
             * update of registered report definition to be in sync,
             *
             * dispatching the pushReportDefinition here will make is possible to keep
             * newly of registered report definition to be in sync,
             *
             * both dispatch action are !important
             */
            dispatch(pushReportDefinition({...params.r,id: reportDefinitionId}))
            return  dispatch(successReport({functionDocumentations,definition:{...params.r,id:reportDefinitionId}}))
        }
        catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'ERRNO',
                description:errorMessage(error)
            })
            return dispatch({type:'NO ACTION'});
        }
    }
}


const loadingMeterReaderEmployees = (message:string)=>({ type: MeterReaderEmployeeActions.LOADING , message })
const successMeterReaderEmployees = (payload:MeterReaderEmployee[])=>({ type: MeterReaderEmployeeActions.SUCCESS,payload })
const errorMeterReaderEmployees = (message:string)=>({ type: MeterReaderEmployeeActions.ERROR,message })
export const fetchAllMeterReaderEmployees: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params: GetAllMeterReaderEmployeesPar) => {
    return async (dispatch: any): Promise<Action> => {
        try{
            dispatch(loadingMeterReaderEmployees('Loading meter reader employees.'))
            const meterReaderEmployees:MeterReaderEmployee[] = (await subscriber_manager_actions.GetAllMeterReaderEmployees(params)).data
            return  dispatch(successMeterReaderEmployees(meterReaderEmployees))
        }
        catch(error){
            return dispatch(errorMeterReaderEmployees(errorMessage(error)));
        }
    }
}
