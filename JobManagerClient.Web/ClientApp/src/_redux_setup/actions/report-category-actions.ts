import {ReportCategorieActions} from "../../_constants/action-types";
import {ReportCategory} from "../../_model/view_model/report-category";
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import errorMessage from "./error.message";
import AccountingService, {
    DeleteReportCategoryPar, DeleteReportPar,
    GetReportByCategoryPar,
    SaveReportCategoryPar
} from "../../_services/accounting.service";
import {
    clearReportDefintions, popReportDefinition,
    reportDefByCatagorySuccess,
    requestReportDefByCatagory
} from "./report-definition-actions";
import {ReportDefination} from "../../_model/level0/accounting-type-library/ehtml";
import {NotificationTypes, openNotification} from "../../shared/components/notification/notification";

function requestReportCategory(message:string){
    return {type:ReportCategorieActions.REPORT_CATAGORY_LOADING,message}
}


function pushReportCategories(payload:ReportCategory){
    return {type:ReportCategorieActions.PUSH,payload}
}

function popReportCategory(payload:number){
    return {type:ReportCategorieActions.POP,payload}
}


function loadingReportCartegorySuccess(payload:ReportCategory[]){
    return {type:ReportCategorieActions.REPORT_CATAGORY_SUCCESS,payload}
}

function loadingReportCartegoryError(message:string){
    return {type:ReportCategorieActions.REPORT_CATAGORY_ERROR,message}
}

const account_service = new AccountingService()

export const fetchReportCategoriesAndDefination: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (sessionId : string) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(requestReportCategory('Fetching Report Categories,Please wait'))
        dispatch(clearReportDefintions())
        dispatch(requestReportDefByCatagory('Loading Report Defintions'))

        try{
            const rootReportCategories:ReportCategory[] = (await account_service.getReportCategories({sessionId,pid:-1}) ).data
            let descendantReportCategories: ReportCategory[] = []
            for(const reportCategory of rootReportCategories){
                let categories: ReportCategory[] = []
                const descendantReportCategoryLoader=async (category : ReportCategory) => {
                    const tempCategories: ReportCategory[] = (await account_service.getReportCategories({sessionId,pid:category.id}) ).data
                    categories = [...categories, ...tempCategories]
                    for(const cat  of tempCategories )
                        await descendantReportCategoryLoader(cat)
                }
                await descendantReportCategoryLoader(reportCategory);
                descendantReportCategories = [...descendantReportCategories,...categories]
            }
            const allReportCategories= [ ...rootReportCategories,...descendantReportCategories ]
            let reportDefinitions : ReportDefination[] = []
            for(const reportCategory of allReportCategories){
                const reportDefParams: GetReportByCategoryPar ={
                    sessionId,
                    catID: reportCategory.id
                }
                const definitions: ReportDefination[] = (await account_service.getReportTypesByCatagory(reportDefParams)).data
                reportDefinitions = [...reportDefinitions,...definitions]
            }
            dispatch(reportDefByCatagorySuccess(reportDefinitions))
            return dispatch(loadingReportCartegorySuccess(allReportCategories))
        }catch(error){
            return dispatch(loadingReportCartegoryError(errorMessage(error)))
        }
    }
}


export const requestSaveReportCategory: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params : SaveReportCategoryPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            openNotification({
                notificationType: NotificationTypes.LOADING,
                message: 'Saving',
                description: 'Saving report category, please wait ...'
            })
            const reportCategoryId: number = (await account_service.SaveReportCategory(params)).data
            openNotification({
                notificationType: NotificationTypes.SUCCESS,
                message: 'Saved',
                description: 'report category saved successfully'
            })
            return dispatch(pushReportCategories({...params.cat, id: reportCategoryId}))
        } catch (error) {
            openNotification({
                notificationType: NotificationTypes.ERROR,
                message: 'Failed To save report category',
                description: errorMessage(error)
            })
            return dispatch({type: 'NO ACTION'})
        }
    }
}


export const requestDeleteReportCategory: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params : DeleteReportCategoryPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            openNotification({
                notificationType: NotificationTypes.LOADING,
                message: 'Deleting',
                description: 'Removing report category, please wait ...'
            })
            await account_service.DeleteReportCategory(params)
            openNotification({
                notificationType: NotificationTypes.SUCCESS,
                message: 'Deleted',
                description: 'report category deleted successfully'
            })
            return dispatch(popReportCategory(params.catID))
        } catch (error) {
            openNotification({
                notificationType: NotificationTypes.ERROR,
                message: 'Failed To delete report category',
                description: errorMessage(error)
            })
            return dispatch({type: 'NO ACTION'})
        }
    }
}


export const requestDeleteReportDefinition: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (params : DeleteReportPar) => {
    return async (dispatch: any): Promise<Action> => {
        try {
            openNotification({
                notificationType: NotificationTypes.LOADING,
                message: 'Deleting',
                description: 'Removing report definition, please wait ...'
            })
            await account_service.DeleteReport(params)
            openNotification({
                notificationType: NotificationTypes.SUCCESS,
                message: 'Deleted',
                description: 'report category deleted successfully'
            })
            return dispatch(popReportDefinition(params.reportID))
        } catch (error) {
            openNotification({
                notificationType: NotificationTypes.ERROR,
                message: 'Failed To delete report definition',
                description: errorMessage(error)
            })
            return dispatch({type: 'NO ACTION'})
        }
    }
}




/*export const fetchReportCategories: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (sessionId : string) => {
    return async (dispatch: any): Promise<Action> => {
        dispatch(requestReportCategory('Fetching Report Categories,Please wait'))
        dispatch(clearReportDefintions())
        return await account_service.getReportCategories({sessionId,pid:-1})
            .then(res => {
                dispatch(loadingReportCartegorySuccess(res.data))
                let repCat:ReportCategory[] =res.data
                dispatch(requestReportDefByCatagory('Loading Report Defintions'))
                repCat.forEach(cat=>dispatch(fetchReportDefinitionsByCategory({catId:cat.id,sessionId})))
                return res.data
            })
            .catch(error => {
                dispatch(loadingReportCartegoryError(errorMessage(error)))
            })
    }
}*/

