import {ReportDefinitionActions} from "../../_constants/action-types";
import {Action, ActionCreator} from "redux";
import {ThunkAction} from "redux-thunk";
import errorMessage from "./error.message";
import AccountingService, {
    GetReportByCategoryPar
} from "../../_services/accounting.service";
import {ReportDefination} from "../../_model/level0/accounting-type-library/ehtml";


export function requestReportDefByCatagory(message:string){
    return {type:ReportDefinitionActions.REPORT_DEFINITION_LOADING,message}
}


export function clearReportDefintions(){
    return {type:ReportDefinitionActions.REPORT_DEFINITION_CLEAR}
}

export function popReportDefinition(payload: number){
    return {type:ReportDefinitionActions.POP,payload}
}

export const pushReportDefinition = (payload : ReportDefination) => ({ type: ReportDefinitionActions.PUSH,payload })

export function reportDefByCatagorySuccess(payload:ReportDefination[]){
    return {type:ReportDefinitionActions.REPORT_DEFINITION_SUCCESS,payload}
}

function reportDefByCatagoryError(message:string){
    return {type:ReportDefinitionActions.REPORT_DEFINITION_ERROR,message}
}


const account_service = new AccountingService()

export const fetchReportDefinitionsByCategory: ActionCreator<ThunkAction<Promise<Action>, any, void, any>> = (reportParams : GetReportByCategoryPar) => {
    return async (dispatch: any): Promise<Action> => {
        return await account_service.getReportTypesByCatagory(reportParams)
            .then(res => {
                return dispatch(reportDefByCatagorySuccess(res.data))
            })
            .catch(error => dispatch(reportDefByCatagoryError(errorMessage(error))))
    }
}

