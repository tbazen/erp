import { Reducer } from 'redux'
import {ActiveJobDetailInformationActions} from "../../../_constants/action-types";
import {
    ActiveJobDetailInformationState,
    IActiveJobDetailPayload
} from "../../../_model/state-model/aj-sm/active-job-detail-information-state";

const initialState: ActiveJobDetailInformationState = {
    loading : false,
    error : false,
    message :'',
    payload:undefined
}

const loading: ActiveJobDetailInformationState = {...initialState,loading:true}
const success: ActiveJobDetailInformationState = {...initialState,loading:false}
const error: ActiveJobDetailInformationState = {...initialState,error:true}

export const activeJobDetailInformationReducer: Reducer<ActiveJobDetailInformationState> = (state: ActiveJobDetailInformationState = initialState,action): ActiveJobDetailInformationState => {
    switch (action.type) {
        case ActiveJobDetailInformationActions.LOADING:
            return {...loading,message:action.message}
        case ActiveJobDetailInformationActions.SUCCESS:
            return {...success,payload:action.payload}
        case ActiveJobDetailInformationActions.ERROR:
            return {...error,message:action.message}
        case ActiveJobDetailInformationActions.UPDATE_APPOINTMENT:
            if(state.payload !== undefined){
                const payloadCpy:IActiveJobDetailPayload = { ...state.payload,appointments:action.payload}
                return {...state,payload:payloadCpy}
            }
            return state
        case ActiveJobDetailInformationActions.UPDATE_STATUS:
            if(state.payload !== undefined){
                const payloadCpy:IActiveJobDetailPayload = { ...state.payload,status:action.status,nextStatus:action.nextStatus,previousStatus: action.previousStatus }
                return {...state,payload:payloadCpy}
            }
            return state
        default:
            return state
    }
}
