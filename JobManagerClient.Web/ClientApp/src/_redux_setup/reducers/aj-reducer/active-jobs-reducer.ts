import {Reducer } from 'redux'
import {JobDataState} from "../../../_model/state-model/aj-sm/job-data-state";
import {ActiveJobsActions, JobDataActions} from "../../../_constants/action-types";
import {ActiveJobsState} from "../../../_model/state-model/aj-sm/active-jobs-state";

const initialState: ActiveJobsState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}
const loading: ActiveJobsState = {...initialState,loading:true}
const success: ActiveJobsState = {...initialState,loading:false}
const error: ActiveJobsState = {...initialState,error:true}

export const activeJobsReducer: Reducer<ActiveJobsState> = (state: ActiveJobsState = initialState,action): ActiveJobsState => {
    switch (action.type) {
        case ActiveJobsActions.LOADING:
            return {...loading,message:action.message}

        case ActiveJobsActions.SUCCESS:
            return {...success,payload:action.payload}

        case ActiveJobsActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
