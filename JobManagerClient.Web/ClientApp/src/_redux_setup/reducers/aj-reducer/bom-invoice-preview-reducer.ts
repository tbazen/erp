import { Reducer } from 'redux'
import {BomInvoicePreviewState} from "../../../_model/state-model/aj-sm/bom-invoice-preview-state";
import {BOMInvoicePreviewActions} from "../../../_constants/action-types";

const initialState: BomInvoicePreviewState = {
    loading: false,
    error: false,
    message:'',
    previewHtml:''
}

const loading: BomInvoicePreviewState = {...initialState,loading:true}
const success: BomInvoicePreviewState = {...initialState,loading:false}
const error: BomInvoicePreviewState = {...initialState,error:true}

export const bomInvoicePreviewReducer: Reducer<BomInvoicePreviewState> = (state: BomInvoicePreviewState = initialState,action): BomInvoicePreviewState => {
    switch (action.type) {
        case BOMInvoicePreviewActions.LOADING:
            return {...loading,message:action.message}

        case BOMInvoicePreviewActions.SUCCESS:
            return {...success,previewHtml:action.payload}

        case BOMInvoicePreviewActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
