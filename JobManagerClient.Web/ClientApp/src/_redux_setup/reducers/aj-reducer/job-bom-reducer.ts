import { Reducer } from 'redux'
import { JobBomState } from '../../../_model/state-model/aj-sm/job-bom-state';
import { JobBOMActions } from '../../../_constants/action-types';

const initialState: JobBomState = {
    loading: false,
    error: false,
    message:'',
    payload: undefined
}

const loading: JobBomState = {...initialState,loading:true}
const success: JobBomState = {...initialState,loading:false}
const error: JobBomState = {...initialState,error:true}

export const jobBOMReducer: Reducer<JobBomState> = (state: JobBomState = initialState,action): JobBomState => {
    switch (action.type) {
        case JobBOMActions.LOADING:
            return {...loading,message:action.message}

        case JobBOMActions.SUCCESS:
            return {...success,payload:action.payload}

        case JobBOMActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
