import {Reducer } from 'redux'
import {JobDataState} from "../../../_model/state-model/aj-sm/job-data-state";
import {JobDataActions} from "../../../_constants/action-types";

const initialState: JobDataState = {
    loading : false,
    error : false,
    message :'',
    jobs:[]
}
const loading: JobDataState = {...initialState,loading:true}
const success: JobDataState = {...initialState,loading:false}
const error: JobDataState = {...initialState,error:true}

export const jobDataReducer: Reducer<JobDataState> = (state: JobDataState = initialState,action): JobDataState => {
    switch (action.type) {
        case JobDataActions.LOADING:
            return {...loading,message:action.message}

        case JobDataActions.SUCCESS:
            return {...success,jobs:action.payload}

        case JobDataActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
