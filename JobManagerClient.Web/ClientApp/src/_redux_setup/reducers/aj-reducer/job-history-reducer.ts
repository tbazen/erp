import { Reducer } from 'redux'
import { JobHistoryActions} from "../../../_constants/action-types";
import { JobHistoryState } from '../../../_model/state-model/aj-sm/job-history-state';

const initialState: JobHistoryState = {
    loading: false,
    error: false,
    message:'',
    payload: []
}

const loading: JobHistoryState = {...initialState,loading:true}
const success: JobHistoryState = {...initialState,loading:false}
const error: JobHistoryState = {...initialState,error:true}

export const jobHistoryReducer: Reducer<JobHistoryState> = (state: JobHistoryState = initialState,action): JobHistoryState => {
    switch (action.type) {
        case JobHistoryActions.LOADING:
            return {...loading,message:action.message}

        case JobHistoryActions.SUCCESS:
            return {...success,payload:action.payload}

        case JobHistoryActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
