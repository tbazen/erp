import { Reducer } from 'redux'
import {JobDataState} from "../../../_model/state-model/aj-sm/job-data-state";
import {JobDataActions, MeasureUnitActions} from "../../../_constants/action-types";
import {MeasureUnitState} from "../../../_model/state-model/aj-sm/measure-unit-state";

const initialState: MeasureUnitState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}

const loading: MeasureUnitState = {...initialState,loading:true}
const success: MeasureUnitState = {...initialState,loading:false}
const error: MeasureUnitState = {...initialState,error:true}

export const measureUnitReducer: Reducer<MeasureUnitState> = (state: MeasureUnitState = initialState,action): MeasureUnitState => {
    switch (action.type) {
        case MeasureUnitActions.LOADING:
            return {...loading,message:action.message}

        case MeasureUnitActions.SUCCESS:
            return {...success,payload:action.payload}

        case MeasureUnitActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
