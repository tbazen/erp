import {Reducer } from 'redux'
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import {WorkflowDataActions} from "../../../_constants/action-types";

const initialState: WorkflowDataState = {
    loading : false,
    error : false,
    message :'',
    payload:undefined
}
const loading: WorkflowDataState = {...initialState,loading:true}
const success: WorkflowDataState = {...initialState,loading:false}
const error: WorkflowDataState = {...initialState,error:true}

export const workflowDataReducer: Reducer<WorkflowDataState> = (state: WorkflowDataState = initialState,action): WorkflowDataState => {
    switch (action.type) {
        case WorkflowDataActions.LOADING:
            return {...loading,message:action.message}

        case WorkflowDataActions.SUCCESS:
            return {...success,payload:action.payload}

        case WorkflowDataActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
