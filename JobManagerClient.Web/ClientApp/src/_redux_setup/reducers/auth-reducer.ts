import { Reducer } from 'redux'
import { AuthenticationState } from '../../_model/state-model/auth-state';
import { AuthActions } from '../../_constants/action-types';

const initialState: AuthenticationState = {
  isCheckingSession:true,
  loading:false,
  error:false,
  message:'',
  authenticated:false,
  sessionId:'',
  userName:''
}

const sessionCheck: AuthenticationState = {...initialState,isCheckingSession:true}
const loading: AuthenticationState = {...initialState,loading:true,isCheckingSession:false}
const success: AuthenticationState = {...initialState,isCheckingSession:false,authenticated:true}
const error: AuthenticationState = {...initialState,error:true,isCheckingSession:false}
const logout: AuthenticationState = {...initialState,isCheckingSession:false}

export const authReducer: Reducer<AuthenticationState> = (state: AuthenticationState = initialState,action): AuthenticationState => {
  switch (action.type) {
    case AuthActions.AUTH_REQUEST:
        return {...loading,message:action.message}

    case AuthActions.AUTH_REQUEST_SUCCESS:
      return {...success,sessionId:action.sessionId,userName:action.userName}

    case AuthActions.AUTH_REQUEST_ERROR:
        return {...error,message:action.message}

    case AuthActions.LOGOUT:
        return {...logout}

    case AuthActions.SESSION_CHECKING:
        return {...sessionCheck}
    default:
      return state
  }
}
