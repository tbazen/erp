import { Reducer } from 'redux'
import {BreadcrumdState} from "../../_model/state-model/breadcrumd-state";
import {BreadcrumbActions} from "../../_constants/action-types";

const initialState:BreadcrumdState = {paths:[]}

export const breadcrumbReducer: Reducer<BreadcrumdState> = (state: BreadcrumdState = initialState,action): BreadcrumdState => {
    switch (action.type) {
        case BreadcrumbActions.PUSH_BC_PATHS:
            return {...state,paths:action.payload}
        default:
            return state
    }
}
