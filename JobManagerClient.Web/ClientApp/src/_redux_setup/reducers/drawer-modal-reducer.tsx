import React,{Fragment} from 'react'
import { DrawerModalState } from '../../_model/state-model/drawer-dialog-state'
import { DrawerModalActions } from '../../_constants/action-types';
import { Reducer } from 'redux';

const init:DrawerModalState = {
    component:<Fragment/>,
    visible:false
}
const openState ={...init, visible:true}


export const drawerModalReducer: Reducer<DrawerModalState> = (state: DrawerModalState = init,action): DrawerModalState => {
    switch (action.type) {
        case DrawerModalActions.OPEN:
            return {...openState,component:action.payload}
        case DrawerModalActions.CLOSE:
            return {...init}
        default:
            return state
    }
}