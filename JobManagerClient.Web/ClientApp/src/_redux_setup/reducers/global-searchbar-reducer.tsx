import { Reducer } from 'redux';
import { GlobalSearchBarState } from '../../_model/state-model/global-search-bar';
import { SearchBarActions } from '../../_constants/action-types';

const init:GlobalSearchBarState = {
    visible:false,
    placeholder:'',
    changeHandler:()=>{}
}
const openState ={...init, visible:true}


export const globalSearchbarReducer: Reducer<GlobalSearchBarState> = (state: GlobalSearchBarState = init,action): GlobalSearchBarState => {
    switch (action.type) {
        case SearchBarActions.VISIBLE:
            return {...openState,placeholder:action.placeholder,changeHandler:action.handler}
        case SearchBarActions.HIDDEN:
            return {...init}
        default:
            return state
    }
}