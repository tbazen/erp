import { Reducer } from 'redux'
import {OrganizationalUnitState} from "../../../_model/state-model/hr-sm/organizational-unit-state";
import {OrgUnitsActions} from "../../../_constants/action-types";

const initialState: OrganizationalUnitState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}
const loading: OrganizationalUnitState = {...initialState,loading:true}
const success: OrganizationalUnitState = {...initialState,loading:false}
const error: OrganizationalUnitState = {...initialState,error:true}

export const organizationalUnitReducer: Reducer<OrganizationalUnitState> = (state: OrganizationalUnitState = initialState,action): OrganizationalUnitState => {
    switch (action.type) {
        case OrgUnitsActions.LOADING:
            return {...loading,message:action.message}

        case OrgUnitsActions.SUCCESS:
            return {...success,payload:action.payload}

        case OrgUnitsActions.DONE:
            return {...state,loading:false,error:false,message:''}

        case OrgUnitsActions.ERROR:
            return {...error,message:action.message}

        case OrgUnitsActions.APPEND:
            return {...state,payload:[...state.payload,...action.payload]}

        default:
            return state
    }
}
