import { Reducer } from 'redux'
import {EmployeeSearchState} from "../../../_model/state-model/hr-sm/employee-search-state";
import {SearchEmployeeActions} from "../../../_constants/action-types";
import { SearchEmployeeOut } from '../../../_model/view_model/employee-search-par'

const initialState: EmployeeSearchState = {
    loading : false,
    error : false,
    message :'',
    payload:null
}

const loading: EmployeeSearchState = {...initialState,loading:true}
const success: EmployeeSearchState = {...initialState,loading:false}
const error: EmployeeSearchState = {...initialState,error:true}

export const searchEmployeeReducer: Reducer<EmployeeSearchState> = (state: EmployeeSearchState = initialState,action): EmployeeSearchState => {
    switch (action.type) {
        case SearchEmployeeActions.SEARCH_EMPLOYEE_LOADING:
            return {...loading,message:action.message}

        case SearchEmployeeActions.SEARCH_EMPLOYEE_SUCCESS:
            return {...success,payload:action.payload}

        case SearchEmployeeActions.SEARCH_EMPLOYEE_ERROR:
            return {...error,message:action.message}

        case SearchEmployeeActions.CLEAR:
            return {...initialState}

        case SearchEmployeeActions.PUSH:
           let store : SearchEmployeeOut | null = state.payload 
           store===null
           ?
            store = {
                   nResult:1,
                   _ret:[action.employee]
               }
            :
            store = {
                nResult:store.nResult+1,
                _ret:[...store._ret,action.employee]
            }
            return {...state,loading:false,payload:store}

        case SearchEmployeeActions.REMOVE:

            const newStore =
                state.payload !== null
                ?
                {
                    nResult: state.payload.nResult-1,
                    _ret:[...state.payload._ret.filter(r=> r.employeeID !== action.employeeID )]
                }
                :
                null;
            return {...state,payload:newStore}
        default:
            return state
    }
}
