import { Reducer } from 'redux'
import {AccountBaseState} from "../../../_model/state-model/ic-sm/account-base-state";
import {AccountBaseActions} from "../../../_constants/action-types";

const initialState: AccountBaseState = {
    loading : false,
    error : false,
    message :'',
    accounts:[]
}

const loading: AccountBaseState = {...initialState,loading:true}
const success: AccountBaseState = {...initialState,loading:false}
const error: AccountBaseState = {...initialState,error:true}

export const accountBaseReducer: Reducer<AccountBaseState> = (state: AccountBaseState = initialState,action): AccountBaseState => {
    switch (action.type) {
        case AccountBaseActions.LOADING:
            return {...state,...loading,message:action.message}
        case AccountBaseActions.SUCCESS:
            return {...success,accounts:[action.payload]}
        case AccountBaseActions.PUSH:
            return {...state,accounts:[...state.accounts,...action.payload]}
        case AccountBaseActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
