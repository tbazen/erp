import { Reducer } from 'redux'
import {ItemCategoryState} from "../../../_model/state-model/ic-sm/item-catagory-state";
import {ItemCategoryActions} from "../../../_constants/action-types";

const initialState: ItemCategoryState = {
    loading : false,
    error : false,
    message :'',
    item_categories:[]
}

const loading: ItemCategoryState = {...initialState,loading:true}
const success: ItemCategoryState = {...initialState,loading:false}
const error: ItemCategoryState = {...initialState,error:true}

export const itemCategoryReducer: Reducer<ItemCategoryState> = (state: ItemCategoryState = initialState,action): ItemCategoryState => {
    switch (action.type) {
        case ItemCategoryActions.ITEM_CATEGORIES_LOADING:
            return {...state,...loading,message:action.message}

        case ItemCategoryActions.ITEM_CATEGORIES_SUCCESS:
            return {...success,item_categories:action.payload}

        case ItemCategoryActions.ITEM_CATEGORIES_PUSH:

            return {...state,item_categories:[...state.item_categories,...action.payload]}


        case ItemCategoryActions.ITEM_CATEGORIES_ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
