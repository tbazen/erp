import { Reducer } from 'redux'
import {TransactionItemsState} from "../../../_model/state-model/ic-sm/transaction-items-state";
import {TransactionItemActions} from "../../../_constants/action-types";

const initialState: TransactionItemsState = {
    loading : false,
    error : false,
    message :'',
    transactions_items:[]
}

const loading: TransactionItemsState = {...initialState,loading:true}
const success: TransactionItemsState = {...initialState,loading:false}
const error: TransactionItemsState = {...initialState,error:true}

export const transactionItemsReducer: Reducer<TransactionItemsState> = (state: TransactionItemsState = initialState,action): TransactionItemsState => {
    switch (action.type) {
        case TransactionItemActions.LOADING:
            return {...loading,message:action.message}

        case TransactionItemActions.SUCCESS:
            return {...success,transactions_items:action.payload}

        case TransactionItemActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
