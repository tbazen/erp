import { combineReducers} from 'redux';
import { ApplicationState } from '../../_model/state-model/application-state';
import { authReducer } from './auth-reducer';
import { searchEmployeeReducer} from "./hr-reducers/search-employee-reducer";
import { organizationalUnitReducer} from "./hr-reducers/org-unit-reducer";
import { breadcrumbReducer } from "./breadcrumd-reducer";
import { reportCategoryReducer } from "./report-category-reducer";
import { reportDefinitionReducer } from "./report-definition-reducer";
import { itemCategoryReducer } from "./ic-reducers/item-category-reducer";
import { transactionItemsReducer } from "./ic-reducers/transaction-items-reducer";
import { kebeleReducer } from "./mn-reducers/kebele-reducer";
import { pressureZoneReducer } from "./mn-reducers/pressure-zone-reducer";
import { dmzReducer } from "./mn-reducers/dmz-reducer";
import { accountBaseReducer } from "./ic-reducers/account-base-reducer";
import { subscriberSearchResultStateReducer } from './mn-reducers/subscriber-search-result-state-reducer';
import { drawerModalReducer } from './drawer-modal-reducer';
import { globalSearchbarReducer } from './global-searchbar-reducer';
import { singleSubscriberReducer } from './mn-reducers/single-subscriber-reducer';
import { paymentCenterReducer } from "./mn-reducers/payment-center-reducer";
import { jobDataReducer } from "./aj-reducer/job-data-reducer";
import { jobRuleReducer } from "./job-rule-reducers/job-rule-reducer";
import { jobWorkerReducer } from './mn-reducers/job-worker-reducer';
import { jobHistoryReducer } from './aj-reducer/job-history-reducer';
import { jobConfigurationReducer} from "./job-configuration-reducer/job-configuration-reducer";
import { measureUnitReducer} from "./aj-reducer/measure-unit-reducer";
import { activeJobDetailInformationReducer} from "./aj-reducer/active-job-detail-information-reducer";
import { workflowDataReducer} from "./aj-reducer/workflow-data-reducer";
import { jobBOMReducer } from './aj-reducer/job-bom-reducer';
import { bomInvoicePreviewReducer } from "./aj-reducer/bom-invoice-preview-reducer";
import { creditSchemeReducer } from "./mn-reducers/credit-scheme-reducer";
import { billPeriodReducer } from "./mn-reducers/bill-periods-reducer";
import { billDocumentsReducer } from "./mn-reducers/bill-documents-reducer";
import { bwfMeterReadingReducer } from "./mn-reducers/bwf-meter-reading-reducer";
import {bwfMeterReadingListReducer} from "./mn-reducers/bwf-meter-reading-list-reducer";
import {employeeReducer} from "./mn-reducers/employee-reducer";
import {subscriptionReducer} from "./mn-reducers/subscription-reducer";
import {activeJobsReducer} from "./aj-reducer/active-jobs-reducer";
import {customerProfileReducer} from "./mn-reducers/customer-profile-reducer";
import {reportReducer} from "./report-reducers/report-reducer";
import {reportTypeReducer} from "./report-reducers/report-type-reducer";
import {reportHTMLReducer} from "./report-reducers/report-html-reducer";
import {meterReaderEmployeeReducer} from "./report-reducers/meter-reader-employee-reducer";


const rootReducer = combineReducers<ApplicationState>({
    auth : authReducer,
    breadcrumb: breadcrumbReducer,
    drawerModalState:drawerModalReducer,
    globalSearchbarState: globalSearchbarReducer,
    reportCategory: reportCategoryReducer,
    reportDefinitions: reportDefinitionReducer,
    employeeSearch: searchEmployeeReducer,
    orgUnits: organizationalUnitReducer,
    item_category_state: itemCategoryReducer,
    transaction_items_state: transactionItemsReducer,
    kebele_state: kebeleReducer,
    pressure_zone_state: pressureZoneReducer,
    dmz_state: dmzReducer,
    accountBaseState: accountBaseReducer,
    subscription_search_result_state: subscriberSearchResultStateReducer,
    subscriber_state: singleSubscriberReducer,
    payment_center_state: paymentCenterReducer,
    job_data_state : jobDataReducer,
    worker_state : jobWorkerReducer,
    job_rule : jobRuleReducer,
    job_history: jobHistoryReducer,
    configuration_state: jobConfigurationReducer,
    measure_unit_state: measureUnitReducer,
    active_job_detail_information_state: activeJobDetailInformationReducer,
    workflow_data_state: workflowDataReducer,
    job_bom_state: jobBOMReducer,
    bom_invoice_preview: bomInvoicePreviewReducer,
    credit_scheme_state: creditSchemeReducer,
    bill_period_state: billPeriodReducer,
    bill_document_state: billDocumentsReducer,
    bwf_meter_reading_state: bwfMeterReadingReducer,
    bwf_meter_reading_list_state: bwfMeterReadingListReducer,
    employee_state: employeeReducer,
    subscription_state: subscriptionReducer,
    active_jobs_state: activeJobsReducer,
    customer_profile_state: customerProfileReducer,
    report_state: reportReducer,
    report_type_state: reportTypeReducer,
    report_html_state: reportHTMLReducer,
    mater_reader_employee_state: meterReaderEmployeeReducer
});

export default rootReducer