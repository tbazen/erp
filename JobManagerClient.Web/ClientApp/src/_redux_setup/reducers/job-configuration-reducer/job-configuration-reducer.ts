import { Reducer } from 'redux'
import {JobConfigurationState} from "../../../_model/state-model/job-configuration-sm/job-configuration-state";
import {JobConfigurationActions} from "../../../_constants/action-types";

const initialState: JobConfigurationState = {
    payload:undefined,
    loading:false,
    error:false,
    message:'',
}

const loading: JobConfigurationState = {...initialState,loading:true , error:false}
const success: JobConfigurationState = {...initialState,loading:false , error:false}
const error: JobConfigurationState = {...initialState,loading:false , error:true}

export const jobConfigurationReducer: Reducer<JobConfigurationState> = (state: JobConfigurationState = initialState, action): JobConfigurationState => {
    switch (action.type) {
        case JobConfigurationActions.LOADING:
            return {...loading,message:action.message}
        case JobConfigurationActions.ERROR:
            return {...error , message : action.message}
        case JobConfigurationActions.SUCCESS:
            return {...success,payload:action.payload}
        default:
            return state
    }
}
