import { Reducer } from 'redux'
import {JobRuleStateModel} from "../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {JobRuleActions} from "../../../_constants/action-types";
import {IClientHandler} from "../../../_model/job-rule-model/client-handler";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";

const initialState: JobRuleStateModel = {
    clientHandlers:[],
    loading:false,
    error:false,
    message:'',
}
const loading: JobRuleStateModel = {...initialState,loading:true , error:false}
const success: JobRuleStateModel = {...initialState,loading:false , error:false}
const error: JobRuleStateModel = {...initialState,loading:false , error:true}

export const jobRuleReducer: Reducer<JobRuleStateModel> = (state: JobRuleStateModel = initialState,action): JobRuleStateModel => {

    switch (action.type) {
        case JobRuleActions.LOADING:
            return {...loading,message:action.message}
        case JobRuleActions.ERROR:
            return {...error , message : action.message}
        case JobRuleActions.PUSH:
            const handlers = validateJobRule(action.payload , state.clientHandlers)
            return {...state ,clientHandlers:[...handlers]}
        case JobRuleActions.SUCCESS:
            return {...success,clientHandlers:[...state.clientHandlers],message: action.message}
        default:
            return state
    }
}

const validateJobRule = (newHandler : IClientHandler ,handlers : IClientHandler[]) : IClientHandler[]  =>{
    let selectedHandler : IClientHandler | undefined = newHandler;
    let tempHandlers = [...handlers]
    handlers.forEach( handler =>{
       if( (newHandler.meta.jobType === handler.meta.jobType)){
           if(newHandler.meta.priority === handler.meta.priority ){
               throw new Error(`Duplicate Job Type and Priority Found  [Job Type :  ${StandardJobTypes[newHandler.meta.jobType]} ]`)
           }
           selectedHandler = ( (handler.meta.priority as number) > (newHandler.meta.priority as number) )
               ?
               undefined
               :
               selectedHandler;
       }
    });

    if(selectedHandler){
        tempHandlers = tempHandlers.filter(th => th.meta.jobType !== selectedHandler!.meta.jobType)
        tempHandlers.push(selectedHandler);
    }

    return tempHandlers;
}