import { Reducer } from 'redux'
import {BillDocumentsState} from "../../../_model/state-model/mn-sm/bill-documents-state";
import {BillDocumentActions} from "../../../_constants/action-types";

const initialState: BillDocumentsState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}

const loading: BillDocumentsState = {...initialState,loading:true}
const success: BillDocumentsState = {...initialState,loading:false}
const error: BillDocumentsState = {...initialState,error:true}

export const billDocumentsReducer: Reducer<BillDocumentsState> = (state: BillDocumentsState = initialState,action): BillDocumentsState => {
    switch (action.type) {
        case BillDocumentActions.LOADING:
            return {...loading,message:action.message}

        case BillDocumentActions.SUCCESS:
            return {...success,payload:action.payload}

        case BillDocumentActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
