import { Reducer } from 'redux'
import {BillPeriodState} from "../../../_model/state-model/mn-sm/bill-period-state";
import {BillPeriodActions} from "../../../_constants/action-types";

const initialState: BillPeriodState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}

const loading: BillPeriodState = {...initialState,loading:true}
const success: BillPeriodState = {...initialState,loading:false}
const error: BillPeriodState = {...initialState,error:true}

export const billPeriodReducer: Reducer<BillPeriodState> = (state: BillPeriodState = initialState,action): BillPeriodState => {
    switch (action.type) {
        case BillPeriodActions.LOADING:
            return {...loading,message:action.message}

        case BillPeriodActions.SUCCESS:
            return {...success,payload:action.payload}

        case BillPeriodActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
