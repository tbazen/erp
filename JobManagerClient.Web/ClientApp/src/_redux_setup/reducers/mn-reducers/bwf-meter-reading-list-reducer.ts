import { Reducer } from 'redux'
import {BWFMeterReadingListState} from "../../../_model/state-model/mn-sm/bwf-meter-reading-list-state";
import {BWFMeterReadingListActions} from "../../../_constants/action-types";

const initialState: BWFMeterReadingListState = {
    loading: false,
    error: false,
    message:'',
    payload: []
}

const loading: BWFMeterReadingListState = {...initialState,loading:true}
const success: BWFMeterReadingListState = {...initialState,loading:false}
const error: BWFMeterReadingListState = {...initialState,error:true}

export const bwfMeterReadingListReducer: Reducer<BWFMeterReadingListState> = (state: BWFMeterReadingListState = initialState, action): BWFMeterReadingListState => {
    switch (action.type) {
        case BWFMeterReadingListActions.LOADING:
            return {...loading,message:action.message}

        case BWFMeterReadingListActions.SUCCESS:
            return {...success,payload:action.payload}

        case BWFMeterReadingListActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
