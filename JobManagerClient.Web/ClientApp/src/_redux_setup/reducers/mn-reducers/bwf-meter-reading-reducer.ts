import { Reducer } from 'redux'
import {BWFMeterReadingState} from "../../../_model/state-model/mn-sm/bwf-meter-reading-state";
import {BWFMeterReadingActions} from "../../../_constants/action-types";

const initialState: BWFMeterReadingState = {
    loading : false,
    error : false,
    message :''
}

const loading: BWFMeterReadingState = {...initialState,loading:true}
const success: BWFMeterReadingState = {...initialState,loading:false}
const error: BWFMeterReadingState = {...initialState,error:true}

export const bwfMeterReadingReducer: Reducer<BWFMeterReadingState> = (state: BWFMeterReadingState = initialState, action): BWFMeterReadingState => {
    switch (action.type) {
        case BWFMeterReadingActions.LOADING:
            return {...loading,message:action.message}

        case BWFMeterReadingActions.SUCCESS:
            return {...success,payload:action.payload}

        case BWFMeterReadingActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
