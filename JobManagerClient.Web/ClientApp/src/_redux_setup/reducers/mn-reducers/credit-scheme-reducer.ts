import { Reducer } from 'redux'
import {CreditSchemeState} from "../../../_model/state-model/mn-sm/credit-scheme-state";
import {CreditSchemeActions} from "../../../_constants/action-types";

const initialState: CreditSchemeState = {
    loading: false,
    error: false,
    message:'',
    payload: {
        _ret:[],
        settlement:[]
    }
}

const loading: CreditSchemeState = {...initialState,loading:true}
const success: CreditSchemeState = {...initialState,loading:false}
const error: CreditSchemeState = {...initialState,error:true}

export const creditSchemeReducer: Reducer<CreditSchemeState> = (state: CreditSchemeState = initialState,action): CreditSchemeState => {
    switch (action.type) {
        case CreditSchemeActions.LOADING:
            return {...loading,message:action.message}

        case CreditSchemeActions.SUCCESS:
            return {...success,payload:action.payload}

        case CreditSchemeActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
