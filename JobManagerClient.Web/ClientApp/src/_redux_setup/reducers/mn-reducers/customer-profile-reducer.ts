import { Reducer } from 'redux'
import {CustomerProfileActions} from '../../../_constants/action-types';
import {CustomerProfileState} from "../../../_model/state-model/mn-sm/customer-profile-state";

const initialState: CustomerProfileState = {
    loading : false,
    error : false,
    message :'',
    payload:undefined
}

const loading: CustomerProfileState = {...initialState,loading:true}
const success: CustomerProfileState = {...initialState,loading:false}
const error: CustomerProfileState = {...initialState,error:true}

export const customerProfileReducer: Reducer<CustomerProfileState> = (state: CustomerProfileState = initialState,action): CustomerProfileState => {
    switch (action.type) {
        case CustomerProfileActions.LOADING:
            return {...loading,message:action.message}

        case CustomerProfileActions.SUCCESS:
            return {...success,payload:action.payload}

        case CustomerProfileActions.ERROR:
            return {...error,message:action.message}

        default:
            return state
    }
}
