import { Reducer } from 'redux'
import {DmzState} from "../../../_model/state-model/mn-sm/dmz-state";
import {DMZActions} from "../../../_constants/action-types";

const initialState: DmzState = {
    loading : false,
    error : false,
    message :'',
    dmzs:[]
}

const loading: DmzState = {...initialState,loading:true}
const success: DmzState = {...initialState,loading:false}
const error: DmzState = {...initialState,error:true}

export const dmzReducer: Reducer<DmzState> = (state: DmzState = initialState,action): DmzState => {
    switch (action.type) {
        case DMZActions.LOADING:
            return {...loading,message:action.message}

        case DMZActions.SUCCESS:
            return {...success,dmzs:action.payload}

        case DMZActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
