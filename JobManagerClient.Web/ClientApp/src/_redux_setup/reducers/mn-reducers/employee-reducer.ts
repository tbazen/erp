import { Reducer } from 'redux'
import {EmployeeActions} from "../../../_constants/action-types";
import {EmployeeState} from "../../../_model/state-model/mn-sm/employee-state";

const initialState: EmployeeState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}

const loading: EmployeeState = {...initialState,loading:true}
const success: EmployeeState = {...initialState,loading:false}
const error: EmployeeState = {...initialState,error:true}

export const employeeReducer: Reducer<EmployeeState> = (state: EmployeeState = initialState,action): EmployeeState => {
    switch (action.type) {
        case EmployeeActions.LOADING:
            return {...loading,message:action.message}

        case EmployeeActions.SUCCESS:
            return {...success,payload:action.payload}

        case EmployeeActions.DONE:
            return {...state,loading:false,error:false,message:'done'}

        case EmployeeActions.ERROR:
            return {...error,message:action.message}

        case EmployeeActions.APPEND:
            return {...state,payload:[ ...state.payload, ...action.payload ]}
        default:
            return state
    }
}
