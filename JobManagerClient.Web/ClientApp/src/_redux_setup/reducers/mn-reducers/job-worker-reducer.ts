import { Reducer } from 'redux'
import {JobWorkerState} from "../../../_model/state-model/mn-sm/job-worker-state";
import {JobWorkerActions} from "../../../_constants/action-types";

const initialState: JobWorkerState = {
    loading : false,
    error : false,
    message :'',
    workers:[]
}
const loading: JobWorkerState = {...initialState,loading:true}
const success: JobWorkerState = {...initialState,loading:false}
const error: JobWorkerState = {...initialState,error:true}

export const jobWorkerReducer: Reducer<JobWorkerState> = (state: JobWorkerState = initialState,action): JobWorkerState => {
    switch (action.type) {
        case JobWorkerActions.LOADING:
            return {...loading,message:action.message}
        case JobWorkerActions.SUCCESS:
            return {...success,workers:action.payload}
        case JobWorkerActions.ERROR:
            return {...error,message:action.message}
        case JobWorkerActions.REMOVE:
            return {...state,workers:[...state.workers.filter(worker => worker.userID !== action.workerId)]}
        default:
            return state
    }
}
