import { Reducer } from 'redux'
import {KebeleState} from "../../../_model/state-model/mn-sm/kebele-state";
import {KebeleActions} from "../../../_constants/action-types";

const initialState: KebeleState = {
    loading : false,
    error : false,
    message :'',
    kebeles:[]
}

const loading: KebeleState = {...initialState,loading:true}
const success: KebeleState = {...initialState,loading:false}
const error: KebeleState = {...initialState,error:true}

export const kebeleReducer: Reducer<KebeleState> = (state: KebeleState = initialState,action): KebeleState => {
    switch (action.type) {
        case KebeleActions.LOADING:
            return {...loading,message:action.message}

        case KebeleActions.SUCCESS:
            return {...success,kebeles:action.payload}

        case KebeleActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
