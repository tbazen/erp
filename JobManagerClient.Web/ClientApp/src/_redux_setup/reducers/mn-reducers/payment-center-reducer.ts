import { Reducer } from 'redux'
import {PaymentCenterActions} from "../../../_constants/action-types"
import {PaymentCenterState} from "../../../_model/state-model/mn-sm/internal-service-sm/payment-center-state";

const initialState: PaymentCenterState = {
    loading : false,
    error : false,
    message :'',
    payment_centers:[]
}

const loading: PaymentCenterState = {...initialState,loading:true}
const success: PaymentCenterState = {...initialState,loading:false}
const error: PaymentCenterState = {...initialState,error:true}

export const paymentCenterReducer: Reducer<PaymentCenterState> = (state: PaymentCenterState = initialState,action): PaymentCenterState => {
    switch (action.type) {
        case PaymentCenterActions.LOADING:
            return {...loading,message:action.message}

        case PaymentCenterActions.SUCCESS:
            return {...success,payment_centers:action.payload}

        case PaymentCenterActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
