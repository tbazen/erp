import { Reducer } from 'redux'
import {PressureZoneState} from "../../../_model/state-model/mn-sm/pressure-zone-state"
import {PressureZoneActions} from "../../../_constants/action-types"

const initialState: PressureZoneState = {
    loading : false,
    error : false,
    message :'',
    pressure_zones:[]
}

const loading: PressureZoneState = {...initialState,loading:true}
const success: PressureZoneState = {...initialState,loading:false}
const error: PressureZoneState = {...initialState,error:true}

export const pressureZoneReducer: Reducer<PressureZoneState> = (state: PressureZoneState = initialState,action): PressureZoneState => {
    switch (action.type) {
        case PressureZoneActions.LOADING:
            return {...loading,message:action.message}

        case PressureZoneActions.SUCCESS:
            return {...success,pressure_zones:action.payload}

        case PressureZoneActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
