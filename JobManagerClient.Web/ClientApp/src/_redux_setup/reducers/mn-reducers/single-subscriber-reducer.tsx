import { Reducer } from 'redux'
import { SingleSubscriberActions } from '../../../_constants/action-types';
import { SingleSubscriberState } from '../../../_model/state-model/mn-sm/single-subscriber-state'

const initialState: SingleSubscriberState = {
    loading : false,
    error : false,
    message :'',
    payload:undefined
}

const loading: SingleSubscriberState = {...initialState,loading:true}
const success: SingleSubscriberState = {...initialState,loading:false}
const error: SingleSubscriberState = {...initialState,error:true}

export const singleSubscriberReducer: Reducer<SingleSubscriberState> = (state: SingleSubscriberState = initialState,action): SingleSubscriberState => {
    switch (action.type) {
        case SingleSubscriberActions.LOADING:
            return {...loading,message:action.message}

        case SingleSubscriberActions.SUCCESS:
            return {...success,payload:action.payload}

        case SingleSubscriberActions.ERROR:
            return {...error,message:action.message}

        default:
            return state
    }
}
