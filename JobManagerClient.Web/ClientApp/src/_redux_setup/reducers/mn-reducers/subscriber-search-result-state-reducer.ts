import { Reducer } from 'redux'
import {SubscriberSearchResultActions} from "../../../_constants/action-types"
import { SubscriberSearchResultState } from '../../../_model/state-model/mn-sm/subscriber-search-result-state';

const initialState: SubscriberSearchResultState = {
    loading : false,
    error : false,
    message :'',
    payload:{
        nRecord:0,
        _ret:[]
    }
}

const loading: SubscriberSearchResultState = {...initialState,loading:true}
const success: SubscriberSearchResultState = {...initialState,loading:false}
const error: SubscriberSearchResultState = {...initialState,error:true}

export const subscriberSearchResultStateReducer: Reducer<SubscriberSearchResultState> = (state: SubscriberSearchResultState = initialState, action): SubscriberSearchResultState => {
    switch (action.type) {
        case SubscriberSearchResultActions.LOADING:
            return {...loading,message:action.message}

        case SubscriberSearchResultActions.SUCCESS:
            return {...success,payload:action.payload}

        case SubscriberSearchResultActions.ERROR:
            return {...error,message:action.message}
        case SubscriberSearchResultActions.CLEAR:
            return {...initialState} 
        default:
            return state
    }
}
