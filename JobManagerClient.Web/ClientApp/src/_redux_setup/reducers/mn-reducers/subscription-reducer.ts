import { Reducer } from 'redux'
import {SubscriptionActions} from "../../../_constants/action-types";
import {SubscriptionState} from "../../../_model/state-model/mn-sm/subscription-state";

const initialState: SubscriptionState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}

const loading: SubscriptionState = {...initialState,loading:true}
const success: SubscriptionState = {...initialState,loading:false}
const error: SubscriptionState = {...initialState,error:true}

export const subscriptionReducer: Reducer<SubscriptionState> = (state: SubscriptionState = initialState,action): SubscriptionState => {
    switch (action.type) {
        case SubscriptionActions.LOADING:
            return {...loading,message:action.message}

        case SubscriptionActions.SUCCESS:
            return {...success,payload:action.payload}

        case SubscriptionActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
