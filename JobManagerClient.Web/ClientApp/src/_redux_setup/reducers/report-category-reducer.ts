import { Reducer } from 'redux'
import { ReportCategorieActions} from "../../_constants/action-types";
import {ReportCategoryState} from "../../_model/state-model/report-category-state";
import {ReportCategory} from "../../_model/view_model/report-category";

const init:ReportCategoryState = {
    loading:false,
    error:false,
    message:'',
    categories:[]
}
const loading : ReportCategoryState={...init,loading:true}
const error : ReportCategoryState={...init,error:true}
const success : ReportCategoryState={...init}


export const reportCategoryReducer: Reducer<ReportCategoryState> = (state: ReportCategoryState = init,action): ReportCategoryState => {
    switch (action.type) {
        case ReportCategorieActions.REPORT_CATAGORY_LOADING:
            return {...loading}
        case ReportCategorieActions.REPORT_CATAGORY_ERROR:
            return {...error}
        case ReportCategorieActions.REPORT_CATAGORY_SUCCESS:
            return {...success,categories:action.payload}
        case ReportCategorieActions.REPORT_CATEGORIES_PUSH:
            return {...state,categories:[...state.categories,action.payload]}
        case ReportCategorieActions.PUSH:
            const reportCategory: ReportCategory = action.payload
            let categoriesCopy = [...state.categories]
            const index = categoriesCopy.findIndex(rc=>rc.id === reportCategory.id)
            if(index>=0)
                categoriesCopy[index] = reportCategory
            else
                categoriesCopy = [...categoriesCopy,reportCategory]
            return {...state,categories:[...categoriesCopy]}
        case ReportCategorieActions.POP:
            const reportCategoryId: number = action.payload
            let filteredCategories = clearNodeAndChildNodes(state.categories,reportCategoryId);
            return {...state,categories:[...filteredCategories]}
        default:
            return state
    }
}


function clearNodeAndChildNodes(reportCategories: ReportCategory[],reportCategoryId: number): ReportCategory[]{
    let categoriesToClean :ReportCategory[]= filterCategoriesToDelete(reportCategories, reportCategoryId)
    function isNotInToCleanList(reportCategory: ReportCategory): boolean {
        const index = categoriesToClean.findIndex(cc => cc.id === reportCategory.id)
        if(index>= 0)
            return false
        return true
    }
    const filteredReportCategories: ReportCategory[] = reportCategories.filter(rc=>isNotInToCleanList(rc))
    return [...filteredReportCategories]
}

function filterCategoriesToDelete(categories: ReportCategory[], deleteCategoryId: number): ReportCategory[] {
    const rootCategoryToDelete = categories.find(ct=> ct.id === deleteCategoryId)
    let cleanUpList: ReportCategory [] =[]
    if(rootCategoryToDelete)
        cleanUpList = [ rootCategoryToDelete ]
    function filterCleanUpList(cats: ReportCategory[],parentId: number){
        let children:ReportCategory[]=[]
        cats.forEach(cat=> {
            if(cat.pid === parentId){
                cleanUpList = [ ...cleanUpList,cat ]
                children = [...children,cat]
            }
        });
        children.forEach(child=> filterCleanUpList(cats,child.id))
    }
    filterCleanUpList(categories,deleteCategoryId)
    return cleanUpList;
}
