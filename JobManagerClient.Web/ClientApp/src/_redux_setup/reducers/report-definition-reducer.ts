import { Reducer } from 'redux'
import {ReportDefinitionActions} from "../../_constants/action-types"
import {ReportDefinitionState} from "../../_model/state-model/report-definition-state";
import {ReportDefination} from "../../_model/level0/accounting-type-library/ehtml";

const init:ReportDefinitionState = {
    loading:false,
    error:false,
    message:'',
    definitions:[]
}
const loading : ReportDefinitionState={...init,loading:true}
const error : ReportDefinitionState={...init,error:true}
const success : ReportDefinitionState={...init}

export const reportDefinitionReducer : Reducer<ReportDefinitionState> = (state: ReportDefinitionState = init,action): ReportDefinitionState => {
    switch (action.type) {
        case ReportDefinitionActions.REPORT_DEFINITION_LOADING:
            return {...loading}
        case ReportDefinitionActions.REPORT_DEFINITION_ERROR:
            return {...error}
        case ReportDefinitionActions.REPORT_DEFINITION_SUCCESS:
            return {...success,definitions:[...state.definitions,...action.payload]}

        case ReportDefinitionActions.REPORT_DEFINITION_CLEAR:
            return {...init}
        case ReportDefinitionActions.POP:
            const reportCategoryId: number = action.payload
            const filteredReportDefns: ReportDefination[] = [ ...state.definitions.filter(rd=> rd.id !== reportCategoryId) ]
            return {...state,definitions:[...filteredReportDefns]}
        case ReportDefinitionActions.PUSH:
            const newReportDefinition: ReportDefination = action.payload
            const oldReportDefinitionList = [...state.definitions]
            const reportDefinitionIndex = state.definitions.findIndex(rd=> rd.id === newReportDefinition.id)
            if( reportDefinitionIndex >= 0){
                oldReportDefinitionList[reportDefinitionIndex] = newReportDefinition
                return {...state,definitions:[...oldReportDefinitionList]}
            } else
                return {...state,definitions:[ ...state.definitions,newReportDefinition ]}
        default:
            return state
    }
}
