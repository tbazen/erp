import { Reducer } from 'redux'
import {MeterReaderEmployeeState} from "../../../_model/state-model/r-sm/meter-reader-employee-state";
import {MeterReaderEmployeeActions} from "../../../_constants/action-types";

const initialState: MeterReaderEmployeeState = {
    loading : false,
    error : false,
    message :'',
    payload:[]
}

const loading: MeterReaderEmployeeState = {...initialState,loading:true}
const success: MeterReaderEmployeeState = {...initialState,loading:false}
const error: MeterReaderEmployeeState = {...initialState,error:true}

export const meterReaderEmployeeReducer: Reducer<MeterReaderEmployeeState> = (state: MeterReaderEmployeeState = initialState,action): MeterReaderEmployeeState => {
    switch (action.type) {
        case MeterReaderEmployeeActions.LOADING:
            return {...loading,message:action.message}

        case MeterReaderEmployeeActions.SUCCESS:
            return {...success,payload:action.payload}

        case MeterReaderEmployeeActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
