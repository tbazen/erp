import { Reducer } from 'redux'
import {ReportHTMLState} from "../../../_model/state-model/r-sm/report-html-state";
import {ReportHTMLActions} from "../../../_constants/action-types";


const initialState: ReportHTMLState = {
    loading : false,
    error : false,
    message :'',
}

const loading: ReportHTMLState = {...initialState,loading:true}
const success: ReportHTMLState = {...initialState,loading:false}
const error: ReportHTMLState = {...initialState,error:true}

export const reportHTMLReducer: Reducer<ReportHTMLState> = (state: ReportHTMLState = initialState,action): ReportHTMLState => {
    switch (action.type) {
        case ReportHTMLActions.LOADING:
            return {...loading,message:action.message}

        case ReportHTMLActions.SUCCESS:
            return {...success,payload:action.payload}

        case ReportHTMLActions.ERROR:
            return {...error,message:action.message}
        case ReportHTMLActions.CLEAR:
            return {...initialState}
        default:
            return state
    }
}
