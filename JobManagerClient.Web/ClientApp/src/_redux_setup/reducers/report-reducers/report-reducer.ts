import { Reducer } from 'redux'
import {ReportState} from "../../../_model/state-model/r-sm/report-state";
import {ReportActions} from "../../../_constants/action-types";

const initialState: ReportState = {
    loading : false,
    error : false,
    message :'',
    payload:undefined
}

const loading: ReportState = {...initialState,loading:true}
const success: ReportState = {...initialState,loading:false}
const error: ReportState = {...initialState,error:true}

export const reportReducer: Reducer<ReportState> = (state: ReportState = initialState,action): ReportState => {
    switch (action.type) {
        case ReportActions.LOADING:
            return {...loading,message:action.message}

        case ReportActions.SUCCESS:
            return {...success,payload:action.payload}

        case ReportActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
