import { Reducer } from 'redux'
import {ReportTypeState} from "../../../_model/state-model/r-sm/report-types-state";
import {ReportTypeActions} from "../../../_constants/action-types";

const initialState: ReportTypeState = {
    loading : false,
    error : false,
    message :'',
    reportTypes: []
}

const loading: ReportTypeState = {...initialState,loading:true}
const success: ReportTypeState = {...initialState,loading:false}
const error: ReportTypeState = {...initialState,error:true}

export const reportTypeReducer: Reducer<ReportTypeState> = (state: ReportTypeState = initialState,action): ReportTypeState => {
    switch (action.type) {
        case ReportTypeActions.LOADING:
            return {...loading,message:action.message}

        case ReportTypeActions.SUCCESS:
            return {...success,reportTypes:action.payload}

        case ReportTypeActions.ERROR:
            return {...error,message:action.message}
        default:
            return state
    }
}
