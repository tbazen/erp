import { ROUTES } from '../_constants/routes'
import ActiveJobsContainer from '../module/active jobs/active-jobs-container'
import {JobContainer} from "../module/active jobs/job-container";


const ActiveJobsModuleRoutes = [
    {
        path:ROUTES.ACTIVE_JOBS,
        component:ActiveJobsContainer
    },
    {
        path: `${ROUTES.ACTIVE_JOBS}/:jobId`,
        component: JobContainer
    }
]

export default ActiveJobsModuleRoutes