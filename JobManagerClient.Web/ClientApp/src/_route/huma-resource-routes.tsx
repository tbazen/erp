import {ROUTES} from "../_constants/routes";
import HumanResource from "../module/human resouce/human-resource";
import {IRoute} from "./route-index";

const HumanResourceModuleRoutes : IRoute[] = [
    {
        path:ROUTES.HUMAN_RESOURCE,
        component:HumanResource
    }
]

export default HumanResourceModuleRoutes