import {IRoute} from "./route-index";
import {ROUTES} from "../_constants/routes";
import ItemConfiguration from "../module/item configuration/item-configuration";


const ItemConfigModuleRoutes:IRoute[]=[
    {
        path:ROUTES.ITEM_CONFIGURATION,
        component:ItemConfiguration
    }
]

export default ItemConfigModuleRoutes