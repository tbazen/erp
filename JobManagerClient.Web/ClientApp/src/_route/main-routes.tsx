import {ROUTES} from "../_constants/routes"
import {Main} from "../module/main/main"
import {IRoute} from "./route-index"
import CustomerListContainer from "../module/main/customer list/customer-list-container"
import OtherServicesContainer from "../module/main/other services/other-services-container";
import InternalServicesContainer from "../module/main/internal services/internal-services-container";
import WorkersListContainer from "../module/main/workers list/workers-list-container";


const StaticMainModuleRoutes: IRoute[] = [
    {
        path:ROUTES.MAIN.INDEX,
        component:Main
    },
    {
        path:ROUTES.MAIN.CUSTOMER_LIST,
        component:CustomerListContainer
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.INDEX,
        component:OtherServicesContainer
    },
    {
        path:ROUTES.JOB.INTERNAL_SERVICE.INDEX,
        component:InternalServicesContainer
    },
    {
        path:ROUTES.MAIN.WORKER_LIST,
        component:WorkersListContainer
    }
]

export default StaticMainModuleRoutes