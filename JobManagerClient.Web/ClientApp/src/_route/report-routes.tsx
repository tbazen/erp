import {IRoute} from "./route-index";
import {ROUTES} from "../_constants/routes";
import ReportContainer from "../module/report/_components/report-container/report-container";
import ReportManageContainer from "../module/report/_components/manage report/report-manage";

const ReportModuleRoutes:IRoute[]=[
    {
        path:ROUTES.REPORT_ABSOLUTE,
        component:ReportContainer
    },
    {
        path:ROUTES.R_MANAGE_REPORT,
        component:ReportManageContainer
    }
]

export default ReportModuleRoutes