import React, {useEffect, useState} from 'react'
import { Redirect, Route, RouteComponentProps, Switch} from 'react-router-dom';
import HumanResourceModuleRoutes from "./huma-resource-routes";
import StaticMainModuleRoutes from "./main-routes";
import ItemConfigModuleRoutes from "./item-config-routes";
import ReportModuleRoutes from "./report-routes";
import {ROUTES} from "../_constants/routes";
import ActiveJobsModuleRoutes from './activejobs-routes';
import NotFound from '../shared/screens/status-code/not-found';
import {useDispatch, useSelector} from "react-redux";
import {JobRuleStateModel} from "../_model/state-model/job-rule-sm/job-rule-state-model";
import {ApplicationState} from "../_model/state-model/application-state";
import {loadClientHandler} from "../_redux_setup/actions/_rule_configuration-actions/job_rule-loader";


export interface IRoute{
    path:string,
    component : React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>
}

const AppRoute = () =>{
    const dispatch = useDispatch()
    const [ jobRule ] = useSelector<ApplicationState,[ JobRuleStateModel ]>( appState => [ appState.job_rule ] )
    const [ mainModuleRoutes ,setMainModuleRoutes ] = useState<IRoute[]>([])

    function updateMainModuleRoutes(newValue : IRoute[] ){
        setMainModuleRoutes(newValue);
    }
    useEffect(()=>{
        dispatch(loadClientHandler())
    },[])


    useEffect(()=>{
        if(!jobRule.loading && !jobRule.error){
            const tempRoutes:IRoute[] = [];
            jobRule.clientHandlers.forEach(handler=>{
               tempRoutes.push({path:handler.meta.route,component:handler.handler.getNewApplicationForm()})
               if(handler.meta.innerRoutes){
                   handler.meta.innerRoutes.forEach(route=>{
                       const tempRoute = tempRoutes.find(rt => rt.path === route.path)
                       if(!tempRoute){
                           tempRoutes.push(route)
                       }
                   });
               }

               if( (handler.meta.hasConfiguration) && (handler.meta.configurationRoute !== undefined)){
                    tempRoutes.push({
                        path:handler.meta.configurationRoute,
                        component : handler.handler.getConfigurationForm()
                    });
               }
            });
            updateMainModuleRoutes(tempRoutes)
        }
    },[jobRule])


    return(
        <Switch>
            <Route exact path={'/'} component={()=><Redirect to={ROUTES.MAIN.INDEX}/>}/>
            {
                /*********** Dynamically loaded main module routes ********************/
                mainModuleRoutes.map((val,key)=><Route key={key} exact path={val.path} component={val.component} />)
            }
            {
                /*********** Statically loaded main module routes ********************/
                StaticMainModuleRoutes.map((val, key)=><Route key={key} exact path={val.path} component={val.component} />)
            }
            {
                /*********** Active Jobs Module Routes ********************/
                ActiveJobsModuleRoutes.map((val,key)=><Route key={key} exact path={val.path} component={val.component} />)
            }
            {
                /*********** Human Resource Module Routes ********************/
                HumanResourceModuleRoutes.map((val,key)=><Route key={key} path={val.path} component={val.component} />)
            }

            {
                /*********** Item Configuration Module Routes ********************/
                ItemConfigModuleRoutes.map((val,key)=><Route key={key} path={val.path} component={val.component} />)
            }

            {
                /*********** Report Module Routes ********************/
                ReportModuleRoutes.map((val,key)=><Route key={key} exact path={val.path} component={val.component} />)
            }
            {
                jobRule.loading &&
                <Route component={()=><p>Loading...</p>} />
            }
            {
                !jobRule.loading &&
                !jobRule.error &&
                <Route component={NotFound} />
            }
        </Switch>
    )
}

export default AppRoute