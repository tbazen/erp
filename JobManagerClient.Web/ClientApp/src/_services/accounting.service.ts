import ServiceBase from "./config/service.base";
import ISession from "../_model/view_model/session-id";
import {ACCOUNTING_API} from "./config/url.config";
import config from "./config/header.config";
import axios from 'axios'
import {AccountDocument} from "../_model/level0/accounting-type-library/account-document";
import {DocumentTypedReference} from "../_model/level0/accounting-type-library/doc-serial";
import {EHTMLData, ReportDefination} from "../_model/level0/accounting-type-library/ehtml";
import {TypeObject} from "../_model/level0/application-server-library/type-object";
import {EData} from "../_model/level0/formula-evaluator/edata";
import {ReportCategory} from "../_model/view_model/report-category";



export interface GetChildReportCategoriesPar extends ISession {pid:number}
export interface GetReportByCategoryPar extends ISession{catID:number}

export interface GetAccountPar extends ISession{ accountID:number; accountType:string}
export interface GetChildAcccountPar extends ISession{parentAccount:number; index:number; pageSize:number; accountType:string}

export interface GetCostCenterAccountPar extends ISession {costCenterID: number;accountID: number;}
export interface GetNetBalanceAsOfPar extends ISession {csAccountID: number;itemID: number;date: string;}

//TODO: Axios call with token return Axios.get(url,{...config,cancelToken: this.TokenFactory.REGISTERED_ASSETS_TOKEN.token})
export interface GetAccountDocumentPar extends ISession { documentID: number; fullData: boolean; }
export interface GetDocumentTypeByTypePar extends ISession{ type:string; }
export interface GetDocumentHTMLPar extends ISession {  accountDocumentID:number;}
export interface GetDocumentTypeByTypeWebPar extends ISession{ type:string; }
export interface GetDocumentTypeByIDPar extends ISession { typeID:number;}
export interface PostGenericDocumentWebPar<T extends AccountDocument> extends ISession {doc:T; dataType:string; }
export interface GetDocumentSerialTypePar extends ISession { prefix: string }
export interface GetDocumentListByTypedReferencePar extends ISession { tref:DocumentTypedReference }
export interface GetFunctionDocumentationsPar extends ISession {reportTypeID: number}
export interface GetAllReportTypesPar extends ISession { }
export interface EvaluateEHTML3WebPar extends ISession {
    reportTypeID: number;
    data: EHTMLData;
    parameter: TypeObject[];
    symbol: string;
}
export interface EvaluateEHTML3WebOut {
    edata: EData;
    edataString: string;
}
export interface EvaluateEHTML2Par extends ISession { reportTypeID: number; data: EHTMLData; parameter: TypeObject[];}
export interface EvaluateEHTML2Out { headerItems: string; _ret: string; }
export interface SaveReportCategoryPar extends ISession { cat:ReportCategory;}
export interface DeleteReportCategoryPar extends ISession{ catID: number; }
export interface DeleteReportPar extends ISession {reportID: number}
export interface SaveEHTMLDataPar extends ISession{  r:ReportDefination;}

export default class AccountingService extends ServiceBase{
    getAllDocumentTypes = (session:ISession) =>axios.post(`${ACCOUNTING_API}GetAllReportTypes`,session,{...config})
    isSingleConstCenter =(session:ISession) => axios.post(`${ACCOUNTING_API}IsSingleCostCenter`,session,{...config})
    getAllDocumentHandlerInfo = (session:ISession)=> axios.post( `${ACCOUNTING_API}GetAllDocumentHandlerInfo`,session,{...config})
    getReportCategories = (session : GetChildReportCategoriesPar)=>axios.post(`${ACCOUNTING_API}GetChildReportCategories`,session,{...config})
    getReportTypesByCatagory=(rprtParams:GetReportByCategoryPar)=> axios.post(`${ACCOUNTING_API}GetReportByCategory`,rprtParams,{...config})
    GetChildAcccount=(params:GetChildAcccountPar)=>axios.post(`${ACCOUNTING_API}GetChildAcccount`,params,{...config})
    GetAccount=(params:GetAccountPar)=>axios.post(`${ACCOUNTING_API}GetAccount`,params,{...config})
    PostGenericDocumentWeb=<T extends AccountDocument>(params:PostGenericDocumentWebPar<T>)=>axios.post(`${ACCOUNTING_API}PostGenericDocumentWeb`,params,{...config})
    GetCostCenterAccount=(params:GetCostCenterAccountPar)=>axios.post(`${ACCOUNTING_API}GetCostCenterAccount`,params,{...config})
    GetNetBalanceAsOf=(params:GetNetBalanceAsOfPar)=>axios.post(`${ACCOUNTING_API}GetNetBalanceAsOf`,params,{...config})
    GetAccountDocument=(params:GetAccountDocumentPar)=>axios.post(`${ACCOUNTING_API}GetAccountDocument`,params,{...config})
    GetDocumentTypeByType=(params:GetDocumentTypeByTypePar)=>axios.post(`${ACCOUNTING_API}GetDocumentTypeByType`,params,{...config})
    GetDocumentTypeByTypeWeb=(params:GetDocumentTypeByTypePar)=>axios.post(`${ACCOUNTING_API}GetDocumentTypeByTypeWeb`,params,{...config})
    GetDocumentHTML=(params:GetDocumentHTMLPar)=>axios.post(`${ACCOUNTING_API}GetDocumentHTML`,params,{...config})
    GetDocumentTypeByID=(params:GetDocumentTypeByIDPar)=>axios.post(`${ACCOUNTING_API}GetDocumentTypeByID`,params,{...config})
    GetDocumentSerialType=(params:GetDocumentSerialTypePar)=>axios.post(`${ACCOUNTING_API}getDocumentSerialType`,params,{...config})
    GetDocumentListByTypedReference=(params:GetDocumentListByTypedReferencePar)=>axios.post(`${ACCOUNTING_API}getDocumentListByTypedReference`,params,{...config})
    GetFunctionDocumentations=(params:GetFunctionDocumentationsPar)=>axios.post(`${ACCOUNTING_API}GetFunctionDocumentations`,params,{...config})
    GetAllReportTypes=(params:GetAllReportTypesPar)=>axios.post(`${ACCOUNTING_API}GetAllReportTypes`,params,{...config})
    EvaluateEHTML3Web=(params:EvaluateEHTML3WebPar)=>axios.post(`${ACCOUNTING_API}EvaluateEHTML3Web`,params,{...config})
    EvaluateEHTML2=(params:EvaluateEHTML2Par)=>axios.post(`${ACCOUNTING_API}EvaluateEHTML2`,params,{...config})
    SaveReportCategory=(params:SaveReportCategoryPar)=>axios.post(`${ACCOUNTING_API}SaveReportCategory`,params,{...config})
    DeleteReportCategory=(params:DeleteReportCategoryPar)=>axios.post(`${ACCOUNTING_API}DeleteReportCategory`,params,{...config})
    DeleteReport=(params:DeleteReportPar)=>axios.post(`${ACCOUNTING_API}DeleteReport`,params,{...config})
    SaveEHTMLData=(params:SaveEHTMLDataPar)=>axios.post(`${ACCOUNTING_API}SaveEHTMLData`,params,{...config})
}