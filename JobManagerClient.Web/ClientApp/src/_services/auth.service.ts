import ServiceBase from "./config/service.base"
import axios from 'axios'
import {SECURITY_API, SERVER_API} from "./config/url.config";
import {CreateUserSessionPar} from "../_model/view_model/login-view-model";
import config from "./config/header.config";
import {LOGGED_IN_USERNAME, SESSION_KEY} from "../_constants/local-storage";
import {IsPermitedPar} from "../_model/view_model/session-id";

export default class AuthService extends ServiceBase{

    validateCredential = (credential : CreateUserSessionPar)=>{
        return axios.post(`${SERVER_API}CreateUserSession`,credential,{...config})
    }

    validateSessionId = (session : IsPermitedPar)=>{
        return axios.post(`${SECURITY_API}IsPermited`,session,{...config})
    }

    destroySession = ()=>{
        localStorage.removeItem(LOGGED_IN_USERNAME)
        localStorage.removeItem(SESSION_KEY)
    }
}