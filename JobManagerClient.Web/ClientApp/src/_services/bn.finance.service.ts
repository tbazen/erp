import ServiceBase from "./config/service.base";
import {BN_FINANCE_API, SUBSCRIBER_MANAGEMENT_API} from "./config/url.config";
import axios from 'axios'
import config from "./config/header.config";
import ISession from "../_model/view_model/session-id";
import {TransactionItems} from "../_model/level0/iERP-transaction-model/transaction-items";


export interface GetItemCategories2Par extends ISession{PID:number}
export interface GetItemsInCategoryPar extends ISession{categID:number}
export interface SearchTransactionItemsPar extends ISession {
    index: number;
    pageSize: number;
    criteria: any[];
    column: string[];
}
export interface GetSystemParamtersPar extends ISession {names: string[];}
export interface GetTransactionItemsPar extends ISession {code: string;}
export interface RegisterTransactionItemPar extends ISession {item :TransactionItems; costCenterID : number[];}
export interface GetMeasureUnitsPar extends ISession { }
export interface GetSystemParametersWebPar extends ISession{names:string[]; }
export interface GetMeasureUnitPar extends ISession {id:number; }
export interface GetAllPaymentInstrumentTypesPar extends ISession{}
export interface GetAllBanksPar extends ISession{}
export interface GetAllBranchsOfBankPar extends ISession {bankID: number; }
export interface GetAllBankAccountsPar extends ISession{}
export interface GetPaymentInstrumentTypePar extends ISession { itemCode: string }
export interface GetBankAccountPar extends ISession{ mainAccountID: number }
export interface GetBankBranchInfoPar extends ISession { branchID: number }
export interface GetBankInfoPar extends ISession { bankID: number }
export default class BNFinanceService extends ServiceBase{
    getAllClientFiles=(session:ISession)=> axios.post(`${BN_FINANCE_API}getAllClientFiles`,session,{...config})
    GetItemCategories2=(params : GetItemCategories2Par )=> axios.post(`${BN_FINANCE_API}GetItemCategories2`,params,{...config})
    GetItemsInCategory=(params: GetItemsInCategoryPar )=>axios.post(`${BN_FINANCE_API}GetItemsInCategory`,params,{...config})
    SearchTransactionItems= (params:SearchTransactionItemsPar)=>axios.post(`${BN_FINANCE_API}SearchTransactionItems`,params,{...config})
    GetSystemParameters=(params:GetSystemParamtersPar)=>axios.post(`${BN_FINANCE_API}GetSystemParameters`,params,{...config})
    GetTransactionItems=(params:GetTransactionItemsPar)=>axios.post(`${BN_FINANCE_API}GetTransactionItems`,params,{...config})
    RegisterTransactionItem = (params:RegisterTransactionItemPar)=>axios.post(`${BN_FINANCE_API}RegisterTransactionItem`,params,{...config})
    GetMeasureUnits= (params:GetMeasureUnitsPar)=>axios.post(`${BN_FINANCE_API}GetMeasureUnits`,params,{...config})
    GetSystemParametersWeb = (params : GetSystemParametersWebPar)=> axios.post(`${BN_FINANCE_API}GetSystemParametersWeb`,params,{...config})
    GetMeasureUnit = (params : GetMeasureUnitPar)=> axios.post(`${BN_FINANCE_API}GetMeasureUnit`,params,{...config})
    GetAllPaymentInstrumentTypes = (params : GetAllPaymentInstrumentTypesPar)=> axios.post(`${BN_FINANCE_API}getAllPaymentInstrumentTypes`,params,{...config})
    GetAllBanks = (params : GetAllBanksPar)=> axios.post(`${BN_FINANCE_API}getAllBanks`,params,{...config})
    GetAllBranchsOfBank = (params : GetAllBranchsOfBankPar)=> axios.post(`${BN_FINANCE_API}getAllBranchsOfBank`,params,{...config})
    GetAllBankAccounts = (params : GetAllBankAccountsPar)=> axios.post(`${BN_FINANCE_API}GetAllBankAccounts`,params,{...config})
    GetPaymentInstrumentType = (params : GetPaymentInstrumentTypePar)=> axios.post(`${BN_FINANCE_API}getPaymentInstrumentType`,params,{...config})
    GetBankAccount = (params : GetBankAccountPar)=> axios.post(`${BN_FINANCE_API}GetBankAccount`,params,{...config})
    GetBankBranchInfo = (params : GetBankBranchInfoPar)=> axios.post(`${BN_FINANCE_API}getBankBranchInfo`,params,{...config})
    GetBankInfo = (params : GetBankInfoPar)=> axios.post(`${BN_FINANCE_API}getBankInfo`,params,{...config})
}