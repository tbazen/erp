//@ts-ignore
export const baseUrl = window.app && window.app.env.REST_SERVER_URL



export const SECURITY_API = `${baseUrl}app/security/`
export const SERVER_API = `${baseUrl}app/server/`
export const ACCOUNTING_API = `${baseUrl}erp/accounting/`
export const BN_FINANCE_API =`${baseUrl}erp/BNFinance/`

export const PAYROLL_API = `${baseUrl}erp/payroll/`

export const SUBSCRIBER_MANAGEMENT_API = `${baseUrl}erp/subscribermanagment/`

export const JOB_MANAGER_API = `${baseUrl}erp/JobManager/`