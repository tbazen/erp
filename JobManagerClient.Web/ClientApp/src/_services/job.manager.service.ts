import config from "./config/header.config";
import axios from 'axios'
import { JOB_MANAGER_API} from "./config/url.config";
import ISession from "../_model/view_model/session-id";
import { Subscriber } from "../_model/view_model/mn-vm/subscriber-search-result";
import {JobWorker} from "../_model/view_model/mn-vm/internal-service-vm/job-worker";
import {JobAppointment} from "../_model/level0/job-appointment";
import {WorkFlowData} from "../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {JobBillOfMaterial} from "../_model/level0/job-manager-type-library/job-bill-of-material";
// import {JobBillOfMaterial} from "../_model/view_model/active-jobs-vm/job-bill-of-material";

export interface GetCustomerJobsPar extends ISession { customerID: number; onlyAcitve: boolean;}
export enum ServiceType {
    New_Line = 1,
    Leak_Diagnosis = 2,
    Connection_Upgrading = 3,
    Meter_Temporary_Return = 3,
    Ownership_Transfer = 4,
    Connection_Transfer = 5,
    Replacement_of_Brokendown_Meter = 6,
    Replacement_of_Meter = 7,
    Defective_Meter_Diagnosis = 8,
    Meter_Return = 9,
    Sewer_Service = 10,
    Contract_Renewal = 12,
    Hydrant_Service = 13
}
export interface JobData {
    id: number;
    jobNo: string | null;
    newCustomer: Subscriber | null;
    description: string;
    serviceTypes: ServiceType[];
    logDate: string;
    startDate: string;
    statusDate: string;
    status: number;
    applicationType: number;
    customerID: number;
    completionDocumentID: number;
}
export interface AddJobPar extends ISession {
    job: JobData;
    data: BinObject;
}
export interface BinObject {
    data: string | null;
}
export interface AddJobWebPar<T> extends ISession { job: JobData; data: T | null;dataType: string; }
export interface GetFilteredJobsPar extends ISession { date: string; }
export interface UpdateJobWorkerPar {
    sessionID: string;
    worker: JobWorker;
}
export interface CreateJobWorkerPar {
    sessionID: string;
    worker: JobWorker;
}
export interface DeleteWorkerPar {
    sessionID: string;
    userName: string;
}
export interface ChangeWorkerStatusPar {
    sessionID: string;
    userID: string;
    active: boolean;
}

export interface GetJobStatusPar extends ISession{ status: number }
export interface GetJobPar extends ISession { jobID: number}

export interface SearchJobPar {
    sessionID: string;
    query: string;
    index: number;
    pageSize: number;
}
export interface SearchJobOut {
    NRecords: number;
    _ret: JobData[];
}

export interface GetPossibleNextStatusPar extends ISession{ date : string; jobID: number; }
export interface GetJobBOMPar extends ISession{jobID:number}
export interface GetJobHistoryPar extends ISession { jobID:number; }

export interface ChangeJobStatusPar extends ISession { jobID:number; date:string; newStatus:number; node:string; }
export interface RemoveJobPar extends ISession {jobHandle:number}
export interface GetWorkerPar extends ISession { userID: string; }
export interface SaveConfigurationPar<T> extends ISession{
    typeID:number; config:T; typeName:string;
}
export interface GetConfigurationWeb extends ISession { typeID: number;  type:string; }
export interface GetJobTypePar extends ISession { typeID: number; }
export interface GetJobAppointmentsPar extends ISession {jobHandle:number;}
export interface AddAppointmentPar extends ISession { appointment: JobAppointment;}
export interface CloseAppointmentPar extends ISession { appointmentID:number; }
export interface UpdateAppointmentPar extends ISession { appointment: JobAppointment;}
export interface GetWorkFlowDataWebPar extends ISession { jobID:number; typeID:number; key:number; fullData:boolean; }
export interface GetBOMTotalPar extends ISession { bomID:number; }
export interface UpdateJobWebPar<T extends WorkFlowData> extends ISession {job:JobData; data:T | null;dataType:string;}
export interface SetWorkFlowDataWebPar<T extends WorkFlowData> extends ISession{data:T; dataType:string;}
export interface GetInvoicePreviewPar extends ISession { bom:JobBillOfMaterial;}
export interface DeleteBOMPar extends ISession {bomID:number;}
export interface SetBillofMateialPar extends ISession { bom:JobBillOfMaterial; }
export interface GetSystemParameterPar extends ISession{ p:string; }
export interface GetLastCashHandoverDocumentPar extends ISession {cashAccountID: number;}
export interface GetPaymentCenterPaymentInstrumentsPar extends ISession{cashAccountID:number; from:string; to:string;}

export default class JobManagerService{
    GetCustomerJobs = (params:GetCustomerJobsPar) => axios.post(`${JOB_MANAGER_API}getCustomerJobs`,params,{...config})
    AddJob = (params : AddJobPar) =>axios.post(`${JOB_MANAGER_API}AddJob`,params,{...config})
    GetAllWorkers=(params:ISession)=>axios.post(`${JOB_MANAGER_API}GetAllWorkers`,params,{...config})
    GetFilteredJobs=(params : GetFilteredJobsPar )=> axios.post(`${JOB_MANAGER_API}GetFilteredJobs`,params,{...config})
    AddJobWeb = <T>(params : AddJobWebPar<T> ) =>axios.post(`${JOB_MANAGER_API}AddJobWeb`,params,{...config});
    UpdateJobWeb = <T extends WorkFlowData>(params : UpdateJobWebPar<T> ) =>axios.post(`${JOB_MANAGER_API}UpdateJobWeb`,params,{...config});
    SetWorkFlowDataWeb = <T extends WorkFlowData>(params : SetWorkFlowDataWebPar<T> ) =>axios.post(`${JOB_MANAGER_API}SetWorkFlowDataWeb`,params,{...config});

    UpdateJobWorker = (params :UpdateJobWorkerPar ) => axios.post(`${JOB_MANAGER_API}UpdateJobWorker`,params,{...config})
    CreateJobWorker = (params :CreateJobWorkerPar ) => axios.post(`${JOB_MANAGER_API}CreateJobWorker`,params,{...config})
    DeleteWorker = (params :DeleteWorkerPar ) => axios.post(`${JOB_MANAGER_API}DeleteWorker`,params,{...config});
    ChangeWorkerStatus = (params :ChangeWorkerStatusPar ) => axios.post(`${JOB_MANAGER_API}ChangeWorkerStatus`,params,{...config});
    GetJobStatus = (params : GetJobStatusPar)=> axios.post(`${JOB_MANAGER_API}getJobStatus`,params,{...config});
    GetJob = (params : GetJobPar)=> axios.post(`${JOB_MANAGER_API}GetJob`,params,{...config});
    SearchJob = (params : SearchJobPar)=> axios.post(`${JOB_MANAGER_API}SearchJob`,params,{...config});
    GetJobBOM = (params : GetJobBOMPar)=> axios.post(`${JOB_MANAGER_API}GetJobBOM`,params,{...config});
    getPossibleNextStatus = (params : GetPossibleNextStatusPar)=> axios.post(`${JOB_MANAGER_API}getPossibleNextStatus`,params,{...config});
    GetJobHistory = (params : GetJobHistoryPar)=> axios.post(`${JOB_MANAGER_API}GetJobHistory`,params,{...config});
    ChangeJobStatus = (params : ChangeJobStatusPar)=> axios.post(`${JOB_MANAGER_API}ChangeJobStatus`,params,{...config});
    RemoveJob = (params : RemoveJobPar)=> axios.post(`${JOB_MANAGER_API}RemoveJob`,params,{...config});
    GetWorker = (params : GetWorkerPar)=> axios.post(`${JOB_MANAGER_API}GetWorker`,params,{...config});
    GetConfigurationWeb = (params : GetConfigurationWeb)=> axios.post(`${JOB_MANAGER_API}GetConfigurationWeb`,params,{...config});
    SaveConfiguration = <T>(params : SaveConfigurationPar<T>)=> axios.post(`${JOB_MANAGER_API}saveConfiguration`,params,{...config});
    GetJobType = (params : GetJobTypePar)=> axios.post(`${JOB_MANAGER_API}getJobType`,params,{...config});
    GetJobAppointments = (params : GetJobAppointmentsPar)=> axios.post(`${JOB_MANAGER_API}GetJobAppointments`,params,{...config});
    AddAppointment = (params : AddAppointmentPar)=> axios.post(`${JOB_MANAGER_API}AddAppointment`,params,{...config});
    CloseAppointment = (params : CloseAppointmentPar)=> axios.post(`${JOB_MANAGER_API}CloseAppointment`,params,{...config});
    UpdateAppointment = (params : UpdateAppointmentPar)=> axios.post(`${JOB_MANAGER_API}UpdateAppointment`,params,{...config});
    GetWorkFlowDataWeb = (params : GetWorkFlowDataWebPar)=> axios.post(`${JOB_MANAGER_API}GetWorkFlowDataWeb`,params,{...config});
    GetBOMTotal = (params : GetBOMTotalPar)=> axios.post(`${JOB_MANAGER_API}getBOMTotal`,params,{...config});
    GetInvoicePreview = (params : GetInvoicePreviewPar)=> axios.post(`${JOB_MANAGER_API}GetInvoicePreview`,params,{...config});
    DeleteBOM = (params : DeleteBOMPar)=> axios.post(`${JOB_MANAGER_API}DeleteBOM`,params,{...config});
    SetBillofMateial = (params : SetBillofMateialPar)=> axios.post(`${JOB_MANAGER_API}SetBillofMateial`,params,{...config});
    GetSystemParameterWeb = (params : GetSystemParameterPar)=> axios.post(`${JOB_MANAGER_API}GetSystemParameterWeb`,params,{...config});
    GetLastCashHandoverDocument = (params : GetLastCashHandoverDocumentPar)=> axios.post(`${JOB_MANAGER_API}getLastCashHandoverDocument`,params,{...config});
    GetPaymentCenterPaymentInstruments = (params : GetPaymentCenterPaymentInstrumentsPar)=> axios.post(`${JOB_MANAGER_API}getPaymentCenterPaymentInstruments`,params,{...config});
}