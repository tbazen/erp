import ServiceBase from "./config/service.base";
import config from "./config/header.config";
import axios from 'axios'
import {PAYROLL_API} from "./config/url.config";
import {SearchEmployeePar} from "../_model/view_model/employee-search-par";
import ISession from "../_model/view_model/session-id";



export interface ChangeEmployeeOrgUnitPar extends ISession{
    id: number
    newOrgUnitID: number
    date: string
}

export interface GetEmployeePar extends ISession{ID: number;}
export interface GetEmployeesPar extends ISession { orgID:number; includeSubOrg:boolean; }
export interface GetOrgUnitsPar extends ISession { pid:number; }

export default class PayrollService extends ServiceBase{
    SearchEmployee = (searchParams:SearchEmployeePar) =>axios.post(`${PAYROLL_API}SearchEmployee`,searchParams,{...config})
    GetOrgUnits = (params:GetOrgUnitsPar) =>axios.post(`${PAYROLL_API}GetOrgUnits`,params,{...config})
    ChangeEmployeeOrgUnit=(params:ChangeEmployeeOrgUnitPar)=>axios.post(`${PAYROLL_API}ChangeEmployeeOrgUnit`,params,{...config})
    GetEmployee=(params:GetEmployeePar)=>axios.post(`${PAYROLL_API}GetEmployee`,params,{...config})
    GetEmployees=(params:GetEmployeesPar)=>axios.post(`${PAYROLL_API}GetEmployees`,params,{...config})
}