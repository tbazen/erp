import ServiceBase from "./config/service.base"
import config from "./config/header.config"
import axios from 'axios'
import ISession from "../_model/view_model/session-id";
import {SUBSCRIBER_MANAGEMENT_API} from "./config/url.config";
import { SubscriberType } from "../_enum/mn-enum/subscriber-type";
import {CreditScheme} from "../_model/level0/subscriber-managment-type-library/credit-scheme";
import {
    BWFMeterReading,
    MeterReadingFilter
} from "../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import {ConnectionProfileOptions} from "../_model/client/customer-profile-builder";


export interface GetCustomerProfilePar extends ISession{ customerId: number,options: ConnectionProfileOptions }

export interface GetSubscriptions2Par extends ISession {
    query: string;
    fields: SubscriberSearchField;
    multipleVersion: boolean;
    kebele: number;
    zone: number;
    dma: number;
    index: number;
    page: number;
}
export enum SubscriberSearchField {
    CustomerName = 1,
    CustomerCode = 2,
    ConstactNo = 4,
    MeterNo = 8,
    PhoneNo = 16,
    All = 31
}
export interface GetCustomersPar extends ISession{
    query: string;
    kebele: number;
    index: number;
    pageSize: number;
}
export interface GetSubscriber2Par extends ISession{ id:number}
export interface  GetSubscriptionsPar extends ISession{ subscriberID:number, version:number}
export interface GetCustomerSubtypePar extends ISession{type: SubscriberType;subTypeID: number;}
export interface GetDMAPar extends ISession{id:number}
export interface GetPressureZonePar extends ISession{id:number}
export interface BWFGetMeterPreviousReadingPar extends ISession{subcriptionID:number,periodID:number}
export interface GetSystemParametersWebPar  extends ISession { names:string[] }
export interface GetCustomerBillRecordPar extends ISession {billRecordID: number; }
export interface GetCustomerCreditSchemesPar extends ISession{ customerID:number; }
export interface GetCustomerCreditSchemesOut {settlement: number[];_ret: CreditScheme[]; }
export interface GetBillPeriodsPar extends ISession { year:number; ethiopianYear:boolean; }
export interface GetBillDocumentsPar extends ISession{mainTypeID:number;customerID:number; connectionID:number; periodID:number; excludePaid:boolean; }
export interface GetBillPeriodPar extends ISession { periodID: number;}
export interface BWFGetMeterReadingByPeriodIDPar extends ISession { subcriptionID: number;periodID: number; }
export interface BWFGetMeterReadings2Par extends ISession {filter:MeterReadingFilter; index:number; pageSize:number; }
export interface BWFGetMeterReadings2Out { nRecords:number;_ret:BWFMeterReading[]; }
export interface GetSubscriptionPar extends ISession {id:number; version:number}
export interface GetKebelePar extends ISession{ kebeleID:number}
export interface GetSubscription2Par extends ISession {contractNo:string; version:number; }
export interface GetCurrentPaymentCenterPar extends ISession {}
export interface GetSystemParametersPar extends ISession { names:string[]}
export interface GetCustomerBillItemsPar extends ISession{billRecordID: number;}
export interface GetPaymentCenterByCashAccountPar extends ISession{ account: number;}
export interface GetAllMeterReaderEmployeesPar extends ISession {periodID: number}
export interface GetAllKebelesPar extends ISession {  }
export interface GetAllPaymentCentersPar extends ISession {  }
export default class SubscriberManagementService extends ServiceBase{
    GetAllKebeles = (params:GetAllKebelesPar) => axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetAllKebeles`,params,{...config})
    GetAllDMAs = (params:ISession) => axios.post(`${SUBSCRIBER_MANAGEMENT_API}getAllDMAs`,params,{...config})
    GetAllPressureZones = (params: ISession)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getAllPressureZones`,params,{...config})
    GetSubscriptions2=(params:GetSubscriptions2Par)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetSubscriptions2`,params,{...config})
    GetSubscriber2=(params:GetSubscriber2Par)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetSubscriber2`,params,{...config})
    GetCustomers=(params:GetCustomersPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetCustomers`,params,{...config})
    GetAllPaymentCenters=(params : GetAllPaymentCentersPar)=> axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetAllPaymentCenters`,params,{...config})
    GetSystemParametersWeb = (params : GetSystemParametersWebPar)=> axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetSystemParametersWeb`,params,{...config})
    ///API Used for fetching customer profile
    GetSubscriptions=(params:GetSubscriptionsPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetSubscriptions`,params,{...config})
    GetCustomerSubtype=(params:GetCustomerSubtypePar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getCustomerSubtype`,params,{...config})
    GetDMA=(params:GetDMAPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getDMA`,params,{...config})
    GetPressureZone=(params:GetPressureZonePar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getPressureZone`,params,{...config})
    BWFGetMeterPreviousReading = (params :BWFGetMeterPreviousReadingPar )=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}BWFGetMeterPreviousReading`,params,{...config})
    GetBillDocuments= (params :GetBillDocumentsPar )=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getBillDocuments`,params,{...config})
    GetCustomerCreditSchemes=(params : GetCustomerCreditSchemesPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getCustomerCreditSchemes`,params,{...config})
    GetCustomerBillRecord=(params : GetCustomerBillRecordPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getCustomerBillRecord`,params,{...config})
    GetBillPeriods=(params : GetBillPeriodsPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetBillPeriods`,params,{...config})
    GetBillPeriod=(params : GetBillPeriodPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetBillPeriod`,params,{...config})
    BWFGetMeterReadingByPeriodID=(params : BWFGetMeterReadingByPeriodIDPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}BWFGetMeterReadingByPeriodID`,params,{...config})
    BWFGetMeterReadings2=(params : BWFGetMeterReadings2Par)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}BWFGetMeterReadings2`,params,{...config})
    GetCreditScheme=(params:number)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getCreditScheme`,params,{...config})
    GetKebele=(params:GetKebelePar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getKebele`,params,{...config})
    GetSubscription=(params:GetSubscriptionPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetSubscription`,params,{...config})
    GetSubscription2=(params:GetSubscription2Par)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetSubscription2`,params,{...config})
    GetCurrentPaymentCenter=(params:GetCurrentPaymentCenterPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetCurrentPaymentCenter`,params,{...config})
    GetCustomerBillItems=(params:GetCustomerBillItemsPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}getCustomerBillItems`,params,{...config})
    GetPaymentCenterByCashAccount=(params:GetPaymentCenterByCashAccountPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetPaymentCenterByCashAccount`,params,{...config})
    GetAllMeterReaderEmployees=(params:GetAllMeterReaderEmployeesPar)=>axios.post(`${SUBSCRIBER_MANAGEMENT_API}GetAllMeterReaderEmployees`,params,{...config})
}