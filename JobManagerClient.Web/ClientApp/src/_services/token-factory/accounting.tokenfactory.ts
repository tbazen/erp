import { CancelTokenSource } from 'axios'
import TokenFactoryBase from './tokenfactory.base';

/**
 * This class is used to generate token that will be use to cancel API calls 
 */
export default class AccountingTokenFactory extends TokenFactoryBase{

    GET_ACCOUTING_TOKEN:CancelTokenSource
    constructor() {
        super()
        this.GET_ACCOUTING_TOKEN = this.getTokenSource()
    }

    getAccountingToken = ()=> this.GET_ACCOUTING_TOKEN.token
    cancelAccountingToken = () => this.GET_ACCOUTING_TOKEN.cancel()
    renewAccountingTokenSource = ()=>{this.GET_ACCOUTING_TOKEN = this.getTokenSource()}
}