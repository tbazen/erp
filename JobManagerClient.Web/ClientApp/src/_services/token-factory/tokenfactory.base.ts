import Axios, { CancelTokenSource } from "axios";

export default class TokenFactoryBase{
    protected TOKEN_FACTORY = Axios.CancelToken
    protected getTokenSource = (): CancelTokenSource => this.TOKEN_FACTORY.source()  
    
}