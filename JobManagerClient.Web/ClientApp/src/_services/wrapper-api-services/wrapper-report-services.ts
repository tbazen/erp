import axios from "axios";
import config from "../config/header.config";
import {ReportDefination} from "../../_model/level0/accounting-type-library/ehtml";

export interface ImportReportDefinitionPar {
     defination: ReportDefination
     base64ReportFile: string
}

export interface ExportReportDefinitionPar{
    defination:ReportDefination
}

export class WrapperReportServices{
    ImportReportDefinition=(param:ImportReportDefinitionPar) => axios.post(`/api/report/ImportReportDefinition`,param,{...config})
    ExportReportDefinition=(param:ExportReportDefinitionPar) => axios.post(`/api/report/ExportReportDefinition`,param,{...config,responseType:'blob'})
}