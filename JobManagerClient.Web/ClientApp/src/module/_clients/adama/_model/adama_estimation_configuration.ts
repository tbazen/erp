import {ItemsListItem} from "../../../../_model/level0/job-manager-model/items-list-item";
import {ProgressiveRate} from "../../../../_model/level0/intaps-dotnet/progressive-rate";

export class AdamaEstimationConfiguration {
    public pipelineItems:ItemsListItem[]=[];
    public hdpPipelineItems:ItemsListItem[] =[];
    public waterMeterItems:ItemsListItem[] = [];
    public newLineFixedServiceItems:ItemsListItem[] = [];
    public maintenanceFixedServiceItems:ItemsListItem[] = [];
    public surveyItems:ItemsListItem[]=[];

    public materialHandlingMultiplier?:string;
    public privateServiceCharge:ProgressiveRate = new ProgressiveRate();
    public comercialServiceCharge:ProgressiveRate = new ProgressiveRate();
    public othersServiceCharge:ProgressiveRate = new ProgressiveRate();

    public itemUnmeteredWaterFee?:string;
    public itemServiceChargePrivate?:string
    public itemServiceChargeComercial?:string
    public itemServiceChargeOther?:string
    public lastMultiplier?:string
    public deposit:number[] = [];
    public depositMeterType:string[] = [];
}