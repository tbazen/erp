import React from 'react';
import CButton from "../../../../../shared/core/cbutton/cbutton";
import {Tabs} from "antd";
import {GeneralFormContainer} from "./general/general-form-container";
import {PrivateServiceChargeFormContainer} from "./private-service-charge/private-service-charge-form-container";
import {CommercialServiceChargeFormContainer} from "./commercial-service-charge/commercial-service-charge-form-container";
import {OtherServiceChargeFormContainer} from "./other-service-charge/other-service-charge-form-container";
import {DepositRuleFormContainer} from "./deposit-rule/deposit-rule-form-container";
import {
    InitEstimationConfigurationParams,
    useEstimationConfigurationInitializer
} from "../../../../../shared/hooks/configuration-estimation-initializer";
import {NAME_SPACES} from "../../../../../_constants/model-namespaces";
import {AdamaEstimationConfiguration} from "../../_model/adama_estimation_configuration";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";

const { TabPane } = Tabs;

export function EstimationConfigurationContainer(){
    const initEstimationConfigurationParams:InitEstimationConfigurationParams<AdamaEstimationConfiguration> = {
        defaultConstructor: AdamaEstimationConfiguration,
        configModelNameSpace: NAME_SPACES.ESTIMATION_CONFIGURATION.ADAMA
    }
    const [ configuration_state , configuration , setConfiguration, saveConfigurationChanges ] = useEstimationConfigurationInitializer(initEstimationConfigurationParams)

    async function submitConfiguration(){ await saveConfigurationChanges() }

    return (
        <StateMachineHOC state={configuration_state}>
            <Tabs
                className={'paper'}
                style={{padding:'10px',paddingBottom:'10px'}}
                defaultActiveKey="1"
                tabBarExtraContent={<CButton disabled type={'primary'} onClick={submitConfiguration}>Save Configuration</CButton>}
            >
                <TabPane tab="General" key="1">
                    <GeneralFormContainer/>
                </TabPane>
                <TabPane tab="Private Service Charge" key="2">
                    <PrivateServiceChargeFormContainer/>
                </TabPane>
                <TabPane tab="Commercial Service Charge" key="3">
                    <CommercialServiceChargeFormContainer/>
                </TabPane>
                <TabPane tab="Other Service Charge" key="4">
                    <OtherServiceChargeFormContainer/>
                </TabPane>
                <TabPane tab="Deposit Rule" key="5">
                    <DepositRuleFormContainer/>
                </TabPane>
            </Tabs>
        </StateMachineHOC>
    )
}