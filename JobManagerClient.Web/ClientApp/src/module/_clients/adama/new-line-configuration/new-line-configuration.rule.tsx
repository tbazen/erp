import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import {NewLineRule} from "../../../main/new line/new-line.rule";
import {EstimationConfigurationContainer} from "./estimation configuration/estimation-configuration-container";

@JobRuleClientDecorator({
    jobType:StandardJobTypes.NEW_LINE,
    jobName:"New Line",
    route:ROUTES.JOB.NEW_LINE.INDEX,

    hasConfiguration:true,
    configurationName:"Configure Estimation",
    configurationRoute:ROUTES.CONFIGURATION.NEW_LINE
})
export class NewLineConfigurationRule extends NewLineRule{
    getConfigurationForm(){
        return EstimationConfigurationContainer
    }
}

