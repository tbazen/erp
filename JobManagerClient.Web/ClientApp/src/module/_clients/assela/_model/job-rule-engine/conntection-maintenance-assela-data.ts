import {WorkFlowData} from "../../../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {MeterData} from "../../../../../_model/view_model/mn-vm/meter-data";
import {Subscription} from "../../../../../_model/level0/subscriber-managment-type-library/subscription";

export class ConntectionMaintenanceAsselaData extends WorkFlowData{
    public estimation:boolean = false;
    public changeMeter: boolean = false;
    public relocateMeter: boolean = false;
    public connectionID: number =0;
    public updatedMeterData: MeterData | null = null;
    public updatedSubscription:Subscription  | null = null;
    public upateConneciton: boolean = false;

    public hasApplicationFee:boolean =   true

}