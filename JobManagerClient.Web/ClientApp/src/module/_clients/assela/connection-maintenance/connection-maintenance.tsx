import React, { useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import { Card, Button } from 'antd';
import {IBreadcrumb} from "../../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../../_constants/routes";
import ButtonGridContainer from "../../../../shared/screens/button-grid-container/button-grid-container";
import {openDrawerModal} from "../../../../_redux_setup/actions/drawer-modal-actions";
import {
    GetSubscriptions2Par,
    SubscriberSearchField
} from "../../../../_services/subscribermanagment.service";
import {SubscriberSearchResult} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {
    fetchSubscriberList, subscriberListClear,
} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import {useGlobalSearchBar} from "../../../../shared/hooks/manage-global-search";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {ConnectionMaintenanceFormContainer} from "./form/connection-maintenance-form-container";

const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.CONNECTION_MAINTENANCE,
        breadcrumbName:'Connection Maintenance'
    }
]

export const ConnectionMaintenanceContainer = () =>{
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const initSearchParams : GetSubscriptions2Par={
        sessionId:auth.sessionId,
        query: '',
        fields: SubscriberSearchField.CustomerName | SubscriberSearchField.ConstactNo,
        multipleVersion: false,
        kebele: -1,
        zone: -1,
        dma: -1,
        index: 0,
        page: 30
    }
    const [searchParams , setSearchParams] = useState<GetSubscriptions2Par>(initSearchParams)
    const subscribersState = useSelector((appState:ApplicationState)=>appState.subscription_search_result_state)

    const searchInputHandler = (event:any) =>{
        const query = event.target.value
        setSearchParams({...initSearchParams,query})
    }
    useBreadcrumb(backwardPath)
    useGlobalSearchBar({ searchHandler:searchInputHandler, searchPlaceHolder:'Customer Code' ,cleanupSearchResult:subscriberListClear})
    //API fetch initiator effect
    useEffect(()=>{
        if(searchParams.query.trim() !== '')
            dispatch(fetchSubscriberList(searchParams))
    },[searchParams])

    const SubscriberCard = (props:SubscriberSearchResult)=>{
        return (
            <Card
                title={props.subscriber.name}
                bordered={false}
                actions={[
                    <Button onClick={()=>dispatch(openDrawerModal(<ConnectionMaintenanceFormContainer data={props}/>))} icon={'plus'} block type={'link'}>
                        Connection Maintenance
                    </Button>
                ]}
            >
                <p>{props.subscription.contractNo}</p>
            </Card>
        )
    }

    return (
        <StateMachineHOC state={subscribersState}>
            <ButtonGridContainer spacing={6} lg={{span:6}} component={SubscriberCard} listItem={subscribersState.payload._ret}/>
        </StateMachineHOC>
    )
}
