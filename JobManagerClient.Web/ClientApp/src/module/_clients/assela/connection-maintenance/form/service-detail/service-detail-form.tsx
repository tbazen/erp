import React, {useEffect, useState} from 'react';
import {Checkbox, Col, Divider, Row} from "antd";


interface ServiceDetailFormPrpos{
    handleServiceDetailChange? : (serviceDetail: ServiceDetail)=>void
    serviceDetail?: ServiceDetail
}
export interface ServiceDetail{
    relocateMeter: boolean,
    estimation: boolean
}
export function ServiceDetailForm(props: ServiceDetailFormPrpos){
    const [ serviceDetail,setServiceDetail ] = useState<ServiceDetail>({
        relocateMeter: (props.serviceDetail && props.serviceDetail.relocateMeter) || false,
        estimation: (props.serviceDetail && props.serviceDetail.estimation) || false
    })
    useEffect(()=>{
        props.handleServiceDetailChange &&
        props.handleServiceDetailChange(serviceDetail)
    },[ serviceDetail ])
    return (
        <Row gutter={2}>
            <Col lg={{span:12}}>
               <div className={'paper'} style={{padding:'10px'}}>
                   <Divider orientation={'left'}>Relocation/ Estimation </Divider>
                   <Checkbox
                       checked={serviceDetail.relocateMeter}
                       onChange={(e)=>setServiceDetail({...serviceDetail,relocateMeter:e.target.checked})}
                   >Relocate Meter</Checkbox>
                   <Checkbox
                       checked={serviceDetail.estimation}
                       onChange={(e)=>setServiceDetail({...serviceDetail,estimation:e.target.checked})}
                   >Estimation</Checkbox>
               </div>
            </Col>
        </Row>
    )
}
