import React, { useEffect, useState } from 'react'
import {ROUTES } from '../../../../_constants/routes';
import {IBreadcrumb } from '../../../../_model/state-model/breadcrumd-state';
import {useDispatch, useSelector } from 'react-redux';
import {CAlert } from "../../../../shared/core/calert/calert"
import pushPathToBC from '../../../../_redux_setup/actions/breadcrumb-actions';
import {Row, Col, Divider } from 'antd';
import CustomerSearchBar from '../../../../shared/components/customer-list-table/customer-search-bar';
import CustomersListTable from '../../../../shared/components/customer-list-table/customer-list-table';
import SharedJobFormFields from '../../../../shared/components/job application form/shared-job-field';
import {SubscriberSearchResult, Subscriber } from '../../../../_model/view_model/mn-vm/subscriber-search-result';
import CButton from '../../../../shared/core/cbutton/cbutton';
import {ApplicationState } from '../../../../_model/state-model/application-state';
import {AuthenticationState } from '../../../../_model/state-model/auth-state';
import {AddJobWebPar, JobData, UpdateJobWebPar} from '../../../../_services/job.manager.service';
import {initialJobData } from '../../../../_helpers/initial value/init-jobdata';
import {
    requestAddJobWeb,
    requestUpdateJobWeb,
    subscriberListClear
} from '../../../../_redux_setup/actions/mn-actions/main-actions';
import CustomerInformationContainer from '../../../../shared/components/customer-information/customer-information';
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {WorkFlowData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {getLocalDateString} from "../../../../_helpers/date-util";
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import {useParams} from "react-router-dom";
import {useEditJobInitializer} from "../../../../shared/hooks/edit-job-intializer";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.INDEX,
        breadcrumbName:'Other Services'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.CUSTOMER_SPONSORED_NETWORK_WORK,
        breadcrumbName:'Customer Sponsored Network Expansion'
    }
]





const CustomerSponsoredNetworkExpansion = ()=>{
    const dispatch = useDispatch()
    const { jobId } = useParams()
    const [selectedCustomer,setSelectedCustomer] = useState<SubscriberSearchResult | undefined>(undefined)
    const [auth] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])
    const [ jobData, setJobData] = useState<JobData>({...initialJobData.job,id:(jobId && !isNaN(+jobId) && +jobId)|| -1 , applicationType: StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK})
    const [ isNewJob, setIsNewJob ] = useState<boolean>( jobData.id===-1)
    const [ newCustomer , setNewCustomer] = useState<Subscriber>()
    useBreadcrumb(backwardPath)
    const [ registeredJob,setRegisteredJob ] = useEditJobInitializer({jobId:(jobId && +jobId)||-1,jobType:StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK})

    useEffect(()=>{return ()=>{ dispatch(subscriberListClear()) }},[])
    useEffect(()=>{ setIsNewJob(jobData.id === -1 ) },[jobData])
    useEffect(()=>{
        if(registeredJob){
            setJobData(registeredJob)
        }
    },[ registeredJob ])
    useEffect(()=>{
        selectedCustomer !== undefined &&
        setJobData({ ...jobData,description:'',startDate: getLocalDateString(), statusDate: getLocalDateString(), logDate: getLocalDateString() })
    },[ selectedCustomer ])


    const handleNewCustomerDataChange = (customer: Subscriber) => { setNewCustomer(customer)}
    const submitNewCustomerSponsoredNetworkData = () => {
        if(newCustomer){
            jobData.newCustomer = newCustomer;
            jobData.customerID = -1;
            const params: AddJobWebPar<WorkFlowData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: null,
                dataType: ''
            }
            dispatch(requestAddJobWeb(params))
        }
    }
    const submitCustomerSponsoredNetworkData = () => {
        if(isNewJob){
            if(selectedCustomer){
                jobData.customerID = selectedCustomer.subscriber.id;
                jobData.newCustomer = null;
                const params: AddJobWebPar<WorkFlowData>={
                    job:jobData,
                    sessionId: auth.sessionId,
                    data: null,
                    dataType: ''
                }
                dispatch(requestAddJobWeb(params))
            }
        } else {
            const updateJobParams: UpdateJobWebPar<WorkFlowData> = {
                job: jobData,
                sessionId: auth.sessionId,
                data: null,
                dataType:''
            }
            dispatch(requestUpdateJobWeb<WorkFlowData>(updateJobParams))
        }
    }
    return (
        <Row gutter={4}>
            {
                isNewJob &&
                <Col span={16}>
                    <CustomerSearchBar
                        appendedForm={
                            <SharedJobFormFields
                                dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                                formItemLayout={'horizontal'}
                            />
                        }
                        title={'Customer Sponsored Network Expansion'}
                        updateCustomerData={handleNewCustomerDataChange}
                        submitCustomerData={submitNewCustomerSponsoredNetworkData}
                    />
                    <CustomersListTable
                        setSelectedCustomers= {setSelectedCustomer}
                        withSelection
                        height='66vh'
                    />
                </Col>
            }
            <Col span={8}>
                <Row style={{marginBottom:'5px'}}>
                    <div className={'paper'} style={{padding:'10px',backgroundColor:'#fff'}}>
                        <SharedJobFormFields
                            disableDate={selectedCustomer!== undefined}
                            disableDescription={selectedCustomer!==undefined}
                            dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                </Row>
                <Row>
                    {
                        isNewJob &&
                        <div className={'paper'}>
                            <div style={{width: '100%', overflow: 'auto', maxHeight: '52vh', padding: '10px'}}>
                                <p>Selected Customer</p>
                                <Divider/>
                                {
                                    selectedCustomer === undefined &&
                                    <Row>
                                        <CAlert
                                            message="Customer Not Selected"
                                            description='To create customer sponsored network expansion please select one customer, or use the "Add new Customer" button to create new customer!'
                                            type="error"
                                        />
                                    </Row>
                                }
                                {
                                    selectedCustomer !== undefined &&
                                    <Row>
                                        <CustomerInformationContainer {...selectedCustomer} />
                                    </Row>
                                }
                            </div>
                        </div>
                    }
                    <div className={'paper'} style={{marginTop:'5px', padding:'10px'}} >
                        <CButton
                            block
                            type={'primary'}
                            onClick={submitCustomerSponsoredNetworkData}
                            disabled={ isNewJob && selectedCustomer === undefined}>Submit</CButton>
                    </div>
                </Row>
            </Col>
        </Row>
    )
}


export default CustomerSponsoredNetworkExpansion
    