import React, {Fragment, useEffect, useState} from 'react';
import { Tabs } from 'antd';
import {GeneralConfigurationContainer} from "./general/general-configuration-container";
import {DepositRuleContainer} from "./deposit-rule/deposit-rule-container";
import {OtherRateContainer} from "./other-rate/other-rate-container";
import {ItemTypedValue, ShashemeneEstimationConfiguration} from "../configuration-model/estimation-configuration-model";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {JobConfigurationState} from "../../../../../_model/state-model/job-configuration-sm/job-configuration-state";
import OperationFailed from "../../../../../shared/screens/status-code/submission-failed";
import CLoadingPage from "../../../../../shared/screens/cloading/cloading-page";
import JobManagerService, {GetConfigurationWeb} from "../../../../../_services/job.manager.service";
import {AuthenticationState} from "../../../../../_model/state-model/auth-state";
import {StandardJobTypes} from "../../../../../_model/view_model/mn-vm/standard-job-types";
import {NAME_SPACES} from "../../../../../_constants/model-namespaces";
import {fetchJobConfiguration} from "../../../../../_redux_setup/actions/job-configuration-actions/job-configuration-actions";
import CButton from "../../../../../shared/core/cbutton/cbutton";
import {ItemsListItem} from "../../../../../_model/level0/job-manager-model/items-list-item";
import {fetchItemsInCategory} from "../../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {GetSystemParametersWebPar} from "../../../../../_services/subscribermanagment.service";
import {NotificationTypes, openNotification} from "../../../../../shared/components/notification/notification";

const { TabPane } = Tabs;

export interface IActiveItem {
    itemKey : string
    title : string
    sector : string
    sectorName? : string
    isItemList? : boolean
}


const job_mgr_service = new JobManagerService()

export function EstimationConfigurationContainer(){
    const [ auth ,configState ] = useSelector<ApplicationState,[  AuthenticationState, JobConfigurationState ]>(appState => [ appState.auth , appState.configuration_state ])
    const [ configuration ,setConfiguration ] = useState<ShashemeneEstimationConfiguration>(new ShashemeneEstimationConfiguration())
    const [ activeItem ,setActiveItem ] = useState<IActiveItem>()
    const [ isConfigurationChanged, setIsConfigurationChanged ] = useState<boolean>(false);
    const dispatch = useDispatch()
    const configParams : GetConfigurationWeb = {
        sessionId : auth.sessionId,
        typeID : StandardJobTypes.NEW_LINE,
        type:NAME_SPACES.ESTIMATION_CONFIGURATION.SHASHEMENE
    }
    const categoryParams : GetSystemParametersWebPar = {
        sessionId:auth.sessionId,
        names : ['meterMaterialCategory']
    }

    useEffect(()=>{
        dispatch(fetchItemsInCategory(categoryParams))
        dispatch(fetchJobConfiguration(configParams))
    },[])
    useEffect(()=>{
        !configState.loading &&
        !configState.error &&
        configState.payload !== undefined &&
        setConfiguration(configState.payload as ShashemeneEstimationConfiguration)
    },[configState])

    function setItemCode( activeItem : IActiveItem, value: string) {
        const tempConfig = {...configuration};
        // @ts-ignore
        tempConfig[activeItem.itemKey][activeItem.sector] = value
        setConfiguration({...tempConfig})
        setIsConfigurationChanged(true);
    }
    function handleSingleItemChange(title : string , sector : string , itemKey : string, isItemList?:boolean,sectorName?:string){
        setActiveItem({title,sector,itemKey,isItemList,sectorName})
    }
    function handleRemoveItemFromList(activeItem: IActiveItem , value : ItemsListItem){
        const tempConfig = {...configuration};
        // @ts-ignore
        const tempList: ItemsListItem[] = tempConfig[activeItem.itemKey]
        const filteredList = tempList.filter(itm => itm.titem.code !== value.titem.code)
        // @ts-ignore
        tempConfig[activeItem.itemKey] = filteredList
        setConfiguration({...tempConfig})
        setIsConfigurationChanged(true);
    }
    function handleAddItemToList(activeItem: IActiveItem , value : ItemsListItem){
        const tempConfig = {...configuration};
        // @ts-ignore
        tempConfig[activeItem.itemKey] = [...tempConfig[activeItem.itemKey],value ]
        setConfiguration({...tempConfig})
        setIsConfigurationChanged(true);
    }
    async function submitConfiguration() {
        try{
            openNotification({
                message:'Saving Configuration',
                description:'saving your changes on estimation parameters, please wait...',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_service.SaveConfiguration<ShashemeneEstimationConfiguration>({
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.NEW_LINE,
                typeName:NAME_SPACES.ESTIMATION_CONFIGURATION.SHASHEMENE,
                config:configuration
            })
            const response = await job_mgr_service.GetConfigurationWeb(configParams)
            setConfiguration(response.data);
            dispatch(fetchItemsInCategory(categoryParams))
            openNotification({
                message:'Configuration Updated',
                description:'estimation parameters updated successfully',
                notificationType:NotificationTypes.SUCCESS
            })
        } catch (e) {
            openNotification({
                message:'Error',
                description:'Failed to save estimation parameters',
                notificationType:NotificationTypes.ERROR
            })
        }
    }
    function handleMeterDepositChange( privateItemCode:string,sector:string, newValue:number){
        const tempConfig = {...configuration}
        const item = tempConfig.meterDeposit.find(itm => itm.privateItemCode === privateItemCode);
        if(item){
            // @ts-ignore
            item[sector] = newValue
            setConfiguration({...tempConfig})
            setIsConfigurationChanged(true)
        } else{
            const newItem :ItemTypedValue = new ItemTypedValue()
            // @ts-ignore
            newItem[sector] = newValue;
            newItem.privateItemCode = privateItemCode
            tempConfig.meterDeposit = [ ...tempConfig.meterDeposit , newItem ]
            setConfiguration({...tempConfig})
            setIsConfigurationChanged(true)
        }
    }
    return (
        <Fragment>
            {
                configState.loading &&
                <CLoadingPage/>
            }
            {
                !configState.loading &&
                configState.error &&
                <OperationFailed errorMessage={configState.message} />
            }
            {
                !configState.error &&
                !configState.loading &&
                configState.payload !== undefined  &&
                <Tabs
                    className={'paper'}
                    style={{padding:'10px',paddingBottom:'10px'}}
                    defaultActiveKey="1"
                    tabBarExtraContent={<CButton disabled={!isConfigurationChanged} type={'primary'} onClick={submitConfiguration}>Save Configuration</CButton>}
                >
                    <TabPane tab="General" key="1">
                        <GeneralConfigurationContainer
                            configuration = { configuration }
                            addItemHandler={handleSingleItemChange}
                            activeItem = { activeItem }
                            setItemCode={setItemCode}
                            handleRemoveItemFromList={handleRemoveItemFromList}
                            handleAddItemToList={handleAddItemToList}
                        />
                    </TabPane>
                    <TabPane tab="Deposit rule" key="2">
                        <DepositRuleContainer
                            configuration = { configuration }
                            handleMeterDepositChange={handleMeterDepositChange}
                        />
                    </TabPane>
                    <TabPane tab="Other Rates" key="3">
                        <OtherRateContainer
                            configuration = { configuration }
                            addItemHandler={handleSingleItemChange}
                            activeItem={activeItem}
                            setItemCode={setItemCode}
                            handleRemoveItemFromList={handleRemoveItemFromList}
                            handleAddItemToList={handleAddItemToList}
                        />
                    </TabPane>
                </Tabs>
            }
        </Fragment>
    )
}