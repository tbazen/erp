import React, {useState} from "react";
import {IActiveItem} from "../estimation-configuration-container";
import {Col, Divider,Row} from "antd";
import CButton from "../../../../../../shared/core/cbutton/cbutton";
import {TransactionItems} from "../../../../../../_model/level0/iERP-transaction-model/transaction-items";
import { useSelector} from "react-redux";
import BNFinanceService from "../../../../../../_services/bn.finance.service";
import {AuthenticationState} from "../../../../../../_model/state-model/auth-state";
import {MeasureUnitState} from "../../../../../../_model/state-model/aj-sm/measure-unit-state";
import {ApplicationState} from "../../../../../../_model/state-model/application-state";
import {ItemCategoryState} from "../../../../../../_model/state-model/ic-sm/item-catagory-state";
import {NotificationTypes, openNotification} from "../../../../../../shared/components/notification/notification";
import {ItemsListItem} from "../../../../../../_model/level0/job-manager-model/items-list-item";
import {ItemRegistrationFormContainer} from "../../../../../../shared/forms/item-registration-form/item-registration-form-container";

const bn_finance_service = new BNFinanceService()

interface IProps{
    activeItem : IActiveItem,
    setItemCode : ( activeItem : IActiveItem, value: string) => void

    handleAddItemToList : ( activeItem : IActiveItem, value: ItemsListItem) => void
}
export function ItemRegistrationForm(props:IProps){
    const [ transactionItem ,setItem ] = useState<TransactionItems>(new TransactionItems())
    const [ auth ] = useSelector<ApplicationState,[AuthenticationState,MeasureUnitState,ItemCategoryState]>(appState=>[ appState.auth,appState.measure_unit_state,appState.item_category_state ])

    const submitTransactionItem = async() => {
        try{
            openNotification({
                message:'Registering Item',
                description:'Registering item please wait',
                notificationType:NotificationTypes.LOADING
            })
            const response = await bn_finance_service.RegisterTransactionItem( {sessionId:auth.sessionId,item: transactionItem,costCenterID:[]})
            const itemCode: string = response.data;
            openNotification({
                message:'Item Registered',
                description:'Item registered successfully!',
                notificationType:NotificationTypes.SUCCESS
            })
            if(!props.activeItem.isItemList){
                props.setItemCode(props.activeItem,itemCode);
            } else {
                const transactionItemResponse = await bn_finance_service.GetTransactionItems({sessionId: auth.sessionId,code:itemCode})
                const tempTransactionItem = transactionItemResponse.data
                const itemTypedValue: ItemsListItem = new ItemsListItem()
                itemTypedValue.titem = tempTransactionItem;
                props.handleAddItemToList(props.activeItem,itemTypedValue);
            }
        } catch(err){
            openNotification({
                message:'Error',
                description:'failed to register item',
                notificationType:NotificationTypes.ERROR
            })
        }
    }
    function handleTransactionItemChange(newTransactionItem:TransactionItems){
        setItem(newTransactionItem)
    }
    return (
        <div className={'paper'} style={{padding:'5px'}}>
            <p>ASSELA....</p>
            <Divider orientation="left">{`${ props.activeItem.title }    ${ (!props.activeItem.isItemList && '[ '+ props.activeItem.sectorName+' ]') || ''   }`}</Divider>

            <ItemRegistrationFormContainer itemChangeHandler={handleTransactionItemChange}/>
           <Row >
                <Col span={6} offset={16}>
                    <CButton
                        onClick={submitTransactionItem}
                        style={{alignSelf:'right'}} type={"primary"} block>Save Item</CButton>
                </Col>
            </Row>
        </div>
    )
}