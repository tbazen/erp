import React from "react";
import {Divider} from 'antd';
import { Form, Input } from 'antd';
import CButton from "../../../../../../shared/core/cbutton/cbutton";
import {ItemTypedValue} from "../../configuration-model/estimation-configuration-model";
import {IActiveItem} from "../estimation-configuration-container";
const InputGroup = Input.Group;

interface IProps{
    title : string
    item : ItemTypedValue
    itemKey : string
    addItemHandler : ( title:string , sector:string , itemKey: string , isItemList?: boolean, sectorName?:string)=> void
    setItemCode : ( activeItem : IActiveItem, value: string) => void
}

export function ItemTypedValueFormCard( props : IProps ){
    const formItemLayout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 18 },
            }
    const formLayout = 'horizontal';
    return (
        <div className={'paper'} style={{width:'100%',padding:'5px'}}>
            <Divider orientation="left">{props.title}</Divider>
            <Form layout={formLayout}>
                <Form.Item label="Private" {...formItemLayout}>
                    <InputGroup compact>
                        <Input
                            style={{ width: '40%' }}
                            value={props.item.privateItemCode}
                            placeholder={'Item'}
                            onChange={(event => props.setItemCode({ itemKey: props.itemKey,title:'',sector:'privateItemCode' },event.target.value))}
                        />
                        <span style={{ width: '20%' }}><CButton type="primary" onClick={()=>props.addItemHandler(props.title,'privateItemCode',props.itemKey,false,'Private')}>Add Item</CButton></span>
                        <Input
                            style={{ width: '40%' }}
                            defaultValue={''+props.item.privateValue}
                            onChange={(event => props.setItemCode({ itemKey: props.itemKey,title:'',sector:'privateValue' },event.target.value))}
                            placeholder={'Value'}  />
                    </InputGroup>
                </Form.Item>
                <Form.Item label="Institutional" {...formItemLayout}>
                    <InputGroup compact>
                        <Input
                            style={{ width: '40%' }}
                            value={props.item.institutionalItemCode}
                            placeholder={'Item'}
                            onChange={(event => props.setItemCode({ itemKey: props.itemKey,title:'',sector:'institutionalItemCode' },event.target.value))}
                        
                        />
                        <span style={{ width: '20%' }}><CButton type="primary" onClick={()=>props.addItemHandler(props.title,'institutionalItemCode',props.itemKey,false, 'Institutional')}>Add Item</CButton></span>
                        <Input
                            onChange={(event => props.setItemCode({ itemKey: props.itemKey,title:'',sector:'institutionalValue' },event.target.value))}
                            defaultValue={''+props.item.institutionalValue}
                            style={{ width: '40%' }}
                            placeholder={'Value'}/>
                    </InputGroup>
                </Form.Item>
                <Form.Item label="Other" {...formItemLayout}>
                    <InputGroup compact>
                        <Input
                            style={{ width: '40%' }}
                            value={props.item.otherItemCode}
                            placeholder={'Item'}
                            onChange={(event => props.setItemCode({ itemKey: props.itemKey,title:'',sector:'otherItemCode' },event.target.value))}
                        />
                        <span style={{ width: '20%' }}><CButton type="primary" onClick={()=>props.addItemHandler(props.title,'otherItemCode',props.itemKey,false,'Other')}>Add Item</CButton></span>
                        <Input
                            onChange={(event => props.setItemCode({ itemKey: props.itemKey,title:'',sector:'otherValue' },event.target.value))}
                            style={{ width: '40%' }}
                            defaultValue={''+props.item.otherValue}
                            placeholder={'Value'} />
                    </InputGroup>
                </Form.Item>
            </Form>
        </div>
    )
}