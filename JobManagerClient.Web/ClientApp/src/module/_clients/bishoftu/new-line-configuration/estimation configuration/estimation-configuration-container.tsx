import React from 'react';
import {Tabs} from "antd";
import CButton from "../../../../../shared/core/cbutton/cbutton";
import {GeneralFormContainer} from "./general/general-form-container";
import {PrivateServiceChargeFormContainer} from "./private-service-charge/private-service-charge-form-container";
import {CommercialServiceChargeFormContainer} from "./commercial-service-charge/commercial-service-charge-form-container";
import {OtherServiceChargeFormContainer} from "./other-service-charge/other-service-charge-form-container";
import {DepositRuleFormContainer} from "./deposit-rule/deposit-rule-form-container";
const { TabPane } = Tabs

export function EstimationConfigurationContainer(){

    function submitConfiguration(){

    }

    return (
        <Tabs
            className={'paper'}
            style={{padding:'10px',paddingBottom:'10px'}}
            defaultActiveKey="1"
            tabBarExtraContent={<CButton disabled type={'primary'} onClick={submitConfiguration}>Save Configuration</CButton>}
        >
            <TabPane tab="General" key="1">
                <GeneralFormContainer/>
            </TabPane>
            <TabPane tab="Private Service Charge" key="2">
                <PrivateServiceChargeFormContainer/>
            </TabPane>
            <TabPane tab="Commercial Service Charge" key="3">
                <CommercialServiceChargeFormContainer/>
            </TabPane>
            <TabPane tab="Other Service Charge" key="4">
                <OtherServiceChargeFormContainer/>
            </TabPane>
            <TabPane tab="Deposit Rule" key="5">
                <DepositRuleFormContainer/>
            </TabPane>
        </Tabs>
    )
}