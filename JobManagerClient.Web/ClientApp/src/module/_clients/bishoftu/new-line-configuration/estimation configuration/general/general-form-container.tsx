import React, {useState} from 'react'
import {Col, Collapse, Form, Icon, Input, Row, Typography} from "antd";
import {TransactionItems} from "../../../../../../_model/level0/iERP-transaction-model/transaction-items";
import {IActiveItem} from "../../../../shashemene/new line/estimation configuration/estimation-configuration-container";
import CButton from "../../../../../../shared/core/cbutton/cbutton";
import {ItemRegistrationFormContainer} from "../../../../../../shared/forms/item-registration-form/item-registration-form-container";

const InputGroup = Input.Group;
const { Panel } = Collapse;
const { Title,Text } = Typography
const customPanelStyle = {
    background: '#f7f7f7',
    margin: 5,
    border: 0
};
export function GeneralFormContainer() {
    const [ transactionItem , setTransactionItem ] = useState<TransactionItems>(new TransactionItems())
    function transactionItemChangeHandler(newTransactionItem:TransactionItems){
        setTransactionItem(newTransactionItem)
    }

    const formItemLayout = {
        labelCol: { span: 21,offset:3 },
        wrapperCol: { span: 20, offset: 4 },
    }
    const AddItem = (activeItem : IActiveItem) => (
        <Icon
            type="plus"
            style={{fontSize:'20px',color:'#1890ff'}}
        />
    );
    return (
        <div style={{backgroundColor:'#eee',padding:'5px',margin:'0px'}}>
            <Row gutter={4}>
                <Col lg={{ span:12 }}>
                    <div className={'paper'} style={{marginBottom:'5px',paddingTop:'5px',paddingBottom:'5px'}}>
                        <Collapse
                            bordered={false}
                            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
                        >
                            <Panel
                                header="Pipeline Items"
                                key="1"
                                style={customPanelStyle}
                                extra={<AddItem title={'Pipeline Items'} itemKey={'pipelineItems'} sector={''} />}
                            >
                            </Panel>
                            <Panel header="HDP Pipeline items"
                                   key="2"
                                   style={customPanelStyle}
                                   extra={<AddItem title={'HDP Pipeline items'} itemKey={'hdpPipelineItems'} sector={''}/>}>
                            </Panel>
                            <Panel
                                header="Water meter items"
                                key="3"
                                style={customPanelStyle}
                                extra={<AddItem title={'Water meter items'} itemKey={'waterMeterItems'} sector={''}/>}
                            >
                            </Panel>
                            <Panel
                                header="Survey items"
                                key="4"
                                style={customPanelStyle}
                                extra={<AddItem title={'Survey items'} itemKey={'surveyItems'} sector={''} />}
                            >
                            </Panel>
                            <Panel
                                header="Newline fixed items"
                                key="5"
                                style={customPanelStyle}
                                extra={<AddItem title={'Survey items'} itemKey={'surveyItems'} sector={''} />}
                            >
                            </Panel>
                            <Panel
                                header="maintenance fixed items"
                                key="4"
                                style={customPanelStyle}
                                extra={<AddItem title={'Survey items'} itemKey={'surveyItems'} sector={''} />}
                            >
                            </Panel>
                        </Collapse>

                        <Form layout={'vertical'} style={{padding:'10px'}}>
                            <Form.Item label="Material handling cost multiplier" >
                                <InputGroup compact>
                                    <Input
                                        style={{ width: '85%' }}
                                        placeholder={'Item Code'}
                                    />
                                    <span style={{ width: '15%' }}><CButton type="primary" onClick={()=>{}}>Add Item</CButton></span>
                                </InputGroup>
                            </Form.Item>

                            <Form.Item label="Private service charge item" >
                                <InputGroup compact>
                                    <Input
                                        style={{ width: '85%' }}
                                        placeholder={'Item Code'}
                                    />
                                    <span style={{ width: '15%' }}><CButton type="primary" onClick={()=>{}}>Add Item</CButton></span>
                                </InputGroup>
                            </Form.Item>

                            <Form.Item label="Commercial service charge item">
                                <InputGroup compact>
                                    <Input
                                        style={{ width: '85%' }}
                                        placeholder={'Item Code'}
                                    />
                                    <span style={{ width: '15%' }}><CButton type="primary" onClick={()=>{}}>Add Item</CButton></span>
                                </InputGroup>
                            </Form.Item>

                            <Form.Item label="Other service charge item" >
                                <InputGroup compact>
                                    <Input
                                        style={{ width: '85%' }}
                                        placeholder={'Item Code'}
                                    />
                                    <span style={{ width: '15%' }}><CButton type="primary" onClick={()=>{}}>Add Item</CButton></span>
                                </InputGroup>
                            </Form.Item>

                            <Form.Item label="Last multiplier">
                                <InputGroup compact>
                                    <Input
                                        style={{ width: '85%' }}
                                        placeholder={'Item Code'}
                                    />
                                    <span style={{ width: '15%' }}><CButton type="primary" onClick={()=>{}}>Add Item</CButton></span>
                                </InputGroup>
                            </Form.Item>
                        </Form>

                    </div>
                </Col>
                <Col lg={{ span:12 }}>
                    <div className={'paper'} style={{padding:'5px'}}>
                        <ItemRegistrationFormContainer itemChangeHandler={transactionItemChangeHandler}/>
                    </div>
                </Col>
            </Row>
        </div>
    )
}