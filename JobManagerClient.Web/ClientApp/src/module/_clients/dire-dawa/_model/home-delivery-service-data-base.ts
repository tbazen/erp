import {WorkFlowData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {Subscriber} from "../../../../_model/view_model/mn-vm/subscriber-search-result";

export class HomeDeliveryServiceDataBase extends WorkFlowData {
    public customer?: Subscriber;
    public kebele: number = -1;
    public houseNo?:string;
    public address?: string;
    public phoneNo?: string;
    public workAmount: number = 0;
    public contactPerson?: string;
}