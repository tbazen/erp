import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import CustomerSponsoredNetworkExpansion from "./customer-sponsored-network-expansion";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";


@JobRuleClientDecorator({
    jobName:"Customer Sponsored Network Expansion",
    jobType:StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK,
    route:ROUTES.JOB.OTHER_SERVICE.CUSTOMER_SPONSORED_NETWORK_WORK
})
export class CustomerSponsoredNetworkExpansionRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return CustomerSponsoredNetworkExpansion
    }
}