import React from "react";
import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {HydrantWaterSupply} from "./hydrant-water-supply";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import {JobData} from "../../../../_services/job.manager.service";
import {HydrantWaterSupplyWorkflowDataContainer} from "./workflow/hydrant-water-supply-workflow-data-container";

@JobRuleClientDecorator({
    jobName:"Hydrant Water Supply",
    jobType: StandardJobTypes.HYDRANT_SERVICE,
    route:ROUTES.JOB.OTHER_SERVICE.HYDRANT_SERVICE,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.HYDRANT_SERVICE].ABSOLUTE,
            component:HydrantWaterSupply
        }
    ]
})

export class HydrantWaterSupplyRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return HydrantWaterSupply
    }
    public getWorkFlowData(job: JobData): JSX.Element {
        return <HydrantWaterSupplyWorkflowDataContainer job={job}/>
    }
}