import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useParams} from "react-router-dom";
import {Subscriber, SubscriberSearchResult} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {AddJobWebPar, JobData, UpdateJobWebPar} from "../../../../_services/job.manager.service";
import {initialJobData} from "../../../../_helpers/initial value/init-jobdata";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import {useEditJobInitializer} from "../../../../shared/hooks/edit-job-intializer";
import {HydrantWaterSupplyData} from "../_model/hydrant-water-supply-data";
import {
    requestAddJobWeb,
    requestUpdateJobWeb,
    subscriberListClear
} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {getLocalDateString} from "../../../../_helpers/date-util";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {WorkFlowData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {Col, Divider, Row} from "antd";
import CustomerSearchBar from "../../../../shared/components/customer-list-table/customer-search-bar";
import {HomeDeliveryServiceForm} from "./home-delivery-service-form";
import CustomersListTable from "../../../../shared/components/customer-list-table/customer-list-table";
import {CAlert} from "../../../../shared/core/calert/calert";
import CustomerInformationContainer from "../../../../shared/components/customer-information/customer-information";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {IBreadcrumb} from "../../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../../_constants/routes";
import {LiquidWasteDisposalData} from "../_model/liquid-waste-disposal-data";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.INDEX,
        breadcrumbName:'Other Services'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.LIQUID_WASTE_DISPOSAL,
        breadcrumbName:'Liquid Waste Disposal'
    }
]


export function LiquidWasteDisposal(){
    const dispatch = useDispatch()
    const { jobId } = useParams()
    const [selectedCustomer,setSelectedCustomer] = useState<SubscriberSearchResult | undefined>(undefined)
    const [ auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth,appState.workflow_data_state])
    const [ jobData, setJobData] = useState<JobData>({...initialJobData.job,id:(jobId && !isNaN(+jobId) && +jobId)|| -1 , applicationType: StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK})
    const [ isNewJob, setIsNewJob ] = useState<boolean>( jobData.id===-1)
    const [ newCustomer , setNewCustomer] = useState<Subscriber>()
    useBreadcrumb(backwardPath)

    const [ registeredJob,setRegisteredJob ] = useEditJobInitializer({jobId:(jobId && +jobId)||-1,jobType:StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK})
    const [ liquidWasteDisposalData, setLiquidWasteDisposalData ] = useState<LiquidWasteDisposalData>(new HydrantWaterSupplyData())

    useEffect(()=>{return ()=>{ dispatch(subscriberListClear()) }},[])
    useEffect(()=>{ setIsNewJob(jobData.id === -1 ) },[jobData])
    useEffect(()=>{ registeredJob && setJobData(registeredJob) },[ registeredJob ])
    useEffect(()=>{
        selectedCustomer && setJobData({ ...jobData,description:'',startDate: getLocalDateString(), statusDate: getLocalDateString(), logDate: getLocalDateString() })
    },[ selectedCustomer ])
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setLiquidWasteDisposalData(workflowdata_state.payload as LiquidWasteDisposalData)
    }, [workflowdata_state])

    const handleNewCustomerDataChange = (customer: Subscriber) => { setNewCustomer(customer)}
    const submitHydrantWaterSupplyDataWithNewCustomer = () => {
        if(newCustomer){
            jobData.newCustomer = newCustomer;
            jobData.customerID = -1;
            const params: AddJobWebPar<LiquidWasteDisposalData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: liquidWasteDisposalData,
                dataType: NAME_SPACES.WORKFLOW.DIRE_DAWA.LIQUID_WASTE_DISPOSAL_DATA
            }
            dispatch(requestAddJobWeb(params))
        }
    }
    const submitHydrantWaterSupplyData = () => {
        if(isNewJob){
            if(selectedCustomer){
                jobData.customerID = selectedCustomer.subscriber.id;
                jobData.newCustomer = null;
                const params: AddJobWebPar<HydrantWaterSupplyData>={
                    job:jobData,
                    sessionId: auth.sessionId,
                    data: liquidWasteDisposalData,
                    dataType: NAME_SPACES.WORKFLOW.DIRE_DAWA.LIQUID_WASTE_DISPOSAL_DATA
                }
                dispatch(requestAddJobWeb(params))
            }
        } else {
            const updateJobParams: UpdateJobWebPar<WorkFlowData> = {
                job: jobData,
                sessionId: auth.sessionId,
                data: liquidWasteDisposalData,
                dataType:NAME_SPACES.WORKFLOW.DIRE_DAWA.LIQUID_WASTE_DISPOSAL_DATA
            }
            dispatch(requestUpdateJobWeb<WorkFlowData>(updateJobParams))
        }
    }
    return (
        <Row gutter={4}>
            {
                isNewJob &&
                <Col span={16}>
                    <CustomerSearchBar
                        appendedForm={
                            <HomeDeliveryServiceForm
                                formItemLayout={'horizontal'}
                                handleHomeDeliveryServiceDataChange={(data)=>setLiquidWasteDisposalData({...liquidWasteDisposalData,...data})}/>
                        }
                        title={'Customer Sponsored Network Expansion'}
                        updateCustomerData={handleNewCustomerDataChange}
                        submitCustomerData={submitHydrantWaterSupplyDataWithNewCustomer}
                    />
                    <CustomersListTable
                        setSelectedCustomers= {setSelectedCustomer}
                        withSelection
                        height='66vh'
                    />
                </Col>
            }
            <Col span={8}>
                <Row style={{marginBottom:'5px'}}>
                    <div className={'paper'} style={{padding:'10px',backgroundColor:'#fff'}}>
                        <HomeDeliveryServiceForm handleHomeDeliveryServiceDataChange={(data)=>setLiquidWasteDisposalData({...liquidWasteDisposalData,...data})}/>
                    </div>
                </Row>
                <Row>
                    {
                        isNewJob &&
                        <div className={'paper'}>
                            <div style={{width: '100%', overflow: 'auto', maxHeight: '52vh', padding: '10px'}}>
                                <p>Selected Customer</p>
                                <Divider/>
                                {
                                    selectedCustomer === undefined &&
                                    <Row>
                                        <CAlert
                                            message="Customer Not Selected"
                                            description='To create customer sponsored network expansion please select one customer, or use the "Add new Customer" button to create new customer!'
                                            type="error"
                                        />
                                    </Row>
                                }
                                {
                                    selectedCustomer !== undefined &&
                                    <Row>
                                        <CustomerInformationContainer {...selectedCustomer} />
                                    </Row>
                                }
                            </div>
                        </div>
                    }
                    <div className={'paper'} style={{marginTop:'5px', padding:'10px'}} >
                        <CButton
                            block
                            type={'primary'}
                            onClick={submitHydrantWaterSupplyData}
                            disabled={ isNewJob && selectedCustomer === undefined}>Submit</CButton>
                    </div>
                </Row>
            </Col>
        </Row>
    )
}