import React, {Fragment, useEffect, useState} from "react";
import {Col, Divider, Form, Input, Row, Select} from "antd";
import {useSelector} from "react-redux";
import {KebeleState} from "../../../../../../_model/state-model/mn-sm/kebele-state";
import {ApplicationState} from "../../../../../../_model/state-model/application-state";
import {DmzState} from "../../../../../../_model/state-model/mn-sm/dmz-state";
import {PressureZoneState} from "../../../../../../_model/state-model/mn-sm/pressure-zone-state";
import {Subscription} from "../../../../../../_model/level0/subscriber-managment-type-library/subscription";
const { Option } = Select

interface ConnectionInformationFormProps{
    subscription?: Subscription | null ,
    handleSubscriptionChange?: (subscription: Subscription)=>void
}
export function ConnectionInformationForm(props: ConnectionInformationFormProps){
    const formItemLayout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 14 },
    }
    const [ kebeleState,dmzState,pressureZoneState ] = useSelector<ApplicationState,[ KebeleState,DmzState,PressureZoneState ]>(appState=>[ appState.kebele_state,appState.dmz_state,appState.pressure_zone_state])
    const [ subscription,setSubscription ] = useState<Subscription>( props.subscription || new Subscription())
    useEffect(()=>{
        props.handleSubscriptionChange &&
        props.handleSubscriptionChange(subscription)
    },[ subscription ])
    return (
        <Fragment>
            <Row gutter={2}>
                <Col span={12}>
                    <div className={'paper'}  style={{padding:'10px'}}>
                        <Divider orientation={'left'}>Connection Information</Divider>
                        <Form.Item label="DMA" {...formItemLayout}>
                            <Select style={{width:'100%'}} onChange={(dma:any)=>{ setSubscription(Object.assign({},subscription,{dma})) }} >
                                { dmzState.dmzs.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                                <Option value={-1}>Unknown</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Pressure Zone" {...formItemLayout}>
                            <Select style={{width:'100%'}} onChange={(pressureZone:any)=>setSubscription(Object.assign({},subscription,{pressureZone}))}>
                                { pressureZoneState.pressure_zones.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                                <Option value={-1}>Unknown</Option>
                            </Select>
                        </Form.Item>
                    </div>
                </Col>

                <Col span={12}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Divider orientation="left">{'Location Information'}</Divider>
                        <Form.Item label="Kebele" {...formItemLayout}>
                            <Select style={{width:'100%'}} onChange={(kebele:any)=>setSubscription(Object.assign({},subscription,{kebele}))}>
                                { kebeleState.kebeles.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                                <Option value={-1}>Unknown</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label={'Water Meter Longitude'} {...formItemLayout}>
                            <Input onChange={( event )=>{ setSubscription(Object.assign({},subscription,{waterMeterY:+event.target.value})) }}/>
                        </Form.Item>
                        <Form.Item label="Lonttitude" {...formItemLayout}>
                            <Input onChange={( event )=>{ setSubscription(Object.assign({},subscription,{waterMeterX:event.target.value})) }} />
                        </Form.Item>
                        <Form.Item label="House No." {...formItemLayout}>
                            <Input onChange={( event )=>{ setSubscription(Object.assign({},subscription,{address:event.target.value}))}}/>
                        </Form.Item>
                        <Form.Item label="Land Certificate No." {...formItemLayout}>
                            <Input onChange={( event )=>{ setSubscription(Object.assign({},subscription,{landCertificateNo:event.target.value})) }} />
                        </Form.Item>
                        <Form.Item label="Parcel No." {...formItemLayout}>
                            <Input onChange={( event )=>{ setSubscription(Object.assign({},subscription,{parcelNo:event.target.value})) }} />
                        </Form.Item>
                    </div>
                </Col>
            </Row>
        </Fragment>
    )
}


