import React, {FC, Fragment} from "react";
import {Divider} from "antd";
import {MeterInformationContainer} from "../../../../../shared/components/job-rule-base-components/mater-information-container";
import {ConntectionMaintenanceDukemData} from "../../_model/conntection-maintenance-dukem-data";
import {ConnectionInformationContainer} from "../../../../../shared/components/job-rule-base-components/connection-information-container";
import {useConnectionMaintenanceWFDataInitializer} from "../../../../../shared/hooks/workflow/connection-maintenance-wf-data-initialializer";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";
import {JobData} from "../../../../../_services/job.manager.service";

interface WorkflowDataContainerProps {
    job: JobData
}

export const WorkflowDataContainer: FC<WorkflowDataContainerProps> = (props) => {
    const [connectionMaintenanceData, subscription, wf_data_state] = useConnectionMaintenanceWFDataInitializer(props.job,ConntectionMaintenanceDukemData)


    return (
        <StateMachineHOC state={wf_data_state}>
            <Divider orientation={'left'}>Connection Detail</Divider>
            { subscription && <ConnectionInformationContainer connection={subscription}/> }
            {
                connectionMaintenanceData.changeMeter &&
                connectionMaintenanceData.updatedMeterData &&
                <Fragment>
                    <Divider orientation={'left'}>New Meter Detail</Divider>
                    <MeterInformationContainer meter={connectionMaintenanceData.updatedMeterData}/>
                </Fragment>
            }
            {
                connectionMaintenanceData.relocateMeter &&
                <Fragment>
                    <Divider orientation={'left'}>Relocated Connection Detail</Divider>
                    {
                        connectionMaintenanceData.updatedSubscription
                            ?
                            <ConnectionInformationContainer connection={connectionMaintenanceData.updatedSubscription}/>
                            :
                            <h6>New Connection Information not saved</h6>
                    }
                </Fragment>
            }
        </StateMachineHOC>
    )
}
