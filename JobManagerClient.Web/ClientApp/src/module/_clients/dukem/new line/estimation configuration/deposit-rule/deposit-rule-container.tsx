import React, {useState} from "react";
import { Table } from 'antd';

const columns = [
    {
        title: 'Meter Type',
        dataIndex: 'meter-type',
        width: '40%',
    },
    {
        title: 'Deposit Amount(Private)',
        dataIndex: 'private-deposit-amount',
        width: '30%',
    },
    {
        title: 'Deposit Amount(Commercial)',
        dataIndex: 'commercial-deposit-amount',

    },
];

export function DepositRuleContainer(){
    const [ data , setData ] = useState([])
    return (
        <Table columns={columns} dataSource={data} pagination={{ pageSize: 50 }} scroll={{ y: 240 }} />
    )
}