import React from 'react';
import { Tabs } from 'antd';
import {GeneralConfigurationContainer} from "./general/general-configuration-container";
import {DepositRuleContainer} from "./deposit-rule/deposit-rule-container";
import {OtherRateContainer} from "./other-rate/other-rate-container";

const { TabPane } = Tabs;

export function EstimationConfigurationContainer(){
    return (
        <Tabs className={'paper'} style={{padding:'10px',paddingBottom:'10px'}} defaultActiveKey="1">
            <TabPane tab="General" key="1">
                <GeneralConfigurationContainer/>
            </TabPane>
            <TabPane tab="Deposit rule" key="2">
                <DepositRuleContainer/>
            </TabPane>
            <TabPane tab="Other Rates" key="3">
                <OtherRateContainer/>
            </TabPane>
        </Tabs>
    )
}