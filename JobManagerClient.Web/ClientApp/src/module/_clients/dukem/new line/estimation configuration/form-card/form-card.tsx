import React from "react";
import {Divider} from 'antd';
import { Form, Input, Button } from 'antd';
const InputGroup = Input.Group;

interface IProps{
    title : string
}

export function FormCard( props : IProps ){
    const formItemLayout = {
                labelCol: { span: 4 },
                wrapperCol: { span: 18 },
            }
    const formLayout = 'horizontal';
    return (
        <div className={'paper'} style={{width:'100%',padding:'5px'}}>
            <Divider orientation="left">{props.title}</Divider>
            <Form layout={formLayout}>
                <Form.Item label="Private" {...formItemLayout}>
                    <InputGroup compact>
                        <Input style={{ width: '80%' }} />
                        <Button style={{ width: '20%' }} type="primary">category</Button>
                    </InputGroup>
                </Form.Item>
                <Form.Item label="Institutional" {...formItemLayout}>
                    <InputGroup compact>
                        <Input style={{ width: '80%' }} />
                        <Button style={{ width: '20%' }} type="primary">category</Button>
                    </InputGroup>
                </Form.Item>
                <Form.Item label="Other" {...formItemLayout}>
                    <InputGroup compact>
                        <Input style={{ width: '80%' }} />
                        <Button style={{ width: '20%' }} type="primary">category</Button>
                    </InputGroup>
                </Form.Item>
            </Form>
        </div>
    )
}