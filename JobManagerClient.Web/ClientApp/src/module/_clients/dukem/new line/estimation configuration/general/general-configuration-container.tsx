import React from "react";
import {Col, Row} from "antd";
import {FormCard} from "../form-card/form-card";

const formTitles = [
    "Form Fee",
    "Estimation Fee",
    "Transit Charge",
    "Technical Service Charge(pipeline: 0-5 pcs)",
    "Technical Service Charge(pipeline: 6-10 pcs)",
    "Technical Service Charge(pipeline: >10 pcs)",
    "Technical Service Charge(Fittings)",
    "Contract Card Fee",
    "File Folder Fee",
    "Permission Fee",
    "Service Charge"
]

export function GeneralConfigurationContainer(){
    return (
        <Row gutter={4}>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    {
                      formTitles.map( (value,key)=> <div style={{marginBottom:'5px'}}>
                                                    <FormCard title={value}/>
                                                 </div>
                      )
                    }
                </div>
            </Col>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    col-6
                </div>

                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'0vh', padding:"10px"}}>
                    col-6
                </div>


            </Col>
        </Row>
    )
}