import React from "react";
import {FormCard} from "../form-card/form-card";
import {Col, Row} from "antd";
export function OtherRateContainer(){
    return (
        <Row gutter={4}>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    <FormCard title={'Ownership transfer fee'}/>
                </div>
            </Col>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    col-6
                </div>
            </Col>
        </Row>
    )
}