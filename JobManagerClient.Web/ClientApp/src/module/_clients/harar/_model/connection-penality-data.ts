import {WorkFlowData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {Subscriber} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";


export enum PenalityType {
    MeterByPass=0,
    MeterRelcoation=1,
    MeterReversing=2,
    OverPipeConstruction=3,
    MeterDamage=4,
    IllegalConnection=5,
    IllegalWateruse=6,
    Others=7
}


export class PenalityInstance {
    public customerID: number = 0;
    public connectionID: number = 0;
    public date: number = 0;
    public type:PenalityType = PenalityType.MeterByPass;
    public description?: string;
}


export class ConnectionPenalityData extends WorkFlowData {
    public customer?: Subscriber;
    public connection?: Subscription;
    public pastPenalities: PenalityInstance[] = [];
    public penalityType: PenalityType = PenalityType.MeterByPass;
    public instanceCount: number = 0;
    public otherAmount: number = 0;
    public description: string = "";
}