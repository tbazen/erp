import {WorkFlowData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {MeterData} from "../../../../_model/view_model/mn-vm/meter-data";

export class ConntectionMaintenanceHararData extends WorkFlowData {
    public changeMeter?: boolean;
    public chargeForMeter?: boolean;
    public estimation?: boolean;
    public relocateMeter?: boolean;
    public meterDiagnosis?: boolean;
    public supportLetter?: boolean;

    public connectionID: number = 0;
    public meterData?: MeterData;
    public hasApplicationFee: boolean = this.estimation || this.relocateMeter || this.meterDiagnosis || this.supportLetter || false;
}