import React, {useEffect, useState} from "react";
import {JobData} from "../../../../../../_services/job.manager.service";
import SharedJobFormFields from "../../../../../../shared/components/job application form/shared-job-field";
import CustomerInformationContainer
    from "../../../../../../shared/components/customer-information/customer-information";
import {SubscriberSearchResult} from "../../../../../../_model/view_model/mn-vm/subscriber-search-result";
import {Col, Row} from "antd";

interface CustomerConnectionInformationFormProps{
    jobData: JobData,
    subscriber?:SubscriberSearchResult,
    handleJobDataChange: (jobData: JobData)=>void
}
export function CustomerConnectionInformationForm(props: CustomerConnectionInformationFormProps){
    const [ jobData,setJobData ] = useState<JobData>(props.jobData)
    useEffect(()=>{
        setJobData(props.jobData)
    },[ props.jobData ])
    useEffect(()=>{
        props.handleJobDataChange &&
        props.handleJobDataChange(jobData)
    },[ jobData ])
    return (
        <Row gutter={2}>
          <Col lg={{span:12}}>
              <div className={'paper'} style={{padding:'10px'}}>
                  <SharedJobFormFields
                      dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                      descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                  />
                  { props.subscriber && <CustomerInformationContainer {...props.subscriber}/> }
              </div>
          </Col>
        </Row>
    )
}