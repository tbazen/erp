import React, { useEffect, useState} from 'react';
import {Checkbox, Col, Divider, Row, Form} from "antd";


interface ServiceDetailFormPrpos{
    handleServiceDetailChange? : (serviceDetail: ServiceDetail)=>void
    serviceDetail?: ServiceDetail
}
export interface ServiceDetail{
    relocateMeter: boolean,
    estimation: boolean,
    chargeForMeter: boolean,
    meterDiagnosis: boolean,
    supportLetter: boolean
}
const colWidth = 24

export function ServiceDetailForm(props: ServiceDetailFormPrpos){
    const [ serviceDetail,setServiceDetail ] = useState<ServiceDetail>({
        relocateMeter: (props.serviceDetail && props.serviceDetail.relocateMeter) || false,
        estimation: (props.serviceDetail && props.serviceDetail.estimation) || false,
        chargeForMeter: (props.serviceDetail && props.serviceDetail.chargeForMeter) || false,
        meterDiagnosis: (props.serviceDetail && props.serviceDetail.meterDiagnosis) || false,
        supportLetter: (props.serviceDetail && props.serviceDetail.supportLetter) || false
    })
    useEffect(()=>{ props.handleServiceDetailChange && props.handleServiceDetailChange(serviceDetail) },[ serviceDetail ])
    return (
        <Row gutter={2}>
            <Col lg={{span:12}}>
               <div className={'paper'} style={{padding:'10px'}}>
                   <Divider orientation={'left'}>Service Detail</Divider>
                   <Form.Item>
                       <Checkbox
                           checked={serviceDetail.relocateMeter}
                           onChange={(e)=>setServiceDetail({...serviceDetail,relocateMeter:e.target.checked})}
                       >Relocate Meter</Checkbox>
                   </Form.Item>
                   <Form.Item>
                       <Checkbox
                           checked={serviceDetail.estimation}
                           onChange={(e)=>setServiceDetail({...serviceDetail,estimation:e.target.checked})}
                       >Estimation Fee</Checkbox>
                   </Form.Item>
                   <Form.Item>
                       <Checkbox
                           checked={serviceDetail.chargeForMeter}
                           onChange={(e)=>setServiceDetail({...serviceDetail,chargeForMeter:e.target.checked})}
                       >Charge For Meter</Checkbox>
                   </Form.Item>
                   <Form.Item>
                       <Checkbox
                           checked={serviceDetail.meterDiagnosis}
                           onChange={(e)=>setServiceDetail({...serviceDetail,meterDiagnosis:e.target.checked})}
                       >Meter Diagnosis</Checkbox>
                   </Form.Item>
                   <Form.Item>
                       <Checkbox
                           checked={serviceDetail.supportLetter}
                           onChange={(e)=>setServiceDetail({...serviceDetail,supportLetter:e.target.checked})}
                       >Prepare Support Letter</Checkbox>
                   </Form.Item>
               </div>
            </Col>

        </Row>
    )
}
