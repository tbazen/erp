import React, {FC, Fragment} from "react";
import {Divider} from "antd";
import {MeterInformationContainer} from "../../../../../shared/components/job-rule-base-components/mater-information-container";
import {ConnectionInformationContainer} from "../../../../../shared/components/job-rule-base-components/connection-information-container";
import {useConnectionMaintenanceWFDataInitializer} from "../../../../../shared/hooks/workflow/connection-maintenance-wf-data-initialializer";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";
import {JobData} from "../../../../../_services/job.manager.service";
import {ConntectionMaintenanceHararData} from "../../_model/conntection-maintenance-harar-data";

interface WorkflowDataContainerProps {
    job: JobData
}

export const WorkflowDataContainer: FC<WorkflowDataContainerProps> = (props) => {
    const [connectionMaintenanceData, subscription, wf_data_state] = useConnectionMaintenanceWFDataInitializer(props.job,ConntectionMaintenanceHararData)


    return (
        <StateMachineHOC state={wf_data_state}>
            <Divider orientation={'left'}>Connection Detail</Divider>
            { subscription && <ConnectionInformationContainer connection={subscription}/> }
            {
                connectionMaintenanceData.changeMeter &&
                connectionMaintenanceData.meterData &&
                <Fragment>
                    <Divider orientation={'left'}>New Meter Detail</Divider>
                    <MeterInformationContainer meter={connectionMaintenanceData.meterData}/>
                </Fragment>
            }
        </StateMachineHOC>
    )
}
