import React from "react";
import {IBreadcrumb} from "../../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../../_constants/routes";
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.INDEX,
        breadcrumbName:'Other Services'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.HYDRANT_SERVICE,
        breadcrumbName:'Connection Penalty'
    }
]


export function ConnectionPenaltyContainer(){
    useBreadcrumb(backwardPath)
    return <p>Module Pending...</p>
}