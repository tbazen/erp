import React from "react";
import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {ConnectionPenaltyContainer} from "./connection-penality";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {ROUTES} from "../../../../_constants/routes";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {JobData} from "../../../../_services/job.manager.service";
import {ConnectionPenaltyWorkflowDataContainer} from "./workflow/connection-penality-wf-data-container";


@JobRuleClientDecorator({
    jobName:"Connection Penalty",
    jobType:StandardJobTypes.CONNECTION_PENALITY,
    route:ROUTES.JOB.OTHER_SERVICE.CONNECTION_PENALITY
})
export class ConnectionPenaltyRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return ConnectionPenaltyContainer
    }
    public getWorkFlowData(job: JobData): JSX.Element {
        return <ConnectionPenaltyWorkflowDataContainer job={job}/>
    }
}