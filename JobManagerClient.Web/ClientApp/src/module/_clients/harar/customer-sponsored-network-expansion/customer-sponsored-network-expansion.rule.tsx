import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import {CustomerSponsoredNetworkWorkBase} from "../../../main/other services/customer-sponsored-network-work/customer-sponsored-network-work-base";
import {CustomerSponsoredNetworkWorkFormContainerBase} from "../../../main/other services/customer-sponsored-network-work/customer-sponsored-network-work-form-container";


@JobRuleClientDecorator({
    jobName:"Customer Sponsored Network Expansion",
    jobType:StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK,
    route:ROUTES.JOB.OTHER_SERVICE.CUSTOMER_SPONSORED_NETWORK_WORK,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK].ABSOLUTE,
            component: CustomerSponsoredNetworkWorkFormContainerBase
        }
    ]
})
export class CustomerSponsoredNetworkExpansionRule extends CustomerSponsoredNetworkWorkBase { }