import React, {FC, useEffect, useState} from "react";
import {GetWorkFlowDataWebPar, JobData} from "../../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../../_model/state-model/aj-sm/workflow-data-state";
import {StandardJobTypes} from "../../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {HydrantWaterSupplyData} from "../../_model/hydrant-water-supply-data";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";
import {DataRow} from "../../../../../shared/components/data-row/data-row";
interface HydrantWaterSupplyWorkflowDataContainerProps{
    job: JobData
}

export const HydrantWaterSupplyWorkflowDataContainer:FC<HydrantWaterSupplyWorkflowDataContainerProps> = (props)=>{
    const dispatch = useDispatch()
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ hydrantWaterSupplyData, setHydrantWaterSupplyData ]  = useState<HydrantWaterSupplyData>()
    
    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:props.job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.HYDRANT_SERVICE,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])


    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setHydrantWaterSupplyData(workflowdata_state.payload as HydrantWaterSupplyData)
    },[ workflowdata_state ])

    return (
        <StateMachineHOC state={workflowdata_state}>
            <DataRow title={'Volume (Cubic Meters)'} content={hydrantWaterSupplyData && hydrantWaterSupplyData.workAmount}/>
        </StateMachineHOC>
    )
}