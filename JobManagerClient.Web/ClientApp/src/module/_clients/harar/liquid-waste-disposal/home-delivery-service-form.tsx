import React, {FC, useEffect, useState} from 'react';
import {Form, Input, Select} from "antd";
import SharedJobFormFields from "../../../../shared/components/job application form/shared-job-field";
import {DeliveryType, HomeDeliveryServiceDataBase} from "../_model/home-delivery-service-data-base";
import {HydrantWaterSupplyData} from "../_model/hydrant-water-supply-data";
import {LiquidWasteDisposalData} from "../_model/liquid-waste-disposal-data";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";

const { Option } = Select

const horizontalFormItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
}
const verticalFormItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}


interface HomeDeliveryServiceFormProps {
    handleJobDateChange?:(date:string) => void
    handleJobDescriptionChange?:(description:string) => void
    handleHomeDeliveryServiceDataChange:(data: HydrantWaterSupplyData | LiquidWasteDisposalData) => void
    formItemLayout?: 'horizontal' | 'vertical'
    jobType: StandardJobTypes.LIQUID_WASTE_DISPOSAL | StandardJobTypes.HYDRANT_SERVICE
}


export const HomeDeliveryServiceForm:FC<HomeDeliveryServiceFormProps>=(props)=>{
    const formItemLayout = props.formItemLayout && props.formItemLayout === 'horizontal' ? horizontalFormItemLayout : verticalFormItemLayout
    const [ homeDeliveryServiceData,setHomeDeliveryServiceData ] = useState<HomeDeliveryServiceDataBase>(new HomeDeliveryServiceDataBase())

    useEffect(()=>{ props.handleHomeDeliveryServiceDataChange(homeDeliveryServiceData)},[ homeDeliveryServiceData ])

    return (
        <div>
            <SharedJobFormFields
                formItemLayout={props.formItemLayout}
                dateChangeHandler={props.handleJobDateChange}
                descriptionChangeHandler={props.handleJobDescriptionChange}
            />
            <Form.Item {...formItemLayout} label={'Delivery Type'}>
                <Select
                    style={{width:'100%'}}
                    value={homeDeliveryServiceData.deliveryType}
                    onChange={(selectedDeliveryType:DeliveryType)=>setHomeDeliveryServiceData({...homeDeliveryServiceData,deliveryType:selectedDeliveryType})}>
                    <Option value={DeliveryType.Regular}>Regular</Option>
                    <Option value={DeliveryType.OutOfTown}>Out Of Town</Option>
                    {props.jobType === StandardJobTypes.HYDRANT_SERVICE && <Option value={DeliveryType.CommunitySupply}>Community Supply</Option> }
                    {props.jobType === StandardJobTypes.HYDRANT_SERVICE && <Option value={DeliveryType.SelfDelivery}>Self Delivery</Option> }
                </Select>
            </Form.Item>
            <Form.Item label="Work Amount" {...formItemLayout}>
                <Input
                    value={homeDeliveryServiceData.workAmount}
                    onChange={(e)=>setHomeDeliveryServiceData({...homeDeliveryServiceData,workAmount:+e.target.value})} />
            </Form.Item>
            <Form.Item label="Address" {...formItemLayout}>
                <Input
                    value={homeDeliveryServiceData.address}
                    onChange={(e)=>setHomeDeliveryServiceData({...homeDeliveryServiceData,address:e.target.value})} />
            </Form.Item>
            <Form.Item label="Phone Number" {...formItemLayout}>
                <Input
                    value={homeDeliveryServiceData.phoneNo}
                    onChange={(e)=>setHomeDeliveryServiceData({...homeDeliveryServiceData,phoneNo:e.target.value})} />
            </Form.Item>
            <Form.Item label="Contact Person" {...formItemLayout}>
                <Input
                    value={homeDeliveryServiceData.contactPerson}
                    onChange={(e)=>setHomeDeliveryServiceData({...homeDeliveryServiceData,contactPerson:e.target.value})} />
            </Form.Item>
        </div>
    )
}