import React from 'react'
import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {LiquidWasteDisposal} from "./liquid-waste-disposal";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import {LiquidWasteDisposalWorkflowDataContainer} from "./workflow/liquid-waste-disposal-workflow-data-container";
import {JobData} from "../../../../_services/job.manager.service";


@JobRuleClientDecorator({
    jobName:"Liquid Waste Disposal",
    jobType:StandardJobTypes.LIQUID_WASTE_DISPOSAL,
    route:ROUTES.JOB.OTHER_SERVICE.LIQUID_WASTE_DISPOSAL,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.LIQUID_WASTE_DISPOSAL].ABSOLUTE,
            component:LiquidWasteDisposal
        }
    ]
})
export class LiquidWasteDisposalRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return LiquidWasteDisposal
    }
    public getWorkFlowData(job: JobData): JSX.Element {
        return <LiquidWasteDisposalWorkflowDataContainer job={job}/>
    }
}