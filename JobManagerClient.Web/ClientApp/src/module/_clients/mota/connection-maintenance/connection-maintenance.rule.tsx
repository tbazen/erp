import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {ConnectionMaintenanceContainer} from "./connection-maintenance";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";

@JobRuleClientDecorator({
    jobName:"Existing Connection Service",
    jobType:StandardJobTypes.CONNECTION_MAINTENANCE,
    route:ROUTES.JOB.CONNECTION_MAINTENANCE
})
export class ConnectionMaintenanceRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
     return ConnectionMaintenanceContainer
    }
}