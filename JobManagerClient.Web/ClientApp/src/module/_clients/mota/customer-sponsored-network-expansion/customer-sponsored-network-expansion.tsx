import React, { useEffect, useState } from 'react'
import { ROUTES } from '../../../../_constants/routes';
import { IBreadcrumb } from '../../../../_model/state-model/breadcrumd-state';
import { useDispatch, useSelector } from 'react-redux';
import { CAlert } from "../../../../shared/core/calert/calert"
import pushPathToBC from '../../../../_redux_setup/actions/breadcrumb-actions';
import { Row, Col, Divider } from 'antd';
import CustomerSearchBar from '../../../../shared/components/customer-list-table/customer-search-bar';
import CustomersListTable from '../../../../shared/components/customer-list-table/customer-list-table';
import SharedJobFormFields from '../../../../shared/components/job application form/shared-job-field';
import { SubscriberSearchResult, Subscriber } from '../../../../_model/view_model/mn-vm/subscriber-search-result';
import CButton from '../../../../shared/core/cbutton/cbutton';
import { ApplicationState } from '../../../../_model/state-model/application-state';
import { AuthenticationState } from '../../../../_model/state-model/auth-state';
import { SingleSubscriberState } from '../../../../_model/state-model/mn-sm/single-subscriber-state';
import { TransactionItemsState } from '../../../../_model/state-model/ic-sm/transaction-items-state';
import { AddJobPar } from '../../../../_services/job.manager.service';
import { initialJobData } from '../../../../_helpers/initial value/init-jobdata';
import { initialCustomerData } from '../../../../_helpers/initial value/init-customer-data';
import { requestAddJob } from '../../../../_redux_setup/actions/mn-actions/main-actions';
import CustomerInformationContainer from '../../../../shared/components/customer-information/customer-information';


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.INDEX,
        breadcrumbName:'Other Services'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.CUSTOMER_SPONSORED_NETWORK_WORK,
        breadcrumbName:'Customer Sponsored Network Expansion'
    }
]
const CustomerSponsoredNetworkExpansion = ()=>{
    const dispatch = useDispatch()
    const [selectedCustomer,setSelectedCustomer] = useState<SubscriberSearchResult | undefined>(undefined)
    
    const [auth, existingCustomerInfo, connectionInformation] = useSelector<ApplicationState, [AuthenticationState, SingleSubscriberState, TransactionItemsState]>(appState => [appState.auth, appState.subscriber_state, appState.transaction_items_state])
    //FIXME: [ BAD PRACTISE : APPLICATION TYPE IS HARD CODED ]
    const initJobData: AddJobPar = { ...initialJobData, sessionId: auth.sessionId, job: { ...initialJobData.job, applicationType: 17 } }
    const [existingCustomerJobData, setExistingCustomerJobData] = useState<AddJobPar>(initJobData)
    const [newCustomerJobData, setNewCustomerJobData] = useState<AddJobPar>({ ...initJobData, job: { ...initJobData.job, newCustomer: initialCustomerData } })

    
    useEffect(()=>{
        dispatch(pushPathToBC(backwardPath))
        return () => {
            dispatch(pushPathToBC([]))    
        };
    },[])


    //Effect to update selected customer
    useEffect(() => {
        selectedCustomer !== undefined &&
            setExistingCustomerJobData({ ...existingCustomerJobData, job: { ...existingCustomerJobData.job, customerID: selectedCustomer.subscriber.id } })
    }, [selectedCustomer])

    const handleNewCustomerDataChange = (customer: Subscriber) => { setNewCustomerJobData({ ...newCustomerJobData, job: { ...newCustomerJobData.job, newCustomer: customer } }) }
    const submitNewCustomerJobData = () => {
        dispatch(requestAddJob(newCustomerJobData))
    }
    const submitExistingCustomerJobData = () => {
        existingCustomerJobData !== undefined &&
            dispatch(requestAddJob(existingCustomerJobData))
    }
    
    return (
        <Row gutter={4}>
            <Col span={16}>
            <CustomerSearchBar
                    appendedForm={<SharedJobFormFields
                        dateChangeHandler={(date: string) => setNewCustomerJobData({ ...newCustomerJobData, job: { ...newCustomerJobData.job, startDate: date, statusDate: date, logDate: date } })}
                        descriptionChangeHandler={(desc: string) => setNewCustomerJobData({ ...newCustomerJobData, job: { ...newCustomerJobData.job, description: desc } })}
                    />
                    }
                    title={'Customer Sponsored Network Expansion'}
                    updateCustomerData={handleNewCustomerDataChange}
                    submitCustomerData={submitNewCustomerJobData}
                />
            <CustomersListTable
                                setSelectedCustomers= {setSelectedCustomer}
                                withSelection
                                height='66vh'
            />
            </Col>
            <Col span={8}>
                <Row style={{marginBottom:'5px'}}>
                    <div className={'paper'} style={{padding:'10px',backgroundColor:'#fff'}}>
                        <SharedJobFormFields
                                dateChangeHandler={(date: string) => setExistingCustomerJobData({ ...existingCustomerJobData, job: { ...existingCustomerJobData.job, startDate: date, statusDate: date, logDate: date } })}
                                descriptionChangeHandler={(desc: string) => setExistingCustomerJobData({ ...existingCustomerJobData, job: { ...existingCustomerJobData.job, description: desc } })}
                            />
                    </div>
                </Row>
                <Row>
                    <div className={'paper'}>
                        <div style={{width:'100%',overflow:'auto',maxHeight:'52vh',padding:'10px'}}>
                            <p>Selected Customer</p>
                            <Divider/>
                            {
                                selectedCustomer === undefined &&
                                <Row>
                                    <CAlert
                                        message="Customer Not Selected"
                                        description='To create customer sponsored network expansion please select one customer, or use the "Add new Customer" button to create new customer!'
                                        type="error"
                                    />
                                </Row>
                            }
                            {
                                selectedCustomer !== undefined &&
                                <Row>
                                    <CustomerInformationContainer {...selectedCustomer} />
                                </Row>
                            }
                        </div>
                        <div style={{padding:'10px'}} >
                            <CButton block onClick={submitExistingCustomerJobData} disabled={selectedCustomer === undefined} >Submit</CButton>
                        </div>
                    </div>
                </Row>
            </Col>
        </Row>
    )
}


export default CustomerSponsoredNetworkExpansion
    