import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {ConnectionMaintenanceContainer} from "./connection-maintenance";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import {ConnectionMaintenanceFormContainer} from "./form/connection-maintenance-form-container";

@JobRuleClientDecorator({
    jobName:"Existing Connection Service",
    jobType:StandardJobTypes.CONNECTION_MAINTENANCE,
    route:ROUTES.JOB.CONNECTION_MAINTENANCE,
    priority:1,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.CONNECTION_MAINTENANCE].ABSOLUTE,
            component:ConnectionMaintenanceFormContainer
        }
    ]
})
export class ConnectionMaintenanceRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
     return ConnectionMaintenanceContainer
    }
}