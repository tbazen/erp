import React, {useEffect, useState} from "react";
import {Tabs} from 'antd';
import {SubscriberSearchResult} from "../../../../../_model/view_model/mn-vm/subscriber-search-result";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../_model/state-model/auth-state";
import {
    AddJobWebPar,
    GetJobPar,
    GetWorkFlowDataWebPar,
    JobData,
    UpdateJobWebPar
} from "../../../../../_services/job.manager.service";
import {initialJobData} from "../../../../../_helpers/initial value/init-jobdata";
import {StandardJobTypes} from "../../../../../_model/view_model/mn-vm/standard-job-types";
import {NAME_SPACES} from "../../../../../_constants/model-namespaces";
import {requestAddJobWeb, requestUpdateJobWeb} from "../../../../../_redux_setup/actions/mn-actions/main-actions";
import CButton from "../../../../../shared/core/cbutton/cbutton";
import {useParams} from "react-router-dom";
import {ServiceDetail, ServiceDetailForm} from "./service-detail/service-detail-form";
import {CustomerConnectionInformationForm} from "./customer-connection-information/customer-connection-information-form";
import {ConnectionInformationForm} from "./connection-information/connection-information-form";
import {MeterPageForm} from "./meter-page/meter-page-form";
import {ActiveJobDetailInformationState} from "../../../../../_model/state-model/aj-sm/active-job-detail-information-state";
import {
    fetchActiveJobDetailInformation,
    fetchWorkflowData
} from "../../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {WorkflowDataState} from "../../../../../_model/state-model/aj-sm/workflow-data-state";
import {useEditJobInitializer} from "../../../../../shared/hooks/edit-job-intializer";
import {MeterData} from "../../../../../_model/view_model/mn-vm/meter-data";
import {Subscription} from "../../../../../_model/level0/subscriber-managment-type-library/subscription";
import {NekemteConnectionMaintenanceData} from "../../_model/nekemte-connection-maintenance-data";

const { TabPane } = Tabs;


interface IProps{
    data? : SubscriberSearchResult
}



export function ConnectionMaintenanceFormContainer(props: IProps) {
    const {jobId} = useParams()
    const dispatch = useDispatch()
    const [ auth,jobDetail,workflowdata_state ] = useSelector<ApplicationState, [AuthenticationState,ActiveJobDetailInformationState,WorkflowDataState]>(appState => [
        appState.auth,
        appState.active_job_detail_information_state,
        appState.workflow_data_state
    ])
    const [ connectionMaintenanceWorkflowData,setConnectionMaintenanceWorkflowData ] = useState<NekemteConnectionMaintenanceData>(new NekemteConnectionMaintenanceData())
    const [ jobData, setJobData] = useState<JobData>({...initialJobData.job, customerID: (props.data && props.data.subscriber.id)||0, applicationType: StandardJobTypes.CONNECTION_MAINTENANCE})
    const [ serviceDetail, setServiceDetail ] = useState<ServiceDetail>({ relocateMeter:false,estimation:false })
    useEditJobInitializer({jobId:(jobId && +jobId) || -1,jobType:StandardJobTypes.CONNECTION_MAINTENANCE})
    useEffect(()=>{
        if(jobId && (+jobId!==-1)){
            const jobDetailParams: GetJobPar  = {
                sessionId:auth.sessionId,
                jobID:+jobId
            }
            dispatch(fetchActiveJobDetailInformation(jobDetailParams));
        }
    },[])
    useEffect(()=>{
        if(  jobDetail.payload ){
            const params : GetWorkFlowDataWebPar =
                {
                    key:0,
                    jobID:jobDetail.payload.job.id,
                    sessionId:auth.sessionId,
                    typeID:StandardJobTypes.CONNECTION_MAINTENANCE,
                    fullData:true
                }
            setJobData(jobDetail.payload.job)
            dispatch(fetchWorkflowData(params))
        }
    },[ jobDetail ])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload &&
        setConnectionMaintenanceWorkflowData(workflowdata_state.payload as NekemteConnectionMaintenanceData)
    },[ workflowdata_state ])

    function handleServiceDetailChange(newServiceDetail: ServiceDetail){ setServiceDetail(newServiceDetail) }
    function handleJobDataChange(newJobData: JobData){ setJobData(newJobData) }
    function handleUpdatedMeterDataChange(newMeterData: MeterData) {
        setConnectionMaintenanceWorkflowData({...connectionMaintenanceWorkflowData,updatedMeterData:newMeterData})
    }
    function handleUpdatedSubscriptionChange(newSubscription: Subscription){
        setConnectionMaintenanceWorkflowData({...connectionMaintenanceWorkflowData,updatedSubscription:newSubscription})
    }
    function handleSubmitConnectionMaintenanceData(){
        if(jobData.id === -1 && props.data){
            let connectionData = new NekemteConnectionMaintenanceData()
            connectionData.connectionID = props.data.subscription.id;
            connectionData.relocateMeter = serviceDetail.relocateMeter;
            connectionData.estimation = serviceDetail.estimation;

            const params: AddJobWebPar<NekemteConnectionMaintenanceData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: connectionData,
                dataType: NAME_SPACES.WORKFLOW.NEKEMTE.NEKEMTE_CONNECTION_MAINTENANCE_DATA
            }
            dispatch(requestAddJobWeb(params))
        }
        else if(jobData.id !== -1 ){
            const updateParams: UpdateJobWebPar<NekemteConnectionMaintenanceData>={
                job:jobData,
                sessionId:auth.sessionId,
                data:connectionMaintenanceWorkflowData,
                dataType:NAME_SPACES.WORKFLOW.NEKEMTE.NEKEMTE_CONNECTION_MAINTENANCE_DATA
            }
            dispatch(requestUpdateJobWeb<NekemteConnectionMaintenanceData>(updateParams))
        }
    }
    return (
        <Tabs
            defaultActiveKey="1"
            tabBarExtraContent={
                <CButton
                    type={'primary'}
                    block
                    size={'large'}
                    onClick={handleSubmitConnectionMaintenanceData}>
                    Submit
                </CButton>
            }
        >
            <TabPane tab="Service Detail" key="1"> <ServiceDetailForm serviceDetail={{relocateMeter:connectionMaintenanceWorkflowData.relocateMeter||false,estimation:connectionMaintenanceWorkflowData.estimation || false}} handleServiceDetailChange={handleServiceDetailChange}/> </TabPane>
            <TabPane tab="Customer/ Connection Information" key="2"> <CustomerConnectionInformationForm jobData={jobData} handleJobDataChange={handleJobDataChange} subscriber={props.data}/> </TabPane>
            { jobData.id !== -1 && connectionMaintenanceWorkflowData.upateConneciton &&  <TabPane key={'3'} tab={'Connection information'}> <ConnectionInformationForm handleSubscriptionChange={handleUpdatedSubscriptionChange} subscription={connectionMaintenanceWorkflowData.updatedSubscription}/></TabPane> }
            { jobData.id !== -1 && connectionMaintenanceWorkflowData.changeMeter &&  <TabPane key={'4'} tab={'Meter Page'}><MeterPageForm handleMeterDataChange={handleUpdatedMeterDataChange} meterData={connectionMaintenanceWorkflowData.updatedMeterData}/></TabPane> }
        </Tabs>
    )
}