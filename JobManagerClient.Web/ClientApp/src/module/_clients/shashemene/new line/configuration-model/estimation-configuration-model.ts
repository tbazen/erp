import {ItemsListItem} from "../../../../../_model/level0/job-manager-model/items-list-item";

export class ItemTypedValue {
        
    public privateItemCode?: string;
    
    public institutionalItemCode?: string;
    
    public otherItemCode?: string;
    
    public privateValue: number = 0;
    
    public institutionalValue: number = 0;
    
    public otherValue: number = 0;

}

export class ShashemeneEstimationConfiguration {
    
    public pipelineItems: ItemsListItem[] = [];
    
    public hdpPipelineItems: ItemsListItem[] =[];
    
    public waterMeterItems: ItemsListItem[] = [];
    
    public surveyItems: ItemsListItem[] = [];
    
    public meterDeposit: ItemTypedValue[] = [];
    
    public meterServiceValues: ItemTypedValue[] = [];
    
    public materialProfitMargin: ItemTypedValue = new ItemTypedValue();
    
    public technicalServiceMultiplier: ItemTypedValue = new ItemTypedValue();
    
    public technicalServiceMultiplierTeir1: ItemTypedValue = new ItemTypedValue();
    
    public technicalServiceMultiplierTeir2: ItemTypedValue = new ItemTypedValue();
    
    public technicalServiceMultiplierTeir3: ItemTypedValue = new ItemTypedValue();
    
    public serviceChargeMultiplier: ItemTypedValue = new ItemTypedValue();
    
    public formFee: ItemTypedValue = new ItemTypedValue();
    
    public estimationFee: ItemTypedValue = new ItemTypedValue();
    
    public transitFee: ItemTypedValue = new ItemTypedValue();
    
    public fileFolderFee: ItemTypedValue = new ItemTypedValue();
    
    public contractCardFee: ItemTypedValue = new ItemTypedValue();
    
    public permissionFee: ItemTypedValue = new ItemTypedValue();
    
    public ownershipTransferFee: ItemTypedValue = new ItemTypedValue();
    
    public smallGuage: string = "101023001";
}
