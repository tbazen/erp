import React, {useEffect, useState} from "react";
import {Input, Table} from 'antd';
import {
    ItemTypedValue,
    ShashemeneEstimationConfiguration
} from "../../configuration-model/estimation-configuration-model";
import {useSelector} from "react-redux";
import {TransactionItemsState} from "../../../../../../_model/state-model/ic-sm/transaction-items-state";
import {ApplicationState} from "../../../../../../_model/state-model/application-state";
import {TransactionItems} from "../../../../../../_model/level0/iERP-transaction-model/transaction-items";
const { Column } = Table;

interface IProps{
    configuration : ShashemeneEstimationConfiguration
    handleMeterDepositChange : ( privateItemCode:string,sector:string, newValue:number)=>void
}

type TableDataType = TransactionItems & ItemTypedValue & { key : number|string }
export function DepositRuleContainer(props  : IProps){
    const [ transactionItemState ] = useSelector<ApplicationState,[ TransactionItemsState ]>(appState => [ appState.transaction_items_state ])
    const [ tableData , setTableData ] = useState<TableDataType[]>([])
    useEffect(()=>{
        !transactionItemState.loading &&
        !transactionItemState.error &&
        initTableData()
    },[transactionItemState])
    function initTableData(){
        const tableData = transactionItemState.transactions_items.map<TableDataType>( (transactionItem,key)=>{
            let td : TableDataType = {
                key,
                ...transactionItem,
                ...new ItemTypedValue()
            }
            const item = props.configuration.meterDeposit.filter(typedvalue => typedvalue.privateItemCode === transactionItem.code);
            if (item.length > 0){
                td = {...td,...item[0]}
            }
            return td;
        } )
        setTableData(tableData)
    }

    return (
        <Table
            dataSource={tableData}
            pagination={false}
            scroll={{ y: '74vh' }}
            size={'small'}
            loading={transactionItemState.loading} >
            <Column
                title={'Meter Type'}
                width={'40%'}
                key={'name'}
                dataIndex={'name'}
            />
            <Column
                title={'Deposit Amount(Private)'}
                key="privateValue"
                width={'30%'}
                dataIndex={'privateValue'}
                render={
                    (value,rowData : TableDataType) => <Input
                                                            defaultValue={value}
                                                            onChange={(event)=> rowData.code && props.handleMeterDepositChange(rowData.code,'privateValue',+event.target.value) }
                                                        />
                }
            />
            <Column
                title={'Deposit Amount(Commercial)'}
                key="institutionalValue"
                width={'40%'}
                dataIndex={'institutionalValue'}
                render={(value,rowData : TableDataType) => <Input
                                                                defaultValue={value}
                                                                onChange={(event)=> rowData.code && props.handleMeterDepositChange(rowData.code,'institutionalValue',+event.target.value) }
                                                            />
                }
            />
        </Table>
    )
}