import React from "react";
import {Col, Row, Typography, Collapse, Icon, Divider} from "antd";
import {ItemTypedValueFormCard} from "../item-typed-value-form/item-typed-value-form-card";
import {ItemRegistrationForm} from "../item-registration-form/item-registration-form";
import {ShashemeneEstimationConfiguration} from "../../configuration-model/estimation-configuration-model";
import {IActiveItem} from "../estimation-configuration-container";
import CButton from "../../../../../../shared/core/cbutton/cbutton";
import {ItemsListItem} from "../../../../../../_model/level0/job-manager-model/items-list-item";

const { Panel } = Collapse;
const { Title,Text } = Typography
interface IProps{
    configuration : ShashemeneEstimationConfiguration
    addItemHandler : (title:string , sector:string , itemKey: string , isItemList?: boolean, sectorName?:string)=> void
    activeItem? : IActiveItem
    setItemCode : ( activeItem : IActiveItem, value: string) => void

    handleRemoveItemFromList : ( activeItem : IActiveItem, value: ItemsListItem) => void
    handleAddItemToList : ( activeItem : IActiveItem, value: ItemsListItem) => void
}


const customPanelStyle = {
    background: '#f7f7f7',
    margin: 5,
    border: 0
};
interface IListItemProps{
    item : ItemsListItem
    activeItem : IActiveItem
}
export function GeneralConfigurationContainer(props : IProps){


    function ListItem( listItemProps : IListItemProps ){
        return (
            <Row gutter={4} style={{margin:'2px'}} className={'paper'}>
                <Col span={18}>
                    <Text strong>
                        {listItemProps.item && listItemProps.item.titem && listItemProps.item.titem.code}
                        <Divider type="vertical" />
                        {listItemProps.item && listItemProps.item.titem && listItemProps.item.titem.name}
                    </Text>
                </Col>
                <Col span={6}>
                    <CButton onClick={()=>props.handleRemoveItemFromList(listItemProps.activeItem,listItemProps.item) } size={'small'} type={'danger'} block>Remove</CButton>
                </Col>
            </Row>
        )
    }
    const AddItem = (activeItem : IActiveItem) => (
        <Icon
            type="plus"
            style={{fontSize:'20px',color:'#1890ff'}}
            onClick={event => {
                // If you don't want click extra trigger collapse, you can prevent this:
                event.stopPropagation();
                props.addItemHandler(activeItem.title,activeItem.sector,activeItem.itemKey, true)
            }}
        />
    );
    return (
        <Row gutter={4}>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    <div className={'paper'} style={{marginBottom:'5px',paddingTop:'5px',paddingBottom:'5px'}}>
                        <Collapse
                            bordered={false}
                            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
                        >
                            <Panel
                                header="Pipeline Items"
                                key="1"
                                style={customPanelStyle}
                                extra={<AddItem title={'Pipeline Items'} itemKey={'pipelineItems'} sector={''} />}
                            >
                                    {
                                        props.configuration.pipelineItems.map((value,key) =><ListItem
                                                                                                            key={key}
                                                                                                            activeItem={
                                                                                                                {
                                                                                                                    itemKey:'pipelineItems',
                                                                                                                    title:'Pipeline Items',
                                                                                                                    sector:''
                                                                                                                }
                                                                                                            }
                                                                                                            item={value}/>)
                                    }
                            </Panel>
                            <Panel header="HDP Pipeline items"
                                   key="2"
                                   style={customPanelStyle}
                                   extra={<AddItem title={'HDP Pipeline items'} itemKey={'hdpPipelineItems'} sector={''}/>}>
                                {
                                    props.configuration.hdpPipelineItems.map((value ,key) =><ListItem
                                                                                                            key={key}
                                                                                                            activeItem={
                                                                                                                {
                                                                                                                    itemKey:'hdpPipelineItems',
                                                                                                                    title:'HDP Pipeline items',
                                                                                                                    sector:''
                                                                                                                }
                                                                                                            }
                                                                                                            item={value}/>)
                                }
                            </Panel>
                            <Panel
                                header="Water meter items"
                                key="3"
                                style={customPanelStyle}
                                extra={<AddItem title={'Water meter items'} itemKey={'waterMeterItems'} sector={''}/>}
                            >
                                {
                                    props.configuration.waterMeterItems.map((value,key)=><ListItem
                                                                                                         key={key}
                                                                                                         activeItem={
                                                                                                             {
                                                                                                                 itemKey:'waterMeterItems',
                                                                                                                 title:'Water meter items',
                                                                                                                 sector:''
                                                                                                             }
                                                                                                         }
                                                                                                         item={value}/>)
                                }
                            </Panel>
                            <Panel
                                header="Survey items"
                                key="4"
                                style={customPanelStyle}
                                extra={<AddItem title={'Survey items'} itemKey={'surveyItems'} sector={''} />}
                            >
                                {
                                    props.configuration.surveyItems.map((value,key)=><ListItem
                                                                                                    key={key}
                                                                                                    activeItem={
                                                                                                        {
                                                                                                            itemKey:'surveyItems',
                                                                                                            title:'Survey items',
                                                                                                            sector:''
                                                                                                        }
                                                                                                    }
                                                                                                    item={value}/>)
                                }
                            </Panel>
                        </Collapse>
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Form Fee'}
                            item={props.configuration.formFee}
                            itemKey={'formFee'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                     </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Estimation Fee'}
                            item={props.configuration.estimationFee}
                            itemKey={'estimationFee'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Transit Charge'}
                            item={props.configuration.transitFee}
                            itemKey={'fileFolderFee'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Technical Service Charge(pipeline: 0-5 pcs)'}
                            item={props.configuration.technicalServiceMultiplierTeir1}
                            itemKey={'technicalServiceMultiplier'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Technical Service Charge(pipeline: 6-10 pcs)'}
                            item={props.configuration.technicalServiceMultiplierTeir2}
                            itemKey={'technicalServiceMultiplierTeir2'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Technical Service Charge(pipeline: >10 pcs)'}
                            item={props.configuration.technicalServiceMultiplierTeir3}
                            itemKey={'technicalServiceMultiplierTeir3'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Technical Service Charge(Fittings)'}
                            item={props.configuration.technicalServiceMultiplier}
                            itemKey={'technicalServiceMultiplier'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Contract Card Fee'}
                            item={props.configuration.contractCardFee}
                            itemKey={'contractCardFee'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'File Folder Fee'}
                            item={props.configuration.fileFolderFee}
                            itemKey={'fileFolderFee'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Permission Fee'}
                            item={props.configuration.permissionFee}
                            itemKey={'permissionFee'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                    <div style={{marginBottom:'5px'}}>
                        <ItemTypedValueFormCard
                            title={'Service Charge'}
                            item={props.configuration.serviceChargeMultiplier}
                            itemKey={'serviceChargeMultiplier'}
                            addItemHandler={props.addItemHandler}
                            setItemCode={props.setItemCode}
                        />
                    </div>
                </div>
            </Col>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    {
                        !props.activeItem &&
                        <div className={'paper'} style={{paddingTop:'35vh',height:'75vh'}}>
                            <Title level={4} style={{textAlign:'center'}}>Select configuration item</Title>
                        </div>
                    }
                    {
                        props.activeItem &&
                        <ItemRegistrationForm
                            activeItem ={ props.activeItem }
                            setItemCode={ props.setItemCode}
                            handleAddItemToList={props.handleAddItemToList}
                        />
                    }
                </div>
            </Col>
        </Row>
    )
}