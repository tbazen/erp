import React, {Fragment, useEffect, useState} from "react";
import {Button, Modal} from "antd";
import CButton from "../../../../../../shared/core/cbutton/cbutton";
import {useDispatch, useSelector} from "react-redux";
import {fetchItemCategories} from "../../../../../../_redux_setup/actions/ic-actions/item-configuration-actions";
import {ApplicationState} from "../../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../../_model/state-model/auth-state";
import {ItemCategoryState} from "../../../../../../_model/state-model/ic-sm/item-catagory-state";
import CLoadingPage from "../../../../../../shared/screens/cloading/cloading-page";

export function ItemCategoryPicker() {
    const [ isOpen , setOpen ] = useState<boolean>(false)
    const [ auth, itemCategoryState ] = useSelector<ApplicationState,[AuthenticationState,ItemCategoryState]>
    (appState=> [appState.auth,appState.item_category_state])
    const dispatch = useDispatch();
    useEffect(()=>{
        if( !(itemCategoryState.item_categories.length > 0 ) )
            dispatch(fetchItemCategories({sessionId:auth.sessionId,PID:-1}))
    },[])
    return (
        <Fragment>
            <CButton type="primary" onClick={()=>setOpen(true)}>category</CButton>
            <Modal
                title="Item Category"
                centered
                visible={isOpen}
                onOk={() => setOpen(false)}
                onCancel={() => setOpen(false)}
            >
                {
                    itemCategoryState.loading &&
                    <CLoadingPage/>
                }
                {
                    !itemCategoryState.loading &&
                    itemCategoryState.error &&
                    <p>Failed to load Item categories</p>
                }
                {
                    !itemCategoryState.loading &&
                    !itemCategoryState.error &&
                    itemCategoryState.item_categories.map((value,key)=><p key={key}>{value.nameCode}</p>)
                }
            </Modal>
        </Fragment>
    )
}