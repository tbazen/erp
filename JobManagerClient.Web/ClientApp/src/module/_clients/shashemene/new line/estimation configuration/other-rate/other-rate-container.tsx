import React from "react";
import {ItemTypedValueFormCard } from "../item-typed-value-form/item-typed-value-form-card";
import {Col, Row, Typography} from "antd";
import {
    ShashemeneEstimationConfiguration
} from "../../configuration-model/estimation-configuration-model";
import {IActiveItem} from "../estimation-configuration-container";
import {ItemRegistrationForm} from "../item-registration-form/item-registration-form";
import {ItemsListItem} from "../../../../../../_model/level0/job-manager-model/items-list-item";
const { Title} = Typography

interface IProps{
    configuration : ShashemeneEstimationConfiguration
    addItemHandler : (title:string , sector:string , itemKey: string , isItemList?: boolean, sectorName?:string)=> void
    activeItem?: IActiveItem
    setItemCode : ( activeItem : IActiveItem, value: string) => void

    handleRemoveItemFromList : ( activeItem : IActiveItem, value: ItemsListItem) => void
    handleAddItemToList : ( activeItem : IActiveItem, value: ItemsListItem) => void
}
export function OtherRateContainer(props : IProps){

    return (
        <Row gutter={4}>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    <ItemTypedValueFormCard
                        item={props.configuration.ownershipTransferFee}
                        title={'Ownership transfer fee'}
                        itemKey={'ownershipTransferFee'}
                        addItemHandler={props.addItemHandler}
                        setItemCode={props.setItemCode}
                    />
                </div>
            </Col>
            <Col span={12}>
                <div style={{backgroundColor:'#eee',overflowY:'auto', maxHeight:'80vh', padding:"10px"}}>
                    {
                        !props.activeItem &&
                        <div className={'paper'} style={{paddingTop:'35vh',height:'75vh'}}>
                            <Title level={4} style={{textAlign:'center'}}>Select configuration item</Title>
                        </div>
                    }
                    {
                        props.activeItem &&
                        <ItemRegistrationForm
                            activeItem ={ props.activeItem }
                            setItemCode={ props.setItemCode}
                            handleAddItemToList={ props.handleAddItemToList }
                        />
                    }
                </div>
            </Col>
        </Row>
    )
}