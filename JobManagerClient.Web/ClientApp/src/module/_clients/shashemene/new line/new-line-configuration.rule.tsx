import {NewLineRule} from "../../../main/new line/new-line.rule";
import {EstimationConfigurationContainer} from "./estimation configuration/estimation-configuration-container";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import {NewConnectionDetailFormContainer} from "../../../main/new line/new-connection-detail/new-connection-detail-form-container";

@JobRuleClientDecorator({
    jobType:StandardJobTypes.NEW_LINE,
    jobName:"New Line",
    route:ROUTES.JOB.NEW_LINE.INDEX,

    hasConfiguration:true,
    configurationName:"Configure Estimation",
    configurationRoute:ROUTES.CONFIGURATION.NEW_LINE,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.NEW_LINE].ABSOLUTE,
            component:NewConnectionDetailFormContainer
        }
    ]
})
export class NewLineConfigurationRule extends NewLineRule{
    getConfigurationForm(){
        return EstimationConfigurationContainer
    }
}

