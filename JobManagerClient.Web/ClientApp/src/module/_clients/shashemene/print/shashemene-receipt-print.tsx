import React,{ FC, Fragment } from "react";
import {Col, Divider, Row, Typography} from "antd";
import {CustomerPaymentReceipt} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {PrintReceiptParameters} from "../../../../_model/client/subscriber-managment/print-receipt-parameters";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";

const { Text } = Typography

interface ShashemeneReceiptPrintProps{
    receipt:CustomerPaymentReceipt
    pars: PrintReceiptParameters
}

export const ShashemeneReceiptPrint:FC<ShashemeneReceiptPrintProps>=(props)=>{
    let total = 0
    props.receipt.billItems && props.receipt.billItems.forEach(bill=>{ total+=bill.price })

    return (
        <Row gutter={4}>
            <Col lg={{span:15,offset:5}}>
                <Divider orientation={'center'}>Receipt</Divider>
                <Text strong>{props.pars.mainTitle}</Text>
                <DataRow title={'NO.'} content={props.receipt.receiptNumber && props.receipt.receiptNumber.reference}/>
                <DataRow title={'Date'} content={props.receipt.documentDate}/>
                <DataRow title={'Name'} content={props.receipt.customer && props.receipt.customer.name}/>
                <DataRow title={'Customer Code'} content={props.receipt.customer && props.receipt.customer.customerCode}/>
                <Divider/>
                <Row gutter={2}>
                    <Col span={6}><Text strong>Item</Text></Col>
                    <Col span={6}><Text strong>Quantity</Text></Col>
                    <Col span={6}><Text strong>Unit Price</Text></Col>
                    <Col span={6}><Text strong>Price</Text></Col>
                </Row>
                {
                    props.receipt.billItems &&
                    props.receipt.billItems.map((billItem,index)=>(
                        <Fragment key={index}>
                            <Divider/>
                            <Row gutter={2}>
                                <Col span={6}><Text>{ index + 1 }</Text></Col>
                                <Col span={6}><Text>{ billItem.quantity>0 ? billItem.quantity  : ''}</Text></Col>
                                <Col span={6}><Text>{ billItem.quantity>0 ? billItem.unitPrice : ''}</Text></Col>
                                <Col span={6}><Text>{ billItem.price }</Text></Col>
                            </Row>
                        </Fragment>
                    ))
                }
                <DataRow title={'Total'} content={parseNumberToMoney(total)}/>
                <DataRow title={''} content={''}/>
                <DataRow title={'Cashier'} content={props.pars.casheirName}/>
                <DataRow title={''} content={''}/>
                <DataRow title={'Signature'} content={''}/>
                <Divider/>
                <DataRow title={''} content={''}/>
                <Divider/>
                <Text strong>Computerized by INTAPS. Call www.intaps.com</Text>
            </Col>
        </Row>
    )
}