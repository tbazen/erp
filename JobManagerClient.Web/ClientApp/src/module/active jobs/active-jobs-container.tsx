import { Empty, Typography } from "antd";
import React, {Fragment, useEffect, useState} from 'react';
import { useDispatch, useSelector } from "react-redux";
import ButtonGridContainer from "../../shared/screens/button-grid-container/button-grid-container";
import { getLocalDateString } from "../../_helpers/date-util";
import { ApplicationState } from "../../_model/state-model/application-state";
import { AuthenticationState } from "../../_model/state-model/auth-state";
import {
    fetchFilteredActiveJobs,
    searchActiveJobs,
} from '../../_redux_setup/actions/aj-actions/active-job-actions';
import { GetFilteredJobsPar, SearchJobPar } from '../../_services/job.manager.service';
import {JobCard} from "./job-card";
import {ActiveJobsState} from "../../_model/state-model/aj-sm/active-jobs-state";
import {useGlobalSearchBar} from "../../shared/hooks/manage-global-search";
import {StateMachineHOC} from "../../shared/hoc/StateMachineHOC";

const {Text}=Typography;

const ActiveJobsContainer = () => {
    const dispatch = useDispatch()
    const [auth, active_jobs_state] = useSelector<ApplicationState, [AuthenticationState, ActiveJobsState]>(appState => [appState.auth, appState.active_jobs_state])
    const jobFilter: GetFilteredJobsPar = { sessionId: auth.sessionId, date: getLocalDateString() }
    const [ jobSearchParams,setJobSearchParams ] = useState<SearchJobPar>({index:0,pageSize:30,query:'',sessionID:auth.sessionId})
    useGlobalSearchBar({
        searchPlaceHolder:'Job Number',
        searchHandler: searchInputHandler
    })
    useEffect(()=>{
        if(jobSearchParams.query.length > 0 ){
            dispatch(searchActiveJobs(jobSearchParams));
        } else {
            active_jobs_state.payload.length === 0 &&
            dispatch(fetchFilteredActiveJobs(jobFilter));
        }
    },[jobSearchParams])

    function searchInputHandler(event:any){
        const query : string = event.target.value.toString()
        setJobSearchParams({...jobSearchParams,query})
    }

    return (
        <StateMachineHOC state={active_jobs_state}>
            {
                active_jobs_state.payload.length > 0 &&
                <div style={{ overflowX:'hidden', overflowY: 'hidden'}}>
                    <ButtonGridContainer spacing={4} lg={{ span: 4 }} component={JobCard} listItem={ active_jobs_state.payload} />
                </div>
            }
            {
                active_jobs_state.payload.length === 0 &&
                <Empty
                    style={{
                        height: '88vh',
                        paddingTop: '30vh'
                    }}
                    description={
                    <span>
                        <Text strong>
                            No active jobs at the time
                        </Text>        
                    </span>
                    }
                ></Empty>
            }
        </StateMachineHOC>
    )
}

export default ActiveJobsContainer