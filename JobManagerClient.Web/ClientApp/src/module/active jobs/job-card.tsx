import React from "react";
import {Card} from "antd";
import {JobIcon} from "../../static/images/index-imgs";
import {ROUTES} from "../../_constants/routes";
import {Link} from "react-router-dom";
import {IActiveJobPayload} from "../../_model/state-model/aj-sm/active-jobs-state";
const { Meta } = Card;

export const JobCard = (activeJob: IActiveJobPayload) => {
    const styledTitle = () => (<div>
                                    <p style={{ textAlign: 'center' }}>{activeJob.job && activeJob.job.jobNo}</p>
                                    <p style={{ textAlign: 'center' }}>{activeJob.customer && activeJob.customer.name}</p>
                                    <p style={{ textAlign: 'center' }}>{activeJob.jobStatus && activeJob.jobStatus.name}</p>
                                </div> )

    return (
        <Link to={`${ROUTES.ACTIVE_JOBS}/${activeJob.job.id}`}>
            <Card
                hoverable
                style={{ height: '250px', maxHeight: '600px' }}
                cover={<img style={{ width: '25%', paddingTop: '25px', margin: 'auto' }} alt={'Job ico'} src={JobIcon} />}
                bordered={false} >
                <Meta description={styledTitle()}/>
            </Card>
        </Link>
    )
}