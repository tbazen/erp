import React, {useEffect} from 'react';
import {Tabs} from 'antd';
import {JobHistoryContainer} from "./job-history/job-history-container";
import {ActiveJobInformationContainer} from "./job-information/active-job-information-container";
import {useDispatch, useSelector} from "react-redux";
import {GetJobHistoryPar, GetJobPar} from "../../_services/job.manager.service";
import {
    fetchActiveJobDetailInformation,
    fetchJobHistory
} from "../../_redux_setup/actions/aj-actions/active-job-actions";
import {AuthenticationState} from "../../_model/state-model/auth-state";
import {ApplicationState} from "../../_model/state-model/application-state";
import {ActiveJobDetailInformationState} from "../../_model/state-model/aj-sm/active-job-detail-information-state";
import {StateMachineHOC} from "../../shared/hoc/StateMachineHOC";

const { TabPane } = Tabs;

interface IProps{
    match : any
}

export function JobContainer(props : IProps){
    const jobId: number = +props.match.params.jobId.toString()
    const [ auth , jobDetailInfoState  ] = useSelector<ApplicationState,[ AuthenticationState , ActiveJobDetailInformationState ]>(appState =>[appState.auth , appState.active_job_detail_information_state])
    const dispatch = useDispatch()
    useEffect(()=>{
        const jobParams : GetJobPar ={
            sessionId:auth.sessionId,
            jobID:jobId
        }
        dispatch(fetchActiveJobDetailInformation(jobParams))
    },[])
    useEffect(()=>{
        if(jobDetailInfoState.payload){
            const params : GetJobHistoryPar = {
                sessionId:auth.sessionId,
                jobID:jobId
            }
            dispatch(fetchJobHistory(params))
        }
    },[ jobDetailInfoState.payload ])

    return (
            <Tabs style={{padding:'5px'}}  defaultActiveKey="1">
                <TabPane tab="General" key="1">
                    <StateMachineHOC state={jobDetailInfoState}>
                        { jobDetailInfoState.payload && <ActiveJobInformationContainer jobInformation={jobDetailInfoState.payload} /> }
                    </StateMachineHOC>
                </TabPane>
                <TabPane tab="Job History" key="2">
                    <JobHistoryContainer/>
                </TabPane>
            </Tabs>
    )
}