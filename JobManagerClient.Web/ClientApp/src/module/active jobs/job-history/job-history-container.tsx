import React from 'react';
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {JobHistoryState} from "../../../_model/state-model/aj-sm/job-history-state";
import {Table} from "antd";
const { Column } = Table;

export function JobHistoryContainer(){
    const [jobHistoryState ] = useSelector<ApplicationState,[JobHistoryState]>(appState => [appState.job_history])
    let index = 0 ;
    return (
      <div className={'paper'}  style={{padding:'5px'}}>
        <Table
            dataSource={jobHistoryState.payload.map(value=>({...value, formatedDate: new Date(value.date).toLocaleString() }))}
            size={'small'}
            loading={jobHistoryState.loading}
            pagination={false}
        >
            <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }}/>
            <Column title="Date" dataIndex="formatedDate" key="date" />
            <Column title="Worker" dataIndex="worker" key="worker" />
            <Column title="Stage" dataIndex="stage" key="stage" />
            <Column title="Forward To" dataIndex="forwardTo" key="forwardTo" />
            <Column title="Note" dataIndex="note" key="note" />
            <Column title="Duration" dataIndex="duration" key="duration" />
        </Table>
      </div>
    )
}