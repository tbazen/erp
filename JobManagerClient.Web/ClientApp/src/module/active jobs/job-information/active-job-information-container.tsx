import React, {Fragment, useEffect} from 'react';
import {useDispatch} from "react-redux";
import {Col, Divider, Row} from "antd";
import {SubscriberInformationContainer} from "../../../shared/components/customer-information/customer-information";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions";
import { hideSearchBar } from '../../../_redux_setup/actions/global-searchbar-actions';
import { JobPossibleNextState } from './job-action-row';
import JobDetail from "./job-detail";
import BillOfQuantitiesContainer from "./bill-of-material/bill-of-quantities";
import {IActiveJobDetailPayload} from "../../../_model/state-model/aj-sm/active-job-detail-information-state";
import {WorkflowDataContainer} from "./workflow-data-container";
import {useBreadcrumb} from "../../../shared/hooks/manage-breadcrumb";
interface IProps{
    jobInformation : IActiveJobDetailPayload
}
export const ActiveJobInformationContainer = (props: IProps) => {
    const dispatch = useDispatch()
    const backwardPath: IBreadcrumb[] = [
        {
            path: ROUTES.ACTIVE_JOBS,
            breadcrumbName: 'Active Jobs'
        },
        {
            path: ROUTES.ACTIVE_JOBS,
            breadcrumbName: `${props.jobInformation.job.id}`
        }
    ]
    useBreadcrumb(backwardPath)

    useEffect(()=>{ dispatch(hideSearchBar()) },[])

    return (
        <Row gutter={4}>
            <Col span={8}>
                <div className={"paper"} style={{padding:'10px',minHeight:'30vh'}}>
                    <Divider orientation="left">Job detail</Divider>
                    <JobDetail jobInformation={props.jobInformation}/>
                </div>
            </Col>
            <Col span={8}>
                <div className={"paper"} style={{padding:'10px',minHeight:'30vh'}}>
                    <Divider orientation="left">Customer detail</Divider>
                    <SubscriberInformationContainer {...props.jobInformation.customer}/>
                </div>
            </Col>
            <Col span={8}>
                <div className={"paper"} style={{padding:'10px',minHeight:'30vh',maxHeight:'40vh',overflowY:'auto'}}>
                    <Divider orientation="left">{props.jobInformation.jobType.name} detail</Divider>
                    <WorkflowDataContainer job={props.jobInformation.job}/>
                </div>
            </Col>
            <Col span={24}>
                <div className={"paper"} style={{padding:'10px',marginTop:'5px'}}>
                    <JobPossibleNextState
                        jobInformation={props.jobInformation}
                        nextStatus={props.jobInformation.nextStatus}/>
                </div>
            </Col>
            <Col span={24}>
                <div className={"paper"} style={{padding:'10px',marginTop:'5px'}}>
                    <BillOfQuantitiesContainer
                        job={props.jobInformation.job}/>
                </div>
            </Col>
        </Row>
    )
}