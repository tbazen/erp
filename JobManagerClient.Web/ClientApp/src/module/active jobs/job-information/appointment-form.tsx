import React, {useState} from 'react'
import CModal from "../../../shared/core/cmodal/cmodal";
import {JobAppointment} from "../../../_model/level0/job-appointment";
import CFormItem from "../../../shared/core/cform-item/cform-item";
import {DatePicker} from "antd";
import TextArea from "antd/lib/input/TextArea";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {
    addActiveJobAppointment,
    updateActiveJobAppointment
} from "../../../_redux_setup/actions/aj-actions/active-job-actions";
import {MomentUtil} from "../../../_helpers/date-util";

interface IProps{
    appointment? : JobAppointment
    jobId: string
}

export function AppointmentForm(props : IProps){
    const initAppointment: JobAppointment ={
        id:0,
        active:true,
        jobID:props.jobId,
        actionDate:MomentUtil.getCurrentDateTimeString(),
        appointmentDate:MomentUtil.getCurrentDateTimeString(),
        appointmentCloseDate:MomentUtil.getCurrentDateTimeString(),
        note:''
    }
    const [ appointment , setAppointment ] = useState<JobAppointment>(props.appointment ? props.appointment: initAppointment)
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=> [appState.auth])
    const dispatch = useDispatch();
    async function handleAddAppointment(){
        dispatch( addActiveJobAppointment({sessionId:auth.sessionId,appointment}))
    }
    async function handleUpdateAppointment() {
        dispatch(updateActiveJobAppointment({sessionId:auth.sessionId,appointment}))
    }
    function localDateChangeHandler(value: any, dateString: string) {
        setAppointment({...appointment,appointmentDate:dateString,actionDate:dateString,appointmentCloseDate:dateString})
    }
    function localDescriptionChangeHandler(event: any) {
        setAppointment({...appointment,note:event.target.value})
    }
    return (
        <CModal
            buttonText={ props.appointment ===undefined? 'Set Appointment' : 'Change Appointment'}
            title={'Appointment Form'}
            withConfirmation
            submitHandler={props.appointment ===undefined? handleAddAppointment : handleUpdateAppointment}
        >
            <div>
                <CFormItem
                    input={<DatePicker defaultValue={MomentUtil.getCurrentDateTime()}  style={{width:'100%'}} showTime={{use12Hours:true}}  onChange={localDateChangeHandler} size={'large'} />}
                    label={'Appointment Date & Time'}
                    required />
                <CFormItem
                    input={<TextArea rows={5} onChange={localDescriptionChangeHandler} />}
                    label={'Appointment Note'}
                    required
                    errorMessage={'Please fill the description'}
                />
            </div>
        </CModal>
    )
}