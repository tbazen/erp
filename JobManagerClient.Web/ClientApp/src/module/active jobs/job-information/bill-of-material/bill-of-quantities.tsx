import React, {useEffect , Fragment} from "react";
import {DeleteBOMPar, JobData} from "../../../../_services/job.manager.service";
import {Table, Divider, Popconfirm, Row, Col} from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState} from "../../../../_model/state-model/application-state";
import { AuthenticationState} from "../../../../_model/state-model/auth-state";
import { GetJobBOMPar } from '../../../../_services/job.manager.service';
import {fetchJobBOM, requestDeleteBOM} from '../../../../_redux_setup/actions/aj-actions/active-job-actions';
import {IJobBOMPayload, JobBomState} from '../../../../_model/state-model/aj-sm/job-bom-state';
import {openDrawerModal} from "../../../../_redux_setup/actions/drawer-modal-actions";
import {JobInvoiceViewer} from "./job-invoice-viewer";
import {BOMEditor, EditableFields} from "./bom-editor";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";
import CModal from "../../../../shared/core/cmodal/cmodal";
import {CashFormContainer} from "./cash-form-container";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {CSpinProps} from "../../../../shared/core/cspin/cspin";
import {StandardJobStatus} from "../../../../_model/job-rule-model/standard-job-status";
import {PrintAction} from "./bom-table-action/print-action";
const { Column } = Table;

interface IProps{
    job:JobData
}

function BillOfQuantitiesContainer(props : IProps){
    const [ auth , jobBOMState ] = useSelector<ApplicationState,[AuthenticationState,JobBomState]>(appState => [appState.auth,appState.job_bom_state]);
    const dispatch = useDispatch();
    let index = 0;
    useEffect(()=>{
        const jobBOMParams : GetJobBOMPar = {
            sessionId:auth.sessionId,
            jobID:props.job.id
        }
        dispatch(fetchJobBOM(jobBOMParams , props.job))
    },[])
    function handleDeleteBOM(record : IJobBOMPayload){
        const params : DeleteBOMPar ={
            sessionId:auth.sessionId,
            bomID:record.billOfMaterial.id
        }
        dispatch(requestDeleteBOM(params,props.job))
    }
    function handleStoreIssue(record: IJobBOMPayload){

    }


    return (
        <Table 
            dataSource={
                jobBOMState.payload !== undefined
                ? 
                jobBOMState.payload.map((value,key)=>({...value,key}))
                :
                []
            }
            pagination={false}
            loading={{...CSpinProps,spinning:jobBOMState.loading}}
        >
            <Column
                title="NO."
                key="number"
                render={(value,key) => { index+=1; return (<a>{index}</a>); }}
            />
            <Column title="Description" dataIndex="billOfMaterial.note" key="lastName" />
            <Column
                title="Amount"
                key="total"
                render={(text, record : IJobBOMPayload) => (<Fragment>
                        { record.total ===-1 && <span style={{color:'red'}}>Error</span>}
                        { record.total !==-1 && <span>{ parseNumberToMoney(record.total) }</span>}
                </Fragment>  )}
            />
            <Column title="Reception No." dataIndex="receiptNo" key="receiptNo" />
            <Column title="Store Issue No." dataIndex="billOfMaterial.storeIssueID !== -1 ? billOfMaterial.storeIssueID : ''  " key="storeIssueID" />
            <Column
                title="Action"
                key="action"
                render={(text, record : IJobBOMPayload) => (
                <Row gutter={2}>
                  <Col lg={{span:4}}>
                          <CButton type={'link'} onClick={()=>{ dispatch(openDrawerModal(<JobInvoiceViewer bom={record} />))}}>View</CButton>
                  </Col>
                    {
                        !record.isPaid &&
                        <Col lg={{span:4}}>
                            <CButton
                                type={'link'}
                                onClick={()=>{
                                    dispatch(openDrawerModal(
                                        <BOMEditor
                                            editableFields={props.job.status === StandardJobStatus.ANALYSIS ? EditableFields.ReferencePrice : EditableFields.ItemQuantity}
                                            bom={record}
                                            job={props.job}/>))}}
                                        >Edit</CButton>
                        </Col>
                    }
                    {
                        !record.isPaid &&
                        <Col lg={{span:4}}>
                            <Popconfirm onConfirm={()=>handleDeleteBOM(record)} title="Are you sure! you want to delete this bill of quantity?" okText="Yes" cancelText="No">
                                <CButton type={'link'} style={{color:'red'}}>Delete</CButton>
                            </Popconfirm>
                        </Col>
                    }
                    {
                        record.isPaymentStatus &&
                        <Col lg={{span:4}}>
                            <PrintAction bom={record} numberOfCopies={3} job={props.job}/>
                        </Col>
                    }
                    {
                        record.isStoreIssue &&
                        <Col lg={{span:4}}>
                                <CButton onClick={()=>handleStoreIssue(record)}>Store Issue</CButton>
                        </Col>
                    }
                </Row>
                )}
            />
        </Table>
    )
}

export default BillOfQuantitiesContainer