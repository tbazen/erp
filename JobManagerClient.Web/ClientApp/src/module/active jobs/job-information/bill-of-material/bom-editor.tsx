import React, {useEffect, useState} from 'react'
import {Checkbox, Col, DatePicker, Divider, Form, Input, Row, Table} from "antd";
import {IJobBOMPayload} from "../../../../_model/state-model/aj-sm/job-bom-state";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {requestSetBOM} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import JobManagerService, {
    GetSystemParameterPar,
    JobData,
    SetBillofMateialPar
} from "../../../../_services/job.manager.service";
import TextArea from "antd/lib/input/TextArea";
import {JobBillOfMaterial} from "../../../../_model/level0/job-manager-type-library/job-bill-of-material";
import {getLocalDateString, MomentUtil} from "../../../../_helpers/date-util";
import {JobBOMItem} from "../../../../_model/level0/job-manager-type-library/job-bom-item";
import {MeasureUnit, TransactionItems} from "../../../../_model/level0/iERP-transaction-model/transaction-items";
import BNFinanceService, {GetMeasureUnitPar, GetTransactionItemsPar} from "../../../../_services/bn.finance.service";
import AccountingService, {
    GetCostCenterAccountPar,
    GetNetBalanceAsOfPar
} from "../../../../_services/accounting.service";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";
import {CostCenterAccount, TransactionItem} from "../../../../_model/level0/accounting-type-library/data-structure";
import {NotificationTypes, openNotification} from "../../../../shared/components/notification/notification";
import errorMessage from "../../../../_redux_setup/actions/error.message";

const { Column } = Table;
const ierp_transaction_service = new BNFinanceService()
const job_mgr_service = new JobManagerService()
const accounting_service = new AccountingService()

export enum EditableFields {
    ItemQuantity = 1,
    ReferencePrice = 2,
    All = ItemQuantity | ReferencePrice
}
interface IProps{
    bom?: IJobBOMPayload
    job: JobData
    editableFields : EditableFields
}

interface ITableData{
    item: TransactionItems
    unit?: MeasureUnit
    customerSourced: boolean
    quantity: number

    stock?: string
    customerPrice?: number

}

const initTable : ITableData = {
    item: new TransactionItems(),
    customerSourced: false,
    quantity: 0
}

export function BOMEditor(props: IProps){
    const formLayout = 'vertical';
    const formItemLayout = {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 },
    }
    const dispatch = useDispatch()
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>( appState => [ appState.auth ] )
    const [ dataCollectionDate, setDataCollectionDate ] = useState<string>(getLocalDateString());
    const [ noteText , setTextNote ] = useState<string>('Work Payment')
    const [ tableData,setTableData ] = useState<ITableData[]>([])

    useEffect(()=>{
        loadData()
    },[props.bom])

    async function loadData(){
        try{
            setTableData([])
            if(props.bom){
                setTextNote(props.bom.billOfMaterial.note)
                setDataCollectionDate(props.bom.billOfMaterial.surveyCollectionTime)
                let tableData: ITableData[] = []
                for(const jobItem of props.bom.billOfMaterial.items){
                    const transactionItemPar : GetTransactionItemsPar ={
                        sessionId:auth.sessionId,
                        code:jobItem.itemID
                    }
                    const transactionItem : TransactionItems = ( await ierp_transaction_service.GetTransactionItems(transactionItemPar)).data
                    let unit: MeasureUnit | undefined = undefined
                    let stock: string | undefined = undefined
                    const measureUnitPar: GetMeasureUnitPar ={
                        sessionId:auth.sessionId,
                        id:transactionItem.measureUnitID
                    }
                    const measureUnit: MeasureUnit = ( await ierp_transaction_service.GetMeasureUnit(measureUnitPar) ).data
                    if(measureUnit){unit = measureUnit}

                    const systemParameterPar:GetSystemParameterPar ={
                        sessionId:auth.sessionId,
                        p:'materialStoreID'
                    }
                    const storeId:number = (await job_mgr_service.GetSystemParameterWeb(systemParameterPar)).data
                    if(storeId === -1){
                        stock= parseNumberToMoney(0).toString()
                    } else {
                        const costCenterPar : GetCostCenterAccountPar = {
                            sessionId:auth.sessionId,
                            costCenterID: storeId,
                            accountID: transactionItem.inventoryAccountID
                        }
                        const costCenterAccount: CostCenterAccount = (await accounting_service.GetCostCenterAccount(costCenterPar) ).data
                        if(costCenterAccount){
                            const netBalanceAsOfPar: GetNetBalanceAsOfPar = {
                                sessionId:auth.sessionId,
                                csAccountID:costCenterAccount.id,
                                itemID: TransactionItem.MATERIAL_QUANTITY,
                                date: getLocalDateString()
                            }
                            const netBalance =(await accounting_service.GetNetBalanceAsOf(netBalanceAsOfPar)).data
                            stock = netBalance
                        }
                    }

                    const tableRowData : ITableData = {
                        item: transactionItem,
                        customerSourced:!jobItem.inSourced,
                        quantity: jobItem.quantity > 0 ? jobItem.quantity : 0,
                        stock,
                        unit,
                    }
                    tableData = [ ...tableData,tableRowData ]
                }
                setTableData(tableData)
            }
            else{setTableData([initTable])}
        } catch(e){

        }
    }
    async function fetchTransactionItem(itemCode:string,index:number){
        try{
            const transactionItemsPar : GetTransactionItemsPar ={
                sessionId: auth.sessionId,
                code:itemCode
            }
            const item: TransactionItems = (await ierp_transaction_service.GetTransactionItems(transactionItemsPar)).data
            if(item){
                let unit: MeasureUnit | undefined = undefined
                let stock: string | undefined = undefined
                const measureUnitPar: GetMeasureUnitPar ={
                    sessionId:auth.sessionId,
                    id:item.measureUnitID
                }
                const measureUnit: MeasureUnit = ( await ierp_transaction_service.GetMeasureUnit(measureUnitPar) ).data
                if(measureUnit){unit = measureUnit}

                const systemParameterPar:GetSystemParameterPar ={
                    sessionId:auth.sessionId,
                    p:'materialStoreID'
                }
                const storeId:number = (await job_mgr_service.GetSystemParameterWeb(systemParameterPar)).data
                if(storeId === -1){
                    stock= parseNumberToMoney(0).toString()
                } else {
                    const costCenterPar : GetCostCenterAccountPar = {
                        sessionId:auth.sessionId,
                        costCenterID: storeId,
                        accountID: item.inventoryAccountID
                    }
                    const costCenterAccount: CostCenterAccount = (await accounting_service.GetCostCenterAccount(costCenterPar) ).data
                    if(costCenterAccount){
                        const netBalanceAsOfPar: GetNetBalanceAsOfPar = {
                            sessionId:auth.sessionId,
                            csAccountID:costCenterAccount.id,
                            itemID: TransactionItem.MATERIAL_QUANTITY,
                            date: getLocalDateString()
                        }
                        const netBalance =(await accounting_service.GetNetBalanceAsOf(netBalanceAsOfPar)).data
                        stock = netBalance
                    }
                }
                const newTableData: ITableData[] = [...tableData]
                const oldItem = newTableData[index]
                newTableData[index] = {...oldItem, item,unit,stock}
                setTableData(newTableData)
            }
            else{
                const newTableData: ITableData[] = [...tableData]
                newTableData[index] = initTable
                setTableData(newTableData)
            }
        } catch (e) {

        }
    }
    function handleAddNewBOM(){
        setTableData([...tableData, initTable])
    }
    function handleRemoveBOM(index: number){
        const newTableData = [...tableData]
        newTableData.splice(index,1)
        setTableData([...newTableData])
    }
    function handleInputChange(row: ITableData,index: number ){
        const newTableData = [...tableData]
        newTableData[index] = row
        setTableData([...newTableData])
    }
    async function handleSubmitBOM(){
        try {
            if (dataCollectionDate) {
                const jobBillOfMaterial = new JobBillOfMaterial()
                jobBillOfMaterial.jobID = props.job.id
                jobBillOfMaterial.analysisTime = getLocalDateString()
                jobBillOfMaterial.note = noteText
                jobBillOfMaterial.surveyCollectionTime = dataCollectionDate
                let jobBOMList: JobBOMItem[] = []
                if(!(tableData.length >= 1))
                    throw new Error("You have to add at least one item")
                for (const tableRow of tableData) {
                    if(!tableRow.item.code)
                        throw new Error("Not all Item codes are valid")
                    if(tableRow.item.code.trim() === '')
                        throw new Error("Not all Item codes are valid")
                    const jobBomItem = new JobBOMItem()
                    jobBomItem.itemID = tableRow.item.code
                    if (tableRow.item.fixedUnitPrice !== 0) {
                        if(!(tableRow.quantity > 0 ))
                            throw new Error("Invalid Quantity,Please make sure all entries have quantity greater than 0")
                        jobBomItem.quantity = tableRow.quantity
                    } else {
                        jobBomItem.quantity = -1.0
                    }
                    jobBomItem.inSourced = !tableRow.customerSourced
                    if (tableRow.customerPrice && tableRow.customerPrice > 0) {
                        jobBomItem.referenceUnitPrice = tableRow.customerPrice
                    } else {
                        jobBomItem.referenceUnitPrice = -1
                    }
                    jobBOMList = [...jobBOMList, jobBomItem]
                }
                jobBillOfMaterial.items = jobBOMList
                if (props.bom) {
                    jobBillOfMaterial.id = props.bom.billOfMaterial.id
                }
                const params: SetBillofMateialPar = {
                    sessionId: auth.sessionId,
                    bom: jobBillOfMaterial
                }
                dispatch(requestSetBOM(params, props.job))
            }
        }catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:"Invalid Input",
                description:errorMessage(error)
            })
        }
    }


    return (
        <div>
            <Divider orientation="left">Bill Of Material Editor</Divider>
            <Form layout={formLayout}>
                <Form.Item label="Data collection date" {...formItemLayout}>
                    <DatePicker
                        defaultValue={MomentUtil.getCurrentDateTime()}
                        onChange={(date,dateString)=>setDataCollectionDate(dateString)}
                        style={{width:'100%'}}
                    />
                </Form.Item>

                <Form.Item label="Note" {...formItemLayout}>
                    <TextArea
                        onChange={(e)=>setTextNote(e.target.value)}
                        rows={5}
                        value={noteText} />
                </Form.Item>
                <Row gutter={4}>
                    <Col span={24}>
                    <Table
                        pagination={false}
                        dataSource={tableData.map((td,key)=>({...td,key}))}>
                        {
                            props.editableFields === EditableFields.ItemQuantity &&
                            <Column
                                title="Item Code"
                                dataIndex="item.code"
                                key="itemCode"
                                render={(itemCode, rowData:ITableData,index)=>
                                    <Input
                                        size={'small'}
                                        value={itemCode}
                                        onChange={(event)=>handleInputChange({...rowData,item:{...rowData.item,code:event.target.value}},index)}
                                        onBlur={()=>fetchTransactionItem(itemCode,index)}
                                        disabled={props.editableFields === EditableFields.ReferencePrice}
                                    />
                                }
                            />
                        }
                        <Column title="Item Name" dataIndex="item.name" key="item.name" />
                        <Column title="Unit" dataIndex="unit.name" key="unit.name" />
                        {
                            props.editableFields === EditableFields.ItemQuantity &&
                            <Column
                                title="Quantity"
                                dataIndex="quantity"
                                key="quantity"
                                render={(quantity, rowData:ITableData, index)=>
                                    <Input
                                        size={'small'}
                                        disabled={props.editableFields === EditableFields.ReferencePrice}
                                        value={quantity == 0 ? '' : quantity}
                                        onChange={(event)=>handleInputChange({...rowData,quantity:+event.target.value},index)}
                                    />}
                            />
                        }
                        <Column
                            title="Standard Price"
                            dataIndex="item.fixedUnitPrice"
                            key="item.fixedUnitPrice"
                         />
                        {
                            props.editableFields === EditableFields.ReferencePrice &&
                            <Column
                                title="Customer Price"
                                dataIndex="customerPrice"
                                key="customerPrice"
                                render={(customerPrice, rowData:ITableData,index)=>
                                    <Input
                                        size={'small'}
                                        disabled={props.editableFields === EditableFields.ItemQuantity}
                                        value={customerPrice}
                                        onChange={(event)=>handleInputChange({...rowData,customerPrice:+event.target.value},index)}
                                    />}
                            />
                        }
                        <Column title="Stock" dataIndex="stock" key="stock" />
                        <Column
                            title="Customer Source"
                            dataIndex="customerSourced"
                            key="customerSourced"
                            render={(customerSourced,rowData)=><Checkbox checked={customerSourced}/>}
                        />
                        {
                            props.editableFields === EditableFields.ItemQuantity &&
                            <Column
                                title="Action"
                                dataIndex="action"
                                key="actions"
                                render={(text: string, rowData: ITableData,index)=> <a onClick={()=>handleRemoveBOM(index)}>Remove</a>}
                            />
                        }
                </Table>
                    </Col>
                    <Col span={6}>
                        <CButton disabled={props.editableFields === EditableFields.ReferencePrice} style={{marginTop:'5px'}} onClick={handleAddNewBOM} block>Add New Item</CButton>
                    </Col>
                    <Col span={6}>
                        <CButton style={{marginTop:'5px'}} type={'primary'} onClick={handleSubmitBOM} block>Submit</CButton>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}