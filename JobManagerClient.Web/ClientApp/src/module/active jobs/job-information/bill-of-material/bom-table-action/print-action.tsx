import React,{ FC } from "react";
import {IJobBOMPayload} from "../../../../../_model/state-model/aj-sm/job-bom-state";
import {JobData} from "../../../../../_services/job.manager.service";
import {CashFormContainer} from "../cash-form-container";
import CModal from "../../../../../shared/core/cmodal/cmodal";

interface PrintActionProps {
    bom: IJobBOMPayload
    numberOfCopies: number
    job:JobData
}

export const PrintAction:FC<PrintActionProps>=(props)=>{
    return (
        <CModal
            title={'Cash Form'}
            buttonProps={{type:'link'}}
            buttonText={'Print Receipt'}
            modalProps={{destroyOnClose:true}}>
            <CashFormContainer job={props.job} bom={props.bom} numberOfCopies={3}/>
        </CModal>
    )
}