import React, {Fragment, useEffect, useState} from 'react'
import CModal from "../../../../shared/core/cmodal/cmodal";
import {PaymentInstrumentFormContainer} from "./payment-instrument-form-container";
import {Col, Form, Input, Row} from "antd";
import {IJobBOMPayload} from "../../../../_model/state-model/aj-sm/job-bom-state";
import AccountingService, {
    GetAccountDocumentPar,
    PostGenericDocumentWebPar
} from "../../../../_services/accounting.service";
import {
    BillItem,
    CustomerBillDocument,
    CustomerPaymentReceipt
} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {IStateMachineBase} from "../../../../_model/state-model/state-machine-base";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";
import {PrintReceiptParameters} from "../../../../_model/client/subscriber-managment/print-receipt-parameters";
import {PaymentCenter} from "../../../../_model/level0/subscriber-managment-type-library/payment-center";
import SubscriberManagementService, {
    GetCustomerBillItemsPar,
    GetSystemParametersWebPar
} from "../../../../_services/subscribermanagment.service";
import {getLocalDateString} from "../../../../_helpers/date-util";
import {Subscriber} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import {
    BankBranchInfo,
    BankInfo,
    PaymentInstrumentItem,
    PaymentInstrumentType
} from "../../../../_model/level0/iERP-transaction-model/payment-instrument-item";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import BNFinanceService, {
    GetBankAccountPar,
    GetBankBranchInfoPar,
    GetBankInfoPar,
    GetPaymentInstrumentTypePar
} from "../../../../_services/bn.finance.service";
import {AccountDocument} from "../../../../_model/level0/accounting-type-library/account-document";
import {NotificationTypes, openNotification} from "../../../../shared/components/notification/notification";
import errorMessage from "../../../../_redux_setup/actions/error.message";
import {GetJobBOMPar, JobData} from "../../../../_services/job.manager.service";
import {fetchJobBOM} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {BankAccountInfo} from "../../../../_model/level0/iERP-transaction-model/bank-account-info";
import {
    PaymentInstrumentItemValidationParams,
    PaymentInstrumentItemExtension
} from "../../../../_helpers/extensions/job-manager/payment-instrument-item-extension";
import {horizontalFormItemLayout} from "../../../../shared/core/item-layout/item-layout";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {StringExtension} from "../../../../_helpers/extensions/string-extension";

const InputGroup = Input.Group;
const accounting_service = new AccountingService()
const subscriber_mgr_service = new SubscriberManagementService()
const bn_finance_service  = new BNFinanceService()

interface CashFormContainerProps {
    bom: IJobBOMPayload
    numberOfCopies: number
    job:JobData
}
interface ICashForm extends IStateMachineBase {
    account_documents: CustomerBillDocument[]
    current_payment_center: PaymentCenter
    receipt_title: string
    customer_bill_items:BillItem[]
    cash_instrument_code: string
}
const initCashForm: ICashForm = {
    loading: false,
    error: false,
    message: '',
    account_documents: [],
    customer_bill_items: [],
    current_payment_center: new PaymentCenter(),
    receipt_title: '',
    cash_instrument_code: ''
}
const loadingCashForm = {...initCashForm,loading: true}
const errorCashForm = {...initCashForm,loading: false, error: true}
const successCashForm = {...initCashForm,loading: false, error:false}

interface PaymentInstrumentTypeAndItem{
    instrumentItem: PaymentInstrumentItem,
    instrumentType: PaymentInstrumentType
}

export function CashFormContainer(props: CashFormContainerProps) {
    const formLayout = 'horizontal';
    const formItemLayout = horizontalFormItemLayout
    const dispatch = useDispatch()
    const [ auth] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])
    const [ cashState, setCashState] = useState<ICashForm>(initCashForm)
    const [ paymentInstrumentItem, setPaymentInstrumentItem ] = useState<PaymentInstrumentItem>(new PaymentInstrumentItem());
    const [ activePaymentInstrumentType, setActivePaymentInstrumentType ] = useState<PaymentInstrumentType>()
    const [ paymentInstrumentsTypeAndItem, setPaymentInstrumentsTypeAndItem ] = useState<PaymentInstrumentTypeAndItem[]>([])
    const [ instrumentPaymentAmount , setInstrumentPaymentAmount ] = useState<number>(0.0)

    const [ cashPaymentAmount, setCashPaymentAmount ] = useState<string>(props.bom.total.toString())
    const [ totalPaymentAmount, setTotalPaymentAmount ] = useState<number>(props.bom.total)
    const [ canAddPaymentInstrumentItem, setCanAddPaymentInstrumentItem ] = useState<boolean>(false)
    const [ changeAmount, setChangeAmount ] = useState<number>(0)


    useEffect(() => { loadCustomerBillDocuments() }, [])

    useEffect(()=>{
        updateTotalInstrumentPayment()
    },[ paymentInstrumentsTypeAndItem,cashPaymentAmount ])

    useEffect(()=>{
        updateCanAddPaymentInstrumentTypeAndItem()
    },[ instrumentPaymentAmount ])

    function updateTotalInstrumentPayment(){
        setChangeAmount(0)
        const instruments: PaymentInstrumentItem[] = [...paymentInstrumentsTypeAndItem.map(itemAndType=>itemAndType.instrumentItem)]
        let totalInstrumentPayment = 0
        instruments.forEach(instrument => totalInstrumentPayment+=instrument.amount)
        if(!StringExtension.isNullOrEmpty(cashPaymentAmount) && !isNaN(+cashPaymentAmount)){
            const remainingAmount = totalPaymentAmount - totalInstrumentPayment
            const change = (+cashPaymentAmount) - remainingAmount
            totalInstrumentPayment+=remainingAmount
            setChangeAmount(change)
        }
        setInstrumentPaymentAmount(totalInstrumentPayment)
    }

    function updateCanAddPaymentInstrumentTypeAndItem(){
        if(instrumentPaymentAmount > totalPaymentAmount) {
            setCanAddPaymentInstrumentItem(false)
            return
        }
        if(totalPaymentAmount > 0 ){
            const amount = totalPaymentAmount - instrumentPaymentAmount
            if(amount < 0) {
                setCanAddPaymentInstrumentItem(false)
                return
            }
        }
        setCanAddPaymentInstrumentItem(true)
    }

    async function loadCustomerBillDocuments() {
        try {
            setCashState(loadingCashForm)
            const accountDocPars: GetAccountDocumentPar = {
                sessionId: auth.sessionId,
                documentID: props.bom.billOfMaterial.invoiceNo,
                fullData: true
            }
            const accunt_document: AccountDocument = (await accounting_service.GetAccountDocument(accountDocPars)).data
            const account_documents: CustomerBillDocument[] = [ accunt_document as CustomerBillDocument  ]

            let customer_bill_items: BillItem[] = []
            for(const acc_doc of account_documents){
                let bill_items: BillItem[] = await getCustomerBillItems(acc_doc.accountDocumentID)
                if(bill_items && bill_items.length > 0)
                    customer_bill_items = [...customer_bill_items,...bill_items]
            }
            const current_payment_center: PaymentCenter = (await subscriber_mgr_service.GetCurrentPaymentCenter({sessionId: auth.sessionId})).data
            const receiptTitleParams: GetSystemParametersWebPar = {
                sessionId: auth.sessionId,
                names: ["receiptTitle"]
            }
            const receipt_title: string = (await subscriber_mgr_service.GetSystemParametersWeb(receiptTitleParams)).data
            const cashInstrumentCodeParams: GetSystemParametersWebPar = {
                sessionId: auth.sessionId,
                names: ["cashInstrumentCode"]
            }
            const cash_instrument_code: string = (await bn_finance_service.GetSystemParametersWeb(cashInstrumentCodeParams)).data.toString()

            const cashForm = {
                ...successCashForm,
                account_documents,
                current_payment_center,
                receipt_title,
                cash_instrument_code,
                customer_bill_items
            }
            setCashState(cashForm)
        } catch (e) {
            setCashState(errorCashForm)
        }
    }
    async function handleAddPaymentInstrument():Promise<void>{
        try{
            if(!activePaymentInstrumentType)
                throw new Error('Select payment instrument type')
            const tempPaymentInstrumentItem = { ...paymentInstrumentItem}
            if(activePaymentInstrumentType.isTransferFromAccount){
                if(tempPaymentInstrumentItem.bankBranchID === 0)
                    throw new Error('Select bank and branch')
                if(tempPaymentInstrumentItem.accountNumber.trim() === '')
                    throw new Error('Please Enter account number')
            }
            else{
                tempPaymentInstrumentItem.bankBranchID = -1
                tempPaymentInstrumentItem.accountNumber =''
            }
            if(activePaymentInstrumentType.isBankDocument && !activePaymentInstrumentType.isToken){
                if(tempPaymentInstrumentItem.despositToBankAccountID === -1)
                    throw new Error('Select deposit bank account')
            }

            if(tempPaymentInstrumentItem.documentReference.trim() === '')
                throw new Error('Please enter reference number')

            if(new Date() < new Date(tempPaymentInstrumentItem.documentDate))
                throw new Error('Future dated document is not valid')

            if(isNaN(tempPaymentInstrumentItem.amount) || tempPaymentInstrumentItem.amount<= 0)
                throw new Error('Please enter valid amount')

            if(tempPaymentInstrumentItem.amount > (totalPaymentAmount-instrumentPaymentAmount))
                throw new Error(`Please enter amount less than or equal to ${parseNumberToMoney((totalPaymentAmount-instrumentPaymentAmount))}`)


            setPaymentInstrumentsTypeAndItem([
                ...paymentInstrumentsTypeAndItem,
                {
                    instrumentItem: tempPaymentInstrumentItem,
                    instrumentType: activePaymentInstrumentType
                }]
            )
            setPaymentInstrumentItem(new PaymentInstrumentItem())
            setActivePaymentInstrumentType(undefined)
        } catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Invalid Input',
                description:errorMessage(error)
            })
        }
    }
    const handlePaymentInstrumentItemValidation=():string[] | undefined => {
        const validationParams: PaymentInstrumentItemValidationParams = {
            amount:(totalPaymentAmount-instrumentPaymentAmount),
            paymentInstrumentType: activePaymentInstrumentType,
            paymentInstrumentItem
        }
        return PaymentInstrumentItemExtension.validate(validationParams)
    }

    function handleActivePaymentInstrumentChange(newPaymentInstrumentItem: PaymentInstrumentItem){ setPaymentInstrumentItem(newPaymentInstrumentItem) }
    function handleActivePaymentInstrumentTypeChange(newPaymentInstrumentType?: PaymentInstrumentType) { setActivePaymentInstrumentType(newPaymentInstrumentType) }

    function handleRemovePaymentInstrument(index: number) {
        const tempItemAndType = [...paymentInstrumentsTypeAndItem]
        tempItemAndType.splice(index,1)
        setPaymentInstrumentsTypeAndItem([...tempItemAndType])
    }


    async function handlePrintReceipt() {
        try{

            openNotification({
                message:'Loading',
                description:'Finishing transaction',
                notificationType:NotificationTypes.LOADING
            })
            const printParams = new PrintReceiptParameters()
            let paymentReceipt = new CustomerPaymentReceipt()
            let paymentInstrumentItems: PaymentInstrumentItem[] = []
            let tempInstrumentPayment = 0
            paymentInstrumentsTypeAndItem.forEach(itm=>{
                tempInstrumentPayment += itm.instrumentItem.amount
                paymentInstrumentItems = [...paymentInstrumentItems,itm.instrumentItem]
            })
            if(isNaN(+cashPaymentAmount))
                throw new Error('Invalid input on cash amount ')
            if(!StringExtension.isNullOrEmpty(cashPaymentAmount) && !isNaN(+cashPaymentAmount)){
                const paymentInstrumentItem = new PaymentInstrumentItem()
                paymentInstrumentItem.instrumentTypeItemCode = cashState.cash_instrument_code
                paymentInstrumentItem.amount = totalPaymentAmount - tempInstrumentPayment
                if(paymentInstrumentItem.amount < 0)
                    throw new Error('Amount less than 0 is not valid')
                paymentInstrumentItems = [...paymentInstrumentItems, paymentInstrumentItem]
            }

            if (cashState.current_payment_center.centerName) {
                printParams.casheirName = cashState.current_payment_center.centerName || undefined
                printParams.mainTitle = cashState.receipt_title
            }
            const date = getLocalDateString()
            paymentReceipt = await payBill(date, paymentInstrumentItems, cashState.account_documents)

            //END
            //PRINT
            openNotification({
                message:'SUCCESS',
                description:'Transaction Complete',
                notificationType:NotificationTypes.SUCCESS
            })
            reloadJobBOMTable()
        } catch(e){
            openNotification({
                message:'ERRNO',
                description:errorMessage(e),
                notificationType:NotificationTypes.ERROR
            })
        }
    }
    function reloadJobBOMTable(){
        const jobBOMParams : GetJobBOMPar = {
            sessionId:auth.sessionId,
            jobID:props.job.id
        }
        dispatch(fetchJobBOM(jobBOMParams , props.job))
    }
    async function payBill(date: string, instruments: PaymentInstrumentItem[], bills: CustomerBillDocument[]):Promise<CustomerPaymentReceipt>{
        const payment = new CustomerPaymentReceipt()
        payment.assetAccountID = cashState.current_payment_center.casherAccountID
        let billItems: BillItem[] = []
        let totalBill = 0
        for(const bill of bills){
            if (payment.customer == null) {
                if (bill.customer) {
                    payment.customer = bill.customer as Subscriber
                }
            } else {
                if (bill.customer !== undefined) {
                    const customer = bill.customer as Subscriber
                    if (payment.customer.id != customer.id) {
                        throw new Error("Only bills of a single customer can be settled with a single receipt")
                    }
                }
            }

            //TODO: while this working fine customer bill items are being loaded already so use loaded data instead of fetching
            payment.settledBills = [...payment.settledBills,bill.accountDocumentID]
            let dbBillItem: BillItem[] = await getCustomerBillItems(bill.accountDocumentID)
            dbBillItem.forEach(item => {billItems = [...billItems, item] })
            dbBillItem.forEach(item => {totalBill += item.netPayment})
        }
        instruments.forEach(instrument => totalBill -= instrument.amount)
        payment.paymentInstruments = instruments
        payment.shortDescription = "Bill Settlment"
        payment.billItems = billItems
        const genericDocPar: PostGenericDocumentWebPar<CustomerPaymentReceipt> = {
            sessionId: auth.sessionId,
            doc: payment,
            dataType: NAME_SPACES.DOCUMENT_TYPE.CUSTOMER_PAYMENT_RECEIPT
        }
        const id = (await accounting_service.PostGenericDocumentWeb<CustomerPaymentReceipt>(genericDocPar)).data
        const accountDocPar: GetAccountDocumentPar = {
            sessionId: auth.sessionId,
            documentID: id,
            fullData: true
        }
        const ret: CustomerPaymentReceipt = (await accounting_service.GetAccountDocument(accountDocPar)).data
        ret.billItems = payment.billItems
        return ret;
    }
    async function getCustomerBillItems(billRecordID: number): Promise<BillItem[]> {
        const params: GetCustomerBillItemsPar = {
            sessionId: auth.sessionId,
            billRecordID
        }
        const billItems: BillItem[] = (await subscriber_mgr_service.GetCustomerBillItems(params)).data
        return billItems
    }

    function PaymentInstrumentCard(props: PaymentInstrumentTypeAndItem & { index:number }){
        const [ bankAccount, setBankAccount] = useState<BankAccountInfo>(new BankAccountInfo());
        const [ branchName, setBranchName ] = useState<string>('')
        useEffect(()=>{
            props.instrumentType.isToken &&
            props.instrumentItem.despositToBankAccountID!==-1 &&
            loadBankAccount()
            props.instrumentType.isTransferFromAccount &&
            loadBranchName()
        },[])
        async function loadBankAccount(){
            try{
                const bankAccountPar: GetBankAccountPar = {
                    sessionId: auth.sessionId,
                    mainAccountID: props.instrumentItem.despositToBankAccountID
                }
                const bankAccountInfo: BankAccountInfo = (await bn_finance_service.GetBankAccount(bankAccountPar)).data
                setBankAccount(bankAccountInfo);
            } catch(error){

            }
        }
        async function loadBranchName(){
            try{
                const branchParams: GetBankBranchInfoPar = {
                    sessionId:auth.sessionId,
                    branchID: props.instrumentItem.bankBranchID
                }
                const branchInfo: BankBranchInfo = ( await bn_finance_service.GetBankBranchInfo(branchParams) ).data
                const bankParams:GetBankInfoPar = {
                    sessionId:auth.sessionId,
                    bankID: branchInfo.bankID
                }
                const bankInfo: BankInfo = ( await bn_finance_service.GetBankInfo(bankParams) ).data
                setBranchName(`${bankInfo.name} - ${branchInfo.name}`)
            } catch (error) {

            }
        }
        return(
            <div style={{marginBottom:'5px',padding:'15px'}} className={'paper'}>
                <DataRow title={'Type'} content={props.instrumentType.name}/>
                <DataRow title={'Reference'} content={props.instrumentItem.documentReference}/>
                <DataRow title={'Bank'} content={props.instrumentItem.despositToBankAccountID ===-1? '' :bankAccount.bankAccountName}/>
                <DataRow title={'Branch Name'} content={branchName}/>
                <DataRow title={'Transfer From Account'} content={props.instrumentType.isTransferFromAccount?props.instrumentItem.accountNumber:''}/>
                <DataRow title={'Remark'} content={props.instrumentItem.remark}/>
                <DataRow title={'Amount'} content={parseNumberToMoney(props.instrumentItem.amount)}/>
                <CButton type={'danger'} onClick={()=>handleRemovePaymentInstrument(props.index)} > Remove Item </CButton>
            </div>
        )
    }

    return (
        <StateMachineHOC state={cashState}>
            <Form layout={formLayout}>
            <Form.Item label="Cash" {...formItemLayout}>
                <Input size={'large'} onChange={(e)=>setCashPaymentAmount(e.target.value)} value={cashPaymentAmount}/>
            </Form.Item>
            <Row>
                { paymentInstrumentsTypeAndItem.map((value,key)=>
                    <Col key={key} lg={{span:18,offset:6}}>
                        <PaymentInstrumentCard {...value} index={key}/>
                    </Col>
                )}
            </Row>
            <Form.Item label="Bill Amount" {...formItemLayout}>
                <InputGroup compact>
                    <Input size={'large'} value={props.bom.total} disabled style={{width: '70%'}}/>
                    <span style={{width: '30%'}}>
                        <CModal
                            block
                            withConfirmation
                            buttonText={'Add New'}
                            title={'Payment Instrument'}
                            modalProps={{ destroyOnClose:true}}
                            submitHandler={handleAddPaymentInstrument}
                            validationHandler={handlePaymentInstrumentItemValidation}
                            buttonProps={{type: 'primary',size:'large',disabled:(!canAddPaymentInstrumentItem || changeAmount > 0.01 )}}
                            >
                            <PaymentInstrumentFormContainer
                                availableCashAmount={(totalPaymentAmount - (instrumentPaymentAmount))}
                                paymentInstrumentChangeHandler={handleActivePaymentInstrumentChange}
                                paymentInstrumentTypeChangeHandler={handleActivePaymentInstrumentTypeChange}/>
                        </CModal>
                    </span>
                </InputGroup>
            </Form.Item>
            <Form.Item label="Change" {...formItemLayout}>
                <Input value={changeAmount> 0? parseNumberToMoney(changeAmount) : '' } size={'large'} disabled/>
            </Form.Item>

            <Row gutter={4}>
               <Col lg={{span:8,offset:16}}>
                   <CButton size={'large'} disabled={cashState.loading || cashState.error} onClick={handlePrintReceipt} block type={'primary'}>Print</CButton>
               </Col>
            </Row>
        </Form>
        </StateMachineHOC>
    )
}