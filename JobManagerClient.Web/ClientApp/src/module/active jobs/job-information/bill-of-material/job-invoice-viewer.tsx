import React, {Fragment, useEffect} from 'react';
import {IJobBOMPayload} from "../../../../_model/state-model/aj-sm/job-bom-state";
import {Divider} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {BomInvoicePreviewState} from "../../../../_model/state-model/aj-sm/bom-invoice-preview-state";
import {GetInvoicePreviewPar} from "../../../../_services/job.manager.service";
import {fetchBOMInvoicePreview} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {HTMLParser} from "../../../../shared/components/html-parser/html-parser";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {AppendJobStyleString} from "./job-style-string";

interface IProps{
    bom : IJobBOMPayload
}

export function JobInvoiceViewer(props : IProps){
    const dispatch = useDispatch()
    const [ auth,bom_state ] = useSelector< ApplicationState,[ AuthenticationState, BomInvoicePreviewState ] >(appState=>[ appState.auth, appState.bom_invoice_preview ])
    useEffect(()=>{
        const params:GetInvoicePreviewPar ={
            sessionId:auth.sessionId,
            bom:props.bom.billOfMaterial
        }
        dispatch(fetchBOMInvoicePreview(params))
    },[])

    return (
        <Fragment>
            <Divider orientation="left">Invoice</Divider>
            <StateMachineHOC state={bom_state}>
                <HTMLParser htmlString={AppendJobStyleString(bom_state.previewHtml)}/>
            </StateMachineHOC>
        </Fragment>
    )
}