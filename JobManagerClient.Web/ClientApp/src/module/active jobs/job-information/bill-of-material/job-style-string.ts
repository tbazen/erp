export const AppendJobStyleString = (htmlString:string): string =>(
`<div>
    <style>
    body{
        font: 14px Microsoft JhengHei UI;
        color: #323232;
    
    }
    .readingBar
    {
        background:orange;
    }
    .readingBarLabel
    {
        text-align:center
    }
    .readingBarValue
    {
        text-align:center
    }
    
    table
    {
        font: 12px Microsoft JhengHei UI;
        color: #323232;
        margin-top:5px;
        width:100%;
    }
    h1, h2, h3
    {
        font-family: calibri;
        color: #323232;
    }
    
    .bomList
    {
        width: 100%;
    }
    .LedgerGridHeadCell
    {
        background-color: #177bad;
        color: white;
        border-bottom: #e0e0e0 thin solid;
        border-left: #e0e0e0 thin solid;
        border-top: #e0e0e0 thin solid;
        border-right: #e0e0e0 thin solid;
        vertical-align: bottom;
        font: 15px Microsoft JhengHei UI;
        font-weight: bold;
    }
    .customerInfoValueStyle
    {
        font-size:12.5px;
    }
    .LedgeSubTotalHeadCell
    {
        background-color: gainsboro;
        color: black;
        border-bottom: #e0e0e0 thin solid;
        border-left: #e0e0e0 thin solid;
        border-top: #e0e0e0 thin solid;
        border-right: #e0e0e0 thin solid;
        vertical-align: bottom;
        font-weight: bold;
    }
    .LedgeDetailedItem
    {
        background-color: white;
        color: black;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
        font-style: italic;
        font-size: smaller;
    }
    .LedgeDetailedItemSmall
    {
        background-color: white;
        color: black;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
        font-style: italic;
        font-size:x-small;
    }
    
    .LedgerGridFooterCell
    {
        background-color: black;
        color: white;
        border-bottom: #e0e0e0 thin solid;
        border-left: #e0e0e0 thin solid;
        border-top: #e0e0e0 thin solid;
        border-right: #e0e0e0 thin solid;
        vertical-align: bottom;
    }
    .rLedgerGridFooterCell
    {
        background:#a9d6ec;
        text-align:right;
    }
    
    .LedgerGridOddRowCell
    {
        background:#def;
        border-bottom: 1px solid #cef;
    }
    .LedgerGridEvenRowCell
    {
        background-color: white;
        color: black;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
    }
    .BorderCell
    {
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
    }
    .CurrencyCell
    {
        text-align: right;
        font: 12px Microsoft JhengHei UI;
    }
    .rightAlignedCell
    {
        text-align:right
    }
    .rCurrencyCell
    {
        background:#177bad;
    }
    .BoldTextHeaderCell
    {
        background-color: #F0F0F0;
        font-weight: bold;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
    }
    .BoldTextFooterCell
    {
        font-weight: bold;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
    }
    TD
    {
        vertical-align: top;
        visibility: visible;
    }
    Table
    {
        border-collapse: collapse;
    }
    thead
    {
        display: table-header-group;
    }
    tr
    {
        page-break-inside: avoid;
    }
    .dataLabel1
    {
        font-weight: bold;
    }
    .dataValue1
    {
        text-decoration: underline;
    }
    .commandLink1
    {
        display: inline-block;
        height: 25px;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        margin-top: 10px;
        margin-right: 3px;
        border: 1px solid #2177A5;
        background-color: #227bad;
        color: #fff;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        text-decoration:none;
        width: 110px;
        text-align: center;
        text-decoration: none;
    }
    .commandLink1:hover, .commandLink2:hover
    {
        text-decoration: underline;
        color: #227bad;
        background: #ffffff;
    }
    .commandLink2
    {
        display: inline-block;
        height: 25px;
        font: normal 13px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10px;
        margin-right: 3px;
        border: 1px solid #2177A5;
        background-color: #227bad;
        color: #fff;
        cursor: pointer;
        width: 20%;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        border-radius: 10px;
        text-decoration: none;
        text-align: center;
        float: right;
    }
    .commandLinkJobProcess
    {
        display: inline-block;
        height: 30px;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        margin-top: 10px;
        margin-right: 3px;
        padding: 6px 10px;
        border: 1px solid #2177A5;
        background-color: #227bad;
        color: #fff;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        border-radius: 10px;
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3), inset 0 1px 0 #3CA5D2;
        text-decoration: none;
    }
    .commandLinkJobProcess:hover
    {
        display: inline-block;
        height: 30px;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        padding: 6px 10px;
        border: 1px solid #2177A5;
        background-color: #ffffff;
        color: #227bad;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        border-radius: 10px;
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3), inset 0 1px 0 #3CA5D2;
        text-decoration: underline;
    }
    .applicationType
    {
        font-size: larger;
        font-weight: bold;
    }
    .outerTable
    {
        background-color: #f3f3f3;
        width:100%;
    }
    .innerTable
    {
        width:100%;
    }
    .jobSection1
    {
        font-size: 18px;
        font-weight: bold;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    .customerProflieName
    {
        font-size: 18px;
        font-weight: bold;
        color: #FFFFA4;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
        background-color: #003366;
    }
    .customerProflieLabel1
    {
        font-size: 10px;
        font-weight: bold;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    .customerProflieValue1
    {
        font-size: 10px;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    .customerProflieLabel2
    {
        font-size: 10px;
        font-weight: bold;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    .customerProflieValue2
    {
        font-size: 10px;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    .customerProflieLabel3
    {
        font-size: 10px;
        font-weight: bold;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    .customerProflieValue3
    {
        font-size: 10px;
        color: #003366;
        font: normal 18px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        margin-top: 10%;
    }
    #jobInfo
    {
        margin-left: 2%;
        color: #636363;
        font: normal 12px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
    }
    #custInfo
    {
        margin-left: 2%;
        color: #636363;
        font: normal 12px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
    }
    #conInfo
    {
        margin-left: 2%;
        color: #636363;
        font: normal 12px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
    }
    .tableHeader1
    {
        font: normal 13px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
    }
    .tableHeader1
    {
        background-color: #342e2e;
        color: white;
        border-bottom: #e0e0e0 thin solid;
        border-left: #e0e0e0 thin solid;
        border-top: #e0e0e0 thin solid;
        border-right: #e0e0e0 thin solid;
        vertical-align: bottom;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        font-size: 14px;
    }
    .oddRow1
    {
        background-color: white;
        color: #636363;
        font: normal 12px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
    }
    .evenRow1
    {
        background-color: #F0F0F0;
        color: #636363;
        font: normal 12px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        border-bottom: black thin solid;
        border-left: black thin solid;
        border-top: black thin solid;
        border-right: black thin solid;
    }
    .commandLink3
    {
        color: #30C;
        font-weight: 500;
        font: normal 14px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        text-decoration: none;
        margin-left: 12px;
    }
    .commandLink3:hover
    {
        text-decoration: underline;
    }
    
    .customerProfileLink1
    {
        color: #30C;
        font-weight: 500;
        font: normal 14px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
        text-decoration: none;
        margin-left: 12px;
    }
    .customerProfileLink1:hover
    {
        text-decoration: underline;
    }
    .SimpleLabel
    {
        color: #636363;
        font: normal 14px/1.5em "Liberation sans" , Arial, Helvetica, sans-serif;
    }
    
    
    .commandLinkJobProcessRed
    {
        display: inline-block;
    
        height: 30px;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        margin-top: 10px;
        margin-right: 3px;
        padding: 6px 10px;
        border: 1px solid #2177A5;
        background-color: #FF6666;
        color: #3333FF;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        border-radius: 10px;
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3), inset 0 1px 0 #3CA5D2;
        text-decoration: none;
    }
    .commandLinkJobProcessRed:hover
    {
        display: inline-block;
        height: 30px;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        padding: 6px 10px;
        border: 1px solid #2177A5;
        background-color: #ffffff;
        color: #227bad;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        border-radius: 10px;
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3), inset 0 1px 0 #3CA5D2;
        text-decoration: underline;
    }
    
    .commandLinkJobProcessDarkBlue
    {
        height: 28px;
        font-family: Microsoft JhengHei UI;
        text-decoration: none;
        margin-top: 10px;
        margin-right: 3px;
        padding: 6px 10px;
        border: 1px solid #2177A5;
        background-color: #227bad;
        color: #fff;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3), inset 0 1px 0 #3CA5D2;
        display: inline-block;
        height: 30px;
        font-family: 'Lucida Grande' , 'Lucida Sans Unicode' , 'Lucida Sans' , Verdana, Sans-serif;
        padding: 6px 10px;
    }
    .commandLinkJobProcessDarkBlue:hover
    {
        border: 1px solid #2177A5;
        background-color: #24b2f9;
        color: #ffffff;
        cursor: pointer;
        text-shadow: 0 -1px 0 rgba(0,0,0,0.5);
        border-radius: 10px;
        box-shadow: 0px 1px 2px rgba(0,0,0,0.3), inset 0 1px 0 #3CA5D2;
        text-decoration: underline;
    }
    .customerDb
    {
        padding: 32px 40px;
    }
    
    .piSubTotalLabel
    {
        text-align: right;
        border-width: medium;
        border-top-style: solid;
        font-weight:bold;
    }
    .piSubTotalValue
    {
        text-align: right;
        border-width: medium;
        border-top-style: solid;
        text-decoration:underline
    }
    
    .piGrandTotalLabel
    {
        font-weight: bold;
        font-size: medium;
        font-weight: bold;
        border-top-style: solid;
        font-weight:bold;
    
    }
    .piGrandTotalValue
    {
        text-decoration: underline;
        text-align: right;
        font-weight: bold;
        border-top-style: solid;
        font-weight:bold;
    }
    .piSummaryLabel
    {
        font-weight: bold;
    }
    .piSummaryValue
    {
        text-decoration: underline;
        text-align: right;
    }
    .piNumberCell
    {
        text-align:right;
    }
    .piTextCell
    {
    }
    .billPanel
    {
        border-style: solid;
        border-color: #ffffff;
        background: white;
        margin: 5px;
        border-top-left-radius:5px;
        border-top-right-radius:5px;
    }
    .billPanelHeader
    {
        text-align: center;
        background: #177bad;
        color: White;
        font-size: larger;
        font-weight: bold;
        padding-top:10px;
        padding-bottom:10px;
    
    }
    
    .rInfoLedgerGridHeadCell{
        padding: 9px 10px;
        background: #177bad;
        color:white;
        font-weight: bold;
        font: 15px Microsoft JhengHei UI;
        width:75px;
    }
    .rInfoLedgerGridOddRowCell
    
    {
        background:#def;
        border-bottom: 1px solid #cef;
    }
    td
    {
        padding: 3px 10px;
    }
    .LedgerGridHeadCell .CurrencyCell
    {
        color:#ffffff;
        font-size:15px;
        font-weight:bold;
    }
    strong
    {
        font-size:14px;
    }
    .cpHeaderCell
    {
        background-color: #177bad;
        color: white;
        vertical-align: bottom;
        font: 15px Microsoft JhengHei UI;
        font-weight: bold;
        text-align:left;
    }
    .cpHeaderCellCurrency
    {
        background-color: #177bad;
        color: white;
        vertical-align: bottom;
        font: 15px Microsoft JhengHei UI;
        font-weight: bold;
        text-align:right;
    }
    
    .cpBodyCell
    {
        background:#def;
        border-bottom: 1px solid #cef;
    }
    </style>
     ${htmlString} 
</div>`
)