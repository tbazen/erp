import React, {useEffect, useState} from 'react'
import {DatePicker, Form, Input, Select} from "antd";
import TextArea from "antd/lib/input/TextArea";
import BNFinanceService, {GetAllBranchsOfBankPar} from "../../../../_services/bn.finance.service";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {useSelector} from "react-redux";
import {
    BankBranchInfo,
    BankInfo, PaymentInstrumentItem,
    PaymentInstrumentType
} from "../../../../_model/level0/iERP-transaction-model/payment-instrument-item";
import {BankAccountInfo} from "../../../../_model/level0/iERP-transaction-model/bank-account-info";
import {horizontalFormItemLayout} from "../../../../shared/core/item-layout/item-layout";
import {MomentUtil} from "../../../../_helpers/date-util";
const { Option } = Select

const ierp_transaction_service = new BNFinanceService()
interface PaymentInstrumentFormContainerProps{
    availableCashAmount: number
    paymentInstrumentChangeHandler?: (instrumentItem : PaymentInstrumentItem)=>void
    paymentInstrumentTypeChangeHandler?: (instrumentType? : PaymentInstrumentType)=>void
}
export function PaymentInstrumentFormContainer(props:PaymentInstrumentFormContainerProps){
    const formLayout = 'horizontal';
    const formItemLayout = horizontalFormItemLayout
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=>[ appState.auth ])
    const [ paymentInstrumentTypes , setPaymentInstrumentTypes ] = useState<PaymentInstrumentType[]>([])
    const [ banks , setBanks ] = useState<BankInfo[]>([])
    const [ bankBranches,setBankBranches ] = useState<BankBranchInfo[]>([])
    const [ depositBanks , setDepositBanks ] = useState<BankAccountInfo[]>([])
    const [ selectedBank, setSelectedBank ] = useState<BankInfo>()
    const [ paymentInstrument , setPaymentInstrument ] = useState<PaymentInstrumentItem>({...new PaymentInstrumentItem(),documentDate:MomentUtil.getCurrentDateTimeString(),amount:props.availableCashAmount})


    /**
     * UI related states
     */
    const [ selectedInstrumentType, setSelectedInstrumentType ] = useState<PaymentInstrumentType>()

    useEffect(()=>{ fetchPaymentInstrumentTypes() },[])
    useEffect(()=>{
        paymentInstrument &&
        props.paymentInstrumentChangeHandler &&
        props.paymentInstrumentChangeHandler(paymentInstrument)
    },[ paymentInstrument ])


    useEffect(()=>{
        if(paymentInstrument.instrumentTypeItemCode){
            const selectedPaymentInstrumentType = paymentInstrumentTypes.find(instrumnetType => instrumnetType.itemCode === paymentInstrument.instrumentTypeItemCode)
            selectedPaymentInstrumentType && setSelectedInstrumentType(selectedPaymentInstrumentType)
        }
    },[ paymentInstrument.instrumentTypeItemCode ])

    useEffect(()=>{
        props.paymentInstrumentTypeChangeHandler &&
        props.paymentInstrumentTypeChangeHandler(selectedInstrumentType)
    },[ selectedInstrumentType ])


    async function fetchPaymentInstrumentTypes(){
        try{
            const paymentInstrumentTypes: PaymentInstrumentType[] = (await ierp_transaction_service.GetAllPaymentInstrumentTypes( { sessionId:auth.sessionId })  ).data
            const banks: BankInfo[] = ( await ierp_transaction_service.GetAllBanks({ sessionId:auth.sessionId }) ).data
            const depositBanks:BankAccountInfo[] = ( await ierp_transaction_service.GetAllBankAccounts({ sessionId:auth.sessionId }) ).data
            const unlistedBank = new BankInfo()
            unlistedBank.id = -1;
            unlistedBank.name= "Unlisted";
            setBanks([...banks,unlistedBank])
            setDepositBanks(depositBanks)
            setPaymentInstrumentTypes(paymentInstrumentTypes)
        } catch (e) {

        }
    }
    async function fetchBranchesOfBank(bankID:number){
        try{
            const unlistedBranch = new BankBranchInfo()
            unlistedBranch.id= -1;
            unlistedBranch.name = "Unlisted";
            const bankBranchPar : GetAllBranchsOfBankPar ={
                sessionId:auth.sessionId,
                bankID
            }
            const bankbranches : BankBranchInfo[] = (await ierp_transaction_service.GetAllBranchsOfBank(bankBranchPar)).data
            setBankBranches([...bankbranches,unlistedBranch])
        }
        catch(e){

        }
    }
    function handleSelectedBank(bankId:number){
        const bank = banks.find(bn=>bn.id === bankId)
        if(bank){
            setSelectedBank(bank)
            fetchBranchesOfBank(bank.id)
        }
    }
    function handleSelectedDepositBankChange(bankAccountNumber:string ){
    }
    function handleSelectedBankBranchChange(id: number){
    }
    return (
        <Form layout={formLayout}>
            <Form.Item label="Type" {...formItemLayout}>
                <Select value={paymentInstrument.instrumentTypeItemCode} onChange={(itemCode:string)=>setPaymentInstrument({...paymentInstrument,instrumentTypeItemCode:itemCode})}>
                    {paymentInstrumentTypes.map((value,key)=><Option key={key} value={value.itemCode}>{ value.name }</Option>) }
                </Select>
            </Form.Item>
            <Form.Item label="Source Bank" {...formItemLayout}>
                <Select disabled={!(selectedInstrumentType && selectedInstrumentType.isTransferFromAccount)} value={selectedBank && selectedBank.id} onChange={handleSelectedBank} >
                    { banks.map((value,key)=><Option key={key} value={value.id}>{ value.name }</Option>) }
                </Select>
            </Form.Item>
            <Form.Item  label="Source Branch" {...formItemLayout}>
                <Select disabled={!(selectedInstrumentType && selectedInstrumentType.isTransferFromAccount)} onChange={(branchId: number)=>setPaymentInstrument({...paymentInstrument,bankBranchID:branchId})} >
                    { bankBranches.map((value,key)=><Option key={key} value={value.id}> { value.name }</Option>) }
                </Select>
            </Form.Item>
            <Form.Item label="Source Bank Account" {...formItemLayout}>
                <Input
                    disabled={!(selectedInstrumentType && selectedInstrumentType.isTransferFromAccount)}
                    value={paymentInstrument.accountNumber}
                   onChange={(e)=>setPaymentInstrument({...paymentInstrument,accountNumber:e.target.value})} />
            </Form.Item>
            <Form.Item label="Deposit Bank Account" {...formItemLayout}>
                <Select
                    disabled={!(selectedInstrumentType && selectedInstrumentType.isBankDocument && !selectedInstrumentType.isToken)}
                    onChange={(depositAccountId:number)=>setPaymentInstrument({...paymentInstrument,despositToBankAccountID:depositAccountId})}>
                    { depositBanks.map((value,key)=><Option key={key} value={value.bankAccountNumber}>{ value.bankBranchAccount }</Option>) }
                </Select>
            </Form.Item>
            <Form.Item label="Reference" {...formItemLayout}>
                <Input
                    value={paymentInstrument.documentReference}
                    onChange={(event)=>setPaymentInstrument({...paymentInstrument,documentReference:event.target.value})}
                    />
            </Form.Item>
            <Form.Item label="Date" {...formItemLayout}>
                <DatePicker
                    defaultValue={MomentUtil.getCurrentDateTime()}
                    onChange={(moment,dateString)=>setPaymentInstrument({...paymentInstrument,documentDate: dateString}) }
                    style={{width:'100%'}}
                      />
            </Form.Item>
            <Form.Item label="Amount" {...formItemLayout}>
                <Input
                    value={paymentInstrument && paymentInstrument.amount}
                    onChange={
                        (event)=>setPaymentInstrument({...paymentInstrument,amount:+event.target.value})} />
            </Form.Item>
            <Form.Item label="Remark" {...formItemLayout}>
                <TextArea
                    rows={4}
                    value={paymentInstrument && paymentInstrument.remark}
                    onChange={( event )=>setPaymentInstrument({...paymentInstrument,remark:event.target.value})}
                />
            </Form.Item>
        </Form>
    )
}