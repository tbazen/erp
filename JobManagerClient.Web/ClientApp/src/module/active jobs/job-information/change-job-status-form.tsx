import React, {useEffect, useState} from "react";
import CFormItem from "../../../shared/core/cform-item/cform-item";
import TextArea from "antd/lib/input/TextArea";
import {ChangeJobStatusPar} from "../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {getLocalDateString} from "../../../_helpers/date-util";
import CModal from "../../../shared/core/cmodal/cmodal";
import {changeJobStatus} from "../../../_redux_setup/actions/aj-actions/active-job-actions";


interface IProps {
    newStatus : number
    jobId : number
    statusText : string
    block?: boolean
}
export function ChangeJobStatusForm(props:IProps){
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState => [appState.auth]);
    const dispatch = useDispatch()
    const initChangeStatus : ChangeJobStatusPar = {
        sessionId:auth.sessionId,
        date:getLocalDateString(),
        newStatus: props.newStatus,
        jobID:props.jobId,
        node:''
    }
    const [ changeStatus, setChangeStatus ] = useState<ChangeJobStatusPar>(initChangeStatus)

    useEffect(()=>{
        const changeStatus : ChangeJobStatusPar = {
            sessionId:auth.sessionId,
            date:getLocalDateString(),
            newStatus: props.newStatus,
            jobID:props.jobId,
            node:''
        }
        setChangeStatus(changeStatus)
    },[ props.newStatus ])


    function  submitNewStatus() {
        dispatch(changeJobStatus(changeStatus))
    }
    return (
        <CModal
            buttonText={props.statusText}
            title={props.statusText}
            block={props.block}
            withConfirmation
            modalProps={{destroyOnClose:true}}
            submitHandler={submitNewStatus}
        >
            <div>
                <CFormItem
                    input={<TextArea rows={5} onChange={(event)=>setChangeStatus({...changeStatus,node:event.target.value})} />}
                    label={'Remark'}
                    required
                    errorMessage={'Type State change remark here...'}
                />
            </div>
        </CModal>
    )
}
