import React, {useEffect, useState} from 'react';
import {RemoveJobPar} from '../../../_services/job.manager.service';
import CButton from '../../../shared/core/cbutton/cbutton';
import {StandardJobStatus, StandardJobStatusTypes} from '../../../_model/job-rule-model/standard-job-status';
import {Row  , Col, Popconfirm} from "antd";
import {Link} from "react-router-dom";
import {ROUTES} from "../../../_constants/routes";
import PreviousJobStatePicker from "./previous-state-selector";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {ChangeJobStatusForm} from "./change-job-status-form";
import {removeJob} from "../../../_redux_setup/actions/aj-actions/active-job-actions";
import {IActiveJobDetailPayload} from "../../../_model/state-model/aj-sm/active-job-detail-information-state";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {openDrawerModal} from "../../../_redux_setup/actions/drawer-modal-actions";
import {BOMEditor, EditableFields} from "./bill-of-material/bom-editor";

interface IProps {
    nextStatus : number[];
    jobInformation : IActiveJobDetailPayload;
}

export function JobPossibleNextState(props : IProps){
    const dispatch = useDispatch();
    const [ isEditable , setIsEditable ] = useState<boolean>(true)
    const [ isAddBillOfQuantityEnabled , setIsBillOfQuantityEnabled ] = useState<boolean>(
        (props.jobInformation.status.id == StandardJobStatus.SURVEY)
        || (props.jobInformation.status.id == StandardJobStatus.APPLICATION && props.jobInformation.job.applicationType == StandardJobTypes.GENERAL_SELL)
    )
    const [ isDeletable , setIsDeletable ] = useState<boolean>(props.jobInformation.status.id === StandardJobStatus.APPLICATION)
    const [ auth ] = useSelector<ApplicationState,[AuthenticationState]>(appState => [appState.auth])

    useEffect(()=>{
        setIsDeletable(props.jobInformation.status.id === StandardJobStatus.APPLICATION)
        setIsBillOfQuantityEnabled( (props.jobInformation.status.id == StandardJobStatus.SURVEY)
            || (props.jobInformation.status.id == StandardJobStatus.APPLICATION && props.jobInformation.job.applicationType == StandardJobTypes.GENERAL_SELL)
        )
    },[props.jobInformation.status])

    function handleDeleteJob(){
        const params : RemoveJobPar = { sessionId:auth.sessionId , jobHandle:props.jobInformation.job.id}
        dispatch(removeJob(params));
    }
    return (
        <Row gutter={4}>
            <Col lg={{span:4}}><PreviousJobStatePicker previousStates={props.jobInformation.previousStatus}/></Col>
            {
                props.nextStatus.map( (value,key)=>
                    <Col key={key} lg={{span:4}}>
                        <ChangeJobStatusForm
                            jobId={props.jobInformation.job.id}
                            statusText={StandardJobStatusTypes[value].commandName}
                            newStatus={value}
                            block
                        />
                    </Col>)
            }
            {
                isEditable &&
                <Col lg={{span:4}}><Link to={`${ROUTES.ACTIVE_JOB.EDIT[props.jobInformation.job.applicationType].INDEX}/${props.jobInformation.job.id}`}> <CButton block>Edit</CButton></Link></Col>
            }
            {
                isAddBillOfQuantityEnabled &&
                <Col lg={{span:4}}>
                    <CButton
                        onClick={()=>dispatch(openDrawerModal(
                        <BOMEditor
                            editableFields={props.jobInformation.job.status === StandardJobStatus.ANALYSIS ? EditableFields.ReferencePrice : EditableFields.ItemQuantity}
                            job={props.jobInformation.job}/>))} block>Add Bill Of Quantity</CButton>
                </Col>
            }
            {
                isDeletable &&
                <Col lg={{span:4}}>
                    <Popconfirm
                            title="Are you sure you want to delete job ?"
                            onConfirm={handleDeleteJob}
                            okText="Yes"
                            cancelText="No"
                        >
                        <CButton type={'danger'} block>Delete Job</CButton>
                    </Popconfirm>
                </Col>
            }
        </Row>
    )
}