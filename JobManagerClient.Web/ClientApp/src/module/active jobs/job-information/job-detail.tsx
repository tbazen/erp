import React, {Fragment} from "react";
import {Col, Divider, Popconfirm, Row, Typography} from "antd";
import {AppointmentForm} from "./appointment-form";
import {IActiveJobDetailPayload} from "../../../_model/state-model/aj-sm/active-job-detail-information-state";
import CButton from "../../../shared/core/cbutton/cbutton";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {useDispatch, useSelector} from "react-redux";
import {closeActiveJobAppointment} from "../../../_redux_setup/actions/aj-actions/active-job-actions";
import {DataRow} from "../../../shared/components/data-row/data-row";
const {Text, Paragraph} = Typography;


interface IProps{
    jobInformation : IActiveJobDetailPayload
}
function JobDetail(props : IProps){
    const dispatch = useDispatch()
    const [ auth ] = useSelector<ApplicationState,[AuthenticationState] >( appState=> [appState.auth] )
    async function handleCloseAppointment(){
        const appointment = props.jobInformation.appointments[ props.jobInformation.appointments.length-1 ]
        dispatch(closeActiveJobAppointment({sessionId:auth.sessionId,appointmentID:appointment.id},props.jobInformation.job.id));
    }
    return (
        <Fragment>
            <DataRow title={"Job Number "} content={`${props.jobInformation.job.jobNo}`}/>
            <DataRow title={"Job Description "} content={`${props.jobInformation.job.description}`}/>
            <DataRow title={"Job Status "} content={props.jobInformation.status.name}/>
            <Divider orientation="left">Appointment</Divider>
            {
                props.jobInformation.appointments.length > 0 &&
                props.jobInformation.appointments[props.jobInformation.appointments.length-1].active &&
                <Fragment>
                    <Text strong>
                        {`${ new Date(props.jobInformation.appointments[props.jobInformation.appointments.length-1].appointmentDate).toLocaleString()}`}
                    </Text>
                    <Paragraph>{props.jobInformation.appointments[props.jobInformation.appointments.length-1].note}</Paragraph>
                    <Row gutter={4}>
                        <Col span={10}>
                            <Popconfirm onConfirm={handleCloseAppointment} title="Are you sure! you want to close appointment?" okText="Yes" cancelText="No">
                                <CButton type={'danger'}>close appointment</CButton>
                            </Popconfirm>
                        </Col>
                        <Col span={10}>
                            <AppointmentForm
                                jobId={props.jobInformation.job.id+''}
                                appointment={props.jobInformation.appointments[props.jobInformation.appointments.length-1]}/>
                        </Col>
                    </Row>
                </Fragment>
            }
            {
                !((props.jobInformation.appointments.length > 0) && props.jobInformation.appointments[props.jobInformation.appointments.length-1].active) &&
                <Fragment>
                    <Text> There is no appointment for this job yet </Text>
                    <AppointmentForm jobId={props.jobInformation.job.id+''}/>
                </Fragment>
            }
        </Fragment>
    )
}
export default JobDetail