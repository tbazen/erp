import React,{ Fragment } from 'react';
import CModal from "../../../shared/core/cmodal/cmodal";
import {Col, Row} from "antd";
import { StandardJobStatusTypes} from "../../../_model/job-rule-model/standard-job-status";
import {JobStatusHistory} from "../../../_model/view_model/active-jobs-vm/job-status-history";
import {ChangeJobStatusForm} from "./change-job-status-form";

interface IProps{ previousStates : JobStatusHistory[] }

function PreviousJobStatePicker(props : IProps){
    return (
        <CModal title={'Select Previous State'}  block={true} buttonText={'Go To Previous State'}>
            <Fragment>
            {
                props.previousStates.map( ( value,key ) =>
                    <Row key={key} style={{marginTop:'5px'}}>
                        <Col span={16} offset={4}>
                                <ChangeJobStatusForm
                                    jobId={+value.jobID}
                                    newStatus={value.newStatus}
                                    statusText={StandardJobStatusTypes[value.newStatus].commandName}
                                    block
                                />
                        </Col>
                    </Row>)
            }
            </Fragment>
        </CModal>
    )
}

export default PreviousJobStatePicker