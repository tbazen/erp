import React, {useEffect, useState} from 'react';
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {JobRuleStateModel} from "../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {IClientHandler} from "../../../_model/job-rule-model/client-handler";
import {JobData} from "../../../_services/job.manager.service";

interface IProps{
    job : JobData
}

export function WorkflowDataContainer(props: IProps) {
    const [ clientHandlers ] = useSelector<ApplicationState,[ JobRuleStateModel]>(appState => [ appState.job_rule ])
    const [ clientHandler , setClientHandler ] = useState<IClientHandler>()
    useEffect(()=>{
        !clientHandlers.loading &&
        !clientHandlers.error &&
        updateClientHandler()
    },[clientHandlers])
    const updateClientHandler = () => {
        const handler = clientHandlers.clientHandlers.find(hdlr => hdlr.meta.jobType === props.job.applicationType)
        setClientHandler(handler);
    }
    return(
        <div> { clientHandler &&  clientHandler.handler.getWorkFlowData(props.job) } </div>
    )
}