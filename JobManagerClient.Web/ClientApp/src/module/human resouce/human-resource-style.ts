import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const initHRStyle = makeStyles((theme: Theme) =>
    createStyles({
        input: {
            display: 'none'
        },
        iconHover: {
            margin: theme.spacing(2),
            '&:hover': {
                color: 'red',
                backgroundColor:'black'
            },
        },
        button: {
            margin: theme.spacing(1),
            borderRadius:'2px',
            overflow:'hidden',
            paddingLeft:'20px',
            paddingRight:'20px'
        },
        firstButton: {
            marginRight: theme.spacing(1),
            borderRadius:'2px',
            paddingLeft:'15px',
            paddingRight:'15px'
        },
        paper:{
            borderRadius:'1px',
            padding:'10px'
        }
    }),
);
