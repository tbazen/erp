import React, { useEffect, useState} from 'react'
import {
    Button,
    Grid
} from "@material-ui/core"
import {initHRStyle} from "./human-resource-style"
import EmployeesearchParamFrm from "./search param form/empl-search-param-form"
import {useDispatch, useSelector} from "react-redux"
import {fetchOrgUnits} from "../../_redux_setup/actions/hr-actions/organization-actions"
import {ApplicationState} from "../../_model/state-model/application-state"
import MaterialTable from "material-table"
import {Close, Delete, Edit, Transform} from "@material-ui/icons";
import tableIcons from "../../shared/icons/table-icons";
import EmployeePicker from "../../shared/components/employee picker/employee-picker";
import {Employee} from "../../_model/view_model/IEmployee";
import {EmployeeSearchType, SearchEmployeePar} from "../../_model/view_model/employee-search-par";
import getCurrentTicks from "../../_helpers/date-util";

export default function HumanResource(){
    const classes = initHRStyle()
    const dispatch = useDispatch()
    const authState = useSelector( (appState:ApplicationState)=>appState.auth)
    const tableData = useSelector((appState:ApplicationState)=>appState.employeeSearch)
    const [openTransfer,setOpenTransfer] = useState(false)
    const initSelectedEmpl:Employee[] =[]
    const [selectedEmployee,setSelectedEmployee] = useState(initSelectedEmpl)


    const initSrchPrm: SearchEmployeePar  = {
        sessionID: authState.sessionId,
        ticks: getCurrentTicks(),
        orgUnitID: 0,
        query: '',
        includeDismissed: false,
        searchType: EmployeeSearchType.All,
        index: 0,
        pageSize: 30,
    }
    const [searchParams,setSearchParams] = useState(initSrchPrm)
    useEffect(()=>{
        dispatch(fetchOrgUnits({sessionID:authState.sessionId,PID:-1}))
    },[])

    const tableColumns = [
        {title:"ID",field:'employeeID'},
        { title: "Name", field: "employeeName" ,type:"numeric"},
        { title: "Gender", field: "sex",lookup: { 0: 'Male', 1: 'Female' ,2:'None'} },
        { title: "TIN", field: "tin" },
        { title: "Salary", field: "grossSalary" ,type:"numeric"},
        { title: "Status", field: "status" ,lookup: { 0: 'Pending', 1: 'Enrolled' ,2:'Fired'}},

    ]
    return (
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <div className={'paper'} style={{padding:'10px'}}>
                            <Grid container spacing={2}>
                                <EmployeesearchParamFrm searchParams={searchParams} setSearchParams={setSearchParams}/>
                                <Grid item xs={12}>
                                <input
                                    accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                    className={classes.input}
                                    id="contained-button-file"
                                    type="file"
                                />
                                <label  htmlFor="contained-button-file">
                                    <Button size="small" color={'primary'} variant={'outlined'} component="span" className={classes.firstButton}>
                                        Import
                                    </Button>
                                </label>
                                <Button className={classes.button} size="small" color={'primary'} variant={'outlined'}>
                                    New
                                </Button>
                                <Button disabled className={classes.button} size="small" color={'primary'} variant={'outlined'}>
                                    Enroll
                                </Button>
                            </Grid>
                            </Grid>
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <MaterialTable
                            icons={tableIcons}
                            options={{
                                selection: true,
                                pageSize:20,
                                search:false,
                                pageSizeOptions:[10,20],
                                selectionProps: () => ({
                                    color: 'primary'
                                }),
                                headerStyle: {
                                    backgroundColor: '#fff',
                                    color: '#000'
                                }
                            }}
                            data={tableData.payload != null ? tableData.payload._ret : []}
                            //@ts-ignore
                            columns={tableColumns}
                            actions={[
                                {
                                    icon:()=><Transform fontSize={'large'}/>,
                                    tooltip: 'Transfer',
                                    iconProps:{classes:{root:classes.iconHover}},
                                    onClick:(evt,data)=>{
                                        console.log('Data To Transfer ',data)
                                        setOpenTransfer(!openTransfer)
                                    }
                                },
                                {
                                    icon:()=><Edit fontSize={'large'}/>,
                                    tooltip: 'Edit',
                                    iconProps:{classes:{root:classes.iconHover}},
                                    onClick:(evt,data)=>alert('Edit')
                                },
                                {
                                    icon:()=><Close fontSize={'large'}/>,
                                    tooltip: 'Dismiss',
                                    iconProps:{classes:{root:classes.iconHover}},
                                    onClick:(evt,data)=>alert('Dismiss')
                                },
                                {
                                    tooltip: 'Delete',
                                    icon: ()=><Delete fontSize={'large'}/>,
                                    iconProps:{classes:{root:classes.iconHover}},
                                    onClick: (evt, data) => alert('You want to delete ' + data.length + ' rows')
                                }

                            ]}
                            onSelectionChange={(rows) =>setSelectedEmployee(rows)}
                            title="Employee List"
                        />
                    </Grid>
                    <Grid item>
                        <EmployeePicker searchParams={searchParams} setSearchParams={setSearchParams} open={openTransfer} setOpen={()=>setOpenTransfer(!openTransfer)} employees={selectedEmployee}/>
                    </Grid>
                </Grid>
            )
}