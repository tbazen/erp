import React, {Fragment, useEffect, useState} from 'react'
import {
    Checkbox,
    FormControl,
    FormControlLabel,
    FormLabel,
    Grid,
    TextField,
} from "@material-ui/core";
import {SearchEmployeePar} from "../../../_model/view_model/employee-search-par";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {requestEmployeeSearch} from "../../../_redux_setup/actions/hr-actions/employee-actions";
import  {getTickFromDate} from "../../../_helpers/date-util";
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {Radio} from "antd";

interface  IProps{
    searchParams:SearchEmployeePar
    setSearchParams:any
}
const radioStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
};
const EmployeesearchParamFrm = (props:IProps)=>{

    const authState = useSelector((appState:ApplicationState)=>appState.auth)
    const orgUnits = useSelector((appState:ApplicationState)=>appState.orgUnits)
    const dispatch = useDispatch()

    const [selectedDate, setSelectedDate] = React.useState<Date | null>(new Date(),)


    const [currentData, setCurrentData] = useState(true)

    const {searchParams,setSearchParams} = props

    const submitQuery = ()=>dispatch(requestEmployeeSearch(searchParams))

    useEffect(()=>{
        submitQuery()
    },[searchParams])

    function handleDateChange(date: Date | null) {
        setSelectedDate(date);
        if(date!=null){
            const ticks = getTickFromDate(date)
            setSearchParams({...searchParams,ticks})
        }
    }






    return(
        <Fragment>
            <Grid item md={3}>
                <FormControl component="fieldset">
                <FormLabel component="legend">Department</FormLabel>

                    <Radio.Group onChange = {(event:any)=>setSearchParams({...searchParams,orgUnitID:event.target.value})}>
                        {
                            orgUnits.payload !== null
                                ?
                                orgUnits.payload.map((value,key)=> <Radio key={key} style={radioStyle} value={value.id+''}>{value.name}</Radio>)
                                :
                                <Fragment/>
                        }
                    </Radio.Group>
            </FormControl>
            </Grid>

            <Grid item md={8}>
                <Grid container spacing={2} justify={'flex-start'}>
                    <Grid item md={12} xs={10}>
                        <TextField
                            value={searchParams.query}
                            onChange={(event)=>setSearchParams({...searchParams,query:event.target.value})}
                            fullWidth
                            placeholder={'Name / ID'}
                        />
                    </Grid>


                    <Grid item md={6} xs={10}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={searchParams.includeDismissed}
                                    value={searchParams.includeDismissed}
                                    onChange={()=>setSearchParams({...searchParams,includeDismissed:!searchParams.includeDismissed})}
                                    color="primary"
                                />
                            }
                            label="Show Dismissed Employee"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={currentData}
                                    value={currentData}
                                    onChange={()=>setCurrentData(!currentData)}
                                    color="primary"
                                />
                            }
                            label="Current Data"
                        />
                    </Grid>

                    <Grid item md={12}>
                        <Grid container justify={'flex-start'}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <Grid container  justify="flex-start">
                                    <KeyboardDatePicker
                                        margin="normal"
                                        hiddenLabel={'Date'}
                                        label="Start Date"
                                        disabled={currentData}
                                        value={selectedDate}
                                        onChange={handleDateChange}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />

                                </Grid>
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Fragment>
    )
}


export default EmployeesearchParamFrm