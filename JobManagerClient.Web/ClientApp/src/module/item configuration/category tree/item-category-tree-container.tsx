import React, {useState} from 'react'
import MaterialTable from "material-table";
import tableIcons from "../../../shared/icons/table-icons";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {ItemCategory} from "../../../_model/view_model/ic-vm/item-category";
import {fetchTransactionItemsInCategory} from "../../../_redux_setup/actions/ic-actions/item-configuration-actions";




export default function ItemCategoryTree() {
    const [selectedRowId,setSelectedRow] = useState(-1000)
    const dispatch = useDispatch()

    const tableData = useSelector((appState:ApplicationState)=>appState.item_category_state)
    const auth = useSelector((appState:ApplicationState)=>appState.auth)

    const tableColumns= [
        {title:'Code',field:'code'},
        {title:'Name',field:'description'},
        {title:'Unit',field:'nameCode'}
    ]
    // @ts-ignore
    return (
        <MaterialTable
            title="Item Category"
            icons={tableIcons}
            data={tableData.item_categories}
            //@ts-ignore
            columns={tableColumns}
            parentChildData={(row: any, rows: any) => rows.find((a: ItemCategory) => a.id === row.pid)}
            onRowClick={((evt, selectedRow) => {
                if(selectedRow!== undefined){
                    setSelectedRow(selectedRow.id)
                    dispatch(fetchTransactionItemsInCategory({sessionId:auth.sessionId,categID:selectedRow.id}))
                }
            })}
            options={{
                selection: false,
                pageSize:10,
                search:false,
                pageSizeOptions:[10,20],
                selectionProps: () => ({
                    color: 'primary'
                }),
                headerStyle: {
                    backgroundColor: '#fff',
                    color: '#000'
                },
                rowStyle: (focusedRow)=>({
                    backgroundColor: focusedRow.id==selectedRowId? 'lightgray' : 'inherit'
                })
            }}
        />
    )
}
