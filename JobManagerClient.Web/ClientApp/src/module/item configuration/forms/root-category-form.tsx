import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {initICStyle} from "../item-configuration-style";
import {Checkbox, Chip, FormControlLabel, FormGroup, Grid, TextField} from "@material-ui/core";
import BrowseTextField from "../../../shared/components/browse textfield/browse-textfield";
import {ItemCategoryType} from "../../../_enum/item-category-type";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {fetchParentAccountBase} from "../../../_redux_setup/actions/ic-actions/item-configuration-actions";
import CMUIButton from "../../../shared/components/custom button/custom-button";

interface IProps{
    title?:string
    chipLabel?:string
}

interface ICategoryAttribute{
    name:string
}


const expenseItemAttributes:ICategoryAttribute[]= [
    {
        name:'Expense Account'
    },
    {
        name:'Summerized Expense Account'
    },
    {
        name:'Direct Cost Account'
    },
    {
        name:'Summerized Direct Account'
    },
    {
        name:'Prepaid Expense Account'
    },
    {
        name:'Summerized Expense Account'
    }
]
const salesItemAttributes:ICategoryAttribute[]=[
    {
        name:'Income Account'
    },
    {
        name:'Summerized Income Account'
    },
    {
        name:'Unearned Revenue Account'
    },
    {
        name:'Summerized Unearned Revenue Account'
    },
    {
        name:'Finished Work Account'
    },
    {
        name:'Summerized Finished Work Account'
    }
]
const fixedItemAttributes:ICategoryAttribute[]=[
    {
        name:'Inventory Account'
    },
    {
        name:'Summerized inventory Account'
    },
    {
        name:'Finished Good Account'
    },
    {
        name:'Summerized Finished Good Account'
    },

    {
        name:'Orginal Fixed Asset Value Account'
    },
    {
        name:'Summerized Fixed Asset Value Account'
    },
    {
        name:'Depriciation Account'
    },
    {
        name:'Summerized Fixed Asset Depriciation Account'
    },
    {
        name:'Accumulated Depriciation Account'
    },
    {
        name:'Summerized Fixed Asset Accumulated Depreciation Account'
    },
    {
        name:'Pending Order Account'
    },
    {
        name:'Summerized Pending Order Account'
    },
    {
        name:'Pending Delivery Account'
    },
    {
        name:'Summerized Pending Delivery Account'
    }
]
const inventoryItemAttributes:ICategoryAttribute[]=[
    {
        name:'Inventory Account'
    },
    {
        name:'Summerized inventory Account'
    },
    {
        name:'Finished Good Account'
    },
    {
        name:'Summerized Finished Good Account'
    },
    {
        name:'Pending Order Account'
    },
    {
        name:'Summerized Pending Order Account'
    },
    {
        name:'Pending Delivery Account'
    },
    {
        name:'Summerized Pending Delivery Account'
    }
]

const initCheckedTypes = {expenseItem:true,salesItem:false,fixedItem:false,inventoryItem:false}

function RootCategoryForm(props:IProps) {
    const classes = initICStyle()
    const [open, setOpen] = useState(false);
    const [checkedTypes,setCheckedTypes ]= useState(initCheckedTypes)

    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)

    useEffect(()=>{
        if(open) dispatch(fetchParentAccountBase(auth.sessionId))
    }, [open])

    function handleClickOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <span>
            <Chip className={classes.chip} onClick={handleClickOpen} label={props.chipLabel}/>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth={'md'}>
                <DialogTitle id="form-dialog-title">{props.title}</DialogTitle>
                <DialogContent>
                    <FormGroup row>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checkedTypes.expenseItem}
                                    value={ItemCategoryType.EXPENSE_ITEM}
                                    color="primary"
                                    onChange={(event:any)=>setCheckedTypes({...checkedTypes,expenseItem:event.target.checked})}
                                />
                            }
                            label="Expense Item"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checkedTypes.salesItem}
                                    value={ItemCategoryType.SALES_ITEM}
                                    color="primary"
                                    onChange={(event:any)=>setCheckedTypes({...checkedTypes,salesItem:event.target.checked})}
                                />
                            }
                            label="Sales Item"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checkedTypes.fixedItem}
                                    value={ItemCategoryType.FIXED_ASSET_ITEM}
                                    color="primary"
                                    onChange={(event:any)=>setCheckedTypes({...checkedTypes,fixedItem:event.target.checked})}
                                />
                            }
                            label="Fixed Asset Item"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checkedTypes.inventoryItem}
                                    value={ItemCategoryType.INVENTORY_ITEM}
                                    color="primary"
                                    onChange={(event:any)=>setCheckedTypes({...checkedTypes,inventoryItem:event.target.checked})}
                                />
                            }
                            label="Inventory Item"
                        />
                    </FormGroup>

                    <TextField
                        autoFocus
                        label="Category Name"
                        type="text"
                        margin="dense"
                        fullWidth
                    />
                    <TextField
                        label="Category Code"
                        type="text"
                        margin="dense"
                        fullWidth
                    />
                    {
                        checkedTypes.expenseItem &&
                        expenseItemAttributes.map((value,key)=><BrowseTextField key={key} label={value.name} type="text"/>)
                    }
                    {
                        checkedTypes.salesItem &&
                        salesItemAttributes.map((value,key)=><BrowseTextField key={key} label={value.name} type="text"/>)
                    }
                    {
                        checkedTypes.fixedItem &&
                        fixedItemAttributes.map((value,key)=><BrowseTextField key={key} label={value.name} type="text"/>)
                    }
                    {
                        checkedTypes.inventoryItem &&
                        inventoryItemAttributes.map((value,key)=><BrowseTextField key={key} label={value.name} type="text"/>)
                    }
                </DialogContent>
                <DialogActions>
                    <CMUIButton onClick={handleClose} color="secondary" variant={'outlined'}>
                        Cancel
                    </CMUIButton>
                    <CMUIButton style={{marginRight:'2%'}} disabled={
                        !checkedTypes.inventoryItem &&
                        !checkedTypes.fixedItem &&
                        !checkedTypes.salesItem &&
                        !checkedTypes.expenseItem} onClick={handleClose} color="primary" variant={'outlined'}>
                        save
                    </CMUIButton>
                </DialogActions>
            </Dialog>
        </span>
    );
}

export default RootCategoryForm;
