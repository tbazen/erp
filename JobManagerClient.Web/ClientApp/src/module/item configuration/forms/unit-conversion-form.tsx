
import React, {Fragment} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {BottomNavigation, BottomNavigationAction, Chip} from "@material-ui/core";
import {Favorite, Restore} from "@material-ui/icons";
import {initICStyle} from "../item-configuration-style";



interface IProps{
    title?:string
    chipLabel?:string
}

function UnitConversionForm(props:IProps) {
    const classes = initICStyle()
    const [open, setOpen] = React.useState(false);
    const [value,setValue] = React.useState(0)
    function handleClickOpen() {
        setOpen(true);
    }
    function handleClose() {
        setOpen(false);
    }

    return (
        <span>
            <Chip className={classes.chip} onClick={handleClickOpen} label={props.chipLabel}/>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth={'sm'}>
                <DialogTitle id="form-dialog-title">Global Unit Converter</DialogTitle>
                <DialogContent>
                    {
                        value === 0 &&
                        <Fragment>
                            <p>List Of Units Will Be Here</p>
                        </Fragment>
                    }
                    {
                        value === 1 &&
                        <p>Unit Form Comes Here</p>
                    }
                </DialogContent>

                <DialogActions>
                    <BottomNavigation
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        style={{width:'96%',margin:'auto',backgroundColor:'lightgray'}}
                        showLabels
                    >

                      <BottomNavigationAction label="Add Item" icon={<Favorite />} />
                      <BottomNavigationAction label="New Measuring Unit" icon={<Restore />} />
                    </BottomNavigation>
                </DialogActions>
            </Dialog>
        </span>
    );
}

export default UnitConversionForm;