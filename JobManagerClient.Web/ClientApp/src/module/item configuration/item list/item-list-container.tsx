import * as React from "react";
import MaterialTable from "material-table";
import tableIcons from "../../../shared/icons/table-icons";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";


export default function ItemListTable() {
    const tableData = useSelector((appState:ApplicationState)=>appState.transaction_items_state)
    const tableColumns = [
        {title:'Code',field:'code'},
        {title:'Name',field:'name'},
        {title:'Unit',field:'measureUnitID'},
        {title:'Type',field:'goodType'},
        {title:'Category Name',field:'categoryID'}
    ]
    return (
        <MaterialTable
            title="Items"
            icons={tableIcons}
            data={tableData.transactions_items}
            //@ts-ignore
            columns={tableColumns}
            options={{
                selection: false,
                pageSize:10,
                pageSizeOptions:[10,20],
            }}
        />
    )
}
