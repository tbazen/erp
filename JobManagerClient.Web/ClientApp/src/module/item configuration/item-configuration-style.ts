import {createStyles, makeStyles, Theme} from "@material-ui/core";
import {PRIMARY_BACKGROUND_700} from "../../_constants/color/color-constant";

export const initICStyle = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            borderRadius:'1px',
            overflow:'hidden',
            paddingLeft:'20px',
            paddingRight:'20px'
        },
        chip: {
            fontSize:'15px',
            marginRight: theme.spacing(1),
            '&:focus': {
                color: '#fff',
                backgroundColor:PRIMARY_BACKGROUND_700
            }
        },
        divStyle:{
            backgroundColor:theme.palette.background.paper,
            padding:'10px',
            borderRadius: '1px'
        }
    })
);
