import React, { useEffect, useState} from 'react'
import {Button, Checkbox, FormControlLabel, FormGroup, Grid, Paper, TextField, Typography} from "@material-ui/core";
import {initICStyle} from "./item-configuration-style";
import RootCategoryForm from "./forms/root-category-form";
import UnitConversionForm from "./forms/unit-conversion-form";
import {useDispatch, useSelector} from "react-redux";
import {
    fetchItemCategories,
    searchTransactionItems
} from "../../_redux_setup/actions/ic-actions/item-configuration-actions";
import {ApplicationState} from "../../_model/state-model/application-state";
import ItemCategoryTree from "./category tree/item-category-tree-container";
import ItemListTable from "./item list/item-list-container";
import { SearchTransactionItemsPar } from '../../_services/bn.finance.service';


const ItemConfiguration = ()=>{
    const classes = initICStyle()
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const initSearchParams : SearchTransactionItemsPar={
        sessionId: auth.sessionId,
        index: 0,
        pageSize: 10,
        criteria: [],
        column: []
    }
    const [searchParams,setSearchParams] = useState(initSearchParams)
    
    useEffect(()=>{
        dispatch(fetchItemCategories({sessionId:auth.sessionId,PID:-1}))
    },[])

    const checkBoxHandler = (event :any)=>{
        if(event.target.checked){
            if(!(searchParams.column.indexOf(event.target.value)>=0)){
                setSearchParams({...searchParams,
                    column:[...searchParams.column,event.target.value],
                    criteria:[...searchParams.criteria,true]
                })
            }
        }
        else{
            let index = searchParams.column.indexOf(event.target.value)
            if(index >=0){
                const {column,criteria} = {...searchParams}
                column.splice(index,1)
                criteria.splice(index,1)
                setSearchParams({...searchParams,column,criteria})
            }
        }
    }

    const textChangeHandler = (event:any)=> {
        if(event.target.value.trim().length >0){
            let index = searchParams.column.indexOf(event.target.name)
            if(index>=0){
                let {criteria} = {...searchParams}
                criteria[index] = event.target.value
                setSearchParams({...searchParams,criteria})
            }
            else {
                setSearchParams({...searchParams,
                    column:[...searchParams.column,event.target.name],
                    criteria:[...searchParams.criteria,event.target.value]
                })
            }
        }
        else{
            let index = searchParams.column.indexOf(event.target.name)
            if(index >=0){
                const {column,criteria} = {...searchParams}
                column.splice(index,1)
                criteria.splice(index,1)
                setSearchParams({...searchParams,column,criteria})
            }
        }
    }

    const resetSearchParams = ()=>setSearchParams(initSearchParams)


    return (
                <Grid container spacing={2} justify={'center'}>
                    <Grid item md={12}>
                        <Paper className={classes.divStyle}>
                        <Grid container spacing={2} justify={'center'}>
                            <Grid item lg={2} md={3}>
                                <Typography variant={'overline'}>Item Catagory</Typography>
                                <FormGroup>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={searchParams.column.indexOf('IsExpenseItem')>=0?true:false}
                                                value="IsExpenseItem"
                                                onChange={checkBoxHandler}
                                                color="primary"
                                            />
                                        }
                                        label="Expense Item"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={searchParams.column.indexOf('IsSalesItem')>=0?true:false}
                                                value="IsSalesItem"
                                                onChange={checkBoxHandler}
                                                color="primary"
                                            />
                                        }
                                        label="Sales Item"
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={searchParams.column.indexOf('IsFixedAssetItem')>=0?true:false}
                                                value='IsFixedAssetItem'
                                                onChange={checkBoxHandler}
                                                color="primary"
                                            />
                                        }
                                        label="Fixed Asset Item"
                                    />
                                </FormGroup>
                            </Grid>
                            <Grid item lg={10} md={9}>
                                <Grid container spacing={2}>
                                    <Grid item md={5}>
                                        <TextField
                                            value={searchParams.column.indexOf('Name')>=0?searchParams.criteria[searchParams.column.indexOf('Name')]:''}
                                            onChange={textChangeHandler}
                                            name={'Name'}
                                            fullWidth
                                            placeholder={'Name'}
                                        />
                                    </Grid>
                                    <Grid item md={5}>
                                        <TextField
                                            name={'Code'}
                                            value={searchParams.column.indexOf('Code')>=0?searchParams.criteria[searchParams.column.indexOf('Code')]:''}
                                            fullWidth
                                            onChange={textChangeHandler}
                                            placeholder={'Code'}
                                        />
                                    </Grid>
                                    <Grid item md={12}>
                                        <Grid container spacing={2} justify={'flex-start'}>
                                            <Grid item>
                                                <Button onClick={()=>dispatch(searchTransactionItems(searchParams))} className={classes.button} size="small" color={'primary'} variant={'outlined'} >Search</Button>
                                            </Grid>
                                            <Grid item>
                                                <Button onClick={()=>resetSearchParams()} className={classes.button} size="small" color={'secondary'} variant={'outlined'} >Clear</Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        </Paper>
                    </Grid>
                    <Grid item md={12}>
                        <div className={classes.divStyle}>
                            <RootCategoryForm title={'Add Root Category'} chipLabel={'Add Root Category'}/>
                            <RootCategoryForm title={'Add Sub Category'} chipLabel={'Add Sub Category'}/>
                            <RootCategoryForm title={'Item Category Editor'} chipLabel={'Edit Category'}/>
                            <UnitConversionForm title={'Global Conversion Factor'} chipLabel={'Unit Conversion'}/>
                        </div>
                    </Grid>
                    <Grid item md={4}>
                        <ItemCategoryTree/>
                    </Grid>
                    <Grid item md={8}>
                        <ItemListTable/>
                    </Grid>
                </Grid>
    )
}

export default ItemConfiguration