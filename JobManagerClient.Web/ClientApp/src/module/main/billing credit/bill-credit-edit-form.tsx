import {Col, Divider, Form, Input, Row, Select, Typography} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector } from 'react-redux';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import {ApplicationState } from '../../../_model/state-model/application-state';
import {AuthenticationState } from '../../../_model/state-model/auth-state';
import {fetchBillPeriods, requestUpdateJobWeb} from '../../../_redux_setup/actions/mn-actions/main-actions';
import {AddJobWebPar, GetJobPar, JobData} from '../../../_services/job.manager.service';
import {BillCreditData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/bill-credit-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {GetBillPeriodsPar} from "../../../_services/subscribermanagment.service";
import {BillPeriodState} from "../../../_model/state-model/mn-sm/bill-period-state";
import {currentYear, years} from "../../../_helpers/date-util";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";

const { Text } = Typography
const { Option } = Select

interface IProps {
    match: any
}

export const BillCreditEditForm = (props: IProps) => {
    const formLayout = 'horizontal';
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [ auth , billPeriodState, workflowdata_state ] = useSelector<ApplicationState, [AuthenticationState,BillPeriodState,WorkflowDataState]>(appState => [appState.auth,appState.bill_period_state,appState.workflow_data_state])
    const [ billCreditData , setBillCreditData ] = useState<BillCreditData>(new BillCreditData())
    const [ year , setYear ] = useState<number>(currentYear);
    const [jobData,setJobData] = useEditJobInitializer({jobId,jobType:StandardJobTypes.BILLING_CERDIT})

    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setBillCreditData(workflowdata_state.payload as BillCreditData)
    },[ workflowdata_state ])
    useEffect(()=>{
        const params: GetBillPeriodsPar ={
            year,
            sessionId:auth.sessionId,
            ethiopianYear:true
        }
        dispatch(fetchBillPeriods(params))
    },[ year ])
    const handleSubmitBillCreditData = () => {
        if(jobData){
            const params : AddJobWebPar<BillCreditData> ={
                job:jobData,
                sessionId:auth.sessionId,
                data:billCreditData,
                dataType:NAME_SPACES.WORKFLOW.BILL_CREDIT_DATA
            }
            dispatch(requestUpdateJobWeb<BillCreditData>(params))
        }
    }

    return (
        <Form layout={formLayout}>
            <Row gutter={4}>
                <Col lg={{span:16}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Divider orientation={'left'}><Text strong>Total To be differed</Text></Divider>
                        <Form.Item label="Credit Amount" {...formItemLayout}>
                            <Input
                                value={billCreditData.schedule.creditedAmount}
                                onChange={(event)=>{
                                    setBillCreditData({
                                        ...billCreditData,
                                        schedule:{
                                            ...billCreditData.schedule,
                                            creditedAmount:+event.target.value
                                        }})
                                }}
                            />
                        </Form.Item>
                        <Form.Item label="Monthly Return Amount" {...formItemLayout}>
                            <Input
                                value={billCreditData.schedule.paymentAmount}
                                onChange={(event)=>{
                                    setBillCreditData({
                                        ...billCreditData,
                                        schedule:{
                                            ...billCreditData.schedule,
                                            paymentAmount:+event.target.value
                                        }})
                                }}
                            />
                        </Form.Item>
                        <Form.Item label="Return Starting From " {...formItemLayout}>
                            <Row gutter={4}>
                                <Col span={12}>
                                    <Select
                                        onChange={(periodID)=>{
                                            setBillCreditData({
                                                ...billCreditData,
                                                schedule:{
                                                    ...billCreditData.schedule,
                                                    startPeriodID:+periodID
                                                }
                                            })
                                        }}
                                        style={{width:'100%'}}
                                        loading = {billPeriodState.loading}>
                                        { billPeriodState.payload.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                                    </Select>
                                </Col>
                                <Col span={12}>
                                    <Select defaultValue={year} onChange={(year)=>setYear(+year)} style={{width:'100%'}} disabled={billPeriodState.loading}>
                                        { years.map((value,key)=><Option key={key} value={value}>{value}</Option>) }
                                    </Select>
                                </Col>
                            </Row>
                        </Form.Item>
                        <Form.Item label="Note " {...formItemLayout}>
                            <TextArea rows={4} />
                        </Form.Item>
                    </div>
                </Col>

                <Col lg={{span:8}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => jobData && setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) =>jobData && setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                    <div className={'paper'} style={{padding:'10px',marginTop:'5px'}}>
                        <CButton type={'primary'} block onClick={handleSubmitBillCreditData}>Submit</CButton>
                    </div>
                </Col>
            </Row>
        </Form>
    )
}
