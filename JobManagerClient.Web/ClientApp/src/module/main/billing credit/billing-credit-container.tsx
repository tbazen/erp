import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions";
import { hideSearchBar, exposeSearchBar } from '../../../_redux_setup/actions/global-searchbar-actions';
import { subscriberListClear, fetchCustomerList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { GetCustomersPar } from '../../../_services/subscribermanagment.service';
import { SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import { Card, Button } from 'antd';
import ButtonGridContainer from '../../../shared/screens/button-grid-container/button-grid-container';
import OperationFailed from '../../../shared/screens/status-code/submission-failed';
import CLoadingPage from '../../../shared/screens/cloading/cloading-page';
import BillingCreditForm from './billing-credit-form';
import { openDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {useBreadcrumb} from "../../../shared/hooks/manage-breadcrumb";
import {useGlobalSearchBar} from "../../../shared/hooks/manage-global-search";
import {StateMachineHOC} from "../../../shared/hoc/StateMachineHOC";

const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.BILLING_CREDIT,
        breadcrumbName:'Billing Credit'
    }
]

const BillingCreditContainer = ()=>{
    const dispatch = useDispatch()
    const [auth] = useSelector<ApplicationState,[AuthenticationState]>((appState:ApplicationState)=>[appState.auth])
    const initSearchParams : GetCustomersPar={
        pageSize:30,
        sessionId:auth.sessionId,
        kebele:-1,
        index:0,
        query:''
    }
    const [searchParams , setSearchParams] = useState<GetCustomersPar>(initSearchParams)
    const subscribersState = useSelector((appState:ApplicationState)=>appState.subscription_search_result_state)
    
    const searchInputHandler = (event:any) =>{
        const query = event.target.value
        setSearchParams({...initSearchParams,query})
    }

    useBreadcrumb(backwardPath)
    useGlobalSearchBar({searchPlaceHolder:'Customer Code',searchHandler:searchInputHandler,cleanupSearchResult:subscriberListClear})

    //API fetch initiator effect
    useEffect(()=>{
        if(searchParams.query.trim() !== '')
            dispatch(fetchCustomerList(searchParams))
    },[searchParams])

    const SubscriberCard = (props:SubscriberSearchResult)=>{
        return (
            <Card 
                title={props.subscriber.name} 
                bordered={false}
                actions={[
                   <Button onClick={()=>dispatch(openDrawerModal(<BillingCreditForm data= {props}/>))} icon={'plus'} block type={'link'}>
                        Add Billing Credit
                    </Button>
                  ]}
                >
                <p>{props.subscriber.customerCode}</p>
            </Card>
        )
    }

    return (
        <StateMachineHOC state={subscribersState}>
             <ButtonGridContainer spacing={6} lg={{span:6}} component={SubscriberCard} listItem={subscribersState.payload._ret}/>
        </StateMachineHOC>
    )
}

export default BillingCreditContainer