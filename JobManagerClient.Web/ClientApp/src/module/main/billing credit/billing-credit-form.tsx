import {Col, Divider, Input, Row, Select, Typography} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector } from 'react-redux';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import CFormItem from '../../../shared/core/cform-item/cform-item';
import {initialJobData } from '../../../_helpers/initial value/init-jobdata';
import {ApplicationState } from '../../../_model/state-model/application-state';
import {AuthenticationState } from '../../../_model/state-model/auth-state';
import {SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {fetchBillPeriods, requestAddJobWeb} from '../../../_redux_setup/actions/mn-actions/main-actions';
import {AddJobWebPar, JobData} from '../../../_services/job.manager.service';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {BillCreditData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/bill-credit-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {GetBillPeriodsPar} from "../../../_services/subscribermanagment.service";
import {BillPeriodState} from "../../../_model/state-model/mn-sm/bill-period-state";
import {currentYear, years} from "../../../_helpers/date-util";

const { Text, Title } = Typography
const { Option } = Select

interface IProps {
    data: SubscriberSearchResult
}




const BillingCreditForm = (props: IProps) => {
    const dispatch = useDispatch()
    const [auth , billPeriodState ] = useSelector<ApplicationState, [AuthenticationState,BillPeriodState]>(appState => [appState.auth,appState.bill_period_state])
    const [jobData, setJobData] = useState<JobData>({...initialJobData.job, customerID: props.data.subscriber.id, applicationType: StandardJobTypes.BILLING_CERDIT})
    const [ billingCreditData , setBillingCreditData ] = useState<BillCreditData>(new BillCreditData())
    const [ year , setYear ] = useState<number>(currentYear);

    useEffect(()=>{
        const params: GetBillPeriodsPar ={
            year,
            sessionId:auth.sessionId,
            ethiopianYear:true
        }
        dispatch(fetchBillPeriods(params))
    },[ year ])

    const handleSubmitBillCreditData = () => {
        const params : AddJobWebPar<BillCreditData> ={
            job:jobData,
            sessionId:auth.sessionId,
            data:billingCreditData,
            dataType:NAME_SPACES.WORKFLOW.BILL_CREDIT_DATA
        }
        dispatch(requestAddJobWeb(params))
    }

    return (
        <div>
            <Title level={4}>{props.data.subscriber.name}</Title>
            <Divider />
            <SharedJobFormFields
                dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
            />
            <Text strong>Total To be differed</Text>
            <Divider />
            <CFormItem
                input={<Input
                    onChange={(event)=>{
                        setBillingCreditData({
                            ...billingCreditData,
                            schedule:{
                                ...billingCreditData.schedule,
                                creditedAmount:+event.target.value
                            }})
                    }}
                />}
                label={'Credit Amount'}
                required />
            <CFormItem
                input={<Input
                    onChange={(event)=>{
                        setBillingCreditData({
                            ...billingCreditData,
                            schedule:{
                                ...billingCreditData.schedule,
                                paymentAmount:+event.target.value
                            }})
                    }}
                />}
                label={'Monthly Return Amount'}
                required />
            <CFormItem
                input={
                    <Row gutter={4}>
                        <Col span={12}>
                            <Select onChange={(periodID)=>{
                                setBillingCreditData({
                                    ...billingCreditData,
                                    schedule:{
                                        ...billingCreditData.schedule,
                                        startPeriodID:+periodID
                                    }
                                })
                            }} style={{width:'100%'}} loading = {billPeriodState.loading}>
                                { billPeriodState.payload.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                            </Select>
                        </Col>
                        <Col span={12}>
                            <Select defaultValue={year} onChange={(year)=>setYear(+year)} style={{width:'100%'}} disabled={billPeriodState.loading}>
                                { years.map((value,key)=><Option key={key} value={value}>{value}</Option>) }
                            </Select>
                        </Col>
                    </Row>
                    }
                label={'Return Starting From'}
                required />
            <CFormItem
                input={<TextArea rows={4} />}
                label={'Note'}
            />
            <div style={{ width: '50%' }} >
                <CButton type={'primary'} block onClick={handleSubmitBillCreditData}>Submit</CButton>
            </div>
        </div>
    )
}

export default BillingCreditForm