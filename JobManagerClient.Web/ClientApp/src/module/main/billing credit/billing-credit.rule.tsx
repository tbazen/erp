import React from 'react';
import { JobRuleClientHandlerBase } from '../../../_model/job-rule-model/job-rule-client-handler-base';
import BillingCreditContainer from './billing-credit-container';
import { JobRuleClientDecorator } from '../../../_decorator/jobrule-client-handler.decorator';
import { StandardJobTypes } from '../../../_model/view_model/mn-vm/standard-job-types';
import {ROUTES} from "../../../_constants/routes";
import {BillCreditWorkflowDataContainer} from "./workflow/bill-credit-workflow-data-container";
import {JobData} from "../../../_services/job.manager.service";
import {BillCreditEditForm} from "./bill-credit-edit-form";


@JobRuleClientDecorator({
    jobName:"Billing Credit",
    route:ROUTES.JOB.BILLING_CREDIT,
    jobType:StandardJobTypes.BILLING_CERDIT,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.BILLING_CERDIT].ABSOLUTE,
            component:BillCreditEditForm
        }
    ]
})
export class BillingCreditRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return BillingCreditContainer 
    }

    public getWorkFlowData(job : JobData){
        return <BillCreditWorkflowDataContainer job={job}/>
    }
}