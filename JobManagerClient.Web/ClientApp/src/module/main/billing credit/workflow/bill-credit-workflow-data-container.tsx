import React, {Fragment ,useEffect, useState} from 'react';
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {BillCreditData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/bill-credit-data";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";
import SubscriberManagementService, {GetBillPeriodPar} from "../../../../_services/subscribermanagment.service";
import {BillPeriod} from "../../../../_model/level0/subscriber-managment-type-library/bill-period";


const subscriber_mgr_service = new SubscriberManagementService()
interface IProps{
    job : JobData
}

export function BillCreditWorkflowDataContainer(props:IProps){
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ billCreditData,setBillCreditData ] = useState<BillCreditData>()
    const [ billPeriod , setBillPeriod ] = useState<BillPeriod>()

    const dispatch = useDispatch()
    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:props.job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.BILLING_CERDIT,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setBillCreditData(workflowdata_state.payload as BillCreditData)
    },[ workflowdata_state ])
    useEffect(()=>{
        billCreditData !== undefined &&
        billCreditData.schedule !== undefined &&
        billCreditData.schedule !== null &&
        fetchBillPeriod(billCreditData.schedule.startPeriodID)
    },[billCreditData])
    async function fetchBillPeriod(periodId:number){
        try
        {
            const params : GetBillPeriodPar={
                sessionId:auth.sessionId,
                periodID:periodId
            }
            const billPeriod = (await subscriber_mgr_service.GetBillPeriod(params)).data
            setBillPeriod(billPeriod)
        }
        catch (e) {

        }
    }
    return(
        <div>
            {
                billCreditData!== undefined &&
                <Fragment>
                    {
                        billCreditData.creditedBills.length > 0 ?
                        <DataRow
                            title={'Bills'}
                            content={''}/>
                        :
                        <DataRow
                            title={'Credit Detail'}
                            content={''}/>
                    }
                    {
                        billCreditData.creditedBills.map((record,key)=>
                            <DataRow
                                title={record.shortDescription}
                                content={record.total}/>)
                    }
                    {
                        billCreditData.schedule !== undefined &&
                        billCreditData.schedule !== null &&
                        <Fragment>
                            <DataRow
                                title={'Payment Amount'}
                                content={`${parseNumberToMoney(billCreditData.schedule.paymentAmount)} ETB`}/>
                            <DataRow
                                title={'Starting Month'}
                                content={billPeriod!==undefined? billPeriod.name : ''}/>
                        </Fragment>
                    }
                </Fragment>
            }
        </div>
    )
}
