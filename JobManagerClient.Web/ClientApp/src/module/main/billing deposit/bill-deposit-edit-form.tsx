import {Col, Divider, Form, Input, Row, Typography} from 'antd';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import {ApplicationState} from '../../../_model/state-model/application-state';
import {AuthenticationState} from '../../../_model/state-model/auth-state';
import {requestUpdateJobWeb} from '../../../_redux_setup/actions/mn-actions/main-actions';
import {AddJobWebPar} from '../../../_services/job.manager.service';
import {BillDepositData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/bill-deposit-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";


const { Title } = Typography
interface IProps {
    match: any
}

export const BillDepositEditForm = (props: IProps) => {
    const formLayout = 'horizontal';
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [auth,workflowdata_state] = useSelector<ApplicationState, [AuthenticationState,WorkflowDataState]>(appState => [appState.auth,appState.workflow_data_state])
    const [jobData, setJobData] = useEditJobInitializer({ jobId,jobType:StandardJobTypes.BILLING_DEPOSIT })
    const [ billDepositData , setBillDepositData ] = useState<BillDepositData>(new BillDepositData());
    useEffect(()=>{
        console.log('WF STATE ',workflowdata_state)
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setBillDepositData(workflowdata_state.payload as BillDepositData)
    },[ workflowdata_state ])
    const submitBillDepositData = () => {
        if(jobData){
            const params: AddJobWebPar<BillDepositData> = {
                job:jobData,
                sessionId:auth.sessionId,
                data:billDepositData,
                dataType:NAME_SPACES.WORKFLOW.BILL_DEPOSIT_DATA,
            }
            dispatch(requestUpdateJobWeb(params))
        }
    }
    return (
        <Form layout={formLayout}>
            <Row gutter={4}>
                <Col lg={{span:16}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Form.Item label="Deposit Amount" {...formItemLayout}>
                            <Input
                                value={billDepositData.deposit}
                                onChange={(event)=>{
                                    setBillDepositData({...billDepositData,deposit:+event.target.value})
                                }}
                                size={'large'} />
                        </Form.Item>
                    </div>
                </Col>

                <Col lg={{span:8}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Divider orientation={'left'}><Title level={4}>Customer Information</Title></Divider>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => jobData && setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) =>jobData &&  setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                    <div className={'paper'} style={{marginTop:'5px',padding:'10px'}}>
                        <CButton block type={'primary'} onClick={submitBillDepositData}>Submit</CButton>
                    </div>
                </Col>
            </Row>
        </Form>
    )
}
