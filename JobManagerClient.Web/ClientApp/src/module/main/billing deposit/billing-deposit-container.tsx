import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions";
import { exposeSearchBar, hideSearchBar } from '../../../_redux_setup/actions/global-searchbar-actions';
import { SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import { subscriberListClear, fetchCustomerList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { GetCustomersPar } from '../../../_services/subscribermanagment.service';
import { Card, Button } from 'antd';
import CLoadingPage from '../../../shared/screens/cloading/cloading-page';
import OperationFailed from '../../../shared/screens/status-code/submission-failed';
import ButtonGridContainer from '../../../shared/screens/button-grid-container/button-grid-container';
import BillingDepositForm from './billling-deposit-form';
import { openDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.BILLING_DEPOSIT,
        breadcrumbName:'Billing Deposit'
    }
]


const BillingDepositContainer = ()=>{
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const initSearchParams : GetCustomersPar={
        pageSize:30,
        sessionId:auth.sessionId,
        kebele:-1,
        index:0,
        query:''
    }
    const [searchParams , setSearchParams] = useState<GetCustomersPar>(initSearchParams)
    const subscribersState = useSelector((appState:ApplicationState)=>appState.subscription_search_result_state)
    
    const searchInputHandler = (event:any) =>{
        const query = event.target.value
        setSearchParams({...initSearchParams,query})
    }
    
    //UI configuration effect
    useEffect(()=>{
        dispatch(pushPathToBC(backwardPath))
        dispatch(exposeSearchBar('Customer Code',searchInputHandler))
        return () => {
            dispatch(pushPathToBC([]))    
            dispatch(hideSearchBar())
            dispatch(subscriberListClear())
        };
    },[])
    //API fetch initiator effect
    useEffect(()=>{
        if(searchParams.query.trim() !== '')
            dispatch(fetchCustomerList(searchParams))
    },[searchParams])

    const CustomerCard = (props:SubscriberSearchResult)=>{
        return (
            <Card 
                title={props.subscriber.name} 
                bordered={false}
                actions={[
                    <Button onClick={()=>dispatch(openDrawerModal(<BillingDepositForm data= {props}/>))} icon={'edit'} size={'large'} block type={'link'}>
                        Add Deposit
                    </Button>
                  ]}
                >
                <p>{props.subscriber.customerCode}</p>
            </Card>
        )
    }

    return (
        <Fragment>
            {
                subscribersState.loading && 
                <CLoadingPage/>
            }
            {
                subscribersState.error && 
                <OperationFailed/>
            }
            {
                !subscribersState.loading &&
                !subscribersState.error &&
                <ButtonGridContainer spacing={6} lg={{span:6}} component={CustomerCard} listItem={subscribersState.payload._ret}/>
            }
        </Fragment>
    )
}

export default BillingDepositContainer