import React from 'react'
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {ROUTES} from "../../../_constants/routes";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import BillingDepositContainer from "./billing-deposit-container";
import {JobData} from "../../../_services/job.manager.service";
import {BillDepositWorkflowDataContainer} from "./workflow/bill-deposit-workflow-data-container";
import {BillDepositEditForm} from "./bill-deposit-edit-form";

@JobRuleClientDecorator({
    jobName:"Billing Deposit",
    route:ROUTES.JOB.BILLING_DEPOSIT,
    jobType:StandardJobTypes.BILLING_DEPOSIT,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.BILLING_DEPOSIT].ABSOLUTE,
            component:BillDepositEditForm
        }
    ]
})
export class BillingDepositRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return BillingDepositContainer
    }
    
    public getWorkFlowData(job:JobData){
        return <BillDepositWorkflowDataContainer job={job}/>
    }
}