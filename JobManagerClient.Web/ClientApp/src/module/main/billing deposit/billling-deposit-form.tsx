import {Divider, Input, Typography} from 'antd';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import CFormItem from '../../../shared/core/cform-item/cform-item';
import {ApplicationState} from '../../../_model/state-model/application-state';
import {AuthenticationState} from '../../../_model/state-model/auth-state';
import {SubscriberSearchResult} from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {requestAddJobWeb} from '../../../_redux_setup/actions/mn-actions/main-actions';
import {AddJobWebPar, JobData} from '../../../_services/job.manager.service';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {BillDepositData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/bill-deposit-data";
import {initialJobData} from "../../../_helpers/initial value/init-jobdata";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {NotificationTypes, openNotification} from "../../../shared/components/notification/notification";
import errorMessage from "../../../_redux_setup/actions/error.message";

const { Title } = Typography
interface IProps {
    data: SubscriberSearchResult
}

const BillingDepositForm = (props: IProps) => {
    const dispatch = useDispatch()
    const [auth] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])
    const [jobData, setJobData] = useState<JobData>({ ...initialJobData.job, customerID: props.data.subscriber.id, applicationType: StandardJobTypes.BILLING_DEPOSIT })
    const [ billDepositData , setBillDepositData ] = useState<BillDepositData>(new BillDepositData());

    const submitBillDepositData = () => {
        try{
            if( isNaN(billDepositData.deposit) || !(billDepositData.deposit > 0))
                throw new Error('Deposit amount must be a number and greater than zero')
            const params: AddJobWebPar<BillDepositData> = {
                job:jobData,
                sessionId:auth.sessionId,
                data:billDepositData,
                dataType:NAME_SPACES.WORKFLOW.BILL_DEPOSIT_DATA,
            }
            dispatch(requestAddJobWeb(params))
        }catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Invalid Input',
                description:errorMessage(error)
            })
        }
    }
    return (
        <div>
            <Title level={4}>Customer Information</Title>
            <Divider />
            <CustomerInformationContainer {...props.data} />
            <br />
            <br />
            <SharedJobFormFields
                dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
            />
            <CFormItem
                input={
                    <Input
                        onChange={(event)=>{
                            setBillDepositData({...billDepositData,deposit:+event.target.value})
                        }}
                        size={'large'} />}
                label={'Deposit Amount'}
                required />
            <div
                style={{ width: '50%' }}
            >
                <CButton block type={'primary'} onClick={submitBillDepositData}>Submit</CButton>
            </div>
        </div>
    )
}

export default BillingDepositForm