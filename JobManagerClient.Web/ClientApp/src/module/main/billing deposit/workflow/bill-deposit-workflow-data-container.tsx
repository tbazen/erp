import React, {Fragment, useEffect, useState} from 'react'
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {BillDepositData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/bill-deposit-data";
import {DataRow} from "../../../../shared/components/data-row/data-row";

interface IProps{
    job : JobData
}
export function BillDepositWorkflowDataContainer(props:IProps){
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ billDepositData,setBillDepositData ] = useState<BillDepositData>()

    const dispatch = useDispatch()
    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:props.job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.BILLING_DEPOSIT,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setBillDepositData(workflowdata_state.payload as BillDepositData)
    },[ workflowdata_state ])

    return (
        <Fragment>
            {
                billDepositData !== undefined &&
                <Fragment>
                    <DataRow
                        title={'Deposit Amount'}
                        content={billDepositData.deposit}/>
                </Fragment>
            }
        </Fragment>
    )
}