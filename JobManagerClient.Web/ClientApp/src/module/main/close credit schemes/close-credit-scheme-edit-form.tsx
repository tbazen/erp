import React, {useEffect, useState} from 'react'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import {Checkbox, Col, Divider, Form, Row, Table, Typography} from 'antd';
import {useDispatch, useSelector} from 'react-redux';
import {ApplicationState} from '../../../_model/state-model/application-state';
import {AuthenticationState} from '../../../_model/state-model/auth-state';
import {AddJobWebPar} from '../../../_services/job.manager.service';
import {
    fetchCustomerCreditSchemes,
    requestUpdateJobWeb
} from '../../../_redux_setup/actions/mn-actions/main-actions';
import CButton from '../../../shared/core/cbutton/cbutton';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {GetCustomerCreditSchemesPar} from "../../../_services/subscribermanagment.service";
import {CreditSchemeState} from "../../../_model/state-model/mn-sm/credit-scheme-state";
import {CreditScheme} from "../../../_model/level0/subscriber-managment-type-library/credit-scheme";
import {CloseCreditSchemeData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/close-credit-scheme";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";

const { Column } = Table;

const {Title } = Typography
interface IProps {
    match: any
}

export const CloseCreditSchemeEditForm = (props:IProps) =>{
    const formLayout = 'horizontal';
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [auth , customerCreditSchemeState, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState , CreditSchemeState, WorkflowDataState]>(appState => [appState.auth, appState.credit_scheme_state, appState.workflow_data_state])
    const [jobData, setJobData] = useEditJobInitializer({jobId,jobType:StandardJobTypes.CLOSE_CREDIT_SCHEME })
    const [ creditSchemeData , setCreditSchemeData ] = useState<CloseCreditSchemeData>(new CloseCreditSchemeData())
    let index = 0;

    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setCreditSchemeData(workflowdata_state.payload as CloseCreditSchemeData)
    },[ workflowdata_state ])

    useEffect(()=>{
        if(jobData){
            const creditSchemeParams: GetCustomerCreditSchemesPar = {
                sessionId: auth.sessionId,
                customerID: jobData.customerID
            }
            dispatch(fetchCustomerCreditSchemes(creditSchemeParams))
        }
    },[jobData])

    const submitJobData = () => {
        if(jobData){
            const creditJobData: AddJobWebPar<CloseCreditSchemeData> = {
                job: jobData,
                data: creditSchemeData,
                dataType: NAME_SPACES.WORKFLOW.CLOSE_CREDIT_SCHEME_DATA,
                sessionId: auth.sessionId
            }
            dispatch(requestUpdateJobWeb(creditJobData))
        }
    }

    return (
        <Form layout={formLayout}>
            <Row gutter={4}>
                <Col lg={{span:16}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Table
                            dataSource={customerCreditSchemeState.payload._ret.map(value=>({...value,differedAmount:value.creditedAmount,paymentAmount:value.paymentAmount , formatedDate: new Date(value.issueDate).toLocaleString() }))}
                            size={'small'}
                            loading={customerCreditSchemeState.loading}
                            pagination={false}
                        >
                            <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }}/>
                            <Column
                                title="Select"
                                key="selectScheme"
                                render={(text , rowData:CreditScheme)=><Checkbox onChange={
                                    (event)=>{
                                        if(event.target.checked){
                                            setCreditSchemeData({...creditSchemeData,schemeIDs:[...creditSchemeData.schemeIDs,rowData.id]})
                                        }
                                    }}  />}
                            />
                            <Column title="Date" dataIndex="formatedDate" key="date" />
                            <Column title="Differed Amount" dataIndex="differedAmount" key="differedAmount" />
                            <Column title="Monthly Amount" dataIndex="paymentAmount" key="paymentAmount" />
                        </Table>
                    </div>
                </Col>
                <Col lg={{span:8}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Divider orientation={'left'}><Title level={4}>Customer Information</Title></Divider>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => jobData && setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) =>jobData && setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                    <div className={'paper'} style={{padding:'10px',marginTop:'5px'}}>
                        <CButton block onClick={submitJobData} type={'primary'}>Submit</CButton>
                    </div>
                </Col>
            </Row>
        </Form>
    )
}
