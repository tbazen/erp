import React, {useEffect, useState} from 'react'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import { SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information';
import {Checkbox, Divider, Table, Typography} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { AuthenticationState } from '../../../_model/state-model/auth-state';
import { AddJobWebPar, JobData} from '../../../_services/job.manager.service';
import { initialJobData } from '../../../_helpers/initial value/init-jobdata';
import {
    fetchCustomerCreditSchemes,
    requestAddJobWeb
} from '../../../_redux_setup/actions/mn-actions/main-actions';
import CButton from '../../../shared/core/cbutton/cbutton';
import { StandardJobTypes } from "../../../_model/view_model/mn-vm/standard-job-types";
import { GetCustomerCreditSchemesPar } from "../../../_services/subscribermanagment.service";
import { CreditSchemeState } from "../../../_model/state-model/mn-sm/credit-scheme-state";
import { CreditScheme } from "../../../_model/level0/subscriber-managment-type-library/credit-scheme";
import { CloseCreditSchemeData } from "../../../_model/level0/job-manager-model/standard-job-datatypes/close-credit-scheme";
import { NAME_SPACES } from "../../../_constants/model-namespaces";

const { Column } = Table;

const {Title } = Typography
interface IProps{
    data : SubscriberSearchResult
}

const CloseCreditSchemeForm = (props:IProps) =>{
    const dispatch = useDispatch()
    const [auth , customerCreditSchemeState] = useSelector<ApplicationState, [AuthenticationState , CreditSchemeState]>(appState => [appState.auth, appState.credit_scheme_state])
    const [jobData, setJobData] = useState<JobData>({...initialJobData.job,customerID:props.data.subscriber.id,applicationType:StandardJobTypes.CLOSE_CREDIT_SCHEME})
    const [ creditScheme , setCreditScheme ] = useState<CloseCreditSchemeData>(new CloseCreditSchemeData())


    let index = 0;
    useEffect(()=>{
        const creditSchemeParams: GetCustomerCreditSchemesPar = {
            sessionId: auth.sessionId,
            customerID: props.data.subscriber.id
        }
        dispatch(fetchCustomerCreditSchemes(creditSchemeParams))
    },[])

    const submitJobData = () => {
        const creditJobData: AddJobWebPar<CloseCreditSchemeData> = {
            job: jobData,
            data: creditScheme,
            dataType: NAME_SPACES.WORKFLOW.CLOSE_CREDIT_SCHEME_DATA,
            sessionId: auth.sessionId
        }
        dispatch(requestAddJobWeb(creditJobData))
    }

    return (
        <div>
            <Title level={4}>Customer Information</Title>
            <Divider/>
            <CustomerInformationContainer {...props.data}/>
            <br/>
            <br/>
            <SharedJobFormFields
                                dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                            />

            <Table
                dataSource={customerCreditSchemeState.payload._ret.map(value=>({...value,differedAmount:value.creditedAmount,paymentAmount:value.paymentAmount , formatedDate: new Date(value.issueDate).toLocaleString() }))}
                size={'small'}
                loading={customerCreditSchemeState.loading}
                pagination={false}
            >
                <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }}/>
                <Column
                    title="Select"
                    key="selectScheme"
                    render={(text , rowData:CreditScheme)=><Checkbox onChange={
                        (event)=>{
                            if(event.target.checked){
                                setCreditScheme({...creditScheme,schemeIDs:[...creditScheme.schemeIDs,rowData.id]})
                            }
                        }}  />}
                />
                <Column title="Date" dataIndex="formatedDate" key="date" />
                <Column title="Differed Amount" dataIndex="differedAmount" key="differedAmount" />
                <Column title="Monthly Amount" dataIndex="paymentAmount" key="paymentAmount" />
            </Table>
            <div style={{ width:'50%',marginTop:'10px'}} >
                <CButton block onClick={submitJobData} type={'primary'}>Submit</CButton>
            </div>   
        </div>
    )
}

export default CloseCreditSchemeForm