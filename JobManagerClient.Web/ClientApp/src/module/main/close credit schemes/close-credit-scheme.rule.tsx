import React from 'react'
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {ROUTES} from "../../../_constants/routes";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import CloseCreditSchemesContainer from "./close-credit-sheme-container";
import {CloseCreditSchemeWorkflowDataContainer} from "./workflow/close-credit-scheme-workflow-data-container";
import {JobData} from "../../../_services/job.manager.service";
import {CloseCreditSchemeEditForm} from "./close-credit-scheme-edit-form";

@JobRuleClientDecorator({
    jobName:"Close Credit Scheme",
    route:ROUTES.JOB.CLOSE_CREDIT_SCHEME,
    jobType:StandardJobTypes.CLOSE_CREDIT_SCHEME,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.CLOSE_CREDIT_SCHEME].ABSOLUTE,
            component:CloseCreditSchemeEditForm
        }
    ]
})
export class CloseCreditSchemeRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return CloseCreditSchemesContainer
    }

    public getWorkFlowData(job:JobData){
        return <CloseCreditSchemeWorkflowDataContainer job={job}/>
    }
}