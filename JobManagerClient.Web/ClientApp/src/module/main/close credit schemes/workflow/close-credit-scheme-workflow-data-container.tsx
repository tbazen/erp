import React, {Fragment, useEffect, useState} from 'react'
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {CloseCreditSchemeData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/close-credit-scheme";
import {CreditScheme} from "../../../../_model/level0/subscriber-managment-type-library/credit-scheme";
import {Table} from "antd";
import SubscriberManagementService from "../../../../_services/subscribermanagment.service";
const { Column } = Table;
interface IProps{
    job : JobData
}
const subscriber_mgr_service = new SubscriberManagementService()

export function CloseCreditSchemeWorkflowDataContainer(props:IProps) {
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ creditSchemeData,setCloseCreditSchemeData ] = useState<CloseCreditSchemeData>()
    const [ creditSchemes , setCreditSchemes ] = useState<CreditScheme[]>([])

    const dispatch = useDispatch()
    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:props.job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.CLOSE_CREDIT_SCHEME,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setCloseCreditSchemeData(workflowdata_state.payload as CloseCreditSchemeData)
    },[ workflowdata_state ])
    useEffect(()=>{
        creditSchemeData!== undefined &&
        fetchCreditSchemes()
    },[ creditSchemeData ])

    async function fetchCreditSchemes(){
        if(creditSchemeData){
            let schemes:CreditScheme[] = []
            for (const schemeId of creditSchemeData.schemeIDs){
                const scheme: CreditScheme = (await subscriber_mgr_service.GetCreditScheme(schemeId)).data
                schemes = [ ...schemes,scheme ]
            }
            setCreditSchemes(schemes)
        }
    }

    return (
        <Fragment>
            {
                creditSchemeData !== undefined &&
                <Table
                    dataSource={creditSchemes}
                    >
                    <Column title="Date" dataIndex="issueDate" key="issueDate"></Column>
                    <Column title="Amount" dataIndex="creditedAmount" key="creditedAmount"></Column>
                    <Column title="Monthly Payment" dataIndex="paymentAmount" key="paymentAmount"></Column>
                </Table>
            }
        </Fragment>
    )
}