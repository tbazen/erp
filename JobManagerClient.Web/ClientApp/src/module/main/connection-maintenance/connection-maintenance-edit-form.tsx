import React, {useEffect, useState} from 'react';
import SharedJobFormFields from "../../../shared/components/job application form/shared-job-field";
import {Col, Row, Tabs} from "antd";
import {AddJobWebPar} from "../../../_services/job.manager.service";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {ConntectionMaintenanceData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/connection-maintenance-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {requestAddJobWeb} from "../../../_redux_setup/actions/mn-actions/main-actions";
import CButton from "../../../shared/core/cbutton/cbutton";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import {MeterData} from "../../../_model/view_model/mn-vm/meter-data";
import {MeterPageForm} from "../../_clients/shashemene/connection-maintenance/form/meter-page/meter-page-form";
import { useParams } from 'react-router-dom';


const { TabPane } = Tabs;

export function ConnectionMaintenanceEditForm(){
    const dispatch = useDispatch()
    const {jobId}=useParams()
    const [auth,workflowdata_state ] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth, appState.workflow_data_state])
    const [jobData, setJobData] = useEditJobInitializer({ jobId:(jobId && +jobId)||-1 ,jobType:StandardJobTypes.CONNECTION_MAINTENANCE})
    const [ connectionMaintenanceData,setConnectionMaintenanceData ] = useState<ConntectionMaintenanceData>()
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setConnectionMaintenanceData(workflowdata_state.payload as ConntectionMaintenanceData)
    },[ workflowdata_state ])
    function handleUpdatedMeterDataChange(newMeterData: MeterData) {
        connectionMaintenanceData &&
        setConnectionMaintenanceData({...connectionMaintenanceData,meterData:newMeterData})
    }
    function handleSubmitConnectionMaintenanceData(){
        if(jobData && connectionMaintenanceData){
            const params: AddJobWebPar<ConntectionMaintenanceData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: connectionMaintenanceData,
                dataType: NAME_SPACES.WORKFLOW.CONNECTION_MAINTENANCE_DATA
            }
            dispatch(requestAddJobWeb(params))
        }
    }

    return (

        <Tabs
            defaultActiveKey="1"
            tabBarExtraContent={
                <CButton
                    type={'primary'}
                    block
                    size={'large'}
                    onClick={handleSubmitConnectionMaintenanceData}>
                    Submit
                </CButton>
            }
        >
            <TabPane tab="Customer/ Connection Information" key="1">
                <Row gutter={4}>
                    <Col lg={{span:8}}>
                        <div className={'paper'} style={{padding:'10px'}}>
                            <SharedJobFormFields
                                dateChangeHandler={(date: string) =>jobData && setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                                descriptionChangeHandler={(desc: string) =>jobData && setJobData({ ...jobData, description: desc })}
                            />
                        </div>
                        <div className={'paper'} style={{padding:'10px',marginTop:'5px'}}>
                            <CButton type={'primary'} block onClick={handleSubmitConnectionMaintenanceData}>Submit</CButton>
                        </div>
                    </Col>
                </Row>
            </TabPane>
                { connectionMaintenanceData && connectionMaintenanceData.changeMeter &&  <TabPane key={'4'} tab={'Meter Page'}><MeterPageForm handleMeterDataChange={handleUpdatedMeterDataChange} meterData={connectionMaintenanceData.meterData}/></TabPane> }

        </Tabs>
    )
}