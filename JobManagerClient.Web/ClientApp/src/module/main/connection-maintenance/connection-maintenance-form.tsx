import React, {useState} from 'react';
import {SubscriberSearchResult} from "../../../_model/view_model/mn-vm/subscriber-search-result";
import SharedJobFormFields from "../../../shared/components/job application form/shared-job-field";
import {SubscriberInformationContainer} from "../../../shared/components/customer-information/customer-information";
import {Divider} from "antd";
import {AddJobWebPar, JobData} from "../../../_services/job.manager.service";
import {initialJobData} from "../../../_helpers/initial value/init-jobdata";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {ConntectionMaintenanceData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/connection-maintenance-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {requestAddJobWeb} from "../../../_redux_setup/actions/mn-actions/main-actions";
import CButton from "../../../shared/core/cbutton/cbutton";

interface IProps {
    data: SubscriberSearchResult
}

export function ConnectionMaintenanceForm(props : IProps){
    const [auth ] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])
    const [jobData, setJobData] = useState<JobData>({...initialJobData.job, customerID: props.data.subscriber.id, applicationType: StandardJobTypes.CONNECTION_MAINTENANCE})
    const dispatch  = useDispatch()

    function handleSubmitConnectionMaintenanceData(){
        let connectionData = new ConntectionMaintenanceData()
        connectionData.connectionID = props.data.subscription.id;
        const params: AddJobWebPar<ConntectionMaintenanceData>={
            job:jobData,
            sessionId: auth.sessionId,
            data: connectionData,
            dataType: NAME_SPACES.WORKFLOW.CONNECTION_MAINTENANCE_DATA
        }
        dispatch(requestAddJobWeb(params))
    }

    return (
        <div>
            <SharedJobFormFields
                dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
            />
            <Divider orientation={'left'}>Customer Information</Divider>
            <SubscriberInformationContainer {...props.data.subscriber}/>
            <div style={{ width: '50%' }} >
                <CButton type={'primary'} block onClick={handleSubmitConnectionMaintenanceData}>Submit</CButton>
            </div>
        </div>
    )
}