import React from 'react'
import {ConnectionMaintenanceContainer} from "./connection-maintenance";
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../_constants/routes";
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import {JobData} from "../../../_services/job.manager.service";
import {ConnectionMaintenanceWorkflowData} from "./workflow/connection-maintenance-workflow-data-container";
import {CloseCreditSchemeEditForm} from "../close credit schemes/close-credit-scheme-edit-form";
import {ConnectionMaintenanceEditForm} from "./connection-maintenance-edit-form";

@JobRuleClientDecorator({
    jobName:"Existing Connection Service",
    jobType:StandardJobTypes.CONNECTION_MAINTENANCE,
    route:ROUTES.JOB.CONNECTION_MAINTENANCE,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.CONNECTION_MAINTENANCE].ABSOLUTE,
            component:ConnectionMaintenanceEditForm
        }
    ]
})
export class ConnectionMaintenanceRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
     return ConnectionMaintenanceContainer
    }

    public getWorkFlowData(job:JobData){
        return <ConnectionMaintenanceWorkflowData job={job}/>
    }
}