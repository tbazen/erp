import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions";
import { hideSearchBar, exposeSearchBar } from '../../../_redux_setup/actions/global-searchbar-actions';
import {
    subscriberListClear,
    fetchCustomerList,
    fetchSubscriberList
} from '../../../_redux_setup/actions/mn-actions/main-actions';
import { ApplicationState } from '../../../_model/state-model/application-state';
import {
    GetCustomersPar,
    GetSubscriptions2Par,
    SubscriberSearchField
} from '../../../_services/subscribermanagment.service';
import { SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import { Card, Button } from 'antd';
import ButtonGridContainer from '../../../shared/screens/button-grid-container/button-grid-container';
import OperationFailed from '../../../shared/screens/status-code/submission-failed';
import CLoadingPage from '../../../shared/screens/cloading/cloading-page';
import { openDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';
import {ConnectionMaintenanceForm} from "./connection-maintenance-form";

const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.CONNECTION_MAINTENANCE,
        breadcrumbName:'Connection Maintenance'
    }
]

export const ConnectionMaintenanceContainer = () =>{
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const initSearchParams : GetSubscriptions2Par={
        sessionId:auth.sessionId,
        query: '',
        fields: SubscriberSearchField.CustomerName | SubscriberSearchField.ConstactNo,
        multipleVersion: false,
        kebele: -1,
        zone: -1,
        dma: -1,
        index: 0,
        page: 30
    }
    const [searchParams , setSearchParams] = useState<GetSubscriptions2Par>(initSearchParams)
    const subscribersState = useSelector((appState:ApplicationState)=>appState.subscription_search_result_state)

    const searchInputHandler = (event:any) =>{
        const query = event.target.value
        setSearchParams({...initSearchParams,query})
    }

    useEffect(()=>{
        dispatch(pushPathToBC(backwardPath))
        dispatch(exposeSearchBar('Customer Code',searchInputHandler))
        return () => {
            dispatch(pushPathToBC([]))
            dispatch(hideSearchBar())
            dispatch(subscriberListClear())
        };
    },[])

    //API fetch initiator effect
    useEffect(()=>{
        if(searchParams.query.trim() !== '')
            dispatch(fetchSubscriberList(searchParams))
    },[searchParams])

    const SubscriberCard = (props:SubscriberSearchResult)=>{
        return (
            <Card
                title={props.subscriber.name}
                bordered={false}
                actions={[
                    <Button onClick={()=>dispatch(openDrawerModal(<ConnectionMaintenanceForm data={props}/>))} icon={'plus'} block type={'link'}>
                        Connection Maintenance
                    </Button>
                ]}
            >
                <p>{props.subscription.contractNo}</p>
            </Card>
        )
    }

    return (
        <Fragment>
            {
                subscribersState.loading &&
                <CLoadingPage/>
            }
            {
                subscribersState.error &&
                <OperationFailed/>
            }
            {
                !subscribersState.loading &&
                !subscribersState.error &&
                <ButtonGridContainer spacing={6} lg={{span:6}} component={SubscriberCard} listItem={subscribersState.payload._ret}/>
            }
        </Fragment>
    )
}
