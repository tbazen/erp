import {MeterData} from "../../../../_model/view_model/mn-vm/meter-data";
import React, {Fragment, useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {TransactionItemsState} from "../../../../_model/state-model/ic-sm/transaction-items-state";
import {Col, Divider, Form, Input, Row, Select, Spin} from "antd";
import {MaterialOperationalStatus} from "../../../../_enum/mn-enum/material-operational-status";
const { Option  } = Select

interface MeterPageFormProps{
    meterData?: MeterData | null
    handleMeterDataChange?: (meterData:MeterData )=>void
}

export function MeterPageForm(props: MeterPageFormProps){
    const formItemLayout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 14 },
    }
    const [ meterData , setMeterData ] = useState<MeterData>(props.meterData || new MeterData())
    const [ transactionItemState ] = useSelector<ApplicationState,[ TransactionItemsState ]>(appState => [ appState.transaction_items_state ])
    useEffect(()=>{
        props.handleMeterDataChange &&
        props.handleMeterDataChange(meterData)
    },[ meterData ])
    return (
        <Fragment>
            <Row gutter={2}>
                <Col span={12}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Divider orientation="left">{'Meter Detail'}</Divider>
                        <Form.Item label="Meter Type" {...formItemLayout}>
                            <Spin spinning={transactionItemState.loading} delay={500}>
                                <Select style={{width:'100%'}} onChange={(itemCode: any)=>{ setMeterData({...meterData,itemCode})}}>
                                    { transactionItemState.transactions_items.map((item,key)=><Option key={key} value={item.code}>{item.name}</Option>) }
                                </Select>
                            </Spin>
                        </Form.Item>
                        <Form.Item label="Model" {...formItemLayout}>
                            <Input onChange={( event )=>{setMeterData({...meterData,modelNo:event.target.value})}} />
                        </Form.Item>
                        <Form.Item label="Serial No." {...formItemLayout}>
                            <Input onChange={( event )=>{ setMeterData({...meterData,serialNo:event.target.value}) }} />
                        </Form.Item>
                        <Form.Item label="Initial Reading" {...formItemLayout}>
                            <Input onChange={( event )=>{ setMeterData({...meterData,initialMeterReading:+event.target.value}) }} />
                        </Form.Item>
                        <Form.Item label="Operational Status" {...formItemLayout}>
                            <Select style={{width:'100%'}} onChange={(operationalStatus: any)=>{ setMeterData({...meterData,opStatus:operationalStatus}) }}>
                                <Option value={MaterialOperationalStatus.Working}>Working</Option>
                                <Option value={MaterialOperationalStatus.Defective}>Defective</Option>
                                <Option value={MaterialOperationalStatus.NotWorking}>Not Working</Option>
                            </Select>
                        </Form.Item>
                    </div>
                </Col>
            </Row>
        </Fragment>
    )
}