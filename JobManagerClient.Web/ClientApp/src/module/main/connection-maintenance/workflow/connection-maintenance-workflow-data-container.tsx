import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {ConntectionMaintenanceData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/connection-maintenance-data";
import {Divider} from "antd";
import {MeterInformationContainer} from "../../../../shared/components/job-rule-base-components/mater-information-container";
import {ConnectionInformationContainer} from "../../../../shared/components/job-rule-base-components/connection-information-container";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import SubscriberManagementService, {GetSubscriptionPar} from "../../../../_services/subscribermanagment.service";
import {getTickFromDate} from "../../../../_helpers/date-util";

interface IProps{
    job : JobData
}
const subscriber_mgr_service = new SubscriberManagementService()

export function ConnectionMaintenanceWorkflowData(props:IProps){
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ connectionMaintenanceData,setConnectionMaintenanceData ] = useState<ConntectionMaintenanceData>()
    const [ subscription, setSubscription ] = useState<Subscription>()

    const dispatch = useDispatch()
    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:props.job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.CONNECTION_MAINTENANCE,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setConnectionMaintenanceData(workflowdata_state.payload as ConntectionMaintenanceData)
    },[ workflowdata_state ])
    useEffect(()=>{
        connectionMaintenanceData && fetchSubscription()
    },[connectionMaintenanceData])

    async function fetchSubscription(){
        try{
            if(connectionMaintenanceData){
                const subscriptionparams : GetSubscriptionPar = {
                    id:connectionMaintenanceData.connectionID,
                    sessionId:auth.sessionId,
                    version: getTickFromDate(new Date(props.job.statusDate))
                }
                const subscription = (await subscriber_mgr_service.GetSubscription(subscriptionparams)).data
                setSubscription(subscription)
            }
        } catch (e) {}
    }

    return (
        <Fragment>
            {
                subscription &&
                <ConnectionInformationContainer connection={subscription}/>
            }
            {
                connectionMaintenanceData !== undefined &&
                <Fragment>
                    {

                    }
                    {
                        connectionMaintenanceData.changeMeter &&
                        connectionMaintenanceData.meterData !== undefined &&
                        connectionMaintenanceData.meterData !== null &&
                        <Fragment>
                            <Divider orientation={'left'}>New Meter Detail</Divider>
                            <MeterInformationContainer meter={connectionMaintenanceData.meterData}/>
                        </Fragment>
                    }
                </Fragment>
            }
        </Fragment>
    )
}