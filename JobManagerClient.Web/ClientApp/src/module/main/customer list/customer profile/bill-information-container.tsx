import React,{ Fragment } from 'react'
import {CustomerBillDocument, PeriodicBill} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";

interface BillInformationContainerProps{
    bill: CustomerBillDocument
}
export function BillInformationContainer(props: BillInformationContainerProps){
    const periodicBill:PeriodicBill = props.bill as PeriodicBill
    return(
        <Fragment>
            <DataRow title={'Type'} content={props.bill.documentTypeID}/>
            <DataRow title={'Invoice NO.'} content={props.bill.invoiceNumber.reference}/>
            <DataRow title={'Period'} content={ (periodicBill.period !== null && periodicBill.period!== undefined) ? periodicBill.period.name : '' }/>
            <DataRow title={'Amount'} content={parseNumberToMoney(props.bill.total)}/>
        </Fragment>
    )
}