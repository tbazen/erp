import React, { Fragment } from "react";
import {CustomerBillDocument} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {CAlert} from "../../../../shared/core/calert/calert";
import {BillInformationContainer} from "./bill-information-container";
import {Col, Row} from "antd";

interface CustomerBillInformationProps{
    bills: CustomerBillDocument[]
}

export function CustomerBillsInformation(props: CustomerBillInformationProps){
    return (
        <Row gutter={4}>
            {
                props.bills.length === 0 &&
                <Col span={24}>
                    <CAlert message={'Customer has No Outstanding Bills'}/>
                </Col>
            }
            {
                props.bills.length > 0 &&
                props.bills.map((value,key)=>
                    <Col lg={{span:12}}>
                        <div className={'paper'} style={{padding:'10px',marginBottom:'5px'}}>
                            <BillInformationContainer bill={value}/>
                        </div>
                    </Col>
                )
            }
        </Row>
    )
}