import React , { Fragment } from 'react'
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import {CAlert} from "../../../../shared/core/calert/calert";
import {ConnectionInformationContainer} from "../../../../shared/components/job-rule-base-components/connection-information-container";
import {Col, Row} from "antd";

interface CustomerConnectionInformationProps{
    connections : Subscription[]
}
export function CustomerConnectionsInformation(props: CustomerConnectionInformationProps){
    return (
        <Row gutter={4}>
            {
                props.connections &&
                props.connections.length === 0 &&
                <Col span={24}>
                    <CAlert message={`Customer Doesn't have connection`}/>
                </Col>
            }
            {
                props.connections &&
                props.connections.length > 0 &&
                props.connections.map((value,key)=>
                    <Col lg={{span:12}}>
                        <div className={'paper'} style={{padding:'10px',marginBottom:'5px'}}>
                            <ConnectionInformationContainer connection={value}/>
                        </div>
                    </Col>
                )
            }
        </Row>
    )
}