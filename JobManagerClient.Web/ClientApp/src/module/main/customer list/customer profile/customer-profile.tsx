import React, { useEffect } from 'react'
import {Divider, Col, Row } from 'antd'
import { SubscriberSearchResult } from '../../../../_model/view_model/mn-vm/subscriber-search-result';
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from '../../../../_model/state-model/application-state';
import {fetchCustomerProfile} from '../../../../_redux_setup/actions/mn-actions/main-actions';
import {GetCustomerProfilePar} from "../../../../_services/subscribermanagment.service";
import {ConnectionProfileOptions} from "../../../../_model/client/customer-profile-builder";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {CustomerProfileState} from "../../../../_model/state-model/mn-sm/customer-profile-state";
import {SubscriberInformationContainer} from "../../../../shared/components/customer-information/customer-information";
import {JobInformationContainer} from "./job-information-container";
import {CustomerConnectionsInformation} from "./customer-connections-information";
import {CustomerBillsInformation} from "./customer-bills-information";
import {CustomerSchemesInformation} from "./customer-schemes-information";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {Link} from "react-router-dom";
import {ROUTES} from "../../../../_constants/routes";
import CButton from "../../../../shared/core/cbutton/cbutton";

interface IProps{
    customer : SubscriberSearchResult
}

const CustomerProfile = (props:IProps)=>{
  const [ auth, customerProfileState ]= useSelector<ApplicationState, [ AuthenticationState, CustomerProfileState  ]>((appState)=>[appState.auth,appState.customer_profile_state])
  const dispatch = useDispatch()
  useEffect(()=>{
      const customerProfilePar: GetCustomerProfilePar = {
          sessionId:auth.sessionId,
          customerId:props.customer.subscriber.id,
          options: ConnectionProfileOptions.All & ~ConnectionProfileOptions.ShowCustomerRef
      }
      dispatch(fetchCustomerProfile(customerProfilePar))
  },[])
  return (
    <StateMachineHOC state={customerProfileState}>
          {
               customerProfileState.payload &&
               <Row gutter={4}>
                   <Col sm={{span: 24}}>
                       <Divider orientation={'left'}>Customer Information</Divider>
                   </Col>
                   <Col sm={{ span:12 }}>
                       <div className={'paper'} style={{padding:'10px'}}>
                           <SubscriberInformationContainer {...customerProfileState.payload.customer}/>
                       </div>
                   </Col>

                   <Col span={24}> <Divider orientation={'left'}>Customer Connections</Divider> </Col>
                   {
                       customerProfileState.payload.connections &&
                       <Col sm={{ span:24 }}>
                          <CustomerConnectionsInformation connections={customerProfileState.payload.connections}/>
                       </Col>
                   }


                   <Col span={24}> <Divider orientation={'left'}>Outstanding Bills</Divider> </Col>
                   {
                       customerProfileState.payload.bills &&
                       <Col sm={{ span:24 }}>
                           <CustomerBillsInformation bills={customerProfileState.payload.bills}/>
                       </Col>
                   }

                   <Col span={24}> <Divider orientation={'left'}>Credit Schemes</Divider> </Col>
                   {
                       customerProfileState.payload.schemes &&
                       <Col sm={{ span:24 }}>
                           <CustomerSchemesInformation settlement={customerProfileState.payload.settlement} schemes={customerProfileState.payload.schemes}/>
                       </Col>
                   }
                   <Col span={24}> <Divider orientation={'left'}>Customer Service History</Divider> </Col>
                   {
                       customerProfileState.payload.jobs.map((jobData,key)=>
                           <Col key={key} sm={{span:12}}>
                               <div className={'paper'} style={{padding:'10px',marginBottom:'5px'}}>
                                    <JobInformationContainer job={jobData}/>
                                    <Divider/>
                                    <Link to={`${ROUTES.ACTIVE_JOBS}/${jobData.id}`}>
                                        <CButton type={'link'} >Show More</CButton>
                                    </Link>
                               </div>
                           </Col>
                       )
                   }
               </Row>
          }
      </StateMachineHOC>
  )
}


export default CustomerProfile