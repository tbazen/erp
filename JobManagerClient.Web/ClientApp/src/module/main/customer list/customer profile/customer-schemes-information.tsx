import React,{ Fragment } from "react";
import {CAlert} from "../../../../shared/core/calert/calert";
import {CreditScheme} from "../../../../_model/level0/subscriber-managment-type-library/credit-scheme";
import {SchemeInformationContainer} from "./scheme-information-container";
import {Col, Row} from "antd";

interface CustomerSchemesInformationProps{
    schemes: CreditScheme[]
    settlement: number[]
}

export function CustomerSchemesInformation(props: CustomerSchemesInformationProps){
    return (
        <Row gutter={4}>
            {
                props.schemes.length === 0 &&
                <Col span={24}>
                    <CAlert message={`Customer has no credit scheme`}/>
                </Col>
            }
            {
                props.schemes.length > 0 &&
                props.schemes.map((value,key)=>
                    <Col lg={{span:12}}>
                        <div className={'paper'} style={{padding:'10px',marginBottom:'5px'}}>
                            <SchemeInformationContainer key={key} settled={props.settlement[key]} scheme={value}/>
                        </div>
                    </Col>
                )
            }
        </Row>
    )
}