import React,{ Fragment } from "react";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {JobData} from "../../../../_services/job.manager.service";
import {getFormattedDateTime} from "../../../../_helpers/date-util";


interface JobInformationContainerProps{
    job: JobData
}

export function JobInformationContainer(props: JobInformationContainerProps){
    return (
        <Fragment>
            <DataRow title={'Job No.'} content={props.job.jobNo}/>
            <DataRow title={'Customer Id'} content={props.job.customerID}/>
            <DataRow title={'Description'} content={props.job.description}/>
            <DataRow title={'Status'} content={props.job.status}/>
            <DataRow title={'Application Type'} content={props.job.applicationType}/>
            <DataRow title={'Start Date'} content={ getFormattedDateTime(props.job.startDate)}/>
            <DataRow title={'Status Date'} content={getFormattedDateTime(props.job.statusDate)}/>
        </Fragment>
    )
}