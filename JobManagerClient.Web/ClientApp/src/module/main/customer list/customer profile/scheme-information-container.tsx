import React, {Fragment, useEffect, useState} from "react";
import {CreditScheme} from "../../../../_model/level0/subscriber-managment-type-library/credit-scheme";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";

interface SchemeInformationContainerProps{
    scheme : CreditScheme,
    settled : number
}
export function SchemeInformationContainer(props: SchemeInformationContainerProps){
    const [ status , setStatus ] = useState<string>('')
    useEffect(()=>{
        if(props.scheme.creditedAmount === props.settled)
            setStatus('Fully Billed')
        else if(props.scheme.creditedAmount < props.settled)
            setStatus('Over Billed')
        if(!props.scheme.active)
            setStatus(`${status} Closed`)
    },[])
    return (
        <Fragment>
            <DataRow title={'Date'} content={props.scheme.issueDate}/>
            <DataRow title={'Credit Amount'} content={parseNumberToMoney(props.scheme.creditedAmount)}/>
            <DataRow title={'Monthly Payment'} content={parseNumberToMoney(props.scheme.paymentAmount)}/>
            <DataRow title={'Bill Amount'} content={props.settled}/>
            <DataRow title={'Status'} content={status}/>
        </Fragment>
    )
}