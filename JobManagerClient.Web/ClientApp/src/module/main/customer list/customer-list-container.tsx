import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions";
import CustomersListTable from "../../../shared/components/customer-list-table/customer-list-table";
import CustomerAdvancedSearchBar from "../../../shared/components/customer-list-table/customer-advanced-search-bar";
import {ApplicationState} from "../../../_model/state-model/application-state";
import { subscriberListClear } from '../../../_redux_setup/actions/mn-actions/main-actions';
import {
    fetchDMZList,
    fetchKebeleList,
    fetchPressureZoneList
} from "../../../_redux_setup/actions/mn-actions/main-actions";
import {useBreadcrumb} from "../../../shared/hooks/manage-breadcrumb";
import {useReduxStoreCleaner} from "../../../shared/hooks/clean-up";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.MAIN.CUSTOMER_LIST,
        breadcrumbName:'Customer List'
    }
]


const CustomerListContainer = ()=>{
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const [pageIndex,setPageIndex] = useState<number>(1)

    useBreadcrumb(backwardPath)
    useReduxStoreCleaner([subscriberListClear])
    useEffect(()=>{
        dispatch(fetchKebeleList({sessionId:auth.sessionId}))
        dispatch(fetchPressureZoneList({sessionId:auth.sessionId}))
        dispatch(fetchDMZList({sessionId:auth.sessionId}))
    },[])


    return (
        <Fragment>
            <CustomerAdvancedSearchBar pageNumber={pageIndex}/>
            <CustomersListTable
                               paginationHandler={setPageIndex}
                               withAllColumns/>
        </Fragment>
    )
}

export default CustomerListContainer