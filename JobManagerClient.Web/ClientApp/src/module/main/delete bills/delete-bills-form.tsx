import React, {useEffect, useState} from 'react'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import { SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {Checkbox, Divider, Table, Typography} from 'antd';
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information';
import { useDispatch, useSelector } from 'react-redux';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { AuthenticationState } from '../../../_model/state-model/auth-state';
import {AddJobWebPar, JobData, UpdateJobWebPar} from '../../../_services/job.manager.service';
import { initialJobData } from '../../../_helpers/initial value/init-jobdata';
import {
    fetchCustomerBillDocuments,
    requestAddJobWeb, requestUpdateJobWeb
} from '../../../_redux_setup/actions/mn-actions/main-actions';
import CButton from '../../../shared/core/cbutton/cbutton';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {BillDocumentsState, IBillDocumentPayload} from "../../../_model/state-model/mn-sm/bill-documents-state";
import {GetBillDocumentsPar} from "../../../_services/subscribermanagment.service";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {DeleteBillsData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/delete-bills-data";
import { useParams } from 'react-router-dom';
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";

const { Column } = Table;
const {Title } = Typography

interface IProps{ data? : SubscriberSearchResult }

const DeleteBillsForm = (props:IProps) => {
    const dispatch = useDispatch()
    const { jobId } = useParams()
    const [ auth,customerBillState,workflowdata_state] = useSelector<ApplicationState, [AuthenticationState,BillDocumentsState,WorkflowDataState]>(appState => [appState.auth,appState.bill_document_state,appState.workflow_data_state])
    const [ jobData, setJobData] = useState<JobData>({...initialJobData.job,id:(jobId && !isNaN(+jobId) && +jobId)||-1,customerID:(props.data && props.data.subscriber.id)||-1 ,applicationType:StandardJobTypes.BATCH_DELETE_BILLS})
    const [ deleteBillData, setDeleteBillData ] = useState<DeleteBillsData>(new DeleteBillsData())
    const [ registeredJob, setRegisteredJob ] = useEditJobInitializer({jobId:(jobId && +jobId)||-1,jobType:StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER })
    let index = 0;
    useEffect(()=>{
        const billDocumentParams: GetBillDocumentsPar = {
            sessionId: auth.sessionId,
            mainTypeID: -1,
            customerID: jobData.customerID,
            periodID: -1,
            connectionID: -1,
            excludePaid: true
        }
        dispatch(fetchCustomerBillDocuments(billDocumentParams))
    },[jobData.customerID])
    useEffect(()=>{
        if(registeredJob)
            setJobData(registeredJob)
    },[ registeredJob ])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setDeleteBillData(workflowdata_state.payload as DeleteBillsData)
    },[ workflowdata_state ])
    const submitDeleteBillJobData = () => {
        if(jobData.id === -1){
            const jobParams: AddJobWebPar<DeleteBillsData> ={
                job:jobData,
                sessionId:auth.sessionId,
                dataType:NAME_SPACES.WORKFLOW.DELETE_BILLS_DATA,
                data:deleteBillData
            }
            dispatch(requestAddJobWeb(jobParams))
        } else {
            const updateJobParams: UpdateJobWebPar<DeleteBillsData> = {
                job: jobData,
                sessionId: auth.sessionId,
                dataType: NAME_SPACES.WORKFLOW.DELETE_BILLS_DATA,
                data: deleteBillData
            }
            dispatch(requestUpdateJobWeb<DeleteBillsData>(updateJobParams))
        }
    }

    return (
        <div>
            <Title level={4}>Customer Information</Title>
            <Divider/>
            { props.data && <CustomerInformationContainer {...props.data}/> }
            <br/>
            <br/>
            <SharedJobFormFields
                                dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date})}
                                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                            />
            <Table
                dataSource={customerBillState.payload}
                size={'small'}
                loading={customerBillState.loading}
                pagination={false}
            >
                <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }}/>
                <Column
                    title="Select"
                    key="selectScheme"
                    render={(text , rowData:IBillDocumentPayload)=>{
                        return (
                            <Checkbox onChange={
                                (event)=>{
                                    if(event.target.checked){
                                        setDeleteBillData({...deleteBillData,deletedBills:[...deleteBillData.deletedBills,rowData.billDocument]})
                                    } else{
                                        setDeleteBillData({...deleteBillData,deletedBills:[...deleteBillData.deletedBills.filter(bill=>bill.accountDocumentID !== rowData.billDocument.accountDocumentID)]})
                                    }
                                }}
                            />)}}
                />
                <Column title="Type" dataIndex="documentType.name" key="type" />
                <Column
                    title="Date"
                    dataIndex="billDocument.documentDate"
                    key="date"
                    render={(text)=><p>{ new Date(text).toLocaleDateString() }</p>}
                />
                <Column title="Amount" dataIndex="billDocument.total" key="amount" />
            </Table>

            <div style={{ width:'50%' ,marginTop:'5px'}} >
                <CButton block onClick={submitDeleteBillJobData} type={'primary'}>Submit</CButton>
            </div>  
        </div>
    )
}

export default DeleteBillsForm