import React from 'react'
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import DeleteBillsContainer from "./delete-bills-container";
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {ROUTES} from "../../../_constants/routes";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {JobData} from "../../../_services/job.manager.service";
import {DeleteBillsWorkflowDataContainer} from "./workflow/delete-bill-workflow-data-container";
import DeleteBillsForm from "./delete-bills-form";

@JobRuleClientDecorator({
    jobName:"Delete Bills",
    route:ROUTES.JOB.BATCH_DELETE_BILLS,
    jobType:StandardJobTypes.BATCH_DELETE_BILLS,
    innerRoutes: [
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.BATCH_DELETE_BILLS].ABSOLUTE,
            component: DeleteBillsForm
        }
    ]
})
export class DeleteBillsRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return DeleteBillsContainer
    }

    public getWorkFlowData(job:JobData){
        return <DeleteBillsWorkflowDataContainer job={job}/>
    }
}