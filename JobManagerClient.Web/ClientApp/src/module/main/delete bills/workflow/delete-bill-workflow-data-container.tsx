import React, { useEffect, useState} from 'react'
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {DeleteBillsData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/delete-bills-data";
import AccountingService, {GetDocumentHTMLPar} from "../../../../_services/accounting.service";
import {HTMLParser} from "../../../../shared/components/html-parser/html-parser";

const accounting_service = new AccountingService()

interface IProps{
    job : JobData
}

export function DeleteBillsWorkflowDataContainer(props:IProps) {
    const [auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth, appState.workflow_data_state])
    const [deleteBillData, setDeleteBillData] = useState<DeleteBillsData>()
    const [billDocumentHTML, setBillDocumentHTML ] = useState<string>('')
    const dispatch = useDispatch()
    useEffect(() => {
        const params: GetWorkFlowDataWebPar =
            {
                key: 0,
                jobID: props.job.id,
                sessionId: auth.sessionId,
                typeID: StandardJobTypes.BATCH_DELETE_BILLS,
                fullData: true
            }
        dispatch(fetchWorkflowData(params))
    }, [])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setDeleteBillData(workflowdata_state.payload as DeleteBillsData)
    },[ workflowdata_state ])
    useEffect(()=>{
        deleteBillData !==undefined &&
        fetchBillDocumentHTML()
    },[deleteBillData])
    async function fetchBillDocumentHTML(){
        try{
            if(deleteBillData){
                let html = ''
                for(const doc of deleteBillData.deletedBills){
                    const param: GetDocumentHTMLPar = {
                        sessionId:auth.sessionId,
                        accountDocumentID: doc.accountDocumentID
                    }
                    const htmlFragment = (await accounting_service.GetDocumentHTML(param)).data
                    html+=htmlFragment;
                }
                setBillDocumentHTML(html)
            }
        } catch (e) {}
    }
    return ( <HTMLParser htmlString={billDocumentHTML}/> )
}