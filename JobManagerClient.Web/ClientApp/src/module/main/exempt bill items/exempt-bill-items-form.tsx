import {Checkbox, Divider, Select, Table, Typography} from 'antd';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import CFormItem from '../../../shared/core/cform-item/cform-item';
import {initialJobData} from '../../../_helpers/initial value/init-jobdata';
import {ApplicationState} from '../../../_model/state-model/application-state';
import {AuthenticationState} from '../../../_model/state-model/auth-state';
import {SubscriberSearchResult} from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {
    fetchCustomerBillDocuments,
    requestAddJobWeb,
    requestUpdateJobWeb
} from '../../../_redux_setup/actions/mn-actions/main-actions';
import {AddJobWebPar, JobData, UpdateJobWebPar} from '../../../_services/job.manager.service';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import SubscriberManagementService, { GetBillDocumentsPar, GetCustomerBillRecordPar } from "../../../_services/subscribermanagment.service";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {ExemptBillItemsData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/exempt-bill-items-data";
import {BillDocumentsState, IBillDocumentPayload} from "../../../_model/state-model/mn-sm/bill-documents-state";
import {BillItem} from "../../../_model/level0/subscriber-managment-type-library/bill";
import {NotificationTypes, openNotification} from "../../../shared/components/notification/notification";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import { useParams } from 'react-router-dom';
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";


const { Column } = Table
const { Title } = Typography
const { Option } = Select

const subscrier_mgr_service = new SubscriberManagementService()

interface IProps { data?: SubscriberSearchResult }


const ExemptBillItemsForm = (props: IProps) => {
    const dispatch = useDispatch()
    const { jobId } = useParams()
    const [auth, customerBillDocumentState, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, BillDocumentsState, WorkflowDataState]>(appState => [appState.auth,appState.bill_document_state,appState.workflow_data_state])
    const [jobData, setJobData] = useState<JobData>({ ...initialJobData.job,id: (jobId && !isNaN(+jobId) && +jobId)||-1, customerID: (props.data && props.data.subscriber.id)||-1, applicationType: StandardJobTypes.EXEMPT_BILL_ITEMS })
    const [ exemptBillItem , setExemptBillItem ] = useState<ExemptBillItemsData>(new ExemptBillItemsData())
    const [ serviceItems , setServiceItems ] = useState<BillItem[]>([])
    const [ selectedBillDocument , setSelectedBillDocument ] = useState<IBillDocumentPayload>()
    const [ registeredJob, setRegisteredJob ] = useEditJobInitializer({jobId:(jobId && +jobId)||-1,jobType:StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER })

    useEffect(()=>{
        if(registeredJob)
            setJobData(registeredJob)
    },[ registeredJob ])

    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setExemptBillItem(workflowdata_state.payload as ExemptBillItemsData)
    },[ workflowdata_state ])

    let index = 0;
    useEffect(()=>{
        const billDocumentParams: GetBillDocumentsPar = {
            sessionId: auth.sessionId,
            mainTypeID: -1,
            customerID: jobData.customerID,
            periodID: -1,
            connectionID: -1,
            excludePaid: true
        }
        dispatch(fetchCustomerBillDocuments(billDocumentParams))
    },[jobData.customerID])

    const submitExemptBillItemJob = async () => {
        try{
            if(selectedBillDocument) {
                openNotification({
                    message: 'Loading',
                    description: 'Getting customer record',
                    notificationType: NotificationTypes.LOADING
                })
                const recordParams: GetCustomerBillRecordPar = {
                    sessionId: auth.sessionId,
                    billRecordID: selectedBillDocument.billDocument.accountDocumentID
                }
                const customerRecord = (await subscrier_mgr_service.GetCustomerBillRecord(recordParams)).data

                if(jobData.id === -1){
                    const jobParams: AddJobWebPar<ExemptBillItemsData> = {
                        job: jobData,
                        sessionId: auth.sessionId,
                        dataType: NAME_SPACES.WORKFLOW.EXEMPT_BILL_ITEMS_DATA,
                        data: {...exemptBillItem,exemptedBill:customerRecord}
                    }
                    dispatch(requestAddJobWeb(jobParams))
                } else {
                    const updateJobWebPar: UpdateJobWebPar<ExemptBillItemsData> = {
                        job: jobData,
                        sessionId: auth.sessionId,
                        dataType: NAME_SPACES.WORKFLOW.EXEMPT_BILL_ITEMS_DATA,
                        data: {...exemptBillItem,exemptedBill:customerRecord}
                    }
                    dispatch(requestUpdateJobWeb(updateJobWebPar))
                }

            }
        } catch(e){
            openNotification({
                message:'Errno',
                description:'Failed to get customer bill record',
                notificationType:NotificationTypes.ERROR
            })
        }
    }
    function handleSelectBillItem(accountDocumentId:string){
        const document = customerBillDocumentState.payload.find(bill=> bill.billDocument.accountDocumentID === +accountDocumentId)
        if(document){
            setServiceItems(document.billDocument.itemsLessExemption)
            setSelectedBillDocument(document)
        }
    }

    return (
        <div>
            <Title level={4}>Customer Information</Title>
            <Divider />
            { props.data && <CustomerInformationContainer {...props.data} /> }
            <br />
            <br />
            <SharedJobFormFields
                dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
            />
            <CFormItem
                input={
                    <Select
                        placeholder="Bill"
                        style={{ width: '100%' }}
                        size={'large'}
                        onChange={handleSelectBillItem}
                    >
                        {
                            customerBillDocumentState.payload.map((bill,key)=><Option key={key} value={bill.billDocument.accountDocumentID}>{ bill.documentType.name }</Option>)
                        }
                    </Select>
                }
                label={'Bill'}
                errorMessage={'Please select bill'}
                required
            />


            <Table
                dataSource={serviceItems}
                size={'small'}
                loading={customerBillDocumentState.loading}
                pagination={false}
            >
                <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }}/>
                <Column
                    title="Select"
                    key="selectScheme"
                    render={(text , rowData:BillItem)=>
                    {
                        console.log('ROW DATA',rowData)
                        return(<Checkbox onChange={
                        (event)=>{
                            if(event.target.checked){
                                setExemptBillItem({...exemptBillItem,exemptedAmounts:[ ...exemptBillItem.exemptedAmounts, rowData.price ],exemptedItems:[ ...exemptBillItem.exemptedItems,rowData.itemTypeID ] })
                            } else{
                                //TODO:
                            }
                        }}  />)}}
                />
                <Column title="Item" dataIndex="description" key="type" />
                <Column title="Amount" dataIndex="price" key="amount" />
            </Table>


            <div style={{ width: '50%' ,marginTop:'5px'}} >
                <CButton block onClick={submitExemptBillItemJob} type={'primary'} >Submit</CButton>
            </div>
        </div>
    )
}

export default ExemptBillItemsForm