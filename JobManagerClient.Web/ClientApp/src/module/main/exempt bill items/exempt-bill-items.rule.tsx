import React from 'react'
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {ROUTES} from "../../../_constants/routes";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import ExemptBillItemsContainer from "./exempt-bill-items";
import {JobData} from "../../../_services/job.manager.service";
import {ExemptBillItemsWorkflowDataContainer} from "./workflow/exempt-bill-items-workflow-data-container";

@JobRuleClientDecorator({
    jobName:"Exempt Bill Items",
    route:ROUTES.JOB.EXEMPT_BILL_ITEMS,
    jobType:StandardJobTypes.EXEMPT_BILL_ITEMS
})
export class ExemptBillItemsRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return ExemptBillItemsContainer
    }

    public getWorkFlowData(job: JobData){
        return <ExemptBillItemsWorkflowDataContainer job={job}/>
    }
}