import React, {Fragment,useEffect, useState} from 'react'
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {ExemptBillItemsData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/exempt-bill-items-data";
import AccountingService, {GetAccountDocumentPar, GetDocumentHTMLPar} from "../../../../_services/accounting.service";
import {CustomerBillDocument} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {HTMLParser} from "../../../../shared/components/html-parser/html-parser";

const accounting_service = new AccountingService()

interface IProps{
    job : JobData
}

export function ExemptBillItemsWorkflowDataContainer(props:IProps) {
    const [auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth, appState.workflow_data_state])
    const [exemptBillData, setExemptBillData] = useState<ExemptBillItemsData>()
    const [billDocumentHTML, setBillDocumentHTML ] = useState<string>('')

    const dispatch = useDispatch()
    useEffect(() => {
        const params: GetWorkFlowDataWebPar =
            {
                key: 0,
                jobID: props.job.id,
                sessionId: auth.sessionId,
                typeID: StandardJobTypes.EXEMPT_BILL_ITEMS,
                fullData: true
            }
        dispatch(fetchWorkflowData(params))
    }, [])
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setExemptBillData(workflowdata_state.payload as ExemptBillItemsData)
    }, [workflowdata_state])

    useEffect(()=>{
        exemptBillData !==undefined &&
        fetchBillDocumentHTML()
    },[exemptBillData])
    async function fetchBillDocumentHTML(){
        try{
            if(exemptBillData){
                const docParams: GetAccountDocumentPar = {
                    sessionId:auth.sessionId,
                    documentID:exemptBillData.exemptedBill.id,
                    fullData:true
                }
                const document : CustomerBillDocument = (await accounting_service.GetAccountDocument(docParams)).data
                const htmlParams: GetDocumentHTMLPar ={
                    accountDocumentID:document.accountDocumentID,
                    sessionId:auth.sessionId
                }
                const html: string = ( await accounting_service.GetDocumentHTML(htmlParams)).data
                setBillDocumentHTML(html)
            }
        } catch (e) {}
    }

    return (
        <HTMLParser htmlString={billDocumentHTML}/>
    )

}