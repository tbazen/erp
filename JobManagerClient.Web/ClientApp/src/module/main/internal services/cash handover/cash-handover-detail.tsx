import React, {useEffect, useState} from 'react'
import {IBreadcrumb} from "../../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../../_constants/routes";
import {useDispatch, useSelector} from "react-redux";
import CashHandoverForm, {BankDepositFormProps} from "./cash-handover-form";
import {Checkbox, Col, Row, Table} from "antd";
import {ApplicationState} from '../../../../_model/state-model/application-state';
import {PaymentCenter} from '../../../../_model/view_model/mn-vm/internal-service-vm/cash-handover/payment-center';
import {AuthenticationState} from '../../../../_model/state-model/auth-state';
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import SubscriberManagementService, {GetPaymentCenterByCashAccountPar} from "../../../../_services/subscribermanagment.service";
import JobManagerService, {
    AddJobWebPar,
    GetLastCashHandoverDocumentPar,
    GetPaymentCenterPaymentInstrumentsPar,
    JobData, UpdateJobWebPar
} from "../../../../_services/job.manager.service";
import {CashHandoverDocument} from "../../../../_model/level0/job-manager-type-library/cash-handover-document";
import BNFinanceService, {
    GetBankBranchInfoPar,
    GetPaymentInstrumentTypePar,
    GetSystemParametersWebPar
} from "../../../../_services/bn.finance.service";
import {
    BankBranchInfo,
    PaymentInstrumentItem,
    PaymentInstrumentType
} from "../../../../_model/level0/iERP-transaction-model/payment-instrument-item";
import {
    getFormattedDate,
    getFormattedLocalDate,
    getLocalDateString
} from "../../../../_helpers/date-util";
import {NullDateValueString} from "../../../../_model/level0/intaps.dotnet/date-tool";
import {useParams} from 'react-router-dom';
import CButton from "../../../../shared/core/cbutton/cbutton";
import {IStateMachineBase} from "../../../../_model/state-model/state-machine-base";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";
import {CSpinProps} from "../../../../shared/core/cspin/cspin";
import {initialJobData} from "../../../../_helpers/initial value/init-jobdata";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {NotificationTypes, openNotification} from "../../../../shared/components/notification/notification";
import {CashHandoverData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/cash-handover-data";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {requestAddJobWeb, requestUpdateJobWeb} from "../../../../_redux_setup/actions/mn-actions/main-actions";

const { Column } = Table

const subscriber_mgr_service = new SubscriberManagementService()
const job_mgr_service = new JobManagerService()
const ierp_transaction_service = new BNFinanceService()

interface ITableData{
    isAmountIncluded: boolean
    paymentInstrumentItem: PaymentInstrumentItem,
    paymentInstrumentType: PaymentInstrumentType,
    bankBranchInfo?: BankBranchInfo
}


interface CashHandoverDetailState extends IStateMachineBase {
    instrumentTypeItemCode?:string
    cashHandoverDocument?: CashHandoverDocument
    paymentCenter?: PaymentCenter
    handoverFrom?:string
    handoverUpto?:string
    noneCashAmount?: number
    cashAmount?: number
    totalAmount?:number
}

const initCashHandoverDetailState:CashHandoverDetailState = { loading:false,error:false,message:'' }
const loadingCashHandoverDetailState:CashHandoverDetailState = {...initCashHandoverDetailState,loading:true}
const errorCashHandoverDetailState: CashHandoverDetailState = {...initCashHandoverDetailState,error:true}
const successCashHandoverDetailState: CashHandoverDetailState = {...initCashHandoverDetailState}

interface TableDataState extends IStateMachineBase {
    payload: ITableData[]
}
const initTableDataState : TableDataState = {loading:false,error:false,message:'',payload:[] }
const loadingTableDataState : TableDataState = {...initTableDataState,loading:true }
const errorTableDataState : TableDataState = {...initTableDataState,error:true }
const successTableDataState : TableDataState = {...initTableDataState }


const CashHandoverDetail = () => {
    const {paymentCenterId}= useParams()
    const dispatch = useDispatch()
    const [auth] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])
    const [cashHandoverDetailState, setCashHandoverDetailState] = useState<CashHandoverDetailState>({...initCashHandoverDetailState})
    const [ tableDataState,setTableDataState ] = useState<TableDataState>({...initTableDataState})
    const [ depositInfo, setDepositInfo ] = useState<BankDepositFormProps>()
    const [ handoverAmount, setHandoverAmount ] = useState<number>(0)

    const [jobData, setJobData] = useState<JobData>({
        ...initialJobData.job,
        logDate:getLocalDateString(),
        startDate:getLocalDateString(),
        statusDate:getLocalDateString(),
        applicationType: StandardJobTypes.CASH_HANDOVER})
    const backwardPath: IBreadcrumb[] = [
        {
            path: ROUTES.MAIN.INDEX,
            breadcrumbName: 'Tools'
        },
        {
            path: ROUTES.JOB.INTERNAL_SERVICE.INDEX,
            breadcrumbName: 'Internal Services'
        },
        {
            path: ROUTES.JOB.INTERNAL_SERVICE.CASH_HANDOVER,
            breadcrumbName: 'Cash Handover'
        },
        {
            path: '',
            breadcrumbName: paymentCenterId||''
        }
    ]
    useBreadcrumb(backwardPath)
    useEffect(() => {
        setCashAccount()
    }, [])
    useEffect(()=>{
        cashHandoverDetailState.paymentCenter &&
        !cashHandoverDetailState.loading &&
        !cashHandoverDetailState.error &&
        loadLedger()
    },[ cashHandoverDetailState.paymentCenter ])
    useEffect(()=>{
        cashHandoverDetailState.noneCashAmount &&
        cashHandoverDetailState.cashAmount &&
        setCashHandoverDetailState({...cashHandoverDetailState,totalAmount:(cashHandoverDetailState.noneCashAmount+cashHandoverDetailState.cashAmount)})
    },[ cashHandoverDetailState.noneCashAmount,cashHandoverDetailState.cashAmount ])


    async function setCashAccount(){
        try{
            setCashHandoverDetailState({...loadingCashHandoverDetailState})
            setTableDataState({...loadingTableDataState})
            if(paymentCenterId) {
                const cashAccountPar: GetPaymentCenterByCashAccountPar = {
                    sessionId: auth.sessionId,
                    account: +paymentCenterId
                }
                let paymentCenter: PaymentCenter | undefined = (await subscriber_mgr_service.GetPaymentCenterByCashAccount(cashAccountPar)).data
                if (!paymentCenter)
                    paymentCenter = undefined

                let cashHandoverDocument: CashHandoverDocument | undefined
                let handoverFrom: string | undefined
                let handoverUpto: string | undefined = new Date().toDateString()
                if(paymentCenter){
                    const cashHandoverDocPar: GetLastCashHandoverDocumentPar = {
                        sessionId: auth.sessionId,
                        cashAccountID: paymentCenter.summaryAccountID
                    }
                    cashHandoverDocument = (await job_mgr_service.GetLastCashHandoverDocument(cashHandoverDocPar)).data
                    if (!cashHandoverDocument){
                        cashHandoverDocument = undefined
                        handoverFrom = undefined
                    }
                    else{
                        handoverFrom = getFormattedDate(cashHandoverDocument.documentDate.toString())
                    }
                }
                setCashHandoverDetailState({
                    ...successCashHandoverDetailState,
                    cashHandoverDocument,
                    paymentCenter,
                    handoverFrom,
                    handoverUpto
                })
            }
        } catch(error){
            setCashHandoverDetailState({...errorCashHandoverDetailState,message:error.message})
        }
    }
    async function loadLedger(){
        try{
            if(!cashHandoverDetailState.paymentCenter)
                return;
            let tableDataTemp:ITableData[] = []
            const systemParameterPar:GetSystemParametersWebPar = {
                sessionId:auth.sessionId,
                names:["cashInstrumentCode"]
            }
            const cashInst: string = ( await ierp_transaction_service.GetSystemParametersWeb(systemParameterPar)).data[0]
            const from:string = cashHandoverDetailState.cashHandoverDocument?  cashHandoverDetailState.cashHandoverDocument.documentDate.toString() : NullDateValueString
            const to: string = getLocalDateString()
            let instrumentsPar: GetPaymentCenterPaymentInstrumentsPar={
                sessionId:auth.sessionId,
                cashAccountID:cashHandoverDetailState.paymentCenter.casherAccountID,
                from,
                to
            }
            const instruments1: PaymentInstrumentItem[] = ( await job_mgr_service.GetPaymentCenterPaymentInstruments(instrumentsPar) ).data
            instrumentsPar = {...instrumentsPar,cashAccountID:cashHandoverDetailState.paymentCenter.summaryAccountID}
            const instruments2: PaymentInstrumentItem[] = ( await job_mgr_service.GetPaymentCenterPaymentInstruments(instrumentsPar) ).data
            const instruments: PaymentInstrumentItem[] = [...instruments1,...instruments2 ]
            let cashAmount:number = 0
            let noneCashAmount:number = 0
            for(const instrument of instruments){
                if(instrument.instrumentTypeItemCode === cashInst){
                    cashAmount+=instrument.amount
                }
                else{
                   noneCashAmount+=instrument.amount
                   const paymentInstrumentTypePar: GetPaymentInstrumentTypePar ={
                       sessionId:auth.sessionId,
                       itemCode:instrument.instrumentTypeItemCode
                   }
                   const paymentInstrumentType:PaymentInstrumentType = ( await ierp_transaction_service.GetPaymentInstrumentType(paymentInstrumentTypePar)).data
                   let bankBranchInfo : BankBranchInfo | undefined =  undefined
                   if(instrument.bankBranchID !== -1){
                        const bankBranchPar :GetBankBranchInfoPar = {
                            sessionId:auth.sessionId,
                            branchID:instrument.bankBranchID
                        }
                        bankBranchInfo = (await ierp_transaction_service.GetBankBranchInfo(bankBranchPar)).data
                   }
                   const tableRow:ITableData = {
                       isAmountIncluded: true,
                       paymentInstrumentItem:instrument,
                       paymentInstrumentType,
                       bankBranchInfo
                   }
                   tableDataTemp = [...tableDataTemp,tableRow]
                }
            }
            const totalAmount = cashAmount + noneCashAmount
            setCashHandoverDetailState({...cashHandoverDetailState,cashAmount,noneCashAmount,totalAmount,instrumentTypeItemCode: cashInst})
            setTableDataState({...successTableDataState,payload:[...tableDataTemp]})

        }catch(error){
            setTableDataState({...errorTableDataState})
        }
    }
    function handleDepositInfoChange(newDepositInfo: BankDepositFormProps){
        setDepositInfo(newDepositInfo)
    }
    function handleHandOverAmountChange(newAmount: number) {
        setHandoverAmount(newAmount)
        setCashHandoverDetailState({...cashHandoverDetailState,cashAmount:newAmount})
    }
    function handleTableDataChange(isAmountIncluded: boolean, index: number){
        const tablePayload = tableDataState.payload
        tablePayload[index].isAmountIncluded = isAmountIncluded
        setTableDataState({...tableDataState,payload:tablePayload})
        if(isAmountIncluded)
            setCashHandoverDetailState({...cashHandoverDetailState,noneCashAmount:(cashHandoverDetailState.noneCashAmount || 0)+tablePayload[index].paymentInstrumentItem.amount})
        else
            setCashHandoverDetailState({...cashHandoverDetailState,noneCashAmount:(cashHandoverDetailState.noneCashAmount || 0)-tablePayload[index].paymentInstrumentItem.amount})
    }
    function handleSubmitCashHandoverData(){
        try{
            if(!cashHandoverDetailState.paymentCenter)
                return
            if(depositInfo && depositInfo.isDepositEnabled){
                if(depositInfo.depositReference.length=== 0 )
                    throw new Error('Please Enter bank deposit slip number')
                //TODO: ADD DATE VALIDATION
                if(depositInfo.bankAccountId === -1)
                    throw new Error('Please select deposit bank account')
            }
            if(handoverAmount === 0)
                throw new Error('Please Enter valid cash amount')

            const cashItem: PaymentInstrumentItem = new PaymentInstrumentItem()
            cashItem.instrumentTypeItemCode = cashHandoverDetailState.instrumentTypeItemCode || ''
            cashItem.amount = handoverAmount

            const cashHandoverData: CashHandoverData = new CashHandoverData()
            cashHandoverData.receiptNumber = null
            cashHandoverData.instruments = [...tableDataState.payload.map((value)=>value.paymentInstrumentItem),cashItem ]
            cashHandoverData.sourceCashAccountID = cashHandoverDetailState.paymentCenter.summaryAccountID
            cashHandoverData.destinationAccountID = (depositInfo && depositInfo.bankAccountId) || -1
            cashHandoverData.bankDespoit = (depositInfo && depositInfo.isDepositEnabled) || false
            cashHandoverData.bankDespositDate = (depositInfo && depositInfo.depositDate) || getLocalDateString()
            cashHandoverData.bankDespositReference = (depositInfo && depositInfo.depositReference) || ''
            if(jobData.id === -1) {
                const addJobWebPar: AddJobWebPar<CashHandoverData> = {
                    job:jobData,
                    dataType:NAME_SPACES.WORKFLOW.CASH_HANDOVER_DATA,
                    data:cashHandoverData,
                    sessionId:auth.sessionId
                }
                dispatch(requestAddJobWeb<CashHandoverData>(addJobWebPar))
            } else{
                const updateJobWebPar: UpdateJobWebPar<CashHandoverData> = {
                    job:jobData,
                    dataType:NAME_SPACES.WORKFLOW.CASH_HANDOVER_DATA,
                    data:cashHandoverData,
                    sessionId:auth.sessionId
                }
                dispatch(requestUpdateJobWeb<CashHandoverData>(updateJobWebPar))
            }
        } catch (e) {
            openNotification({
                message:'ERRNO',
                description:e.message,
                notificationType:NotificationTypes.ERROR
            })
        }
    }

    return (
        <Row gutter={4}>
            <Col lg={{span:16}}>
                <div className={'paper'} style={{marginBottom:'10px'}}>
                    <Table
                        dataSource={tableDataState.payload}
                        loading={{...CSpinProps,spinning:tableDataState.loading}}
                        size="small"
                        rowKey={(record,index)=>index.toString()}
                        scroll={{ y: '80vh' }}
                        bordered={false}
                        pagination={{pageSize:20}}
                    >
                        <Column
                            title=''
                            key ='paymentIncluded'
                            render = {(text,record:ITableData,index)=><Checkbox checked={record.isAmountIncluded} onChange={(e)=>{ handleTableDataChange(e.target.checked,index) }} />}
                            width={'2%'}/>
                        <Column
                            title='Type'
                            dataIndex='paymentInstrumentType.name'
                            key = 'paymentInstrumentType.name'
                            width={'15%'}/>
                        <Column
                            title='Number'
                            key= 'paymentInstrumentItem.documentReference'
                            render={(text,record:ITableData)=><span style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>{record.paymentInstrumentItem.documentReference}</span>}
                            width={'23%'}/>
                        <Column
                            title='Date'
                            key='paymentInstrumentItem.documentDate'
                            render={(text,rowData:ITableData)=><span>{getFormattedLocalDate(rowData.paymentInstrumentItem.documentDate)}</span>}
                            width={'12%'}/>
                        <Column
                            title='Amount'
                            key='paymentInstrumentItem.amount'
                            render={(text,rowData:ITableData)=><span>{parseNumberToMoney(rowData.paymentInstrumentItem.amount)}</span>}
                            width={'15%'}/>
                        <Column title='Bank Name' dataIndex='bankBranchInfo.name' key = 'bankBranchInfo.name' width={'18%'}/>
                        <Column title='Account' dataIndex='paymentInstrumentItem.accountNumber' key = 'paymentInstrumentItem.accountNumber' width={'15%'} />
                    </Table>
                </div>
            </Col>

            <Col lg={{span:8}}>
                <CashHandoverForm
                    handoverAmount={cashHandoverDetailState.cashAmount || 0}
                    handleCashHandoverChange={handleHandOverAmountChange}
                    handleDepositInfoChange={handleDepositInfoChange}

                    handoverFrom={cashHandoverDetailState.handoverFrom}
                    handoverUpto={cashHandoverDetailState.handoverUpto}
                    cashAmount={cashHandoverDetailState.cashAmount}
                    noneCashAmount={cashHandoverDetailState.noneCashAmount}
                    total={cashHandoverDetailState.totalAmount}
                />
                <div className={'paper'} style={{padding:'10px'}} >
                    <CButton onClick={handleSubmitCashHandoverData} type="primary" block>Submit</CButton>
                </div>
            </Col>
        </Row>
    )
}

export default CashHandoverDetail