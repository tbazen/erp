import React, {useEffect, useState} from 'react'
import { Checkbox, Col, Input, Row, Select, DatePicker, Divider} from "antd";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import FormItem from "antd/es/form/FormItem";
import BNFinanceService from "../../../../_services/bn.finance.service";
import {useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {
    BankInfo,
    PaymentInstrumentType
} from "../../../../_model/level0/iERP-transaction-model/payment-instrument-item";
import {BankAccountInfo} from "../../../../_model/level0/iERP-transaction-model/bank-account-info";

const { Option } = Select

export interface BankDepositFormProps{
    isDepositEnabled: boolean
    depositReference: string
    depositDate: string
    bankAccountId: number
}
const initBankDepositForm: BankDepositFormProps ={
    isDepositEnabled:false,
    depositReference:'',
    depositDate:'',
    bankAccountId: -1
}
interface CashHandoverFormProps{
    handleDepositInfoChange?: (newDepositInfo: BankDepositFormProps)=>void
    handleCashHandoverChange?: (cashHandoverAmount:number)=>void
    handoverAmount: number

    handoverFrom?: string
    handoverUpto?: string
    total?: number
    noneCashAmount?: number
    cashAmount?: number
}

const ierp_transaction_service = new BNFinanceService()

function CashHandoverForm (props: CashHandoverFormProps){
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 18 },
    }
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=>[ appState.auth ])
    const [ depositBanks, setDepositBanks ] = useState<BankAccountInfo[]>([])
    const [ depositFormState, setDepositFormState ] = useState<BankDepositFormProps>({...initBankDepositForm})
    useEffect(()=>{
        fetchAllDepositBankAccounts()
    },[  ])
    useEffect(()=>{
        props.handleDepositInfoChange &&
        props.handleDepositInfoChange(depositFormState)
    },[ depositFormState ])


    async function fetchAllDepositBankAccounts(){
        try{
            const depositBanks:BankAccountInfo[] = ( await ierp_transaction_service.GetAllBankAccounts({ sessionId:auth.sessionId }) ).data
            setDepositBanks([...depositBanks])
        } catch (e) {
        }
    }

    return (
            <Row gutter={4}>
                <Col span={24}>
                    <div className={'paper'} style={{ padding: '10px', marginBottom:'5px' }}>
                        <FormItem {...formItemLayout} label={'Bank Account'}>
                            <Select
                                onChange={(bankAccountId:number)=>setDepositFormState({...depositFormState,bankAccountId})}
                                disabled={!depositFormState.isDepositEnabled}
                                style={{ width: '100%' }} >
                                { depositBanks.map((value,key)=> <Option key={key} value={value.bankAccountNumber}>{value.bankBranchAccount}</Option> ) }
                            </Select>
                        </FormItem>
                        <FormItem {...formItemLayout} label={'Deposit Slip'}>
                            <Input disabled={!depositFormState.isDepositEnabled} onChange={(e)=>setDepositFormState({...depositFormState,depositReference:e.target.value})}/>
                        </FormItem>
                        <FormItem {...formItemLayout} label={'Date'}>
                            <DatePicker disabled={!depositFormState.isDepositEnabled} onChange={(date,dateString)=>setDepositFormState({...depositFormState,depositDate:dateString})} style={{width:'100%'}} />
                        </FormItem>
                        <FormItem {...formItemLayout} label={'Bank Deposit'}>
                            <Checkbox onChange={(event)=>{ setDepositFormState({...depositFormState,isDepositEnabled:event.target.checked}) }}/>
                        </FormItem>
                        <FormItem {...formItemLayout} label={'Handed over cash'}>
                            <Input value={props.handoverAmount} placeholder="Handed over cash" onChange={(e)=>props.handleCashHandoverChange && props.handleCashHandoverChange(+e.target.value)} />
                        </FormItem>
                    </div>
                </Col>
                <Col span={24}>
                    <div className={'paper'} style={{ padding: '10px', marginBottom:'5px' }}>
                        <Divider orientation={'left'}>Cash Information</Divider>
                        <DataRow title={'Cash Amount'} content={props.cashAmount}/>
                        <DataRow title={'None Cash Amount'} content={props.noneCashAmount}/>
                        <Divider/>
                        <DataRow title={'Total'} content={props.total}/>
                    </div>
                </Col>

                <Col span={24}>
                    <div className={'paper'} style={{padding:'10px',marginBottom: '5px'}}>
                        <Divider orientation={'left'}>Handover Date</Divider>
                        <DataRow title={'Handover From'} content={props.handoverFrom}/>
                        <DataRow title={'Handover up to'} content={props.handoverUpto}/>
                    </div>
                </Col>
            </Row>
    )
}

export default CashHandoverForm