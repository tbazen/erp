import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import PaymentCenterContainer from "./payment-center-container";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import CashHandoverDetail from "./cash-handover-detail";

@JobRuleClientDecorator({
    jobName:"Cash Handover",
    jobType:StandardJobTypes.CASH_HANDOVER,
    route:ROUTES.JOB.INTERNAL_SERVICE.CASH_HANDOVER,

    innerRoutes:[
        {
            path:`${ROUTES.JOB.INTERNAL_SERVICE.CASH_HANDOVER}/:paymentCenterId`,
            component: CashHandoverDetail
        }
    ]
})
export class CashHandoverRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return PaymentCenterContainer
    }
}