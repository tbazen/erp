import { Card } from "antd";
import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import ButtonGridContainer from "../../../../shared/screens/button-grid-container/button-grid-container";
import CLoadingPage from "../../../../shared/screens/cloading/cloading-page";
import OperationFailed from "../../../../shared/screens/status-code/submission-failed";
import { PaymentCenterIcon } from "../../../../static/images/index-imgs";
import { ROUTES } from '../../../../_constants/routes';
import { ApplicationState } from "../../../../_model/state-model/application-state";
import { AuthenticationState } from "../../../../_model/state-model/auth-state";
import { IBreadcrumb } from '../../../../_model/state-model/breadcrumd-state';
import { PaymentCenterState } from "../../../../_model/state-model/mn-sm/internal-service-sm/payment-center-state";
import { PaymentCenter } from "../../../../_model/view_model/mn-vm/internal-service-vm/cash-handover/payment-center";
import { fetchPaymentCenters } from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
const { Meta } = Card;


const backwardPath: IBreadcrumb[] = [
    {
        path: ROUTES.MAIN.INDEX,
        breadcrumbName: 'Tools'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.INDEX,
        breadcrumbName: 'Internal Services'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.CASH_HANDOVER,
        breadcrumbName: 'Cash Handover'
    }
]

const PaymentCenterContainer = () => {
    const dispatch = useDispatch()
    const [auth, payment_center_state] = useSelector<ApplicationState, [AuthenticationState, PaymentCenterState]>(appState => [appState.auth, appState.payment_center_state])
    useBreadcrumb(backwardPath)
    useEffect(() => {dispatch(fetchPaymentCenters({ sessionId: auth.sessionId }))}, [])

    const styledTitle = (title: string) => <p style={{ textAlign: 'center', color: '#444', fontWeight: 'bold' }}>{title}</p>
    const PaymentCenterCard = (paymentCenter: PaymentCenter) => {
        return (
            <Link to={`${ROUTES.JOB.INTERNAL_SERVICE.CASH_HANDOVER}/${paymentCenter.casherAccountID}`}>
                <Card
                    hoverable
                    style={{ height: '180px', maxHeight: '300px' }}
                    cover={<img style={{ width: '30%', paddingTop: '20px', margin: 'auto' }} alt={'user ico'} src={PaymentCenterIcon} />}
                    bordered={false} >
                    <Meta
                        description={styledTitle(paymentCenter.centerName)}
                    />
                </Card>
            </Link>
        )
    }


    return (
        <Fragment>
            {
                payment_center_state.loading &&
                <CLoadingPage />
            }
            {
                payment_center_state.error &&
                <OperationFailed />
            }
            {
                !payment_center_state.loading &&
                !payment_center_state.error &&
                <ButtonGridContainer
                    spacing={8}
                    lg={{ span: 4 }}
                    component={PaymentCenterCard}
                    listItem={payment_center_state.payment_centers != null ? payment_center_state.payment_centers : []} />
            }
        </Fragment>
    )
}

export default PaymentCenterContainer