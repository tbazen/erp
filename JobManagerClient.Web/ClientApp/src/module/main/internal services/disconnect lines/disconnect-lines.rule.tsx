import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import DisconnectLine from "./disconnect-lines";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";

@JobRuleClientDecorator({
    jobName:"Disconnect Lines",
    jobType:StandardJobTypes.BATCH_DISCONNECT,
    route:ROUTES.JOB.INTERNAL_SERVICE.BATCH_DISCONNECT
})
export class DisconnectLinesRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return DisconnectLine
    }
}