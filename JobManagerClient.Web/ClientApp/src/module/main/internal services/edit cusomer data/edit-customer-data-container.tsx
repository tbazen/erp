import React, {useEffect, useState} from 'react';
import CustomersListTable from '../../../../shared/components/customer-list-table/customer-list-table';
import {ROUTES} from '../../../../_constants/routes';
import {IBreadcrumb} from '../../../../_model/state-model/breadcrumd-state';
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import {EditCustomerData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/edit-customer-data";
import {Subscriber, SubscriberSearchResult} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import {DiffObject, GenericDiffType} from "../../../../_model/level0/application-server-library/generic-diff-object";
import {JobData} from "../../../../_services/job.manager.service";
import {initialJobData} from "../../../../_helpers/initial value/init-jobdata";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {getLocalDateString} from "../../../../_helpers/date-util";
import { Tabs } from 'antd';
import {EditCustomerDataForm} from "./edit-customer-data-form";
import {initialCustomerData} from "../../../../_helpers/initial value/init-customer-data";

const { TabPane } = Tabs;

const backwardPath: IBreadcrumb[] = [
    {
        path: ROUTES.MAIN.INDEX,
        breadcrumbName: 'Tools'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.INDEX,
        breadcrumbName: 'Internal Services'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.EDIT_CUSTOMER_DATA,
        breadcrumbName: 'Edit Customer Data'
    }
]

const EditCustomerDataContainer = () => {
    useBreadcrumb(backwardPath)
    const [ tabActiveKey , setTabActiveKey ] = useState<string>('select_customer')
    const [ selectedCustomer , setSelectedCustomer ] = useState<Subscriber>()
    useEffect(()=>{
        selectedCustomer &&
        setTabActiveKey('edit_selected_customer')
    },[selectedCustomer])
    function handleSelectCustomer(text:string , customer: SubscriberSearchResult){
        setSelectedCustomer(customer.subscriber)
    }
    return (
        <Tabs
            activeKey={tabActiveKey}
            tabPosition={'right'}
            onTabClick={(key: string, event: MouseEvent)=>{if(key !== 'edit_selected_customer')setSelectedCustomer(undefined);setTabActiveKey(key)}}
            >
            <TabPane tab={`Select Customer`} key={'select_customer'}>
                <CustomersListTable
                    height={'68vh'}
                    withSearchBar
                    withActions={[{
                        actionName:'Edit',
                        handler: handleSelectCustomer
                    }]}
                    searchBarWithOutAddOption
                />
            </TabPane>
            <TabPane tab={`Edit Customer`} disabled key={'edit_selected_customer'}>
                {
                    selectedCustomer &&
                    <EditCustomerDataForm customer={selectedCustomer}/>
                }
                {
                    !selectedCustomer &&
                    <p>Select Customer</p>
                }
            </TabPane>
            <TabPane tab={`Add New Customer`} key={'new_customer'}>
                <EditCustomerDataForm/>
            </TabPane>
        </Tabs>
    )
}

export default EditCustomerDataContainer