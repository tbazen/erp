import React, {useEffect, useState} from 'react'
import {Col, Divider, Row, Spin, Typography} from "antd";
import {Subscriber} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import CustomerForm from "../../../../shared/components/customer-list-table/customer-form";
import {AddJobWebPar, JobData} from "../../../../_services/job.manager.service";
import {initialJobData} from "../../../../_helpers/initial value/init-jobdata";
import getCurrentTicks, {getLocalDateString} from "../../../../_helpers/date-util";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {EditCustomerData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/edit-customer-data";
import {DiffObject, GenericDiffType} from "../../../../_model/level0/application-server-library/generic-diff-object";
import {initialCustomerData} from "../../../../_helpers/initial value/init-customer-data";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {fetchCustomerSubscriptions, requestAddJobWeb} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {SubscriptionState} from "../../../../_model/state-model/mn-sm/subscription-state";
import {GetSubscriptionsPar} from "../../../../_services/subscribermanagment.service";
import CButton from "../../../../shared/core/cbutton/cbutton";

const { Text } = Typography
interface IProps{
    customer? : Subscriber
}

export function EditCustomerDataForm(props:IProps){
    const [jobData, setJobData] = useState<JobData>({
        ...initialJobData.job,
        customerID:props.customer? props.customer.id :-1,
        logDate:getLocalDateString(),
        startDate:getLocalDateString(),
        statusDate:getLocalDateString(),
        applicationType: StandardJobTypes.EDIT_CUTOMER_DATA})
    const [ customer , setCustomer ] = useState<Subscriber>(props.customer ? props.customer : initialCustomerData)
    const [ auth , subscriptionState ] = useSelector<ApplicationState,[ AuthenticationState,SubscriptionState ]>(appState=>[ appState.auth, appState.subscription_state ])
    const [ customerSubscriptions , setCustomerSubscriptions ] = useState<Subscription[]>([])
    const [ deletedCustomerSubscriptions , setDeletedCustomerSubscriptions ] = useState<Subscription[]>([])

    const dispatch= useDispatch()
    useEffect(()=>{
        props.customer &&
        !subscriptionState.loading &&
        !subscriptionState.error &&
        setCustomerSubscriptions(subscriptionState.payload)
    },[ subscriptionState ])

    useEffect(()=>{
        if(props.customer){
            setCustomer(props.customer)
            const subsctiptionParams: GetSubscriptionsPar = {
                sessionId:auth.sessionId,
                version:getCurrentTicks(),
                subscriberID: customer.id
            }
            dispatch(fetchCustomerSubscriptions(subsctiptionParams))
        }
    },[props.customer])



    function handleSubmitCustomerData(){
        if(customer){
            const editCustomerData = new EditCustomerData()
            editCustomerData.customerDiff = new DiffObject<Subscriber>({data:customer,type:customer.id<1?GenericDiffType.Add : GenericDiffType.Replace})
            let list = new Array<DiffObject<Subscription>>()
            deletedCustomerSubscriptions.forEach(deletedSubscription=>{
                const temp = new DiffObject<Subscription>()
                temp.diffType = GenericDiffType.Delete
                temp.data = deletedSubscription
                list = [...list,temp]
            })
            editCustomerData.connections = list;
            const jobParams : AddJobWebPar<EditCustomerData> = {
                job:{...jobData},
                sessionId:auth.sessionId,
                data:editCustomerData,
                dataType:NAME_SPACES.WORKFLOW.EDIT_CUSTOMER_DATA
            }
            dispatch(requestAddJobWeb(jobParams))
        }
    }


    function handleDeleteCustomer(){
        if(customer) {
            const editCustomerData = new EditCustomerData()
            jobData.customerID = customer.id
            if (customer.id != -1) {
                editCustomerData.customerDiff = new DiffObject<Subscriber>()
                editCustomerData.customerDiff.data = customer
                editCustomerData.customerDiff.data.id = customer.id
                editCustomerData.customerDiff.diffType = GenericDiffType.Delete
                const jobParams: AddJobWebPar<EditCustomerData> = {
                    job: jobData,
                    sessionId: auth.sessionId,
                    data: editCustomerData,
                    dataType: NAME_SPACES.WORKFLOW.EDIT_CUSTOMER_DATA
                }
                dispatch(requestAddJobWeb(jobParams))
            }
        }
    }

    function handleCustomerDataChange(newCustomer: Subscriber){ setCustomer(newCustomer) }
    function handleRemoveSubscription(subscription:Subscription){
        setCustomerSubscriptions([...customerSubscriptions.filter(subs=>subs.contractNo !== subscription.contractNo)])
        setDeletedCustomerSubscriptions([...deletedCustomerSubscriptions,subscription])
    }


    interface ISubscriptionListItem{
        subscription : Subscription
    }
    function SubscriptionListItem( subscriptionListItem:ISubscriptionListItem ){
        return (
            <Row gutter={4} style={{margin:'3px',padding:'5px'}} className={'paper'}>
                <Col span={18}>
                    <Text strong>
                        Contract NO. {subscriptionListItem.subscription.contractNo}
                        <Divider type="vertical" />
                        {subscriptionListItem.subscription.itemCode}
                    </Text>
                </Col>
                <Col span={6}>
                    <CButton onClick={()=>handleRemoveSubscription(subscriptionListItem.subscription) } size={'small'} type={'danger'} block>Remove</CButton>
                </Col>
            </Row>
        )
    }

    return (
        <Row gutter={4}>
            <Col span={14} >
                <div className={'paper'} style={{padding:'5px',minHeight:'70vh'}}>
                    <Divider orientation={'left'}>Customer information</Divider>
                    <CustomerForm customer={customer} updateCustomerData={handleCustomerDataChange}/>
                    <Row gutter={6}>
                        <Col span={8}>
                            <CButton onClick={handleSubmitCustomerData} type={'primary'} block>Submit changes</CButton>
                        </Col>
                        <Col span={8}>
                            <CButton onClick={handleDeleteCustomer} type={'danger'} block>Delete Customer</CButton>
                        </Col>
                    </Row>
                </div>
            </Col>
            <Col span={10} className={'paper'}>
                <Divider orientation={'left'}>Contracts</Divider>
                <Spin spinning={subscriptionState.loading} >
                    <div style={{padding:'5px',minHeight:'70vh'}}>
                        {
                            customerSubscriptions.map((value: Subscription,key)=><SubscriptionListItem subscription={value} key={key} />)
                        }
                    </div>
                </Spin>
            </Col>
        </Row>
    )
}