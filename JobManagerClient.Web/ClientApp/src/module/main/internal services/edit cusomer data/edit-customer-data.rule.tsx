import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import EditCustomerDataContainer from "./edit-customer-data-container";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {ROUTES} from "../../../../_constants/routes";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";

@JobRuleClientDecorator({
    jobName:"Edit Customer Data",
    jobType:StandardJobTypes.EDIT_CUTOMER_DATA,
    route:ROUTES.JOB.INTERNAL_SERVICE.EDIT_CUSTOMER_DATA
})
export class EditCustomerDataRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return EditCustomerDataContainer
    }
}