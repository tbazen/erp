import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions";
import { EditCustomerDataIcon, VoidReceiptIcon, CashhandoverIcon, IconDisconnectLinesIcon, IconReconnectLineIcon } from '../../../static/images/index-imgs';
import ButtonGrid, { IButtonGrid } from '../../../shared/components/button-grid/button-grid';
import ButtonGridContainer from '../../../shared/screens/button-grid-container/button-grid-container';
import {ApplicationState} from "../../../_model/state-model/application-state";
import {JobRuleStateModel} from "../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {isInternalService, isOtherService} from "../job-group-identifier";
import {constructButtonGridItem} from "../../../shared/components/button-grid/button-grid-constructor";
import LoadingCircle from "../../../shared/components/loading/loading-circle";
import OperationFailed from "../../../shared/screens/status-code/submission-failed";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.INTERNAL_SERVICE.INDEX,
        breadcrumbName:'Internal Services'
    }
]

const InternalServicesContainer = ()=>{
    const dispatch = useDispatch()
    const [ jobRule ] = useSelector<ApplicationState,[JobRuleStateModel]>(appState => [appState.job_rule])
    const [ buttonGridItems,setButtonGridItems ] = useState<IButtonGrid[]>([])
    useEffect(()=>{
        dispatch(pushPathToBC(backwardPath))
        return () => {
            dispatch(pushPathToBC([]))    
        };
    },[])

    useEffect(()=>{
        !jobRule.loading && !jobRule.error && updateButtonGridItems()
    },[jobRule])

    function updateButtonGridItems() {
        const buttonGridItems: IButtonGrid[] = []
        jobRule.clientHandlers.forEach(clientHandler=>{
            isInternalService(clientHandler.meta.jobType) &&
            buttonGridItems.push(constructButtonGridItem(clientHandler))
        });
        setButtonGridItems([...buttonGridItems])
    }

    return <Fragment>
                {
                    jobRule.loading &&
                    <LoadingCircle/>
                }
                {
                    !jobRule.loading &&
                    jobRule.error &&
                    <OperationFailed errorMessage={jobRule.message}/>
                }
                {
                    !jobRule.loading &&
                    !jobRule.error &&
                    <ButtonGridContainer
                        spacing={16}
                        lg={{span: 4}}
                        component={ButtonGrid}
                        listItem={buttonGridItems}/>
                }
            </Fragment>
}

export default InternalServicesContainer