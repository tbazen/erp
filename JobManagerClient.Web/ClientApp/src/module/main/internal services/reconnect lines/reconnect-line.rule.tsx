import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";
import { ReconnectLine } from "./reconnect-line";
@JobRuleClientDecorator({
    jobName:"Reconnect Line",
    jobType:StandardJobTypes.BATCH_RECONNECT,
    route : ROUTES.JOB.INTERNAL_SERVICE.BATCH_RECONNECT
})
export class ReconnectLineRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return ReconnectLine
    }
}