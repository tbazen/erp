import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {ROUTES} from '../../../../_constants/routes';
import {IBreadcrumb} from '../../../../_model/state-model/breadcrumd-state';
import {Button, Card, Checkbox, Col, Row, Table} from 'antd';
import SubscriberManagementService, {
    GetBillDocumentsPar,
    GetSubscription2Par,
    GetSubscriptions2Par,
    SubscriberSearchField
} from "../../../../_services/subscribermanagment.service";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {
    fetchSubscriberList,
    requestAddJobWeb,
    subscriberListClear
} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {SubscriberSearchResult} from "../../../../_model/view_model/mn-vm/subscriber-search-result";
import ButtonGridContainer from "../../../../shared/screens/button-grid-container/button-grid-container";
import {SubscriberSearchResultState} from "../../../../_model/state-model/mn-sm/subscriber-search-result-state";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import {CustomerBillDocument} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import {parseNumberToMoney} from "../../../../_helpers/numbers-util";
import getCurrentTicks, {getLocalDateString} from "../../../../_helpers/date-util";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {AddJobWebPar, JobData} from "../../../../_services/job.manager.service";
import {initialJobData} from "../../../../_helpers/initial value/init-jobdata";
import {
    BatchDisconnectData,
    BatchReconnectData
} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/disconnect-connections-data";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {useGlobalSearchBar} from "../../../../shared/hooks/manage-global-search";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {NotificationTypes, openNotification} from "../../../../shared/components/notification/notification";
import {SubscriptionStatus} from "../../../../_enum/mn-enum/subscription-status";

const { Column } = Table
const backwardPath: IBreadcrumb[] = [
    {
        path: ROUTES.MAIN.INDEX,
        breadcrumbName: 'Tools'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.INDEX,
        breadcrumbName: 'Internal Services'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.BATCH_DISCONNECT,
        breadcrumbName: 'Reconnect line'
    }
]

const subscriber_mgr_service= new SubscriberManagementService()


interface ITableRecord{
    subscription: Subscription
    customerBillDocuments: CustomerBillDocument[]
}


export const ReconnectLine = () => {
    const dispatch = useDispatch()
    const [ auth, subscriptionSearchResultState ] = useSelector<ApplicationState,[ AuthenticationState,SubscriberSearchResultState ]>((appState)=>[appState.auth,appState.subscription_search_result_state])
    const initSearchParams : GetSubscriptions2Par={
        sessionId:auth.sessionId,
        query: '',
        fields: SubscriberSearchField.CustomerName | SubscriberSearchField.ConstactNo,
        multipleVersion: false,
        kebele: -1,
        zone: -1,
        dma: -1,
        index: 0,
        page: 30
    }

    const [jobData, setJobData] = useState<JobData>({
        ...initialJobData.job,
        logDate:getLocalDateString(),
        startDate:getLocalDateString(),
        statusDate:getLocalDateString(),
        applicationType: StandardJobTypes.BATCH_DISCONNECT})

    const [ searchParams , setSearchParams] = useState<GetSubscriptions2Par>(initSearchParams)
    const [ tableRecord, setTableRecord ] = useState<ITableRecord[]>([])
    const [ discontinueChecked , setDiscontinueChecked  ] = useState<boolean>(false)

    useBreadcrumb(backwardPath)
    useGlobalSearchBar({ searchPlaceHolder:'Contract Number',searchHandler: searchInputHandler, cleanupSearchResult: subscriberListClear })
    //API fetch initiator effect
    useEffect(()=>{
        if(searchParams.query.trim() !== '')
            dispatch(fetchSubscriberList(searchParams))
    },[searchParams])

    function searchInputHandler(event:any){
        const query = event.target.value
        setSearchParams({...initSearchParams,query})
    }
    function removeSubscriptionFromTable(record: ITableRecord){
        setTableRecord([...tableRecord.filter(td=>td.subscription.id!== record.subscription.id)])
    }
    async function addSubscription(subscriptionParam: Subscription) {
        try{
            openNotification({
               message:'Loading',
               description:'Adding To reconnect Line',
               notificationType:NotificationTypes.LOADING
            });
            if(!subscriptionParam)
                throw new Error('Invalid contract number')
            if(subscriptionParam.subscriptionStatus === SubscriptionStatus.Active)
                throw new Error(`Connection for contract no: ${subscriptionParam.contractNo} Is Already Active`)
            if(tableRecord.find(td=>td.subscription.id === subscriptionParam.id))
                throw new Error(('Connection already added'))
            const billDocParams : GetBillDocumentsPar = {
                sessionId:auth.sessionId,
                mainTypeID:-1,
                customerID:-1,
                periodID:-1,
                excludePaid:true,
                connectionID:subscriptionParam.id
            }
            const subscrptionParams: GetSubscription2Par = {
                sessionId:auth.sessionId,
                version:getCurrentTicks(),
                contractNo:subscriptionParam.contractNo || ''
            }
            const customerBillDocuments :CustomerBillDocument[] = (await subscriber_mgr_service.GetBillDocuments(billDocParams)).data;
            const subscription :Subscription = (await subscriber_mgr_service.GetSubscription2(subscrptionParams)).data;
            setTableRecord([...tableRecord,{ subscription, customerBillDocuments }])
            openNotification({
                message:'Success',
                description:'Successfully added to reconnect line',
                notificationType:NotificationTypes.SUCCESS
            });
        } catch(err){
            openNotification({
                message:'ERRNO',
                description:err.message,
                notificationType:NotificationTypes.ERROR
            });
        }
    }
    function handleSubmitDisconnectJobData(){
        const batchDisconnectData = new BatchReconnectData()
        batchDisconnectData.discontinue = discontinueChecked
        tableRecord.forEach((record)=>{
            batchDisconnectData.connections.push(record.subscription.versionedID)
        })
        if(jobData.id === -1){
            const jobParams: AddJobWebPar<BatchReconnectData> = {
                job:jobData,
                data:batchDisconnectData,
                sessionId:auth.sessionId,
                dataType:NAME_SPACES.WORKFLOW.RECONNECT_CONNECTION_DATA
            }
            dispatch(requestAddJobWeb(jobParams))
        }
    }
    const SubscriptionCard = (props:SubscriberSearchResult)=>{
        return (
            <Card
                className={'paper'}
                title={props.subscriber.name}
                bordered={false}
                actions={[
                    <Button onClick={()=>addSubscription(props.subscription)} icon={'plus'} block type={'link'}>
                        Add
                    </Button>
                ]}
            >
                <p>{props && props.subscription && props.subscription.contractNo}</p>
            </Card>
        )
    }
    return (
        <Row gutter={4}>
            <Col span={12}>
                <div className={'paper'} style={{padding:'5px',height:'87vh', maxHeight:'87vh',overflowY:'auto'}}>
                    <StateMachineHOC state={subscriptionSearchResultState}>
                        <ButtonGridContainer spacing={4} lg={{span:8}} component={SubscriptionCard} listItem={subscriptionSearchResultState.payload._ret}/>
                    </StateMachineHOC>
                </div>
            </Col>

            <Col span={12}>
                <div className={'paper'} style={{padding:'5px'}}>
                    <Table
                        dataSource={tableRecord.map((record,key)=>{
                            let tempAmount: number = 0
                            let billCount = record.customerBillDocuments.length
                            record.customerBillDocuments.forEach(doc=>{ tempAmount+=doc.total })
                            let amount=parseNumberToMoney(tempAmount)
                            const contractNumber = record.subscription.contractNo
                            const customerName = record.subscription.subscriber? record.subscription.subscriber.name : ''
                            const customerCode = record.subscription.subscriber? record.subscription.subscriber.customerCode : ''
                            return {...record,contractNumber,customerName,customerCode,billCount,amount,key}
                        })}
                        size="small"
                        scroll={{ y: '84vh' }}
                        bordered={false}
                        pagination={false}
                    >
                        <Column title='Contract Number' dataIndex='contractNumber' key = 'subscription.contractNo' width='20%'/>
                        <Column title='Customer Code' dataIndex='customerCode' key = 'subscription.subscriber.name' width='15%'/>
                        <Column title='Customer Name' dataIndex='customerName' key = 'subscriber.customerCode' width='15%'/>
                        <Column title='Outstanding Bill Count' dataIndex='billCount' key = 'billCount' width='20%'/>
                        <Column title='Amount' dataIndex='amount' key = 'amount' width='15%'/>
                        <Column
                            title='Action'
                            key = 'action'
                            width='15%'
                            render={(record: ITableRecord)=><CButton onClick={()=>removeSubscriptionFromTable(record)} type={'danger'}>Remove</CButton>}
                        />
                    </Table>
                    <Checkbox checked={discontinueChecked} onChange={()=>setDiscontinueChecked(!discontinueChecked)}>Discontinue</Checkbox>
                    <Row>
                        <Col span={8}><CButton disabled={tableRecord.length === 0} onClick={handleSubmitDisconnectJobData} type={'primary'} block>Submit</CButton></Col>
                    </Row>
                </div>
            </Col>
        </Row>
    )
}
