import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import VoidReciept from "./void-receipt";
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../../_constants/routes";

@JobRuleClientDecorator({
    jobName:"Void Receipt",
    jobType:StandardJobTypes.VOID_RECEIPT,
    route:ROUTES.JOB.INTERNAL_SERVICE.VOID_RECEIPT
})
export class VoidReceiptRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return VoidReciept
    }
}