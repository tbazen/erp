import React, {useEffect, useState} from 'react';
import { ROUTES } from '../../../../_constants/routes';
import { IBreadcrumb } from '../../../../_model/state-model/breadcrumd-state';
import {Input, Row, Col, Form, Table, Divider} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import CButton from '../../../../shared/core/cbutton/cbutton';
import {useBreadcrumb} from "../../../../shared/hooks/manage-breadcrumb";
import {AddJobWebPar, JobData, UpdateJobWebPar} from "../../../../_services/job.manager.service";
import {initialJobData} from "../../../../_helpers/initial value/init-jobdata";
import {getLocalDateString} from "../../../../_helpers/date-util";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {VoidReceiptData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/void-receipt-data";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {requestAddJobWeb, requestUpdateJobWeb} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {DocumentSerialType, DocumentTypedReference} from "../../../../_model/level0/accounting-type-library/doc-serial";
import AccountingService, {
    GetAccountDocumentPar, GetDocumentHTMLPar,
    GetDocumentListByTypedReferencePar,
    GetDocumentSerialTypePar, GetDocumentTypeByTypePar
} from "../../../../_services/accounting.service";
import {CustomerPaymentReceipt} from "../../../../_model/level0/subscriber-managment-type-library/bill";
import {AccountDocument} from "../../../../_model/level0/accounting-type-library/account-document";
import {HTMLParser} from "../../../../shared/components/html-parser/html-parser";
import {PaymentCenter} from "../../../../_model/level0/subscriber-managment-type-library/payment-center";
import SubscriberManagementService, {GetPaymentCenterByCashAccountPar} from "../../../../_services/subscribermanagment.service";
import {Employee} from "../../../../_model/view_model/IEmployee";
import PayrollService, {GetEmployeePar} from "../../../../_services/payroll.service";
const { Column } = Table

const backwardPath: IBreadcrumb[] = [
    {
        path: ROUTES.MAIN.INDEX,
        breadcrumbName: 'Tools'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.INDEX,
        breadcrumbName: 'Internal Services'
    },
    {
        path: ROUTES.JOB.INTERNAL_SERVICE.VOID_RECEIPT,
        breadcrumbName: 'Void Reciept'
    }
]
const accounting_service = new AccountingService()
const subscriber_mgr_service = new SubscriberManagementService()
const payroll_service = new PayrollService()

interface ITableData {
    receipt: CustomerPaymentReceipt,
    cashier: string,
}

const VoidReciept = () => {
    const formLayout = 'horizontal';
    const formItemLayout = {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 },
    }
    const dispatch = useDispatch()
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=> [ appState.auth ])
    const [ isValidReceiptNumber, setIsValidReceiptNumber ] = useState<boolean>(false)
    const [ receiptHtml , setReceiptHtml ] = useState<string>('')
    const [ receiptNumber , setReceiptNumber ] = useState<string>()
    const [ selectedReceiptId , setSelectedReceiptId ] = useState<number>(-1)
    const [ receiptTableData, setReceiptTableData ] = useState<ITableData[]>([])

    const [jobData, setJobData] = useState<JobData>({
        ...initialJobData.job,
        logDate:getLocalDateString(),
        startDate:getLocalDateString(),
        statusDate:getLocalDateString(),
        applicationType: StandardJobTypes.VOID_RECEIPT})
    const [ voidReceiptData, setVoidReceiptData ] = useState<VoidReceiptData>(new VoidReceiptData())
    useBreadcrumb(backwardPath)
    useEffect(()=>{
        receiptNumber &&
        validateReceiptNumber()
    },[ receiptNumber ]  )
    useEffect(()=> {
        selectedReceiptId !== -1 &&
        loadReceipt(selectedReceiptId)
    },[selectedReceiptId])

    async function validateReceiptNumber(){
        setReceiptTableData([])
        setSelectedReceiptId(-1)
        try{
            if(!receiptNumber)
                throw new Error('Invalid Receipt Number');
            if(receiptNumber === '-1')
                throw new Error('Invalid Receipt Number')
            const serialTypePar: GetDocumentSerialTypePar = {
                sessionId: auth.sessionId,
                prefix:'BNS'
            }
            const serialType: DocumentSerialType = (await accounting_service.GetDocumentSerialType(serialTypePar)).data
            if(!serialType)
                throw new Error('Please configure BSN reference type')
            const docListPar:GetDocumentListByTypedReferencePar = {
                sessionId: auth.sessionId,
                tref:new DocumentTypedReference({typeID:serialType.id,reference:receiptNumber,primary: false})
            }
            const docs: number[] = (await accounting_service.GetDocumentListByTypedReference(docListPar)).data
            const docTypePar: GetDocumentTypeByTypePar = {
                sessionId:auth.sessionId,
                type:NAME_SPACES.DOCUMENT_TYPE.CUSTOMER_PAYMENT_RECEIPT
            }
            const receiptDocumentTypeID: number = ( await accounting_service.GetDocumentTypeByType(docTypePar)).data
            let receipts: CustomerPaymentReceipt[] = []
            for(const doc of docs)
            {
                const accountDocPar: GetAccountDocumentPar = {
                    sessionId:auth.sessionId,
                    documentID:doc,
                    fullData: true
                }
                const accountDoc: AccountDocument = ( await accounting_service.GetAccountDocument(accountDocPar) ).data
                if(accountDoc.documentTypeID !== receiptDocumentTypeID)
                    continue;
                receipts = [...receipts,accountDoc as CustomerPaymentReceipt]
            }
            if(receipts.length === 0)
                throw new Error('Receipt not found')
            if(receipts.length > 1)
                await loadReceiptTableData(receipts)
            else
                setSelectedReceiptId(receipts[0].accountDocumentID)
        } catch(err){
            setIsValidReceiptNumber(false)
        }
    }
    async function loadReceiptTableData(receipts: CustomerPaymentReceipt[]){
        try{
            let tableData:ITableData[] = []
            for(const receipt of receipts)
            {
                let cashier: string =''
                const paymentCenterPar: GetPaymentCenterByCashAccountPar = {
                    sessionId:auth.sessionId,
                    account:receipt.assetAccountID
                }
                const paymentCenter: PaymentCenter = ( await subscriber_mgr_service.GetPaymentCenterByCashAccount(paymentCenterPar)).data
                if(paymentCenter){
                    cashier = `Center Name : ${paymentCenter.centerName}`
                    const employeePar: GetEmployeePar = {
                        sessionId: auth.sessionId,
                        ID:paymentCenter.casheir
                    }
                    const employee: Employee = (await payroll_service.GetEmployee(employeePar)).data
                    if(employee)
                        cashier = employee.employeeName
                }
               tableData = [...tableData,{receipt,cashier}]
            }
            setReceiptTableData([...tableData])
        } catch(err){
        }
    }
    async function loadReceipt(documentId: number){
        try{
            const htmlDocumentPar: GetDocumentHTMLPar = {
                accountDocumentID:documentId,
                sessionId:auth.sessionId
            }
            const receiptHtml: string = ( await accounting_service.GetDocumentHTML(htmlDocumentPar)).data
            setVoidReceiptData({...voidReceiptData,receitDocumentID:documentId})
            setIsValidReceiptNumber(true)
            setReceiptHtml(receiptHtml);
        } catch (err){
            setIsValidReceiptNumber(false)
        }
    }
    function handleSubmitVoidReceiptData(){
        if(jobData.id === -1){
            const jobParams: AddJobWebPar<VoidReceiptData> = {
                sessionId: auth.sessionId,
                job:jobData,
                data: voidReceiptData,
                dataType: NAME_SPACES.WORKFLOW.VOID_RECEIPT_DATA
            }
            dispatch(requestAddJobWeb<VoidReceiptData>(jobParams))
        } else{
            const jobParams: UpdateJobWebPar<VoidReceiptData> = {
                sessionId: auth.sessionId,
                job:jobData,
                data: voidReceiptData,
                dataType: NAME_SPACES.WORKFLOW.VOID_RECEIPT_DATA
            }
            dispatch(requestUpdateJobWeb<VoidReceiptData>(jobParams))
        }
    }
    return (
        <Row gutter={4}>
            <Col lg={{span:12}}>
                <div className={'paper'} style={{padding:'10px',marginBottom:'10px' }}>
                    <Form layout={formLayout}>
                        <Form.Item label="Receipt Number" {...formItemLayout}>
                            <Input size={'large'} onChange={(event)=>{setReceiptNumber(event.target.value)}}/>
                        </Form.Item>
                        <Form.Item label="Reason" {...formItemLayout}>
                            <TextArea rows={5} onChange={(event)=>{ setJobData({...jobData,description: event.target.value}) }}/>
                        </Form.Item>
                    </Form>
                </div>
                <div className={'paper'} style={{padding:'10px',height:'45vh',maxHeight:'45vh',overflowY:'auto',marginBottom:'10px'}}>
                    <Divider orientation={'left'}>Selected Receipt</Divider>
                    <HTMLParser htmlString={receiptHtml}/>
                </div>
                <Row className={'paper'} style={{padding:'10px'}}>
                    <Col span={12}>
                        <CButton disabled={!isValidReceiptNumber} onClick={handleSubmitVoidReceiptData} type={'primary'} block size={'large'}>Submit</CButton>
                    </Col>
                </Row>
            </Col>
            <Col span={12}>
                <Table
                    dataSource={receiptTableData}
                    size="small"
                    scroll={{ y: '84vh' }}
                    bordered={false}
                    pagination={false}
                >
                    <Column title='Receipt' dataIndex='receipt.paperRef' key = 'receipt.paperRef' width='20%'/>
                    <Column title='Date' dataIndex='receipt.documentDate' key = 'receipt.documentDate' width='20%'/>
                    <Column title='Cashier' dataIndex='cashier' key = 'cashier' width='20%'/>
                    <Column title='Amount' dataIndex='receipt.totalInstrument' key = 'receipt.totalInstrument' width='20%'/>
                    <Column
                        title='Action'
                        key = 'action'
                        width='15%'
                        render={(text,rowData:ITableData)=><CButton type={'danger'} onClick={()=>{setSelectedReceiptId(rowData.receipt.accountDocumentID)}}>Select</CButton>}
                    />
                </Table>
            </Col>
        </Row>
    )
}

export default VoidReciept