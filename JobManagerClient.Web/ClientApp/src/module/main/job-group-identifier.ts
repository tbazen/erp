import {StandardJobTypes} from "../../_model/view_model/mn-vm/standard-job-types";

export function isInternalService(jobtype:StandardJobTypes): boolean {
    switch (jobtype) {
        case StandardJobTypes.CASH_HANDOVER:
        case StandardJobTypes.EDIT_CUTOMER_DATA:
        case StandardJobTypes.VOID_RECEIPT:
        case StandardJobTypes.BATCH_DISCONNECT:
        case StandardJobTypes.BATCH_RECONNECT:
            return true;
        default:
            return false;
    }
}

export function isOtherService(jobType :StandardJobTypes): boolean {
    switch(jobType){
        case StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK:
        case StandardJobTypes.LIQUID_WASTE_DISPOSAL:
        case StandardJobTypes.HYDRANT_SERVICE:
        case StandardJobTypes.CONNECTION_PENALITY:
        case StandardJobTypes.GENERAL_SELL:
            return true;
        default:
            return false;
    }
}
