import React, {Fragment, useEffect, useState} from "react"
import ButtonGridContainer from "../../shared/screens/button-grid-container/button-grid-container"
import ButtonGrid, {IButtonGrid} from "../../shared/components/button-grid/button-grid"
import {useSelector} from "react-redux";
import {JobRuleStateModel} from "../../_model/state-model/job-rule-sm/job-rule-state-model";
import {ApplicationState} from "../../_model/state-model/application-state";
import OperationFailed from "../../shared/screens/status-code/submission-failed";
import {isInternalService, isOtherService} from "./job-group-identifier";
import {constructButtonGridItem} from "../../shared/components/button-grid/button-grid-constructor";
import {ButtonGridIcons, IconTypes} from "../../shared/components/button-grid/button-grid-icons";
import {ROUTES} from "../../_constants/routes";
import CLoadingPage from "../../shared/screens/cloading/cloading-page";
import {StateMachineHOC} from "../../shared/hoc/StateMachineHOC";

const customerList: IButtonGrid = {
    imgUrl: ButtonGridIcons[IconTypes.CUSTOMER_LIST].iconUrl,
    path: ROUTES.MAIN.CUSTOMER_LIST,
    title: "Customer List"
}
const workerList: IButtonGrid = {
    imgUrl: ButtonGridIcons[IconTypes.WORKER_LIST].iconUrl,
    path: ROUTES.MAIN.WORKER_LIST,
    title: "Worker List"
}
const otherServices: IButtonGrid = {
    imgUrl: ButtonGridIcons[IconTypes.OTHER_TECHNICAL_SERVICES].iconUrl,
    path: ROUTES.JOB.OTHER_SERVICE.INDEX,
    title: "Other services"
}
const internalServices: IButtonGrid = {
    imgUrl: ButtonGridIcons[IconTypes.OTHER_NONE_TECHNICAL_SERVICES].iconUrl,
    path: ROUTES.JOB.INTERNAL_SERVICE.INDEX,
    title: "Internal Services"
}

export const Main = () => {
    const [jobRule] = useSelector<ApplicationState, [JobRuleStateModel]>(appState => [appState.job_rule])
    const [buttonGridItems, setButtonGridItems] = useState<IButtonGrid[]>([])
    useEffect(() => {
        !jobRule.loading && !jobRule.error && updateButtonGridItems()
    }, [jobRule])

    function updateButtonGridItems() {
        const buttonGridItems: IButtonGrid[] = []
        jobRule.clientHandlers.forEach(clientHandler => {
            !isInternalService(clientHandler.meta.jobType) &&
            !isOtherService(clientHandler.meta.jobType) &&
            buttonGridItems.push(constructButtonGridItem(clientHandler))
        });
        buttonGridItems.push(customerList);
        buttonGridItems.push(workerList);
        buttonGridItems.push(otherServices);
        buttonGridItems.push(internalServices);
        setButtonGridItems([...buttonGridItems])
    }

    return (
        <StateMachineHOC state={jobRule}>
            <ButtonGridContainer
                spacing={16}
                lg={{span: 4}}
                component={ButtonGrid}
                listItem={buttonGridItems}/>
        </StateMachineHOC>
    )
}