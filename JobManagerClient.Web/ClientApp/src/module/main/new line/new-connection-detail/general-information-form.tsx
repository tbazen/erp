import React, {Fragment, useEffect} from "react";
import {Col, DatePicker, Divider, Form, Input, Row, Select} from "antd";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {
    fetchDMZList,
    fetchPressureZoneList
} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {DmzState} from "../../../../_model/state-model/mn-sm/dmz-state";
import {PressureZoneState} from "../../../../_model/state-model/mn-sm/pressure-zone-state";
import {SubscriptionType} from "../../../../_enum/mn-enum/subscription-type";
import {SubscriptionStatus} from "../../../../_enum/mn-enum/subscription-status";
import {getDateFromTick, getTickFromDate} from "../../../../_helpers/date-util";

const {Option } = Select

interface IProps{
    newLineData : NewLineData
    handleConnectionDataChange: <T>( name : string , value:T )=> void
}

export function GeneralInformationForm(props : IProps){
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const dispatch = useDispatch()
    const [ auth,dmzState , pressureZoneState ] = useSelector<ApplicationState, [
        AuthenticationState,
        DmzState,
        PressureZoneState ]>(appState=>[appState.auth,appState.dmz_state, appState.pressure_zone_state])
    useEffect(()=>{
        dispatch(fetchDMZList({sessionId:auth.sessionId}))
        dispatch(fetchPressureZoneList({sessionId:auth.sessionId}))
    },[])


    return (
        <Fragment>
            <Row gutter={4}>
                <Col span={24}><Divider orientation="left">{'General Information'}</Divider></Col>
                <Col span={12}>
                    <Form.Item label="Date" {...formItemLayout}>
                    <DatePicker
                        onChange={(moment,dateString)=>{
                            const tick = getTickFromDate(new Date(dateString));
                            props.handleConnectionDataChange<number>('ticksFrom',tick)
                        }}
                        placeholder={ getDateFromTick(props.newLineData.connectionData.ticksFrom).toLocaleString()}  style={{width:'100%'}} showToday  />
                </Form.Item>
                    <Form.Item label="Contract No." {...formItemLayout}>
                        <Input disabled value={props.newLineData.connectionData.contractNo || ''}/>
                    </Form.Item>
                    <Form.Item label="Type" {...formItemLayout}>
                        <Select
                            onChange={(subscriptionType: any)=>{ props.handleConnectionDataChange<SubscriptionType>('subscriptionType',subscriptionType) }}
                            defaultValue={props.newLineData.connectionData.subscriptionType}
                        >
                            <Option value={SubscriptionType.Tap}>Tap</Option>
                            <Option value={SubscriptionType.Well}>Well</Option>
                            <Option value={SubscriptionType.Shared}>Shared</Option>
                            <Option value={SubscriptionType.Hydrant}>Hydrant</Option>
                            <Option value={SubscriptionType.CattleDrink}>Cattle Drink</Option>
                            <Option value={SubscriptionType.Unknown}>Unknown</Option>
                        </Select>
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item label="Status" {...formItemLayout}>
                        <Select
                            onChange={(subscriptionStatus: any)=>{ props.handleConnectionDataChange<SubscriptionStatus>('subscriptionStatus',subscriptionStatus) }}
                            defaultValue={props.newLineData.connectionData.subscriptionStatus}
                        >
                            <Option value={SubscriptionStatus.Pending}>Pending</Option>
                            <Option value={SubscriptionStatus.Active}>Active</Option>
                            <Option value={SubscriptionStatus.Discontinued}>Discontinued</Option>
                            <Option value={SubscriptionStatus.Diconnected}>Disconnected</Option>
                            <Option value={SubscriptionStatus.Unknown}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="DMA" {...formItemLayout}>
                        <Select
                            onChange={(dma: any)=>{ props.handleConnectionDataChange<number>('dma',dma) }}
                            defaultValue={props.newLineData.connectionData.dma}
                        >
                            {
                                dmzState.dmzs.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>)
                            }
                            <Option value={-1}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="Pressure Zone" {...formItemLayout}>
                        <Select
                            onChange={(pressureZone: any)=>{ props.handleConnectionDataChange<number>('pressureZone',pressureZone) }}
                            defaultValue={props.newLineData.connectionData.pressureZone}
                        >
                            {
                                pressureZoneState.pressure_zones.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>)
                            }
                            <Option value={-1}>Unknown</Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
        </Fragment>
    )
}