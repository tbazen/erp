import React, {Fragment, useEffect} from "react";
import {Col, Divider, Form, Input, Row, Select} from "antd";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {fetchKebeleList} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {KebeleState} from "../../../../_model/state-model/mn-sm/kebele-state";
import {number} from "prop-types";
const { Option } = Select

interface IProps{
    newLineData : NewLineData
    handleConnectionDataChange: <T>( name : string , value:T )=> void
}
export function LocationForm(props: IProps){
    const formItemLayout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 14 },
    }

    const dispatch = useDispatch()
    const [ auth,kebeleState ] = useSelector<
        ApplicationState, [
            AuthenticationState,
            KebeleState
        ]>(appState=>[appState.auth,appState.kebele_state])
    useEffect(()=>{
        dispatch(fetchKebeleList({sessionId:auth.sessionId}))
    },[])

    return (
        <Fragment>
            <Row gutter={2}>
                <Col span={24}><Divider orientation="left">{'Location Information'}</Divider></Col>
                <Col span={12}>
                    <Form.Item label="Kebele" {...formItemLayout}>
                        <Select
                            onChange={(kebele: any)=>{ props.handleConnectionDataChange<number>('kebele',kebele) }}
                            defaultValue={props.newLineData.connectionData.kebele}
                        >
                            {
                                kebeleState.kebeles.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>)
                            }
                            <Option value={-1}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label={'Water Meter Longitude'} {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('waterMeterX',event.target.value) }}/>
                    </Form.Item>
                    <Form.Item label="Connection Longitude" {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('houseConnectionY',event.target.value) }} />
                    </Form.Item>
                    <Form.Item label="Parcel No." {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('parcelNo',event.target.value) }} />
                    </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item label="House No." {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('address',event.target.value)  }} />
                    </Form.Item>
                    <Form.Item label="Latitude" {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('waterMeterX',event.target.value)  }} />
                    </Form.Item>
                    <Form.Item label="Latitude" {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('houseConnectionX',event.target.value) }} />
                    </Form.Item>
                    <Form.Item label="Land Certificate No." {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('landCertificateNo',event.target.value) }} />
                    </Form.Item>
                </Col>
            </Row>
        </Fragment>
    )
}