
import React, { Fragment } from "react";
import {Col, Divider, Form, Input, Row, Select, Spin} from "antd";
import {SubscriptionType} from "../../../../_enum/mn-enum/subscription-type";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {MaterialOperationalStatus} from "../../../../_enum/mn-enum/material-operational-status";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {TransactionItemsState} from "../../../../_model/state-model/ic-sm/transaction-items-state";
import {MeterData} from "../../../../_model/view_model/mn-vm/meter-data";
const {Option } = Select

interface IProps{
    newLineData : NewLineData
    handleConnectionDataChange: <T>( name : string , value:T )=> void
}
export function MeterDetailForm(props : IProps){
    const [ transactionItemState ] = useSelector<ApplicationState,[ TransactionItemsState ]>(appState => [ appState.transaction_items_state ])
    const formItemLayout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 14 },
    }

    return (
        <Fragment>
            <Row gutter={2}>
                <Col span={24}><Divider orientation="left">{'Meter Detail'}</Divider></Col>
                <Col span={24}>
                    <Form.Item label="Meter Type" {...formItemLayout}>
                        <Spin spinning={transactionItemState.loading} delay={500}>
                            <Select
                                onChange={(itemCode: any)=>{props.handleConnectionDataChange<MaterialOperationalStatus>('itemCode',itemCode)}}
                                value={props.newLineData.connectionData.itemCode}
                            >
                                {
                                    transactionItemState.transactions_items.map((item,key)=><Option key={key} value={item.code}>{item.name}</Option>)
                                }
                            </Select>
                        </Spin>
                    </Form.Item>
                    <Form.Item label="Model" {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('modelNo',event.target.value)  }} />
                    </Form.Item>
                    <Form.Item label="Serial No." {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('serialNo',event.target.value)  }} />
                    </Form.Item>
                    <Form.Item label="Initial Reading" {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('initialMeterReading',event.target.value)  }} />
                    </Form.Item>


                    <Form.Item label="Operational Status" {...formItemLayout}>
                        <Select
                            onChange={(operationalStatus: any)=>{ props.handleConnectionDataChange<MaterialOperationalStatus>('opStatus',operationalStatus) }}
                            value={props.newLineData.connectionData.opStatus}
                        >
                            <Option value={MaterialOperationalStatus.Working}>Working</Option>
                            <Option value={MaterialOperationalStatus.Defective}>Defective</Option>
                            <Option value={MaterialOperationalStatus.NotWorking}>Not Working</Option>
                        </Select>
                    </Form.Item>
                </Col>
            </Row>
        </Fragment>
    )
}