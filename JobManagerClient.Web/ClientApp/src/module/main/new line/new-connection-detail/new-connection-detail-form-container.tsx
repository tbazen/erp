import React, {useEffect, useState} from 'react';
import {Col, Divider, Form, Row} from 'antd';
import {GeneralInformationForm} from "./general-information-form";
import {LocationForm} from "./location-form";
import {MeterDetailForm} from "./meter-detail-form";
import {ServeyDataForm} from "./servey-data-form";
import {RemarkForm} from "./remark-form";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../../_constants/routes";
import pushPathToBC from "../../../../_redux_setup/actions/breadcrumb-actions";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import SharedJobFormFields from "../../../../shared/components/job application form/shared-job-field";
import {SubscriberInformationContainer} from "../../../../shared/components/customer-information/customer-information";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {ActiveJobDetailInformationState} from "../../../../_model/state-model/aj-sm/active-job-detail-information-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {
    fetchActiveJobDetailInformation,
    fetchItemsInCategory,
    fetchWorkflowData
} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {
    GetJobPar,
    GetWorkFlowDataWebPar,
    SetWorkFlowDataWebPar,
    UpdateJobWebPar
} from "../../../../_services/job.manager.service";
import {StandardJobStatus} from "../../../../_model/job-rule-model/standard-job-status";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import {requestSetWorkflowDataWeb, requestUpdateJobWeb} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {TransactionItemsState} from "../../../../_model/state-model/ic-sm/transaction-items-state";
import {GetSystemParametersWebPar} from "../../../../_services/subscribermanagment.service";
import {useEditJobInitializer} from "../../../../shared/hooks/edit-job-intializer";

interface IProps{
    match : any
}

export function NewConnectionDetailFormContainer(props:IProps){
    const formLayout = 'horizontal';
    const dispatch = useDispatch();
    const jobId: number = +props.match.params.jobId.toString()
    const [
        auth ,
        jobDetail ,
        workflowdata_state,
    ] = useSelector< ApplicationState ,[ AuthenticationState , ActiveJobDetailInformationState, WorkflowDataState, TransactionItemsState ] >( appState => [
        appState.auth,
        appState.active_job_detail_information_state,
        appState.workflow_data_state,
        appState.transaction_items_state
    ]  );
    const [isSharedJobFormDisabled, setIsSharedJobFormDisabled] = useState<boolean>(false)
    const [ newLineWorkflowData , setNewLineWorkflowData ] = useState<NewLineData>(new NewLineData())
    const [ jobData, setJobData ] = useEditJobInitializer({jobId,jobType:StandardJobTypes.NEW_LINE});
    useEffect(()=>{
        const jobDetailParams: GetJobPar  = {
            sessionId:auth.sessionId,
            jobID:jobId
        }
        const categoryParams : GetSystemParametersWebPar = {
            sessionId:auth.sessionId,
            names : ['meterMaterialCategory']
        }
        dispatch(fetchItemsInCategory(categoryParams))
        jobDetail.payload === undefined &&
        dispatch(fetchActiveJobDetailInformation(jobDetailParams));
        },[])
    useEffect(()=>{
        if(  jobDetail.payload ){
            const isEnabled = jobDetail.payload.job.status === StandardJobStatus.APPLICATION || jobDetail.payload.job.status === StandardJobStatus.FINALIZATION
            setIsSharedJobFormDisabled(!isEnabled)

            const params : GetWorkFlowDataWebPar =
                {
                    key:0,
                    jobID:jobDetail.payload.job.id,
                    sessionId:auth.sessionId,
                    typeID:StandardJobTypes.NEW_LINE,
                    fullData:true
                }
            setJobData(jobDetail.payload.job)
            dispatch(fetchWorkflowData(params))
        }
    },[ jobDetail ])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        workflowdata_state.payload.connectionData !== undefined &&
        workflowdata_state.payload.connectionData !== null &&
        setNewLineWorkflowData(workflowdata_state.payload as NewLineData)
    },[ workflowdata_state ])

    function handleConnectionDataChange<T>( name : string , value:T ){
        const connectionData:Subscription = newLineWorkflowData.connectionData
        // @ts-ignore
        connectionData[name] = value
        setNewLineWorkflowData({...newLineWorkflowData,connectionData})
    }

    const handleSubmitJobData = ()=> {
        if(!isSharedJobFormDisabled){
            if(jobData){
                if(jobDetail.payload) {
                    const updateParams: UpdateJobWebPar<NewLineData> = {
                        job: jobData,
                        sessionId:auth.sessionId,
                        dataType:NAME_SPACES.WORKFLOW.NEW_LINE_DATA,
                        data:newLineWorkflowData
                    }
                    dispatch(requestUpdateJobWeb<NewLineData>(updateParams))
                }
            }
        } else{
            if(jobDetail.payload){
                const setWorkflowParams : SetWorkFlowDataWebPar<NewLineData> ={
                    sessionId:auth.sessionId,
                    dataType:NAME_SPACES.WORKFLOW.NEW_LINE_DATA,
                    data:{
                        ...newLineWorkflowData,
                        jobID:jobDetail.payload.job.id,
                        typeID:StandardJobTypes.NEW_LINE
                    }
                }
                dispatch(requestSetWorkflowDataWeb(setWorkflowParams))
            }
        }
    }


    return (
        <Row gutter={4}>
            <Col span={16}>
            <div style={{overflowY:'hidden',overflowX:'hidden'}}>
                <Form layout={formLayout}>
                    <div className={'paper'} style={{width:'100%',padding:'5px'}}>
                        <GeneralInformationForm
                            newLineData={newLineWorkflowData}
                            handleConnectionDataChange={handleConnectionDataChange}
                        />
                    </div>
                    <div className={'paper'} style={{width:'100%',padding:'5px',marginTop:'5px'}}>
                        <LocationForm
                            newLineData={newLineWorkflowData}
                            handleConnectionDataChange={handleConnectionDataChange}
                        />
                    </div>
                    <Row gutter={4} style={{marginTop:'5px'}}>
                        <Col span={12}>
                            <div className={'paper'} style={{width:'100%',padding:'5px'}}>
                                <MeterDetailForm
                                    newLineData={newLineWorkflowData}
                                    handleConnectionDataChange={handleConnectionDataChange}
                                />
                            </div>
                        </Col>
                        <Col span={12}>
                            <div className={'paper'} style={{width:'100%',padding:'5px'}}>
                                <ServeyDataForm
                                    newLineData={newLineWorkflowData}
                                    handleConnectionDataChange={handleConnectionDataChange}
                                />
                            </div>
                        </Col>
                    </Row>

                    <Row gutter={4}  className={'paper'} style={{marginTop:'5px'}}>
                        <Col span={21} offset={3} >
                            <div style={{width:'100%',padding:'5px'}}>
                                <RemarkForm
                                    newLineData={newLineWorkflowData}
                                    handleConnectionDataChange={handleConnectionDataChange}
                                />
                            </div>
                        </Col>
                    </Row>
                </Form>
            </div>
            </Col>

            <Col span={8}>
                <div>
                <div className={'paper'} style={{ padding: '10px'}}>
                    <SharedJobFormFields
                        dateChangeHandler={(date: string) => {
                           jobData && setJobData({...jobData,startDate:date,statusDate:date,logDate:date})
                        }}
                        descriptionChangeHandler={(desc: string) => {
                           jobData && setJobData({...jobData,description:desc})
                        }}
                        disableDate={ isSharedJobFormDisabled }
                        disableDescription = { isSharedJobFormDisabled }
                    />
                </div>
                <div className={'paper'}>
                    <div style={{ marginTop:'5px', padding: '10px' }}>
                        <Divider orientation={'left'}>Customer Information</Divider>
                        {
                            !jobDetail.loading &&
                            !jobDetail.error &&
                            jobDetail.payload !== undefined &&
                            <SubscriberInformationContainer {...jobDetail.payload.customer} />
                        }
                    </div>
                    <div style={{ padding: '10px' }} >
                        <CButton
                            block
                            disabled={jobDetail.payload === undefined}
                            type={'primary'}
                            onClick={handleSubmitJobData}
                        >Save Changes</CButton>
                    </div>
                </div>
                </div>
            </Col>
        </Row>
    )
}

