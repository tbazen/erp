import React,{ Fragment } from "react";
import TextArea from "antd/lib/input/TextArea";
import {Col, Divider, Form, Input, Row} from "antd";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";

interface IProps{
    newLineData : NewLineData
    handleConnectionDataChange: <T>( name : string , value:T )=> void
}
export function RemarkForm(props: IProps){
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    return (
        <Row>
            <Col span={24}><Divider orientation="left">{'Remark'}</Divider></Col>
            <Col span={24}>
                <Form.Item>
                    <TextArea
                        rows={4}
                        defaultValue={props.newLineData.connectionData.remark}
                        onChange={( event )=>{ props.handleConnectionDataChange<string>('remark',event.target.value)  }}
                         />
                </Form.Item>
            </Col>
        </Row>
    )
}