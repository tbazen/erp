import React, { Fragment } from "react";
import {Col, Divider, Form, Input, Row, Select} from "antd";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {SubscriptionType} from "../../../../_enum/mn-enum/subscription-type";
import {DataStatus} from "../../../../_enum/mn-enum/data-status";
import {ConnectionType} from "../../../../_enum/mn-enum/connection-type";
import {SupplyCondition} from "../../../../_enum/mn-enum/supply-condition";
import {MeterPositioning} from "../../../../_enum/mn-enum/meter-positioning";
const {Option } = Select

interface IProps{
    newLineData : NewLineData
    handleConnectionDataChange: <T>( name : string , value:T )=> void
}
export function ServeyDataForm(props : IProps){
    const formItemLayout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 14 },
    }

    return (
        <Fragment>
            <Row gutter={2}>
                <Col span={24}><Divider orientation="left">{'Servey Data'}</Divider></Col>
                <Col span={24}>
                    <Form.Item label="Data Status" {...formItemLayout}>
                        <Select
                            onChange={(dataStatus: any)=>{ props.handleConnectionDataChange<DataStatus>('dataStatus',dataStatus) }}
                            defaultValue={props.newLineData.connectionData.dataStatus}
                        >
                            <Option value={DataStatus.Complete_1}>Complete 1</Option>
                            <Option value={DataStatus.Partial_3}>Partial 3</Option>
                            <Option value={DataStatus.No_Meter_4}>No Meter 4</Option>
                            <Option value={DataStatus.Critical_Complete_2}>Critical Complete 2</Option>
                            <Option value={DataStatus.Unknown}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="Connection Type" {...formItemLayout}>
                        <Select
                            onChange={(connectionType: any)=>{ props.handleConnectionDataChange<ConnectionType>('connectionType',connectionType) }}
                            defaultValue={props.newLineData.connectionData.connectionType}
                        >
                            <Option value={ConnectionType.Yard_1}>Yard 1</Option>
                            <Option value={ConnectionType.House_2}>House 2</Option>
                            <Option value={ConnectionType.Shared_3}>Shared 3</Option>
                            <Option value={ConnectionType.Unknown}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="Supply Condition" {...formItemLayout}>
                        <Select
                            onChange={(supplyCondition: any)=>{ props.handleConnectionDataChange<SupplyCondition>('supplyCondition',supplyCondition) }}
                            defaultValue={props.newLineData.connectionData.supplyCondition}
                        >
                            <Option value={SupplyCondition.Bad_3}>Bad 3</Option>
                            <Option value={SupplyCondition.Very_Bad_4}>Very Bad 4</Option>
                            <Option value={SupplyCondition.Good_2}>Good 2</Option>
                            <Option value={SupplyCondition.Very_Good_1}>Very Good 1</Option>
                            <Option value={SupplyCondition.Unknown}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="Meter Location" {...formItemLayout}>
                        <Select
                            onChange={(meterPosition: any)=>{ props.handleConnectionDataChange<MeterPositioning>('meterPosition',meterPosition) }}
                            defaultValue={props.newLineData.connectionData.meterPosition}
                        >
                            <Option value={MeterPositioning.Bad_For_Reading_2}>Bad for reading 2</Option>
                            <Option value={MeterPositioning.Good_For_Reading_1}>Good for reading 1</Option>
                            <Option value={MeterPositioning.Unknown}>Unknown</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="Household Size" {...formItemLayout}>
                        <Input onChange={( event )=>{ props.handleConnectionDataChange<string>('householdSize',event.target.value)  }} />
                    </Form.Item>
                </Col>
            </Row>
        </Fragment>
    )
}