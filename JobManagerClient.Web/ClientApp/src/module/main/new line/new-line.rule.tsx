import React from "react";
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import NewLine from "./new-line";
import { NewlineWorkFlowData} from "./workflow-data/newline-workflow-data-container";
import { JobData} from "../../../_services/job.manager.service";

export class NewLineRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return NewLine
    }
    public getWorkFlowData(job: JobData) {
        return <NewlineWorkFlowData job={job}/>
    }

}