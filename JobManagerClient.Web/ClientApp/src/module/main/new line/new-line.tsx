import { Button, Col, Divider, Row } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from "react-redux"
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information'
import CustomersListTable from '../../../shared/components/customer-list-table/customer-list-table'
import CustomerSearchBar from '../../../shared/components/customer-list-table/customer-search-bar'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field'
import { CAlert } from '../../../shared/core/calert/calert'
import { ROUTES } from "../../../_constants/routes"
import { initialCustomerData } from '../../../_helpers/initial value/init-customer-data'
import { initialJobData } from '../../../_helpers/initial value/init-jobdata'
import { ApplicationState } from '../../../_model/state-model/application-state'
import { AuthenticationState } from '../../../_model/state-model/auth-state'
import { IBreadcrumb } from "../../../_model/state-model/breadcrumd-state"
import { Subscriber, SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result'
import {
    requestAddJob,
    requestAddJobWeb,
    subscriberListClear
} from '../../../_redux_setup/actions/mn-actions/main-actions'
import { AddJobPar } from '../../../_services/job.manager.service'
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {NewLineData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {useBreadcrumb} from "../../../shared/hooks/manage-breadcrumb";
import {JobDataExtension} from "../../../_helpers/extensions/job-manager/job-data-extension";
import {useReduxStoreCleaner} from "../../../shared/hooks/clean-up";

const backwardPath: IBreadcrumb[] = [
    {
        path: ROUTES.MAIN.INDEX,
        breadcrumbName: 'Tools'
    },
    {
        path: ROUTES.JOB.NEW_LINE.INDEX,
        breadcrumbName: 'New Line'
    }
]

const NewLine = () => {

    const dispatch = useDispatch()
    const [auth] = useSelector<ApplicationState, [AuthenticationState]>((appState) => [appState.auth])
    const initJobData: AddJobPar = { ...initialJobData, sessionId: auth.sessionId, job: { ...initialJobData.job, applicationType: StandardJobTypes.NEW_LINE } }
    const [selectedCustomer, setSelectedCustomer] = useState<SubscriberSearchResult | undefined>(undefined)
    const [existingCustomerJobData, setExistingCustomerJobData] = useState<AddJobPar>(initJobData)
    const [newCustomerJobData, setNewCustomerJobData] = useState<AddJobPar>({ ...initJobData, job: { ...initJobData.job, newCustomer: initialCustomerData } })

    useBreadcrumb(backwardPath)
    useReduxStoreCleaner([subscriberListClear])

    //Effect to update selected customer
    useEffect(() => {
        if(selectedCustomer)
            setExistingCustomerJobData({ ...existingCustomerJobData, job: { ...existingCustomerJobData.job, customerID: selectedCustomer.subscriber.id } })
    }, [selectedCustomer])

    const newCustomerJobDataValidationHandler= () =>  JobDataExtension.validate(newCustomerJobData.job)
    const handleNewCustomerDataChange = (customer: Subscriber) => { setNewCustomerJobData({ ...newCustomerJobData, job: { ...newCustomerJobData.job, newCustomer: customer } }) }
    const submitNewCustomerJobData = () => { dispatch(requestAddJob(newCustomerJobData)) }
    const submitExistingCustomerJobData = () => {
        existingCustomerJobData !== undefined &&
        dispatch(requestAddJobWeb<NewLineData>( { sessionId:auth.sessionId, job: existingCustomerJobData.job,data:null , dataType:NAME_SPACES.WORKFLOW.NEW_LINE_DATA } ))
    }


    return (
        <Row gutter={4}>
            <Col span={16}>
                <CustomerSearchBar
                    appendedForm={
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => setNewCustomerJobData({ ...newCustomerJobData, job: { ...newCustomerJobData.job, startDate: date, statusDate: date, logDate: date } })}
                            descriptionChangeHandler={(desc: string) => setNewCustomerJobData({ ...newCustomerJobData, job: { ...newCustomerJobData.job, description: desc } })}
                            formItemLayout={'horizontal'}
                        />
                    }
                    title={'New Line Form'}
                    validationHandler={newCustomerJobDataValidationHandler}
                    updateCustomerData={handleNewCustomerDataChange}
                    submitCustomerData={submitNewCustomerJobData}
                />
                <CustomersListTable
                    setSelectedCustomers={setSelectedCustomer}
                    withSelection
                    height='66vh'
                />
            </Col>
            <Col span={8}>
                <Row style={{ marginBottom: '5px' }}>
                    <div className={'paper'} style={{ padding: '10px', backgroundColor: '#fff' }}>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => setExistingCustomerJobData({ ...existingCustomerJobData, job: { ...existingCustomerJobData.job, startDate: date, statusDate: date, logDate: date } })}
                            descriptionChangeHandler={(desc: string) => setExistingCustomerJobData({ ...existingCustomerJobData, job: { ...existingCustomerJobData.job, description: desc } })}
                        />
                    </div>
                </Row>
                <Row>
                    <div className={'paper'}>
                        <div style={{ width: '100%', overflow: 'auto', maxHeight: '52vh', padding: '10px' }}>
                            <p>Selected Customer</p>
                            <Divider />
                            {
                                selectedCustomer === undefined &&
                                <Row>
                                    <CAlert
                                        message="Customer Not Selected"
                                        description='To create new line please select one customer, or use the "Add new Customer" button to create new customer!'
                                        type="error"
                                    />
                                </Row>
                            }
                            { selectedCustomer !== undefined && <CustomerInformationContainer {...selectedCustomer} /> }
                        </div>
                        <div style={{ padding: '10px' }} >
                            <Button block onClick={submitExistingCustomerJobData} disabled={selectedCustomer === undefined}>Submit</Button>
                        </div>
                    </div>
                </Row>
            </Col>
        </Row>
    )
}

export default NewLine