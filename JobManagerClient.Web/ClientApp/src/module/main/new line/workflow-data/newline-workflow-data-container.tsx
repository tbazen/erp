import React, { Fragment, useEffect, useState} from "react";
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {NewLineData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import BNFinanceService from "../../../../_services/bn.finance.service";
import {TransactionItems} from "../../../../_model/level0/iERP-transaction-model/transaction-items";
import {DataRow} from "../../../../shared/components/data-row/data-row";


const bnfinance_service = new BNFinanceService()



interface IProps{
    job : JobData
}
export function NewlineWorkFlowData(props  : IProps){
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ newLineData, setNewLineData ] = useState<NewLineData>(new NewLineData())
    const [ transactionItem, setTransactionItem ] = useState<TransactionItems>()
    const dispatch = useDispatch()

    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:props.job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.NEW_LINE,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])

    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        workflowdata_state.payload.connectionData !== undefined &&
        workflowdata_state.payload.connectionData !== null &&
        setNewLineData(workflowdata_state.payload as NewLineData)
    },[ workflowdata_state ])

    useEffect(()=>{
        newLineData.connectionData &&
        newLineData.connectionData.meterData &&
        loadTransactionItem()
    },[ newLineData ])

    async function loadTransactionItem(){
        if(newLineData.connectionData && newLineData.connectionData.meterData ){
            const item : TransactionItems = (await bnfinance_service.GetTransactionItems({ sessionId:auth.sessionId,code:newLineData.connectionData.meterData.itemCode||'' })).data
            setTransactionItem(item)
        }
    }
    return (
        <div>
            {
                !workflowdata_state.loading &&
                !workflowdata_state.error &&
                workflowdata_state.payload !== null &&
                workflowdata_state.payload !== undefined &&
                newLineData.connectionData!== undefined &&
                <Fragment>
                    <DataRow
                        title={'Contract Number'}
                        content={newLineData.connectionData.contractNo===null?'' : newLineData.connectionData.contractNo}
                    />
                    <DataRow
                        title={'Meter Number'}
                        content={newLineData.connectionData.meterData.itemCode+''}
                    />
                    <DataRow
                        title={'House Number'}
                        content={newLineData.connectionData.address===null?'' : newLineData.connectionData.address }
                    />
                    <DataRow
                        title={'Meter type'}
                        content={transactionItem && transactionItem.name || ''}
                    />
                    <DataRow
                        title={'Kebele'}
                        content={newLineData.connectionData.kebele > 0? newLineData.connectionData.kebele.toString() :''}
                    />
                </Fragment>
            }
        </div>
    )
}