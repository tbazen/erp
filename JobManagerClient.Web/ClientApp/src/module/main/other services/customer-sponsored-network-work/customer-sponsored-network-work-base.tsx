import React from 'react'
import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base";
import {CustomerSponsoredNetworkWorkFormContainerBase} from "./customer-sponsored-network-work-form-container";
import {CustomerSponsoredNetworkWorkWFDataContainer} from "./workflow/customer-sponsored-network-work-wf-data-container";
import {JobData} from "../../../../_services/job.manager.service";


export class CustomerSponsoredNetworkWorkBase extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return CustomerSponsoredNetworkWorkFormContainerBase
    }

    public getWorkFlowData(job: JobData): JSX.Element {
        return <CustomerSponsoredNetworkWorkWFDataContainer job={job}/>
    }
}
