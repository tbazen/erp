import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {SubscriberSearchResult} from '../../../../_model/view_model/mn-vm/subscriber-search-result'
import {Col, Row} from 'antd'
import SharedJobFormFields from '../../../../shared/components/job application form/shared-job-field'
import {ApplicationState} from '../../../../_model/state-model/application-state'
import {AuthenticationState} from '../../../../_model/state-model/auth-state'
import {AddJobWebPar} from '../../../../_services/job.manager.service'
import {requestUpdateJobWeb} from '../../../../_redux_setup/actions/mn-actions/main-actions'
import CButton from '../../../../shared/core/cbutton/cbutton'
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {GeneralSellsData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";
import {useEditJobInitializer} from "../../../../shared/hooks/edit-job-intializer";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";

interface IProps {
    match: any
}

export const GeneralPaidServiceEditForm = (props:IProps) =>{
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [jobData, setJobData] = useEditJobInitializer({jobId,jobType:StandardJobTypes.GENERAL_SELL })
    const [auth,workflowdata_state] = useSelector<ApplicationState, [AuthenticationState,WorkflowDataState]>(appState => [appState.auth,appState.workflow_data_state])
    const [ generalSellsData, setGeneralSellsData ] = useState<GeneralSellsData>()
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setGeneralSellsData(workflowdata_state.payload as GeneralSellsData)
    },[ workflowdata_state ])
    const submitGeneralCellData = () => {
        if(jobData && generalSellsData){
            const params: AddJobWebPar<GeneralSellsData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: generalSellsData,
                dataType: NAME_SPACES.WORKFLOW.GENERAL_SELLS_DATA
            }
            dispatch(requestUpdateJobWeb(params))
        }
    }
    return(
        <Row gutter={4}>
            <Col span={16}>
                <div className={'paper'} style={{padding:'10px'}}>
                </div>
            </Col>
            <Col span={8}>
                <div className={'paper'} style={{padding:'10px'}}>
                    <SharedJobFormFields
                        dateChangeHandler={(date: string) =>jobData && setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                        descriptionChangeHandler={(desc: string) =>jobData && setJobData({ ...jobData, description: desc })}
                    />
                </div>

                <div className={'paper'} style={{padding:'10px',marginTop:'5px'}} >
                    <CButton
                        block
                        type={'primary'}
                        onClick={submitGeneralCellData}>Submit</CButton>
                </div>

            </Col>
        </Row>
    )
}