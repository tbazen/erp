import React from 'react'
import {JobRuleClientHandlerBase} from "../../../../_model/job-rule-model/job-rule-client-handler-base"
import GeneralPaidService from "./general-paid-services"
import {JobRuleClientDecorator} from "../../../../_decorator/jobrule-client-handler.decorator"
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types"
import {ROUTES} from "../../../../_constants/routes"
import {JobData} from "../../../../_services/job.manager.service"
import {GeneralPaidServiceWorkflowDataContainer} from "./workflow/general-paid-service-workflow-data-container"
import {BillDepositEditForm} from "../../billing deposit/bill-deposit-edit-form";
import {GeneralPaidServiceEditForm} from "./general-paid-service-edit-form";


@JobRuleClientDecorator({
    jobName:"General Paid Service",
    jobType:StandardJobTypes.GENERAL_SELL,
    route : ROUTES.JOB.OTHER_SERVICE.GENERAL_CELL,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.GENERAL_SELL].ABSOLUTE,
            component:GeneralPaidServiceEditForm
        }
    ]
})
export class GeneralPaidServiceRule extends JobRuleClientHandlerBase {
    public getNewApplicationForm(){
        return GeneralPaidService
    }
    public getWorkFlowData(job:JobData){
        return <GeneralPaidServiceWorkflowDataContainer job={job}/>
    }
}