import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import pushPathToBC from '../../../../_redux_setup/actions/breadcrumb-actions'
import { ROUTES } from '../../../../_constants/routes'
import { IBreadcrumb } from '../../../../_model/state-model/breadcrumd-state'
import { SubscriberSearchResult, Subscriber } from '../../../../_model/view_model/mn-vm/subscriber-search-result'
import { Row, Col, Divider } from 'antd'
import CustomerSearchBar from '../../../../shared/components/customer-list-table/customer-search-bar'
import SharedJobFormFields from '../../../../shared/components/job application form/shared-job-field'
import CustomersListTable from '../../../../shared/components/customer-list-table/customer-list-table'
import { ApplicationState } from '../../../../_model/state-model/application-state'
import { AuthenticationState } from '../../../../_model/state-model/auth-state'
import {AddJobWebPar, JobData} from '../../../../_services/job.manager.service'
import { initialJobData } from '../../../../_helpers/initial value/init-jobdata'
import {requestAddJobWeb} from '../../../../_redux_setup/actions/mn-actions/main-actions'
import CButton from '../../../../shared/core/cbutton/cbutton'
import CustomerInformationContainer from '../../../../shared/components/customer-information/customer-information'
import { CAlert } from '../../../../shared/core/calert/calert'
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {NAME_SPACES} from "../../../../_constants/model-namespaces";
import {GeneralSellsData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/new-line-data";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.INDEX,
        breadcrumbName:'Other Services'
    },
    {
        path:ROUTES.JOB.OTHER_SERVICE.GENERAL_CELL,
        breadcrumbName:'General Paid Service'
    }
]

const GeneralPaidService = () =>{
    const dispatch = useDispatch()
    const [selectedCustomer,setSelectedCustomer] = useState<SubscriberSearchResult | undefined>(undefined)
    const [jobData, setJobData] = useState<JobData>({...initialJobData.job, applicationType: StandardJobTypes.GENERAL_SELL})
    const [auth] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])

    const [ newCustomer , setNewCustomer] = useState<Subscriber>()
    useEffect(()=>{
        dispatch(pushPathToBC(backwardPath))
        return () => {
            dispatch(pushPathToBC([]))    
        };
    },[])

    const handleNewCustomerDataChange = (customer: Subscriber) => {
        setNewCustomer(customer)
    }
    const submitNewCustomerGeneralSellData = () => {
        if(newCustomer){
            const generalSellsData = new GeneralSellsData()
            jobData.newCustomer = newCustomer;
            jobData.customerID = -1;
            const params: AddJobWebPar<GeneralSellsData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: generalSellsData,
                dataType: NAME_SPACES.WORKFLOW.GENERAL_SELLS_DATA
            }
            dispatch(requestAddJobWeb(params))
        }
    }
    const submitExistingCustomerGeneralCellData = () => {
        if(selectedCustomer){
            const generalSellsData = new GeneralSellsData()
            jobData.customerID = selectedCustomer.subscriber.id;
            jobData.newCustomer = null;
            const params: AddJobWebPar<GeneralSellsData>={
                job:jobData,
                sessionId: auth.sessionId,
                data: generalSellsData,
                dataType: NAME_SPACES.WORKFLOW.GENERAL_SELLS_DATA
            }
            dispatch(requestAddJobWeb(params))
        }
    }
    
    
    return(
        <Row gutter={4}>
            <Col span={16}>
            <CustomerSearchBar
                    appendedForm={
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                            formItemLayout={'horizontal'}
                        />
                    }
                    title={'General Paid Service'}
                    updateCustomerData={handleNewCustomerDataChange}
                    submitCustomerData={submitNewCustomerGeneralSellData}
                />
            <CustomersListTable
                                setSelectedCustomers= {setSelectedCustomer}
                                withSelection
                                height='66vh'
            />
            </Col>
            <Col span={8}>
                <Row style={{marginBottom:'5px'}}>
                    <div className={'paper'} style={{padding:'10px',backgroundColor:'#fff'}}>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                </Row>
                <Row>
                    <div className={'paper'}>
                        <div style={{width:'100%',overflow:'auto',maxHeight:'52vh',padding:'10px'}}>
                            <p>Selected Customer</p>
                            <Divider/>
                            {
                                selectedCustomer === undefined &&
                                <Row>
                                     <CAlert
                                        message="Customer Not Selected"
                                        description='To create general paid service please select one customer, or use the "Add new Customer" button to create new customer!'
                                        type="error"
                                    />
                                </Row>
                            }
                            {
                                selectedCustomer !== undefined &&
                                <Row>
                                    <CustomerInformationContainer {...selectedCustomer} />
                                </Row>
                            }
                        </div>
                        <div style={{padding:'10px'}} >
                           <CButton
                               block
                               type={'primary'}
                               onClick={submitExistingCustomerGeneralCellData} disabled={selectedCustomer === undefined} >Submit</CButton>
                        </div>
                    </div>
                </Row>
            </Col>
        </Row>
    )
}
export default GeneralPaidService