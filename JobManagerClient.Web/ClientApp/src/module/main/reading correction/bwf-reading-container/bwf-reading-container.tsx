import React, {ReactNode} from 'react';
import {IBWFMeterReadingPayload} from "../../../../_model/state-model/mn-sm/bwf-meter-reading-state";
import {Col, Divider, Row, Typography} from "antd";
import {MeterReadingType} from "../../../../_model/level0/subscriber-managment-type-library/mater-reading-type";
import {ReadingExtraInfo} from "../../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";

const {Text, Paragraph} = Typography;


const DataRow = ({title,content}:{title:string,content:ReactNode})=>{
    return (
        <Row gutter={4}>
            <Col lg={{span:6}}>
                <Text strong>{title}</Text>
            </Col>
            <Col lg={{span:18}}>
                <Text>{content}</Text>
            </Col>
        </Row>
    )
}


interface IProps{
    materReading : IBWFMeterReadingPayload
}

export function BWFReadingContainer(props: IProps){
    let readingTypeMessage: string ='';
    switch (props.materReading.reading.readingType) {
        case MeterReadingType.MeterReset:
            readingTypeMessage = 'Mater is reset';
            break;
        case MeterReadingType.Average:
            readingTypeMessage = `Mater is estimated by averaging last ${props.materReading.reading.averageMonths} months reading`
            break;
    }
    return (
        <div>
             <DataRow title={'Date'} content={props.materReading.period.name}/>
             <DataRow title={'Consumption'} content={props.materReading.reading.consumption}/>
             <DataRow title={'Meter reading type'} content={readingTypeMessage}/>
            {
                props.materReading.reading.extraInfo === ReadingExtraInfo.Coordinate &&
                <DataRow title={'Read On'} content={props.materReading.reading.readingTime}/>
            }
            {
                props.materReading.reading.extraInfo === ReadingExtraInfo.Coordinate &&
                <Row gutter={4}>
                    <Col span={24}><Divider orientation={'left'}>Coordinates</Divider></Col>
                    <Col span={12}><DataRow title={'Longitude'} content={props.materReading.reading.readingY}/></Col>
                    <Col span={12}><DataRow title={'Latitude'} content={props.materReading.reading.readingX}/></Col>
                </Row>
            }
            {
                props.materReading.reading.extraInfo === ReadingExtraInfo.Remark &&
                <DataRow title={'Reading remark'} content={props.materReading.reading.readingRemark}/>
            }
        </div>
    )
}