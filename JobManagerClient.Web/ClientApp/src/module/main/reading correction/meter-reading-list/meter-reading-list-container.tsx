import React, {useEffect, useState} from 'react'
import {BWFGetMeterReadings2Par} from "../../../../_services/subscribermanagment.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {BWFMeterReadingListState} from "../../../../_model/state-model/mn-sm/bwf-meter-reading-list-state";
import {
    BWFMeterReading,
    MeterReadingFilter
} from "../../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import {BillPeriod} from "../../../../_model/level0/subscriber-managment-type-library/bill-period";
import {fetchBWFMeterReadingList} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {Checkbox, Table} from "antd";
import {IBWFMeterReadingPayload} from "../../../../_model/state-model/mn-sm/bwf-meter-reading-state";
const { Column } = Table

interface IProps {
    connectionId: number,
    readings: BWFMeterReading[]
    period: BillPeriod
    addSelectedReading: (reading: BWFMeterReading) => void
    removeSelectedReading: (reading: BWFMeterReading) => void
}


export function MeterReadingListContainer(props:IProps) {
    const dispatch = useDispatch()
    const [ localMeterReading,setLocalMaterReading]= useState<IBWFMeterReadingPayload[]>([]);
    const [auth, meterReadingListState] = useSelector<ApplicationState,[ AuthenticationState,BWFMeterReadingListState ]>(appState=>[appState.auth,appState.bwf_meter_reading_list_state])
    let index = 0;
    useEffect(()=>{
        const meterReadingFilter = new MeterReadingFilter()
        meterReadingFilter.subscriptions = [props.connectionId]
        const filterParams : BWFGetMeterReadings2Par ={
            sessionId: auth.sessionId,
            filter:meterReadingFilter,
            index:0,
            pageSize: -1,
        }
        dispatch(fetchBWFMeterReadingList(filterParams))
    },[ props.period ])
    useEffect(()=>{
        const filteredArray = meterReadingListState.payload.filter(p=> (new Date(p.period.fromDate) < new Date(props.period.fromDate))  )
        setLocalMaterReading(filteredArray)
    },[meterReadingListState])


    return (
        <div>
            <Table
                dataSource={localMeterReading}
                size={'small'}
                loading={meterReadingListState.loading}
                pagination={false}
            >
                <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }}/>
                <Column
                    title="Period"
                    key="selectScheme"
                    render={(text , rowData:IBWFMeterReadingPayload)=><Checkbox onChange={
                        (event)=>{
                            if(event.target.checked){
                                props.addSelectedReading(rowData.reading)
                            } else{
                                props.removeSelectedReading(rowData.reading)
                            }
                        }}  >{ rowData.period.name}</Checkbox>}
                />
                <Column title="Reading" dataIndex="reading.reading" key="reading" />
                <Column title="Consumption" dataIndex="reading.consumption" key="consumption" />
                <Column title="Reading Type" dataIndex="reading.readingType" key="amount" />
            </Table>
        </div>
    )
}