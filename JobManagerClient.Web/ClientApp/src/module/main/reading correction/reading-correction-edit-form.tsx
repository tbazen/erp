import React, {useEffect, useState} from 'react'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import {Col, Collapse, Divider, Form, Icon, Input, Row, Select} from 'antd';
import CButton from '../../../shared/core/cbutton/cbutton';
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {BWFMeterReadingState} from "../../../_model/state-model/mn-sm/bwf-meter-reading-state";
import {BWFGetMeterReadingByPeriodIDPar, GetBillPeriodsPar} from "../../../_services/subscribermanagment.service";
import {
    fetchBillPeriods,
    fetchBWFMeterReading,
    requestUpdateJobWeb
} from "../../../_redux_setup/actions/mn-actions/main-actions";
import {currentYear, years} from "../../../_helpers/date-util";
import {BillPeriodState} from "../../../_model/state-model/mn-sm/bill-period-state";
import CLoadingPage from "../../../shared/screens/cloading/cloading-page";
import {BWFReadingContainer} from "./bwf-reading-container/bwf-reading-container";
import {CAlert} from "../../../shared/core/calert/calert";
import {AddJobWebPar} from "../../../_services/job.manager.service";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {ReadingCorrectionData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/reading-correction-data";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {MeterReadingListContainer} from "./meter-reading-list/meter-reading-list-container";
import {MeterReadingType} from "../../../_model/level0/subscriber-managment-type-library/mater-reading-type";
import {BWFMeterReading} from "../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import {BillPeriod} from "../../../_model/level0/subscriber-managment-type-library/bill-period";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {StateMachineHOC} from "../../../shared/hoc/StateMachineHOC";

const { Panel } = Collapse;
const {Option } = Select
const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: 0,
    overflow: 'hidden',
};
interface IProps {
    match: any
}
export const ReadingCorrectionEditForm = (props : IProps)=>{
    const formLayout = 'horizontal';
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [ auth, bwfMaterReadingState,billPeriodState,workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState, BWFMeterReadingState, BillPeriodState,WorkflowDataState ]>( appState=>[ appState.auth,appState.bwf_meter_reading_state, appState.bill_period_state,appState.workflow_data_state]  )
    const [ year , setYear ] = useState<number>(currentYear);
    const [ periodID , setPeriodId ] = useState<number>()
    const [ averageReadings , setAverageReadings ] = useState<BWFMeterReading[]>([])
    const [ selectedPeriod , setSelectedPeriod  ] = useState<BillPeriod>()
    const [jobData, setJobData] = useEditJobInitializer({jobId,jobType:StandardJobTypes.READING_CORRECTION })
    const [ readingType , setReadingType ] = useState<MeterReadingType>(MeterReadingType.Normal)
    const [ readingAmount , setReadingAmount ] = useState<number>(0)
    const [ consumptionAmount, setConsumptionAmount ] = useState<number>(0)
    const [readingCorrectionData, setReadingCorrectionData] = useState<ReadingCorrectionData>()
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setReadingCorrectionData(workflowdata_state.payload as ReadingCorrectionData)
    }, [workflowdata_state])
    useEffect(()=>{
        const params: GetBillPeriodsPar ={
            year,
            sessionId:auth.sessionId,
            ethiopianYear:true
        }
        dispatch(fetchBillPeriods(params))
    },[ year ])
    useEffect(()=>{
        const period = billPeriodState.payload.find(p=>p.id === periodID)
        setSelectedPeriod(period)
        if(periodID && jobData){
            const meterReadingParams: BWFGetMeterReadingByPeriodIDPar ={
                periodID,
                sessionId: auth.sessionId,
                subcriptionID: jobData.customerID,
            }
            dispatch(fetchBWFMeterReading(meterReadingParams))
        }
    },[periodID])

    const submitReadingData = () =>{
        if(readingCorrectionData && jobData){
            if(selectedPeriod){
                const readingCorrectionWorkflowData = readingCorrectionData
                if(!readingCorrectionWorkflowData.newReading)
                    readingCorrectionWorkflowData.newReading = new BWFMeterReading()
                readingCorrectionWorkflowData.newReading.periodID = selectedPeriod.id
                readingCorrectionWorkflowData.newReading.readingType = readingType
                if(
                    (readingCorrectionWorkflowData.newReading.readingType == MeterReadingType.MeterReset ) ||
                    (readingCorrectionWorkflowData.newReading.readingType == MeterReadingType.Normal )
                ){
                    readingCorrectionWorkflowData.newReading.reading = readingAmount
                }
                if(readingCorrectionWorkflowData.newReading.readingType !== MeterReadingType.Normal ){
                    readingCorrectionWorkflowData.newReading.consumption = consumptionAmount
                }
                const jobParams: AddJobWebPar<ReadingCorrectionData> = {
                    job:jobData,
                    sessionId:auth.sessionId,
                    dataType:NAME_SPACES.WORKFLOW.READING_CORRECTION_DATA,
                    data:readingCorrectionWorkflowData
                }
                dispatch(requestUpdateJobWeb(jobParams))
            }
        }
    }
    const removeFromAverageReading = (reading: BWFMeterReading)=>{
        setAverageReadings([...averageReadings.filter(r=>r.periodID !== reading.periodID)])
    }
    const addToAverageReading = ( reading: BWFMeterReading) => {
        setAverageReadings([...averageReadings, reading])
    }
    return (
        <Form layout={formLayout}>
            <Row gutter={4}>
                <Col lg={{span:16}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Divider orientation={'left'}>Reading Information</Divider>
                        <StateMachineHOC
                            state={bwfMaterReadingState}
                            fallBack={<CAlert
                                type={'error'}
                                message={bwfMaterReadingState.message}/>
                            }
                        >
                            { bwfMaterReadingState.payload &&  <BWFReadingContainer materReading={bwfMaterReadingState.payload}/> }
                        </StateMachineHOC>
                        <Form.Item label="Period" {...formItemLayout}>
                            <Row gutter={4}>
                                <Col span={12}>
                                    <Select
                                        onChange={(periodID)=>{ setPeriodId(+periodID)}}
                                        style={{width:'100%'}}
                                        loading = {billPeriodState.loading}>
                                        { billPeriodState.payload.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                                    </Select>
                                </Col>
                                <Col span={12}>
                                    <Select
                                        defaultValue={year} onChange={(year)=>setYear(+year)}
                                        style={{width:'100%'}}
                                        disabled={billPeriodState.loading}>
                                        { years.map((value,key)=><Option key={key} value={value}>{value}</Option>) }
                                    </Select>
                                </Col>
                            </Row>
                        </Form.Item>
                        <Form.Item label="Reading" {...formItemLayout}>
                            <Input onChange={(event)=>setReadingAmount(+event.target.value)}/>
                        </Form.Item>
                        <Form.Item label="New Consumption" {...formItemLayout}>
                            <Input onChange={(event)=>setConsumptionAmount(+event.target.value)}/>
                        </Form.Item>
                        <Form.Item label="New Type" {...formItemLayout}>
                            <Select
                                placeholder="Select Type"
                                style={{width:'100%'}}
                                value={readingType}
                                onChange={(readingType:MeterReadingType)=>{setReadingType(readingType)}}
                            >
                                <Option  value={MeterReadingType.MeterReset}>Mater Reset</Option>
                                <Option  value={MeterReadingType.Normal}>Normal</Option>
                            </Select>
                        </Form.Item>
                        <Collapse
                        bordered={false}
                        expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
                    >
                        <Panel header={averageReadings.length > 0? `Average of ${averageReadings.length} readings` : 'Calculate Average'} key="1" style={customPanelStyle}>
                            {
                                selectedPeriod !== undefined &&
                                jobData &&
                                <MeterReadingListContainer
                                    addSelectedReading={addToAverageReading}
                                    removeSelectedReading={removeFromAverageReading}
                                    readings={averageReadings}
                                    period={selectedPeriod}
                                    connectionId={jobData.customerID}/>
                            }
                            {
                                selectedPeriod == undefined &&
                                <CAlert message={'Please select period'}/>
                            }
                        </Panel>
                    </Collapse>
                    </div>
                </Col>

                <Col lg={{span:8}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) =>jobData && setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date})}
                            descriptionChangeHandler={(desc: string) => jobData && setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                    <div className={'paper'} style={{marginTop:'5px'}}>
                        <CButton onClick={submitReadingData} type={'primary'} block>submit</CButton>
                    </div>
                </Col>
            </Row>

        </Form>
    )
}