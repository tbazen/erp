import React, {useEffect, useState} from 'react'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import {SubscriberSearchResult} from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {Col, Collapse, Divider, Icon, Input, Row, Select} from 'antd';
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information';
import CFormItem from '../../../shared/core/cform-item/cform-item';
import CButton from '../../../shared/core/cbutton/cbutton';
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {BWFMeterReadingState} from "../../../_model/state-model/mn-sm/bwf-meter-reading-state";
import {BWFGetMeterReadingByPeriodIDPar, GetBillPeriodsPar} from "../../../_services/subscribermanagment.service";
import {
    fetchBillPeriods,
    fetchBWFMeterReading,
    requestAddJobWeb
} from "../../../_redux_setup/actions/mn-actions/main-actions";
import {currentYear, years} from "../../../_helpers/date-util";
import {BillPeriodState} from "../../../_model/state-model/mn-sm/bill-period-state";
import CLoadingPage from "../../../shared/screens/cloading/cloading-page";
import {BWFReadingContainer} from "./bwf-reading-container/bwf-reading-container";
import {CAlert} from "../../../shared/core/calert/calert";
import {AddJobWebPar, JobData} from "../../../_services/job.manager.service";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {ReadingCorrectionData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/reading-correction-data";
import {initialJobData} from "../../../_helpers/initial value/init-jobdata";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {MeterReadingListContainer} from "./meter-reading-list/meter-reading-list-container";
import {MeterReadingType} from "../../../_model/level0/subscriber-managment-type-library/mater-reading-type";
import {BWFMeterReading} from "../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import {BillPeriod} from "../../../_model/level0/subscriber-managment-type-library/bill-period";
import {StateMachineHOC} from "../../../shared/hoc/StateMachineHOC";

const { Panel } = Collapse;
const {Option } = Select
const customPanelStyle = {
    background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: 0,
    overflow: 'hidden',
};
interface IProps{
    data : SubscriberSearchResult
}
const ReadingCorrectionForm = (props : IProps)=>{
    const dispatch = useDispatch()
    const [ auth, bwfMaterReadingState,billPeriodState ] = useSelector<ApplicationState,[ AuthenticationState, BWFMeterReadingState, BillPeriodState ]>( appState=>[ appState.auth,appState.bwf_meter_reading_state, appState.bill_period_state]  )
    const [ year , setYear ] = useState<number>(currentYear);
    const [ periodID , setPeriodId ] = useState<number>()
    const [ averageReadings , setAverageReadings ] = useState<BWFMeterReading[]>([])
    const [ selectedPeriod , setSelectedPeriod  ] = useState<BillPeriod>()
    const [ jobData, setJobData] = useState<JobData>({...initialJobData.job, customerID: props.data.subscriber.id, applicationType: StandardJobTypes.READING_CORRECTION})
    const [ readingType , setReadingType ] = useState<MeterReadingType>(MeterReadingType.Normal)
    const [ readingAmount , setReadingAmount ] = useState<number>(0)
    const [ consumptionAmount, setConsumptionAmount ] = useState<number>(0)

    useEffect(()=>{
        const params: GetBillPeriodsPar ={
            year,
            sessionId:auth.sessionId,
            ethiopianYear:true
        }
        dispatch(fetchBillPeriods(params))
    },[ year ])
    useEffect(()=>{
        const period = billPeriodState.payload.find(p=>p.id === periodID)
        setSelectedPeriod(period)
        if(periodID){
            const meterReadingParams: BWFGetMeterReadingByPeriodIDPar ={
                periodID,
                sessionId: auth.sessionId,
                subcriptionID: props.data.subscription.id,
            }
            dispatch(fetchBWFMeterReading(meterReadingParams))
        }
    },[periodID])
    useEffect(()=>{
        console.log('AVERAGE READING ',averageReadings)
    },[ averageReadings ])

    const submitReadingData = () =>{
        if(selectedPeriod){
            const readingCorrectionData = new ReadingCorrectionData()
            readingCorrectionData.newReading = new BWFMeterReading()
            readingCorrectionData.newReading.periodID = selectedPeriod.id
            readingCorrectionData.newReading.subscriptionID = props.data.subscription.id
            readingCorrectionData.newReading.readingType = readingType
            if(
                (readingCorrectionData.newReading.readingType == MeterReadingType.MeterReset ) ||
                (readingCorrectionData.newReading.readingType == MeterReadingType.Normal )
            ){
                readingCorrectionData.newReading.reading = readingAmount
            }
            if(readingCorrectionData.newReading.readingType !== MeterReadingType.Normal ){
                readingCorrectionData.newReading.consumption = consumptionAmount
            }
            /*
               Average Readings has never been used
               if(averageReadings.length > 0)
               readingCorrectionData.averageReadings = averageReadings;
               */
            const jobParams: AddJobWebPar<ReadingCorrectionData> = {
                job:jobData,
                sessionId:auth.sessionId,
                dataType:NAME_SPACES.WORKFLOW.READING_CORRECTION_DATA,
                data:readingCorrectionData
            }
            dispatch(requestAddJobWeb(jobParams))
        }
    }
    const removeFromAverageReading = (reading: BWFMeterReading)=>{
        setAverageReadings([...averageReadings.filter(r=>r.periodID !== reading.periodID)])
    }
    const addToAverageReading = ( reading: BWFMeterReading) => {
        setAverageReadings([...averageReadings, reading])
    }

    return (
        <div>
            <Divider orientation={'left'}>Customer Information</Divider>
            <CustomerInformationContainer {...props.data}/>
            <Divider orientation={'left'}>Reading Information</Divider>
            <StateMachineHOC
                state={bwfMaterReadingState}
                fallBack={
                    <CAlert
                        type={'error'}
                        message={bwfMaterReadingState.message}/>
                }
            >
                { bwfMaterReadingState.payload != undefined &&  <BWFReadingContainer materReading={bwfMaterReadingState.payload}/> }
            </StateMachineHOC>
            <Divider orientation={'left'}>Reading Form</Divider>
            <SharedJobFormFields
                dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date})}
                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
            />

            <CFormItem
                input={
                    <Row gutter={4}>
                        <Col span={12}>
                            <Select
                                onChange={(periodID)=>{ setPeriodId(+periodID)}}
                                style={{width:'100%'}}
                                loading = {billPeriodState.loading}>
                                { billPeriodState.payload.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                            </Select>
                        </Col>
                        <Col span={12}>
                            <Select
                                defaultValue={year} onChange={(year)=>setYear(+year)}
                                style={{width:'100%'}}
                                disabled={billPeriodState.loading}>
                                { years.map((value,key)=><Option key={key} value={value}>{value}</Option>) }
                            </Select>
                        </Col>
                    </Row>
                }
                label={'Period'}
                required />
            <Row gutter={4}>
                <Col span={12}>
                    <CFormItem
                        input={<Input onChange={(event)=>setReadingAmount(+event.target.value)}/>}
                        label={'Reading'}
                        required/>
                </Col>
                <Col span={12}>
                    <CFormItem
                        input={<Input onChange={(event)=>setConsumptionAmount(+event.target.value)}/>}
                        label={'New Consumption'}
                        required/>
                </Col>
                <Col span={12}>
                    <CFormItem
                        input={
                            <Select
                                placeholder="Select Type"
                                style={{width:'100%'}}
                                value={readingType}
                                onChange={(readingType:MeterReadingType)=>{setReadingType(readingType)}}
                            >
                                <Option  value={MeterReadingType.MeterReset}>Mater Reset</Option>
                                <Option  value={MeterReadingType.Normal}>Normal</Option>
                            </Select>
                        }
                        label={'New Type'}
                        errorMessage={'Please select type'}
                        required
                    />
                </Col>



                <Col span={24}>
                    <Collapse
                        bordered={false}
                        expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
                    >
                        <Panel header={averageReadings.length > 0? `Average of ${averageReadings.length} readings` : 'Calculate Average'} key="1" style={customPanelStyle}>
                            {
                                selectedPeriod !== undefined &&
                                <MeterReadingListContainer
                                    addSelectedReading={addToAverageReading}
                                    removeSelectedReading={removeFromAverageReading}
                                    readings={averageReadings}
                                    period={selectedPeriod}
                                    connectionId={props.data.subscription.id}/>
                            }
                            {
                                selectedPeriod == undefined &&
                                <CAlert message={'Please select period'}/>
                            }
                        </Panel>
                    </Collapse>
                </Col>
            </Row>
            <div style={{width:'50%',marginTop:'5px'}}>
                <CButton onClick={submitReadingData} type={'primary'} block>submit</CButton>
            </div>
        </div>
    )
}


export default ReadingCorrectionForm