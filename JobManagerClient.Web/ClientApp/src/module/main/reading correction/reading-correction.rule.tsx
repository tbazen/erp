import React from 'react';
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import ReadingCorrectionContainer from "./reading-correction";
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../_constants/routes";
import {JobData} from "../../../_services/job.manager.service";
import {ReadingCorrectionWorkflowDataContainer} from "./workflow/reading-correction-workflow-data-container";
import {ReadingCorrectionEditForm} from "./reading-correction-edit-form";

@JobRuleClientDecorator({
    jobName:"Reading Correction",
    jobType:StandardJobTypes.READING_CORRECTION,
    route:ROUTES.JOB.READING_CORRECTION,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.READING_CORRECTION].ABSOLUTE,
            component:ReadingCorrectionEditForm
        }
    ]
})
export class ReadingCorrectionRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return ReadingCorrectionContainer
    }
    
    public getWorkFlowData(job:JobData){
        return <ReadingCorrectionWorkflowDataContainer job={job}/>
    }
}