import React ,{ Fragment } from 'react'
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {useEffect, useState} from "react";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {ReadingCorrectionData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/reading-correction-data";
import {BWFMeterReading} from "../../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import SubscriberManagementService, {BWFGetMeterReadingByPeriodIDPar} from "../../../../_services/subscribermanagment.service";
import {Divider} from "antd";
import {BWFReadingInformationContainer} from "../../../../shared/components/job-rule-base-components/bwf-meter-information-container/bwf-reading-information-container";


const subscriber_mgr_service = new SubscriberManagementService()
interface IProps{
    job : JobData
}

export function ReadingCorrectionWorkflowDataContainer(props:IProps) {
    const [auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth, appState.workflow_data_state])
    const [readingCorrectionData, setReadingCorrectionData] = useState<ReadingCorrectionData>()
    const [ oldReading , setOldReading ] = useState<BWFMeterReading>()

    const dispatch = useDispatch()
    useEffect(() => {
        const params: GetWorkFlowDataWebPar =
            {
                key: 0,
                jobID: props.job.id,
                sessionId: auth.sessionId,
                typeID: StandardJobTypes.READING_CORRECTION,
                fullData: true
            }
        dispatch(fetchWorkflowData(params))
    }, [])
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setReadingCorrectionData(workflowdata_state.payload as ReadingCorrectionData)
    }, [workflowdata_state])
    useEffect(()=>{
        readingCorrectionData &&
        fetchOldReading()
    },[readingCorrectionData])
    async function fetchOldReading(){
        try{
            if(readingCorrectionData){
                if(readingCorrectionData.oldReading !== null &&
                    readingCorrectionData.oldReading !== undefined){
                    setOldReading(readingCorrectionData.oldReading)
                    return;
                }
                if(readingCorrectionData.newReading){
                    const readingParams:BWFGetMeterReadingByPeriodIDPar= {
                        sessionId:auth.sessionId,
                        subcriptionID:readingCorrectionData.newReading.subscriptionID,
                        periodID:readingCorrectionData.newReading.periodID
                    }
                    const oldReading: BWFMeterReading = ( await subscriber_mgr_service.BWFGetMeterReadingByPeriodID(readingParams)).data
                    setOldReading(oldReading)
                }
            }
        } catch (e) {}
    }

    return (
        <Fragment>
            {
                readingCorrectionData !==undefined &&
                <Fragment>
                    <Divider orientation={'left'}>New Reading</Divider>
                    {
                        readingCorrectionData.newReading &&
                        <BWFReadingInformationContainer reading={readingCorrectionData.newReading}/>
                    }
                </Fragment>
            }
            {
                oldReading &&
                <Fragment>
                    <Divider orientation={'left'}>Old Reading</Divider>
                    <BWFReadingInformationContainer reading={oldReading}/>
                </Fragment>
            }
        </Fragment>
    )
}