import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import pushPathToBC from "../../../_redux_setup/actions/breadcrumb-actions"
import { exposeSearchBar, hideSearchBar } from '../../../_redux_setup/actions/global-searchbar-actions';
import { subscriberListClear, fetchSubscriberList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { Card, Button } from 'antd';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { GetSubscriptions2Par, SubscriberSearchField } from '../../../_services/subscribermanagment.service';
import { SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import CLoadingPage from '../../../shared/screens/cloading/cloading-page';
import OperationFailed from '../../../shared/screens/status-code/submission-failed';
import ButtonGridContainer from '../../../shared/screens/button-grid-container/button-grid-container';
import ReturnMeterForm from './return-meter-form';
import { openDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';
import {useBreadcrumb} from "../../../shared/hooks/manage-breadcrumb";
import {useGlobalSearchBar} from "../../../shared/hooks/manage-global-search";
import {StateMachineHOC} from "../../../shared/hoc/StateMachineHOC";


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.JOB.RETURN_METER,
        breadcrumbName:'Return Meter'
    }
]


const ReturnMeterContainer = ()=>{
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)

    const initSearchParams : GetSubscriptions2Par={
        sessionId:auth.sessionId,
        query: '',
        fields: SubscriberSearchField.CustomerName | SubscriberSearchField.ConstactNo,
        multipleVersion: false,
        kebele: -1,
        zone: -1,
        dma: -1,
        index: 0,
        page: 30
    }

    const [searchParams , setSearchParams] = useState<GetSubscriptions2Par>(initSearchParams)
    const subscribersState = useSelector((appState:ApplicationState)=>appState.subscription_search_result_state)
    useBreadcrumb(backwardPath)
    useGlobalSearchBar({ searchPlaceHolder:'Contract Number',searchHandler:searchInputHandler,cleanupSearchResult:subscriberListClear})
    function searchInputHandler(event:any){
        const query = event.target.value
        setSearchParams({...initSearchParams,query})
    }

    //API fetch initiator effect
    useEffect(()=>{
        if(searchParams.query.trim() !== '')
          dispatch(fetchSubscriberList(searchParams))
    },[searchParams])


    const SubscriberCard = (props:SubscriberSearchResult)=>{
        return (
            <Card 
                title={props.subscriber.name} 
                bordered={false}
                actions={[
                    <Button onClick={()=>dispatch(openDrawerModal(<ReturnMeterForm data={props}/>))} icon={'disconnect'} block type={'link'}>
                        Return
                    </Button>
                  ]}
                >
                <p>{props && props.subscription && props.subscription.contractNo}</p>
            </Card>
        )
    }

    return (
        <StateMachineHOC state={subscribersState}>
            <ButtonGridContainer spacing={6} lg={{span:6}} component={SubscriberCard} listItem={subscribersState.payload._ret}/>
        </StateMachineHOC>
    )
}

export default ReturnMeterContainer