import {Checkbox, Col, Form, Row, Select, Typography} from 'antd';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import {ApplicationState} from '../../../_model/state-model/application-state';
import {AuthenticationState} from '../../../_model/state-model/auth-state';
import {AddJobWebPar} from '../../../_services/job.manager.service';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {requestUpdateJobWeb} from "../../../_redux_setup/actions/mn-actions/main-actions";
import {ReturnMeterData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/return-meter-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {MaterialOperationalStatus} from "../../../_enum/mn-enum/material-operational-status";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";


const { Option } = Select


interface IProps {
    match: any
}

export const ReturnMeterEditForm = (props: IProps) => {
    const formLayout = 'horizontal';
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [auth,workflowdata_state] = useSelector<ApplicationState, [AuthenticationState,WorkflowDataState]>(appState => [appState.auth,appState.workflow_data_state])
    const [jobData, setJobData] = useEditJobInitializer({jobId,jobType:StandardJobTypes.RETURN_METER })
    const [ operationalStatus , setOperationalStatus ] = useState<MaterialOperationalStatus>(MaterialOperationalStatus.Working)
    const [ isReturnPermanent, setIsReturnPermanent ] = useState<boolean>(false)
    const [ returnMeterData , setReturnMeterData ] = useState<ReturnMeterData>()

    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setReturnMeterData(workflowdata_state.payload as ReturnMeterData)
    },[ workflowdata_state ])

    const submitReturnMeterJobData = () => {
        if(jobData && returnMeterData){
            returnMeterData.permanent = isReturnPermanent
            returnMeterData.opStatus = operationalStatus
            const jobParams: AddJobWebPar<ReturnMeterData> ={
                job: jobData,
                data: returnMeterData,
                sessionId: auth.sessionId,
                dataType: NAME_SPACES.WORKFLOW.RETURN_METER_DATA
            }
            dispatch(requestUpdateJobWeb(jobParams))
        }
    }
    return (
        <Form layout={formLayout}>
            <Row gutter={4}>
                <Col lg={{span:16}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <Form.Item label="Operational Status" {...formItemLayout}>
                            <Select
                                placeholder="Operational Status"
                                style={{ width: '100%' }}
                                size={'large'}
                                value={operationalStatus}
                                onChange={(operationalStatus:MaterialOperationalStatus)=>setOperationalStatus(operationalStatus)}
                            >
                                <Option value={MaterialOperationalStatus.Working}>Working</Option>
                                <Option value={MaterialOperationalStatus.NotWorking}>Not Working</Option>
                                <Option value={MaterialOperationalStatus.Defective}>Defective</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Return Permanently" {...formItemLayout}>
                            <Checkbox onChange={(event)=>setIsReturnPermanent(event.target.checked)} />
                        </Form.Item>
                    </div>
                </Col>
                <Col lg={{span:8}}>
                    <div className={'paper'} style={{padding:'10px'}}>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => jobData && setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) =>jobData && setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                    <div className={'paper'} style={{padding:'10px',marginTop:'5px'}}>
                        <CButton type={'primary'} block onClick={submitReturnMeterJobData}>Submit</CButton>
                    </div>
                </Col>
            </Row>
        </Form>
    )
}
