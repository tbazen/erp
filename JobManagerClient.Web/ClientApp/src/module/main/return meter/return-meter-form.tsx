import {Checkbox, Col, Divider, Row, Select, Typography} from 'antd';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import CustomerInformationContainer from '../../../shared/components/customer-information/customer-information';
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field';
import CButton from '../../../shared/core/cbutton/cbutton';
import CFormItem from '../../../shared/core/cform-item/cform-item';
import {initialJobData} from '../../../_helpers/initial value/init-jobdata';
import {ApplicationState} from '../../../_model/state-model/application-state';
import {AuthenticationState} from '../../../_model/state-model/auth-state';
import {SubscriberSearchResult} from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {AddJobWebPar, JobData} from '../../../_services/job.manager.service';
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {requestAddJobWeb} from "../../../_redux_setup/actions/mn-actions/main-actions";
import {ReturnMeterData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/return-meter-data";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {MaterialOperationalStatus} from "../../../_enum/mn-enum/material-operational-status";
import {DataRow} from "../../../shared/components/data-row/data-row";


const { Title } = Typography
const { Option } = Select
interface IProps {
    data: SubscriberSearchResult
}


const ReturnMeterForm = (props: IProps) => {
    const dispatch = useDispatch()
    const [auth] = useSelector<ApplicationState, [AuthenticationState]>(appState => [appState.auth])
    const [jobData, setJobData] = useState<JobData>({ ...initialJobData.job, customerID: props.data.subscriber.id, applicationType: StandardJobTypes.RETURN_METER })
    const [ operationalStatus , setOperationalStatus ] = useState<MaterialOperationalStatus>(MaterialOperationalStatus.Working)
    const [ isReturnPermanent, setIsReturnPermanent ] = useState<boolean>(false)

    const submitReturnMeterJobData = () => {
        const returnMeterJobData = new ReturnMeterData()
        returnMeterJobData.connectionID = props.data.subscription.id
        returnMeterJobData.permanent = isReturnPermanent
        returnMeterJobData.opStatus = operationalStatus
        const jobParams: AddJobWebPar<ReturnMeterData> ={
            job: jobData,
            data: returnMeterJobData,
            sessionId: auth.sessionId,
            dataType: NAME_SPACES.WORKFLOW.RETURN_METER_DATA
        }
        dispatch(requestAddJobWeb(jobParams))
    }

    return (
        <div>
            <Divider orientation={'left'}><Title level={4}>Customer Information</Title></Divider>
            <CustomerInformationContainer {...props.data} />
            <DataRow title={'Meter Type'} content={props.data.subscription.meterData.itemCode } />
            <DataRow title={'Meter Number'} content={props.data.subscription.meterData.serialNo } />
            <DataRow title={'Meter Model'} content={props.data.subscription.meterData.modelNo } />
            <br />
            <br />
            <SharedJobFormFields
                dateChangeHandler={(date: string) => setJobData({ ...jobData,startDate: date, statusDate: date, logDate: date })}
                descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
            />
            <CFormItem
                input={
                    <Select
                        placeholder="Operational Status"
                        style={{ width: '100%' }}
                        size={'large'}
                        value={operationalStatus}
                        onChange={(operationalStatus:MaterialOperationalStatus)=>setOperationalStatus(operationalStatus)}
                    >
                        <Option value={MaterialOperationalStatus.Working}>Working</Option>
                        <Option value={MaterialOperationalStatus.NotWorking}>Not Working</Option>
                        <Option value={MaterialOperationalStatus.Defective}>Defective</Option>
                    </Select>
                }
                label={'Operational Status'}
                errorMessage={'Please select operational status'}
                required
            />
            <Checkbox onChange={(event)=>setIsReturnPermanent(event.target.checked)} >Return Permanently</Checkbox>
            <div style={{ width: '50%' }} >
                <CButton type={'primary'} block onClick={submitReturnMeterJobData}>Submit</CButton>
            </div>
        </div>
    )
}

export default ReturnMeterForm