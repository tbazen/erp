import React from 'react'
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../_constants/routes";
import ReturnMeterContainer from "./return-meter-container";
import {ReturnMeterWorkflowDataContainer} from "./workflow/return-meter-workflow-data-container";
import {JobData} from "../../../_services/job.manager.service";
import {ReturnMeterEditForm} from "./return-meter-edit-form";

@JobRuleClientDecorator({
    jobName:"Return Meter",
    jobType:StandardJobTypes.RETURN_METER,
    route:ROUTES.JOB.RETURN_METER,
    innerRoutes:[
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.RETURN_METER].ABSOLUTE,
            component:ReturnMeterEditForm
        }
    ]
})
export class ReturnMeterRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return ReturnMeterContainer
    }

    public getWorkFlowData(job:JobData){
        return <ReturnMeterWorkflowDataContainer job={job}/>
    }
}