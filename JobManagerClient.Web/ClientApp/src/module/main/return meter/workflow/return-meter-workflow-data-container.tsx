import React,{ Fragment ,useEffect, useState} from 'react'
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import SubscriberManagementService, {GetSubscriptionPar} from "../../../../_services/subscribermanagment.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import {getTickFromDate} from "../../../../_helpers/date-util";
import {ReturnMeterData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/return-meter-data";
import {ConnectionInformationContainer} from "../../../../shared/components/job-rule-base-components/connection-information-container";
import {Divider} from "antd";
import {DataRow} from "../../../../shared/components/data-row/data-row";

interface IProps{
    job : JobData
}
const subscriber_mgr_service = new SubscriberManagementService()

export function ReturnMeterWorkflowDataContainer(props:IProps) {
    const [auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth, appState.workflow_data_state])
    const [returnMeterData, setReturnMeterData] = useState<ReturnMeterData>()
    const [subscription, setSubscription] = useState<Subscription>()

    const dispatch = useDispatch()
    useEffect(() => {
        const params: GetWorkFlowDataWebPar =
            {
                key: 0,
                jobID: props.job.id,
                sessionId: auth.sessionId,
                typeID: StandardJobTypes.RETURN_METER,
                fullData: true
            }
        dispatch(fetchWorkflowData(params))
    }, [])
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setReturnMeterData(workflowdata_state.payload as ReturnMeterData)
    }, [workflowdata_state])
    useEffect(() => {
        returnMeterData !== undefined &&
        fetchSubscription()
    }, [returnMeterData])

    async function fetchSubscription() {
        try {
            if (returnMeterData) {
                const subscriptionparams: GetSubscriptionPar = {
                    id: returnMeterData.connectionID,
                    sessionId:auth.sessionId,
                    version: getTickFromDate(new Date(props.job.statusDate))
                }
                const subscription = (await subscriber_mgr_service.GetSubscription(subscriptionparams)).data
                setSubscription(subscription)
            }
        } catch (e) {
        }
    }
    
    return (
        <Fragment>
            {
                returnMeterData !== undefined &&
                returnMeterData.permanent &&
                <DataRow title={'Return Meter'} content={'Permanent'}/>
            }
            {
                returnMeterData !== undefined &&
                !returnMeterData.permanent &&
                <DataRow title={'Return Meter'} content={'Temporary'}/>
            }
            {
                subscription !== undefined &&
                <Fragment>
                    <Divider orientation={'left'}>Connection Detail</Divider>
                    <ConnectionInformationContainer connection={subscription}/>
                </Fragment>
            }
        </Fragment>
    )
}