import { Col, Row, Tabs } from 'antd'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomerInformationContainer, { SubscriberInformationContainer } from "../../../shared/components/customer-information/customer-information"
import CustomersListTable from '../../../shared/components/customer-list-table/customer-list-table'
import CustomerSearchBar from '../../../shared/components/customer-list-table/customer-search-bar'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field'
import { CAlert } from "../../../shared/core/calert/calert"
import CLoadingPage from '../../../shared/screens/cloading/cloading-page'
import { ROUTES } from '../../../_constants/routes'
import { initializeSubscriberSearchParams } from '../../../_helpers/initial value/init-global-subscriber'
import { initialJobData } from '../../../_helpers/initial value/init-jobdata'
import { ApplicationState } from '../../../_model/state-model/application-state'
import { AuthenticationState } from '../../../_model/state-model/auth-state'
import { IBreadcrumb } from '../../../_model/state-model/breadcrumd-state'
import { TransactionItemsState } from '../../../_model/state-model/ic-sm/transaction-items-state'
import { SingleSubscriberState } from '../../../_model/state-model/mn-sm/single-subscriber-state'
import { Subscriber, SubscriberSearchResult } from '../../../_model/view_model/mn-vm/subscriber-search-result'
import pushPathToBC from '../../../_redux_setup/actions/breadcrumb-actions'
import { fetchTransactionItems } from '../../../_redux_setup/actions/ic-actions/item-configuration-actions'
import {
    fetchSubscriberProfile,
    requestAddJobWeb
} from '../../../_redux_setup/actions/mn-actions/main-actions'
import { AddJobWebPar, JobData} from '../../../_services/job.manager.service'
import { GetSubscriptions2Par, SubscriberSearchField } from '../../../_services/subscribermanagment.service'
import CButton from '../../../shared/core/cbutton/cbutton'
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {OwnerShipTransferData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/ownership-transfer-data";

const { TabPane } = Tabs

const backwardPath: IBreadcrumb[] = [
    {
        path: ROUTES.MAIN.INDEX,
        breadcrumbName: 'Tools'
    },
    {
        path: ROUTES.JOB.CONNECTION_OWNERSHIP_TRANSFER,
        breadcrumbName: 'Transfer Connection Ownership'
    },
]
interface IProps {
    match: any
}


const ChangeExistingCustomer = (props: IProps) => {
    const dispatch = useDispatch()
    const [ auth, existingCustomerInfo, connectionInformation] = useSelector<ApplicationState, [AuthenticationState, SingleSubscriberState, TransactionItemsState]>(appState => [appState.auth, appState.subscriber_state, appState.transaction_items_state])
    const [ selectedCustomer, setSelectedCustomer] = useState<SubscriberSearchResult | undefined>(undefined)
    const [ jobData , setJobData ] = useState<JobData>({ ...initialJobData.job, applicationType: StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER })
    const [ newCustomer , setNewCustomer ] = useState<Subscriber>()

    const customerCode: string = props.match.params.id
    const initSearchParams: GetSubscriptions2Par = {
        ...initializeSubscriberSearchParams,
        sessionId: auth.sessionId,
        query: customerCode,
        fields: SubscriberSearchField.CustomerCode,
    }

    // #region effects for manipulating the UI and making initial API calls
    useEffect(() => {
        !existingCustomerInfo.loading &&
        !existingCustomerInfo.error &&
        existingCustomerInfo.payload !== undefined &&
        existingCustomerInfo.payload.nRecord > 0 &&
        dispatch(fetchTransactionItems({ sessionId: auth.sessionId, code: existingCustomerInfo.payload._ret[0].subscription.itemCode }))
    }, [existingCustomerInfo])
    useEffect(() => {
        dispatch(pushPathToBC([...backwardPath, { path: '', breadcrumbName: customerCode }]))
        dispatch(fetchSubscriberProfile({ ...initSearchParams }))
        return () => {
            dispatch(pushPathToBC([]))
        };
    }, [])

    const handleNewCustomerDataChange = (customer: Subscriber) => { setNewCustomer({...customer}) }
    const submitOwnershipTransferToExistingCustomer = ()=>{
        if(!existingCustomerInfo.loading &&
            !existingCustomerInfo.error &&
            existingCustomerInfo.payload !== undefined &&
            existingCustomerInfo.payload.nRecord > 0 &&
            selectedCustomer !== undefined
        ){
            const jobApplication = {...jobData}
            jobApplication.customerID = selectedCustomer.subscriber.id
            jobApplication.newCustomer = null
            const ownershipTransferData = new OwnerShipTransferData()
            ownershipTransferData.connectionID = existingCustomerInfo.payload._ret[0].subscription.id
            const jobParams : AddJobWebPar<OwnerShipTransferData>= {
                job: jobApplication,
                sessionId: auth.sessionId,
                dataType: NAME_SPACES.WORKFLOW.OWNERSHIP_TRANSFER_DATA,
                data: ownershipTransferData
            }
            dispatch(requestAddJobWeb(jobParams))
        }
    }
    const submitOwnershipTransferToNewCustomer = ()=>{
        if(!existingCustomerInfo.loading &&
            !existingCustomerInfo.error &&
            existingCustomerInfo.payload !== undefined &&
            existingCustomerInfo.payload.nRecord > 0 &&
            newCustomer !== undefined
        ){
            const jobApplication = {...jobData}
            jobApplication.customerID = -1
            jobApplication.newCustomer = newCustomer
            const ownershipTransferData = new OwnerShipTransferData()
            ownershipTransferData.connectionID = existingCustomerInfo.payload._ret[0].subscription.id
            const jobParams : AddJobWebPar<OwnerShipTransferData>= {
                job: jobApplication,
                sessionId: auth.sessionId,
                dataType: NAME_SPACES.WORKFLOW.OWNERSHIP_TRANSFER_DATA,
                data: ownershipTransferData
            }
            dispatch(requestAddJobWeb(jobParams))
        }
    }

    return (
        <Row gutter={4}>
            <Col span={16}>
                <CustomerSearchBar
                    appendedForm={
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                        />
                    }
                    title={'Transfer Connection OwnerShip'}
                    updateCustomerData={handleNewCustomerDataChange}
                    submitCustomerData={submitOwnershipTransferToNewCustomer}
                />
                <CustomersListTable
                    setSelectedCustomers={setSelectedCustomer}
                    withSelection
                    height='66vh'
                />
            </Col>
            <Col span={8}>
                <Row style={{ marginBottom: '5px' }}>
                    <div className={'paper'} style={{ padding: '10px' }}>
                        <SharedJobFormFields
                            dateChangeHandler={(date: string) => setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                            descriptionChangeHandler={(desc: string) => setJobData({ ...jobData, description: desc })}
                        />
                    </div>
                </Row>
                <Row>
                    <div className={'paper'}>
                        <div style={{ width: '100%', overflow: 'auto', maxHeight: '52vh', padding: '10px' }}>
                            <Tabs defaultActiveKey="1">
                                <TabPane tab="New Customer" key="1">
                                    {
                                        selectedCustomer === undefined &&
                                        <Row>
                                            <CAlert
                                                message="Customer Not Selected"
                                                description='To transfer connection owner ship please select one customer, or use the "Add new Customer" button to create new customer!'
                                                type="error"
                                            />
                                        </Row>
                                    }
                                    {
                                        selectedCustomer !== undefined &&
                                        <Row>
                                            <CustomerInformationContainer {...selectedCustomer} />
                                        </Row>
                                    }
                                </TabPane>
                                <TabPane tab="Current/Existing Customer" key="2">
                                    {
                                        existingCustomerInfo.loading &&
                                        <CLoadingPage />
                                    }
                                    {
                                        existingCustomerInfo.error &&
                                        <p>Error loading existing customer</p>
                                    }
                                    {
                                        !existingCustomerInfo.loading &&
                                        !existingCustomerInfo.error &&
                                        existingCustomerInfo.payload !== undefined &&
                                        <Row>
                                            {
                                                existingCustomerInfo.payload.nRecord > 0 &&
                                                <SubscriberInformationContainer {...existingCustomerInfo.payload._ret[0].subscriber} />
                                            }
                                        </Row>
                                    }

                                    {
                                        connectionInformation.loading &&
                                        <CLoadingPage />
                                    }
                                    {
                                        connectionInformation.error &&
                                        <p>Error loading existing customer</p>
                                    }
                                    {
                                        !connectionInformation.loading &&
                                        !connectionInformation.error &&
                                        connectionInformation.transactions_items !== undefined &&
                                        <Row>
                                            {
                                                connectionInformation.transactions_items.map((value, key) => <p>{value.description}</p>)
                                            }
                                        </Row>
                                    }
                                </TabPane>
                            </Tabs>
                        </div>
                        <div style={{ padding: '10px' }} >
                            <CButton
                                block
                                type={'primary'}
                                onClick={submitOwnershipTransferToExistingCustomer}
                                disabled={selectedCustomer === undefined}
                            >Submit</CButton>
                        </div>
                    </div>
                </Row>
            </Col>
        </Row>
    )
}

export default ChangeExistingCustomer