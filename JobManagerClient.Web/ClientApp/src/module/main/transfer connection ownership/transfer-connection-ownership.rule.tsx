import React from 'react'
import {JobRuleClientHandlerBase} from "../../../_model/job-rule-model/job-rule-client-handler-base";
import TransferConnectionOwnerShipContainer from "./transfer-connection-ownership-container";
import {JobRuleClientDecorator} from "../../../_decorator/jobrule-client-handler.decorator";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {ROUTES} from "../../../_constants/routes";
import ChangeExistingCustomer from "./change-exsting-customer";
import {JobData} from "../../../_services/job.manager.service";
import {TransferOwnershipWorkflowDataContainer} from "./workflow/transfer-ownership-workflow-data-container";
import {TransferOwnershipEditForm} from "./transfer-ownership-edit-form";

@JobRuleClientDecorator({
    jobName:"Transfer Connection Ownership",
    jobType:StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER,
    route:ROUTES.JOB.CONNECTION_OWNERSHIP_TRANSFER,
    innerRoutes:[
        {
            path:`${ROUTES.JOB.CONNECTION_OWNERSHIP_TRANSFER}/:id`,
            component:ChangeExistingCustomer
        },
        {
            path:ROUTES.ACTIVE_JOB.EDIT[StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER].ABSOLUTE,
            component:TransferOwnershipEditForm
        }
    ]
})
export class TransferConnectionOwnershipRule extends JobRuleClientHandlerBase{
    public getNewApplicationForm(){
        return TransferConnectionOwnerShipContainer
    }

    public getWorkFlowData(job:JobData) {
        return <TransferOwnershipWorkflowDataContainer job={job}/>
    }
}