import React, { Fragment, useEffect, useState} from 'react'
import {Col, Divider, Row} from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import SharedJobFormFields from '../../../shared/components/job application form/shared-job-field'
import { ApplicationState } from '../../../_model/state-model/application-state'
import { AuthenticationState } from '../../../_model/state-model/auth-state'
import { requestUpdateJobWeb } from '../../../_redux_setup/actions/mn-actions/main-actions'
import { AddJobWebPar} from '../../../_services/job.manager.service'
import CButton from '../../../shared/core/cbutton/cbutton'
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {NAME_SPACES} from "../../../_constants/model-namespaces";
import {OwnerShipTransferData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/ownership-transfer-data";
import {useEditJobInitializer} from "../../../shared/hooks/edit-job-intializer";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import {SubscriberInformationContainer} from "../../../shared/components/customer-information/customer-information";
import SubscriberManagementService, {GetSubscriber2Par} from "../../../_services/subscribermanagment.service";
import {Subscriber} from "../../../_model/view_model/mn-vm/subscriber-search-result";


interface IProps {
    match: any
}
const subscriber_mgr_service = new SubscriberManagementService()

export const TransferOwnershipEditForm = (props: IProps) => {
    const dispatch = useDispatch()
    const jobId: number = +props.match.params.jobId.toString()
    const [ auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState,WorkflowDataState]>(appState => [appState.auth,appState.workflow_data_state])
    const [jobData, setJobData] = useEditJobInitializer({jobId,jobType:StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER })
    const [ transferOwnershipData , setTransferOwnershipData ] = useState<OwnerShipTransferData>()
    const [ newCustomer, setNewCustomer ] = useState<Subscriber>()
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setTransferOwnershipData(workflowdata_state.payload as OwnerShipTransferData)
    }, [workflowdata_state])
    useEffect(()=>{
        if(jobData){
            if(jobData.customerID === -1 && jobData.newCustomer)
                setNewCustomer(jobData.newCustomer)
            else if(jobData.customerID !== -1)
                loadNewCustomerData()
        }
    }, [ jobData ])
    async function loadNewCustomerData(){
        try{
            if(jobData){
                const subscriberParams: GetSubscriber2Par = {
                    id:jobData.customerID,
                    sessionId: auth.sessionId
                }
                const subscriber:Subscriber = (await subscriber_mgr_service.GetSubscriber2(subscriberParams)).data
                setNewCustomer(subscriber)
            }
        } catch(e){ setNewCustomer(undefined) }
    }
    const submitOwnershipTransferToExistingCustomer = ()=>{
        if(jobData && transferOwnershipData){
            const jobParams : AddJobWebPar<OwnerShipTransferData>= {
                job: jobData,
                sessionId: auth.sessionId,
                dataType: NAME_SPACES.WORKFLOW.OWNERSHIP_TRANSFER_DATA,
                data: transferOwnershipData
            }
            dispatch(requestUpdateJobWeb(jobParams))
        }
    }

    return (
        <Row gutter={4}>
            <Col span={8}>
                <div className={'paper'} style={{ padding: '10px' }}>
                    {
                        newCustomer &&
                        <Fragment>
                            <Divider orientation={'left'}>New Customer</Divider>
                            <SubscriberInformationContainer {...newCustomer}/>
                        </Fragment>
                    }
                </div>
            </Col>
            <Col span={8}>
                <div className={'paper'} style={{ padding: '10px' }}>
                    {
                        transferOwnershipData &&
                        transferOwnershipData.oldCustomer &&
                        <Fragment>
                            <Divider orientation={'left'}>Old Customer</Divider>
                            <SubscriberInformationContainer {...transferOwnershipData.oldCustomer}/>
                        </Fragment>
                    }
                </div>
            </Col>
            <Col span={8}>
                <div className={'paper'} style={{ padding: '10px' }}>
                    <SharedJobFormFields
                        dateChangeHandler={(date: string) =>jobData && setJobData({ ...jobData, startDate: date, statusDate: date, logDate: date })}
                        descriptionChangeHandler={(desc: string) =>jobData && setJobData({ ...jobData, description: desc })}
                    />
                </div>
                <div className={'paper'} style={{ padding: '10px',marginTop:'5px' }} >
                    <CButton
                        block
                        type={'primary'}
                        onClick={submitOwnershipTransferToExistingCustomer}
                        disabled={jobData === undefined || transferOwnershipData === undefined}
                    >Submit</CButton>
                </div>
            </Col>
        </Row>
    )
}
