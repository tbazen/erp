import React,{Fragment, useEffect, useState} from "react";
import {GetWorkFlowDataWebPar, JobData} from "../../../../_services/job.manager.service";
import SubscriberManagementService, {GetSubscriptionPar} from "../../../../_services/subscribermanagment.service";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../../_model/state-model/aj-sm/workflow-data-state";
import {Subscription} from "../../../../_model/level0/subscriber-managment-type-library/subscription";
import {StandardJobTypes} from "../../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../../_redux_setup/actions/aj-actions/active-job-actions";
import getCurrentTicks, {getTickFromDate} from "../../../../_helpers/date-util";
import {OwnerShipTransferData} from "../../../../_model/level0/job-manager-model/standard-job-datatypes/ownership-transfer-data";
import {SubscriberInformationContainer} from "../../../../shared/components/customer-information/customer-information";
import {DataRow} from "../../../../shared/components/data-row/data-row";
import {Divider} from "antd";
import {ConnectionInformationContainer} from "../../../../shared/components/job-rule-base-components/connection-information-container";

interface IProps{
    job : JobData
}
const subscriber_mgr_service = new SubscriberManagementService()

export function TransferOwnershipWorkflowDataContainer(props:IProps) {
    const [auth, workflowdata_state] = useSelector<ApplicationState, [AuthenticationState, WorkflowDataState]>(appState => [appState.auth, appState.workflow_data_state])
    const [transferOwnershipData, setTransferOwnershipData] = useState<OwnerShipTransferData>()
    const [oldSubscription, setOldSubscription] = useState<Subscription>()
    const [newSubscription, setNewSubscription] = useState<Subscription>()

    const dispatch = useDispatch()
    useEffect(() => {
        const params: GetWorkFlowDataWebPar =
            {
                key: 0,
                jobID: props.job.id,
                sessionId: auth.sessionId,
                typeID: StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER,
                fullData: true
            }
        dispatch(fetchWorkflowData(params))
    }, [])
    useEffect(() => {
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setTransferOwnershipData(workflowdata_state.payload as OwnerShipTransferData)
    }, [workflowdata_state])
    useEffect(() => {
        transferOwnershipData !== undefined &&
        fetchSubscription()
    }, [transferOwnershipData])

    async function fetchSubscription() {
        try {
            if (transferOwnershipData) {
                const oldSubscriptionParams: GetSubscriptionPar = {
                    id: transferOwnershipData.connectionID,
                    sessionId:auth.sessionId,
                    version: getTickFromDate(new Date(props.job.statusDate))
                }
                const oldSubscription = (await subscriber_mgr_service.GetSubscription(oldSubscriptionParams)).data
                setOldSubscription(oldSubscription)
                if(transferOwnershipData.newConnectionID != -1){
                    const newSubscriptionParams: GetSubscriptionPar = {
                        id: transferOwnershipData.newConnectionID,
                        sessionId:auth.sessionId,
                        version: getCurrentTicks()
                    }
                    const newSubscription = (await subscriber_mgr_service.GetSubscription(newSubscriptionParams)).data
                    setNewSubscription(newSubscription)
                }
            }
        } catch (e) {
            console.log(e)
        }
    }

    return (
        <Fragment>
            {
                transferOwnershipData !== undefined &&
                <Fragment>
                    {
                        oldSubscription !== undefined &&
                        <Fragment>
                            <ConnectionInformationContainer connection={oldSubscription}/>
                        </Fragment>
                    }
                    <Divider orientation={'left'}>Old Connection Detail</Divider>
                    {
                        transferOwnershipData.oldCustomer !== undefined &&
                        transferOwnershipData.oldCustomer !== null &&
                        <SubscriberInformationContainer {...transferOwnershipData.oldCustomer}/>
                    }
                    {
                        transferOwnershipData.oldCustomer === null &&
                        <DataRow title={'No Old Customer detail'} content={''}/>
                    }
                    {
                        newSubscription !== undefined &&
                        <Fragment>
                            <Divider orientation={'left'}>New Connection Detail</Divider>
                            <ConnectionInformationContainer connection={newSubscription}/>
                        </Fragment>
                    }
                </Fragment>
            }
        </Fragment>
    )
}