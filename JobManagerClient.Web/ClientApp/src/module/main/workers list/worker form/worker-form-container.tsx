import React, {useEffect, useState} from 'react'
import {Checkbox, Col, Divider, Row, Typography} from 'antd'
import CButton from "../../../../shared/core/cbutton/cbutton";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {JobRuleStateModel} from "../../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {useDispatch, useSelector} from 'react-redux';
import {JobWorker} from '../../../../_model/view_model/mn-vm/internal-service-vm/job-worker';
import {Employee} from '../../../../_model/view_model/IEmployee';
import {StandardJobTypes} from '../../../../_model/view_model/mn-vm/standard-job-types';
import {JobWorkerRole} from '../../../../_enum/mn-enum/job-worker-role';
import JobManagerService from "../../../../_services/job.manager.service";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import errorMessage from "../../../../_redux_setup/actions/error.message";
import {closeDrawerModal} from "../../../../_redux_setup/actions/drawer-modal-actions";
import {
    fetchAllWorkerList,
    fetchOrganizationalUnitsAndEmployee
} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {GetOrgUnitsPar} from "../../../../_services/payroll.service";
import {EmployeeState} from "../../../../_model/state-model/mn-sm/employee-state";
import {WorkerSelector} from "./worker-selector";
import {NotificationTypes, openNotification} from "../../../../shared/components/notification/notification";

const {Text, Title} = Typography


const job_manager = new JobManagerService()
const initWorker : JobWorker = {
    userID: '',
    active: true,
    employeeID: -1,
    roles: [],
    jobTypes: [],
}

const WorkerFormContainer = () =>{
    const [ clientHandlers,auth ,employeesState ] = useSelector<ApplicationState, [JobRuleStateModel,AuthenticationState, EmployeeState]>(
        appState => [
            appState.job_rule,
            appState.auth,
            appState.employee_state
        ]);
    const [ worker , setWorker ] = useState<JobWorker>(initWorker)
    const [ employee , setEmployee ] = useState<Employee>()
    const dispatch = useDispatch();

    useEffect(()=>{
        const params: GetOrgUnitsPar = {
            sessionId:auth.sessionId,
            pid:-1
        }
        dispatch(fetchOrganizationalUnitsAndEmployee(params))
    },[])
    useEffect(()=>{
        if(employee){
            worker.employeeID=employee.id
        }
    },[ employee ])
    function handleSelectedEmployee(employeeId:string){
        const employee = employeesState.payload.find(empl => empl.employeeID===employeeId)
        setEmployee(employee)
    }
    function isWorkersJobType(jobType : StandardJobTypes): boolean {
        if(!worker)
            return false;
        const job = worker.jobTypes.find(job => job === jobType);
        return job === undefined ? false : true;
    }
    function isWorkerRole(role : JobWorkerRole): boolean {
        if(!worker)
            return false;
        const workerRole = worker.roles.find(rl => rl === role);
        return workerRole === undefined ? false : true;
    }
    const handleProcessCheckBox = (event : any ) => {
        if(!worker)
            return;
        const isChecked = event.target.checked;
        const processID = event.target.value as number;
        if(isChecked)
            setWorker({...worker,jobTypes:[...worker.jobTypes,processID]})
        else
            setWorker({...worker,jobTypes:[...worker.jobTypes.filter(rl => rl !== processID)]})
    }
    const handleRoleCheckBox = (event : any ) => {
        if(!worker)
            return;
        const isChecked = event.target.checked;
        const roleId = event.target.value as number;
        if(isChecked)
            setWorker({...worker,roles:[...worker.roles,roleId]})
        else
            setWorker({...worker,roles:[...worker.roles.filter(rl => rl !== roleId)]})
    }
    const closeWorkerForm = () =>dispatch(closeDrawerModal())
    const saveWorkerData = async ()=>{
        if(!worker)
            return;
        if(worker.employeeID === -1){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Error',
                description:'Select employee'
            })
            return;
        }
        if( !(worker.jobTypes.length > 0) ){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Process',
                description:'Select at least one process'
            })
            return;
        }
        if( !(worker.roles.length > 0 ) ){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Role Error',
                description:'Select at least one role'
            })
            return;
        }
        try{
            openNotification({
                notificationType:NotificationTypes.LOADING,
                message:'Creating new worker',
                description:'registering new worker please wait'
            })
            await job_manager.CreateJobWorker({worker,sessionID:auth.sessionId})
            openNotification({
                notificationType:NotificationTypes.SUCCESS,
                message:'New Worker Created',
                description:'registering new worker please wait'
            })
            dispatch(fetchAllWorkerList({sessionId:auth.sessionId}))
        }catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'ERRNO',
                description:errorMessage(error)
            })
        }
    }

    return (
        <div>
            <Title level={4}>Add New Worker</Title>
            <Divider/>
            <div style={{margin:'auto',width:'100%',padding:'10px',marginTop:'10px'}} className={'paper'}>
                <Row gutter={4}>
                    <Col span={24}><Divider orientation={'left'}><Text strong>Employee Information</Text></Divider></Col>
                    <Col span={24}>
                        <div style={{marginBottom:'15px'}}>
                            <WorkerSelector handleSelectedEmployee={handleSelectedEmployee}/>
                        </div>
                    </Col>
                </Row>
                <Row gutter={4}>
                    <Col span={12}>
                        <Row style={{borderRightColor:'#999',borderRightStyle:'solid',borderRightWidth:'1px'}}>
                            <Col span={12}><Text><span><Text strong>Name</Text></span> </Text></Col>
                            <Col span={12}><Text>{employee && employee.employeeName}</Text></Col>
                            <Col span={12}><Text><span><Text strong>Address</Text></span> </Text></Col>
                            <Col span={12}><Text>{employee && employee.address}</Text></Col>
                        </Row>
                    </Col>
                    <Col span={12}>
                        <Row>
                            <Col span={12}><Text strong>Title</Text></Col>
                            <Col span={12}><Text>{employee &&  employee.title}</Text></Col>
                            <Col span={12}><Text><span><Text strong>Phone Number</Text></span> </Text></Col>
                            <Col span={12}><Text>{employee &&  employee.telephone}</Text></Col>
                        </Row>
                    </Col>
                </Row>
            </div>
            <div style={{margin:'auto',width:'100%',padding:'10px', minHeight:'25vh',marginTop:'15px'}} className={'paper'}>
                <Text strong>Roles</Text>
                <Divider/>
                <Row gutter={4}>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Receiption} checked={ isWorkerRole(JobWorkerRole.Receiption)}>Reception</Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Survey} checked={ isWorkerRole(JobWorkerRole.Survey)}>Servey</Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Technician} checked={ isWorkerRole(JobWorkerRole.Technician)}>Technician</Checkbox></Col>
                </Row>
                <Row gutter={4}>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Analaysis} checked={ isWorkerRole(JobWorkerRole.Analaysis)}><Text>Analysis</Text></Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Process_Owner} checked={ isWorkerRole(JobWorkerRole.Process_Owner)}><Text>Process Owner</Text></Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Casheir} checked={ isWorkerRole(JobWorkerRole.Casheir)}><Text>Cashier</Text></Checkbox></Col>
                </Row>
            </div>
            <div style={{margin:'auto',width:'100%',padding:'10px',marginTop:'15px'}} className={'paper'}>
                <Text strong>Process</Text>
                <Divider/>
                <Row gutter={4}>
                    {
                        !clientHandlers.loading &&
                        !clientHandlers.error &&
                        clientHandlers.clientHandlers.map( (handler,key)=><Col key={key} span={8}>
                            <Checkbox onChange={handleProcessCheckBox} value={handler.meta.jobType} checked={isWorkersJobType(handler.meta.jobType)}>
                                <Text>{handler.meta.jobName}</Text>
                            </Checkbox>
                        </Col> )
                    }
                </Row>
            </div>
            <div style={{marginTop:'15px'}}>
                <Row gutter={8}>
                    <Col span={8}>
                        <CButton block icon={'save'} type={'primary'} onClick={saveWorkerData}>
                            Save
                        </CButton>
                    </Col>
                    <Col span={8}>
                        <CButton block icon={'cancel'} type={'danger'} onClick={closeWorkerForm}>
                           Cancel
                        </CButton>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default WorkerFormContainer