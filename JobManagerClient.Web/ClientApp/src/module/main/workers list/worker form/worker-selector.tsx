import React from 'react'
import {Spin, TreeSelect} from 'antd';
import {useWorkerTreeConstructor} from "../../../../shared/hooks/workers-tree-constructor";

interface IProps{
    handleSelectedEmployee:(employeeId:string)=>void
}
export function WorkerSelector(props : IProps) {
    const [ workersTree , loading, error ] = useWorkerTreeConstructor()
    function handleSelectedEmployeeChange(selectedEmployee: string){
        props.handleSelectedEmployee(selectedEmployee)
    }

    return (
        <Spin spinning={loading}>
            <TreeSelect
                showSearch
                style={{ width: '100%' }}
                dropdownStyle={{ maxHeight: '50vh', overflow: 'auto' }}
                treeData={workersTree}
                placeholder="Select Employee"
                treeDefaultExpandAll
                onChange={handleSelectedEmployeeChange}
            />
        </Spin>
    )
}