import React, {useState, useEffect } from 'react'
import {Checkbox, Col, Divider, notification, Popconfirm, Row, Typography} from 'antd'
import CButton from "../../../../shared/core/cbutton/cbutton";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {JobRuleStateModel} from "../../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {useDispatch, useSelector} from 'react-redux';
import { JobWorker } from '../../../../_model/view_model/mn-vm/internal-service-vm/job-worker';
import { Employee } from '../../../../_model/view_model/IEmployee';
import { JobWorkerState } from '../../../../_model/state-model/mn-sm/job-worker-state';
import { StandardJobTypes } from '../../../../_model/view_model/mn-vm/standard-job-types';
import { JobWorkerRole } from '../../../../_enum/mn-enum/job-worker-role';
import JobManagerService from "../../../../_services/job.manager.service";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import errorMessage from "../../../../_redux_setup/actions/error.message";
import {closeDrawerModal} from "../../../../_redux_setup/actions/drawer-modal-actions";
import {jobWorkerRemove} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {searchEmployeeRemove} from "../../../../_redux_setup/actions/hr-actions/employee-actions";
const {Text, Title} = Typography

interface IProps{
    employee: Employee
}
const job_manager = new JobManagerService()
const WorkerProfileContainer = (props:IProps) =>{
    const [ clientHandlers,auth , workerState ] = useSelector<ApplicationState, [JobRuleStateModel,AuthenticationState,JobWorkerState]>(
        appState => [ 
            appState.job_rule,
            appState.auth,
            appState.worker_state
        ]);
    const [ worker , setWorker ] = useState<JobWorker>()
    const dispatch = useDispatch();
    useEffect(()=>{
        const tempWorker = workerState.workers.find(wrk => wrk.employeeID === props.employee.id)    
        setWorker(tempWorker)
    },[workerState]);
    function isWorkersJobType(jobType : StandardJobTypes): boolean {
        if(!worker)
            return false;
        const job = worker.jobTypes.find(job => job === jobType);
        return job === undefined ? false : true;
    }
    function isWorkerRole(role : JobWorkerRole): boolean {
        if(!worker)
            return false;
        const workerRole = worker.roles.find(rl => rl === role);
        return workerRole === undefined ? false : true;
    }
    const saveWorkerData = async ()=>{
        if(!worker)
            return;
        if( !(worker.jobTypes.length > 0) ){
            openNotificationWithIcon('error',"Process","Select at least one process")
            return;
        }
        if( !(worker.roles.length > 0 ) ){
            openNotificationWithIcon('error',"Role","Select at least one role")
            return;
        }
        try{
            openNotificationWithIcon('info',"Update worker information","Updating worker information please wait")
            await job_manager.UpdateJobWorker({worker,sessionID:auth.sessionId})
            openNotificationWithIcon('success',"Success","Worker information updated successfully!")
        }catch(error){
            openNotificationWithIcon('error',"Errno",errorMessage(error))
        }
    }
    const deleteWorker = async()=>{
        if(!worker)
            return;
        try{
            openNotificationWithIcon('info',"Deleting worker information","Deleting worker information please wait")
            await job_manager.DeleteWorker({sessionID:auth.sessionId,userName:worker.userID})
            openNotificationWithIcon('success',"Success","Worker deleted successfully!")
            dispatch(closeDrawerModal());
            dispatch(jobWorkerRemove(worker.userID))
            dispatch(searchEmployeeRemove(props.employee.employeeID))
        }
        catch(error){openNotificationWithIcon('error',"Errno",errorMessage(error))}
    }
    const toggleWorkerActivation = async ()=>{
        if(!worker)
            return;
        try{
            openNotificationWithIcon('info',"Changing worker status","Changing worker status, please wait ...")
            await job_manager.ChangeWorkerStatus({sessionID:auth.sessionId,userID:worker.userID,active:!worker.active})
            setWorker({...worker,active:!worker.active})
            openNotificationWithIcon('success',"Success","Worker status changed successfully!")
        }
        catch(error){openNotificationWithIcon('error',"Errno",errorMessage(error))}
    }
    const openNotificationWithIcon = (type : 'success' | 'info' | 'warning' | 'error',message: string,description: string) => {
        notification[type]({
            message,
            description,
        });
    };
    const handleProcessCheckBox = (event : any ) => {
        if(!worker)
            return;
        const isChecked = event.target.checked;
        const processID = event.target.value as number;
        if(isChecked)
            setWorker({...worker,jobTypes:[...worker.jobTypes,processID]})
        else
            setWorker({...worker,jobTypes:[...worker.jobTypes.filter(rl => rl !== processID)]})
    }
    const handleRoleCheckBox = (event : any ) => {
        if(!worker)
            return;
        const isChecked = event.target.checked;
        const roleId = event.target.value as number;
        if(isChecked)
            setWorker({...worker,roles:[...worker.roles,roleId]})
        else
            setWorker({...worker,roles:[...worker.roles.filter(rl => rl !== roleId)]})
    }
    return (
        <div>
            <Title level={4}>Technical Staff Profile</Title>
            <Divider/>
            <div style={{margin:'auto',width:'100%',padding:'10px',marginTop:'10px'}} className={'paper'}>
                <Text strong>Employee Information</Text>
                <Divider/>
                <Row gutter={4}>
                    <Col span={12}>
                        <Row style={{borderRightColor:'#999',borderRightStyle:'solid',borderRightWidth:'1px'}}>
                            <Col span={12}><Text><span><Text strong>Name</Text></span> </Text></Col>
                            <Col span={12}><Text>{props.employee.employeeName}</Text></Col>

                            <Col span={12}><Text><span><Text strong>Address</Text></span> </Text></Col>
                            <Col span={12}><Text>{props.employee.address}</Text></Col>
                        </Row>
                    </Col>
                    <Col span={12}>
                        <Row>
                            <Col span={12}><Text strong>Title</Text></Col>
                            <Col span={12}><Text>{props.employee.title}</Text></Col>
                            <Col span={12}><Text><span><Text strong>Phone Number</Text></span> </Text></Col>
                            <Col span={12}><Text>{props.employee.telephone}</Text></Col>
                        </Row>
                    </Col>
                </Row>
            </div>
            <div style={{margin:'auto',width:'100%',padding:'10px', minHeight:'25vh',marginTop:'15px'}} className={'paper'}>
                <Text strong>Roles</Text>
                <Divider/>
                <Row gutter={4}>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Receiption} checked={ isWorkerRole(JobWorkerRole.Receiption)}>Reception</Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Survey} checked={ isWorkerRole(JobWorkerRole.Survey)}>Servey</Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Technician} checked={ isWorkerRole(JobWorkerRole.Technician)}>Technician</Checkbox></Col>
                </Row>
                <Row gutter={4}>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Analaysis} checked={ isWorkerRole(JobWorkerRole.Analaysis)}><Text>Analysis</Text></Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Process_Owner} checked={ isWorkerRole(JobWorkerRole.Process_Owner)}><Text>Process Owner</Text></Checkbox></Col>
                    <Col span={8}><Checkbox onChange={handleRoleCheckBox} value={JobWorkerRole.Casheir} checked={ isWorkerRole(JobWorkerRole.Casheir)}><Text>Cashier</Text></Checkbox></Col>
                </Row>
            </div>
            <div style={{margin:'auto',width:'100%',padding:'10px',marginTop:'15px'}} className={'paper'}>
                <Text strong>Process</Text>
                <Divider/>
                <Row gutter={4}>
                    {
                        !clientHandlers.loading &&
                        !clientHandlers.error &&
                        clientHandlers.clientHandlers.map( (handler,key)=><Col key={key} span={8}>
                                                                            <Checkbox onChange={handleProcessCheckBox} value={handler.meta.jobType} checked={isWorkersJobType(handler.meta.jobType)}>
                                                                                <Text>{handler.meta.jobName}</Text>
                                                                            </Checkbox>
                                                                          </Col> )
                    }
                </Row>
            </div>
            <div style={{marginTop:'15px'}}>
                <Row gutter={8}>
                    <Col span={8}>
                        <CButton block icon={'save'} type={'primary'} onClick={saveWorkerData}>
                        Save
                        </CButton>
                    </Col>
                    <Col span={8}>
                        <Popconfirm
                            title="Are you sure you want to change the worker status?"
                            onConfirm={toggleWorkerActivation}
                            okText="Yes"
                            cancelText="No"
                        >
                            <CButton block icon={'close'} type={'ghost'}>{worker && worker.active ? 'Deactivate' : 'Activate' }</CButton>
                        </Popconfirm>
                    </Col>
                    <Col span={8}>
                        <Popconfirm
                            title="Are you sure you want to delete worker?"
                            onConfirm={deleteWorker}
                            okText="Yes"
                            cancelText="No"
                        >
                            <CButton block icon={'delete'} type={'danger'}>Delete</CButton>
                        </Popconfirm>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default WorkerProfileContainer