import React, {useEffect, Fragment} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {IBreadcrumb} from "../../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../../_constants/routes";
import { Card } from 'antd';
import { UserIcon, NewUserIcon } from '../../../static/images/index-imgs';
import ButtonGridContainer from '../../../shared/screens/button-grid-container/button-grid-container';
import { fetchAllWorkerList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { Employee } from '../../../_model/view_model/IEmployee';
import { openDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';
import WorkerProfileContainer from './worker profile/worker-profile';
import WorkerFormContainer from "./worker form/worker-form-container";
import {useBreadcrumb} from "../../../shared/hooks/manage-breadcrumb";
import {StateMachineHOC} from "../../../shared/hoc/StateMachineHOC";
const { Meta } = Card;


const backwardPath :IBreadcrumb[] =[
    {
        path:ROUTES.MAIN.INDEX,
        breadcrumbName:'Tools'
    },
    {
        path:ROUTES.MAIN.WORKER_LIST,
        breadcrumbName:'Workers List'
    }
]
const styledTitle = (title: string) => <p style={{ textAlign: 'center', color: '#444', fontWeight: 'bold' }}>{title}</p>

const WorkersListContainer = ()=>{
    const dispatch = useDispatch()
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const workerState = useSelector((appState:ApplicationState)=>appState.employeeSearch)
    useBreadcrumb(backwardPath)
    useEffect(()=>{
        dispatch(fetchAllWorkerList({sessionId:auth.sessionId}))
    },[])


    const NewWorker = () =>{
        return(
            <Card
                hoverable
                cover={<img style={{width:'40%',paddingTop:'20px',margin:'auto'}} alt={'user ico'} src={NewUserIcon} />}
                onClick={()=>dispatch(openDrawerModal(<WorkerFormContainer/>))}
                bordered={false} >
                <Meta
                    title={styledTitle('New Worker')}
                />
            </Card>
        )
    }

    const WorkerCard = (worker:Employee) =>{
        return(
            <Fragment>
                {
                    !worker &&
                    <Card
                        hoverable={false}
                        style={{ minHeight:'250px' }}
                        cover={<img style={{width:'40%',paddingTop:'20px',margin:'auto'}} alt={'user ico'} src={UserIcon} />}
                        bordered={false} >
                        <Meta
                            title={<p style={{color:'red',textAlign:'center'}}>Failed to load data</p>}
                        />
                    </Card>
                }
                {
                    worker &&
                    <Card
                        hoverable
                        style={{ minHeight:'250px' }}
                        cover={<img style={{width:'40%',paddingTop:'20px',margin:'auto'}} alt={'user ico'} src={UserIcon} />}
                        onClick={()=>dispatch(openDrawerModal(<WorkerProfileContainer employee={worker}/>))}
                        bordered={false} >
                        <Meta
                            description={styledTitle(worker.employeeName)}
                        />
                    </Card>
                }
            </Fragment>
        )
    }


    return (
        <Fragment>
            <ButtonGridContainer
                    spacing={16}
                    lg={{span:4}}
                    component={NewWorker}
                    listItem={[1]}/>
            <StateMachineHOC state={ workerState }>
                <ButtonGridContainer
                    spacing={16}
                    lg={{span:4}}
                    component={WorkerCard}
                    listItem={workerState.payload!= null ? workerState.payload._ret : []}/>
            </StateMachineHOC>
        </Fragment>
    )  
}

export default WorkersListContainer