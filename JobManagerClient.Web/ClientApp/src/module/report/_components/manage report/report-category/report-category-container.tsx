import React, {FC, useEffect} from "react";
import {Divider} from "antd";
import {useSelector} from "react-redux";
import {AuthenticationState} from "../../../../../_model/state-model/auth-state";
import {ReportCategoryState} from "../../../../../_model/state-model/report-category-state";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";
import {
    ExtendedTreeNodeNormal,
    TreeMetaData,
    useTreeConstructorFromList
} from "../../../../../shared/hooks/tree-constructor-from-list";
import {ReportCategory} from "../../../../../_model/view_model/report-category";
import {CustomTreeDisplay} from "../../../../../shared/components/tree/custom-tree";

interface ReportCategoryContainerProps {
    handleSelectedReportCategoryChange:(selectedReportCategory: ReportCategory)=> void
}

export const ReportCategoryContainer: FC<ReportCategoryContainerProps> = (props) => {
    const [auth, report_category_state] = useSelector<ApplicationState, [AuthenticationState, ReportCategoryState]>(appState => [appState.auth, appState.reportCategory])
    const reportCategoryTreeMetaData : TreeMetaData = {
        rootNodeId: -1,
        valueKey: 'id',
        parentIdKey: 'pid',
        titleKey: 'name'
    }
    const [ reportCategoryTreeState ] = useTreeConstructorFromList<ReportCategory>(report_category_state.categories,reportCategoryTreeMetaData)
    function  handleNodeClick(clickedNode: ExtendedTreeNodeNormal){
        const reportCategory = report_category_state.categories.find(rc=> rc.id === clickedNode.value)
        reportCategory && props.handleSelectedReportCategoryChange(reportCategory)
    }
    return (
        <StateMachineHOC state={report_category_state}>
            <Divider orientation={'left'}>Report Categories</Divider>
            <StateMachineHOC state={reportCategoryTreeState}>
                <CustomTreeDisplay nodeClickHandler={handleNodeClick} treeNodes={reportCategoryTreeState.tree}/>
            </StateMachineHOC>
        </StateMachineHOC>
    )
}