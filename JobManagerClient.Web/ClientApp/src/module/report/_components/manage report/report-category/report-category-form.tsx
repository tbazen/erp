import React, {FC, useEffect, useState} from "react";
import {Form, Input} from "antd";
import {ReportCategory} from "../../../../../_model/view_model/report-category";

interface ReportCategoryFormProps {
    reportCategory?: ReportCategory
    handleReportCategoryChange: (newReportCategory: ReportCategory) => void
}

const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}
const initReportCategory: ReportCategory = {
    id:0,
    pid:-1,
    name:''
}
export const ReportCategoryForm: FC<ReportCategoryFormProps>= (props)=>{
    const [ reportCategory,setReportCategory ] = useState<ReportCategory>(props.reportCategory || initReportCategory)
    useEffect(()=>{
        reportCategory &&
        props.handleReportCategoryChange(reportCategory)
    },[ reportCategory ])
    return (
        <Form>
            <Form.Item label="Report Category Name" {...formItemLayout}>
                <Input
                    value={  reportCategory.name}
                    onChange={(event)=>setReportCategory({...reportCategory,name: event.target.value})}
                />
            </Form.Item>
        </Form>
    )
}