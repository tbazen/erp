import React, {FC, useEffect, useState} from 'react'
import {Col, Popconfirm, Row, Table} from "antd";
import CButton from "../../../../../shared/core/cbutton/cbutton";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../_model/state-model/auth-state";
import {ReportDefinitionState} from "../../../../../_model/state-model/report-definition-state";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";
import {ReportCategory} from "../../../../../_model/view_model/report-category";
import {ReportDefination} from "../../../../../_model/level0/accounting-type-library/ehtml";
import {DeleteReportPar} from "../../../../../_services/accounting.service";
import {requestDeleteReportDefinition} from "../../../../../_redux_setup/actions/report-category-actions";

const {Column} = Table

interface ReportDefinitionContainerProps {
    selectedReportCategory?: ReportCategory
}

export const ReportDefinitionContainer: FC<ReportDefinitionContainerProps> = (props) => {
    const dispatch = useDispatch()
    const [auth, report_definition_state] = useSelector<ApplicationState, [AuthenticationState, ReportDefinitionState]>(appState => [appState.auth, appState.reportDefinitions])
    const [ tableData, setTableData ] = useState<ReportDefination[]>([])
    useEffect(()=>{
        if(props.selectedReportCategory) {
            const filteredReportDefinitions = report_definition_state.definitions.filter(rf=> props.selectedReportCategory && rf.categoryID === props.selectedReportCategory.id)
            setTableData(filteredReportDefinitions)
        }else setTableData([])
    },[ props.selectedReportCategory ])

    useEffect(()=>{
        if(props.selectedReportCategory) {
            const filteredReportDefinitions = report_definition_state.definitions.filter(rf=> props.selectedReportCategory && rf.categoryID === props.selectedReportCategory.id)
            setTableData(filteredReportDefinitions)
        }else setTableData([])
    },[ report_definition_state ])

    function handleDeleteReportDefinition(reportDefinitionId: number){
        const deleteReportDefinitionPar: DeleteReportPar = {
            sessionId: auth.sessionId,
            reportID: reportDefinitionId
        }
        dispatch(requestDeleteReportDefinition(deleteReportDefinitionPar))
    }
    return (
        <StateMachineHOC state={report_definition_state}>
            <Table
                scroll={{y: '65vh'}}
                size={'small'}
                dataSource={tableData}
                pagination={false}
                rowKey={(record, index) => index.toString()}
            >
                <Column
                    width={'40%'}
                    title="Name"
                    key="name"
                    render={(text, record: ReportDefination) => (
                        <p style={{wordWrap: 'break-word', wordBreak: 'break-word'}}>{record.name}</p>)}
                />
                <Column
                    width={'15%'}
                    title="Code"
                    key="code"
                    render={(text, record: ReportDefination) => (
                        <p style={{wordWrap: 'break-word', wordBreak: 'break-word'}}>{record.code}</p>)}
                />
                <Column
                    width={'15%'}
                    title="Type"
                    key="type"
                    render={(text, record: ReportDefination) => (
                        <p style={{wordWrap: 'break-word', wordBreak: 'break-word'}}>{record.reportTypeID}</p>)}
                />
                <Column
                    width={'30'}
                    title="Action"
                    key="actions"
                    render={(text, record: ReportDefination, index) => (
                        <Row gutter={2}>
                            <Col span={6}>
                                <Popconfirm
                                    title="Are you sure delete this report definition?"
                                    onConfirm={()=>{ handleDeleteReportDefinition(record.id) }}
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <CButton icon={'delete'} type={'danger'}>Delete</CButton>
                                </Popconfirm>

                            </Col>
                        </Row>
                    )}
                />
            </Table>
        </StateMachineHOC>
    )
}