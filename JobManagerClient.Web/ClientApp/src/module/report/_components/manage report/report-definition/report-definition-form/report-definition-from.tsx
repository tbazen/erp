import React, {FC, useEffect, useState} from "react";
import {ReportDefination} from "../../../../../../_model/level0/accounting-type-library/ehtml";
import { Divider, Form, Input, Row} from "antd";
import CModal from "../../../../../../shared/core/cmodal/cmodal";
import {FormulaEditorFrom} from "../../../report-container/formula-container/formula-editor-form/formula-editor-form";
import {ReportFormulaListItemContainer} from "./report-formula-list-item";
import {ReportDefinitionMainFormula} from "./report-definition-main-formula";
import {ReportTypeSelector} from "./report-type-selector";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../../_model/state-model/auth-state";
import {ReportTypeState} from "../../../../../../_model/state-model/r-sm/report-types-state";
const { TextArea } = Input

interface ReportDefinitionFormContainerProps {
    reportDefinition: ReportDefination
    handleReportDefinitionChange: (newReportDefinition: ReportDefination) => void
}
const formLayout= 'horizontal'
const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

export const ReportDefinitionFormContainer:FC<ReportDefinitionFormContainerProps> = (props) =>{
    const [ auth, report_type_state  ] = useSelector<ApplicationState,[ AuthenticationState, ReportTypeState ]>(appState=>[ appState.auth,appState.report_type_state ])
    const [ reportDefinition , setReportDefinition] =useState<ReportDefination>(props.reportDefinition)
    const [ activeFormula,setActiveFormula ] =useState<{variable: string,expression: string}>({variable:'',expression:''})
    useEffect(()=>{
        props.handleReportDefinitionChange(reportDefinition)
    },[ reportDefinition ])

    function handleActiveFormulaChange(variable: string,expression: string){ setActiveFormula({variable,expression}) }
    function handleAddFormula(){
        setReportDefinition({
            ...reportDefinition,
            formula:{
                ...reportDefinition.formula,
                expressions:[...reportDefinition.formula.expressions,activeFormula.expression],
                variables:[...reportDefinition.formula.variables,activeFormula.variable]
            }})
    }
    function handleUpdateReportFormula(index: number){
        const variables = reportDefinition.formula.variables
        const expressions = reportDefinition.formula.expressions
        variables[index] = activeFormula.variable
        expressions[index] = activeFormula.expression
        setReportDefinition({...reportDefinition,formula:{...reportDefinition.formula,variables,expressions}})
    }
    function handleUpdateMainFormula(){
        setReportDefinition({
            ...reportDefinition,
            formula: { ...reportDefinition.formula,mainFormula:activeFormula.expression }
        })
    }
    function handleRemoveReportFormula(index: number){
        const variables = [...reportDefinition.formula.variables]
        const expressions = [...reportDefinition.formula.expressions]
        variables.splice(index,1)
        expressions.splice(index,1)
        console.log('DELETED INDEX',index)
        setReportDefinition({...reportDefinition,formula:{...reportDefinition.formula,variables:[...variables],expressions:[...expressions]}})
    }
    function handleReportTypeChange(selectedReportTypeId: number){
        const selectedReport = report_type_state.reportTypes.find(rt=> rt.id === selectedReportTypeId)
        if(selectedReport){
            setReportDefinition({
                ...reportDefinition,
                name:selectedReport.name,
                reportTypeID:selectedReport.id,
                description:selectedReport.description
            })
        }
    }

    return (
        <Row>
            <Divider orientation={'left'} >Report Definition Type</Divider>
            <ReportTypeSelector handleReportTypeChange={handleReportTypeChange}/>
            <Divider orientation={'left'} >Report Definition Formula</Divider>
            <CModal
                withConfirmation
                modalProps={{destroyOnClose:true}}
                buttonText={'Add Formula'}
                buttonProps={{size:'small',type:'primary'}}
                submitHandler={handleAddFormula}
            >
                <FormulaEditorFrom variable={''} expression={''} handleFormulaChange={handleActiveFormulaChange}/>
            </CModal>
            <ReportDefinitionMainFormula
                reportDefinition={reportDefinition}
                handleActiveFormulaChange={handleActiveFormulaChange}
                handleUpdateMainFormula={handleUpdateMainFormula}
            />
            <ReportFormulaListItemContainer
                reportDefinition={reportDefinition}
                handleActiveFormulaChange={handleActiveFormulaChange}
                handleRemoveReportFormula={handleRemoveReportFormula}
                handleUpdateReportFormula={handleUpdateReportFormula}
            />
            <Divider orientation={'left'}>General Information</Divider>
            <Form layout={formLayout}>
                <Form.Item {...formItemLayout} label={'Name'}>
                    <Input
                        value={reportDefinition.name}
                        onChange={(e)=>setReportDefinition({...reportDefinition,name:e.target.value})} />
                </Form.Item>
                <Form.Item {...formItemLayout} label={'Stylesheet'}>
                    <Input
                        value={reportDefinition.styleSheet}
                        onChange={(e)=>setReportDefinition({...reportDefinition,styleSheet:e.target.value})} />
                </Form.Item>
                <Form.Item {...formItemLayout} label={'Code'}>
                    <Input
                        value={reportDefinition.code}
                        onChange={(e)=>setReportDefinition({...reportDefinition,code:e.target.value})} />
                </Form.Item>
                <Form.Item {...formItemLayout} label={'Description'}>
                    <TextArea
                        autosize={{minRows:5}}
                        value={reportDefinition.description}
                        onChange={(e)=>setReportDefinition({...reportDefinition,description:e.target.value})} />
                </Form.Item>
            </Form>
        </Row>
    )
}