import React,{ FC }  from "react";
import {ReportDefination} from "../../../../../../_model/level0/accounting-type-library/ehtml";
import {Col, Divider, Row, Typography} from "antd";
import CModal from "../../../../../../shared/core/cmodal/cmodal";
import {FormulaEditorFrom} from "../../../report-container/formula-container/formula-editor-form/formula-editor-form";
import CButton from "../../../../../../shared/core/cbutton/cbutton";
const { Title,Text } = Typography
interface ReportDefinitionMainFormula{
    reportDefinition: ReportDefination,
    handleUpdateMainFormula: ()=>void,
    handleActiveFormulaChange: (variable: string, expression: string)=>void
}
export const ReportDefinitionMainFormula:FC<ReportDefinitionMainFormula>=(props)=>{

    return (
        <Row gutter={2} style={{marginTop:'10px',marginBottom:'10px',padding:'5px'}} className={'paper'}>
            <Col span={16}>
                <Text strong>
                    mainFormula
                    <Divider type="vertical" />
                    {props.reportDefinition.formula.mainFormula}
                </Text>
            </Col>
            <Col span={4}>
                <CModal
                    block
                    withConfirmation
                    modalProps={{destroyOnClose:true}}
                    buttonText={'Edit'}
                    buttonProps={{size:'small',type:'primary'}}
                    submitHandler={props.handleUpdateMainFormula}
                >
                    <FormulaEditorFrom variable={'mainFormula'} expression={props.reportDefinition.formula.mainFormula || ''} isVariableNotEditable handleFormulaChange={props.handleActiveFormulaChange}/>
                </CModal>
            </Col>
            <Col span={4}>
                <CButton size={'small'} disabled type={'danger'} block>Remove</CButton>
            </Col>
        </Row>
    )
}