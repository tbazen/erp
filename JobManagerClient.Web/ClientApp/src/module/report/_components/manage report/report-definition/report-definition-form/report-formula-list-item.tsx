import React,{ FC } from "react";
import {ReportDefination} from "../../../../../../_model/level0/accounting-type-library/ehtml";
import {Col, Divider, Popconfirm, Row, Typography} from "antd";
import CModal from "../../../../../../shared/core/cmodal/cmodal";
import {FormulaEditorFrom} from "../../../report-container/formula-container/formula-editor-form/formula-editor-form";
import CButton from "../../../../../../shared/core/cbutton/cbutton";
const { Title,Text } = Typography
interface ReportFormulaListItemProps{
    reportDefinition: ReportDefination
    handleUpdateReportFormula: (index: number)=> void
    handleRemoveReportFormula: (index: number)=> void
    handleActiveFormulaChange: ( variable: string,expression: string )=> void
}

export const ReportFormulaListItemContainer:FC<ReportFormulaListItemProps> = (props)=>{
    return (
        <div>
            { props.reportDefinition.formula.variables.map((variable,index)=>(
                <Row gutter={2} style={{marginBottom:'10px',padding:'5px'}} key={index} className={'paper'}>
                    <Col span={16}>
                        <Row gutter={2}>
                            <Col lg={{span:12}}>
                                <Text strong>{props.reportDefinition.formula.variables[index]}</Text>
                            </Col>
                            <Col lg={{span:12}}>
                                <Divider type="vertical" />
                                {props.reportDefinition.formula.expressions[index]}
                            </Col>
                        </Row>
                    </Col>
                    <Col span={4}>
                        <CModal
                            block
                            withConfirmation
                            modalProps={{destroyOnClose:true}}
                            buttonText={'Edit'}
                            buttonProps={{size:'small',type:'primary'}}
                            submitHandler={()=>props.handleUpdateReportFormula(index)}
                        >
                            <FormulaEditorFrom variable={props.reportDefinition.formula.variables[index]} expression={props.reportDefinition.formula.expressions[index]} handleFormulaChange={props.handleActiveFormulaChange}/>
                        </CModal>
                    </Col>
                    <Col span={4}>
                        <Popconfirm
                            title="Are you sure delete this report category?"
                            onConfirm={()=>props.handleRemoveReportFormula(index)}
                            okText="Yes"
                            cancelText="No"
                        >
                            <CButton size={'small'} type={'danger'} block>Remove</CButton>
                        </Popconfirm>
                    </Col>
                </Row>
            )) }
        </div>
    )
}