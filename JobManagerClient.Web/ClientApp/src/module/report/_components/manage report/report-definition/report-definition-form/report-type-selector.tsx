import React, {FC, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../../_model/state-model/auth-state";
import {ReportTypeState} from "../../../../../../_model/state-model/r-sm/report-types-state";
import {GetAllReportTypesPar} from "../../../../../../_services/accounting.service";
import {fetchReportTypes} from "../../../../../../_redux_setup/actions/report-actions/report-actions";
import {Form, Select} from "antd";
import {StateMachineHOC} from "../../../../../../shared/hoc/StateMachineHOC";
const { Option } = Select

interface ReportTypeSelectorProps{
    handleReportTypeChange: (selectedReportTypeId:number)=> void
}

const formItemLayout = {
    labelCol:{span:24},
    wrapperCol:{span: 24}
}
export const ReportTypeSelector:FC<ReportTypeSelectorProps> = (props) =>{
    const dispatch = useDispatch()
    const [ auth, report_type_state ] = useSelector<ApplicationState,[ AuthenticationState,ReportTypeState ]>(appState=>[ appState.auth,appState.report_type_state ])
    useEffect(()=>{
        if(report_type_state.reportTypes.length === 0) {
            const reportTypePar: GetAllReportTypesPar = {
                sessionId: auth.sessionId
            }
            dispatch(fetchReportTypes(reportTypePar))
        }
    },[ ])

    return (
        <StateMachineHOC state={report_type_state}>
            <Form.Item label="Report Type" {...formItemLayout}>
                <Select style={{width:'100%'}} onChange={(reportTypeId: number)=>props.handleReportTypeChange(reportTypeId)} >
                    { report_type_state.reportTypes.map((value,key)=><Option key={key} value={value.id}> { value.name }</Option>) }
                </Select>
            </Form.Item>
        </StateMachineHOC>
    )
}