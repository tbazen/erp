import React, {useState} from 'react'
import {Col, Divider, Popconfirm, Row} from "antd";
import {ReportCategoryContainer} from "./report-category/report-category-container";
import {ReportDefinitionContainer} from "./report-definition/report-definition-container";
import {ReportCategory} from "../../../../_model/view_model/report-category";
import {CAlert} from "../../../../shared/core/calert/calert";
import CButton from "../../../../shared/core/cbutton/cbutton";
import CModal from "../../../../shared/core/cmodal/cmodal";
import {ReportCategoryForm} from "./report-category/report-category-form";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {
    DeleteReportCategoryPar,
    SaveEHTMLDataPar,
    SaveReportCategoryPar
} from "../../../../_services/accounting.service";
import {
    requestDeleteReportCategory,
    requestSaveReportCategory
} from "../../../../_redux_setup/actions/report-category-actions";
import {ReportDefinitionFormContainer} from "./report-definition/report-definition-form/report-definition-from";
import {EHTMLData, ReportDefination} from "../../../../_model/level0/accounting-type-library/ehtml";
import {requestSaveReportDefinition} from "../../../../_redux_setup/actions/report-actions/report-actions";
import {NotificationTypes, openNotification} from "../../../../shared/components/notification/notification";

//TODO: styeSheet field hard coded not good practice
const initReportDefinition : ReportDefination = {
    formula : new EHTMLData(),
    description:'',
    styleSheet:'berp.css',
    name: '',
    id:-1,
    reportTypeID:-1,
    categoryID:-1,
    code:''
}

export default function ReportManageContainer() {
    const dispatch = useDispatch()
    const [ auth ] = useSelector< ApplicationState,[ AuthenticationState ] >(appState=> [ appState.auth ])
    const [ selectedReportCategory,setSelectedReportCategory ] = useState<ReportCategory>()
    const [ activeReportCategory , setActiveReportCategory ] = useState<ReportCategory>()
    const [ reportDefinition, setReportDefinition ] = useState<ReportDefination>({...initReportDefinition})

    function handleSelectedReportCategoryChange(selectedReportCategory: ReportCategory){
        setSelectedReportCategory(selectedReportCategory)
    }
    function handleReportCategoryChange(newReportCategory: ReportCategory){
        setActiveReportCategory(newReportCategory)
    }
    function handleRenameReportCategory() {
        if(selectedReportCategory) {
            if (activeReportCategory) {
                const reportCategoryPar: SaveReportCategoryPar = {
                    sessionId: auth.sessionId,
                    cat: { ...selectedReportCategory,name: activeReportCategory.name}
                }
                dispatch(requestSaveReportCategory(reportCategoryPar))
            }
        }
    }
    function handleAddChildReportCategory(){
        if(selectedReportCategory) {
            if (activeReportCategory) {
                const reportCategoryPar: SaveReportCategoryPar = {
                    sessionId: auth.sessionId,
                    cat: {...activeReportCategory,pid: selectedReportCategory.id}
                }
                dispatch(requestSaveReportCategory(reportCategoryPar))
            }
        }
    }
    function handleDeleteReportCategory() {
        if(selectedReportCategory){
            const deleteReportCategoryPar : DeleteReportCategoryPar ={
                sessionId:auth.sessionId,
                catID: selectedReportCategory.id
            }
            dispatch(requestDeleteReportCategory(deleteReportCategoryPar))
            setSelectedReportCategory(undefined)
        }
    }
    function handleAddNewReportCategory() {
        if(activeReportCategory) {
            const reportCategoryPar: SaveReportCategoryPar = {
                sessionId: auth.sessionId,
                cat: activeReportCategory
            }
            dispatch(requestSaveReportCategory(reportCategoryPar))
        }
    }
    function handleReportDefinitionChange(newReportDefinition : ReportDefination){
        setReportDefinition(newReportDefinition)
    }
    function handleSubmitReportDefinition(){
        try{
            if(reportDefinition.reportTypeID === -1)
                throw new Error('Please select the report type')
            if(!selectedReportCategory)
                throw new Error('Please select report category')
            if(selectedReportCategory){
                const saveEHTMLDataPar : SaveEHTMLDataPar = {
                    sessionId: auth.sessionId,
                    r: {...reportDefinition,categoryID:selectedReportCategory.id}
                }
                dispatch(requestSaveReportDefinition(saveEHTMLDataPar))
            }
        } catch(error){
            openNotification({
                notificationType:NotificationTypes.ERROR,
                message:'Request not sent',
                description:error.message
            })
        }
    }
    return (
        <Row gutter={4}>
            <Col lg={{span: 8}}>
                <div className={'paper'} style={{padding: '10px'}}>
                    <ReportCategoryContainer handleSelectedReportCategoryChange={handleSelectedReportCategoryChange}/>
                </div>
            </Col>

            <Col lg={{span: 16}}>
                <Row>
                    <Col lg={{span:24}}>
                        <div className={'paper'} style={{padding: '10px',marginBottom:'5px'}}>
                            <Divider orientation={'left'}>Selected Report Category</Divider>
                            <div style={{marginBottom:'10px'}}>
                                <CAlert
                                    type={selectedReportCategory? 'info': 'error'}
                                    message={selectedReportCategory? selectedReportCategory.name : 'Report category not selected'}
                                />
                            </div>
                            <Row gutter={4}>
                                <Col lg={{span:24}}/>
                                <Col lg={{span:4}}>
                                    <CModal
                                        block
                                        withConfirmation
                                        title={'Report Category'}
                                        buttonText={'Add Root Category'}
                                        buttonProps={{type:'primary'}}
                                        modalProps={{destroyOnClose:true}}
                                        submitHandler={handleAddNewReportCategory}
                                    >
                                        <ReportCategoryForm handleReportCategoryChange={handleReportCategoryChange}/>
                                    </CModal>
                                </Col>
                                <Col lg={{span:5}}>
                                    <CModal
                                        block
                                        withConfirmation
                                        title={'Report Category'}
                                        buttonText={'Add Child Category'}
                                        modalProps={{destroyOnClose:true}}
                                        submitHandler={handleAddChildReportCategory}
                                        buttonProps={{disabled:selectedReportCategory===undefined}}
                                    >
                                        <ReportCategoryForm handleReportCategoryChange={handleReportCategoryChange}/>
                                    </CModal>
                                </Col>
                                <Col lg={{span:5}}>
                                    <CModal
                                        block
                                        withConfirmation
                                        title={'Report Category'}
                                        modalProps={{destroyOnClose:true}}
                                        buttonText={'Rename Category'}
                                        submitHandler={handleRenameReportCategory}
                                        buttonProps={{disabled:selectedReportCategory===undefined}}
                                    >
                                        <ReportCategoryForm reportCategory={selectedReportCategory} handleReportCategoryChange={handleReportCategoryChange}/>
                                    </CModal>
                                </Col>
                                <Col lg={{span:5}}>
                                    <CModal
                                        block
                                        withConfirmation
                                        title={'Report Definition'}
                                        modalProps={{destroyOnClose:true}}
                                        buttonText={'Add Report Definition'}
                                        submitHandler={handleSubmitReportDefinition}
                                        buttonProps={{disabled:selectedReportCategory===undefined}}
                                    >
                                        <ReportDefinitionFormContainer reportDefinition={reportDefinition} handleReportDefinitionChange={handleReportDefinitionChange}/>
                                    </CModal>
                                </Col>
                                <Col lg={{span:5}}>
                                    <Popconfirm
                                        title="Are you sure delete this report category?"
                                        onConfirm={handleDeleteReportCategory}
                                        okText="Yes"
                                        cancelText="No"
                                    >
                                        <CButton type={'danger'} disabled={selectedReportCategory===undefined}  block>Delete Category</CButton>
                                    </Popconfirm>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    <Col lg={{span:24}}>
                        <div className={'paper'} style={{padding: '10px'}}>
                            <ReportDefinitionContainer selectedReportCategory={selectedReportCategory}/>
                        </div>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}