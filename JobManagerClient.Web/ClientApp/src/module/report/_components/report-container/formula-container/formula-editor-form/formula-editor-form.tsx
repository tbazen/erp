import React, {FC, useEffect, useState} from "react";
import {Form, Input} from "antd";
const { TextArea } = Input

interface FormulaEditorFormProps{
    variable: string,
    isVariableNotEditable?:boolean
    expression: string
    handleFormulaChange: (variable: string,expression: string)=> void
}

const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

export const FormulaEditorFrom : FC<FormulaEditorFormProps>=(props)=>{
    const [ formula,setFormula ] = useState<FormulaEditorFormProps>(props)
    useEffect(()=>{
        props.handleFormulaChange(formula.variable,formula.expression)
    },[ formula ])


    return (
        <Form>
            <Form.Item label="Variable" {...formItemLayout}>
                <Input
                    disabled={props.isVariableNotEditable}
                    value={formula.variable}
                    onChange={(event)=>setFormula({...formula,variable:event.target.value})}
                />
            </Form.Item>
            <Form.Item label="Expression" {...formItemLayout}>
                <TextArea
                    autosize={{minRows:8}}
                    value={formula.expression}
                    onChange={(event)=>setFormula({...formula,expression:event.target.value})}
                />
            </Form.Item>
        </Form>
    )
}