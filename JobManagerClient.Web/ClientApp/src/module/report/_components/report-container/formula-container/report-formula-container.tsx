import React, {ChangeEvent, createRef, FC, useEffect, useState} from 'react'
import {Col, Row, Table, Upload} from "antd";
import {EHTMLData, ReportDefination} from "../../../../../_model/level0/accounting-type-library/ehtml";
import CModal from "../../../../../shared/core/cmodal/cmodal";
import {FormulaEditorFrom} from "./formula-editor-form/formula-editor-form";
import CButton from "../../../../../shared/core/cbutton/cbutton";
import {ReportTab} from "../report-container";
import {
    ExportReportDefinitionPar, ImportReportDefinitionPar,
    WrapperReportServices
} from "../../../../../_services/wrapper-api-services/wrapper-report-services";
import {RcFile} from "antd/es/upload";
import {FileExtension} from "../../../../../_helpers/extensions/file-extension";
import {useDispatch, useSelector} from "react-redux";
import {requestImportReportDefinition} from "../../../../../_redux_setup/actions/report-actions/report-actions";
import ISession from "../../../../../_model/view_model/session-id";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../../_model/state-model/auth-state";

const {Column} = Table

interface ITableData {
    variable: string,
    expression: string,
    isMainFormula?: boolean
}

interface ReportFormulaContainerProps{
    reportDefinition: ReportDefination
    evaluateFormulaHandler: (symbol: string)=> void
    handleReportDefinitionChange: ( newReportDefinition:ReportDefination )=> void
    handleSaveReportDefinition: ()=>void
}

enum SwitchDirection{
    UP='up',
    DOWN='down'
}

interface Formula{
    variable: string
    expression: string
}
const wrapper_report_service = new WrapperReportServices()

const ReportFormulaContainer:FC<ReportFormulaContainerProps> = (props) => {
    const dispatch = useDispatch()
    const reportFileInputRef = createRef<HTMLInputElement>()
    const exportReportAnchorTag = createRef<HTMLAnchorElement>()
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=> [appState.auth])
    const [tableData, setTableData] = useState<ITableData[]>([])
    const [ reportDefinition,setReportDefinition ] = useState<ReportDefination>(props.reportDefinition)
    const [ activeFormula, setActiveFormula ] = useState<Formula>({ variable:'',expression:'' })
    useEffect(()=>{
        props.handleReportDefinitionChange(reportDefinition)
    },[ reportDefinition ])
    useEffect(() => { updateTableData() }, [ reportDefinition.formula ])

    function updateTableData() {
        if (reportDefinition) {
            const formula: EHTMLData = reportDefinition.formula
            let tableData: ITableData[] = []
            if (!formula) {
                tableData = [{isMainFormula: true,variable: 'mainFormula', expression: ''}]
                setTableData([...tableData])
                return
            }
            tableData = [{ isMainFormula:true, variable: 'mainFormula', expression: formula.mainFormula || ''}]
            for (let index = 0; index < formula.variables.length; index++) {
                tableData = [...tableData, {variable: formula.variables[index], expression: formula.expressions[index]}]
            }
            setTableData([...tableData])
        }
    }
    function switchFormulaPosition(index: number ,switchDirection: SwitchDirection){
        const record = tableData[index];
        if(switchDirection === SwitchDirection.DOWN){
            const upperRecord = tableData[index+1]
            tableData[index] = upperRecord
            tableData[index+1] = record
         } else {
            const lowerRecord = tableData[index -1]
            tableData[index] = lowerRecord
            tableData[ index-1 ] = record
        }
        setTableData([...tableData])
    }
    function updateTableRow(index: number) {
        let newTableData = [...tableData]
        if(index >= 0){
            newTableData[index] = {...newTableData[index], variable: activeFormula.variable,expression: activeFormula.expression }
        } else{
            newTableData = [...newTableData, { variable: activeFormula.variable,expression: activeFormula.expression }]
        }
        setTableData([...newTableData])
    }
    function handleFormulaChange(variable: string, expression: string){setActiveFormula({variable,expression})}

    async function handleExportReportDefination(){
        const exportParams : ExportReportDefinitionPar = {
            defination:reportDefinition,
        }
        const file = (await  wrapper_report_service.ExportReportDefinition(exportParams)).data
        FileExtension.startFileDownload(file,exportReportAnchorTag,reportDefinition.name)
    }

    async function handleImportReportDefinition(event:ChangeEvent<HTMLInputElement>){
        if(event.target.files){
            const file = event.target.files[0]
            if(file){
                const base64String = await FileExtension.parseBlobToBase64String(file)
                const importParams: ImportReportDefinitionPar & ISession = {
                    sessionId: auth.sessionId,
                    base64ReportFile: base64String,
                    defination: reportDefinition
                }
                dispatch(requestImportReportDefinition(importParams))
            }
        }
    }


    function handleEvaluateClick(symbol: string){
        props.evaluateFormulaHandler(symbol)
    }
    async function handleSaveReportDefinition(){
        await extractTableData()
        props.handleSaveReportDefinition()
    }
    async function extractTableData(){
        let variables:string[]= []
        let expressions:string[] =[]
        let mainFormula: string =''
        tableData.forEach(tableEntry=>{
            if(tableEntry.isMainFormula)
                mainFormula = tableEntry.expression
            else{
                variables = [...variables,tableEntry.variable]
                expressions = [...expressions,tableEntry.expression]
            }
        });
        setReportDefinition({...reportDefinition,formula:{...reportDefinition.formula,mainFormula,variables,expressions}})
        //TODO: this is temporary workaround update this code with custom hook that accepts call back
        await new Promise((res,rej)=>{
            setTimeout(()=>{
                res()
            },1000)
        })
    }


    return (
        <Row gutter={4}>
            <Col lg={{span: 24}}>
                <div style={{padding:'5px',margin:'5px'}}>
                    <Row gutter={4}>
                        <Col lg={{span:4}}>
                            <CModal
                                block
                                withConfirmation
                                buttonText={'Add New'}
                                buttonProps={{icon:'plus'}}
                                submitHandler={()=>updateTableRow(-1)}
                            >
                                <FormulaEditorFrom
                                    variable={''}
                                    expression={''}
                                    handleFormulaChange={handleFormulaChange}/>
                            </CModal>
                        </Col>
                        <Col lg={{span:4}}>
                            <CButton onClick={handleExportReportDefination} block  icon={'export'}>Export</CButton>
                            <a ref={exportReportAnchorTag} hidden></a>
                        </Col>
                        <Col lg={{span:4}}>
                            <CButton onClick={handleSaveReportDefinition} block icon={'save'}>Save</CButton>
                        </Col>
                        <Col lg={{span:4}}>
                            <CButton onClick={()=>{ reportFileInputRef && reportFileInputRef.current && reportFileInputRef.current.click()  }}  block icon={'import'}>
                                Import Report
                            </CButton>
                            <input ref={reportFileInputRef} onChange={handleImportReportDefinition} type={'file'} accept={'.reportdef'} hidden/>
                        </Col>
                    </Row>
                </div>
            </Col>

            <Col lg={{span: 24}}>
                <Table
                    scroll={{y: '70vh'}}
                    dataSource={tableData}
                    pagination={false}
                    rowKey={(record, index) => index.toString()}
                >
                    <Column title="Variable" width={'10%'} dataIndex="variable" key="variable"/>
                    <Column
                        width={'55%'}
                        title="Formula"
                        key="expression"
                        render={(text, record: ITableData) => (
                            <p style={{wordWrap: 'break-word', wordBreak: 'break-word'}}>{record.expression}</p>)}
                    />
                    <Column
                        title="Action"
                        key="actions"
                        render={(text, record: ITableData,index) => (
                            <Row gutter={2}>
                                <Col span={6}>
                                    <CModal
                                        withConfirmation
                                        buttonText={'Edit'}
                                        title={'Edit Formula'}
                                        children={<FormulaEditorFrom variable={record.variable}
                                                                     expression={record.expression}
                                                                     isVariableNotEditable={record.isMainFormula}
                                                                     handleFormulaChange={handleFormulaChange}
                                        />}
                                        submitHandler={()=>updateTableRow(index)}
                                        modalProps={{destroyOnClose: true}}
                                        buttonProps={{type: 'link',icon:'edit'}}/>
                                </Col>
                                <Col span={4}>
                                    <CButton disabled={index === 0} onClick={()=>switchFormulaPosition(index,SwitchDirection.UP)} icon={'up'} type={'link'}>up</CButton>
                                </Col>
                                <Col span={4}>
                                    <CButton disabled={ index === (tableData.length-1)} onClick={()=>switchFormulaPosition(index,SwitchDirection.DOWN)} icon={'down'} type={'link'}>down</CButton>
                                </Col>
                                <Col span={6}>
                                    <CButton icon={'caret-right'} onClick={()=>handleEvaluateClick(record.variable)} type={'link'}>Evaluate</CButton>
                                </Col>
                            </Row>
                        )}
                    />
                </Table>
            </Col>
        </Row>
    )
}

export default ReportFormulaContainer