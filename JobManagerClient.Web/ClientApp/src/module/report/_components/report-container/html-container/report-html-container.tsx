import React, {FC,Fragment, Ref} from 'react'
import {HTMLParser} from "../../../../../shared/components/html-parser/html-parser";
import {ReportHTMLState} from "../../../../../_model/state-model/r-sm/report-html-state";
import {ApplicationState} from "../../../../../_model/state-model/application-state";
import {useSelector} from "react-redux";
import {StateMachineHOC} from "../../../../../shared/hoc/StateMachineHOC";
import {ReportDefination} from "../../../../../_model/level0/accounting-type-library/ehtml";
import {appendReportStyle} from "../../../styles/report-style-dictionary";


interface ReportHTMLContainerProps{
    reportDefinition: ReportDefination
    //ref: Ref<HTMLDivElement>
}


const ReportHTMLContainer: FC<ReportHTMLContainerProps> = (props)=>{
    const [ report_html_state ] = useSelector<ApplicationState,[ ReportHTMLState ]>(appState=> [ appState.report_html_state ])
    return(
        <StateMachineHOC state={report_html_state}>
            <div>
            {
                report_html_state.payload &&
                <Fragment>
                    <HTMLParser htmlString={appendReportStyle(props.reportDefinition.styleSheet,report_html_state.payload.htmlRenderString)}/>
                    <div><code>{report_html_state.payload.htmlCodeString}</code></div>
                </Fragment>
            }
            </div>
        </StateMachineHOC>
    )
}

export default ReportHTMLContainer