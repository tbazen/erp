import React, {createRef, Fragment, useEffect, useState} from 'react'
import {Button, Col, Divider, Row, Tabs} from "antd";
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ReportDefinitionState} from "../../../../_model/state-model/report-definition-state";
import {
    clearReportHTML,
    fetchReport,
    FetchReportPar,
    loadingReport, requestEvaluate2, requestEvaluateFormula3Web, requestSaveReportDefinition
} from "../../../../_redux_setup/actions/report-actions/report-actions";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {ReportState} from "../../../../_model/state-model/r-sm/report-state";
import {ReportDefination} from "../../../../_model/level0/accounting-type-library/ehtml";
import {ReportDescriptionContainer} from "./report-description/report-description";
import {useReportComponentLoader} from "../../report-component-loader";
import ReportFormulaContainer from "./formula-container/report-formula-container";
import ReportHTMLContainer  from "./html-container/report-html-container";
import CButton from "../../../../shared/core/cbutton/cbutton";
import {
    EvaluateEHTML2Par,
    EvaluateEHTML3WebPar,
    SaveEHTMLDataPar
} from "../../../../_services/accounting.service";
import {PrintWrapper} from "../../../../shared/components/print-wrapper/print-wrapper";

const {TabPane} = Tabs;

export enum ReportTab {
    HTML = '1',
    FORMULA = '2',
    DESCRIPTION = '3'
}

export default function ReportContainer() {
    const dispatch = useDispatch()
    const printTriggerRef = createRef<Button>();
    const componentToPrintRef = createRef<HTMLDivElement>();

    const {reportTypeId, reportDefinitionId} = useParams()
    const [selectedTab, setSelectedTab] = useState<ReportTab>(ReportTab.FORMULA)
    const [auth, report_definition_state, report_state] = useSelector<ApplicationState, [AuthenticationState, ReportDefinitionState, ReportState]>(appState => [appState.auth, appState.reportDefinitions, appState.report_state])
    const [reportDefinition, setReportDefinition] = useState<ReportDefination>()
    const [reportBase] = useReportComponentLoader((reportTypeId && +reportTypeId) || -1)


    useEffect(() => {
        dispatch(clearReportHTML())
        if (report_definition_state.loading)
            dispatch(loadingReport(report_definition_state.message))
        if (reportTypeId && reportDefinitionId) {
            if (!report_definition_state.loading && !report_definition_state.error) {
                const definition = report_definition_state.definitions.find(def => def.id === +reportDefinitionId && def.reportTypeID === +reportTypeId)
                if (definition) {
                    const reportPar: FetchReportPar = {
                        sessionId: auth.sessionId,
                        definition
                    }
                    dispatch(fetchReport(reportPar))
                }
            }
        }
    }, [report_definition_state, reportTypeId, reportDefinitionId])
    useEffect(()=>{
        if(report_state.payload){
            setReportDefinition(report_state.payload.definition)
        }
    },[ report_state ])

    function handleGenerateReportClick() {
        setSelectedTab(ReportTab.HTML)
        if(reportDefinition){
            const evaluateEHTML2Par: EvaluateEHTML2Par = {
                sessionId: auth.sessionId,
                data: reportDefinition.formula,
                reportTypeID: reportDefinition.reportTypeID,
                parameter: reportBase.additionalFilterParams()
            }
            dispatch(requestEvaluate2(evaluateEHTML2Par))
        }
    }
    function handleReportDefinitionChange(newReportDefinition: ReportDefination) {
        setReportDefinition({...newReportDefinition})
    }
    function handleSaveReportDefinition(){
        if(reportDefinition){
            const saveEHTMLDataPar : SaveEHTMLDataPar = {
                sessionId: auth.sessionId,
                r: reportDefinition
            }
            dispatch(requestSaveReportDefinition(saveEHTMLDataPar))
        }
    }
    function handleEvaluateSymbol(symbol: string) {
        setSelectedTab(ReportTab.HTML);
        if (reportDefinition) {
            const evaluateEHTML3WebPar: EvaluateEHTML3WebPar = {
                sessionId: auth.sessionId,
                data: reportDefinition.formula,
                reportTypeID: reportDefinition.reportTypeID,
                symbol: symbol.toUpperCase(),
                parameter: reportBase.additionalFilterParams()
            }
            dispatch(requestEvaluateFormula3Web(evaluateEHTML3WebPar));
        }
    }


    return (
        <StateMachineHOC state={report_definition_state}>
            <StateMachineHOC state={report_state}>
                {
                    reportDefinition &&
                    <Row gutter={4}>
                        <Col lg={{span: 18}}>
                            <Tabs
                                className={'paper'}
                                activeKey={selectedTab}
                                onChange={(newSelectedTab) => setSelectedTab(newSelectedTab as ReportTab)}
                                style={{padding: '5px'}}
                                >
                                <TabPane tab="HTML" key={ReportTab.HTML}>
                                    <div style={{height: '80vh',maxHeight: '80vh', overflowY: 'auto', overflowX: 'hidden'}} >
                                        <div ref={componentToPrintRef}>
                                            <ReportHTMLContainer reportDefinition={reportDefinition}/>
                                        </div>
                                    </div>
                                </TabPane>
                                <TabPane tab="Report Formula" key={ReportTab.FORMULA}>
                                    <ReportFormulaContainer evaluateFormulaHandler={handleEvaluateSymbol}
                                                            handleReportDefinitionChange={handleReportDefinitionChange}
                                                            handleSaveReportDefinition={handleSaveReportDefinition}
                                                            reportDefinition={reportDefinition}/>
                                </TabPane>
                                <TabPane closable tab="Description" key={ReportTab.DESCRIPTION}>
                                    <ReportDescriptionContainer
                                        handleReportDefinitionChange={handleReportDefinitionChange}
                                        reportDefinition={reportDefinition}/>
                                </TabPane>
                            </Tabs>
                        </Col>



                        <Col lg={{span:6}}>
                            <div className={'paper'} style={{ padding:'10px',marginBottom:'5px' }}>
                                <Divider orientation={'left'}>Filters</Divider>
                                {reportBase.AppendedComponent()}
                            </div>
                            <div className={'paper'} style={{ padding:'5px' }}>
                                <Row gutter={4}>
                                    <Col lg={{span: 24}}>
                                        <PrintWrapper
                                            trigger={()=><Button
                                                            disabled={selectedTab !== ReportTab.HTML}
                                                            ref={printTriggerRef}
                                                            style={{ marginBottom:'5px',marginTop:'5px' }}
                                                            block >
                                                            Print Report
                                                        </Button>
                                            }
                                            content={()=>
                                                //@ts-ignore
                                                componentToPrintRef.current
                                            }/>
                                    </Col>
                                    <Col lg={{span: 24}}>
                                        <CButton style={{ marginBottom:'5px' }} block type={'primary'} onClick={handleGenerateReportClick}>
                                            Generate Report
                                        </CButton>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                }
            </StateMachineHOC>
        </StateMachineHOC>
    )
}