import React, {FC, useEffect, useState} from "react";
import {Col, Form, Input, Row} from "antd";
import {ReportDefination} from "../../../../../_model/level0/accounting-type-library/ehtml";
const { TextArea } = Input


interface ReportDescriptionContainerProps{
    reportDefinition: ReportDefination,
    handleReportDefinitionChange: ( newReportDefinition:ReportDefination )=> void
}

const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
}

export const ReportDescriptionContainer : FC<ReportDescriptionContainerProps>=(props)=>{
    const [ reportDefinition , setReportDefinition ] = useState<ReportDefination>(props.reportDefinition)
    useEffect(()=>{ props.handleReportDefinitionChange(reportDefinition)},[ reportDefinition ])
    return (
        <Row gutter={4}>
            <Col lg={{span:16,offset:4}} >
                <Form layout={'horizontal'}>
                    <Form.Item label="Name" {...formItemLayout}>
                        <Input
                            value={reportDefinition.name}
                            onChange={(event)=> setReportDefinition({...reportDefinition,name:event.target.value}) }
                        />
                    </Form.Item>
                    <Form.Item label="Stylesheets" {...formItemLayout}>
                        <Input
                            value={reportDefinition.styleSheet}
                            onChange={(event)=>setReportDefinition({...reportDefinition,styleSheet:event.target.value})}
                        />
                    </Form.Item>
                    <Form.Item label="Code" {...formItemLayout}>
                        <Input
                            value={reportDefinition.code}
                            onChange={(event)=>setReportDefinition({...reportDefinition,code:event.target.value})}
                        />
                    </Form.Item>
                    <Form.Item label="Description" {...formItemLayout}>
                        <TextArea
                            rows={8}
                            value={reportDefinition.description}
                            onChange={(event)=>setReportDefinition({...reportDefinition,description:event.target.value})}
                        />
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    )
}