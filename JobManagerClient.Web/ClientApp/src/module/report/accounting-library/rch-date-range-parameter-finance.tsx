import {ReportBase} from "../report-base";
import {MomentUtil} from "../../../_helpers/date-util";
import {DatePicker, Divider, Form} from "antd";
import React from "react";

const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

export class RCHDateRangeParameterFinance extends ReportBase {
    constructor() {
        super();
        this.appendedFilterParams = [
            { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() },
            { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() }
        ]
        this.appendedComponent =
            <div>
                <Form.Item label="From" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(0,dateString)} />
                </Form.Item>
                <Form.Item label="To" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(1,dateString)} />
                </Form.Item>
            </div>
    }
}