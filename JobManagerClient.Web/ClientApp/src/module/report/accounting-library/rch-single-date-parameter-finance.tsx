import {ReportBase} from "../report-base";
import {JobTypeSelector} from "../job-manager-library/rf-single-period/job-type-selector";
import React from "react";
import {DatePicker, Form} from "antd";
import {MomentUtil} from "../../../_helpers/date-util";


const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

export class RCHSingleDateParameterFinance extends ReportBase {
    constructor() {
        super();
        this.appendedFilterParams = [ { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() } ]
        this.appendedComponent =
            <div>
                <Form.Item label="Date" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(0,dateString)} />
                </Form.Item>
            </div>
    }
}