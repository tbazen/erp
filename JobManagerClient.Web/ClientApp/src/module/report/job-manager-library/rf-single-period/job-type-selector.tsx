import {Form, Select} from "antd";
import React,{ FC } from "react";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {JobRuleStateModel} from "../../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {useSelector} from "react-redux";
const { Option } = Select

interface JobTypeSelectorProps{
    handleSelectedJobChange: ( selectedJobType: number ) => void
}
const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}
export const JobTypeSelector:FC<JobTypeSelectorProps> = (props ) => {
    const [ job_state ] = useSelector<ApplicationState, [ JobRuleStateModel  ] >(appState => [ appState.job_rule ])

    return (
        <Form.Item label="Period" {...formItemLayout}>
            <Select defaultValue={-1} style={{width:'100%'}} onChange={(selectedJob: any)=>{ props.handleSelectedJobChange(selectedJob) }}>
                <Option value={-1} >All Jobs</Option>
                { job_state.clientHandlers.map((clientHandler,key) => <Option key={key} value={clientHandler.meta.jobType}>{clientHandler.meta.jobName}</Option>) }
            </Select>
        </Form.Item>
    )
}