import React from "react";
import {ReportBase} from "../../report-base";
import {JobTypeSelector} from "./job-type-selector";

export class RFSinglePeriod extends ReportBase {
    constructor() {
        super();
        this.appendedFilterParams = [ { typeName:'System.Int32,mscorlib',val:-1 } ]
        this.appendedComponent =
            <div>
                <JobTypeSelector handleSelectedJobChange={(selectedJobType: number)=>this.handleFilterParamsChange(0,selectedJobType)}/>
            </div>
    }
}