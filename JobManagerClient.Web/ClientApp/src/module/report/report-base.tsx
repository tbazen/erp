import React, { Fragment, ReactNode} from "react";
import {TypeObject} from "../../_model/level0/application-server-library/type-object";

export class ReportBase {
    protected appendedComponent: ReactNode = <Fragment/>
    protected appendedFilterParams: TypeObject[] = []
    protected handleFilterParamsChange = (paramIndex: number,value: any) => {
        this.appendedFilterParams[paramIndex].val = value
    }

    public additionalFilterParams = ()=> this.appendedFilterParams;
    public AppendedComponent=()=> this.appendedComponent;
}
