import {ReportBase} from "./report-base";
import {RFSinglePeriod as JobManagerRFSinglePeriod} from "./job-manager-library/rf-single-period/rf-single-period";
import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../_model/state-model/application-state";
import {ReportTypeState} from "../../_model/state-model/r-sm/report-types-state";
import {AuthenticationState} from "../../_model/state-model/auth-state";
import {GetAllReportTypesPar} from "../../_services/accounting.service";
import {fetchReportTypes} from "../../_redux_setup/actions/report-actions/report-actions";
import {NAME_SPACES} from "../../_constants/model-namespaces";
import {RCHSingleDateParameterFinance as AccountingRCHSingleDateParameterFinance } from "./accounting-library/rch-single-date-parameter-finance";
import {RCHDateRangeParameterFinance as AccountingRCHDateRangeParameterFinance} from "./accounting-library/rch-date-range-parameter-finance";
import {ECHSalePointSummary as SubscriberManagerECHSalePointSummary} from "./subscriber-managment-library/ech-sale-point-summary";
import {ECHSinglePeriod as SubscriberManagerECHSinglePeriod} from "./subscriber-managment-library/ech-single-period";
import {ECHSinglePeriodDateRange as SubscriberManagerECHSinglePeriodDateRange} from "./subscriber-managment-library/ech-single-period-date-range";
import {ECHPeriodRange as SubscriberManagerECHPeriodRange} from "./subscriber-managment-library/ech-period-range";
import {ECHReaderPeriod as SubscriberManagerECHReaderPeriod} from "./subscriber-managment-library/ech-reader-period";
import {ECHKebelePeriod as SubscriberManagerECHKebelePeriod} from "./subscriber-managment-library/ech-kebele-period";
import {ECHPaymentCenterPeriod as SubscriberManagerECHPaymentCenterPeriod} from "./subscriber-managment-library/ech-payment-center-period";

export function useReportComponentLoader(reportTypeId: number): [ReportBase] {
    const dispatch = useDispatch()
    const [ reportBase , setReportBase ] = useState<ReportBase>(new ReportBase())
    const [auth, report_type_state ] = useSelector<ApplicationState,[ AuthenticationState , ReportTypeState ]>(appState=> [ appState.auth, appState.report_type_state ])
    useEffect(()=>{
        if(report_type_state.reportTypes.length === 0 && !report_type_state.loading && !report_type_state.error) {
            const reportTypesPar : GetAllReportTypesPar = {
                sessionId: auth.sessionId
            }
            dispatch(fetchReportTypes(reportTypesPar))
        }
        else if(!report_type_state.loading && !report_type_state.error && report_type_state.reportTypes.length > 0 ){
            updateReportType(reportTypeId)
        }
    },[ report_type_state ])
    useEffect(()=>{
        updateReportType(reportTypeId)
    },[ reportTypeId ])


    function updateReportType(reportTypeId: number){
        const reportType = report_type_state.reportTypes.find(rt=> rt.id === reportTypeId)
        if(reportType){
            const reportComponent = findReportComponentByNameSpace(reportType.clientClass)
            setReportBase(reportComponent)
        }
    }


    function findReportComponentByNameSpace(nameSpace: string): ReportBase {
        switch(nameSpace){
            case NAME_SPACES.REPORT.CLIENT.JOB_MANAGER_LIBRARY.ECH_JOB_TYPE:
            case NAME_SPACES.REPORT.CLIENT.JOB_MANAGER_LIBRARY.RF_SINGLE_PERIOD:
                return new JobManagerRFSinglePeriod()

            case NAME_SPACES.REPORT.CLIENT.ACCOUNTING_LIBRARY.RCH_SINGLE_DATE_PARAMETER_FINANCE:
                return new AccountingRCHSingleDateParameterFinance()
            case NAME_SPACES.REPORT.CLIENT.ACCOUNTING_LIBRARY.RCH_DATE_RANGE_PARAMETER_FINANCE:
                return new AccountingRCHDateRangeParameterFinance()
            case NAME_SPACES.REPORT.CLIENT.ACCOUNTING_LIBRARY.RCH_NO_PARAMETER_FINANCE:
                return new ReportBase()




            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_SALE_POINT_SUMMERY:
                return new SubscriberManagerECHSalePointSummary()
            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_SINGLE_PERIOD:
                return new SubscriberManagerECHSinglePeriod();
            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_SINGLE_PERIOD_DATE_RANGE:
                return new SubscriberManagerECHSinglePeriodDateRange()
            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_PERIOD_RANGE:
                return new SubscriberManagerECHPeriodRange()
            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_READER_PERIOD:
                return new SubscriberManagerECHReaderPeriod()
            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_KEBELE_PERIOD:
                return new SubscriberManagerECHKebelePeriod();
            case NAME_SPACES.REPORT.CLIENT.SUBSCRIBER_MANAGEMENT_LIBRARY.ECH_PAYMENT_CENTER_PERIOD:
                return new SubscriberManagerECHPaymentCenterPeriod()

            default:
                return new ReportBase();
        }
    }

    return [reportBase ]
}