import {jobStyle} from "./job-style";
import {AppendBERPStyle} from "./berp-style";

enum ReportStyles{
    JOB_STYLE="jobstyle.css",
    BERP_STYLE='berp.css'
}

export function appendReportStyle(styleSheet: string, htmlString: string){
    return reportStyleFactory(styleSheet,htmlString)
}

function reportStyleFactory (styleSheet : string, htmlString : string): string {
    switch(styleSheet.toLowerCase()){
        case ReportStyles.JOB_STYLE:
            return jobStyle(htmlString)
        case ReportStyles.BERP_STYLE:
            return AppendBERPStyle(htmlString)
        default :
            return htmlString;
    }
}
