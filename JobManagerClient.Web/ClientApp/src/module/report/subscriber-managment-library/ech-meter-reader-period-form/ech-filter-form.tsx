import React, {FC, useEffect, useState} from "react";
import {Form, Select} from "antd";
import {SinglePeriodSelector} from "../rf-single-period/single-period-selector";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {MeterReaderEmployeeState} from "../../../../_model/state-model/r-sm/meter-reader-employee-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {StateMachineHOC} from "../../../../shared/hoc/StateMachineHOC";
import {GetAllMeterReaderEmployeesPar} from "../../../../_services/subscribermanagment.service";
import {fetchAllMeterReaderEmployees} from "../../../../_redux_setup/actions/report-actions/report-actions";
const { Option } = Select

interface MeterReaderSelectorProps{
    handleSelectedPeriodChange:(selectedPeriodId: number )=> void
    handleSelectedMeterReaderEmployeeChange:( meterReaderEmployeeId: number )=> void
}


const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}


export const ECHFilterForm:FC<MeterReaderSelectorProps>=(props)=>{
    const dispatch = useDispatch()
    const [ selectedPeriodId , setSelectedPeriodId ] = useState<number>(-1)
    const [ selectedMeterReaderEmployeeId, setSelectedMeterReaderEmployeeId ] = useState<number>(-1)
    const [ auth, meter_reader_employee_state ] = useSelector<ApplicationState,[ AuthenticationState, MeterReaderEmployeeState ]>(appState=> [ appState.auth,appState.mater_reader_employee_state ])

    useEffect(()=>{
        props.handleSelectedPeriodChange(selectedPeriodId)
        if(selectedPeriodId !== -1){
            const meterReaderEmployeesPar: GetAllMeterReaderEmployeesPar = {
                sessionId: auth.sessionId,
                periodID: selectedPeriodId
            }
            dispatch(fetchAllMeterReaderEmployees(meterReaderEmployeesPar))
        }
        else
            setSelectedMeterReaderEmployeeId(-1)
    },[ selectedPeriodId ])
    useEffect(()=>{ props.handleSelectedMeterReaderEmployeeChange(selectedMeterReaderEmployeeId) },[ selectedMeterReaderEmployeeId ])

    function handleSelectedPeriodChange(selectedPeriodID: number){ setSelectedPeriodId(selectedPeriodID) }

    return (
        <div>
            <SinglePeriodSelector
                handleSelectedPeriodChange={handleSelectedPeriodChange}/>
            <StateMachineHOC state={ meter_reader_employee_state }>
                <Form.Item label="Period" {...formItemLayout}>
                    <Select defaultValue={-1} style={{width:'100%'}} onChange={(meterReaderEmployeeId: any)=>{ setSelectedMeterReaderEmployeeId(meterReaderEmployeeId) }}>
                        { meter_reader_employee_state.payload.map((readerEmploee,key) => <Option key={key} value={readerEmploee.employeeID}>{(readerEmploee.emp && readerEmploee.emp.employeeNameID) || readerEmploee.employeeID}</Option>) }
                    </Select>
                </Form.Item>
            </StateMachineHOC>
        </div>
    );
}