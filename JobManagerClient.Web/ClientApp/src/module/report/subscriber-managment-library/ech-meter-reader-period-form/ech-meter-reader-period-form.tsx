import {ReportBase} from "../../report-base";
import {SinglePeriodSelector} from "../rf-single-period/single-period-selector";
import React from "react";
import {ECHFilterForm} from "./ech-filter-form";

export class ECHMeterReaderPeriodForm extends ReportBase {
    constructor() {
        super();
        this.appendedFilterParams = [
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
        ]
        this.appendedComponent = (
            <div>
                <ECHFilterForm
                    handleSelectedPeriodChange={(selectedPeriodId)=>this.handleFilterParamsChange(0,selectedPeriodId)}
                    handleSelectedMeterReaderEmployeeChange={(meterReaderEmployeeId => { this.handleFilterParamsChange(1,meterReaderEmployeeId) })}/>
            </div>
        )
    }
}