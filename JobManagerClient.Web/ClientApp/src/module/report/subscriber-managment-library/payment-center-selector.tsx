import {Form, Select} from "antd";
import React, {FC, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {PaymentCenterState} from "../../../_model/state-model/mn-sm/internal-service-sm/payment-center-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {GetAllPaymentCentersPar} from "../../../_services/subscribermanagment.service";
import {StateMachineHOC} from "../../../shared/hoc/StateMachineHOC";
import {fetchPaymentCenters} from "../../../_redux_setup/actions/mn-actions/main-actions";
const { Option } = Select

interface PaymentCenterSelectorProps{
    handleSelectedPaymentCenterChange: ( selectedJobType: number ) => void
}
const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}
export const PaymentCenterSelector:FC<PaymentCenterSelectorProps> = (props ) => {
    const dispatch = useDispatch()
    const [ auth, payment_center_state ] = useSelector<ApplicationState, [ AuthenticationState ,PaymentCenterState  ] >(appState => [ appState.auth,appState.payment_center_state ])
    useEffect(()=>{
        if(payment_center_state.payment_centers.length === 0){
            const paymentCenterPar:GetAllPaymentCentersPar = {sessionId: auth.sessionId}
            dispatch(fetchPaymentCenters(paymentCenterPar))
        }
    },[])
    return (
        <StateMachineHOC state={payment_center_state}>
            <Form.Item label="Payment Center" {...formItemLayout}>
                <Select style={{width:'100%'}} onChange={(selectedPaymentCenter: any)=>{ props.handleSelectedPaymentCenterChange(selectedPaymentCenter) }}>
                    { payment_center_state.payment_centers.map((paymentCenter,key) => <Option key={key} value={paymentCenter.id}>{paymentCenter.centerName}</Option>) }
                </Select>
            </Form.Item>
        </StateMachineHOC>
    )
}