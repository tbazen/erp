import React from "react";
import {RFPaymentCenterPeriod as SubscriberManagerRFPaymentCenterPeriod } from "./rf-payment-center-period";
import {SinglePeriodSelector} from "./rf-single-period/single-period-selector";


export class RFPaymentCenterDateRangePeriod extends SubscriberManagerRFPaymentCenterPeriod {
    constructor() {
        super();
        this.appendedComponent =
            <div>
                { this.appendedComponent }
                <SinglePeriodSelector handleSelectedPeriodChange={(selectedPeriodId => { this.handleFilterParamsChange(3,selectedPeriodId) })}/>
            </div>
    }

}