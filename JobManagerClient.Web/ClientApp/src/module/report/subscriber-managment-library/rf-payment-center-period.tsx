import React from "react";
import {ReportBase} from "../report-base";
import {MomentUtil} from "../../../_helpers/date-util";
import {DatePicker, Divider, Form} from "antd";
import {PaymentCenterSelector} from "./payment-center-selector";

const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

export class RFPaymentCenterPeriod extends ReportBase{
    constructor() {
        super();
        this.appendedFilterParams = [
            { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() },
            { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
        ]
        this.appendedComponent =
            <div>
                <Form.Item label="From" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(0,dateString)} />
                </Form.Item>
                <Form.Item label="To" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(1,dateString)} />
                </Form.Item>
                <PaymentCenterSelector handleSelectedPaymentCenterChange={(selectedPaymentCenter: number)=>this.handleFilterParamsChange(2,selectedPaymentCenter)}/>
            </div>
    }
}