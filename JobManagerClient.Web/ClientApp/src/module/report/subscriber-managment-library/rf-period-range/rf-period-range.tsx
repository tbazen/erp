import {ReportBase} from "../../report-base";
import {CurrentPaymentCenter} from "../rf-single-period/current-payment-center";
import {SinglePeriodSelector} from "../rf-single-period/single-period-selector";
import React from "react";
import {Divider} from "antd";

export class RFPeriodRange extends ReportBase{
    constructor() {
        super();
        this.appendedFilterParams = [
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
        ]
        this.appendedComponent = (
            <div>
                <CurrentPaymentCenter
                    handleCurrentPaymentCenterChange={
                        (currentPaymentCenterId)=>this.handleFilterParamsChange(0,currentPaymentCenterId)
                    } />
                <Divider orientation={'left'}>From</Divider>
                <SinglePeriodSelector
                    handleSelectedPeriodChange={
                        (selectedPeriodId)=>{
                            this.handleFilterParamsChange(2,selectedPeriodId);
                        }}/>
                <Divider orientation={'left'}>To</Divider>
                <SinglePeriodSelector
                    handleSelectedPeriodChange={
                        (selectedPeriodId)=>{
                            this.handleFilterParamsChange(3,selectedPeriodId);
                        }}/>
            </div>
        )
    }
}