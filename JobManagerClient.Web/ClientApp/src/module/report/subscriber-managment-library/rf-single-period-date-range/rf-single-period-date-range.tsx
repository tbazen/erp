import {ReportBase} from "../../report-base";
import {CurrentPaymentCenter} from "../rf-single-period/current-payment-center";
import {SinglePeriodSelector} from "../rf-single-period/single-period-selector";
import React from "react";
import {DatePicker, Form} from "antd";
import {MomentUtil} from "../../../../_helpers/date-util";


const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}


export class RFSinglePeriodDateRange extends ReportBase {
    constructor(){
        super();
        this.appendedFilterParams = [
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() },
            { typeName:'System.DateTime,mscorlib',val:MomentUtil.getCurrentDateTime() }
        ]
        this.appendedComponent = (
            <div>
                <CurrentPaymentCenter
                    handleCurrentPaymentCenterChange={
                        (currentPaymentCenterId)=>this.handleFilterParamsChange(0,currentPaymentCenterId)
                    } />
                <SinglePeriodSelector
                    handleSelectedPeriodChange={
                        (selectedPeriodId)=>{
                            this.handleFilterParamsChange(2,selectedPeriodId);
                            this.handleFilterParamsChange(3,selectedPeriodId);
                        }}/>
                <Form.Item label="From" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(4,dateString)} />
                </Form.Item>
                <Form.Item label="To" {...formItemLayout}>
                    <DatePicker style={{width:'100%'}} showTime defaultValue={MomentUtil.getCurrentDateTime()} placeholder="Select Date & Time" onChange={(moment,dateString) => this.handleFilterParamsChange(5,dateString)} />
                </Form.Item>
            </div>
        )
    }
}