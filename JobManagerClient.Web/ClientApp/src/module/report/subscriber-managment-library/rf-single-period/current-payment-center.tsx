import React, {FC, Fragment, useEffect, useState} from 'react'
import {useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import SubscriberManagementService, {GetCurrentPaymentCenterPar} from "../../../../_services/subscribermanagment.service";
import {PaymentCenter} from "../../../../_model/level0/subscriber-managment-type-library/payment-center";

const subscriber_mgr_service = new SubscriberManagementService()

interface CurrentPaymentCenterProps{
    handleCurrentPaymentCenterChange:(currentPaymentCenterId: number)=> void
}

export const CurrentPaymentCenter: FC<CurrentPaymentCenterProps> = (props)=>{
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=> [ appState.auth ])
    const [ currentPaymentCenterId, setCurrentPaymentCenterId ] = useState<number>(-1)
    useEffect(()=>{
        fetchCurrentpaymentCenter()
    },[  ])

    useEffect(()=>{
        props.handleCurrentPaymentCenterChange(currentPaymentCenterId)
    },[currentPaymentCenterId])
    async function fetchCurrentpaymentCenter(){
        try{
            const currentPaymentCenterPar : GetCurrentPaymentCenterPar ={
                sessionId: auth.sessionId
            }
            const currentPaymentCenter:PaymentCenter = (await subscriber_mgr_service.GetCurrentPaymentCenter(currentPaymentCenterPar)).data
            if(currentPaymentCenter) {
                setCurrentPaymentCenterId(currentPaymentCenter.id)
            }
        }catch(error){

        }
    }
    return <Fragment/>
}