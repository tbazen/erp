import React from 'react'
import {ReportBase} from "../../report-base";
import {SinglePeriodSelector} from "./single-period-selector";
import {CurrentPaymentCenter} from "./current-payment-center";

export class RFSinglePeriod extends ReportBase {
    constructor() {
        super();
        this.appendedFilterParams = [
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
            { typeName:'System.Int32,mscorlib',val:-1 },
        ]
        this.appendedComponent = (
            <div>
                <CurrentPaymentCenter
                    handleCurrentPaymentCenterChange={
                        (currentPaymentCenterId)=>this.handleFilterParamsChange(0,currentPaymentCenterId)
                    } />
                <SinglePeriodSelector
                    handleSelectedPeriodChange={
                        (selectedPeriodId)=>{
                            this.handleFilterParamsChange(2,selectedPeriodId);
                            this.handleFilterParamsChange(3,selectedPeriodId);
                        }}/>
            </div>
        )
    }
}