import React, {FC, useEffect, useState} from 'react'
import {Col, Form, Row, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {currentYear, years} from "../../../../_helpers/date-util";
import {GetBillPeriodsPar} from "../../../../_services/subscribermanagment.service";
import {fetchBillPeriods} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {BillPeriodState} from "../../../../_model/state-model/mn-sm/bill-period-state";
const { Option } = Select

const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

interface SinglePeriodSelectorProps{
    handleSelectedPeriodChange: (selectedPeriodId: number)=> void
}

export const SinglePeriodSelector:FC<SinglePeriodSelectorProps> =(props)=>{
    const dispatch = useDispatch()
    const [ auth, period_state ] = useSelector<ApplicationState, [ AuthenticationState,BillPeriodState ]>(appState => [ appState.auth,appState.bill_period_state ])
    const [ year , setYear ] = useState<number>(currentYear);

    useEffect(()=>{
        const params: GetBillPeriodsPar ={
            year,
            sessionId:auth.sessionId,
            ethiopianYear:true
        }
        dispatch(fetchBillPeriods(params))
    },[ year ])
    return (
        <Row gutter={2}>
            <Col lg={{span:16}}>
                <Form.Item label="Period" {...formItemLayout}>
                    <Select style={{width:'100%'}} onChange={(selectedPeriodId: any)=>{ props.handleSelectedPeriodChange(selectedPeriodId) }}>
                        { period_state.payload.map((period,key) => <Option key={key} value={period.id}>{period.name}</Option>) }
                    </Select>
                </Form.Item>
            </Col>
            <Col span={8}>
                <Form.Item label="Period" {...formItemLayout}>
                    <Select defaultValue={year} onChange={(year)=>setYear(+year)} style={{width:'100%'}} disabled={period_state.loading}>
                        { years.map((value,key)=><Option key={key} value={value}>{value}</Option>) }
                    </Select>
                </Form.Item>
            </Col>
        </Row>
    )
}