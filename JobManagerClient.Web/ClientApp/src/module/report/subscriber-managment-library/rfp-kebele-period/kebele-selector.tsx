import React, {FC, useEffect} from 'react'
import {Col, Form, Row, Select} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {GetAllKebelesPar} from "../../../../_services/subscribermanagment.service";
import { fetchKebeleList} from "../../../../_redux_setup/actions/mn-actions/main-actions";
import {KebeleState} from "../../../../_model/state-model/mn-sm/kebele-state";
const { Option } = Select

const formItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}

interface SinglePeriodSelectorProps{
    handleSelectedKebeleChange: (selectedKebeleId: number)=> void
}

export const KebeleSelector:FC<SinglePeriodSelectorProps> =(props)=>{
    const dispatch = useDispatch()
    const [ auth, kebele_state ] = useSelector<ApplicationState, [ AuthenticationState,KebeleState ]>(appState => [ appState.auth,appState.kebele_state ])
    useEffect(()=>{
        const params: GetAllKebelesPar ={ sessionId:auth.sessionId }
        dispatch(fetchKebeleList(params))
    },[  ])
    return (
        <Form.Item label="Kebele" {...formItemLayout}>
            <Select style={{width:'100%'}} onChange={(selectedKebeleId: any)=>{ props.handleSelectedKebeleChange(selectedKebeleId) }}>
                { kebele_state.kebeles.map((kebele,key) => <Option key={key} value={kebele.id}>{kebele.name}</Option>) }
            </Select>
        </Form.Item>
    )
}