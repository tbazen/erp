import {ReportBase} from "../../report-base";
import {CurrentPaymentCenter} from "../rf-single-period/current-payment-center";
import {SinglePeriodSelector} from "../rf-single-period/single-period-selector";
import React from "react";
import {KebeleSelector} from "./kebele-selector";

export class RFPKebelePeriod extends ReportBase {
    constructor() {
        super();
        this.appendedFilterParams = [
            {typeName: 'System.Int32,mscorlib', val: -1},
            {typeName: 'System.Int32,mscorlib', val: -1},
            {typeName: 'System.Int32,mscorlib', val: -1},
            {typeName: 'System.Int32,mscorlib', val: -1},
        ]
        this.appendedComponent = (
            <div>
                <CurrentPaymentCenter
                    handleCurrentPaymentCenterChange={
                        (currentPaymentCenterId) => this.handleFilterParamsChange(0, currentPaymentCenterId)
                    }/>
                <KebeleSelector handleSelectedKebeleChange={(selectedKebeleId => this.handleFilterParamsChange(1,selectedKebeleId))}/>
                <SinglePeriodSelector
                    handleSelectedPeriodChange={
                        (selectedPeriodId) => {
                            this.handleFilterParamsChange(2, selectedPeriodId);
                            this.handleFilterParamsChange(3, selectedPeriodId);
                        }}/>
            </div>
        )
    }
}