import React, {Fragment, useEffect} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {BottomNavigation, BottomNavigationAction, IconButton} from "@material-ui/core";
import {Add, Apps, List} from "@material-ui/icons";
import CTreeView from "../tree/CTree";


function AccountPicker() {
    const [open, setOpen] = React.useState(false);
    const [value,setValue] = React.useState(0)


    function handleClickOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <span>
            <IconButton aria-label="Delete"  onClick={handleClickOpen} >
              <Apps fontSize="large" />
            </IconButton>
            <Dialog open={open} onClose={handleClose} fullWidth={true} maxWidth={'sm'}>
                <DialogTitle id="form-dialog-title">Account Picker</DialogTitle>
                <DialogContent>
                    {
                        value === 0 &&
                        <Fragment>
                            <CTreeView/>
                        </Fragment>
                    }
                    {
                        value === 1 &&
                        <p>Account Form Comes Here</p>
                    }
                </DialogContent>
                <DialogActions>
                    <BottomNavigation
                        value={value}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                        style={{width:'96%',margin:'auto',backgroundColor:'lightgray'}}
                        showLabels
                    >

                      <BottomNavigationAction label="Select Acciount" icon={<List />} />
                      <BottomNavigationAction label="Create New Account" icon={<Add />} />
                    </BottomNavigation>
                </DialogActions>
            </Dialog>
        </span>
    );
}

export default AccountPicker;
