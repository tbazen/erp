import {FormGroup} from "@material-ui/core";
import {TextFieldProps} from "@material-ui/core/TextField";
import TextField from "@material-ui/core/TextField/TextField";
import React from "react";
import AccountPicker from "../account picker/account-picker";


const BrowseTextField =(props:TextFieldProps)=>{
    return (
            <FormGroup row>
                <TextField
                    margin="dense"
                    type="text"
                    fullWidth
                    {...props}
                    InputProps={{
                        endAdornment: <AccountPicker/>,
                    }}
                />
            </FormGroup>
    )
}

export default BrowseTextField