import {IButtonGrid} from "./button-grid";
import {ButtonGridIcons} from "./button-grid-icons";
import {IClientHandler} from "../../../_model/job-rule-model/client-handler";

export function constructButtonGridItem(clientHandler:IClientHandler) : IButtonGrid{
    const buttonGridItem: IButtonGrid  = {
        imgUrl : ButtonGridIcons[clientHandler.meta.jobType].iconUrl,
        title : clientHandler.meta.jobName,
        path : clientHandler.meta.route
    }
    return buttonGridItem;
}