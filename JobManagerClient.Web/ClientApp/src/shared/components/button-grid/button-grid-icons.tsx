import {
    BillingCreditIcon,
    BillingDepositIcon, CashhandoverIcon,
    CloseCreditIcon, ConnectionPenalityIcon,
    CustomerListIcon, CustomerSponsoredNetworkIcon,
    DeleteBillsIcon, EditCustomerDataIcon,
    ExcemptBillIcon, GeneralCellIcon, HydrantServiceIcon,
    IconDisconnectLinesIcon,
    IconReconnectLineIcon,
    InternalServicesIcon, LiquidDisposalIcon, MaintenanceIcon,
    NewLineIcon,
    OtherServicesIcon,
    ReadingCorrectionIcon,
    ReturnMeterIcon,
    TransferConnectionIcon, VoidReceiptIcon,
    WorkerListIcon
} from "../../../static/images/index-imgs";

export enum IconTypes {
    /**
     *  icons for standard job types
     */
    NEW_LINE = 1,
    GENERAL_SELL = 97,
    CONNECTION_PENALITY = 20,
    EXEMPT_BILL_ITEMS = 19,
    CLOSE_CREDIT_SCHEME = 18,
    CUSTOMER_SPONSORED_NETWORK_WORK = 17,
    HYDRANT_SERVICE = 16,
    LIQUID_WASTE_DISPOSAL = 15,
    BATCH_DELETE_BILLS = 14,
    BATCH_RECONNECT = 13,
    OTHER_NONE_TECHNICAL_SERVICES = 98,
    BATCH_DISCONNECT = 12,
    VOID_RECEIPT = 10,
    BILLING_CERDIT = 9,
    BILLING_DEPOSIT = 8,
    READING_CORRECTION = 7,
    EDIT_CUTOMER_DATA = 6,
    RETURN_METER = 5,
    CONNECTION_MAINTENANCE = 4,
    CONNECTION_OWNERSHIP_TRANSFER = 3,
    LINE_MAINTENANCE = 2,
    CASH_HANDOVER = 11,
    OTHER_TECHNICAL_SERVICES = 99,

    /**
     * icons for button grid elements but not standad job types
     */
    CUSTOMER_LIST,
    WORKER_LIST,

}

export const ButtonGridIcons :{ [jobtype :number] : { iconUrl:string } } = {
    [ IconTypes.NEW_LINE] : { iconUrl : NewLineIcon },
    [ IconTypes.GENERAL_SELL] : { iconUrl : GeneralCellIcon },
    [ IconTypes.CONNECTION_PENALITY ] : { iconUrl : ConnectionPenalityIcon},
    [ IconTypes.EXEMPT_BILL_ITEMS ] : { iconUrl : ExcemptBillIcon },
    [ IconTypes.CLOSE_CREDIT_SCHEME ] : { iconUrl : CloseCreditIcon },
    [ IconTypes.CUSTOMER_SPONSORED_NETWORK_WORK ] : { iconUrl : CustomerSponsoredNetworkIcon},
    [ IconTypes.HYDRANT_SERVICE ] : { iconUrl : HydrantServiceIcon },
    [ IconTypes.LIQUID_WASTE_DISPOSAL ] : { iconUrl : LiquidDisposalIcon },
    [ IconTypes.BATCH_DELETE_BILLS ] : { iconUrl: DeleteBillsIcon },
    [ IconTypes.BATCH_RECONNECT ] : { iconUrl : IconReconnectLineIcon },
    [ IconTypes.OTHER_NONE_TECHNICAL_SERVICES ] : { iconUrl : InternalServicesIcon },
    [ IconTypes.BATCH_DISCONNECT ] : { iconUrl : IconDisconnectLinesIcon },
    [ IconTypes.VOID_RECEIPT ] : { iconUrl : VoidReceiptIcon },
    [ IconTypes.BILLING_CERDIT ] : { iconUrl : BillingCreditIcon },
    [ IconTypes.BILLING_DEPOSIT ] : { iconUrl : BillingDepositIcon },
    [ IconTypes.READING_CORRECTION ] : { iconUrl : ReadingCorrectionIcon },
    [ IconTypes.EDIT_CUTOMER_DATA ] : { iconUrl : EditCustomerDataIcon },
    [ IconTypes.RETURN_METER ] : { iconUrl : ReturnMeterIcon },
    [ IconTypes.CONNECTION_MAINTENANCE ] : { iconUrl : MaintenanceIcon },
    [ IconTypes.CONNECTION_OWNERSHIP_TRANSFER ] : { iconUrl : TransferConnectionIcon },
    [ IconTypes.LINE_MAINTENANCE ] : { iconUrl : MaintenanceIcon },
    [ IconTypes.CASH_HANDOVER ] : { iconUrl : CashhandoverIcon },
    [ IconTypes.OTHER_TECHNICAL_SERVICES ] : { iconUrl : OtherServicesIcon  },

    [ IconTypes.CUSTOMER_LIST ] : { iconUrl : CustomerListIcon },
    [ IconTypes.WORKER_LIST ] : { iconUrl : WorkerListIcon }
}
