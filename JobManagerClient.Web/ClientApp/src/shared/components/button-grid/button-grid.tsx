import React from 'react';
import {Link} from "react-router-dom";
import {Card} from "antd";
export interface  IButtonGrid {
    imgUrl:string
    title:string
    path:string
}
const { Meta } = Card;
const styledTitle = (title: string) => <p style={{ textAlign: 'center', color: '#444', fontWeight: 'bold' }}>{title}</p>
const ButtonGrid = (props:IButtonGrid)=> {
    return (
        <Link to={props.path}>
            <Card
                hoverable
                cover={<img style={{width:'20%',paddingTop:'50px',margin:'auto'}} alt={props.title} src={props.imgUrl} />}
                style={{ height: '200px', maxHeight: '250px' }}
                bordered={false} >
                <Meta description={styledTitle(props.title)}/>
            </Card>
        </Link>
    );
}
export default ButtonGrid