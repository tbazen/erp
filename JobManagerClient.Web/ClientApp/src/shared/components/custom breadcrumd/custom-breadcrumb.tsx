import React from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {Breadcrumb} from "antd";

//@ts-ignore
function itemRender(route, params, routes, paths) {
    const last = routes.indexOf(route) === routes.length - 1;
    return last ? <span>{route.breadcrumbName}</span> : <Link to={route.path}>{route.breadcrumbName}</Link>;
}
const CustomBreadcrumd = ()=>{
    const bcr = useSelector((appState:ApplicationState)=>appState.breadcrumb)
    return (<Breadcrumb itemRender={itemRender}  routes={bcr.paths}/> )
}

export default CustomBreadcrumd