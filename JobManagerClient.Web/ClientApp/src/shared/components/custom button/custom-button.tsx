import React from 'react'
import {Button} from "@material-ui/core";
import {ButtonProps} from "@material-ui/core/Button";
import {createStyles, makeStyles, Theme} from "@material-ui/core";

export const initButtonStyle = makeStyles((theme: Theme) =>
    createStyles({
        border:{
            borderRadius:'1px',
        }
    })
);


const CMUIButton =(props:ButtonProps) =>{
    const classes = initButtonStyle()
    return (
        <Button {...props} className={classes.border} >
            {props.children}
        </Button>
    )
}

export default CMUIButton