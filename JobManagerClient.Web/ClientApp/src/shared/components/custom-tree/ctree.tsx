import React, {useState} from "react";
import { TreeSelect } from 'antd';
import {TreeNode} from "antd/es/tree-select";

interface IProps{
    list : TreeNode[]
    onSelect : (value:string ) => void
}

export function Ctree(props : IProps){
    const [state ,setState]  = useState<string>()
    const onChange = (value:string) => {
        setState(value)
        props.onSelect(value)
    };
    return (
        <TreeSelect
            style={{width:'100%'}}
            value={state}
            loading={true}
            dropdownStyle={{ maxHeight: '40vh', overflow: 'auto' }}
            treeData={props.list}
            placeholder="Please select"
            treeDefaultExpandAll
            onChange={onChange}
        />
    );

}
