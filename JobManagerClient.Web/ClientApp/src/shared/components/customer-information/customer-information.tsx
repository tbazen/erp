import React, { useEffect } from 'react'
import {Subscriber, SubscriberSearchResult} from '../../../_model/view_model/mn-vm/subscriber-search-result';
import {Row, Col, Typography, Divider} from 'antd';
import {lookupKebele, lookupSubscriberType} from '../../../_helpers/table-lookup-util';
import { useSelector, useDispatch } from 'react-redux';
import { KebeleState } from '../../../_model/state-model/mn-sm/kebele-state';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { fetchKebeleList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { AuthenticationState } from '../../../_model/state-model/auth-state';


const { Title,Text } = Typography;
const DataRow = ({title,content}:{title:string,content:string})=>{
    return (
        <Row>
            <Col span={9}>
                <Text strong>{title}</Text>
            </Col>
            <Col span={12} offset={1}>
                <Text>{content}</Text>
            </Col>
        </Row>
    )
}

export const SubscriberInformationContainer  = (props : Subscriber)=>{
    const [kebeleState,auth] = useSelector<ApplicationState,[KebeleState,AuthenticationState]>((appState)=>[appState.kebele_state,appState.auth])
    const dispatch = useDispatch()
    useEffect(() => {
        kebeleState.kebeles.length === 0 &&
        dispatch(fetchKebeleList({sessionId:auth.sessionId}))
    }, [])

    return (
        <div>
            {/*<Text strong style={{textDecoration:'underline'}}>Subscriber Information</Text>*/}
            <DataRow
                title={'Account Id '}
                content={props.accountID+''}
            />
            <DataRow
                title={'Kebele'}
                content={lookupKebele(kebeleState.kebeles,props.kebele)}
            />
            <DataRow
                title={'Customer Code'}
                content={props.customerCode}
            />
            <DataRow
                title={'Customer Name'}
                content={props.name}
            />
            <DataRow
                title={'Customer Type'}
                content={lookupSubscriberType(props.subscriberType)}
            />
            <DataRow
                title={'House Number'}
                content={props.address}
            />
            <DataRow
                title={'Phone Number'}
                content={props.phoneNo}
            />
            <DataRow
                title={'Name Of Place'}
                content={props.nameOfPlace}
            />
        </div>
    )
}


const CustomerInformationContainer =(props : SubscriberSearchResult) =>{
    const [kebeleState,auth] = useSelector<ApplicationState,[KebeleState,AuthenticationState]>((appState)=>[appState.kebele_state,appState.auth])
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchKebeleList({sessionId:auth.sessionId}))
    }, [])



    return (
        <div>
            <SubscriberInformationContainer {...props.subscriber}/>
        </div>
    )
}

export default CustomerInformationContainer