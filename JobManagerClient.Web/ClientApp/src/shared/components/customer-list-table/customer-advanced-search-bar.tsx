import React, { useState, useEffect } from 'react'
import {Form, Input, Select, Button, Row, Col, Checkbox} from 'antd';
import {useSelector, useDispatch} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import { GetSubscriptions2Par, SubscriberSearchField } from '../../../_services/subscribermanagment.service';
import { fetchSubscriberList } from '../../../_redux_setup/actions/mn-actions/main-actions';

const Option = Select.Option;
const InputGroup = Input.Group;
interface IProps{
    pageNumber:number
}
export default function CustomerAdvancedSearchBar(props:IProps){
    const kbl = useSelector((appState:ApplicationState)=>appState.kebele_state)
    const dmz = useSelector((appState:ApplicationState)=>appState.dmz_state)
    const pz = useSelector((appState:ApplicationState)=>appState.pressure_zone_state)
    const auth = useSelector((appState:ApplicationState)=>appState.auth)

    const dispatch = useDispatch()
    const initSearchParams : GetSubscriptions2Par={
        sessionId:auth.sessionId,
        query: '',
        fields: SubscriberSearchField.All,
        multipleVersion: false,
        kebele: -1,
        zone: -1,
        dma: -1,
        index: 0,
        page: 100
    }
    const [searchParams,setSearchParams] = useState<GetSubscriptions2Par>(initSearchParams)

    const submitFilter=()=>{
        dispatch(fetchSubscriberList(searchParams))
    }
    useEffect(()=>{ submitFilter() },[searchParams.index])

    useEffect(()=>{
        let newIndex = (props.pageNumber -1 ) * 100
        if(newIndex !== searchParams.index )
            setSearchParams({...searchParams,index:newIndex})
    },[props.pageNumber])

    return (
        <div style={{marginBottom:'10px'}}>
            <Row gutter={4}>
                <Col span ={8}>
                    <div className={'paper'} style={{padding:'10px',minHeight:160}}>
                        <Row gutter={2} style={{marginBottom:'5px'}}>
                            <Select value={searchParams.dma} onChange={(event:any)=>setSearchParams({...searchParams,dma:event})} style={{ width:'100%' }} >
                                <Option value={-1}>All DMA</Option>
                                { dmz.dmzs.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                            </Select>
                        </Row>
                        <Row style={{marginBottom:'5px'}}>
                            <Select value={searchParams.kebele} onChange={(event:any)=>setSearchParams({...searchParams,kebele:event})}  style={{ width:'100%' }}>
                                <Option value={-1}>All Kebele</Option>
                                { kbl.kebeles.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                            </Select>
                        </Row>
                        <Row style={{marginBottom:'5px'}}>
                            <Select value={searchParams.zone} onChange={(event:any)=>setSearchParams({...searchParams,kebele:event})} style={{ width:'100%' }}>
                                <Option value={-1}>Pressure Zone</Option>
                                { pz.pressure_zones.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>) }
                            </Select>
                        </Row>
                        <Row>
                            <Checkbox checked={searchParams.multipleVersion} onChange={()=>{setSearchParams({...searchParams,multipleVersion:!searchParams.multipleVersion})}}>Include Old Version Data</Checkbox>
                        </Row>
                    </div>
                </Col>

                <Col span={16}>
                    <div className={'paper'} style={{padding:'10px',minHeight:160}}>
                        <Row gutter={16}>
                            <Col span={22} offset={1} style={{marginBottom:'5px'}}>
                                <InputGroup compact>
                                    <Select value={searchParams.fields} onChange={(event:any)=>setSearchParams({...searchParams,fields:event})} style={{ width:'30%' }}>
                                        <Option value={SubscriberSearchField.All}>All</Option>
                                        <Option value={SubscriberSearchField.CustomerName}>Customer Name</Option>
                                        <Option value={SubscriberSearchField.CustomerCode}>Customer Code</Option>
                                        <Option value={SubscriberSearchField.ConstactNo}>Contact Number</Option>
                                        <Option value={SubscriberSearchField.MeterNo}>Meter Number</Option>
                                        <Option value={SubscriberSearchField.PhoneNo}>Phone Number</Option>
                                    </Select>
                                    <Input  value={searchParams.query} onChange={(event)=>setSearchParams({...searchParams,query:event.target.value})} style={{ width: '70%' }} placeholder="Search Text" />
                                </InputGroup>
                            </Col>
                        </Row>
                        <Row gutter={4}>
                            <Col span={22} offset={1} style={{marginTop:'5px'}}>
                                <Col span={5}>
                                    <Form.Item>
                                        <Button type="primary" onClick={submitFilter} block>Filter</Button>
                                    </Form.Item>
                                </Col>
                                <Col span={5}>
                                    <Form.Item>
                                        <Button type="danger" onClick={()=>setSearchParams({...initSearchParams})} block>Clear Filter</Button>
                                    </Form.Item>
                                </Col>
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
        </div>
    )
}