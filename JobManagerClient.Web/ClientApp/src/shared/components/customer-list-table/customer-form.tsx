import React, { useEffect, useState } from 'react'
import {Select, Input, Form} from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { AuthenticationState } from '../../../_model/state-model/auth-state';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { fetchKebeleList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { KebeleState } from '../../../_model/state-model/mn-sm/kebele-state';
import { SubscriberType } from '../../../_enum/mn-enum/subscriber-type';
import { Subscriber } from '../../../_model/view_model/mn-vm/subscriber-search-result';
import { initialCustomerData } from '../../../_helpers/initial value/init-customer-data';
import {horizontalFormItemLayout} from "../../core/item-layout/item-layout";

const { Option } = Select;
interface IProps{
  updateCustomerData? : (customer : Subscriber)=>void
  customer?: Subscriber
}

const CustomerForm = (props : IProps)=>{
    const formLayout = 'horizontal';
    const dispatch = useDispatch()
    const [ auth ,kebeleState ] = useSelector<ApplicationState,[AuthenticationState,KebeleState]>((appState)=>[appState.auth,appState.kebele_state]) 
    const [ customer , setCustomer ] = useState<Subscriber>( props.customer? props.customer : initialCustomerData)

    useEffect(()=>{
        kebeleState.kebeles.length === 0  &&
        dispatch(fetchKebeleList({sessionId:auth.sessionId}))
    },[])

    //Effect for external customer state
    useEffect(()=>{
      props.updateCustomerData &&
      props.updateCustomerData(customer)
    },[customer])
    return(
        <Form layout={formLayout}>
            <Form.Item label="Customer Type" {...horizontalFormItemLayout}>
                <Select
                    style={{width:'100%'}}
                    placeholder="Customer Type"
                    value={customer.subscriberType}
                    onChange={(type: any)=>setCustomer({...customer,subscriberType:type})}
                >
                    <Option value={SubscriberType.CommercialInstitution}>Commercial Institution</Option>
                    <Option value={SubscriberType.Community}>Community</Option>
                    <Option value={SubscriberType.GovernmentInstitution}>Government Institution</Option>
                    <Option value={SubscriberType.Industry}>Industry</Option>
                    <Option value={SubscriberType.NGO}>NGO</Option>
                    <Option value={SubscriberType.Private}>Private Residence</Option>
                    <Option value={SubscriberType.RelegiousInstitution}>Relegious Institution</Option>
                    <Option value={SubscriberType.Other}>Other</Option>
                    <Option value={SubscriberType.Unknown}>Unknown</Option>
                </Select>
            </Form.Item>
            <Form.Item validateStatus={customer.name.trim()===''?'error': 'success'} label="Customer Name" {...horizontalFormItemLayout}>
                <Input value={customer.name} onChange={(event)=>setCustomer({...customer,name:event.target.value})}/>
            </Form.Item>
            <Form.Item label='ስም በ አማርኛ' {...horizontalFormItemLayout}>
                <Input value={customer.amharicName} onChange={(event)=>setCustomer({...customer,amharicName:event.target.value})} />
            </Form.Item>
            <Form.Item label='Kebele' {...horizontalFormItemLayout}>
                <Select
                    style={{width:'100%'}}
                    placeholder="Select Kebele"
                    value={customer.kebele}
                    onChange={(kbl: any)=>setCustomer({...customer,kebele:kbl})}
                >
                    {
                        kebeleState.kebeles.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>)
                    }
                    <Option value={-1}>Unknown</Option>
                </Select>
            </Form.Item>
            <Form.Item label='Name Of Place' {...horizontalFormItemLayout}>
                <Input value={customer.nameOfPlace}  onChange={(event)=>setCustomer({...customer,nameOfPlace:event.target.value})}/>
            </Form.Item>
            <Form.Item label='House Number' {...horizontalFormItemLayout}>
                <Input value={customer.address} onChange={(event)=>setCustomer({...customer,address:event.target.value})} />
            </Form.Item>
            <Form.Item label={'Phone Number'} {...horizontalFormItemLayout}>
                <Input value={customer.phoneNo} onChange={(event)=>setCustomer({...customer,phoneNo:event.target.value})}/>
            </Form.Item>
            <Form.Item label={'Email'} {...horizontalFormItemLayout}>
                <Input value={customer.email} onChange={(event)=>setCustomer({...customer,email:event.target.value})} />
            </Form.Item>
        </Form>
    )    
}

export default CustomerForm