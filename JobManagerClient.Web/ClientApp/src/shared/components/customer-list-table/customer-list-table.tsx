import React, {Fragment,  useEffect, useState } from "react"
import {Divider, Table} from "antd"
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { openDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';
import CustomerProfile from '../../../module/main/customer list/customer profile/customer-profile';
import CustomerSearchBar from "./customer-search-bar";
import {Subscriber, SubscriberSearchResult} from "../../../_model/view_model/mn-vm/subscriber-search-result";
import { SubscriberSearchResultState } from "../../../_model/state-model/mn-sm/subscriber-search-result-state";
import { KebeleState } from '../../../_model/state-model/mn-sm/kebele-state';
import { AuthenticationState } from '../../../_model/state-model/auth-state';
import { fetchKebeleList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import { lookupSubscriptionType, lookupCustomerStatus, lookupKebele, lookupPressureZone, lookupDMZ } from '../../../_helpers/table-lookup-util';
import { PressureZoneState } from "../../../_model/state-model/mn-sm/pressure-zone-state";
import { DmzState } from '../../../_model/state-model/mn-sm/dmz-state';
import CButton from "../../core/cbutton/cbutton";
import {CSpinProps} from "../../core/cspin/cspin";
const { Column } = Table;

interface IProps{
    paginationHandler?:React.Dispatch<React.SetStateAction<number>>
    pageSize?:number
    withAllColumns? : boolean
    withSearchBar? : boolean
    withSelection?:boolean
    setSelectedCustomers?:React.Dispatch<React.SetStateAction<SubscriberSearchResult | undefined>>
    height?:string
    searchBarWithOutAddOption?:boolean
    withActions? : { actionName: string , handler:(text: string , record : SubscriberSearchResult )=>void }[]
}


interface ITableRecord extends SubscriberSearchResult{
    kebele : string
    status : string
    subscriptionType : string  
    pressureZone : string
    dmz:string
    key:number
}
export default function CustomersListTable(props:IProps) {
    let index = 0;
    const dispatch = useDispatch()
    const [subscription,kebeleState,auth,pressure_zone_state,dmz_state] = useSelector<ApplicationState,[
                SubscriberSearchResultState,
                KebeleState,
                AuthenticationState,
                PressureZoneState,
                DmzState
            ]>(
        (appState)=>[appState.subscription_search_result_state,appState.kebele_state,appState.auth,appState.pressure_zone_state,appState.dmz_state]
    )
    const [tableData,setTableData] = useState<ITableRecord[]>([]) 
    useEffect(()=>{
        !kebeleState.error &&
        !kebeleState.loading &&
        kebeleState.kebeles.length === 0 &&
        dispatch(fetchKebeleList({sessionId:auth.sessionId}))
    },[])
    /**
     * Effect to customize table data to add look up functionality
     */
    useEffect(() => {
        setTableData([])
        const temp:ITableRecord[] = []
        let key = 0;
        subscription.payload._ret.forEach(subs=>{
            const kebele = lookupKebele(kebeleState.kebeles,subs.subscriber.kebele)
            const subscriptionType = lookupSubscriptionType( subs.subscription!== null ? subs.subscription.subscriptionType : undefined)                
            const status = lookupCustomerStatus(subs.subscriber.status) 
            const pressureZone = lookupPressureZone(pressure_zone_state.pressure_zones,subs.subscription!== null ? subs.subscription.pressureZone : undefined)
            const dmz = lookupDMZ(dmz_state.dmzs,subs.subscription!==null ? subs.subscription.dma : undefined)
            const newTableData : ITableRecord = {...subs,kebele , status, subscriptionType,pressureZone,dmz, key }
            temp.push(newTableData)
            key+=1;
        })
        setTableData(temp)
        return () => {
            setTableData([])
        };
    }, [subscription,kebeleState])
    /**
     * this mathod handles change in selection of customers
     * @param selectedKeys contains index of selected value
     * @param rows containes array of selected value
     */
    const onSelectChange = (selectedKeys:any,rows:any) => 
        props.setSelectedCustomers !== undefined  &&
        props.setSelectedCustomers !== null  &&
        props.setSelectedCustomers(rows[0])

    /**
     * Pagination handler 
     * @param pageNumber 
     *
     */
    const handlePagination = (pageNumber:number)=>
        props.paginationHandler !== undefined &&
        props.paginationHandler !== null &&
        props.paginationHandler(pageNumber)
    
    /**
     * Initial value setters 
     */
    const rowSelection = props.withSelection
    ?
    { 
        onChange: onSelectChange, 
        type:"radio"
    }
    :
    undefined

    return (
        <Fragment>
            {
                props.withSearchBar &&
                <CustomerSearchBar withOutAddOption={props.searchBarWithOutAddOption}/>
            }
            <Table 
                    onRow={(record, rowIndex) => {
                        return {
                         onDoubleClick: event => {
                            dispatch(openDrawerModal(<CustomerProfile customer= {record}/>))
                            },
                        };
                    }}
                    dataSource={tableData}
                    size="small" scroll={{ y: props.height || '49vh' }} 
                    pagination={{ pageSize: props.pageSize || 100,total:subscription.payload.nRecord, onChange:handlePagination}}
                    loading={{...CSpinProps,spinning:subscription.loading}}
                    //@ts-ignore
                    rowSelection={rowSelection}
            >
                <Column title="NO." key="number" render={(value,key) => { index+=1; return (<a>{index}</a>); }} width={'5%'}/>
                <Column title="Customer Code" dataIndex="subscriber.customerCode" key="subscriber.customerCode" width={props.withAllColumns?"10%": "25%"}/>
                <Column title="Name" dataIndex="subscriber.name" key="subscriber.name"  width={props.withAllColumns? '15%' : '40%'}/>
                <Column title="Kebele" dataIndex="kebele" key="kebele" width={props.withAllColumns? '10%' : '20%'}/>
                {
                    props.withAllColumns &&
                    <Column title="Contract N0." dataIndex="subscription.contractNo" key="subscription.contractNo" width={'10%'} />
                }
                {
                    props.withAllColumns &&
                    <Column title="Meter NO." dataIndex="subscription.meterData.serialNo" key="measureUnitID" width={'10%'} />
                }
                {
                    props.withAllColumns &&
                    <Column title="Type" dataIndex="subscriptionType" key="subscriptionType" width={'10%'} />
                }
                {
                    props.withAllColumns &&
                    <Column title="Status" dataIndex="status" key="status" width={'8%'} />
                }
                {
                    props.withAllColumns &&
                    <Column title="Pressure Zone" dataIndex="pressureZone" key="pressureZone" width={'8%'} />
                }
                {
                    props.withAllColumns &&
                    <Column title="DMA" dataIndex="dmz" key="dmz" width={'8%'} />
                }
                {
                    props.withActions &&
                    <Column
                        title="Action"
                        dataIndex="tableAction"
                        key="tableAction"
                        width={'15%'}
                        render={(text, record: ITableRecord) => {
                           return props.withActions
                               ?
                               props.withActions.map((value, key) =>
                                <span key={key}>
                                    <Divider type="vertical"/>
                                    <a type={'link'} color={'primary'} onClick={()=>value.handler(text,record)}>{ value.actionName }</a>
                                </span>)
                            :
                               <Fragment/>
                        }}
                    />
                }
            </Table>
        </Fragment>
    )
}
