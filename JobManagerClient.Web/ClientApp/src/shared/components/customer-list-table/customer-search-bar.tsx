import { Col, Row} from 'antd';
import CSearch from "../../core/cinput/csearch";
import React, { useState, Fragment } from "react";
import CModal from "../../core/cmodal/cmodal";
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { GetCustomersPar } from "../../../_services/subscribermanagment.service";
import { fetchCustomerList } from '../../../_redux_setup/actions/mn-actions/main-actions';
import CustomerForm from './customer-form';
import { Subscriber } from "../../../_model/view_model/mn-vm/subscriber-search-result";


interface CustomerSearchBarProps{
    appendedForm?: JSX.Element
    title?: string
    updateCustomerData?: (customer : Subscriber)=>void
    submitCustomerData?:()=>void
    withOutAddOption?: boolean
    validationHandler?:()=>string[] | undefined
}


const CustomerSearchBar = (props : CustomerSearchBarProps)=>{
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const dispatch = useDispatch()
    const initSearchParams: GetCustomersPar ={
        index:0,
        sessionId:auth.sessionId,
        kebele:-1,
        pageSize:30,
        query:'',
    } 
    const [searchParams,setSearchParams] = useState<GetCustomersPar>(initSearchParams)

    return (
        <div className={'paper'} style={{padding:'10px',marginBottom:'5px'}}>
            <Row gutter={4}>
                <Col span={props.withOutAddOption ? 24:18}>
                    <CSearch
                        placeholder="Customer Code / Name"
                        enterButton="Search"
                        value={searchParams.query}
                        onChange={(event)=>setSearchParams({...searchParams,query:event.target.value})}
                        onSearch={value => dispatch(fetchCustomerList(searchParams))}
                    />
                </Col>
                {
                    !props.withOutAddOption &&
                    <Col span={6}>
                        <CModal
                            block
                            withConfirmation
                            buttonText={'Add New Customer'}
                            validationHandler={props.validationHandler}
                            title={props.title || 'Add New Customer'}
                            submitHandler={props.submitCustomerData}
                        >
                            <Fragment>
                                { props.appendedForm }
                                <CustomerForm updateCustomerData={props.updateCustomerData}/>
                            </Fragment>
                        </CModal>
                    </Col>
                }
            </Row>
        </div>
    )
}

export default CustomerSearchBar