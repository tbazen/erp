import React, {ReactNode} from "react";
import {Col, Row, Typography} from "antd";
const { Title,Text } = Typography;

interface IProps{
    title:ReactNode,
    content:ReactNode
}

export const DataRow = (props:IProps)=>{
    return (
        <Row>
            <Col span={9}>
                <Text strong>{props.title}</Text>
            </Col>
            <Col span={12} offset={1}>
                <Text>{props.content}</Text>
            </Col>
        </Row>
    )
}