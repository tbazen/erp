import React from 'react'
import { Drawer} from 'antd'
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from '../../../_model/state-model/application-state';
import { closeDrawerModal } from '../../../_redux_setup/actions/drawer-modal-actions';



const CDrawerModal = () =>{
  const state = useSelector((appState:ApplicationState)=>appState.drawerModalState)
  const dispatch = useDispatch()
  return (
      <div>
        <Drawer
          width={'50%'}
          placement="right"
          closable={false}
          onClose={()=>dispatch(closeDrawerModal())}
          visible={state.visible}
        >
          {state.component}
        </Drawer>
      </div>
    );  
}
export default CDrawerModal