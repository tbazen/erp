import React, {Fragment, useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CMUIButton from "../custom button/custom-button";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {FormControl, FormControlLabel, FormLabel, Radio, RadioGroup} from "@material-ui/core";
import {Employee} from "../../../_model/view_model/IEmployee";
import {SearchEmployeePar} from "../../../_model/view_model/employee-search-par";
import {changeEmployeeOrganization} from "../../../_redux_setup/actions/hr-actions/employee-actions";
import {ChangeEmployeeOrgUnitPar} from "../../../_services/payroll.service";
import getCurrentTicks, {getLocalDateString} from "../../../_helpers/date-util";


interface IProps{
    open:boolean
    setOpen:()=>void
    employees:Employee[]
    searchParams:SearchEmployeePar
    setSearchParams:any
}



function EmployeePicker(props:IProps) {
    const orgUnits = useSelector((appState:ApplicationState)=>appState.orgUnits)
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const [newOrg,setNewOrg] = useState(0)
    const dispatch = useDispatch()
    const {searchParams,setSearchParams} = props

    const submitChange = async ()=>{
        await props.employees.forEach(empl=>{
            let param :ChangeEmployeeOrgUnitPar = {
                id : empl.id,
                sessionId:auth.sessionId,
                newOrgUnitID:newOrg,
                date:getLocalDateString()
            }
            dispatch(changeEmployeeOrganization(param))
        })
        setSearchParams({...searchParams})
    }



    return (
           <Dialog open={props.open} onClose={()=>props.setOpen()} fullWidth={true} maxWidth={'sm'}>
                <DialogTitle id="form-dialog-title">Employee Picker</DialogTitle>
                <DialogContent>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Department</FormLabel>
                        <RadioGroup
                            aria-label="Work Unit"
                            onChange = {(event:any)=>setNewOrg(event.target.value)}
                        >
                            {
                                orgUnits.payload !== null
                                    ?
                                    orgUnits.payload.map((value,key)=>
                                        <FormControlLabel key={key} value={value.id+''} control={<Radio color={'primary'}/>} label={value.name}/>)
                                    :
                                    <Fragment/>
                            }
                        </RadioGroup>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <CMUIButton onClick={submitChange} color={'primary'} >Save</CMUIButton>
                    <CMUIButton color={'secondary'} onClick={()=>props.setOpen()}>Cancel</CMUIButton>
                </DialogActions>
            </Dialog>
    );
}

export default EmployeePicker;
