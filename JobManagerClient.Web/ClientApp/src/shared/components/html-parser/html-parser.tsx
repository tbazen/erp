import React,{ Fragment } from 'react';

interface IProps{
    htmlString: string
}

export function HTMLParser(props : IProps){
    return (
        <Fragment>
            <div dangerouslySetInnerHTML={{__html:props.htmlString}}/>
        </Fragment>
    );
}
