import React, {Fragment, useEffect, useState} from 'react';
import {DatePicker, Form} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import {MomentUtil} from "../../../_helpers/date-util";
import {horizontalFormItemLayout, verticalFormItemLayout} from "../../core/item-layout/item-layout";
interface IProps {
    dateChangeHandler?: (date: string) => void
    descriptionChangeHandler?: (description: string) => void

    disableDate?:boolean
    disableDescription?:boolean
    formItemLayout?: 'horizontal' | 'vertical'
}




const SharedJobFormFields = (props: IProps) => {
    const [ date,setDate ] = useState<string>(MomentUtil.getCurrentDateTimeString())
    const [ description, setDescription ] = useState<string>('')
    const formItemLayout = (props.formItemLayout && props.formItemLayout === 'horizontal') ? horizontalFormItemLayout : verticalFormItemLayout
    useEffect(()=>{ props.dateChangeHandler && props.dateChangeHandler(date) },[date])
    useEffect(()=>{ props.descriptionChangeHandler &&  props.descriptionChangeHandler(description)},[description])

    return (
        <Fragment>
            <Form.Item label="Date & Time" {...formItemLayout}>
                <DatePicker
                    allowClear={false}
                    showTime={{use12Hours:true}}
                    defaultValue={MomentUtil.getCurrentDateTime()}
                    disabled={props.disableDate}
                    style={{width:'100%'}}
                    onChange={(value: any, dateString: string)=>setDate(dateString)}
                    size={'large'} />
            </Form.Item>
            <Form.Item label="Job Description" {...formItemLayout}>
                <TextArea
                    rows={5}
                    disabled={props.disableDescription}
                    onChange={(event)=>setDescription(event.target.value)}
                />
            </Form.Item>
        </Fragment>
    )
}

export default SharedJobFormFields
