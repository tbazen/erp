import React, {Fragment, useEffect, useState} from 'react'
import {
    BWFMeterReading,
    ReadingExtraInfo
} from "../../../../_model/level0/subscriber-managment-type-library/bwf-meter-reading";
import {BWFStatus} from "../../../../_model/level0/subscriber-managment-type-library/bwf-status";
import {DataRow} from "../../data-row/data-row";
import {MeterReadingType} from "../../../../_model/level0/subscriber-managment-type-library/mater-reading-type";
import {Divider} from "antd";
import {BillPeriod} from "../../../../_model/level0/subscriber-managment-type-library/bill-period";
import SubscriberManagementService from "../../../../_services/subscribermanagment.service";
import {ApplicationState} from "../../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../../_model/state-model/auth-state";
import {useSelector} from "react-redux";


const subscriber_mgr_service  = new SubscriberManagementService()
interface IProps{
    reading : BWFMeterReading
}

export function BWFReadingInformationContainer(props: IProps){
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=>[ appState.auth ])
    const [ period,setPeriod ] = useState<BillPeriod>()
    useEffect(()=>{
        fetchPeriod()
    },[])
    async function fetchPeriod(){
        try{
            const period : BillPeriod = (await subscriber_mgr_service.GetBillPeriod({
                periodID:props.reading.periodID,
                sessionId:auth.sessionId
            })).data
            setPeriod(period)
        }catch (e){

        }
    }
    return (
        <Fragment>
            <DataRow title={'Reading For'} content={period && period.name}/>
            <DataRow title={'Consumption'} content={props.reading.consumption}/>
            {
                props.reading.bwfStatus !== BWFStatus.Read &&
                <DataRow title={'Reading Status'} content={'Unread'}/>
            }
            {
                props.reading.bwfStatus === BWFStatus.Read &&
                <Fragment>
                    {
                        props.reading.readingType === MeterReadingType.MeterReset &&
                        <DataRow title={'Reading Type'} content={'Meter Reset'}/>
                    }
                    {
                        props.reading.readingType === MeterReadingType.Average &&
                        <DataRow title={'Reading Type'} content={`Estimated by averaging the last ${props.reading.averageMonths} Months `}/>
                    }
                </Fragment>
            }
            {
                props.reading.extraInfo === ReadingExtraInfo.Coordinate &&
                <Fragment>
                    <Divider orientation={'left'}>Coordinates</Divider>
                    <DataRow title={'Latitude'} content={props.reading.readingX}/>
                    <DataRow title={'Longitude'} content={props.reading.readingY}/>
                    <DataRow title={'Read on '} content={props.reading.readingTime}/>
                </Fragment>
            }
            {
                props.reading.extraInfo === ReadingExtraInfo.Remark &&
                <Fragment>
                    <DataRow title={'Remark'} content={props.reading.readingRemark}/>
                </Fragment>
            }
        </Fragment>
    )
}