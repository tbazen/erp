import React, {Fragment, useEffect, useState} from 'react'
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {useSelector} from "react-redux";
import {TransactionItems} from "../../../_model/level0/iERP-transaction-model/transaction-items";
import {Kebele} from "../../../_model/view_model/mn-vm/kebele";
import {Subscription} from "../../../_model/level0/subscriber-managment-type-library/subscription";
import {DataRow} from "../data-row/data-row";
import BNFinanceService, {GetTransactionItemsPar} from "../../../_services/bn.finance.service";
import SubscriberManagementService, {GetKebelePar} from "../../../_services/subscribermanagment.service";

interface IProps{
    connection: Subscription
}

const bn_finance_service = new BNFinanceService()
const subscriber_mgr_service = new SubscriberManagementService()

export function ConnectionInformationContainer(props:IProps){
    const [ auth ] = useSelector<ApplicationState,  [ AuthenticationState ]>(appState=>[ appState.auth ])
    const [ transactionItem , setTransactionItem ] = useState<TransactionItems>()
    const [ kebele , setKebele ] = useState<Kebele>()
    useEffect(()=>{
        props.connection.meterData !== undefined &&
        props.connection.meterData !== null &&
        fetchTransactionAndKebele()
    },[props.connection])

    async function fetchTransactionAndKebele() {
        try {
            if(props.connection.itemCode){
                const itemParams : GetTransactionItemsPar ={
                    sessionId:auth.sessionId,
                    code:props.connection.itemCode
                }
                const transactionItem : TransactionItems = ( await bn_finance_service.GetTransactionItems(itemParams)).data
                if(props.connection.kebele > 0) {
                    const kebeleParams : GetKebelePar = {
                        sessionId: auth.sessionId,
                        kebeleID: props.connection.kebele,
                    }
                    const kebele = (await subscriber_mgr_service.GetKebele(kebeleParams)).data
                    setKebele(kebele)
                }
                setTransactionItem(transactionItem)
            }
        } catch (e) {
            console.log('ERROR ',e)
        }
    }

    return (
        <Fragment>
            <DataRow title={'Contract NO.'} content={props.connection.contractNo}/>
            <DataRow title={'Meter Type'} content={transactionItem && transactionItem.name}/>
            <DataRow title={'Meter NO.'} content={props.connection.serialNo}/>
            <DataRow title={'Kebele'} content={kebele && kebele.name}/>
            <DataRow title={'House NO.'} content={props.connection.address}/>
        </Fragment>
    )
}