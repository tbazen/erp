import React, {Fragment, useEffect, useState} from 'react'
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {MeterData} from "../../../_model/view_model/mn-vm/meter-data";
import {DataRow} from "../data-row/data-row";
import {TransactionItems} from "../../../_model/level0/iERP-transaction-model/transaction-items";
import BNFinanceService, {GetTransactionItemsPar} from "../../../_services/bn.finance.service";

interface IProps{
    meter: MeterData
}

const bn_finance_service = new BNFinanceService()
export function MeterInformationContainer(props:IProps){
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=>[ appState.auth ])
    const [ transactionItem , setTransactionItem ] = useState<TransactionItems>()
    useEffect(()=>{
        props.meter !== undefined &&
        props.meter !== null &&
        fetchTransactionItem()
    },[ props.meter ])

    async function fetchTransactionItem(){
        try{
            if(props.meter.itemCode){
                const itemParams : GetTransactionItemsPar ={
                    sessionId:auth.sessionId,
                    code:props.meter.itemCode
                }
                const transactionItem : TransactionItems = ( await bn_finance_service.GetTransactionItems(itemParams)).data
                setTransactionItem(transactionItem)
            }
        } catch(e){}
    }

    return (
        <Fragment>
            <DataRow title={'Meter Type'} content={transactionItem && transactionItem.name}/>
            <DataRow title={'Meter NO.'} content={props.meter.serialNo}/>
            <DataRow title={'Meter Model'} content={props.meter.modelNo}/>
        </Fragment>
    )
}