import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import { PRIMARY_FOREGROUND} from "../../../_constants/color/color-constant";

let LoadingCircle= () => {
  return (
    <CircularProgress style={{
        color:PRIMARY_FOREGROUND,
        animationDuration: '550ms'
      }} 
    size={30}
    thickness={5}
    variant="indeterminate"
    disableShrink />
  )
}
export default LoadingCircle