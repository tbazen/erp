import React from 'react'
import Modal from '@material-ui/core/Modal/Modal';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Fab } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';

function LoadingModal(){
    return (
        <Modal open={true}>
            <Paper
                style={{
                width:'60%',
                margin:'auto',
                marginTop:'10vh',
                maxHeight:'80vh',
                overflowY:'auto',
                padding:'20px'
                }}>
                
                    
                        <Grid container justify="center">
                            <Grid item xs={10}>
                                <Typography variant="body1">
                                </Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <Fab style={{backgroundColor:"green",color:"white"}}>
                                    <CheckIcon />
                                </Fab>
                            </Grid>
                        </Grid>    
                        <Grid container justify="center">
                            <Grid item xs={10}>
                                <Typography variant="body1">
                                </Typography>
                            </Grid>
                            <Grid xs={2}>
                                <CircularProgress size={55} color="primary" />
                            </Grid>
                        </Grid>    
                        <Grid container justify="center">
                            <Grid item xs={10}>
                                <Typography variant="body1">
                                </Typography>
                            </Grid>
                            <Grid item xs={2}>
                                <Fab color="secondary">
                                    errno
                                </Fab>
                            </Grid>
                        </Grid>
            </Paper>
        </Modal>
    )
}

export default LoadingModal
