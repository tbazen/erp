import {Icon, notification, Spin} from "antd";
import React from "react";

export enum NotificationTypes{
    LOADING,
    ERROR,
    SUCCESS
}
export interface INotification{
    notificationType : NotificationTypes;
    message : string;
    description : string;
    duration? : number;
}

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

export function openNotification(notificationConfig:INotification){
    switch (notificationConfig.notificationType) {
        case NotificationTypes.ERROR:
            return openErrorNotification(notificationConfig)
        case NotificationTypes.SUCCESS:
            return openSuccessNotification(notificationConfig)
        case NotificationTypes.LOADING:
            return openLoadingNotification(notificationConfig)
        case NotificationTypes.ERROR:
    }
}

function openLoadingNotification(config:INotification){
    notification.destroy()
    notification.open({
        icon:<Spin indicator={antIcon} />,
        message:config.message,
        description:config.description,
        duration:0
    })
}
function openErrorNotification(config:INotification){
    notification.destroy()
    notification['error']({
        message:config.message,
        description:config.description,
    })
}
function openSuccessNotification(config:INotification){
    notification.destroy()
    notification['success']({
        message:config.message,
        description:config.description,
    })
}