import React, {FC} from "react";
import ReactToPrint, {IReactToPrintProps} from "react-to-print";


interface PrintWrapperProps extends IReactToPrintProps {}

export const PrintWrapper:FC<PrintWrapperProps> = (props) => {
    return <ReactToPrint {...props}/>;
};