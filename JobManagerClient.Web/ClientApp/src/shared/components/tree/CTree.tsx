import React, {Fragment, useEffect, useState} from 'react'
import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon'
import { fade, makeStyles, withStyles, Theme, createStyles } from '@material-ui/core/styles'
import TreeView from '@material-ui/lab/TreeView'
import TreeItem, { TreeItemProps } from '@material-ui/lab/TreeItem'
import Collapse from '@material-ui/core/Collapse'
import { useSpring, animated } from 'react-spring'
import { TransitionProps } from '@material-ui/core/transitions/transition'
import {useSelector} from "react-redux"
import {ApplicationState} from "../../../_model/state-model/application-state"
import {Account} from "../../../_model/view_model/ic-vm/account-base"
import {AccountBaseState} from "../../../_model/state-model/ic-sm/account-base-state"
import AccountingService, {GetChildAcccountPar} from "../../../_services/accounting.service"
import errorMessage from "../../../_redux_setup/actions/error.message"


function MinusSquare(props: SvgIconProps) {
    return (
        <SvgIcon fontSize="inherit" {...props}>
            {/* tslint:disable-next-line: max-line-length */}
            <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 11.023h-11.826q-.375 0-.669.281t-.294.682v0q0 .401.294 .682t.669.281h11.826q.375 0 .669-.281t.294-.682v0q0-.401-.294-.682t-.669-.281z" />
        </SvgIcon>
    );
}
function PlusSquare(props: SvgIconProps) {
    return (
        <SvgIcon fontSize="inherit" {...props}>
            {/* tslint:disable-next-line: max-line-length */}
            <path d="M22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0zM17.873 12.977h-4.923v4.896q0 .401-.281.682t-.682.281v0q-.375 0-.669-.281t-.294-.682v-4.896h-4.923q-.401 0-.682-.294t-.281-.669v0q0-.401.281-.682t.682-.281h4.923v-4.896q0-.401.294-.682t.669-.281v0q.401 0 .682.281t.281.682v4.896h4.923q.401 0 .682.281t.281.682v0q0 .375-.281.669t-.682.294z" />
        </SvgIcon>
    );
}
function CloseSquare(props: SvgIconProps) {
    return (
        <SvgIcon className="close" fontSize="inherit" {...props}>
            {/* tslint:disable-next-line: max-line-length */}
            <path d="M17.485 17.512q-.281.281-.682.281t-.696-.268l-4.12-4.147-4.12 4.147q-.294.268-.696.268t-.682-.281-.281-.682.294-.669l4.12-4.147-4.12-4.147q-.294-.268-.294-.669t.281-.682.682-.281.696 .268l4.12 4.147 4.12-4.147q.294-.268.696-.268t.682.281 .281.669-.294.682l-4.12 4.147 4.12 4.147q.294.268 .294.669t-.281.682zM22.047 22.074v0 0-20.147 0h-20.12v0 20.147 0h20.12zM22.047 24h-20.12q-.803 0-1.365-.562t-.562-1.365v-20.147q0-.776.562-1.351t1.365-.575h20.147q.776 0 1.351.575t.575 1.351v20.147q0 .803-.575 1.365t-1.378.562v0z" />
        </SvgIcon>
    );
}
function TransitionComponent(props: TransitionProps) {
    const style = useSpring({
        from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
        to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` },
    });

    return (
        <animated.div style={style}>
            <Collapse {...props} />
        </animated.div>
    );
}


const StyledTreeItem = withStyles((theme: Theme) =>
    createStyles({
        iconContainer: {
            '& .close': {
                opacity: 0.3,
            },
        },
        group: {
            marginLeft: 12,
            paddingLeft: 12,
            borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
        },
    }),
)((props: TreeItemProps) => <TreeItem id={'123'} {...props} TransitionComponent={TransitionComponent} />);

const useStyles = makeStyles(
    createStyles({
        root: {
           // height: 264,
        },
    }),
);

const LeafTreeItem =(props:Account)=><StyledTreeItem nodeId={props.id+''} label={props.name} children={<Fragment/>} collapseIcon={<CloseSquare/>} expandIcon={<CloseSquare />}/>

const CustomComponent = (props:AccountBaseState)=>{
    return (
        <Fragment>
            {
                props.loading &&
                <p>Loading, Categories</p>
            }
            {
                !props.loading &&
                props.error &&
                <p>Error Loading Categories</p>
            }
            {
                !props.loading &&
                !props.error &&
                 props.accounts.map((value,key)=> value.childCount>0 ? <ParentTreeItem key={key} {...value}/>:<LeafTreeItem key={key} {...value}/>)
            }
        </Fragment>
    )
}

const account_service = new AccountingService()

const ParentTreeItem = (props:Account)=>{
    const acts = useSelector((appState:ApplicationState)=>appState.accountBaseState)
    const [accountState,setAccountState] = useState<AccountBaseState>({loading:true,error:false,message:'',accounts:[]})
    const auth = useSelector((appState:ApplicationState)=>appState.auth)
    const tempChildPar:GetChildAcccountPar={
        index:0,
        sessionId:auth.sessionId,
        pageSize:50,
        parentAccount:props.id,
        accountType:'Account'
    }
    const fetchChildCategories = async ()=>{
        const response = account_service.GetChildAcccount(tempChildPar)
        response
            .then(res=>{
                let restructured : Account[] = []
                res.data._ret.forEach((acc:any)=>restructured.push(acc.val))
                setAccountState({error:false,message:'',loading:false,accounts:restructured})
            })
            .catch(error=>setAccountState({error:true,message:errorMessage(error),loading:false,accounts:[]}))

    }
    useEffect(()=>{
        fetchChildCategories()
    },[])

    return (
        <StyledTreeItem nodeId={props.id+''} label={props.name} >
            <CustomComponent {...accountState}/>
        </StyledTreeItem>
    )
}
export default function CTreeView() {
    const classes = useStyles();
    const acts = useSelector((appState:ApplicationState)=>appState.accountBaseState)
    return (
        <TreeView
            className={classes.root}
            defaultExpanded={['1']}
            defaultCollapseIcon={<MinusSquare />}
            defaultExpandIcon={<PlusSquare />}
            defaultEndIcon={<CloseSquare />}
            onNodeToggle= {(nodeId: string, expanded: boolean)=>console.log(`Tree Node Changed [ Node Id : ${nodeId}, Expanded : ${expanded} ]`)}
        >
            {
                acts.accounts.map((value,key)=>
                    value.childCount > 0
                    ?
                    <ParentTreeItem key={key} {...value}/>
                    :
                    <LeafTreeItem key={key} {...value}/>
                )
            }
        </TreeView>
    );
}
