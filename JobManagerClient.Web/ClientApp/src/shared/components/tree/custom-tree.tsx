import React,{ FC } from "react";
import {Icon, Tree} from "antd";
import {TreeNodeNormal} from "antd/lib/tree-select/interface";
import {AntTreeNode} from "antd/lib/tree/Tree";
import {ExtendedTreeNodeNormal} from "../../hooks/tree-constructor-from-list";

interface CustomTreeProps{
    nodeClickHandler?: (clickedNode: ExtendedTreeNodeNormal) => void,
    treeNodes: TreeNodeNormal[]
}
export const CustomTreeDisplay:FC<CustomTreeProps> =(props)=>{

    function  handleNodeClick(e: React.MouseEvent<HTMLElement>, node: AntTreeNode){
       const extendedTreeNodeNormal: ExtendedTreeNodeNormal = node.props as ExtendedTreeNodeNormal
        props.nodeClickHandler &&
        props.nodeClickHandler(extendedTreeNodeNormal)
    }
    return (
        <Tree
            showLine
            showIcon
            treeData={props.treeNodes}
            onClick={handleNodeClick}
        />
    )
}