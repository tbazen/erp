import React from 'react'
import {Alert} from 'antd'
import { AlertProps } from 'antd/lib/alert'

type IProps = AlertProps

export const CAlert = (props:IProps) =>{
    return (
        <Alert {...props}/>
    )
}
