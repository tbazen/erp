import React from 'react'
import {Button} from "antd";
import {ButtonProps} from "antd/lib/button";

const CButton = (props:ButtonProps) => <Button {...props}/>

export default CButton