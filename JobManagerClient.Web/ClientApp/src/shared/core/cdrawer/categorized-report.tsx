import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {Icon, Menu} from "antd";
import {Link} from "react-router-dom";
import {ROUTES} from "../../../_constants/routes";
import React, {Fragment} from "react";
import {ReportCategory} from "../../../_model/view_model/report-category";

interface ICategorizedReport{
    parent: ReportCategory
}

export const CategorizedReportDefinition = (props  : ICategorizedReport) =>{
    const reportDefinition = useSelector((appState:ApplicationState)=>appState.reportDefinitions)
    return (
        <Menu
            mode={'vertical'}
            theme={'dark'}
        >
            {
                reportDefinition.loading &&
                <p>loading...</p>
            }
            {
                !reportDefinition.loading &&
                reportDefinition.error &&
                <p>Error loading report category</p>
            }
            {
                !reportDefinition.loading &&
                !reportDefinition.error &&
                reportDefinition.definitions.map((value,key)=>
                    value.categoryID === props.parent.id
                        ?
                        <Menu.Item key={`${value.name}${value.id}`}>
                            <Link to={`${ROUTES.REPORT_INDEX}/${value.reportTypeID}/${value.id}`} style={{textDecoration:'none'}}>
                                <Icon type="appstore" />
                                <span>{value.name}</span>
                            </Link>
                        </Menu.Item>
                        :
                        <Fragment key={Math.random().toString()}/>
                )
            }
        </Menu>
    )
}
