import React,{useState, useEffect} from "react";
import {Menu, Icon, Divider,} from 'antd';
import {ROUTES} from "../../../_constants/routes";
import {Link} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import { ApplicationState } from "../../../_model/state-model/application-state";
import { fetchReportCategoriesAndDefination } from '../../../_redux_setup/actions/report-category-actions';
import { AuthenticationState } from '../../../_model/state-model/auth-state';
import { ReportCategoryState } from "../../../_model/state-model/report-category-state";
import CLoadingPage from "../../screens/cloading/cloading-page";
import {CategorizedReportDefinition} from "./categorized-report";
import {JobRuleStateModel} from "../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {constructDrawerItem} from "./construct-drawer-item";

const { SubMenu } = Menu;

const DBNavigationList : IDashboardItem[] = [
    {
        text:'Tools',
        path:ROUTES.MAIN.INDEX,
        icon:'appstore'
    },
    {
        text:'Active Jobs',
        path:ROUTES.ACTIVE_JOBS,
        icon:'reconciliation'
    },
    {
        text:'Human Resource',
        path:ROUTES.HUMAN_RESOURCE,
        icon:'team'
    },
    {
        text:'Item Configuration',
        path:ROUTES.ITEM_CONFIGURATION,
        icon:'tool'
    }
]

export interface IDashboardItem{
    path:string
    icon : string
    text : string
}





type Theme = 'dark' | 'light'
type Mode = 'vertical'|'inline'
const CDrawer =()=> {
    const [drawer,setDrawer] = useState<{mode:Mode,theme:Theme,collapsed:boolean,width:number}>({
        mode: 'vertical',
        theme: 'dark',
        collapsed:true,
        width:50
    })
    const dispatch =   useDispatch()
    const [auth, reportCatagory] = useSelector<ApplicationState,[AuthenticationState,ReportCategoryState]>((
        appState:ApplicationState)=>[appState.auth,appState.reportCategory])
    useEffect(()=>{
        dispatch(fetchReportCategoriesAndDefination(auth.sessionId))
    },[])




    /**
     * Configuring dynamic drawer content loading
     *
     */
    const [jobRule] = useSelector<ApplicationState,[JobRuleStateModel]>(appState=>[appState.job_rule])
    const [drawerItems , setDashboardItems] = useState<IDashboardItem[]>([])

    useEffect(()=>{
        !jobRule.error && !jobRule.loading && updateConfigurationList()
    },[jobRule])

    function updateConfigurationList(){
        const drawerItems: IDashboardItem[] = []
        jobRule.clientHandlers.forEach(clientHandler=>{
            clientHandler.meta.hasConfiguration &&
            drawerItems.push(constructDrawerItem(clientHandler))
        });
        setDashboardItems([...drawerItems])
    }





    return (
        <Menu
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            inlineCollapsed={drawer.collapsed}
            mode={drawer.mode}
            theme={drawer.theme}
        >
            {
                DBNavigationList.map((value,key)=>
                        <Menu.Item key={value.path}>
                            <Link to={value.path} style={{textDecoration:'none'}}>
                                <Icon type={value.icon} />
                                <span>{value.text}</span>
                            </Link>
                        </Menu.Item>
                )
            }


            {
                jobRule.loading &&
                <Menu.Item key={'Loading job rule'} disabled>
                    <span>...</span>
                </Menu.Item>
            }
            {
                jobRule.loading &&
                jobRule.error &&
                <p>Failed to load configuration</p>
            }
            {
                drawerItems.map((value,key)=>
                    <Menu.Item key={value.path}>
                        <Link to={value.path} style={{textDecoration:'none'}}>
                            <Icon type={value.icon} />
                            <span>{value.text}</span>
                        </Link>
                    </Menu.Item>
                )
            }

            <SubMenu
                key="report"
                title={
                    <span>
                        <Icon type="snippets" />
                        <span>Report</span>
                    </span>
                }
            >
                {
                    reportCatagory.loading &&
                    <CLoadingPage/>
                }
                {
                    !reportCatagory.loading &&
                    reportCatagory.error &&
                    <p>Failed To load report Categories</p>
                }
                {
                    !reportCatagory.loading &&
                    !reportCatagory.error &&
                    reportCatagory.categories.map((value,key)=>
                    <SubMenu
                            key={value.name}
                            style={{width:275}}
                            title={
                                <span>
                                    <Icon type="appstore" />
                                    <span>{value.name}</span>
                                </span>
                            }
                        >
                            <CategorizedReportDefinition parent={value}/>
                    </SubMenu>
                    )
                }
                <Menu.Item key={'report-category-separator'}>
                    <Divider/>
                </Menu.Item>
                <Menu.Item key={'report-manager'}>
                    <Link to={ROUTES.R_MANAGE_REPORT} style={{textDecoration:'none'}}>
                        <Icon type="tool" />
                        <span>Manage Report</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key={'refresh-report-list'} onClick={()=>{ dispatch(fetchReportCategoriesAndDefination( auth.sessionId)) }}>
                    <Icon type="reload" />
                    <span>Refresh</span>
                </Menu.Item>
            </SubMenu>
        </Menu>
    );
}

export default CDrawer