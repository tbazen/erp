import {IDashboardItem} from "./cdrawer";
import {IClientHandler} from "../../../_model/job-rule-model/client-handler";

export function constructDrawerItem(clientHandler : IClientHandler) : IDashboardItem {
    const drawerItem : IDashboardItem = {
        text : clientHandler.meta.configurationName || "NO CONFIGURATION",
        path : clientHandler.meta.configurationRoute || '/NOT FOUND',
        icon : "rise"
    }
    return drawerItem
}