import React, {Fragment, useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {JobRuleStateModel} from "../../../_model/state-model/job-rule-sm/job-rule-state-model";
import {IDashboardItem} from "./cdrawer";
import {constructDrawerItem} from "./construct-drawer-item";
import LoadingCircle from "../../components/loading/loading-circle";
import {Link} from "react-router-dom";
import {Icon, Menu} from "antd";

export function  DrawerConfigurationList() {
    const [jobRule] = useSelector<ApplicationState,[JobRuleStateModel]>(appState=>[appState.job_rule])
    const [drawerItems , setDashboardItems] = useState<IDashboardItem[]>([])

    useEffect(()=>{
        !jobRule.error && !jobRule.loading && updateConfigurationList()
    },[jobRule])

    function updateConfigurationList(){
        const drawerItems: IDashboardItem[] = []
        jobRule.clientHandlers.forEach(clientHandler=>{
            clientHandler.meta.hasConfiguration &&
            drawerItems.push(constructDrawerItem(clientHandler))
        });
        setDashboardItems([...drawerItems])
    }
    return(
        <Menu
            theme={'dark'}
        >
            {
                jobRule.loading &&
                <LoadingCircle/>
            }
            {
                jobRule.loading &&
                jobRule.error &&
                <p>Failed to load configuration</p>
            }
            {
                drawerItems.map((value,key)=>
                    <Menu.Item key={value.path}>
                        <Link to={value.path} style={{textDecoration:'none'}}>
                            <Icon type={value.icon} />
                            <span>{value.text}</span>
                        </Link>
                    </Menu.Item>
                )
            }
        </Menu>
    )
}