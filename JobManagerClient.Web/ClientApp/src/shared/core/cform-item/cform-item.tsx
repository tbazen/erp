import React from 'react'
import { Form } from 'antd'
import { FormComponentProps } from 'antd/lib/form'

interface IProps extends FormComponentProps{ 
    input : JSX.Element
    label?:string
    required?:boolean
    errorMessage?:string
}

const WarppedFormItem  = (props :IProps )=>{
    const { getFieldDecorator } = props.form;
    return (
        <Form.Item label={props.label || ''}>
            {getFieldDecorator( 'formdecorator', {
                rules: [{ required: props.required, message: props.errorMessage || 'Invalid Input' }]
            })(props.input)}
        </Form.Item>
    )
}

const CFormItem = Form.create<IProps>({ name: 'coordinated' })(WarppedFormItem)
export default CFormItem
