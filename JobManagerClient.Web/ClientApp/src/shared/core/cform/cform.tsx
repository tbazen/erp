import React, { ReactChildren } from 'react'
import { Form } from 'antd'
import { FormComponentProps, FormProps } from 'antd/lib/form'

interface IProps extends FormComponentProps<FormProps>{ 
    submitHandler? : (value : any) =>void
}

const WarppedForm = (props :IProps )=>{
    const handleSubmit = ( e :any ) => {
        e.preventDefault();
        props.form.validateFields((err : any, values : any) => {
          if (!err) {
            console.log('[ No ERROR : ', values);
            props.submitHandler &&
            props.submitHandler(values)
          }else{
              console.log('ERROR IN FIELDS ',values)
          }
        });
      };
    return (
        <Form onSubmit={handleSubmit}>
            {props.wrappedComponentRef}
        </Form>
    )
}

const CForm = Form.create<IProps>({ name: 'coordinated' })(WarppedForm)
export default CForm
