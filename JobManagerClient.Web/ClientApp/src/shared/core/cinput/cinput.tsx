import React from 'react'
import {Input} from "antd";
import {InputProps} from "antd/lib/input";

const style={
    borderRadius:'1px',
    color:'red'
}
const CInput = (props:InputProps) => <Input style={style} {...props}/>

export default CInput