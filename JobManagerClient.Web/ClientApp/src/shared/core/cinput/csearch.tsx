import React from 'react'
import {Input} from "antd"
import {SearchProps} from "antd/lib/input"

const { Search } = Input;
const CSearch = (props: SearchProps)=><Search {...props}/>

export default CSearch