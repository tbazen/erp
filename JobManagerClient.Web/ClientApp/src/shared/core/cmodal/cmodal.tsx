import React, {useState} from 'react'
import {Modal, Button, Row, Col, Divider} from 'antd'
import {ButtonProps} from "antd/lib/button";
import {ModalProps} from "antd/lib/modal/Modal";
import { List,Typography } from "antd";
import {modalErrorContainerLayout, modalFooterLayout} from "../item-layout/item-layout";

const { Text } = Typography

interface IProps{
    modalProps?: ModalProps
    buttonProps?: ButtonProps
    buttonText:string
    block?:boolean
    title?:string
    children?:JSX.Element
    withConfirmation? : boolean
    submitHandler ? : ()=>void
    validationHandler? : ()=> string[] | undefined
    footerText?:{okText:string,cancelText:string}
}

const CModal = (props:IProps)=>{
    const [visible ,setVisibility] = useState(false)
    const [ validationErrors,setValidationErrors ] = useState<string[]>([])
    const showModal = () => setVisibility(true)

    const handleOk = (e:any) => {
        if(props.validationHandler){
            const error = props.validationHandler()
            if(!error || (error && error.length === 0)){
                invokeSubmit()
                return
            }
            setValidationErrors(error)
        }
        else
            invokeSubmit()
    };

    const invokeSubmit = ()=> {
        setValidationErrors([])
        props.submitHandler && props.submitHandler()
        setVisibility(false)
    }
    const handleCancel = (e:any) => {
        setVisibility(false)
    };
    const footer = props.withConfirmation
    ?
    <Row gutter={6}>
        <Col lg={modalFooterLayout}>
            <Button block size={'large'} key="back" type={'danger'} onClick={handleCancel}>
                {props.footerText ? props.footerText.cancelText : 'Cancel'}
            </Button>
        </Col>
        <Col lg={{span:4}}>
            <Button block size={'large'} key="submit" type="primary" onClick={handleOk}>
                {props.footerText ? props.footerText.okText : 'Submit'}
            </Button>
        </Col>
    </Row>
    :
    null

    return (
        <div>
            <Button {...props.buttonProps} onClick={showModal} block={props.block}>
                {props.buttonText}
            </Button>
            <Modal
                {...props.modalProps}
                title={props.title || ''}
                visible={visible}
                maskClosable={false}
                onOk={handleOk}
                afterClose={()=>setValidationErrors([])}
                width={'50%'}
                onCancel={handleCancel}
                footer={footer}
            >
                <Row>
                    <Col lg={{span:24}}>
                        { props.children }
                    </Col>
                    <Col lg={modalErrorContainerLayout} style={{marginTop:'10px',marginBottom:'5px'}}>
                        {
                            validationErrors &&
                            validationErrors.length > 0 &&
                            <List
                                bordered
                                size={'small'}
                                header={<Text type={'danger'} strong>One/More Error's found</Text>}
                                dataSource={validationErrors}
                                renderItem={(item,index)=><List.Item><Text type={'danger'}>{index+1}. {item}</Text></List.Item>}
                            />
                        }
                    </Col>
                </Row>
            </Modal>
        </div>
    );
}

export default CModal