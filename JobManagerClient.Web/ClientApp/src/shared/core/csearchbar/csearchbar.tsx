import React from "react";
import {Col, Row} from "antd";
import CSearch from "../cinput/csearch";
import {SearchProps} from "antd/lib/input"


interface IProps extends SearchProps{
    width? : 10 | 11 | 12| 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24
}

const CSearchBar = (props:IProps)=>{
    return (
        <div className={'paper'} style={{marginBottom:'5px',padding:'10px'}}>
            <Row gutter={4}>
                <Col span={ props.width!=null && props.width!== undefined ? props.width : 12}>
                    <CSearch {...props}/>
                </Col>
            </Row>
        </div>
    )
}

export default CSearchBar