import React from 'react'
import { Spin, Icon } from 'antd';
import {SpinProps} from "antd/lib/spin";

const LoadingIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
export const CSpin = (props: SpinProps)=> <Spin {...props} indicator={LoadingIcon} />
export const CSpinProps:SpinProps = { indicator:LoadingIcon }