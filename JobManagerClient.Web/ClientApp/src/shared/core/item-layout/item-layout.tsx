export const modalFooterLayout = {span:4,offset:16}
export const modalErrorContainerLayout = {span:18,offset:6}
export const horizontalFormItemLayout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
}

export const verticalFormItemLayout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
}