import {TreeNodeNormal} from "antd/es/tree-select/interface";
import {ItemCategory} from "../../../_model/view_model/ic-vm/item-category";
import {ItemNode} from "./item-registration-form-container";

export function constructItemCategoryTree(item_categories:ItemCategory[]) : ItemNode[] {
    let marker :number[] = [];
    let tree : ItemNode[] = [] ;
    for(const item of item_categories){
        const isMarked = marker.find(mark => mark === item.id);
        if(isMarked)
            continue;
        const treeNode:TreeNodeNormal & ItemCategory = {
            title:`[${item.code }] -  ${item.description}`,
            value: item.id,
            key: `${item.code}`,
            children:[],
            ...item
        }
        marker= [...marker,item.id];
        const children = item_categories.filter(itm=>itm.pid === item.id );
        const childNodes = children.map((itemValue)=>{
            marker= [ ...marker,itemValue.id ]
            const transformed : TreeNodeNormal & ItemCategory = {
                title:`[${itemValue.code }] -  ${itemValue.description}`,
                value:itemValue.id,
                key:`${itemValue.code}`,
                ...itemValue
            }
            return transformed
        });
        treeNode.children = childNodes;
        tree = [ ...tree,treeNode ];
    }
    let restructuredTree:ItemNode[] = []
    let outOfPlaceNodes: ItemNode[] = []
    for(const node of tree){
        if(node.pid === -1 ){
            restructuredTree = [ ...restructuredTree,node ]
        } else outOfPlaceNodes= [...outOfPlaceNodes,node]
    }
    for(const node of outOfPlaceNodes){
        (function placeNode(n:ItemNode, nodes : ItemNode[]) {
            for (const nd of nodes){
                if (nd.id === node.pid) {
                    if(nd.children){
                        // @ts-ignore
                        nd.children = [...nd.children, node]
                    } else nd.children = [ node ]
                    return;
                } else nd.children && placeNode(node,nd.children as ItemNode[])
            }
        })(node,restructuredTree)
    }
    return restructuredTree;
}