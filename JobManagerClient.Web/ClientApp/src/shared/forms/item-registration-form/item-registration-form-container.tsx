import React, {Fragment, useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {MeasureUnitState} from "../../../_model/state-model/aj-sm/measure-unit-state";
import {ItemCategoryState} from "../../../_model/state-model/ic-sm/item-catagory-state";
import {GetMeasureUnitsPar} from "../../../_services/bn.finance.service";
import {fetchMeasureUnits} from "../../../_redux_setup/actions/aj-actions/active-job-actions";
import {fetchItemCategories} from "../../../_redux_setup/actions/ic-actions/item-configuration-actions";
import {ItemCategory} from "../../../_model/view_model/ic-vm/item-category";
import {TreeNode} from "antd/es/tree-select";
import {Alert, Checkbox, Col, Divider, Form, Input, Row, Select, Spin} from "antd";
import {Ctree} from "../../components/custom-tree/ctree";
import {GoodOrService, ItemTaxStatus, ServiceType} from "../../../_model/view_model/ic-vm/transaction-item";
import {
    DepreciationType,
    ExpenseType,
    InventoryType, TransactionItems
} from "../../../_model/level0/iERP-transaction-model/transaction-items";
import {constructItemCategoryTree} from "./item-category-tree-constructor";

const InputGroup = Input.Group;
const { TextArea } = Input;
const { Option } = Select;

export type ItemNode = TreeNode & ItemCategory

interface IProps{
    isItemList?: boolean
    itemChangeHandler: (transactionItem: TransactionItems)=>void
}

export function ItemRegistrationFormContainer(props : IProps){
    const [ transactionItem ,setItem ] = useState<TransactionItems>(new TransactionItems())
    const [ auth, measure_unit , item_category ] = useSelector<ApplicationState,[AuthenticationState,MeasureUnitState,ItemCategoryState]>(appState=>[ appState.auth,appState.measure_unit_state,appState.item_category_state ])
    const [ treeNode , setTreeNode ] = useState< ItemNode[]>([]);
    const dispatch = useDispatch();
    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 16 },
    }
    const formLayout = 'horizontal';
    useEffect(()=>{
        const params :GetMeasureUnitsPar ={sessionId:auth.sessionId}
        dispatch(fetchMeasureUnits(params))
        if(item_category.item_categories.length === 0 )
            dispatch(fetchItemCategories({sessionId:auth.sessionId,PID:-1}))
    },[])
    useEffect(()=>{
        !item_category.loading &&
        !item_category.error &&
        updateTreeData()
    },[item_category])

    useEffect(()=>{
        props.itemChangeHandler(transactionItem)
    },[ transactionItem ])

    function updateTreeData(){
        const treeData = constructItemCategoryTree(item_category.item_categories)
        setTreeNode(treeData)
    }

    return (
          <Form layout={formLayout}>
                <Form.Item label="Item Code" {...formItemLayout}>
                    {
                        item_category.loading &&
                        <Spin spinning={true} delay={500}>
                            <Alert
                                message="Loading item Category"
                                type="info"
                            />
                        </Spin>
                    }
                    {
                        !item_category.loading &&
                        item_category.error &&
                        <Alert
                            message="Failed to load item category"
                            description={item_category.message}
                            type="error"

                        />
                    }
                    {
                        !item_category.loading &&
                        !item_category.error &&
                        <InputGroup compact>
                            <Ctree
                                list={treeNode}
                                onSelect={(value) => setItem({...transactionItem, categoryID: +value})}
                            />
                        </InputGroup>
                    }
                </Form.Item>
                {
                    props.isItemList &&
                    <Form.Item label="Category" {...formItemLayout}>
                        <Input  />
                    </Form.Item>
                }
                <Divider orientation="left">{`General Information`}</Divider>
                <Row gutter={4}>
                    <Col span={6}>
                        <Checkbox
                            checked={transactionItem.isExpenseItem}
                            onChange={(event)=>setItem({...transactionItem,isExpenseItem:event.target.checked})}
                        >Expense Item</Checkbox>
                    </Col>
                    <Col span={6}>
                        <Checkbox
                            checked={transactionItem.isSalesItem}
                            onChange={(event)=>setItem({...transactionItem,isSalesItem:event.target.checked})}
                        >Sales Item</Checkbox>
                    </Col>
                    <Col span={6}>
                        <Checkbox
                            checked={transactionItem.isFixedAssetItem}
                            onChange={(event)=>setItem({...transactionItem,isFixedAssetItem:event.target.checked})}
                        >Fixed Asset Item</Checkbox>
                    </Col>
                    <Col span={6}>
                        <Checkbox
                            checked={transactionItem.isInventoryItem}
                            onChange={(event)=>setItem({...transactionItem,isInventoryItem:event.target.checked})}
                        >Inventory Item</Checkbox>
                    </Col>
                </Row>
                <Divider />
                <Form.Item label="Code" {...formItemLayout}>
                    <Input  />
                </Form.Item>
                <Form.Item label="Name" {...formItemLayout}>
                    <Input value={transactionItem.name}
                           onChange={(event)=>setItem({...transactionItem,name:event.target.value})} />
                </Form.Item>
                <Form.Item label="Tax status" {...formItemLayout}>
                    <Select
                        onChange={(type: any)=>{ setItem({...transactionItem,taxStatus:type}) }}
                    >
                        <Option value={ItemTaxStatus.NonTaxable}>Non Taxable</Option>
                        <Option value={ItemTaxStatus.Taxable}>Taxable</Option>
                    </Select>
                </Form.Item>
                {
                    transactionItem.isExpenseItem &&
                    <Fragment>
                        <Form.Item label="Expense Type" {...formItemLayout}>
                            <Select
                                onChange={(type: any)=>{  setItem({...transactionItem,inventoryType:type}) }}
                            >
                                <Option value={ExpenseType.GeneralExpense}>Directly related to production of service</Option>
                                <Option value={ExpenseType.PurchaseExpense}>Non directly related to production of service</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="VAT form as" {...formItemLayout}>
                            <Select
                                onChange={(type: any)=>{ setItem({...transactionItem,expenseType:type}) }}
                            >
                                <Option value={ExpenseType.PurchaseExpense}>Value of local purchase input</Option>
                                <Option value={ExpenseType.GeneralExpense}>Value of general expense input </Option>
                                <Option value={ExpenseType.UnclaimedExpense}>Unclaimed Input </Option>
                                <Option value={ExpenseType.TaxAuthorityUnclaimable}>Do not declare in VAT form (do not report to ECRA )</Option>
                            </Select>
                        </Form.Item>
                    </Fragment>
                }

                {
                    transactionItem.isSalesItem &&
                    <Fragment>
                        <Form.Item label={'Has Fixed Unit Price'} {...formItemLayout}>
                            <Checkbox>

                            </Checkbox>
                        </Form.Item>
                        <Form.Item label="Unit Price" {...formItemLayout}>
                            <Input
                                value={transactionItem.fixedUnitPrice}
                                onChange={(event)=>setItem({...transactionItem,fixedUnitPrice:+event.target.value})} />
                        </Form.Item>
                    </Fragment>
                }


                {
                    transactionItem.isFixedAssetItem &&
                    <Form.Item label="Deprciation Type" {...formItemLayout}>
                        <Select
                            onChange={(type: any)=>{ setItem({...transactionItem,expenseType:type}) }}
                        >
                            <Option value={DepreciationType.Building}>Building</Option>
                            <Option value={DepreciationType.IntangibleAssets}>Intangeble Asset</Option>
                            <Option value={DepreciationType.ComputersAndSoftwareProductsRelated}>Compouter And Software Related Product</Option>
                            <Option value={DepreciationType.OtherBusinessAssets}>Other Bussiness Asset</Option>
                        </Select>
                    </Form.Item>
                }

                {
                    (transactionItem.isInventoryItem || transactionItem.isFixedAssetItem)  &&
                    <Form.Item label="Inventory Type" {...formItemLayout}>
                        <Select
                            onChange={(type: any) => {
                                setItem({...transactionItem, expenseType: type})
                            }}
                        >
                            <Option value={InventoryType.GeneralInventory}>General Inventory</Option>
                            <Option value={InventoryType.FinishedGoods}>Finished Good Inventory</Option>
                        </Select>
                    </Form.Item>
                }

                <Form.Item label="Item type" {...formItemLayout}>
                    <Select
                        value={transactionItem.goodOrService}
                        onChange={(type: any)=>{ setItem({...transactionItem,goodOrService:type})  }}
                    >
                        <Option value={GoodOrService.Good}>Good</Option>
                        <Option value={GoodOrService.Service}>Service</Option>
                    </Select>
                </Form.Item>
                {
                    transactionItem.goodOrService === GoodOrService.Service &&
                    <Form.Item label="Service Type" {...formItemLayout}>
                        <Select
                            onChange={(type: any)=>{ setItem({...transactionItem,serviceType:type})  }}
                        >
                            <Option value={ServiceType.Contractors}>Contructor</Option>
                            <Option value={ServiceType.MillServices}>Mill Service</Option>
                            <Option value={ServiceType.TractorsAndCombinedHarvesters}>Transaction and combined harvesters</Option>
                            <Option value={ServiceType.None}>None</Option>
                        </Select>
                    </Form.Item>
                }

                <Form.Item label="Measure unit" {...formItemLayout}>
                    <Select
                        onChange={(type: any)=>{ setItem({...transactionItem,measureUnitID:type}) }}
                    >
                        {
                            measure_unit.payload.map((value,key)=><Option key={key} value={value.id}>{value.name}</Option>)
                        }
                    </Select>
                </Form.Item>
                <Form.Item label="Description" {...formItemLayout}>
                    <TextArea value={transactionItem.description}
                              onChange={(event)=> setItem({...transactionItem,description:event.target.value}) }
                              rows={5}/>
                </Form.Item>
            </Form>
    )
}