import React, {Fragment, ReactNode} from 'react'
import {IStateMachineBase} from "../../_model/state-model/state-machine-base";
import CLoadingPage from "../screens/cloading/cloading-page";
import OperationFailed from "../screens/status-code/submission-failed";

interface StateMachineHOCProps<T extends IStateMachineBase>{
    state: T,
    children : ReactNode,
    fallBack?: ReactNode
}

export function StateMachineHOC<T extends IStateMachineBase>(props : StateMachineHOCProps<T>) {
    return  (
        <Fragment>
            {
                props.state.loading &&
                <CLoadingPage/>
            }
            {
                props.state.error &&
                props.fallBack === undefined &&
                <OperationFailed errorMessage={props.state.message}/>
            }
            {
                props.state.error &&
                props.fallBack !== undefined &&
                props.fallBack
            }
            {
                !props.state.loading &&
                !props.state.error &&
                props.children
            }
        </Fragment>
    )
}