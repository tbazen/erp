import React, {useEffect} from "react";
import {useDispatch} from "react-redux";

export function useReduxStoreCleaner<T extends Function>(cleanUPFunctions: T[]){
    const dispatch = useDispatch()
    useEffect(()=>{
        return ()=>{cleanUPFunctions.forEach(cleanUPFunction=>dispatch(cleanUPFunction()))}
    },[])
}