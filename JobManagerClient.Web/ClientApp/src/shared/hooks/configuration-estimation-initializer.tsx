import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../_model/state-model/application-state";
import {AuthenticationState} from "../../_model/state-model/auth-state";
import {JobConfigurationState} from "../../_model/state-model/job-configuration-sm/job-configuration-state";
import {Dispatch, SetStateAction, useEffect, useState} from "react";
import JobManagerService, {GetConfigurationWeb, JobData} from "../../_services/job.manager.service";
import {StandardJobTypes} from "../../_model/view_model/mn-vm/standard-job-types";
import {GetSystemParametersWebPar} from "../../_services/subscribermanagment.service";
import {fetchItemsInCategory} from "../../_redux_setup/actions/aj-actions/active-job-actions";
import {fetchJobConfiguration} from "../../_redux_setup/actions/job-configuration-actions/job-configuration-actions";
import {NotificationTypes, openNotification} from "../components/notification/notification";

const job_mgr_service = new JobManagerService()

export interface InitEstimationConfigurationParams<T>{
    defaultConstructor:new ()=> T
    configModelNameSpace: string
}
export function useEstimationConfigurationInitializer<T>(params: InitEstimationConfigurationParams<T>):[JobConfigurationState, T , Dispatch<SetStateAction<T>> , ()=>Promise<void> ] {
    const dispatch = useDispatch()
    const [ auth, estimation_configuration_state] = useSelector<ApplicationState,[ AuthenticationState,JobConfigurationState ]>(appState=>[ appState.auth, appState.configuration_state ])
    const [ estimationConfiguration, setEstimationConfiguration ] = useState<T>(new params.defaultConstructor())
    const configParams : GetConfigurationWeb = {
        sessionId : auth.sessionId,
        typeID : StandardJobTypes.NEW_LINE,
        type: params.configModelNameSpace
    }
    const categoryParams : GetSystemParametersWebPar = {
        sessionId:auth.sessionId,
        names : ['meterMaterialCategory']
    }
    useEffect(()=>{
        dispatch(fetchItemsInCategory(categoryParams))
        dispatch(fetchJobConfiguration(configParams))
    },[])

    useEffect(()=>{
        !estimation_configuration_state.loading &&
        !estimation_configuration_state.error &&
        estimation_configuration_state.payload !== undefined &&
        setEstimationConfiguration(estimation_configuration_state.payload as T)
    },[estimation_configuration_state])


    async function saveEstimationConfigurationChanges() {
        try{
            openNotification({
                message:'Saving Configuration',
                description:'saving your changes on estimation parameters, please wait...',
                notificationType:NotificationTypes.LOADING
            })
            await job_mgr_service.SaveConfiguration<T>({
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.NEW_LINE,
                typeName:params.configModelNameSpace,
                config:estimationConfiguration
            })
            const response = await job_mgr_service.GetConfigurationWeb(configParams)
            setEstimationConfiguration(response.data);
            dispatch(fetchItemsInCategory(categoryParams))
            openNotification({
                message:'Configuration Updated',
                description:'estimation parameters updated successfully',
                notificationType:NotificationTypes.SUCCESS
            })
        } catch (e) {
            openNotification({
                message:'Error',
                description:'Failed to save estimation parameters',
                notificationType:NotificationTypes.ERROR
            })
        }
    }


    return [ estimation_configuration_state, estimationConfiguration, setEstimationConfiguration, saveEstimationConfigurationChanges ]
}