import React, {Dispatch, SetStateAction, useEffect, useState} from 'react'
import {IBreadcrumb} from "../../_model/state-model/breadcrumd-state";
import {ROUTES} from "../../_constants/routes";
import {StandardJobTypes} from "../../_model/view_model/mn-vm/standard-job-types";
import {useDispatch, useSelector} from "react-redux";
import pushPathToBC from "../../_redux_setup/actions/breadcrumb-actions";
import JobManagerService, {GetJobPar, GetWorkFlowDataWebPar, JobData} from "../../_services/job.manager.service";
import {fetchWorkflowData} from "../../_redux_setup/actions/aj-actions/active-job-actions";
import {ApplicationState} from "../../_model/state-model/application-state";
import {AuthenticationState} from "../../_model/state-model/auth-state";
const job_mgr_service = new JobManagerService()

interface IProps{
    jobId:number,
    jobType:StandardJobTypes
}
export function useEditJobInitializer(props:IProps):[ JobData|undefined , Dispatch<SetStateAction<JobData | undefined>> ]{
    const backwardPath: IBreadcrumb[] = [
        {
            path: ROUTES.ACTIVE_JOBS,
            breadcrumbName: 'Active Jobs'
        },
        {
            path: `${ROUTES.ACTIVE_JOBS}/${props.jobId}`,
            breadcrumbName: `Job Detail - [${props.jobId}] `
        },
        {
            path:`${ROUTES.ACTIVE_JOB.EDIT[props.jobType].INDEX}/${props.jobId}`,
            breadcrumbName:'Edit'
        }
    ]
    const dispatch = useDispatch()
    const [ auth ] = useSelector<ApplicationState,[ AuthenticationState ]>(appState=>[ appState.auth ])
    const [ jobData, setJobData ] = useState<JobData>()
    useEffect(()=>{
        if(!isNaN(props.jobId) && props.jobId !== -1) {
            const workFlowparams: GetWorkFlowDataWebPar =
                {
                    key: 0,
                    jobID: props.jobId,
                    sessionId: auth.sessionId,
                    typeID: props.jobType,
                    fullData: true
                }
            dispatch(fetchWorkflowData(workFlowparams))
            dispatch(pushPathToBC(backwardPath))
            fetchJobData()
        }
        return ()=>{ dispatch(pushPathToBC([])) }
    },[])
    async function fetchJobData(){
        try{
            const jobParams:GetJobPar ={
                sessionId:auth.sessionId,
                jobID:props.jobId
            }
            const jobData:JobData = (await job_mgr_service.GetJob(jobParams)).data
            setJobData(jobData)
        } catch (e){}
    }
    return [ jobData, setJobData ]
}