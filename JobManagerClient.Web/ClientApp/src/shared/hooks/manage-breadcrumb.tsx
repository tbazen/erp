import React, {useEffect} from 'react'
import {IBreadcrumb} from "../../_model/state-model/breadcrumd-state";
import {useDispatch} from "react-redux";
import pushPathToBC from "../../_redux_setup/actions/breadcrumb-actions";

export function useBreadcrumb(backwardPath: IBreadcrumb[]){
    const dispatch = useDispatch()
    useEffect(()=>{
        dispatch(pushPathToBC(backwardPath))
        return ()=>{
            dispatch(pushPathToBC([]))
        }
    },[])
}