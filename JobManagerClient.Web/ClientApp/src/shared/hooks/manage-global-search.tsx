import {useEffect} from "react";
import {exposeSearchBar, hideSearchBar} from "../../_redux_setup/actions/global-searchbar-actions";
import {useDispatch} from "react-redux";

interface IProps{
    searchPlaceHolder:string
    searchHandler:(event:any)=>void
    cleanupSearchResult?:()=>void
}

export function useGlobalSearchBar(props:IProps){
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(exposeSearchBar(props.searchPlaceHolder,props.searchHandler))
        return ()=>{
            dispatch(hideSearchBar())
            props.cleanupSearchResult &&
            dispatch(props.cleanupSearchResult())
        }
    }, [])

}