import React, {createRef, ReactNode, useRef, Fragment, Component, FC} from 'react'
import ReactToPrint from "react-to-print";



export function usePrinter():[ ()=>void,(children: ReactNode)=>JSX.Element ] {
    const printTriggerRef = createRef<HTMLAnchorElement>();
    const componentToPrintRef = createRef<HTMLDivElement>();

    const startPrinting = () => {
        if(printTriggerRef && printTriggerRef.current){
            printTriggerRef.current.click();
        }
    };

    const PrintComponent = (children: ReactNode):JSX.Element => {
        return (
            <div style={{ display: "none" }}>
                <ReactToPrint
                    trigger={() => <a ref={printTriggerRef} />}
                    content={() =>
                        // @ts-ignore
                        componentToPrintRef.current
                    }
                />
                <div ref={componentToPrintRef}>{ children }</div>
            </div>
        );
    };
    return [startPrinting, PrintComponent];
}