import React, {useEffect, useState} from 'react'
import {TreeNodeNormal} from "antd/es/tree-select/interface";
import {IStateMachineBase} from "../../_model/state-model/state-machine-base";
import {GenerateNewUUiD} from "../../_helpers/uuid";

export interface TreeMetaData{
    rootNodeId: string | number,
    parentIdKey: string,
    valueKey: string,
    titleKey: string,
}

export type ExtendedTreeNodeNormal = TreeNodeNormal & { parentId: string | number }

interface TreeConstructorState extends IStateMachineBase{
    tree: TreeNodeNormal[]
}
const initTreeConstructor: TreeConstructorState = {
    loading: false,
    error: false,
    message: '',
    tree : []
}
const constructingTree : TreeConstructorState= {...initTreeConstructor,loading: true}
const errorConstructingTree: TreeConstructorState = {...initTreeConstructor, error: true}
const successConstructingTree: TreeConstructorState = {...initTreeConstructor}

export function useTreeConstructorFromList<T>(list: T[], treeMetaData: TreeMetaData): [ TreeConstructorState ]{
    const [ treeConstructor , setTreeConstructor ] = useState<TreeConstructorState>({...initTreeConstructor})

    useEffect(()=>{
        constructTreeFromList()
    },[ list ])

    function constructTreeFromList(){
        try {
            setTreeConstructor({...constructingTree})
            let rootNodes: ExtendedTreeNodeNormal[] = []
            let descendantNodes: ExtendedTreeNodeNormal[] = []
            const newNode=(node: T) =>{
                const newNode: ExtendedTreeNodeNormal = {
                    selectable: true,
                    //@ts-ignore
                    value: node[treeMetaData.valueKey],
                    //@ts-ignore
                    title: node[treeMetaData.titleKey],
                    key: GenerateNewUUiD(),
                    //@ts-ignore
                    parentId: node[treeMetaData.parentIdKey],
                    children: []
                }
                return newNode
            }
            const filterRootAndDescendantNodes=()=> {
                for (const listItem of list) {
                    //@ts-ignore
                    if (listItem[treeMetaData.parentIdKey] === treeMetaData.rootNodeId)
                        rootNodes = [...rootNodes, newNode(listItem)]
                    else
                        descendantNodes = [...descendantNodes, newNode(listItem)]
                }
            }

            filterRootAndDescendantNodes()

            const addNodeToTree = (item: ExtendedTreeNodeNormal, tree: ExtendedTreeNodeNormal[]) => {
                for (let i = 0; i < tree.length; i++) {
                    if (item.parentId.toString() === (tree[i].value.toString())) {
                        const children = tree[i].children as ExtendedTreeNodeNormal[];
                        if(children && children.length > 0){
                            const child = children.find(et=>et.key === item.key)
                            if(!child)
                                (tree[i].children as ExtendedTreeNodeNormal[]).push(item)
                        }
                        else
                            (tree[i].children as ExtendedTreeNodeNormal[]).push(item)
                        return true
                    }
                    addNodeToTree(item, tree[i].children as ExtendedTreeNodeNormal[])
                }
            }

            const mergeDescendantNodesWithTree = (descendantNodeList: ExtendedTreeNodeNormal[])=> {
                if (descendantNodeList.length === 0)
                    return;
                let isNodeAdded: boolean
                let unitsCopy = [...descendantNodeList]
                for (const node of descendantNodeList) {
                    isNodeAdded = addNodeToTree(node, descendantNodes) || addNodeToTree(node, rootNodes) || false
                    if (isNodeAdded) {
                        unitsCopy = [...unitsCopy.filter(u => u.key !== node.key)]
                    }
                }
                mergeDescendantNodesWithTree(unitsCopy)
            }
            mergeDescendantNodesWithTree(descendantNodes)
            setTreeConstructor({...successConstructingTree,tree: [...rootNodes]})
        } catch(error){
            setTreeConstructor({...errorConstructingTree,message: error.message})
        }
    }
    return [treeConstructor];
}