import React, {useEffect, useState} from 'react'
import {TreeNodeNormal} from "antd/es/tree-select/interface";
import {OrgUnit} from "../../_model/view_model/hr-vm/orginanizational-unit";
import {Employee} from "../../_model/view_model/IEmployee";
import {useSelector} from "react-redux";
import {ApplicationState} from "../../_model/state-model/application-state";
import {EmployeeState} from "../../_model/state-model/mn-sm/employee-state";
import {OrganizationalUnitState} from "../../_model/state-model/hr-sm/organizational-unit-state";
export function useWorkerTreeConstructor(): [ TreeNodeNormal[],boolean,boolean ]{
    const [ employeeListState , organizationUnitsState ] = useSelector<ApplicationState, [ EmployeeState , OrganizationalUnitState ]>(appState=>[appState.employee_state,appState.orgUnits ])
    const [ treeNode , setTreeNode ] = useState<TreeNodeNormal[]>([])

    useEffect(()=>{
        !employeeListState.loading &&
        !employeeListState.error &&
        !organizationUnitsState.loading &&
        !organizationUnitsState.error &&
        constructWorkersTree()
    },[ employeeListState , organizationUnitsState ])
    function constructWorkersTree(){
        let rootNodes:TreeNodeNormal[] = []
        let descendantOrgNodes:OrgUnit[] = []
        function newOrganizationNode(unit: OrgUnit){
            const newNode : TreeNodeNormal = {
                selectable:false,
                value:unit.id,
                title:unit.name,
                key: unit.id+'',
                children: []
            }
            return newNode
        }
        function newEmployeeNode(employee:Employee) {
            const newNode : TreeNodeNormal = {
                selectable:true,
                value:employee.employeeID,
                title:employee.employeeName,
                key: employee.employeeID,
                children: []
            }
            return newNode
        }
        function filterRootNodes(){
            for(const organizationUnit of organizationUnitsState.payload){
                if(organizationUnit.pid === -1){
                    rootNodes = [...rootNodes , newOrganizationNode(organizationUnit)]
                } else {
                    descendantOrgNodes = [...descendantOrgNodes , organizationUnit]
                }
            }
        }
        filterRootNodes()
        function addNodeToTree(unit: OrgUnit , tree:TreeNodeNormal[]){
            for (let i=0;i<tree.length ; i++){
                if(unit.pid === ( tree[i].value as number )){
                    (tree[i].children as TreeNodeNormal[]).push(newOrganizationNode(unit))
                    return true
                }
                addNodeToTree(unit , tree[i].children as TreeNodeNormal[])
            }
        }
        function addDescendantOrganizationsToTree(orgUnits: OrgUnit[]){
            if(orgUnits.length === 0)
                return;
            let isNodeAdded: boolean
            let unitsCopy = [...orgUnits]
            for (const unit of orgUnits){
                isNodeAdded = addNodeToTree(unit,rootNodes) || false
                if(isNodeAdded){
                    unitsCopy = [...unitsCopy.filter(u=>u.id !== unit.id)]
                }
            }
            addDescendantOrganizationsToTree(unitsCopy)
        }
        addDescendantOrganizationsToTree(descendantOrgNodes)
        function addEmployeeToTree(employee: Employee , nodes: TreeNodeNormal[]){
            for (let i=0;i<nodes.length ; i++){
                if(employee.orgUnitID === ( nodes[i].value as number )){
                    (nodes[i].children as TreeNodeNormal[]).push(newEmployeeNode(employee))
                    return true
                }
                addEmployeeToTree(employee , nodes[i].children as TreeNodeNormal[])
            }
        }
        function addEmployeesToTree(){
            for(const employee of employeeListState.payload){
                addEmployeeToTree(employee,rootNodes)
            }
        }
        addEmployeesToTree()
        setTreeNode([...rootNodes])
    }
    return [
        treeNode,
        employeeListState.loading || organizationUnitsState.loading,
        employeeListState.error || organizationUnitsState.error,
    ]
}