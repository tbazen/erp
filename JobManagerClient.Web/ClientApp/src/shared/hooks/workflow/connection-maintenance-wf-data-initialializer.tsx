import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {AuthenticationState} from "../../../_model/state-model/auth-state";
import {WorkflowDataState} from "../../../_model/state-model/aj-sm/workflow-data-state";
import {Subscription} from "../../../_model/level0/subscriber-managment-type-library/subscription";
import {WorkFlowData} from "../../../_model/level0/job-manager-model/standard-job-datatypes/job-item-setting";
import {GetWorkFlowDataWebPar, JobData} from "../../../_services/job.manager.service";
import {StandardJobTypes} from "../../../_model/view_model/mn-vm/standard-job-types";
import {fetchWorkflowData} from "../../../_redux_setup/actions/aj-actions/active-job-actions";
import SubscriberManagementService, {GetSubscriptionPar} from "../../../_services/subscribermanagment.service";
import {getTickFromDate} from "../../../_helpers/date-util";

const subscriber_mgr_service = new SubscriberManagementService()

export function useConnectionMaintenanceWFDataInitializer<T extends WorkFlowData>(job: JobData,defaultInstanceConstructor:new ()=> T):[T, Subscription, WorkflowDataState ]{
    const dispatch = useDispatch()
    const [ auth, workflowdata_state ] = useSelector<ApplicationState,[ AuthenticationState,WorkflowDataState ]>(appState => [appState.auth, appState.workflow_data_state ])
    const [ connectionMaintenanceWFData,setConnectionMaintenanceWFData ] = useState<T>(new defaultInstanceConstructor())
    const [ subscription, setSubscription ] = useState<Subscription>(new Subscription())
    useEffect(()=>{
        const params : GetWorkFlowDataWebPar =
            {
                key:0,
                jobID:job.id,
                sessionId:auth.sessionId,
                typeID:StandardJobTypes.CONNECTION_MAINTENANCE,
                fullData:true
            }
        dispatch(fetchWorkflowData(params))
    },[])
    useEffect(()=>{
        !workflowdata_state.loading &&
        !workflowdata_state.error &&
        workflowdata_state.payload !== undefined &&
        workflowdata_state.payload !== null &&
        setConnectionMaintenanceWFData(workflowdata_state.payload as T)
    },[ workflowdata_state ])
    useEffect(()=>{ connectionMaintenanceWFData && fetchSubscription() },[connectionMaintenanceWFData])
    async function fetchSubscription(){
        try{
            if(connectionMaintenanceWFData){
                //@ts-ignore
                const connectionId: number = connectionMaintenanceWFData['connectionID'] as number
                if(!connectionId)
                    return;
                const subscriptionparams : GetSubscriptionPar = {
                    id:connectionId,
                    sessionId:auth.sessionId,
                    version: getTickFromDate(new Date(job.statusDate))
                }
                const subscription = (await subscriber_mgr_service.GetSubscription(subscriptionparams)).data
                setSubscription(subscription)
            }
        } catch (e) {}
    }

    return [ connectionMaintenanceWFData,subscription,workflowdata_state ]
}