import {forwardRef, RefAttributes} from "react";
import {
    AddBox, ArrowUpward,
    Check, ChevronLeft,
    ChevronRight,
    Clear,
    DeleteOutline,
    Edit,
    FilterList,
    FirstPage, LastPage, Remove,
    SaveAlt, Search, ViewColumn
} from "@material-ui/icons";
import React from "react";

const tableIcons = {
    Add: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <Clear {...props} ref={ref} />),
    SortArrow: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <Remove {...props} ref={ref} />),
    Search: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <Search {...props} ref={ref} />),
    ViewColumn: forwardRef<SVGSVGElement & RefAttributes<{}>>((props, ref) => <ViewColumn {...props} ref={ref} />)
};

export default tableIcons