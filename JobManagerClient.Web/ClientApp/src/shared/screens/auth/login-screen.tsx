import React, {useState} from 'react'
import {useDispatch, useSelector} from 'react-redux';
import {CreateUserSessionPar} from "../../../_model/view_model/login-view-model";
import {ApplicationState} from "../../../_model/state-model/application-state";
import {requestLogin} from "../../../_redux_setup/actions/auth-actions";
import CButton from "../../core/cbutton/cbutton";
import {Col, Form, Icon, Input, Row, Typography} from "antd";
import CInput from "../../core/cinput/cinput";


const initCredential : CreateUserSessionPar = {
    userName:'',
    source:'',
    password:''
}
/**
 * old theme color #536DFE
 */

const LoginScreen = () => {
    const dispatch = useDispatch()
    const [credential,setCredential] = useState(initCredential)
    const authState = useSelector((appState:ApplicationState)=>appState.auth)
    const submitCredential = (event:any) =>{
        event.preventDefault();
        dispatch(requestLogin(credential))
    }
    // new color #001529
    // old color ##2f54eb
    return (
       <div>
        <Row style={{
            backgroundColor:'#001529',
            height:'100vh',
            textAlign:'center',
            top:'0',
            left:'0'
        }}>
            <Col xs={{span:0}}  md={{span:16}} style={{paddingTop:'35vh'}}>
                <Typography.Title style={{color:'#fff'}} >
                    WSIS,<br/>Customer Services
                </Typography.Title>
            </Col>
            <Col span={8} style={{
                backgroundColor:'#fff',
                paddingTop:'35vh',
                height:'100vh'
            }}>
                <Row>
                    <Col span={16} offset={4}>
                        <Typography.Text type="danger">
                            {authState.error && authState.message}
                        </Typography.Text>
                    </Col>
                    <Col span={16} offset={4}>
                        <Form onSubmit={submitCredential}>
                            <Form.Item>
                                <CInput
                                    placeholder="Username"
                                    size="large"
                                    onChange={(event)=>setCredential({...credential,userName:event.target.value})}
                                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    value={credential.userName}
                                />
                            </Form.Item>
                            <Form.Item>
                                <Input.Password
                                    placeholder="Password"
                                    size="large"
                                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                    onChange={(event)=>setCredential({...credential,password:event.target.value})}
                                    value={credential.password}
                                />
                            </Form.Item>
                            <Row>
                              <Col span={12}>
                                <CButton  block size={'large'}  htmlType="submit" loading={authState.loading}>
                                    Login
                                </CButton>
                              </Col>
                            </Row>
                        </Form>
                        <Row style={{textAlign:'left',marginTop:'20vh'}}>
                            <Typography.Paragraph>
                                <span>
                                    <Typography.Text strong>
                                    DISCLAIMER :
                                    </Typography.Text>
                                </span>
                                This is WSIS Customer Services,which may be accessed and used only for authorised personnel. Unauthorized access or use of this computer system may subject violators to criminal,civil,and/or administrative action.
                            </Typography.Paragraph>
                        </Row>
                    </Col>
                </Row>
            </Col>
        </Row>
       </div>
    )
}


export default LoginScreen


