import React from 'react'
import {Col, Row} from "antd";

interface IProps{
    component:(props:any)=>JSX.Element
    listItem : any[],
    xs?:{span:number,offset?:number},
    md?:{span:number,offset?:number},
    lg?:{span:number,offset?:number},
    spacing?:number
}

const ButtonGridContainer = (props:IProps)=> {
    const small = props.xs!=null && props.xs!==undefined ?props.xs : {span:24}
    const medium = props.md!=null && props.md!==undefined ? props.md : {span:8}
    const large = props.lg!=null && props.lg!==undefined ?props.lg : {span:6}
    const spacing = props.spacing!==null && props.spacing!==undefined ? props.spacing : 8
    return (
        <Row gutter={spacing}>
            {
                props.listItem.map((value, key) =>
                    <Col key={key} xs={small}  md={medium} lg={large} style={{marginBottom:spacing}}>
                        { props.component(value) }
                    </Col>)
            }
        </Row>
    )
}
export default ButtonGridContainer