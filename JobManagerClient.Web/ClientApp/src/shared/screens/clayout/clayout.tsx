import React, { FC ,useState, Fragment} from 'react'
import AppRoute from "../../../_route/route-index";
import {Col, Icon, Layout, Row, Popover, Button, BackTop, Affix, Typography, Avatar} from "antd";
import CDrawer from "../../core/cdrawer/cdrawer";
import CSearch from "../../core/cinput/csearch";
import { useDispatch, useSelector } from 'react-redux';
import { requestLogout } from '../../../_redux_setup/actions/auth-actions';
import { ApplicationState } from '../../../_model/state-model/application-state';
import CustomBreadcrumd from '../../components/custom breadcrumd/custom-breadcrumb';
import CDrawerModal from '../../components/drawer modal/drawer-modal';
import {GlobalSearchBarState} from "../../../_model/state-model/global-search-bar";
import {JobRuleStateModel} from "../../../_model/state-model/job-rule-sm/job-rule-state-model";
import { useHistory } from 'react-router-dom';
import {AuthenticationState} from "../../../_model/state-model/auth-state";
const { Text } = Typography

const DRAWER_CLOSE=80
const DRAWER_OPEN=250
const { Header, Sider, Content } = Layout;

const CLayout =()=> {
    const history = useHistory()
    const [collapsed,setCollapsed] = useState<boolean>(false)
    const toggle = () =>     setCollapsed(!collapsed)
    const dispatch = useDispatch()
    const [ searchbarState, jobRule,auth] = useSelector<ApplicationState,[GlobalSearchBarState,JobRuleStateModel,AuthenticationState]>(appState=>[appState.globalSearchbarState,appState.job_rule,appState.auth])
    
    const PopoverContent:FC=()=> {
        return (
            <Row gutter={2}>
                <Col lg={{span:24}}>
                    <div style={{marginLeft:'30%'}}>
                        <Avatar size="large" icon="user" />
                    </div>
                </Col>
                <Col lg={{span:24}}>
                    <div style={{textAlign:'center'}}>
                        <Text strong>{auth.userName}</Text>
                    </div>
                </Col>
                <Col>
                    <Button onClick={() => {
                        dispatch(requestLogout())
                        history.replace("/")
                    }} type="link" icon='logout' block size={'large'}>
                        Logout
                    </Button>
                </Col>
            </Row>
        )
    };


    return (
        <Fragment>
            <Layout>
                <Sider style={{height: '100vh',position:'fixed'}} width={DRAWER_OPEN} trigger={null} theme={'dark'} collapsible collapsed={collapsed}>
                    <CDrawer/>
                </Sider>
                <Layout style={{marginLeft: collapsed?DRAWER_CLOSE:DRAWER_OPEN}}>
                    <Header style={{ background: '#fff',position: 'fixed', zIndex: 1, width: '100%',paddingLeft:'5px'}}>
                        <Row>
                            <Col span={2}>
                                <Icon
                                    style={{fontSize:20,marginTop:20}}
                                    type={collapsed ? 'menu-unfold' : 'menu-fold'}
                                    onClick={toggle}
                                />
                            </Col>
                            <Col span={12} offset={3}>
                               { searchbarState.visible && <CSearch size={'large'} placeholder={searchbarState.placeholder} onChange={searchbarState.changeHandler}/> }
                               { !searchbarState.visible && <Fragment/> }
                            </Col>
                            <Col span={2} lg={{}} offset={collapsed ? 5 : 3}>
                               <Popover placement="bottom" content={<PopoverContent/>} trigger="click">
                                        <Icon style={{fontSize:30,marginTop:15}} type="setting" />
                                </Popover>
                            </Col>
                        </Row>
                    </Header>

                    <Content
                        style={{
                            marginTop: '8vh',
                            padding: 7,
                            background: 'inherit',
                            overflow:'initial',
                            minHeight:'90vh'
                        }}
                    >
                        <BackTop/>
                        <Affix offsetTop={45}>
                            <CustomBreadcrumd/>
                        </Affix>
                        <AppRoute/>
                    </Content>

                </Layout>
            </Layout>
            <CDrawerModal/>
        </Fragment>
    );
}

export default CLayout
