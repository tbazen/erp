import React from 'react'
import { Result, Button, Icon, Typography } from 'antd'
const { Paragraph, Text } = Typography

interface  IProps {
    errorMessage ? : string
}

const OperationFailed = (props : IProps)=>{
return(
        <Result
            status="error"
            title="Failed To Process Request"
            subTitle=""
        >
            <div className="desc">
            <Paragraph>
                <Text
                strong
                style={{
                    fontSize: 16,
                }}
                >
                Reason for why you are getting this page might be:
                </Text>
            </Paragraph>
            <Paragraph>
                <Icon style={{ color: 'red' }} type="close-circle" /> {props.errorMessage}
            </Paragraph>
            </div>
        </Result>
    )
}
export default OperationFailed