import IconUser from './user.png'
import IconNewUser from './new-user.png'
import IconCustomerList  from './customer-list.png'
import IconNewLine from './new-line.png'
import IconTransferConnection from './transfer-connection.png'
import IconReadingCorrection from './reading-correction.png'
import IconBillingCredit from './billing-credit.png'
import IconCloseCredit from './close-credit-scheme.png'
import IconBillingDeposit from './billing-deposit.png'
import IconDeletebills from './delete-bills.png'
import IconExcemptBill from './exempt-bill-item.png'
import IconReturnMeter from './return-meter.png'
import IconOtherServices from './other-services.png'
import IconInternalServices from './internal-services.png'
import IconWorkerList from './worker-list.png'
import IconCustmerSponsoredNetwork from './customer-sponsored-network-expansion.png'
import IconGeneralPaidService from './general-paid-service.png'

/**
 * Internal services icons
 */
import IconEditCustomerData from './edit-customer-data.png'
import IconVoidReceipt from './void-receipt.png'
import IconCashhandover from './cash-handover.png'
import IconDisconnectLines from './disconnect-line.png'
import IconReconnectLine from './reconnect-line.png'
import IconPaymentCenter from './payment-center.png'

/**
 * Other services icons
 */
import IconGeneralcell from './general-cell.png'
import IconConnectionPenality from './connection-penality.png'
import IconHydrantService from './hydrant-service.png'
import IconLiquidDisposal from './liquid-disposal.png'
import IconMaintenance from './maintenance.png'
/**
 * Active jobs icon
 */
import IconJob from './job.png';

export const EditCustomerDataIcon = IconEditCustomerData
export const VoidReceiptIcon= IconVoidReceipt
export const CashhandoverIcon = IconCashhandover
export const IconDisconnectLinesIcon = IconDisconnectLines
export const IconReconnectLineIcon = IconReconnectLine
export const PaymentCenterIcon = IconPaymentCenter


export const CustomerListIcon =  IconCustomerList
export const NewLineIcon = IconNewLine
export const TransferConnectionIcon = IconTransferConnection
export const ReadingCorrectionIcon = IconReadingCorrection
export const BillingCreditIcon = IconBillingCredit
export const CloseCreditIcon = IconCloseCredit
export const BillingDepositIcon = IconBillingDeposit
export const DeleteBillsIcon = IconDeletebills
export const ExcemptBillIcon = IconExcemptBill
export const ReturnMeterIcon = IconReturnMeter
export const OtherServicesIcon = IconOtherServices
export const InternalServicesIcon = IconInternalServices
export const WorkerListIcon = IconWorkerList
export const CustomerSponsoredNetworkIcon = IconCustmerSponsoredNetwork
export const GeneralPaidServiceIcon= IconGeneralPaidService


export const GeneralCellIcon = IconGeneralPaidService
export const ConnectionPenalityIcon = IconConnectionPenality
export const HydrantServiceIcon = IconHydrantService
export const LiquidDisposalIcon = IconLiquidDisposal
export const MaintenanceIcon = IconMaintenance


/**
 * General Icons
 */
export const UserIcon = IconUser
export const NewUserIcon = IconNewUser

/**
 * Active Jobs Icon
 */

export const JobIcon = IconJob
