﻿using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using Microsoft.AspNetCore.Mvc;
using System;

namespace JobManagerClient.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrintServiceController : Controller
    {
        #region Print

        public class PrintPar
        {
            public CustomerPaymentReceipt PaymentReciept { get; set; }
            public PrintReceiptParameters RecieptParameters { get; set; }
            public bool Reprint { get; set; }
            public int NumberOfCopies { get; set; }
        }

        [HttpPost("[action]")]
        public IActionResult Print([FromBody]PrintPar param)
        {
            try
            {
                SubscriberManagmentClient.billingRuleClient.printReceipt(param.PaymentReciept, param.RecieptParameters, param.Reprint, param.NumberOfCopies);
                PrintLogData.WritePrintLog(param.PaymentReciept, param.RecieptParameters, param.NumberOfCopies, null, false);
                return Ok();
            }
            catch (Exception e)
            {
                PrintLogData.WritePrintLog(param.PaymentReciept, param.RecieptParameters, param.NumberOfCopies, e, false);
                return StatusCode(501, new { message = e.Message });
            }
        }
        #endregion
    }
}
