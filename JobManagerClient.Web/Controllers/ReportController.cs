﻿using INTAPS.Accounting;
using JobManagerClient.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;

namespace JobManagerClient.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : Controller
    {
        #region ImportReportDefinition

        public class ImportReportDefinitionPar
        {
            public ReportDefination Defination { get; set; }
            public string Base64ReportFile { get; set; }
        }

        [HttpPost("[action]")]
        public IActionResult ImportReportDefinition([FromBody] ImportReportDefinitionPar param)
        {
            try
            {
                string tempFilePath = AppData.CreateTemporaryFileFromBase64String(param.Base64ReportFile, ".reportDef");
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ReportDefination));
                System.IO.StreamReader sreader = System.IO.File.OpenText(tempFilePath);
                ReportDefination def = s.Deserialize(sreader) as ReportDefination;
                def.id = param.Defination.id;
                def.categoryID = param.Defination.categoryID;
                def.reportTypeID = param.Defination.reportTypeID;
                sreader.Close();
                System.IO.File.Delete(tempFilePath);
                return Ok(def);
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        #endregion
        #region ExportReportDefinition

        public class ExportReportDefinitionPar
        {
            public ReportDefination Defination { get; set; }
        }

        [HttpPost("[action]")]
        public IActionResult ExportReportDefinition([FromBody]ExportReportDefinitionPar param)
        {
            try
            {
                string tempFileName = Guid.NewGuid().ToString().Replace(":", String.Empty).Replace("/", String.Empty);
                string path = Path.Combine(AppData.AppDataTempDirectory, $"{tempFileName}.reportdef");
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ReportDefination));
                StreamWriter sw = System.IO.File.CreateText(path);
                s.Serialize(sw, param.Defination);
                sw.Close();
                byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                System.IO.File.Delete(path);
                return File(fileBytes, "application/unknown", $"{param.Defination.name}.reportDef");
            }
            catch (Exception e)
            {
                return StatusCode(501, new { message = e.Message });
            }
        }

        #endregion
    }
}
