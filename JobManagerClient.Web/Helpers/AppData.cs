﻿using System;
using System.IO;

namespace JobManagerClient.Web.Helpers
{
    public static class AppData
    {
        private const string intapsDir = "Intaps";
        private const string customerServicesWebDir = "Intaps/CustomerServicesWeb";

        static AppData()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string customerServicesPath = Path.Combine(folder, intapsDir);

            if (!Directory.Exists(customerServicesPath))
                Directory.CreateDirectory(customerServicesPath);
        }
        public static string AppDataPath
        {
            get
            {

                string folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                string datapath = Path.Combine(folder, customerServicesWebDir);

                if (!Directory.Exists(datapath))
                    Directory.CreateDirectory(datapath);
                return datapath;
            }
        }


        public static string AppDataTempDirectory
        {
            get
            {
                string tempPath = Path.Combine(AppDataPath, "temp");

                if (!Directory.Exists(tempPath))
                    Directory.CreateDirectory(tempPath);
                return tempPath;
            }
        }

        public static string CreateTemporaryFileFromBytes(byte[] fileBytes, string fileExtention)
        {
            string tempFilename = Guid.NewGuid().ToString().Replace(":", String.Empty).Replace("/", String.Empty);
            string filePath = Path.Combine(AppDataTempDirectory, $"{tempFilename}${fileExtention}");
            File.WriteAllBytes(filePath, fileBytes);
            return filePath;
        }
        public static string CreateTemporaryFileFromBase64String(string base64File, string fileExtention)
        {
            byte[] bytes = Convert.FromBase64String(base64File);
            return CreateTemporaryFileFromBytes(bytes, fileExtention);
        }


    }
}
