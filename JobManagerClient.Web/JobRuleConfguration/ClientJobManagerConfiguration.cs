﻿using INTAPS.SubscriberManagment.Client;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace JobManagerClient.Web.JobRuleConfguration
{
    public class ClientJobManagerConfiguration
    {
        private readonly ConfigurationModel clientConfiguration;
        private readonly string[] codeSnippit;
        private readonly string serverUri;

        public ClientJobManagerConfiguration(IConfiguration _configuration)
        {
            clientConfiguration = _configuration.GetSection("JobRuleConfig").Get<ConfigurationModel>();
            serverUri = _configuration.GetSection("ServerUri").Get<string>();
            codeSnippit = File.ReadAllLines(clientConfiguration.CodeSnippitFile);
        }

        public void ConnectToSubscriberManagment()
        {

            //  string assemblyName = Configuration.ConfigurationManager.AppSettings["billingRuleAssembly"];
            string assemblyPath = "bin\\Debug\\netcoreapp2.2\\ShashemeneBillingRuleClientCore.dll";
            byte[] assemblyByte = File.ReadAllBytes(assemblyPath);

            var loadedFromByte = Assembly.Load(assemblyByte);
            var loaded = Assembly.LoadFrom(assemblyPath);
            SubscriberManagmentClient.loadBillingRuleEngineClient(assemblyByte);
        }
        public void ConfigureClientApp()
        {
            var fileMap = new List<string[]>();
            var clientmoduleSource = GetClientModuleSource(clientConfiguration.SelectedClient);
            var moduleSources = new List<string>
            {
                clientConfiguration.StandardJobSource,
                clientmoduleSource
            };

            foreach (var moduleSorce in moduleSources)
            {
                var files = Directory.GetFiles($"{moduleSorce}", clientConfiguration.JobRuleFileExtention, SearchOption.AllDirectories);
                for (int i = 0; i < files.Length; i++)
                {
                    files[i] = files[i].Replace($"ClientApp/src", "../");
                    files[i] = files[i].Replace($"//", "/");
                    files[i] = files[i].Replace($"\\", "/");
                    files[i] = files[i].Replace(".tsx", "");
                    files[i] = files[i].Replace(".ts", "");
                }
                fileMap.Add(files);
            }

            File.Create($"{clientConfiguration.ConfigurationDestination}").Close();
            fileMap.ForEach(
                fileSet =>
                {

                    fileSet.ToList().ForEach(
                    modulePath =>
                    {
                        CreateTSMethod(modulePath);
                    });
                });
        }

        private string GetClientModuleSource(string selectedClient) => clientConfiguration.Clients[selectedClient];

        public void CreateTSMethod(string modulePath)
        {
            string[] snippit = new string[codeSnippit.Length];
            codeSnippit.CopyTo(snippit, 0);
            for (int i = 0; i < snippit.Length; i++)
            {
                string methodId = Guid.NewGuid().ToString();
                methodId = methodId.Replace("-", "");
                snippit[i] = snippit[i].Replace("METHOD_NAME", $"method_{methodId}");
                snippit[i] = snippit[i].Replace("MODULE_PATH", modulePath);
            }
            File.AppendAllLines($"{clientConfiguration.ConfigurationDestination}", snippit);
        }
    }
}
