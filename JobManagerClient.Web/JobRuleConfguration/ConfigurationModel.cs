﻿using System.Collections.Generic;

namespace JobManagerClient.Web.JobRuleConfguration
{
    public class ConfigurationModel
    {
        public string SelectedClient { get; set; }
        public string CodeSnippitFile { get; set; }
        public string ConfigurationDestination { get; set; }
        public string StandardJobSource { get; set; }
        public string JobRuleFileExtention { get; set; }

        public Dictionary<string, string> Clients { get; set; }

    }
}
