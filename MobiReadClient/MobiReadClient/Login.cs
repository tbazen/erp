
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using INTAPS.SubscriberManagment.Client;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
namespace MobiReadClient
{

    public partial class Login : BIZNET.iERP.Client.LoginWithUpdate
    {
        private Container components = null;
        public bool logedin;
        private bool m_boolValidUser = false;
        public Login()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            try
            {
                string url = ConfigurationManager.AppSettings["Server"];
                ApplicationClient.Connect(url, this.txtUserName.Text, this.txtPassword.Text);
                SecurityClient.Connect(url);
                INTAPS.Accounting.Client.AccountingClient.Connect(url);
                PayrollClient.Connect(url);
                BIZNET.iERP.Client.iERPTransactionClient.Connect(url);
                SubscriberManagmentClient.Connect(url);
                this.logedin = true;
                base.Close();
            }
            catch (Exception exception)
            {
                MessageBox.ShowErrorMessage(exception.Message);
                this.txtUserName.Focus();
                this.txtUserName.Text = "";
                this.txtPassword.Text = "";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public void TryLogin()
        {
            this.logedin = false;
            base.ShowDialog();
        }

        public bool ValidUser
        {
            get
            {
                return this.m_boolValidUser;
            }
        }
    }
}

