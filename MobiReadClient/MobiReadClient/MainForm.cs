﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using DevExpress.XtraEditors;
using INTAPS.Ethiopic;
using INTAPS.SubscriberManagment;
using DevExpress.XtraCharts;
using DevExpress.Utils.Drawing;
using DevExpress.Utils.Text;
using DevExpress.Utils;
using INTAPS.SubscriberManagment.Client;
using DevExpress.XtraBars;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace MobiReadClient
{
    public partial class MainForm : XtraForm, IReportSelector
    {
        public static int periodID = -1;
        private DataTable m_GridTable;
        public MainForm()
        {
            InitializeComponent();
            m_GridTable = new DataTable();
            PrepareDataTable();
            LoadReadingPeriods();
            SetDefaultBillingandReadingPeriod();
            LoadGridDashBoard();
            LoadChartDashBoard();
            AccountingClient.PopulateReportSelector(-1, this);
        }

        private void SetDefaultBillingandReadingPeriod()
        {
            SubscriberManagmentClient.SetSystemParameters(new string[] { "currentPeriod"
                    , "readingPeriod"
                }, new object[] { MainForm.periodID
                         , MainForm.periodID
                });
        }

        private void LoadChartDashBoard()
        {
            chartControl1.Series[0].Points.Clear();
            var readers = SubscriberManagmentClient.GetAllMeterReaderEmployees(MainForm.periodID);
            foreach (MeterReaderEmployee reader in readers)
            {
                var readerID = reader.employeeID;
                var readCount = SubscriberManagmentClient.getReaderCount(MainForm.periodID, readerID);
                if (readCount > 0)
                {
                    chartControl1.Series[0].Points.Add(new SeriesPoint(reader.emp.employeeName, readCount));
                }
            }
            chartControl1.Refresh();
            chartControl1.Update();
        }

        private void PrepareDataTable()
        {
            m_GridTable.Columns.Add("KETENA", typeof(string));
            m_GridTable.Columns.Add("NO OF SUBSCRIPTIONS", typeof(int));
            m_GridTable.Columns.Add("READ", typeof(int));
            m_GridTable.Columns.Add("UNREAD", typeof(int));
            m_GridTable.Columns.Add("READ PERCENTAGE", typeof(string));
            gridControl1.DataSource = m_GridTable;
        }

        private void LoadReadingPeriods()
        {
            try
            {
                var year = EtGrDate.ToEth(DateTime.Now).Year;
                var month = EtGrDate.ToEth(DateTime.Now).Month;
                if (month == 13)
                    month = 12;
                var periods = SubscriberManagmentClient.GetBillPeriods(year, true);
                var prevPeriods = SubscriberManagmentClient.GetBillPeriods(year - 1, true);
                foreach (BillPeriod period in prevPeriods)
                {
                    cmbPeriod.Properties.Items.Add(period);
                }
                foreach (BillPeriod period in periods)
                {
                    cmbPeriod.Properties.Items.Add(period);
                    if (EtGrDate.ToEth(period.fromDate).Month.Equals(month))
                    {
                        cmbPeriod.SelectedItem = period;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void tileReaders_ItemClick(object sender, TileItemEventArgs e)
        {
            var manager = new ReaderManager2();
            manager.ShowDialog();
        }

        private void tileTransferRead_ItemClick(object sender, TileItemEventArgs e)
        {
            var transferCustomer = new TransferCustomers();
            transferCustomer.ShowDialog();
        }

        private void cmbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            periodID = ((BillPeriod)cmbPeriod.SelectedItem).id;
            SetDefaultBillingandReadingPeriod();
            LoadGridDashBoard();
            LoadChartDashBoard();
            ControlTileVisibility();
        }

        private void tileReadingCycle_ItemClick(object sender, TileItemEventArgs e)
        {
            var cycle = new MobileReadingCycle();
            cycle.ShowDialog();
        }

        private void ControlTileVisibility()
        {
            tileTransferRead.Enabled = tileReaders.Enabled = tileReadingCycle.Enabled = tileUploadReading.Enabled = true;

            var ret = SubscriberManagmentClient.getReadingCycle((int)ReadingCycleStatus.Closed, MainForm.periodID);
            var disableControl = false;
            if (ret != null)
            {
                disableControl = true;
            }
            if (disableControl)
            {
                tileTransferRead.Enabled = tileReaders.Enabled = tileReadingCycle.Enabled = tileUploadReading.Enabled = false;
            }
        }

        private void LoadGridDashBoard()
        {
            var tbl = SubscriberManagmentClient.getReadingStatistics(MainForm.periodID);
            m_GridTable.Rows.Clear();
            foreach (DataRow row in tbl.Rows)
            {
                var newRow = m_GridTable.NewRow();
                Kebele keb = SubscriberManagmentClient.GetKebele(Convert.ToInt32(row[0]));
                newRow[0] = keb==null?"":keb.name;
                newRow[1] = Convert.ToInt32(row[1]);
                newRow[2] = Convert.ToInt32(row[3]);
                newRow[3] = Convert.ToInt32(row[1]) - Convert.ToInt32(row[3]);
                var percent = Math.Round((double.Parse(row[3].ToString()) * 100) / double.Parse(row[1].ToString()), 2);
                newRow[4] = percent + "%";
                m_GridTable.Rows.Add(newRow);
                gridControl1.DataSource = m_GridTable;
            }
            gridView1.BestFitColumns();
        }
        private void tileUploadReading_ItemClick(object sender, TileItemEventArgs e)
        {
            var uploadReading = new UploadMobileReading();
            uploadReading.ShowDialog();
        }

        private void gridView1_CustomDrawColumnHeader(object sender, DevExpress.XtraGrid.Views.Grid.ColumnHeaderCustomDrawEventArgs e)
        {
            e.Handled = true;
            e.Painter.DrawObject(e.Info);
            e.Cache.FillRectangle(new SolidBrush(Color.FromArgb(0x7B, 0x91, 0xCA)), new Rectangle(e.Bounds.X + 1, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height));
            var painter = new HeaderObjectPainter(e.Painter);
            var r = painter.GetObjectClientRectangle(e.Info);
            var foreBrush = e.Cache.GetSolidBrush(e.Appearance.ForeColor);
            var offs = new Point(r.X - e.Info.TopLeft.X, r.Y - e.Info.TopLeft.Y);

            e.Info.InnerElements.DrawObjects(e.Info, e.Cache, offs);
            if (e.Info.Caption.Length == 0 || e.Info.CaptionRect.IsEmpty)
            {
                return;
            }
            r = e.Info.CaptionRect;
            r.Offset(offs);
            if (e.Info.UseHtmlTextDraw)
            {
                StringPainter.Default.DrawString(e.Cache, e.Appearance, e.Info.Caption, r, DevExpress.Utils.TextOptions.DefaultOptionsNoWrapEx, e.Info.HtmlContext);
            }
            else
            {
                e.Info.Appearance.DrawString(e.Cache, e.Info.Caption, r, foreBrush, e.Info.Appearance.GetStringFormat(DevExpress.Utils.TextOptions.DefaultOptionsNoWrapEx));
            }
        }

        private void tileMap_ItemClick(object sender, TileItemEventArgs e)
        {
            initAllocationMapMen();
            var groupInfo = ((ITileControl)tileControl1).ViewInfo.Groups[0];
            popupMenu1.MinWidth = tileControl1.ItemSize;

            popupMenu1.ShowPopup(new Point(groupInfo.Items[4].Bounds.Left + 12, groupInfo.Items[4].Bounds.Height + 100));
        }
        private void initAllocationMapMen()
        {
            popupMenu1.ItemLinks.Clear();
            var allMetersItem = new BarButtonItem(barManager1, "All Meters");
            allMetersItem.ItemClick += allMetersItem_ItemClick;
            var readingProgItem = new BarButtonItem(barManager1, "Reading Progress");
            readingProgItem.ItemClick += readingProgItem_ItemClick;
            popupMenu1.AddItems(new BarItem[] { allMetersItem, readingProgItem });

            foreach (MeterReaderEmployee e in SubscriberManagmentClient.GetAllMeterReaderEmployees(MainForm.periodID))
            {
                var reader = new BarSubItem(barManager1, INTAPS.Payroll.Client.PayrollClient.GetEmployee(e.employeeID).employeeName);
                reader.Glyph = Properties.Resources.reader_list1;
                reader.Tag = e.employeeID;
                popupMenu1.AddItems(new BarItem[] { reader });

                var dates = SubscriberManagmentClient.getReadingDates(MainForm.periodID, e.employeeID);
                var readDates = new List<BarItem>();
                foreach (DateTime d in dates)
                {
                    var item = new BarButtonItem(barManager1, d.ToString("MMM dd/yyyy"));
                    item.Tag = new EmployeDate { date = d, employeeID = e.employeeID };
                    readDates.Add(item);
                    item.ItemClick += new ItemClickEventHandler(item_ItemClick);
                }
                reader.AddItems(readDates.ToArray());
            }
        }

        private void readingProgItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            SubscriberManagmentClient.showReadingMap(SubscriberManagmentClient.ReadingPeriod.id);
        }

        private void allMetersItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            SubscriberManagmentClient.showAllCustomersMap();
        }

        private void item_ItemClick(object sender, ItemClickEventArgs e)
        {
            var emp = (EmployeDate)e.Item.Tag;
            SubscriberManagmentClient.showReadingPath(SubscriberManagmentClient.ReadingPeriod.id, emp.employeeID, emp.date);
        }



        public object CreateCategoryItem(object parentItem, INTAPS.Accounting.ReportCategory rt)
        {
            if (parentItem == null)
            {
                var reader = new BarSubItem(barManager1, rt.name);
                reader.Glyph = Properties.Resources.reader_list1;
                reader.Tag = rt;
                popupMenu2.AddItems(new BarItem[] { reader });
                return reader;
            }
            else
            {
                var reader = new BarSubItem(barManager1, rt.name);
                reader.Glyph = Properties.Resources.reader_list1;
                reader.Tag = rt;
                ((BarSubItem)parentItem).AddItems(new BarItem[] { reader });
                return reader;
            }
        }
        private void OnShowReport(object sender, ItemClickEventArgs e)
        {
            try
            {
                ReportDefination def = ((BarButtonItem)e.Item).Tag as ReportDefination;
                System.Windows.Forms.Form f = INTAPS.Accounting.Client.AccountingClient.reportClientHandlers[def.reportTypeID].LoadReport(def);
                f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to load report form", ex);
            }
        }
        public object CreateReportItem(object parentItem, INTAPS.Accounting.ReportDefination rd, bool enabled)
        {
            var item = new BarButtonItem(barManager1, rd.name);
            item.Tag = rd;
            item.ItemClick += OnShowReport;
            ((BarSubItem)parentItem).AddItems(new BarItem[] { item });
            return item;
        }

        private void tileReport_ItemClick(object sender, TileItemEventArgs e)
        {
            var groupInfo = ((ITileControl)tileControl1).ViewInfo.Groups[0];

            popupMenu2.MinWidth = tileControl1.ItemSize;

            popupMenu2.ShowPopup(new Point(groupInfo.Items[5].Bounds.Left + 12, groupInfo.Items[5].Bounds.Height + 100));
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            LoadGridDashBoard();
            LoadChartDashBoard();
        }
    }
    internal class EmployeDate
    {
        public DateTime date;
        public int employeeID;
    }
}
