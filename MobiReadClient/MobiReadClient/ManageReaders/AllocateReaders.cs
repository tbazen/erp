﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Payroll.Client;
using DevExpress.XtraTreeList.Nodes;
using INTAPS.Ethiopic;
using DevExpress.XtraEditors.Repository;

namespace MobiReadClient
{
    public partial class AllocateReadersPage : DevExpress.XtraEditors.XtraUserControl
    {
        private DevExpress.XtraTreeList.Columns.TreeListColumn colReader;
        private DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        private RepositoryItemCheckEdit selectCheckMark;
        private DataTable m_tblReaders;
        private int m_selectedBlockID = -1;
        private BWFDescribedMeterReading[] m_ReadingArray;
        public AllocateReadersPage()
        {
            InitializeComponent();
            selectCheckMark = new RepositoryItemCheckEdit();
            gridControlReaders.RepositoryItems.Add(selectCheckMark);
            m_tblReaders = new DataTable();

            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(ReaderManager2.Manager, typeof(global::MobiReadClient.WaitForm1), true, true);
            colReader = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            colReader.Caption = "Readers";
            colReader.FieldName = "Readers";
            colReader.Name = "colReader";
            colReader.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            colReader.Visible = true;
            colReader.VisibleIndex = 1;
            colReader.Width = 240;
            treeView.Columns.Clear();
            treeView.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[]
            {
                colReader
            });
            treeView.OptionsBehavior.Editable = false;
            LoadReadingPeriods();
            PopulateKebeles();
            controlTransferButtonVisibility();
            PopulateCustomerTypes();
            LoadTree(-1);
        }

        private void controlTransferButtonVisibility()
        {
            var cycle = SubscriberManagmentClient.getReadingCycle(((BillPeriod)cmbPeriod.SelectedItem).id);
            if (cycle != null)
            {
                btnTransfer.Enabled = false;
            }
            else
            {
                btnTransfer.Enabled = true;
            }
        }

        private void prepareDataTable()
        {
            m_tblReaders.Columns.Add("Select", typeof(bool));
            m_tblReaders.Columns.Add("Block", typeof(string));
            m_tblReaders.Columns.Add("Customer Code", typeof(string));
            m_tblReaders.Columns.Add("Name", typeof(string));
            m_tblReaders.Columns.Add("Reading", typeof(double));
            m_tblReaders.Columns.Add("Reading Type", typeof(string));
            m_tblReaders.Columns.Add("Prevous Month", typeof(string));
            m_tblReaders.Columns.Add("Prevous Reading", typeof(double));
            m_tblReaders.Columns.Add("Consumption", typeof(double));
            m_tblReaders.Columns.Add("subscriptionID", typeof(int));
            

            gridControlReaders.DataSource = m_tblReaders;
            gridViewReaders.Columns["Block"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Customer Code"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Name"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Reading"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Reading Type"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Prevous Month"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Prevous Reading"].OptionsColumn.AllowEdit = false;
            gridViewReaders.Columns["Consumption"].OptionsColumn.AllowEdit = false;

            gridViewReaders.Columns["Select"].OptionsColumn.AllowSize = false;
            gridViewReaders.Columns["Select"].OptionsColumn.FixedWidth = true;
            gridViewReaders.Columns["Select"].Width = 40;
            gridViewReaders.Columns["Select"].ColumnEdit = selectCheckMark;

            gridViewReaders.Columns["subscriptionID"].Visible = false;
        }

        private void PopulateCustomerTypes()
        {
            cmbCustomerType.Properties.Items.Clear();
            cmbCustomerType.Properties.Items.Add("All");
            cmbCustomerType.Properties.Items.Add("Unknown");
            cmbCustomerType.Properties.Items.Add("Private");
            cmbCustomerType.Properties.Items.Add("Government");
            cmbCustomerType.Properties.Items.Add("Commercial");
            cmbCustomerType.Properties.Items.Add("NGO");
            cmbCustomerType.Properties.Items.Add("Religious");
            cmbCustomerType.Properties.Items.Add("Community");
            cmbCustomerType.Properties.Items.Add("Industry");
            cmbCustomerType.SelectedIndex = 0;
        }

        private void PopulateKebeles()
        {
            var kebeles = SubscriberManagmentClient.GetAllKebeles();
            cmbKebele.Properties.Items.Clear();
            cmbKebele.Properties.Items.Add("All");
            foreach (var kebele in kebeles)
            {
                cmbKebele.Properties.Items.Add(kebele);
            }
            cmbKebele.SelectedIndex = 0;
        }

        private void LoadReadingPeriods()
        {
            try
            {
                var year = EtGrDate.ToEth(DateTime.Now).Year;
                var month = EtGrDate.ToEth(DateTime.Now).Month;
                if (month == 13)
                    month = 12;
                var periods = SubscriberManagmentClient.GetBillPeriods(year, true);
                var prevPeriods = SubscriberManagmentClient.GetBillPeriods(year - 1, true);
                foreach (BillPeriod period in prevPeriods)
                {
                    cmbPeriod.Properties.Items.Add(period);
                }

                foreach (BillPeriod period in periods)
                {
                    cmbPeriod.Properties.Items.Add(period);
                    if (EtGrDate.ToEth(period.fromDate).Month.Equals(month))
                    {
                        cmbPeriod.SelectedItem = period;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void LoadTree(int selectBlockID)
        {
            treeView.Nodes.Clear();
            try
            {
                var meterReaders = SubscriberManagmentClient.GetAllMeterReaderEmployees(((BillPeriod)cmbPeriod.SelectedItem).id);
                foreach (MeterReaderEmployee employee in  meterReaders)
                {
                    if (employee.employeeID != -1)
                    {
                        var empNameID = PayrollClient.GetEmployee(employee.employeeID).EmployeeNameID;
                        var node = treeView.Nodes.Add(empNameID);
                        node.SetValue(colReader, empNameID);
                        node.StateImageIndex = 0;
                        node.Tag = employee;
                    }
                }
                object node2 = null;
                var readingBlocks = SubscriberManagmentClient.GetBlocks(((BillPeriod)cmbPeriod.SelectedItem).id, -1);
                foreach (BWFReadingBlock block in readingBlocks)
                {
                    var node4 = node2;
                    foreach (TreeListNode node5 in this.treeView.Nodes)
                    {
                        var tag = node5.Tag as MeterReaderEmployee;
                        if ((tag != null) && (tag.employeeID == block.readerID))
                        {
                            node4 = node5.Nodes;
                            break;
                        }
                    }
                    if ((node4 == node2))
                    {
                        var unknownNode = this.treeView.Nodes.Add("Unknown reader");
                        unknownNode.SetValue(colReader, "Unknown reader");
                        unknownNode.StateImageIndex = 0;
                        node2 = unknownNode.Nodes;
                    }
                    var node3 = ((TreeListNodes)node4).Add(block.blockName);
                    node3.SetValue(colReader, block.blockName);
                    node3.StateImageIndex = 1;
                    node3.Tag = block;
                    if (block.id == selectBlockID)
                    {
                        node3.ParentNode.ExpandAll();
                        node3.Selected = true;
                    }
                }
                var unallocatedNode = treeView.AppendNode(new object[] { "Unallocated" }, -1);
                unallocatedNode.StateImageIndex = 1;
                var unallocatedBlock = new BWFReadingBlock();
                unallocatedBlock.blockName = "Unallocated";
                unallocatedBlock.id = -1;
                unallocatedNode.Tag = unallocatedBlock;
            }
            catch (Exception exception)
            {
                MessageBox.ShowErrorMessage("Error loading reading structure." + exception.Message);
            }
            treeView.ExpandAll();
        }

        private void treeView_AfterFocusNode(object sender, DevExpress.XtraTreeList.NodeEventArgs e)
        {
            if ((e.Node.Tag != null) && (e.Node.Tag is BWFReadingBlock))
            {
                var block = (BWFReadingBlock)e.Node.Tag;
                lblDescription.Text = "Block: " + block.blockName;
                pageNavigator.RecordIndex = 0;
                m_selectedBlockID = block.id;
                LoadPage();
            }
            else
            {
                m_selectedBlockID = -1;
                m_tblReaders.Rows.Clear();
                gridControlReaders.DataSource = m_tblReaders;
            }
        }

        private void LoadPage()
        {
            try
            {
                int NRec;
                var kebele = cmbKebele.SelectedIndex == 0 ? -1 : ((Kebele)cmbKebele.SelectedItem).id;
                var subscriberType = cmbCustomerType.SelectedIndex == 0 ? -1 : cmbCustomerType.SelectedIndex - 1;
                waitScreen.ShowWaitForm();
                m_ReadingArray = SubscriberManagmentClient.BWFGetDescribedReadings(m_selectedBlockID, ((BillPeriod)cmbPeriod.SelectedItem).id, txtCode.Text, txtName.Text, kebele, subscriberType, pageNavigator.RecordIndex, pageNavigator.PageSize, out NRec);
                pageNavigator.NRec = NRec;
                m_tblReaders.Rows.Clear();
                INTAPS.CachedObject<int,BWFReadingBlock> blocks=new INTAPS.CachedObject<int,BWFReadingBlock>(
                    blockID=>SubscriberManagmentClient.GetBlock(blockID));

                foreach (var read in m_ReadingArray)
                {
                    var row = m_tblReaders.NewRow();
                    waitScreen.SetWaitFormDescription("loading subscriptions: " + read.customerName);
                    row[0] = false;
                    row[1] = read.reading.readingBlockID==-1?"Unallocated":blocks[read.reading.readingBlockID].blockName;
                    row[2] = read.contractNo;
                    row[3] = read.customerName;
                    row[4] = read.reading != null ? read.reading.reading : 0;
                    row[5] = read.reading != null ? read.reading.readingType.ToString() : MeterReadingType.Normal.ToString();
                    row[6] = read.previousPeriod != null ? read.previousPeriod.name : string.Empty;
                    row[7] = read.previousReading != null ? read.previousReading.reading : 0;
                    row[8] = read.reading != null ? read.reading.consumption : 0;
                    row[9] = read.reading.subscriptionID;
                    m_tblReaders.Rows.Add(row);
                    gridControlReaders.DataSource = m_tblReaders;
                    gridViewReaders.BestFitColumns();
                }
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                {
                    waitScreen.CloseWaitForm();
                }
            }
            if (chkSelectAll.Checked)
            {
                SelectAll();
            }
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            LoadPage();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            treeView.Selection.Clear();
            m_selectedBlockID = -2;
            LoadPage();
        }

        private void cmbPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            controlTransferButtonVisibility();
            LoadTree(-1);
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (gridViewReaders.DataRowCount > 0)
            {
                if (chkSelectAll.Checked)
                {
                    SelectAll();
                }
                else
                {
                    ClearSelection();
                }
            }
        }

        private void ClearSelection()
        {
            for (var i = 0; i < gridViewReaders.DataRowCount; i++)
            {
                gridViewReaders.SetRowCellValue(i, "Select", false);
            }
        }

        private void SelectAll()
        {
            for (var i = 0; i < gridViewReaders.DataRowCount; i++)
            {
                gridViewReaders.SetRowCellValue(i, "Select", true);
            }
        }

        private void AllocateReadersPage_Load(object sender, EventArgs e)
        {
            prepareDataTable();
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            int NRec;
            var subscriptions = new List<int>();
            var kebele = cmbKebele.SelectedIndex == 0 ? -1 : ((Kebele)cmbKebele.SelectedItem).id;
            var subscriberType = cmbCustomerType.SelectedIndex == 0 ? -1 : cmbCustomerType.SelectedIndex - 1;

            if (chkSelectAll.Checked)
            {
                try
                {
                    waitScreen.ShowWaitForm();
                    waitScreen.SetWaitFormDescription("Initialization transfer..");
                    m_ReadingArray = SubscriberManagmentClient.BWFGetDescribedReadings(m_selectedBlockID, ((BillPeriod)cmbPeriod.SelectedItem).id, txtCode.Text, txtName.Text, kebele, subscriberType, 0, 10000000, out NRec);

                    foreach (var s in m_ReadingArray)
                    {
                        subscriptions.Add(s.reading.subscriptionID);
                    }
                }
                finally
                {
                    if (waitScreen.IsSplashFormVisible)
                    {
                        waitScreen.CloseWaitForm();
                    }
                }
            }
            else
            {
                try
                {
                    waitScreen.ShowWaitForm();
                    waitScreen.SetWaitFormDescription("Initialization transfer..");

                    m_ReadingArray = SubscriberManagmentClient.BWFGetDescribedReadings(m_selectedBlockID, ((BillPeriod)cmbPeriod.SelectedItem).id, txtCode.Text, txtName.Text, kebele, subscriberType, pageNavigator.RecordIndex, pageNavigator.PageSize, out NRec);

                    for (var i = 0; i < gridViewReaders.DataRowCount; i++)
                    {
                        var selected = (bool)gridViewReaders.GetRowCellValue(i, "Select");
                        if (selected)
                        {
                            var id = (int)gridViewReaders.GetRowCellValue(i, "subscriptionID");
                            subscriptions.Add(id);
                        }
                    }
                }
                finally
                {
                    if (waitScreen.IsSplashFormVisible)
                    {
                        waitScreen.CloseWaitForm();
                    }
                }
            }
            if (subscriptions.Count > 0)
            {
                var picker = new ReaderPicker(subscriptions.ToArray(), ((BillPeriod)cmbPeriod.SelectedItem).id);
                if (picker.ShowDialog() == DialogResult.OK)
                {
                    LoadTree(m_selectedBlockID);
                }
            }
        }
    }
}
