﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using INTAPS.SubscriberManagment.Client;

namespace MobiReadClient
{
    public partial class ChangePassword : DevExpress.XtraEditors.XtraForm
    {
        private string userName;
        public ChangePassword(string user)
        {
            InitializeComponent();
            userName = user;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (dxValidationProvider1.Validate())
            {
                if (!txtNewPass.Text.Equals(txtConfirmPassword.Text))
                {
                    MessageBox.ShowErrorMessage("New password and confirm password should be the same");
                }
                else
                {
                    SubscriberManagmentClient.changePassword(userName, txtNewPass.Text);
                    DialogResult = DialogResult.OK;
                }
            }
            else
            {
                MessageBox.ShowErrorMessage("Please clear the errors marked in red and try again");
            }
        }
    }
}
