﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;

namespace MobiReadClient
{
    public partial class MobileReadersPage : DevExpress.XtraEditors.XtraUserControl
    {
        private DataTable m_readersTable;
        public MobileReadersPage()
        {
            InitializeComponent();
            m_readersTable = new DataTable();
            AddReadersToCurrentPeriod();
        }

        private void prepareDataTable()
        {
            m_readersTable.Columns.Add("Full Name", typeof(string));
            m_readersTable.Columns.Add("User Name", typeof(string));
            m_readersTable.Columns.Add("EmpID", typeof(int));
            gridControlReaders.DataSource = m_readersTable;
            gridViewReaders.Columns["EmpID"].Visible = false;
        }
        private void AddReadersToCurrentPeriod()
        {
            try
            {
                var clerks = SubscriberManagmentClient.GetAllReadingEntryClerk();
                foreach (ReadingEntryClerk clerk in clerks)
                {
                    var reader = SubscriberManagmentClient.GetMeterReaderEmployees(MainForm.periodID, clerk.employeeID);
                    if (reader == null)
                    {
                        SubscriberManagmentClient.CreateMeterReaderEmployee(new MeterReaderEmployee() { periodID = MainForm.periodID, employeeID = clerk.employeeID });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.ShowErrorMessage(ex.Message);
            }
        }

        private void GetAllReaders()
        {
            m_readersTable.Rows.Clear();
            var employees = SubscriberManagmentClient.GetAllMeterReaderEmployees(MainForm.periodID);
            foreach (MeterReaderEmployee emp in employees)
            {
                var row = m_readersTable.NewRow();
                row[0] = emp.emp.employeeName;
                row[1] = emp.emp.loginName;
                row[2] = emp.employeeID;
                m_readersTable.Rows.Add(row);
                gridControlReaders.DataSource = m_readersTable;
            }
        }

        private void btnNewReader_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var reader = new RegisterMobileReader();
            if (reader.ShowDialog() == DialogResult.OK)
            {
                MessageBox.ShowSuccessMessage("Reader successfully registered");
                GetAllReaders();
            }
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewReaders.SelectedRowsCount > 0)
            {
            }
        }

        private void btnChangePassword_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridViewReaders.SelectedRowsCount > 0)
            {
                var userName = (string)gridViewReaders.GetRowCellValue(gridViewReaders.FocusedRowHandle, "User Name");
                var changePass = new ChangePassword(userName);
                if (changePass.ShowDialog() == DialogResult.OK)
                {
                    MessageBox.ShowSuccessMessage("Password successfully changed!");
                }
            }
        }

        private void gridControlReaders_DoubleClick(object sender, EventArgs e)
        {
            int empID = (int)gridViewReaders.GetRowCellValue(gridViewReaders.FocusedRowHandle, "EmpID");
            RegisterMobileReader reader = new RegisterMobileReader(empID);
            if (reader.ShowDialog() == DialogResult.OK)
                MessageBox.ShowSuccessMessage("Reader information successfully updated");
        }

        private void MobileReadersPage_Load(object sender, EventArgs e)
        {
            prepareDataTable();
            GetAllReaders();
        }
    }
}
