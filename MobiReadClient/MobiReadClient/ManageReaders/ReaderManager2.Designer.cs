﻿namespace MobiReadClient
{
    partial class ReaderManager2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabReader = new System.Windows.Forms.TabPage();
            this.tapAssign = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabReader);
            this.tabControl1.Controls.Add(this.tapAssign);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 450);
            this.tabControl1.TabIndex = 0;
            // 
            // tabReader
            // 
            this.tabReader.Location = new System.Drawing.Point(4, 22);
            this.tabReader.Name = "tabReader";
            this.tabReader.Padding = new System.Windows.Forms.Padding(3);
            this.tabReader.Size = new System.Drawing.Size(792, 424);
            this.tabReader.TabIndex = 0;
            this.tabReader.Text = "Readers";
            this.tabReader.UseVisualStyleBackColor = true;
            // 
            // tapAssign
            // 
            this.tapAssign.Location = new System.Drawing.Point(4, 22);
            this.tapAssign.Name = "tapAssign";
            this.tapAssign.Padding = new System.Windows.Forms.Padding(3);
            this.tapAssign.Size = new System.Drawing.Size(792, 424);
            this.tapAssign.TabIndex = 1;
            this.tapAssign.Text = "Assignments";
            this.tapAssign.UseVisualStyleBackColor = true;
            // 
            // ReaderManager2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "ReaderManager2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reader Manager ";
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabReader;
        private System.Windows.Forms.TabPage tapAssign;
    }
}