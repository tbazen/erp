﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobiReadClient
{
    
    public partial class ReaderManager2 : Form
    {
        private MobileReadersPage readersPage;
        private AllocateReadersPage allocatePage;
        public static ReaderManager2 Manager;
        public ReaderManager2()
        {
            InitializeComponent();
            Manager = this;
            readersPage = new MobileReadersPage();
            allocatePage = new AllocateReadersPage();
            AddMobileReadersPage();
            AddAllocateReadersPage();
        }

        private void AddMobileReadersPage()
        {
            tabReader.Controls.Clear();
            tabReader.Controls.Add(readersPage);
            readersPage.Dock = DockStyle.Fill;
        }
        private void AddAllocateReadersPage()
        {
            tapAssign.Controls.Clear();
            tapAssign.Controls.Add(allocatePage);
            allocatePage.Dock = DockStyle.Fill;
        }
    }
}
