﻿using INTAPS.UI.Windows;
namespace MobiReadClient
{
    partial class ReaderPicker:AsyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cmbReaders = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnTransfer = new DevExpress.XtraEditors.SimpleButton();
            this.progressBar = new DevExpress.XtraEditors.ProgressBarControl();
            this.labelProgress = new DevExpress.XtraEditors.LabelControl();
            this.btnCLose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.cmbReaders.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Location = new System.Drawing.Point(160, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(44, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Reader:";
            // 
            // cmbReaders
            // 
            this.cmbReaders.Location = new System.Drawing.Point(205, 12);
            this.cmbReaders.Name = "cmbReaders";
            this.cmbReaders.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbReaders.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbReaders.Size = new System.Drawing.Size(225, 20);
            this.cmbReaders.TabIndex = 1;
            // 
            // btnTransfer
            // 
            this.btnTransfer.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnTransfer.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnTransfer.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnTransfer.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnTransfer.Appearance.Options.UseBackColor = true;
            this.btnTransfer.Appearance.Options.UseBorderColor = true;
            this.btnTransfer.Appearance.Options.UseFont = true;
            this.btnTransfer.Appearance.Options.UseForeColor = true;
            this.btnTransfer.Appearance.Options.UseImage = true;
            this.btnTransfer.Appearance.Options.UseTextOptions = true;
            this.btnTransfer.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnTransfer.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnTransfer.Image = global::MobiReadClient.Properties.Resources.transferR;
            this.btnTransfer.Location = new System.Drawing.Point(166, 88);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(98, 30);
            this.btnTransfer.TabIndex = 2;
            this.btnTransfer.Text = "&Transfer";
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressBar.BackgroundImage = global::MobiReadClient.Properties.Resources.bg2;
            this.progressBar.Location = new System.Drawing.Point(-1, 57);
            this.progressBar.Name = "progressBar";
            this.progressBar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.progressBar.Size = new System.Drawing.Size(548, 25);
            this.progressBar.TabIndex = 3;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelProgress.Location = new System.Drawing.Point(-1, 43);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(124, 13);
            this.labelProgress.TabIndex = 0;
            this.labelProgress.Text = "Transferring subscriptions";
            // 
            // btnCLose
            // 
            this.btnCLose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCLose.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnCLose.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnCLose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnCLose.Appearance.Options.UseBackColor = true;
            this.btnCLose.Appearance.Options.UseBorderColor = true;
            this.btnCLose.Appearance.Options.UseFont = true;
            this.btnCLose.Appearance.Options.UseForeColor = true;
            this.btnCLose.Appearance.Options.UseImage = true;
            this.btnCLose.Appearance.Options.UseTextOptions = true;
            this.btnCLose.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnCLose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnCLose.Image = global::MobiReadClient.Properties.Resources.close;
            this.btnCLose.Location = new System.Drawing.Point(280, 88);
            this.btnCLose.Name = "btnCLose";
            this.btnCLose.Size = new System.Drawing.Size(81, 30);
            this.btnCLose.TabIndex = 2;
            this.btnCLose.Text = "&Close";
            this.btnCLose.Click += new System.EventHandler(this.btnCLose_Click);
            // 
            // ReaderPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 117);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnCLose);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.cmbReaders);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.labelControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReaderPicker";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reader Picker";
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelProgress, 0);
            this.Controls.SetChildIndex(this.cmbReaders, 0);
            this.Controls.SetChildIndex(this.btnTransfer, 0);
            this.Controls.SetChildIndex(this.btnCLose, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            ((System.ComponentModel.ISupportInitialize)(this.cmbReaders.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ComboBoxEdit cmbReaders;
        private DevExpress.XtraEditors.SimpleButton btnTransfer;
        private DevExpress.XtraEditors.ProgressBarControl progressBar;
        private DevExpress.XtraEditors.LabelControl labelProgress;
        private DevExpress.XtraEditors.SimpleButton btnCLose;
    }
}