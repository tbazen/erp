﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using INTAPS.UI.Windows;
using INTAPS.Accounting.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.SubscriberManagment;

namespace MobiReadClient
{
    public partial class ReaderPicker : AsyncForm
    {
        private int[] _subscriptions;
        private int _periodID;
        public ReaderPicker(int[] subscriptions, int periodID)
        {
            InitializeComponent();
            _periodID = periodID;
            populateMeterReaders();
            _subscriptions = subscriptions;
        }

        private void populateMeterReaders()
        {
            var readers = SubscriberManagmentClient.GetAllMeterReaderEmployees(_periodID);
            foreach (var reader in readers)
            {
                cmbReaders.Properties.Items.Add(reader);
            }
            if (cmbReaders.Properties.Items.Count > 0)
            {
                cmbReaders.SelectedIndex = 0;
            }
        }
        protected override bool ShowAnmiation
        {
            get
            {
                return false;
            }
        }
        protected override void CheckProgress()
        {
            string str;
            progressBar.EditValue = (int)(100.0 * AccountingClient.GetProccessProgress(out str));
            labelProgress.Text = str;
            base.CheckProgress();
        }
        protected override void WaitIsOver(ServerDataDownloader d)
        {
            var job = (transferSubscriptionsJob)d;
            progressBar.EditValue = 0;
            labelProgress.Text = "Ready";
            if (job.error != null)
            {
                MessageBox.ShowErrorMessage("Error uploading readings" + job.error);
            }
            else
            {
                MessageBox.ShowSuccessMessage("Uploading reading successfully completed.");
                DialogResult = DialogResult.OK;
            }
            base.WaitIsOver(d);
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            if (cmbReaders.Properties.Items.Count == 0)
            {
                MessageBox.ShowErrorMessage("No reader found to transfers subscriptions to!");
            }
            else
            {
                base.WaitFor(new transferSubscriptionsJob(_subscriptions, ((MeterReaderEmployee)cmbReaders.SelectedItem).employeeID, _periodID));
            }
        }

        private void btnCLose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    internal class transferSubscriptionsJob : ServerDataDownloader
    {
        private int _periodID;
        private int[] _subscriptions;
        private int _readerID;
        public Exception error = null;

        public transferSubscriptionsJob(int[] subscriptions, int readerID, int periodID)
        {
            _periodID = periodID;
            _subscriptions = subscriptions;
            _readerID = readerID;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.transferSubscriptionsToReader(_subscriptions, _readerID, _periodID);
                error = null;
            }
            catch (Exception exception)
            {
                error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Transfer subscriptions";
            }
        }
    }
}
