﻿namespace MobiReadClient
{
    partial class ReadersManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navMobileReaders = new DevExpress.XtraNavBar.NavBarItem();
            this.navAllocateKebele = new DevExpress.XtraNavBar.NavBarItem();
            this.panConfigPages = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panConfigPages)).BeginInit();
            this.SuspendLayout();
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.AllowSelectedLink = true;
            this.navBarControl1.Appearance.GroupHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Appearance.GroupHeader.Options.UseFont = true;
            this.navBarControl1.Appearance.GroupHeaderActive.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Appearance.GroupHeaderActive.Options.UseFont = true;
            this.navBarControl1.Appearance.NavigationPaneHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Appearance.NavigationPaneHeader.ForeColor = System.Drawing.Color.Black;
            this.navBarControl1.Appearance.NavigationPaneHeader.Options.UseFont = true;
            this.navBarControl1.Appearance.NavigationPaneHeader.Options.UseForeColor = true;
            this.navBarControl1.BackColor = System.Drawing.Color.Transparent;
            this.navBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl1.ExplorerBarShowGroupButtons = false;
            this.navBarControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navAllocateKebele,
            this.navMobileReaders});
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 219;
            this.navBarControl1.OptionsNavPane.ShowExpandButton = false;
            this.navBarControl1.OptionsNavPane.ShowOverflowButton = false;
            this.navBarControl1.OptionsNavPane.ShowOverflowPanel = false;
            this.navBarControl1.OptionsNavPane.ShowSplitter = false;
            this.navBarControl1.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            this.navBarControl1.Size = new System.Drawing.Size(219, 493);
            this.navBarControl1.StoreDefaultPaintStyleName = true;
            this.navBarControl1.TabIndex = 5;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.navBarGroup1.Appearance.Options.UseFont = true;
            this.navBarGroup1.AppearanceBackground.BackColor = System.Drawing.Color.Transparent;
            this.navBarGroup1.AppearanceBackground.Image = global::MobiReadClient.Properties.Resources.bg2;
            this.navBarGroup1.AppearanceBackground.Options.UseBackColor = true;
            this.navBarGroup1.AppearanceBackground.Options.UseImage = true;
            this.navBarGroup1.BackgroundImage = global::MobiReadClient.Properties.Resources.bg2;
            this.navBarGroup1.Caption = "Task";
            this.navBarGroup1.DragDropFlags = DevExpress.XtraNavBar.NavBarDragDrop.None;
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.GroupCaptionUseImage = DevExpress.XtraNavBar.NavBarImage.Small;
            this.navBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.SmallIconsText;
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navMobileReaders),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navAllocateKebele)});
            this.navBarGroup1.Name = "navBarGroup1";
            this.navBarGroup1.NavigationPaneVisible = false;
            this.navBarGroup1.SelectedLinkIndex = 0;
            this.navBarGroup1.SmallImageIndex = 0;
            // 
            // navMobileReaders
            // 
            this.navMobileReaders.Caption = "Mobile Readers";
            this.navMobileReaders.Name = "navMobileReaders";
            this.navMobileReaders.SmallImage = global::MobiReadClient.Properties.Resources.reader_list;
            this.navMobileReaders.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navMobileReaders_LinkClicked);
            // 
            // navAllocateKebele
            // 
            this.navAllocateKebele.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.navAllocateKebele.Appearance.Image = global::MobiReadClient.Properties.Resources.assignReader;
            this.navAllocateKebele.Appearance.Options.UseImage = true;
            this.navAllocateKebele.CanDrag = false;
            this.navAllocateKebele.Caption = "Assign Readers";
            this.navAllocateKebele.LargeImage = global::MobiReadClient.Properties.Resources.assignReader;
            this.navAllocateKebele.Name = "navAllocateKebele";
            this.navAllocateKebele.SmallImage = global::MobiReadClient.Properties.Resources.assignReader;
            this.navAllocateKebele.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navAllocateKebele_LinkClicked);
            // 
            // panConfigPages
            // 
            this.panConfigPages.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panConfigPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panConfigPages.Location = new System.Drawing.Point(219, 0);
            this.panConfigPages.Name = "panConfigPages";
            this.panConfigPages.Size = new System.Drawing.Size(595, 493);
            this.panConfigPages.TabIndex = 7;
            // 
            // ReadersManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(814, 493);
            this.Controls.Add(this.panConfigPages);
            this.Controls.Add(this.navBarControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ReadersManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Readers";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panConfigPages)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navAllocateKebele;
        private DevExpress.XtraEditors.PanelControl panConfigPages;
        private DevExpress.XtraNavBar.NavBarItem navMobileReaders;

    }
}