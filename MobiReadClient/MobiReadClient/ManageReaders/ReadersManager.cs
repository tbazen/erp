﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using DevExpress.XtraEditors;

namespace MobiReadClient
{
    public partial class ReadersManager : XtraForm
    {
        private MobileReadersPage readersPage;
        private AllocateReadersPage allocatePage;
        public static ReadersManager Manager;
        public ReadersManager()
        {
            InitializeComponent();
            Manager = this;
            readersPage = new MobileReadersPage();
            allocatePage = new AllocateReadersPage();
            navMobileReaders_LinkClicked(null, null);
        }

        private void AddMobileReadersPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(readersPage);
            readersPage.Dock = DockStyle.Fill;
        }

        private void navMobileReaders_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddMobileReadersPage();
        }

        private void navAllocateKebele_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            AddAllocateReadersPage();
        }

        private void AddAllocateReadersPage()
        {
            panConfigPages.Controls.Clear();
            panConfigPages.Controls.Add(allocatePage);
            allocatePage.Dock = DockStyle.Fill;
        }
    }
}
