﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using INTAPS.Payroll;
using INTAPS.SubscriberManagment.Client;
using BIZNET.iERP.Client;
using INTAPS.Payroll.Client;

namespace MobiReadClient
{
    public partial class RegisterMobileReader : DevExpress.XtraEditors.XtraForm
    {
        int _employeeId = -1;
        public RegisterMobileReader()
        {
            InitializeComponent();
        }
        public RegisterMobileReader(int empID):this()
        {
            _employeeId = empID;
            Employee emp = PayrollClient.GetEmployee(empID);
            txtEmpID.Text = emp.employeeID;
            txtFullName.Text = emp.employeeName;
            cmbSex.SelectedIndex = (int)emp.sex;
            txtUserName.Text = emp.loginName;
            layoutPassword.HideToCustomization();
            layoutConfirmPassword.HideToCustomization();
            txtFullName.Properties.ReadOnly = true;
            cmbSex.Properties.ReadOnly = true;
            txtUserName.Properties.ReadOnly = true;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnRegsiter_Click(object sender, EventArgs e)
        {
            if (dxValidationProvider1.Validate())
            {
                try
                {
                    if (!txtPassword.Text.Equals(txtConfirmPassword.Text))
                    {
                        MessageBox.ShowErrorMessage("The password and confirm password values should be the same");
                        return;
                    }
                    //Employee emp = PayrollClient.GetEmployee(txtEmpID.Text);
                    //if (emp != null && emp.id != _employeeId)
                    //{
                    //    MessageBox.ShowErrorMessage("Another employee already exists with the same employee id");
                    //    return;
                    //}
                    var employee = _employeeId == -1 ? new Employee() : PayrollClient.GetEmployee(_employeeId);
                    employee.id = _employeeId;
                    employee.employeeID = txtEmpID.Text;
                    employee.BankAccountNo = string.Empty;
                    employee.loginName = txtUserName.Text;
                    employee.employeeName = txtFullName.Text;
                    employee.costCenterID = (int)iERPTransactionClient.GetSystemParameters(new string[] { "mainCostCenterID" })[0];
                    employee.birthDate = new DateTime(1980, 1, 1);
                    employee.enrollmentDate = DateTime.Now.Date;
                    employee.employmentType = EmploymentType.Permanent;
                    employee.sex = (Sex)cmbSex.SelectedIndex;
                    employee.status = EmployeeStatus.Enrolled;
                    employee.grossSalary = 1;
                    employee.ticksFrom = DateTime.Now.Ticks;
                    SubscriberManagmentClient.registerMobileReader(employee, txtPassword.Text, MainForm.periodID);
                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    MessageBox.ShowErrorMessage(ex.Message);
                }
            }
            else
            {
                MessageBox.ShowErrorMessage("Please clear the errors marked in red and try again!");
            }
        }
    }
}
