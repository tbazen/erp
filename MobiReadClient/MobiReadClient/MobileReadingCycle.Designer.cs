﻿using INTAPS.UI.Windows;
namespace MobiReadClient
{
    partial class MobileReadingCycle : AsyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelProgress = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnCreateCycle = new DevExpress.XtraEditors.SimpleButton();
            this.progressBar = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelProgress
            // 
            this.labelProgress.Appearance.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labelProgress.Location = new System.Drawing.Point(12, 12);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(130, 18);
            this.labelProgress.TabIndex = 0;
            this.labelProgress.Text = "Create Reading Cycle";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnClose.Appearance.Options.UseBackColor = true;
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Appearance.Options.UseForeColor = true;
            this.btnClose.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnClose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnClose.Image = global::MobiReadClient.Properties.Resources.close;
            this.btnClose.Location = new System.Drawing.Point(377, 154);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 30);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCreateCycle
            // 
            this.btnCreateCycle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCreateCycle.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnCreateCycle.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnCreateCycle.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnCreateCycle.Appearance.Options.UseBackColor = true;
            this.btnCreateCycle.Appearance.Options.UseFont = true;
            this.btnCreateCycle.Appearance.Options.UseForeColor = true;
            this.btnCreateCycle.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnCreateCycle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnCreateCycle.Image = global::MobiReadClient.Properties.Resources.creating;
            this.btnCreateCycle.Location = new System.Drawing.Point(195, 154);
            this.btnCreateCycle.Name = "btnCreateCycle";
            this.btnCreateCycle.Size = new System.Drawing.Size(176, 30);
            this.btnCreateCycle.TabIndex = 2;
            this.btnCreateCycle.Text = "C&reate Reading Cycle";
            this.btnCreateCycle.Click += new System.EventHandler(this.btnCreateCycle_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressBar.BackgroundImage = global::MobiReadClient.Properties.Resources.bg2;
            this.progressBar.Location = new System.Drawing.Point(12, 123);
            this.progressBar.Name = "progressBar";
            this.progressBar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.progressBar.Size = new System.Drawing.Size(623, 22);
            this.progressBar.TabIndex = 1;
            // 
            // MobileReadingCycle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 196);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCreateCycle);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.labelProgress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MobileReadingCycle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reading Cycle";
            this.Controls.SetChildIndex(this.labelProgress, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.btnCreateCycle, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelProgress;
        private DevExpress.XtraEditors.ProgressBarControl progressBar;
        private DevExpress.XtraEditors.SimpleButton btnCreateCycle;
        private DevExpress.XtraEditors.SimpleButton btnClose;
    }
}