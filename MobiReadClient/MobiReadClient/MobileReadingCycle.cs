﻿using System;
using System.Collections.Generic;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.Windows;
using INTAPS.Accounting.Client;
using INTAPS.UI;

namespace MobiReadClient
{
    public partial class MobileReadingCycle : AsyncForm
    {
        public MobileReadingCycle()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCreateCycle_Click(object sender, EventArgs e)
        {
            var ret = SubscriberManagmentClient.getReadingCycle(MainForm.periodID - 1, (int)ReadingCycleStatus.Open);
            if (ret != null)
            {
                SubscriberManagmentClient.closeReadingCycle(MainForm.periodID - 1);
            }
            base.WaitFor(new ReadingCycleJob(MainForm.periodID));
        }
        protected override void CheckProgress()
        {
            string str;
            progressBar.EditValue = (int)(100.0 * AccountingClient.GetProccessProgress(out str));
            labelProgress.Text = str;
            base.CheckProgress();
        }

        protected override void WaitIsOver(ServerDataDownloader d)
        {
            var job = (ReadingCycleJob)d;
            progressBar.EditValue = 0;
            labelProgress.Text = "Ready";
            if (job.error != null)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error creating reading cycle", job.error);
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Reading cycle successfully created.");
            }
            base.WaitIsOver(d);
        }
    }
    internal class ReadingCycleJob : ServerDataDownloader
    {
        private int _periodID;
        public Exception error = null;

        public ReadingCycleJob(int periodID)
        {
            _periodID = periodID;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.openReadingCycle(_periodID);
                error = null;
            }
            catch (Exception exception)
            {
                error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Creating reading cycle";
            }
        }
    }
}
