﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;
using System.Runtime.Remoting;
using System.Configuration;
using System.Reflection;
using INTAPS.ClientServer.Client;
using BIZNET.iERP;
using DevExpress.XtraEditors;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;
using INTAPS.UI;
using INTAPS.Payroll.Client;
using BIZNET.iERP.Client;

namespace MobiReadClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
   
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //InitializeFormSkinning();
            Login login = new Login();
            login.TryLogin();
            if (login.logedin)
            {
                MainForm main = new MainForm();
                new UIFormApplicationBase(main);
                Application.Run(main);
            }
        }
        private static void InitializeFormSkinning()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
            DevExpress.UserSkins.BonusSkins.Register();
        }
    }
    public class MessageBox
    {
        public static DialogResult ShowErrorMessage(string error)
        {
            return XtraMessageBox.Show(error, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        public static DialogResult ShowWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        public static DialogResult ShowNormalWarningMessage(string warning)
        {
            return XtraMessageBox.Show(warning, "Warning", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
        public static DialogResult ShowSuccessMessage(string success)
        {
            return XtraMessageBox.Show(success, "Success", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }

        internal static void ShowException(Exception ex)
        {
            string msg = null;
            while (ex != null)
            {
                if (ex is ServerUserMessage)
                {
                    ShowErrorMessage(ex.Message);
                    return;
                }

                if (ex is ServerValidationErrorException)
                {
                    ShowErrorMessage(ex.Message);
                    return;
                }
                if (ex is SystemConfigurationErrorException)
                {
                    MessageBox.ShowErrorMessage("System not properly configured. Additional info :" + ex.Message);
                    return;
                }
                if (msg == null)
                    msg = ex.Message;
                else
                    msg = ex.Message + "\n" + msg;
                ex = ex.InnerException;
            }
            MessageBox.ShowErrorMessage("Unknown system error, please report to system adminisrator.\n" + msg);
        }
    }

}
