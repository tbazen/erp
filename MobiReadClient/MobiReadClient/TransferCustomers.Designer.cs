﻿namespace MobiReadClient
{
    partial class TransferCustomers : INTAPS.UI.Windows.AsyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new DevExpress.XtraEditors.SimpleButton();
            this.progressBar = new DevExpress.XtraEditors.ProgressBarControl();
            this.btnCLose = new DevExpress.XtraEditors.SimpleButton();
            this.labelProgress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnStart.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnStart.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnStart.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnStart.Appearance.Options.UseBackColor = true;
            this.btnStart.Appearance.Options.UseFont = true;
            this.btnStart.Appearance.Options.UseForeColor = true;
            this.btnStart.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnStart.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnStart.Image = global::MobiReadClient.Properties.Resources.start2;
            this.btnStart.Location = new System.Drawing.Point(211, 147);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(79, 30);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "&Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressBar.BackgroundImage = global::MobiReadClient.Properties.Resources.bg2;
            this.progressBar.Location = new System.Drawing.Point(12, 120);
            this.progressBar.Name = "progressBar";
            this.progressBar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.Properties.Appearance.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.progressBar.Properties.Appearance.ForeColor2 = System.Drawing.Color.Lime;
            this.progressBar.Properties.Appearance.Image = global::MobiReadClient.Properties.Resources.bg2;
            this.progressBar.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.progressBar.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.progressBar.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.progressBar.Properties.EndColor = System.Drawing.Color.Silver;
            this.progressBar.Properties.LookAndFeel.SkinName = "Office 2010 Blue";
            this.progressBar.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.progressBar.Properties.StartColor = System.Drawing.Color.White;
            this.progressBar.Size = new System.Drawing.Size(623, 22);
            this.progressBar.TabIndex = 1;
            // 
            // btnCLose
            // 
            this.btnCLose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCLose.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnCLose.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnCLose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnCLose.Appearance.Options.UseBackColor = true;
            this.btnCLose.Appearance.Options.UseFont = true;
            this.btnCLose.Appearance.Options.UseForeColor = true;
            this.btnCLose.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnCLose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnCLose.Image = global::MobiReadClient.Properties.Resources.close;
            this.btnCLose.Location = new System.Drawing.Point(312, 147);
            this.btnCLose.Name = "btnCLose";
            this.btnCLose.Size = new System.Drawing.Size(80, 30);
            this.btnCLose.TabIndex = 2;
            this.btnCLose.Text = "&Close";
            this.btnCLose.Click += new System.EventHandler(this.btnCLose_Click);
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProgress.Location = new System.Drawing.Point(12, 32);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(172, 20);
            this.labelProgress.TabIndex = 3;
            this.labelProgress.Text = "Transferring customers";
            // 
            // TransferCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 188);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.btnCLose);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.progressBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TransferCustomers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "`";
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.btnStart, 0);
            this.Controls.SetChildIndex(this.btnCLose, 0);
            this.Controls.SetChildIndex(this.labelProgress, 0);
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.ProgressBarControl progressBar;
        private DevExpress.XtraEditors.SimpleButton btnStart;
        private DevExpress.XtraEditors.SimpleButton btnCLose;
        private System.Windows.Forms.Label labelProgress;
    }
}