﻿using System;
using System.Collections.Generic;
using System.Linq;
using INTAPS.UI.Windows;
using INTAPS.Accounting.Client;
using INTAPS.UI;
using INTAPS.SubscriberManagment.Client;

namespace MobiReadClient
{
    public partial class TransferCustomers : AsyncForm
    {
        public TransferCustomers()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            var cycle = SubscriberManagmentClient.getReadingCycle(MainForm.periodID);
            if (cycle != null)
            {
                MessageBox.ShowErrorMessage("A reading cycle already exists for the selected period hence transferring previous customer data is not allowed");
                return;
            }
            base.WaitFor(new TransferCustomersJob(MainForm.periodID));
        }

        protected override void CheckProgress()
        {
            string str;
            progressBar.EditValue = (int)(100.0 * AccountingClient.GetProccessProgress(out str));
            labelProgress.Text = str;
            base.CheckProgress();
        }

        protected override void WaitIsOver(ServerDataDownloader d)
        {
            var job = (TransferCustomersJob)d;
            progressBar.EditValue = 0;
            labelProgress.Text = "Ready";
            if (job.error != null)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error transferring data", job.error);
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Transferring customer data successfully completed.");
            }
            base.WaitIsOver(d);
        }

        private void btnCLose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
    internal class TransferCustomersJob : ServerDataDownloader
    {
        private int _periodID;
        public Exception error = null;

        public TransferCustomersJob(int periodID)
        {
            _periodID = periodID;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.transferCustomerData(_periodID);
                error = null;
            }
            catch (Exception exception)
            {
                error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Transferring customer data";
            }
        }
    }
}
