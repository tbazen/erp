﻿namespace MobiReadClient
{
    partial class UploadMobileReading : INTAPS.UI.Windows.AsyncForm
    {
        
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelProgress = new DevExpress.XtraEditors.LabelControl();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnUploadReading = new DevExpress.XtraEditors.SimpleButton();
            this.progressBar = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelProgress
            // 
            this.labelProgress.Appearance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProgress.Location = new System.Drawing.Point(24, 26);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(103, 19);
            this.labelProgress.TabIndex = 0;
            this.labelProgress.Text = "Upload Reading";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnClose.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnClose.Appearance.Options.UseBackColor = true;
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.Appearance.Options.UseForeColor = true;
            this.btnClose.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnClose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnClose.Image = global::MobiReadClient.Properties.Resources.close;
            this.btnClose.Location = new System.Drawing.Point(359, 151);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(78, 30);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "&Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUploadReading
            // 
            this.btnUploadReading.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnUploadReading.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.btnUploadReading.Appearance.Font = new System.Drawing.Font("Tahoma", 10.25F);
            this.btnUploadReading.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnUploadReading.Appearance.Options.UseBackColor = true;
            this.btnUploadReading.Appearance.Options.UseFont = true;
            this.btnUploadReading.Appearance.Options.UseForeColor = true;
            this.btnUploadReading.BackgroundImage = global::MobiReadClient.Properties.Resources.bg;
            this.btnUploadReading.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btnUploadReading.Image = global::MobiReadClient.Properties.Resources.upload1;
            this.btnUploadReading.Location = new System.Drawing.Point(198, 151);
            this.btnUploadReading.Name = "btnUploadReading";
            this.btnUploadReading.Size = new System.Drawing.Size(137, 30);
            this.btnUploadReading.TabIndex = 2;
            this.btnUploadReading.Text = "&Upload Reading";
            this.btnUploadReading.Click += new System.EventHandler(this.btnUploadReading_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressBar.BackgroundImage = global::MobiReadClient.Properties.Resources.bg2;
            this.progressBar.Location = new System.Drawing.Point(12, 126);
            this.progressBar.Name = "progressBar";
            this.progressBar.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.progressBar.Size = new System.Drawing.Size(623, 22);
            this.progressBar.TabIndex = 1;
            // 
            // UploadMobileReading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 203);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUploadReading);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.labelProgress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "UploadMobileReading";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UploadMobileReading";
            this.Controls.SetChildIndex(this.labelProgress, 0);
            this.Controls.SetChildIndex(this.progressBar, 0);
            this.Controls.SetChildIndex(this.btnUploadReading, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            ((System.ComponentModel.ISupportInitialize)(this.progressBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelProgress;
        private DevExpress.XtraEditors.ProgressBarControl progressBar;
        private DevExpress.XtraEditors.SimpleButton btnUploadReading;
        private DevExpress.XtraEditors.SimpleButton btnClose;
    }
}