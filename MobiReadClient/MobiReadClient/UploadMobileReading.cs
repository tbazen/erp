﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using INTAPS.UI.Windows;
using INTAPS.Accounting.Client;
using INTAPS.UI;
using INTAPS.SubscriberManagment.Client;

namespace MobiReadClient
{
    public partial class UploadMobileReading : AsyncForm
    {
        private IContainer components = null;
        public UploadMobileReading()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUploadReading_Click(object sender, EventArgs e)
        {
            base.WaitFor(new UploadReadingJob(MainForm.periodID));
        }

        protected override void CheckProgress()
        {
            string str;
            progressBar.EditValue = (int)(100.0 * AccountingClient.GetProccessProgress(out str));
            labelProgress.Text = str;
            base.CheckProgress();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        protected override void WaitIsOver(ServerDataDownloader d)
        {
            var job = (UploadReadingJob)d;
            progressBar.EditValue = 0;
            labelProgress.Text = "Ready";
            if (job.error != null)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error uploading readings", job.error);
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Uploading reading successfully completed.");
            }
            base.WaitIsOver(d);
        }
    }
    internal class UploadReadingJob : ServerDataDownloader
    {
        private int _periodID;
        public Exception error = null;

        public UploadReadingJob(int periodID)
        {
            _periodID = periodID;
        }

        public override void RunDownloader()
        {
            try
            {
                SubscriberManagmentClient.UploadReading(_periodID);
                error = null;
            }
            catch (Exception exception)
            {
                error = exception;
            }
        }

        public override string Description
        {
            get
            {
                return "Upload reading";
            }
        }
    }
}
