﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom;
using Microsoft.CodeAnalysis;

namespace NetCoreUpgradeTools
{
    class Program
    {
        static int nOkay = 0, nError = 0;
        static List<Tuple<Microsoft.CodeAnalysis.Text.TextSpan, String>> replacements = new List<Tuple<Microsoft.CodeAnalysis.Text.TextSpan, String>>();
        static void printSyntaxTree(SyntaxNode n, String prefix)
        {
         
            if(n is Microsoft.CodeAnalysis.CSharp.Syntax.MethodDeclarationSyntax)
            {
                var method = (Microsoft.CodeAnalysis.CSharp.Syntax.MethodDeclarationSyntax)n;
                bool isHttpPost = method.AttributeLists.Where(x => x.ToString().Equals("[HttpPost]")).Any();
                if (isHttpPost)
                {
                    //Console.WriteLine(n.ToFullString());
                    if(method.ReturnType.ToString().Equals("ActionResult"))
                    {
                        nOkay++;
                    }
                    else
                    {
                        var methodBody = "";
                        bool hasReturn = false;
                        foreach(var st in method.Body.ChildNodes())
                        {
                            if (st is Microsoft.CodeAnalysis.CSharp.Syntax.ReturnStatementSyntax)
                            {
                                var ret = (Microsoft.CodeAnalysis.CSharp.Syntax.ReturnStatementSyntax)st;
                                methodBody += $"return Json({ret.Expression.ToFullString()});";
                                hasReturn = true;
                            }
                            else
                                methodBody+= st.ToFullString();
                        }
                        if (!hasReturn)
                            methodBody += "\nreturn Ok();";
                        nError++;
                        var replacement = 
$@"
{method.AttributeLists}
{method.Modifiers} ActionResult {method.Identifier}{method.ParameterList}
{{
    try
    {{
        {methodBody}
    }}
    catch(Exception ex)
    {{
        return base.Exception(ex);
    }}
}}";
                        replacements.Add(new Tuple<Microsoft.CodeAnalysis.Text.TextSpan, string>(method.Span, replacement));
                        
                        /*var c = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(replacement);
                        Console.ForegroundColor = c;
                        */
                    }
                }
            }
            foreach (var ch in n.ChildNodes())
            {
                printSyntaxTree(ch, prefix);
                
            }
        }
        static void setAPIResturnActionResult()
        {
            String sourceFile;
            while (true)
            {
                //sourceFile = Console.ReadLine();
                sourceFile = @"C:\intaps_repo\erp\RESTServer\Controllers\SecurityController.cs";
                if (!System.IO.File.Exists(sourceFile))
                    Console.WriteLine($"File {sourceFile} doesn't exist");
                break;
            }
            try
            {
                String code;
                using (var s = new System.IO.StreamReader(sourceFile))
                {
                    code = s.ReadToEnd();
                }
                var t = Microsoft.CodeAnalysis.CSharp.CSharpSyntaxTree.ParseText(code);
                printSyntaxTree(t.GetRoot(), "");

                StringBuilder sb = new StringBuilder(code);
                for (var i = replacements.Count - 1; i >= 0; i--)
                {
                    var ov = code.Substring(replacements[i].Item1.Start, replacements[i].Item1.Length);
                    sb.Remove(replacements[i].Item1.Start, replacements[i].Item1.Length);
                    sb.Insert(replacements[i].Item1.Start, replacements[i].Item2);
                }
                Console.WriteLine(sb.ToString());
                Console.Write($"//{nOkay} Ok. {nError} not fixed. Replace?");
                if (Console.ReadLine().Equals("Y"))
                {
                    System.IO.File.WriteAllText(sourceFile, sb.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ReadLine();
            }
        }


        static void changeServerCall()
        {
            String clientFileSource;
            while (true)
            {
                //sourceFile = Console.ReadLine();
                clientFileSource = @"C:\source\code.core\Accounting\PayrollService\PayrollController.cs";
                if (!System.IO.File.Exists(clientFileSource))
                    Console.WriteLine($"File {clientFileSource} doesn't exist");
                break;
            }

            String[] serverFileSource;
            while (true)
            {
                //sourceFile = Console.ReadLine();
                serverFileSource = new string[]{
                    @"C:\source\code.core\Accounting\PayrollService\PayrollController.cs"
                };
                var allOk = true;
                foreach (var f in serverFileSource)
                {
                    if (!System.IO.File.Exists(f))
                    {
                        allOk = false;
                        Console.WriteLine($"File {f} doesn't exist");
                        break;
                    }                        
                }
                if(allOk)
                    break;
            }
            try
            {
                String code;
                using (var s = new System.IO.StreamReader(clientFileSource))
                {
                    code = s.ReadToEnd();
                }
                var t = Microsoft.CodeAnalysis.CSharp.CSharpSyntaxTree.ParseText(code);
                printSyntaxTree(t.GetRoot(), "");

                StringBuilder sb = new StringBuilder(code);
                for (var i = replacements.Count - 1; i >= 0; i--)
                {
                    var ov = code.Substring(replacements[i].Item1.Start, replacements[i].Item1.Length);
                    sb.Remove(replacements[i].Item1.Start, replacements[i].Item1.Length);
                    sb.Insert(replacements[i].Item1.Start, replacements[i].Item2);
                }
                Console.WriteLine(sb.ToString());
                Console.Write($"//{nOkay} Ok. {nError} not fixed. Replace?");
                if (Console.ReadLine().Equals("Y"))
                {
                    System.IO.File.WriteAllText(clientFileSource, sb.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.ReadLine();
            }
        }
        static void Main(string[] args)
        {
            setAPIResturnActionResult();
        }
    }
}
