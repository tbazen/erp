﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace OutstandingBillEditorTool
{
    public partial class OutstandingBillEditor : DevExpress.XtraEditors.XtraForm
    {
        DevExpress.XtraSplashScreen.SplashScreenManager waitScreen;
        DataTable m_OutstandingTable = null;
        public OutstandingBillEditor()
        {
            InitializeComponent();
            waitScreen = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::OutstandingBillEditorTool.WaitForm1), true, true);
            prepareDataTable();
        }

        private void prepareDataTable()
        {
            m_OutstandingTable = new DataTable();
            m_OutstandingTable.Columns.Add("Period", typeof(string));
            m_OutstandingTable.Columns.Add("Meter Rent", typeof(double));
            m_OutstandingTable.Columns.Add("Bill Amount", typeof(double));
            m_OutstandingTable.Columns.Add("LegacyBillID", typeof(int));
            m_OutstandingTable.Columns.Add("subscriptionID", typeof(int)); 
            m_OutstandingTable.Columns.Add("subscriberID", typeof(int));
            m_OutstandingTable.Columns.Add("periodID", typeof(int));

            gridControlOutstanding.DataSource = m_OutstandingTable;
            gridViewOutstanding.Columns["LegacyBillID"].Visible = false;
            gridViewOutstanding.Columns["subscriptionID"].Visible = false;
            gridViewOutstanding.Columns["subscriberID"].Visible = false;
            gridViewOutstanding.Columns["periodID"].Visible = false;
            gridViewOutstanding.Columns["Period"].OptionsColumn.AllowEdit = false;
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                waitScreen.ShowWaitForm();
                waitScreen.SetWaitFormDescription("Searching Customer...");

                if (string.IsNullOrEmpty(txtContractNo.Text))
                {
                    XtraMessageBox.Show("Please enter contract number!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                loadOutstandingBills();
            }
            finally
            {
                if (waitScreen.IsSplashFormVisible)
                    waitScreen.CloseWaitForm();
            }
        }

        private void loadOutstandingBills()
        {
            m_OutstandingTable.Rows.Clear();
            gridControlOutstanding.DataSource = m_OutstandingTable;

            //Load all outstanding bills for the contract no
            string sql = String.Format("select id,subscriberID from Subscription where contractNo='{0}' and timeBinding=1", txtContractNo.Text);
            DataTable result = Program.DBConnection.GetDataTable(sql,false);
            if (result == null || result.Rows.Count==0)
            {
                XtraMessageBox.Show("Contract number not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int subscriptionID = (int)result.Rows[0][0];
            int subscriberID=(int)result.Rows[0][1];
            string sqlLegacy = "select * from LegacyBill where customerID=" + subscriptionID;
            DataTable table = Program.DBConnection.GetDataTable(sqlLegacy, false);
            foreach (DataRow row in table.Rows)
            {
                int periodID = (int)row[2];
                string periodName = getPeriodName(periodID);
                double meterRentAmount = getMeterRentAmount((int)row[0]);
                double billAmount = getBillAmount((int)row[0]);
                DataRow gridRow = m_OutstandingTable.NewRow();
                gridRow[0] = periodName;
                gridRow[1] = meterRentAmount;
                gridRow[2] = billAmount;
                gridRow[3] = (int)row[0];
                gridRow[4] = subscriptionID;
                gridRow[5] = subscriberID;
                gridRow[6] = periodID;
                m_OutstandingTable.Rows.Add(gridRow);
            }
            gridControlOutstanding.DataSource = m_OutstandingTable;
        }

        private double getBillAmount(int legacyBillID)
        {
            string sql = String.Format("Select amount from LegacyBillItem where legacyBillID={0} and itemID=1", legacyBillID);
            object result = Program.DBConnection.ExecuteScalar(sql);
            if (result == null)
                return 0;
            else
                return (double)result;
        }

        private double getMeterRentAmount(int legacyBillID)
        {
            string sql = String.Format("Select amount from LegacyBillItem where legacyBillID={0} and itemID=2", legacyBillID);
            object result = Program.DBConnection.ExecuteScalar(sql);
            if (result == null)
                return 0;
            else
                return (double)result;
        }

        private string getPeriodName(int periodID)
        {
            string sql = "select name from BillPeriod where id=" + periodID;
            string periodName = (string)Program.DBConnection.ExecuteScalar(sql);
            return periodName;
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
               
                Program.DBConnection.BeginTransaction();
                for (int i = 0; i < gridViewOutstanding.DataRowCount; i++)
                {
                    int legacyBillID = (int)gridViewOutstanding.GetRowCellValue(i, "LegacyBillID");
                    string periodName = (string)gridViewOutstanding.GetRowCellValue(i, "Period");
                    double meterRent = (double)gridViewOutstanding.GetRowCellValue(i, "Meter Rent");
                    double billAmount = (double)gridViewOutstanding.GetRowCellValue(i, "Bill Amount");
                    int connectionID = (int)gridViewOutstanding.GetRowCellValue(i, "subscriptionID");
                    int subscriberID = (int)gridViewOutstanding.GetRowCellValue(i, "subscriberID");
                    int periodID = (int)gridViewOutstanding.GetRowCellValue(i, "periodID");
                    if (isBillGenerated(connectionID, subscriberID, periodID))
                        throw new Exception("Outstanding bill is already generated for " + periodName);
                    if (meterRent != 0)
                    {
                        string sql = String.Format("Update LegacyBillItem Set amount={0} where legacyBillID={1} and itemID=2", meterRent, legacyBillID);
                        Program.DBConnection.ExecuteScalar(sql);
                    }
                    if (billAmount != 0)
                    {
                        string sql = String.Format("Update LegacyBillItem Set amount={0} where legacyBillID={1} and itemID=1", billAmount, legacyBillID);
                        Program.DBConnection.ExecuteScalar(sql);
                    }
                }
                Program.DBConnection.CommitTransaction();
                XtraMessageBox.Show("Bill successfully updated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                Program.DBConnection.RollBackTransaction();
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private bool isBillGenerated(int subscriptionID, int subscriberID, int periodID)
        {
            string sql = String.Format("select count(*) from CustomerBill where billDocumentTypeID=5 and connectionID={0} and customerID ={1} and periodID={2}", subscriptionID, subscriberID, periodID);
            int count = (int)Program.DBConnection.ExecuteScalar(sql);
            if (count > 0)
                return true;
            return false;
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                int legacyBillID = (int)gridViewOutstanding.GetRowCellValue(gridViewOutstanding.FocusedRowHandle, "LegacyBillID");
                int connectionID = (int)gridViewOutstanding.GetRowCellValue(gridViewOutstanding.FocusedRowHandle, "subscriptionID");
                int subscriberID = (int)gridViewOutstanding.GetRowCellValue(gridViewOutstanding.FocusedRowHandle, "subscriberID");
                string periodName = (string)gridViewOutstanding.GetRowCellValue(gridViewOutstanding.FocusedRowHandle, "Period");
                int periodID = (int)gridViewOutstanding.GetRowCellValue(gridViewOutstanding.FocusedRowHandle, "periodID");
                if (isBillGenerated(connectionID, subscriberID, periodID))
                    throw new Exception("Outstanding bill is already generated for " + periodName);
                if (XtraMessageBox.Show(String.Format("Are you sure you want to delete bill for {0}?", periodName), "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    string sql = "Delete from LegacyBill where id=" + legacyBillID;
                    string sql2 = "Delete from LegacyBillItem where legacyBillID=" + legacyBillID;
                    Program.DBConnection.BeginTransaction();
                    Program.DBConnection.ExecuteScalar(sql2);
                    Program.DBConnection.ExecuteScalar(sql);
                    Program.DBConnection.CommitTransaction();
                    gridViewOutstanding.DeleteSelectedRows();
                    XtraMessageBox.Show("Bill successfully Deleted!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
              
            }
            catch (Exception ex)
            {
                Program.DBConnection.RollBackTransaction();
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}