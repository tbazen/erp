﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using PWF.domain;
using PWF.domain.Admin;
using PWF.domain.Infrastructure;
using PWF.api.Helpers;
using intaps.cis.Extensions;
using Microsoft.AspNetCore.Http;
using PWF.domain.Payment.StateMachines;
using PWF.data;

namespace PWF.api.Controllers
{
    [Route("api/pwf/[controller]")]
    public class AuthController : BaseController
    {
        private IUserFacade _userFacade;
        private IUserService _userService;
        private PWFConfiguration Config;
        private readonly AppSettings _appSettings;
        private PWFContext _context;
        public AuthController(PWFContext Context,IUserFacade userFacade, IOptions<PWFConfiguration> config, IOptions<AppSettings> appSettings,IUserService _service)
        {
            _userFacade = userFacade;
            Config = config.Value;
            _appSettings = appSettings.Value;
            _userService = _service;
            _context = Context;
        }

        [HttpGet("[action]")]
        public IActionResult Test(Guid id)
        {
            //var name = typeof(PaymentWorkflow).Name;
            //var type = Type.GetType($"PWF.domain.Payment.StateMachines.{name}, PWF.domain");

            //System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(name);
            //PaymentWorkflow wf = (PaymentWorkflow)Activator.CreateInstance(type);
            //wf.SetSession(GetSession());
            //wf.SetContext(_context);
            // wf.ConfigureMachine(id);
            //return Json(wf.GetData());
            //var i = id;
            var result = _context.Workflow.ToList();
            return Json(result);
        }

        [HttpPost("[action]")]
        public IActionResult CheckAuthentication([FromBody] LoginViewModel Model)
        {
            try
            {
                
            var response = _userService.CheckAuthentication(Model.username, Model.password);
                return Json(new
                {
                    status = true,
                    response = response
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
            
        }


        [HttpPost("[action]")]
        public IActionResult Login([FromBody] LoginViewModel Model)
        {
            try
            {
                var session = _userFacade.LoginUser(null, Model, Config.Host);
                session.Role = (long)Model.Role;
                session.Token = "";
                var serializedSession = JsonConvert.SerializeObject(session);
                HttpContext.Session.SetSession("pwfSession", session);
                
                return Ok(new
                {
                    status = true,
                    message = "Successful",
                    response = session
                });

            }
            catch (Exception ex)
            {

                return Ok(new
                {
                    status = false,
                    error = ex.Message
                });
            }



        }


        [HttpGet("[action]")]
        public IActionResult SetRole(long role)
        {
            var claims = User.Claims;
            var session = GetSession();
            session.Role = role; 
            var sess = JsonConvert.SerializeObject(session);
            //(User.Identity as ClaimsIdentity).RemoveClaim(((User.Identity) as ClaimsIdentity)
            //    .FindFirst("Session"));
            (User.Identity as ClaimsIdentity).AddClaim(new Claim("Session", sess));
            return Ok(new
            {
                status = "Success",
                message = "Successful",
                session = session
            });
        }

        [HttpGet("[action]")]
        public UserSession Who()
        {
            //var headers = Request.Headers;
            //var claims = HttpContext.User.Claims;
            //var cl = User.Claims;
            //var session = cl.Where(m => m.Type == "Session").First().Value;
            //return JsonConvert.DeserializeObject<UserSession>(session);
            var r = Request;
            var x = User.Identity as ClaimsIdentity;
            return HttpContext.Session.GetSession<UserSession>("pwfSession");
        }

        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();

            return Json(true);
        }
    }
}