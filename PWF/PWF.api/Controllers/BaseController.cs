﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using intaps.cis.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PWF.api.Helpers;
using PWF.domain.Infrastructure;
using PWF.domain.Payment.StateMachines;
using PWF.domain.Store;

namespace PWF.api.Controllers
{



    public class BaseController : Controller
    {

        public UserSession GetSession()
        {

          var session =  HttpContext.Session.GetString("pwfSession");
            if(session == null)
            {
                //throw new Exception("Not Authenticated Exception");
                throw new NotAuthenticatedException("Not Authenticated");
            }
            return JsonConvert.DeserializeObject<UserSession>(session);
        }

        public APIResponseModel ErrorResponse(Exception ex)
        {
            
            return new APIResponseModel() {
                status = false,
                error = ex.Message
            };

        }

        public APIResponseModel SuccessfulResponse(Object response)
        {
            return new APIResponseModel() {
                status = true,
                response = response
            };

        }

    }
}