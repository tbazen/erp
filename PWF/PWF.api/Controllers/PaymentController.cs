﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIZNET.iERP;
using INTAPS.Accounting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PWF.data.Entities;
using PWF.domain;
using PWF.domain.Documents;
using PWF.domain.Payment;
using PWF.domain.Payment.Models;
using PWF.domain.Store;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using DocumentType = INTAPS.Accounting.DocumentType;

namespace PWF.api.Controllers
{

    [Route("api/pwf/[controller]")]
    [ApiController]
    public class PaymentController : BaseController
    {

        public PWFConfiguration _config;
        private IPaymentService _paymentService;
        private IPaymentFacade _paymentFacade;
        private IWorkflowFacade _wfFacade;
        private IWorkflowService _wfService;
        private IDocumentService _documentService;
        private IWorkflowHelper _wfHelper;
        public PaymentController(IOptions<PWFConfiguration> Config, IPaymentFacade facade, IPaymentService service, IWorkflowFacade Service
            , IDocumentService docService, IWorkflowService wfService, IWorkflowHelper wfHelper
            )
        {
            _paymentService = service;
            _config = Config.Value;
            _wfFacade = Service;
            _paymentFacade = facade;
            _documentService = docService;
            _wfService = wfService;
            _wfHelper = wfHelper;
        }


        [HttpPost("[action]")]
        public IActionResult CreatePaymentRequest(PaymentRequest Request)
        {

            try
            {

                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _documentService.SetSessionConfiguration(_config, GetSession());
                var doc = Request.Files;
                doc.Documenttypeid = (int)DocumentTypes.PaymentRequestDocument;
                doc.Filename = doc.Name;
                var d = _documentService.AddDocument(doc);
                Request.Files = d;
                var result = _paymentFacade.CreatePaymentRequest(Request, Request.Note);
               // doc.WorkflowId = result;
                

                return Json(SuccessfulResponse(result));
            }
            catch (Exception e)
            {
                return Json(ErrorResponse(e));
               // throw;
            }

        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult ResubmitPaymentRequest(Guid wfid, PaymentRequest Request)
        {

            try
            {

                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _documentService.SetSessionConfiguration(_config, GetSession());
                var doc = Request.Files;
                doc.Documenttypeid = (int)DocumentTypes.PaymentRequestDocument;
                doc.Filename = doc.Name;
                var d = _documentService.AddDocument(doc);
                Request.Files = d;
                _paymentFacade.ResubmitPaymentRequest(wfid, Request, Request.Note);
                // doc.WorkflowId = result;


                return Json(SuccessfulResponse(""));
            }
            catch (Exception e)
            {
                return Json(ErrorResponse(e));
                // throw;
            }

        }

        [HttpPost("[action]")]
        public IActionResult SearchDocuments(DocumentSearchModel model)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var resonse = _paymentFacade.SearchDocuments(model);
                return Json(resonse);
            }
            catch (Exception e)
            {
                return Json(new
                {
                    status = true,
                    error = e.Message
                });
                // throw;
            }



        }

        [HttpPost("[action]")]
        public IActionResult UploadAttachment(PaymentRequestModel Doc)
        {
            return Ok(Doc);
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult AddDocumentAttachment(Guid wfid,[FromBody] Document Doc)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                if(Doc.File == null || Doc.File.Length == 0)
                {
                    throw new Exception("Invalid Document Attached");
                }
                var response = _paymentFacade.AddAttachmentDocuments(wfid, Doc);
                return Json(new
                {
                    status = true,
                    response = response
                });

            }
            catch (Exception e)
            {
                return Json(new
                {
                    status = true,
                    error = e.Message
                });
                // throw;
            }
        }

        [HttpGet("[action]/{wfid}/{docID}")]
        public IActionResult RemoveAttachedDocument(Guid wfid, int docID)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var response = _paymentFacade.RemoveAttachedDocuments(wfid, docID);
                return Json(new
                {
                    status = true,
                    response = response
                });

            }
            catch (Exception e)
            {
                return Json(new
                {
                    status = true,
                    error = e.Message
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult FinHeadReject(Guid wfid, string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _paymentFacade.RejectByFinHead(wfid, description);
                return Json(new
                {
                    status = true
                });

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult SearchAccount(string query)
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.SearchAccountByCode(query);
                return Json(new
                {
                    status = true,
                    response = result
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult SearchCostCenter(string query)
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.SearchCostCenter(query);
                return Json(new
                {
                    status = true,
                    response = result
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult CloseRequest(Guid wfid,Document SignedDocument)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _paymentFacade.CloseRequest(wfid, SignedDocument.Description, SignedDocument);
                return Json(new
                {
                    status = true
                });

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult FinalizeRequest(Guid wfid,string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _paymentFacade.FinalizeRequest(wfid, description);
                return Json(new
                {
                    status = true
                });

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult RejectFinalCheck(Guid wfid, string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _paymentFacade.RejectFinalCheck(wfid, description);
                return Json(new
                {
                    status = true
                });

            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult TestMethod(Guid id)
        {
            _paymentFacade.SetSessionConfiguration(_config, GetSession());
            var result = _paymentFacade.TestMethod(id);
            return Json(result);
        }


        [HttpPost("[action]")]
        public IActionResult TestPost([FromBody] PaymentSourceModel Model)
        {
            return Ok(Model);
        }

        public class Source
        {
            public double Amount { get; set; }
        }

        [HttpPost("[action]")]
        public IActionResult Check([FromBody] Source source)
        {
            return Ok(source);
        }

       

        [HttpPost("[action]/{wfid}/{description}")]
        public IActionResult CheckPaymentRequest(Guid wfid, string description,PaymentRequestModel Model)
        {
            try
            {
                var Sources = Model.Sources;
                if (Sources == null || Sources.Count() == 0)
                {
                    throw new Exception("Payment Source is required");
                }
                if(Model.BudgetDocument.transactions.Length == 0)
                {
                    throw new Exception("Please add Budget Transaction Document");
                }
                if(Model.BudgetDocument.transactions.Sum(m => m.creditAmount) != Model.BudgetDocument.transactions.Sum(m => m.debitAmount))
                {
                    throw new Exception("The budget transaction document debit and credit sum are not equal, please correct your input.");
                }

                var totalSum = Sources.Sum(m => m.Amount);

                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _wfFacade.SetSession(GetSession());
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var wItem = _wfFacade.GetLastWorkItem(wfid);
                var data = JsonConvert.DeserializeObject<PaymentRequestModel>(wItem.Data.ToString());
                if(data.Request.Items.Sum(m => m.Amount) != totalSum)
                {
                    throw new Exception("Payment sources total amount not equal to requested amount");

                }
                var totalBudgetAmount = Model.BudgetDocument.transactions.Sum(m => m.debitAmount);
                if(totalBudgetAmount != totalSum)
                {
                    throw new Exception("Sum of payment source amount and budget transaction amount are not equal, please correct your input");
                }
                
                var accounts = _paymentService.GetAllBankAccounts();
                var cashAccounts = _paymentService.GetAllCashAccounts();
                foreach (var item in Sources)
                {
                    if(item.Type == "cash")
                    {
                        item.Name = cashAccounts.Where(m => m.csAccountID == item.AssetAccountID).First().name;
                    } 
                    if(item.Type == "bank")
                    {
                        item.Name = accounts.Where(m => m.mainCsAccount == item.AssetAccountID).First().BankBranchAccount;
                    }
                }
                
                data.Sources = Sources.ToList();
                data.BudgetDocument = Model.BudgetDocument;
                _paymentFacade.CheckPayment(wfid, data, description);
                return Json(new
                {
                    status = true
                });
             }

            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetAccountName(int id)
        {
            var name = _paymentService.GetAccountName(id);
            return Ok(name);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetCostCenterName(int id)
        {
            var name = _paymentService.GetCostCenterName(id);
            return Ok(name);
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult ApprovePaymentRequest(Guid wfid, string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());

                DocumentTypedReference reference = new DocumentTypedReference();
                _wfFacade.SetSession(GetSession());
                var wItem = _wfFacade.GetLastWorkItem(wfid);
                var data = JsonConvert.DeserializeObject<PaymentRequestModel>(wItem.Data.ToString());
                _paymentFacade.ApprovePayment(wfid,data, description);

                    return Json(new
                    {
                        status = true
                    });
                




            }

            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult OwnerRejectRequest(Guid wfid, string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _paymentFacade.RejectByOwner(wfid, description);

                return Json(new
                {
                    status = true
                });

            }

            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult CancelPaymentRequest(Guid wfid, string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());

                _paymentFacade.CancelPaymentRequest(wfid, description);

                return Json(new
                {
                    status = true
                });

            }

            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult ExecutePaymentRequest(Guid wfid, string description)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _paymentFacade.ExecutePayment(wfid, description);

                return Json(new
                {
                    status = true
                });

            }

            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult PostCashierToCashierDocument(Guid wfid, CashierToCashierDocument Doc)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _wfFacade.SetSession(GetSession());
                var wItem = _wfFacade.GetLastWorkItem(wfid);
                var data = JsonConvert.DeserializeObject<PaymentRequestModel>(wItem.Data.ToString());
                var reference = data.Reference;
                Doc.voucher = reference;
                Doc.PaperRef = reference.reference;
                var allowedAmount = GetTotalDocumentExpenseAmount(wfid);
                var sourceIDs = data.Sources.Select(m => m.AssetAccountID).ToArray();

                var response = _paymentFacade.PostCashierToCashierDocument(Doc,-1,sourceIDs);
                if (response.status == true)
                {
                    var doc = (CashierToCashierDocument)(response.response);
                    var listOfDocs = _paymentFacade.AddPaymentDocument(wfid, doc,true);
                    return Json(new
                    {
                        status = true,
                        response = listOfDocs
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        error = response.error,
                    });
                }


            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult PostLabourPaymentDocument(Guid wfid, LaborPaymentDocument Doc)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _wfFacade.SetSession(GetSession());
                var wItem = _wfFacade.GetLastWorkItem(wfid);
                var data = JsonConvert.DeserializeObject<PaymentRequestModel>(wItem.Data.ToString());
                var reference = data.Reference;
                Doc.voucher = reference;
                Doc.PaperRef = reference.reference;
                var sourceIDs = data.Sources.Select(m => m.AssetAccountID).ToArray();
                var allowedAmount = GetTotalDocumentExpenseAmount(wfid);
                var response = _paymentFacade.PostLaborPaymentDocument(Doc,-1, sourceIDs);
                if (response.status == true)
                {
                    var doc = (LaborPaymentDocument)(response.response);
                    var listOfDocs = _paymentFacade.AddPaymentDocument(wfid, doc,true);
                    return Json(new
                    {
                        status = true,
                        response = listOfDocs
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        error = response.error,
                    });
                }


            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        public PaymentRequestModel GetPaymentRequestModel(Guid guid)
        {
            _wfFacade.SetSession(GetSession());
            var response = _wfFacade.GetLastWorkItem(guid);
            var data = JsonConvert.DeserializeObject<PaymentRequestModel>(response.Data.ToString());
            return data;
        }



        [HttpGet("[action]/{wfid}")]
        public IActionResult AddPostedDocument(Guid wfid, int docID)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var model = GetPaymentRequestModel(wfid);
                var allDocs = new List<AccountDocument>();
                allDocs.AddRange(model.ImportedDocuments != null ? model.ImportedDocuments : new  List<AccountDocument>());
                allDocs.AddRange(model.NewDocuments != null ? model.NewDocuments : new List<AccountDocument>());
                var allDocsID = allDocs.Select(m => m.AccountDocumentID).ToList();
                if (allDocsID.Contains(docID))
                {
                    throw(new Exception("Adding Duplicated Document is not allowed"));
                }
                
                var response = _paymentFacade.GetAccountDocument(docID);
                if(response.status == true)
                {
                    
                    var doc = (AccountDocument)(response.response);
                    var description = $"Document Attached with document ID {docID} and reference of {doc.PaperRef}";
                    var listOfDocs = _paymentFacade.AddPaymentDocument(wfid, doc,false, description);
                    return Json(new
                    {
                        status = true,
                        response = listOfDocs
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        error = response.error,
                    });
                }


            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult RemoveDocuments(Guid wfid, int docID)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var result = _paymentFacade.RemovePaymentDocument(wfid, docID, $"Removed document with Document ID of {docID}");
                return Json(result);


            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        [HttpGet("[action]/{typeID}")]
        public IActionResult GetSerialType(int typeID)
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetSerialType(typeID);
                return Json(result);
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        public double GetTotalDocumentExpenseAmount(Guid id)
        {
            _wfFacade.SetSession(GetSession());
            _paymentService.SetSessionConfiguration(_config, GetSession());
            var response = _wfFacade.GetLastWorkItem(id);
            var model = JsonConvert.DeserializeObject<PaymentRequestModel>(response.Data.ToString());
            var source = model.Sources;
            var sourceIDs = source.Select(m => m.AssetAccountID).ToArray();

            var sourceAmount = source.Sum(m => m.Amount);

            var allDocs = new List<AccountDocument>();
            allDocs.AddRange(model.ImportedDocuments != null ? model.ImportedDocuments : new List<AccountDocument>());
            allDocs.AddRange(model.NewDocuments != null ? model.NewDocuments : new List<AccountDocument>());
            var allDocsID = allDocs.Select(m => m.AccountDocumentID).Distinct().ToList();
            List<double> amounts = new List<double>();
            foreach (int dID in allDocsID)
            {
                var result = _paymentService.GetTransactionAmount(dID, sourceIDs);
                var amt = (double)result.response;
                amounts.Add((-1) * amt);
            }
            var spentAmount = amounts.Sum(m => m);
            return sourceAmount - spentAmount;
        }





        [HttpPost("[action]/{wfid}")]
        public IActionResult PostBankWithdrawalDocument(Guid wfid, BankWithdrawalDocument Doc)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                _wfFacade.SetSession(GetSession());
                var wItem = _wfFacade.GetLastWorkItem(wfid);
                var data = JsonConvert.DeserializeObject<PaymentRequestModel>(wItem.Data.ToString());
                var reference = data.Reference;
                Doc.voucherReference = reference;
                Doc.PaperRef = reference.reference;
                var sourceIDs = data.Sources.Select(m => m.AssetAccountID).ToArray();

                var allowedAmount = GetTotalDocumentExpenseAmount(wfid);
                var response = _paymentFacade.PostBankWithdrawalDocument(Doc,-1,sourceIDs);
                if (response.status == true)
                {
                    var doc = (BankWithdrawalDocument)(response.response);
                    var listOfDocs = _paymentFacade.AddPaymentDocument(wfid, doc,true);
                    return Json(new
                    {
                        status = true,
                        response = listOfDocs
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = false,
                        error = response.error,
                    });
                }


            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        [HttpGet("[action]/{docID}")]
        public IActionResult GetAccountDocumentHTML(int docID)
        {
        
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var response = _paymentFacade.GetAccountDocumentHTML(docID);
                return Json(response);
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        [HttpGet("[action]/{docID}")]
        public IActionResult GetDocumentTransactionAmount(int docID)
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var response = _paymentFacade.GetDocumentTrasactionAmount(docID);
                return Json(response);
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAllDocumentTypes()
        {
            try
            {
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var response = _paymentFacade.GetAllDocumentTypes();
                if (response.status)
                {
                    var result =  (DocumentType[])(response.response);
                    return Json(new
                    {
                        status = true,
                        response = result
                    });
                }
                else
                {
                    return Json(response);
                }
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message,
                });
            }
        }

        //[HttpPost("[action]/{wfid}")]
        //public IActionResult AddPaymentDocument(Guid wfid, string Document)
        //{
        //    try
        //    {
        //        _paymentFacade.SetSessionConfiguration(_config, GetSession());

        //        var docs = _paymentFacade.AddPaymentDocument(wfid, Document);

        //        return Json(new
        //        {
        //            status = true,
        //            response = docs
        //        });

        //    }

        //    catch (Exception ex)
        //    {

        //        return Json(new
        //        {
        //            status = false,
        //            error = ex.Message
        //        });
        //    }
        //}

        [HttpGet("[action]")]
        public IActionResult GetPaymentTypes()
        {
            
            return Json(Enum.GetNames(typeof(PaymentType)));
        }

        [HttpPost("[action]")]
        public IActionResult TestAPI(TestModel Upload)
        {
            return Json(Upload);
        }

        [HttpGet("[action]")]
        public IActionResult GetPaymentRequestWorkflows()
        {
            try
            {
                _wfFacade.SetSession(GetSession());
                var response = _wfFacade.GetUserWorkflows().Where(m => m.TypeId == (long)WorkflowTypes.PaymentWorkflow).ToList();
                return Json(new
                {
                    status = true,
                    response = response
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = true,
                    error = ex.Message
                });
                throw;
            }

        }

        [HttpGet("[action]")]
        public WorkItemResponse GetLastPaymentWorkItem(Guid guid)
        {
            _wfFacade.SetSession(GetSession());
            _documentService.SetSession(GetSession());
            var response = _wfFacade.GetLastWorkItem(guid);
            var requestDoc = _documentService.GetDocument(guid);
            var data = JsonConvert.DeserializeObject<PaymentRequestModel>(response.Data.ToString());
        //    data.Request.Files = requestDoc;
            response.Data = data;
            return response;
        }

        [HttpGet("[action]/{id}")]
        public ActionResult DownloadFile(int id)
        {
            var doc =  _documentService.GetDocument(id);
            return File(doc.File, doc.Contenttype);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetTransactionAmount(Guid id,int docID)
        {
            try
            {
                var amount = Helper.GetTransactionAmount(new List<int> { docID});
                return Json(SuccessfulResponse(amount));
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]/{docID}")]
        public IActionResult GetNetTransactionAmount(int docID)
        {
            try
            {
                var amount = Helper.GetNetTransactionAmount(new List<int> { docID });
                return Json(SuccessfulResponse(amount));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{docID}")]
        public IActionResult GetTransactionAmounts(int docID)
        {
            try
            {
                var netAmount = Helper.GetNetTransactionAmount(new List<int> { docID });
                var amount = Helper.GetTransactionAmount(new List<int> { docID });
                return Json(SuccessfulResponse(new double[] { amount, netAmount }));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }


        [HttpGet("[action]/{id}")]
        public IActionResult CheckTransactionAmount(Guid id, int docID = -1, double amount = -1)
        {
            try
            {
                _wfFacade.SetSession(GetSession());
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var response = _wfFacade.GetLastWorkItem(id);
                var model = JsonConvert.DeserializeObject<PaymentRequestModel>(response.Data.ToString());
                var source = model.Sources;
                var sourceIDs = source.Select(m => m.AssetAccountID).ToArray();

                var sourceAmount = source.Sum(m => m.Amount);

                var allDocs = new List<AccountDocument>();
                allDocs.AddRange(model.ImportedDocuments != null ? model.ImportedDocuments : new List<AccountDocument>());
                allDocs.AddRange(model.NewDocuments != null ? model.NewDocuments : new List<AccountDocument>());
                var allDocsID = allDocs.Select(m => m.AccountDocumentID).Distinct().ToList();
                List<double> amounts = new List<double>();
                foreach(int dID in allDocsID)
                {
                    var result = _paymentService.GetTransactionAmount(dID, sourceIDs);
                    var amt = (double)result.response;
                    amounts.Add((-1) * amt);
                }
                var spentAmount = amounts.Sum(m => m);
                double requestedAmount = 0;
                if (docID != -1)
                {
                    requestedAmount = (-1) * (double)_paymentFacade.GetTransactionAmount(docID, sourceIDs).response;
                }
                else
                {
                    if(amount == -1)
                    {
                        throw new Exception("Invalid argument, either docID or amount should be set");
                    }
                    requestedAmount = amount;
                }
                
                var totalAmount = spentAmount + requestedAmount;
                if(totalAmount > sourceAmount)
                {
                    var difference = totalAmount - sourceAmount;
                    return Json(new
                    {
                        status = false,
                        error = $"You are requesting an amount greter than allowed , The document has an extra amount of {difference}"
                    });
                }

            else{
                    return Json(new
                    {
                        status = true
                    });

                }
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAllBankAccounts()
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetAllBankAccounts();
                return Json(new
                {
                    status = true,
                    response = result
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }

        }

        [HttpGet("[action]")]
        public IActionResult GetAllCashAccounts()
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetAllCashAccounts();
                return Json(new
                {
                    status = true,
                    response = result
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAllTaxCenters()
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetAllTaxCenters();
                return Json(new
                {
                    status = true,
                    response = result
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }

        }
        
        [HttpGet("[action]")]
        public IActionResult GetAllCustomers()
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetAllCustomers();
                return Json(result);
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAllEmployees()
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetAllEmployees();
                return Json(result);
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }


        [HttpGet("[action]")]
        public IActionResult GetPaymentInstrumentTypes()
        {
            try
            {
                _paymentService.SetSessionConfiguration(_config, GetSession());
                var result = _paymentService.GetPaymentInstrumentTypes();
                return Json(new
                {
                    status = true,
                    response = result
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }
        }


        [HttpGet("[action]")]
        public IActionResult GetBIZNETPaymentMethods()
        {
            var result = Enum.GetNames(typeof(BizNetPaymentMethod));
            return Json(new
            {
                status = true,
                response = result
            });
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult GetPaymentWorkflowHistory(Guid wfid)
        {
            try
            {
                _wfHelper.SetSession(GetSession());
                var history = _wfHelper.GetPaymentWorkflowHistory(wfid);
                return Json(new
                {
                    status = true,
                    response = history
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }

        }

        [HttpGet("[action]/{wfid}/{type}")]
        public IActionResult GetWorkflowHistory(Guid wfid,string type)
        {
            try
            {
                _wfHelper.SetSession(GetSession());
                var history = new WorkflowHistoryModel();
                switch (type)
                {
                    case "payment":
                        history = _wfHelper.GetPaymentWorkflowHistory(wfid);
                        break;
                    case "purchase":
                        history = _wfHelper.GetPurchaseWorkflowHistory(wfid);
                        break;
                    case "order":
                        history = _wfHelper.GetPurchaseOrderWorkflowHistory(wfid);
                        break;
                    case "store":
                        history = _wfHelper.GetStoreRequestWorkflowHistory(wfid);
                        break;
                    default:
                        break;
                }
                return Json(SuccessfulResponse(history));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }


        [HttpGet("[action]/{wfid}")]
        public IActionResult GetPaymentRequestSigners(Guid wfid)
        {
            try
            {
                _wfHelper.SetSession(GetSession());
                var signers = _wfHelper.GetPaymentRequestSigners(wfid);
                return Json(new
                {
                    status = true,
                    response = signers
                });
            }
            catch (Exception ex)
            {

                return Json(new
                {
                    status = false,
                    error = ex.Message
                });
            }

        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult GetTotalPaidAmount(Guid wfid)
        {
            try
            {
                //throw new Exception("Test for failiure");
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var result = _paymentFacade.GetTotalPaidAmount(wfid);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }


        [HttpGet("[action]/{wfid}")]
        public IActionResult GetTotalPaidAmountFromSource(Guid wfid)
        {
            try
            {
                //throw new Exception("Test for failiure");
                _paymentFacade.SetSessionConfiguration(_config, GetSession());
                var result = _paymentFacade.GetTotalPaidAmountFromSource(wfid);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        //[HttpGet("[action]/{wfid}")]
        //public IActionResult GetTotalPaidAmount(Guid wfid)
        //{
        //    try
        //    {
        //        //throw new Exception("Test for failiure");
        //        _paymentFacade.SetSessionConfiguration(_config, GetSession());
        //        var result = _paymentFacade.GetTotalPaidAmount(wfid);
        //        return Json(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message);
        //    }
        //}





    }

    public class TestModel 
    {
        public double InputNumber {get; set;}
        public double Rate {get; set;}
        public Document Upload {get; set;}
    }

}