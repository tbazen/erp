﻿using INTAPS.Payroll;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PWF.data.Entities;
using PWF.domain;
using PWF.domain.Documents;
using PWF.domain.Payment.Models;
using PWF.domain.Purchase;
using PWF.domain.Purchase.Models;
using PWF.domain.PurchaseOrder;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Store.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.api.Controllers
{

    [Route("api/pwf/[controller]")]
    [ApiController]
    public class PurchaseController : BaseController
    {
        public PWFConfiguration _config;
        private IPurchaseService _purchaseService;
        private IPurchaseFacade _purchaseFacade;
        private IPurchaseOrderService _orderService;
        private IPurchaseOrderFacade _orderFacade;
        private IWorkflowFacade _wfFacade;
        private IWorkflowService _wfService;
        private IDocumentService _docService;

        public PurchaseController(IOptions<PWFConfiguration> Config, IPurchaseService purchService, IPurchaseFacade purchFacade, IPurchaseOrderService orderService,
                                    IPurchaseOrderFacade orderFacade, IWorkflowFacade wfFacade, IWorkflowService wfService, IDocumentService docService)

        {
            _config = Config.Value;
            _purchaseService = purchService;
            _purchaseFacade = purchFacade;
            _orderService = orderService;
            _orderFacade = orderFacade;
            _wfFacade = wfFacade;
            _wfService = wfService;
            _docService = docService;
        }

        [HttpGet("[action]")]
        public IActionResult GetUserPurchaseWorkflow()
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result = _purchaseService.GetUserPurchaseWorkflows();
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }


        }

        [HttpGet("[action]")]
        public IActionResult GetUserPurchaseOrderWorkflow()
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result = _purchaseService.GetUserPurchaseOrderWorkflows();
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }


        }

         
        [HttpGet("[action]")]
        public IActionResult GetUserItemDeliveryrWorkflows()
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result = _purchaseService.GetUserItemDeliveryrWorkflows();
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }


        }

        [HttpGet("[action]")]
        public IActionResult GetUserServiceDeliveryWorkflows()
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result = _purchaseService.GetUserServiceDeliveryWorkflows();
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }


        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult GetUncertifiedService(Guid wfid)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result = _purchaseService.GetUncertifiedServices(wfid);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }


        }

        [HttpGet("[action]")]
        public IActionResult GetCurrentEmployee()
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                Employee result = _purchaseService.GetEmployee();
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]")]
        public IActionResult CreatePurchaseWorkflow([FromBody] PurchaseRequest Request)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var items = Request.Items;
                Request.RequestDate = DateTime.Now;
                foreach (var item in items)
                {
                    var i = _purchaseService.GetTransactionItem(item.Code);
                    item.Description = i.Name;
                    item.type = i.GoodOrService;
                    
                }
                Request.Items = items;
                var result = _purchaseFacade.CreatePurchaseRequest(Request);
                return Json(SuccessfulResponse(result));

            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult ResubmitPurchaseRequest(Guid wfid, PurchaseRequest Request)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var items = Request.Items;
                Request.RequestDate = DateTime.Now;
                foreach (var item in items)
                {
                    var i = _purchaseService.GetTransactionItem(item.Code);
                    item.Description = i.Name;
                    item.type = i.GoodOrService;

                }
                Request.Items = items;
                _purchaseFacade.ResubmitPurchaseRequest(wfid,Request);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult IssuePurchaseOrder(Guid wfid,PurchaseOrderModel Order)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _orderService.SetSessionConfiguration(_config, GetSession());
                _docService.SetSession(GetSession());
                Order.Document.Documenttypeid = (int)DocumentTypes.PurchaseOrderDocument;
                var doc = _docService.AddDocument(Order.Document);
                Order.Document = doc;
                var items = Order.Items;
                foreach (var item in items)
                {
                    var itemn = _purchaseService.GetTransactionItem(item.Code);
                    item.Description = itemn.NameCode;
                    item.type = itemn.GoodOrService;
                }
                Order.Items = items;
                Order.Supplier = _purchaseService.GetSupplier(Order.Supplier.Code);
                var newID = _purchaseFacade.IssueOrder(wfid, Order);
               var result = _orderService.GetPurchaseOrderWorkItem(newID);
                return Json(SuccessfulResponse(result));
          }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }

            }
    

        [HttpPost("[action]/{wfid}")]
        public IActionResult ResubmitPurchaseOrder(Guid wfid, PurchaseOrderModel Order)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                _orderService.SetSessionConfiguration(_config, GetSession());
                _docService.SetSession(GetSession());
                Order.Document.Documenttypeid = (int)DocumentTypes.PurchaseOrderDocument;
                var doc = _docService.AddDocument(Order.Document);
                Order.Document = doc;
                var items = Order.Items;
                foreach (var item in items)
                {
                    var itemn = _purchaseService.GetTransactionItem(item.Code);
                    item.Description = itemn.NameCode;
                    item.type = itemn.GoodOrService;
                }
                Order.Items = items;
                Order.Supplier = _purchaseService.GetSupplier(Order.Supplier.Code);
                _orderFacade.ResubmitPurchaseOrder(wfid, Order);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }

        }




        [HttpGet("[action]")]
        public IActionResult GetLastPurchaseWorkItem(Guid guid)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var response = _purchaseService.GetLastPurchaseWorkitem(guid);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }

        }

        [HttpGet("[action]/{guid}")]
        public IActionResult GetLastPurchaseOrderWorkItem(Guid guid)
        {
            try
            {
                _orderService.SetSessionConfiguration(_config, GetSession());
                var response = _orderService.GetPurchaseOrderWorkItem(guid);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult GetItemDeliveryWorkItem(Guid wfid)
        {
            try
            {
                _orderService.SetSessionConfiguration(_config, GetSession());
                var response = _orderService.GetItemDeliveryWorkItem(wfid);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult GetServiceDeliveryWorkItem(Guid wfid)
        {
            try
            {
                _orderService.SetSessionConfiguration(_config, GetSession());
                var response = _orderService.GetServiceDeliveryWorkItem(wfid);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }


        [HttpGet("[action]/{guid}")]
        public IActionResult GetPurchaseOrderWorkitemsOf(Guid guid)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var response = _purchaseService.GetPurchaseOrderWorkitemsOf(guid);
                return Json(SuccessfulResponse(response));
                
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetAllowedOrderQuantity(Guid id)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result =_purchaseService.GetAllowedMaxOrderUnits(id);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        

        [HttpGet("[action]")]
        public IActionResult ApprovePurchaseRequest(Guid wfid, string description)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.ApprovePurchaseRequest(wfid, description);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult FinalizePurchaseRequest(Guid wfid, string description)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.FinalizePurchase(wfid, description);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }


        [HttpGet("[action]")]
        public IActionResult RejectPurchaseRequest(Guid wfid, string description)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.RejectPurchaseRequest(wfid, description);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetItemName(string code)
        {
            _purchaseService.SetSessionConfiguration(_config, GetSession());
            var result = _purchaseService.GetItemName(code);
            return Json(result);
        }

        [HttpGet("[action]")]
        public string GetSupplierName(string code)
        {
            _purchaseService.SetSessionConfiguration(_config, GetSession());
            var result = _purchaseService.GetSupplierName(code);
            return result;
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult AddPriceQuotation(Guid wfid, PriceQuotation Quotation)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                var supplier = _purchaseService.GetSupplier(Quotation.Supplier.Code);
                if(supplier == null)
                {
                    throw new Exception("Supplier not selected");
                }
                Quotation.Supplier = supplier;
                var items = Quotation.Items;
                foreach (var item in items)
                {
                    var ti = _purchaseService.GetTransactionItem(item.Code) ;
                    item.Description = ti.Name;
                    item.type = ti.GoodOrService;
                }
                Quotation.Items = items;
                var result = _purchaseFacade.AddPriceQuotation(wfid, Quotation);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult ApprovePurchaseOrder(Guid wfid,string description)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                _orderFacade.ApprovePurchaseOrder(wfid, description);
                return Json(SuccessfulResponse("Success"));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult CancelPurchaseRequest(Guid wfid)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.CancelPurchaseRequest(wfid, "Cancel Purhcase Request");
                return Json(SuccessfulResponse(true));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{parentWFID}/{orderWFID}")]
        public IActionResult CancelPurchaseOrder(Guid parentWFID,Guid orderWFID)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _orderFacade.CancelPurchaseOrder(orderWFID, "Cancel Purchase Order");
                _purchaseFacade.CancelPurchaseOrder(parentWFID, orderWFID);
                return Json(SuccessfulResponse("Success"));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }


        [HttpGet("[action]/{wfid}")]
        public IActionResult RejectPurchaseOrder(Guid wfid, string description)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                _orderFacade.RejectPurchaseOrder(wfid, description);
                return Json(SuccessfulResponse("Success"));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult ClosePurchaseOrder(Guid wfid, string description)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                _orderFacade.ClosePurchaseOrder(wfid, description);
                return Json(SuccessfulResponse("Success"));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult RequestPurchasePayment(Guid wfid,PaymentWorkflowModel Model)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                var wf = _orderFacade.RequestPurchasePayment(wfid, Model);
                return Json(SuccessfulResponse(wf));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }

        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult RequestItemDelivery(Guid wfid, ItemDeliveryModel Model)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                var response = _orderFacade.RequestItemDelivery(wfid,Model);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{wfid}")]
        public IActionResult RequestServiceDelivery(Guid wfid, ServiceDeliveryModel Model)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                var response = _orderFacade.RequestServiceDelivery(wfid, Model);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }


        [HttpPost("[action]")]
        public IActionResult RequestPurchaseDelivery(ItemDeliveryModel Model)
        {
            try
            {
                _orderFacade.SetSessionConfiguration(_config, GetSession());
                var response =_orderFacade.RequestItemDelivery(new Guid(), Model);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }

        }




        [HttpGet("[action]/{wfid}")]
        public IActionResult SubmitQuotation(Guid wfid, string description)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.SubmitPriceQuotations(wfid, description);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }

        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult CanCloseRequest(Guid wfid)
        {
            try
            {
                _purchaseService.SetSessionConfiguration(_config, GetSession());
                var result = _purchaseService.CanCloseRequest(wfid);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }

        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult CloseRequest(Guid wfid, string description)
        {
            try
            {
                _purchaseFacade.SetSessionConfiguration(_config, GetSession());
                _purchaseFacade.CloseRequest(wfid, description);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }

        }

    }

}
