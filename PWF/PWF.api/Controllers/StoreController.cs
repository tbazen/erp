﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIZNET.iERP;
using INTAPS.Accounting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PWF.api.Helpers;
using PWF.data.Entities;
using PWF.domain;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Store;
using PWF.domain.Store.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;

namespace PWF.api.Controllers
{
    [Route("api/pwf/[controller]")]
    [ApiController]
    public class StoreController : BaseController
    {
        public PWFConfiguration _config;
        private IStoreRequestService _storeRequest;
        private IStoreRequestFacade _storeRequestFacade;
        private IWorkflowFacade _wfService;
        private IDeliveryFacade _deliveryFacade;
        private IDeliveryService _deliveryService;
        public StoreController(IOptions<PWFConfiguration> Config, IStoreRequestFacade facade, 
            IStoreRequestService storeRequest, IWorkflowFacade Service, IDeliveryFacade delivFacade, 
            IDeliveryService delivService)
        {
            _storeRequest = storeRequest;
            _config = Config.Value;
            _wfService = Service;
            _storeRequestFacade = facade;
            _deliveryFacade = delivFacade;
            _deliveryService = delivService;
        }

        [HttpGet("[action]")]
        public List<ItemViewModel> GetAllItems()
        {
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            var result = _storeRequest.GetAllItems();
            return result;
        }

        [HttpGet("[action]")]
        public List<TransactionItems> SearchItem(string term)
        {
          //  TransactionDocumentItem
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            _storeRequestFacade.SetSessionConfiguration(_config,GetSession());
            var result = _storeRequestFacade.SearchTransactionItems(term);
            return result;
        }

        [HttpGet("[action]")]
        public List<TransactionItems> SearchStoreItems(string term)
        {
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var result = _storeRequest.SearchStoreItems(term);
            return result;
        }



        [HttpPost("[action]")]
        public IActionResult FileRequest([FromBody]StoreRequestModel Model)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var result = _storeRequestFacade.FileNewStoreRequest(Model, Model.requestDescription);
            return Ok(new
            {
                status = "success",
                message = result
            });
        }


        [HttpPost("[action]/{id}")]
        public IActionResult ResubmitRequest([FromBody] StoreRequestModel model, Guid id)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            _wfService.SetSession(GetSession());
            var Model = JsonConvert.DeserializeObject<StoreRequestModel>(_wfService.GetLastWorkItem(id).Data.ToString());
            var requestName = Model.requestName;
            StoreRequestModel M = new StoreRequestModel() {
                requestName = requestName,
                requestDescription = model.requestDescription,
                Requested = model.Requested
            };
            _storeRequestFacade.ResubmitStoreRequest(id, M, model.requestDescription);
            return Ok(new {
                status = "Success"
            });
        }

        [HttpPost("[action]/{id}")]
        public IActionResult ReorganizeRequest([FromBody]StoreRequestModel Model, Guid id)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            _wfService.SetSession(GetSession());
            var model = JsonConvert.DeserializeObject<StoreRequestModel>(_wfService.GetLastWorkItem(id).Data.ToString());

            StoreRequestModel M = new StoreRequestModel()
            {
                Reorganized = Model.Reorganized,
                requestDescription = model.requestDescription,
                requestName = model.requestName,
                Requested = model.Requested
            };
            _storeRequestFacade.ReorganizeStoreRequest(id, M, Model.requestDescription);
            return Ok(new
            {
                status = "Success"
            });

        }

        [HttpGet("[action]/{id}")]
        public IActionResult CancelStoreRequest(Guid id, string description) {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            _storeRequestFacade.CancelStoreRequest(id, description);
            return Ok(new {
                status = "Success"
            });

        }

        [HttpPost("[action]/{id}")]
        public IActionResult DeliverItems(Guid id, ItemDeliveryModel Model)
        {
            try
            {
                _deliveryFacade.SetSessionConfiguration(_config, GetSession());
                _deliveryFacade.DeliverItems(id, Model);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{id}")]
        public IActionResult CertifyItems(Guid id, ServiceDeliveryModel Model)
        {
            try
            {
                _deliveryFacade.SetSessionConfiguration(_config, GetSession());
                _deliveryFacade.ApproveServiceDelivery(id, Model);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{id}")]
        public IActionResult SubmitDeliveryVoucher(Guid id, ItemDeliveryModel Model)
        {
            try
            {
                _deliveryFacade.SetSessionConfiguration(_config, GetSession());
                _deliveryFacade.SubmitDeliveryVoucher(id, Model);
                return Json(SuccessfulResponse(""));
            }
            catch (Exception ex)
            {
                return Json(ErrorResponse(ex));
            }
        }


        [HttpPost("[action]/{wfid}/{description}/{status}")]
        public IActionResult SubmitRequestReview(Guid wfid, string description, int status, ItemRequestModel[] Model)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            if (status == 1)
            {
                _storeRequestFacade.ApproveStoreRequest(wfid, Model, description);
                return Ok(new
                {
                    status = "Success"
                });
            }
            if (status == 0)
            {
                _storeRequestFacade.RejectStoreRequest(wfid, description);
                return Ok(new
                {
                    status = "Success"
                });
            }
            return Ok(new
            {
                status = "failed"
            });
        }

        [HttpGet("[action]")]
        public IActionResult CancelReorganization(Guid wfid,string description){
            try{
                _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
                _storeRequestFacade.CancelRequestReorganizing(wfid, description);
                return Ok(new
                {
                    status = true
                });
            }
            catch(Exception e){
                return Ok(new {
                    status = false,
                    error= e.Message
                });
            }

        }

        [HttpGet("[action]/{wfid}")]
        public IActionResult GetGRVDocSigners(Guid wfid)
        {
            try
            {
                _storeRequest.SetSessionConfiguration(_config, GetSession());
                var response = _storeRequest.GetGRVDocumentSigners(wfid);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{docID}")]
        public IActionResult GetStoreDeliverDocument(int docID)
        {
            try
            {
                _deliveryService.SetSessionConfiguration(_config, GetSession());
                var result = _deliveryService.GetDeliveryDocument(docID);
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpPost("[action]/{id}")]
        public IActionResult PostStoreIssueDocument(Guid id, StoreIssueDocument Doc)
        {
            try
            {
                _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
                _wfService.SetSession(GetSession());
                var result = _storeRequestFacade.PostStoreIssueDocument(Doc);
                if (result.status)
                {
                    var Model = JsonConvert.DeserializeObject<StoreRequestModel>(_wfService.GetLastWorkItem(id).Data.ToString());
                    var doc = (StoreIssueDocument)(result.response);
                    if (Model.Issued != null && Model.Issued.Count > 0)
                    {
                        Model.Issued.Add(doc);
                    }
                    else
                    {
                        var issued = new List<StoreIssueDocument>();
                        issued.Add(doc);
                        Model.Issued = issued;
                    }
                    _storeRequestFacade.IssueStoreRequest(id, Model);
                    return Json(new
                    {
                        status = true,
                        response = Model.Issued
                    });
                }
                else
                {
                    return Json(result);
                }
                
            }
            catch (System.Exception e)
            {
                return Json(new {
                    status = false,
                    error = e.Message
                });
                //throw;
            }
           
           
        }

        

        [HttpGet("[action]")]
        public IActionResult GetStoreRequestWorkflows() {
            //_wfService.SetSession(GetSession());
            //var response = _wfService.GetUserWorkflows().Where(m => m.TypeId == (int)WorkflowTypes.StoreRequestWorkflow).ToList();
            //return response;
            try
            {
                _wfService.SetSession(GetSession());
                var response = _wfService.GetUserWorkflows().Where(m => m.TypeId == (int)WorkflowTypes.StoreRequestWorkflow).ToList();
                return Json(response);
            }
            catch (Exception ex)
            {
                if (ex is NotAuthenticatedException)
                {
                    return StatusCode(401, "Not Authenticated");
                }
                else
                    throw ex;
            }

        }

        [HttpGet("[action]")]
        public WorkItemResponse GetLastWorkitem(Guid guid)
        {
            _wfService.SetSession(GetSession());
            var response = _wfService.GetLastWorkItem(guid);
            return response;
        }

        [HttpGet("[action]")]
        public ItemAccountBalance GetItemAccountBalance(string itemCode,int costCenterID)
        {
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            var balance = _storeRequest.GetAccountBalance(itemCode, costCenterID, DateTime.Now);
            return balance;
        }

        [HttpGet("[action]")]
        public ItemCategory[] GetItemCategories()
        {
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            var categories = _storeRequest.GetItemCategories();
            return categories.ToArray();
        }

        [HttpGet("[action]")]
        public TransactionItems[] GetItemsInCategory(int catID)
        {
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            var items = _storeRequest.GetItemsInCategory(catID);
            return items.ToArray();
        }

        [HttpPost("[action]")]
        public int RegisterItemCategory(ItemCategory             category) {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var cat = _storeRequestFacade.RegisterItemCategory(category);
            return cat;
        }

        [HttpPost("[action]")]
        public int RegisterItem(TransactionItems Item)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var cat = _storeRequestFacade.RegisterItem(Item);
            return cat;
        }

        [HttpGet("[action]")]
        public StoreInfo[] GetAllStores()
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var stores = _storeRequestFacade.GetAllStores();
            return stores;
        }

        [HttpGet("[action]")]
        public CostCenter[] GetAllCostCenter(int parentID)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var centers = _storeRequestFacade.GetAllCostCenters(parentID);
            return centers;
        }

        [HttpGet("[action]/{costCenterID}")]
        public IActionResult GetStore(int costCenterID)
        {
            try
            {
                _storeRequest.SetSessionConfiguration(_config, GetSession());
                var store = _storeRequest.GetStore(costCenterID);
                return Json(SuccessfulResponse(store));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }

           
        }

        [HttpGet("[action]")]
        public Account[] GetChildAccounts(int parentID)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var accounts = _storeRequestFacade.GetAllChildAccounts(parentID);
            return accounts;
        }

        [HttpGet("[action]")]
        public MeasureUnit[] GetAllMeasureUnit()
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var units = _storeRequestFacade.GetAllMeasureUnits();
            return units;
        }

        [HttpGet("[action]")]
        public FixedAssetRuleAttribute[] GetAllFixedAssetRuleAttributes()
        {
                _storeRequestFacade.SetSessionConfiguration(_config,GetSession());
                var rules = _storeRequestFacade.GetFixedAssetRuleAttributes();
                return rules.ToArray();
        }

        [HttpGet("[action]")]
        public DocumentSerialType[] GetAllDocumentSerialTypes(){
            _storeRequestFacade.SetSessionConfiguration(_config,GetSession());
            var rules = _storeRequestFacade.GetAllDocumentSerialTypes();
            return rules;
        }

       
         public static Dictionary<string, string> ConvertToJson(Type e) 
         {

            // var ret = "{";

            // foreach (var val in Enum.GetValues(e)) {

            //     var name = Enum.GetName(e, val);

            //     ret += name + ":" + ((int)val).ToString() + ",";

            // }
            // ret += "}";
            // return ret;    
            Dictionary<string,string> dict = new Dictionary<string, string> ();
            foreach (var val in Enum.GetValues(e)) {

                var name = Enum.GetName(e, val);

                var value = ((int)val).ToString();
                dict.Add(name,value);

            }
            return dict;

        }

        [HttpGet("[action]")]
        public IActionResult GetServiceTypes()
        {
            return Json(ConvertToJson(typeof(ServiceType)));

        }

        
        [HttpPost("[action]/{wfid}")]
        public IActionResult CloseRequest(Guid wfid, [FromBody]Document Model)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            CloseSotreRequestModel Close = new CloseSotreRequestModel()
            {
                Description = Model.Description,
                Voucher = Model
            };
            var result = _storeRequestFacade.CloseStoreRequest(wfid, Close);
            return Json(result);

        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetNameForUID(int id)
        {
            _storeRequest.SetSessionConfiguration(_config, GetSession());
            var name = _storeRequest.getUserNameWithID(id);
            return Json(SuccessfulResponse(name));
        }

        [HttpGet("[action]")]
        public IActionResult GetExpenseTypes(){
            return Json(ConvertToJson(typeof(ExpenseType)));
        }
        
        [HttpGet("[action]")]
        public IActionResult GetInventoryTypes(){
            return Json(ConvertToJson(typeof(InventoryType)));
        }

        [HttpGet("[action]")]
        public IActionResult GetGoodTypes(){
            return Json(ConvertToJson(typeof(GoodType)));
        }

        [HttpGet("[action]")]
        public IActionResult GetStoreIssueDocumentSigners(Guid wfid)
        {
            _storeRequestFacade.SetSessionConfiguration(_config, GetSession());
            var result = _storeRequestFacade.GetStoreIssueDocumentSigner(wfid);
            return Json(result);
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetRemainingItemOrders(Guid id)
        {
            try
            {
                _deliveryFacade.SetSessionConfiguration(_config, GetSession());
                var response = _deliveryFacade.RemainingOrderedItems(id);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

        [HttpGet("[action]/{id}")]
        public IActionResult GetRemainingServiceOrders(Guid id)
        {
            try
            {
                _deliveryFacade.SetSessionConfiguration(_config, GetSession());
                var response = _deliveryFacade.RemainingOrderedServices(id);
                return Json(SuccessfulResponse(response));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
        }

    }

}