﻿using Microsoft.AspNetCore.Mvc;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.api.Controllers
{

    [Route("api/pwf/[controller]")]
    [ApiController]
    public class WorkflowController : BaseController
    {
        public IWorkflowSearchService _wfSearch;



        public WorkflowController(IWorkflowSearchService wfSearch)
        {
            _wfSearch = wfSearch;
        }

        [HttpPost("[action]")]
        public IActionResult SearchWorkflow(WFSearchRequestModel Model)
        {

            try
            {
                var result = _wfSearch.SearchWF(Model).Result;
                return Json(SuccessfulResponse(result));
            }
            catch (Exception ex)
            {

                return Json(ErrorResponse(ex));
            }
           
            
        }
    }
}
