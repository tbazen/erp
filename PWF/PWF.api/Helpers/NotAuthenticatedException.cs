﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.api.Helpers
{
    public class NotAuthenticatedException : Exception
    {
        public NotAuthenticatedException(string exception) : base(exception)
        {

        }
    }
}
