﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PWF.data;
using PWF.domain;
using PWF.domain.Admin;
using PWF.domain.Workflows;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Store;
using PWF.api.Helpers;
using System.Text;
using PWF.domain.Documents;
using PWF.domain.Payment;
using PWF.domain.PurchaseOrder;
using PWF.domain.Purchase;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PWF.api
{
    //internal class TimedHostedService : IHostedService, IDisposable
    //{
    //    private Timer _timer;

    //    public TimedHostedService()
    //    {
    //    }

    //    public Task StartAsync(CancellationToken cancellationToken)
    //    {

    //        _timer = new Timer(DoWork, null, TimeSpan.Zero,
    //            TimeSpan.FromSeconds(5));

    //        return Task.CompletedTask;
    //    }

    //    private void DoWork(object state)
    //    {
    //        Console.WriteLine("Recurring Task");
    //    }

    //    public Task StopAsync(CancellationToken cancellationToken)
    //    {

    //        _timer?.Change(Timeout.Infinite, 0);

    //        return Task.CompletedTask;
    //    }

    //    public void Dispose()
    //    {
    //        _timer?.Dispose();
    //    }
    //}


    public class PWFApp
    {
        public static void InitWebApp(IServiceCollection services, IConfiguration Configuration)
        {
            var connString = Configuration.GetConnectionString("pwf_context");
            var configurationSection = Configuration.GetSection("PWFConfiguration");

          //  services.AddDbContext<PWFContext>(option => option.UseSqlServer(connString));
            services.Configure<PWFConfiguration>(configurationSection);

            services.AddEntityFrameworkNpgsql().AddDbContext<PWFContext>(options => { options.UseNpgsql(connString); });
            // services.AddCors()
            //services.AddDbContext<PWFContext>(option => 
            //option.UseSqlServer(
            //    Configuration.GetConnectionString("pwf_context"
            //    //Configuration.GetConnectionString("DefaultConnection"),
            //  //b => b.MigrationsAssembly("PWF.api")
            //  ));
            services.AddCors();


            services.AddMemoryCache();

            services.AddTransient<IPWFService, PWFService>();
            services.AddTransient<IPWFFacade, PWFFacade>();
            services.AddTransient<ILookupService, LookupService>();
            services.AddTransient<IUserActionService, UserActionService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserFacade, UserFacade>();
            services.AddTransient<IWorkflowService, WorkflowService>();
            services.AddTransient<IWorkflowFacade, WorkflowFacade>();
            services.AddTransient<IStoreRequestService, StoreRequestService>();
            services.AddTransient<IStoreRequestFacade, StoreRequestFacade>();
            services.AddTransient<IDocumentService, DocumentService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IPaymentFacade, PaymentFacade>();
            services.AddTransient<IPurchaseOrderService, PurchaseOrderService>();
            services.AddTransient<IPurchaseOrderFacade, PurchaseOrderFacade>();
            services.AddTransient<IPurchaseService, PurchaseService>();
            services.AddTransient<IPurchaseFacade, PurchaseFacade>();
            services.AddTransient<IDeliveryFacade, DeliveryFacade>();
            services.AddTransient<IDeliveryService, DeliveryService>();
            services.AddTransient<IWorkflowHelper, WorkflowHelper>();
            services.AddTransient<IWorkflowSearchService, WorkflowSearchService>();
            //services.AddHostedService<TimedHostedService>();
            services.AddDistributedMemoryCache();
            services.AddSession();
            

            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<AppSettings>(appSettingsSection);

            //// configure jwt authentication
            //var appSettings = appSettingsSection.Get<AppSettings>();

            //var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(x =>
            //{
            //    x.RequireHttpsMetadata = false;
            //    x.SaveToken = true;
            //    x.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(key),
            //        ValidateIssuer = false,
            //        ValidateAudience = false,
            //    };
            //});
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

        }
    }
}
