﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.data.Entities
{
    public class ActionType
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public ICollection<UserAction> Useraction { get; set; }
    }
}
