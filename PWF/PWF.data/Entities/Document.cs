﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PWF.data.Entities
{
    public class Document
    {
        public int ID { get; set; }
        public Guid? WorkflowId { get; set; }
        public int? Fileid { get; set; }

        [ForeignKey("DocumentType")]
        public int Documenttypeid { get; set; }
        public string Filename { get; set; }
        public string Contentdisposition { get; set; }
        public string Contenttype { get; set; }
        public string Name { get; set; }
        public long Length { get; set; }
        public byte[] File { get; set; }
        public string ByteString { get; set; }
        public string Description { get; set; }


    }

    public class DocumentType
    {
        public int ID { get; set; }
        public string Name{ get; set; }
        public string Description { get; set; }
    }

    public enum DocumentTypes
    {
        StoreIssueDocument = 1,
        PaymentRequestDocument = 2,
        PaymentAttachmentDocuments = 3,
        SignedPaymentDocument = 4,
        PurchaseRequestDocument = 5,
        PriceQuotationDocument = 6,
        PurchaseOrderDocument = 7,
        StoreDeliveryDocument = 8,
        SignedStoreVoucherDocument = 9,
        ServiceCertificationDocument = 10,
    }


}
