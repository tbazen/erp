﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.data.Entities
{
    public class Role
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public ICollection<UserRole> UserRole { get; set; }
        public ICollection<WorkItem> WorkItem { get; set; }
    }
}
