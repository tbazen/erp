﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PWF.data.Entities
{
    public class User
    {
        public User()
        {
            Useraction = new HashSet<UserAction>();
            Userrole = new HashSet<UserRole>();
            Workitem = new HashSet<WorkItem>();
        }

        public long Id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public int Status { get; set; }
        public long Regon { get; set; }
        public int? Employeeid { get; set; }

        public ICollection<UserAction> Useraction { get; set; }
        public ICollection<UserRole> Userrole { get; set; }
        public ICollection<WorkItem> Workitem { get; set; }


    }
}
