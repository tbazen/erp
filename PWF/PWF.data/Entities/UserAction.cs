﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.data.Entities
{
    public partial class UserAction
    {
        public UserAction()
        {
            Userrole = new HashSet<UserRole>();
            Workitem = new HashSet<WorkItem>();
            Workflow = new HashSet<Workflow>();
        }

        public long Id { get; set; }
        public long? Timestamp { get; set; }
        public string Username { get; set; }
        public int Actiontypeid { get; set; }
        public string Remark { get; set; }
        public long? Usernamenavigationid { get; set; }

        public ActionType Actiontype { get; set; }
        public User Usernamenavigation { get; set; }
        public ICollection<UserRole> Userrole { get; set; }
        public ICollection<WorkItem> Workitem { get; set; }
        public ICollection<Workflow> Workflow { get; set; }
    }
}
