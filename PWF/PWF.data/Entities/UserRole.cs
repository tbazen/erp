﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.data.Entities
{
    public partial class UserRole
    {
        public int Id { get; set; }
        public long Userid { get; set; }
        public long Roleid { get; set; }
        public long? Aid { get; set; }

        public UserAction A { get; set; }
        public Role Role { get; set; }
        public User User { get; set; }
    }
}
