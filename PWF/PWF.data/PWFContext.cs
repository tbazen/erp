﻿using PWF.data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;


namespace PWF.data
{
    public class PWFContext : DbContext
    {

        public PWFContext(DbContextOptions<PWFContext> options) : base(options)
        {

        }



        public DbSet<User> User { get; set; }
        public DbSet<Entities.ActionType> ActionType { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<UserAction> UserAction { get; set; }
        public DbSet<UserRole> UserRole { get; set; }
        public DbSet<Workflow> Workflow { get; set; }
        public DbSet<WorkflowType> WorkflowType { get; set; }
        public DbSet<WorkItem> WorkItem { get; set; }
        public DbSet<AuditLog> AuditLog { get; set; }
        public DbSet<WorkflowDocuments> WorkflowDocuments { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbQuery<WFSearchResultModel> WFSearchModel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Host=localhost;Database=pwf;Username=postgres;Password=postgres");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<ActionType>(entity =>
            {
                entity.ToTable("actiontype");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('actiontype_seq'::regclass)");

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<AuditLog>(entity =>
            {
                entity.ToTable("auditlog");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('auditlog_seq'::regclass)");

                entity.Property(e => e.KeyValues).HasColumnName("keyvalues");

                entity.Property(e => e.NewValues).HasColumnName("newvalues");
                
                entity.Property(e => e.OldValues).HasColumnName("oldvalues");

                entity.Property(e => e.TableName).HasColumnName("tablename");

                entity.Property(e => e.TimeStamp).HasColumnName("timestamp");

                entity.Property(e => e.UserAction).HasColumnName("useraction");

                entity.Property(e => e.UserName).HasColumnName("username");
            });

            modelBuilder.Entity<Document>(entity =>
            {
                entity.ToTable("documents");

                entity.Property(e => e.ID)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('documents_seq'::regclass)");

                entity.Property(e => e.ByteString).HasColumnName("bytestring");

                entity.Property(e => e.Contentdisposition).HasColumnName("contentdisposition");

                entity.Property(e => e.Contenttype).HasColumnName("contenttype");
                
                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Documenttypeid).HasColumnName("documenttypeid");

                entity.Property(e => e.File).HasColumnName("file");

                entity.Property(e => e.Fileid).HasColumnName("fileid");

                entity.Property(e => e.Filename).HasColumnName("filename");

                entity.Property(e => e.Length).HasColumnName("length");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.WorkflowId)
                    .HasColumnName("workflowid")
                    .HasColumnType("uuid");
            });

            modelBuilder.Entity<DocumentType>(entity =>
            {
                entity.ToTable("documenttypes");

                entity.Property(e => e.ID)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('documenttypes_seq'::regclass)");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name).HasColumnName("name");
            });


            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('role_seq'::regclass)");

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('user_seq'::regclass)");

                entity.Property(e => e.Employeeid).HasColumnName("employeeid");

                entity.Property(e => e.Fullname).HasColumnName("fullname");

                entity.Property(e => e.Regon).HasColumnName("regon");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Username).HasColumnName("username");
            });

            modelBuilder.Entity<UserAction>(entity =>
            {
                entity.ToTable("useraction");

                entity.HasIndex(e => e.Actiontypeid)
                    .HasName("ix_useraction_actiontypeid");

                entity.HasIndex(e => e.Usernamenavigationid)
                    .HasName("ix_useraction_usernamenavigationid");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('useraction_seq'::regclass)");

                entity.Property(e => e.Actiontypeid).HasColumnName("actiontypeid");

                entity.Property(e => e.Remark).HasColumnName("remark");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");

                entity.Property(e => e.Username).HasColumnName("username");

                entity.Property(e => e.Usernamenavigationid).HasColumnName("usernamenavigationid");

                entity.HasOne(d => d.Actiontype)
                    .WithMany(p => p.Useraction)
                    .HasForeignKey(d => d.Actiontypeid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_useraction_actiontype_actiontypeid");

                entity.HasOne(d => d.Usernamenavigation)
                    .WithMany(p => p.Useraction)
                    .HasForeignKey(d => d.Usernamenavigationid)
                    .HasConstraintName("fk_useraction_user_usernamenavigationid");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("userrole");

                entity.HasIndex(e => e.Aid)
                    .HasName("ix_userrole_aid");

                entity.HasIndex(e => e.Roleid)
                    .HasName("ix_userrole_roleid");

                entity.HasIndex(e => e.Userid)
                    .HasName("ix_userrole_userid");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('userrole_seq'::regclass)");

                entity.Property(e => e.Aid).HasColumnName("aid");

                entity.Property(e => e.Roleid).HasColumnName("roleid");

                entity.Property(e => e.Userid).HasColumnName("userid");

                entity.HasOne(d => d.A)
                    .WithMany(p => p.Userrole)
                    .HasForeignKey(d => d.Aid)
                    .HasConstraintName("fk_userrole_useraction_aid");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.Roleid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userrole_role_roleid");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userrole)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userrole_user_userid");
            });

            modelBuilder.Entity<Workflow>(entity =>
            {
                entity.ToTable("workflow");

                entity.HasIndex(e => e.Aid)
                    .HasName("ix_workflow_aid");

                entity.HasIndex(e => e.Typeid)
                    .HasName("ix_workflow_typeid");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("uuid")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aid).HasColumnName("aid");

                entity.Property(e => e.Currentstate).HasColumnName("currentstate");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Employeeid).HasColumnName("employeeid");

                entity.Property(e => e.Initiatoruser).HasColumnName("initiatoruser");

                entity.Property(e => e.Observer).HasColumnName("observer");

                entity.Property(e => e.Parentwfid)
                    .HasColumnName("parentwfid")
                    .HasColumnType("uuid");

                entity.Property(e => e.Typeid).HasColumnName("typeid");

                entity.HasOne(d => d.A)
                    .WithMany(p => p.Workflow)
                    .HasForeignKey(d => d.Aid)
                    .HasConstraintName("fk_workflow_useraction_aid");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Workflow)
                    .HasForeignKey(d => d.Typeid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_workflow_workflowtype_typeid");
            });

            modelBuilder.Entity<WorkflowDocuments>(entity =>
            {
                entity.ToTable("workflowdocuments");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('workflowdocuments_seq'::regclass)");

                entity.Property(e => e.Contentdisposition).HasColumnName("contentdisposition");

                entity.Property(e => e.Contenttype).HasColumnName("contenttype");

                entity.Property(e => e.File).HasColumnName("file");

                entity.Property(e => e.Filename).HasColumnName("filename");

                entity.Property(e => e.Length).HasColumnName("length");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Workflowid)
                    .IsRequired()
                    .HasColumnName("workflowid")
                    .HasColumnType("uuid");
            });

            modelBuilder.Entity<WorkflowType>(entity =>
            {
                entity.ToTable("workflowtype");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('workflowtype_seq'::regclass)");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<WorkItem>(entity =>
            {
                entity.ToTable("workitem");

                entity.HasIndex(e => e.Aid)
                    .HasName("ix_workitem_aid");

                entity.HasIndex(e => e.Assignedrolenavigationid)
                    .HasName("ix_workitem_assignedrolenavigationid");

                entity.HasIndex(e => e.Assignedusernavigationid)
                    .HasName("ix_workitem_assignedusernavigationid");

                entity.HasIndex(e => e.WorkflowId)
                    .HasName("ix_workitem_workflowid");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("uuid")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aid).HasColumnName("aid");

                entity.Property(e => e.Assignedrole).HasColumnName("assignedrole");

                entity.Property(e => e.Assignedrolenavigationid).HasColumnName("assignedrolenavigationid");

                entity.Property(e => e.Assigneduser).HasColumnName("assigneduser");

                entity.Property(e => e.Assignedusernavigationid).HasColumnName("assignedusernavigationid");

                entity.Property(e => e.Data).HasColumnName("data").HasColumnType("json");

                entity.Property(e => e.Datatype).HasColumnName("datatype");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Fromstate).HasColumnName("fromstate");

                entity.Property(e => e.Seqno).HasColumnName("seqno");

                entity.Property(e => e.Trigger).HasColumnName("trigger");

                entity.Property(e => e.WorkflowId)
                    .IsRequired()
                    .HasColumnName("workflowid")
                    .HasColumnType("uuid");

                entity.HasOne(d => d.A)
                    .WithMany(p => p.Workitem)
                    .HasForeignKey(d => d.Aid)
                    .HasConstraintName("fk_workitem_useraction_aid");

                entity.HasOne(d => d.Assignedrolenavigation)
                    .WithMany(p => p.WorkItem)
                    .HasForeignKey(d => d.Assignedrolenavigationid)
                    .HasConstraintName("fk_workitem_role_assignedrolenavigationid");

                entity.HasOne(d => d.Assignedusernaviagtion)
                    .WithMany(p => p.Workitem)
                    .HasForeignKey(d => d.Assignedusernavigationid)
                    .HasConstraintName("fk_workitem_user_assignedusernavigationid");

                entity.HasOne(d => d.Workflow)
                    .WithMany(p => p.Workitem)
                    .HasForeignKey(d => d.WorkflowId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_workitem_workflow_workflowid");
            });

            modelBuilder.HasSequence("actiontype_seq");

            modelBuilder.HasSequence("auditlog_seq");

            modelBuilder.HasSequence("documents_seq");

            modelBuilder.HasSequence("documenttypes_seq");

            modelBuilder.HasSequence("role_seq");

            modelBuilder.HasSequence("user_seq");

            modelBuilder.HasSequence("useraction_seq");

            modelBuilder.HasSequence("userrole_seq");

            modelBuilder.HasSequence("workflowdocuments_seq");

            modelBuilder.HasSequence("workflowtype_seq");
        }

        public bool ContextOwnsConnection { get; }

        public void SaveChanges(string username, UserAction userAction)
        {
            UserAction.Add(userAction);
            base.SaveChanges();
            var auditEnries = OnBeforeSaveChanges(username, userAction.Id);
            base.SaveChanges();
            OnAfterSaveChanges(auditEnries);
        }

        public UserAction SaveChanges(string username, int actionType)
        {
            var userAction = new UserAction
            {
                Actiontypeid = actionType,
                Username = username,
                Timestamp = DateTime.Now.Ticks
            };
            UserAction.Add(userAction);
            var auditEnries = OnBeforeSaveChanges(username, userAction.Id);
            base.SaveChanges();
            OnAfterSaveChanges(auditEnries);

            return userAction;
        }

        private List<AuditEntry> OnBeforeSaveChanges(string username, long actionId)
        {
            ChangeTracker.DetectChanges();
            var auditEntries = new List<AuditEntry>();

            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity is AuditLog || entry.Entity is UserAction || entry.State == EntityState.Detached ||
                    entry.State == EntityState.Unchanged)
                    continue;

                var auditEntry = new AuditEntry(entry)
                {
                    TableName = entry.Metadata.Model.ToString(),
                    UserName = username,
                    UserAction = actionId
                };
                auditEntries.Add(auditEntry);

                foreach (var property in entry.Properties)
                {
                    if (property.IsTemporary) //For auto-generated properties such as id
                    {
                        //get the value after save
                        auditEntry.TemporaryProperties.Add(property);
                        continue;
                    }

                    var propertyName = property.Metadata.Name;
                    if (property.Metadata.IsPrimaryKey())
                    {
                        auditEntry.KeyValues[propertyName] = property.CurrentValue;
                        continue;
                    }

                    switch (entry.State)
                    {
                        case EntityState.Added:
                            auditEntry.NewValues[propertyName] = property.CurrentValue;
                            break;

                        case EntityState.Deleted:
                            auditEntry.OldValues[propertyName] = property.OriginalValue;
                            break;

                        case EntityState.Modified:
                            if (property.IsModified)
                            {
                                auditEntry.OldValues[propertyName] = property.OriginalValue;
                                auditEntry.NewValues[propertyName] = property.CurrentValue;
                            }

                            break;
                    }
                }
            }

            //Save audit log for all the changes
            foreach (var auditEntry in auditEntries.Where(_ => !_.HasTemporaryProperties))
                this.AuditLog.Add(auditEntry.ToAudit());

            //return those for which we need to get their primary keys for
            return auditEntries.Where(_ => _.HasTemporaryProperties).ToList();
        }

        private void OnAfterSaveChanges(List<AuditEntry> entries)
        {
            if (entries == null || entries.Count == 0) return;


            foreach (var auditEntry in entries)
            {
                //Get the auto-generated values
                foreach (var prop in auditEntry.TemporaryProperties)
                    if (prop.Metadata.IsPrimaryKey())
                        auditEntry.KeyValues[prop.Metadata.Name] = prop.CurrentValue;
                    else
                        auditEntry.NewValues[prop.Metadata.Name] = prop.CurrentValue;

                AuditLog.Add(auditEntry.ToAudit());
            }

            base.SaveChanges();
        }
    }
}
