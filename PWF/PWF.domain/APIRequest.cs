﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PWF.domain
{
    public class PWFConfiguration
    {
        public string Host { get; set; }
        public bool Ethiopian { get; set; }
        public string OrganizationName { get; set; }
        public int CostCenterID { get; set; }
        public int TaxCenter { get; set; }
        public string CityName { get; set; }
        public string OrganizationLogo { get; set; }
        public int StoreIssueVoucherBatchID { get; set; }
        public double TwoStepApprovalMinimumAmount { get; set; }
        public DocumentTypeIDs DocumentTypeIDs { get; set; }
        public BatchIDs BatchIDs { get; set; }
        public string SupplierAdvancedPaymentCode { get; set; }
        public string SupplierCreditSettlementCode { get; set; }



    }

    public class DocumentTypeIDs
    {
        public int StoreIssueDocument { get; set; }
        public int BankWithdrawalDocument { get; set; }
        public int LabourPaymentDocument { get; set; }
        public int CashierToCashierDocument { get; set; }
    }

    public class BatchIDs
    {
        public int SIV { get; set; }
        public int CPV { get; set; }
        public int CHKPV { get; set; }
        public int SRV { get; set; }
        public int PO { get; set; }
        public int SRN { get; set; }
        public int PRN { get; set; }


    }


    public class APIRequest
    {
        public static async Task<T> GetRequest<T>(string url,string path)
        {
           T result;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var response = await client.GetAsync(path);
                response.EnsureSuccessStatusCode();
                var stringResult = await response.Content.ReadAsStringAsync();
                 result = JsonConvert.DeserializeObject<T>(stringResult);
            }
            return result;
        }

        public static async Task<T> PostRequest<T,S>(string url,string path,S Model)
        {
            T result;
            var data = JsonConvert.SerializeObject(Model);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                var response = await client.PostAsync(path, new StringContent(data, Encoding.UTF8, "application/json"));
                response.EnsureSuccessStatusCode();
                var stringResult = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<T>(stringResult);
                return result;
            }
        }
    }
}
