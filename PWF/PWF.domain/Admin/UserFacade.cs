﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Infrastructure;

namespace PWF.domain.Admin
{
    public interface IUserFacade
    {
        UserSession LoginUser(UserSession session, LoginViewModel loginView, string WSISUrl);
        void RegisterUser(UserSession session, PWF.domain.ViewModels.RegisterViewModel user);
        IList<UserDetialViewModel> GetAllUsers(UserSession session);
        void DeactivateUser(UserSession session, string username);
        void ActivateUser(UserSession session, string username);
        void UpdateUser(UserSession session, UserViewModel userModel);
        bool CheckUser(UserSession userSession, string username);
        IList<UserActionViewModel> GetAllAction(UserSession getSession);
        IList<UserViewModel> GetUsers(UserSession userSession, string query);
        User GetUser(UserSession userSession, string username);
        void AddUserRole(UserSession userSession, string username, int[] roles);

        object GetRoles(UserSession session);
        long GetRole(UserSession session);

        int GetUserCostCenter(string userName);
    }


    public class UserFacade : IUserFacade
    {
        private readonly IUserService _userService;

        public UserFacade(IUserService service)
        {
            _userService = service;
        }

        public UserSession LoginUser(UserSession userSession, LoginViewModel loginView, string WSISUrl)
        {
            _userService.SetSession(userSession);
            var session = _userService.LoginUser(loginView, WSISUrl);
            return session;

        }

        public void RegisterUser(UserSession userSession, PWF.domain.ViewModels.RegisterViewModel registerViewModel)
        {
            _userService.SetSession(userSession);
            _userService.RegisterUser(registerViewModel);
        }



        public IList<UserViewModel> GetUsers(UserSession userSession, string query)
        {
            _userService.SetSession(userSession);

            return _userService.GetUsers(query);
        }

        public User GetUser(UserSession userSession, string username)
        {
            _userService.SetSession(userSession);
            return _userService.GetUser(username);
        }

        public object GetRoles(UserSession session)
        {
            _userService.SetSession(session);

            return _userService.GetRoles();
        }

        public long GetRole(UserSession session)
        {
            return _userService.GetRoles(session);
        }

        public int GetUserCostCenter(string userName)
        {
            return _userService.GetUserCostCenter(userName);
        }

        public IList<UserDetialViewModel> GetAllUsers(UserSession userSession)
        {
            _userService.SetSession(userSession);

            return _userService.GetAllUsers();
        }

        public IList<UserActionViewModel> GetAllAction(UserSession userSession)
        {
            _userService.SetSession(userSession);
            return _userService.GetAllActions();
        }

        public void AddUserRole(UserSession userSession, string username, int[] roles)
        {
            _userService.SetSession(userSession);
            _userService.AddUserRole(username, roles);
        }

        public void DeactivateUser(UserSession userSession, string username)
        {
            _userService.SetSession(userSession);
            _userService.DeactivateUser(username);
        }

        public void ActivateUser(UserSession userSession, string username)
        {
            _userService.SetSession(userSession);
            _userService.ActivateUser(username);
        }

        public void UpdateUser(UserSession userSession, UserViewModel userVm)
        {
            _userService.SetSession(userSession);
            _userService.UpdateUser(userVm);
        }

        public bool CheckUser(UserSession userSession, string username)
        {
            _userService.SetSession(userSession);

            return _userService.CheckUser(username);
        }


    }
}
