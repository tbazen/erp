﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Admin
{

    public class WSISResponseModel
    {
        public string SessionID { get; set; }
        public int UserID { get; set; }
    }


    public enum UserActionType
    {
        Login = 1,
        Logout = 2,
        Register = 3,
        UpdateUser = 4,
        ActivateUser = 5,
        DeactivateUser = 6,
        ChangePassword = 7,
        ResetPassword = 8,

        AddWorkflow = 9,
        UpdateWorkflow = 10,
        AddWorkItem = 11,

        AddDocument = 12,
        UpdateDocument = 13,
        DeleteDocument = 14

        

    }

    public enum UserRoles
    {
      Employee = 1,
      StoreKeeper = 2,
      StoreManager = 3,
      FinanceManager = 4,
      Owner = 5,
      Cashier = 6,
      Finalizer = 7,
      Purchaser1 = 8,
      Purchaser2 = 9,
      Purchaser3 = 10,
      FinanceOfficer = 11,
      
    }

    public class UserViewModel
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int Status { get; set; }
        public string[] Roles { get; set; }
    }

    public class UserDetialViewModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public int? EmployeeID { get; set; }
        public int Status { get; set; }
        public string[] Roles { get; set; }
        public string RegOn { get; set; }
        public string LastSeen { get; set; }
    }

    public class UserActionViewModel
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Action { get; set; }
        public DateTime ActionTime { get; set; }
    }

    public class LoginViewModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public long Role { get; set; }
    }

    public class RegisterViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public int[] Roles { get; set; }
    }

    public class ChangePasswordViewModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
    }

    public class UserRoleViewModel
    {
        public string UserName { get; set; }
        public int[] Roles { get; set; }

    }
}
