﻿using INTAPS.Payroll.BDE;
using Newtonsoft.Json;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Exceptions;
using PWF.domain.Infrastructure;
using PWF.domain.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static INTAPS.ClientServer.SecurityProvider;

namespace PWF.domain.Admin
{
    public interface IUserService
    {
        void SetSession(UserSession session);

        WSISResponseModel WSISLogin(string url, string username, string password);

        UserSession LoginUser(LoginViewModel loginView, string WSISUrl);

        void RegisterUser(PWF.domain.ViewModels.RegisterViewModel user);

        void AddUserRole(string username, int[] roles);

        IList<UserDetialViewModel> GetAllUsers();
        bool CheckUser(string username);
        void UpdateUser(UserViewModel userModle);
        void ActivateUser(string username);
        void DeactivateUser(string username);
        IList<UserActionViewModel> GetAllActions();
        User GetUser(string username);
        IList<UserViewModel> GetUsers(string query);
        bool HasRole(string userWname, int role);
        object GetRoles();
        long GetRoles(UserSession userSession);

        int GetUserCostCenter(string userName);
        UserSession CheckAuthentication(string username, string password);
    }

    public class UserService : IUserService
    {
        private readonly IUserActionService _actionService;
        private readonly PWFContext _context;
        private UserSession _userSession;

        public UserService(PWFContext context, IUserActionService actionService)
        {
            _context = context;
            _actionService = actionService;
        }

        public void SetSession(UserSession session)
        {
            _userSession = session;
        }


        //public async Task<string> WSISLogin(string url, string username, string password)
        //{
        //    try
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(url);
        //            var response = await client.GetAsync($"/payroll/login?source=WSISLight&username={username}&password={password}");
        //            response.EnsureSuccessStatusCode();
        //            var stringResult = await response.Content.ReadAsStringAsync();
        //            string sessionID = JsonConvert.DeserializeObject(stringResult).ToString();
        //            return sessionID;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        //if(e.Message == "Invalid password or username")
        //        //{
        //        //    return "Wrong Password";
        //        //}
        //        return "Wrong Password";
        //    }
        //}


        public UserSession CheckAuthentication(string username, string password)
        {


            //var dbUser = _context.User.First(u => u.Username == username);
            //if (dbUser == null) throw new Exception("Invalid username");
            //if (dbUser.Status == 0) throw new AccessDeniedException("User Deactivated");
            //var roles = GetRoles(username);

            UserPermissions response = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUserPermmissions(username, password);

                var roles = Helper.GetPWFRoles(response);

                var costCenterid = GetUserCostCenter(username);
                return new UserSession()
                {
                    Username = username,
                    CreatedTime = DateTime.Now,
                    LastSeen = DateTime.Now,
                    Roles = roles,
                    //Role = role,
                    //  EmployeeID = dbUser.EmployeeID,
                    Id = response.UserID,
                    costCenterID = costCenterid,
                    wsisUserID = response.UserID
                };
            
           
        }

        public WSISResponseModel WSISLogin(string url, string username, string password)
        {
            
            var wsissession = INTAPS.ClientServer.ApplicationServer.CreateUserSession(username, password, "PWF Web");
            var sinfo = INTAPS.ClientServer.ApplicationServer.getSessionInfo(wsissession);
            
            return new WSISResponseModel()
            {
                SessionID = wsissession,
                UserID = sinfo.UserID,

            };
        }

        public bool WSISAPILogout(string sessionID, string url)
        {

            try
            {
                INTAPS.ClientServer.ApplicationServer.CloseSession(sessionID);
                return true;
            }

            catch (Exception)
            {
                return true;
            }
        }

        public UserSession LoginUser(LoginViewModel loginView, string url)
        {

            UserPermissions response = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUserPermmissions(loginView.username, loginView.password);
            var wsisResponse = WSISLogin(url, loginView.username, loginView.password);
            var sessionID = wsisResponse.SessionID;
            if (sessionID == "Wrong Password") throw new AccessDeniedException("Invalid Username or Password");
            var roles = Helper.GetPWFRoles(response);

            _actionService.AddUserAction(new UserSession { Username = loginView.username }, UserActionType.Login);
            _context.SaveChanges();

            var costCenterid = GetUserCostCenter(loginView.username);
            return new UserSession()
            {
                Username = loginView.username,
                payrollSessionID = sessionID,
                CreatedTime = DateTime.Now,
                LastSeen = DateTime.Now,
                Roles = roles,
                Role = loginView.Role,
                Id = response.UserID,
                costCenterID = costCenterid,
                wsisUserID = response.UserID
            };


        }

        public void RegisterUser(PWF.domain.ViewModels.RegisterViewModel userVm)
        {
            var user = new User
            {
                Username = userVm.Username,
                Employeeid = userVm.EmpID,
                Status = 1
            };


            _context.User.Add(user);

            foreach (var ur in userVm.Roles)
            {
                var role = GetRole(ur);

                var userRole = new UserRole
                {
                    User = user,
                    Role = role
                };
                _context.UserRole.Add(userRole);
            }
            //ActivateUser(userVm.Username);
            _context.SaveChanges(_userSession.Username, (int)UserActionType.Register);

        }



        public void AddUserRole(string username, int[] roles)
        {
            var user = GetUser(username);
            var userRoles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Id);
            foreach (var role in roles)
            {
                if (userRoles.Contains(role)) continue;
                var userRole = new UserRole { User = user, Role = GetRole(role) };
                _context.UserRole.Add(userRole);
            }

            var userAction = _actionService.AddUserAction(_userSession, UserActionType.Register);
            _context.SaveChanges(_userSession.Username, userAction);
        }

        public object GetRoles()
        {
            return _context.UserRole.Where(ur => ur.User.Username.Equals(_userSession.Username))
                .Select(ur => new { id = ur.Roleid, name = ur.Role.Name }).ToList();
        }

        public long GetRoles(UserSession user)
        {
            return _context.UserRole.Where(ur => ur.User.Username.Equals(user.Username))
                .Select(ur => ur.Roleid).FirstOrDefault();
        }

        internal long[] GetRoles(string username)
        {
            return _context.UserRole.Where(ur => ur.User.Username.Equals(username))
                .Select(ur => ur.Roleid).ToArray();
        }

        public void DeactivateUser(string username)
        {
            var user = GetUser(username);

            user.Status = 0;
            _context.User.Update(user);

            var userAction = _actionService.AddUserAction(_userSession, UserActionType.DeactivateUser);
            //userAction.Id = 0;
            //_context.SaveChanges(_userSession.Username, userAction);
        }

        public void ActivateUser(string username)
        {
            var user = GetUser(username);
            user.Status = 1;

            _context.User.Update(user);

            var userAction = _actionService.AddUserAction(_userSession, UserActionType.ActivateUser);
            //_context.SaveChanges(_userSession.Username, userAction);
        }

        public IList<UserViewModel> GetUsers(string filter)
        {
            var users = _context.User.Where(u => u.Username.Contains(filter) || u.Fullname.Contains(filter));
            var userVms = new List<UserViewModel>();

            foreach (var user in users)
            {
                userVms.Add(new UserViewModel
                {
                    UserName = user.Username,
                    FullName = user.Fullname,
                    Status = user.Status,
                    Roles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Name).ToArray()
                });
            }

            return userVms;
        }

        public IList<UserDetialViewModel> GetAllUsers()
        {
            var users = _context.User;

            var userVms = new List<UserDetialViewModel>();

            foreach (var user in users)
            {
                var userVm = new UserDetialViewModel
                {
                    UserName = user.Username,
                    Status = user.Status,
                    Id = user.Id
                };
                if (user.Employeeid != null)
                {

                    userVm.FullName = user.Fullname;
                    userVm.EmployeeID = user.Employeeid;
                }
                userVm.Roles = _context.UserRole.Where(u => u.Userid == user.Id).Select(ur => ur.Role.Name).ToArray();


                var time = _context.UserAction.Where(u => u.Username == user.Username && u.Actiontypeid == 1)
                    .Select(ua => ua.Timestamp).DefaultIfEmpty(0).Max();
                var lastSeen = time ?? -1;
                var dt = new DateTime(lastSeen);
                userVm.LastSeen = string.Format("{0:G}", dt);
                var regDate = new DateTime(user.Regon);
                userVm.RegOn = string.Format("{0:G}", regDate);

                userVms.Add(userVm);
            }

            return userVms;
        }


        public IList<UserActionViewModel> GetAllActions()
        {
            var actions = _context.UserAction.ToList();

            var actionVms = new List<UserActionViewModel>();

            foreach (var userAction in actions)
            {
                var username = GetUser(userAction.Username).Username;
                var actionType = GetActionType(userAction.Actiontypeid).Name;
                if (!actionType.Equals("LOGIN"))
                    actionVms.Add(new UserActionViewModel
                    {
                        UserName = username,
                        Action = actionType,
                        ActionTime = new DateTime(userAction.Timestamp ?? -1)
                    });
            }

            return actionVms;
        }

        public void UpdateUser(UserViewModel userVm)
        {
            var user = GetUser(userVm.UserName);

            user.Fullname = userVm.FullName;


            var userRoles = _context.UserRole.Where(u => u.Userid == user.Id);

            //Remove Previous roles
            foreach (var role in userRoles) _context.UserRole.Remove(role);

            foreach (var role in userVm.Roles)
            {
                var ur = _context.Role.First(r => r.Name.Equals(role));
                var userRole = new UserRole { User = user, Role = ur };

                _context.UserRole.Add(userRole);
            }

            var userAction = _actionService.AddUserAction(_userSession, UserActionType.UpdateUser);

            _context.User.Update(user);

            _context.SaveChanges(_userSession.Username, userAction);
        }

        public User GetUser(string username)
        {
            return _context.User.First(u => u.Username == username);
        }

        public bool HasRole(string username, int role)
        {
            var userId = GetUser(username).Id;
            var roleId = GetRole(role).Id;

            return _context.UserRole.Any(ur => ur.Roleid == roleId && ur.Userid == userId);
        }

        public bool CheckUser(string username)
        {
            return _context.User.Any(u => u.Username == username);
        }

        private ActionType GetActionType(int id)
        {
            return _context.ActionType.First(at => at.Id == id);
        }

        private UserAction GetUserAction(User user, UserActionType type)
        {
            var actionType = GetActionType((int)type);

            var action = new UserAction
            {
                Timestamp = DateTime.Now.Ticks,
                Actiontype = actionType,
                Username = user.Username
            };


            return action;
        }

        private User GetUser(long id)
        {
            return _context.User.First(u => u.Id == id);
        }

        private Role GetRole(int role)
        {
            return _context.Role.First(r => r.Id == role);
        }

        public int GetUserCostCenter(string userName){
            //string path = $"/pwf/getEmployeeCostCenter?loginName={userName}&sessionID={sessionID}";
            //var result = APIRequest.GetRequest<int>(url, path).Result;
            //return result;
            PayrollBDE _bde = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            var employee = _bde.GetEmployeeByLoginName(userName);
            if (employee == null)
                return -1;
            return employee.costCenterID;
        }
    }
}
