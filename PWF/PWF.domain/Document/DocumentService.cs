using PWF.data;

using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using Microsoft.Extensions.Caching.Memory;
using PWF.domain.Workflows;
using PWF.data.Entities;
using PWF.domain.Admin;
using System.Linq;
using System;

namespace PWF.domain.Documents
{
    public interface IDocumentService : IPWFService
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        Document AddDocument(Document doc);
        Document GetDocument(Guid wfid);
        Document GetDocument(int id);
        void RemoveDocument(int DocID);
    }

    public class DocumentService : PWFService, IDocumentService{


        private PWFContext _context;
        private UserSession _session;
        private string _url;
        private string _sessionID;
        private IMemoryCache _cache;
        private PWFConfiguration _config;

        private IWorkflowService _wfService;
        private IUserActionService _userActionService;
        
        
        public DocumentService(PWFContext Context, IMemoryCache memoryCache, IWorkflowService wfService, IUserActionService actionSercice)
        {
            _context = Context;
            _cache = memoryCache;
            _wfService = wfService;
            _wfService.SetContext(Context);
            _userActionService = actionSercice;

        }
        public void SetSession(UserSession session)
        {
            _session = session;
            _sessionID = session.payrollSessionID;
            _wfService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _sessionID = session.payrollSessionID;
            _session = session;
            _url = Config.Host;
            _config = Config;
        }

        public Document AddDocument(Document doc)
        {
            _context.Documents.Add(doc);
            _context.SaveChanges(_session.Username, (int)UserActionType.AddDocument);
            return doc;
        }

        public Document GetDocument(Guid wfid)
        {
            var doc = _context.Documents.Where(m => m.WorkflowId == wfid).FirstOrDefault();
            return doc;
        }

        public Document GetDocument(int id)
        {
            var doc = _context.Documents.Find(id);
            return doc;
        }

        public void RemoveDocument(int DocID)
        {
            _context.Documents.Remove(_context.Documents.Find(DocID));
            _context.SaveChanges(_session.Username, (int)UserActionType.DeleteDocument);
        }
    }
}