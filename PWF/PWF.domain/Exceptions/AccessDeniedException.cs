﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Exceptions
{
    public class AccessDeniedException : Exception
    {
        public AccessDeniedException(string message) : base(message)
        {
        }
    }
}
