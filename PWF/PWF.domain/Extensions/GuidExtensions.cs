﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Extensions
{
    public static class GuidExtensions
    {
        public static Guid ToGuid(this string s)
        {
            return Guid.Parse(s);
        }
    }
}
