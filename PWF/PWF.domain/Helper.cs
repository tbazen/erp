﻿using BIZNET.iERP.Server;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using PWF.domain.Admin;
using PWF.domain.Infrastructure;
using PWF.domain.Payment.StateMachines;
using PWF.domain.Store.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static INTAPS.ClientServer.SecurityProvider;

namespace PWF.domain
{
    public static class Helper
    {

        public static int PostGenericDocument(AccountDocument Doc, int AID)
        {



            AccountingBDE _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();



            lock (_IERPService.WriterHelper)
            {

                int docID = _AccountingService.PostGenericDocument(AID, Doc);
                return docID;

            }
        }
        public static double getWithHeldAMount(int documentID)
        {

            //TODO: logic for detecting VAT witholding
            AccountingBDE _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            var trans=_AccountingService.GetTransactionsOfDocument(documentID);

            var ret = 0.0d;
            foreach(var t in trans)
            {
                var accountID = _AccountingService.GetCostCenterAccountAccountID<Account>(t.AccountID);
                if (_AccountingService.IsControlOf<Account>(_IERPService.SysPars.collectedWithHoldingTaxAccountID,accountID)
                    || _AccountingService.IsControlOf<Account>(_IERPService.SysPars.collectedWithHoldingTaxNotInvoicedAccountID, accountID)
                   || _AccountingService.IsControlOf<Account>(_IERPService.SysPars.laborIncomeTaxAccountID, accountID)
                    )
                    ret -= t.Amount;

            }
            return ret;

        }
        public static long [] GetPWFRoles(UserPermissions user)
        {
            //var allRoles=ApplicationServer.SecurityBDE.GetChildList(ApplicationServer.SecurityBDE.GetObjectID("root/PWF");
            var ret = new List<long>();
            foreach (UserRoles role in Enum.GetValues(typeof(UserRoles)))
            {
                if (user.IsPermited("root/PWF/" + role))
                    ret.Add((long)(int)role);
            }
            return ret.ToArray();
        }

        public static int PostGenericDocument(AccountDocument Doc,int AID, double allowedAmount, int[] sourceIDs)
        {
        
            
            
            AccountingBDE _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            


            lock (_IERPService.WriterHelper)
            {

                    int docID = _AccountingService.PostGenericDocument(AID, Doc);
                    var trans = GetTransactionOfBatch(docID);
                    var total = 0.0;
                    foreach (var t in trans)
                    {
                        if (sourceIDs.Where(x => x == t.AccountID).Any())
                            total += t.Amount;
                        //}
                    }
                total = (-1) * total;
                if(allowedAmount != -1 && total > allowedAmount)
                {
                    throw new Exception($"You are requesting an amount greater than allowed, The amount exceeds by {total - allowedAmount}");
                }
                else
                {
                    return docID;

                }

            }
        }

        public static void UseSerial(UsedSerial ser,int AID)
        {

            AccountingBDE _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();

            
            lock (_IERPService.WriterHelper)
            {

                    _AccountingService.UseSerial(AID, ser);

                }
            }


        public static double GetTransactionAmountFromPaymentSource(List<int> docIDs, int[] AccountIDs)
        {
            List<double> Amount = new List<double>();
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            var assetAccounts = new List<int>();
            assetAccounts.AddRange(_IERPService.GetAllCashAccounts(true).Select(x => x.csAccountID));
            assetAccounts.AddRange(_IERPService.GetAllBankAccounts().Select(x => x.mainCsAccount));

            foreach (var docID in docIDs)
            {
                var trans = GetTransactionOfBatch(docID);
                var total = 0.0;
                foreach (var t in trans)
                {
                    if (AccountIDs.Where(x => x == t.AccountID).Any())
                        total += t.Amount;
                    //}
                }
                var withHeld = getWithHeldAMount(docID);
                total -= withHeld;
                Amount.Add(-(total));
            }
            return Math.Round(Amount.Sum(),2);
        }


        public static double GetTransactionAmount(List<int> docIDs)
        {
            List<double> Amount = new List<double>();
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            var assetAccounts = new List<int>();
            assetAccounts.AddRange(_IERPService.GetAllCashAccounts(true).Select(x => x.csAccountID));
            assetAccounts.AddRange(_IERPService.GetAllBankAccounts().Select(x => x.mainCsAccount));

            foreach (var docID in docIDs)
            {
                var trans = GetTransactionOfBatch(docID);
                var total = 0.0;
                
                foreach (var t in trans)
                {
                   // if (assetAccounts.Where(x => x == t.AccountID).Any())
                        //if (AccountIDs.Where(x => x == t.AccountID).Any())
                      if (t.Amount > 0 && assetAccounts.Where(x => x == t.AccountID).Any())
                         total += t.Amount;
                    //}
                }
                if(total == 0)
                {
                    foreach (var t in trans)
                    {
                        if (t.Amount < 0 && assetAccounts.Where(x => x == t.AccountID).Any())
                            total -= t.Amount;
                    }
                }
                var withHeld = getWithHeldAMount(docID);
                total += withHeld;
                Amount.Add((total));
            }
            return Math.Round(Amount.Sum(),2);
        }

        public static double GetNetTransactionAmount(List<int> docIDs)
        {
            List<double> Amount = new List<double>();
            iERPTransactionBDE _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            var assetAccounts = new List<int>();
            assetAccounts.AddRange(_IERPService.GetAllCashAccounts(true).Select(x => x.csAccountID));
            assetAccounts.AddRange(_IERPService.GetAllBankAccounts().Select(x => x.mainCsAccount));

            foreach (var docID in docIDs)
            {
                var trans = GetTransactionOfBatch(docID);
                var total = 0.0;

                foreach (var t in trans)
                {
                    if (assetAccounts.Where(x => x == t.AccountID).Any())
                        total += t.Amount;
                }
                var withHeld = getWithHeldAMount(docID);
                total -= withHeld;
                Amount.Add(-(total));
            }
            return Math.Round(Amount.Sum(),2);
        }



        public static DocumentTypedReference GetNextSerialType(string prefix, int batchID, out int serial)
        {
            AccountingBDE _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            serial = _AccountingService.GetNextSerial(batchID);
            DocumentSerialType st = _AccountingService.getDocumentSerialType(prefix);
            DocumentTypedReference reference = new DocumentTypedReference()
            {
                primary = true,
                reference = serial.ToString(st.format),
                typeID = st.id
            };

            return reference;
        }

        public static AccountTransaction[] GetTransactionOfBatch(int BatchID)
        {
            AccountingBDE bdeAccounting = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();

            try
            {
                return bdeAccounting.GetTransactionsOfDocument(BatchID);
            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException(ex.Message, ex);
                throw new Exception(ex.Message, ex);
            }


        }

        public static void DeleteGenericDocument(int docID,int AID)
        {
            AccountingBDE bdeAccounting = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();

            lock (bdeAccounting.WriterHelper)
            {

                    bdeAccounting.DeleteGenericDocument(AID, docID);

                
            }
        }

        public static int PaymentWFStateIdentifier(PaymentWorkflow.States FromState, PaymentWorkflow.Triggers trigger)
        {
            if(FromState == PaymentWorkflow.States.Filing && trigger == PaymentWorkflow.Triggers.Request)
            {
                return (int)PaymentWorkflow.States.Checking;
            }
            else if (FromState == PaymentWorkflow.States.Filing && trigger == PaymentWorkflow.Triggers.Cancel)
            {
                return (int)PaymentWorkflow.States.Cancelled;
            }
            else if(FromState == PaymentWorkflow.States.Filing && trigger == PaymentWorkflow.Triggers.Abort)
            {
                return (int)PaymentWorkflow.States.Aborted;
            }
            else if (FromState == PaymentWorkflow.States.Checking && trigger == PaymentWorkflow.Triggers.Check)
            {
                return (int)PaymentWorkflow.States.Reviewing;
            }
            else if (FromState == PaymentWorkflow.States.Checking && trigger == PaymentWorkflow.Triggers.CheckApprove)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else if (FromState == PaymentWorkflow.States.Checking && trigger == PaymentWorkflow.Triggers.Reject1)
            {
                return (int)PaymentWorkflow.States.Filing;
            }
            else if (FromState == PaymentWorkflow.States.Reviewing && trigger == PaymentWorkflow.Triggers.Approve)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else if (FromState == PaymentWorkflow.States.Reviewing && trigger == PaymentWorkflow.Triggers.Reject2)
            {
                return (int)PaymentWorkflow.States.Checking;
            }
            else if (FromState == PaymentWorkflow.States.Payment && trigger == PaymentWorkflow.Triggers.AddDocument)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else if (FromState == PaymentWorkflow.States.Payment && trigger == PaymentWorkflow.Triggers.RemoveDocument)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else if (FromState == PaymentWorkflow.States.Payment && trigger == PaymentWorkflow.Triggers.AttachDocument)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else if (FromState == PaymentWorkflow.States.Payment && trigger == PaymentWorkflow.Triggers.DetachDocument)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else if (FromState == PaymentWorkflow.States.Payment && trigger == PaymentWorkflow.Triggers.Close)
            {
                return (int)PaymentWorkflow.States.FinalCheck;
            }
            else if (FromState == PaymentWorkflow.States.FinalCheck && trigger == PaymentWorkflow.Triggers.Finalize)
            {
                return (int)PaymentWorkflow.States.Closed;
            }
            else if (FromState == PaymentWorkflow.States.FinalCheck && trigger == PaymentWorkflow.Triggers.RejectFinalCheck)
            {
                return (int)PaymentWorkflow.States.Payment;
            }
            else
            {
                return -4;
            }

    }

        public static int ItemDeliveryWFStateIdentifier(ItemDeliveryWorkflow.States FromState, ItemDeliveryWorkflow.Triggers trigger)
        {
           if(FromState == ItemDeliveryWorkflow.States.Filing && trigger == ItemDeliveryWorkflow.Triggers.Request)
            {
                return (int)ItemDeliveryWorkflow.States.Review;
            }
           else if(FromState == ItemDeliveryWorkflow.States.Review && trigger == ItemDeliveryWorkflow.Triggers.Deliver)
            {
                return (int)ItemDeliveryWorkflow.States.Voucher;
            }

           else if(FromState == ItemDeliveryWorkflow.States.Voucher && trigger == ItemDeliveryWorkflow.Triggers.SubmitVoucher)
            {
                return (int)ItemDeliveryWorkflow.States.Close;
            }
            else if (FromState == ItemDeliveryWorkflow.States.Filing && trigger == ItemDeliveryWorkflow.Triggers.Abort)
            {
                return (int)ItemDeliveryWorkflow.States.Aborted;
            }
            else if (FromState == ItemDeliveryWorkflow.States.Review && trigger == ItemDeliveryWorkflow.Triggers.Abort)
            {
                return (int)ItemDeliveryWorkflow.States.Aborted;
            }

            return -4;
        }

        public static int ServiceDeliveryWFStateIdentifier(ServiceDeliveryWorkflow.States FromState, ServiceDeliveryWorkflow.Triggers triggers)
        {
            if(FromState == ServiceDeliveryWorkflow.States.Filing && triggers == ServiceDeliveryWorkflow.Triggers.Request)
            {
                return (int)ServiceDeliveryWorkflow.States.Review;
            }
            else if(FromState == ServiceDeliveryWorkflow.States.Review && triggers == ServiceDeliveryWorkflow.Triggers.Approve)
            {
                return (int)ServiceDeliveryWorkflow.States.Close;
            }
            else if(FromState == ServiceDeliveryWorkflow.States.Review && triggers == ServiceDeliveryWorkflow.Triggers.Reject)
            {
                return (int)ServiceDeliveryWorkflow.States.Cancelled;
            }
            else if(FromState == ServiceDeliveryWorkflow.States.Filing && triggers == ServiceDeliveryWorkflow.Triggers.Abort)
            {
                return (int)ServiceDeliveryWorkflow.States.Aborted;
            }
            else if (FromState == ServiceDeliveryWorkflow.States.Review && triggers == ServiceDeliveryWorkflow.Triggers.Abort)
            {
                return (int)ServiceDeliveryWorkflow.States.Aborted;
            }

            return -4;
        }


    }
}

