﻿using Microsoft.EntityFrameworkCore.Storage;
using PWF.data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace PWF.domain.Infrastructure.Architecture
{
    public interface IPWFFacade
    {
        void PassContext(IPWFService service, PWFContext context);
        //void PassContext(IPWFService service, DbConnection connection);
        //void PassContext(IPWFService service);

        DbTransaction BeginTransaction();
        TReturn Transact<TReturn>(Func<IDbContextTransaction, TReturn> func);
        void Transact(Action<IDbContextTransaction> func);
    }

    public class PWFFacade : IPWFFacade
    {
        protected PWFFacade(PWFContext Context)
        {
            //Connection = new PWFConnection();
            //Connection.SetContext(Context);
            // _dbConnection = Context.Database.GetDbConnection();
            _context = Context;

        }
        protected PWFContext _context;
        protected DbConnection _dbConnection { get; }

        public virtual void PassContext(IPWFService service, PWFContext context)
        {
            service.SetContext(context);
        }

        public virtual DbTransaction BeginTransaction()
        {
            _dbConnection.Open();
            return _dbConnection.BeginTransaction();
            // return Connection.GetWriteConnection().BeginTransaction();
        }
        public delegate TReturn TRansactWithWithWSISDelegate<IDbContextTransaction, TReturn>(IDbContextTransaction t, int AID);
        public delegate void TRansactWithWithWSISDelegate<IDbContextTransaction>(IDbContextTransaction t, int AID);

        public virtual TReturn TransactWIthWSIS<TReturn>(TRansactWithWithWSISDelegate<IDbContextTransaction, TReturn> func, INTAPS.RDBMS.SQLHelper wsisDB,String actionName,UserSession session )
        {
            lock(wsisDB)
                using (var transaction = _context.Database.BeginTransaction())
                {
                    int AID = INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriteAddAudit(session.Username, session.wsisUserID, $"{actionName} attempt", "", -1);
                    wsisDB.BeginTransaction();
                    try
                    {
                        var ret = func.Invoke(transaction,AID);
                        transaction.Commit();
                        INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriteAddAudit(session.Username, session.wsisUserID, $"{actionName} Success", "", AID);
                        wsisDB.CommitTransaction();
                        return ret;
                    }
                    catch(Exception ex)
                    {
                        wsisDB.RollBackTransaction();
                        INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriteAudit(session.Username, session.wsisUserID, $"{actionName} Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        wsisDB.CheckTransactionIntegrity();
                    }
                }
        }
        public virtual void TransactWIthWSIS(TRansactWithWithWSISDelegate<IDbContextTransaction> func, INTAPS.RDBMS.SQLHelper wsisDB, String actionName, UserSession session)
        {
            lock (wsisDB)
                using (var transaction = _context.Database.BeginTransaction())
                {
                    int AID = INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriteAddAudit(session.Username, session.wsisUserID, $"{actionName} attempt", "", -1);
                    wsisDB.BeginTransaction();
                    try
                    {
                        func.Invoke(transaction, AID);
                        transaction.Commit();
                        INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriteAddAudit(session.Username, session.wsisUserID, $"{actionName} Success", "", AID);
                        wsisDB.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        wsisDB.RollBackTransaction();
                        INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriteAudit(session.Username, session.wsisUserID, $"{actionName} Failure", "", ex.Message + "\n" + ex.StackTrace, AID);
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        wsisDB.CheckTransactionIntegrity();
                    }
                }
        }
        public virtual TReturn Transact<TReturn>(Func<IDbContextTransaction, TReturn> func)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var ret = func.Invoke(transaction);
                    transaction.Commit();
                    return ret;
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }

        }

        public virtual void Transact(Action<IDbContextTransaction> func)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    func.Invoke(transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }

        }
    }
}
