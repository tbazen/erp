﻿using PWF.data;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Infrastructure.Architecture
{
    public interface IPWFService
    {
        PWFContext GetContext();
        void SetContext(PWFContext value);
    }

    public class PWFService : IPWFService
    {
        protected PWFContext Context;

        public virtual PWFContext GetContext()
        {
            return Context;
        }

        public virtual void SetContext(PWFContext value)
        {
            Context = value;
        }
    }
}
