﻿using PWF.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Infrastructure
{
    public interface ILookupService
    {
        object GetRoles();
    }

    public class LookupService : ILookupService
    {
        private readonly PWFContext _PWFContext;

        public LookupService(PWFContext context)
        {
            _PWFContext = context;
        }

        public object GetRoles()
        {
            return _PWFContext.Role.Select(role => new { id = role.Id, name = role.Name }).ToList();
        }
    }
}
