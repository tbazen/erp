﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Admin;

namespace PWF.domain.Infrastructure
{
    public interface IUserActionService
    {
        UserAction AddUserAction(UserSession session, UserActionType actionType);
        UserAction GetUserAction(long aid);
    }

    public class UserActionService : IUserActionService
    {
        private readonly PWFContext _PWFContext;

        public UserActionService(PWFContext PWFContext)
        {
            _PWFContext = PWFContext;
        }

        public UserAction AddUserAction(UserSession session, UserActionType type)
        {


            var actionType = GetActionType((int)type);

            //var user = _PWFContext.User.First(u => u.Username.Equals(session.Username));

            var action = new UserAction
            {
                Timestamp = DateTime.Now.Ticks,
                Actiontype = actionType,
                Username = session.Username,
            };
            _PWFContext.UserAction.Add(action);
            _PWFContext.SaveChanges();

            return action;
        }

        private ActionType GetActionType(int id)
        {
            return _PWFContext.ActionType.First(at => at.Id == id);
        }

        public  UserAction GetUserAction(long aid)
        {
            return _PWFContext.UserAction.Where(m => m.Id == aid).FirstOrDefault();
        }
    }
}
