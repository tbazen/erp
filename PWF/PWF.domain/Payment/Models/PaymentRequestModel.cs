﻿using BIZNET.iERP;
using INTAPS.Accounting;
using PWF.data.Entities;
using PWF.domain.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Payment.Models
{

    public enum RequestType
    {
        General,
        MissionAdvance,
        PurchasePayment
    }


    public enum PaymentType
    {
        BankWithdrawal,
        LabourPayment,
        PayrollPayment,
        CashierToCashier,
        CustomerAdvanceReturn,
        BondPayment,
        PurchaseAdvance,
        PurchaseCreditSettlement,
        PurchasePayment,
        SupplierReceivable
    }

    public class PaidTo
    {
        public int Type { get; set; }
        public string name { get; set; }
        public int id { get; set; }
        public string code { get; set; }
    }

    public class PaymentRequestModel
    {

        public PaymentRequest Request { get; set; }


        public DocumentTypedReference Reference { get; set; }
        public DocumentTypedReference RequestNo { get; set; }

        public List<AccountDocument> ImportedDocuments { get; set; }
        public List<AccountDocument> NewDocuments { get; set; } 
        public List<PaymentSourceModel> Sources { get; set; }

        public BudgetTransactionDocument BudgetDocument { get; set; }
        public List<Document> AttachedDocs { get; set; }

        public Document SignedDocument { get; set; }


    }

    public class PaymentSourceModel
    {
        public int AssetAccountID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public double Amount { get; set; }
    }


    public class PaymentRequest
    {
        public RequestType type { get; set; }
        public List<ItemRequestModel> Items { get; set; }
        public System.Nullable<DateTime> DateFrom { get; set; }
        public System.Nullable<DateTime> DateTo { get; set; }
        public double AmountPerDay { get; set; } = 0;
        public int DocID { get; set; }
        public Document Files { get; set; }
        public PaidTo PaidTo { get; set; }
        public string Note { get; set; }
        public UserRoles SubmittedBy { get; set; }
        public DateTime RequestDate { get; set; }


    }

    public class ItemRequestModel
    {
        public string Description { get; set; }
        public string UnitID { get; set; }
        public double? UnitPrice { get; set; }
        public double? Quantity { get; set; }
        public double? Amount { get; set; }
        public string Code { get; set; }
        public GoodOrService type { get; set; }

    }

    public class ItemDeliveryRequestModel : ItemRequestModel
    {
        public double Delivered { get; set; }
    }


    public class PaymentDocumentModel
    {
        public PaymentType Type { get; set; }
        public AccountDocument Document { get; set; }
    }

    public class DocumentSearchModel
    {
        public Boolean DateFromChecked {get; set;}
        public Boolean DateToChecked {get; set;}

        public bool Date { get; set; }


        public int Type { get; set; }

        public string Query {get; set;}

        public DateTime DateFrom {get; set;}

        public DateTime DateTo {get; set;}
    }
}
