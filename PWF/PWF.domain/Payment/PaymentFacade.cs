﻿using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using Microsoft.AspNetCore.Http;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Payment.Models;
using PWF.domain.Payment.StateMachines;
using PWF.domain.Store;
using PWF.domain.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PWF.domain.Payment
{
    public interface IPaymentFacade : IPWFFacade
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        Guid CreatePaymentRequest(PaymentRequest Reqeust, string description);
        void ResubmitPaymentRequest(Guid wfid, PaymentRequest Request, string description);
        void RejectByFinHead(Guid wfid, string description);
        void RejectByOwner(Guid wfid, string description);
        void CheckPayment(Guid wfid,PaymentRequestModel model, string description);
        //void CheckAndApprovePayment(Guid wfid, string description);
        void ApprovePayment(Guid wfid,PaymentRequestModel Model, string description);
        void ExecutePayment(Guid wfid, string description);
        void CancelPaymentRequest(Guid wfid, string description);
        void CloseRequest(Guid wfid, string description, Document SignedDocument);
        void FinalizeRequest(Guid wfid, string description);
        void RejectFinalCheck(Guid wfid, string description);
        PaymentRequestModel AddPaymentDocument(Guid wfid, AccountDocument Document, bool newOoc, string description = "");
        APIResponseModel PostBankWithdrawalDocument(BankWithdrawalDocument Doc,double allowedAmount, int[] sourceIDs);
        APIResponseModel PostLaborPaymentDocument(LaborPaymentDocument Doc, double allowedAmount, int[] sourceIDs);
        APIResponseModel PostCashierToCashierDocument(CashierToCashierDocument Doc, double allowedAmount, int[] sourceIDs);
        APIResponseModel GetAccountDocumentHTML(int docID);
        APIResponseModel GetDocumentTrasactionAmount(int docID);
        APIResponseModel GetAllDocumentTypes();
        APIResponseModel SearchDocuments(DocumentSearchModel Model);
        APIResponseModel GetAccountDocument(int docID);
        APIResponseModel RemovePaymentDocument(Guid wfid, int DocID, string description = "");
        APIResponseModel GetCashPaymentVoucher();
        APIResponseModel GetCheckPaymentVoucher();
        APIResponseModel GetTransactionAmount(int docID, int[] AccuntIDs);
        APIResponseModel DeleteGenericDocument(int docID);
        List<Document> AddAttachmentDocuments(Guid wfid, Document Doc);
        List<Document> RemoveAttachedDocuments(Guid wfid, int docID);
        Guid CreatePaymentRequest(PaymentRequest Reqeust, string description, Guid parentWFID, string observer);
        IBaseStateMachine TestMethod(Guid id);
        double GetTotalPaidAmount(Guid wfid);
        double GetTotalPaidAmountFromSource(Guid wfid);
        void AbortPaymentWorkflow(Guid wfid);

    }


    public class PaymentFacade : PWFFacade, IPaymentFacade
    {
        private readonly IPaymentService _service;
        private PWFConfiguration _Config;
        private UserSession _session;
        private iERPTransactionBDE _IERPService;
        private AccountingBDE _AccountingService;
        private IDocumentService _documentService;

        public PaymentFacade(PWFContext Context, IPaymentService service, IDocumentService docService): base(Context)
        {
            _service = service;
            _context = Context;
            _service.SetContext(Context);
            _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            _documentService = docService;

        }

        public void SetSession(UserSession session)
        {
            INTAPS.ClientServer.ApplicationServer.RenewSession(session.payrollSessionID);
            _session = session;
            _service.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _service.SetSessionConfiguration(Config, session);
            _documentService.SetSessionConfiguration(Config, session);
            _session = session;
            _Config = Config;
        }

        public IBaseStateMachine TestMethod(Guid id)
        {
            var name = typeof(PaymentWorkflow).ToString();
            var type = Type.GetType($"PWF.domain.Payment.StateMachines.{name}, PWF.domain");

            System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(name);
            IBaseStateMachine wf = (IBaseStateMachine)Activator.CreateInstance(type);
            wf.SetSession(_session);
            PassContext(wf, _context);
            wf.ConfigureMachine(id);
            return wf;
        }

        public Guid CreatePaymentRequest(PaymentRequest Reqeust, string description)
        {
            return Transact(t =>
            {
                PaymentRequestModel Model = new PaymentRequestModel()
                {
                    Request = Reqeust
                };


                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(description, _session.EmployeeID,null,null);
                var wfid = workflow.Workflow.Id;
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Request, Model, description);
                return wfid;
            });
        }

        public void ResubmitPaymentRequest(Guid wfid, PaymentRequest Request, string description)
        {
            Transact(t =>
            {
                PaymentRequestModel Model = new PaymentRequestModel()
                {
                    Request = Request
                };
                var workflow = new PaymentWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Request, Model, description);
                return wfid;
            });
        }

        public Guid CreatePaymentRequest(PaymentRequest Reqeust, string description,Guid parentWFID,string observer)
        {
            PaymentRequestModel Model = new PaymentRequestModel()
            {
                Request = Reqeust
            };


            var workflow = new PaymentWorkflow(); workflow.SetSession(_session); ;
            PassContext(workflow, _context);
            workflow.ConfigureMachine(description, _session.EmployeeID, parentWFID, observer);
            var wfid = workflow.Workflow.Id;
            workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Request, Model, description);
            return wfid;
        }

        public double GetTotalPaidAmount(Guid wfid)
        {

            var workflow = new PaymentWorkflow(); workflow.SetSession(_session); ;
            PassContext(workflow, _context);
            workflow.ConfigureMachine(wfid);
            var data = workflow.GetData();
            List<AccountDocument> accDocs = new List<AccountDocument>();
            if (data.ImportedDocuments != null && data.ImportedDocuments.Count > 0)
                accDocs.AddRange(data.ImportedDocuments);
            if (data.NewDocuments != null && data.NewDocuments.Count > 0)
                accDocs.AddRange(data.NewDocuments);
            if(accDocs.Count == 0)
                return 0.0;
            else
            {
                var accIDs = accDocs.Select(m => m.AccountDocumentID).ToList();
                var amount = Helper.GetNetTransactionAmount(accIDs);
                return amount;
            }
        }

        public double GetTotalPaidAmountFromSource(Guid wfid)
        {
            var workflow = new PaymentWorkflow(); workflow.SetSession(_session); ;
            PassContext(workflow, _context);
            workflow.ConfigureMachine(wfid);
            var data = workflow.GetData();
            List<AccountDocument> accDocs = new List<AccountDocument>();
            if (data.ImportedDocuments != null && data.ImportedDocuments.Count > 0)
                accDocs.AddRange(data.ImportedDocuments);
            if (data.NewDocuments != null && data.NewDocuments.Count > 0)
                accDocs.AddRange(data.NewDocuments);
            if (accDocs.Count == 0)
                return 0.0;
            else
            {
                var accIDs = accDocs.Select(m => m.AccountDocumentID).ToList();
                var sources = data.Sources.Select(m => m.AssetAccountID).ToArray();
                var amount = Helper.GetTransactionAmountFromPaymentSource(accIDs, sources);
                return amount;
            }
        }

        public void RejectByFinHead(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Reject1, description);
            });
        }

        public void AbortPaymentWorkflow(Guid wfid)
        {
            var workflow = new PaymentWorkflow();
            workflow.SetSession(_session);
            PassContext(workflow, _context);
            workflow.ConfigureMachine(wfid);
            workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Abort, "Aborted");
        }

        public void RejectByOwner(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Reject2, description);
            });
        }


        //public void CheckAndApprovePayment(Guid wfid, string description)
        //{
        //    Transact(t =>
        //    {
        //        var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
        //        PassContext(workflow, _context);
        //        workflow.ConfigureMachine(wfid);
        //        workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.CheckApprove, description);
        //    });
        //}




        public void ApprovePayment(Guid wfid,PaymentRequestModel Model, string description)
        {
            TransactWIthWSIS( (t,AID) =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                DocumentTypedReference reference = new DocumentTypedReference();
                var types = Model.Sources.Select(m => m.Type).Distinct().ToArray();
                var response = GetVoucherReference(types,AID);
                if (response.status == false)
                {
                    throw new Exception(response.error);
                }
                else
                {
                    reference = (DocumentTypedReference)response.response;
                    data.Reference = reference;
                    //data.BudgetDocument.PaperRef = data.Reference.reference;
                    //var budgetDocID = Helper.PostGenericDocument(data.BudgetDocument, AID);
                    //var budgetDoc = (BudgetTransactionDocument)_AccountingService.GetAccountDocument(budgetDocID, true);
                    //_AccountingService.addDocumentSerials(AID, budgetDoc, new DocumentTypedReference[] { reference });
                    //data.BudgetDocument = budgetDoc;
                    workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Approve, data, description);

                }
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF Approve Payment and Generating voucher", _session);
        }

        public void ExecutePayment(Guid wfid,string description)
        {
            

            Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
               // List<PaymentDocumentModel> PaymentDocuments = _service.PostPaymentDocuments(Data.PostedDocumetns);
               // Data.PaymentDocuments = PaymentDocuments;
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Close,Data, description);

            });
        }

        public void CloseRequest(Guid wfid,string description,Document SignedDocument)
        {
            Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
                SignedDocument.Documenttypeid = (int)DocumentTypes.SignedPaymentDocument;
                var result = _documentService.AddDocument(SignedDocument);
                Data.SignedDocument = result;
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Close, Data, description);

            });
        }

        public void FinalizeRequest(Guid wfid, string description)
        {
            TransactWIthWSIS((t, AID) =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
                var budgetDoc = Data.BudgetDocument;
                var newDocsID = Data.NewDocuments != null && Data.NewDocuments.Count() > 0 ? Data.NewDocuments.Select(m => m.AccountDocumentID).ToList() : new List<int>();
                var importedDocsID = Data.ImportedDocuments != null && Data.ImportedDocuments.Count() > 0 ? Data.ImportedDocuments.Select(m => m.AccountDocumentID).ToList() : new List<int>();
                var accountDocIDs = new List<int>();
                accountDocIDs.AddRange(newDocsID);
                accountDocIDs.AddRange(importedDocsID);
                budgetDoc.referedDocs = accountDocIDs;
                budgetDoc.transferProperties = false;
                budgetDoc.ShortDescription = $"Budget Transaction Document for Payment Request on ${DateTime.Now}";
                budgetDoc.DocumentDate = DateTime.Now;
                var docID = Helper.PostGenericDocument(budgetDoc, AID);
                var accountDoc = _AccountingService.GetAccountDocument(docID, true);
                Data.BudgetDocument = (BudgetTransactionDocument)accountDoc;
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Finalize, Data, description);
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF Finalize Request & Post Budget Transaction Document", _session);
        }

        public void RejectFinalCheck(Guid wfid,string description)
        {
               Transact(t =>
               {
                   var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                   PassContext(workflow, _context);
                   workflow.ConfigureMachine(wfid);
                   workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.RejectFinalCheck,  description);
               });
        }

        public void CancelPaymentRequest(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Cancel, description);
            });
        }

        public PaymentRequestModel AddPaymentDocument(Guid wfid, AccountDocument Document, bool newDoc, string description  = "")
        {
            return Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
                if (!newDoc)
                {
                    var Docs = Data.ImportedDocuments != null ? Data.ImportedDocuments : new List<AccountDocument>();
                    Docs.Add(Document);
                    Data.ImportedDocuments = Docs;
                }
                else
                {
                    var Docs = Data.NewDocuments != null ? Data.NewDocuments : new List<AccountDocument>();
                    Docs.Add(Document);
                    Data.NewDocuments = Docs;
                }

                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.AddDocument, Data, String.IsNullOrWhiteSpace(description)? Document.ShortDescription : description);
                return Data;


            });
        }

        public List<Document> AddAttachmentDocuments(Guid wfid, Document Doc)
        {
            return Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
                var Docs = Data.AttachedDocs != null ? Data.AttachedDocs : new List<Document>();
                Doc.Documenttypeid = (int)DocumentTypes.PaymentAttachmentDocuments;
                var d = _documentService.AddDocument(Doc);
               // d.File = new byte[] { };
                Docs.Add(d);
                Data.AttachedDocs = Docs;
                workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.AttachDocument, Data, $"Attached document with description {Doc.Description}"); 
                return Docs;
            });

        }

        public List<Document> RemoveAttachedDocuments(Guid wfid, int docID)
        {
            return Transact(t =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
                var Docs = Data.AttachedDocs != null ? Data.AttachedDocs : new List<Document>();
                if(Docs.Count() == 0)
                {
                    return Docs;
                }
                else
                {
                    var doc = Docs.Where(m => m.ID == docID).First();
                    Docs.Remove(doc);
                    _documentService.RemoveDocument(docID);
                    Data.AttachedDocs = Docs;
                    workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.DetachDocument, Data, $"Detached document with ID {docID}");
                    return Docs;
                }

            });
        }

        public APIResponseModel RemovePaymentDocument(Guid wfid, int DocID, string description = "")
        {
      

                return TransactWIthWSIS((t, AID) =>
                {
                    var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                    PassContext(workflow, _context);
                    workflow.ConfigureMachine(wfid);
                    PaymentRequestModel Data = workflow.GetData();
                    var ImportedDocs = Data.ImportedDocuments;
                    var NewDocs = Data.NewDocuments;
                    if (Data.NewDocuments != null && Data.NewDocuments.Count > 0 && NewDocs.Select(m => m.AccountDocumentID).Distinct().Contains(DocID))
                    {
                        var Doc = NewDocs.Where(m => m.AccountDocumentID == DocID).First();
                        _service.DeleteGenericDocument(DocID, AID);
                        NewDocs.Remove(Doc);
                    }
                    if (Data.ImportedDocuments != null && Data.ImportedDocuments.Count > 0 && ImportedDocs != null && ImportedDocs.Select(m => m.AccountDocumentID).Distinct().Contains(DocID))
                    {
                        var Doc = ImportedDocs.Where(m => m.AccountDocumentID == DocID).First();
                        ImportedDocs.Remove(Doc);
                    }
                    Data.ImportedDocuments = ImportedDocs;
                    Data.NewDocuments = NewDocs;
                    workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.RemoveDocument, Data, $"Remove Accound Document with ID {DocID}");
                    return new APIResponseModel()
                    {
                        status = true,
                        response = Data
                    };
                }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF RemovePaymentDocument", _session);


       }

        public APIResponseModel PostBankWithdrawalDocument(BankWithdrawalDocument Doc, double allowedAmount, int[] sourceIDs)
        {

            return TransactWIthWSIS((t,AID) =>
            {
                PassContext(_service, _context);
                var result = _service.PostBankWithdrawalDocument(Doc,AID,allowedAmount,sourceIDs);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper,"PWFPostBankWithdrawalDocument",_session);
        } 

        public APIResponseModel PostLaborPaymentDocument(LaborPaymentDocument Doc, double allowedAmount, int[] sourceIDs)
        {
            return TransactWIthWSIS( (t,AID) =>
            {
                PassContext(_service, _context);
                var result = _service.PostLaborPaymentDocument(Doc,AID, allowedAmount, sourceIDs);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWFPostLabourPaymentDocument", _session);
        }

        public APIResponseModel PostCashierToCashierDocument(CashierToCashierDocument Doc, double allowedAmount,int[] sourceIDs)
        {
            return TransactWIthWSIS( (t,AID) =>
            {
                PassContext(_service, _context);
                var result = _service.PostCashierToCashierDocument(Doc,AID, allowedAmount, sourceIDs);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWFPostCashierToCashierDOcument", _session);
        }

        public APIResponseModel GetAccountDocumentHTML(int docID)
        {

                PassContext(_service, _context);
                var result = _service.GetAccountDocumentHTML(docID);
                return result;
        }

        public APIResponseModel GetDocumentTrasactionAmount(int docID)
        {
                PassContext(_service, _context);
                var result = _service.GetDocumentTrasactionAmount(docID);
                return result;
        }

        public APIResponseModel GetAllDocumentTypes()
        {
                PassContext(_service, _context);
                var result = _service.GetAllDocumentTypes();
                return result;
        }

        public APIResponseModel SearchDocuments(DocumentSearchModel Model)
        {
                PassContext(_service, _context);
                var result = _service.SearchDocuments(Model);
                return result;
        }

        public APIResponseModel GetAccountDocument(int docID)
        {
                PassContext(_service, _context);
                var result = _service.GetAccountDocument(docID);
                return result;
        }

        internal APIResponseModel GetVoucherReference(string[] types,int AID)
        {
            if (types.Length > 1)
            {
                var refer = _service.GetCheckPaymentVoucher(AID);
                if (refer.status == true)
                {
                    return refer;
                }
                else
                {
                    return new APIResponseModel()
                    {
                        status = false,
                        error = $"Generatng check payment voucher {refer.error}"
                    };
                }
            }
            else if (types.Length == 1)
            {
                if (types[0] == "bank")
                {
                    var refer = _service.GetCheckPaymentVoucher(AID);
                    if (refer.status == true)
                    {
                        return refer;
                    }
                    else
                    {
                        return new
                        APIResponseModel()
                        {
                            status = false,
                            error = $"Generatng check payment voucher {refer.error}"
                        };
                    }
                }
                else
                {
                    var refer = _service.GetCashPaymentVoucher(AID);
                    if (refer.status == true)
                    {
                        return refer;
                    }
                    else
                    {
                        return new APIResponseModel()
                        {
                            status = false,
                            error = $"Generatng cash payment voucher {refer.error}"
                        };
                    }
                }
            }
            else
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = "Payment Source is empty"
                };
            }
        }

        public void CheckPayment(Guid wfid, PaymentRequestModel model, string description)
        {
            TransactWIthWSIS( (t,AID) =>
            {
                var workflow = new PaymentWorkflow( ); workflow.SetSession(_session);;
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                var requestType = data.Request.type;
                if (_service.IsSingleStepWorkflow(data))
                {
                    DocumentTypedReference reference = new DocumentTypedReference();
                    var types = model.Sources.Select(m => m.Type).Distinct().ToArray();
                    var response = GetVoucherReference(types,AID);
                    if(response.status == false)
                    {
                        throw new Exception(response.error);
                    }
                    else
                    {
                        reference = (DocumentTypedReference)response.response;
                        data.Reference = reference;
                        model.Reference = reference;
                        workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.CheckApprove, model, description);

                    }
                }
                else
                {
                    workflow.Fire(wfid, PaymentWorkflow.ParametrizedTriggeres.Check, model, description);
                }

            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF Check Approve Payment", _session);
        }

        public APIResponseModel GetCashPaymentVoucher()
        {
            return TransactWIthWSIS( (t,Aid) =>
            {
                PassContext(_service, _context);
                var result = _service.GetCashPaymentVoucher(Aid);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWFGetCashPaymentVoucher", _session);
        }

        public APIResponseModel GetCheckPaymentVoucher()
        {
            return TransactWIthWSIS((t, Aid) =>
            {
                PassContext(_service, _context);
                var result = _service.GetCheckPaymentVoucher(Aid);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWFGetCheckPaymentVoucher", _session);
        }

        public APIResponseModel GetTransactionAmount(int docID, int[] AccountIDs)
        {
                PassContext(_service, _context);
                var result = _service.GetTransactionAmount(docID, AccountIDs);
                return result;
        }

        public APIResponseModel DeleteGenericDocument(int docID)
        {
            return TransactWIthWSIS((t, AID) =>
           {
               PassContext(_service, _context);
               var result = _service.DeleteGenericDocument(docID, AID);
               return result;
           }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWFDeleteGenericDocument", _session);
            
        }
    }
}
