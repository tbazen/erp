﻿using PWF.data;

using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Payment.Models;
using System;
using System.Collections.Generic;
using System.Text;
using PWF.domain;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using NinjaNye.SearchExtensions;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using System.Linq;
using Microsoft.AspNetCore.Http;
using PWF.data.Entities;
using System.IO;
using PWF.domain.Documents;
using PWF.domain.Store;
using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting.BDE;
using INTAPS.Accounting;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;

namespace PWF.domain.Payment
{

    public interface IPaymentService : IPWFService
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        bool IsSingleStepWorkflow(PaymentRequestModel type);
        List<PaymentDocumentModel> PostPaymentDocuments(List<PaymentDocumentModel> PreparedDocuments);
        PaymentDocumentModel GenerateAccountDocument(Guid wfid, string Document);
        Array GetPaymentTypes();
        BankAccountInfo[] GetAllBankAccounts();
        CashAccount[] GetAllCashAccounts();
        TaxCenter[] GetAllTaxCenters();
        PaymentInstrumentType[] GetPaymentInstrumentTypes();
        APIResponseModel PostBankWithdrawalDocument(BankWithdrawalDocument Doc,int AID,double allowedAmount,int[] sourceIDs);
        APIResponseModel PostLaborPaymentDocument(LaborPaymentDocument Doc,int AID, double allowedAmount,int[] sourceIDs);
        APIResponseModel PostCashierToCashierDocument(CashierToCashierDocument Doc,int AID, double allowedAmount,int[] sourceIDs);
        APIResponseModel GetAccountDocumentHTML(int docID);
        APIResponseModel GetDocumentTrasactionAmount(int docID);
        APIResponseModel GetAllDocumentTypes();
        APIResponseModel SearchDocuments(DocumentSearchModel Model);
        APIResponseModel GetAccountDocument(int docID);
        APIResponseModel GetCashPaymentVoucher(int AID);
        APIResponseModel GetCheckPaymentVoucher(int AID);
        APIResponseModel GetTransactionAmount(int docID, int[] AccuntIDs);
        APIResponseModel DeleteGenericDocument(int docID,int AID);
        APIResponseModel GetAllEmployees();
        APIResponseModel GetAllCustomers();
        APIResponseModel GetSerialType(int typeID);
        string GetAccountName(int accountID);
        string GetCostCenterName(int costCenterID);
        APIResponseModel SearchAccountByCode(string code);
        APIResponseModel SearchCostCenter(string code);
        double GetTransactionAmount(List<int> docIDs, int[] AccountIDs);

    }




    public class PaymentService : PWFService, IPaymentService
    {
        private PWFContext _context;
        private UserSession _session;
        private string _url;
        private string _sessionID;
        private IMemoryCache _cache;
        private PWFConfiguration _config;

        private IWorkflowService _wfService;
        private IUserActionService _userActionService;
        private IDocumentService _documentService;
        private iERPTransactionBDE _IERPService;
        private AccountingBDE _AccountingService;
        public PaymentService(PWFContext Context, IMemoryCache memoryCache, IWorkflowService wfService, IUserActionService actionSercice, IDocumentService docService)
        {
            _context = Context;
            _cache = memoryCache;
            _wfService = wfService;
            _wfService.SetContext(Context);
            _userActionService = actionSercice;
            _documentService = docService;
            _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();


        }

        public PaymentDocumentModel GenerateAccountDocument(Guid wfid, string Document)
        {
            throw new NotImplementedException();
        }

        public bool IsSingleStepWorkflow(PaymentRequestModel Model)
        {
            var totalAmount = Model.Request.Items.Sum(m => m.Amount);
            if (totalAmount > _config.TwoStepApprovalMinimumAmount)
            {
                return false;
            }
            else
                return true;
        }

        public List<PaymentDocumentModel> PostPaymentDocuments(List<PaymentDocumentModel> PreparedDocuments)
        {
            throw new NotImplementedException();
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _sessionID = session.payrollSessionID;
            _wfService.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _sessionID = session.payrollSessionID;
            _session = session;
            _url = Config.Host;
            _config = Config;
            _documentService.SetSessionConfiguration(Config, session);
        }

        public Array GetPaymentTypes()
        {
            var result = Enum.GetValues(typeof(PaymentType));
            return result;
        }

        public BankAccountInfo[] GetAllBankAccounts()
        {
            var result = _IERPService.GetAllBankAccounts();
            return result;

        }

        public CashAccount[] GetAllCashAccounts()
        {

            var cashAccounts = _IERPService.GetAllCashAccounts(true);
            return cashAccounts;
        }

        public TaxCenter[] GetAllTaxCenters()
        {

            var centers = _IERPService.GetTaxCenters();
            return centers;
        }



        public PaymentInstrumentType[] GetPaymentInstrumentTypes()
        {

            var types = _IERPService.getAllPaymentInstrumentTypes();
            return types;
        }

        public APIResponseModel PostBankWithdrawalDocument(BankWithdrawalDocument Doc,int AID, double allowedAmount, int[] sourceIDs)
        {

            try
            {
                Doc.DocumentDate = DateTime.Now;
                Doc.PaperRef = Doc.voucher.reference;
                int docID = Helper.PostGenericDocument(Doc,AID,allowedAmount,sourceIDs);
                Doc.AccountDocumentID = docID;

                return new APIResponseModel()
                {
                    status = true,
                    response = Doc,
                };
            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public APIResponseModel PostLaborPaymentDocument(LaborPaymentDocument Doc,int AID, double allowedAmount, int[] sourceIDs)
        {


            try
            {

                Doc.DocumentDate = DateTime.Now;
                Doc.PaperRef = Doc.voucher.reference;
                int docID = Helper.PostGenericDocument(Doc,AID,allowedAmount,sourceIDs);
                Doc.AccountDocumentID = docID;

                return new APIResponseModel()
                {
                    status = true,
                    response = Doc,
                };
            }
            catch (Exception ex)
            {

                return new APIResponseModel
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public APIResponseModel PostCashierToCashierDocument(CashierToCashierDocument Doc,int AID, double  allowedAmount,int[] sourceIDs)
        {

            try
            {
                Doc.DocumentDate = DateTime.Now;
                Doc.PaperRef = Doc.voucher.reference;
                int docID = Helper.PostGenericDocument(Doc,AID,allowedAmount,sourceIDs);
                Doc.AccountDocumentID = docID;

                return new APIResponseModel()
                {
                    status = true,
                    response = Doc,
                };
            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public APIResponseModel GetAccountDocumentHTML(int docID)
        {


            try
            {
                var response = _AccountingService.getDocumentEntriesHTML(docID,_IERPService.SysPars.summerizeItemTransactions);
                return new APIResponseModel()
                {
                    status = true,
                    response = response
                };


            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
            }
        }

        public  APIResponseModel GetDocumentTrasactionAmount(int docID)
        {

            try
            {
                var trans = Helper.GetTransactionOfBatch(docID);
                var cashAccounts =_IERPService.GetAllCashAccounts(true);
                var bankAccounts = _IERPService.GetAllBankAccounts();
                var accounts = new List<int>(cashAccounts.Select(x => x.csAccountID));
                accounts.AddRange(bankAccounts.Select(x => x.bankServiceChargeCsAccountID));

                var total = 0.0;
                foreach (var t in trans)
                {
                    if (accounts.Where(x => x == t.AccountID).Any())
                        total += t.Amount;
                    //}
                }

                return new APIResponseModel()
                {
                    status = true,
                    response = total
                };
            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
            }
        }

        public APIResponseModel GetAllDocumentTypes()
        {


            try
            {
                var result = _AccountingService.GetAllDocumentTypes();
                return new APIResponseModel()
                {
                    status = true,
                    response = result,
                };

            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
            }
        }

        public APIResponseModel GetAccountDocument(int docID)
        {


            try
            {
                var doc = _AccountingService.GetAccountDocument(docID, true);
                return new APIResponseModel()
                {
                    status = true,
                    response = doc
                };

            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
            }
        }


        public APIResponseModel GetCashPaymentVoucher(int AID)
        {


            try
            {
                int serial;
                var batchID = _config.BatchIDs.CPV;
                var reference = Helper.GetNextSerialType("CPV", batchID, out serial);
               Helper.UseSerial(new UsedSerial()
                {
                    batchID = batchID,
                    date = DateTime.Now,
                    isVoid = false,
                    val = serial,
                    note = "Cash Payment Voucher"
                },AID);
                return new APIResponseModel()
                {
                    status = true,
                    response = reference,
                };
            }
            catch (Exception e)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = e.Message
                };
            }
        }

        public APIResponseModel GetCheckPaymentVoucher(int AID)
        {
 
            try
            {
                int serial;
                var batchID = _config.BatchIDs.CHKPV;
                var reference = Helper.GetNextSerialType("CHKPV", batchID, out serial);
                Helper.UseSerial(new UsedSerial()
                {
                    batchID = batchID,
                    date = DateTime.Now,
                    isVoid = false,
                    val = serial,
                    note = "Check Payment Voucher"
                },AID);
                return new APIResponseModel()
                {
                    status = true,
                    response = reference,
                };
            }
            catch (Exception e)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = e.Message
                };
            }
        }


        public APIResponseModel GetTransactionAmount(int docID, int[] AccountIDs)
        {

            try
            {
                var trans = Helper.GetTransactionOfBatch(docID);
                var total = 0.0;
                foreach (var t in trans)
                {
                    if (AccountIDs.Where(x => x == t.AccountID).Any())
                        total += t.Amount;
                    //}
                }
                return new APIResponseModel()
                {
                    status = true,
                    response = total
                };
            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
            }
        }

        public double GetTransactionAmount(List<int> docIDs, int[] AccountIDs)
        {
            List<double> Amount = new List<double>();
            foreach (var docID in docIDs)
            {
                var trans = Helper.GetTransactionOfBatch(docID);
                var total = 0.0;
                foreach (var t in trans)
                {
                    if (AccountIDs.Where(x => x == t.AccountID).Any())
                        total += t.Amount;
                    //}
                }
                Amount.Add(total);
            }
            return Amount.Sum();
        }

        public APIResponseModel DeleteGenericDocument(int docID,int AID)
        {

            try
            {
                Helper.DeleteGenericDocument(docID,AID);
                return new APIResponseModel()
                {
                    status = true
                };

            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public APIResponseModel GetAllEmployees()
        {
            try
            {
                PayrollBDE payroll = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
                var employees = payroll.GetEmployees(-1, true, DateTime.Now.Ticks);
                return new APIResponseModel()
                {
                    status = true,
                    response = employees
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
         
        }

        public APIResponseModel GetAllCustomers()
        {
            try
            {
                var customers  = _IERPService.GetAllSuppliers();
                return new APIResponseModel()
                {
                    status = true,
                    response = customers
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public APIResponseModel GetSerialType(int typeID)
        {
            try
            {
                var serialType = _AccountingService.getDocumentSerialType(typeID);
                return new APIResponseModel()
                {
                    status = true,
                    response = serialType
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public APIResponseModel SearchAccountByCode(string code)
        {
            try
            {
                int nRecords;
                var result = _AccountingService.SearchAccount<Account>(code, 0, 10, out nRecords);
                return new APIResponseModel()
                {
                    status = true,
                    response = result
                };
            }

            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public string GetAccountName(int accountID)
        {
            var account = _AccountingService.GetAccount<Account>(accountID);
            return account.NameCode;
        }

        public string GetCostCenterName(int costCenterID)
        {
            var costCenter = _AccountingService.GetAccount<CostCenter>(costCenterID);
            return costCenter.NameCode;
        }


        public APIResponseModel SearchDocuments(DocumentSearchModel Model)
        {
            var type = Model.Type;
            var query = Model.Query;
            bool date = Model.DateFromChecked;
            DateTime fromDate = Model.DateFromChecked ? Model.DateFrom : DateTime.Now;
            DateTime toDate;
            if(Model.DateFromChecked && Model.DateToChecked)
            {
                toDate = Model.DateTo.AddDays(1);
            }
            if(Model.DateFromChecked && !Model.DateToChecked)
            {
                toDate = Model.DateFrom.AddDays(1);
            }
            if(!Model.DateFromChecked && !Model.DateToChecked)
            {
                toDate = DateTime.Now.AddDays(1); 
            }
            else
            {
                toDate = DateTime.Now.AddDays(1);
            }


            DocumentSearchModel model = new DocumentSearchModel()
            {
                DateFrom = fromDate,
                DateTo = toDate,
                Type = type,
                Query = query,
                Date = date
            };
            try
            {
                int record;
                var docs = _AccountingService.GetAccountDocuments(model.Query, model.Type, model.Date, model.DateFrom, model.DateTo, 0, -1, out record);
                return new APIResponseModel()
                {
                    status = true,
                    response = docs
                };

            }
            catch (Exception ex)
            {

                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
            }

        }

        public APIResponseModel SearchCostCenter(string code)
        {
            try
            {
                int nRecords;
                var result = _AccountingService.SearchAccount<CostCenter>(code, 0, 10, out nRecords);
                return new APIResponseModel()
                {
                    status = true,
                    response = result
                };
            }

            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

       
    }
}
