﻿using PWF.domain.Workflows;
using System;
using System.Collections.Generic;
using System.Text;
using Stateless;
using PWF.domain.Infrastructure;
using PWF.data.Entities;
using PWF.domain.Infrastructure.Architecture;
using PWF.data;
using PWF.domain.Payment.Models;
using System.Linq;
using Newtonsoft.Json;
using PWF.domain.Workflows.Models;
using PWF.domain.Admin;

namespace PWF.domain.Payment.StateMachines
{
    public class PaymentWorkflow : PWFService , IBaseStateMachine
    {
        public WorkflowTypes type = WorkflowTypes.PaymentWorkflow;

        public enum States
        {
            Filing = 0,
            Checking = 1,
            Reviewing = 2,
            Payment = 3,
            FinalCheck = 4, 
            Closed = -1,
            Cancelled = -2,
            Aborted = -3,
        }

        public enum Triggers
        {
            Request = 1, 
            AddDocument = 2,
            Reject1 = 3,
            Reject2 = 4, 
            Check = 5,
            CheckApprove = 6,
            Approve = 7,
            Close = 8,
            Cancel = 9,
            Finalize = 10,
            RejectFinalCheck = 11,
            RemoveDocument = 12,
            AttachDocument = 13,
            DetachDocument = 14,
            Abort = 15,
            
        }

        private readonly IWorkflowService _workflowService;
        private IPaymentService _service;
        private UserSession _session;

        private StateMachine<States, Triggers> _machine;

        public PaymentWorkflow()
        {
            _workflowService = new WorkflowService();
          //  _service = service;

        }

        public Workflow Workflow { get; set; }

        public override void SetContext(PWFContext value)
        {
            Context = value;
          //  _service.SetContext(value);
            _workflowService.SetContext(Context);
            base.SetContext(value);
        }

        public void ConfigureMachine(string description,int? EmployeeID, Guid? Guid, string observer = "")
        {
            Workflow = _workflowService.CreateWorkflow(new Workflows.Models.WorkflowRequest
            {
                CurrentState = (int)States.Filing,
                Description = description,
                TypeId = (int)type,
                initiatorUser = _session.Username,
                EmployeeID = EmployeeID,
                Observer = observer,
                parentWFID = Guid
            });
            _machine = new StateMachine<States, Triggers>(States.Filing);
            DefineStateMachines();
        }

        public void ConfigureMachine(Guid workflowId)
        {
            Workflow = Context.Workflow.First(wf => wf.Id == workflowId && wf.Typeid == (int)type);
            _machine = new StateMachine<States, Triggers>((States)Workflow.Currentstate);
            DefineStateMachines();
        }


        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
            _session = session;
        }



        public long getInitialUser()
        {
            var user = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(Workflow.Initiatoruser);
            return (long)user;
        }

        public long? determineUser()
        {
            var data = GetData();
            if (data.Request.SubmittedBy == UserRoles.Employee || data.Request.SubmittedBy == UserRoles.FinanceOfficer)
            {
                return getInitialUser();
            }
            else
            {
                return null;
            }
        }

        public void DefineStateMachines()
        {
            ParametrizedTriggeres.ConfigureParameters(_machine);

            _machine.Configure(States.Cancelled)
                .OnEntryFrom(ParametrizedTriggeres.Cancel, onCancel);

            _machine.Configure(States.Filing)
                .Permit(Triggers.Request, States.Checking)
                .Permit(Triggers.Cancel, States.Cancelled)
                .Permit(Triggers.Abort, States.Aborted)
                .OnEntryFrom(ParametrizedTriggeres.Reject1, onReject1);

            _machine.Configure(States.Checking)
                .Permit(Triggers.Check, States.Reviewing)
                .Permit(Triggers.CheckApprove, States.Payment)
                .Permit(Triggers.Reject1, States.Filing)
                
                
                .OnEntryFrom(ParametrizedTriggeres.Reject2, onReject2)
                .OnEntryFrom(ParametrizedTriggeres.Request, onRequest);

            _machine.Configure(States.Reviewing)
                .Permit(Triggers.Approve, States.Payment)
                .Permit(Triggers.Reject2, States.Checking)
                .OnEntryFrom(ParametrizedTriggeres.Check, onCheck);

            _machine.Configure(States.Payment)
                .PermitReentry(Triggers.AddDocument)
                .PermitReentry(Triggers.RemoveDocument)
                .PermitReentry(Triggers.AttachDocument)
                .PermitReentry(Triggers.DetachDocument)
                .Permit(Triggers.Close, States.FinalCheck)
                .OnEntryFrom(ParametrizedTriggeres.AddDocument, onAddDocument)
                .OnEntryFrom(ParametrizedTriggeres.RemoveDocument,onRemoveDocument)
                .OnEntryFrom(ParametrizedTriggeres.AttachDocument,OnAttachDocument)
                .OnEntryFrom(ParametrizedTriggeres.DetachDocument,OnDetachDocument)
                .OnEntryFrom(ParametrizedTriggeres.Approve, onApprove)
                .OnEntryFrom(ParametrizedTriggeres.CheckApprove, onCheckApprove)
                .OnEntryFrom(ParametrizedTriggeres.RejectFinalCheck, onRejectFinalCheck);

            _machine.Configure(States.FinalCheck)
                .Permit(Triggers.Finalize, States.Closed)
                .Permit(Triggers.RejectFinalCheck, States.Payment)
                .OnEntryFrom(ParametrizedTriggeres.Close, onClose);

            _machine.Configure(States.Closed)
                .OnEntryFrom(ParametrizedTriggeres.Finalize, onFinalize);

            _machine.Configure(States.Aborted)
                .OnEntryFrom(ParametrizedTriggeres.Abort, onAbort);

        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<string> trigger, string description)
        {
            
            _machine.Fire(trigger, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> trigger,
            PaymentRequestModel data, string description)
        {
            _machine.Fire(trigger, data, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);

        }


        public void onRequest(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            long? assignedUser;
            if (Model.Request.SubmittedBy == UserRoles.Employee || Model.Request.SubmittedBy == UserRoles.FinanceOfficer)
            {
                assignedUser =  getInitialUser();
            }
            else
            {
                assignedUser = null;
            }
            ConfigureAndAddWorkItem((long)UserRoles.FinanceManager, Model, description, assignedUser, transition);
        }

        public void onAddDocument(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, Model, description, determineUser(), transition);
        }

        public void onRemoveDocument(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, Model, description, determineUser(), transition);
        }

        public void OnAttachDocument(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, Model, description, determineUser(), transition);
        }

        public void OnDetachDocument(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, Model, description, determineUser(), transition);
        }


        public void onClose(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Finalizer, Model, description, determineUser(), transition);
        }

        public void onFinalize(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, Model, description, determineUser(), transition);
        }

        public void onRejectFinalCheck(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, GetData(), description, determineUser(), transition);
        }

        public void onCheck(PaymentRequestModel model,string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Owner, model, description, determineUser(), transition);
        }
        public void onCheckApprove(PaymentRequestModel model,string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, model, description, determineUser(), transition);
        }

        public void onReject1(string description, StateMachine<States, Triggers>.Transition transition)
        {
            var data = GetData();
            if(data.Request.SubmittedBy == UserRoles.Employee)
            {
                ConfigureAndAddWorkItem(null, GetData(), description, getInitialUser(), transition);
            }
            else if(data.Request.SubmittedBy == UserRoles.FinanceOfficer)
            {
                ConfigureAndAddWorkItem((long)UserRoles.FinanceOfficer, GetData(), description, null, transition);
            }
            else if(data.Request.SubmittedBy == UserRoles.Purchaser2)
            {
                ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, GetData(), description, null, transition);

            }

        }
        public void onReject2(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.FinanceManager, GetData(), description, determineUser(), transition);
        }

        public void onApprove(PaymentRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Cashier, Model, description, determineUser(), transition);
        }
        public void onCancel(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public void onAbort(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public PaymentRequestModel GetData()
        {
            var workItem = Context.WorkItem.Where(w => w.WorkflowId == Workflow.Id).OrderBy(wi => wi.Seqno).
                LastOrDefault();
            return workItem != null ? JsonConvert.DeserializeObject<PaymentRequestModel>(workItem.Data, new JsonSerializerSettings()
            {
                ContractResolver = new IgnoreIFormFileResolver()
            }) : null;
        }


        public void ConfigureAndAddWorkItem(long? role, PaymentRequestModel data, string description, long? assignedUser,
            StateMachine<States,Triggers>.Transition transition)
        {
            _workflowService.CreateWorkItem(new WorkItemRequest
            {
                WorkflowId = Workflow.Id.ToString(),
                FromState = (int)transition.Source,
                Trigger = (int)transition.Trigger,
                DataType = typeof(PaymentRequestModel).ToString(),
                Data = data != null ? JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
                {
                    ContractResolver = new IgnoreIFormFileResolver()
                }) : null,
                Description = description,
                AssignedRole = role,
                AssignedUser = assignedUser
            });
            if (!String.IsNullOrWhiteSpace(Workflow.Observer))
            {
                var observer = Workflow.Observer;
                var type = Type.GetType(observer);
                var wf = (IBaseStateMachine)Activator.CreateInstance(type);
                wf.SetContext(Context);
                wf.SetSession(_session);
                wf.ConfigureMachine((Guid)Workflow.Parentwfid);
                wf.OnWorkItemAdded(Workflow.Id);     
            }
        }

        public void OnWorkItemAdded(Guid wfid)
        {
            throw new NotImplementedException();
        }

        public static class ParametrizedTriggeres
        {
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> Request;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> AddDocument;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> RemoveDocument;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> AttachDocument;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> DetachDocument;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Reject1;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Reject2;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> Check;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> CheckApprove;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> Approve;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel, string> Close;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PaymentRequestModel,string> Finalize;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> RejectFinalCheck;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Cancel;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Abort;

            public static void ConfigureParameters(StateMachine<States,Triggers> machine)
            {
                Request = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.Request);
                AddDocument = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.AddDocument);
                RemoveDocument = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.RemoveDocument);
                AttachDocument = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.AttachDocument);
                DetachDocument = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.DetachDocument);
                Reject1 = machine.SetTriggerParameters<string>(Triggers.Reject1);
                Reject2 = machine.SetTriggerParameters<string>(Triggers.Reject2);
                Check = machine.SetTriggerParameters<PaymentRequestModel,string>(Triggers.Check);
                CheckApprove = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.CheckApprove);
                Approve = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.Approve);
                Close = machine.SetTriggerParameters<PaymentRequestModel, string>(Triggers.Close);
                Cancel = machine.SetTriggerParameters<string>(Triggers.Cancel);
                Abort = machine.SetTriggerParameters<string>(Triggers.Abort);
                Finalize = machine.SetTriggerParameters<PaymentRequestModel,string>(Triggers.Finalize);
                RejectFinalCheck = machine.SetTriggerParameters<string>(Triggers.RejectFinalCheck);

            }


        }
    } 
}
