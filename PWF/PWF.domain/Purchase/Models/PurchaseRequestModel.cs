﻿using BIZNET.iERP;
using INTAPS.Accounting;
using PWF.data.Entities;
using PWF.domain.Payment.Models;
using PWF.domain.PurchaseOrder.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Purchase.Models
{
    public class PurchaseRequest
    {
        public List<ItemRequestModel> Items { get; set; }
        public string Note { get; set; }
        public Document Document { get; set; }
        public RequestedBy RequestedBy { get; set; }
        public DateTime RequestDate { get; set; }

    }

    public class RequestedBy
    {
        public int Type { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }

    }

    public class PriceQuotation
    {
        public TradeRelation Supplier { get; set; }
        public List<ItemRequestModel> Items { get; set; }
        public string Note { get; set; }
        public Document Document { get; set; }

    }

    public class PurchaseRequestModel
    {
        public PurchaseRequest Request { get; set; }
        public List<PriceQuotation> Quotations { get; set; }
        public List<Guid> PurchaseOrders { get; set; }
        public DocumentTypedReference RequestNo { get; set; }
    }
     
}
    
