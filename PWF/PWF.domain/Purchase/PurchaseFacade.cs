﻿using INTAPS.Accounting;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Purchase.Models;
using PWF.domain.Purchase.StateMachines;
using PWF.domain.PurchaseOrder;
using PWF.domain.PurchaseOrder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Purchase
{

    public interface IPurchaseFacade : IPWFFacade
    {
        Guid CreatePurchaseRequest(PurchaseRequest Model);
        void ResubmitPurchaseRequest(Guid wfid, PurchaseRequest Model);
        void CancelPurchaseRequest(Guid wfid, string description);
        void ApprovePurchaseRequest(Guid wfid, string description);
        void RejectPurchaseRequest(Guid wfid, string description);
        List<PriceQuotation> AddPriceQuotation(Guid wfid, PriceQuotation Quotation);
        void SubmitPriceQuotations(Guid wfid, string description);
        Guid IssueOrder(Guid wfid,  PurchaseOrderModel Order);
        void CloseRequest(Guid wfid, string description);
        void FinalizePurchase(Guid wfid, string description);
        void RejectFinalization(Guid wfid, string description);
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        void CancelPurchaseOrder(Guid parentWFID, Guid orderWFID);

    }

    public class PurchaseFacade : PWFFacade, IPurchaseFacade
    {

        private readonly IPurchaseService _purchaseService;
        private PWFConfiguration _Config;
        private UserSession _session;
        private IDocumentService _docService;
        private IPurchaseOrderFacade _purchaseOrderFacade;
        private IPurchaseOrderService _purchaseOrderService;



        public  PurchaseFacade(PWFContext Context,IPurchaseService service, IDocumentService docService,
            IPurchaseOrderFacade purchaseOrder, IPurchaseOrderService poservice) : base(Context)
        {
            _purchaseService = service;
            _context = Context;
            _purchaseService.SetContext(Context);
            _docService = docService;
            _purchaseOrderFacade = purchaseOrder;
            _purchaseOrderService = poservice;
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _purchaseService.SetSession(session);
            _docService.SetSession(session);
            _purchaseOrderFacade.SetSession(session);
            _purchaseOrderService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _purchaseService.SetSessionConfiguration(Config, session);
            _docService.SetSessionConfiguration(Config, session);
            _session = session;
            _Config = Config;
            _purchaseOrderFacade.SetSessionConfiguration(Config, _session);
            _purchaseOrderService.SetSessionConfiguration(Config, _session);

        }

        public List<PriceQuotation> AddPriceQuotation(Guid wfid, PriceQuotation Quotation)
        {
            return Transact(t =>
            {
                var doc = Quotation.Document;
                doc.Documenttypeid = (int)DocumentTypes.PriceQuotationDocument;
                var d = _docService.AddDocument(doc);
                Quotation.Document = d;
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                List<PriceQuotation> Qots = new List<PriceQuotation>();
                if(data.Quotations != null && data.Quotations.Count > 0)
                {
                    Qots.AddRange(data.Quotations);
                }
                Qots.Add(Quotation);
                data.Quotations = Qots;
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.AddQuotation, data, Quotation.Note);
                return Qots;
            });
        }

        public  void SubmitQuotation(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.SubmitQuotaion,workflow.GetData(), description);
            });
        }



        public void ApprovePurchaseRequest(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.Approve, description);
            });
        }

        public void CancelPurchaseRequest(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.Cancel, description);
            });
        }

        public void CancelPurchaseOrder(Guid parentWFID, Guid orderWFID)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(parentWFID);
                var data = workflow.GetData();
                var orders = data.PurchaseOrders.Where(m => m != orderWFID).ToList();
                data.PurchaseOrders = orders;
                workflow.Fire(parentWFID, PurchaseWorkflow.ParametrizedTriggers.CancelOrder,data, "Cancel Purchase Order");
            });
        }

        public void CloseRequest(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.CloseRequest,data, description);
            });
        }

        public Guid CreatePurchaseRequest(PurchaseRequest Model)
        {
            return TransactWIthWSIS( (t,AID) =>
            {
                Model.Document.Documenttypeid = (int)DocumentTypes.PurchaseRequestDocument;
                var doc =_docService.AddDocument(Model.Document);
                Model.Document = doc;
                var reference = GetPurchaseRequisitionNumber(AID);
                PurchaseRequestModel model = new PurchaseRequestModel()
                {
                    Request = Model,
                    RequestNo = reference,
                };
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(Model.Note, _session.EmployeeID,null,null);
                var wfid = workflow.Workflow.Id;
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.Request, model, Model.Note);
                return wfid;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWf_File New Purchase Request", _session);
        }

        public void ResubmitPurchaseRequest(Guid wfid, PurchaseRequest Model)
        {
            Transact(t =>
            {
                Model.Document.Documenttypeid = (int)DocumentTypes.PurchaseRequestDocument;
                var doc = _docService.AddDocument(Model.Document);
                Model.Document = doc;
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                data.Request = Model;
                data.Request.RequestDate = DateTime.Now;
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.Request, data, Model.Note);

            });
        }

        public DocumentTypedReference GetPurchaseRequisitionNumber(int AID)
        {
            int serial;
            var batchID = _Config.BatchIDs.PRN;
            var reference = Helper.GetNextSerialType("PRN", batchID, out serial);
            Helper.UseSerial(new UsedSerial()
            {
                batchID = batchID,
                date = DateTime.Now,
                isVoid = false,
                val = serial,
                note = "Purchase Requisition Number"
            }, AID);
            return reference;
        }

        public void FinalizePurchase(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.Finalize, description);
            });

        }

        public Guid IssueOrder(Guid wfid, PurchaseOrderModel Order)
        {
           
            return Transact(t =>
            {
                var Guid = _purchaseOrderFacade.CreatePurchaseOrder(Order,wfid,typeof(PurchaseWorkflow).Name);
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                List<Guid> Orders = new List<Guid>();
                if (data.PurchaseOrders != null && data.PurchaseOrders.Count > 0)
                {
                    Orders.AddRange(data.PurchaseOrders);
                }
                Orders.Add(Guid);
                data.PurchaseOrders = Orders;
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.IssueOrder, data, Order.Note);
                return Guid;
            });
        }

        public void RejectFinalization(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.RejectFinalization, description);
            });

        }

        public void RejectPurchaseRequest(Guid wfid, string description)
        {
            Transact(t =>
            {
            var workflow = new PurchaseWorkflow();
            workflow.SetSession(_session);
            PassContext(workflow, _context);
            workflow.ConfigureMachine(wfid);
            workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.Reject, description);
            });

        }

        public void SubmitPriceQuotations(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseWorkflow.ParametrizedTriggers.SubmitQuotaion, workflow.GetData(), description);
            });

        }


    }
}
