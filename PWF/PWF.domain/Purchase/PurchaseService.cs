﻿using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting.BDE;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using Newtonsoft.Json;
using PWF.data;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Payment.Models;
using PWF.domain.Purchase.Models;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Purchase
{
    public interface IPurchaseService : IPWFService
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        List<WorkflowResponse> GetUserPurchaseWorkflows();
        string GetItemName(string code);
        string GetSupplierName(string code);
        TradeRelation GetSupplier(string code);
        WorkItemResponse GetLastPurchaseWorkitem(Guid guid);
        List<WorkItemResponse> GetPurchaseOrderWorkitemsOf(Guid guid);
        List<ItemRequestModel> GetAllowedMaxOrderUnits(Guid id);
        List<WorkflowResponse> GetUserPurchaseOrderWorkflows();
        TransactionItems GetTransactionItem(string code);
        List<WorkflowResponse> GetUserItemDeliveryrWorkflows();
        List<WorkflowResponse> GetUserServiceDeliveryWorkflows();
        bool CanCloseRequest(Guid wfid);
        Employee GetEmployee();
        List<ItemRequestModel> GetUncertifiedServices(Guid wfid);
    }

    public class PurchaseService : PWFService, IPurchaseService
    {

        private PWFContext _context;
        private string _sessionID;
        private UserSession _session;
        private PWFConfiguration _config;
        private IWorkflowService _wfService;
        private IUserActionService _userActonService;
        private IDocumentService _documentService;
        private iERPTransactionBDE _iERPTransactionBDE;
        private AccountingBDE _AccountingBDE;


        public PurchaseService(PWFContext Context, IWorkflowService wfService, IUserActionService userActionService, IDocumentService docService)
        {
            _context = Context;
            _wfService = wfService;
            _wfService.SetContext(Context);
            _userActonService = userActionService;
            _documentService = docService;
            _iERPTransactionBDE = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingBDE = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
        }


        public void SetSession(UserSession session)
        {
            _session = session;
            _sessionID = session.payrollSessionID;
            _wfService.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _sessionID = session.payrollSessionID;
            _session = session;
            _config = Config;
            _wfService.SetSession(_session);
            _documentService.SetSessionConfiguration(Config, session);
        }

        public Employee GetEmployee()
        {
            PayrollBDE _payroll = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            var employee = _payroll.GetEmployeeByLoginName(_session.Username);
            return employee;
        }

        public List<WorkflowResponse> GetUserPurchaseWorkflows()
        {
            var wfs = _wfService.GetUserWorkflows(_session).Where(m => m.TypeId == (int)WorkflowTypes.PurchaseWorkflow).ToList();
            return wfs;
        }

        public List<WorkflowResponse> GetUserPurchaseOrderWorkflows()
        {
            var wfs = _wfService.GetUserWorkflows(_session).Where(m => m.TypeId == (int)WorkflowTypes.PurchaseOrderWorkflow).ToList();
            return wfs;

        }

        public bool hasUndeliveredOrders(Guid wfid)
        {
            var witem = _wfService.GetLastWorkItem((Guid)wfid);
            var data = JsonConvert.DeserializeObject<PurchaseOrderModel>(witem.Data.ToString());
            List<ItemRequestModel> delivery = new List<ItemRequestModel>();
            foreach (var item in data.ItemDeliveryWorkflows)
            {
                if(item.DeliveredItems != null && item.DeliveredItems.Count > 0)
                {
                    delivery.AddRange(item.DeliveredItems);
                }
            }
            var ordered = data.Items.Where(m => m.type == GoodOrService.Good).Sum(m => m.Quantity);
            var delivred = delivery.Sum(m => m.Quantity);
            return (ordered - delivred) > 0;
        }

        public List<WorkflowResponse> GetUserItemDeliveryrWorkflows()
        {
            var wfs = _wfService.GetUserWorkflows(_session).Where(m => m.TypeId == (int)WorkflowTypes.StoreDeliveryWorkflow).ToList();
            List<WorkflowResponse> response = new List<WorkflowResponse>();
            foreach (var item in wfs)
            {
                if(item.CurrentState == 2 || hasUndeliveredOrders((Guid)item.parentWFID))
                {
                    response.Add(item);
                }
            }
            return response;

        }

        public bool hasUncertifiedServices(Guid wfid)
        {
            var witem = _wfService.GetLastWorkItem((Guid)wfid);
            var data = JsonConvert.DeserializeObject<PurchaseOrderModel>(witem.Data.ToString());
            List<ItemRequestModel> delivery = new List<ItemRequestModel>();
            foreach (var item in data.ServiceDeliveryWorkflows)
            {
                if (item.ApprovedItems != null && item.ApprovedItems.Count > 0)
                {
                    delivery.AddRange(item.ApprovedItems);
                }
            }
            var ordered = data.Items.Where(m => m.type == GoodOrService.Service).Sum(m => m.Quantity);
            var delivred = delivery.Sum(m => m.Quantity);
            return (ordered - delivred) > 0;
        }

        public List<ItemRequestModel> GetUncertifiedServices(Guid wfid)
        {
            var witem = _wfService.GetLastWorkItem(wfid);
            var data = JsonConvert.DeserializeObject<PurchaseOrderModel>(witem.Data.ToString());
            List<ItemRequestModel> delivery = new List<ItemRequestModel>();
            foreach (var item in data.ServiceDeliveryWorkflows)
            {
                if (item.ApprovedItems != null && item.ApprovedItems.Count > 0)
                {
                    delivery.AddRange(item.ApprovedItems);
                }
            }
            var ordered = data.Items.Where(m => m.type == GoodOrService.Service).ToList();
            foreach(var item in ordered)
            {
                var delivered = delivery.Where(m => m.Code == item.Code).Sum(m => m.Quantity);
                item.Quantity = item.Quantity - delivered;
            }

            return ordered;
        }

        public List<WorkflowResponse> GetUserServiceDeliveryWorkflows()
        {
            var wfs = _wfService.GetUserWorkflows(_session).Where(m => m.TypeId == (int)WorkflowTypes.ServiceDeliveryWorkflow).ToList();
            List<WorkflowResponse> response = new List<WorkflowResponse>();
            foreach (var item in wfs)
            {
                if (item.CurrentState == 2 || hasUncertifiedServices((Guid)item.parentWFID))
                {
                    response.Add(item);
                }
            }
            return response;

        }

        public string GetItemName(string code)
        {
            var item = _iERPTransactionBDE.GetTransactionItems(code);
            return item.NameCode;
        }

        public TransactionItems GetTransactionItem(string code)
        {
            var item = _iERPTransactionBDE.GetTransactionItems(code);
            return item;
        }

        public string GetSupplierName(string code)
        {

            var relation = _iERPTransactionBDE.GetSupplier(code);
            return relation.NameCode;
        }

        public TradeRelation GetSupplier(string code)
        {
            return _iERPTransactionBDE.GetSupplier(code);
        }

        public WorkItemResponse GetLastPurchaseWorkitem(Guid guid)
        {
            var response = _wfService.GetLastWorkItem(guid);
            var data = JsonConvert.DeserializeObject<PurchaseRequestModel>(response.Data.ToString());
            response.Data = data;
            return response;
        }

        public List<WorkItemResponse> GetPurchaseOrderWorkitemsOf(Guid guid)
        {
            var response = _wfService.GetLastWorkItem(guid);
            var data = JsonConvert.DeserializeObject<PurchaseRequestModel>(response.Data.ToString());
            var result = new List<WorkItemResponse>();
            if (data.PurchaseOrders != null && data.PurchaseOrders.Count > 0)
            {
                var orders = data.PurchaseOrders;
                foreach (var item in orders)
                {
                    var res = _wfService.GetLastWorkItem(item);
                    if(res.CurrentState != 0 && res.CurrentState != -3)
                    {
                        var d = JsonConvert.DeserializeObject<PurchaseOrderModel>(res.Data.ToString());
                        res.Data = d;
                        result.Add(res);
                    }
                    
                }
                return result;
            }
            else
            {
                return result;
            }
        }

        public bool CanCloseRequest(Guid wfid)
        {
            var response = _wfService.GetLastWorkItem(wfid);
            var data = JsonConvert.DeserializeObject<PurchaseRequestModel>(response.Data.ToString());
            if (data.PurchaseOrders == null)
                return false;
            var orderIDs = data.PurchaseOrders;
            List<WorkflowResponse> orders = new List<WorkflowResponse>();
            foreach (var item in orderIDs)
            {
                orders.Add(_wfService.GetWorkflow(item));
            }
            var notClosed = orders.Where(m => m.CurrentState >= 0).Count();
            if(notClosed > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public List<ItemRequestModel> GetAllowedMaxOrderUnits(Guid id)
        {
            var workItem = GetLastPurchaseWorkitem(id);
            var data = (PurchaseRequestModel)workItem.Data;
            var requestedItems = data.Request.Items;
            if(data.PurchaseOrders != null && data.PurchaseOrders.Count > 0)
            {
                var orders = GetPurchaseOrderWorkitemsOf(id).Select(m => m.Data as PurchaseOrderModel);
                var allItems = orders.Select(m => m.Items).ToList().SelectMany(list => list).ToList();
                List<ItemRequestModel> Items = new List<ItemRequestModel>();
                foreach (var item in requestedItems)
                {
                    var requiredQuantity = (item.Quantity);
                    var orderedItems = allItems.Where(m => m.Code == item.Code).ToList();
                    double orderedQuantity = 0;
                    if (orderedItems.Count > 0)
                    {
                        orderedQuantity = orderedItems.Sum(m => (double)(m.Quantity));
                    }
                    var allowedQuantity = requiredQuantity - orderedQuantity;
                    Items.Add(new ItemRequestModel()
                    {
                        Code = item.Code,
                        Quantity = allowedQuantity
                    });                 
                }
                return Items;

            }
            else
            {
                return requestedItems;
            }

        }

     
    }
}
