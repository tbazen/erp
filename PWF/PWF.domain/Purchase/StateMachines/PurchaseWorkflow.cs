﻿using Newtonsoft.Json;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Admin;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Purchase.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using Stateless;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Purchase.StateMachines
{
    public class PurchaseWorkflow : PWFService, IBaseStateMachine
    {
        public WorkflowTypes type = WorkflowTypes.PurchaseWorkflow;

        public enum States
        {
            Filing = 0,
            Reviewing = 1,
            GatheringQuotation = 2,
            Order = 3,
   
            Finalized = 4,
            Closed = -1, 
            Cancelled = -2
        }

        public enum Triggers
        {
            Request = 1,
            Approve = 2,
            Reject= 3,
            Cancel = 4,
            AddQuotation = 5,
            SubmitQuotation = 6,
            IssueOrder = 7,
            CloseRequest = 8,
            Finalize = 9,
            RejectFinalization = 10,
            CancelOrder = 11,
        }

        private readonly IWorkflowService _workflowService;
        private IPurchaseService _service;
        private UserSession _session;

        public StateMachine<States, Triggers> _machine;

        public PurchaseWorkflow()
        {
            _workflowService = new WorkflowService();
         //   _service = service;
        }

        public Workflow Workflow { get; set; }

        public override void SetContext(PWFContext value)
        {
            Context = value;
         //   _service.SetContext(value);
            _workflowService.SetContext(Context);
            base.SetContext(value);
        }


        public void ConfigureMachine(string description,int? EmployeeID, Guid? Guid, string observer = "")
        {
            Workflow = _workflowService.CreateWorkflow(new Workflows.Models.WorkflowRequest
            {
                CurrentState = (int)States.Filing,
                Description = description,
                TypeId = (int)type,
                initiatorUser = _session.Username,
                EmployeeID = EmployeeID,
                Observer = observer,
                parentWFID = Guid
            });
            _machine = new StateMachine<States, Triggers>(States.Filing);
            DefineStateMachines();
        }

        public void ConfigureMachine(Guid workflowId)
        {
            Workflow = Context.Workflow.First(wf => wf.Id == workflowId && wf.Typeid == (int)type);
            _machine = new StateMachine<States, Triggers>((States)Workflow.Currentstate);
            DefineStateMachines();
        }

        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
            _session = session;
        }

        public long getInitialUser()
        {
            var user = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(Workflow.Initiatoruser);
            return (long)user;
        }


       

        public void DefineStateMachines()
        {
            ParametrizedTriggers.ConfigureParameters(_machine);

            _machine.Configure(States.Filing)
                .Permit(Triggers.Request, States.Reviewing)
                .Permit(Triggers.Cancel, States.Cancelled)
                .OnEntryFrom(ParametrizedTriggers.Reject, OnReject);

            _machine.Configure(States.Reviewing)
                .Permit(Triggers.Approve,States.GatheringQuotation)
                 .Permit(Triggers.Reject, States.Filing)
                  .OnEntryFrom(ParametrizedTriggers.Request, OnRequest);

            _machine.Configure(States.GatheringQuotation)
                .PermitReentry(Triggers.AddQuotation)

                .Permit(Triggers.SubmitQuotation, States.Order)
                
                .OnEntryFrom(ParametrizedTriggers.AddQuotation, OnAddQuotation)
                .OnEntryFrom(ParametrizedTriggers.Approve, OnApprove);
               

            _machine.Configure(States.Order)
                .PermitReentry(Triggers.IssueOrder)
                .PermitReentry(Triggers.CancelOrder)
                .Permit(Triggers.CloseRequest, States.Finalized)
                .OnEntryFrom(ParametrizedTriggers.IssueOrder, OnIssueOrder)
                .OnEntryFrom(ParametrizedTriggers.CancelOrder, OnCancelOrder)
                .OnEntryFrom(ParametrizedTriggers.SubmitQuotaion, OnSubmitQuotation)
                .OnEntryFrom(ParametrizedTriggers.RejectFinalization, onRejectFinalize);

            _machine.Configure(States.Finalized)
                .Permit(Triggers.Finalize, States.Closed)
                .Permit(Triggers.RejectFinalization, States.Order)
                .OnEntryFrom(ParametrizedTriggers.CloseRequest, OnCloseRequest);

            _machine.Configure(States.Closed)
                .OnEntryFrom(ParametrizedTriggers.Finalize, onFinalize);

            _machine.Configure(States.Cancelled)
                .OnEntryFrom(ParametrizedTriggers.Cancel, OnCancel);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<string> trigger, string description)
        {

            _machine.Fire(trigger, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> trigger,
            PurchaseRequestModel data, string description)
        {
            _machine.Fire(trigger, data, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);

        }

        public void OnRequest(PurchaseRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser3, Model, description, _session.Id, transition);
        }

        public void OnReject(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, getInitialUser(), transition);
        }

        public void OnApprove(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser1, GetData(), description, getInitialUser(), transition);
        }


        public void OnAddQuotation(PurchaseRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser1, Model, description, getInitialUser(), transition);
        }


        public void OnSubmitQuotation(PurchaseRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, getInitialUser(), transition);
        }

        public void OnIssueOrder(PurchaseRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, getInitialUser(), transition);
        }

        public void OnCancelOrder(PurchaseRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, getInitialUser(), transition);
        }


        public void OnCloseRequest(PurchaseRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser3, Model, description, null, transition);
        }



       

        public void OnCancel(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public void onFinalize(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }
        public void onRejectFinalize(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, GetData(), description, getInitialUser(), transition );
        }

        public PurchaseRequestModel GetData()
        {
            var workItem = Context.WorkItem.Where(w => w.WorkflowId == Workflow.Id).OrderBy(wi => wi.Seqno).
                LastOrDefault();
            return workItem != null ? JsonConvert.DeserializeObject<PurchaseRequestModel>(workItem.Data, new JsonSerializerSettings()
            {
                ContractResolver = new IgnoreIFormFileResolver()
            }) : null;
        }

        public void ConfigureAndAddWorkItem(long? role, PurchaseRequestModel data, string description, long? assignedUser,
           StateMachine<States, Triggers>.Transition transition)
        {
            _workflowService.CreateWorkItem(new WorkItemRequest
            {
                WorkflowId = Workflow.Id.ToString(),
                FromState = (int)transition.Source,
                Trigger = (int)transition.Trigger,
                DataType = typeof(PurchaseRequestModel).ToString(),
                Data = data != null ? JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
                {
                    ContractResolver = new IgnoreIFormFileResolver()
                }) : null,
                Description = description,
                AssignedRole = role,
                AssignedUser = assignedUser
            });
            //if (!String.IsNullOrWhiteSpace(Workflow.Observer))
            //{
            //    //var observer = Workflow.Observer;
            //    //var type = Type.GetType(observer);
            //    //var wf = (IBaseStateMachine)Activator.CreateInstance(type);
            //    //wf.SetContext(Context);
            //    //wf.SetSession(_session);
            //    //wf.ConfigureMachine(Workflow.parentWFID);
            //    //wf.OnWorkItemAdded(Workflow.Id);
            //}
        }


        public void OnWorkItemAdded(Guid wfid)
        {
            throw new NotImplementedException();
        }

      

        public static class ParametrizedTriggers
        {
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> Request;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Approve;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Reject;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Cancel;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Finalize;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> RejectFinalization;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> AddQuotation;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> SubmitQuotaion;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> IssueOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> CancelOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseRequestModel, string> CloseRequest;

            public static void ConfigureParameters(StateMachine<States,Triggers> machine)
            {
                Request = machine.SetTriggerParameters<PurchaseRequestModel, string>(Triggers.Request);
                Approve = machine.SetTriggerParameters<string>(Triggers.Approve);
                AddQuotation = machine.SetTriggerParameters<PurchaseRequestModel, string>(Triggers.AddQuotation);
                IssueOrder = machine.SetTriggerParameters<PurchaseRequestModel, string>(Triggers.IssueOrder);
                CancelOrder = machine.SetTriggerParameters<PurchaseRequestModel, string>(Triggers.CancelOrder);
                CloseRequest = machine.SetTriggerParameters<PurchaseRequestModel, string>(Triggers.CloseRequest);
                Reject = machine.SetTriggerParameters<string>(Triggers.Reject);
                Cancel = machine.SetTriggerParameters<string>(Triggers.Cancel);
                Finalize = machine.SetTriggerParameters<string>(Triggers.Finalize);
                RejectFinalization = machine.SetTriggerParameters<string>(Triggers.RejectFinalization);
                SubmitQuotaion = machine.SetTriggerParameters<PurchaseRequestModel,string>(Triggers.SubmitQuotation);
            }

        }


    }
}
