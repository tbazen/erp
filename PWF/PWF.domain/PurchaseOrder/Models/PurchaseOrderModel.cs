﻿using BIZNET.iERP;
using INTAPS.Accounting;
using PWF.data.Entities;
using PWF.domain.Payment.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.PurchaseOrder.Models
{
    public class PurchaseOrderModel
    {
        public DocumentTypedReference Voucher { get; set; }
        public TradeRelation Supplier { get; set; }
        //Add Order Date to PurhcaseOrderModel
        public DateTime OrderDate { get; set; }
        public List<ItemRequestModel> Items { get; set; }
        public string Note { get; set; }
        public Document Document { get; set; }
        public double PaidAmount { get; set; }
        public double DeliveredAmount { get; set; }
        public double RequestedPaymentAmount { get; set; }
        public List<PaymentWorkflowModel> PaymentWorkflows { get; set; }
        public List<ItemDeliveryModel> ItemDeliveryWorkflows { get; set; }
        public List<Store.Models.ServiceDeliveryModel> ServiceDeliveryWorkflows { get; set; }

    }

    public class PaymentWorkflowModel
    {
        public Guid WorkflowID { get; set; }
        public int CurrentState { get; set; }
        public double PaidAmount { get; set; }
        public double RequestedAmount { get; set; }
        public PurchasePaymentTypes Type { get; set; }
        public string  Note { get; set; }
        public Document Document { get; set; }


    }

    public enum PurchasePaymentTypes
    {
        AdvancePayment = 1,
        CreditSettlement = 2,
        CashPurchase = 3,
    }

    public class ItemDeliveryModel
    {
        public Guid WorkflowID { get; set; }
        public DocumentTypedReference Voucher { get; set; }
        public List<ItemRequestModel> OrderedItems { get; set; }
        public List<ItemRequestModel> DeliveredItems { get; set; }
        public int CostCenterID { get; set; }
        public Document Document { get; set; }
        public string Note { get; set; }
        public int AccountDocumentID { get; set; }
        public TradeRelation Supplier { get; set; }
        public int CurrentState { get; set; }
        public Document VoucherDoc { get; set; }

    }
}
