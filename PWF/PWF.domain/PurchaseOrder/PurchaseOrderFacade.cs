﻿using PWF.data;
using PWF.domain.Infrastructure.Architecture;
using System;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Infrastructure;
using PWF.domain.Documents;
using PWF.domain.PurchaseOrder.StateMachines;
using System.Collections.Generic;
using PWF.domain.Payment;
using PWF.domain.Payment.Models;
using PWF.data.Entities;
using INTAPS.Accounting;
using PWF.domain.Store;
using BIZNET.iERP.Server;
using INTAPS.Payroll.BDE;

namespace PWF.domain.PurchaseOrder
{

    public interface IPurchaseOrderFacade : IPWFFacade
    {
        Guid CreatePurchaseOrder(PurchaseOrderModel Model, Guid? parentWFID, string observer = "");
        void CancelPurchaseOrder(Guid wfid, string description);
        void ApprovePurchaseOrder(Guid wfid, string description);
        void RejectPurchaseOrder(Guid wfid, string description);
        PaymentWorkflowModel RequestPurchasePayment(Guid wfid, PaymentWorkflowModel Model);
        ItemDeliveryModel RequestItemDelivery(Guid wfid, ItemDeliveryModel Model);
        Store.Models.ServiceDeliveryModel RequestServiceDelivery(Guid wfid, Store.Models.ServiceDeliveryModel Model);
        void ClosePurchaseOrder(Guid wfid, string description);
        void TerminatePurchaseOrder(Guid wfid, string description);
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        void ResubmitPurchaseOrder(Guid wfid, PurchaseOrderModel Model);



    }

    public class PurchaseOrderFacade : PWFFacade, IPurchaseOrderFacade
    {

        private readonly IPurchaseOrderService _service;
        private readonly IPaymentFacade _paymentFacade;
        private readonly IDeliveryFacade _deliveryFacade;
        private readonly IPaymentService _payemntService;
        private PWFConfiguration _Config;
        private UserSession _session;
        private IDocumentService _documentService;

        public PurchaseOrderFacade(PWFContext Context, IPurchaseOrderService service, IDocumentService documentService,
            IPaymentService payment, IPaymentFacade paymentFacade, IDeliveryFacade deliveryFacade) : base(Context)
        {
            _service = service;
            _context = Context;
            _service.SetContext(Context);
            _documentService = documentService;
            _payemntService = payment;
            _paymentFacade = paymentFacade;
            _deliveryFacade = deliveryFacade;
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _service.SetSession(session);
            _documentService.SetSession(session);
            _payemntService.SetSession(session);
            _paymentFacade.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _service.SetSessionConfiguration(Config, session);
            _documentService.SetSessionConfiguration(Config, session);
            _payemntService.SetSessionConfiguration(Config, session);
            _paymentFacade.SetSessionConfiguration(Config, session);
            _session = session;
            _Config = Config;
        }



        public void ApprovePurchaseOrder(Guid wfid, string description)
        {
            TransactWIthWSIS((t,AID) =>
            {
                
                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();

                var voucher = _service.GetPurchaseOrderVoucher(AID);
                data.Voucher = voucher;          
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.ApproveOrder,data, description);

            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF Approve Purchase Order, Generate POV", _session);
        }

        public void CancelPurchaseOrder(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.CancelOrder, description);

            });
        }

        public void ClosePurchaseOrder(Guid wfid, string description)
        {
            Transact(t =>
            {
                _paymentFacade.SetSessionConfiguration(_Config, _session);
                _deliveryFacade.SetSessionConfiguration(_Config, _session);
                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                foreach(var item in data.PaymentWorkflows)
                {
                    if(item.CurrentState == 0)
                    {
                        _paymentFacade.AbortPaymentWorkflow(item.WorkflowID);
                    }
                }
                foreach(var item in data.ItemDeliveryWorkflows)
                {
                    if(item.CurrentState == 0 || item.CurrentState == 1)
                    {
                        _deliveryFacade.AbortItemDelivery(item.WorkflowID);
                    }
                }
                foreach (var item in data.ServiceDeliveryWorkflows)
                {
                    if (item.CurrentState == 0 || item.CurrentState == 1)
                    {
                        _deliveryFacade.AbortServiceDelivery(item.WorkflowID);
                    }

                }
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.CloseOrder, description);

            });
        }

        public Guid CreatePurchaseOrder(PurchaseOrderModel Model,Guid? parentWFID, string observer = "")
        {

                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(Model.Note, _session.EmployeeID,parentWFID,observer);
                var wfid = workflow.Workflow.Id;
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.IssueOrder, Model, Model.Note);
                return wfid;       
        }

        public void ResubmitPurchaseOrder(Guid wfid, PurchaseOrderModel Model)
        {
            var workflow = new PurchaseOrderWorkflow();
            workflow.SetSession(_session);
            PassContext(workflow, _context);
            workflow.ConfigureMachine(wfid);
            workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.IssueOrder, Model, Model.Note);
        }

        public void RejectPurchaseOrder(Guid wfid, string description)
        {
            Transact(t =>
            {
                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.RejectOrder, description);

            });
        }

        public ItemDeliveryModel RequestItemDelivery(Guid wfid, ItemDeliveryModel Model)
        {
            return Transact(t =>
            {
                //var _iERPTransactionBDE = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
                //Model.Supplier = _iERPTransactionBDE.GetSupplier(Model.Supplier.relationCode);
                _deliveryFacade.SetSessionConfiguration(_Config, _session);
                var guid = _deliveryFacade.RequestItemDelivery(Model, wfid, typeof(PurchaseOrderWorkflow).ToString());
                Model.WorkflowID = guid;
                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                List<ItemDeliveryModel> M = new List<ItemDeliveryModel>();
                if (data.ItemDeliveryWorkflows != null && data.ItemDeliveryWorkflows.Count > 0)
                    M.AddRange(data.ItemDeliveryWorkflows);
                M.Add(Model);
                data.ItemDeliveryWorkflows = M;
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.RequestItemDelivery, data, Model.Note);
                return Model;
            });
        }

        public Store.Models.ServiceDeliveryModel RequestServiceDelivery(Guid wfid, Store.Models.ServiceDeliveryModel Model)
        {
            return Transact(t =>
            {
                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                
                var data = workflow.GetData();
                Model.Voucher = data.Voucher;
                //var empID = Model.ApprovedID;
                PayrollBDE payroll = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
                var emp = payroll.GetEmployee(Model.ApproverID);
                var userID = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(emp.loginName);
                Model.ApproverID = userID;
                _deliveryFacade.SetSessionConfiguration(_Config, _session);
                var guid = _deliveryFacade.RequestServiceDeliver(Model, wfid, typeof(PurchaseOrderWorkflow).ToString());
                Model.WorkflowID = guid;

                List<Store.Models.ServiceDeliveryModel> M = new List<Store.Models.ServiceDeliveryModel>();
                if (data.ServiceDeliveryWorkflows != null && data.ServiceDeliveryWorkflows.Count > 0)
                    M.AddRange(data.ServiceDeliveryWorkflows);
                M.Add(Model);
                data.ServiceDeliveryWorkflows = M;
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.RequestServiceDelivery, data, Model.Note);
                return Model;
            });
        }

        public DocumentTypedReference GetStoreReceivingReference(int AID)
        {
            int serial;
            var batchID = _Config.BatchIDs.SRV;
            var reference = Helper.GetNextSerialType("SRV", batchID, out serial);
            Helper.UseSerial(new UsedSerial()
            {
                batchID = batchID,
                date = DateTime.Now,
                isVoid = false,
                val = serial,
                note = "Store Receiving Voucher"
            }, AID);
            return reference;
        }

        public PaymentWorkflowModel RequestPurchasePayment(Guid wfid, PaymentWorkflowModel Model)
        {
            return Transact(t =>
            {
                Model.Document.Documenttypeid = (int)DocumentTypes.PaymentRequestDocument;
                Model.Document.Filename = Model.Document.Name;
                var doc = _documentService.AddDocument(Model.Document);
                Model.Document = doc;

                var workflow = new PurchaseOrderWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var Data = workflow.GetData();
                
                List<ItemRequestModel> Items = new List<ItemRequestModel>();
                if(Model.Type == PurchasePaymentTypes.AdvancePayment)
                {
                    Items.Add(new ItemRequestModel()
                    {
                        Amount = Model.RequestedAmount,
                        Quantity = 1,
                        UnitPrice = Model.RequestedAmount,
                        Description = "Supplier Advanced Payment",
                        Code = _Config.SupplierAdvancedPaymentCode
                    });
                }
                if(Model.Type == PurchasePaymentTypes.CreditSettlement)
                {
                    Items.Add(new ItemRequestModel()
                    {
                        Amount = Model.RequestedAmount,
                        Quantity = 1,
                        UnitPrice = Model.RequestedAmount,
                        Description = "Supplier Credit Settlement",
                        Code = _Config.SupplierCreditSettlementCode
                    });
                }
                if(Model.Type == PurchasePaymentTypes.CashPurchase)
                {
                    Items = Data.Items;
                }

                PaymentRequest Requets = new PaymentRequest()
                {
                    type = RequestType.PurchasePayment,
                    Items = Items,
                    Files = Model.Document,
                    PaidTo = new PaidTo()
                    {
                        Type = 2,
                        code = Data.Supplier.Code,
                        name = Data.Supplier.Name

                    },
                    Note = Model.Note,
                    SubmittedBy = Admin.UserRoles.Purchaser2
                };

                var Guid = _paymentFacade.CreatePaymentRequest(Requets, Model.Note, wfid, typeof(PurchaseOrderWorkflow).ToString());

                Model.WorkflowID = Guid;
                Model.CurrentState = 1;
                List<PaymentWorkflowModel> paymentwf = new List<PaymentWorkflowModel>();
                if(Data.PaymentWorkflows != null && Data.PaymentWorkflows.Count > 0)
                {
                    paymentwf.AddRange(Data.PaymentWorkflows);
                }
                paymentwf.Add(Model);
                Data.PaymentWorkflows = paymentwf;
                workflow.Fire(wfid, PurchaseOrderWorkflow.ParametrizedTriggers.RequestPayment,Data, Model.Note);
                return Model;
            });
        }

        public void TerminatePurchaseOrder(Guid wfid, string description)
        {
            throw new NotImplementedException();
        }
    }
}
