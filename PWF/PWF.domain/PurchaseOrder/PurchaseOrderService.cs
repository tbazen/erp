﻿using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using Newtonsoft.Json;
using PWF.data;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Payment.Models;
using PWF.domain.Purchase.Models;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.PurchaseOrder
{
    public interface IPurchaseOrderService : IPWFService
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        WorkItemResponse GetPurchaseOrderWorkItem(Guid guid);
        PurchasedItemDeliveryDocument SetPurchasedItemDeliveryDocument(ItemDeliveryModel Model);
        WorkItemResponse GetItemDeliveryWorkItem(Guid guid);
        WorkItemResponse GetServiceDeliveryWorkItem(Guid guid);
        DocumentTypedReference GetPurchaseOrderVoucher(int AID);
    }

    public class PurchaseOrderService : PWFService, IPurchaseOrderService
    {
        private PWFContext _context;
        private string _sessionID;
        private UserSession _session;
        private PWFConfiguration _config;
        private IWorkflowService _wfService;
        private IUserActionService _userActonService;
        private IDocumentService _documentService;
        private iERPTransactionBDE _iERPTransactionBDE;
        private AccountingBDE _AccountingBDE;


        public PurchaseOrderService(PWFContext Context, IWorkflowService wfService, IUserActionService userActionService, IDocumentService docService)
        {
            _context = Context;
            _wfService = wfService;
            _wfService.SetContext(Context);
            _userActonService = userActionService;
            _documentService = docService;
            _iERPTransactionBDE = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingBDE = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
        }

        public WorkItemResponse GetPurchaseOrderWorkItem(Guid guid)
        {

            var response = _wfService.GetLastWorkItem(guid);
            //var requestDoc = _docService.GetDocument(guid);
            var data = JsonConvert.DeserializeObject<PurchaseOrderModel>(response.Data.ToString());
            //data.Request.Files = requestDoc;
            response.Data = data;
            return response;
        }

        public WorkItemResponse GetItemDeliveryWorkItem(Guid guid)
        {
            var response = _wfService.GetLastWorkItem(guid);
            var data = JsonConvert.DeserializeObject<ItemDeliveryModel>(response.Data.ToString());
            response.Data = data;
            return response;
        }

        public WorkItemResponse GetServiceDeliveryWorkItem(Guid guid)
        {
            var response = _wfService.GetLastWorkItem(guid);
            var data = JsonConvert.DeserializeObject<Store.Models.ServiceDeliveryModel>(response.Data.ToString());
            response.Data = data;
            return response;
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _sessionID = session.payrollSessionID;
            _wfService.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _sessionID = session.payrollSessionID;
            _session = session;
            _config = Config;
            _documentService.SetSessionConfiguration(Config, session);
            _wfService.SetSession(session);
        }

        public PurchasedItemDeliveryDocument SetPurchasedItemDeliveryDocument(ItemDeliveryModel Model)
        {
            List<TransactionDocumentItem> Items = new List<TransactionDocumentItem>();
            foreach (var i in Model.DeliveredItems)
            {
                var transactionItem = _iERPTransactionBDE.GetTransactionItems(i.Code);
                Items.Add(new TransactionDocumentItem(Model.CostCenterID, transactionItem, (double)(i.Quantity), (double)(i.UnitPrice)));
            }
            PurchasedItemDeliveryDocument Doc = new PurchasedItemDeliveryDocument()
            {
                relationCode = Model.Supplier.Code,
                items = Items.ToArray(),
                DocumentDate = DateTime.Now,
                ShortDescription = Model.Note
            };
            List<TransactionDocumentItem> Ordered = new List<TransactionDocumentItem>();
            foreach (var i in Model.OrderedItems)
            {
                var transactionItem = _iERPTransactionBDE.GetTransactionItems(i.Code);
                Ordered.Add(new TransactionDocumentItem(Model.CostCenterID, transactionItem, (double)(i.Quantity), (double)(i.UnitPrice)));
                Doc.orderItems = Ordered.ToArray();
            }
                return Doc;
            }

        public DocumentTypedReference  GetPurchaseOrderVoucher(int AID)
        {
            int serial;
            var batchID = _config.BatchIDs.PO;
            var reference = Helper.GetNextSerialType("PO", batchID, out serial);
            Helper.UseSerial(new UsedSerial()
            {
                batchID = batchID,
                date = DateTime.Now,
                isVoid = false,
                val = serial,
                note = "Purchase Order Voucher"
            }, AID);
            return reference;
        }

        }
    }


