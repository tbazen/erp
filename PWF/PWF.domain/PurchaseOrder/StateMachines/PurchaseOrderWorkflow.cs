﻿using INTAPS.Accounting;
using Newtonsoft.Json;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Admin;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Payment;
using PWF.domain.Payment.StateMachines;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Store.StateMachines;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using Stateless;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.PurchaseOrder.StateMachines
{
    public class PurchaseOrderWorkflow : PWFService, IBaseStateMachine
    {
        public WorkflowTypes type = WorkflowTypes.PurchaseOrderWorkflow;

        public enum States
        {
            Filing = 0,
            Reviewing = 1,
            Process = 2,
            Closed = -1,
            Terminated = -2,
            Cancelled = -3
        }

        public enum Triggers
        {
            IssueOrder = 1,
            ApproveOrder = 2,
            RejectOrder = 3,
            RequestPayment = 4,
            RequestItemDelivery = 5,
            RequestServiceDelivery = 11,
            UpdatePayment = 6, 
            UpdateItemDelivery = 7,
            UpdateServiceDelivery = 12,
            TerminateOrder = 8,
            CloseOrder = 9,
            CancelOrder = 10
        }

        private readonly IWorkflowService _workflowService;
        private IPurchaseOrderService _service;
        private IPaymentService _paymentService;
        private StateMachine<States, Triggers> _machine;
        private UserSession _session;

        public PurchaseOrderWorkflow()
        {
            _workflowService = new WorkflowService();
        }

        public PWF.data.Entities.Workflow  Workflow { get; set; }

        public override void SetContext(PWFContext value)
        {
            Context = value;
          //  _service.SetContext(value);
            _workflowService.SetContext(value);
           // _paymentService.SetContext(value);
            base.SetContext(value);
        }

        public void ConfigureMachine(string description, int? EmpID,Guid? Guid, string observer = "")
        {
            Workflow = _workflowService.CreateWorkflow(new WorkflowRequest
            {
                CurrentState = (int)States.Filing,
                Description = description,
                TypeId = (int)type,
                Observer = observer,
                parentWFID = Guid
            });
            _machine = new StateMachine<States, Triggers>(States.Filing);
            DefineStateMachine();
        }

        public void ConfigureMachine(Guid WorkflowId)
        {
            Workflow = Context.Workflow.First(wf => wf.Id == WorkflowId && wf.Typeid == (int)type);
            _machine = new StateMachine<States, Triggers>((States)Workflow.Currentstate);
            DefineStateMachine();
        }

        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
            //_paymentService.SetSession(session);
            _session = session;
        }

        public long getInitialUser()
        {
            var user = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(Workflow.Initiatoruser);
            return (long)user;
        }


        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<string> trigger, string description)
        {

            _machine.Fire(trigger, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> trigger,
            PurchaseOrderModel data, string description)
        {
            _machine.Fire(trigger, data, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);

        }

        public void DefineStateMachine()
        {
            ParametrizedTriggers.ConfigureParameters(_machine);

            _machine.Configure(States.Filing)
                .Permit(Triggers.IssueOrder, States.Reviewing)
                .Permit(Triggers.CancelOrder,States.Cancelled)
                .OnEntryFrom(ParametrizedTriggers.RejectOrder,OnRejectOrder);

            _machine.Configure(States.Reviewing)
                .Permit(Triggers.ApproveOrder, States.Process)
                .Permit(Triggers.RejectOrder, States.Filing)
                .OnEntryFrom(ParametrizedTriggers.IssueOrder, OnIssuerOrder);

            _machine.Configure(States.Process)
                .PermitReentry(Triggers.RequestItemDelivery)
                .PermitReentry(Triggers.RequestServiceDelivery)
                .PermitReentry(Triggers.RequestPayment)
                .PermitReentry(Triggers.UpdatePayment)
                .PermitReentry(Triggers.UpdateItemDelivery)
                .PermitReentry(Triggers.UpdateServiceDelivery)
                .Permit(Triggers.CloseOrder, States.Closed)
                .Permit(Triggers.TerminateOrder, States.Terminated)
                .OnEntryFrom(ParametrizedTriggers.ApproveOrder, OnApproveOrder)
                .OnEntryFrom(ParametrizedTriggers.RequestPayment, OnRequestPayment)
                .OnEntryFrom(ParametrizedTriggers.RequestItemDelivery, OnRequestItemDelivery)
                .OnEntryFrom(ParametrizedTriggers.RequestServiceDelivery, OnRequestServiceDelivery)
                .OnEntryFrom(ParametrizedTriggers.UpdatePayment, OnUpdateItemDelivery)
                .OnEntryFrom(ParametrizedTriggers.UpdateItemDelivery, OnUpdateDelivery)
                .OnEntryFrom(ParametrizedTriggers.UpdateServiceDelivery, OnUpdateServiceDelivery);

            _machine.Configure(States.Closed)
                .OnEntryFrom(ParametrizedTriggers.CloseOrder, OnCloseOrder);

            _machine.Configure(States.Terminated)
                .OnEntryFrom(ParametrizedTriggers.TerminateOrder, OnTerminateOrder);

            _machine.Configure(States.Cancelled)
                .OnEntryFrom(ParametrizedTriggers.CancelOrder, OnCancelOrder);


        }


        public void OnIssuerOrder(PurchaseOrderModel Model, string description, StateMachine<States,Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser3, Model, description, null, transition);
        }

        public void OnRequestPayment(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public void OnRequestItemDelivery(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public void OnRequestServiceDelivery(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public void OnUpdateItemDelivery(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public void OnUpdateServiceDelivery(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public void OnUpdateDelivery(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public void OnCancelOrder(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public void OnTerminateOrder(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public void OnCloseOrder(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public void OnRejectOrder(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, getInitialUser(), transition);
        }

        public void OnApproveOrder(PurchaseOrderModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.Purchaser2, Model, description, null, transition);
        }

        public PurchaseOrderModel GetData()
        {
            var workItem = Context.WorkItem.Where(w => w.WorkflowId == Workflow.Id).OrderBy(wi => wi.Seqno).
                LastOrDefault();
            return workItem != null ? JsonConvert.DeserializeObject<PurchaseOrderModel>(workItem.Data, new JsonSerializerSettings()
            {
                ContractResolver = new IgnoreIFormFileResolver()
            }) : null;
        }


        public void ConfigureAndAddWorkItem(long? role, PurchaseOrderModel data, string description, long? assignedUser,
            StateMachine<States, Triggers>.Transition transition)
        {
            _workflowService.CreateWorkItem(new WorkItemRequest
            {
                WorkflowId = Workflow.Id.ToString(),
                FromState = (int)transition.Source,
                Trigger = (int)transition.Trigger,
                DataType = typeof(PurchaseOrderModel).ToString(),
                Data = data != null ? JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
                {
                    ContractResolver = new IgnoreIFormFileResolver()
                }) : null,
                Description = description,
                AssignedRole = role,
                AssignedUser = assignedUser
            });
        }
        
        public void OnWorkItemAdded(Guid wfid)
        {
 
            var workflow = _workflowService.GetWorkflow(wfid);
            if(workflow.TypeId == (int)WorkflowTypes.StoreDeliveryWorkflow)
            {
                var witem = _workflowService.GetLastWorkItem(wfid);
                var wf = new ItemDeliveryWorkflow();
                wf.SetSession(_session);
                wf.SetContext(Context);
                wf.ConfigureMachine(wfid);
                var data = wf.GetData();
                data.CurrentState = Helper.ItemDeliveryWFStateIdentifier((ItemDeliveryWorkflow.States)witem.FromState, (ItemDeliveryWorkflow.Triggers)(witem.Trigger));
                var parentData = GetData();
                var itemDeliveries = parentData.ItemDeliveryWorkflows;
                if(itemDeliveries != null)
                {
                    var index = itemDeliveries.FindIndex(m => m.WorkflowID == wfid);
                    if(index != -1)
                    {
                        data.WorkflowID = parentData.ItemDeliveryWorkflows[index].WorkflowID;
                        parentData.ItemDeliveryWorkflows[index] = data;
                    }
                }                               
                Fire(wfid, ParametrizedTriggers.UpdateItemDelivery, parentData, data.Note);
               
            }

            else if(workflow.TypeId == (int)WorkflowTypes.ServiceDeliveryWorkflow)
            {
                var witem = _workflowService.GetLastWorkItem(wfid);
                var wf = new ServiceDeliveryWorkflow();
                wf.SetSession(_session);
                wf.SetContext(Context);
                wf.ConfigureMachine(wfid);
                var data = wf.GetData();
                data.CurrentState = Helper.ServiceDeliveryWFStateIdentifier((ServiceDeliveryWorkflow.States)witem.FromState, (ServiceDeliveryWorkflow.Triggers)(witem.Trigger));
                var parentData = GetData();
                var ServiceDeliveries = parentData.ServiceDeliveryWorkflows;
                if (ServiceDeliveries != null)
                {
                    var index = ServiceDeliveries.FindIndex(m => m.WorkflowID == wfid);
                    if (index != -1)
                    {
                        data.WorkflowID = parentData.ServiceDeliveryWorkflows[index].WorkflowID;
                        parentData.ServiceDeliveryWorkflows[index] = data;
                    }
                }
                Fire(wfid, ParametrizedTriggers.UpdateServiceDelivery, parentData, data.Note);
            }


            else if(workflow.TypeId == (int)WorkflowTypes.PaymentWorkflow)
            {

                var witem  = _workflowService.GetLastWorkItem(wfid);
                var wf = new PaymentWorkflow();
                wf.SetSession(_session);
                wf.SetContext(Context);
                wf.ConfigureMachine(wfid);
                var data = wf.GetData();
                PaymentWorkflowModel model = new PaymentWorkflowModel()
                {
                    CurrentState = Helper.PaymentWFStateIdentifier((PaymentWorkflow.States)witem.FromState,(PaymentWorkflow.Triggers)(witem.Trigger)),
                    Note = witem.Description,
                    WorkflowID = wfid,
                    Document = data.Request.Files
                };
               
                var om = GetData().PaymentWorkflows;
                if(om == null || !(om.Where(m =>m.WorkflowID == wfid).Any()))
                {
                    model.RequestedAmount = data.Request.Items.Sum(m => (double)m.Amount);
                    //if(data.Request.Items.Count == 1)
                    //{
                        if(data.Request.Items.Count == 1 && data.Request.Items[0].Description == "Supplier Advanced Payment")
                        {
                            model.Type = PurchasePaymentTypes.AdvancePayment;
                        }
                        else if(data.Request.Items.Count == 1 && data.Request.Items[0].Description == "Supplier Credit Settlement")
                        {
                            model.Type = PurchasePaymentTypes.CreditSettlement;
                        }
                    //}
                    else
                    {
                        model.Type = PurchasePaymentTypes.CashPurchase;
                    }
                }
                else
                {
                    var OrderModel = GetData().PaymentWorkflows.Where(m => m.WorkflowID == wfid).First();
                    model.RequestedAmount = OrderModel.RequestedAmount;
                    model.Type = OrderModel.Type;
                }
                double paidAmount = 0;
                
                if (witem.Trigger == (int)PaymentWorkflow.Triggers.Finalize)
                {
                    List<AccountDocument> allDocuments = new List<AccountDocument>();
                    if(data.ImportedDocuments != null) { allDocuments.AddRange(data.ImportedDocuments); }
                    if(data.NewDocuments != null) { allDocuments.AddRange(data.NewDocuments); }
                    var docIDs = allDocuments.Select(m => m.AccountDocumentID).ToList();                  
                    var sourceIDs = data.Sources.Select(m => m.AssetAccountID).ToArray();
                    paidAmount = Helper.GetNetTransactionAmount(docIDs);
                    model.PaidAmount = paidAmount;            
                }
                //else if(witem.Trigger == (int)PaymentWorkflow.Triggers.Reject1)
                //{
                //    model.RequestedAmount = 0;
                //}
                var modifiedData = GetData();
                if(om == null || !(om.Where(m => m.WorkflowID == wfid).Any()))
                {
                    List<PaymentWorkflowModel> m = new List<PaymentWorkflowModel>();
                    if(om != null && om.Count > 0)
                    {
                        m.AddRange(om);
                    }
                    m.Add(model);
                    modifiedData.PaymentWorkflows = m;
                }
                else
                {
                    var index = modifiedData.PaymentWorkflows.FindIndex(m => m.WorkflowID == wfid);
                    modifiedData.PaymentWorkflows[index] = model;
                }
                Fire(Workflow.Id, ParametrizedTriggers.UpdatePayment, modifiedData, witem.Description);
                

            }
        }

        public static class ParametrizedTriggers
        {
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> IssueOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> ApproveOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> RejectOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> RequestPayment;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> RequestItemDelivery;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> RequestServiceDelivery;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> UpdatePayment;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> UpdateItemDelivery;
            public static StateMachine<States, Triggers>.TriggerWithParameters<PurchaseOrderModel, string> UpdateServiceDelivery;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> TerminateOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> CloseOrder;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> CancelOrder;


            public static void ConfigureParameters(StateMachine<States,Triggers> machine)
            {
                IssueOrder = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.IssueOrder);
                RequestPayment = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.RequestPayment);
                RequestItemDelivery = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.RequestItemDelivery);
                RequestServiceDelivery = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.RequestServiceDelivery);
                UpdatePayment = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.UpdatePayment);
                UpdateItemDelivery = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.UpdateItemDelivery);
                UpdateServiceDelivery = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.UpdateServiceDelivery);
                TerminateOrder = machine.SetTriggerParameters<string>(Triggers.TerminateOrder);
                CloseOrder = machine.SetTriggerParameters<string>(Triggers.CloseOrder);
                ApproveOrder = machine.SetTriggerParameters<PurchaseOrderModel, string>(Triggers.ApproveOrder);
                RejectOrder = machine.SetTriggerParameters<string>(Triggers.RejectOrder);
                CancelOrder = machine.SetTriggerParameters<string>(Triggers.CancelOrder);

            }
        }

    }
}
