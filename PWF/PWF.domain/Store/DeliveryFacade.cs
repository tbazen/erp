﻿using BIZNET.iERP.Server;
using INTAPS.Accounting.BDE;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.PurchaseOrder.StateMachines;
using PWF.domain.Store.Models;
using PWF.domain.Store.StateMachines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Store
{
    public interface IDeliveryFacade : IPWFFacade
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        Guid RequestItemDelivery(ItemDeliveryModel Model, Guid? parentWFID, string observer);
        Guid RequestServiceDeliver(ServiceDeliveryModel Model, Guid? parentWFID, string observer);
        void ApproveServiceDelivery(Guid wfid, ServiceDeliveryModel Model);
        void RejectServiceDelivery(Guid wfid, ServiceDeliveryModel Model);
        void DeliverItems(Guid wfid, ItemDeliveryModel Model);
        void SubmitDeliveryVoucher(Guid wfid, ItemDeliveryModel Model);
        List<Payment.Models.ItemRequestModel> RemainingOrderedItems(Guid wfid);
        List<Payment.Models.ItemRequestModel> RemainingOrderedServices(Guid wfid);
        void AbortItemDelivery(Guid wfid);
        void AbortServiceDelivery(Guid wfid);
    }

    public class DeliveryFacade : PWFFacade, IDeliveryFacade
    {

        private readonly IDeliveryService _service;
        private PWFConfiguration _Config;
        private UserSession _session;
        private iERPTransactionBDE _IERPService;
        private AccountingBDE _AccountingService;
        private IDocumentService _documentService;

        public DeliveryFacade(PWFContext Context, IDeliveryService delivService, IDocumentService docService) : base(Context)
        {
            _service = delivService;
            _context = Context;
            _service.SetContext(Context);
            _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();
            _documentService = docService;

        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _service.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _service.SetSessionConfiguration(Config, session);
            _documentService.SetSessionConfiguration(Config, session);
            _session = session;
            _Config = Config;
        }

        public Guid RequestItemDelivery(ItemDeliveryModel Model, Guid? parentWFID, string observer)
        {
            var workflow = new ItemDeliveryWorkflow();
            PassContext(workflow, _context);
            workflow.SetSession(_session);
            workflow.ConfigureMachine(Model.Note, _session.EmployeeID, parentWFID, observer);
            var wfid = workflow.Workflow.Id;
            workflow.Fire(wfid, ItemDeliveryWorkflow.ParametrizedTriggers.Request, Model, Model.Note);
            return wfid;
        }

        public Guid RequestServiceDeliver(ServiceDeliveryModel Model, Guid? parentWFID, string observer)
        {
            var workflow = new ServiceDeliveryWorkflow();
            PassContext(workflow, _context);
            workflow.SetSession(_session);
            workflow.ConfigureMachine(Model.Note, _session.EmployeeID, parentWFID, observer);
            var wfid = workflow.Workflow.Id;
            workflow.Fire(wfid, ServiceDeliveryWorkflow.ParametrizedTriggers.Request, Model, Model.Note);
            return wfid;
        }



        public void RejectServiceDelivery(Guid wfid, ServiceDeliveryModel Model)
        {
            Transact(t =>
            {
                var workflow = new ServiceDeliveryWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, ServiceDeliveryWorkflow.ParametrizedTriggers.Reject, Model, Model.Note);
            });

        }

        public void AbortItemDelivery(Guid wfid)
        {
      
                var workflow = new ItemDeliveryWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                workflow.Fire(wfid, ItemDeliveryWorkflow.ParametrizedTriggers.Abort, "Aborted");
        
        }

        public void AbortServiceDelivery(Guid wfid)
        {
            var workflow = new ServiceDeliveryWorkflow();
            workflow.SetSession(_session);
            PassContext(workflow, _context);
            workflow.ConfigureMachine(wfid);
            workflow.Fire(wfid, ServiceDeliveryWorkflow.ParametrizedTriggers.Abort, "Aborted");
        }



        public void ApproveServiceDelivery(Guid wfid, ServiceDeliveryModel Model)
        {
            TransactWIthWSIS((t,AID) =>
            {

                var certifDoc = Model.Document;
                certifDoc.Documenttypeid = (int)DocumentTypes.ServiceCertificationDocument;
                var d = _documentService.AddDocument(certifDoc);
                Model.Document = d;

                var workflow = new ServiceDeliveryWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();

                foreach (var item in Model.ApprovedItems)
                {
                    var i = data.OrderedItems.Where(m => m.Code == item.Code).First();
                    item.Description = i.Description;
                    item.UnitPrice = i.UnitPrice;
                    item.type = i.type;
                    item.Amount = (i.UnitPrice) * (item.Quantity);
                }

                Model.ApproverID = data.ApproverID;
                Model.Supplier = data.Supplier;
                Model.OrderedItems = data.OrderedItems;
                Model.WorkflowID = wfid;
                Model.Voucher = data.Voucher;

                var Doc = _service.SetServiceDeliveryDocument(Model);
                var docID = Helper.PostGenericDocument(Doc, AID);
                Model.AccountDocumentID = docID;
                workflow.Fire(wfid, ServiceDeliveryWorkflow.ParametrizedTriggers.Approve, Model, Model.Note);
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF Post Service Delivery Document", _session);

        }



        public void DeliverItems(Guid wfid, ItemDeliveryModel Model)
        {
            TransactWIthWSIS((t,AID) =>
            {
                var workflow = new ItemDeliveryWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                Model.Supplier = data.Supplier;
                Model.OrderedItems = data.OrderedItems;
                Model.WorkflowID = wfid;

                var Doc = _service.SetPurchasedItemDeliveryDocument(Model);
                var reference = _service.GetStoreReceivingReference(AID);
                Doc.voucher = reference;
                Doc.PaperRef = reference.reference;
                var docID = Helper.PostGenericDocument(Doc, AID);

                foreach(var i in Model.DeliveredItems)
                {
                    var item = Model.OrderedItems.Where(m => m.Code == i.Code).First();
                    i.Description = item.Description;
                    i.UnitPrice = item.UnitPrice;
                    i.type = item.type;
                    i.Amount = i.Quantity * item.UnitPrice;
                }

                var DocToUpload = Model.Document;
                DocToUpload.Documenttypeid = (int)DocumentTypes.StoreDeliveryDocument;
                var doc = _documentService.AddDocument(DocToUpload);
                Model.Document = doc;
                Model.AccountDocumentID = docID;
                Model.Voucher = reference;
                
                workflow.Fire(wfid, ItemDeliveryWorkflow.ParametrizedTriggers.Deliver, Model, Model.Note);
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF Deliver Items to store", _session);
        }

        public void SubmitDeliveryVoucher(Guid wfid, ItemDeliveryModel Model)
        {
            Transact(t =>
            {
                var workflow = new ItemDeliveryWorkflow();
                workflow.SetSession(_session);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();

                var voucherDoc = Model.VoucherDoc;
                voucherDoc.Documenttypeid = (int)DocumentTypes.SignedStoreVoucherDocument;
                var doc = _documentService.AddDocument(voucherDoc);

                data.VoucherDoc = doc;
                data.Note = Model.Note;
                workflow.Fire(wfid, ItemDeliveryWorkflow.ParametrizedTriggers.SubmitVoucher, data, Model.Note);
            });
        }

        public List<Payment.Models.ItemRequestModel> RemainingOrderedItems(Guid wfid)
        {
            var order = new PurchaseOrderWorkflow();
            order.SetSession(_session);
            PassContext(order, _context);
            order.ConfigureMachine(wfid);
            var orderData = order.GetData();
            var itemDeliveries = orderData.ItemDeliveryWorkflows;
            var Ordered = orderData.Items.Where(m => m.type == BIZNET.iERP.GoodOrService.Good).ToList();
            if (itemDeliveries == null)
                return Ordered;
            //List<Payment.Models.ItemDeliveryRequestModel> AllDeliveries = 
            
            var delivered = itemDeliveries.Where(m => m.CurrentState == -1 || m.CurrentState == 2).SelectMany(m => m.DeliveredItems).ToList();

            foreach(var item in Ordered)
            {
                var dItems = delivered.Where(m => m.Code == item.Code).ToArray();
                var d = dItems.Length > 0 ? dItems.Sum(m => m.Quantity) : 0;
                var or = item.Quantity;
                item.Quantity = or - d;
            }
            return Ordered; 
        }

        public List<Payment.Models.ItemRequestModel> RemainingOrderedServices(Guid wfid)
        {
            var order = new PurchaseOrderWorkflow();
            order.SetSession(_session);
            PassContext(order, _context);
            order.ConfigureMachine(wfid);
            var orderData = order.GetData();
            var itemDeliveries = orderData.ServiceDeliveryWorkflows;
            var Ordered = orderData.Items.Where(m => m.type == BIZNET.iERP.GoodOrService.Service).ToList();
            if (itemDeliveries == null)
                return Ordered;
            //List<Payment.Models.ItemDeliveryRequestModel> AllDeliveries = 
            var delivered = itemDeliveries.Where(m => m.CurrentState == 2 || m.CurrentState == -1).SelectMany(m => m.ApprovedItems).ToList();

            foreach (var item in Ordered)
            {
                var dItems = delivered.Where(m => m.Code == item.Code).ToArray();
                var d = dItems.Length > 0 ? dItems.Sum(m => m.Quantity) : 0;
                var or = item.Quantity;
                item.Quantity = or - d;
            }
            return Ordered;
        }

    }
}
