﻿using BIZNET.iERP;
using BIZNET.iERP.Server;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using PWF.data;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Store.Models;
using PWF.domain.Workflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Store
{
    public interface IDeliveryService : IPWFService
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        PurchasedItemDeliveryDocument SetPurchasedItemDeliveryDocument(ItemDeliveryModel Model);
        DocumentTypedReference GetStoreReceivingReference(int AID);
        PurchasedItemDeliveryDocument GetDeliveryDocument(int docID);
        Purchase2Document SetServiceDeliveryDocument(ServiceDeliveryModel Model);
    }


    public class DeliveryService : PWFService, IDeliveryService
    {
        private PWFContext _context;
        private UserSession _session;
        private string _url;
        private string _sessionID;
        private PWFConfiguration _config;

        private IWorkflowService _wfService;
        private IUserActionService _userActionService;
        private IDocumentService _documentService;
        private iERPTransactionBDE _iERPTransactionBDE;
        private AccountingBDE _AccountingService;

        public DeliveryService(PWFContext Context,IWorkflowService wfService, IUserActionService actionSercice, IDocumentService docService)
        {
            _context = Context;
            _wfService = wfService;
            _wfService.SetContext(Context);
            _userActionService = actionSercice;
            _documentService = docService;
            _iERPTransactionBDE = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();


        }


        public void SetSession(UserSession session)
        {
            _session = session;
            _sessionID = session.payrollSessionID;
            _wfService.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _sessionID = session.payrollSessionID;
            _session = session;
            _url = Config.Host;
            _config = Config;
            _documentService.SetSessionConfiguration(Config, session);
        }

        public PurchasedItemDeliveryDocument GetDeliveryDocument(int docID)
        {
            var document = _AccountingService.GetAccountDocument<PurchasedItemDeliveryDocument>(docID);
            return document;
        }

        public Purchase2Document SetServiceDeliveryDocument(ServiceDeliveryModel Model)
        {
            var costCenterID = _iERPTransactionBDE.SysPars.mainCostCenterID;
            Purchase2Document Doc = new Purchase2Document()
            {
                paymentVoucher = Model.Voucher,
                supplierCredit = (double)Model.ApprovedItems.Sum(m => m.Amount),
                paidAmount = 0,
                amount = (double)Model.ApprovedItems.Sum(m => m.Amount),
                taxImposed = new TaxImposed[] { },
                relationCode = Model.Supplier.Code,
                paymentMethod = BizNetPaymentMethod.Credit,
                ShortDescription = "Post Service Delivery Document",
                PaperRef = Model.Voucher.reference,
                DocumentDate = DateTime.Now,
                manualVATBase = 0,
                manualWithholdBase = 0
            };
            List<TransactionDocumentItem> Items = new List<TransactionDocumentItem>();
            foreach (var i in Model.ApprovedItems)
            {
                var transactionItem = _iERPTransactionBDE.GetTransactionItems(i.Code);
                var unitPrice = Model.OrderedItems.Where(m => m.Code == i.Code).First().UnitPrice;

                Items.Add(new TransactionDocumentItem(costCenterID, transactionItem, (double)i.Quantity, (double)unitPrice));
            }
            Doc.items = Items.ToArray();
            return Doc;

        }


        public PurchasedItemDeliveryDocument SetPurchasedItemDeliveryDocument(ItemDeliveryModel Model)
        {
            List<TransactionDocumentItem> Items = new List<TransactionDocumentItem>();
            foreach (var i in Model.DeliveredItems)
            {
                var transactionItem = _iERPTransactionBDE.GetTransactionItems(i.Code);
                var unitPrice = Model.OrderedItems.Where(m => m.Code == i.Code).First().UnitPrice;
                Items.Add(new TransactionDocumentItem(Model.CostCenterID, transactionItem,(double)i.Quantity,(double)unitPrice));
            }
            PurchasedItemDeliveryDocument Doc = new PurchasedItemDeliveryDocument()
            {
                relationCode = Model.Supplier.Code,
                items = Items.ToArray(),
                DocumentDate = DateTime.Now,
                ShortDescription = Model.Note
            };
            List<TransactionDocumentItem> Ordered = new List<TransactionDocumentItem>();
            foreach (var i in Model.OrderedItems)
            {
                var transactionItem = _iERPTransactionBDE.GetTransactionItems(i.Code);
                Ordered.Add(new TransactionDocumentItem(Model.CostCenterID, transactionItem, (double)i.Quantity, (double)i.UnitPrice));
            }
            Doc.orderItems = Ordered.ToArray();

            return Doc;
        }

        public DocumentTypedReference GetStoreReceivingReference(int AID)
        {
            int serial;
            var batchID = _config.BatchIDs.SRV;
            var reference = Helper.GetNextSerialType("SRV", batchID, out serial);
            Helper.UseSerial(new UsedSerial()
            {
                batchID = batchID,
                date = DateTime.Now,
                isVoid = false,
                val = serial,
                note = "Store Receiving Voucher"
            }, AID);
            return reference;
        }


    }
}
