﻿
using BIZNET.iERP;
using INTAPS.Accounting;
using PWF.data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Store.Models
{
    public class ItemViewModel
    {
        public ItemCategory Category { get; set; }
        public List<TransactionItems> Items { get; set; }
    }

    public class ItemRequestModel
    {
        public string code { get; set; }
        public string name { get; set; }
        public string quantity { get; set; }
        public double balance { get; set; }
        public double unitPrice { get; set; }


    }


    public class StoreRequestModel
    {
        public string requestName { get; set; }
        public string requestDescription { get; set; }
        public DocumentTypedReference RequestNo { get; set; }
        public DateTime RequestDate { get; set; }
        public ItemRequestModel[] Approved { get; set; }

        public ItemRequestModel[] Requested { get; set; }
        public ItemRequestModel[] Reorganized { get; set; }
        public List<StoreIssueDocument> Issued { get; set; }

        public CloseSotreRequestModel Closed {get; set;}

    }

    
    public class CloseSotreRequestModel
    {
        public Document Voucher { get; set; }
        public string Description { get; set; }
    }


    public class ItemAccountBalance
    {
        public string code { get; set; }
        public double totalQuantity { get; set; }
        public double totalValue { get; set; }

    }
}
