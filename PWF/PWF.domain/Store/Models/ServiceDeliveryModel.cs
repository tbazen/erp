﻿using BIZNET.iERP;
using INTAPS.Accounting;
using PWF.data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Store.Models
{
    public class ServiceDeliveryModel
    {
        public int ApproverID { get; set; }
        public Guid WorkflowID { get; set; }
        public List<Payment.Models.ItemRequestModel> OrderedItems { get; set; }
        public List<Payment.Models.ItemRequestModel> ApprovedItems { get; set; }
        public Document Document { get; set; }
        public string Note { get; set; }
        public int AccountDocumentID { get; set; }
        public TradeRelation Supplier { get; set; }
        public int CurrentState { get; set; }
        public DocumentTypedReference Voucher { get; set; }
    }
}
