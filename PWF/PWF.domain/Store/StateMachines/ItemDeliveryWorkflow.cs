﻿using Newtonsoft.Json;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Admin;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.PurchaseOrder.Models;
using PWF.domain.Store.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using Stateless;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Store.StateMachines
{
    public class ItemDeliveryWorkflow : PWFService, IBaseStateMachine
    {
        public WorkflowTypes type = WorkflowTypes.StoreDeliveryWorkflow;

        public enum States
        {
            Filing = 0,
            Review = 1,
            Voucher = 2,
            Close = -1,
            Aborted = -2,
        }

        public enum Triggers
        {
            Request = 1,
            Deliver = 2,
            SubmitVoucher = 3,
            Abort = 4,
        }

        private readonly IWorkflowService _workflowService;
        private UserSession _session;

        private StateMachine<States, Triggers> _machine;

        public ItemDeliveryWorkflow()
        {
            _workflowService = new WorkflowService();
        }

        public Workflow Workflow { get; set; }

        public override void SetContext(PWFContext value)
        {
            Context = value;
            _workflowService.SetContext(Context);
            base.SetContext(value); 
        }

        public void ConfigureMachine(string description, int? EmployeeID, Guid? Guid, string observer = "")
        {
            Workflow = _workflowService.CreateWorkflow(new Workflows.Models.WorkflowRequest
            {
                CurrentState = (int)States.Filing,
                Description = description,
                TypeId = (int)type,
                initiatorUser = _session.Username,
                EmployeeID = EmployeeID,
                Observer = observer,
                parentWFID = Guid
            });
            _machine = new StateMachine<States, Triggers>(States.Filing);
            DefineStateMachines();
        }

        public void ConfigureMachine(Guid workflowId)
        {
            Workflow = Context.Workflow.First(wf => wf.Id == workflowId && wf.Typeid == (int)type);
            _machine = new StateMachine<States, Triggers>((States)Workflow.Currentstate);
            DefineStateMachines();
        }

        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
            _session = session;
        }

        public long getInitialUser()
        {
            var user = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(Workflow.Initiatoruser);
            return (long)user;
        }

        public void DefineStateMachines()
        {
            ParametrizedTriggers.ConfigureParameters(_machine);

            _machine.Configure(States.Filing)
                .Permit(Triggers.Abort, States.Aborted)
                .Permit(Triggers.Request, States.Review);

            _machine.Configure(States.Review)
                .Permit(Triggers.Deliver, States.Voucher)
                .Permit(Triggers.Abort, States.Aborted)
                .OnEntryFrom(ParametrizedTriggers.Request, OnRequest);

            _machine.Configure(States.Voucher)
                .Permit(Triggers.SubmitVoucher, States.Close)
                .OnEntryFrom(ParametrizedTriggers.Deliver, OnDeliver);

            _machine.Configure(States.Close)
                .OnEntryFrom(ParametrizedTriggers.SubmitVoucher, OnSubmitVoucher);

            _machine.Configure(States.Aborted)
             .OnEntryFrom(ParametrizedTriggers.Abort, onAbort);

        }


        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<string> trigger, string description)
        {

            _machine.Fire(trigger, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<ItemDeliveryModel, string> trigger,
            ItemDeliveryModel data, string description)
        {
            _machine.Fire(trigger, data, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);

        }

        public void OnRequest(ItemDeliveryModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.StoreKeeper, Model, description, null, transition);
        }

        public void OnDeliver(ItemDeliveryModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.StoreKeeper, Model, description, null, transition);
        }

        public void OnSubmitVoucher(ItemDeliveryModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, Model, description, null, transition);
        }
        public void onAbort(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public ItemDeliveryModel GetData()
        {
            var workItem = Context.WorkItem.Where(w => w.WorkflowId == Workflow.Id).OrderBy(wi => wi.Seqno).
    LastOrDefault();
            return workItem != null ? JsonConvert.DeserializeObject<ItemDeliveryModel>(workItem.Data, new JsonSerializerSettings()
            {
                ContractResolver = new IgnoreIFormFileResolver()
            }) : null;
        }

        public void ConfigureAndAddWorkItem(long? role, ItemDeliveryModel data, string description, long? assignedUser,
    StateMachine<States, Triggers>.Transition transition)
        {
            _workflowService.CreateWorkItem(new WorkItemRequest
            {
                WorkflowId = Workflow.Id.ToString(),
                FromState = (int)transition.Source,
                Trigger = (int)transition.Trigger,
                DataType = typeof(ItemDeliveryModel).ToString(),
                Data = data != null ? JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
                {
                    ContractResolver = new IgnoreIFormFileResolver()
                }) : null,
                Description = description,
                AssignedRole = role,
                AssignedUser = assignedUser
            });
            if (!String.IsNullOrWhiteSpace(Workflow.Observer))
            {
                var observer = Workflow.Observer;
                var type = Type.GetType(observer);
                var wf = (IBaseStateMachine)Activator.CreateInstance(type);
                wf.SetContext(Context);
                wf.SetSession(_session);
                wf.ConfigureMachine((Guid)Workflow.Parentwfid);
                wf.OnWorkItemAdded(Workflow.Id);
            }
        }


        public void OnWorkItemAdded(Guid wfid)
        {
            throw new NotImplementedException();
        }

        public static class ParametrizedTriggers
        {
            public static StateMachine<States, Triggers>.TriggerWithParameters<ItemDeliveryModel, string> Request;
            public static StateMachine<States, Triggers>.TriggerWithParameters<ItemDeliveryModel, string> Deliver;
            public static StateMachine<States, Triggers>.TriggerWithParameters<ItemDeliveryModel, string> SubmitVoucher;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Abort;
            public static void ConfigureParameters(StateMachine<States,Triggers> machine)
            {
                Request = machine.SetTriggerParameters<ItemDeliveryModel, string>(Triggers.Request);
                Deliver = machine.SetTriggerParameters<ItemDeliveryModel, string>(Triggers.Deliver);
                SubmitVoucher = machine.SetTriggerParameters<ItemDeliveryModel, string>(Triggers.SubmitVoucher);
                Abort = machine.SetTriggerParameters<string>(Triggers.Abort);
            }
        }
    }

}
