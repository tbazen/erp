﻿using Newtonsoft.Json;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Admin;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Store.Models;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using Stateless;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Store.StateMachines
{
    public class ServiceDeliveryWorkflow : PWFService, IBaseStateMachine
    {
        public WorkflowTypes type = WorkflowTypes.ServiceDeliveryWorkflow;

        public enum States
        {
            Filing = 0,
            Review = 1,
            Close = -1,
            Cancelled = -2,
            Aborted = -3,
        }

        public enum Triggers
        {
            Request = 1,
            Approve = 2,
            Reject = 3,
            Abort = 4,
        }

        private readonly IWorkflowService _workflowService;
        private UserSession _session;

        private StateMachine<States, Triggers> _machine;

        public ServiceDeliveryWorkflow()
        {
            _workflowService = new WorkflowService();
        }

        public Workflow Workflow { get; set; }

        public override void SetContext(PWFContext value)
        {
            Context = value;
            _workflowService.SetContext(Context);
            base.SetContext(value);
        }

        public void ConfigureMachine(string description, int? EmployeeID, Guid? Guid, string observer = "")
        {
            Workflow = _workflowService.CreateWorkflow(new Workflows.Models.WorkflowRequest
            {
                CurrentState = (int)States.Filing,
                Description = description,
                TypeId = (int)type,
                initiatorUser = _session.Username,
                EmployeeID = EmployeeID,
                Observer = observer,
                parentWFID = Guid
            });
            _machine = new StateMachine<States, Triggers>(States.Filing);
            DefineStateMachines();
        }

        public void ConfigureMachine(Guid workflowId)
        {
            Workflow = Context.Workflow.First(wf => wf.Id == workflowId && wf.Typeid == (int)type);
            _machine = new StateMachine<States, Triggers>((States)Workflow.Currentstate);
            DefineStateMachines();
        }

        public void SetSession(UserSession session)
        {
            _workflowService.SetSession(session);
            _session = session;
        }

        public long getInitialUser()
        {
            var user = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(Workflow.Initiatoruser);
            return (long)user;
        }

        public void DefineStateMachines()
        {
            ParametrizedTriggers.ConfigureParameters(_machine);

            _machine.Configure(States.Filing)
                .Permit(Triggers.Abort,States.Aborted)
                .Permit(Triggers.Request, States.Review);

            _machine.Configure(States.Review)
                .Permit(Triggers.Approve, States.Close)
                .Permit(Triggers.Reject, States.Cancelled)
                .Permit(Triggers.Abort, States.Aborted)
                .OnEntryFrom(ParametrizedTriggers.Request, OnRequest);

            _machine.Configure(States.Close)
                .OnEntryFrom(ParametrizedTriggers.Approve, OnDeliver);

            _machine.Configure(States.Cancelled)
                .OnEntryFrom(ParametrizedTriggers.Reject, OnReject);

            _machine.Configure(States.Aborted)
                .OnEntryFrom(ParametrizedTriggers.Abort, onAbort);
        }


        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<string> trigger, string description)
        {

            _machine.Fire(trigger, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<ServiceDeliveryModel, string> trigger,
            ServiceDeliveryModel data, string description)
        {
            _machine.Fire(trigger, data, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);

        }

        public void OnRequest(ServiceDeliveryModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, Model, description, Model.ApproverID, transition);
        }

        public void OnDeliver(ServiceDeliveryModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, Model, description, null, transition);
        }

        public void OnReject(ServiceDeliveryModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, Model, description, null, transition);
        }
        public void onAbort(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, null, transition);
        }

        public ServiceDeliveryModel GetData()
        {
            var workItem = Context.WorkItem.Where(w => w.WorkflowId == Workflow.Id).OrderBy(wi => wi.Seqno).
    LastOrDefault();
            return workItem != null ? JsonConvert.DeserializeObject<ServiceDeliveryModel>(workItem.Data, new JsonSerializerSettings()
            {
                ContractResolver = new IgnoreIFormFileResolver()
            }) : null;
        }

        public void ConfigureAndAddWorkItem(long? role, ServiceDeliveryModel data, string description, long? assignedUser,
    StateMachine<States, Triggers>.Transition transition)
        {
            _workflowService.CreateWorkItem(new WorkItemRequest
            {
                WorkflowId = Workflow.Id.ToString(),
                FromState = (int)transition.Source,
                Trigger = (int)transition.Trigger,
                DataType = typeof(ServiceDeliveryModel).ToString(),
                Data = data != null ? JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
                {
                    ContractResolver = new IgnoreIFormFileResolver()
                }) : null,
                Description = description,
                AssignedRole = role,
                AssignedUser = assignedUser
            });
            if (!String.IsNullOrWhiteSpace(Workflow.Observer))
            {
                var observer = Workflow.Observer;
                var type = Type.GetType(observer);
                var wf = (IBaseStateMachine)Activator.CreateInstance(type);
                wf.SetContext(Context);
                wf.SetSession(_session);
                wf.ConfigureMachine((Guid)Workflow.Parentwfid);
                wf.OnWorkItemAdded(Workflow.Id);
            }
        }


        public void OnWorkItemAdded(Guid wfid)
        {
            throw new NotImplementedException();
        }

        public static class ParametrizedTriggers
        {
            public static StateMachine<States, Triggers>.TriggerWithParameters<ServiceDeliveryModel, string> Request;
            public static StateMachine<States, Triggers>.TriggerWithParameters<ServiceDeliveryModel, string> Approve;
            public static StateMachine<States, Triggers>.TriggerWithParameters<ServiceDeliveryModel, string> Reject;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Abort;

            public static void ConfigureParameters(StateMachine<States, Triggers> machine)
            {
                Request = machine.SetTriggerParameters<ServiceDeliveryModel, string>(Triggers.Request);
                Approve = machine.SetTriggerParameters<ServiceDeliveryModel, string>(Triggers.Approve);
                Reject = machine.SetTriggerParameters<ServiceDeliveryModel, string>(Triggers.Reject);
                Abort = machine.SetTriggerParameters<string>(Triggers.Abort);

            }
        }
    }

}
