﻿using PWF.domain.Workflows;
using System;
using System.Collections.Generic;
using System.Text;
using Stateless;
using PWF.domain.Infrastructure;
using PWF.data.Entities;
using PWF.domain.Infrastructure.Architecture;
using PWF.data;
using PWF.domain.Store.Models;
using System.Linq;
using Newtonsoft.Json;
using PWF.domain.Workflows.Models;
using PWF.domain.Admin;

namespace PWF.domain.Store.StateMachines
{
    public class StoreRequestWorkflow : PWFService
    {
        public WorkflowTypes type = WorkflowTypes.StoreRequestWorkflow;

        public enum States
        {
            Filing = 0,
            Reorganize = 1,
            Reviewing = 2,
            Issuance = 3,
            Final = -1,
            Cancelled = -2
        }

        public enum Triggers
        {
            Cancel = 1,
            File = 2,
            Reorganize = 3,

            
            Approve = 4,
            Reject = 5,
            Issue = 6,
            Close = 7,

            CancelReorganization = 8,

        }

        private readonly IWorkflowService _workflowService;
        private IStoreRequestService _service;

        private StateMachine<States, Triggers> _machine;

        public StoreRequestWorkflow(UserSession session,IStoreRequestService service,PWFConfiguration Config)
        {
            _workflowService = new WorkflowService();
            _workflowService.SetSession(session);
            _service = service;
        }

        public Workflow Workflow { get; set; }

        public override void SetContext(PWFContext value)
        {
            Context = value;
            _service.SetContext(value);
            _workflowService.SetContext(Context);

        }

        public void ConfigureMachine(string description, int? EmpID)
        {
            Workflow = _workflowService.CreateWorkflow(new Workflows.Models.WorkflowRequest
            {
                CurrentState = (int)States.Filing,
                Description = description,
                TypeId = (int)type,
                EmployeeID = EmpID
            });
            _machine = new StateMachine<States, Triggers>(States.Filing);
            DefineStateMachines();
        }

        public void ConfigureMachine(Guid workflowId)
        {
            Workflow = Context.Workflow.First(wf => wf.Id == workflowId && wf.Typeid == (int)type);
            _machine = new StateMachine<States, Triggers>((States)Workflow.Currentstate);
            DefineStateMachines();
        }

        

        public long getInitialUser()
        {
            //long user = Context.User.Where(m => m.Username == Workflow.initiatorUser).First().Id;
            //return user;
            var user = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUIDForName(Workflow.Initiatoruser);
            return (long)user;
        }


        public void DefineStateMachines()
        {
            ParametrizedTriggers.ConfigureParameters(_machine);

            _machine.Configure(States.Filing)
            .Permit(Triggers.Cancel, States.Cancelled)
            .Permit(Triggers.File, States.Reorganize)
            .OnEntryFrom(ParametrizedTriggers.Reject, OnReject)
            .OnEntryFrom(ParametrizedTriggers.CancelReorganization,OnCancelReorganization);

            _machine.Configure(States.Cancelled)
            .OnEntryFrom(ParametrizedTriggers.Cancel, OnCancelled);

            _machine.Configure(States.Reorganize)
            .Permit(Triggers.Reorganize, States.Reviewing)
            .Permit(Triggers.CancelReorganization, States.Filing)
            .OnEntryFrom(ParametrizedTriggers.File, OnFile);


            _machine.Configure(States.Reviewing)
            .Permit(Triggers.Approve, States.Issuance)
            .Permit(Triggers.Reject, States.Filing)
            .OnEntryFrom(ParametrizedTriggers.Reorganize, OnReorganize);

            _machine.Configure(States.Issuance)
                .OnEntryFrom(ParametrizedTriggers.Approve, OnApprove)
                .PermitReentry(Triggers.Issue)
                .Permit(Triggers.Close, States.Final)
                .OnEntryFrom(ParametrizedTriggers.Issue, OnIssue);

            _machine.Configure(States.Final)
                .OnEntryFrom(ParametrizedTriggers.Close, OnClose);

                


        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<string> trigger,
            string description)
        {

            _machine.Fire(trigger, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);
        }

        public void Fire(Guid workflowId, StateMachine<States, Triggers>.TriggerWithParameters<StoreRequestModel, string> trigger,
            StoreRequestModel data, string description)
        {
            _machine.Fire(trigger, data, description);
            _workflowService.UpdateWorkflow(workflowId, (int)_machine.State, description);

        }

        public void OnReject(string description, StateMachine<States,Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, getInitialUser(), transition);
        }

        public void OnCancelReorganization(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, GetData(), description, getInitialUser(), transition);
        }


        public void OnFile(StoreRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.StoreKeeper, Model, description, getInitialUser(), transition);
        }

        public void OnReorganize(StoreRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.StoreManager, Model, description, getInitialUser(), transition);
        }

        public void OnApprove(StoreRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.StoreKeeper,Model, description, getInitialUser(), transition);
        }

        public void OnIssue(StoreRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem((long)UserRoles.StoreKeeper, Model, description, getInitialUser(), transition);
        }

        public void OnCancelled(string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null,GetData(), description, null, transition);
        }

        public void OnClose(StoreRequestModel Model, string description, StateMachine<States, Triggers>.Transition transition)
        {
            ConfigureAndAddWorkItem(null, Model, description, null, transition);
        }

        public StoreRequestModel GetData()
        {
            var workItem = Context.WorkItem.Where(w => w.WorkflowId == Workflow.Id).OrderBy(wi => wi.Seqno).
                LastOrDefault();
            return workItem != null ? JsonConvert.DeserializeObject<StoreRequestModel>(workItem.Data, new JsonSerializerSettings()
            {
                ContractResolver = new IgnoreIFormFileResolver()
            }) : null;
        }

        public void ConfigureAndAddWorkItem(long? role, StoreRequestModel data, string description, long? assignedUser,
            StateMachine<States, Triggers>.Transition transition)
        {
            _workflowService.CreateWorkItem(new WorkItemRequest
            {
                WorkflowId = Workflow.Id.ToString(),
                FromState = (int)transition.Source,
                Trigger = (int)transition.Trigger,
                DataType = typeof(StoreRequestModel).ToString(),
                Data = data != null ? JsonConvert.SerializeObject(data, Formatting.None, new JsonSerializerSettings()
                {
                    ContractResolver = new IgnoreIFormFileResolver()
                }) : null,
                Description = description,
                AssignedRole = role,
                AssignedUser = assignedUser
            });
        }


        public static class ParametrizedTriggers
        {
            public static StateMachine<States, Triggers>.TriggerWithParameters<StoreRequestModel, string> File;
            public static StateMachine<States, Triggers>.TriggerWithParameters<StoreRequestModel, string> Reorganize;
            public static StateMachine<States, Triggers>.TriggerWithParameters<StoreRequestModel,string> Approve;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Reject;
            public static StateMachine<States, Triggers>.TriggerWithParameters<StoreRequestModel, string> Issue;
            public static StateMachine<States, Triggers>.TriggerWithParameters<string> Cancel;
            public static StateMachine<States, Triggers>.TriggerWithParameters<StoreRequestModel, string> Close;

            public static StateMachine<States, Triggers>.TriggerWithParameters<string> CancelReorganization;


            public static void ConfigureParameters(StateMachine<States,Triggers> machine)
            {
                File = machine.SetTriggerParameters<StoreRequestModel, string>(Triggers.File);
                Reorganize = machine.SetTriggerParameters<StoreRequestModel, string>(Triggers.Reorganize);
                Approve = machine.SetTriggerParameters<StoreRequestModel,string>(Triggers.Approve);
                Reject = machine.SetTriggerParameters<string>(Triggers.Reject);
                Issue = machine.SetTriggerParameters<StoreRequestModel, string>( Triggers.Issue);
                Cancel = machine.SetTriggerParameters<string>(Triggers.Cancel);
                Close = machine.SetTriggerParameters<StoreRequestModel, string>(Triggers.Close);
                CancelReorganization = machine.SetTriggerParameters<string>(Triggers.CancelReorganization);
            }

        }

    }





}
