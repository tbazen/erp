﻿using BIZNET.iERP;
using INTAPS.Accounting;
using Microsoft.AspNetCore.Http;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Documents;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Store.Models;
using PWF.domain.Store.StateMachines;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PWF.domain.Store
{
    public interface IStoreRequestFacade : IPWFFacade
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        List<ItemViewModel> GetAllItems();
        Guid FileNewStoreRequest(StoreRequestModel Model, string description = "File new store request");
        void CancelStoreRequest(Guid WorkflowId, string descripiton = "Cancel Store Request");
        void ReorganizeStoreRequest(Guid WorkflowId, StoreRequestModel Model, string description = "Reorganize Store Request");
        void ApproveStoreRequest(Guid WorkflowId,ItemRequestModel[] Model, string description = "Approve Store Request");
        void RejectStoreRequest(Guid WorkflowId, string description = "Reject Store Request");

        void ResubmitStoreRequest(Guid WorkflowId, StoreRequestModel model, string description = "Resubmit Store Request");
        StoreInfo[] GetAllStores();
        CostCenter[] GetAllCostCenters(int parentID);
        Account[] GetAllChildAccounts(int parentID);
        MeasureUnit[] GetAllMeasureUnits();

        List<TransactionItems> SearchTransactionItems(string term);
        int RegisterItemCategory(ItemCategory Category);
        int RegisterItem(TransactionItems Item);
        List<FixedAssetRuleAttribute> GetFixedAssetRuleAttributes();
        DocumentSerialType[] GetAllDocumentSerialTypes();

        APIResponseModel PostStoreIssueDocument(StoreIssueDocument Doc);

        StoreRequestModel IssueStoreRequest(Guid WorkflowId, StoreRequestModel model, string descripiton = "Issued Items");
        void CancelRequestReorganizing(Guid WorkflowId, string descripition = "Request Cancelled");

        string getUserNameWithLoginName(string username);
        Dictionary<string, string> GetStoreIssueDocumentSigner(Guid wfid);
        bool CloseStoreRequest(Guid wfid, CloseSotreRequestModel Model);
    }

    public class StoreRequestFacade : PWFFacade, IStoreRequestFacade
    {
        private readonly IStoreRequestService _service;
        public IDocumentService _documentService; 
        private PWFConfiguration _Config;
        private UserSession _session;

        public StoreRequestFacade(PWFContext Context,IStoreRequestService StoreRequest,IDocumentService docService) : base(Context)
        {
            _service = StoreRequest;
            _context = Context;
            _documentService = docService;
            _service.SetContext(Context);
            _documentService.SetContext(Context);
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _service.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _service.SetSessionConfiguration(Config, session);
            _documentService.SetSessionConfiguration(Config, session);
            _session = session;
            _Config = Config;
        }

        public List<ItemViewModel> GetAllItems()
        {
            return _service.GetAllItems();
        }

        public DocumentTypedReference GetStoreRequisitionNumber(int AID)
        {
            int serial;
            var batchID = _Config.BatchIDs.SRN;
            var reference = Helper.GetNextSerialType("SRN", batchID, out serial);
            Helper.UseSerial(new UsedSerial()
            {
                batchID = batchID,
                date = DateTime.Now,
                isVoid = false,
                val = serial,
                note = "Store Requisition Number"
            }, AID);
            return reference;
        }

        public Guid FileNewStoreRequest(StoreRequestModel Model,string description = "File new store request")
        {
            return TransactWIthWSIS( (t,AID) =>
           {
               var reference = GetStoreRequisitionNumber(AID);
               Model.RequestNo = reference;
               Model.RequestDate = DateTime.Now;
               var workflow = new StoreRequestWorkflow(_session, _service, _Config);
               PassContext(workflow, _context);
               workflow.ConfigureMachine(description,null);
               var workflowId = workflow.Workflow.Id;
               workflow.Fire(workflowId, StoreRequestWorkflow.ParametrizedTriggers.File, Model, description);
               return workflowId;
           }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWf_File New Store Request", _session);
        }

        public void CancelStoreRequest(Guid WorkflowId,string descripiton = "Cancel Store Request")
        {
            Transact(t =>
            {
                var workflow = new StoreRequestWorkflow(_session, _service, _Config);
                
                PassContext(workflow, _context);
                workflow.ConfigureMachine(WorkflowId);
                workflow.Fire(workflow.Workflow.Id, StoreRequestWorkflow.ParametrizedTriggers.Cancel, descripiton);
            });
        }

        public void ReorganizeStoreRequest(Guid WorkflowId, StoreRequestModel Model,string description="Reorganize Store Request")
        {
            Transact(t =>
            {
                var workflow = new StoreRequestWorkflow(_session, _service, _Config);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(WorkflowId);
                var workflowId = workflow.Workflow.Id;
                var data = workflow.GetData();
                data.Reorganized = Model.Reorganized;
                workflow.Fire(workflowId, StoreRequestWorkflow.ParametrizedTriggers.Reorganize, data, description);
                return workflowId;
            });
        }

        public void ResubmitStoreRequest(Guid WorkflowId, StoreRequestModel model, string description = "Resubmit Store Request")
        {
            Transact(t => {
               var workflow = new StoreRequestWorkflow(_session,_service,_Config);
               PassContext(workflow,_context);
               workflow.ConfigureMachine(WorkflowId);
               var workflowId = workflow.Workflow.Id;
               workflow.Fire(workflowId,StoreRequestWorkflow.ParametrizedTriggers.File,model,description);
               return workflowId; 
            });
        }

        public StoreRequestModel IssueStoreRequest(Guid WorkflowId, StoreRequestModel model, string descripiton = "Issued Items")
        {
            return Transact(t => {
                var workflow = new StoreRequestWorkflow(_session,_service,_Config);
                PassContext(workflow,_context);
                workflow.ConfigureMachine(WorkflowId);
                var workflowId = workflow.Workflow.Id;
                workflow.Fire(workflowId,StoreRequestWorkflow.ParametrizedTriggers.Issue,model,descripiton);
                return model;
            });
        }



        public void ApproveStoreRequest(Guid WorkflowId, ItemRequestModel[] Items, string descripiton = "Approve Store Request")
        {
            Transact(t =>
            {
                var workflow = new StoreRequestWorkflow(_session, _service, _Config);

                PassContext(workflow, _context);
                workflow.ConfigureMachine(WorkflowId);
                var data = workflow.GetData();
                data.Approved = Items;
                workflow.Fire(workflow.Workflow.Id, StoreRequestWorkflow.ParametrizedTriggers.Approve,data, descripiton);
            });
        }

        public void RejectStoreRequest(Guid WorkflowId, string description = "Reject Store Request")
        {
            Transact(t =>
            {
                var workflow = new StoreRequestWorkflow(_session, _service, _Config);

                PassContext(workflow, _context);
                workflow.ConfigureMachine(WorkflowId);
                workflow.Fire(workflow.Workflow.Id, StoreRequestWorkflow.ParametrizedTriggers.Reject, description);
            });
        }

        public void CancelRequestReorganizing(Guid WorkflowId, string descripition = "Request Cancelled"){
            Transact(t =>
            {
                var workflow = new StoreRequestWorkflow(_session, _service, _Config);

                PassContext(workflow, _context);
                workflow.ConfigureMachine(WorkflowId);
                workflow.Fire(workflow.Workflow.Id, StoreRequestWorkflow.ParametrizedTriggers.CancelReorganization, descripition);
            });
        }

       public bool CloseStoreRequest(Guid wfid, CloseSotreRequestModel Model)
        {
            return Transact(t => {
                var workflow = new StoreRequestWorkflow(_session, _service, _Config);
                PassContext(workflow, _context);
                workflow.ConfigureMachine(wfid);
                var data = workflow.GetData();
                var doc = Model.Voucher;
                doc.Documenttypeid = (int)DocumentTypes.StoreIssueDocument;
                Model.Voucher = _documentService.AddDocument(doc);
                data.Closed = Model;
               workflow.Fire(wfid, StoreRequestWorkflow.ParametrizedTriggers.Close, data, Model.Description);
               return true;
            });
        }

        public int RegisterItemCategory(ItemCategory category){
            return TransactWIthWSIS((t, AID) => {
                PassContext(_service,_context);
                var result = _service.RegisterItemCategory(category,AID);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF_RegisterItemCategory", _session);
        }

        public int RegisterItem(TransactionItems items){
            return TransactWIthWSIS( (t,AID) => {
                PassContext(_service,_context);
                var result = _service.RegisterItem(items,AID);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWf_RegisterItem", _session);
        }

        public List<TransactionItems> SearchTransactionItems(string term){
            return Transact(t => {
                PassContext(_service,_context);
                var result = _service.SearchTransactionItems(term);
                return result;
            });
        }

        public StoreInfo[] GetAllStores(){
            return Transact(t =>
            {
                PassContext(_service, _context);
                var result = _service.GetAllStores();
                return result;
            });
        }

        public CostCenter[] GetAllCostCenters(int parentID)
        {
            return Transact(t =>
            {
                PassContext(_service, _context);
                var result = _service.GetAllCostCenters(parentID);
                return result;
            });
        }

        public Account[] GetAllChildAccounts(int parentID)
        {
            return Transact(t =>
            {
                PassContext(_service, _context);
                var result = _service.GetAllChildAccounts(parentID);
                return result;
            });
        }

        public MeasureUnit[] GetAllMeasureUnits()
        {
            return Transact(t =>
            {
                PassContext(_service, _context);
                var result = _service.GetAllMeasureUnits();
                return result;
            });
        }

        public List<FixedAssetRuleAttribute> GetFixedAssetRuleAttributes(){
            return Transact(t => {
                PassContext(_service,_context);
                var result = _service.GetFixedAssetRuleAttributes();
                return result;
            });
        }

        public DocumentSerialType[] GetAllDocumentSerialTypes(){
            return Transact( t=> {
                PassContext(_service,_context);
                var result = _service.GetAllDocumentSerialTypes();
                return result;
            });
        }

        public APIResponseModel PostStoreIssueDocument(StoreIssueDocument Doc){
            return TransactWIthWSIS( (t,AID) => {
                PassContext(_service,_context);
                var result = _service.PostStoreIssueDocument(Doc,AID);
                return result;
            }, INTAPS.ClientServer.ApplicationServer.SecurityBDE.WriterHelper, "PWF_PostStoreIssueDocument", _session);
        }

        public string getUserNameWithLoginName(string username)
        {
            return Transact(t =>
            {
                PassContext(_service, _context);
                var name =_service.getUserNameWithLoginName(username);
                return name;
            });
        }

        public Dictionary<string, string> GetStoreIssueDocumentSigner(Guid wfid)
        {
            return Transact(t =>
            {
                PassContext(_service, _context);
                var result =_service.GetStoreIssueDocumentSigner(wfid);
                return result;
            });
        }
    }
}
