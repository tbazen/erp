﻿using PWF.data;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Store.Models;
using System;
using System.Collections.Generic;
using System.Text;
using PWF.domain;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;

using NinjaNye.SearchExtensions;
using PWF.domain.Workflows;
using PWF.domain.Workflows.Models;
using System.Linq;
using Microsoft.AspNetCore.Http;
using PWF.data.Entities;
using System.IO;
using PWF.domain.Documents;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.Payroll.BDE;

namespace PWF.domain.Store
{
    public interface IStoreRequestService : IPWFService
    {
        void SetSession(UserSession session);
        void SetSessionConfiguration(PWFConfiguration Config, UserSession session);
        List<ItemViewModel> GetAllItems();
        List<TransactionItems> GetTransactionItems();
        EnumerableStringSearch<TransactionItems> Search(string term);
        List<ItemCategory> GetItemCategories();
        List<TransactionItems> GetItemsInCategory(int catID);
        int RegisterItemCategory(ItemCategory Categor,int AID);
        int RegisterItem(TransactionItems Item,int AID);
        ItemAccountBalance GetAccountBalance(string itemCode, int costCenterID, DateTime time);
        DocumentTypedReference GetDocumentTypedReference(int serialTypeID, string referenceText, bool primary);
        APIResponseModel PostStoreIssueDocument(StoreIssueDocument Doc,int AID);
        StoreInfo[] GetAllStores();
        CostCenter[] GetAllCostCenters(int parentID);
        Account[] GetAllChildAccounts(int parentID);
        MeasureUnit[] GetAllMeasureUnits();
        StoreInfo GetStore(int costCenterID);
        List<FixedAssetRuleAttribute> GetFixedAssetRuleAttributes();
        List<TransactionItems> SearchStoreItems(string term);
        DocumentSerialType[] GetAllDocumentSerialTypes();
        List<TransactionItems> SearchTransactionItems(string term);
        string getUserNameWithLoginName(string username);
        Dictionary<string, string> GetStoreIssueDocumentSigner(Guid wfid);
        CloseSotreRequestModel CreateCloseStoreRequestModel(List<IFormFile> files, string description, Guid wfid);
        string[][] GetGRVDocumentSigners(Guid wfid);

        string getUserNameWithID(int userID);
    };

    public class StoreRequestService : PWFService, IStoreRequestService
    {
        private PWFContext _context;
        private UserSession _session;
        private string _url;
        private string _sessionID;
        private IMemoryCache _cache;
        private PWFConfiguration _config;

        private IWorkflowService _wfService;
        private IUserActionService _userActionService;
        private IDocumentService _documentService;
        private iERPTransactionBDE _IERPService;
        private AccountingBDE _AccountingService;
        private SessionObjectBase<iERPTransactionBDE> _ierpSession;
        public StoreRequestService(PWFContext Context, IMemoryCache memoryCache, IWorkflowService wfService, IUserActionService actionSercice,IDocumentService docService)
        {
            _context = Context;
            _cache = memoryCache;
            _wfService = wfService;
            _wfService.SetContext(Context);
            _userActionService = actionSercice;
            _documentService = docService;
            _IERPService = INTAPS.ClientServer.ApplicationServer.GetBDE<iERPTransactionBDE>();
            _AccountingService = INTAPS.ClientServer.ApplicationServer.GetBDE<AccountingBDE>();

            
        }

        public void SetSession(UserSession session)
        {
            _session = session;
            _sessionID = session.payrollSessionID;
            _wfService.SetSession(session);
            _documentService.SetSession(session);
        }

        public void SetSessionConfiguration(PWFConfiguration Config, UserSession session)
        {
            _sessionID = session.payrollSessionID;
            _session = session;
            _url = Config.Host;
            _config = Config;
            _documentService.SetSessionConfiguration(Config, session);
        }

        public List<ItemViewModel> GetAllItems()
        {

            List<ItemViewModel> Model = new List<ItemViewModel>();
            var categories = _IERPService.GetItemCategories(-1);
            
            foreach (var cat in categories)
            {
                var items = _IERPService.GetItemsInCategory(cat.ID);
                if (items.Length > 0)
                {
                    Model.Add(new ItemViewModel()
                    {
                        Category = cat,
                        Items = items.ToList()
                    });
                }
            }
            return Model;
        }

        public List<TransactionItems> GetTransactionItems()
        {

            var items = GetAllItems();
            List<TransactionItems> list = new List<TransactionItems>();
            foreach (var i in items)
            {
                list.AddRange(i.Items);
            }
            _cache.Set("TransactionItemsList", list, TimeSpan.FromHours(1));
            return list;

        }



        public List<TransactionItems> SearchTransactionItems(string term)
        {
            string[] Columns = new string[] { "Code" };
            string[] Column2 = new string[] { "Name" };

            object[] criteria = new object[] { term };
            int n;
            var result = _IERPService.SearchTransactionItems(0, -1, criteria, Columns, out n).ToList();
            var result2 = _IERPService.SearchTransactionItems(0, 10, criteria, Column2, out n).ToList();
            List<TransactionItems> 
                Items = new List<TransactionItems>();
            Items.AddRange(result); Items.AddRange(result2);
            return Items;

        }

        public List<TransactionItems> SearchStoreItems(string term)
        {
            string[] Columns = new string[] { "Code" };
            string[] Column2 = new string[] { "Name" };

            object[] criteria = new object[] { term };
            int n;
            var result = _IERPService.SearchTransactionItems(0, -1, criteria, Columns, out n).ToList();
            var result2 = _IERPService.SearchTransactionItems(0, -1, criteria, Column2, out n).ToList();
            List<TransactionItems>
                Items = new List<TransactionItems>();
            Items.AddRange(result); Items.AddRange(result2);
            return Items.Where(m => m.IsInventoryItem == true || m.IsFixedAssetItem == true).OrderBy(m => m.Code).ToList();
        }

        public List<ItemCategory> GetItemCategories()
        {

            var categories = _IERPService.GetAllItemCategories();
            return categories.ToList();
        }

        public List<TransactionItems> GetItemsInCategory(int catID)
        {
            var items = _IERPService.GetItemsInCategory(catID);
            return items.ToList();
        }

        public int RegisterItemCategory(ItemCategory category,int AID)
        {
            lock (_IERPService.WriterHelper)
            {

                    int ID = _IERPService.RegisterItemCategory(AID, category);

                    return ID;

            }

        }

        public int RegisterItem(TransactionItems Item,int AID)
        {

            Item.Activated = true;
            lock (_IERPService.WriterHelper)
            {
                   string code = _IERPService.RegisterTransactionItem(AID, Item, null);
                    
                    return Int32.Parse(code);
                

            }
        }

        public ItemAccountBalance GetAccountBalance(string itemCode, int costCenterID, DateTime time)
        {


            TransactionItems item = _IERPService.GetTransactionItems(itemCode);
            int accountID = item.materialAssetAccountID(true);
            CostCenterAccount csa = _AccountingService.GetCostCenterAccount(costCenterID, accountID);
            if (csa == null)
            {
                return new ItemAccountBalance()
                {
                    code = item.Code,
                    totalQuantity = 0,
                    totalValue = 0
                };
            }
            double totalQuantity = _AccountingService.GetNetBalanceAsOf(csa.id, TransactionItem.MATERIAL_QUANTITY, time);
            double totalValue = _AccountingService.GetNetBalanceAsOf(csa.id, TransactionItem.DEFAULT_CURRENCY, time);
            return (new ItemAccountBalance()
            {
                code = item.Code,
                totalQuantity = totalQuantity,
                totalValue = totalValue
            });

        }

        public DocumentTypedReference GetDocumentTypedReference(int serialTypeID, string referenceText, bool primary)
        {
            DocumentTypedReference ret = new DocumentTypedReference(serialTypeID, referenceText, primary);
            string r;
            var _currentType = _AccountingService.getDocumentSerialType(serialTypeID);
            if (_currentType.serialType == DocumentSerializationMode.Serialized)
            {
                int ser = ret.serial;
                if (ser == -1)
                    r = "";
                else
                    r = _AccountingService.formatSerialNo(_currentType.id, ser);
                ret.reference = r;
            }
            return ret;
        }

        public StoreInfo[] GetAllStores() {

            var stores = _IERPService.GetAllStores();
            return stores;
        }

        public CostCenter[] GetAllCostCenters(int parentID) {

            var centerIDs = _AccountingService.getAllChildAccounts<CostCenter>(parentID);
            List<CostCenter> CostCenters = new List<CostCenter>();
            foreach (var id in centerIDs)
            {
                CostCenters.Add(_AccountingService.GetAccount<CostCenter>(id));
            }
            return CostCenters.ToArray();
        }

        public Account[] GetAllChildAccounts(int parentID) {

            var accounts = _AccountingService.GetChildAllAcccount<Account>(parentID);
            return accounts;
        }

        public MeasureUnit[] GetAllMeasureUnits() {

            var units = _IERPService.GetMeasureUnits();
            return units;
        }

        

        

        public APIResponseModel PostStoreIssueDocument(StoreIssueDocument Doc,int AID)
        {

            try
            {

                var batchID = _config.StoreIssueVoucherBatchID;
                var serial = _AccountingService.GetNextSerial(batchID);
                DocumentSerialType st = _AccountingService.getDocumentSerialType("SIV");
                Doc.voucher = new DocumentTypedReference()
                {
                    primary = true,
                    reference = serial.ToString(st.format),
                    typeID = st.id
                };
                Doc.DocumentDate = DateTime.Now;
                Doc.PaperRef = Doc.voucher.reference;
                int docID = Helper.PostGenericDocument(Doc,AID);
                Helper.UseSerial(new UsedSerial()
                {
                    batchID = batchID,
                    date = Doc.DocumentDate,
                    isVoid = false,
                    val = serial,
                    note = "Store Issue Voucher"

                },AID);
                Doc.AccountDocumentID = docID;

                return new APIResponseModel
                {
                    status = true,
                    response = Doc,
                };
            }
            catch (Exception ex)
            {
                return new APIResponseModel()
                {
                    status = false,
                    error = ex.Message
                };
                throw;
            }
        }

        public EnumerableStringSearch<TransactionItems> Search(string term)
        {
            var items = GetTransactionItems();
            var result = items.Search(x => x.NameCode.ToLower(), x => x.Description.ToLower()).Containing(term.ToLower());
            return result;
        }

        public List<FixedAssetRuleAttribute> GetFixedAssetRuleAttributes() {

            var result = _IERPService.getAllFixedAssetRuleAttributes();
            return result.ToList();
        }

        public DocumentSerialType[] GetAllDocumentSerialTypes() {
            var result = _AccountingService.getAllDocumentSerialTypes();
            return result;
        }

        public string getUserNameWithLoginName(string username)
        {

            PayrollBDE service = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            var emp = service.GetEmployeeByLoginName(username);
            return (emp != null ? emp.employeeName : "");
        }

        public string getUserNameWithID(int userID)
        {
            var username = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUserPermissions(userID).UserName;
            var name = getUserNameWithLoginName(username);
            return name;
        }

        public StoreInfo GetStore(int costCenterID)
        {
            var iERPBDE = ApplicationServer.GetBDE<iERPTransactionBDE>();
            var store = iERPBDE.GetStoreByCostCenterID(costCenterID);
            return store;
        }



        public CloseSotreRequestModel CreateCloseStoreRequestModel(List<IFormFile> files, string description,Guid wfid)
        {
            List<Document> Docs = new List<Document>();
            foreach(var file in files)
            {
                Document doc = new Document()
                {
                    Contentdisposition = file.ContentDisposition,
                    Contenttype = file.ContentType,
                    Documenttypeid = 1,
                    Name = file.Name,
                    Filename = file.FileName,
                    Length = file.Length,
                    WorkflowId = wfid
                };
                MemoryStream ms = new MemoryStream();
                file.CopyToAsync(ms);
                doc.File = ms.ToArray();
                ms.Flush();
                var d = _documentService.AddDocument(doc);
                Docs.Add(doc);
            }
            CloseSotreRequestModel Model = new CloseSotreRequestModel()
            {
               // Document = Docs.ToList().Select(m => m.ID).Distinct().ToArray(),
                Description = description
            };
            return Model;
        }



        public Dictionary<string, string> GetStoreIssueDocumentSigner(Guid wfid)
        {
            var worktiems = _wfService.GetWorkItems(wfid).OrderByDescending(m => m.aid).ToList();
            var filingaid = worktiems.Where(m => m.FromState == 0 && m.Trigger == 2).FirstOrDefault().aid;
            var reviewaid = worktiems.Where(m => m.FromState == 2 && m.Trigger == 4).FirstOrDefault().aid;
            var issueaid = worktiems.Where(m => m.FromState == 3 && m.Trigger == 6).FirstOrDefault().aid;
            var fiiller = getUserNameWithLoginName(_userActionService.GetUserAction((long)filingaid).Username);
            var reviewer = getUserNameWithLoginName(_userActionService.GetUserAction((long)reviewaid).Username);
            var issuer = getUserNameWithLoginName(_userActionService.GetUserAction((long)issueaid).Username);
            Dictionary<string, string> Signers = new Dictionary<string, string>();
            Signers["Filler"] = fiiller;
            Signers["Reiewer"] = reviewer;
            Signers["Issuer"] = issuer;
            return Signers;
            
        }

        public string[][] GetGRVDocumentSigners(Guid wfid)
        {

            var worktiems = _wfService.GetWorkItems(wfid).OrderByDescending(m => m.aid).ToList();
            var purchaseraid = worktiems.Where(m => m.FromState == 0 && m.Trigger == 1).FirstOrDefault().aid;
            var receiveraid = worktiems.Where(m => m.FromState == 1 && m.Trigger == 2).FirstOrDefault().aid;
            var purchaser = getUserNameWithLoginName(_userActionService.GetUserAction((long)purchaseraid).Username);
            var receiver = getUserNameWithLoginName(_userActionService.GetUserAction((long)receiveraid).Username);
            var response = new string[][]
            {
                new string[]{"Purchaser","Received By"},
                new string[]{purchaser, receiver}
            };
            return response;
        }
    }

    public class APIResponseModel {
        public bool status {get; set;}
        public object response {get; set;}

        public string error {get; set; }
    }
}
