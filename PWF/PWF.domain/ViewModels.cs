﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PWF.domain.ViewModels
{
    public class RegisterViewModel
    {
        //[Required]
        //[Display(Name = "Name")]
        //public string Name { get; set; }

        [Display(Name = "User Name")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Employee")]
        public int EmpID { get; set; }

        //[Display(Name = "Employee")]
        //public int EmpID { get; set; }


        //[Required]
        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        public List<SelectListItem> EmployeeSelectList { get; set; }

        public int[] Roles { get; set; }
    }
}
