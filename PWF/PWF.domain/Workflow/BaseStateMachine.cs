﻿using PWF.data;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using System;
using System.Collections.Generic;
using System.Text;

namespace PWF.domain.Workflows
{
    public interface IBaseStateMachine : IPWFService
    {
        void OnWorkItemAdded(Guid wfid);
        void SetSession(UserSession session);
        void ConfigureMachine(string description,int? EmployeeID,Guid? Guid, string observer="");
        void ConfigureMachine(Guid workflowID);
     
    }
}
