﻿using INTAPS.Payroll.BDE;
using PWF.data;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWF.domain.Workflows
{
    public interface IWorkflowHelper : IPWFService
    {
        void SetSession(UserSession session);
        WorkflowHistoryModel GetPaymentWorkflowHistory(Guid wfid);
        Dictionary<string, string> GetPaymentRequestSigners(Guid wfid);
        WorkflowHistoryModel GetWorkflowHistory(Guid wfid, Func<int, int, string> action);
        WorkflowHistoryModel GetPurchaseWorkflowHistory(Guid wfid);
        WorkflowHistoryModel GetPurchaseOrderWorkflowHistory(Guid wfid);
        WorkflowHistoryModel GetStoreRequestWorkflowHistory(Guid wfid);

    }

    public class WorkflowHelper : PWFService, IWorkflowHelper
    {
        private UserSession _session;

        public WorkflowHelper(PWFContext context)
        {
            Context = context;
        }

        public void SetSession(UserSession session)
        {
            _session = session;
        }

        public WorkflowHistoryModel GetWorkflowHistory(Guid wfid,Func<int,int,string> action)
        {
            PayrollBDE service = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            WorkflowHistoryModel Model = new WorkflowHistoryModel();
            Model.Workflow = Context.Workflow.Find(wfid);
            var workitems = Context.WorkItem.Where(m => m.WorkflowId == wfid).OrderBy(m => m.Seqno).ToList();
            List<WorkflowHistoryItem> Items = new List<WorkflowHistoryItem>();
            foreach (var item in workitems)
            {
                WorkflowHistoryItem i = new WorkflowHistoryItem();
                i.workitem = item;
                i.AID = (long)item.Aid;
                i.Action = action(item.Fromstate, item.Trigger);
                i.Description = item.Description;
                i.key = $"{item.Fromstate}{item.Trigger}";
                var log = Context.UserAction.Find(item.Aid);
                i.username = log.Username;
                i.Name = service.GetEmployeeByLoginName(log.Username).employeeName;
                i.Date = new DateTime((long)log.Timestamp);
                Items.Add(i);
            }
            Model.History = Items;
            return Model;
        }

        public WorkflowHistoryModel GetPurchaseWorkflowHistory(Guid wfid)
        {
            var Model = GetWorkflowHistory(wfid, TriggerActionMapper.GetPurchaseAction);
            return Model;

        }

        public WorkflowHistoryModel GetPaymentWorkflowHistory(Guid wfid)
        {
            var Model = GetWorkflowHistory(wfid, TriggerActionMapper.GetPaymentAction);
            return Model;
        }

        public WorkflowHistoryModel GetPurchaseOrderWorkflowHistory(Guid wfid)
        {
            var Model = GetWorkflowHistory(wfid, TriggerActionMapper.GetPurchaseOrderAction);
            return Model;
        }

        public WorkflowHistoryModel GetStoreRequestWorkflowHistory(Guid wfid)
        {
            var Model = GetWorkflowHistory(wfid, TriggerActionMapper.GetStoreRequestAction);
            return Model;
        }

        public Dictionary<string, string> GetPaymentRequestSigners(Guid wfid)
        {
            PayrollBDE service = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
            WorkflowHistoryModel Model = new WorkflowHistoryModel();
            Model.Workflow = Context.Workflow.Find(wfid);
            var workitems = Context.WorkItem.Where(m => m.WorkflowId == wfid).OrderBy(m => m.Seqno).ToList();

            var CheckedBy = workitems.Where(m => m.Fromstate == 1 && m.Trigger == 5).Count() > 0
                        ? workitems.Where(m => m.Fromstate == 1 && m.Trigger == 5).First() : null;
            var CheckedAndApproved = workitems.Where(m => m.Fromstate == 1 && m.Trigger == 6).Count() > 0
                            ? workitems.Where(m => m.Fromstate == 1 && m.Trigger == 6).First() : null;
            var ApprovedBy = workitems.Where(m => m.Fromstate == 2 && m.Trigger == 7).Count() > 0 ?
                                workitems.Where(m => m.Fromstate == 2 && m.Trigger == 7).First() : null;
            var ExecutedBy = workitems.Where(m => m.Fromstate == 3 && m.Trigger == 2).First();
            Dictionary<string, string> Signers = new Dictionary<string, string>();
            var executedByLog = Context.UserAction.Find(ExecutedBy.Aid);
            Signers.Add("Prepared By", service.GetEmployeeByLoginName(executedByLog.Username).employeeName);
            if (CheckedBy != null)
            {
                var checkedByLog = Context.UserAction.Find(CheckedBy.Aid);
                Signers.Add("Checked  By", service.GetEmployeeByLoginName(checkedByLog.Username).employeeName);
            }
            if (CheckedAndApproved != null)
            {
                var CheckedAndApprovedLog = Context.UserAction.Find(CheckedAndApproved.Aid);
                Signers.Add("Checked and Approved By", service.GetEmployeeByLoginName(CheckedAndApprovedLog.Username).employeeName);

            }
            if (ApprovedBy != null)
            {
                var ApprovedByLog = Context.UserAction.Find(ApprovedBy.Aid);
                Signers.Add("Approved  By", service.GetEmployeeByLoginName(ApprovedByLog.Username).employeeName);
            }

            return Signers;

        }
    }
}
