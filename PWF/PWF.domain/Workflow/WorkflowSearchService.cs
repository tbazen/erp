﻿using INTAPS.Payroll.BDE;
using Microsoft.EntityFrameworkCore;
using PWF.data;
using PWF.data.Entities;
using PWF.domain.Infrastructure;
using PWF.domain.Infrastructure.Architecture;
using PWF.domain.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PWF.domain.Workflows
{
    public interface IWorkflowSearchService : IPWFService
    {
        void SetSession(UserSession session);
        string BuildSQLQuery(WFSearchRequestModel Model);
        Task<List<WFSearchResultModel>> SearchWF(WFSearchRequestModel Model);

    }

    public class WorkflowSearchService : PWFService, IWorkflowSearchService
    {
        private UserSession _session;

        public WorkflowSearchService(PWFContext _Context)
        {
            Context = _Context;
        }

        public void SetSession(UserSession session)
        {
            _session = session;
        }

        string GetRequestDateExpression(WorkflowTypes type)
        {
            Dictionary<WorkflowTypes, string> Expressions = new Dictionary<WorkflowTypes, string>();
            Expressions.Add(WorkflowTypes.StoreRequestWorkflow, ">'RequestDate'");
            Expressions.Add(WorkflowTypes.PaymentWorkflow, "'Request'->>'RequestDate'");
            Expressions.Add(WorkflowTypes.PurchaseWorkflow, "'Request'->>'RequestDate'");
            Expressions.Add(WorkflowTypes.PurchaseOrderWorkflow, ">'OrderDate'");
            try
            {
                return Expressions[type];
            }
            catch (Exception ex)
            {

                return String.Empty;
            }
        }

        string GetReferenceExpression(WorkflowTypes type)
        {
            Dictionary<WorkflowTypes, string> Expressions = new Dictionary<WorkflowTypes, string>();
            Expressions.Add(WorkflowTypes.StoreRequestWorkflow, "'RequestNo'->>'reference'");
            //Expressions.Add(WorkflowTypes.PaymentWorkflow, ".RequestNo.reference");
            Expressions.Add(WorkflowTypes.PaymentWorkflow, "'Reference'->>'reference'");
            Expressions.Add(WorkflowTypes.PurchaseWorkflow, "'RequestNo'->>'reference'");
            Expressions.Add(WorkflowTypes.PurchaseOrderWorkflow, "'Voucher'->>'reference'");

            try
            {
                return Expressions[type];
            }
            catch (Exception ex)
            {

                return String.Empty;
            }

        }

        string GetNoteExpression(WorkflowTypes type)
        {
            Dictionary<WorkflowTypes, string> Expressions = new Dictionary<WorkflowTypes, string>();
            Expressions.Add(WorkflowTypes.StoreRequestWorkflow, ">'requestDescription'");
            Expressions.Add(WorkflowTypes.PaymentWorkflow, "'Request'->>'Note'");
            Expressions.Add(WorkflowTypes.PurchaseWorkflow, "'Request'->>'Note'");
            Expressions.Add(WorkflowTypes.PurchaseOrderWorkflow, ">'Note'");
            try
            {
                return Expressions[type];
            }
            catch (Exception ex)
            {

                return String.Empty;
            }
        }

        string GetEmployeeExpression(WorkflowTypes type)
        {
            Dictionary<WorkflowTypes, string> Expressions = new Dictionary<WorkflowTypes, string>();
            Expressions.Add(WorkflowTypes.PaymentWorkflow, "'PaidTo'->>'id'");
            try
            {
                return Expressions[type];
            }
            catch (Exception ex)
            {

                return String.Empty;
            }
            
        }

        string GetSupplierExpression(WorkflowTypes type)
        {
            Dictionary<WorkflowTypes, string> Expressions = new Dictionary<WorkflowTypes, string>();
            Expressions.Add(WorkflowTypes.PaymentWorkflow, "'PaidTo'->>'code'");
            Expressions.Add(WorkflowTypes.PurchaseOrderWorkflow, "'Supplier'->>'Code'");
            try
            {
                return Expressions[type];
            }
            catch (Exception ex)
            {

                return String.Empty;
            }

        }

        public string BuildSQLQuery(WFSearchRequestModel Model)
        {
            int count = 0;
            string query = "select a.id, a.workflowid, a.description, a.seqno, a.fromstate, a.data, a.trigger, a.datatype, b.currentstate, b.typeid, b.initiatoruser, b.employeeid, b.observer, b.parentwfid, " +
                "  row_number from (select *, ROW_NUMBER() over(Partition by workflowid Order by aid desc) row_number from public.workitem ";

            string inner = "";


            if (Model.FromDate.HasValue)
            {
                var dateExp = GetRequestDateExpression(Model.Type);
                if (!String.IsNullOrEmpty(dateExp))
                {
                    string where = count == 0 ? " where " : " ";
                    string and = count > 0 ? " and " : "";
                    inner += $"{ where } {and} (data->{dateExp})::date >= '{DateTime.Parse(Model.FromDate.ToString()).ToString("yyyy-MM-dd")} 12:00:00 AM' ";
                    count++;
                }
               
            }
            if (Model.ToDate.HasValue)
            {
                var dateExp = GetRequestDateExpression(Model.Type);
                if (!String.IsNullOrEmpty(dateExp))
                {
                    string where = count == 0 ? " where " : " ";
                    string and = count > 0 ? " and " : "";
                    inner += $"{ where } {and} (data->{dateExp})::date  <= '{DateTime.Parse(Model.ToDate.ToString()).ToString("yyyy-MM-dd")} 11:59:59 PM' ";
                    count++;
                }
               
            }

            if (Model.Reference != null)
            {
                var refExp = GetReferenceExpression(Model.Type);
                if (!String.IsNullOrEmpty(refExp))
                {
                    string where = count == 0 ? " where " : " ";
                    string and = count > 0 ? " and " : "";
                    inner += $"{ where } {and} data->{refExp} like '%{Model.Reference}%' ";
                    count++;
                }
               
            }

            if (Model.Note != null)
            {
                var notExp = GetNoteExpression(Model.Type);
                if (!String.IsNullOrEmpty(notExp))
                {
                 string where = count == 0 ? " where " : " ";
                string and = count > 0 ? " and " : "";
                inner += $"{ where } {and} data->{notExp} like '%{Model.Note}%' ";
                count++;
                }
               
            }

            if(Model.Supplier != null)
            {
                var supExp = GetSupplierExpression(Model.Type);
                if (!String.IsNullOrEmpty(supExp))
                {
                    string where = count == 0 ? " where " : " ";
                    string and = count > 0 ? " and " : "";
                    inner += $"{ where } {and} data->{supExp} like '%{Model.Supplier}%' ";
                    count++;
                }
               
            }

            if (Model.EmployeeID != null)
            {
                var supExp = GetEmployeeExpression(Model.Type);
                if (!String.IsNullOrEmpty(supExp))
                {
                    string where = count == 0 ? " where " : " ";
                    string and = count > 0 ? " and " : "";
                    inner += $"{ where } {and} data->{supExp} like '%{Model.EmployeeID}%' ";
                    count++;
                }

            }

            string outer = $" ) as a inner join public.workflow as b on a.workflowid = b.id and b.typeid = {(int)Model.Type}  ";
            if (!String.IsNullOrWhiteSpace(Model.Note))
            {
                outer = outer + $" and b.description like '%{Model.Note}%' ";
            }
            if (!String.IsNullOrWhiteSpace(Model.CurrentState))
            {
                outer = outer + $" and b.currentstate = {Model.CurrentState} ";
            }

            if (!String.IsNullOrWhiteSpace(Model.EmployeeID))
            {
                PayrollBDE payroll = INTAPS.ClientServer.ApplicationServer.GetBDE<PayrollBDE>();
                var emp = payroll.GetEmployee(Int32.Parse(Model.EmployeeID));
                if (!String.IsNullOrWhiteSpace(emp.loginName))
                {
                    outer = outer + $" and b.initiatoruser = '{emp.loginName}' ";
                }
               
            }
            return query + inner + outer + " where row_number = 1;";
        }

        public async Task<List<WFSearchResultModel>> SearchWF(WFSearchRequestModel Model)
        {
            var query = BuildSQLQuery(Model);
            var result = await Context.WFSearchModel.FromSql(query).ToListAsync();
            return result;
        }



    }
}
