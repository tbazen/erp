﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PWF.domain.Workflows
{
    public enum WorkflowTypes
    {
        StoreRequestWorkflow = 1,
        PaymentWorkflow = 2,
        PurchaseOrderWorkflow = 1002,
        PurchaseWorkflow = 1003,
        StoreDeliveryWorkflow = 1004,
        ServiceDeliveryWorkflow = 1005,
    }
}
