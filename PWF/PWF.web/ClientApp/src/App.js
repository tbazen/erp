﻿import React, {Component}from 'react';
import { Route } from 'react-router';
import { Login } from './components/Auth/Login';
import Layout from './components/Layout';
import Home from './components/Home';
import Counter from './components/Counter';
import FetchData from './components/FetchData';
import Authorization from './components/Routes/Authorize';
import './App.scss';
import  Loadable from 'react-loadable';
import { HashRouter,  Switch } from 'react-router-dom';
import DefaultLayout from './containers/DefaultLayout';
import '../src/assets/css/style.css';
import Authorize from './Authorize';
import DocumentViewer from '../src/components/Document/DocumentViewer';


// export default () => (
//   <div>
//     <Route exact path='/' component={Authorization(Home)} />
//     <Route path='/counter' component={Authorization(Counter)} />
//     <Route path='/fetchdata/:startDateIndex?' component={Authorization(FetchData)} />
//     <Route path='/login' component={Login} />
//   </div>
// );


class App extends Component{
  render(){
    return (  
      <HashRouter>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/Document/:type/:wfid/:index" component={DocumentViewer} />
                <Route path="/" component={DefaultLayout} />



        </Switch>
      </HashRouter>
    )
  }
}

export default App;