import React, { PropTypes, Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { authActionCreators } from './store/auth';
import { loadActionCreators } from './store/load';
/**
 * HOC that Handles whether or not the user is allowed to see the page.
 *- user roles that are allowed to see the page.
 *
 */
const Authorize = (WrappedComponent) => {
    class WithAuthorization extends Component {
        // static propTypes = {
        //     authenticated: PropTypes.bool,
        //     session : PropTypes.object
        // };
        constructor(props) {
            super(props);
        }

        componentWillMount(){
            console.log(this.props);
            if(!this.props.authenticated){
                this.props.history.push("/login");
            }
        }

        render() {
            // const { authenticated } = this.props;
            return <WrappedComponent {...this.props} />
        }
    };

    function mapStateToProps(state) {
        return {
            authenticated: state.auth.authenticated,
            session: state.auth.session,
            loading: state.load.loading
        }
    }

    function mapDispatchToProps(dispatch) {
        return {
            auth: bindActionCreators(authActionCreators, dispatch),
            loader: bindActionCreators(loadActionCreators, dispatch)
        };
    }
    const app = connect(mapStateToProps, mapDispatchToProps)(WithAuthorization);
    return withRouter(app);
};


export default Authorize;