import React, { Component } from 'react';
import { PaymentService } from '../../service/payment.service';
import { Menu, Icon, Card, Row, Col, Form, Input, Select, Button, Checkbox, DatePicker } from 'antd';
import { Element } from 'react-scroll';
import DocumentTypeSelect from '../miniComponents/DocumentTypeSelect';
import ReactTable from 'react-table';
import swal from 'sweetalert';
import 'react-table/react-table.css';
import '../../assets/css/berp.scss';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;


class SearchAccountDocuments extends Component {

    constructor(props){
        super(props);
        this.state = {
            DateFromChecked : false,
            DateToChecked : false,
            searchResult : [],
            searchItem : [],
            selected : null,
            html : '',
            DocumentTypes : []
        };
        this.DateChecked = this.DateChecked.bind(this);
        this.searchItem = this.searchItem.bind(this);
        this.service = new PaymentService(this.props.session.token);
        this.selectItem = this.selectItem.bind(this);

    }

    componentDidMount(){
        var self = this;
       
        this.service.GetAllDocumentTypes().then(function(response){
            console.log(response);
            if(response.data.status){
                self.setState({
                    DocumentTypes : response.data.response
                });
            }
            if(!response.data.status){
                swal("",response.error,"");
            }
        }).catch(function(error){
            swal("",error.message,"");
        }).then(function(){
            console.log(self.state);
        });
    }

    handleClick = (e) => {
        console.log('click ', e);
      }


    selectItem(docID){
        var self = this;
        this.service.GetAccountDocumentHTML(docID).then(function(response){
            if(response.data.status == true){
                self.setState({
                    html : response.data.response
                });
                if(self.props.SelectItem != null){
                    self.props.SelectItem(docID);
                }
            }
            if(response.data.status == false){
                swal("",response.data.error);
            }
        }).catch(function(error){
            swal("",error.message,"");
        });
    };
    
    searchItem(e){
        var self = this;
        e.preventDefault();
        var result = [];
        this.props.form.validateFields((err, values) => {
            console.log(err);
            console.log(values);
            if (!err) {
              values.type = values.type.value;
              values.dateFromChecked = this.state.DateFromChecked;
              values.dateToChecked = this.state.DateToChecked;
              this.service.SearchDocuments(values).then(function(response){
                    if(response.data.status){
                        console.log(response.data);
                        response.data.response.map(function(item){
                            result.push({
                                AccountDocumentID : item.accountDocumentID,
                                DocumentDate : item.documentDate,
                                DocumentTypeID : item.documentTypeID,
                                ShortDescription : item.shortDescription,
                                PaperRef : item.paperRef
                            });
                        })
                    }
                    if(!response.data.status){
                        swal("",response.data.error,"");
                    }
              }).catch(function(error){
                  console.log("Error FOUND");
                  swal("",error.message,"");
              }).then(function(){
                  if(result.length > 0){
                      self.setState({
                          searchItem : result
                      });
                  }
                  console.log(self.state);
              });
            }
          })
    }  

    DateChecked(key){
        if(key == 1){
            this.setState({
                DateFromChecked : !this.state.DateFromChecked
            });
            if(this.state.DateToChecked == true){
                this.setState({
                    DateToChecked : false
                });
            }
            
        }
        if(key == 2){
            this.setState({
                DateToChecked : !this.state.DateToChecked
            });
        }
    }

      

    render(){

        const columns = [{
            "Header" : "Document Date",
            "accessor" : "DocumentDate",
            Cell : d => <span>
                {
                    new Date(d.original.DocumentDate).toLocaleString()
                }
            </span>
        },{
            "Header" : "Document Type",
            "accessor" : "DocumentTypeID",
            Cell : d => <span>{
                this.state.DocumentTypes.find(obj => {
                   // console.log(d);
                   return obj.id === d.original.DocumentTypeID
                }).name
            }</span>
        },{
            "Header" : "Reference",
            "accessor" :"PaperRef",

        },{
            "Header": "Note",
            "accessor":"ShortDescription"
        }
            ]

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
           
            
          };
          const formTailLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 8, offset: 4 },
          };
          const style = { padding : '0 !important', margin : '0 !important', paddingBotton : '-5px', marginBottom : '0px'};
        return(
            <Row>
                <Col span={8}>
                <Row>
                   
                    <Form className="card" onSubmit={this.searchItem}>
                    <h4>Search Account Documents</h4>
                        <Form.Item
                        {...formItemLayout}
                        style={style}
                        label = "Query"
                        >
                        {
                            getFieldDecorator("query",{
                            rules : [],
                            initialValue : (this.props.reference != null ? this.props.reference : '')
                            })(<Input />)
                        }
                        </Form.Item>
                        <Form.Item
                        {...formItemLayout}
                        label = "Type"
                        style = {style}
                        >
                        {
                            getFieldDecorator("type",{
                                rules: [{
                                    required: true,
                                    message: 'Please select type',
                                  }],
                            initialValue : {
                                value : -1 || []
                            },
                            valuePropName : 'option'
                            })(
                            <DocumentTypeSelect {...this.props}/>
                            )
                        }
                        </Form.Item>
                        <Form.Item
                        labelCol={16}
                        style ={style}
                        extra = {
                            <div>
                               <Checkbox onChange={() => this.DateChecked(1)} defaultChecked={false} >Date From</Checkbox>
                            {'   '}
                            <Checkbox 
                            disabled={!this.state.DateFromChecked}
                            checked = {this.state.DateFromChecked && this.state.DateToChecked}
                            onChange={() => this.DateChecked(2)} defaultChecked={false}>Date To</Checkbox> 
                            </div>
                            
                        }
                        >


                        </Form.Item>
                        {
                            this.state.DateFromChecked ?<Form.Item 
                            {...formItemLayout}
                            style = {style}
                            label = "Date From"
                            >
                            {
                                getFieldDecorator("dateFrom",{
                                    rules: [{
                                        required: this.state.DateFromChecked,
                                        message: 'Please select start date',
                                      }],
                                })(
                                    <DatePicker />
                                )
                            }
    
                            </Form.Item> : null
                        }
                        {
                            this.state.DateToChecked ?
                            <Form.Item 
                            {...formItemLayout}
                            style = {style}
                            label = "Date To"
                            >
                            {
                                getFieldDecorator("dateTo",{
                                    rules: [{
                                        required: this.state.DateToChecked,
                                        message: 'Please select end date',
                                      }],
                                })(
                                    <DatePicker />
                                )
                            }
    
                            </Form.Item> : null
                        }
        <Form.Item
        {...formItemLayout}>
          <Button type="primary" htmlType="submit">Search</Button>
        </Form.Item>
                        

                    </Form>
                </Row>
                <Row>
                <ReactTable
                        
                        data={this.state.searchItem}
                        columns={columns}
                        
                        noDataText={"No items Found"}
                            showPagination={this.state.searchItem.length > 10 ? true : false}
                        defaultPageSize={this.state.searchItem.length > 10 ? -1 : 10}
                        getTrProps={(state, rowInfo) => {

                            if (rowInfo && rowInfo.row) {
                                return {
                                    onClick: (e) => {
                                        console.log(rowInfo);
                                        this.setState({
                                            selected : rowInfo.index,
                                            selectedID: rowInfo.original.AccountDocumentID
                                        });
                                        this.selectItem(rowInfo.original.AccountDocumentID);
                                    },
                                    style: {
                                        background: rowInfo.index === this.state.selected ? '#00afec' : 'white',
                                        color: rowInfo.index === this.state.selected ? 'white' : 'black'
                                    }

                                }
                            } else {
                                return {}
                            }
                        }}
                    />

                </Row>
               
                </Col>

           
           <Col span={16}>
           <Card style={{minHeight : 500}}>
           <div className=" documentViewer" dangerouslySetInnerHTML={{__html : this.state.html}}>

</div>
           </Card>
     
           </Col>
            </Row>

        )
    }
}

export default Form.create()(SearchAccountDocuments);