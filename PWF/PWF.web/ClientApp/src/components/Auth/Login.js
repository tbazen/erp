import React, { Component } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import '../../css/login.css';
import { authActionCreators } from '../../store/auth';
import { loadActionCreators } from '../../store/load';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Modal, ModalHeader, ModalBody, ModalFooter, Row } from 'reactstrap';
import Axios from 'axios';
import { baseUrl } from '../../service/urlConfig';
import AuthService from '../../service/auth.service';
import swal from 'sweetalert';

class Signin extends Component{
    constructor(props){
        super(props);
        this.state = {
            username : '',
            password: '',
            modal: false,
            roles: [],
            role: ''

        }
        this.AuthService = new AuthService();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggle = this.toggle.bind(this);
        this.handleRoleSelection = this.handleRoleSelection.bind(this);
        this.submitRole = this.submitRole.bind(this);
    }




    handleRoleSelection = (event) => {
        this.setState({
            role: event.target.value
        });

    }

    handleSubmit = (event) => {
        event.preventDefault();
        const self = this;
        this.AuthService.CheckAuth(this.state.username, this.state.password).then(function(response){
            if(response.data.status == true){
                self.setState({
                    session: response.data.response,
                    roles: response.data.response.roles
                });
                if(response.data.response.roles.length > 1){
                    self.toggle();
                }
                
                else if(response.data.response.roles.length == 1)
                {

                    self.AuthService.Login(self.state.username,self.state.password,response.data.response.roles[0]).then(function(response){
                        if(response.data.status == true){
                            self.props.auth.login(response.data.response);
                            self.setState({
                                session: response.data.response,
                                roles: response.data.response.roles
                            });
                            self.toggle();
                            self.props.history.push('/');
                        }
                        if(response.data.status == false){
                            swal("Error",response.data.error,"warning");
                        }
                }).catch(function(error){
                    swal("Error",error.message,"warning");
                })
                }
            }
            if(!response.data.status){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        });
    }

    submitRole = () =>
    {
        var self = this;
        var data = new FormData();
        data.append("username", this.state.username);
        data.append("password", this.state.password);
        data.append("role", this.state.role);
        if(this.state.role == ''){
            swal("Error","Select role","warning");
        }
        else{
            this.AuthService.Login(this.state.username,this.state.password,this.state.role).then(function(response){
                    if(response.data.status == true){
                        self.props.auth.login(response.data.response);
                        self.setState({
                            session: response.data.response,
                            roles: response.data.response.roles
                        });
                        self.toggle();
                        self.props.history.push('/');
                    }
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
            }).catch(function(error){
                swal("Error",error.message,"warning");
            })
        }

    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render(){
        const style = {
            width: 300
        };

        const goHome = () => {
            this.props.history.push('/');
        }

        const Roles = ["", "Employee", "Store Keeper","Store Manager","Finance Manager","Owner","Cashier","Finalizer","Purchase Officer", "Purchase Processor","Purchase Supervisor", "Finance Officer"];
        const r = [0,2];
        return <div className="app flex-row align-items-center">
            <Container>
                <Row className="justify-content-center">
                    <Col md="8">
                        <CardGroup>
                            <Card className="p-4">
                                <CardBody>
                                    <Form onSubmit={this.handleSubmit}>
                                        <h1>Login</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        <InputGroup className="mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-user"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input type="text"                      
                                                name="username"
                                                required
                                                value={this.state.username}
                                                onChange={(e) => this.handleChange(e)}
                                            placeholder="Username" autoComplete="username" />
                                        </InputGroup>
                                        <InputGroup className="mb-4">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-lock"></i>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input type="password" placeholder="Password"
                                                name="password"
                                                required
                                                value={this.state.password}
                                                onChange={(e) => this.handleChange(e)}
                                            autoComplete="current-password" />
                                        </InputGroup>
                                        <Row>
                                            <Col xs="6">
                                                <Button color="primary" className="px-4">Login</Button>
                                            </Col>
                                            
                                        </Row>
                                    </Form>
                                </CardBody>
                            </Card>
                            <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                                <CardBody className="text-center">
                                    <div>
                                        <h2>Purchasing Workflow System</h2>
                                       
                                       
                                    </div>

                                    <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                        <ModalHeader toggle={this.toggle}>Select Roles</ModalHeader>
                                        <ModalBody>
                                            <FormGroup tag="fieldset">
                                                <legend>Roles</legend>
                                                {

                                                    (this.state.roles).map((key, value) => {
                                                        return (
                                                        <FormGroup check>
                                                            <Label check>
                                                                    <Input type="radio" name="role" value={key}
                                                                        onChange={(e) => this.handleRoleSelection(e)}
                                                                    />{' '}
                                                                    {Roles[key]}
                                                            </Label>
                                                        </FormGroup>)
                                                    })
                                                }

                                            </FormGroup>          </ModalBody>
                                        <ModalFooter>
                                            <Button color="primary" onClick={this.submitRole}>Select</Button>{' '}
                                        </ModalFooter>
                                    </Modal>
                                </CardBody>
                            </Card>
                        </CardGroup>
                    </Col>
                </Row>
            </Container>
        </div>

    }
}

function mapStateToProps(state) {
    return {
        authenticated: state.auth.authenticated,
        session: state.auth.session,
        loading: state.load.loading
    }
}

function mapDispatchToProps(dispatch) {
    return {
        auth: bindActionCreators(authActionCreators, dispatch),
        loader: bindActionCreators(loadActionCreators, dispatch)
    };
}
const Login = connect(mapStateToProps, mapDispatchToProps)(Signin);

export { Login };