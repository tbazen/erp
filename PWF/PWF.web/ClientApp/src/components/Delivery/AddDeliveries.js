import React, { Component } from 'react';

import { PurchaseService } from '../../service/purchase.service'
import { StoreService } from '../../service/store.service'

import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button, Form , Select, InputNumber, Icon} from 'antd';
import swal from 'sweetalert';
import Uploader from '../miniComponents/DocumentUploader';
import CostCenterSearch from '../miniComponents/CostCenterSearch';
import StoreSelect from '../miniComponents/StoreSelect2';

const { Option } = Select;
class AddDelivery extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Workitem : null,
            Data : null,
            SelectedFile : null,
            MeasureUnits : [],
            RemainingItemOrders : [],
        };

        this.Service = new PurchaseService();
        this.StoreService = new StoreService("");
        this.checkCostCenter = this.checkCostCenter.bind(this);
        this.getItemKeys = this.getItemKeys.bind(this);
        this.UploaderChild = React.createRef();
        this.AddDelivery = this.AddDelivery.bind(this);
        this.setFile = this.setFile.bind(this);
    }



    componentWillMount(){
        this.props.loader.stop();
        var wfid = this.props.match.params.id;
        var self = this;
        self.setState({
            wfid : wfid
        });
        this.StoreService.GetAllMeasureUnit().then(function(response){
            self.setState({
                MeasureUnits : response.data
            });
        })
       this.Service.GetItemDeliveryWorkitem(wfid).then(function(response){
           if(response.data.status == false){
               swal("Error",response.data.error,"warning");
           }
           if(response.data.status == true){
               self.setState({
                   Workitem : response.data.response,
                   Data : response.data.response.data
               });
           }
       }).catch(function(error){
           console.log(error);
           swal("Error",error.message,"warning");
       }).then(function(){
           if(self.state.Workitem != null){
               self.StoreService.GetRemainingItemOrders(self.state.Workitem.parentWFID).then(function(response){
                if(response.data.status == false){
                    swal("Error",response.data.error,"warning");
                }
                if(response.data.status == true){
                    self.setState({
                        RemainingItemOrders : response.data.response
                    })
                }
            }).catch(function(error){
                swal("Error",error.message,"warning");
            }).then(function(){
                console.log(self.state);
            })
           }
       })

    }

    setFile(doc){
        this.setState({
            SelectedFile : doc
        });
    }

    checkCostCenter = (rule,value,callback) => {
        if(value != undefined){
          callback();
          return;
        }
        callback("Select Cost Center");

    }

    getItemKeys(){

        var arr = [];
        if(this.state.Data == null || this.state.Data.orderedItems.length == 0){
            return [];
        }
        else{
            var i = this.state.Data.orderedItems.length;
            for (let index = 1; index <= i; index++) {
                if(this.state.Data.orderedItems[index-1].type == 1){
                    arr.push(index);
                }
               
             }
             return arr;
        }

    }

    removeItemRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        // We need at least one passenger
        if (itemKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          itemKeys: itemKeys.filter(key => key !== k),
        });
      }

      AddDelivery(e){
          e.preventDefault();
          var self = this;
          this.props.form.validateFields((err,values) => {
              if(err == null){
                  self.props.loader.start();
                  var delivered = values.itemKeys.map(function(key){
                      return values.item[key];
                  })
                  var data = {
                      note : values.Description,
                      orderedItems : this.state.Data.orderedItems,
                      deliveredItems : delivered,
                      costCenterID : values.costCenterID.value,
                      document : this.state.SelectedFile,
                      supplier : this.state.Data.supplier,

                  }
                  self.StoreService.DeliverItems(self.state.wfid,data).then(function(response){
                      self.props.loader.stop();
                      if(response.data.status == false){
                          swal("Error",response.data.error,"warning");
                      }
                      if(response.data.status == true){
                          swal("Success","Successfully delivered items","success").then(function(value){
                              self.props.history.push('/deliveries');
                          });
                      }
                      
                  }).catch(function(error){
                        self.props.loader.stop();
                        swal("Error",error.message,"warning");
                  })
              }
          })
      }
    // componentDidMount(){
    //     var self = this;
    //     this.Service.getwo().then(function(response){
    //         console.log(response);
    //         if(response.data.status == true){
    //             var result = response.data.response;
    //             self.setState({
    //                 Workflows : result
    //             });
    //         }
    //     })
        
    // }

    CancelRequest(wfid){
        var self = this;
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to remove this document?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willCancel => {
            if(willCancel){
                
            }
          });
    }

    render(){
        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }

        };

        var self= this;

        getFieldDecorator('itemKeys',{initialValue: this.getItemKeys() });
        const itemKeys = getFieldValue('itemKeys');
        const formLabel =
        <div style={{marginBottom : '-10px'}}>
            {itemKeys.length >= 1 ? (
        <Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          label="Item"
          style={{display: 'inline-block', width : 'calc(40%)'}}
          >

          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          label="Delivered"
          style={{display: 'inline-block', width : 'calc(10%)'}}
          >

          </Form.Item>

          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
           label="Remaining"
          style={{display: 'inline-block', width : 'calc(7%)'}}
          >

          </Form.Item>

          <span style={{ display: 'inline-block', width: '40px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          label="Unit"
          style={{display: 'inline-block', width : 'calc(15%)'}}
          >

          </Form.Item>





             <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
    </span>

       </Form.Item>
      ) : null}




            </div>
        ;
        const formItems =
        this.state.Data != null != null ?
        itemKeys.map((k,index) => (
            
            <div>


<Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          style={{display: 'inline-block', width : 'calc(40%)'}}
          >
          {
              getFieldDecorator(`item[${k}].code`,{
                initialValue : this.state.Data != null ? this.state.Data.orderedItems[k-1].code : '',
                
               rules : [{ validator : this.checkItem}]
              })(
                <Select placeholder=""
                disabled
                //style={{ width: 300 }}
                //onSelect={this.onSelect}
                >
                {
                   this.state.Data.orderedItems != null ?
                    this.state.Data.orderedItems.map(function(item){
                        return  <Option value={item.code}
                        key={item.code}
                        >
                  {item.description}
                    </Option>
                    })
                    : null

                    
                }


                </Select>
              )
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '20px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          style={{display: 'inline-block', width : 'calc(15%)'}}
          >
          {
              getFieldDecorator(`item[${k}].quantity`,{
                initialValue : 0 || '',
                rules : [{ required : true, message : 'Add Quantity'}]
              })(
                  <InputNumber
                   max={            this.state.RemainingItemOrders != null && this.state.RemainingItemOrders.length > 0 ?   this.state.RemainingItemOrders[k-1].quantity : 0
                   }
                  {...this.props} />
              )
          }
          </Form.Item>

          <span style={{ display: 'inline-block', width: '20px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          style={{display: 'inline-block', width : 'calc(7%)'}}
          >
          {
            this.state.RemainingItemOrders != null && this.state.RemainingItemOrders.length > 0 ?   this.state.RemainingItemOrders[k-1].quantity : null
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          style={{display: 'inline-block', width : 'calc(15%)'}}
          >
          
              {
                //this.state.Data != null ? this.state.Data.orderedItems[k-1].unitID : ''
              
                getFieldDecorator(`item[${k}].unitID`,{
                    initialValue : this.state.Data.orderedItems[k-1].unitID ,
                    rules : [{ required : true, message : 'Add Quantity'}]
                  })(
                    <Select placeholder=""
                    disabled
                    //style={{ width: 300 }}
                    //onSelect={this.onSelect}
                    >
                    {
                       this.state.MeasureUnits != null ?
                        this.state.MeasureUnits.map(function(item){
                            return  <Option value={item.id}
                            key={item.id}
                            >
                      {item.name}
                        </Option>
                        })
                        : null
    
                        
                    }
    
    
                    </Select>
                  )
              }
        
              
          
          </Form.Item>
         

             <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
             {' '}
             {itemKeys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={itemKeys.length === 1}
            onClick={() => this.removeItemRow(k)}
            style={{display: 'inline-block', width : '10px'}}
          />
        ) : null}
    </span>

         



</Form.Item>


            </div>

       )) : null;



        return(
            <Row>
                <Col span={24}>
                <Card 
                title="Store Delivery"
                // extra = { this.props.session.role == 1 || this.props.session.role == 8 ? <Button onClick={() => this.props.history.push('/purchase/request')}>Add New Request</Button> : null}
                >
                {
                    this.state.Data != null 
                        ?
                        <div>

                        
<Row>
                    <Col span={4} className="prop">Supplier</Col>
                    <Col span={6} className="value">{this.state.Data.supplier.nameCode}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note</Col>
                    <Col span={14} className="value">{this.state.Data.note}</Col>
                </Row>
                <Form
                onSubmit={(e) => this.AddDelivery(e)}
                >
                    <Form.Item 
                     label = "Store"
                     className="prop"
                     labelCol={{span : 4}}
                     wrapperCol={{span : 8}}
                     >
                      {
                        getFieldDecorator(`costCenterID`,{
                         //   rules : [{ validator : this.checkCostCenter}]
                        })(
                            // <Select>

                            // </Select>

                             <StoreSelect  {...this.props} />
                        )
                     }
                     </Form.Item>
                     <Form.Item
                     label="Delivered Items"
                     labelCol={{span : 4}}
                     wrapperCol={{span : 16}}
                     style={{ fontWeight : "bold"}}>
                      {formLabel}
                        {formItems}
                     </Form.Item>
                     <Form.Item  labelCol={{span : 4}}
                        wrapperCol ={{span :12 }}
                        className="prop"
                        label="Document">
                    <Uploader 
                    ref={this.UploaderChild}
                    {...this.props}
                    setFile={this.setFile}/
                    
                    >
                    </Form.Item>

                    
                <Form.Item {...formItemLayoutWithOutLabel}>

                                     

                                        <Button type="primary" htmlType="submit">Deliver</Button>

								
               
                </Form.Item>
                </Form>


                </div>     : null

                }

                </Card>
                </Col>
            </Row>
        )
    }
}

export default Form.create()(AddDelivery);