import React, { Component } from 'react';

import { PurchaseService } from '../../service/purchase.service'
import { StoreService } from '../../service/store.service'

import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button, Form , Select, InputNumber, Icon} from 'antd';
import swal from 'sweetalert';
import Uploader from '../miniComponents/DocumentUploader';
import CostCenterSearch from '../miniComponents/CostCenterSearch';
import StoreSelect from '../miniComponents/StoreSelect2';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';

const { Option } = Select;


function DownloadDoc(id){
    Axios(baseUrl +`/Payment/DownloadFile/${id}`, {
    method: 'GET',
    responseType: 'blob' //Force to receive data in a Blob Format
})
.then(response => {
//Create a Blob from the PDF Stream
    const file = new Blob(
      [response.data], 
      {type: 'application/pdf'});
//Build a URL from the file
    const fileURL = URL.createObjectURL(file);
//Open the URL on new Window
    window.open(fileURL);
})
.catch(error => {
    console.log(error);
});}


class SubmitVoucher extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Workitem : null,
            Data : null,
            SelectedFile : null,
            MeasureUnits : [],
            Store : null,
        };

        
        this.Service = new PurchaseService();
        this.StoreService = new StoreService("");
        // this.checkCostCenter = this.checkCostCenter.bind(this);
        // this.getItemKeys = this.getItemKeys.bind(this);
        this.UploaderChild = React.createRef();
        this.SubmitVoucher = this.SubmitVoucher.bind(this);
        this.setFile = this.setFile.bind(this);
    }

    componentWillMount(){
        this.props.loader.stop();
        var wfid = this.props.match.params.id;
        var self = this;
        self.setState({
            wfid : wfid
        });
        this.StoreService.GetAllMeasureUnit().then(function(response){
            self.setState({
                MeasureUnits : response.data
            });
        })
       this.Service.GetItemDeliveryWorkitem(wfid).then(function(response){
           if(response.data.status == false){
               swal("Error",response.data.error,"warning");
           }
           if(response.data.status == true){
               self.setState({
                   Workitem : response.data.response,
                   Data : response.data.response.data
               });
           }
       }).catch(function(error){
           console.log(error);
           swal("Error",error.message,"warning");
       }).then(function(){
           console.log(self.state);
           if(self.state.Data != null){

           

               self.StoreService.GetStoreInfo(self.state.Data.costCenterID).then(function(response){
                   console.log(response);
                   if(response.data.status == false){
                       swal("Error",response.data.error,"warning");
                   }
                   if(response.data.status == true){
                        self.setState({
                            Store : response.data.response
                        });
                   }
               }).catch(function(error){
                   swal("Error",error.message,"warning");
               })
           }
       })

    }

    setFile(doc){
        this.setState({
            SelectedFile : doc
        });
    }

    
    SubmitVoucher(e){
        e.preventDefault();
        var self = this;
        this.props.form.validateFields((err,values) => {
            if(err == null){
                if(self.state.SelectedFile == null){
                    swal("Error","Please attach the voucher document","warning");
                }
                else{
                    self.props.loader.start();
                    var data = {
                        voucherDoc : self.state.SelectedFile,
                        note : values.Description
                    };
                    console.log(data);
                    self.StoreService.SubmitDeliveryVoucher(self.state.wfid,data).then(function(response){
                        self.props.loader.stop();
                        if(response.data.status == false){
                            swal("Erro",response.data.error,"warning");
                        }
                        if(response.data.status == true){
                            swal("Success","Successfully submitted voucher","success").then(function(value){
                                self.props.history.push('/deliveries');
                            });
                        }
                        
                    }).catch(function(error){
                        self.props.loader.stop();
                          swal("Error",error.message,"warning");
                    })
                }

            }
        })
    }

    render(){

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }

        };

        const GetItemName = (name,code) => {
            return `${name} ${code}`;
        }

        const deliveredColums = [
            {
                title : "Item",
                render : d => d.description
            },
            {
                title : "Unit",
                dataIndex : "unitID",

            },
            {
                title : "Quantity",
                dataIndex : "quantity"
            }
        ]

        var self= this;

        return(

            <Row>
                <Card
                 title="Upload Voucher"
                 extra = { 
                     <div>
                         <Button.Group>
                         <Link to={  `/Document/3/${this.props.match.params.id}/1`}><Button>Download Voucher</Button></Link>
                     {
                         this.state.Data != null ?
                         <Link to={`/ViewAccountDocument/${this.state.Data.accountDocumentID}`}>
                         <Button> View Account Document</Button>
                          </Link>  : null
                     }
                         </Button.Group>

                     </div>
                     }>
                 
                {
                    this.state.Data != null
                    ?
                    <div>
                        <Row>
                    <Col span={4} className="prop">Reference</Col>
                    <Col span={6} className="value">{`SRV ${this.state.Data.voucher.reference}`}</Col>
                </Row>
                     <Row>
                    <Col span={4} className="prop">Supplier</Col>
                    <Col span={6} className="value">{this.state.Data.supplier.nameCode}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Document</Col>
                    <Col span={14} className="value">
                    <a onClick={() => DownloadDoc(this.state.Data.document.id)}>
{this.state.Data.document.name}
                    </a>
                    </Col>
                </Row>  
                <Row>
                    <Col span={4} className="prop">Note</Col>
                    <Col span={14} className="value">{this.state.Data.note}</Col>
                </Row>   
                {
                    this.state.Store != null ?
                    <Row>
                    <Col span={4} className="prop">Store</Col>
                    <Col span={14} className="value">{this.state.Store.description}</Col>
                </Row> 
                    : null
                }
               
                <Row>
                    <Col span={4} className="prop">Delivered Items</Col>
                    <Col span={14} className="value">
                    <Table
                        dataSource = {this.state.Data.deliveredItems}
                        columns = {deliveredColums}
                        pagination={false}
                    />
                    
                    </Col>
                </Row>
                <Form
                onSubmit={(e) => this.SubmitVoucher(e)}
                >
                    <Form.Item  labelCol={{span : 4}}
                        wrapperCol ={{span :12 }}
                        className="prop"
                        label="Signed Voucher">
                    <Uploader 
                    ref={this.UploaderChild}
                    {...this.props}
                    setFile={this.setFile}/
                    
                    >
                    </Form.Item>

                    
                <Form.Item {...formItemLayoutWithOutLabel}>

                                     

                                        <Button type="primary" htmlType="submit">Upload Voucher</Button>

								
               
                </Form.Item>
                </Form>
                    </div>
                    :null
                }


                 </Card>
            </Row>
        )
    }
}

export default Form.create()(SubmitVoucher);