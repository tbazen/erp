import React, { Component } from 'react';

import { PurchaseService } from '../../service/purchase.service'
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button } from 'antd';
import swal from 'sweetalert';

export default class View extends Component {
    constructor(props){
        super(props);
        this.state = {
            Workflows : []
        };

        this.Service = new PurchaseService();
    }

    componentDidMount(){
        var self = this;
        this.Service.GetStoreDeliveryWorkflows().then(function(response){
            console.log(response);
            if(response.data.status == true){
                var result = response.data.response;
                self.setState({
                    Workflows : result
                });
            }
        })
        
    }

    CancelRequest(wfid){
        var self = this;
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to remove this document?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willCancel => {
            if(willCancel){
                
            }
          });
    }

    render(){
        

       
        const DescriptionMinimizer = (desc) => {
            if(desc == null || desc == '')
                return <p></p>
            else
                return <p>{desc.substring(0,30)}</p>
        };

        const GetViewLink = (wfid,state) => {
            console.log(state);
            if(state == 1 ){
                return  <Link to={`/deliveries/add/${wfid}`}>
                <Button>Add Delivery</Button>
                
                </Link>
            }
            if(state == 2 ){
                return  <Link to={`/deliveries/voucher/${wfid}`}>
                <Button>Upload Voucher</Button>
                
                </Link>
            }
        }

        const columns = [{
            title: "Initiator",
            key : "id",
            dataIndex: "action.username"
        }, {
            title: "Descrition",
            render : d => DescriptionMinimizer(d.description)
        }, {
            title: "Time",
            dataIndex: "action.timeStr"
        }, {

            title: 'Action',
            render: d => GetViewLink(d.id,d.currentState)
        }

        ];

        return(
            <Row>
                <Col span={24}>
                <Card 
                title="Store Deliveries"
                // extra = { this.props.session.role == 1 || this.props.session.role == 8 ? <Button onClick={() => this.props.history.push('/purchase/request')}>Add New Request</Button> : null}
                >
                
                <Table
                columns = {columns}
                dataSource = {this.state.Workflows}
                /> 
                </Card>
                </Col>
            </Row>
        )
    }
}