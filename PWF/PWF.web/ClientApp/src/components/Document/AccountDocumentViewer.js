import React, { Component } from 'react';

import { PurchaseService } from '../../service/purchase.service'
import { StoreService } from '../../service/store.service'

import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button, Form , Select, InputNumber, Icon} from 'antd';
import swal from 'sweetalert';
import Uploader from '../miniComponents/DocumentUploader';
import CostCenterSearch from '../miniComponents/CostCenterSearch';
import StoreSelect from '../miniComponents/StoreSelect2';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';
import { PaymentService } from '../../service/payment.service';

const { Option } = Select;


export default class AccountDocumentViewer extends Component {
    constructor(props){
        super(props);

        this.state = {
            DocID : '',
            DocHTML : ''
        }

        this.Service = new PaymentService('');
    }


    componentWillMount(){
        var docID = this.props.match.params.id;
        var self = this;

        this.Service.GetAccountDocumentHTML(docID).then(function(response){
            if(response.data.status == true){
                self.setState({
                    DocID : docID,
                    DocHTML : response.data.response
                });
            }
            if(response.data.status == false){
                swal("",response.data.error);
            }
        }).catch(function(error){
            swal("",error.message,"");
        })
    }


    render(){

        return(
            this.state.DocHTML != null ?

            <div className="documentViewer" dangerouslySetInnerHTML={{__html : this.state.DocHTML}}>

            </div>

            : null
            
        )
    }
}