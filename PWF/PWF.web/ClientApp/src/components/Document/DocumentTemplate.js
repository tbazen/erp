import React from 'react';
import ReactPDF, { Document, Page, Text, View, StyleSheet } from '@react-pdf/renderer';
import { Badge, Button, ButtonGroup, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'antd';
import CostCenterSelect from '../miniComponents/CostCenterSelect';
// Create styles
import '../../assets/css/document.css';
import 'antd/dist/antd.min.css';

// Create Document Component

export default class DocumentTemplate extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            signers : [],
            colSpan : '',
        };
    }
    componentWillMount(){
        this.setState({
            signers : this.props.signers,
            colSpan : 24 / this.props.signers.length
        });
    }

    render(){

        const styles = StyleSheet.create({
            page: {
                flexDirection: 'row',
              //  backgroundColor: '#E4E4E4'
            },
            title  : {

                fontSize : 40,
                textAlign : 'center',
                marginBottom : -5,
            },
            title2: {
                fontSize: 40,
                textAlign: 'center',
            }
        });
        var self = this;
        return (
            <div className="mainDoc">
        <Document >
                <Page style={styles.page} >
                    <View style={styles.title}>
                        <p className="title">{this.props.authority}</p>
                        <p className="title">{this.props.documentType}</p>
                    </View>
                    <View style={styles.Date} className="section">
                    <div className="date">
                            <p>Date : {this.props.date}</p>
                            <p>Reference : {this.props.reference}</p>
                    </div>

                    </View>
                    <View>
                        <div className="">
                            {this.props.children}
                        </div>

                     </View>
                     <View>
                         <Row className="footerRow">

                          {
                                self.props.signers != null && self.props.signers.length > 0  ?
                                self.props.signers.map(function(item,index){
                                return (
                                    <Col span={5} offset={1} className="signatureSpace">
                                        <p>{item}</p>
                                        <p>{self.props.names[index]}</p>
                            </Col> 
                                )
                              })
                          : null 
                        }
                          
                           
                         </Row>
                     </View>
                </Page>
             </Document>
     
            </div>
       );
    }
}

