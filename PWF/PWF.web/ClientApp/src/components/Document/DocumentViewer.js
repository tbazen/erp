import React, { Component } from 'react';
import StoreIssueDocument from './StoreIssueDocument';
import PaymenntWFDocument from './PaymentWFDocument';
import StoreDeliveryDocument from './StoreDeliveryDocument';
import PurchaseOrderDocument from './PurchaseOrderDocument';
import '../../assets/css/documentViewer.scss';
export default class DocumentViewer extends Component {
    constructor(props){
        super(props);
    }


    render (){
        const { wfid , index, type } = this.props.match.params;

        return(
            <div>
                { type == 1 ? 
                <StoreIssueDocument {...this.props} />    
            : null
            }
            {
                type == 2 ? 
                <PaymenntWFDocument
                {...this.props} /> : null
            }
            {
                type == 3 ?
                <StoreDeliveryDocument
                {...this.props} /> : null
            }
            {
                type == 4 ?
                <PurchaseOrderDocument
                {...this.props} /> : null
            }
            </div>
        )
    }
}