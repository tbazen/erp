import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { StoreService } from  '../../service/store.service';
import { Link } from 'react-router-dom';
import { baseUrl } from '../../service/urlConfig';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload } from 'antd';
import swal from 'sweetalert';
// import Axios from 'axios';
// import DocumentFormHandler from './PaymentDocumentsForm/DocumentFormHandler';
// import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
// import DocumentBrowser from '../AccountDocument/SearchAccountDocument';
import '../../assets/css/berp.scss';
import BudgetTransactionViewer from '../Payment/BudgetTransactionViewer';
import DocumentTemplate from './DocumentTemplate';
const { TabPane } = Tabs;
const { TextArea } = Input;
const ButtonGroup = Button.Group;




class PaymentWFDocument extends Component {
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            activeTab : "1",
            Request : null,
            ImportedDocuments : [],
            NewDocuments : [],
            PaymentSource : [],
            DocumentTableData : [],
            AttachedDocs : [],
            Reference : {},
            BudgetDocument : null,
            Description : '',
            dropdownOpen: false,
            showFormModal : false,
            showAddDocumentModal : false,
            showUploaderModal : false,
            selectedDocumentID : '',
            selectedFormKey : '',
            DocumentTypes : [],
            DocumentAmount : {},
            docHTML: '',
            documentViewModal : false,
            serialType : '',
            selectedFile : null,
            docDescription : '',
            date : null,
            Signers : null,
            keys : [],
            names : [],

        };
        this.handleChange = this.handleChange.bind(this);
        this.Service = new PaymentService("");




        this.service = new PaymentService("");
        this.selecteDocument = this.selecteDocument.bind(this);
    }

    componentWillMount() {
        this.setState({
            wfid: this.props.match.params.id
        });

    }

    

    getAllDocuments(){
        if(this.state.NewDocuments == null && this.state.ImportedDocuments == null){
            return [];
        }
        else if(this.state.NewDocuments == null && this.state.ImportedDocuments != null){
            return this.state.ImportedDocuments;
        }
        else if(this.state.NewDocuments != null && this.state.ImportedDocuments == null){
            return this.state.NewDocuments;
        }
        else
            return [ ...this.state.NewDocuments, ...this.state.ImportedDocuments ];
    }



    componentDidMount() {
        var self = this;
        var service = new PaymentService("");
        const { wfid , index } = this.props.match.params;
        service.GetAllDocumentTypes().then(function(response){
            console.log(response);
            if(response.data.status){
                self.setState({
                    DocumentTypes : response.data.response
                });
            }
            if(!response.data.status){
                swal("",response.error,"");
            }
        }).catch(function(error){
            swal("",error.message,"");
        });

        service.GetPaymentRequestSigners(wfid).then(function(response){
           if(response.data.status == true){
            const keys = Object.keys(response.data.response);
            const names = keys.map(function(item){
              return response.data.response[item];
      })
               self.setState({
                   Signers : response.data.response,
                   keys : keys,
                   names : names
               });

           }
           if(response.data.status == false){
               swal("Error",response.error,"warning");
           }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            console.log(self.state);
        })

        service.GetLastPaymentWorkitem(wfid).then(function (response) {
            console.log(response);
            self.setState({
                Request : response.data.data.request,
                ImportedDocuments : response.data.data.importedDocuments,
                NewDocuments : response.data.data.newDocuments,
                PaymentSource : response.data.data.sources,
                Reference : response.data.data.reference,
                BudgetDocument : response.data.data.budgetDocument,
                AttachedDocs : response.data.data.attachedDocs,

            });  
            console.log(self);
        }).then(function(){
            console.log(self.state);
            var docs = self.getAllDocuments();
            if(docs != null && docs.length > 0){
                self.setState({
                    date : docs[0].documentDate
                })
            }
            service.GetSerialType(self.state.Reference.typeID).then(function(response){
                if(response.data.status){
                    self.setState({
                        serialType : response.data.response.code
                    })
                }
            });
            
        })
        

    }

    selecteDocument(docID){
        this.setState({
            selectedDocumentID : docID
        });
    }

    onChange = (activeKey) => {
        this.setState({ activeTab : activeKey });
      }

    

    openForm(key){
        this.setState({
            selectedFormKey : key
        });
        this.toggleModal();
    }

    toggle() {
        var self = this;
        this.setState({
          dropdownOpen: !self.state.dropdownOpen
        });
      }

    handleChange = (event) => {
        console.log(event);
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        console.log(this.state);
    }

    handleMenuClick(e) {
        message.info('Click on menu item.');
        console.log('click', e);
      }

   


    



    render() {



        const paymentSourceCol = [
            {
                title : "Source",
                key : "assetAccountID",
                dataIndex : "name"
            }, {
                title : "Amount",
                dataIndex : "amount"
            }
        ]

        const itemColumns = [
            {
                title : "Description",
                dataIndex : "description"
            },
            {
                title : "Unit",
                dataIndex : "unit"
            },
            {
                title : "Unit Price",
                dataIndex : "unitPrice",
            },
            {
                title : "Quantity",
                dataIndex : "quantity",
            },{
                title : "Amount",
                dataIndex : "amount"
            }
        ];

        var docs = this.getAllDocuments();
        console.log(this.state);

        const html = "<h2>Transfer from Cashier to Cashier</h2><h2>Date: Jan 20,19 12:03 PM</h2><h2>Ref: 2</h2><h2>Affected accounts</h2><h3>No accounting entry for this document</h3><h2>More Information</h2><b>Amount:<b/>1,200.00<h2>References</h2><br/><strong>Cash Payment Voucher: </strong>00000002";
        const paidTo = ["","Employee","Customer","Other"];
        

        

        return(<div>
            {
                this.state.Request != null  ? 
<DocumentTemplate
                date={new Date(this.state.date).toLocaleString()    }
                reference = {`${this.state.serialType} ${this.state.Reference.reference}`}
                authority="Bishoftu Water Supply and Swearage Authority"
                documentType = "Payment Voucher"
                signers = {this.state.keys}
                names={ this.state.names}

>


{/*                
                <Card 
                title="Payment Request"
                style = {{marginTop : -20}}
              
                 > */}
                 
                   {
                     this.state.Request != null ?
                        <div>
                                            
                <Row>
                
                    <Col span={4} className="prop">Type</Col>
                    <Col span={18} className="value">{this.state.Request.type == 0 ? "General" : "Misson Advance"}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Items</Col>
                    <Col span={18} className="value"> <Table
                 columns={itemColumns}
                 pagination={false}
                  dataSource={this.state.Request.items}

               /></Col>
                </Row> 
                {
                    this.state.Request.type == 1 ?  
                    <div>
                        <Row>
                    <Col span={4} className="prop">Date
                    </Col>
                    <Col  span={18} className="value">{new Date(this.state.Request.dateFrom).toLocaleDateString() } - {new Date(this.state.Request.dateTo).toLocaleDateString()}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Amount Per Day </Col>
                    <Col span={18} className="value">{this.state.Request.amountPerDay}</Col>
                </Row>
                    </div>
                    
                    : null 
                }
                <Row>
                <Col span={4} className="prop">Paid From </Col>
                    <Col span={18} className="value">
                    <Table 
                        columns = {paymentSourceCol}
                        dataSource = {this.state.PaymentSource}
                        pagination ={false}
                    />
                        </Col>
                </Row>
                <Row>
                <Col span={4} className="prop">Budget Source </Col>
                    <Col span={18} className="value">
                   <BudgetTransactionViewer 
                   budgetDocs={this.state.BudgetDocument}
                   {...this.props}
                   />
                        </Col>
                </Row>

                      {/* <Row>
                      {/* <Col span={4} className="prop">Document </Col>
                    <Col span={18} className="value">
                    <a
                     onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.files.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                              [response.data], 
                              {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                        
                       
                     }}
                     
                     >

                    {this.state.Request.files.name}</a>
                    </Col>
                </Row> */} */}
                <Row>
                    <Col span={4} className="prop">Paid To </Col>
                    <Col span={18} className="value">{
                        this.state.Request.paidTo.name
                        
                        }</Col>
                </Row>
              
                <Row>
                    <Col span={4} className="prop"></Col>
                    <Col span={18} className="value">{
                        paidTo[this.state.Request.paidTo.type]
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={18} offset={4}>_______________________</Col>
                </Row>
                {/* {
                    this.state.AttachedDocs != null && this.state.AttachedDocs.length > 0 ?
                    <div>
                                            <Row>
                    <Col span={4} className="prop">Attached Documents</Col>
                    <Col span={18} className="value">
                    <DocumentsRenderer {...this.props} docs={this.state.AttachedDocs} remove={this.RemoveDocument}/>
                    </Col>
                </Row>

                    </div>

                    : null
                } */}

               
                        </div>

                     : null
                 }
          
               

                {/* </Card>
           */}
              


     

        </DocumentTemplate>


                : null
            }
                     
        </div>
         
            )
    }

    
}

export default PaymentWFDocument;
// export default Form.create() (Execute);