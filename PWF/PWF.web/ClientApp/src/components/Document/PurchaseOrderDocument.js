import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { StoreService} from '../../service/store.service';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Select, InputNumber } from 'antd';
import swal from 'sweetalert';
import DocumentTemplate from './DocumentTemplate';

export default class PurchaseOrderPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Order : null,
            PaymentWorkflows : [],
            TotalAmount : '',
            cashPayment : false,
            ItemDeliveryWorkflows : [],
            ServiceDeliveryWorkflows : [],
            IDNameDict : null,
        }
        this.Service = new PurchaseService();
        this.StoreService = new StoreService("");

    }

    componentWillMount(){
        var self = this;
        const { wfid , index } = this.props.match.params;
        this.setState({
            wfid 
        });


        console.log(wfid);

        this.Service.GetLastPurchaseOrderWorkitem(
            wfid).then(function(response){
            console.log(response);
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                console.log(response.data.response);
                self.setState({
                    Order : response.data.response,
                    TotalAmount : response.data.response.data.items.sum("amount"),
                });


            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            console.log(self.state);
           
        });
    }

    render(){

        const itemColumn = [
            { title: 'Name', dataIndex: 'description', key: 'code' },
            { title: 'Quantity', dataIndex: 'quantity'},
            { title: 'Unit Price', dataIndex: 'unitPrice' },
            { title: 'Amount', dataIndex : 'amount' },
            
            ];
        
        return (
            this.state.Order != null ?
            <DocumentTemplate
                date={new Date().toLocaleDateString()}
                reference = {`POV ${this.state.Order.data.voucher.reference}`}
                authority="Bishoftu Water Supply and Swearage Authority"
                documentType = "Purchase Order Voucher"
                signers = {["Sincerely"]}
                names={ [""]}
            >
               {
                   this.state.Order != null ?

                    <div
                    className="PurchaseOrderDoc"
                    >

                    <div className="address">
                        <p>To {this.state.Order.data.supplier.name}</p>
                        <p>{this.state.Order.data.supplier.addresss}</p>
                        <p>{this.state.Order.data.supplier.city}</p>

                    </div>
                    <div  className="Object">
                      <p>OBJECT : PURCHASE ORDER LETTER</p> 
                       <hr/>
                    </div>
                    <div className="request">
                        <p>Our Organization would like to ordered items that are listed below.</p>
                    </div>
                    <div className="itemTable">
                    <Table columns={itemColumn}
                    className="itemTable"
                    pagination={false}
                    dataSource={this.state.Order.data.items}
                    footer ={ () =>
                        <Row className="footer">
                            <Col span={6}>Total</Col>
                            <Col span={18} style={{textAlign : "right"}}>{this.state.TotalAmount}</Col>
                        </Row>
                    }
                    />


                    </div>
                    <div className="request">
                        <p>We would like to thank you for your cooperation.</p>
                    </div>

                        </div>

                        : null

            }
            </DocumentTemplate>
            :null
        )

    }
}