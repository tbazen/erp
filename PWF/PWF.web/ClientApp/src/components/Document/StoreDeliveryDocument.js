import React, { Component } from 'react';

import { PurchaseService } from '../../service/purchase.service'
import { StoreService } from '../../service/store.service'

import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button, Form , Select, InputNumber, Icon} from 'antd';
import swal from 'sweetalert';
import Uploader from '../miniComponents/DocumentUploader';
import CostCenterSearch from '../miniComponents/CostCenterSearch';
import StoreSelect from '../miniComponents/StoreSelect2';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';
import DocumentTemplate from './DocumentTemplate';
const { Option } = Select;


function DownloadDoc(id){
    Axios(baseUrl +`/Payment/DownloadFile/${id}`, {
    method: 'GET',
    responseType: 'blob' //Force to receive data in a Blob Format
})
.then(response => {
//Create a Blob from the PDF Stream
    const file = new Blob(
      [response.data], 
      {type: 'application/pdf'});
//Build a URL from the file
    const fileURL = URL.createObjectURL(file);
//Open the URL on new Window
    window.open(fileURL);
})
.catch(error => {
    console.log(error);
});}


export default class StoreDeliveryDocument extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Workitem : null,
            Data : null,
            SelectedFile : null,
            MeasureUnits : [],
            Store : null,
            AccountDocument : null,
            Signers: [],
        };

        
        this.Service = new PurchaseService();
        this.StoreService = new StoreService("");
        // this.checkCostCenter = this.checkCostCenter.bind(this);
        // this.getItemKeys = this.getItemKeys.bind(this);
        this.UploaderChild = React.createRef();
        this.SubmitVoucher = this.SubmitVoucher.bind(this);
        this.setFile = this.setFile.bind(this);
    }

    componentWillMount(){
        var wfid = this.props.match.params.wfid;
        var self = this;
        self.setState({
            wfid : wfid
        });

        this.StoreService.GetGRVDocumentSigners(wfid).then(function(response){
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                console.log(response.data.response);
                self.setState({
                    Signers : response.data.response,
                });
            }
        }).catch(function(error){
            console.log(error);
            swal("Error",error.message,"warning");
        })

       this.Service.GetItemDeliveryWorkitem(wfid).then(function(response){
           if(response.data.status == false){
               swal("Error",response.data.error,"warning");
           }
           if(response.data.status == true){
               self.setState({
                   Workitem : response.data.response,
                   Data : response.data.response.data
               });
           }
       }).catch(function(error){
           console.log(error);
           swal("Error",error.message,"warning");
       }).then(function(){
           console.log(self.state);
           if(self.state.Data != null){
        

            self.StoreService.GetStoreDeliveryDocument(self.state.Data.accountDocumentID).then(function(response){
                console.log(response);
                if(response.data.status == false){
                    swal("Error",response.data.error,"warning");
                }
                if(response.data.status == true){
                     self.setState({
                         AccountDocument : response.data.response
                     });
                }
            }).catch(function(error){
                swal("Error",error.message,"warning");
            });

               self.StoreService.GetStoreInfo(self.state.Data.costCenterID).then(function(response){
                   console.log(response);
                   if(response.data.status == false){
                       swal("Error",response.data.error,"warning");
                   }
                   if(response.data.status == true){
                        self.setState({
                            Store : response.data.response
                        });
                   }
               }).catch(function(error){
                   swal("Error",error.message,"warning");
               })
           }
       })

    }

    setFile(doc){
        this.setState({
            SelectedFile : doc
        });
    }

    
    SubmitVoucher(e){
        e.preventDefault();
        var self = this;
        this.props.form.validateFields((err,values) => {
            if(err == null){
                
                // self.StoreService.DeliverItems(self.state.wfid,data).then(function(response){
                //     if(response.data.status == false){
                //         swal("Erro",response.data.error,"warning");
                //     }
                //     if(response.data.status == true){
                //         swal("Success","Successfully delivered items","success").then(function(value){
                //             self.props.history.push('/deliveries');
                //         });
                //     }
                    
                // }).catch(function(error){
                //       swal("Error",error.message,"warning");
                // })
            }
        })
    }

    render(){


        const deliveredColums = [
            {
                title : "Item",
                render : d => d.description
            },
            {
                title : "Unit",
                dataIndex : "unitID",

            },
            {
                title : "Quantity",
                dataIndex : "quantity"
            }
        ]

        var self= this;

        return(
<div className="docViewer">
<DocumentTemplate
                date={ this.state.AccountDocument != null ? new Date(this.state.AccountDocument.documentDate).toLocaleString() : null   }
                reference = {this.state.Data != null ? `GRV ${this.state.Data.voucher.reference}` : ''}
                authority="Bishoftu Water Supply and Swearage Authority"
                documentType = "Good Receving Voucher"
                signers = {this.state.Signers != null && this.state.Signers.length > 0 ? this.state.Signers[0] : []}
                names={ this.state.Signers != null && this.state.Signers.length > 0 ? this.state.Signers[1] : []}>
                <Row>

                 
                {
                    this.state.Data != null
                    ?
                    <div>

                     <Row>
                    <Col span={4} className="prop">Supplier</Col>
                    <Col span={6} className="value">{this.state.Data.supplier.nameCode}</Col>
                </Row>
 
                {
                    this.state.Store != null ?
                    <Row>
                    <Col span={4} className="prop">Store</Col>
                    <Col span={14} className="value">{this.state.Store.description}</Col>
                </Row> 
                    : null
                }
               
                <Row>
                    <Col span={4} className="prop">Delivered Items</Col>
                    <Col span={14} className="value">
                    <Table
                        dataSource = {this.state.Data.deliveredItems}
                        columns = {deliveredColums}
                        pagination={false}
                    />
                    
                    </Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note</Col>
                    <Col span={14} className="value">{this.state.Data.note}</Col>
                </Row>  \
                    </div>
                    :null
                }
            </Row>
       
</DocumentTemplate>

</div>

        )
    }
}

