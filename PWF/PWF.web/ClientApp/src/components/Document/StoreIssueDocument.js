import React from 'react';
import ReactPDF, { Document, Page, Text, View, StyleSheet } from '@react-pdf/renderer';
import { Badge, Button, ButtonGroup, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'antd';
import CostCenterSelect from '../miniComponents/CostCenterSelect';
import DocumentTemplate from '../Document/DocumentTemplate';
// Create styles
import '../../assets/css/document.css';
import 'antd/dist/antd.min.css';
import { StoreService } from '../../service/store.service';


// Create Document Component

export default class StoreIssueDocument extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            Issued : {},
            Signers : {},
        };

    }

    componentDidMount(){
        const { wfid , index } = this.props.match.params;
        var self = this;
        var Service = new StoreService("");
        Service.GetLastWorkItem(wfid).then(function(data){
            self.setState({
                Issued: data.data.data.Issued.filter(function(item){
                    return item.AccountDocumentID == index
                })[0]
        })});
        Service.GetStoreIssueDocumentSigners(wfid).then(function(response){
            self.setState({
                Signers : response.data
            });
        });
    }

    render() {

        function DocumentTable(record) {
            console.log(record)
            return (
                <Table
                rowClassName="datatable"
                className="datatable"
                    dataSource={record.items}
                    columns={iColumns}
                    pagination={false}
                    scrol={{ y: 600 }}
                />
            );

        }
        const iColumns = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Quantity',
            dataIndex: 'quantity',
        }];
        const styles = StyleSheet.create({
            page: {
                flexDirection: 'row',
                //  backgroundColor: '#E4E4E4'
            },
            title: {

                fontSize: 40,
                textAlign: 'center',
                marginBottom: -5,
            },
            title2: {
                fontSize: 40,
                textAlign: 'center',
            }
        });
        return (

            <DocumentTemplate
                date={new Date(this.state.Issued.DocumentDate ).toLocaleString()}
                reference = {"SIV" + this.state.Issued.PaperRef}
                authority="Bishoftu Water Supply and Swearage Authority"
                documentType = "Store Issue Voucher"
                signers = {["Filed By","Approved By","Issued By"]}
                names={ [ this.state.Signers["Filler"], this.state.Signers["Reiewer"], this.state.Signers["Issuer"] ]}
            >
              {
                  DocumentTable(this.state.Issued)
              }
            </DocumentTemplate>
                      
        );
    }
}

