import React, { Component } from 'react';
import '../assets/css/search.scss';

export default class Forbidden extends Component {


    render(){
        return(
            <div className="forbidden">
                <img src={"/assets/img/403.png"} className="icon" />
                <div className="title">403: Access Forbidden</div>
                <div className="message">You Are Not Allowed To Access This Resource</div>
            </div>
        )
    }
}