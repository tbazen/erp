import React, { Component }  from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { authActionCreators } from '../store/auth';
import { loadActionCreators } from '../store/load';
import { PaymentService } from '../service/payment.service';
import { StoreService } from '../service/store.service';
import { PurchaseService } from '../service/purchase.service';
import swal from 'sweetalert';
import axios from 'axios';
import 'react-table/react-table.css'; 
import { Menu, Dropdown, Button, Icon, Row, Col, Card } from 'antd';
import { Link } from 'react-router-dom';
const { ButtonGroup } = Button;



class Home extends Component {
  constructor(props){
    super(props);

    this.state = {
      StoreRequestWorkflows : [],
      PaymentWorkflows : [],
      ServiceDeliveryWorkflows : [],
      PurchaseWorkflows : [],
    }

    this.PaymentService = new PaymentService("");
    this.StoreService = new StoreService("");
    this.PurchaseService = new PurchaseService();
    this.workItemIdentifier = this.workItemIdentifier.bind(this);

  }

  componentDidMount(){
    // this.props.loader.start();
    // console.log(this);
    
    const RoleState = { 4 : 1, 1 : 0, 5 : 2,  6 : 3, 7: 4}
        var self = this;
        var Service = new PaymentService(this.props.session.token);
        self.PurchaseService.GetUserServiceDeliveryWorkflows().then(function(response){
          console.log(response);
          if(response.data.status == false){
            swal("Error",response.data.error,"warning");
          }
          if(response.data.status == true){
            self.setState({
              ServiceDeliveryWorkflows : response.data.response
            });
          }
        })
        .catch(function(error){
          swal("Error",error.message,"warning");
        })
        self.PaymentService.GetPaymentRequsetWorkflows().then(function (response) {
            var result = response.data;
            if (result.status == true) {
                console.log(RoleState[self.props.session.role]);
                    if(result.response != null && result.response.length > 0){
                        self.setState({
                            PaymentWorkflows : result.response.filter(function(item){
                                return item.currentState == RoleState[self.props.session.role]
                            })
                        });
                    }    
            }
            if(result.status == false){
              swal("Error",result.error,"warning");
            }
        })
        .catch(function(error){
          swal("Error",error.message,"warning");
        });

        var d = [];
        self.StoreService.StoreRequestWorkflows().then(function (response) {
                console.log(response);
            //     d = response.data.filter(function(el){
            //         return self.workItemIdentifier(el.currentState, self.props.session.role);                  
            // })
            self.setState({
              StoreRequestWorkflows: response.data.filter(function(el){
                return self.workItemIdentifier(el.currentState, self.props.session.role);                  
        })
          });
          
        })
        .catch(function(error){
          swal("Error",error.message,"warning");
        })
        .then(function(){
          console.log(self.state);
        });
        const PurchaseRoleState = { 1 : 0 , 9 : 3};
        if([8,9,10].indexOf(self.props.session.role) >=0){
          self.PurchaseService.GetPurchaseWorkflows().then(function(response){
            console.log(response);
            if(response.data.status == true){
                var result = response.data.response;
                if(result != null && result.length > 0){
                    console.log(result);
                    if(self.props.session.role != 10 && self.props.session.role != 8){
                        self.setState({
                            PurchaseWorkflows : result.filter(function(item){
                                return item.currentState == PurchaseRoleState[self.props.session.role]
                            })
                        });
                    }
                    if(self.props.session.role == 8 || self.props.session.role == 2){
                        self.setState({
                          PurchaseWorkflows : result.filter(function(item){
                                return item.currentState == 0 || item.currentState == 2;
                            })
                        });
                    }
                    if(self.props.session.role == 10){
                        self.setState({
                          PurchaseWorkflows : result.filter(function(item){
                                return item.currentState == 1 || item.currentState == 4;
                            })
                        });
                    }
                }
            }
        })    }

    }


    workItemIdentifier = (state, role) => {
      if (role == 1) {
          return state == 0;
      }
      if (role == 2) {
          return (state == 1 || state == 3);
      }
      if (role == 3) {
          return (state == 2)
      };
      return false;
  }

      render(){
        const DescriptionMinimizer = (desc) => {
          if(desc == null || desc == '')
              return <p></p>
          else
              return <p>{desc.substring(0,30)}</p>
      }

      const GetPurchaseActionLink = (wfid,state) => {
        var role = this.props.session.role;
        if(state == 0){
            return  <div>
                <Button
            onClick={
                () => this.props.history.push("/purchase")
            }
            >Cancel</Button>
            <Link to={`/purchase/resubmit/${wfid}`}>
            <Button>Resubmit</Button>
            </Link>
            
                </div>
            
          
        }
        if(state == 1 ){
            return  <Link to={`/purchase/reviewReq/${wfid}`}>
            <Button>Review</Button>
            
            </Link>
        }
        if(state == 2 ){
            return  <Link to={`/purchase/process/${wfid}`}>
            <Button>Process</Button>
            
            </Link>
        }
        if(state == 3){
            return  <Link to={`/purchase/order/${wfid}`}>
            <Button>Execute</Button>
            
            </Link>
        }
        if(state == 4 ){
            return  <Link to={`/purchase/finalize/${wfid}`}>
            <Button>Finalize</Button>
            
            </Link>
        }
    }

      const GetPaymentViewLink = (wfid) => {
        var role = this.props.session.role;
        if(role == 1 || role == 11){
          return  <Link to={`/payment/resubmit/${wfid}`}>
          <Button>Resubmit Request</Button>
          
          </Link>
        }

        if(role == 4 ){
            return  <Link to={`/payment/check/${wfid}`}>
            <Button>Check</Button>
            
            </Link>
        }
        if(role == 5 ){
            return  <Link to={`/payment/review/${wfid}`}>
            <Button>Review</Button>
            
            </Link>
        }
        if(role == 6 ){
            return  <Link to={`/payment/execute/${wfid}`}>
            <Button>Execute</Button>
            
            </Link>
        }
        if(role == 7 ){
            return  <Link to={`/payment/finalize/${wfid}`}>
            <Button>Finalize</Button>
            
            </Link>
        }
    }

    const States = ["Filing", "Reogrganizing", "Reviewing", "Issuance"];
        const reorganizeLin = (d) => { return '/store/reorganize/' + d.id; }
        const issueLink = (d) => { return '/store/issue/' + d.id };
        const resubmitLink = (d) => { return '/store/resubmit/' + d.id };
        const ViewLin = (d) => { return '/store/review/' + d.id; }
        var cellLink = (d) => {
            if(this.props.session.role == 1){
                console.log(d);
                if(d.currentState == 0){
                    return (
                        <div>
                            <Button.Group>
                            <Link to={"/store/resubmit/"+d.id}><Button sm outline color="primary"> Resubmit</Button></Link>
                            <Link to="/store">
                            <Button sm outline color="danger" > Cancel </Button>
                            </Link>
                            </Button.Group>
                           
                        </div>




                                       
                    )
                }
            }
            if (this.props.session.role == 2) {
                if(d.currentState == 1){
                    return (<Link to={reorganizeLin(d)}><Button sm outline color="primary"> Reorganize</Button> </Link>);
                }
                else {
                    return (<Link to={issueLink(d)}><Button sm outline color="primary"> Issue </Button></Link>);
                }
                
            }
            if (this.props.session.role == 3) {
                return (<Link to={ViewLin(d)}><Button sm outline color="primary"> Review</Button> </Link>);
                }
            }
            console.log(this.state);
            var role = this.props.session.role;

            return(
              <div>
                {
                  role == 1  ? this.state.ServiceDeliveryWorkflows.length > 0 ?
                    <Row gutter={24}>
                                                 <h3 style={{textAlign : "center", paddingTop : 30}}>Service Certifications</h3>
                        <hr/>
                        {
                          this.state.ServiceDeliveryWorkflows.map(function(item){
                            return (
                              <Col span={8} style={{marginTop: 15}}>
                              <Card title="Service Certification" bordered={false}
                              extra = {
                               <Link to={`/Certify/${item.id}`}><Button>Certify</Button></Link>
                              }
                              >
                              <p>Initiator : {item.action.username}</p>
                              <p>Description : {item.description}</p>
                              <p>{new Date(item.action.timeStr).toLocaleString()}</p>
                              </Card>
                            </Col>
    
                            )
                          })
                        }
                    </Row>

                  : <div>

                  <h3 style={{textAlign : "center", paddingTop : 30}}>No pending Service Certifications</h3>
                  <hr/> </div>
                    : null
                }
                      {
                          role == 1 || role == 2 || role == 3 ? 
                        
                        this.state.StoreRequestWorkflows != null && this.state.StoreRequestWorkflows.length > 0 ?
                        <Row gutter={16} >
                           <h3 style={{textAlign : "center", paddingTop : 30}}>Store Requests</h3>
                        <hr/>
                        {
                          this.state.StoreRequestWorkflows.map(function(item){
                            return (
                              <Col span={8} style={{marginTop: 15}}>
                              <Card title="Store Request" bordered={false}
                              extra = {
                                cellLink(item)
                              }
                              >
                              <p>Initiator : {item.action.username}</p>
                              <p>Description : {item.description}</p>
                              <p>{new Date(item.action.timeStr).toLocaleString()}</p>
                              </Card>
                            </Col>
    
                            )
                          })
                        }
                       
                      </Row>
                        :    
                        <div>

                        <h3 style={{textAlign : "center", paddingTop : 30}}>No pending store requests</h3>
                        <hr/> </div>

                        : null
                      }

{  
          
   role == 1 || role == 4 || role == 5 || role == 6 || role ==  7 ?
 
    this.state.PaymentWorkflows != null && this.state.PaymentWorkflows.length > 0 ?
                        <Row gutter={16}>
                          <h3 style={{textAlign : "center", paddingTop : 30}}>Payment Requests</h3>
                        {
                          this.state.PaymentWorkflows.map(function(item){
                            return (
                              <Col span={8} style={{marginTop: 15}}>
                              <Card title="Payment Request"
                              extra = {
                                GetPaymentViewLink(item.id)
                              }
                              bordered={false}>
                              <p>Initiator : {item.action.username}</p>
                              <p>Description : {item.description}</p>
                              <p>{new Date(item.action.timeStr).toLocaleString()}</p>
                              </Card>
                            </Col>
    
                            )
                          })
                        }
                       
                      </Row>
                        : 
                        <div>

                        <h3 style={{textAlign : "center", paddingTop : 30}}>No Pending Payment Request</h3>
                        <hr/> </div>

                     : null


                      }

{  
          
  role == 8 || role == 9 || role == 10  ?
        
           this.state.PurchaseWorkflows != null && this.state.PurchaseWorkflows.length > 0 ?
                               <Row gutter={16}>
                                 <h3 style={{textAlign : "center", paddingTop : 30}}>Purchase Requests</h3>
                               {
                                 this.state.PurchaseWorkflows.map(function(item){
                                   return (
                                     <Col span={8} style={{marginTop: 15}}>
                                     <Card title="Purchase Request"
                                     extra = {
                                       GetPurchaseActionLink(item.id,item.currentState)
                                     }
                                     bordered={false}>
                                     <p>Initiator : {item.action.username}</p>
                                     <p>Description : {item.description}</p>
                                     <p>{new Date(item.action.timeStr).toLocaleString()}</p>
                                     </Card>
                                   </Col>
           
                                   )
                                 })
                               }
                              
                             </Row>
                               : 
                               <div>
       
                               <h3 style={{textAlign : "center", paddingTop : 30}}>No Pending Purchase Requests</h3>
                               <hr/> </div>
       
                            : null
       
       
                             }
              </div>
         

         )


      }
}



// function mapStateToProps(state) {
//   return {
//     authenticated: state.auth.authenticated,
//     session: state.auth.session,
//     onLoad: state.load.loading
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     auth: bindActionCreators(authActionCreators, dispatch),
//     loading: bindActionCreators(loadActionCreators, dispatch)
//   };
// }


export default (Home);
