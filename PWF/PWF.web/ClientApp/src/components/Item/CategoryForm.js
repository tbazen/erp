import React from 'react';
import { Menu, Icon } from 'antd';
//import 'antd/dist/antd.min.css';
import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select, DatePicker , Checkbox} from 'antd';
import { Element } from 'react-scroll';
import AccountSelect from '../miniComponents/AccountSelect';


class CategoryForm extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            showExpense : false,
            showSales : false,
            showFixed : false,
            showInventory: false,
            rootCategory: true,
            categoryID : ''
        }

        this.handleCategoryFormSubmit = this.handleCategoryFormSubmit.bind(this);
        this.onTick = this.onTick.bind(this);
        this.showAll = this.showAll.bind(this);
    }

    componentWillMount() {
        this.setState({
            rootCategory: this.props.root,
            categoryID: this.props.categoryID
        });

    }



    showAll(){
        return !(this.state.showExpense || this.state.showSales || this.showAll.showFixed || this.state.showInventory);
    }

    handleCategoryFormSubmit(e) {
        e.preventDefault();
        var self = this;
        var service = new StoreService(this.props.session.token);
        this.props.form.validateFieldsAndScroll((err, values) => {
            Object.keys(values).forEach(function(key){
                if(key.search("PID") != -1){
                    values[key] = values[key].value;
                }
            });
            values.id = -1;
            if (!this.props.root) {
                values.PID = this.props.categoryID;
            }
            console.log(values);
            service.RegisterItemCategory(values).then(function (response) {
                console.log(response);
                this.props.toggle_drawer("Category");
                this.props.history.push('/item');
            }).catch(function(error){
                console.log(error);
            })
        });

    }


    onTick(checkedValues){
        console.log('checked = ', checkedValues);
        this.setState({
            showExpense: checkedValues.indexOf("expense") != -1,
            showSales: checkedValues.indexOf("sales") != -1,
            showFixed: checkedValues.indexOf("fixed") != -1,
            showInventory: checkedValues.indexOf("inventory") != -1,
        })

        console.log(this.state);
        console.log(this.showAll());
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 10 },
            wrapperCol: { span: 14 },
            style : {
                marginBottom : 0,
                paddingBottom : 0
            }
        };

        const typeOptions = [
            {label : "Expense Item",  value : 'expense'},
            {label : "Sales Item", value : 'sales'},
            {label : "Fixed Asset Item", value : 'fixed'},
            {label : "Inventory Item", value : "inventory"}
        ];
        return(
            <Form layout="vertical" hideRequiredMark
                onSubmit={this.handleCategoryFormSubmit}
            >
                <h3>Add Item Category</h3>
                <Row>
                    <Col span={24}>

                        <Checkbox.Group style={{ width: '100%' }} onChange={this.onTick}>
                            <Row>
                                { typeOptions.map(function(item){
                                    return (<Col span={6}><Checkbox value={item.value}>{item.label}</Checkbox></Col>)
                                })}

                            </Row>
                        </Checkbox.Group>
                        <br /><br /><br /><br />
                        <Form.Item label="Category Name"
                        {...formItemLayout}>
                        {
                            getFieldDecorator('description',{ 
                                rules :  [{
                                    required : true, message : 'Please input category name'
                                }]
                            })(
                                <Input />
                            )
                        }
                        </Form.Item>
                        <Form.Item label="Category Code"
                            {...formItemLayout}>
                            {
                                getFieldDecorator('code', {
                                   
                                    rules: [{
                                        required: true, message: 'Please input category Code'
                                    }]
                                })(
                                    <Input />
                                )
                            }
                        </Form.Item>

                        {
                            !this.props.root ?  
                                <div>
                                    <Form.Item label="Parent Category"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('PID', {
                                            initialValue: this.props.categoryID,
                                            rules: [],
                                        })(<Input  />)}

                                    </Form.Item>
                                </div>
                                : null
                        }
                        <br /><br /><br /><br />
                        {
                            this.state.showExpense || this.showAll() ? 
                            <div>
                                <Form.Item label="Expense Account"

                                    {...formItemLayout}
                                >
                                    {getFieldDecorator('expenseAccountPID', {
                                        rules: [],
                                    })(<AccountSelect {...this.props} />)}

                                </Form.Item>
                                <Form.Item label="Expense Summary Account"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator('expenseSummaryAccountPID', {
                                        rules: [],
                                    })(<AccountSelect {...this.props} />)}
                                </Form.Item>
                                <Form.Item label="Direct Cost Account"

                                    {...formItemLayout}
                                >
                                    {getFieldDecorator('directCostAccountPID', {
                                        rules: [],
                                    })(<AccountSelect {...this.props} />)}

                                </Form.Item>
                                <Form.Item label="Direct Cost Summary Account"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator('directCostSummaryAccountPID', {
                                        rules: [],
                                    })(<AccountSelect {...this.props} />)}
                                </Form.Item>
                                <Form.Item label="PrePaid Expense Account"

                                    {...formItemLayout}
                                >
                                    {getFieldDecorator('prePaidExpenseAccountPID', {
                                        rules: [],
                                    })(<AccountSelect {...this.props} />)}

                                </Form.Item>
                                <Form.Item label="PrePaid Expense Summary Account"
                                    {...formItemLayout}
                                >
                                    {getFieldDecorator('prePaidExpenseSummaryAccountPID', {
                                        rules: [],
                                    })(<AccountSelect {...this.props} />)}
                                </Form.Item>
                                </div>
                            : null
                        }

                        {
                            this.state.showSales || this.showAll() ?
                            <div>
                                    <Form.Item label="Income Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('salesAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Income Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('salesSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                    <Form.Item label="Unearned Revenue Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('unearnedRevenueAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Unearned Revenue Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('unearnedSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                    <Form.Item label="Finished Work Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('finishedWorkAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Finished Work Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('finishedWorkSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                            </div> : null
                        }
                        
                       {
                           this.state.showFixed || this.state.showInventory || this.showAll() ?
                           <div>
                                    <Form.Item label="Inventory Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('generalInventoryAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Inventory Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('generalInventorySummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                    <Form.Item label="Finished Good Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('finishedGoodsAcountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Finished Good Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('finishedGoodsSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                           </div> : null
                        }{
                            this.state.showFixed || this.showAll() ?
                                <div>
                                    <Form.Item label="Original Fixed Asset Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('originalFixedAssetAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Original Fixed Asset Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('originalFixedAssetSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                    <Form.Item label="Depreciation Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('depreciationAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Depreciation Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('depreciationSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                    <Form.Item label="Accumulated Depreciation Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('accumulatedDepreciationAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Accumulated Depreciation Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('accumulatedDepreciattionSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}



                                    </Form.Item>
                                </div> : null
                        }


                        {/* <Form.Item label="Finished Work Account"

                            {...formItemLayout}
                        >
                            {getFieldDecorator('finishedWorkAccountPID', {
                                rules: [],
                            })(<AccountSelect {...this.props} />)}

                        </Form.Item>
                        <Form.Item label="Finished Work Summary Account"
                            {...formItemLayout}
                        >
                            {getFieldDecorator('finishedWorkSummaryAccountID', {
                                rules: [],
                            })(<AccountSelect {...this.props} />)}
                        </Form.Item> */}

                        

                        {
                            this.state.showFixed || this.state.showInventory || this.showAll() ?
                                <div>
                                    <Form.Item label="Pending Order Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('pendingOrderAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Pending Order Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('pendingOrderSummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                    <Form.Item label="Pending Delivery Account"

                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('pendingDeliveryAccountPID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}

                                    </Form.Item>
                                    <Form.Item label="Pending Delivery Summary Account"
                                        {...formItemLayout}
                                    >
                                        {getFieldDecorator('pendingDeliverySummaryAccountID', {
                                            rules: [],
                                        })(<AccountSelect {...this.props} />)}
                                    </Form.Item>
                                </div> : null
                        }


                      
                    </Col>
                </Row>
                <div
                    style={{
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        width: '100%',
                        borderTop: '1px solid #e9e9e9',
                        padding: '10px 16px',
                        background: '#fff',
                        textAlign: 'right',
                    }}
                >
                    <Button onClick={() => this.props.toggle_drawer("Category")} style={{ marginRight: 8 }}>
                        Cancel
            </Button>
                    <Button htmlType="submit" type="primary">
                        Submit
            </Button>
                </div>
            </Form>
        )
    }
}


export default Form.create()(CategoryForm);