import React from 'react';

import { StoreService } from '../../service/store.service';
import { Tree, Input,InputNumber, Card, Row, Col, Table, Button, Drawer, Form, Select, DatePicker, Checkbox, Icon } from 'antd';
import 'antd/dist/antd.min.css';
import '../../assets/css/style.css';
import CostCenterSelect from '../miniComponents/CostCenterSelect';
import UnitSelect from '../miniComponents/UnitSelect';
import SweetAlert from 'react-bootstrap-sweetalert';

const { Option } = Select;

class IssueItemsListForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
 
            rootCategory: true,
            showAlert: false,
            categoryID: '',
            expenseTypes: {},
            serviceTypes: {},
            inventoryTypes: {},
            goodTypes: {},
            depreciationType: [],
            measureUnits: [],
            checkedValues: [],
            hasFixedUnitPrice: false,
            ApprovedItems: [],
            Reorganized : [],
        }

        this.checkUnit = this.checkUnit.bind(this);


    }

    componentDidMount() {
        var Service = new StoreService(this.props.session.token);
        var self = this;
        var reorg = self.props.Reorganized;
        self.setState({
            Reorganized: []
        });
        reorg.map(function (item, index) {
            var balance = Service.GetAccountBalance(item.code, self.props.storeID).then(function (result) {
                item.balance = result.data.totalQuantity;
                item.unitPrice = (Number(result.data.totalValue) / Number(result.data.totalQuantity)).toFixed(2);
                console.log(result.data);
            }).catch(function (error) {
                console.log(error);
            }).then(function () {
                self.setState({
                    Reorganized: [...self.state.Reorganized, item]
                });


                console.log(self.state);
            })
        })
    }

    componentWillReceiveProps(nextProps){
        var self = this;
        var Service = new StoreService(this.props.session.token);
        if(nextProps.storeID != this.props.storeID){
            self.setState({
                Reorganized : []
            });
            self.state.Reorganized.map(function (item, index) {
                var balance = Service.GetAccountBalance(item.code, nextProps.storeID).then(function (result) {
                    item.balance = result.data.totalQuantity;
                    item.unitPrice = (Number(result.data.totalValue) / Number(result.data.totalQuantity)).toFixed(2);
                    console.log(result.data);
                }).catch(function (error) {
                    console.log(error);
                }).then(function () {
                    self.setState({
                        Reorganized: [...self.state.Reorganized, item]
                    });
    
    
                    console.log(self.state);
                })
            })
        }
    }

    
    checkUnit = (rule,value,callback) => {
        console.log(value);
        if(value.value != "" || value.value != undefined){
          callback();
          return;
        }
        callback("Select Unit");

    }

    removeItemRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        // We need at least one passenger
        if (itemKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          itemKeys: itemKeys.filter(key => key !== k),
        });
      }

    render() {

        var self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {

            //labelCol: { span: 10 },
            //wrapperCol: { span: 14 },
            style: {
                marginBottom: 0,
                paddingBottom: 0
            }
        };

        const getCustomName = (prop, code) => {
            return prop + code;
        }

        const getLabel = (name,index) => {
            return index > 0 ? null : name;
        }

        return (
            <Form onSubmit={this.handleItemSubmit} layout="horizontal">
                <SweetAlert title="Alert!"
                    warning
                    confirmBtnText="Ok"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    onConfirm={() => {
                        this.setState({
                            showAlert: false
                        });
                    }}
                    show={this.state.showAlert}>
                    The item quantity is more than approved quantity?
                        </SweetAlert>
               
                {
                    

                    this.state.Reorganized.map(function(item,index){
                        
                        return <Row gutter={24}>
                            <Col span={5}>
                                <Form.Item label={getLabel("Code",index)}
                                    {...formItemLayout}
                                >
                                    {
                                        getFieldDecorator(getCustomName("code",item.code), {
                                            rules: [{
                                                required: true, message: 'Please input item code'
                                            }],
                                            initialValue : item.code
                                        
                                        })(
                                            <Select placeholder="Select Item ..."
                                                onSelect={self.codeSelect}
                                                disabled={true}
                                                
                                            //className="codeDropDown"
                                            //style = {{ width : "inherit"}}
                                            // /dropdownStyle={{ maxHeight: 400, overflow: 'auto', width: '300px !important' }}
                                            >
                                                {
                                                    self.props.Reorganized.map(function (item) {
                                                        return <Option value={item.code}>{item.name}</Option>
                                                    })
                                                }
                                            </Select>
                                        )
                                    }
                                </Form.Item>



                            </Col>






                            <Col span={3}>
                                <Form.Item label={getLabel("Current Balance", index)}
                                    labelCol="10"

                                >
                                    { 
                                        getFieldDecorator(getCustomName('balance',item.code), {
                                            rules: [{
                                                required: true, message: 'Please input balance'
                                            }],
                                            
                                            initialValue : item.balance
                                           // initialValue : price[index][0]
                                        })(
                                            <Input  disabled/>
                                        )
                                    }
                                </Form.Item>



                            </Col>
                            <Col span={3}>
                                <Form.Item label={getLabel("Quantity", index)}
                                    {...formItemLayout}>
                                    {
                                        getFieldDecorator(getCustomName('quantity',item.code), {
                                            rules: [{
                                                required: true, message: 'Please input quantity'
                                            },
                                        ]
                                        })(
                                            <Input />
                                        )
                                    }
                                </Form.Item>



                            </Col>
                            <Col span={3}>
                                <Form.Item label={getLabel("Unit", index)}
                                    {...formItemLayout}>
                                    {
                                        getFieldDecorator(getCustomName('unitID',item.code), {
                                            rules : [{ validator : self.checkUnit}]
                                        })(
                                            <UnitSelect {...self.props} />
                                        )
                                    }
                                </Form.Item>



                            </Col>
                            <Col span={3}>
                                <Form.Item label={getLabel("Unit Price", index)}
                                    {...formItemLayout}>
                                    {
                                        getFieldDecorator(getCustomName('unitPrice',item.code), {
                                            rules: [{
                                                required: true, message: ''
                                            }],
                                            initialValue: item.unitPrice
                                        })(
                                            <Input disabled />
                                        )
                                    }
                                </Form.Item>



                            </Col>
                            <Col span={3}>
                                <Form.Item label={getLabel("Expense Fixed", index)}
                                    {...formItemLayout}>
                                    {
                                        getFieldDecorator(getCustomName('fixedExpense',item.code), {
                                            rules: []
                                        })(
                                            <Checkbox />
                                        )
                                    }
                                </Form.Item>



                            </Col>
                            {/* <Col span={2}>
                            <Form.Item 
                            label={getLabel(" ", index)}
                                >
                                      <Icon
                                className="dynamic-delete-button"
                                type="minus-circle-o"
                                disabled={self.state.Reorganized.length === 1}
                                onClick={() => this.removeItemRow(index)}
                                style={{display: 'inline-block', width : '10px'}}
                            />
                                </Form.Item>
                         
                            </Col> */}
                        </Row>
                    })
                    
                }


            </Form>
        )
    }

}

export default Form.create()(IssueItemsListForm);