import React from 'react';
import { Menu, Icon } from 'antd';
import 'antd/dist/antd.min.css';
import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select,DatePicker } from 'antd';
import { Element } from 'react-scroll';
import AccountSelect from '../miniComponents/AccountSelect';
import CategoryFrom from './CategoryForm';
import ItemForm from './ItemForm';

const { Option } = Select;


const TreeNode = Tree.TreeNode;
const Search = Input.Search;

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

function accountToNode(data) {
    const arr = [];
    data.map(function (item, index) {
        var el = {
            name: item.nameCode,
            id: item.id,
            key : item.id,
            title: item.nameCode,
            value: item.id,
            pid: item.pid,
        };
        if (item.childCount > 0) {
            el.isLeaf = false;
            el.selectable = false;
            el.children = [];
        }
        else {
            el.isLeaf = true;
            el.selectable = true;
            //el.children = null;
        }
        arr.push(el);
    });
    return arr;
}

class ItemBrowser extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            Categories : [],
            Items : [],
            categoryDrawer : false,
            itemDrawer: false,
            subCategoryForm: false,
            rootCategory: true,
            categoryID : '',
        }
        this.list_to_tree = this.list_to_tree.bind(this);
        this.renderTreeNodes = this.renderTreeNodes.bind(this);
        this.toggle_drawer = this.toggle_drawer.bind(this);
        this.handleCategoryFormSubmit = this.handleCategoryFormSubmit.bind(this);
    }

    componentDidMount(){
        var self = this;
        var Service = new StoreService(this.props.session.token);
        Service.GetItemCategories().then(function(response){
            var Categories = response.data;
            self.setState({
                Categories
            });
            console.log(response.data);

        });
    }

    handleCategoryFormSubmit(e){
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
                console.log('Received values of form: ', values);
        });

    }

    toggle_drawer(type){
        if(type == "Category"){
            var val = !this.state.categoryDrawer;
            this.setState({
                categoryDrawer : val
            });
        }
        if(type == "Item"){
            var val = !this.state.itemDrawer;
            this.setState({
                itemDrawer : val
            });
        }
    }

    list_to_tree = (list) => {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i; // initialize the map
            list[i].children = []; // initialize the children
        }

        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.pid != -1) {
                // if you have dangling branches check that map[node.parentId] exists
                if (list[map[node.pid]] != undefined){
                    list[map[node.pid]].children.push(node);
                }
                
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    renderTreeNodes = (data) => {
        return data.map((item) => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item} className="treeNode">
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} dataRef={item} />;
        });
    }


    handleClick = (e) => {
        var catID = e[0];
        var self = this;
        var Service = new StoreService(this.props.session.token);
        Service.GetItemsInCategory(catID).then(function(response){
            var data = response.data;
            self.setState({
                Items: data,
                categoryID : catID
            });
            console.log(data);
        });
    }

    openCategortDrawer = (type) => {
        if (type == "New") {
            this.setState({
                rootCategory : true
            })
        };
        if (type == "Sub") {
            this.setState({
                rootCategory : false
            })
        }
        this.toggle_drawer("Category");
    }
    render() {

        const { getFieldDecorator } = this.props.form;

        const typeRenderer = (text,record) => {
            var direct = record.isDirectCostItem ? (' Direct Cost |') : (' ');
            var expense = record.isExpenseItem ? (' Expense |') : (' ');
            var fixed = record.isFixedAssetItem ? (' Fixed Asset |') : (' ');
            var sales = record.isSalesItem ? (' Sales |') : ( ' ');
            return direct + expense + fixed + sales;
        };

        const itemColumns = [{
            title : 'Code',
            dataIndex  : 'code',
            key: 'code'
        }, {
            title : "Name",
            dataIndex : 'name',
            key : 'name',
        }, {
            title : 'Unit',
            dataIndex: 'measureUnitID',
        }, {
            title : "Type",
            render : typeRenderer
        }];
        const nodeList = accountToNode(this.state.Categories);
        const CatTree = this.list_to_tree(nodeList);
        console.log(CatTree);
        const TreeNodes = this.renderTreeNodes(CatTree);


    
        return (
            <div>
              
            {/* <Row>
                <Col span={8}>
                    <Card >
                        <Button icon="plus-circle" onClick={() => this.openCategortDrawer("New")}>Add Category</Button>
                            <Button icon="plus-circle" onClick={() => this.openCategortDrawer("Sub")}>Add Sub Category</Button>
                    </Card>
                </Col>
                <Col span={16}>
                    <Card >
                            <Button icon="plus-circle" onClick={() => this.toggle_drawer("Item")}>Add Item</Button>
                            <Button icon="plus-circle" onClick={() => this.toggle_drawer("Item")}>Edit Item</Button>
                        <Button type="danger" icon="delete" >Delete</Button>
                    </Card>
                </Col>
            </Row> */}
            <Row>
                <Col span={8}>
                    <Card title="Item Categories">
                        <Element 
                            className="element" id="containerElement" style={{
                               
                                height: '500px',
                                overflow: 'scroll',
                                }}
                        >
                            <Tree
                                style={{ width: 300 }}
                                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                onSelect={this.handleClick}
                            >
                                {TreeNodes}
                            </Tree>
                        </Element>

                    </Card>
                </Col>
                <Col span={16}>
                <Card title="Items List">

                    <Table dataSource={this.state.Items}
                    columns={itemColumns} 
                    pagination={false}
                    scroll={{y : 500}}
                    />

                    </Card>
                </Col>
            </Row>
           
                <Drawer
                    title="Add Item Category"
                    width={720}
                    onClose={() => this.toggle_drawer("Category")}
                    visible={this.state.categoryDrawer}
                    style={{
                        overflow: 'auto',
                        height: 'calc(100% - 108px)',
                        paddingBottom: '108px',
                    }}
                >
                    <CategoryFrom {...this.props} toggle_drawer={this.toggle_drawer} root={this.state.rootCategory} categoryID={this.state.categoryID}
                        
                    />

                </Drawer>
                <Drawer
                    title="Add Item"
                    width={720}
                    onClose={() => this.toggle_drawer("Item")}
                    visible={this.state.itemDrawer}
                    style={{
                        overflow: 'auto',
                        height: 'calc(100% - 108px)',
                        paddingBottom: '108px',
                    }}
                >
                    <ItemForm {...this.props} toggle_drawer={this.toggle_drawer} categoryID={this.state.categoryID}

                    />

                </Drawer>
            </div>
           
            // <Menu
            //     onClick={this.handleClick}
            //     style={{ width: 240 }}
                
            //     //defaultSelectedKeys={['3']}
            //     //defaultOpenKeys={['sub1']}
            //     mode="inline"
            // >
            //    {MenuItems}
                        
            // </Menu>
        );
    }
}

export default Form.create()(ItemBrowser);