import React from 'react';

import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select, DatePicker, Checkbox } from 'antd';

const { Option } = Select;

class ItemForm extends React.Component {
    
    constructor(props){
        super(props);

        this.state = {
            showExpense: false,
            showSales: false,
            showFixed: false,
            showInventory: false,
            rootCategory: true,
            categoryID: '',
            expenseTypes : {},
            serviceTypes : {},
            inventoryTypes : {},
            goodTypes : {},
            depreciationType : [],
            measureUnits : [],
            checkedValues : [],
            hasFixedUnitPrice : false,
        }
        this.onTick = this.onTick.bind(this);
        this.showAll = this.showAll.bind(this);
        this.handleItemSubmit = this.handleItemSubmit.bind(this);
    }

    componentDidMount(){
        var service = new StoreService(this.props.session.token);
        var self = this;
        service.GetServiceTypes().then(function(response){
            self.setState({
                serviceTypes : response.data
            });
        });
        service.GetExpenseTypes().then(function(response){
            console.log(response);
            self.setState({
                expenseTypes : response.data
            });
        });
        service.GetInventoryTypes().then(function(response){
            self.setState({
                inventoryTypes : response.data
            });
        });
        service.GetGoodTypes().then(function(response){
            self.setState({
                goodTypes : response.data
            });
        });
        service.GetAllMeasureUnit().then(function(response){
            self.setState({
                measureUnits : response.data
            });
        })
        service.GetAllFixedAssetRuleAttributes().then(function(response){
            self.setState({
                depreciationType : response.data
            });
        })

        console.log(self.state);
    }

    showAll() {
        return !(this.state.showExpense || this.state.showSales || this.showAll.showFixed || this.state.showInventory);
    }

    onTick(checkedValues) {
        console.log('checked = ', checkedValues);
        var fixedIndex = checkedValues.indexOf("fixed");
        var inventoryIndex = checkedValues.indexOf("inventory");
        console.log(fixedIndex);
        console.log(inventoryIndex);
        if(fixedIndex != -1 && inventoryIndex != -1){
            if(fixedIndex > inventoryIndex){
                checkedValues = checkedValues.filter(function(item){
                    return item != "inventory";
                })
            }
            else{
                checkedValues = checkedValues.filter(function (item) {
                    return item != "fixed";
                })
            }
        }
        
        this.setState({
            showExpense: checkedValues.indexOf("expense") != -1,
            showSales: checkedValues.indexOf("sales") != -1,
            showFixed: checkedValues.indexOf("fixed") != -1,
            showInventory: checkedValues.indexOf("inventory") != -1,
            checkedValues : checkedValues,
        })

        console.log(this.state);
        console.log(this.showAll());
    }

    handleItemSubmit(e) {
        e.preventDefault();
        var self = this;
        var service= new StoreService(this.props.session.token);

        this.props.form.validateFieldsAndScroll((err,values) => {
            values.categoryID = this.props.categoryID;
            values.isInventoryItem = this.state.showInventory;
            values.isExpenseItem = this.state.showExpense;
            values.isSalesItem = this.state.showSales;
            values.isFixedAssetItem = this.state.showFixed;
            console.log(values);
            service.RegisterItem(values).then(function(response){
                self.props.toggle_drawer("Item");
                console.log(response);
            }).catch(function(error){
                console.log(error);
            })
        });

    }

    render(){

        var self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 10 },
            wrapperCol: { span: 14 },
            style: {
                marginBottom: 0,
                paddingBottom: 0
            }
        };

        const typeOptions = [
            { label: "Expense Item", value: 'expense' },
            { label: "Sales Item", value: 'sales' },
            { label: "Fixed Asset Item", value: 'fixed' },
            { label: "Inventory Item", value: "inventory" }
        ];

        return(
            <Form layout="vertical" onSubmit={this.handleItemSubmit}>
                <h3>Add Item</h3>
             <Row>
                 <Col span={24}>

                                <Checkbox.Group style={{ width: '100%' }} onChange={this.onTick} value={this.state.checkedValues}>
                                    <Row>
                                        {typeOptions.map(function (item) {
                                            return (<Col span={6}><Checkbox value={item.value}>{item.label}</Checkbox></Col>)
                                        })}

                                    </Row>
                                </Checkbox.Group>

                        <br /><br /><br /><br />

                        <Form.Item label="Code"
                        {...formItemLayout}>
                        {
                            getFieldDecorator('code',{
                                rules : [{
                                    required : true, message : 'Please input item code'
                                }]
                            })(
                                <Input />
                            )
                        }
                        </Form.Item> 

                        <Form.Item label="Name"
                            {...formItemLayout}>
                            {
                                getFieldDecorator('name', {
                                    rules: [{
                                        required: true, message: 'Please input item name'
                                    }]
                                })(
                                    <Input />
                                )
                            }
                        </Form.Item>        
                        <br /><br />
                        {
                            this.state.showExpense || this.showAll() ?  
                            <div>
                                    <Form.Item label="Expense Type" {...formItemLayout}>
                                        {
                                            getFieldDecorator("isDirectCost", {
                                                rules: []
                                            })(
                                                <Select placeholder="Select Expense Type">
                                                    <Option value={true}>Directly related to Production or Service</Option>
                                                    <Option value={false}>Not directly related to Production or Service</Option>

                                                </Select>
                                            )
                                        }
                                    </Form.Item>
                                    <Form.Item label="Declare In Vat Form As" {...formItemLayout}>
                                        {
                                            getFieldDecorator("expenseType", {
                                                rules: []
                                            })(
                                                <Select placeholder="Select Form Type ...">
                                                    <Option value="2">Value of Local Purchase Input</Option>
                                                    <Option value="1">Value of General Purchase Input</Option>
                                                    <Option value="3">Unclaimed Input</Option>
                                                    <Option value="4">Don't Declare in Vat Fomr (Don't report  to ERCA)</Option>

                                                </Select>
                                            )
                                        }


                                    </Form.Item>
                            </div>
                              : 
                        null
                        }
                       
                        <Form.Item label="Tax Status" {...formItemLayout}>
                            {
                                getFieldDecorator("taxStatus", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Item Type ...">
                                        <Option value="1">Taxable</Option>
                                        <Option value="2">Non-Taxable</Option>
                                    </Select>
                                )
                            }


                        </Form.Item>
                        {
                            this.showAll() || this.state.showSales ?
                                <Form.Item label="Item has fixed unit price" {...formItemLayout}>
                                <Checkbox
                                    onChange={(e) => {

                                            this.setState({
                                                hasFixedUnitPrice : e.target.checked
                                            })

                                    }}
                                ></Checkbox>
                            </Form.Item>  : null
                        }

                        {
                            this.state.showSales && this.state.hasFixedUnitPrice ?
                            <Form.Item label="Unit Price" {...formItemLayout}>
                            {
                                getFieldDecorator("fixedUnitPrice",{
                                    rules : []
                                })(
                                    <Input />
                                )
                            }
                                
                            </Form.Item> : null
                        }


                        <Form.Item label="Item Type" {...formItemLayout}>
                            {
                                getFieldDecorator("goodOrService", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Item Type ...">
                                        <Option value="1">Good</Option>
                                        <Option value="2">Service</Option>
                                    </Select>
                                )
                            }


                        </Form.Item>
                        <Form.Item label="Good Type" {...formItemLayout}>
                            {
                                getFieldDecorator("goodType", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Good Type ...">
                                        {
                                            Object.keys(this.state.goodTypes).map(function (key) {
                                                return <Option value={self.state.goodTypes[key]}>{key}</Option>
                                            })
                                        }
                                    </Select>
                                )
                            }


                        </Form.Item>
                        <Form.Item label="Service Type" {...formItemLayout}>
                            {
                                getFieldDecorator("serviceType", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Service Type ...">
                                        <Option value="0">None</Option>
                                        <Option value="1">Contractors</Option>
                                        <Option value="2">Mill Services</Option>
                                        <Option value="3">Tractors And Combined Harvesters</Option>
                                    </Select>
                                )
                            }


                        </Form.Item>
                        <Form.Item label="Measure Unit" {...formItemLayout}>
                            {
                                getFieldDecorator("measureUnitID", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Measure Unit ...">
                                        {
                                            
                                            this.state.measureUnits.map(function (item) {
                                                console.log(self.state.measureUnits);
                                                return <Option value={item.id}>{item.name }</Option>
                                            })
                                        }
                                    </Select>
                                )
                            }


                        </Form.Item>
                        {
                            this.showAll() || this.state.showFixed || this.state.showInventory ? 
                             <Form.Item label="Inventory Type" {...formItemLayout}>
                            {
                                getFieldDecorator("inventoryType", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Service Type ...">
                                        <Option value="0">General Inventory</Option>
                                        <Option value="1">Finished Goods</Option>

                                    </Select>
                                )
                            }


                        </Form.Item> : null
                        }
                        {
                            this.showAll() || this.state.showFixed  ? 
                            
                            <Form.Item label="Depreciation Type" {...formItemLayout}>
                            {
                                getFieldDecorator("depreciationType", {
                                    rules: []
                                })(
                                    <Select placeholder="Select Depreciation Type ..." > 
                                    {
                                        this.state.depreciationType.map(function(item){
                                            return <Option value={item.id}>{item.name}</Option>
                                        })
                                    }
                                    </Select>
                                )
                            }


                        </Form.Item> : null
                        }
                        
                        <Form.Item label="Description " {...formItemLayout}>
                            {
                                getFieldDecorator("description", {
                                    rules: []
                                })(
                                    <Input.TextArea />
                                )
                            }


                        </Form.Item>
                 </Col>
             </Row>
                <div
                    style={{
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        width: '100%',
                        borderTop: '1px solid #e9e9e9',
                        padding: '10px 16px',
                        background: '#fff',
                        textAlign: 'right',
                    }}
                >
                    <Button onClick={() => this.props.toggle_drawer("Item")} style={{ marginRight: 8 }}>
                        Cancel
            </Button>
                    <Button htmlType="submit" type="primary">
                        Submit
            </Button>
                </div>
            </Form>
        )
    }

}

export default Form.create()(ItemForm);