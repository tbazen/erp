﻿import React, { Component } from 'react';
import { StoreService } from '../../service/store.service';
import { Navbar, Row, Col, Grid, FormGroup, ControlLabel, FormControl, Button, Form, Tooltip, OverlayTrigger, Overlay, ButtonGroup } from 'react-bootstrap';
import { Card, CardHeader, CardBody, Label, Input, FormText } from 'reactstrap';
import { Table } from 'antd';
import ReactTable from "react-table";
import { Element } from 'react-scroll';

export default class SearchItem extends Component {
    constructor(props) {
        super(props);
        this.Service = new StoreService(this.props.session.token);
        this.state = {
            searchTerm: '',
            searchResult: [],
            selected: '',
            selectedCode: '',
            selectedName: '',

        }
        this.handleChange = this.handleChange.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.isActive = this.isActive.bind(this);
    }

    isActive() {
        return this.state.searchResult.length > 0;
    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        if (target.name == "description") {
            this.setState({
                searchResult: [],
                selected: '',
                selectedCode: '',
                selectedName: '',
                searchTerm: ''
            });
        }
        if (target.name == "searchTerm") {
            this.setState({
                description: '',
                addWithDescription: true
            })
        }
    }

    onKeyPress = (event) => {

        const self = this;
        if (event.key == "Enter") {
            //self.props.start();
            this.Service.SearchStoreItems(this.state.searchTerm).then(function (data) {
                console.log(data);
                self.setState({
                    searchResult: data.data,
                    selected: '',
                    selectedCode: '',
                    selectedName: ''
                });
            }).catch(function (error) {
                console.log(error);
            }).then(() => {
                console.log(this.state);
                //this.props.stop();

            })
        };
    }

    render() {
        const rowStyle = {
            "paddingTop": 15
        }

        const columns = [{
            Header: 'Code',
            accessor: 'code',

        }, {
            Header: 'Name',
            accessor: 'name'
        }];

        const col = [{
            title : "Code",
            dataIndex : "code",
        
        }, {
            title : "Name",
            dataIndex : "name"
        }];

        const cartColumns = [{
            Header: 'Code',
            accessor: 'code',

        }, {
            Header: 'Name',
            accessor: 'name'
        },
        {
            Header: 'Quantity',
            accessor: 'quantity'
        }
        ]
        return (
<div>
                    <FormGroup controlId="searchTerm">
                        <ControlLabel>
                            Search Items / Services
                            </ControlLabel>
                            <Col span={8}>
                            <FormControl name="searchTerm" type="text" value={this.state.searchTerm} onKeyPress={this.onKeyPress} onChange={(e) => this.handleChange(e)} placeholder="Enter Search term here" />

                            </Col>
                    </FormGroup>
                    {this.props.children}

                    <Element
                        className="element" id="containerElement" style={{

                          //  height: '500px',
                            overflow: 'scroll',
                        }}
                    >


                    <Table
                    dataSource = {this.state.searchResult}
                    columns = {col}
                    rowClassName="search_row"
                   // className="search_item"
                    onRowClick={(record,index,event) => {
                        this.setState({
                            selected: index,
                            selectedCode: record.code,
                            selectedName: record.name
                        });
                        this.props.selectItem(index, record.code, record.name);
                        var root = event.target.parentElement.parentElement;
                        console.log(root.childNodes);
                        root.childNodes.forEach(el => {
                            if(el.dataset.rowKey == index){
                                el.classList.add("highlighted_row");
                            }
                            else{
                                el.classList.remove("highlighted_row");
                            }
                        });
                    }}
                    />

                    {/* <ReactTable
                        
                        data={this.state.searchResult}
                        columns={columns}
                        
                        noDataText={"No items Found"}
                            showPagination={this.state.searchResult.length > 10 ? true : false}
                        defaultPageSize={this.state.searchResult.length > 10 ? -1 : 10}
                        getTrProps={(state, rowInfo) => {

                            if (rowInfo && rowInfo.row) {
                                return {
                                    onClick: (e) => {

                                        this.setState({
                                            selected: rowInfo.index,
                                            selectedCode: rowInfo.row.code,
                                            selectedName: rowInfo.row.name
                                        });
                                        this.props.selectItem(rowInfo.index, rowInfo.row.code, rowInfo.row.name);
                                    },
                                    style: {
                                        background: rowInfo.index === this.state.selected ? '#00afec' : 'white',
                                        color: rowInfo.index === this.state.selected ? 'white' : 'black'
                                    }

                                }
                            } else {
                                return {}
                            }
                        }}
                    /> */}
                </Element></div>

                               )
    }
}