import React from 'react';

import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select, DatePicker, Checkbox } from 'antd';
import 'antd/dist/antd.min.css';
import '../../assets/css/style.css';
import CostCenterSelect from '../miniComponents/CostCenterSelect';
import UnitSelect from '../miniComponents/UnitSelect';
import SweetAlert from 'react-bootstrap-sweetalert';

const { Option } = Select;

class TransactionDocumentItemForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showExpense: false,
            showSales: false,
            showFixed: false,
            showInventory: false,
            rootCategory: true,
            showAlert : false,
            categoryID: '',
            expenseTypes: {},
            serviceTypes: {},
            inventoryTypes: {},
            goodTypes: {},
            depreciationType: [],
            measureUnits: [],
            checkedValues: [],
            hasFixedUnitPrice: false,
           
        }
       
        this.showAll = this.showAll.bind(this);
        this.handleItemSubmit = this.handleItemSubmit.bind(this);
    }

 

    showAll() {
        return !(this.state.showExpense || this.state.showSales || this.showAll.showFixed || this.state.showInventory);
    }




    handleItemSubmit(e) {
        e.preventDefault();
        var self = this;
        var service = new StoreService(this.props.session.token);

        

        this.props.form.validateFieldsAndScroll((err, values) => {
            var approvedQuantity = self.props.Reorganized.filter(function(item){
                return item.code == values.code;
            })[0].quantity;
            var issuedItems = self.props.issued.filter(function(item){
                return item.code = values.code;
            });

            Array.prototype.sum = function (prop) {
                var total = 0
                for (var i = 0, _len = this.length; i < _len; i++) {
                    total += Number(this[i][prop])
                }
                return total
            }

            var totalIssued = issuedItems.sum("quantity");
            console.log(totalIssued);
            console.log(approvedQuantity);
            var allowedQuantity = approvedQuantity - totalIssued;
            if(Number(values.quantity) > Number(allowedQuantity)){
                this.setState({
                    showAlert : true
                });
            }
            else{
                var costCenter = values.costCenterID;
                var unit = values.unitID;
                values.costCenterID = this.props.session.costCenterID;
                values.unitID = values.unitID.value;
                //console.log(values);
                this.props.addToCart(values);
                this.props.form.resetFields();
                this.props.form.setFieldsValue({
                    costCenterID: costCenter,
                    unitID: unit
                });
            }

        });
        

        
    }

    codeSelect = (value,node,extra) =>{
        console.log(value);
        console.log(node);
        console.log(extra);
        var name = this.props.Reorganized.filter(function(item){
            return item.code == value;
        })[0].name;
        this.props.form.setFieldsValue({
            name : name
        });
        var self = this;
        var service = new StoreService(this.props.session.token);
        console.log(self.props.storeID());
        service.GetAccountBalance(value,self.props.storeID()).then(function(response){
            var balance = response.data.totalQuantity;
            var totalPrice = response.data.totalValue;
            var unitPrice = balance == 0 ? 0 : Number(totalPrice)/ Number(balance);
            self.props.form.setFieldsValue({
                balance : balance,
                unitPrice : unitPrice.toFixed(2)
            });
            
        })
    }

    render() {

        var self = this;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
        
            //labelCol: { span: 10 },
            //wrapperCol: { span: 14 },
            style: {
                marginBottom: 0,
                paddingBottom: 0
            }
        };



        return (
            <Form  onSubmit={this.handleItemSubmit} layout="horizontal">
                <SweetAlert title="Alert!"
                    warning
                    confirmBtnText="Ok"
                    confirmBtnBsStyle="danger"
                    cancelBtnBsStyle="default"
                    onConfirm={() => {
                        this.setState({
                            showAlert: false
                        });
                    }}
                 show={this.state.showAlert}>
                    The item quantity is more than approved quantity?
                        </SweetAlert>
                <Row gutter={24}> 
                    <Col span={6}>
                        <Form.Item label="Code"
                            {...formItemLayout}
                            >
                            {
                                getFieldDecorator('code', {
                                    rules: [{
                                        required: true, message: 'Please input item code'
                                    }]
                                })(
                                    <Select placeholder="Select Item ..."
                                    onSelect={this.codeSelect}
                                    //className="codeDropDown"
                                    //style = {{ width : "inherit"}}
                                        // /dropdownStyle={{ maxHeight: 400, overflow: 'auto', width: '300px !important' }}
                                    >
                                        {
                                            this.props.Reorganized.map(function(item){
                                                return <Option value={item.code}>{item.name }</Option>
                                            })
                                        }
                                    </Select>
                                )
                            }
                        </Form.Item>


                       
                    </Col>
                    

                    
                    <Col span={5} hidden={true}>
                        <Form.Item label="Description"
                            {...formItemLayout}
                        >
                            {
                                getFieldDecorator('name', {
                                    rules: [{
                                        required: true, message: ''
                                    }]
                                })(
                                    <Input disabled/>
                                )
                            }
                        </Form.Item>



                    </Col>
                    {
                        this.props.showCostCenter ? <Col span={4}>
                            <Form.Item label="Accounting Center"
                                {...formItemLayout}>
                                {
                                    getFieldDecorator('costCenterID', {
                                        rules: [{
                                            
                                        }]
                                    })(
                                        <CostCenterSelect {...this.props} />
                                    )
                                }
                            </Form.Item>



                        </Col>
                        : null
                    }


                    <Col span={3}>
                        <Form.Item label="Current Balance"
                        labelCol="10"
                        
                            >
                            {
                                getFieldDecorator('balance', {
                                    rules: [{
                                        required: true, message: 'Please input balance'
                                    }]
                                })(
                                    <Input disabled/>
                                )
                            }
                        </Form.Item>



                    </Col>
                    <Col span={3}>
                        <Form.Item label="Quantity"
                            {...formItemLayout}>
                            {
                                getFieldDecorator('quantity', {
                                    rules: [{
                                        required: true, message: 'Please input quantity'
                                    }]
                                })(
                                    <Input />
                                )
                            }
                        </Form.Item>



                    </Col>
                    <Col span={3}>
                        <Form.Item label="Unit"
                            {...formItemLayout}>
                            {
                                getFieldDecorator('unitID', {
                                    rules: [{
                                        required: true, message: 'Please select measure unit'
                                    }]
                                })(
                                    <UnitSelect {...this.props} />
                                )
                            }
                        </Form.Item>



                    </Col>
                    <Col span={3}>
                        <Form.Item label="Unit Price"
                            {...formItemLayout}>
                            {
                                getFieldDecorator('unitPrice', {
                                    rules: [{
                                        required: true, message: ''
                                    }]
                                })(
                                    <Input disabled/>
                                )
                            }
                        </Form.Item>



                    </Col>
                    <Col span={3}>
                        <Form.Item label="Expense Fixed"
                            {...formItemLayout}>
                            {
                                getFieldDecorator('fixedExpense', {
                                    rules: []
                                })(
                                   <Checkbox />
                                )
                            }
                        </Form.Item>



                    </Col>
                </Row>
                <Row>
                        <Button htmlType="submit" type="primary">
                            Add To List
                        </Button>
                </Row>

                
            </Form>
        )
    }

}

export default Form.create()(TransactionDocumentItemForm);