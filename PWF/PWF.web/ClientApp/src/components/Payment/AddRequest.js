import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button, Icon,
Form, Select, InputNumber, Input, textArea,
DatePicker,
Drawer
} from 'antd';
import ItemRequestForm from '../miniComponents/ItemRequestForm';
import EmployeeSelect from '../miniComponents/EmployeeSelect';
import CustomerSelect from '../miniComponents/CustomerSelect';

import swal from 'sweetalert';
import { PaymentWFHistoryViewer } from './PaymentWFHistoryViewer';

const { Option } = Select;
let id = 0;

class AddRequest extends Component {
    constructor(props){
        super(props);

        this.state = {
            wfid : null,
            selectedFile : null,
            selectedFileList : [],
            isMissionAdvanceForm : false,
            paidToType : "",
            toEmployee : false,
            toCustomer : false,
            toOther : false,
            historyDrawer : false,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.processFile = this.processFile.bind(this);
        this.add = this.add.bind(this);
        this.remove = this.remove.bind(this);
        this.paidToChange = this.paidToChange.bind(this);
        this.toggleHistory = this.toggleHistory.bind(this);
    }

    componentDidMount(){
        this.props.loader.stop();
        var wfid = this.props.match.params.id;
        if(wfid != null && wfid != ''){
            this.setState({
                wfid 
            });
        }
    }


    handleSubmit(e){
        this.props.loader.start();
        var Service = new PaymentService(this.props.session.token);
        var self = this;
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            console.log(err);
            console.log(values);
            if(err == null){
                if(values.keys.length == 0){
                    self.props.loader.stop();
                    swal("","Add Items to request","warning");
                }
                else if(self.state.isMissionAdvanceForm){
                    if(values.DateFrom > values.DateTo){
                        self.props.loader.stop();
                        swal("Error","End Date is earliner than start date","warning");
                    }
                }
                else {
                    
                    var items =  [];
                    values.keys.map(function(i){
                        items.push(values.items[i])
                    });
                    values.items = items;
                    var testObj = {
                        Files : self.state.selectedFile
                    };   
                    console.log(this.state);
                    if(self.props.session.role == 1){
                        Service.GetCurrentEmployee().then(function(response){
                            if(response.data.status == false){
                                self.props.loader.stop();

                                swal("Error",response.data.error,"error");
                            }
                            if(response.data.status == true){
                                var data = response.data.response;
                                values.PaidTo = {
                                    Type : "1",
                                    id : data.id,
                                    name : data.employeeName
                                }
                                values.SubmittedBy = 1;
                                values.RequestDate = new Date();
                                var newValue = Object.assign(values,testObj); 

                                if(self.state.wfid != null && self.state.wfid != ""){
                                    Service.ResubmitPaymentRequest(self.state.wfid,newValue).then(function(data){
                                        console.log(data);
                                        self.props.loader.stop();
                                          if(data.data.status){
                                              swal("Success","Successfully resubmitted request","success");
                                              self.props.form.resetFields();
                                           self.props.history.push('/payment');
                                          }
                                          if(!data.data.status){
                                              swal("Error",data.data.error,"warning");
                                          }
                                      }).catch(function(error){
                                        self.props.loader.stop();
                                        swal("Error",error.message,"warning");
                                      })
                                }
                                else{
                                    Service.CreatePaymentRequest(newValue).then(function(data){
                                        console.log(data);
                                        self.props.loader.stop();
                                          if(data.data.status){
                                              swal("Success","Successfuly submitted request","success");
                                              self.props.form.resetFields();
                                           self.props.history.push('/payment');
                                          }
                                          if(!data.data.status){
                                              swal("Error",data.data.error,"warning");
                                          }
                                      }).catch(function(error){
                                        self.props.loader.stop();
                                        swal("Error",error.message,"warning");
                                      })
                                }
                                
                            }
                        }).catch(function(error){
                            self.props.loader.stop();
                            swal("Error",error.message,"warning");
                        });
                    }
                    else if(self.props.session.role == 11){
                        if(values.PaidTo.Type == "1"){
                            var id = values.PaidTo.id.value;
                            var emp = values.PaidTo.id.Employees.filter(function(item){
                                return item.id == id
                            })[0].employeeName;
                            values.PaidTo.id = id;
                            values.PaidTo.name = emp;
                        }
                        if(values.PaidTo.Type == "2"){
                            var code = values.PaidTo.code.value;
                            var cust = values.PaidTo.code.Customers.filter(function(item){
                                return item.code == code;
                            })[0].name;
                            values.PaidTo.code = code;
                            values.PaidTo.name = cust;
                        }
                        values.SubmittedBy = 11;
                        values.RequestDate = new Date();
                        console.log(values);
                        var newValue = Object.assign(values,testObj); 
                        if(self.state.wfid != null && self.state.wfid != ""){
                            Service.ResubmitPaymentRequest(self.state.wfid,newValue).then(function(data){
                                console.log(data);
                                self.props.loader.stop();
                                  if(data.data.status){
                                      swal("Success","Successfully resubmitted request","success");
                                      self.props.form.resetFields();
                                   self.props.history.push('/payment');
                                  }
                                  if(!data.data.status){
                                      swal("",data.data.error,"warning");
                                  }
                              }).catch(function(error){
                                self.props.loader.stop();
                                swal("",error.message,"warning");
                              })
                        }
                        else{
                            Service.CreatePaymentRequest(newValue).then(function(data){
                                console.log(data);
                                self.props.loader.stop();
                                  if(data.data.status){
                                      swal("Success","Successfuly submitted request","success");
                                      self.props.form.resetFields();
                                   self.props.history.push('/payment');
                                  }
                                  if(!data.data.status){
                                      swal("",data.data.error,"warning");
                                  }
                              }).catch(function(error){
                                self.props.loader.stop();
                                swal("",error.message,"warning");
                              })
                        }
                    }    
                        
                        //newValue.items = newItems;

                        
                    


                }
            }
            else {
                self.props.loader.stop();
            }
        })



    }
    handleTypeChange(e){
        if(e == 1){
            this.setState({
                isMissionAdvanceForm : true
            });
        }
        if(e == 0){
            this.setState({
                isMissionAdvanceForm : false
            });
        }
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    paidToChange(paidToType){
        this.setState({
            paidToType
        });
        this.setState({
            toCustomer : false,
            toEmployee : false,
            toOther : false,
        })
        if(paidToType == "1"){
            this.setState({
                toEmployee : true
            });
        }
        if(paidToType == "2"){
            this.setState({
                toCustomer : true
            });
        }
        if(paidToType == "3"){
            this.setState({
                toOther : true
            });
        }
    }

    processFile(e){
        var fileList = document.getElementById('files').files;
        console.log(e.target.value);
        console.log(fileList.length);
        console.log(fileList[0]);
        var doc = {
            ContentType : fileList[0].type,
            Length : fileList[0].size,
            Name : fileList[0].name
        }
        var fileReader = new FileReader();
        if (fileReader && fileList && fileList.length) {
           fileReader.readAsArrayBuffer(fileList[0]);
           fileReader.onload = function () {
              doc.File =Array.from(new Uint8Array(fileReader.result));
           };
        }
        this.setState({
            selectedFile : doc
        });
        console.log("State set to document");
      }

      add = () => {
        const { form } = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(++id);
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
          keys: nextKeys,
        });
      }



      remove = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        // We need at least one passenger
        if (keys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          keys: keys.filter(key => key !== k),
        });
      }

      validateItem = (rule,value, callback) => {
        if(value == null){
            callback("Please input the appropriate fields");
            return;
        }
         else if(value.description !== "" && value.amount !== ""){
              callback();
              return;
          }
          
         callback("Description and amount are required fields, please add description and amout")

      }

      

    render() {

        const { getFieldDecorator, getFieldValue } = this.props.form;

        // const formItemLayout ={
        //     labelCol : { span : 4},
        //     wrapperCol : {span : 20}
        // }
        // const formItemLayoutWithOutLabel = {
        //     wrapperCol: {
        //         span : 20, offset: 4
        //     }
            
        // };
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 4 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 20 },
            },
          };
          const formItemLayoutWithOutLabel = {
            wrapperCol: {
              xs: { span: 24, offset: 0 },
              sm: { span: 20, offset: 4 },
            },
          };
        getFieldDecorator('keys', { initialValue: [] });
        const keys = getFieldValue('keys');
        const formItems = keys.map((k, index) => (
        
            <Form.Item
            {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
            label={index === 0 ? 'Items' : ''}
            required={false}
            key={k}
          >
            {getFieldDecorator(`items[${k}]`, {
              validateTrigger: ['onBlur'],
              rules: [{
                validator : this.validateItem
              }],
            })(
              <ItemRequestForm {...this.props} />
            )}
            {keys.length > 1 ? (
              <Icon
                className="dynamic-delete-button"
                type="minus-circle-o"
                disabled={keys.length === 1}
                onClick={() => this.remove(k)}
              />
            ) : null}
          </Form.Item>
            
           
          ));

            const config = {
                rules: [{ required: true, message: 'Please input field!' }],
              }; 

        return(
            <div>
        <Row>
            <Col >
                <Card 
                title={this.state.wfid != null && this.state.wfid != '' ? "Resubmit Payment Request" :"Create New Payment Request"}
                    extra={
                        this.state.wfid != null && this.state.wfid != ''?
                        <Button onClick={() => this.toggleHistory()}>Show History</Button>
                            :null
                    }
                >
               <Form 
               onSubmit={this.handleSubmit}
               >
            {/* <Button onClick={ () => console.log(this)} >Click Here</Button> */}
            <Form.Item {...formItemLayout} label="Request Type" style={{fontStyle : 'bold'}}>
            {
                   getFieldDecorator("type",{
                        rules : [{
                            required : true, message : "Please select type"
                        }]
                })(
                    <Select
                    onChange={this.handleTypeChange}
                    style={{ width: '40%' }}
                    >
                        <Option value={0}>
                            General
                        </Option>
                        <Option value={1}>Mission Advance</Option>
                    </Select>
                )
            }
            </Form.Item>
            {formItems}
            <Form.Item {...formItemLayoutWithOutLabel}>

                <Button type="dashed"
                  onClick={() => this.add()} 
                 style={{ width: '60%' }}>
                    <Icon type="plus" /> Add Items Request
                </Button>
                
                </Form.Item>
              

              

               
            {
                this.state.isMissionAdvanceForm ?  
                <div>
            <Form.Item
          {...formItemLayout}
          label="From Date"
        >
          {getFieldDecorator('DateFrom', {
              rules: [{ required : this.state.isMissionAdvanceForm, message : 'Please Select Start Date'}]
          })(
            <DatePicker />
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="To Date"
        >
          {getFieldDecorator('DateTo', {
              rules: [{ required : this.state.isMissionAdvanceForm, message : 'Please Select End Date'}]
          })(
            <DatePicker />
          )}
        </Form.Item>
        <Form.Item {...formItemLayout} label="Amount Per Day">
            {
                getFieldDecorator("AmountPerDay",{
                    rules: [{ required : this.state.isMissionAdvanceForm, message : 'Please enter amount per day'}]
                })(
                    <InputNumber 
                    style= {{width: '20%'}}/> 
                    
                )
            }
            </Form.Item>
                    </div>
                    : null
                    }
        {
            this.props.session.role == 11 ?
                <div>
   <Form.Item {...formItemLayout} label="Paid To">
        {
            getFieldDecorator("PaidTo.Type",{
                rules : [{required : true, message : "Please select who is the payment receipent"}]
            })(
                <Select onChange={this.paidToChange}
                style = {{width : '40%'}} >
                    <Option value="1" key="1">Employee</Option>
                    <Option value="2" key="2">Supplier</Option>
                    <Option value="3" key="3">Other</Option>
                </Select>
            )
            
        } </Form.Item>
    {
        this.state.paidToType == "2" ?   
        <Form.Item {...formItemLayout} 
        labelCol={{span : 4}}
        wrapperCol={{span : 8}}
        label="Supplier">
        {
            getFieldDecorator("PaidTo.code",{
                rules: [{ required : this.state.toCustomer , message : "Please select supplier"}]
            })(
                <CustomerSelect   {...this.props} />
            )
        }
        </Form.Item>
        : null
    }
    {
        this.state.paidToType == "3" ?  
        <Form.Item         
        labelCol={{span : 4}}
        wrapperCol={{span : 8}}
        
        label="Enter Name">
        {
            getFieldDecorator("PaidTo.name",{
                rules : [{ required : this.state.toOther , message : "Please enter name"}]
            })(
                <Input  style = {{width : '40%'}} />
            )
        }
        </Form.Item> : null
    }

    {
        this.state.paidToType == "1" ? 
        <Form.Item 
        labelCol={{span : 4}}
        wrapperCol={{span : 8}}
        label="Employee">
        {
            getFieldDecorator("PaidTo.id",{
                rules : [{ required : this.state.toEmployee , message : "Please select employee"}]
            })(
                <EmployeeSelect   {...this.props} />
            )
        }
        </Form.Item>  : null
    }
                </div>

            : null
        }

      
                    <Form.Item {...formItemLayout} label="Upload Document">
                    <input required type="file" id="files" name="files" onChange={ (e) => this.processFile(e)}  />
  
                    </Form.Item>
                    <Form.Item {...formItemLayout} label="Enter Note">
        {
            getFieldDecorator("note",{
                rules : [{ required : true , message : "Please add note"}]
            })(
                <Input.TextArea rows={5}  style = {{width : '60%'}} />
            )
        }
        </Form.Item>
                    <Form.Item
          wrapperCol={{ span: 12, offset: 6 }}
        >
                                        <Button  htmlType="submit">Submit</Button>
        </Form.Item>
               </Form>
               </Card>
            </Col>
        </Row>
      
      {
          this.state.wfid != null && this.state.wfid != '' 
          ?
          <Drawer
          title="Payment History"
          placement="right"
          closable={true}
          onClose={this.toggleHistory}
          visible={this.state.historyDrawer}
          width={400}
          >
           <PaymentWFHistoryViewer 
          {...this.props} 
          
          wfid={this.state.wfid} />
          </Drawer>

          : null
      }
      
        </div> )
    }

    
}

export default Form.create()(AddRequest);