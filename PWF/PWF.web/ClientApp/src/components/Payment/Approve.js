import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import PaymentSourceSelect from '../miniComponents/PaymentSourceSelect';
import { StoreService } from  '../../service/store.service';
import { Link } from 'react-router-dom';
import { baseUrl } from '../../service/urlConfig';
import { Table, Divider,Card, Row, Col, Button, Tabs, Input, Alert, message, Form, Icon, Drawer } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import BudgetTransactionViewer from '../Payment/BudgetTransactionViewer';
import { PaymentWFHistoryViewer } from './PaymentWFHistoryViewer';
const { TabPane } = Tabs;
const { TextArea } = Input;
const ButtonGroup = Button.Group;


let id = 0;

function checkDuplicateInObject(propertyName, inputArray) {
    var seenDuplicate = false,
        testObject = {};
  
    inputArray.map(function(item) {
      var itemPropertyName = item[propertyName];
      if (itemPropertyName in testObject) {
        testObject[itemPropertyName].duplicate = true;
        item.duplicate = true;
        seenDuplicate = true;
      }
      else {
        testObject[itemPropertyName] = item;
        delete item.duplicate;
      }
    });
  
    return seenDuplicate;
  }


class Approve extends Component {
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            Request : null,
            Document : [],
            PaymentSource : [],
            Description : '',
            BudgetDocument : null,
            Workitem : null,
            historyDrawer : false,
        };
        this.handleChange = this.handleChange.bind(this);
        this.RejectRequest = this.RejectRequest.bind(this);
        this.CheckRequest = this.CheckRequest.bind(this);
        this.Service = new PaymentService(this.props.session.token);
        this.toggleHistory = this.toggleHistory.bind(this);
    }

    componentWillMount() {
        this.setState({
            wfid: this.props.match.params.id
        });

    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }

    RejectRequest = (e) =>{
        var self = this;
        if(this.props.session.role == 4){
            this.Service.FinHeadReject(this.props.match.params.id,this.state.Description).then(function(response){
                if(response.data.status == true){
                    swal("","Payment Request Rejected","success").then((value) => {
                        self.props.history.push('/payment');
                    })
                }
                if(!response.data.state){
                    swal("",response.data.error,"error");
                    
                }
            })
            .catch(function(error){
                swal("",error.message,"error");
            })
        }
        if(this.props.session.role == 5){
            this.Service.OwnerRejectRequest(this.props.match.params.id,this.state.Description).then(function(response){
                if(response.data.status == true){
                    swal("","Payment Request Rejected","success").then((value) => {
                        self.props.history.push('/payment');
                    })
                }
                if(!response.data.state){
                    swal("",response.data.error,"error");
                    
                }
            })
            .catch(function(error){
                swal("",error.message,"error");
            })
        }
        

    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }


    CheckRequest = (e) => {
        var self = this;
        if(this.props.session.role == 4){
            this.Service.CheckPaymentRequset(this.props.match.params.id,this.state.Description).then(function(response){
                if(response.data.status == true){
                    swal("","Payment Request Approved","success").then((value) => {
                        self.props.history.push('/payment');
                    })
                }
                if(!response.data.state){
                    swal("",response.data.error,"error");
                }
            })
            .catch(function(error){
                swal("",error.message,"error");
            })
        }
        if(this.props.session.role == 5){
            this.Service.ApprovePaymentRequest(this.props.match.params.id,this.state.Description).then(function(response){
                if(response.data.status == true){
                    swal("","Payment Request Approved","success").then((value) => {
                        self.props.history.push('/payment');
                    })
                }
                if(!response.data.state){
                    swal("",response.data.error,"error");
                }
            })
            .catch(function(error){
                swal("",error.message,"error");
            })
        }

    }


    componentDidMount() {
        this.props.loader.stop();
        var self = this;
        var service = new PaymentService(this.props.session.token);
        service.GetLastPaymentWorkitem(this.state.wfid).then(function (response) {
            console.log(response);
            
            self.setState({
                Request : response.data.data.request,
                Documents : response.data.data.postedDocuments,
                PaymentSource : response.data.data.sources,
                BudgetDocument : response.data.data.budgetDocument,
                Workitem : response.data,
          //      Document : response.data.data.PostedDocuments
            });
            console.log(self.state);
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.loader.start();
        var source = [];
        var self = this;
        this.props.form.validateFields((err, values) => {
            if(err == null){
                this.Service.ApprovePaymentRequest(this.props.match.params.id,this.state.Description).then(function(response){
                    self.props.loader.stop();
                    if(response.data.status == true){
                        swal("","Payment Request Approved","success").then((value) => {
                            self.props.history.push('/payment');
                        })
                    }
                    if(!response.data.state){
                        swal("",response.data.error,"error");
                    }
                })
                .catch(function(error){
                    self.props.loader.stop();
                    swal("",error.message,"error");
                })
            }
            else{
                self.props.loader.stop();
            }
          
        });
      }

    



    checkPaymentSource = (rule, value, callback) => {
        if(value.value != ""){
            callback();
            return;
        }
        callback("Please select payment source");
      }

    render() {

        const getRequestType = (type) => {
            if(type == 0){
                return "General"
            }
            else if(type == 1){
                return "Mission Advance";
            }
            else if(type == 2){
                return "Purchase Payment";
            }
            else{
                return "";
            }
        }
        const getDownloadLink = () => {
            return `/api/Payment/DownloadDoc/${this.state.Request.files.id}`;
        }

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 16}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 16, offset: 4
            }
            
        };

        const paymentSourceCol = [
            {
                title : "Source",
                key : "assetAccountID",
                dataIndex : "name"
            }, {
                title : "Amount",
                dataIndex : "amount"
            }
        ]

        const itemColumns = [
            {
                title : "Description",
                dataIndex : "description"
            },
            {
                title : "Unit",
                dataIndex : "unitID"
            },
            {
                title : "Unit Price",
                dataIndex : "unitPrice",
            },
            {
                title : "Quantity",
                dataIndex : "quantity",
            },{
                title : "Amount",
                dataIndex : "amount"
            }
        ];
      
        const paidTo = ["","Employee","Supplier","Other"];
        
        return(
        <Row>
            <Col span={24}>
                <Card 
                title="Payment Request"
                extra = {

                    <div>
                      <Button onClick={() => this.toggleHistory()}>Show History</Button>
{  this.state.Request != null && this.state.Request.type == 2 ?
                    <Button
                    onClick={() =>this.props.history.push(`/purchase/view/order/${this.state.Workitem.parentWFID}`)}
                    >View Purchase Order</Button> : null}
                    </div>
                  
                }
                 >
                   {
                     this.state.Request != null ?
                        <div>
                <Row>
                    <Col span={4} className="prop">Type</Col>
                    <Col span={18} className="value">{getRequestType(this.state.Request.type)}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Items</Col>
                    <Col span={18} className="value"> <Table
                 columns={itemColumns}
                 pagination={false}
                  dataSource={this.state.Request.items}
                  footer ={ () =>
                    <Row>
                        <Col span={6}>Total</Col>
                        <Col span={18} style={{textAlign : "right"}}>{this.state.Request.items.sum("amount")}</Col>
                    </Row>
                }
               /></Col>
                </Row> 
                {
                    this.state.Request.type == 1 ?  
                    <div>
                        <Row>
                    <Col span={4} className="prop">Date
                    </Col>
                    <Col  span={18} className="value">{new Date(this.state.Request.dateFrom).toLocaleDateString() } - {new Date(this.state.Request.dateTo).toLocaleDateString()}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Amount Per Day </Col>
                    <Col span={18} className="value">{this.state.Request.amountPerDay}</Col>
                </Row>
                    </div>
                    
                    : null 
                }

                <Row>
                <Col span={4} className="prop">Payment Source </Col>
                    <Col span={18} className="value">
                    <Table 
                        columns = {paymentSourceCol}
                        dataSource = {this.state.PaymentSource}
                        pagination = {false}
                        footer ={ () =>
                            <Row>
                                <Col span={6}>Total</Col>
                                <Col span={18} style={{textAlign : "right"}}>{this.state.PaymentSource.sum("amount")}</Col>
                            </Row>
                        }
                    />
                        </Col>
                </Row>
                <Row>
                <Col span={4} className="prop">Budget Transaction Document </Col>
                    <Col span={18} className="value">
                   <BudgetTransactionViewer 
                   budgetDocs={this.state.BudgetDocument}
                   {...this.props}
                   />
                        </Col>
                </Row>
                      <Row>
                      <Col span={4} className="prop">Document </Col>
                    <Col span={18} className="value">
                    <a
                     onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.files.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                              [response.data], 
                              {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                        
                       
                     }}
                     
                     >

                    {this.state.Request.files.name}</a>
                    </Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Paid To </Col>
                    <Col span={18} className="value">{
                        this.state.Request.paidTo.name
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop"></Col>
                    <Col span={18} className="value">{
                        paidTo[this.state.Request.paidTo.type]
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note </Col>
                    <Col span={18} className="value">{
                        this.state.Request.note
                        
                        }</Col>
                </Row>
                <Form onSubmit={this.handleSubmit}>
                  
                <Form.Item
                {...formItemLayout}
                label = "Description"
                >
                {
                   getFieldDecorator("Description",{
                        rules : [{
                            required : true, message : "Please add description"
                        }]
                }) (
                    <TextArea name="Description"  onChange={(e) => this.handleChange(e)} rows={4} />
                )
                }
                   
                </Form.Item>

                <Form.Item {...formItemLayoutWithOutLabel}>
                <ButtonGroup className="pull-right"
                    >

                                        <Button 
                                        disabled={this.state.Description == ''} 
                                        onClick={this.RejectRequest}
                                        >Reject
                                        </Button>

                                        <Button  htmlType="submit">Approve</Button>

									</ButtonGroup>
               
                </Form.Item>
            </Form>

                {/* <Row>
                    <Col className="prop" span={14}>
                    <ButtonGroup className="pull-right"
                    >

                                        <Button 
                                        disabled={this.state.Description == ''} type="danger"
                                        onClick={this.RejectRequest}
                                        >Reject
                                        </Button>

                                        <Button 
                                        onClick={this.CheckRequest}
                                        disabled={this.state.Description == ''} type="primary">
                                       {this.props.session.role == 4 ? "Check" : "Approve"} 
                                        </Button>

									</ButtonGroup>
                    </Col>
                </Row> */}
                        </div>

                     : null
                 }
          
               

                </Card>
            </Col>
            <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <PaymentWFHistoryViewer
        {...this.props} 
        
        wfid={this.state.wfid} />
        </Drawer>
        </Row>
            )
    }

    
}

export default Form.create()(Approve);