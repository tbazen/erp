import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { Table, Divider,Card, Row, Col, Button, Tabs, Input, Alert, message, Form, Icon } from 'antd';
import swal from 'sweetalert';

export default class BudgetTransactionViewer extends Component {
    constructor(props){
        super(props);
        this.state = {
            BudgetDocument : null,
            AccountNames : {},
            CostCenterNames : {},
        }

        this.service = new PaymentService('');
    }

    componentDidMount(){
        var self = this;
        if(this.props.budgetDocs != null){
            this.setState({
                BudgetDocument : this.props.budgetDocs
            });
            console.log(this);
            var AccountNames ={};
            var CostCenterNames = {};
            this.props.budgetDocs.transactions.map(function(item){
                self.service.GetAccountName(item.accountID).then(function(response){
                    AccountNames[item.accountID] = response.data;
                }).then(function(){
                    self.setState({
                        AccountNames
                    });
                });
                self.service.GetCostCenterName(item.costCenterID).then(function(response){
                    CostCenterNames[item.costCenterID] = response.data;
                }).then(function(){
                    self.setState({
                        CostCenterNames
                    });
                });
            })
        }

    }

    
    render(){
        const columns = [
            {
                title : "Account",
                dataIndex : "accountID",
                width : '30%',
                render : (d) => {
                    return this.state.AccountNames[d]
                }
            },
            {
                title : "Cost Center",
                dataIndex : "costCenterID",
                width : '30%',
                render : (d) => {
                    return this.state.CostCenterNames[d]
                }
            },
            {
                title : "Debit",
                dataIndex : "debitAmount"
            },
            {
                title : "Credit",
                dataIndex : "creditAmount"
            },{
                title : "Note",
                dataIndex : "description"
            }
        ];

        return(
            <Table 
            dataSource={this.props.budgetDocs.transactions}
            columns = {columns}
            pagination={false}
            />
        )
    }
}