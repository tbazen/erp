import React, { Component } from 'react';
import { Col, Row, Icon } from 'antd';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';


export default class DocumentsRenderer extends Component {

    render(){
        var self = this;
        console.log(this.props);
        return(
            <div>
                {this.props.docs != null && this.props.docs.length > 0 ?
                this.props.docs.map(function(item){
                    return (
                        <Row>
                        <Col span={4} className="prop"> </Col>
                      <Col span={14} className="value">
                      <a
                       onClick={()=> {
                          Axios(baseUrl +`/Payment/DownloadFile/${item.id}`, {
                              method: 'GET',
                              responseType: 'blob' //Force to receive data in a Blob Format
                          })
                          .then(response => {
                          //Create a Blob from the PDF Stream
                              const file = new Blob(
                                [response.data], 
                                {type: 'application/pdf'});
                          //Build a URL from the file
                              const fileURL = URL.createObjectURL(file);
                          //Open the URL on new Window
                              window.open(fileURL);
                          })
                          .catch(error => {
                              console.log(error);
                          });
                          
                         
                       }}
                       
                       >
  
                      {item.name}{' '} ({item.description})</a>
                      </Col>
                      <Col span={4}>
                      {
                          self.props.showRemove != false ? 
                          <Icon
                          className="dynamic-delete-button"
                          type="minus-circle-o"
                          onClick={() => self.props.remove(item.id)
                          }
                        />

                          : null
                      }

                      </Col>
                      </Row>
                    )
                })
                    : null
            }
            </div>

        )
    }
}