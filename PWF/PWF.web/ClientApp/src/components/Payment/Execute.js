import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { StoreService } from  '../../service/store.service';
import { Link } from 'react-router-dom';
import { baseUrl } from '../../service/urlConfig';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Drawer, Spin } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import DocumentFormHandler from './PaymentDocumentsForm/DocumentFormHandler';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { PaymentWFHistoryViewer } from './PaymentWFHistoryViewer';
import DocumentBrowser from '../AccountDocument/SearchAccountDocument';
import '../../assets/css/berp.scss';
import BudgetTransactionViewer from './BudgetTransactionViewer';
import DocumentUploader from '../miniComponents/DocumentUploader';
import DocumentsRenderer from './DocumentsRenderer';
const { TabPane } = Tabs;
const { TextArea } = Input;
const ButtonGroup = Button.Group;

const Uploader = Form.create()(DocumentUploader);


class Execute extends Component {
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            activeTab : "1",
            Request : null,
            ImportedDocuments : [],
            NewDocuments : [],
            PaymentSource : [],
            DocumentTableData : [],
            AttachedDocs : [],
            Reference : {},
            BudgetDocument : null,
            Description : '',
            dropdownOpen: false,
            showFormModal : false,
            showAddDocumentModal : false,
            showUploaderModal : false,
            closeRequestModal : false,
            selectedDocumentID : '',
            selectedFormKey : '',
            DocumentTypes : [],
            DocumentAmount : {},
            docHTML: '',
            documentViewModal : false,
            serialType : '',
            selectedFile : null,
            docDescription : '',
            Workitem: null,
            historyDrawer : false,
            totalPaidAmount : 0.0,
            totalPaidAmountFromSource: 0.0,
            spinning : false,

        };
        this.handleChange = this.handleChange.bind(this);
        this.Service = new PaymentService(this.props.session.token);

        this.modalChild = React.createRef();
        this.uploaderChild = React.createRef();
        this.closeRequestChild = React.createRef();
        
        this.toggle = this.toggle.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleDocumentBrowser = this.toggleDocumentBrowser.bind(this);
        this.toggleUploaderModal = this.toggleUploaderModal.bind(this);

        this.openForm = this.openForm.bind(this);
        this.refreshDocumentList = this.refreshDocumentList.bind(this);
        this.prepareDocumentTable =this.prepareDocumentTable.bind(this);
        this.service = new PaymentService(this.props.session.token);
        this.selecteDocument = this.selecteDocument.bind(this);
        this.AddDocument = this.AddDocument.bind(this);
        this.RemoveDocument = this.RemoveDocument.bind(this);
        this.setFile = this.setFile.bind(this);
        this.setDocumentDescription= this.setDocumentDescription.bind(this);
        this.CanCloseRequest = this.CanCloseRequest.bind(this);
        this.CloseRequest = this.CloseRequest.bind(this);
        this.toggleHistory = this.toggleHistory.bind(this);
        this.getTotalPaidAmount = this.getTotalPaidAmount.bind(this);
        this.UploadDocument = this.UploadDocument.bind(this);

        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);

    }

    componentWillMount() {
        this.setState({
            wfid: this.props.match.params.id
        });



    }


    componentDidMount() {
        this.StopSpin();
        var self = this;
        var service = new PaymentService(this.props.session.token);

        

        service.GetAllDocumentTypes().then(function(response){
            console.log(response);
            if(response.data.status){
                self.setState({
                    DocumentTypes : response.data.response
                });
            }
            if(!response.data.status){
                swal("",response.error,"");
            }
        }).catch(function(error){
            swal("",error.message,"");
        });

        service.GetLastPaymentWorkitem(this.state.wfid).then(function (response) {
            console.log(response);
            self.setState({
                Request : response.data.data.request,
                ImportedDocuments : response.data.data.importedDocuments,
                NewDocuments : response.data.data.newDocuments,
                PaymentSource : response.data.data.sources,
                Reference : response.data.data.reference,
                BudgetDocument : response.data.data.budgetDocument,
                AttachedDocs : response.data.data.attachedDocs,
                Workitem : response.data,

            });  
            console.log(self);
        }).then(function(){
            console.log(self.state);
            var docs = self.getAllDocuments();
            console.log(docs);
            if(docs != null && docs.length > 0){
                self.prepareDocumentTable(docs);
            }
            service.GetSerialType(self.state.Reference.typeID).then(function(response){
                if(response.data.status){
                    self.setState({
                        serialType : response.data.response.name
                    })
                }
            });
            
        })
        

    }

    getTotalPaidAmount(){
        var self = this;
        var service = new PaymentService(this.props.session.token);

        service.GetTotalPaidAmount(this.state.wfid).then(function(response){
           if(response.data.status == false){
               return swal("Error",response.data.error,"warning")
           }
           else if(response.data.status == true){
               self.setState({
                   totalPaidAmount : response.data.response
               });
           }
        }).catch(function(error){
            swal("",error.message,"");
        });

        service.GetTotalPaidAmountFromSource(this.state.wfid).then(function(response){
            if(response.data.status == false){
                return swal("Error",response.data.error,"warning")
            }
            else if(response.data.status == true){
                self.setState({
                    totalPaidAmountFromSource : response.data.response
                });
            }
         }).catch(function(error){
             swal("",error.message,"");
         });
    }

    setFile(file){
        console.log(file);
        this.setState({
            selectedFile : file
        });
    }

    toggleDocumentBrowser(){
        this.setState({
        showAddDocumentModal : !this.state.showAddDocumentModal
        });
    }

    toggleUploaderModal(){
        this.setState({
            showUploaderModal : !this.state.showUploaderModal
        })
    }

    
    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    setDocumentDescription(description){
        this.setState({
            docDescription : description
        });
    }

    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }

    UploadDocument(){
        var self = this;
        var description = this.state.docDescription;
        console.log(description);
        console.log(this.state);
        var description = '';
        this.uploaderChild.current.validateFields((err,values) => {
            if(err == null){
               description = values.Description;
               if(self.state.selectedFile == null){
                   swal("Error","File not selected","warning");
               }
               else if(self.state.selectedFile != null){
                self.StartSpin();
                var file = self.state.selectedFile;
                file.Description = description;
                console.log(file);
                self.Service.AddDocumentAttachment(self.state.wfid,file).then(function(response){
                    console.log(response);
                    self.StopSpin();
                        if(response.data.status == true){
                            self.setState({
                                AttachedDocs : response.data.response 
                            });
                            swal("Success","Document successfuly attached","success");
                            self.toggleUploaderModal();
                        };
                        if(response.data.status == false){
                            swal("Error",response.data.error,"warning");
                        }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                });
            }
            else{

            }
            }});

       
    }

    CloseRequest(){
        this.StartSpin();
        var self = this;
        var description = this.state.docDescription;
        console.log(description);
        console.log(this.state);
        var description = '';
        this.closeRequestChild.current.validateFields((err,values) => {
            if(err == null){
               description = values.Description;
               console.log(self.closeRequestChild);
              if(self.state.selectedFile == null){
                  self.StopSpin();
                   swal("Error","File not selected","warning");
               }
               else if(self.state.selectedFile != null){
                   self.StartSpin();
                var file = self.state.selectedFile;
                file.Description = description;
                console.log(file);
                self.Service.CloseRequest(self.state.wfid,file).then(function(response){
                    console.log(response);
                        self.StopSpin();
                        if(response.data.status == true){
                            swal("Success","You have successfully closed the request","success");
                            self.props.history.push("/payment");
                        };
                        if(response.data.status == false){
                            swal("Error",response.data.error,"warning");
                        }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                });
            }
            else{
                self.StopSpin();
            }
            }});
    };

    toggleCloseRequestModal(){
        
        this.setState({
            closeRequestModal : !this.state.closeRequestModal
        });
    }

    RemoveDocument(docID){
        var self = this;
        var service =  new PaymentService("");
        var wfid = this.state.wfid;
        if(docID != null){
            swal("","Are you sure you want to remove this attachment","").then(function(val){
               if(val){
                service.RemoveAttachedDocument(wfid,docID).then(function(response){
                    if(response.data.status == true){
                        self.setState({
                            AttachedDocs : response.data.response 
                        });
                        swal("Success","Document successfuly removed","success");

                    };
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
            }).catch(function(error){
                swal("Error",error.message,"warning");
            });
               }
            })

        }
    }

    getAllDocuments(){
        console.log(this.state);
        if(this.state.NewDocuments == null && this.state.ImportedDocuments == null){
            console.log(1);
            return [];
        }
        else if(this.state.NewDocuments == null && this.state.ImportedDocuments != null){
            console.log(2);
            return this.state.ImportedDocuments;
        }
        else if(this.state.NewDocuments != null && this.state.ImportedDocuments == null){
            console.log(3);
            return this.state.NewDocuments;
        }
        else{
            console.log(4);
            return [ ...this.state.NewDocuments, ...this.state.ImportedDocuments ];
        }
    }

    CanCloseRequest(){
        //var totalAmount = 0;
        // var amounts = this.state.DocumentAmount;
        // var keys = Object.keys(amounts);
        // keys.map(function(item){
        //     totalAmount -= amounts[item]
        // });
        var sourceAmount = 0;
        this.state.PaymentSource.map(function(item){
            sourceAmount += item.amount;
        })
        console.log(sourceAmount);
        console.log(this.state.totalPaidAmount);
        console.log(this.state.totalPaidAmountFromSource);
        var diff = Math.abs(sourceAmount) - Math.abs(this.state.totalPaidAmount);
        var diff2 = Math.abs(sourceAmount) - Math.abs(this.state.totalPaidAmountFromSource);
        const approvedEqualPaid = sourceAmount.toFixed(2) == this.state.totalPaidAmount.toFixed(2);
        const sourceEqualPaid = sourceAmount.toFixed(2) == this.state.totalPaidAmountFromSource.toFixed(2);
        const a1 = approvedEqualPaid || Math.abs(diff) < 1;
        const a2 = sourceEqualPaid || Math.abs(diff2) < 1;
        console.log(approvedEqualPaid);
        console.log(diff);
        console.log(sourceEqualPaid);
        console.log(diff2);
        if(a1 && a2){
            return true;
        }
        // else if(Math.abs(diff) < 1 && Math.abs(diff2) < 1){
        //     return true;
        // }
        else
            return false;
    }



    selecteDocument(docID){
        this.setState({
            selectedDocumentID : docID
        });
    }

    onChange = (activeKey) => {
        this.setState({ activeTab : activeKey });
      }

    AddDocument(){

        var importedDocs = this.state.ImportedDocuments != null && this.state.ImportedDocuments.length != 0 ? this.state.ImportedDocuments.map(a => a.accountDocumentID) : [];
        var newDocs = this.state.NewDocuments != null && this.state.NewDocuments.length != 0 ? this.state.NewDocuments.map(a => a.accountDocumentID) : [];
        console.log(this.state.selectedDocumentID);
        console.log(importedDocs);
        console.log(newDocs);
        var self = this;
        if(this.state.selectedDocumentID == ''){
            swal("","Document not selected, Please select document","");
        }
        else if(importedDocs.indexOf(this.state.selectedDocumentID) != -1 || newDocs.indexOf(this.state.selectedDocumentID) != -1){
            swal("","Document already added in the document list");
        }
        
        else{
            self.StartSpin();
            self.service.CheckTransactionAmountForDocument(self.props.match.params.id,self.state.selectedDocumentID).then(function(response){
                   if(!response.data.status){
                        swal("Error",response.data.error,"warning");
                    }
                    // if(response.data.status){
                        self.service.AddDocumentFromBrowser(self.state.wfid,self.state.selectedDocumentID).then(function(response){
                            console.log(response);
                            self.StopSpin();
                            if(response.data.status){
                                console.log(response);
                                self.setState({
                                    ImportedDocuments : response.data.response.importedDocuments,
                                    NewDocuments : response.data.response.newDocuments,
                                    activeTab : "2"
                                })
                                
                                
                            }
                            if(!response.data.status){
                                swal("",response.data.error,"");
                            }
                        }).catch(function(error){
                            swal("",error.message,"");
                        }).then(function(){
                            var docs = self.getAllDocuments();
                            if(docs != null && docs.length > 0){
                                self.prepareDocumentTable(docs);
                                self.toggleDocumentBrowser();
                                swal("","Document Successfully Added","");
                            }
                            
                        })
                    // }
            }).catch(function(error){
                self.StopSpin();
                swal("Error",error.message,"warning");
            })
           
        }

    }

    refreshDocumentList(response){
        if(response.status == true){
            this.setState({
                ImportedDocuments : response.response.importedDocuments,
                NewDocuments : response.response.newDocuments,
            })
            this.toggleModal();
            this.prepareDocumentTable(this.getAllDocuments());   
            this.setState({
                activeTab : "2"
            })
        }

    }

    DeleteDocument(docID){
        var self = this;
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to remove this document?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willDelete => {
            if (willDelete) {
                        self.service.RemoveDocuments(self.state.wfid,docID).then(function(response){
                            console.log(response);
                            if(response.data.status){
                                console.log(response);
                                self.setState({
                                    ImportedDocuments : response.data.response.importedDocuments,
                                    NewDocuments : response.data.response.newDocuments,
                                    activeTab : "2"
                                })
                                swal("","Document Successfully Removed","");
                                var docs = self.getAllDocuments();
                                console.log(docs);
                                if(docs != null && docs.length > 0){
                                    self.prepareDocumentTable(docs);
                                    
                            }
                            else{
                                self.setState({
                                    DocumentTableData : [],
                                })
                            }
                            
                                // if(response.data.response.importedDocuments == null || response.data.response.importedDocuments.length == 0){
                                //     if(response.data.response.newDocuments == null || response.data.response.newDocuments.length == 0){
                                //         self.setState({
                                //         DocumentTableData : []
                                //     });
                                //     }
                                    
                                // }
                                
                            }
                            if(response.data.status == false){
                                swal("Error",response.data.error,"");
                            }
                        }).catch(function(error){
                            swal("",error.message,"");
                        }).then(function(){
                            // var docs = self.getAllDocuments();
                            // console.log(docs);
                            // if(docs != null && docs.length > 0){
                            //     self.prepareDocumentTable(docs);
                                
                            // }
                            
                        })
             
            }
          });
        
    }

    prepareDocumentTable(Documents){
        var service = new PaymentService(this.props.session.token);
        var self = this;
        self.getTotalPaidAmount();
        var docAmounts = {};
        var tData = [];
        console.log(Documents);
        Documents.map(function(item){
            var id = item.accountDocumentID;
            service.GetTransactionAmounts(id).then(function(response){
                console.log(response);
                if(response.data.status == true){
                     docAmounts[id] = response.data.response;
                }
                if(!response.data.status){
                    swal("",response.data.error,"");
                }
            }).catch(function(error){
                swal("",error.message,"");
            }).then(function(){
                console.log(self);
                var type = self.state.DocumentTypes.filter(function(i){
                    return i.id == item.documentTypeID;
                })[0].name;
                var s = {
                    'id' : item.accountDocumentID,
                    'docType' : type,
                    'docDate' : new Date(item.documentDate).toLocaleString(),
                    'amount' : docAmounts[item.accountDocumentID][0],
                    'net' : docAmounts[item.accountDocumentID][1],
                    'reference' : item.paperRef
                };
                tData.push(s);
            }).then(function(){
                console.log(docAmounts);
                console.log(tData);
                self.setState({
                    DocumentAmount : docAmounts
                });
                console.log(tData);
                self.setState({
                    DocumentTableData : tData
                });
            })

        })
    }




    handleOk(){
        console.log(this);
        this.modalChild.current.postDocument();
    }

    toggleModal() {
        this.setState({
            showFormModal : !this.state.showFormModal
        })
    }

    toggleDocumentViewModal(){
        this.setState({
            documentViewModal : !this.state.documentViewModal
        });
    }

    GetDocumentHTML(docID){

        var self = this;
        this.service.GetAccountDocumentHTML(docID).then(function(response){
            if(response.data.status == true){
                self.setState({
                    docHTML : response.data.response
                });
            }
            if(response.data.status == false){
                swal("",response.data.error);
            }
        }).catch(function(error){
            swal("",error.message,"");
        }).then(function(){
            self.toggleDocumentViewModal();
            console.log(self.state);
        })
    }

    openForm(key){
        this.setState({
            selectedFormKey : key
        });
        this.toggleModal();
    }

    toggle() {
        var self = this;
        this.setState({
          dropdownOpen: !self.state.dropdownOpen
        });
      }

    handleChange = (event) => {
        console.log(event);
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        console.log(this.state);
    }

    handleMenuClick(e) {
        message.info('Click on menu item.');
        console.log('click', e);
      }

   


    



    render() {

        const DocumentColumns = [
            {
                title : "Document Type",
                key: "id",
                dataIndex : "docType"
            },
            {
                title : "Date",
                dataIndex : "docDate"
            },
            {
                title : "Amount",
                dataIndex : "amount",
            },
            {
                title : "Net Amount",
                dataIndex : "net",
            } ,{
                title : "Reference",
                dataIndex : "reference"
            },
            {
                title: "Action",
                id : 'id',
                render : d => (
                    <div>
<Button onClick={() => this.GetDocumentHTML(d.id)} icon="eye"></Button>
<Button onClick={() => this.DeleteDocument(d.id)} icon="delete"></Button>
                    </div>
                
                
                )
            }
        ]

        const paymentSourceCol = [
            {
                title : "Source",
                key : "assetAccountID",
                dataIndex : "name"
            }, {
                title : "Amount",
                dataIndex : "amount"
            }
        ]

        const itemColumns = [
            {
                title : "Description",
                dataIndex : "description"
            },
            {
                title : "Unit",
                dataIndex : "unitID"
            },
            {
                title : "Unit Price",
                dataIndex : "unitPrice",
            },
            {
                title : "Quantity",
                dataIndex : "quantity",
            },{
                title : "Amount",
                dataIndex : "amount"
            }
        ];

        const html = "<h2>Transfer from Cashier to Cashier</h2><h2>Date: Jan 20,19 12:03 PM</h2><h2>Ref: 2</h2><h2>Affected accounts</h2><h3>No accounting entry for this document</h3><h2>More Information</h2><b>Amount:<b/>1,200.00<h2>References</h2><br/><strong>Cash Payment Voucher: </strong>00000002";
        const paidTo = ["","Employee","Customer","Other"];

        const getRequestType = (type) => {
            if(type == 0){
                return "General"
            }
            else if(type == 1){
                return "Mission Advance";
            }
            else if(type == 2){
                return "Purchase Payment";
            }
            else{
                return "";
            }
        }

        const getAddDocumentButton = ()  => {
            
            const AddDocButtons =  <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>{' '}
            <DropdownToggle
            style={{background : 'white', borderRadius:5, marginLeft : 15}}
            >            
             Add Account Document
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={() => this.openForm(1)} >Bank Withdrawal Document</DropdownItem>
              <DropdownItem onClick={() => this.openForm(2)}>Cashier to Cashier</DropdownItem>
              <DropdownItem onClick={() => this.openForm(3)}>Labour Payment</DropdownItem>
              <DropdownItem onClick={() => this.toggleDocumentBrowser()}>Add Document from Browser</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>;


            if(this.CanCloseRequest() == true ){
                const menu = (
                    <Menu>
                        
                        {
                    this.state.Request != null && this.state.Request.type == 2 ?
                    <Menu.Item>  <a
                    onClick={() =>this.props.history.push(`/purchase/view/order/${this.state.Workitem.parentWFID}`)}
                    >View Purchase Order</a></Menu.Item> : null
                }
                        
                        <Menu.Item>
                        <a onClick={() => this.toggleUploaderModal()}>Attach Document</a>
                            </Menu.Item>
                            <Menu.Item>
                            <a
                    onClick={() => this.toggleCloseRequestModal()}
                    >Close Request</a> 
                            </Menu.Item>
                            <Menu.Item>
                            <Link to={  `/Document/2/${this.state.wfid}/1`}>
                        <a   icon="download" 

                        >Download Document</a>
                </Link>
                            </Menu.Item>
                    </Menu>
                )
                return (
                <div>
                    <Dropdown overlay={menu} placement="bottomLeft">
                    <Button>Action</Button>
                    </Dropdown>
                    {' '}
                    {AddDocButtons}
                    {' '}
                    <Button onClick={() => this.toggleHistory()}>Show History</Button>
                </div> 
                )

                
            }
            else{
                const menu = (
                    <Menu>
                        <Menu.Item>
                        {
                    this.state.Request != null && this.state.Request.type == 2 ?
                    <a
                    onClick={() =>this.props.history.push(`/purchase/view/order/${this.state.Workitem.parentWFID}`)}
                    >View Purchase Order</a> : null
                }
                        </Menu.Item>
                        <Menu.Item>
                        <Link to={  `/Document/2/${this.state.wfid}/1`}>
                        <a   icon="download" 

                        >Download Document</a>
                </Link>
                            </Menu.Item>
                            <Menu.Item>
                            <a onClick={() => this.toggleUploaderModal()}>Attach Document</a>

                            </Menu.Item>
                    </Menu>
                )
                return(
                    
                    <div>

                                <Dropdown overlay={menu} placement="bottomLeft">
                    <Button>Action</Button>
                    </Dropdown>
                        {AddDocButtons}
                          {' '}
                          <Button onClick={() => this.toggleHistory()}>Show History</Button>
                          </div>
                )
            }
               

        }

        return(<div>
                        {/* Form Modal */}
                        <Modal 
                visible={this.state.showFormModal}
                title = "Add Document"
                onOk={() => this.handleOk()}
                onCancel = {() => this.toggleModal()}  
                width = '60%'
                
                >
                <DocumentFormHandler ref={this.modalChild} 
                refreshDocs = {this.refreshDocumentList}
                selectedKey={this.state.selectedFormKey} 
                {...this.props} />
                </Modal>
            {/*  */}

            {/* Add Document Modal */}
            <Modal 
            visible={this.state.showAddDocumentModal}
            title = "Document Browser"
            onOk={() => this.AddDocument()}
            onCancel = { () => this.toggleDocumentBrowser() }
            okText="Add Document"
            width = '85%'
            style = {{paddingLeft : '10%'}}
            
            >
            <Spin
            spinning={this.state.spinning}>
            <DocumentBrowser 
             reference={this.state.Reference.reference} 
             {...this.props} SelectItem={this.selecteDocument} />
            </Spin>

            
            </Modal>


            {/*  */}

            {/* Document Uploader Modal */}
            <Modal
            visible={this.state.showUploaderModal}
            title="Add Attachment"
            onOk={() => this.UploadDocument()}
            onCancel = {() => this.toggleUploaderModal() }
            okText = "Upload Document"
            width = "40%"
            footer = {
                [<Button onClick={this.toggleUploaderModal}>Cancel</Button>,
                <Button loading={this.state.spinning} onClick={this.UploadDocument}>Upload Document</Button>]
            }
            >
            <Uploader ref={this.uploaderChild} {...this.props} 
            setFile = {this.setFile}
            setDescription = {this.setDocumentDescription}
            />
            </Modal>
            {/*  */}

                        {/* Close Request Modal */}
                        <Modal
            visible={this.state.closeRequestModal}
            title="Close Request"
            onOk={() => this.CloseRequest()}
            onCancel = {() => this.toggleCloseRequestModal() }
            okText = "Close Request"
            width = "40%"
            footer = {
                [<Button onClick={this.toggleCloseRequestModal}>Cancel</Button>,
                <Button loading={this.state.spinning} onClick={this.CloseRequest}>Close Request</Button>]
            }
            >
            <Spin
            spinning={this.state.spinning}
            >
                <h5>Attach Signed Document</h5>
            <Uploader ref={this.closeRequestChild} {...this.props} 
            setFile = {this.setFile}
            setDescription = {this.setDocumentDescription}
            />
            </Spin>

            </Modal>
            {/*  */}



             {/* Document Viewer Modal */}
             <Modal 
                visible={this.state.documentViewModal}
                title = "View Document"
                onOk={() => this.toggleDocumentViewModal()}
                onCancel = {() => this.toggleDocumentViewModal()}  
                
                width = '70%'
                
                >
               <div className="documentViewer" dangerouslySetInnerHTML={{__html : this.state.docHTML}}>

               </div>
                </Modal>
            {/*  */}


         
            <Tabs  
            onChange = {this.onChange}
            activeKey = {this.state.activeTab}
            type="card">
    <TabPane tab="Request" key="1">

               
                <Card 
                title="Payment Request"
                style = {{marginTop : -20}}
                extra = { getAddDocumentButton() }
                 >
                 
                   {
                     this.state.Request != null ?
                        <div>
                                            <Row>
                                            <Col offset={12} span={4} className="prop">Reference {' '}</Col>
                    <Col span={6}  className="value"> {
                    this.state.serialType 
                    }{' '}{this.state.Reference.reference}</Col>
                    </Row>
                <Row>
                
                    <Col span={4} className="prop">Type</Col>
                    <Col span={18} className="value">{getRequestType(this.state.Request.type)}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Items</Col>
                    <Col span={18} className="value"> <Table
                 columns={itemColumns}
                 pagination={false}
                  dataSource={this.state.Request.items}
                  footer ={ () =>
                    <Row>
                        <Col span={6}>Total</Col>
                        <Col span={18} style={{textAlign : "right"}}>{this.state.Request.items.sum("amount")}</Col>
                    </Row>
                }
               /></Col>
                </Row> 
                {
                    this.state.Request.type == 1 ?  
                    <div>
                        <Row>
                    <Col span={4} className="prop">Date
                    </Col>
                    <Col  span={18} className="value">{new Date(this.state.Request.dateFrom).toLocaleDateString() } - {new Date(this.state.Request.dateTo).toLocaleDateString()}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Amount Per Day </Col>
                    <Col span={18} className="value">{this.state.Request.amountPerDay}</Col>
                </Row>
                    </div>
                    
                    : null 
                }
                <Row>
                <Col span={4} className="prop">Payment Source </Col>
                    <Col span={18} className="value">
                    <Table 
                        columns = {paymentSourceCol}
                        dataSource = {this.state.PaymentSource}
                        pagination ={false}
                        footer ={ () =>
                            <Row>
                                <Col span={6}>Total</Col>
                                <Col span={18} style={{textAlign : "right"}}>{this.state.PaymentSource.sum("amount")}</Col>
                            </Row>
                        }
                    />
                        </Col>
                </Row>
                <Row>
                <Col span={4} className="prop">Budget Transaction Document </Col>
                    <Col span={18} className="value">
                   <BudgetTransactionViewer 
                   budgetDocs={this.state.BudgetDocument}
                   {...this.props}
                   />
                        </Col>
                </Row>

                      <Row>
                      <Col span={4} className="prop">Document </Col>
                    <Col span={18} className="value">
                    <a
                     onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.files.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                              [response.data], 
                              {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                        
                       
                     }}
                     
                     >

                    {this.state.Request.files.name}</a>
                    </Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Total Expense From Source </Col>
                    <Col span={18} className="value">{
                        this.state.totalPaidAmountFromSource
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Net Paid Amount </Col>
                    <Col span={18} className="value">{
                        this.state.totalPaidAmount
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Paid To </Col>
                    <Col span={18} className="value">{
                        this.state.Request.paidTo.name
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop"></Col>
                    <Col span={18} className="value">{
                        paidTo[this.state.Request.paidTo.type]
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note </Col>
                    <Col span={18} className="value">{
                        this.state.Request.note
                        
                        }</Col>
                </Row>
                {
                    this.state.AttachedDocs != null && this.state.AttachedDocs.length > 0 ?
                    <div>
                                            <Row>
                    <Col span={4} className="prop">Attached Documents</Col>
                    <Col span={18} className="value">
                    <DocumentsRenderer 
                    {...this.props} docs={this.state.AttachedDocs} 
                    showRemove={true}
                    remove={this.RemoveDocument}/>
                    </Col>
                </Row>

                    </div>

                    : null
                }

                <Row>
                    <Col className="prop" span={14}>
                    <ButtonGroup className="pull-right"
                    >

                                    
									</ButtonGroup>
                    </Col>
                </Row>
                        </div>

                     : null
                 }
          
               

                </Card>
          
                </TabPane>
    <TabPane tab="Document" key="2">
    <Card 
                title="Payment Documents"
                style = {{marginTop : -20}}
               
                 >
                 <Table
                 columns={DocumentColumns}
                  dataSource={this.state.DocumentTableData}
                  pagination = {false}
               />

                </Card>
      
    </TabPane>

  </Tabs>
        

  <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <PaymentWFHistoryViewer 
        {...this.props} 
        
        wfid={this.state.wfid} />
        </Drawer>
        </div>
         
            )
    }

    
}

export default Execute;
// export default Form.create() (Execute);