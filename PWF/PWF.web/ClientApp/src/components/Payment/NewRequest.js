import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button,
Form, Select, InputNumber, Input
} from 'antd';
import BankAccountSelect from '../miniComponents/BankAccountSelect';
import CashAccountSelect from '../miniComponents/CashAccountSelect';
import TaxCenterSelect from '../miniComponents/TaxCenterSelect';
import PaymentMethodSelect from '../miniComponents/PaymentMethodSelect';
const { Option } = Select;

class NewRequest extends Component {
    constructor(props){
        super(props);

        this.state = {
            selecectedFile : null,
            selectedFileList : []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
    }
    handleTypeChange(){

    }


    render() {

        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: { span: 6 },
          wrapperCol: { span: 14 }
        };

        return(
        <Row>
            <Col span={16} offset={2}>
                <Card 
                title="Create New Payment Request"
     
                >
             <Form layout="inline" onSubmit={this.handleSubmit}>
        <Form.Item label="Bank Account">
          {getFieldDecorator('bankAccountID', {
           
          })(<BankAccountSelect {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Cash Account">
          {getFieldDecorator('cashAccountID', {
           
          })(<CashAccountSelect {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Tax Cenetr">
          {getFieldDecorator('taxCenterID', {
           
          })(<TaxCenterSelect {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Payment Method">
          {getFieldDecorator('paymentMethod', {
           
          })(<PaymentMethodSelect {...this.props}/>)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item>
      </Form>
                </Card>
            </Col>
        </Row>
            )
    }

    
}

export default Form.create()(NewRequest);