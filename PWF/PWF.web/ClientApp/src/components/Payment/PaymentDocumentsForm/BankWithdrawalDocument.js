import React, { Component } from 'react';

import { PaymentService } from '../../../service/payment.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button,
Form, Select, InputNumber, Input, textArea
} from 'antd';
import BankAccountSelect from '../../miniComponents/BankAccountSelect';
import CashAccountSelect from '../../miniComponents/CashAccountSelect';
const { Option } = Select;

class BankWithdrawalDocument extends Component {
    constructor(props){
        super(props);
        this.state = {

        }
       // this.handleSubmit = React.createRef();
        this.handleSubmit=  this.handleSubmit.bind(this);
        this.postDocument= this.postDocument.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
    this.props.form.validateFields((err, values) => {
      console.log(err);
      console.log(values);
      if (!err) {
          values.bankAccountID = values.bankAccountID.value;
          values.cashAccountID = values.cashAccountID.value;
        console.log('Received values of form: ', values);
        return values;
      }
    })
    }

    postDocument(){
      this.props.form.validateFields((err, values) => {
      
        if (!err) {
            values.bankAccountID = values.bankAccountID.value;
            values.cashAccountID = values.cashAccountID.value;
          console.log('Received values of form: ', values);
          return values;
        }
      })
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: { span: 6 },
          wrapperCol: { span: 10 },
          style : {
            marginBottom : 5,
            paddingBottom : 5
        }
        };

        return(
        <Row>
            <Col span={16} offset={2}>
             <Form layout="vertical" onSubmit={this.handleSubmit}>
        <Form.Item label="Bank Account"
        {...formItemLayout}
        >
          {getFieldDecorator('bankAccountID', {
           rules :  [{
            required : true, message : 'Please select bank account'
        }]
          })(<BankAccountSelect {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Cash Account"
        {...formItemLayout}>
          {getFieldDecorator('casherAccountID', {
            rules :  [{
                required : true, message : 'Please select cash account'
            }]
          })(<CashAccountSelect {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Amount"
        {...formItemLayout}>
          {getFieldDecorator('amount', {
          
            rules :  [{
                required : true, message : 'Please enter amount'
            }]
          })(<InputNumber min={0} {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Check Number"
          {...formItemLayout}>
      
          {getFieldDecorator('chekNumber', {
            rules :  [{
                required : true, message : 'Please enter check number'
            }]
          })(<Input {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Note"
        {...formItemLayout}>
        
          {getFieldDecorator('ShortDescription', {
            rules :  [{
                required : true, message : 'Please add note'
            }]
          })(<textArea
            style={{width : 500}}
          {...this.props}/>)}
        </Form.Item>
        {/* <Form.Item
        {...formItemLayout}>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item> */}
      </Form>
            </Col>
        </Row>
            )
    }
}

export default Form.create()(BankWithdrawalDocument);