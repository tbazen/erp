import React, { Component } from 'react';
import { Select, Input, Row, Col, Form, Icon, Button, InputNumber} from 'antd';
import AccountSearch from '../../miniComponents/AccountSearch';
import CostCenterSearch from '../../miniComponents/CostCenterSearch';
import swal from 'sweetalert';
import 'antd/dist/antd.min.css';
import FormItem from 'antd/lib/form/FormItem';

const ButtonGroup = Button.Group;
let budgetRowID = 0;
export default class BudgetTransactionDocument extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props){
        super(props);
        const value = props.value || '';
        this.state = {
            costCenterID : '',
            accountID : '',
            debitAmount : '',
            creditAmount : '',
            description : '',
        }
        this.postDocument = this.postDocument.bind(this);
    }

    onSelect = (value) => {
        console.log(value);
        if(!('value' in this.props)){
            this.setState({ value } );
        }
        this.triggerChange({value})
    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        this.triggerChange({
            [target.name] : target.value
        });
        //console.log(target.value);
    }

    postDocument(){
        var finalItems = []
        this.props.form.validateFields((err,values) => {
            if(err == null){
                if(values.budgetKeys.length == 0){
                    swal("Error","Please insert budget items","warning");
                }
                else{
                    values.budgetKeys.map(function(item){
                        finalItems.push({
                            accountID : values.item[item].accountID.value,
                            costCenterID : values.item[item].costCenterID.value,
                            creditAmount : values.item[item].creditAmount,
                            debitAmount : values.item[item].debitAmount,
                            description : values.item[item].description
                        });
                    })
                    var totalDebit = finalItems.sum("debitAmount");
                    var totalCredit = finalItems.sum("creditAmount");
                    if(totalCredit != totalDebit){
                        finalItems = [];
                        swal("Error","Total debit amount is not equal to total credit amount, Corret your input","warning");
                    }
                }
            }
        })
        return finalItems;
    }

    addBudgetRow = () => {
        const { form } = this.props;
        // can use data-binding to get
        const budgetKeys = form.getFieldValue('budgetKeys');
        const nextbudgetKeys = budgetKeys.concat(++budgetRowID);
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
          budgetKeys: nextbudgetKeys,
        });
      }

      removeBudgetRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const budgetKeys = form.getFieldValue('budgetKeys');
        // We need at least one passenger
        if (budgetKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          budgetKeys: budgetKeys.filter(key => key !== k),
        });
      }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }

      checkCostCenter = (rule,value,callback) => {
          if(value != undefined){
            callback();
            return;
          }
          callback("Select Cost Center");

      }

      checkAccount = (rule,value,callback) => {
        if(value != undefined){
            callback();
            return;
          }
          callback("Select Account");
    }

    render(){
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 0 },
            wrapperCol: { span: 20, offset:4 },
            style : {
              marginBottom : '1px !important',

          }
          };
          const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 16, offset: 4
            }
            
        };
          getFieldDecorator('budgetKeys',{initialValue:[] });
          const budgetKeys = getFieldValue('budgetKeys');
          const formLabel =
          <div style={{marginBottom : '-10px'}}>
              {budgetKeys.length >= 1 ? (
          <Form.Item
              style={{marginBottom : '-10px'}}
                >

              

            
            <Form.Item
            label="Account"
            style={{display: 'inline-block', width : 'calc(20%)'}}
            >

            </Form.Item>
            <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
       {' '}
      </span>
            <Form.Item
            label="Cost Center"
            style={{display: 'inline-block', width : 'calc(20%)'}}
            >

            </Form.Item>
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>
              <Form.Item
              label="Debit"
             style={{display: 'inline-block', width : 'calc(15%)'}}
            >

              </Form.Item>
              <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>
                       <Form.Item
            label="Credit"
                        style={{display: 'inline-block', width : 'calc(15%)'}}
                      >

            </Form.Item>
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>
             <Form.Item
             label="Note"
               style={{display: 'inline-block', width : 'calc(15%)'}}
             >

               </Form.Item>
               <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>
             
         </Form.Item>  
        ) : null}

            
 

              </div>
          ;
          const formItems = budgetKeys.map((k,index) => (
              <div>


  <Form.Item
              style={{marginBottom : '-10px'}}
                >

              

            
            <Form.Item
            style={{display: 'inline-block', width : 'calc(20%)'}}
            >
            {
                getFieldDecorator(`item[${k}].accountID`,{
                 rules : [{ validator : this.checkAccount}]
                })(
                    <AccountSearch
                    leafAccounts={true}
                    {...this.props} />
                )
            }
            </Form.Item>
            <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
       {' '}
      </span>
            <Form.Item
            style={{display: 'inline-block', width : 'calc(20%)'}}
            >
            {
                getFieldDecorator(`item[${k}].costCenterID`,{
                    rules : [{ validator : this.checkCostCenter}]
                })(
                    <CostCenterSearch  {...this.props} />
                )
            }
            </Form.Item>
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>
              <Form.Item
             style={{display: 'inline-block', width : 'calc(15%)'}}
            >
              {
                  getFieldDecorator(`item[${k}].debitAmount`,{
                    initialValue : 0 || '',
                    rules : [{ required : true, message : 'add debit'}]
                  })(
                      <InputNumber placeholder="Debit"/>
                  )
              }
              </Form.Item>
              <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>
                       <Form.Item
                        style={{display: 'inline-block', width : 'calc(15%)'}}
                      >
                       {
                           getFieldDecorator(`item[${k}].creditAmount`,{
                            initialValue : 0 || '',
                            rules : [{ required : true, message : 'add credit'}]
                           })(
                               <InputNumber  placeholder="Credit"/>
                           )
                       }
            </Form.Item>
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>
             <Form.Item
               style={{display: 'inline-block', width : 'calc(15%)'}}
             >
             {
                 getFieldDecorator(`item[${k}].description`,{
                    rules : [{ required : true, message : 'add description'}]
                 })(
                     <Input  placeholder="Note"/>
                 )
             }
               </Form.Item>
               <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>
               {budgetKeys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={budgetKeys.length === 1}
            onClick={() => this.removeBudgetRow(k)}
            style={{display: 'inline-block', width : '10px'}}
          />
        ) : null}

            
  </Form.Item>

    
              </div>
         
         ));

        return (
            <span>
                <Row>
                  <Form layout="vertical"
                  onSubmit={this.postDocument}>
                  {formLabel}
                  {formItems}
                  <Form.Item {...formItemLayoutWithOutLabel}>

                <Button type="dashed"
                  onClick={this.addBudgetRow} 
                 style={{ width: '100%' }}
                 >
                    <Icon type="plus" /> Add Budget Transaction
                </Button>
                
                </Form.Item>
               
                  </Form>
                </Row>
            </span>
        )
    }
}

//export default Form.create()(BudgetTransactionDocument);