import React, { Component } from 'react';
import BankWithdrawalDocument from './BankWithdrawalDocument';
import CashierToCashierDocument from './CashierToCashierDocument';
import LabourPaymentDocument from './LabourPaymentDocument';
import { PaymentService } from '../../../service/payment.service';
import swal from 'sweetalert';
import { Spin } from 'antd';

export default class DocumentFormHandler extends Component {
    constructor(props){
        super(props);
        this.state = {
            response : null,
            spinning : false,
        }
        this.child = React.createRef();
        this.postDocument = this.postDocument.bind(this);
        this.service = new PaymentService(this.props.session.token);
        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
    }

    componentDidMount(){
        this.StopSpin();
    }

    postDocument(){
        var self = this;
        self.setState({
            response : null
        });
        console.log(this);
        var resp = "";
        this.child.current.validateFields((err,values) => {
            console.log(err);
            console.log(values);
            if (err == null) {
                        this.StartSpin();
                        if(self.props.selectedKey == 1){
                            values.bankAccountID = values.bankAccountID.value;
                            values.casherAccountID = values.casherAccountID.value;
                            self.service.PostBankWithdrawalDocument(self.props.match.params.id,values)
                            .then(function(response){
                                self.StopSpin();
                                if(response.data.status == true){
                                    swal("","Document Successfully Posted").then(function(value){
                                        self.props.refreshDocs(response.data);
                                    })
                                    
                                   
                                }
                                if(!response.data.status){
                                    swal("",response.data.error,"");
                                }
                            }).catch(function(error){
                                self.StopSpin();
                                swal("",error.message,"");
                            });
                            console.log(values);
                            }
                            if(self.props.selectedKey == 2){
                                values.fromCashierAccountID = values.fromCashierAccountID.value;
                                values.toCashierAccountID = values.toCashierAccountID.value;
                                self.service.PostCashierToCashierDocument(self.props.match.params.id,values)
                                .then(function(response){
                                    self.StopSpin();
                                    if(response.data.status == true){
                                        swal("","Document Successfully Posted").then(function(value){
                                            self.props.refreshDocs(response.data);
                                        })
                                    }
                                        
                                    if(!response.data.status){
                                        swal("",response.data.error,"");
                                    }
                                }).catch(function(error){
                                    self.StopSpin();
                                    swal("",error.message,"");
                                });
                            }
                            if(self.props.selectedKey == 3){
                                values.assetAccountID = values.assetAccountID.value;
                                values.costCenterID = values.costCenterID.value;
                                values.taxCenter = values.taxCenter.value;
                                self.service.PostLabourPaymentDocument(self.props.match.params.id,values)
                                .then(function(response){
                                    self.StopSpin();
                                    if(response.data.status == true){
                                        swal("","Document Successfully Posted").then(function(value){
                                            self.props.refreshDocs(response.data);
                                        })
                                    }
            
                                    if(!response.data.status){
                                        swal("",response.data.error,"");
                                    }
                                }).catch(function(error){
                                    self.StopSpin();
                                    swal("",error.message,"");
                                });
                            }            
            }

            //return values;
        })
        console.log(self.state.response);
        return self.state.response;
    };

    
    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }



    render(){

        return(
            <Spin
            spinning={this.state.spinning}>

           
           <div>

               {/* <BankWithdrawalDocument ref={this.child} {...this.props} /> */}

               {
                   this.props.selectedKey == 1 ?
                <BankWithdrawalDocument ref={this.child} {...this.props} /> : null
               }
               {
                   this.props.selectedKey == 2 ? 
                   <CashierToCashierDocument ref={this.child} {...this.props} /> : null
               }
               {
                   this.props.selectedKey == 3 ?
                   <LabourPaymentDocument ref={this.child} {...this.props} /> : null
               }
           </div>
           </Spin>
        )
    }
}