import React, { Component } from 'react';

import { PaymentService } from '../../../service/payment.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button,
Form, Select, InputNumber, Input, textArea
} from 'antd';
import BankAccountSelect from '../../miniComponents/BankAccountSelect';
import CashAccountSelect from '../../miniComponents/CashAccountSelect';
import CostCenterSelect from '../../miniComponents/CostCenterSelect';
import TaxCenterSelect from '../../miniComponents/TaxCenterSelect';
const { Option } = Select;

class LabourPaymentDocument extends Component {
    constructor(props){
        super(props);
        this.state = {
            isCashPayment : false,
        }
        this.handleSubmit=  this.handleSubmit.bind(this);

        this.onPaymentMethodSelect = this.onPaymentMethodSelect.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
          values.assetAccountID = values.assetAccountID.value;
          values.costCenterID = values.costCenterID.value;
          values.taxCenter = values.taxCenter.value;
        console.log('Received values of form: ', values);
        return values;
      }
    })
    }

    onPaymentMethodSelect(value){
        if(value == 0){
            this.setState({
                isCashPayment : true
            });
        }
        if(value == 1){
            this.setState({
                isCashPayment:false
            });
        }
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
          labelCol: { span: 6 },
          wrapperCol: { span: 10 },
          style : {
            marginBottom : 5,
            paddingBottom : 5
        }
        };

        return(
        <Row>
            <Col span={16} offset={2}>
         
             <Form layout="vertical" onSubmit={this.handleSubmit}>
             <Form.Item label="Cost Center"
        {...formItemLayout}
        >
          {getFieldDecorator('costCenterID', {
           rules :  [{
            required : true, message : 'Please select cost center'
        }]
          })(<CostCenterSelect {...this.props}/>)}
        </Form.Item>        
        
              <Form.Item label="Tax Center"
        {...formItemLayout}
        >
          {getFieldDecorator('taxCenter', {
           rules :  [{
            required : true, message : 'Please select tax center'
        }]
          })(<TaxCenterSelect {...this.props}/>)}
        </Form.Item> 

        <Form.Item label="Payment Method"
        {...formItemLayout}
        >
          {getFieldDecorator('paymentMethod', {
           rules :  [{
            required : true, message : 'Please select payment method'
        }]
          })(
            <Select placeholder="Please select payment method .."
            style={{ width: 300 }}
            onSelect={this.onPaymentMethodSelect}
            >
            <Option value={0}
                    key={0}
                    >
              Cash
                    </Option>
                    <Option value={1}
                    key={1}
                    >
              Check
                    </Option>
            
           
            </Select>
          
          
          )}
        </Form.Item> 

                {
                    this.state.isCashPayment ?
                    <Form.Item label="Cash Account"
                    {...formItemLayout}>
                      {getFieldDecorator('assetAccountID', {
                        rules :  [{
                            required : true, message : 'Please select cash account'
                        }]
                      })(<CashAccountSelect {...this.props}/>)}
                    </Form.Item>


                    :
                    <Form.Item label="Bank Account"
                    {...formItemLayout}
                    >
                      {getFieldDecorator('assetAccountID', {
                       rules :  [{
                        required : true, message : 'Please select bank account'
                    }]
                      })(<BankAccountSelect {...this.props}/>)}
                    </Form.Item> 
                }

    

      
        <Form.Item label="Gross Payment Amount"
        {...formItemLayout}>
          {getFieldDecorator('amount', {
          
            rules :  [{
                required : true, message : 'Please enter amount'
            }]
          })(<InputNumber min={0} {...this.props}/>)}
        </Form.Item>
        {
            !this.state.isCashPayment ?


<Form.Item label="Check Number"
          {...formItemLayout}>
      
          {getFieldDecorator('checkNumber', {
            rules :  [{
                required : true, message : 'Please enter check number'
            }]
          })(<Input {...this.props}/>)}
        </Form.Item> 
            : null
        }
        <Form.Item label="Paid To"
          {...formItemLayout}>
      
          {getFieldDecorator('paidTo', {
            rules :  [{
                required : true, message : 'Please enter field'
            }]
          })(<Input {...this.props}/>)}
        </Form.Item>
        <Form.Item label="Note"
        {...formItemLayout}>
        
          {getFieldDecorator('ShortDescription', {
            rules :  [{
                required : true, message : 'Please add note'
            }]
          })(<textArea
            style={{width : 500}}
          {...this.props}/>)}
        </Form.Item>
        {/* <Form.Item
        {...formItemLayout}>
          <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item> */}
      </Form>
               
            </Col>
        </Row>
            )
    }
}

export default Form.create()(LabourPaymentDocument);