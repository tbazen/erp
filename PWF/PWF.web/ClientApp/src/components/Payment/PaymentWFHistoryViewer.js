import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { Col, Row, Timeline, Spin } from 'antd';
import swal from 'sweetalert';
import { Element } from 'react-scroll';

export class PaymentWFHistoryViewer extends Component {
    constructor(props){
        super(props);

        this.state = {
            History : {},
        
        }
        this.Service = new PaymentService("");
    }

    componentDidMount(){
        var self = this;
        self.Service.GetPaymentWFHistory(this.props.wfid).then(function(response){
            if(response.data.status == true){
                console.log(response);
                self.setState({
                    History : response.data.response
                });
            }
            if(response.data.status != true){
                swal("Error",response.data.error,"warning")
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
    }

    render(){

        return(
            <Element
            className="element" id="containerElement" style={{

                height: '800px',
                overflow: 'scroll',
            }}
        >
            
                {
                    this.state.History.history != null && this.state.History.history.length > 0 ?
                    <Timeline>
                        {
                                this.state.History.history.map(function(item){
                                    return (
                                        <Timeline.Item>
                                        <p>{item.action} {item.name}</p>
                                        <p>Date: {new Date(item.date).toLocaleString()}</p>
                                        <p>Description: {item.description}</p>
                                    </Timeline.Item>
                                    )
                                })
                                
                        }
                       
                           </Timeline>
                    :  <Spin
                    style={{margin : 50 }}
                    size="large" />
                }
           
        </Element>

        )
    }


}