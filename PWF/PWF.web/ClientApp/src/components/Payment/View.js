import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button } from 'antd';
import swal from 'sweetalert';
import { PRW, PW } from '../../service/WFStates';

export default class View extends Component {
    constructor(props){
        super(props);

        this.state = {
            Workflows : []
        };
        this.Service = new PaymentService("");

    }

    componentDidMount() {
        const RoleState = { 4 : 1, 1 : 0, 5 : 2,  6 : 3, 7: 4, 11 : 0}
        var self = this;
        var Service = new PaymentService(this.props.session.token);
        Service.GetPaymentRequsetWorkflows().then(function (response) {
            var result = response.data;
            console.log(result);
            if (result.status == true) {
                console.log(RoleState[self.props.session.role]);
                    if(result.response != null && result.response.length > 0){
                        self.setState({
                            Workflows : result.response.filter(function(item){
                                if(self.props.session.role == 1){
                                    return true;
                                }
                                else{
                                    return item.currentState == RoleState[self.props.session.role]
                                }
                                
                            })
                        });
                    }
                    
                
          
            }
        });
    }

    CancelRequest(wfid){
        var self = this;
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to remove this document?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willCancel => {
            if(willCancel){
                this.Service.CancelPaymentRequest(wfid,"Cancelled Payment Request").then(function(response){
                    if(response.data.status == true){
                        var filtered = self.state.Workflows.filter(function(item){
                            return item.id != wfid
                        });
                        self.setState({
                            Workflows : filtered
                        });
                    }
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                }).catch(function(error){
                    swal("Error",error.message,"warning");
                })
            }
          });
    }


    render() {

        const columns = [{
            title: "Initiator",
            key : "id",
            dataIndex: "action.username"
        }, {
            title: "Descrition",
            render : d => DescriptionMinimizer(d.description)
        }, {
            title: "Time",
            dataIndex: "action.timeStr"
        }, {

            title: 'Action',
            id: "aid",
            render: d => GetViewLink(d.id,d.currentState)
        }

        ];

       
        const DescriptionMinimizer = (desc) => {
            if(desc == null || desc == '')
                return <p></p>
            else
                return <p>{desc.substring(0,30)}</p>
        }

        const GetViewLink = (wfid,state) => {
            var role = this.props.session.role;
            if(role == 1 || role == 11){
                if(state == 0){
                   return ( <Button.Group> <Link to={`/payment/resubmit/${wfid}`}>
                    <Button>Resubmit</Button>            
                    </Link>
                    <Button onClick={() => this.CancelRequest(wfid)}>Cancel</Button>
                    <Link to={`/wf/view/${PW}/${wfid}`}><Button >View</Button></Link>
                   </Button.Group> )
                }
                else{
                    return (<Link to={`/wf/view/${PW}/${wfid}`}><Button >View</Button></Link>)
                }
                
               
              }

            if(role == 4 ){
                return  <Link to={`/payment/check/${wfid}`}>
                <Button>Check</Button>
                
                </Link>
            }
            if(role == 5 ){
                return  <Link to={`/payment/review/${wfid}`}>
                <Button>Review</Button>
                
                </Link>
            }
            if(role == 6 ){
                return  <Link to={`/payment/execute/${wfid}`}>
                <Button>Execute</Button>
                
                </Link>
            }
            if(role == 7 ){
                return  <Link to={`/payment/finalize/${wfid}`}>
                <Button>Finalize</Button>
                
                </Link>
            }
        }

        return(
        <Row>
            <Col span={24}>
                <Card 
                title="Payment Request"
                extra = { this.props.session.role == 1 || this.props.session.role == 11 ? <Button onClick={() => this.props.history.push('/payment/request')}>Add New Request</Button> : null}
                >
                
                <Table
                columns = {columns}
                dataSource = {this.state.Workflows}
                /> 

                </Card>
            </Col>
        </Row>
            )
    }

    
}