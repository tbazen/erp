import React, { PropTypes, Component } from "react";
import ReactDOM from "react-dom";
import Axios from 'axios';
import {
  Form,
  Select,
  message,
  InputNumber,
  Switch,
  Radio,
  Slider,
  Button,
  Upload,
  Icon,
  Rate,
  Checkbox,
  Row,
  Col
} from "antd";

const { Option } = Select;

const dummyRequest = ({ file, onSuccess }) => {
  setTimeout(() => {
    onSuccess("ok");
  }, 0);
};

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      selectedFileList: []
    };

    this.processFile = this.processFile.bind(this);
  }



  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        const path = `api/Payment/TestAPI`;
        // //values.upload = this.state.selectedFile;
        
        // const data = new FormData();
        // data.append("InputNumber",values.InputNumber);
        // console.log(values);
        // data.append("Rate",values.Rate);
        values.Upload = this.state.selectedFile;
        console.log(values);
        Axios.post(path,values).then(function(response){
            console.log(response);
        }).catch(function(error){
            console.log(error.message);
        });
      }
    });
  };
  
//   handleUplaod = (file) => {
//     return new Promise(async (resolve, reject) => {
//       const fileName = `nameThatIwant.type`;
//       const url = await S3Fetcher.getPresignedUrl(fileName);
//       resolve(url);
//     })
    
// };

handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
    console.log(event.target.files);
  }

  normFile = e => {
    console.log("Upload event:", e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  onRemove(file){
      console.log(file);
  }

  processFile(e){
    var fileList = document.getElementById('files').files;
    console.log(e.target.value);
    console.log(fileList.length);
    console.log(fileList[0]);
    var doc = {
        ContentType : fileList[0].type,
        Length : fileList[0].size,
        Name : fileList[0].name
    }
    var fileReader = new FileReader();
    if (fileReader && fileList && fileList.length) {
       fileReader.readAsArrayBuffer(fileList[0]);
       fileReader.onload = function () {
         // var imageData = fileReader.result;
        //  fileReader.result.split('base64,')[1];
          doc.File =Array.from(new Uint8Array(fileReader.result));
       };
    }
    this.setState({
        selectedFile : doc
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 }
    };
    const props = {
        name: 'Upload',
        action: '/api/Payment/TestAPI/',
        headers: {
          authorization: 'authorization-text',
        },
        onChange(info) {
          if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
          }
          if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
          } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
          }
        },
      };
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item {...formItemLayout} label="InputNumber">
          {getFieldDecorator("InputNumber", { initialValue: 3 })(
            <InputNumber min={1} max={10} />
          )}
          <span className="ant-form-text"> machines</span>
        </Form.Item>

        <Form.Item {...formItemLayout} label="Rate">
          {getFieldDecorator("Rate", {
            initialValue: 3.5
          })(<Rate />)}
        </Form.Item>
     

        <Form.Item
          {...formItemLayout}
          label="Upload"
          extra="longgggggggggggggggggggggggggggggggggg"
        >
   <input type="file" id="files" name="files" onChange={ (e) => this.processFile(e)}  />
        
          {/* {getFieldDecorator("upload", {
            valuePropName: "fileList",
            getValueFromEvent: this.normFile
          })(
         
  )}         )} */}
        </Form.Item>

        <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Form.create()(Demo);