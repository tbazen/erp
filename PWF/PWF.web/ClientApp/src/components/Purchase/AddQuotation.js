import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { Card, Row, Col, Button, Form, Table, Input, InputNumber, Select, Modal, Icon, Drawer, Spin } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import { baseUrl } from '../../service/urlConfig';
import UnitSelect from '../miniComponents/UnitSelect';
import SupplierSelect from '../miniComponents/CustomerSelect';
import DocumentUploader from '../miniComponents/DocumentUploader';
import { WFHistoryViewer } from '../WFHistoryViewer';

const { TextArea } = Input;
const ButtonGroup = Button.Group;
const { Option } = Select;
var Uploader = (DocumentUploader);
let itemRowID = 0;


const expandedRowRender = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    { title: 'Unit Price', dataIndex: 'unitPrice' },
    { title: 'Amount', dataIndex : 'amount' },
    
    ];

    const data = d.items;
    
    return (
    <Table
        columns={columns}
        dataSource={data}
        pagination={false}
    />
    );
};



class AddQuotation extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Request : null,
            Worflow : null,
            Quotations : [],
            codeNameDict : {},
            supplierNameDict : {},
            Description : '',
            ItemLength : [],
            QuotationModal: false,
            selectedFile : null,
            historyDrawer : false,
            spinning: false,
            

        }

        this.handleChange = this.handleChange.bind(this);
        this.AddQuotation = this.AddQuotation.bind(this);
        this.UploaderChild = React.createRef();
        this.RejectRequest = this.RejectRequest.bind(this);
        this.Service = new PurchaseService();
        this.getItemName = this.getItemName.bind(this);
        this.toggleQuotationModal = this.toggleQuotationModal.bind(this);
        this.setFile = this.setFile.bind(this);
        this.getSupplierName = this.getSupplierName.bind(this);
        this.findSupplierNames = this.findSupplierNames.bind(this);
        this.SubmitQuotation = this.SubmitQuotation.bind(this); this.toggleHistory = this.toggleHistory.bind(this);
        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
    }

    componentWillMount(){
        this.setState({
            wfid : this.props.match.params.id
        });
    }

    componentDidMount(){
        this.StopSpin();
        var self = this;
        this.Service.GetLastPurchaseWorkitem(this.state.wfid).then(function(response){
            console.log(response);
            if(response.data.status == true){
                self.setState({
                    Request : response.data.response.data,
                    Worflow : response.data.response
                });
                if(response.data.response.data.quotations != null){
                    self.setState({
                        Quotations : response.data.response.data.quotations
                    });
                }
            }
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            if(self.state.Request != null){
                var dict = {};
                self.state.Request.request.items.map(function(item){
                    self.Service.GetItemName(item.code).then(function(response){
                       dict[item.code]=response.data;
                    }).catch(function(error){
                        swal("Error",error.message,"warning");
                    })
                })
                setTimeout(() => {
                    self.setState({
                        codeNameDict : dict
                    });
                }, 1000);

                var length= self.state.Request.request.items.length;
                var a = [];
                for (let index = 1; index <= length; index++) {
                a.push(index)
                }
                self.setState({
                    ItemLength : a
                });
              console.log(self.state);

            }
        })
    }

    findSupplierNames(){
        var self = this;
        var suppDict = {};
        self.state.Quotations.map(function(item){
            self.Service.GetSupplierName(item.code).then(function(response){
                suppDict[item.code] = response.data;
            }).catch(function(error){
                swal("Error",error.message,"warning");
            });
        });
        self.setState({
            supplierNameDict : suppDict
        });
    }

    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }
    removeItemRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        // We need at least one passenger
        if (itemKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          itemKeys: itemKeys.filter(key => key !== k),
        });
      }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }

    setFile(doc){
        this.setState({
            selectedFile : doc
        });
    }

    toggleQuotationModal(){
        this.props.form.setFieldsValue({
            'itemKeys' : this.state.ItemLength
        })


        this.setState({
            QuotationModal : !this.state.QuotationModal
        });

    }

    getItemName = (code) => {
        console.log(this.state.codeNameDict[code]);
        return this.state.codeNameDict[code]
    };

    getSupplierName = (code) => {
        return this.state.supplierNameDict[code];
    }

    SubmitQuotation(){
        var self = this;
        swal("Type something:", {
            title : "Description",
            text : "Enter a description for submitting the quotations",
            content: "input",
            showCancelButton: true,
            attributes : {
                required : true
            }
          })
          .then((value) => {
              if(value){
                self.Service.SubmitPriceQuotation(self.state.wfid,value).then(function(response){
                    if(response.data.status == true){
                        swal("Succsss","Successfully submitted the quotations","success");
                        self.props.history.push("/purchase");
                    }
                    if(response.data.status == false){
                        swal("Error",response.data.error,"error");
                    }
                }).catch(function(error){
                    swal("Error",error.message,"error");
                });
              }
            
          });

        //   const { form } = this.props;
        //   // can use data-binding to get
        //   const itemKeys = form.getFieldValue('itemKeys');
        //   itemKeys.map(function(value){
        //       //var name = ;
        //       form.setFieldsValue(
        //       {
        //           [`item${value}.unitPrice`] : 0
        //       });
        //   });
  
    }

    AddQuotation(e) {
       
        e.preventDefault();
        var finalItems = []
        var self = this;
        this.props.form.validateFields((err,values) => {
            if(err == null){
                self.StartSpin();
                if(this.state.selectedFile == null){
                    self.StopSpin();
                    swal("Error","Please select a file","warning");
                }
                else if(values.itemKeys.length == 0){
                    self.StopSpin();
                    swal("Error","Please insert items","warning");
                }
                else{
                    var description = this.UploaderChild.current.postDocument();
                    values.itemKeys.map(function(item){
                            finalItems.push({
                                code : values.item[item].code,
                                quantity : values.item[item].quantity,
                                unitPrice : values.item[item].unitPrice,
                                unitID : values.item[item].unitID.value,
                                amount : values.item[item].amount
                            });
                       
                    })
                    
                   var data = {
                       supplier : { code : values.supplier.value},
                       note : description,
                       document : this.state.selectedFile,
                       items : finalItems
                   };
                   self.Service.AddPriceQuotation(this.state.wfid,data).then(function(response){
                       self.StopSpin();
                       if(response.data.status == false){
                            swal("Error",response.data.error,"warning");
                       }
                       if(response.data.status == true){
                           var result = response.data.response;
                           self.setState({
                               Quotations : result
                           });
                           swal("Successful","Successfully added price quotation","success");
                           self.toggleQuotationModal();

                       }
                   }).catch(function(error){
                       self.StopSpin();
                       swal("Error",error.message,"warning");
                   });
                }
            }
        })
      
    }

    RejectRequest() {
        var self = this;
        

    }

  

    handleBlur = (e) => {
            var target = e.target;
            var key = target.id.split("[")[1].split("]")[0];
            var type = target.id.split('.')[1];
            var value = target.value;
            if(type == "quantity"){
                var n = `item[${key}].unitPrice`;
                var am =  `item[${key}].amount`;
                console.log(n);
                var v = this.props.form.getFieldValue(n);
                var product = (value * v).toFixed(2);
                this.props.form.setFieldsValue({
                    [am] : product
                });


            }
            if(type == "unitPrice"){
                var n = `item[${key}].quantity`;
                var am =  `item[${key}].amount`;
                console.log(n);
                var v = this.props.form.getFieldValue(n);
                var product = (value * v).toFixed(2);
                this.props.form.setFieldsValue({
                    [am] : product
                });
            }
        

    }

    
    checkUnit = (rule,value,callback) => {

            console.log(value);
            if(value.value != undefined){
                callback();
                return;
              }
              callback("Select Unit");

        
    }

    render(){
        const itemColumn = [
            {
                title : "Item",
                id : "code",
                //render : d => this.getItemName(d.code)
               dataIndex : "description"
            },{
                title : "Quantity",
                dataIndex : "quantity"
            }
        ]

        const getSupplierDocumentDownloader = (d) => {
            return (
                <a
                onClick={()=> {
                    console.log(d);
                   Axios(baseUrl +`/Payment/DownloadFile/${d.document.id}`, {
                       method: 'GET',
                       responseType: 'blob' //Force to receive data in a Blob Format
                   })
                   .then(response => {
                   //Create a Blob from the PDF Stream
                       const file = new Blob(
                         [response.data],
                         {type: 'application/pdf'});
                   //Build a URL from the file
                       const fileURL = URL.createObjectURL(file);
                   //Open the URL on new Window
                       window.open(fileURL);
                   })
                   .catch(error => {
                       console.log(error);
                   });


                }}

                >

               {d.document.name}</a>
            )
        }

        const getTotalAmount = (d) => {
            console.log(d);
            var items = d.items;
            var total = 0;
            items.map(function(item){
                total += item.amount
            });
            return total;
        }

        const QuotaionColumns = [
            {
                title : "Name",
                dataIndex : "supplier.nameCode"
            },{
                title  : "Description",
                dataIndex : "note"
            },
            {
                title : "Document",
                render : d => getSupplierDocumentDownloader(d)
            },{
                title : "Total Amount",
                render : d => getTotalAmount(d)
            }
        ]

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }

        };

        const itemInitializer = () => {

        }
        var self= this;

        getFieldDecorator('itemKeys',{initialValue: this.state.ItemLength });
        const itemKeys = getFieldValue('itemKeys');
        const formLabel =
        <div style={{marginBottom : '-10px'}}>
            {itemKeys.length >= 1 && this.state.Request.request.items.length > 0 ? (
        <Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          label="Item"
          style={{display: 'inline-block', width : 'calc(20%)'}}
          >

          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          label="Quantity"
          style={{display: 'inline-block', width : 'calc(10%)'}}
          >

          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
          {' '}
    </span>


            <Form.Item
            label="Unit Price"
           style={{display: 'inline-block', width : 'calc(10%)'}}
          >

            </Form.Item>


            <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
            {' '}
    </span>


            <Form.Item
          label="Unit"
                      style={{display: 'inline-block', width : 'calc(15%)'}}
                    >

          </Form.Item>




          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
          {' '}
    </span>


            <Form.Item
           label="Amount"
             style={{display: 'inline-block', width : 'calc(10%)'}}
           >

             </Form.Item>






             <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
    </span>

       </Form.Item>
      ) : null}




            </div>
        ;
        const formItems = itemKeys.map((k,index) => (
            
            <div>


<Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          style={{display: 'inline-block', width : 'calc(20%)'}}
          >
          {
              getFieldDecorator(`item[${k}].code`,{
                initialValue : this.state.Request != null ? this.state.Request.request.items[k-1].code : '',
                
               rules : [{ validator : this.checkItem}]
              })(
                <Select placeholder=""
             //   dropdownMenuStyle={{ minWidth : 250}}
                dropdownClassName="itemDropDown"
              // dropdownStyle={{ maxHeight: 400, overflow: 'auto' , width : '250px !important'}}
              //  disabled
                //style={{ width: 300 }}
                //onSelect={this.onSelect}
                >
                {
                    Object.keys(self.state.codeNameDict).length > 0 ?
                    Object.keys(self.state.codeNameDict).map(function(item){
                        if(item == self.state.Request.request.items[k-1].code){
                            return  <Option value={item}
                            key={item}
                            >
                      {self.state.codeNameDict[item]}
                        </Option>
                        }
                       
                    })
                    

                     : null
                }


                </Select>
              )
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '20px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          style={{display: 'inline-block', width : 'calc(10%)'}}
          >
          {
              getFieldDecorator(`item[${k}].quantity`,{
                initialValue : this.state.Request != null ? this.state.Request.request.items[k-1].quantity : '', 
                rules : [{ required : true, message : 'Add Quantity'}]
              })(
                  <InputNumber
                  disabled
                  onBlur={(e) => this.handleBlur(e)}
                  {...this.props} />
              )
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
          {' '}
    </span>

 <Form.Item
           style={{display: 'inline-block', width : 'calc(10%)'}}
          >
            {
                getFieldDecorator(`item[${k}].unitPrice`,{
                  initialValue : 0 || '',
                  rules : [{ required : true, message : 'Add Unit Price'}]
                })(
                    <InputNumber
                    onBlur={(e) => this.handleBlur(e)}
                    placeholder="Unit Price"/>
                )
            }
            </Form.Item>


            <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
            {'  '}
    </span>

<Form.Item
                      style={{display: 'inline-block', width : 'calc(15%)'}}
                    >
                     {
                         getFieldDecorator(`item[${k}].unitID`,{
                          initialValue : 0 || '',
                          rules : [{ validator : this.checkUnit}]
                         })(
                             <UnitSelect  {...this.props}/>
                         )
                     }
          </Form.Item>



          <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
          {' '}
    </span>


<Form.Item
             style={{display: 'inline-block', width : 'calc(10%)'}}
           >
           {
               getFieldDecorator(`item[${k}].amount`,{
                  rules : [{ required : true, message : 'Enter Amount'}]
               })(
                   <InputNumber  placeholder="Amount"/>
               )
           }
          
             </Form.Item>

             <Form.Item
           style={{display: 'inline-block', width : 'calc(8%)'}}
          >
           
            </Form.Item>



             <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
             {itemKeys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={itemKeys.length === 1}
            onClick={() => this.removeItemRow(k)}
            style={{display: 'inline-block', width : '10px'}}
          />
        ) : null}
    </span>

         



</Form.Item>


            </div>

       ));

        return (
            <Row>
                  <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <WFHistoryViewer 
        {...this.props} 
        type="purchase"
        wfid={this.state.wfid} />
        </Drawer>
                <Card
                title="Purchase Request"
                extra={
                    <Button.Group>
                        <Button onClick={() => this.toggleHistory()}>Show History</Button>
                    <Button onClick={() => this.toggleQuotationModal()}>Add Quotation</Button>
                    {
                        this.state.Quotations.length > 0 ? 
                        <Button onClick={() => this.SubmitQuotation()}>Submit Quotations</Button> : null
                    }
                    </Button.Group>
                   
                  }
                >
                {
                    this.state.Request != null ?
                    <div>


<div className="pull-right value">
                            PRN {this.state.Request.requestNo.reference}
                        </div>

                    <Row>
                    <Col span={4} className="prop">Requested By</Col>
                    <Col span={10} className="value">{this.state.Request.request.requestedBy.name}</Col>
                </Row>
                <Row>
                <Col span={4} className="prop">Items</Col>

                <Col span={10} className="value">
                <Table columns={itemColumn}
                pagination={false}
                dataSource={this.state.Request.request.items}
                />
                </Col>
            </Row>
            <Row>
                    <Col span={4} className="prop">Document </Col>
                    <Col span={18} className="value">
                    <a
                     onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.request.document.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                              [response.data],
                              {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });


                     }}

                     >

                    {this.state.Request.request.document.name}</a>
                    </Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note</Col>
                    <Col span={10} className="value">{this.state.Worflow.description}</Col>
                </Row>
                {
                    
                }
                <Row>
                <Col span={4} className="prop">Price Quotations</Col>
                    <Col span={18} className="value">
                    {
                        this.state.Quotations.length > 0  ?

                        <Table
                        className="components-table-demo-nested"
                        columns={QuotaionColumns}
                        expandedRowRender={expandedRowRender}
                        dataSource={this.state.Quotations}
                        />

                        : "No Quotations Added"
                    }
                   
                    </Col>

                
                </Row>
                
                <Modal
                title="Add Price Quotation"
                visible={this.state.QuotationModal}
                onOk={this.AddQuotation}
                onCancel={this.toggleQuotationModal}
                width='60%'
                footer={
                    [<Button onClick={this.toggleQuotationModal}>Cancel</Button>,
                     <Button onClick={this.AddQuotation}>Add Quotation</Button>]
                }
                >
                <Spin spinning={this.state.spinning}>
                <Form onSubmit={this.AddQuotation}>
                                            <Form.Item
                                           labelCol={{span : 4}}
                                           wrapperCol = {{span : 10}}
                                            label="Supplier"
                                            >
                    {
                        getFieldDecorator("supplier",{
                            rules : [{
                                required : true, message : "Please select supplier"
                            }]
                        })(
                            <SupplierSelect {...this.props} />
                        )
                    }
                                            </Form.Item>
                    <Row>
                        <Col offset={4}>
                        {formLabel}
                        {formItems}
                        </Col>
                    </Row>

                    <Form.Item  labelCol={{span : 4}}
                    wrapperCol ={{span :12 }}
                    
                    label="Document">
                  <Uploader 
                  ref={this.UploaderChild}
                  {...this.props}
                   setFile={this.setFile}/
                   
                   >
                  </Form.Item>


                    <Form.Item {...formItemLayoutWithOutLabel}>
               

                    </Form.Item>
                    </Form>


                </Spin>
                  
                </Modal>
               
            </div>
                    : null
                }


                </Card>
            </Row>
        )
    }
}

export default Form.create()(AddQuotation)