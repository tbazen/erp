import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { Card, Row, Col, Button, Form, Table, Input, InputNumber, Select, Modal, Icon, Tabs, Drawer } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import { baseUrl } from '../../service/urlConfig';
import UnitSelect from '../miniComponents/UnitSelect';
import SupplierSelect from '../miniComponents/CustomerSelect';
import DocumentUploader from '../miniComponents/DocumentUploader';
import PurchaseOrderPage from './PurchaseOrderPage';
import '../../assets/css/order.scss';
import { WFHistoryViewer } from '../WFHistoryViewer';
const { TextArea } = Input;
const ButtonGroup = Button.Group;
const { Option } = Select;
var Uploader = (DocumentUploader);
let itemRowID = 0;
const { TabPane } = Tabs;


const expandedRowRender = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    { title: 'Unit Price', dataIndex: 'unitPrice' },
    { title: 'Amount', dataIndex : 'amount' },
    
    ];

    const data = d.items;
    
    return (
    <Table
        columns={columns}
        dataSource={data}
        pagination={false}
    />
    );
};



class Finalize extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Request : null,
            Worflow : null,
            activeTab : "1",
            Quotations : [],
            Orders : [],
            OrderIDs : [],
            codeNameDict : {},
            supplierNameDict : {},
            Description : '',
            ItemLength : [],
            QuotationModal: false,
            selectedFile : null,
            selectedQuotation: null,
            AllowedQuantity : [],
            Closable : false,
            CloseRequestModal : false,
            historyDrawer : false,
            spinning : false,
        }

        this.handleChange = this.handleChange.bind(this);
        this.IssueOrder = this.IssueOrder.bind(this);
        this.UploaderChild = React.createRef();
        this.RejectRequest = this.RejectRequest.bind(this);
        this.Service = new PurchaseService();
        this.getItemName = this.getItemName.bind(this);
        this.toggleQuotationModal = this.toggleQuotationModal.bind(this);
        this.setFile = this.setFile.bind(this);
        this.getSupplierName = this.getSupplierName.bind(this);
        this.findSupplierNames = this.findSupplierNames.bind(this);
        this.SubmitQuotation = this.SubmitQuotation.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.getAllowedQuantity = this.getAllowedQuantity.bind(this);
        this.toggleCloseRequestModal = this.toggleCloseRequestModal.bind(this);
        this.CloseRequest = this.CloseRequest.bind(this);
        this.toggleHistory = this.toggleHistory.bind(this);

        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
    }

    componentWillMount(){
        var self = this;
        this.setState({
            wfid : this.props.match.params.id
        });

        this.Service.CanCloseRequest(this.props.match.params.id).then(function(response){
            console.log(response);
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                self.setState({
                    Closable : response.data.response
                });
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
    }



    componentDidMount(){
        var self = this;
        this.Service.GetLastPurchaseWorkitem(this.state.wfid).then(function(response){
            console.log(response);
            if(response.data.status == true){
                self.setState({
                    Request : response.data.response.data,
                    Worflow : response.data.response
                });
                if(response.data.response.data.quotations != null){
                    self.setState({
                        Quotations : response.data.response.data.quotations
                    });
                }
                if(response.data.response.data.purchaseOrders != null){
                    self.setState({
                        OrderIDs : response.data.response.data.purchaseOrders
                    });
                }
            }
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            if(self.state.Request != null){
                var dict = {};
                self.state.Request.request.items.map(function(item){
                    self.Service.GetItemName(item.code).then(function(response){
                       dict[item.code]=response.data;
                    }).catch(function(error){
                        swal("Error",error.message,"warning");
                    })
                })
                setTimeout(() => {
                    self.setState({
                        codeNameDict : dict
                    });
                }, 1000);

                var length= self.state.Request.request.items.length;
                var a = [];
                for (let index = 1; index <= length; index++) {
                a.push(index)
                }
                self.setState({
                    ItemLength : a
                });
              console.log(self.state);

            }

            console.log(self.state);
        })

        
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    getItemKeys(i){

        var arr = []
        for (let index = 1; index <= i; index++) {
           arr.push(index);
        }
        return arr;
    }

    onTabChange = (activeKey) => {
        this.setState({ activeTab : activeKey });
      }

      toggleCloseRequestModal(){

          this.setState({
              CloseRequestModal : !this.state.CloseRequestModal
          });
      }

      CloseRequest(e){
        var self = this;
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            console.log(values);
            if(values.note != undefined){
                self.StartSpin();
                self.Service.FinalizeRequest(self.state.wfid,values.note).then(function(response){
                    self.StopSpin();
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        swal("Success","Successfully Finalized Purchase Request","success").then(function(value){
                            self.props.history.push('/purchase');
                        });
                        
                    }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                })
            }
        })
      }

    OnOrderModalOpen(d){
        var self = this;
        self.Service.GetAllowedOrderQuantity(this.state.wfid).then(function(response){
            console.log(response);
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                var result = response.data.response;
                self.setState({
                    AllowedQuantity : result
                });
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
        var docID = d.document.id;
        var quotation = this.state.Quotations.filter(function(item){
            return item.document.id == docID;
        })[0];
        this.setState({
            selectedQuotation : quotation
        });
        this.props.form.setFieldsValue({
            'itemKeys' : this.getItemKeys(quotation.items.length),
            'supplier' :  quotation.supplier.code
            
        });
        console.log(this.props.form.getFieldValue('itemKeys'));
        this.toggleQuotationModal();
    }

    findSupplierNames(){
        var self = this;
        var suppDict = {};
        self.state.Quotations.map(function(item){
            self.Service.GetSupplierName(item.code).then(function(response){
                suppDict[item.code] = response.data;
            }).catch(function(error){
                swal("Error",error.message,"warning");
            });
        });
        self.setState({
            supplierNameDict : suppDict
        });
    }


    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }

    setFile(doc){
        this.setState({
            selectedFile : doc
        });
    }

    toggleQuotationModal(){
        this.setState({
            QuotationModal : !this.state.QuotationModal
        });
    }

    getItemName = (code) => {
        console.log(this.state.codeNameDict[code]);
        return this.state.codeNameDict[code]
    };

    getSupplierName = (code) => {
        return this.state.supplierNameDict[code];
    }

    SubmitQuotation(){
        var self = this;
        swal({
            title : "Enter Description",
            content: "input",
            attributes : {
                required : true
            }
          }).then(function(value){
              self.Service.SubmitPriceQuotation(self.state.wfid,value).then(function(response){
                  if(response.data.status == true){
                      swal("Succsss","Successfully submitted the quotations","success");
                      self.props.history.push("/purchase");
                  }
                  if(response.data.status == false){
                      swal("Error",response.data.error,"error");
                  }
              }).catch(function(error){
                  swal("Error",error.message,"error");
              });
          })
    }


    
    
    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }


    
    removeItemRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        // We need at least one passenger
        if (itemKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          itemKeys: itemKeys.filter(key => key !== k),
        });
      }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }

    IssueOrder(e) {
       
        e.preventDefault();
        var finalItems = []
        var self = this;
        this.props.form.setFieldsValue({
            "note" : " _ "
        });
        this.props.form.validateFields((err,values) => {
            if(err == null){
                if(this.state.selectedFile == null){
                    swal("Error","Please select a file","warning");
                }
                else if(values.itemKeys.length == 0){
                    swal("Error","Please insert items","warning");
                }
                else{
                    var description = this.UploaderChild.current.postDocument();
                    values.itemKeys.map(function(item){
                            finalItems.push({
                                code : values.item[item].code,
                                quantity : values.item[item].quantity,
                                unitPrice : values.item[item].unitPrice,
                                unitID : values.item[item].unitID.value,
                                amount : values.item[item].amount
                            });
                       
                    })
                    
                   var data = {
                       supplier : { code : values.supplier},
                       note : description,
                       document : this.state.selectedFile,
                       items : finalItems
                   };
                   console.log(data);
                   self.Service.IssuePurchaseOrder(this.state.wfid,data).then(function(response){
                       console.log(response);
                       if(response.data.status == false){
                            swal("Error",response.data.error,"warning");
                       }
                       if(response.data.status == true){
                           var result = response.data.response;
                           self.setState({
                               OrderIDs : [...self.state.OrderIDs, result.workflowId]
                           });
                           swal("Successful","Successfully Created Purchase Order","success");
                           self.toggleQuotationModal();

                       }
                   }).catch(function(error){
                       swal("Error",error.message,"warning");
                   });
                }
            }
        })
      
    }

    RejectRequest() {
        var self = this;
        

    }

  

    handleBlur = (e) => {
            var target = e.target;
            var key = target.id.split("[")[1].split("]")[0];
            var type = target.id.split('.')[1];
            var value = target.value;
            if(type == "quantity"){
                var n = `item[${key}].unitPrice`;
                var am =  `item[${key}].amount`;
                console.log(n);
                var v = this.props.form.getFieldValue(n);
                var product = (value * v).toFixed(2);
                this.props.form.setFieldsValue({
                    [am] : product
                });


            }
            if(type == "unitPrice"){
                var n = `item[${key}].quantity`;
                var am =  `item[${key}].amount`;
                console.log(n);
                var v = this.props.form.getFieldValue(n);
                var product = (value * v).toFixed(2);
                this.props.form.setFieldsValue({
                    [am] : product
                });
            }
        

    }

    
    checkUnit = (rule,value,callback) => {
            if(value.value != undefined){
                callback();
                return;
              }
              callback("Select Unit");

        
    }

    getAllowedQuantity(code){
        var self = this;
        if(this.state.AllowedQuantity.length > 0){
            var item = self.state.AllowedQuantity.filter(function(item){
                return item.code == code;
            })[0];
            
            return item.quantity;
        }
        else{
            return 0;
        }
    }

    render(){
        var self = this;
        const itemColumn = [
            {
                title : "Item",
                dataIndex : "description",
              //  render : d => 
              // dataIndex : "code"
            },{
                title : "Quantity",
                dataIndex : "quantity"
            },{
                title : "Remaining Quantity",
                key : "code",
                render : d => this.getAllowedQuantity(d.code)
            }
        ]

        const getSupplierDocumentDownloader = (d) => {
            return (
                <a
                onClick={()=> {
                    console.log(d);
                   Axios(baseUrl +`/Payment/DownloadFile/${d.document.id}`, {
                       method: 'GET',
                       responseType: 'blob' //Force to receive data in a Blob Format
                   })
                   .then(response => {
                   //Create a Blob from the PDF Stream
                       const file = new Blob(
                         [response.data],
                         {type: 'application/pdf'});
                   //Build a URL from the file
                       const fileURL = URL.createObjectURL(file);
                   //Open the URL on new Window
                       window.open(fileURL);
                   })
                   .catch(error => {
                       console.log(error);
                   });


                }}

                >

               {d.document.name}</a>
            )
        }

        const getTotalAmount = (d) => {
            var items = d.items;
            var total = 0;
            items.map(function(item){
                total += item.amount
            });
            return total;
        }

        const QuotaionColumns = [
            {
                title : "Name",
                dataIndex : "supplier.nameCode"
            },{
                title  : "Descriptoin",
                dataIndex : "note"
            },
            {
                title : "Document",
                render : d => getSupplierDocumentDownloader(d)
            },{
                title : "Total Amount",
                render : d => getTotalAmount(d)
            }
        ]

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }

        };

        const itemInitializer = () => {

        }
        var self= this;

        getFieldDecorator('itemKeys',{initialValue: '' });
        const itemKeys = getFieldValue('itemKeys');
        const formLabel =
        <div style={{marginBottom : '-10px'}}>
            {itemKeys.length >= 1 ? (
        <Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          label="Item"
          style={{display: 'inline-block', width : 'calc(18%)'}}
          >

          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          label="Quantity"
          style={{display: 'inline-block', width : 'calc(8%)'}}
          >

          </Form.Item>
          <span style={{ display: 'inline-block', width: '100px', textAlign: 'center' }}>
          {'Allowed Quantity'}
    </span>


            <Form.Item
            label="Unit Price"
           style={{display: 'inline-block', width : 'calc(8%)'}}
          >

            </Form.Item>


            <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
            {' '}
    </span>


            <Form.Item
          label="Unit"
                      style={{display: 'inline-block', width : 'calc(12%)'}}
                    >

          </Form.Item>




          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
          {' '}
    </span>


            <Form.Item
           label="Amount"
             style={{display: 'inline-block', width : 'calc(10%)'}}
           >

             </Form.Item>






             <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
    </span>

       </Form.Item>
      ) : null}




            </div>
        ;
        const formItems =
        this.state.selectedQuotation != null ?
        itemKeys.map((k,index) => (
            
            <div>


<Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          style={{display: 'inline-block', width : 'calc(18%)'}}
          >
          {
              getFieldDecorator(`item[${k}].code`,{
                initialValue : this.state.selectedQuotation != null ? this.state.selectedQuotation.items[k-1].code : '',
                
               rules : [{ validator : this.checkItem}]
              })(
                <Select placeholder=""
                //disabled
                dropdownClassName="itemDropDown"
                //style={{ width: 300 }}
                //onSelect={this.onSelect}
                >
                {
                    Object.keys(self.state.codeNameDict).length > 0 ?
                    Object.keys(self.state.codeNameDict).map(function(item){
                        if(item == self.state.selectedQuotation.items[k-1].code){
                            return  <Option value={item}
                            key={item}
                            >
                      {self.state.codeNameDict[item]}
                        </Option>
                        }
                       
                    })
                    

                     : null
                }


                </Select>
              )
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '20px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          style={{display: 'inline-block', width : 'calc(8%)'}}
          >
          {
              getFieldDecorator(`item[${k}].quantity`,{
                initialValue : 0 || '',
                rules : [{ required : true, message : 'Add Quantity'}]
              })(
                  <InputNumber
                  max={this.getAllowedQuantity(this.state.selectedQuotation.items[k-1].code)}
                  onBlur={(e) => this.handleBlur(e)}
                  {...this.props} />
              )
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '100px', textAlign: 'center' }}>
          {this.getAllowedQuantity(this.state.selectedQuotation.items[k-1].code)}
    </span>

 <Form.Item
           style={{display: 'inline-block', width : 'calc(8%)'}}
          >
            {
                getFieldDecorator(`item[${k}].unitPrice`,{
                  initialValue : this.state.selectedQuotation != null ? this.state.selectedQuotation.items[k-1].unitPrice : '',
                  rules : [{ required : true, message : 'Add Unit Price'}]
                })(
                    <InputNumber
                    disabled
                    onBlur={(e) => this.handleBlur(e)}
                    placeholder="Unit Price"/>
                )
            }
            </Form.Item>


            <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
            {'  '}
    </span>

<Form.Item
                      style={{display: 'inline-block', width : 'calc(12%)'}}
                    >
                     {
                         getFieldDecorator(`item[${k}].unitID`,{
                          initialValue : 0 || '',
                          rules : [{ validator : this.checkUnit}]
                         })(
                             <UnitSelect
                             initialValue = {this.state.selectedQuotation != null ? this.state.selectedQuotation.items[k-1].unitID : ''}
                             {...this.props}/>
                         )
                     }
          </Form.Item>



          <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
          {''}
    </span>


<Form.Item
             style={{display: 'inline-block', width : 'calc(10%)'}}
           >
           {
               getFieldDecorator(`item[${k}].amount`,{
                  rules : [{ required : true, message : 'Enter Amount'}]
               })(
                   <InputNumber  placeholder="Amount"/>
               )
           }
          
             </Form.Item>

             <Form.Item
           style={{display: 'inline-block', width : 'calc(8%)'}}
          >
           
            </Form.Item>



             <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
             {itemKeys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={itemKeys.length === 1}
            onClick={() => this.removeItemRow(k)}
            style={{display: 'inline-block', width : '10px'}}
          />
        ) : null}
    </span>

         



</Form.Item>


            </div>

       )) : null;

        return (
                <div className="purchseOrder">
                    <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <WFHistoryViewer 
        {...this.props} 
        type="purchase"
        wfid={this.state.wfid} />
        </Drawer>
 <Tabs  
 defaultActiveKey="1"
            onChange = {this.onTabChange}
           activeKey = {this.state.activeTab}
           type='card'
           style={{margin : '0 !important'}}
            >
    <TabPane tab="Purchase Request" key="1">
    <Card
                title="Purchase Request"
                extra ={
                    <Button.Group>
                        <Button onClick={() => this.toggleHistory()}>Show History</Button>
                        {
                          this.state.Closable ? <Button onClick={() => this.toggleCloseRequestModal()}>Finalize Request</Button> : null

                        }
                    </Button.Group>
                }
                >
                {
                    this.state.Request != null ?
                    <div>

    
    <Row>
                        <Col span={4} className="prop">Requested By</Col>
                        <Col span={10} className="value">{this.state.Request.request.requestedBy.name}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Items</Col>
    
                    <Col span={16} className="value">
                    <Table columns={itemColumn}
                    pagination={false}
                    dataSource={this.state.Request.request.items}
                    />
                    </Col>
                </Row>
                <Row>
                        <Col span={4} className="prop">Document </Col>
                        <Col span={18} className="value">
                        <a
                         onClick={()=> {
                            Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.request.document.id}`, {
                                method: 'GET',
                                responseType: 'blob' //Force to receive data in a Blob Format
                            })
                            .then(response => {
                            //Create a Blob from the PDF Stream
                                const file = new Blob(
                                  [response.data],
                                  {type: 'application/pdf'});
                            //Build a URL from the file
                                const fileURL = URL.createObjectURL(file);
                            //Open the URL on new Window
                                window.open(fileURL);
                            })
                            .catch(error => {
                                console.log(error);
                            });
    
    
                         }}
    
                         >
    
                        {this.state.Request.request.document.name}</a>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4} className="prop">Note</Col>
                       
                    </Row>
                    <Row>
                    <Row>
                        <Col span={4} className="prop">Price Quotations</Col>
                        <Col span={18} className="value"> <Table
                        className="components-table-demo-nested"
                        columns={QuotaionColumns}
                        expandedRowRender={expandedRowRender}
                        dataSource={this.state.Quotations}
                        /></Col>
                    </Row>
                    <Row>

                   
                    </Row>
            
                    </Row>
                    
                        
                    
    </div>
             
    : null
    
                }

                </Card> 
    </TabPane>
            <TabPane tab="Purchase Order" key="2">
            <Card 
            title="Purchase Orders">
            {
               this.state.OrderIDs.length > 0 ? 
               <Tabs
               tabPosition="left"
             >
             {
                 this.state.OrderIDs.map(function(item,index){
                        return <TabPane tab={`Order #${index+1}`} key={index}>
                        <PurchaseOrderPage {...self.props} 
                        wfid={item}
                        />                       
                        </TabPane>
                 })
             }
               

             </Tabs>

               : null
           }
           

               
             
            </Card>
            </TabPane>
    </Tabs>
                        <Modal
                    title="Issue Purchase Order"
                    visible={this.state.QuotationModal}
                    onOk={this.IssueOrder}
                    onCancel={this.toggleQuotationModal}
                    okText="Issue Order"
                    cancelText="Cancel"
                    width='70%'                >

                        <Form onSubmit={this.IssueOrder}>
                    <Form.Item  {...formItemLayout}   label="Supplier"
                                                >
                        {
                            getFieldDecorator("supplier",{
                                rules : [{
                                    required : true, message : "Please select supplier"
                                }]
                            })(
                                <Select placeholder="Please select supplier .." disabled >
                                {
                                    this.state.Quotations != null ?
                                    this.state.Quotations.map(function(item){
                                        return  <Option value={item.supplier.code}
                                        key={item.supplier.code}
                                        
                                        >
                                {item.supplier.name}
                                        </Option>
                                    }) : null
                                }
                                
                            
                                </Select>
                            )
                        }
                                                </Form.Item>
                        <Row>
                            <Col offset={4}>
                            {formLabel}
                            {formItems}
                            </Col>
                        </Row>

                        <Form.Item  labelCol={{span : 4}}
                        wrapperCol ={{span :12 }}
                        
                        label="Document">
                    <Uploader 
                    ref={this.UploaderChild}
                    {...this.props}
                    setFile={this.setFile}/
                    
                    >
                    </Form.Item>


                        <Form.Item {...formItemLayoutWithOutLabel}>


                        </Form.Item>
                        </Form>


                    </Modal>      

                    <Modal
                    title="Finalize Request"
                    visible={this.state.CloseRequestModal}
                    onOk={this.CloseRequest}
                    onCancel={this.toggleCloseRequestModal}
                    okText="Close Request"
                    cancelText="Cancel"
                    width="40%"
                    footer={[
                        <Button key="back" onClick={this.toggleCloseRequestModal}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={this.state.spinning} onClick={this.CloseRequest}>
                          Finalize
                        </Button>,
                      ]}  
                    >
                         <Form onSubmit={this.CloseRequest}>
                            <Form.Item
                            label="Note"
                            labelCol={ {span : 4 }}
                            wrapperCol = {{ span : 18}}
                            >
                            {
                                getFieldDecorator('note',{
                                    rules : [{ required : true, message : "Please add note"}]
                                })(
                                    <TextArea rows={5} {...this.props} />
                                )
                            }
                            </Form.Item>
                            </Form>
                    </Modal>
                </div>

        )
        
    }
}

export default Form.create() (Finalize);


