import React, { Component } from 'react';
import { PurchaseService } from '../../../service/purchase.service';
import { Card, Row, Col, Button, Form, Table, Input, InputNumber, Select, Modal, Icon, Tabs } from 'antd';
import swal from 'sweetalert';
import PurchaseOrderviewer from '../Viewer/PurchaseOrderViewer';
import { Link } from '@react-pdf/renderer';

const { TextArea } = Input;
const ButtonGroup = Button.Group;

class ReviewOrder extends Component {
    constructor(props){
        super(props);
        this.state ={
            wfid : '',
            Workitem : '',
            Description : '',
        };
        this.Service=  new PurchaseService();
        this.handleChange = this.handleChange.bind(this);
        this.ApproveRequest = this.ApproveRequest.bind(this);
        this.RejectRequest = this.RejectRequest.bind(this);
    }

    componentWillMount(){
        var self = this;
        this.setState({
            wfid : this.props.match.params.id
        });
        this.Service.GetLastPurchaseOrderWorkitem(this.props.match.params.id).then(function(response){
            if(response.data.status == true){
                self.setState({
                    Workitem : response.data.response
                });
            }
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
    }
    
    componentDidMount(){
        this.props.loader.stop();
    }

    
    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        console.log(this.state.Description);

    }

    ApproveRequest(e) {
        var self = this;
        e.preventDefault();
        self.props.loader.start();
        this.Service.ApprovePurchaseOrder(this.state.wfid,this.state.Description).then(function(response){
            self.props.loader.stop();
            if(response.data.status){
                swal("Success","Purchase Order Approved","success");
                self.props.history.push('/purchaseOrder');
            }
            if(!response.data.status){
                swal("Error",response.data.error,"error")
            }
        }).catch(function(error){
            self.props.loader.stop();
            swal("Error",error.message,"warning");
        });
    }

    RejectRequest() {
        var self = this;
        self.props.loader.start();
        this.Service.RejectPurchaseOrder(this.state.wfid,this.state.Description).then(function(response){
            self.props.loader.stop();
            if(response.data.status){
                swal("Success","Purchase Order Rejected","success");
                self.props.history.push('/purchaseOrder');
            }
            if(!response.data.status){
                swal("Error",response.data.error,"error");
            }
        }).catch(function(error){
            self.props.loader.stop();
            swal("Error",error.message,"warning");
        });

    }


    render(){

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }
            
        };

        return(


          
            <PurchaseOrderviewer {...this.props}
            wfid={this.state.wfid}
            >
        <Form onSubmit={this.ApproveRequest}>
                


                        <Form.Item
                        {...formItemLayout}
                        label = "Description"
                        className="prop"
                        >
                        {
                        getFieldDecorator("Description",{
                                rules : [{
                                    required : true, message : "Please add description"
                                }]
                        }) (
                            <TextArea name="Description" required  onChange={(e) => this.handleChange(e)} rows={4} />
                        )
                        }
                        
                        </Form.Item>

                        <Form.Item {...formItemLayoutWithOutLabel}>
                        <ButtonGroup className="pull-right"
                            >

                                                <Button 
                                                disabled={this.state.Description == ''} type="danger"
                                                onClick={this.RejectRequest}
                                                >Reject
                                                </Button>

                                                <Button type="primary" htmlType="submit">Approve</Button>

                                            </ButtonGroup>
                    
                        </Form.Item>
                    </Form>

                    </PurchaseOrderviewer>

        )
    }
}

export default Form.create()(ReviewOrder);