import React, { Component } from 'react';

import { PurchaseService } from '../../../service/purchase.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button } from 'antd';
import swal from 'sweetalert';

export default class View extends Component {
    constructor(props){
        super(props);
        this.state = {
            Workflows : []
        };

        this.Service = new PurchaseService();
    }

    componentDidMount(){
        var self = this;
        this.Service.GetPurchaseOrderWorkflows().then(function(response){
            console.log(response);
            if(response.data.status == true){
                var result = response.data.response;
                if(result != null && result.length > 0){
                    console.log(result);
                    if(self.props.session.role == 10){
                        self.setState({
                            Workflows : result.filter(function(item){
                                return item.currentState == 1;
                            })
                        });
                    }
                }
            }
        })
        
    }

    CancelRequest(wfid){
        var self = this;
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want to remove this document?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willCancel => {
            if(willCancel){
                
            }
          });
    }

    render(){
        

       
        const DescriptionMinimizer = (desc) => {
            if(desc == null || desc == '')
                return <p></p>
            else
                return <p>{desc.substring(0,30)}</p>
        };

        const GetViewLink = (wfid,state) => {
            var role = this.props.session.role;
            if(state == 0){
                return  
                <div>
                    <Button
                onClick={
                    () => this.CancelRequest(wfid)
                }
                >Cancel</Button>
                <Button>Resubmit</Button>
                    </div>
                
              
            }
            if(state == 1 ){
                return  <Link to={`/purchase/reviewReq/${wfid}`}>
                <Button>Review Request</Button>
                
                </Link>
            }
            if(state == 2 ){
                return  <Link to={`/purchase/process/${wfid}`}>
                <Button>Process</Button>
                
                </Link>
            }
            if(state == 3){
                return  <Link to={`/purchase/order/${wfid}`}>
                <Button>Execute</Button>
                
                </Link>
            }
            if(state == 4 ){
                return  <Link to={`/purchase/revieOrder/${wfid}`}>
                <Button>Review Order</Button>
                
                </Link>
            }
        }

        const columns = [{
            title: "Initiator",
            key : "id",
            dataIndex: "action.username"
        }, {
            title: "Descrition",
            render : d => DescriptionMinimizer(d.description)
        }, {
            title: "Time",
            dataIndex: "action.timeStr"
        }, {

            title: 'Action',
            id: "aid",
            render: d => <Link to={`/purchaseOrder/review/${d.id}`}>
            <Button>Review Order</Button>
            </Link>
        }

        ];

        return(
            <Row>
                <Col span={24}>
                <Card 
                title="Purchase Request"
                extra = { this.props.session.role == 1 || this.props.session.role == 8 ? <Button onClick={() => this.props.history.push('/purchase/request')}>Add New Request</Button> : null}
                >
                
                <Table
                columns = {columns}
                dataSource = {this.state.Workflows}
                /> 
                </Card>
                </Col>
            </Row>
        )
    }
}