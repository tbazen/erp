import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { StoreService} from '../../service/store.service';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Select, InputNumber, Drawer } from 'antd';
import swal from 'sweetalert';
import DocumentUploader from '../miniComponents/DocumentUploader';
import CostCenterSearch from '../miniComponents/CostCenterSearch';
import EmployeeSelect from '../miniComponents/EmployeeSelect';
import { Link } from 'react-router-dom';
import { WFHistoryViewer } from '../WFHistoryViewer';
const { Option } = Select;
var Uploader = (DocumentUploader);
const  ButtonGroup  = Button.Group;
const { TextArea } = Input;

const serviceRows = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    
    ];

    if(d.approvedItems != null && d.approvedItems.length > 0){
        const data = d.approvedItems;
    
        return (
        <Table
            columns={columns}
            dataSource={data}
            pagination={false}
        />
        );
    }
    else{
        return <div className="value">Delivery On Process</div>;
    }

};


const itemRows = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    
    ];

    if(d.deliveredItems != null && d.deliveredItems.length > 0){
        const data = d.deliveredItems;
    
        return (
        <Table
            columns={columns}
            dataSource={data}
            pagination={false}
        />
        );
    }
    else{
        return <div className="value">Delivery On Process</div>;
    }

};



export default class PurchaseOrderPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Order : null,
            PaymentWorkflows : [],
            TotalAmount : '',
            PaymentModal : false,
            selectedFile : null,
            cashPayment : false,
            DeliveryModal : false,
            ServiceDeliveryModal : false,
            ItemDeliveryWorkflows : [],
            ServiceDeliveryWorkflows : [],
            IDNameDict : null,
            RemainingItemOrders : [],
            RemainingServiceOrders : [],
            CloseOrderModal : false,
            historyDrawer : false,
            spinning : false,
        }
        this.Service = new PurchaseService();
        this.StoreService = new StoreService("");
        this.getOrderStatus = this.getOrderStatus.bind(this);
        this.togglePaymetModal = this.togglePaymetModal.bind(this);
        this.RequestPayment= this.RequestPayment.bind(this);
        this.AllowedRequestAmount = this.AllowedRequestAmount.bind(this);
        this.UploaderChild = React.createRef();
        this.setFile= this.setFile.bind(this);
        this.PaymentTypeSelected = this.PaymentTypeSelected.bind(this);
        this.toggleDeiveryModal = this.toggleDeiveryModal.bind(this);
        this.toggleServiceDeliveryModal = this.toggleServiceDeliveryModal.bind(this);
        this.getItemKeys = this.getItemKeys.bind(this);
        this.SubmitRequestDelivery = this.SubmitRequestDelivery.bind(this);
        this.OrderContainsGoods = this.OrderContainsGoods.bind(this);
        this.OrderContainsService = this.OrderContainsService.bind(this);
        this.RequestItemDelivery = this.RequestItemDelivery.bind(this);
        this.RequestServiceDelivery = this.RequestServiceDelivery.bind(this);
        this.GetEmployeeName = this.GetEmployeeName.bind(this);
        this.UndeliveredItemsRemain = this.UndeliveredItemsRemain.bind(this);
        this.UndeliveredServiceRemain = this.UndeliveredServiceRemain.bind(this);
        this.CanCloseOrder = this.CanCloseOrder.bind(this);
        this.toggleCloseOrderModal = this.toggleCloseOrderModal.bind(this);
        this.CloseOrder = this.CloseOrder.bind(this);
        this.toggleHistory = this.toggleHistory.bind(this);
        this.ResubmitOrder = this.ResubmitOrder.bind(this);
        this.RemainingPaymentAmount = this.RemainingPaymentAmount.bind(this);

        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
    }

    componentWillMount(){
        var self = this;
        this.setState({
            wfid : this.props.wfid
        });




        this.Service.GetLastPurchaseOrderWorkitem(this.props.wfid).then(function(response){
            console.log(response);
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                console.log(response.data.response);
                const itemDeliveryWFs = response.data.response.data.itemDeliveryWorkflows;
                const serviceDeliveryWFs = response.data.response.data.serviceDeliveryWorkflows;
                self.setState({
                    Order : response.data.response,
                    TotalAmount : response.data.response.data.items.sum("amount"),
                    ItemDeliveryWorkflows : itemDeliveryWFs != null && itemDeliveryWFs.length > 0 ? itemDeliveryWFs : [],
                    ServiceDeliveryWorkflows : serviceDeliveryWFs != null && serviceDeliveryWFs.length > 0 ?  serviceDeliveryWFs : [],
                });
                var pwf = response.data.response.data.paymentWorkflows;
                if(pwf != null && pwf.length > 0){
                    self.setState({
                        PaymentWorkflows: pwf
                    });
                }

            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            console.log(self.state);
            if(self.state.Order.currentState != 1){
                self.StoreService.GetRemainingItemOrders(self.props.wfid).then(function(response){
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        self.setState({
                            RemainingItemOrders : response.data.response
                        })
                    }
                }).catch(function(error){
                    swal("Error",error.message,"warning");
                });
                
                self.StoreService.GetRemainingServiceOrders(self.props.wfid).then(function(response){
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        self.setState({
                            RemainingServiceOrders : response.data.response
                        })
                    }
                }).catch(function(error){
                    swal("Error",error.message,"warning");
                }).then(function(){
                    console.log(self.state);
                });
            }
            if(self.state.ServiceDeliveryWorkflows.length > 0){
                var dict = {};
                self.state.ServiceDeliveryWorkflows.map(function(item){
                    self.StoreService.GetNameForUID(item.approverID).then(function(response){
                        if(response.data.status == true){
                            dict[item.approverID] = response.data.response
                        }
                        
                    });
                })
                self.setState({
                    IDNameDict : dict
                })
            }
        }).then(function(){
            console.log(self.state);
        });
    }

    AllowedRequestAmount(){
        if(this.state.PaymentWorkflows.length == 0){
            return Number(this.state.TotalAmount).toFixed(2);
        }
        else{
            var amount = this.state.PaymentWorkflows.filter(function(item){
                return item.currentState != 0;
            }).sum("requestedAmount");
            return (this.state.TotalAmount - amount).toFixed(2);
        }
    }

    RemainingPaymentAmount(){
        if(this.state.PaymentWorkflows.length == 0){
            return Number(this.state.TotalAmount).toFixed(2);
        }
        else{
            var amount = this.state.PaymentWorkflows.filter(function(item){
                return item.currentState == -1;
            }).sum("requestedAmount");
            return (this.state.TotalAmount - amount).toFixed(2);
        }
    }

    

    
    setFile(doc){
        this.setState({
            selectedFile : doc
        });
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    componentDidMount(){

    }



    togglePaymetModal(){
        this.setState({
            PaymentModal : !this.state.PaymentModal
        });
    }

    toggleDeiveryModal(){
        this.setState({
            DeliveryModal : !this.state.DeliveryModal
        });
    }

    toggleServiceDeliveryModal(){
        this.setState({
            ServiceDeliveryModal : !this.state.ServiceDeliveryModal
        });
    }

    toggleCloseOrderModal(){
        this.setState({
            CloseOrderModal : !this.state.CloseOrderModal
        })
    };

    checkCostCenter = (rule,value,callback) => {
        if(value != undefined){
          callback();
          return;
        }
        callback("Select Cost Center");

    }

    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }

    ResubmitOrder(){
        var supplierCode = this.state.Order.data.supplier.code;
        this.props.ResubmitOrder(supplierCode,this.state.wfid);
    }

    CancelOrder(){
        this.props.CancelOrder(this.state.wfid);
    }


    PaymentTypeSelected(value){
        if(value == 3){
            this.setState({
                cashPayment : true
            });
            this.props.form.setFieldsValue({
                "amount" : this.AllowedRequestAmount()
            })
        }
        else{
            this.setState({
                cashPayment : false
            })
            this.props.form.setFieldsValue({
                "amount" : 0
            })
        }


    }

    CloseOrder(e){
        var self = this;
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            console.log(values);
            if(values.note != undefined){
                self.StartSpin();
                self.Service.ClosePurchaseOrder(self.state.wfid,values.note).then(function(response){
                    self.StopSpin();
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        swal("Success","Successfully Closed Order","success").then(function(value){
                            window.location.reload();
                        });
                        
                    }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                })
            }
        })
    }



    getItemKeys(){

        var arr = [];
        if(this.state.Order == null || this.state.Order.data.items.length == 0){
            return [];
        }
        else{
            var i = this.state.Order.data.items.length;
            for (let index = 1; index <= i; index++) {
                if(this.state.Order.data.items[index-1].type == 1){
                    arr.push(index);
                }
               
             }
             return arr;
        }

    }

    RequestPayment(e){
        e.preventDefault();
        var self = this;
        this.props.form.validateFields((err,values) => {
            if(this.state.selectedFile == null){
                swal("Error","Please select a file","warning");
            }
            else{
                self.StartSpin();
                var description = this.UploaderChild.current.postDocument();
                values.note = values.Description;
                values.document = this.state.selectedFile;
                values.requestedAmount = values.amount;
                self.Service.RequestPurchasePayment(self.state.wfid,values).then(function(response){
                    self.StopSpin();
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        swal("Success","Payment requested successfully","success");
                        var data = response.data.response;
                        self.setState({
                            PaymentWorkflows : [...self.state.PaymentWorkflows, data]
                        })
                        self.togglePaymetModal();
                    }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                })
            }
        })

    }

    GetEmployeeName (d)  {
        
        console.log(this.state.IDNameDict);
        if(this.state.IDNameDict == null || Object.keys(this.state.IDNameDict).length == 0){
            return "";
        }
        else{
            return this.state.IDNameDict[d.approverID];
        }

    }


    RequestItemDelivery(e){
        e.preventDefault();
        var self = this;
        this.props.form.validateFields((err,values) => {
            console.log(err);
            console.log(values);
            if(values.note != undefined){
                self.StartSpin();
                var data = {

                    orderedItems : self.state.Order.data.items.filter(function(i){

                        return i.type == 1;
                    }),
                    note : values.note,
                    supplier : self.state.Order.data.supplier
                };
                self.Service.RequestItemDelivery(self.state.wfid,data).then(function(response){
                    self.StopSpin();
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        swal("Success","Successfully requested item delivery","success");
                        var data = response.data.response;
                        self.setState({
                            ItemDeliveryWorkflows : [...self.state.ItemDeliveryWorkflows, data]
                        })
                        self.toggleDeiveryModal();
                    }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                })
            }
            
        })
    }

    RequestServiceDelivery(e){
        e.preventDefault();
        var self =this;
        this.props.form.validateFields((err,values) => {
            
            if(values.note != '' && values.approverID != undefined){
                console.log(values);
                self.StartSpin();
                var data = {
                    approverID : values.approverID.value,
                    orderedItems : self.state.Order.data.items.filter(function(i){
                        return i.type == 2;
                    }),
                    note : values.note,
                    supplier : self.state.Order.data.supplier
                };
                console.log(data);
                self.Service.RequestServiceDelivery(self.state.wfid,data).then(function(response){
                    self.StopSpin();
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        swal("Success","Successfully requested service certification","success");
                        var data = response.data.response;
                        self.setState({
                            ServiceDeliveryWorkflows : [...self.state.ServiceDeliveryWorkflows, data]
                        })
                        self.toggleServiceDeliveryModal();
                    }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                })
            }
        
        })
    }

    OrderContainsGoods(){
        if(this.state.Order != null){
            var result = this.state.Order.data.items.filter(function(i){
                return i.type == 1;
            });
            return result.length > 0;
        }
        else
            return false;
    }

    OrderContainsService(){
        if(this.state.Order != null){
            var result = this.state.Order.data.items.filter(function(i){
                return i.type == 2;
            });
            return result.length > 0;
        }
        else
            return false;
    }

    UndeliveredItemsRemain(){
        if(this.state.RemainingItemOrders != null && this.state.RemainingItemOrders.length > 0){
            var quantity = this.state.RemainingItemOrders.sum("quantity");
            if(quantity == 0){
                return false;
            }
            else{
                return true;
            }
        }
        else 
            return true;
    }
    
    UndeliveredServiceRemain(){
        if(this.state.RemainingServiceOrders != null && this.state.RemainingServiceOrders.length > 0){
            var quantity = this.state.RemainingServiceOrders.sum("quantity");
            if(quantity == 0){
                return false;
            }
            else{
                return true;
            }
        }
        else 
            return true;
    }

    CanCloseOrder(){

        if(this.OrderContainsGoods() && !this.UndeliveredItemsRemain() && !this.OrderContainsService()){
            return true;
        }
        else if(this.OrderContainsGoods() && !this.UndeliveredItemsRemain() && this.OrderContainsService() && !this.UndeliveredServiceRemain()){
            return true;
        }
        else if(this.OrderContainsService()&& !this.UndeliveredServiceRemain() && !this.OrderContainsGoods()){
            return true;
        }
        else {
            return false;
        }

    }



    SubmitRequestDelivery(e){
        e.preventDefault();
        var self = this;
        this.props.form.validateFields((err,values) => {
            console.log(values);
        })
    }
        
    removeItemRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        // We need at least one passenger
        if (itemKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          itemKeys: itemKeys.filter(key => key !== k),
        });
      }


    getOrderStatus(){
        var state = this.state.Order.currentState;
        if(state == 0){
            return "Rejected";
        }
        else if(state == 1){
            return "Under Review";
        }
        else if(state == 2){
            return "Approved";
        }
        else if(state == -1){
            return "Closed";
        }
        else if(state == -2){
            return "Terminated";
        }

    }

    render(){

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }

        };

        var self= this;

        getFieldDecorator('itemKeys',{initialValue: this.getItemKeys() });
        const itemKeys = getFieldValue('itemKeys');
        const formLabel =
        <div style={{marginBottom : '-10px'}}>
            {itemKeys.length >= 1 ? (
        <Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          label="Item"
          style={{display: 'inline-block', width : 'calc(40%)'}}
          >

          </Form.Item>
          <span style={{ display: 'inline-block', width: '30px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          label="Quantity"
          style={{display: 'inline-block', width : 'calc(8%)'}}
          >

          </Form.Item>
        





             <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
    </span>

       </Form.Item>
      ) : null}




            </div>
        ;
        const formItems =
        this.state.Order != null && this.state.Order.data != null ?
        itemKeys.map((k,index) => (
            
            <div>


<Form.Item
            style={{marginBottom : '-10px'}}
              >




          <Form.Item
          style={{display: 'inline-block', width : 'calc(40%)'}}
          >
          {
              getFieldDecorator(`item[${k}].code`,{
                initialValue : this.state.Order.data != null ? this.state.Order.data.items[k-1].code : '',
                
               rules : [{ validator : this.checkItem}]
              })(
                <Select placeholder=""
                disabled
                //style={{ width: 300 }}
                //onSelect={this.onSelect}
                >
                {
                   this.state.Order.data.items != null ?
                    this.state.Order.data.items.map(function(item){
                        return  <Option value={item.code}
                        key={item.code}
                        >
                  {item.description}
                    </Option>
                    })
                    : null

                    
                }


                </Select>
              )
          }
          </Form.Item>
          <span style={{ display: 'inline-block', width: '20px', textAlign: 'center' }}>
     {' '}
    </span>
          <Form.Item
          style={{display: 'inline-block', width : 'calc(40%)'}}
          >
          {
              getFieldDecorator(`item[${k}].quantity`,{
                initialValue : 0 || '',
                rules : [{ required : true, message : 'Add Quantity'}]
              })(
                  <InputNumber
                //   max={this.getAllowedQuantity(this.state.Order.data.items[k-1].code)}
                  onBlur={(e) => this.handleBlur(e)}
                  {...this.props} />
              )
          }
          </Form.Item>
         

             <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
             {itemKeys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={itemKeys.length === 1}
            onClick={() => this.removeItemRow(k)}
            style={{display: 'inline-block', width : '10px'}}
          />
        ) : null}
    </span>

         



</Form.Item>


            </div>

       )) : null;

        const getPaymentTypes = (d) => {
            if(d.type == 1){
                return "Supplier Advance Payment"
            }
            if(d.type == 2){
                return "Supplier Credit Settlement"
            }
            if(d.type == 3){
                return "Cash Payment"
            }
        }


        const getPaymentStatus = (d) => {
            var state = d.currentState;
            if(state == 0){
                return "Rejected"
            }
            if(state == 1){
                return "Check"
            }
            if(state == 2){
                return "Review"
            }
            if(state == 3){
                return "Payment"
            }
            if(state == 4){
                return "Finalization"
            }
            if(state == -1){
                return "Closed"
            }
            if(state == -2){
                return "Cancelled"
            }

            
        }

        const getItemDeliveryStatus = (d) => {
            var state = d.currentState;
            if(state == 0){
                return "Filing"
            }
            if(state == 1){
                return "Check"
            }
            if(state == 2){
                return "Review"
            }
            if(state == -1){
                return "Closed"
            }
            if(state == -2){
                return "Cancelled"
            }

        }

        
        const getServiceDeliveryStatus = (d) => {
            var state = d.currentState;
            if(state == 0){
                return "Filing"
            }
            if(state == 1){
                return "Check"
            }
            if(state == 2){
                return "Review"
            }
            if(state == -1){
                return "Closed"
            }
            if(state == -2){
                return "Cancelled"
            }

        }

        const renderDocument = (d) => {
            console.log(d);
            if(d.document == null){
                return "";
            }
            return (
                <a
                onClick={()=> {
                    console.log(d);
                   Axios(baseUrl +`/Payment/DownloadFile/${d.document.id}`, {
                       method: 'GET',
                       responseType: 'blob' //Force to receive data in a Blob Format
                   })
                   .then(response => {
                   //Create a Blob from the PDF Stream
                       const file = new Blob(
                         [response.data],
                         {type: 'application/pdf'});
                   //Build a URL from the file
                       const fileURL = URL.createObjectURL(file);
                   //Open the URL on new Window
                       window.open(fileURL);
                   })
                   .catch(error => {
                       console.log(error);
                   });


                }}

                >

               {d.document.name}</a>
            )
        }

        const pwfColumns = [{
            title : "Type",
            render : d => getPaymentTypes(d)
        }, {
            title : "Requested Amount",
            dataIndex : "requestedAmount"
        }, {
            title : "Paid Amount",
            dataIndex : "paidAmount",
        },
        {
            title : "Status",
            render : d => getPaymentStatus(d)
        }
        ,{
            title : "Document",
            render : d => renderDocument(d)
        },{
            title : "Note",
            dataIndex : "note",
            width : "30%"
        }
    ]

    const itemDeiveryColumns = [
        {
            title : "Note",
            dataIndex : "note"
        },
        {
            title : "Document",
            render : d => renderDocument(d)
        },
        {
            title : "Current State",
            render : d => getItemDeliveryStatus(d)
        }
    ]

    const serviceDeliveryColumns = [
        {
            title : "Approver",
            render : d => this.GetEmployeeName(d)
        },{
            title : "Note",
            dataIndex : "note"
        },
        {
            title : "Document",
            render : d => renderDocument(d)
        },
        {
            title : "Current State",
            render : d => getServiceDeliveryStatus(d)
        }
    ]


        const itemColumn = [
            { title: 'Name', dataIndex: 'description', key: 'code' },
            { title: 'Quantity', dataIndex: 'quantity'},
            { title: 'Unit Price', dataIndex: 'unitPrice' },
            { title: 'Amount', dataIndex : 'amount' },
            
            ];

            const getActionButtons = (state) => {
               if(state == 0){
                   return <ButtonGroup>
                       <Button onClick={() => this.ResubmitOrder()}>Resubmit Order</Button>
                       <Button type="danger" onClick={() => this.CancelOrder()}>Cancel Order</Button>
                       <Button onClick={() => this.toggleHistory()}>Show History</Button>
                   </ButtonGroup>
               }
               if(state == 1){
                   return  <Button onClick={() => this.toggleHistory()}>Show History</Button>
               }
               if(state == 2){
                return <ButtonGroup>
                    <Button onClick={() => this.toggleHistory()}>Show History</Button>
                    {
                        this.state.wfid != null || this.state.wfid != '' ?
                         <Link to={  `/Document/4/${this.state.wfid}/1`}>
                         <Button>Download PO</Button>
                        </Link>
                       : null
                    }
                    {
                        this.AllowedRequestAmount() != 0  ?
                        <Button onClick={() => this.togglePaymetModal()}>Request Payment</Button>
                        : null
                    }
                    {
                        this.OrderContainsGoods() && this.UndeliveredItemsRemain() ? <Button onClick={() => this.toggleDeiveryModal()}>Request Item Delivery</Button> : null
                    }
                    {
                        this.OrderContainsService() && this.UndeliveredServiceRemain() ? <Button onClick={() => this.toggleServiceDeliveryModal()}>Request Service Delivery</Button>   : null
                    }
                    {
                        this.CanCloseOrder() && this.AllowedRequestAmount() == 0 && this.RemainingPaymentAmount() == 0 ? <Button onClick={() => this.toggleCloseOrderModal()}>Close Order </Button> : null
                    }
                    
                </ButtonGroup>
            }
            }

        return(
            <div>
            <Drawer
                    title="Payment History"
                    placement="right"
                    closable={true}
                    onClose={this.toggleHistory}
                    visible={this.state.historyDrawer}
                    width={400}
                    >
                    <WFHistoryViewer 
                    {...this.props} 
                    type="order"
                    wfid={this.state.wfid} />
                    </Drawer>
               {
                   this.state.Order != null ?

                    <div>
                        <Row>
                            <div className="pull-right">
                    {getActionButtons(this.state.Order.currentState)}
                            </div>
                        </Row>
                         <Row>
                        <Col span={4} className="prop">Supplier</Col>
                        <Col span={10} className="value">{this.state.Order.data.supplier.name}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Items</Col>
    
                    <Col span={16} className="value">
                    <Table columns={itemColumn}
                    pagination={false}
                    dataSource={this.state.Order.data.items}
                    footer ={ () =>
                        <Row>
                            <Col span={6}>Total</Col>
                            <Col span={18} style={{textAlign : "right"}}>{this.state.TotalAmount}</Col>
                        </Row>
                    }
                    />
                    </Col>
                </Row>
                <Row>
                        <Col span={4} className="prop">Document </Col>
                        <Col span={18} className="value">
                        <a
                         onClick={()=> {
                            Axios(baseUrl +`/Payment/DownloadFile/${this.state.Order.data.document.id}`, {
                                method: 'GET',
                                responseType: 'blob' //Force to receive data in a Blob Format
                            })
                            .then(response => {
                            //Create a Blob from the PDF Stream
                                const file = new Blob(
                                  [response.data],
                                  {type: 'application/pdf'});
                            //Build a URL from the file
                                const fileURL = URL.createObjectURL(file);
                            //Open the URL on new Window
                                window.open(fileURL);
                            })
                            .catch(error => {
                                console.log(error);
                            });
    
    
                         }}
    
                         >
    
                        {this.state.Order.data.document.name}</a>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4} className="prop">Note</Col>
                        <Col span={18} className="value">{this.state.Order.data.note}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Status</Col>
                        <Col span={18} className="value">{this.getOrderStatus()}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Workitem Description</Col>
                        <Col span={18} className="value">{this.state.Order.description}</Col>
                    </Row>
                    {
                        this.state.Order.currentState != 1 && this.state.Order.currentState != 0 && this.state.Order.currentState != -3 ?
                                <div>
                            
                                  <hr/>   
                    <Card
                    bordered={false}
                    title="Payment Requests">
                      {
                            this.state.PaymentWorkflows.length > 0 ?
                            <div>
 <Table
                            dataSource={this.state.PaymentWorkflows.filter(function(item){
                                return item.currentState != -3;
                            })}
                            columns ={pwfColumns} pagination={false}
                            /> 
                            </div> : "No Payment Requested"
                           
                        }
                    </Card>

                        <hr/>
                        
                        {
                            this.OrderContainsGoods() ?
                            <div>
                               
    
                                <Card
                                title="Item Deliveries"
                                bordered={false}>
                                    {
                                    this.state.ItemDeliveryWorkflows != null && this.state.ItemDeliveryWorkflows.length > 0 ?
                                    <Table
                                    dataSource= {this.state.ItemDeliveryWorkflows.filter(function(item){
                                        return item.currentState != -2;
                                    })}
                                    columns={itemDeiveryColumns}
                                    expandedRowRender={itemRows} 
                                    pagination={false}/>

                                : "No Item Delivery Requests Made"
                                }
                                </Card>
                                <hr />
                            </div>
                             : null
                        }
                        {
                            this.OrderContainsService()  ?
                            <div>
                                <Card
                                title="Service Deliveries"
                                bordered={false}>
                                  {
                                   this.state.ServiceDeliveryWorkflows != null && this.state.ServiceDeliveryWorkflows.length > 0 ?
                                   
                                   <div>
                                                <hr/>
                                                <Table
                                    dataSource= {this.state.ServiceDeliveryWorkflows.filter(function(item){
                                        return item.currentState != -3;
                                    })}
                                    columns={serviceDeliveryColumns}
                                    expandedRowRender={serviceRows} pagination={false}/>
                                   </div>
                                  

                                : "No Service Delivery Requests Made"
                                }
                                </Card>
                              <hr/>
                                </div>
                             : null
                        }
                                    </div>
                        : null
                    }
                  
                    </div>
                   : null
               }
                  <Modal
                    title="Request Payment"
                    visible={this.state.PaymentModal}
                    onOk={this.RequestPayment}
                    onCancel={this.togglePaymetModal}
                    okText="Request Payment"
                    cancelText="Cancel"
                    width='70%' 
                    footer={[
                        <Button key="back" onClick={this.togglePaymetModal}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={this.state.spinning} onClick={this.RequestPayment}>
                          Submit
                        </Button>,
                      ]}               >

                        <Form onSubmit={this.RequestPayment}>
                    <Form.Item 
                    {...formItemLayout}
                    label = "Request Type">
                    {
                        getFieldDecorator("type",{
                            rules : [{required : true, message: "Please select payment request type"}]
                        })(
                            <Select
                            onChange={(e) => this.PaymentTypeSelected(e)}
                            placeholder="Please select payment request type">
                            <Option key="1">Supplier Advanced Payment</Option>
                            <Option key="2">Supplier Credit Settlement</Option>
                            <Option key="3">Cash Payment</Option>
                            </Select>
                        )
                    }
                    </Form.Item>
                    <Form.Item 
                    {...formItemLayout}
                    label = "Amount">
                    {
                        getFieldDecorator("amount",{
                            rules : [{required : true, message: "Please insert amount"}]
                        })(
                           <InputNumber 
                           disabled={this.state.cashPayment}
                           max ={this.AllowedRequestAmount()}
                           />
                        )
                    }
                    {`  of ${this.AllowedRequestAmount()} Birr`}
                    </Form.Item>

                        <Form.Item  labelCol={{span : 4}}
                        wrapperCol ={{span :12 }}
                        
                        label="Document">
                    <Uploader 
                    ref={this.UploaderChild}
                    {...this.props}
                    setFile={this.setFile}/
                    
                    >
                    </Form.Item>


                        <Form.Item {...formItemLayoutWithOutLabel}>


                        </Form.Item>
                        </Form>



                    </Modal>    

                    <Modal
                    title="Close Order"
                    visible={this.state.CloseOrderModal}
                    onOk={this.CloseOrder}
                    onCancel={this.toggleCloseOrderModal}
                    okText="Close Order"
                    cancelText="Cancel"
                    width="40%"
                    footer={[
                        <Button key="back" onClick={this.toggleCloseOrderModal}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={this.state.spinning} onClick={this.CloseOrder}>
                          Submit
                        </Button>,
                      ]}      
                    >
                         <Form onSubmit={this.CloseOrder}>
                            <Form.Item
                            label="Note"
                            labelCol={ {span : 4 }}
                            wrapperCol = {{ span : 18}}
                            >
                            {
                                getFieldDecorator('note',{
                                    rules : [{ required : true, message : "Please add note"}]
                                })(
                                    <TextArea {...this.props} />
                                )
                            }
                            </Form.Item>
                            </Form>
                    </Modal>


                    <Modal
                    title="Request Service Delivery"
                    visible={this.state.ServiceDeliveryModal}
                    onOk={this.RequestServiceDelivery}
                    onCancel={this.toggleServiceDeliveryModal}
                    okText="Submit Request"
                    cancelText="Cancel"
                    width="40%"
                    footer={[
                        <Button key="back" onClick={this.toggleServiceDeliveryModal}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={this.state.spinning} onClick={this.RequestServiceDelivery}>
                          Submit
                        </Button>,
                      ]}      
                    >
                        <Form onSubmit={this.RequestServiceDelivery}>
                            <Form.Item
                            label="Certifier"
                            labelCol={ {span : 4 }}
                            wrapperCol = {{ span : 12}}
                            >
                            {
                                getFieldDecorator('approverID',{
                                    rules : [{ required : true, message : "Please select employee"}]
                                })(
                                    <EmployeeSelect {...this.props} />
                                )
                            }
                            </Form.Item>

                        <Form.Item
                        label="Note"
                        labelCol={ {span : 4 }}
                            wrapperCol = {{ span : 16}}>
                        {
                            getFieldDecorator('note',{
                                rules : [{ required : true, message : "Please add note"}]
                            })(
                                <TextArea rows={6} {...this.props} rows={5} />
                            )
                        }
                        </Form.Item>

                        </Form>
                    </Modal>

                      <Modal
                    title="Request Item Delivery"
                    visible={this.state.DeliveryModal}
                    onOk={this.RequestItemDelivery}
                    onCancel={this.toggleDeiveryModal}
                    okText="Submit Request"
                    cancelText="Cancel"
                    width='40%'         
                    footer={[
                        <Button key="back" onClick={this.toggleDeiveryModal}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={this.state.spinning} onClick={this.RequestItemDelivery}>
                          Submit
                        </Button>,
                      ]}              >

                        <Form onSubmit={this.RequestItemDelivery}>
                        <Form.Item
                        label="Note"
                        labelCol={ {span : 4 }}
                        wrapperCol = {{ span : 16}}>
                        {
                            getFieldDecorator('note',{
                                rules : [ { required : true, message: "Please add note"}]
                            })(
                                <TextArea rows={6}/>
                            )
                        }

                        </Form.Item>
                    
                        </Form>


                    </Modal>      
         
         
            </div>
        )
    }

}
