import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { Card, Row, Col, Button, Icon,
    Form, Select, InputNumber, Input, textArea,
    DatePicker } from 'antd';
import ItemRequestForm2 from '../miniComponents/ItemRequestForm2';
import EmployeeSelect from '../miniComponents/EmployeeSelect';
import DocumentUploader from '../miniComponents/DocumentUploader';
import swal from 'sweetalert';

const { Option } = Select;
var ItemForm = (ItemRequestForm2);
var Uploader = (DocumentUploader);
class PurchaseRequest extends Component {
    constructor(props){
        super(props);

        this.state = {
            selectedFile : null,
        }

        this.ItemFormChild = React.createRef();
        this.UploaderChild = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setFile = this.setFile.bind(this);
        this.Service = new PurchaseService();
    }

    componentDidMount(){
        this.props.loader.stop();
    }

    handleSubmit(e){
        e.preventDefault();
        console.log(this.ItemFormChild);
        console.log(this.UploaderChild);
        var self = this;

        this.props.form.validateFields((err,values) => { 
            console.log(err);
            console.log(values);
            if(err == null){
                self.props.loader.start();
                var items = this.ItemFormChild.current.postDocument();
                var description = this.UploaderChild.current.postDocument();
                console.log(items);
                        console.log(values);
                        console.log(description);
                        console.log(this.state);
                        if(items == null || items.length == 0){
                            self.props.loader.stop();
                        }
                else if(items.length > 0 && (description != null || description != ''))
                {
                    if(this.state.selectedFile == null){

                        self.props.loader.stop();
                        swal("Error","Please Select File","warning");
                    }
                    else{
                        var name = values.requestedBy.Employees.filter(function(item){
                            return item.id == values.requestedBy.value;
                        })[0].employeeName;
                        console.log(name);
                        var requestedBy = {
                            id : values.requestedBy.value,
                            name : name
                        };
                        var data = {
                            requestedBy,
                            note : description,
                            items : items,
                            document  : this.state.selectedFile,
                            requestDate : new Date()

                        }
                        console.log(data);
                        self.Service.CreatePurchaseWorkflow(data).then(function(response){
                            self.props.loader.stop();
                            if(response.data.status == true){
                                swal("Success","Purchase Request Submitted","success");
                                self.props.history.push('/purchase');
                            }
                            if(response.data.status == false){
                                swal("Error",response.data.error,"warning");
                            }
                        }).catch(function(error){
                            self.props.loader.stop();
                            swal("Error",error.message,"warning");
                        })
                    }
                }
            }

        })
    }

    setFile(doc){
        if(doc != null){
            this.setState({
                selectedFile : doc
            });
        }
    }

    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 4 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 20 },
            },
          };
          const formItemLayoutWithOutLabel = {
            wrapperCol: {
              xs: { span: 24, offset: 0 },
              sm: { span: 20, offset: 4 },
            },
          };

          return(
              <Row>
                  <Card
                  title="Create Purchase Request">
                  
                  <Form
                  onSubmit={this.handleSubmit}
                  layout="horizontal"
                  >
                  <Form.Item 
                  labelCol={{span : 4}}
                    wrapperCol ={{span : 6}}
                  label="Requested By">
                  {
                      getFieldDecorator("requestedBy", {
                          rules : [{ required : true, message : "Please Select Employee"}]
                      })(
                        <EmployeeSelect   {...this.props} />
                      )
                  }
                  </Form.Item>
                  
                   
                  <Form.Item {...formItemLayout} label="Requested Items">
                  <ItemForm 
                  ref={this.ItemFormChild}
                  {...this.props}
                  showPriceFields={false} />
                  </Form.Item>
                  <Form.Item  labelCol={{span : 4}}
                    wrapperCol ={{span :12 }}
                    
                    label="Document">
                  <Uploader 
                  ref={this.UploaderChild}
                  {...this.props}
                   setFile={this.setFile}/
                   
                   >
                  </Form.Item>
                  <Form.Item
          wrapperCol={{ span: 12, offset: 6 }}
        >
                                        <Button htmlType="submit">Submit</Button>
        </Form.Item>
                  </Form>
                  </Card>
              </Row>
          )
    }
}

export default Form.create()(PurchaseRequest);