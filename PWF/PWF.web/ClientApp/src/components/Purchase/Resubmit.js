import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { Card, Row, Col, Button, Icon,
    Form, Select, InputNumber, Input, textArea, Table,
    DatePicker, 
    Drawer} from 'antd';
import ItemRequestForm2 from '../miniComponents/ItemRequestForm2';
import EmployeeSelect from '../miniComponents/EmployeeSelect';
import DocumentUploader from '../miniComponents/DocumentUploader';
import swal from 'sweetalert';
import { WFHistoryViewer } from '../WFHistoryViewer';

const { Option } = Select;
var ItemForm = (ItemRequestForm2);
var Uploader = (DocumentUploader);
class Resubmit extends Component {
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            selectedFile : null,
            Request : null,
            Worflow : null,
            codeNameDict : {},
            Description : '',
            historyDrawer : false,
        }

        this.ItemFormChild = React.createRef();
        this.UploaderChild = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setFile = this.setFile.bind(this);
        this.Service = new PurchaseService();
        this.toggleHistory = this.toggleHistory.bind(this);
    }

    componentWillMount(){
        this.setState({
            wfid : this.props.match.params.id
        });
    }

    componentDidMount(){
        this.props.loader.stop();
        var self = this;
        this.Service.GetLastPurchaseWorkitem(this.state.wfid).then(function(response){
            console.log(response);
            if(response.data.status == true){
                self.setState({
                    Request : response.data.response.data,
                    Worflow : response.data.response
                });
            }
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            if(self.state.Request != null){
                var dict = {};
                self.state.Request.request.items.map(function(item){
                    self.Service.GetItemName(item.code).then(function(response){
                       dict[item.code]=response.data;                     
                    }).catch(function(error){
                        swal("Error",error.message,"warning");
                    })
                })
                setTimeout(() => {
                    self.setState({
                        codeNameDict : dict
                    });
                }, 500);

            }
        })
    }

    handleSubmit(e){
        e.preventDefault();
        console.log(this.ItemFormChild);
        console.log(this.UploaderChild);
        var self = this;

        this.props.form.validateFields((err,values) => { 
            console.log(err);
            console.log(values);
            if(err == null){
                self.props.loader.start();
                var items = this.ItemFormChild.current.postDocument();
                if(items == null || items.length == 0){
                    self.props.loader.stop();
                }
                var description = this.UploaderChild.current.postDocument();
                console.log(items);
                        console.log(values);
                        console.log(description);
                        console.log(this.state);
                if(items.length > 0 && (description != null || description != '')){
                    if(this.state.selectedFile == null){

                        self.props.loader.stop();
                        swal("Error","Please Select File","warning");
                    }
                    else{
                        var name = values.requestedBy.Employees.filter(function(item){
                            return item.id == values.requestedBy.value;
                        })[0].employeeName;
                        console.log(name);
                        var requestedBy = {
                            id : values.requestedBy.value,
                            name : name
                        };
                        var data = {
                            requestedBy,
                            note : description,
                            items : items,
                            document  : this.state.selectedFile,
                            requestDate : new Date()

                        }
                        console.log(data);
                        self.Service.ResubmitPurchaseRequest(self.state.wfid,data).then(function(response){
                            self.props.loader.stop();
                            if(response.data.status == true){
                                swal("Success","Purchase Request Resubmitted","success");
                                self.props.history.push('/purchase');
                            }
                            if(response.data.status == false){
                                swal("Error",response.data.error,"warning");
                            }
                        }).catch(function(error){
                            self.props.loader.stop();
                            swal("Error",error.message,"warning");
                        })
                    }
                }
            }

        })
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    setFile(doc){
        if(doc != null){
            this.setState({
                selectedFile : doc
            });
        }
    }

    render() {
                const itemColumn = [
            {
                title : "Item",
                id : "code",
               // render : d => this.getItemName(d.code)
               dataIndex : "description"
            },{
                title : "Quantity",
                dataIndex : "quantity"
            }
        ]
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 4 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 20 },
            },
          };
          const formItemLayoutWithOutLabel = {
            wrapperCol: {
              xs: { span: 24, offset: 0 },
              sm: { span: 20, offset: 4 },
            },
          };

          return(
              <Row>
                  <Drawer
        title="Purchase History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <WFHistoryViewer 
        {...this.props} 
        type="purchase"
        wfid={this.state.wfid} />
        </Drawer>
                  <Card
                  title="Resubmit Purchase Request"
                  extra={<Button onClick={() => this.toggleHistory()}>Show History</Button>}
                  >
                   {
                    this.state.Request != null ? 
                    <div>

                        <div className="pull-right value">
                            PRN {this.state.Request.requestNo.reference}
                        </div>
                   
                    <Row>
                    <Col span={4} className="prop">Requested By</Col>
                    <Col span={10} className="value">{this.state.Request.request.requestedBy.name}</Col>
                </Row>
                <Row>
                <Col span={4} className="prop">Items</Col>

                <Col span={10} className="value">
                <Table columns={itemColumn}
                pagination={false}
                dataSource={this.state.Request.request.items}
                />
                </Col>
            </Row>
            <Row>
                    <Col span={4} className="prop">Note</Col>
                    <Col span={10} className="value">{this.state.Worflow.description}</Col>
                </Row>
            </div> : null}
                   
                  <Form
                  onSubmit={this.handleSubmit}
                  layout="horizontal"
                  >
                  <Form.Item 
                  labelCol={{span : 4}}
                    wrapperCol ={{span : 6}}
                  label="Requested By">
                  {
                      getFieldDecorator("requestedBy", {
                          rules : [{ required : true, message : "Please Select Employee"}]
                      })(
                        <EmployeeSelect   {...this.props} />
                      )
                  }
                  </Form.Item>
                  
                   
                  <Form.Item {...formItemLayout} label="Requested Items">
                  <ItemForm 
                  ref={this.ItemFormChild}
                  {...this.props}
                  showPriceFields={false} />
                  </Form.Item>
                  <Form.Item  labelCol={{span : 4}}
                    wrapperCol ={{span :12 }}
                    
                    label="Document">
                  <Uploader 
                  ref={this.UploaderChild}
                  {...this.props}
                   setFile={this.setFile}/
                   
                   >
                  </Form.Item>
                  <Form.Item
          wrapperCol={{ span: 12, offset: 6 }}
        >
                                        <Button type="primary" htmlType="submit">Submit</Button>
        </Form.Item>
                  </Form>
                  </Card>
              </Row>
          )
    }
}

export default Form.create()(Resubmit);