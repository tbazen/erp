import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { Card, Row, Col, Button, Form, Table, Input, Modal, Drawer } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import { baseUrl } from '../../service/urlConfig';
import { WFHistoryViewer } from '../WFHistoryViewer';

const { TextArea } = Input;
const ButtonGroup = Button.Group;

class ReviewRequest extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Request : null,
            Worflow : null,
            codeNameDict : {},
            Description : '',
            historyDrawer : false,
            
        }

        this.handleChange = this.handleChange.bind(this);
        this.ApproveRequest = this.ApproveRequest.bind(this);
        this.RejectRequest = this.RejectRequest.bind(this);
        this.Service = new PurchaseService();
        this.getItemName = this.getItemName.bind(this);
        this.toggleHistory = this.toggleHistory.bind(this);
    }


    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }



    AddQuotation(e){
        e.preventDefault();
    }

    getItemName = (code) => {
        console.log(this.state.codeNameDict[code]);
        return this.state.codeNameDict[code]
    };

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    ApproveRequest(e) {
        var self = this;
        e.preventDefault();
        this.props.form.validateFields((err,values) => {
            if(err == null){
                console.log(values);
                self.props.loader.start();
                self.Service.ApprovePurchaseRequest(this.state.wfid,values.Description).then(function(response){
                   self.props.loader.stop();
                    if(response.data.status){
                        swal("Success","Purchase Request Approved","success");
                        self.props.history.push('/purchase');
                    }
                    if(!response.data.status){
                        swal("Error",response.data.error,"error")
                    }
                }).catch(function(error){
                    self.props.loader.stop();
                    swal("Error",error.message,"warning");
                });
            }
        })

    }

    RejectRequest() {
        var self = this;
        this.props.form.validateFields((err,values) => {
            if(err == null){
                console.log(values);
                self.props.loader.start();
                self.Service.RejectPurchaseRequest(this.state.wfid,values.Description).then(function(response){
                    self.props.loader.stop();
                    if(response.data.status){
                        swal("Success","Purchase Request Rejected","success");
                        self.props.history.push('/purchase');
                    }
                    if(!response.data.status){
                        swal("Error",response.data.error,"error");
                    }
                }).catch(function(error){
                    self.props.loader.stop();
                    swal("Error",error.message,"warning");
                });
            }
        })


    }

    componentWillMount(){
        this.setState({
            wfid : this.props.match.params.id
        });
    }

    componentDidMount(){
        var self = this;
        self.props.loader.stop();
        this.Service.GetLastPurchaseWorkitem(this.state.wfid).then(function(response){
            console.log(response);
            if(response.data.status == true){
                self.setState({
                    Request : response.data.response.data,
                    Worflow : response.data.response
                });
            }
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            if(self.state.Request != null){
                var dict = {};
                self.state.Request.request.items.map(function(item){
                    self.Service.GetItemName(item.code).then(function(response){
                       dict[item.code]=response.data;                     
                    }).catch(function(error){
                        swal("Error",error.message,"warning");
                    })
                })
                setTimeout(() => {
                    self.setState({
                        codeNameDict : dict
                    });
                }, 500);

            }
        })
    }

    render(){
        const itemColumn = [
            {
                title : "Item",
                id : "code",
               // render : d => this.getItemName(d.code)
               dataIndex : "description"
            },{
                title : "Quantity",
                dataIndex : "quantity"
            }
        ]

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 10}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 10, offset: 4
            }
            
        };


        return (
            <Row>
                        <Drawer
        title="Purchase History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <WFHistoryViewer 
        {...this.props} 
        type="purchase"
        wfid={this.state.wfid} />
        </Drawer>
                <Card
                title="Purchase Request"
               extra={<Button onClick={() => this.toggleHistory()}>Show History</Button>}
                >
                {
                    this.state.Request != null ? 
                    <div>

<div className="pull-right value">
                            PRN {this.state.Request.requestNo.reference}
                        </div>
                    <Row>
                    <Col span={4} className="prop">Requested By</Col>
                    <Col span={10} className="value">{this.state.Request.request.requestedBy.name}</Col>
                </Row>
                <Row>
                <Col span={4} className="prop">Items</Col>

                <Col span={10} className="value">
                <Table columns={itemColumn}
                pagination={false}
                dataSource={this.state.Request.request.items}
                />
                </Col>
            </Row>
            <Row>
                    <Col span={4} className="prop">Document </Col>
                    <Col span={18} className="value">
                    <a
                     onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.request.document.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                              [response.data], 
                              {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                        
                       
                     }}
                     
                     >

                    {this.state.Request.request.document.name}</a>
                    </Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note</Col>
                    <Col span={18} className="value">{this.state.Request.request.note}</Col>
                </Row>
                <Form onSubmit={this.ApproveRequest}>
        


                <Form.Item
                {...formItemLayout}
                label = "Description"
                >
                {
                   getFieldDecorator("Description",{
                        rules : [{
                            required : true, message : "Please add description"
                        }]
                }) (
                    <TextArea name="Description" required  onChange={(e) => this.handleChange(e)} rows={4} />
                )
                }
                   
                </Form.Item>

                <Form.Item {...formItemLayoutWithOutLabel}>
                <ButtonGroup className="pull-right"
                    >

                                        <Button 
                                        disabled={this.state.Description == ''} 
                                        onClick={this.RejectRequest}
                                        >Reject
                                        </Button>

                                        <Button htmlType="submit">Approve</Button>

									</ButtonGroup>
               
                </Form.Item>
            </Form>

  
            </div>
                    : null
                }
             

                </Card>
            </Row>
        )
    }
}

export default Form.create()(ReviewRequest)