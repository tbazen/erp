import React, { Component } from 'react';

import { PurchaseService } from '../../service/purchase.service';
import { Link } from 'react-router-dom';
import { Table, Divider,Card, Row, Col, Button } from 'antd';
import swal from 'sweetalert';
import { PRW } from '../../service/WFStates';

export default class View extends Component {
    constructor(props){
        super(props);
        this.state = {
            Workflows : []
        };

        this.Service = new PurchaseService();
    }

    componentDidMount(){
        const RoleState = { 1 : 0 , 9 : 3};
        var self = this;
        this.Service.GetPurchaseWorkflows().then(function(response){
            console.log(response);
            if(response.data.status == true){
                var result = response.data.response;
                if(result != null && result.length > 0){
                    console.log(result);
                    if(self.props.session.role != 10 && self.props.session.role != 8){
                        self.setState({
                            Workflows : result.filter(function(item){
                                return item.currentState == RoleState[self.props.session.role]
                            })
                        });
                    }
                    if(self.props.session.role == 8 || self.props.session.role == 2){
                        self.setState({
                            Workflows : result
                            // .filter(function(item){
                            //     return item.currentState == 0 || item.currentState == 2;
                            // })
                        });
                    }
                    if(self.props.session.role == 10){
                        self.setState({
                            Workflows : result.filter(function(item){
                                return item.currentState == 1 || item.currentState == 4;
                            })
                        });
                    }
                }
            }
        })
        
    }

    CancelRequest(wfid){
        var self = this;
        swal({
            title: "Are you sure?",
            text: "Are you sure that you want cancel this request?",
            icon: "warning",
            dangerMode: true,
          })
          .then(willCancel => {
              console.log(willCancel);
            if(willCancel == true){
                self.Service.CancelPurchaseRequest(wfid).then(function(response){
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        swal("","Successfully cancelled purchase request","success");
                            self.setState({
                                Workflows : self.state.Workflows.filter(function(item){
                                    return item.id != wfid;
                                })
                          
                        })
                    }
                })
            }
          });
    }

    render(){
        

       
        const DescriptionMinimizer = (desc) => {
            if(desc == null || desc == '')
                return <p></p>
            else
                return <p>{desc.substring(0,30)}</p>
        };

        const GetViewLink = (wfid,state) => {
            var role = this.props.session.role;
            if(role == 8){
                if(state == 0){
                    return  <div>
                        <Button
                    onClick={
                        () => this.CancelRequest(wfid)
                    }
                    >Cancel</Button>
                    <Link to={`/purchase/resubmit/${wfid}`}>
                    <Button>Resubmit</Button>
                    </Link>
                    
                        </div>
                    
                  
                }
                else if(state == 2 ){
                    return  <Link to={`/purchase/process/${wfid}`}>
                    <Button>Process</Button>
                    
                    </Link>
                }
                else{
                   return <Link to={`/wf/view/${PRW}/${wfid}`}>
                    <Button>View</Button>
                    
                    </Link>
                }
            }
            else if(role == 2){
                if(state == 0){
                    return  <div>
                        <Button
                    onClick={
                        () => this.CancelRequest(wfid)
                    }
                    >Cancel</Button>
                    <Link to={`/purchase/resubmit/${wfid}`}>
                    <Button>Resubmit</Button>
                    </Link>
                    
                        </div>
                    
                  
                }
                else{
                   return <Link to={`/wf/view/${PRW}/${wfid}`}>
                    <Button>View</Button>
                    
                    </Link>
                }
            }
            else{
                
                if(state == 1 ){
                    return  <Link to={`/purchase/reviewReq/${wfid}`}>
                    <Button>Review</Button>
                    
                    </Link>
                }
                
                if(state == 3){
                    return  <Link to={`/purchase/order/${wfid}`}>
                    <Button>Execute</Button>
                    
                    </Link>
                }
                if(state == 4 ){
                    return  <Link to={`/purchase/finalize/${wfid}`}>
                    <Button>Finalize</Button>
                    
                    </Link>
                }
            }

        }

        const columns = [{
            title: "Initiator",
            key : "id",
            dataIndex: "action.username"
        }, {
            title: "Descrition",
            render : d => DescriptionMinimizer(d.description)
        }, {
            title: "Time",
            dataIndex: "action.timeStr"
        }, {

            title: 'Action',
            id: "aid",
            render: d => GetViewLink(d.id, d.currentState)
        }

        ];

        return(
            <Row>
                <Col span={24}>
                <Card 
                title="Purchase Request"
                extra = { this.props.session.role == 2 || this.props.session.role == 8 ? <Button onClick={() => this.props.history.push('/purchase/request')}>Add New Request</Button> : null}
                >
                
                <Table
                columns = {columns}
                dataSource = {this.state.Workflows}
                /> 
                </Card>
                </Col>
            </Row>
        )
    }
}