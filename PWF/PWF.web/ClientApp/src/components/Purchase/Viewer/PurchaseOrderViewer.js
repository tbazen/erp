import React, { Component } from 'react';
import { PurchaseService } from '../../../service/purchase.service';
import { baseUrl } from '../../../service/urlConfig';
import Axios from 'axios';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Drawer } from 'antd';
import swal from 'sweetalert';
import { WFHistoryViewer } from '../../WFHistoryViewer';

export default class PurchaseOrderViewer extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Order : null,
            TotalAmount : '',
            historyDrawer : false,
        }
        this.Service = new PurchaseService();
        this.toggleHistory = this.toggleHistory.bind(this);
    }

    componentWillMount(){
        console.log(this);
        var self = this;
        this.setState({
            wfid : this.props.match.params.id || this.props.wfid
        });
        this.Service.GetLastPurchaseOrderWorkitem(this.props.match.params.id || this.props.wfid).then(function(response){
            console.log(response);
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                self.setState({
                    Order : response.data.response,
                    TotalAmount : response.data.response.data.items.sum("amount")
                });
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
    }

    componentDidMount(){

    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    render(){
        const itemColumn = [
            { title: 'Name', dataIndex: 'description', key: 'code' },
            { title: 'Quantity', dataIndex: 'quantity'},
            { title: 'Unit Price', dataIndex: 'unitPrice' },
            { title: 'Amount', dataIndex : 'amount' },
            
            ];

        return(
            <div>
                    <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <WFHistoryViewer 
        {...this.props} 
        type="order"
        wfid={this.state.wfid} />
        </Drawer>
                <Card 
                title="Purchase Order"
                extra = {
                    <div>
                        <Button onClick={() => this.toggleHistory()}>Show History</Button>
                        {
                              this.state.Order && this.props.showLink == null != null ?
                              <Button
                              onClick={() =>this.props.history.push(`/purchase/view/request/${this.state.Order.parentWFID}`)}
                              >View Purchase Request</Button> : null
                        }
                    </div>
                  
                }>

                

               {
                   this.state.Order != null ?

                    <div>
                         <Row>
                        <Col span={4} className="prop">Supplier</Col>
                        <Col span={10} className="value">{this.state.Order.data.supplier.name}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Items</Col>
    
                    <Col span={18} className="value">
                    <Table columns={itemColumn}
                    pagination={false}
                    dataSource={this.state.Order.data.items}
                    footer ={ () =>
                        <Row>
                            <Col span={6}>Total</Col>
                            <Col span={18} style={{textAlign : "right"}}>{this.state.TotalAmount}</Col>
                        </Row>
                    }
                    />
                    </Col>
                </Row>
                <Row>
                        <Col span={4} className="prop">Document </Col>
                        <Col span={18} className="value">
                        <a
                         onClick={()=> {
                            Axios(baseUrl +`/Payment/DownloadFile/${this.state.Order.data.document.id}`, {
                                method: 'GET',
                                responseType: 'blob' //Force to receive data in a Blob Format
                            })
                            .then(response => {
                            //Create a Blob from the PDF Stream
                                const file = new Blob(
                                  [response.data],
                                  {type: 'application/pdf'});
                            //Build a URL from the file
                                const fileURL = URL.createObjectURL(file);
                            //Open the URL on new Window
                                window.open(fileURL);
                            })
                            .catch(error => {
                                console.log(error);
                            });
    
    
                         }}
    
                         >
    
                        {this.state.Order.data.document.name}</a>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4} className="prop">Note</Col>
                        <Col span={18} className="value">{this.state.Order.data.note}</Col>
                    </Row>
                    </div>
                   : null
               }
            {

                this.props.children
            }
            </Card>
            </div>
        )
    }

}