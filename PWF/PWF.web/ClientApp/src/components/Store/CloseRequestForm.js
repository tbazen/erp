
import React, { Component } from 'react';
import {  Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Button  } from 'antd';
import Axios from 'axios';
import swal from 'sweetalert';
import { baseUrl } from '../../service/urlConfig';
import { StoreService } from '../../service/store.service';
import { PaymentService } from '../../service/payment.service';
export default class CloseRequestForm extends React.Component{

    constructor(props){
        super(props);
        this.state = { 
        selectedFile: null, 
        loaded: 0,
        description : null,
        spinning : false,
        }

        this.handleUpload = this.handleUpload.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.Service = new StoreService("");
    }

     
    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    }

    handleselectedFile = event => {
        console.log(event.target.files);
        this.setState({
            selectedFile: event.target.files,
            loaded: 0,
        })
    }

    handleUpload = () => {
        var self = this;
        self.setState({
            spinning : true
        });
        const config = {
            headers: {
                'Authorization': 'bearer ' + this.props.session.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
        console.log(this.state);
        var self = this;
     const data = new FormData();
        if(this.state.selectedFile == null){
            swal("Error","Please attach document","warning");
            self.setState({
                spinning : false
            });
        }
        else if(this.state.description == null || this.state.description == ""){
            swal("Error","Please add description","warning");
            self.setState({
                spinning : false
            });
        }
        else{
            var doc = {
                ContentType : this.state.selectedFile[0].type,
               // Length : this.state.selectedFile[0].size,
                Name : this.state.selectedFile[0].name
            }
            var fileReader = new FileReader();
            var array = [];
            if (fileReader && this.state.selectedFile && this.state.selectedFile.length) {
               fileReader.readAsArrayBuffer(this.state.selectedFile[0]);
               fileReader.onload = function () {
                  doc.Content =Array.from(new Uint8Array(fileReader.result));
               };
            }

           //doc.Description = this.state.description;
            var t = {
                 
                    File : doc,
                    Description : this.state.description
                
                
            };
            console.log(t);

            self.Service.UploadIssueVoucher(self.props.wfid,doc).then(function(response){
                self.setState({
                    spinning : false
                });
                if(response.data == true){
                    swal("Success","Successfully closed request","success").then(function(value){
                        self.props.history.push('/store');
                    })
                    
                }
            }).catch(function(error){
                self.setState({
                    spinning : false
                });
                swal("Error",error.message,"warning");
            });
        }


    }


    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
    }

    render(){


        return (
            <div>

                    <FormGroup>
                        <Label for="exampleText">Description</Label>
                        <Input type="textarea" name="description" onChange={(e) => this.handleChange(e)} id="exampleText" />
                    </FormGroup>

                    <FormGroup>
                        <Label for="exampleFile">Signed Store Issue Voucher Documents</Label>
                        <Input type="file" name="files" id="" onChange={this.handleselectedFile} id="exampleFile" />
                        <FormText color="muted">
                            Upload Documents
                         </FormText>
                    </FormGroup>
                <Button className="pullRight" onClick={this.handleUpload} type="primary" loading={this.state.spinning}>Close Request</Button>
                   
                    </div>

            /* </form>
                <input  />
               
                <div> {Math.round(this.state.loaded, 2)} %</div>
            </div> */
        )
    }
}

