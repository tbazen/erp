<Modal
title="Request Delivery"
visible={this.state.DeliveryModal}
onOk={this.toggleDeiveryModal}
onCancel={this.toggleDeiveryModal}
okText="Deliver Items"
cancelText="Cancel"
width='50%'                >

    <Form onSubmit={this.toggleDeiveryModal}>
    {formLabel}
    {formItems}
    <Form.Item 
    {...formItemLayout}
    label="Cost Center">
{
    getFieldDecorator(`costCenterID`,{
        rules : [{ validator : this.checkCostCenter}]
    })(
        <CostCenterSearch  {...this.props} />
    )
 }
    </Form.Item>
{/* <Form.Item 
{...formItemLayout}
label = "Request Type">
{
    getFieldDecorator("type",{
        rules : [{required : true, message: "Please select payment request type"}]
    })(
        <Select
        onChange={(e) => this.PaymentTypeSelected(e)}
        placeholder="Please select payment request type">
        <Option key="1">Supplier Advanced Payment</Option>
        <Option key="2">Supplier Credit Settlement</Option>
        <Option key="3">Cash Payment</Option>
        </Select>
    )
}
</Form.Item>
<Form.Item 
{...formItemLayout}
label = "Amount">
{
    getFieldDecorator("amount",{
        rules : [{required : true, message: "Please insert amount"}]
    })(
       <InputNumber 
       disabled={this.state.cashPayment}
       max ={this.AllowedRequestAmount()}
       />
    )
}
{`  of ${this.AllowedRequestAmount()} Birr`}
</Form.Item>

    <Form.Item  labelCol={{span : 4}}
    wrapperCol ={{span :12 }}
    
    label="Document">
<Uploader 
ref={this.UploaderChild}
{...this.props}
setFile={this.setFile}/

>
</Form.Item>


    <Form.Item {...formItemLayoutWithOutLabel}>


    </Form.Item> */}
    </Form>


</Modal>      
