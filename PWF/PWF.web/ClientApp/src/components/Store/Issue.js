import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import { StoreService } from '../../service/store.service';
import ReactTable from 'react-table';
import 'antd/dist/antd.min.css';
import { Tree, Input, Card, Row, Col, Table, Tabs, DatePicker, Modal, Button, Drawer, Form, Select, Spin } from 'antd';
import { Link } from 'react-router-dom';
import PostStoreIssueDocument from '../Document/StoreIssueDocument';
import Item from 'antd/lib/list/Item';
import TransactionDocumentItemForm from '../Item/TransactionDocumentItemForm';
import IssueItemsListForm from '../Item/IssueItemsListForm';
import DocumentReference from '../miniComponents/DocumentReference';
import StoreSelect from '../miniComponents/StoreSelect';
import SweetAlert from 'react-bootstrap-sweetalert';
import StoreIssueDocument from '../Document/StoreIssueDocument';
import CloseRequestForm from './CloseRequestForm';
import DocumentUploader from '../miniComponents/DocumentUploader';
import { WFHistoryViewer } from '../WFHistoryViewer';
import swal from 'sweetalert';
const TabPane = Tabs.TabPane;
const Uploader = Form.create()(DocumentUploader);


export default class Issue extends Component{
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            Request : {},
            Reorganized : [],
            Issued : [],
            IssuedItemsList : [],
            toBeIssuedItems : [],
            typeID : '',
            reference : '',
            primary : false,
            DocumentDate : '',
            storeID : '',
            showAlert : false,
            alertMessage : '',
            showModal : false,
            showCloseRequestModal : false,
            spinning : false,
            selectedFile : null,
            docDescription : '',
            
        };

        this.addItemsToCart = this.addItemsToCart.bind(this);
        this.setDocumentReference = this.setDocumentReference.bind(this);
        this.setStore = this.setStore.bind(this);
        this.setDate = this.setDate.bind(this);
        this.issueItem = this.issueItem.bind(this);
        this.showMessage = this.showMessage.bind(this);
        this.allItemsIssued = this.allItemsIssued.bind(this);

        this.child = React.createRef();
        this.storeChild = React.createRef();
        this.CloseRequestChild = React.createRef();

        this.getIssuedArray = this.getIssuedArray.bind(this);
        this.getUniqueCode = this.getUniqueCode.bind(this);
        this.reconstructData =this.reconstructData.bind(this);
        this.submitData = this.submitData.bind(this);
        this.checkQuantity = this.checkQuantity.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleCloseReqModal = this.toggleCloseReqModal.bind(this);
        this.toggleSpin = this.toggleSpin.bind(this);

        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
        this.CloseRequest = this.CloseRequest.bind(this);
        this.setFile = this.setFile.bind(this);
        this.setDocumentDescription = this.setDocumentDescription.bind(this);
        
    }
    

    componentWillMount(){
        this.setState({
            wfid : this.props.match.params.id
        });
    }



    getIssuedArray(issued){
        var items = []
        for (let i = 0; i < issued.length; i++) {
            for (let j = 0; j < issued[i].items.length; j++) {
                items.push({
                    code: issued[i].items[j].code,
                    name: issued[i].items[j].name,
                    quantity: issued[i].items[j].quantity,
                    docID: issued[i].AccountDocumentID,
                    date: issued[i].materializedOn,
                    reference : "SIV "+ issued[i].PaperRef
                });
            }

        }
        return items;
    }

  



    componentDidMount(){
        this.props.loader.stop();
        var self = this;
        var Service = new StoreService(this.props.session.token)
        Service.GetLastWorkItem(self.state.wfid).then(function(data){
            console.log(data);
            self.setState({
                Request : data.data,
                Reorganized : data.data.data.Approved
            })
            if(data.data.data.Issued != null){
                var items = self.getIssuedArray(data.data.data.Issued);
                console.log(data);
                    self.setState({
                        Issued: data.data.data.Issued,
                        IssuedItemsList : items,
                    });
            }
          
        }).then(function(){
            console.log(self.state);
            
            
        })
    }

    toggleModal = (e) => {
        this.setState({
            showModal: !this.state.showModal,
        });
    }

    toggleCloseReqModal= (e) => {
        this.setState({
            showCloseRequestModal : !this.state.showCloseRequestModal
        });
    }

    toggleSpin = () =>{
        this.setState({
            spinning : !this.state.spinning
        });
    }

    getUniqueCode(){
        var codes = [];
        this.state.Reorganized.map(function(item){
            codes.push(item.code);
        });
        return codes;
    }

    setFile(file){
        console.log(file);
        this.setState({
            selectedFile : file
        });
    }

    setDocumentDescription(description){
        this.setState({
            docDescription : description
        });
    }

    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }

    CloseRequest(){
        var Service = new StoreService("");
        this.StartSpin();
        var self = this;
        var description = this.state.docDescription;
        console.log(description);
        console.log(this.state);
        var description = '';

        this.CloseRequestChild.current.validateFields((err,values) => {
            if(err == null){
               description = values.Description;
               console.log(self.CloseRequestChild);
              if(self.state.selectedFile == null){
                  self.StopSpin();
                   swal("Error","File not selected","warning");
               }
               else if(self.state.selectedFile != null){
                   self.StartSpin();
                var file = self.state.selectedFile;
                file.Description = description;
                console.log(file);
                Service.CloseStoreRequest(self.state.wfid,file).then(function(response){
                    console.log(response);
                        self.StopSpin();
                        if(response.data == true){
                            swal("Success","You have successfully closed the request","success");
                            self.props.history.push("/store");
                        };
                        if(response.data == false){
                            swal("Error","Attempt to close store request failed","warning");
                        }
                }).catch(function(error){
                    self.StopSpin();
                    swal("Error",error.message,"warning");
                });
            }
            else{
                self.StopSpin();
            }
            }});
    };

    reconstructData(input){
        var self = this;
        var codes = this.getUniqueCode();
        var data = [];
        codes.forEach(function(value,index){
            var temp = {};
            var code = value;
            var name = self.state.Reorganized.filter(function(item){
                return item.code == code;
            })[0].name
            temp["name"] = name;
            temp["code"] = input["code"+code];
            temp["balance"] = input["balance"+code];
            temp["quantity"] = input["quantity"+code];
            temp["unitID"] = input["unitID"+code].value;
            temp["unitPrice"] = input["unitPrice"+code];
            temp["fixedExpense"] = input["fixedExpense"+code];
            temp["costCenterID"] = self.props.session.costCenterID;
            data.push(temp);
        });
        return data;
    }

    submitData() {
        var self = this;
        var i = 0;
        var codes = self.getUniqueCode();

        console.log("Stop spin");
        this.child.current.validateFields((err, values) => {
            console.log(err);
            console.log(values);
            codes.forEach(function(v,index){
                var temp = {};
                var code = v;
                var val = values["unitID"+code].value;
                if(val == ""){
                    i++;
                    self.child.current.setFields({                     
                        ["unitID"+code] :  {
                            value : { value : ""},
                            errors : [ { message : "Select Unit", field: ["unitID"+code]}]
                        }
                        
                    })
                    i++;
                }
            });
            if (err != null || i > 0) {
             
                self.setState({
                    spinning : false
                });
            }
            else {
                var storeID = '';
                this.storeChild.current.validateFieldsAndScroll((err2, values2) => {
                    console.log(err2);
                  



                    if (err2 != null) {
                        self.setState({
                            spinning : false
                        });
                    }
                    else {


                        var storeID = values2["StoreID"];
                        var items = this.reconstructData(values);
                        var cond = this.checkQuantity(items);
                        items = items.filter(function(i){
                            return i.quantity > 0
                        });
                        console.log(cond);
                        if(!cond){
                            var issueRequest = {
                                storeID: storeID,
                                shortDescription: "Store Issue Document",
                                items: items
                            };
                            self.setState({
                                spinning : true
                            });
                            var service = new StoreService(this.props.session.token);
                            service.PostStoreIssueDocument(issueRequest, this.state.wfid).then(function (response) {
                                self.setState({
                                    spinning : false
                                });
                                console.log(response);
                                if (response.data.status) {
                                    swal("Success","Item Issued Successfully","success");
                                    self.setState({
                                        toBeIssuedItems: [],

                                    })
                                    console.log(response.data.response);
                                    var result = response.data.response.map(function(item){
                                        item.AccountDocumentID = item.accountDocumentID;
                                        item.PaperRef = item.paperRef;
                                        item.DocumentDate = item.documentDate;
                                        return item;
                                    });
                                    var i = self.getIssuedArray(result);
                                        self.setState({
                                            Issued: result,
                                            IssuedItemsList : i,
                                        });
                                    self.child.current.resetFields();
                                    self.toggleModal();
                                }

                                if (!response.data.status) {
                                    swal("Error",response.data.error,"warning")
                                }
                            }).catch(function (error) {
                                console.log(error);
                                self.setState({
                                    spinning : false
                                });
                                swal("Error",error.message,"warning")
                            })
                        }
                 
                    }
                })
            }
        })
    };
    allItemsIssued() {
        Array.prototype.sum = function (prop) {
            var total = 0
            for (var i = 0, _len = this.length; i < _len; i++) {
                total += Number(this[i][prop])
            }
            return total
        }
        var totalAllowed = this.state.Reorganized.sum("quantity");
        var totalIssued = this.state.IssuedItemsList.sum("quantity");
        if (totalIssued < totalAllowed) {
            return false;
        }
        else {
            return true;
        }
    }
    checkQuantity(submittedItems) {
        console.log(submittedItems);
        var codes = this.getUniqueCode();

        var result = false;
        var self = this;
        codes.forEach(function (value, index) {
            var approvedQuantity = self.state.Reorganized.filter(function (item) {
                return item.code == value;
            })[0].quantity;
            var issuedItems = self.state.IssuedItemsList.filter(function (item) {
                return item.code == value;
            });

            Array.prototype.sum = function (prop) {
                var total = 0;
                for (var i = 0, _len = this.length; i < _len; i++) {
                    total += Number(this[i][prop]);
                }
                return total;
            }

            var totalIssued = issuedItems.sum("quantity");
            console.log(totalIssued);
            console.log(approvedQuantity);
            var submittedQuantity = submittedItems.filter(function (item) {
                return item.code == value;
            }).sum("quantity");
            var balance = submittedItems.filter(function (item) {
                return item.code == value;
            })[0].balance;
            var allowedQuantity = approvedQuantity - totalIssued;
            if(Number(submittedQuantity) > 0 && Number(balance)<= 0){
                result = true;
                var propname = "quantity" + value;
                console.log(propname);
                self.child.current.setFields({
                    [propname]: {
                        value: submittedQuantity,
                        errors: [new Error("No Stock in Store")]
                    }
                });
            }
            else if (Number(submittedQuantity) > Number(allowedQuantity)) {
                result = true;
                var propname = "quantity" + value;
                console.log(propname);
                self.child.current.setFields({
                    [propname]: {
                        value: submittedQuantity,
                        errors: [new Error("Maximum allowed quantity is " + allowedQuantity)]
                    }
                });
            }

           
        })
        console.log(result);
        if(result){
            self.setState({
                spinning : false
            });
        }
        return result;

    }

    setDocumentReference(type,value){
        if(type == "typeID"){
            this.setState({
                typeID : value
            });
        }
        if(type == "reference"){
            this.setState({
                reference : value
            });
        }
        if(type == "primary"){
            this.setState({
                primary : value
            });
        }
        
    }

    showMessage(message){
        this.setState({
            showAlert : true,
            alertMessage : message
        })
    }

    setDate(date,dateString){
        this.setState({
            DocumentDate : dateString
        });
    }

    setStore(type,value){
        if(type == "storeID"){
            this.setState({
                storeID : value
            });
        }
    }

    addItemsToCart(items){
        console.log(items);
        var newItem = [].push(items);
        this.setState({
            toBeIssuedItems : [...this.state.toBeIssuedItems, items]
        })
        console.log(this.state);
    }

    issueItem(){
        var self = this;
        self.props.loader.start();
        var doc = {
            documentDate : this.state.DocumentDate,
            paperRef : this.state.reference,
            shortDescription : "Store Issue Document",
            storeID: this.state.storeID,
            items : this.state.toBeIssuedItems,
            voucher : {
                typeID : this.state.typeID,
                reference: this.state.reference,
                primary: this.state.primary
            }

        };
        console.log(doc);
        var service = new StoreService(this.props.session.token);
        service.PostStoreIssueDocument(doc,this.state.wfid).then(function(response){
            console.log(response);
            self.props.loader.stop();
            if(response.data.status){
                self.showMessage("Document Successfully Posted");
                self.setState({
                    toBeIssuedItems : [],
                   
                })
                var items = self.getIssuedArray(response.data.response);
                setTimeout(() => {
                    self.setState({
                        Issued: items
                    });
                }, 1000);
            }
            if(response.data.state){
                self.showMessage(response.data.error);
            }
        }).catch(function(error){
            console.log(error);
            self.props.loader.stop();
            self.showMessage(error.message);
        });
        
    }

    render(){

        function DocumentTable (record){
            console.log(record)
            return (
                <Table
                    dataSource={record.items}
                    columns={iColumns}
                    pagination={false}
                    scrol={{ y: 600 }}
                />
            );
            
            }


        const columns = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Quantity',
            dataIndex: 'quantity',
        }];

        const iColumns = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Quantity',
            dataIndex: 'quantity',
        }];

        const OuterTable = [{
            title : "SIV",
            dataIndex : "PaperRef",
            
        },{
            title : "Date",
            render : d => new Date(d.DocumentDate).toLocaleString() //"DocumentDate"
        },{
                title: 'Document',
                dataIndex : '',
                
                key : 'x',
                render: (text,record) => 
                <Link to={  `/Document/1/${this.state.wfid}/${record.AccountDocumentID}`}>
                        <Button type="primary" shape="circle" icon="download" size={'large'}

                        />
                </Link>
                    
                
        }];
        
        const issuedColumns = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Balance',
            dataIndex: 'balance',
            },
             {
                title: 'Quantity',
                dataIndex: 'quantity',
            },
        {
            title : "Unit Price",
            dataIndex : "unitPrice"
        }];

 
        return(
            <div>
                <Modal
                    title="Issue Item"
                    visible={this.state.showModal}
                    onOk={() => this.submitData()}
                    onCancel={() => this.toggleModal()}
                    okText="Issue"
                    width={900}
                    footer={[
                        <Button key="back" onClick={() => this.toggleModal()}>Cancel</Button>,
                        <Button key="submit" type="primary" loading={this.state.spinning} onClick={() => this.submitData()}>
                          Issue
                        </Button>,
                      ]}
                >

                    <Row className="issueFormSection">
                        <Col span={6}>
                            Issued From Store
                                        </Col>
                        <Col span={12}>
                            <StoreSelect ref={this.storeChild} {...this.props} setStore={this.setStore} storeProp="storeID" />

                        </Col>
                    </Row>

                    <Row className="issueFormSection">
                        <IssueItemsListForm
                            ref={this.child}
                            showCostCenter={false}
                            cartItems={this.state.toBeIssuedItems}
                            Reorganized={this.state.Reorganized}
                            {...this.props} addToCart={this.addItemsToCart}
                            storeID={this.state.storeID}
                            issued={this.state.Issued}
                        />
                    </Row>



                </Modal>

                                        {/* Close Request Modal */}
                                        <Modal
                    title="Close Reqeust"
                   
                    visible={this.state.showCloseRequestModal}
                    
                    onOk={() => this.CloseRequest()}
                    onCancel={() => this.toggleCloseReqModal()}
                    footer = {
                        [<Button onClick={this.toggleCloseRequestModal}>Cancel</Button>,
                        <Button loading={this.state.spinning} onClick={this.CloseRequest}>Close Request</Button>]
                    }
                    okText="Close Request"
                    width={"40%"}
            >
            <Spin
            spinning={this.state.spinning}
            >
                <h5>Attach Signed Document</h5>
            <Uploader ref={this.CloseRequestChild} {...this.props} 
            setFile = {this.setFile}
            setDescription = {this.setDocumentDescription}
            />
            </Spin>

            </Modal>
            {/*  */}

                {/* <Modal
                    title="Close Reqeust"
                   
                    visible={this.state.showCloseRequestModal}
                    
                    onOk={() => this.toggleCloseReqModal()}
                    onCancel={() => this.toggleCloseReqModal()}
                    footer={[
                        <Button key="back" style={{display : 'none'}}>Return</Button>,
                        <Button key="submit" type="primary" style={{ display: 'none' }} >
                            Submit
            </Button>,
                    ]}
                    okText="Close Request"
                    width={"40%"}
                >

                    <CloseRequestForm 
                    
                    ref={
                        this.CloseRequestChild
                    } 
                    {...this.props} 
                    wfid={this.state.wfid}/>


                </Modal> */}
                <SweetAlert title="Store Issue!"
                    warning
                    confirmBtnText="Ok"
                    confirmBtnBsStyle="default"
                    cancelBtnBsStyle="default"
                    onConfirm={() => {
                        this.setState({
                            showAlert: false
                        });
                    }}
                    show={this.state.showAlert}>
                    {this.state.alertMessage}
                        </SweetAlert>
                <Row>

                         <Col  md={16}>
                         <Card title="Approved Items"
                                extra=  {
                                    !this.allItemsIssued() ?
                                        <Button  className="pull-right" onClick={() => this.toggleModal()}>Issue Item</Button>
                                        : <Button  className="pull-right" onClick={() => this.toggleCloseReqModal()}>Close Request</Button>}
                                >
                               
                                    <Table
                                        dataSource={this.state.Reorganized}
                                        columns={columns}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    />
                                </Card>

                            <Card title="Issued Items"> 

                                 
                                <Table
                                    columns={OuterTable}
                                    expandedRowRender={record => <Table
                                        dataSource={record.items}
                                        columns={iColumns}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    /> }
                                    dataSource={this.state.Issued}
                                />

                            </Card>                  

 
                         </Col>
                         <Col md={8}>
                         <Card
                         title="Store Request WF History"
                         >



                            <WFHistoryViewer 
                            {...this.props} 
                            type="store"
                            wfid={this.state.wfid}
                            />
                        </Card>
                         </Col>
                            </Row>
            </div>
        )
    }
}