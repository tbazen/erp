﻿import React, { Component } from 'react';
import { Drawer } from 'antd';
import { Label, Navbar, Nav, NavItem, Input, NavLink, Modal, ModalHeader, ModalBody, ModalFooter, CardText, TabPane, TabContent, Row, Col, Grid, FormGroup, ControlLabel, FormControl, Form, Tooltip, OverlayTrigger, Overlay, ButtonGroup } from 'reactstrap';
import { StoreService } from '../../service/store.service';
import { Button } from 'antd';
import SearchItem from '../Item/SearchItem';
import ReactTable from 'react-table';
import { Card, Table } from 'antd';
import { WFHistoryViewer } from '../WFHistoryViewer';
import swal from 'sweetalert';

export default class Reorganize extends Component {

    constructor(props) {
        super(props);

        this.state = {
            wfid: '',
            storeRequest: {},
            activeTab: '1',
            requestName: '',
            requestDescription: '',
            withCode: [],
            withDescription: [],
            
            selected: '',
            selectedCode: '',
            selectedName: '',
            withCodeSelected: '',
            withCodeSelectedCode: '',
            withCodeSelectedName: '',
            withCodeSelectedQuantity: '',
            Reorganized: [],
            reqeusted: [],
            quantity: '',
            showModal : false,
            selectedIndex : '',
            historyDrawer : false,
        };
        this.toggle = this.toggle.bind(this);
        this.selectItem = this.selectItem.bind(this);
        this.addWithCodeToList = this.addWithCodeToList.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.submitReorganized = this.submitReorganized.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        //this.Service = new StoreService(this.props.session.token);
        this.toggleHistory = this.toggleHistory.bind(this);
    }

    componentWillMount() {
        this.setState({
            wfid: this.props.match.params.id
        });

    }

    componentDidMount() {
        this.props.loader.stop();
        var self = this;
        var Service = new StoreService(this.props.session.token);
        Service.GetLastWorkItem(this.props.match.params.id).then(function (data) {
            self.setState({
                storeRequest: data.data,
                requestName: data.data.requestName,
                requestDescription: data.data.requestDescription,
                requested: data.data.data.Requested,
                withCode: data.data.data.Requested.filter(function (el) {
                    return el.code !== "";
                }),
                withoutCode: data.data.data.Requested.filter(function (el) {
                    return el.code === "";
                })
            })
            console.log(data);
        }).catch(function (error) {
            console.log(error);
        });
    }

    OpenModal(index,quantity){
        this.setState({
            showModal : true,
            selectedIndex : index,
            quantity : quantity
        });
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }


    addWithCodeToList = () => {
        var self = this;
        var contained = this.state.Reorganized.filter(function (el) {
            return el.code == self.state.withCodeSelectedCode
        });
        if (contained.length == 0) {
            var wcSelected = {
                code: this.state.withCodeSelectedCode,
                name: this.state.withCodeSelectedName,
                quantity: this.state.withCodeSelectedQuantity
            };
            this.setState({
                Reorganized: [...this.state.Reorganized, wcSelected]
            });
        }


    }


    addToCart(e) {
        e.preventDefault();
        var newItem = {};

        var setItem = {
            code: this.state.selectedCode,
            name: this.state.selectedName,
            quantity: this.state.quantity
        };

        var request = this.state.requested;
        request[this.state.selectedIndex] = setItem

        this.setState({
            requested: request,
            selected: '',
            selectedCode: '',
            selectedName: '',
            quantity: ''
        });
        console.log(newItem);
        this.toggleModal();
    }

    toggleModal = (e) => {
        this.setState({
            showModal: !this.state.showModal,
        });
    }

  
    submitReorganized() {
        const desc = this.state.requestDescription;
        var count = this.state.requested.filter(function(item){
            return (item.code == null) || (item.code == "");
        }).length;
        console.log(count);
        if(count > 0){
            swal("Error","There are items in the request that are not codified. Set code for all items.","warning")
        }
        else if(desc == undefined || desc == ""){
            swal("Error","Enter Description","warning")
        }
        else{
            this.props.loader.start();
            var Service = new StoreService(this.props.session.token);
            var model = {
                Reorganized: this.state.requested,
                requestDescription : this.state.requestDescription
            }
            var self = this;
            console.log(this.state.requestDescription);
            Service.ReorganizeRequest(this.state.storeRequest.workflowId, model).then(function (data) {
                swal("Success","Successfully forwarded store request","success").then(function(value){
                    self.props.loader.stop();
                    self.props.history.push('/store');
                }).catch(function (error) {
                    self.props.loader.stop();
                    swal("Error",error.message,"warning");
                });
            })
        }


    }

    cancelReorganized(){
        const desc = this.state.requestDescription;
        if(desc == undefined || desc == ""){
            swal("Error","Enter Description","warning")
        }
        else{
            this.props.loader.start();
            var Service = new StoreService(this.props.session.token);
            var self = this;
            Service.CancelReorganization(this.state.storeRequest.workflowId,this.state.requestDescription).then(function (data) {
                swal("Success","Successfully declined store request","success").then(function(value){
                    self.props.loader.stop();
                    self.props.history.push('/store');
                }).catch(function (error) {
                    self.props.loader.stop();
                    swal("Error",error.message,"warning");
                });
            })

        }

    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
    }

    selectItem = (selected, selectedCode, selectedName) => {
        this.setState({
            selected: selected,
            selectedCode: selectedCode,
            selectedName: selectedName
        });
    }





    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {

        var cellLink = (d) => {
            if (d.code == null || d.code == ""){

                return <Button sm outline color="primary" onClick={() => this.OpenModal(this.state.requested.indexOf(d),d.quantity)}> Set Code</Button>

            }
        }

        const withCodeColumns = [{
            title: "Code",

            dataIndex: "code"
        }, {
            title: "Name",
            dataIndex: "name"
        }, {
            title: "Quantity",
            dataIndex: "quantity"
        },{
            title : "Action",
            render : d => cellLink(d)
        }

        ];
        const withoutCodeColumns = [{
            Header: "Description",
            accessor: "name"
        }, {
            Header: "Quantity",
            accessor: "quantity"
        }

        ];

        return (
            <div className="animated fadeIn">
                <Row>
                    
                    <Col md={8}>
                      <Row>
                      <Card
                      title="Requested Items"
                      style={{width : '100%'}}
                      >
                            
                                <Table
                                    dataSource={this.state.requested}
                                    columns={withCodeColumns}
                                 
                                    showPagination={false}
                                 />
                              

                           
                        </Card>
                   
                   
                      </Row>
                      <Row>
                      <Card
                      style={{width : '100%'}}>
                         
                                <Form>
                                    <FormGroup>
                                        <Label for="descriptionText">Description</Label>
                                        <Input required type="textarea" name="requestDescription" id="descriptionText"
                                            valid={this.state.requestDescription}
                                            onChange={(e) => this.handleChange(e)}
                                        />
                                    </FormGroup>
                                    <ButtonGroup className="pull-right" >
                                        <Button
                                            disabled={this.state.requestDescription != '' ? false : true}
                                            onClick={(e) => this.submitReorganized()}
                                            className="pull-right"
                                            outline
                                            color="primary"
                                        >Forward</Button>
                                        <Button
                                            disabled=
                                            {this.state.requestDescription != '' ? false : true}
                                            onClick={(e) => this.cancelReorganized()}
                                            className="pull-right"
                                            outline
                                            color="danger"
                                        >Reject</Button>
                                    </ButtonGroup>

                                </Form>
                          
                        </Card>
                      </Row>
                    </Col>

                     <Col  md={4}>
                        <Card>
                         
                            <WFHistoryViewer 
                            {...this.props} 
                            type="store"
                            wfid={this.state.wfid}
                            />
                        
                        </Card>
                   

                     </Col>
                     </Row>
                    <Modal isOpen={this.state.showModal} toggle={this.toggleModal} className={this.props.className}>
                        <ModalHeader toggle={this.toggleModal}>Modal title</ModalHeader>
                        <ModalBody>
                            <SearchItem {...this.props} selectItem={this.selectItem}>

                                <form className="form form-inline" onSubmit={this.addToCart}>

                                    <FormGroup>
                                        <Label>Quantity :</Label>{' '}
                                        <Input value={this.state.quantity} name="quantity" type="text" bsSize="small" onChange={(e) => this.handleChange(e)} required />
                                    </FormGroup>{' '}
                                    <ButtonGroup>

                                        <Button
                                            htmlType="submit"
                                            className="pull-right"
                                            bsSize="small"
                                            bsStyle="primary"

                                            disabled={!((this.state.selectedCode != ''))}
                                        >
                                            Add To List
                                </Button>{' '}
                                    </ButtonGroup>

                                </form>
                            </SearchItem>          </ModalBody>
                        {/* <ModalFooter>
                            <Button color="primary" onClick={this.toggleModal}>Do Something</Button>{' '}
                            <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
                        </ModalFooter> */}
                    </Modal>
                   
          


      
                   


            </div>
        );
    }
}

