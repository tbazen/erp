import React , {Component} from 'react';
import Axios from 'axios';
import { StoreService } from '../../service/store.service';
import { Navbar, Row, Col, Grid, FormGroup,  ControlLabel, FormControl,  Form, Tooltip, OverlayTrigger, Overlay, ButtonGroup } from 'react-bootstrap';

import { CardHeader, CardBody, Label, Input } from 'reactstrap';
import {Card, Button, Table, Modal } from 'antd';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import SearchItem from '../Item/SearchItem';
import { Element } from 'react-scroll';
import swal from 'sweetalert';

export default class StoreRequest extends Component{
    constructor(props){
        super(props);

        this.Service = new StoreService(this.props.session.token);
        this.state = {
            sessions: {} ,
            searchTerm: '',
            searchResult: [] ,
            cartItems: [] ,
            selected: '',
            selectedCode: '',
            quantity: '',
            selectedName: '',
            description: '',
            addWithDescription: true,
            requestName: '',
            requestDescription: '',
            wfData : {},
            showModal: false,
            removableIndex : '',
            removableData : null,
        }

        this.handleChange = this.handleChange.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.isActive = this.isActive.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.addWithDesc = this.addWithDesc.bind(this);
        this.sendRequest = this.sendRequest.bind(this);
        this.selectItem = this.selectItem.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.removeItemWithCode = this.removeItemWithCode.bind(this);
    }




    componentWillMount(){
        const self = this;
        this.Service.SetRole().then(function (data) {
            console.log(data);
        }).catch(function(error){
            console.log(error);
        })
    }

    removeItem(){
        if(this.state.removableData != null){
            var index = this.state.removableIndex;
            var filteredItems = this.state.cartItems.filter(function(item,i){
                return i != index; 
            });
            console.log(filteredItems);
            this.setState({
                cartItems : filteredItems,
                removableIndex : '',
                removableData : null
            });
        }

    }

    removeItemWithCode(c){
        if(this.state.cartItems != null){
            var filteredItems = this.state.cartItems.filter(function(item,i){
                 return item != c
            });
            this.setState({
                cartItems : filteredItems
            });
        }
    }

    componentDidMount(){
        this.props.loader.stop();
        const { id } = this.props.match.params;
        if(id != null){
            var self = this;
            var Service = new StoreService(this.props.session.token);
            Service.GetLastWorkItem(id).then(function (data) {
                console.log(data);
                self.setState({
                    wfData : data.data,
                    cartItems : data.data.data.Requested
                });
                console.log(data);
            }).catch(function (error) {
                console.log(error);
            }).then(function(){
                console.log(self);
            })
        }
    }


    toggleModal = (e) => {
        this.setState({
            showModal: !this.state.showModal,
        });
    }

    selectItem = (selected, selectedCode, selectedName) => {
        this.setState({
            selected: selected,
            selectedCode: selectedCode,
            selectedName: selectedName
        });

    }

    isActive() {
        return this.state.searchResult.length > 0;
    }

    addToCart(e) {
        e.preventDefault();
        var newItem =  {};
        if (this.state.addWithDescription) {
            newItem = {
                code: this.state.selectedCode,
                name: this.state.selectedName,
                quantity: this.state.quantity
            };
        }
        else {
            newItem = {
                code: '',
                name: this.state.description,
                quantity: this.state.quantity
            };
        }

        this.setState({
            cartItems: [...this.state.cartItems, newItem],
            selected: '',
            selectedCode: '',
            selectedName: '',
            quantity: '',
            description: '',
            addWithDescription: true
        });
        console.log(newItem);
        this.toggleModal();
    }

    sendRequest(e){
        e.preventDefault();
        this.props.loader.start();
        var request = {
            requestDescription : this.state.requestDescription,
            requested : this.state.cartItems
        };
        var self = this;
        if(this.props.match.params.id == null){
            this.Service.FileRequest(request).then(function (data) {
                self.props.loader.stop();
                swal("Success","Successfully submitted store request","success").then(function(value){
                    self.props.history.push('/store');
                })
                
            }).catch(function (error) {
                self.props.loader.stop();
                swal("Error",error.message,"warning");
            });
        }
        else{
            this.Service.ResubmitStoreRequest(this.props.match.params.id,request).then(function(data){
                swal("Success","Successfully submitted store request","success").then(function(value){
                    self.props.loader.stop();
                    self.props.history.push('/store');
                })
            }).catch(function(error){
                self.props.loader.stop();
                swal("Error",error.message,"warning");
            })
        };

    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        if (target.name == "description") {
            this.setState({
                searchResult: [],
                selected: '',
                selectedCode: '',
                selectedName: '',
                searchTerm: ''
            });
        }
        if (target.name == "searchTerm") {
            this.setState({
                description: '',
                addWithDescription: true
            })
        }
    }

    onKeyPress = (event) => {

        const self = this;
        if (event.key == "Enter") {
            //self.props.start();
            this.Service.SearchItems(this.state.searchTerm).then(function (data) {
                console.log(data);
                self.setState({
                    searchResult: data.data,
                    selected: '',
                    selectedCode: '',
                    selectedName: ''
                });
            }).catch(function(error){
                console.log(error);
            }).then(() => {
                console.log(this.state);
                //this.props.stop();

            })
        };
    }

    addWithDesc = () => {
        this.setState({
            searchResult: [],
            selected: '',
            selectedCode: '',
            selectedName: '',
            searchTerm: '',
            addWithDescription: false
        });
    }

    render(){
        
        const { id } = this.props.match.params;

        const rowStyle = {
            "paddingTop": 15
        }

        const columns = [{
            Header: 'Code',
            accessor: 'code',

        }, {
            Header: 'Name',
            accessor: 'name'
        }];

        const cartCol = [
            {
                title : "Code",
                dataIndex : "code",
            },{
                title : "Name",
                dataIndex : "name",
            },{
                title : "Quantity",
                dataIndex : "quantity"
            },{
                title : "Action",
                render : (d) =>  <Button onClick={() => this.removeItemWithCode(d)}>Remove</Button>
            }

        ];


        const cartColumns = [{
            Header: 'Code',
            accessor: 'code',

        }, {
            Header: 'Name',
            accessor: 'name'
        },
        {
            Header: 'Quantity',
            accessor: 'quantity'
        }
        ];



        return(
            <div>

                <Grid className="store">
                    <Row style={rowStyle}>
                        <Modal 
                        title="Search Items"
                        visible={this.state.showModal}
                        onCancel={this.toggleModal}
   
                    width={"50%"}
                    footer = {[] }
                    >

                                <SearchItem {...this.props} selectItem={this.selectItem}>
                                    <form className="form form-inline" onSubmit={this.addToCart}>

                                        <FormGroup>
                                            <ControlLabel>Quantity :</ControlLabel>{' '}
                                            <FormControl value={this.state.quantity} name="quantity" type="text" bsSize="small" onChange={(e) => this.handleChange(e)} required />
                                        </FormGroup>{' '}
                                        <ButtonGroup>

                                            <Button
                                                htmlType="submit"
                                                className="pull-right"
                                                disabled={!((this.state.selectedCode != '') || (!this.state.addWithDescription && this.state.description.length > 0))}
                                            //     disabled={!this.validateForm()}
                                            >
                                                Add To Cart
          </Button>{' '}
                                            <Button
                                                className="pull-right"
                                                onClick={this.addWithDesc}
                                            >
                                                Add With Description
          </Button>{' '}  </ButtonGroup>

                                    </form>
                                    <FormGroup hidden={this.state.addWithDescription}>
                                        <ControlLabel>Description :</ControlLabel>
                                        <FormControl
                                            value={this.state.description}

                                            onChange={(e) => this.handleChange(e)} placeholder="Enter description of the item"
                                            name="description" bsSize="large" />
                                    </FormGroup>


                                </SearchItem>


          {/* </ModalBody> */}

                        </Modal>

                       
  
                              




                 

                        <Col xs={12} md={8} >
                                <Card
                                title="Cart Items"
                                extra = {
                                    <div>
                                    <Button  className="pull-right" onClick={ () => { this.toggleModal()}}>Add Item</Button>
 
                                    </div>
                                }
                                >

                                    <Table
                                     columns={cartCol}
                                     dataSource={this.state.cartItems} />

                                    <Form className="form-horizontal" onSubmit={(e) => this.sendRequest(e)}>

                                        
                                        <FormGroup row>
                                            <Col md="6">
                                                <Label htmlFor="text-input">Description</Label>
                                            </Col>
                                            <Col xs="12" md="12">
                                                <Input type="text" id="text-input" name="text-input" placeholder="Desrcripton of the text" 
                                                 name="requestDescription" value={this.state.requestDescription}
                                                    onChange={(e) => { this.handleChange(e); }}/>
                                              
                                            </Col>
                                        </FormGroup>
                                        <Button

                                            className="pull-right"

                                            htmlType="submit"
                                            disabled={!(!!this.state.requestDescription )}
                                        //     disabled={!this.validateForm()}
                                        >
                                            Submit Request
                            </Button>
                                    </Form>
                                 
                                </Card>
                    
                        </Col>
                    </Row>
                </Grid>

            </div>
        )
    }

}