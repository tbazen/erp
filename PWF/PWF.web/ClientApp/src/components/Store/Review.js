import React, { Component } from 'react';
import { StoreService } from '../../service/store.service';

import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { EditableFormRow , EditableCell} from '../miniComponents/EditableRow'
import { Row, Col, Form, ButtonGroup, FormGroup, Label, Input } from 'reactstrap';
import { Card } from 'antd';
import { Element } from 'react-scroll';
import { WFHistoryViewer } from '../WFHistoryViewer';
import swal from 'sweetalert';
import { Table, Popconfirm, Button } from 'antd';

export default class Review extends Component {

	constructor(props) {
		super(props);

		this.state = {
			wfid: '',
			Request: {},
			Reorganized: [],
			Quantity : {},
			description: ''
		};
		this.cols = [{
			title: "Code",
			dataIndex: "code"
		}, {
			title: "Name",
			dataIndex: "name"
		},
		{
			title : "Requested",
			render : (text,record) => {
			//	console.log(record);
			return( <p>{this.getQuantity(record.code)}</p>)
			}
		},
		{
			title: "Quantity",
			dataIndex: "quantity",
			editable: true
		}, {
			title: "Balance",
			dataIndex: "balance"
		}, {
			title: "Unit Price",
			dataIndex: "unitPrice"
		}, {
			title: 'Action',
			dataIndex: 'operation',
			render: (text, record) => (
			  this.state.Reorganized.length >= 1
				? (
				  <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.code)}>
					<Button href="javascript:;"><i className="fa fa-trash"></i></Button>
				  </Popconfirm>
				) : null
			),
		  }];
		this.handleChange = this.handleChange.bind(this);
		this.submitReview = this.submitReview.bind(this);
		this.getQuantity = this.getQuantity.bind(this);
	}




	componentWillMount() {
		this.setState({
			wfid: this.props.match.params.id
		});
	}

	getQuantity(code){
		return this.state.Quantity[code];
	}

	handleChange = (event) => {
		let target = event.target;
		this.setState({
			[target.name]: target.value
		});
	}

	handleDelete = (key) => {
		const dataSource = [...this.state.Reorganized];
		this.setState({ Reorganized: dataSource.filter(item => item.code !== key) });
	  }

	  handleSave = (row) => {
		const newData = [...this.state.Reorganized];
		const index = newData.findIndex(item => row.code === item.code);
		const item = newData[index];
		newData.splice(index, 1, {
		  ...item,
		  ...row,
		});
		this.setState({ Reorganized: newData });
	  }
	

	submitReview(type) {
		const desc = this.state.description;
		console.log(this.state.Reorganized);
		if(this.state.Reorganized.length == 0 && type == 1){
			swal("Error","There are no items in the list. You can not approve the request.","warning")
		}
		else if(desc == null || desc == ""){
			swal("Error","Enter Description","warning")
		}
		else{
			this.props.loader.start();
			var Service = new StoreService(this.props.session.token);
			var self = this;
			var status = ["rejected","approved"];
			console.log(this.state.Reorganized);
			Service.SubmitRequestReview(this.state.Request.workflowId, this.state.description, type,this.state.Reorganized).then(function (data) {
				self.props.loader.stop();
				swal("Success",`Successfully ${status[type]} store request`,"success").then(function(value){
					self.props.history.push('/store');
				})
				
			}).catch(function (error) {
				self.props.loader.stop();
				swal("Error",error.message,"warning");
			});
		}

	}

	componentDidMount() {
		var self = this;
		var Service = new StoreService(this.props.session.token);
		Service.GetLastWorkItem(this.props.match.params.id).then(function (data) {
			self.setState({
				Request: data.data,
				Reorganized: data.data.data.Reorganized
			})
			var Quant = [];
			data.data.data.Reorganized.map(function(item){
				Quant[item.code]=item.quantity 
				
			});
			self.setState({
				Quantity : Quant
			});
			console.log(data);
		}).catch(function (error) {
			console.log(error);
		}).then(function () {
			var reorg = self.state.Reorganized;
			self.setState({
				Reorganized: []
			});
			reorg.map(function (item, index) {
				var balance = Service.GetAccountBalance(item.code,1).then(function (result) {
					item.balance = result.data.totalQuantity;
					item.unitPrice = (Number(result.data.totalValue) / Number(result.data.totalQuantity)).toFixed(2);
					console.log(result.data);
				}).catch(function (error) {
					console.log(error);
				}).then(function () {
					self.setState({
						Reorganized: [...self.state.Reorganized, item]
					});

					
					console.log(self.state);
				})
			})
		});
	}

	render() {


		const components = {
			body: {
			  row: EditableFormRow,
			  cell: EditableCell,
			},
		  };
		  const columns = this.cols.map((col) => {
			if (!col.editable) {
			  return col;
			}
			return {
			  ...col,
			  onCell: record => ({
				record,
				editable: col.editable,
				dataIndex: col.dataIndex,
				title: col.title,
				handleSave: this.handleSave,
			  }),
			};
		  });



		return (
			<div class="animated fadeIn">
				<Row>
					<Col xs={12} md={8}>
						<Card
						title="Reorganized Items">


							<Table
									components={components}
									rowClassName={() => 'editable-row'}
									bordered
									dataSource={this.state.Reorganized}
									columns={columns}
									pagination={false}
									/>

{/* </Element> */}
								<Form>
									<FormGroup>
										<Label for="descriptionText">Description</Label>
										<Input type="textarea" name="description" id="descriptionText"
											valid={this.state.description}
											onChange={(e) => this.handleChange(e)}
										/>
									</FormGroup>
									<ButtonGroup className="pull-right">
										<Button
											onClick={() => this.submitReview(1)}
											className="pull-right"
											outline
											color="primary"
										>Approve</Button>
										<Button
											onClick={() => this.submitReview(0)}
											className="pull-right"
											outline
											color="danger"
										>Decline</Button>
									</ButtonGroup>

								</Form>
						</Card>
					</Col>
				
			        <Col xs={12} md={4}>
					<Card>
                          
                            <WFHistoryViewer 
                            {...this.props} 
                            type="store"
                            wfid={this.state.wfid}
                            />
                         
                        </Card>
					</Col>
				</Row>
			</div>
		)
	}


}