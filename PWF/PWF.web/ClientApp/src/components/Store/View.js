import React, { Component } from 'react';

import { StoreService } from '../../service/store.service';

import { Badge, ButtonGroup, Col, Pagination, PaginationItem, PaginationLink, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import ReactTable from 'react-table';
import 'react-table/react-table.css'; 
import SweetAlert from 'react-bootstrap-sweetalert';
import { Button, Card, Table } from 'antd';
import { SRW } from '../../service/WFStates';

export default class View extends Component{
    constructor(props){
        super(props);
      //  this.Service = new StoreService(this.props.session.token);
        this.state = {
            Workflows: [],
            show : false,
            showPrompt: false,
            forDeletion : '',
        };
        this.workItemIdentifier = this.workItemIdentifier.bind(this);
        this.toggle = this.toggle.bind(this);
        this.deleteRequest = this.deleteRequest.bind(this);
    }

    toggle(){
        if(this.state.show){
            this.setState({
                show: false
            });
        }
        else{
            this.setState({
                show: true
            });
        }
    }

    deleteRequest(description){
        const id = this.state.forDeletion;
        this.setState({
            showPrompt : false,
            forDeletion : ''
        });
        var self = this;
        var Service = new StoreService(this.props.session.token);
        Service.CancelStoreRequest(id,description).then(function(response){
            console.log(response);
            console.log(id);
            if(response.data.status == "Success"){
                var filtered  = self.state.Workflows.filter(function(el){
                    return el.id != id;
                })
                self.setState({
                    Workflows: filtered
                });
            }

        }).catch(function(error){
            console.log(error);
        })
    }

    

    componentDidMount() {
        var self = this;
        var Service = new StoreService(this.props.session.token);
        var d = [];
        Service.StoreRequestWorkflows().then(function (response) {
                d = response.data.filter(function(el){
                    return self.workItemIdentifier(el.currentState, self.props.session.role);
                    
            })
            
            console.log(response.data);
            console.log(self.state);
        }).then(function(){
            self.setState({
                Workflows: d
            });
            console.log(self);
        });
        
    }

    workItemIdentifier = (state, role) => {
        if (role == 1) {
            return [0,1,2,3,-1].indexOf(state) >= 0;
        }
        if (role == 2) {
            return (state == 1 || state == 3);
        }
        if (role == 3) {
            return (state == 2)
        };
        return false;
    }

    render() {
        const States = ["Filing", "Reogrganizing", "Reviewing", "Issuance"];
        const reorganizeLin = (d) => { return '/store/reorganize/' + d.id; }
        const issueLink = (d) => { return '/store/issue/' + d.id };
        const resubmitLink = (d) => { return '/store/resubmit/' + d.id };
        const ViewLin = (d) => { return '/store/review/' + d.id; }
        var cellLink = (d) => {
            console.log(d);
            if(this.props.session.role == 1){
                if(d.currentState == 0){
                    return (
                    <div>
                            <Link to={resubmitLink(d)}><Button sm outline color="primary"> Resubmit</Button></Link>
                            <Button sm outline color="danger" 
                            onClick={() => {
                                this.setState({
                                    show: true,
                                    forDeletion : d.id
                                });
                            }}>
                            Cancel Request
                            </Button>
                    </div>

                                       
                    )
                }
                else{
                   return( <Link to={`/wf/view/${SRW}/${d.id}`}><Button icon="eye" shape="circle"></Button></Link> )
                }
            }
            if (this.props.session.role == 2) {
                if(d.currentState == 1){
                    return (<Link to={reorganizeLin(d)}><Button sm outline color="primary"> Reorganize</Button> </Link>);
                }
                else {
                    return (<Link to={issueLink(d)}><Button sm outline color="primary"> Issue </Button></Link>);
                }
                
            }
            if (this.props.session.role == 3) {
                return (<Link to={ViewLin(d)}><Button sm outline color="primary"> Review</Button> </Link>);
                }
            }
        const columns = [{
            title: "Initiator",
            
            dataIndex: "action.username"
        }, {
            title : "Descrition",
            dataIndex: "description"
        }, {
            title : "Time",
            dataIndex: "action.timeStr"
            }, {

                title : 'Action',
                key : "aid",
                render: d => cellLink(d),
            }

        ];
        return(
            
            <div className="animated fadeIn">
               
                <Row>
                    <Col xs="12" lg="12">
                        <SweetAlert title="Alert!" 
                            warning
                            showCancel
                            confirmBtnText="Yes"
                            confirmBtnBsStyle="danger"
                            cancelBtnBsStyle="default"
                        onConfirm={() => { this.setState({
                            show : false,
                            showPrompt: true});}} 
                            onCancel={this.toggle} show={this.state.show}>
                            Are you sure you want to delete this request?
                        </SweetAlert>
                        <SweetAlert
                            input
                            
                            showCancel
                            cancelBtnBsStyle="default"
                            title="Delete Store Request"
                            placeHolder="Description ..."
                            show = {this.state.showPrompt}
                            onConfirm={(s) => {this.deleteRequest(s)}}
                            onCancel={() => this.setState({ showPrompt: false })}
                        >
                            Why do you want to delete this request?
                    </SweetAlert>
                        <Card
                        
                        title="Store Request"
                        extra={
                            this.props.session.role === 1 ? <Link to="/store/request">
                            <Button sm outline color="primary" className="pull-right">Add New Request</Button>
                        </Link> : ''
                        }
                        >
                           
                              
                             
                       
            
                                        <Table
                                            dataSource={this.state.Workflows}
                                            columns={columns}
                                           // defaultPageSize={10}
                                       />
                          
                        </Card>
                    </Col>
                </Row>
            </div>
        )
        
    }
}