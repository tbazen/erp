import React, { Component } from 'react';

import { PaymentService } from '../../service/payment.service';
import { Link } from 'react-router-dom';
import { baseUrl } from '../../service/urlConfig';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Drawer } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'; 
import '../../assets/css/berp.scss';
import BudgetTransactionViewer from '../Payment/BudgetTransactionViewer';
import DocumentUploader from '../miniComponents/DocumentUploader';
import { PaymentWFHistoryViewer } from '../Payment/PaymentWFHistoryViewer';
import DocumentsRenderer from '../Payment/DocumentsRenderer';
const { TabPane } = Tabs;
const { TextArea } = Input;
const ButtonGroup = Button.Group;

const Uploader = Form.create()(DocumentUploader);


class Finalize extends Component {
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            activeTab : "1",
            state : 0,
            Request : null,
            ImportedDocuments : [],
            NewDocuments : [],
            PaymentSource : [],
            DocumentTableData : [],
            AttachedDocs : [],
            Reference : {},
            BudgetDocument : null,
            Description : '',
            dropdownOpen: false,
            showFormModal : false,
            showAddDocumentModal : false,
            showUploaderModal : false,
            closeRequestModal : false,
            selectedDocumentID : '',
            selectedFormKey : '',
            DocumentTypes : [],
            DocumentAmount : {},
            docHTML: '',
            documentViewModal : false,
            serialType : '',
            selectedFile : null,
            docDescription : '',
            SignedDocument : {},
            historyDrawer : false,

        };
        this.handleChange = this.handleChange.bind(this);
        this.Service = new PaymentService(this.props.session.token);

        this.modalChild = React.createRef();
        this.uploaderChild = React.createRef();
        this.closeRequestChild = React.createRef();
        
        this.toggle = this.toggle.bind(this);
        this.handleOk = this.handleOk.bind(this);
        this.toggleDocumentBrowser = this.toggleDocumentBrowser.bind(this);

        this.prepareDocumentTable =this.prepareDocumentTable.bind(this);
        this.service = new PaymentService(this.props.session.token);
        this.toggleHistory = this.toggleHistory.bind(this);

    }

    componentWillMount() {
        this.props.loader.stop();
        this.setState({
            wfid: this.props.match.params.id
        });

    }

    setFile(file){
        console.log(file);
        this.setState({
            selectedFile : file
        });
    }

    toggleDocumentBrowser(){
        this.setState({
        showAddDocumentModal : !this.state.showAddDocumentModal
        });
    }

    toggleUploaderModal(){
        this.setState({
            showUploaderModal : !this.state.showUploaderModal
        })
    }

    setDocumentDescription(description){
        this.setState({
            docDescription : description
        });
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

   
    getAllDocuments(){
        if(this.state.NewDocuments == null && this.state.ImportedDocuments == null){
            return [];
        }
        else if(this.state.NewDocuments == null && this.state.ImportedDocuments != null){
            return this.state.ImportedDocuments;
        }
        else if(this.state.NewDocuments != null && this.state.ImportedDocuments == null){
            return this.state.NewDocuments;
        }
        else
            return [ ...this.state.NewDocuments, ...this.state.ImportedDocuments ];
    }

 
    componentDidMount() {
        var self = this;
        var service = new PaymentService(this.props.session.token);

        service.GetAllDocumentTypes().then(function(response){
            console.log(response);
            if(response.data.status){
                self.setState({
                    DocumentTypes : response.data.response
                });
            }
            if(!response.data.status){
                swal("",response.error,"");
            }
        }).catch(function(error){
            swal("",error.message,"");
        });

        service.GetLastPaymentWorkitem(this.state.wfid).then(function (response) {
            console.log(response);
            self.setState({
                state : response.data.currentState,
                Request : response.data.data.request,
                ImportedDocuments : response.data.data.importedDocuments,
                NewDocuments : response.data.data.newDocuments,
                PaymentSource : response.data.data.sources,
                Reference : response.data.data.reference,
                BudgetDocument : response.data.data.budgetDocument,
                AttachedDocs : response.data.data.attachedDocs,
                SignedDocument : response.data.data.signedDocument,

            });  
            console.log(self);
        }).then(function(){
            console.log(self.state);
            var docs = self.getAllDocuments();
            if(docs != null && docs.length > 0){
                self.prepareDocumentTable(docs);
            }
            if([3,4,-1].indexOf(self.state.state) != -1){
                service.GetSerialType(self.state.Reference.typeID).then(function(response){
                    if(response.data.status){
                        self.setState({
                            serialType : response.data.response.name
                        })
                    }
                });
            }
            
            
        })
        

    }

   


    onChange = (activeKey) => {
        this.setState({ activeTab : activeKey });
      }

  

  

  

    prepareDocumentTable(Documents){
        var service = new PaymentService(this.props.session.token);
        var self = this;
        var docAmounts = {};
        var tData = [];
        Documents.map(function(item){
            var id = item.accountDocumentID;
            service.GetTransactionAmount(self.state.wfid,id).then(function(response){
                console.log(response);
                if(response.data.status == true){
                     docAmounts[id] = response.data.response;
                }
                if(!response.data.status){
                    swal("",response.data.error,"");
                }
            }).catch(function(error){
                swal("",error.message,"");
            }).then(function(){
                console.log(self);
                var type = self.state.DocumentTypes.filter(function(i){
                    return i.id == item.documentTypeID;
                })[0].name;
                var s = {
                    'id' : item.accountDocumentID,
                    'docType' : type,
                    'docDate' : new Date(item.documentDate).toLocaleString(),
                    'amount' : docAmounts[item.accountDocumentID],
                    'reference' : item.paperRef
                };
                tData.push(s);
            }).then(function(){
                self.setState({
                    DocumentAmount : docAmounts
                });
                console.log(tData);
                self.setState({
                    DocumentTableData : tData
                });
            })

        })
    }




    handleOk(){
        console.log(this);
        this.modalChild.current.postDocument();
    }

  

    toggleDocumentViewModal(){
        this.setState({
            documentViewModal : !this.state.documentViewModal
        });
    }

    GetDocumentHTML(docID){

        var self = this;
        this.service.GetAccountDocumentHTML(docID).then(function(response){
            console.log(response);
            if(response.data.status == true){
                self.setState({
                    docHTML : response.data.response
                });
            }
            if(response.data.status == false){
                swal("",response.data.error);
            }
        }).catch(function(error){
            swal("",error.message,"");
        }).then(function(){
            self.toggleDocumentViewModal();
            console.log(self.state);
        })
    }

  
    toggle() {
        var self = this;
        this.setState({
          dropdownOpen: !self.state.dropdownOpen
        });
      }

    handleChange = (event) => {
        console.log(event);
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        console.log(this.state);
    }

    handleMenuClick(e) {
        message.info('Click on menu item.');
        console.log('click', e);
      }

   


    



    render() {

        const getRequestType = (type) => {
            if(type == 0){
                return "General"
            }
            else if(type == 1){
                return "Mission Advance";
            }
            else if(type == 2){
                return "Purchase Payment";
            }
            else{
                return "";
            }
        }


        const DocumentColumns = [
            {
                title : "Document Type",
                key: "id",
                dataIndex : "docType"
            },
            {
                title : "Date",
                dataIndex : "docDate"
            },
            {
                title : "Amount",
                dataIndex : "amount",
            }, {
                title : "Reference",
                dataIndex : "reference"
            },
            {
                title: "Action",
                id : 'id',
                render : d => (
                    <div>
<Button onClick={() => this.GetDocumentHTML(d.id)} icon="eye"></Button>
                    </div>
                
                
                )
            }
        ]

        const state = this.state.state;
        const paymentSourceCol = [
            {
                title : "Source",
                key : "assetAccountID",
                dataIndex : "name"
            }, {
                title : "Amount",
                dataIndex : "amount"
            }
        ]

        const itemColumns = [
            {
                title : "Description",
                dataIndex : "description"
            },
            {
                title : "Unit",
                dataIndex : "unitID"
            },
            {
                title : "Unit Price",
                dataIndex : "unitPrice",
            },
            {
                title : "Quantity",
                dataIndex : "quantity",
            },{
                title : "Amount",
                dataIndex : "amount"
            }
        ];

        const { getFieldDecorator, getFieldValue } = this.props.form;

        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 16}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 16, offset: 4
            }
            
        };

        const html = "<h2>Transfer from Cashier to Cashier</h2><h2>Date: Jan 20,19 12:03 PM</h2><h2>Ref: 2</h2><h2>Affected accounts</h2><h3>No accounting entry for this document</h3><h2>More Information</h2><b>Amount:<b/>1,200.00<h2>References</h2><br/><strong>Cash Payment Voucher: </strong>00000002";
        const paidTo = ["","Employee","Customer","Other"];

        const getAddDocumentButton = ()  => {
            
            return <div></div>
               

        }

        return(
        
        <div>
                                     {/* Document Viewer Modal */}
             <Modal 
                visible={this.state.documentViewModal}
                title = "View Document"
                onOk={() => this.toggleDocumentViewModal()}
                onCancel = {() => this.toggleDocumentViewModal()}  
                
                width = '70%'
                
                >
               <div className="documentViewer" dangerouslySetInnerHTML={{__html : this.state.docHTML}}>

               </div>
                </Modal>
            {/*  */}
            <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <PaymentWFHistoryViewer 
        {...this.props} 
        
        wfid={this.state.wfid} />
        </Drawer>

    <Row>

            <Col span={24} offset={0}>
            <Tabs  
            onChange = {this.onChange}
            activeKey = {this.state.activeTab}
            type="card">
    <TabPane tab="Request" key="1">

               
                <Card 
                title="Payment Request"
                style = {{marginTop : -20}}
                extra = {  <Button onClick={() => this.toggleHistory()}>Show History</Button>}

                 >
                 
                   {
                     this.state.Request != null ?
                        <div>
                            {
                                state == 3 || state  == 4 || state == -1 ?
                                <div>
                                     <Row>
                                            <Col offset={12} span={4} className="prop">Reference {' '}</Col>
                    <Col span={6}  className="value"> {
                    this.state.serialType 
                    }{' '}{this.state.Reference.reference}</Col>
                    </Row>
                                </div>
                                :null


                            }
                                           
                <Row>
                
                    <Col span={4} className="prop">Type</Col>
                    <Col span={18} className="value">{getRequestType(this.state.Request.type)}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Items</Col>
                    <Col span={18} className="value"> <Table
                 columns={itemColumns}
                 pagination={false}
                  dataSource={this.state.Request.items}

               /></Col>
                </Row> 
                {
                    this.state.Request.type == 1 ?  
                    <div>
                        <Row>
                    <Col span={4} className="prop">Date
                    </Col>
                    <Col  span={18} className="value">{new Date(this.state.Request.dateFrom).toLocaleDateString() } - {new Date(this.state.Request.dateTo).toLocaleDateString()}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Amount Per Day </Col>
                    <Col span={18} className="value">{this.state.Request.amountPerDay}</Col>
                </Row>
                    </div>
                    
                    : null 
                }
                {
                     state == 2 || state == 3 || state == 4 || state == -1
                     
                     ?
                     <div>
                     <Row>
                     <Col span={4} className="prop">Payment Source </Col>
                         <Col span={18} className="value">
                         <Table 
                             columns = {paymentSourceCol}
                             dataSource = {this.state.PaymentSource}
                             pagination ={false}
                         />
                             </Col>
                     </Row>
                     <Row>
                     <Col span={4} className="prop">Budget Transaction Document </Col>
                         <Col span={18} className="value">
                        <BudgetTransactionViewer 
                        budgetDocs={this.state.BudgetDocument}
                        {...this.props}
                        />
                             </Col>
                     </Row>
                     </div>
                     
                     :null
                }
               

                      <Row>
                      <Col span={4} className="prop">Document </Col>
                    <Col span={18} className="value">
                    <a
                     onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.files.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                              [response.data], 
                              {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                        
                       
                     }}
                     
                     >

                    {this.state.Request.files.name}</a>
                    </Col>
                </Row>
                    {
                        this.state.SignedDocument != null  ?
                        <Row>
                        <Col span={4} className="prop">Signed Document </Col>
                    <Col span={18} className="value">
                    <a
                        onClick={()=> {
                        Axios(baseUrl +`/Payment/DownloadFile/${this.state.SignedDocument.id}`, {
                            method: 'GET',
                            responseType: 'blob' //Force to receive data in a Blob Format
                        })
                        .then(response => {
                        //Create a Blob from the PDF Stream
                            const file = new Blob(
                                [response.data], 
                                {type: 'application/pdf'});
                        //Build a URL from the file
                            const fileURL = URL.createObjectURL(file);
                        //Open the URL on new Window
                            window.open(fileURL);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                        
                        
                        }}
                        
                        >

                    {this.state.SignedDocument.name}</a>
                    </Col>
                    </Row>

                        : null
                    }
                


                <Row>
                    <Col span={4} className="prop">Paid To </Col>
                    <Col span={18} className="value">{
                        this.state.Request.paidTo.name
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop"></Col>
                    <Col span={18} className="value">{
                        paidTo[this.state.Request.paidTo.type]
                        
                        }</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Note </Col>
                    <Col span={18} className="value">{
                        this.state.Request.note
                        
                        }</Col>
                </Row>
                {
                    this.state.AttachedDocs != null && this.state.AttachedDocs.length > 0 ?
                    <div>
                                            <Row>
                    <Col span={4} className="prop">Signed Documents</Col>
                    <Col span={18} className="value">
                    <DocumentsRenderer 
                    {...this.props} docs={this.state.AttachedDocs} 
                    remove={this.RemoveDocument}
                    showRemove={false}
                    />
                    </Col>
                </Row>

                    </div>

                    : null
                }

                <Row>            
                </Row>
                        </div>

                     : null
                 }
          
               

                </Card>
          
                </TabPane>
    <TabPane tab="Document" key="2">
    <Card 
                title="Payment Documents"
                style = {{marginTop : -20}}
               
                 >
                 {
                     state == 3 | state == 4 || state == -1
                     ?
                     <div>
  <Table
                 columns={DocumentColumns}
                  dataSource={this.state.DocumentTableData}
                  pagination = {false}
               />


                     </div>

                     : null
                 }
               
                </Card>
      
    </TabPane>

  </Tabs>
            </Col>
        
        
        </Row>

      
        </div>
         
            )
    }

    
}

//export default Finalize;
export default Form.create() (Finalize);