import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { StoreService} from '../../service/store.service';
import { baseUrl } from '../../service/urlConfig';
import Axios from 'axios';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Select, InputNumber, Drawer } from 'antd';
import swal from 'sweetalert';
import DocumentUploader from '../miniComponents/DocumentUploader';
import CostCenterSearch from '../miniComponents/CostCenterSearch';
import EmployeeSelect from '../miniComponents/EmployeeSelect';
import { Link } from 'react-router-dom';
import { WFHistoryViewer } from '../WFHistoryViewer';
const { Option } = Select;
var Uploader = (DocumentUploader);
const  ButtonGroup  = Button.Group;
const { TextArea } = Input;

const serviceRows = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    
    ];

    if(d.approvedItems != null && d.approvedItems.length > 0){
        const data = d.approvedItems;
    
        return (
        <Table
            columns={columns}
            dataSource={data}
            pagination={false}
        />
        );
    }
    else{
        return <div className="value">Delivery On Process</div>;
    }

};


const itemRows = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    
    ];

    if(d.deliveredItems != null && d.deliveredItems.length > 0){
        const data = d.deliveredItems;
    
        return (
        <Table
            columns={columns}
            dataSource={data}
            pagination={false}
        />
        );
    }
    else{
        return <div className="value">Delivery On Process</div>;
    }

};



export default class PurchaseOrderPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Order : null,
            PaymentWorkflows : [],
            TotalAmount : '',
            PaymentModal : false,
            selectedFile : null,
            cashPayment : false,
            DeliveryModal : false,
            ServiceDeliveryModal : false,
            ItemDeliveryWorkflows : [],
            ServiceDeliveryWorkflows : [],
            IDNameDict : null,
            RemainingItemOrders : [],
            RemainingServiceOrders : [],
            CloseOrderModal : false,
            historyDrawer : false,
            spinning : false,
        }
        this.Service = new PurchaseService();
        this.StoreService = new StoreService("");
        this.getOrderStatus = this.getOrderStatus.bind(this);

        this.setFile= this.setFile.bind(this);

        this.getItemKeys = this.getItemKeys.bind(this);

        this.OrderContainsGoods = this.OrderContainsGoods.bind(this);
        this.OrderContainsService = this.OrderContainsService.bind(this);

        this.GetEmployeeName = this.GetEmployeeName.bind(this);

        this.toggleHistory = this.toggleHistory.bind(this);


        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
    }

    componentWillMount(){
         var self = this;
        // const wfid = '';
        // if(this.props.wfid != null)
        // {
        //     wfid = this.props.wfid;
        // }
        // else{
        //     wfid = this.props.match.params.id;}
        this.setState({
            wfid : this.props.wfid
        });



        console.log(this);
        this.Service.GetLastPurchaseOrderWorkitem(this.props.wfid).then(function(response){
            console.log(response);
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
            if(response.data.status == true){
                console.log(response.data.response);
                const itemDeliveryWFs = response.data.response.data.itemDeliveryWorkflows;
                const serviceDeliveryWFs = response.data.response.data.serviceDeliveryWorkflows;
                self.setState({
                    Order : response.data.response,
                    TotalAmount : response.data.response.data.items.sum("amount"),
                    ItemDeliveryWorkflows : itemDeliveryWFs != null && itemDeliveryWFs.length > 0 ? itemDeliveryWFs : [],
                    ServiceDeliveryWorkflows : serviceDeliveryWFs != null && serviceDeliveryWFs.length > 0 ?  serviceDeliveryWFs : [],
                });
                var pwf = response.data.response.data.paymentWorkflows;
                if(pwf != null && pwf.length > 0){
                    self.setState({
                        PaymentWorkflows: pwf
                    });
                }

            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            console.log(self.state);
            if(self.state.Order != null && self.state.Order.currentState != 1){
                self.StoreService.GetRemainingItemOrders(self.props.wfid).then(function(response){
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        self.setState({
                            RemainingItemOrders : response.data.response
                        })
                    }
                }).catch(function(error){
                    swal("Error",error.message,"warning");
                });
                
                self.StoreService.GetRemainingServiceOrders(self.props.wfid).then(function(response){
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        self.setState({
                            RemainingServiceOrders : response.data.response
                        })
                    }
                }).catch(function(error){
                    swal("Error",error.message,"warning");
                }).then(function(){
                    console.log(self.state);
                });
            }
            if(self.state.ServiceDeliveryWorkflows.length > 0){
                var dict = {};
                self.state.ServiceDeliveryWorkflows.map(function(item){
                    self.StoreService.GetNameForUID(item.approverID).then(function(response){
                        if(response.data.status == true){
                            dict[item.approverID] = response.data.response
                        }
                        
                    });
                })
                self.setState({
                    IDNameDict : dict
                })
            }
        }).then(function(){
            console.log(self.state);
        });
    }

    

    
    setFile(doc){
        this.setState({
            selectedFile : doc
        });
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }

    componentDidMount(){

    }



 

    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }








    getItemKeys(){

        var arr = [];
        if(this.state.Order == null || this.state.Order.data.items.length == 0){
            return [];
        }
        else{
            var i = this.state.Order.data.items.length;
            for (let index = 1; index <= i; index++) {
                if(this.state.Order.data.items[index-1].type == 1){
                    arr.push(index);
                }
               
             }
             return arr;
        }

    }


    GetEmployeeName (d)  {
        
        console.log(this.state.IDNameDict);
        if(this.state.IDNameDict == null || Object.keys(this.state.IDNameDict).length == 0){
            return "";
        }
        else{
            return this.state.IDNameDict[d.approverID];
        }

    }




 

    OrderContainsGoods(){
        if(this.state.Order != null){
            var result = this.state.Order.data.items.filter(function(i){
                return i.type == 1;
            });
            return result.length > 0;
        }
        else
            return false;
    }

    OrderContainsService(){
        if(this.state.Order != null){
            var result = this.state.Order.data.items.filter(function(i){
                return i.type == 2;
            });
            return result.length > 0;
        }
        else
            return false;
    }







    getOrderStatus(){
        var state = this.state.Order.currentState;
        if(state == 0){
            return "Rejected";
        }
        else if(state == 1){
            return "Under Review";
        }
        else if(state == 2){
            return "Approved";
        }
        else if(state == -1){
            return "Closed";
        }
        else if(state == -2){
            return "Terminated";
        }

    }

    render(){

      
        const getPaymentTypes = (d) => {
            if(d.type == 1){
                return "Supplier Advance Payment"
            }
            if(d.type == 2){
                return "Supplier Credit Settlement"
            }
            if(d.type == 3){
                return "Cash Payment"
            }
        }


        const getPaymentStatus = (d) => {
            var state = d.currentState;
            if(state == 0){
                return "Rejected"
            }
            if(state == 1){
                return "Check"
            }
            if(state == 2){
                return "Review"
            }
            if(state == 3){
                return "Payment"
            }
            if(state == 4){
                return "Finalization"
            }
            if(state == -1){
                return "Closed"
            }
            if(state == -2){
                return "Cancelled"
            }

            
        }

        const getItemDeliveryStatus = (d) => {
            var state = d.currentState;
            if(state == 0){
                return "Filing"
            }
            if(state == 1){
                return "Check"
            }
            if(state == 2){
                return "Review"
            }
            if(state == -1){
                return "Closed"
            }
            if(state == -2){
                return "Cancelled"
            }

        }

        
        const getServiceDeliveryStatus = (d) => {
            var state = d.currentState;
            if(state == 0){
                return "Filing"
            }
            if(state == 1){
                return "Check"
            }
            if(state == 2){
                return "Review"
            }
            if(state == -1){
                return "Closed"
            }
            if(state == -2){
                return "Cancelled"
            }

        }

        const renderDocument = (d) => {
            console.log(d);
            if(d.document == null){
                return "";
            }
            return (
                <a
                onClick={()=> {
                    console.log(d);
                   Axios(baseUrl +`/Payment/DownloadFile/${d.document.id}`, {
                       method: 'GET',
                       responseType: 'blob' //Force to receive data in a Blob Format
                   })
                   .then(response => {
                   //Create a Blob from the PDF Stream
                       const file = new Blob(
                         [response.data],
                         {type: 'application/pdf'});
                   //Build a URL from the file
                       const fileURL = URL.createObjectURL(file);
                   //Open the URL on new Window
                       window.open(fileURL);
                   })
                   .catch(error => {
                       console.log(error);
                   });


                }}

                >

               {d.document.name}</a>
            )
        }

        const pwfColumns = [{
            title : "Type",
            render : d => getPaymentTypes(d)
        }, {
            title : "Requested Amount",
            dataIndex : "requestedAmount"
        }, {
            title : "Paid Amount",
            dataIndex : "paidAmount",
        },
        {
            title : "Status",
            render : d => getPaymentStatus(d)
        }
        ,{
            title : "Document",
            render : d => renderDocument(d)
        },{
            title : "Note",
            dataIndex : "note",
            width : "30%"
        }
    ]

    const itemDeiveryColumns = [
        {
            title : "Note",
            dataIndex : "note"
        },
        {
            title : "Document",
            render : d => renderDocument(d)
        },
        {
            title : "Current State",
            render : d => getItemDeliveryStatus(d)
        }
    ]

    const serviceDeliveryColumns = [
        {
            title : "Approver",
            render : d => this.GetEmployeeName(d)
        },{
            title : "Note",
            dataIndex : "note"
        },
        {
            title : "Document",
            render : d => renderDocument(d)
        },
        {
            title : "Current State",
            render : d => getServiceDeliveryStatus(d)
        }
    ]


        const itemColumn = [
            { title: 'Name', dataIndex: 'description', key: 'code' },
            { title: 'Quantity', dataIndex: 'quantity'},
            { title: 'Unit Price', dataIndex: 'unitPrice' },
            { title: 'Amount', dataIndex : 'amount' },
            
            ];

            const getActionButtons = (state) => {
                return  <Button onClick={() => this.toggleHistory()}>Show History</Button>
            }

        return(
            <div>
            <Drawer
                    title="Payment History"
                    placement="right"
                    closable={true}
                    onClose={this.toggleHistory}
                    visible={this.state.historyDrawer}
                    width={400}
                    >
                    <WFHistoryViewer 
                    {...this.props} 
                    type="order"
                    wfid={this.state.wfid} />
                    </Drawer>
               {
                   this.state.Order != null ?
                 
                 
                   <Card
                   bordered={this.props.title != ""}
                   title={this.props.title}>


                    <div>
                        <Row>
                            <div className="pull-right">
                    {getActionButtons(this.state.Order.currentState)}
                            </div>
                        </Row>
                         <Row>
                        <Col span={4} className="prop">Supplier</Col>
                        <Col span={10} className="value">{this.state.Order.data.supplier.name}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Items</Col>
    
                    <Col span={16} className="value">
                    <Table columns={itemColumn}
                    pagination={false}
                    dataSource={this.state.Order.data.items}
                    footer ={ () =>
                        <Row>
                            <Col span={6}>Total</Col>
                            <Col span={18} style={{textAlign : "right"}}>{this.state.TotalAmount}</Col>
                        </Row>
                    }
                    />
                    </Col>
                </Row>
                <Row>
                        <Col span={4} className="prop">Document </Col>
                        <Col span={18} className="value">
                        <a
                         onClick={()=> {
                            Axios(baseUrl +`/Payment/DownloadFile/${this.state.Order.data.document.id}`, {
                                method: 'GET',
                                responseType: 'blob' //Force to receive data in a Blob Format
                            })
                            .then(response => {
                            //Create a Blob from the PDF Stream
                                const file = new Blob(
                                  [response.data],
                                  {type: 'application/pdf'});
                            //Build a URL from the file
                                const fileURL = URL.createObjectURL(file);
                            //Open the URL on new Window
                                window.open(fileURL);
                            })
                            .catch(error => {
                                console.log(error);
                            });
    
    
                         }}
    
                         >
    
                        {this.state.Order.data.document.name}</a>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4} className="prop">Note</Col>
                        <Col span={18} className="value">{this.state.Order.data.note}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Status</Col>
                        <Col span={18} className="value">{this.getOrderStatus()}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Workitem Description</Col>
                        <Col span={18} className="value">{this.state.Order.description}</Col>
                    </Row>
                    {
                        this.state.Order.currentState != 1 && this.state.Order.currentState != 0 && this.state.Order.currentState != -3 ?
                                <div>
                            
                                  <hr/>   
                    <Card
                    bordered={false}
                    title="Payment Requests">
                      {
                            this.state.PaymentWorkflows.length > 0 ?
                            <div>
 <Table
                            dataSource={this.state.PaymentWorkflows.filter(function(item){
                                return item.currentState != -3;
                            })}
                            columns ={pwfColumns} pagination={false}
                            /> 
                            </div> : "No Payment Requested"
                           
                        }
                    </Card>

                        <hr/>
                        
                        {
                            this.OrderContainsGoods() ?
                            <div>
                               
    
                                <Card
                                title="Item Deliveries"
                                bordered={false}>
                                    {
                                    this.state.ItemDeliveryWorkflows != null && this.state.ItemDeliveryWorkflows.length > 0 ?
                                    <Table
                                    dataSource= {this.state.ItemDeliveryWorkflows.filter(function(item){
                                        return item.currentState != -2;
                                    })}
                                    columns={itemDeiveryColumns}
                                    expandedRowRender={itemRows} 
                                    pagination={false}/>

                                : "No Item Delivery Requests Made"
                                }
                                </Card>
                                <hr />
                            </div>
                             : null
                        }
                        {
                            this.OrderContainsService()  ?
                            <div>
                                <Card
                                title="Service Deliveries"
                                bordered={false}>
                                  {
                                   this.state.ServiceDeliveryWorkflows != null && this.state.ServiceDeliveryWorkflows.length > 0 ?
                                   
                                   <div>
                                                <hr/>
                                                <Table
                                    dataSource= {this.state.ServiceDeliveryWorkflows.filter(function(item){
                                        return item.currentState != -3;
                                    })}
                                    columns={serviceDeliveryColumns}
                                    expandedRowRender={serviceRows} pagination={false}/>
                                   </div>
                                  

                                : "No Service Delivery Requests Made"
                                }
                                </Card>
                              <hr/>
                                </div>
                             : null
                        }
                                    </div>
                        : null
                    }
                  
                    </div>
                 
               </Card>
                 : null
               }
   
         
         
            </div>
        )
    }

}
