import React, { Component } from 'react';
import { PurchaseService } from '../../service/purchase.service';
import { Card, Row, Col, Button, Form, Table, Input, InputNumber, Select, Modal, Icon, Tabs, Drawer } from 'antd';
import swal from 'sweetalert';
import Axios from 'axios';
import { baseUrl } from '../../service/urlConfig';
import DocumentUploader from '../miniComponents/DocumentUploader';
import PurchaseOrderWF from './PurchaseOrderWF';
import '../../assets/css/order.scss';
import { WFHistoryViewer } from '../WFHistoryViewer';

const { TabPane } = Tabs;


const expandedRowRender = (d) => {
    const columns = [
    { title: 'Name', dataIndex: 'description', key: 'code' },
    { title: 'Quantity', dataIndex: 'quantity'},
    { title: 'Unit Price', dataIndex: 'unitPrice' },
    { title: 'Amount', dataIndex : 'amount' },
    
    ];

    const data = d.items;
    
    return (
    <Table
        columns={columns}
        dataSource={data}
        pagination={false}
    />
    );
};



class Finalize extends Component {
    constructor(props){
        super(props);
        this.state = {
            wfid : '',
            Request : null,
            Worflow : null,
            activeTab : "1",
            Quotations : [],
            Orders : [],
            OrderIDs : [],
            codeNameDict : {},
            supplierNameDict : {},
            Description : '',
            ItemLength : [],
            QuotationModal: false,
            selectedFile : null,
            selectedQuotation: null,
            AllowedQuantity : [],
            Closable : false,
            CloseRequestModal : false,
            historyDrawer : false,
            spinning : false,
        }

        this.handleChange = this.handleChange.bind(this);

        this.Service = new PurchaseService();
        this.getItemName = this.getItemName.bind(this);

        this.setFile = this.setFile.bind(this);
        this.getSupplierName = this.getSupplierName.bind(this);
        this.findSupplierNames = this.findSupplierNames.bind(this);

        this.onTabChange = this.onTabChange.bind(this);

        this.toggleHistory = this.toggleHistory.bind(this);

        this.StartSpin = this.StartSpin.bind(this);
        this.StopSpin = this.StopSpin.bind(this);
    }

    componentWillMount(){
        var self = this;
        this.setState({
            wfid : this.props.match.params.id
        });

  
    }



    componentDidMount(){
        var self = this;
        this.Service.GetLastPurchaseWorkitem(this.state.wfid).then(function(response){
            console.log(response);
            if(response.data.status == true){
                self.setState({
                    Request : response.data.response.data,
                    Worflow : response.data.response
                });
                if(response.data.response.data.quotations != null){
                    self.setState({
                        Quotations : response.data.response.data.quotations
                    });
                }
                if(response.data.response.data.purchaseOrders != null){
                    self.setState({
                        OrderIDs : response.data.response.data.purchaseOrders
                    });
                }
            }
            if(response.data.status == false){
                swal("Error",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        }).then(function(){
            if(self.state.Request != null){
                var dict = {};
                self.state.Request.request.items.map(function(item){
                    self.Service.GetItemName(item.code).then(function(response){
                       dict[item.code]=response.data;
                    }).catch(function(error){
                        swal("Error",error.message,"warning");
                    })
                })
                setTimeout(() => {
                    self.setState({
                        codeNameDict : dict
                    });
                }, 1000);

                var length= self.state.Request.request.items.length;
                var a = [];
                for (let index = 1; index <= length; index++) {
                a.push(index)
                }
                self.setState({
                    ItemLength : a
                });
              console.log(self.state);

            }

            console.log(self.state);
        })

        
    }

    toggleHistory(){
        this.setState({
            historyDrawer : !this.state.historyDrawer
        })
    }



    onTabChange = (activeKey) => {
        this.setState({ activeTab : activeKey });
      }

 


    findSupplierNames(){
        var self = this;
        var suppDict = {};
        self.state.Quotations.map(function(item){
            self.Service.GetSupplierName(item.code).then(function(response){
                suppDict[item.code] = response.data;
            }).catch(function(error){
                swal("Error",error.message,"warning");
            });
        });
        self.setState({
            supplierNameDict : suppDict
        });
    }


    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }

    setFile(doc){
        this.setState({
            selectedFile : doc
        });
    }

    toggleQuotationModal(){
        this.setState({
            QuotationModal : !this.state.QuotationModal
        });
    }

    getItemName = (code) => {
        console.log(this.state.codeNameDict[code]);
        return this.state.codeNameDict[code]
    };

    getSupplierName = (code) => {
        return this.state.supplierNameDict[code];
    }



    
    
    StartSpin(){
        this.setState({
            spinning : true
        });
    }

    StopSpin(){
        this.setState({
            spinning : false
        });
    }



    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });

    }

   

  




    render(){
        var self = this;
        const itemColumn = [
            {
                title : "Item",
                dataIndex : "description",
              //  render : d => 
              // dataIndex : "code"
            },{
                title : "Quantity",
                dataIndex : "quantity"
            }
        ]

        const getSupplierDocumentDownloader = (d) => {
            return (
                <a
                onClick={()=> {
                    console.log(d);
                   Axios(baseUrl +`/Payment/DownloadFile/${d.document.id}`, {
                       method: 'GET',
                       responseType: 'blob' //Force to receive data in a Blob Format
                   })
                   .then(response => {
                   //Create a Blob from the PDF Stream
                       const file = new Blob(
                         [response.data],
                         {type: 'application/pdf'});
                   //Build a URL from the file
                       const fileURL = URL.createObjectURL(file);
                   //Open the URL on new Window
                       window.open(fileURL);
                   })
                   .catch(error => {
                       console.log(error);
                   });


                }}

                >

               {d.document.name}</a>
            )
        }

        const getTotalAmount = (d) => {
            var items = d.items;
            var total = 0;
            items.map(function(item){
                total += item.amount
            });
            return total;
        }

        const QuotaionColumns = [
            {
                title : "Name",
                dataIndex : "supplier.nameCode"
            },{
                title  : "Descriptoin",
                dataIndex : "note"
            },
            {
                title : "Document",
                render : d => getSupplierDocumentDownloader(d)
            },{
                title : "Total Amount",
                render : d => getTotalAmount(d)
            }
        ]



        const itemInitializer = () => {

        }
        var self= this;

       const currentState = this.state.Worflow != null ? this.state.Worflow.currentState : 0;

        return (
                <div className="purchseOrder">
                    <Drawer
        title="Payment History"
        placement="right"
        closable={true}
        onClose={this.toggleHistory}
        visible={this.state.historyDrawer}
        width={400}
        >
         <WFHistoryViewer 
        {...this.props} 
        type="purchase"
        wfid={this.state.wfid} />
        </Drawer>
 <Tabs  
 defaultActiveKey="1"
            onChange = {this.onTabChange}
           activeKey = {this.state.activeTab}
           type='card'
           style={{margin : '0 !important'}}
            >
    <TabPane tab="Purchase Request" key="1">
    <Card
                title="Purchase Request"
                extra ={
                    <Button.Group>
                        <Button onClick={() => this.toggleHistory()}>Show History</Button>
                     
                    </Button.Group>
                }
                >
                {
                    this.state.Request != null ?
                    <div>
                     
                        {
                            [2,3,4,-1].indexOf(currentState) != -1 ?
                        <div className="pull-right value">
                                                    PRN {this.state.Request.requestNo.reference}
                                                </div>
                            : null
                        }
    


    <Row>
                        <Col span={4} className="prop">Requested By</Col>
                        <Col span={10} className="value">{this.state.Request.request.requestedBy.name}</Col>
                    </Row>
                    <Row>
                    <Col span={4} className="prop">Items</Col>
    
                    <Col span={16} className="value">
                    <Table columns={itemColumn}
                    pagination={false}
                    dataSource={this.state.Request.request.items}
                    />
                    </Col>
                </Row>
                <Row>
                        <Col span={4} className="prop">Document </Col>
                        <Col span={18} className="value">
                        <a
                         onClick={()=> {
                            Axios(baseUrl +`/Payment/DownloadFile/${this.state.Request.request.document.id}`, {
                                method: 'GET',
                                responseType: 'blob' //Force to receive data in a Blob Format
                            })
                            .then(response => {
                            //Create a Blob from the PDF Stream
                                const file = new Blob(
                                  [response.data],
                                  {type: 'application/pdf'});
                            //Build a URL from the file
                                const fileURL = URL.createObjectURL(file);
                            //Open the URL on new Window
                                window.open(fileURL);
                            })
                            .catch(error => {
                                console.log(error);
                            });
    
    
                         }}
    
                         >
    
                        {this.state.Request.request.document.name}</a>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={4} className="prop">Note</Col>
                       <Col span={16} className="value">
                       {this.state.Request.request.note}
                       </Col>
                    </Row>
                    <Row>
                        {
                            [2,3,4,-1].indexOf(currentState) != -1 ?

<Row>
                        <Col span={4} className="prop">Price Quotations</Col>
                        <Col span={18} className="value"> <Table
                        className="components-table-demo-nested"
                        columns={QuotaionColumns}
                        expandedRowRender={expandedRowRender}
                        dataSource={this.state.Quotations}
                        /></Col>
                    </Row>
                            : null
                        }
                    
                    <Row>

                   
                    </Row>
            
                    </Row>
                    
                        
                    
    </div>
             
    : null
    
                }

                </Card> 
    </TabPane>
            <TabPane tab="Purchase Order" key="2">
            <Card 
            title="Purchase Orders">
            {
               this.state.OrderIDs.length > 0 ? 
               <Tabs
               tabPosition="left"
             >
             {
                 this.state.OrderIDs.map(function(item,index){
                        return <TabPane tab={`Order #${index+1}`} key={index}>
                        <PurchaseOrderWF {...self.props} 
                        title=""
                        wfid={item}
                        />                       
                        </TabPane>
                 })
             }
               

             </Tabs>

               : null
           }
           

               
             
            </Card>
            </TabPane>
    </Tabs>
                   

              
       
                </div>

        )
        
    }
}

export default Form.create() (Finalize);


