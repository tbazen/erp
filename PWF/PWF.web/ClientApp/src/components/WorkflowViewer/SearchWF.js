import React, { Component } from 'react';
import { Table, Divider,Card, Row, Col, Button,  Input,Modal, Alert, message, Menu, Dropdown, Icon, Tabs, Form, Upload, Drawer, Spin, Select, DatePicker } from 'antd';
import swal from 'sweetalert';
import TextArea from 'antd/lib/input/TextArea';
import EmployeeSelect from '../miniComponents/EmployeeSelect';
import SupplierSelect from '../miniComponents/CustomerSelect';
import {StoreStates,PaymentStates,PurchaseState,OrderState, Workflows, SRW, PW, PRW, POW} from '../../service/WFStates';
import '../../assets/css/search.scss';
import { WFSearchService } from '../../service/wfsearch.service';
import { Link } from 'react-router-dom';

const Option  = Select.Option;



class SearchWF extends Component {
    constructor(props){
        super(props);
        this.state = {
            wftype : null,
            SearchResult : [],

        }

        this.Search = this.Search.bind(this);
        this.TypeSelected = this.TypeSelected.bind(this);
        this.Service = new WFSearchService();
        this.GetWFStates = this.GetWFStates.bind(this);
    }

    componentDidMount(){
        this.props.loader.stop();
    }

    Search(){

        var self = this;
        this.props.form.validateFields((err,values) => {
            console.log(err);
            console.log(values);
            if(err == null){
                self.props.loader.start();
                if(values.supplier != null){
                    values.supplier = values.supplier.value;
                }
                if(values.employeeID != null){
                    values.employeeID = values.employeeID.value;
                }
                self.Service.SearchWorkflow(values).then(function(response){
                    self.props.loader.stop();
                    if(response.data.status == false){
                        swal("Error",response.data.error,"warning");
                    }
                    if(response.data.status == true){
                        console.log(response.data.response);
                        self.setState({
                            SearchResult : response.data.response
                        });
                    }
                }).catch(function(error){
                    self.props.loader.stop();
                    swal("Error",error.message,"warning");
                    console.log(error);
                })
            }
            
        })
    }

    GetWFStates(){
        if(this.state.wftype == SRW){
            return StoreStates;
        }
        else if(this.state.wftype == PW){
            return PaymentStates;
        }
        else if(this.state.wftype == PRW){
            return PurchaseState;
        }
        else if(this.state.wftype == POW){
            return OrderState;
        }
        else {
            return [];
        }
    }

    TypeSelected(value){
        this.setState({
            wftype : value
        })
    }

    render(){

        const getWFType = (d) => {
            var self = this;
           var wf = Workflows.filter(function(item){
                return item.id == d.typeId
            })[0].name;
            return wf;
        }

        const getState = (d) => {
            var state = this.GetWFStates().filter(function(item){
                return item.id == d.currentState
            })[0].name;
            return state;
        }

        

        const { getFieldDecorator, getFieldValue } = this.props.form;
        const formItemLayout ={
            labelCol : { span : 4},
            wrapperCol : {span : 18}
        }
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 16, offset: 4
            }
            
        };

        const resultColumns = [
            {
                title : "Type",
                render : d => getWFType(d)
            },
            {
                title : "Current State",
                render : d => getState(d)
            },
            {
                title : "Description",
                dataIndex : "description"
            },
            {
                title : "View",
                render : d => (<Link to={`/wf/view/${d.typeId}/${d.workflowId}`}><Button icon="eye" shape="circle"></Button></Link>)
            }
        ]

        return(
            <div className="search_wf">
                        <Row>
            <Col span={8}>
            <Card
            title="Search Workflows"
            style={{minHeight : 600}}
            extra= {
                <Button icon="search" onClick={this.Search}>Search</Button>
            }
            >
                <Form layout="horizontal">
                   
                <Form.Item
                {...formItemLayout}
                className="form_items"
                label = "Type"
                >
                {
                   getFieldDecorator("type",{
                        rules : [{
                            required : true, message : "Please select workflow type"
                        }]
                }) (
                   <Select
                   onSelect={this.TypeSelected}>
                      {
                          Workflows.map(function(item){
                              return <Option value={item.id} key={item.id}>{item.name}</Option>
                          })
                      }
                   </Select>
                )
                }
                   
                </Form.Item>
                <Form.Item
                 className="form_items"
               >
               <Form.Item
                style={{display: 'inline-block', width : 'calc(50%)'}}
                className="search_labels"
                label="From">
                {
                    getFieldDecorator("fromDate",{

                    })(
                        <DatePicker />
                    )
                }

                </Form.Item>
                <Form.Item
                style={{display: 'inline-block', width : 'calc(50%)'}}
                className="search_labels"
                label="To">
                {
                    getFieldDecorator("toDate",{

                    })(
                        <DatePicker />
                    )
                }

                </Form.Item>

                </Form.Item>
                <Form.Item
                 className="form_items"
                >
                <Form.Item
                 style={{display: 'inline-block', width : 'calc(50%)'}}   className="search_labels"
                 label="Reference"> 
                 {
                     getFieldDecorator("reference",{
 
                     })(
                         <Input />
                     )
                 }
 
                 </Form.Item>
                 <Form.Item
                 style={{display: 'inline-block', width : 'calc(50%)'}}   className="search_labels"
                 label="Current State">
                 {
                     getFieldDecorator("currentState",{
 
                     })(
                        <Select>
                    {
                          this.GetWFStates().map(function(item){
                              return <Option value={item.id} key={item.id}>{item.name}</Option>
                          })
                      }
                        </Select>
                     )
                 }
 
                 </Form.Item>
 
                 </Form.Item>
                 <Form.Item
                 style={{display: 'inline-block', width : 'calc(100%)'}}   className="search_labels"
                className="form_items"
                label = "Note"
                >
                {
                   getFieldDecorator("Note",{
                       
                }) (
                  <TextArea />
                )
                }
                   
                </Form.Item>
                <Form.Item
                 className="form_items"
                >
                {
                    this.state.wftype == 1 || this.state.wftype == 2 ?
                <Form.Item
                 style={{display: 'inline-block', width : 'calc(50%)'}}   className="search_labels"
                 label="Requested By"> 
                 {
                     getFieldDecorator("employeeID",{
 
                     })(
                         <EmployeeSelect />
                     )
                 }
 
                 </Form.Item>

                    : null
                }
                 {
                    this.state.wftype == 1002 || this.state.wftype == 2 ?
                    <Form.Item
                    style={{display: 'inline-block', width : 'calc(50%)'}}   className="search_labels"
                    label="Supplier">
                    {
                        getFieldDecorator("supplier",{
    
                        })(
                           <SupplierSelect />
                        )
                    }
    
                    </Form.Item>

                    : null
                }
                
                 
 
                 </Form.Item>
                </Form>
            </Card>
            </Col>

            <Col span={16}>
            <Card 
            title="Search Result"
            style={{minHeight : 600}}
            >
            <Table
            dataSource={this.state.SearchResult}
            columns={resultColumns} />
            </Card>
            </Col>
        </Row>
    
            </div>
        )

        
        
    }
}

export default Form.create()(SearchWF);