import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import { StoreService } from '../../service/store.service';
import ReactTable from 'react-table';
import 'antd/dist/antd.min.css';
import { Tree, Input, Card, Row, Col, Table, Tabs, DatePicker, Modal, Button, Drawer, Form, Select, Spin } from 'antd';
import { Link } from 'react-router-dom';
import PostStoreIssueDocument from '../Document/StoreIssueDocument';
import Item from 'antd/lib/list/Item';
import TransactionDocumentItemForm from '../Item/TransactionDocumentItemForm';
import IssueItemsListForm from '../Item/IssueItemsListForm';
import DocumentReference from '../miniComponents/DocumentReference';
import StoreSelect from '../miniComponents/StoreSelect';
import SweetAlert from 'react-bootstrap-sweetalert';
import StoreIssueDocument from '../Document/StoreIssueDocument';
import { WFHistoryViewer } from '../WFHistoryViewer';
import Axios from 'axios';
import { baseUrl } from '../../service/urlConfig';
const TabPane = Tabs.TabPane;

export default class StoreRequestWF extends Component{
    constructor(props){
        super(props);

        this.state = {
            wfid : '',
            Request : {},
            Reorganized : [],
            Approved : [],
            Issued : [],
            IssuedItemsList : [],
            toBeIssuedItems : [],
            Closed : [],
            typeID : '',
            reference : '',
            primary : false,
            DocumentDate : '',
            storeID : '',
            showAlert : false,
            alertMessage : '',
            showModal : false,
            showCloseRequestModal : false,
            spinning : false,
            
        };


        this.setDocumentReference = this.setDocumentReference.bind(this);

        this.setDate = this.setDate.bind(this);

        this.showMessage = this.showMessage.bind(this);

        this.child = React.createRef();
        this.storeChild = React.createRef();
        this.CloseRequestChild = React.createRef();

        this.getIssuedArray = this.getIssuedArray.bind(this);
        this.getUniqueCode = this.getUniqueCode.bind(this);
        this.reconstructData =this.reconstructData.bind(this);

        
    }
    

    componentWillMount(){
        this.setState({
            wfid : this.props.match.params.id
        });
    }



    getIssuedArray(issued){
        var items = []
        for (let i = 0; i < issued.length; i++) {
            for (let j = 0; j < issued[i].items.length; j++) {
                items.push({
                    code: issued[i].items[j].code,
                    name: issued[i].items[j].name,
                    quantity: issued[i].items[j].quantity,
                    docID: issued[i].AccountDocumentID,
                    date: issued[i].materializedOn,
                    reference : "SIV "+ issued[i].PaperRef
                });
            }

        }
        return items;
    }

  



    componentDidMount(){
        this.props.loader.stop();
        var self = this;
        var Service = new StoreService(this.props.session.token)
        Service.GetLastWorkItem(self.state.wfid).then(function(data){
            console.log(data);
            self.setState({
                Request : data.data,
                Reorganized : data.data.data.Reorganized,
                Approved : data.data.data.Approved,
                Closed : data.data.data.Closed,
            })
            if(data.data.data.Issued != null){
                var items = self.getIssuedArray(data.data.data.Issued);
                console.log(data);
                    self.setState({
                        Issued: data.data.data.Issued,
                        IssuedItemsList : items,
                    });
            }
          
        }).then(function(){
            console.log(self.state);
            
            
        })
    }


    getUniqueCode(){
        var codes = [];
        this.state.Approved.map(function(item){
            codes.push(item.code);
        });
        return codes;
    }

    reconstructData(input){
        var self = this;
        var codes = this.getUniqueCode();
        var data = [];
        codes.forEach(function(value,index){
            var temp = {};
            var code = value;
            var name = self.state.Approved.filter(function(item){
                return item.code == code;
            })[0].name
            temp["name"] = name;
            temp["code"] = input["code"+code];
            temp["balance"] = input["balance"+code];
            temp["quantity"] = input["quantity"+code];
            temp["unitID"] = input["unitID"+code].value;
            temp["unitPrice"] = input["unitPrice"+code];
            temp["fixedExpense"] = input["fixedExpense"+code];
            temp["costCenterID"] = self.props.session.costCenterID;
            data.push(temp);
        });
        return data;
    }

    

    setDocumentReference(type,value){
        if(type == "typeID"){
            this.setState({
                typeID : value
            });
        }
        if(type == "reference"){
            this.setState({
                reference : value
            });
        }
        if(type == "primary"){
            this.setState({
                primary : value
            });
        }
        
    }

    showMessage(message){
        this.setState({
            showAlert : true,
            alertMessage : message
        })
    }

    setDate(date,dateString){
        this.setState({
            DocumentDate : dateString
        });
    }




 

    render(){

        function DocumentTable (record){
            console.log(record)
            return (
                <Table
                    dataSource={record.items}
                    columns={iColumns}
                    pagination={false}
                    scrol={{ y: 600 }}
                />
            );
            
            }



        const iColumns = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Requested',
            dataIndex: 'quantity',
        },
    {
        title : "Approved",
        render : d => getApprovedQuantity(d.code)
    }];


        const OuterTable = [{
            title : "SIV",
            dataIndex : "PaperRef",
            
        },{
            title : "Date",
            render : d => new Date(d.DocumentDate).toLocaleString() //"DocumentDate"
        }];
        
        const issuedColumns = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Balance',
            dataIndex: 'balance',
            },
             {
                title: 'Quantity',
                dataIndex: 'quantity',
            },
        {
            title : "Unit Price",
            dataIndex : "unitPrice"
        }];

        const getApprovedQuantity = (code) => {
            var i = this.state.Approved.find(function(item){
                return item.code == code
            });
            if(i != null && i.quantity != null){
                return i.quantity;
            }
            else {
                return 0;
            }
        }

        const reorganizeColumns = [{
            title: "Code",

            dataIndex: "code"
        }, {
            title: "Name",
            dataIndex: "name"
        }, {
            title: "Quantity",
            dataIndex: "quantity"
        }

        ];

        const issued = [{
            title: 'Code',
            dataIndex: 'code',
            key: 'code'
        }, {
            title: "Name",
            dataIndex: 'name',
            key: 'name',
        }, {
            title: 'Quantity',
            dataIndex: 'quantity',
        }];

 
        return(
            <div>
             
                <SweetAlert title="Store Issue!"
                    warning
                    confirmBtnText="Ok"
                    confirmBtnBsStyle="default"
                    cancelBtnBsStyle="default"
                    onConfirm={() => {
                        this.setState({
                            showAlert: false
                        });
                    }}
                    show={this.state.showAlert}>
                    {this.state.alertMessage}
                        </SweetAlert>
                <Row>

                         <Col  md={16}>
                         <Card title="Requested & Approved Items"
                                      >
                               <Row>
                    <Col span={4} className="prop">Request No</Col>
                    <Col span={18} className="value">{this.state.Request.data != null ? `SRN ${this.state.Request.data.RequestNo.reference}` : null}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Request Date</Col>
                    <Col span={18} className="value">{this.state.Request.data != null ? new Date(this.state.Request.data.RequestDate).toLocaleString() : null}</Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Description</Col>
                    <Col span={18} className="value">{this.state.Request.data != null ? this.state.Request.data.requestDescription : null}</Col>
                </Row>
                {
                    this.state.Request.currentState == 0 ||  this.state.Request.currentState == 1 || this.state.Request.currentState == -2?
                    
                                     <Row>
                    <Col span={4} className="prop">Requested Items</Col>
                    <Col span={18}> <Table
                                        dataSource={this.state.Request.data.Requested}
                                        columns={reorganizeColumns}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    /></Col>
                </Row>
                   
                    : null
                }
                 {
                    this.state.Request.currentState == 2 ?
                    
                                     <Row>
                    <Col span={4} className="prop">Reorganized Items</Col>
                    <Col span={18}> <Table
                                        dataSource={this.state.Request.data.Reorganized}
                                        columns={reorganizeColumns}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    /></Col>
                </Row>
                   
                    : null
                }
                {
                    this.state.Request.currentState == 3 || this.state.Request.currentState == -1 ?
                    <div>
   <Row>
                    <Col span={4} className="prop">Requested Items</Col>
                    <Col span={18}> <Table
                                        dataSource={this.state.Reorganized}
                                        columns={iColumns}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    /></Col>
                </Row>
                <Row>
                    <Col span={4} className="prop">Issued Items</Col>
                    <Col span={18}>    <Table
                                    columns={OuterTable}
                                    expandedRowRender={record => <Table
                                        dataSource={record.items}
                                        columns={issued}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    /> }
                                    dataSource={this.state.Issued}
                                /></Col>
                </Row>
              
                    </div> : null
                }
                {

                        this.state.Request.currentState == -1 ?
               
               <div>

<Row>
                    <Col span={4} className="prop">Voucher</Col>
                    <Col span={18} className="value">
                    <a
                onClick={()=> {
                    this.state.Request.data != null ?
                   Axios(baseUrl +`/Payment/DownloadFile/${this.state.Closed.Voucher.ID}`, {
                       method: 'GET',
                       responseType: 'blob' //Force to receive data in a Blob Format
                   })
                   .then(response => {
                   //Create a Blob from the PDF Stream
                       const file = new Blob(
                         [response.data],
                         {type: 'application/pdf'});
                   //Build a URL from the file
                       const fileURL = URL.createObjectURL(file);
                   //Open the URL on new Window
                       window.open(fileURL);
                   })
                   .catch(error => {
                       console.log(error);
                   })

                   : null


                }}

                >

              {this.state.Closed.Voucher.Name}</a>
                    </Col>
                </Row>
                    </div> : null
                }
             
                                   
                                </Card>


                            {/* <Card title="Issued Items"> 

                                 
                                <Table
                                    columns={OuterTable}
                                    expandedRowRender={record => <Table
                                        dataSource={record.items}
                                        columns={iColumns}
                                        pagination={false}
                                        scrol={{ y: 600 }}
                                    /> }
                                    dataSource={this.state.Issued}
                                />

                            </Card>                   */}

 
                         </Col>
                         <Col md={8}>
                         <Card
                         title="Store Request WF History"
                         >



                            <WFHistoryViewer 
                            {...this.props} 
                            type="store"
                            wfid={this.state.wfid}
                            />
                        </Card>
                         </Col>
                            </Row>
            </div>
        )
    }
}