import React, { Component } from 'react';
import swal from 'sweetalert';
import {StoreStates,PaymentStates,PurchaseState,OrderState, Workflows, SRW, PW, PRW, POW} from '../../service/WFStates';
import StoreRequestWF from './StoreRequestWF';
import PaymentWF from './PaymentWF';
import PurchaseWF from './PurchaseWF';
import PurchaseOrdeerWF from './PurchaseOrderWF';
export default class ViewWF extends Component {

    constructor(props){
        super(props);
    }

    componentDidMount(){
        if(this.props.match.params.type == POW){
            this.props.match.params = {};
        }
    }


    render(){

        const orderProp =  {
                params : {
                    id : ''
                }
            
        };

        return(
            <div>
                {
                    this.props.match.params.type == SRW ?
                        <StoreRequestWF
                      
                        {...this.props} />
                    : null

                    
                }

                    {
                    this.props.match.params.type == PW ?
                        <PaymentWF
                      
                        {...this.props} />
                    : null
                }

{
                    this.props.match.params.type == PRW ?
                        <PurchaseWF
                      
                        {...this.props} />
                    : null
                }

{
                    this.props.match.params.type == POW ?
                        <PurchaseOrdeerWF
                      wfid={this.props.match.params.id}
                        match={orderProp}
                        title="Purchase Order"
                        {...this.props} />
                    : null
                }
            </div>
        )
    }
}