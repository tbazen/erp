import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';

export class SideBar extends Component{
    constructor(props){
        super(props);
    }

    render(){
        const style = {
            'maxWidth': 150,
            'paddingLeft': 20
        }
        const style2 = {
            "lineHeight": 25
        }

        return <div className="main-navbar">
            <nav className="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
                <a className="navbar-brand w-100 mr-0" href="#" style={style2}>
                    <div className="d-table m-auto">
                        <img id="main-logo" className="d-inline-block align-top mr-1" style={style} src="images/intaps1.png"
                            alt="Shards Dashboard" />
                        <span className="d-none d-md-inline ml-1">Shards Dashboard</span>
                    </div>
                </a>
                <a className="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                    <i className="material-icons">&#xE5C4;</i>
                </a>
            </nav>
            <div className="nav-wrapper">
                <ul className="nav flex-column">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link" activeClassName="active">
                            <i className="material-icons">edit</i>
                            <span>Blog Dashboard</span>
                        </NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/store" className="nav-link " activeClassName="active">
                            <i className="material-icons">vertical_split</i>
                            <span>Store</span>
                        </NavLink>
                    </li>

                </ul>
            </div>
        </div>
    }
}