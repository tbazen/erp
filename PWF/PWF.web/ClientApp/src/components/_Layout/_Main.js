import React, { Component } from 'react';
import { SideBar } from './SideBar';
import { NavBar } from './NavBar';

export class Main extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return <div className="container-fluid">
            <div className="row">
                <aside className="main-sidebar col-12 col-md-3 col-lg-2 px-0">
                    <SideBar />
                </aside>
                <main className="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
                    <NavBar {...this.props} />
                    <div className="main-content-container container-fluid px-4">
                        {this.props.children}
                    </div>
                </main>
            </div>
        </div>;
    }
}