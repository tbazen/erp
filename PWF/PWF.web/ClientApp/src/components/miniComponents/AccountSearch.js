import React, {Component } from 'react';
import {
    Icon, Button, Input, AutoComplete,
  } from 'antd';
  import { PaymentService } from '../../service/payment.service';

  const Option = AutoComplete.Option;
  

  
  function getRandomInt(max, min = 0) {
    return Math.floor(Math.random() * (max - min + 1)) + min; // eslint-disable-line no-mixed-operators
  }
  
  function searchResult(query) {
    return (new Array(getRandomInt(5))).join('.').split('.')
      .map((item, idx) => ({
        query,
        category: `${query}${idx}`,
        count: getRandomInt(200, 100),
      }));
  }
  
  function renderOption(item) {
    return (
      <Option key={item.id} text={item.codeName}>
        {/* {item.query} 在
        <a
          target="_blank"
          rel="noopener noreferrer"
        >
          {item.category}
        </a> */}

        <span className="global-search-item-count">{item.codeName}</span>
      </Option>
    );
  }
  
  export default class AccountSearch extends React.Component {

    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            value : '',
          }
        this.service = new PaymentService("");
        this.onSelect = this.onSelect.bind(this);
        this.triggerChange = this.triggerChange.bind(this);
    }
 
  
    handleSearch = (value) => {
        var self = this;
     this.service.SearchAccount(value).then(function(response){
         if(response.data.status == true){
             var result = response.data.response;
             console.log(result);
             if(self.props.leafAccounts != null && self.props.leafAccounts == true){
                    self.setState({
                      dataSource : result.response != null ? result.response.filter(function(item){
                        return item.childCount == 0;
                      }) : []
                  });
             }
             else{
                self.setState({
                  dataSource : result.response != null ? result.response : []
              });
             }
             
         }
     })
    }

    onSelect(value) {
        this.setState({
            value : value
        });
        this.triggerChange({value});
      }

      triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }
  
    render() {
      const { dataSource } = this.state;
      return (
        <div className="autocomplete">
          <AutoComplete
            className="global-search-wrapper"
        //     dropdownMatchSelectWidth = {500}
            
        //     style={{ width: this.props.width }}
          dropdownMenuStyle={{ minWidth : 500}}
          dropdownClassName="searchDropDown"
         dropdownStyle={{ maxHeight: 400, overflow: 'auto' , width : '500px !important'}}
            dataSource={dataSource.map(renderOption)}
            onSelect={this.onSelect}
            onSearch={this.handleSearch}
            placeholder="Search Account"
            optionLabelProp="text"
          >
            <Input
              suffix={(
                
                  <Icon type="search" />

              )}
            />
          </AutoComplete>
        </div>
      );
    }
  }
  
