import React, { Component } from 'react';
import { TreeSelect, Tree, Button } from 'antd';
import 'antd/dist/antd.min.css';
import { StoreService } from '../../service/store.service';


function accountToNode(data) {
    const arr = [];
    data.map(function (item, index) {
        var el = {
            name: item.codeName,
            id: item.id,
            title: item.codeName,
            value: item.id,
            pid: item.pid,
        };
        if (item.childCount > 0) {
            el.isLeaf = false;
            el.selectable = false;
            el.children = [];
        }
        else {
            el.isLeaf = true;
            el.selectable = true;
            //el.children = null;
        }
        arr.push(el);
    });
    return arr;
}

export default class AccountSelect extends Component {

    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }


    constructor(props) {
        super(props);
        const value = props.value || '';
        this.state = {
            treeData: [],
            checkedKeys: [],
            value : value,
        };
        //this.loop = this.loop.bind(this);
        this.onLoadData = this.onLoadData.bind(this);
        this.getNewData = this.getNewData.bind(this);
        this.extract_tree = this.extract_tree.bind(this);
        // this.getFieldName = this.getFieldName.bind(this);
        // this.getFieldValue = this.getFieldValue.bind(this);
        // this.setFieldName = this.setFieldName.bind(this);
    }

    componentDidMount() {
        var self = this;
        var Service = new StoreService(self.props.session.token);
        var arr;
        Service.GetChildAccounts(-1).then(function (response) {
            var data = response.data;
            arr = accountToNode(data);

        }).catch(function (error) {
            console.log(error);
        }).then(function () {
            setTimeout(() => {
                this.setState({
                    treeData: arr
                });
            }, 100);
            console.log("Mounting ...");
        }.bind(this))


    }

    // getFieldValue() {
    //     return this.state.value
    // }

    // setFieldName(value) {
    //     this.setState({ value })
    // }

    // getFieldName(value) {
    //     return this.state.value
    // }

    onSelect = (value, node, extra) => {
        console.log('selected', { value, node, extra });
        if(!('value' in this.props)){
            this.setState({
                value : value
            })
        }

        this.triggerChange({ value })
    }

    onCheck = (checkedKeys) => {
        this.setState({
            checkedKeys,
        });
    }

    extract_tree = (tree) => {
        var list = [];
        for (var i = 0; i < tree.length; i++) {
            if (tree[i].children != null && tree[i].children.length > 0) {

                var ext = this.extract_tree(tree[i].children);
                list = [...list, tree[i], ...ext];
            }
            else {
                list = [...list, tree[i]];
            }
        }
        return list;
    }

    list_to_tree = (list) => {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i; // initialize the map
            list[i].children = []; // initialize the children
        }

        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.pid != -1) {
                // if you have dangling branches check that map[node.parentId] exists
                list[map[node.pid]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    getNewData = (treeNode) => {
        var self = this;
        const arr = [];
        var key = treeNode.props.id;
        var Service = new StoreService(self.props.session.token);
        Service.GetChildAccounts(key).then(function (response) {
            var data = response.data;
            const resp = accountToNode(data);
            resp.map(function (item) {
                arr.push(item);
            });

        }).then(function () {
            var newData = [...self.state.treeData, ...arr];
            const extracted = self.extract_tree(newData);
            const treeData = self.list_to_tree(extracted);
            self.setState({ treeData });



        });
    }

    onLoadData = (treeNode) => {
        var self = this;
        var promise = new Promise((resolve) => {
            setTimeout(() => {
                self.getNewData(treeNode);
                resolve();
            }, 100);

        });
        return promise.then(function (value) {
        });
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue));
        }
    }



    render() {

        return (
            <div>
                <TreeSelect
                    //style={{ width: 300 }}
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    onSelect={this.onSelect}
                    onCheck={this.onCheck}
                    loadData={this.onLoadData}
                    treeData={this.state.treeData}
                    treeDataSimpleMode={true}
                    placeholder="Please select account"
                />
                {/* {treeNodes}
                </TreeSelect> */}
            </div>
        );
    }
}