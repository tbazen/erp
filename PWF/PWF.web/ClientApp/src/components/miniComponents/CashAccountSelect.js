import React, { Component } from 'react';
import { TreeSelect, Tree, Button, Select } from 'antd';
import 'antd/dist/antd.min.css';
import { PaymentService } from '../../service/payment.service';

const { Option } = Select;
export default class CashAccountSelect extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        const value = props.value || '';
        this.state = {
            CashAccounts: [],
            value: value,
        };


    }

    componentDidMount() {
        var self = this;
        var service = new PaymentService(this.props.session.token);
        console.log("Calling API ...");
        service.GetAllCashAccounts().then(function (response) {
            console.log(response);
            if (response.data.status) {
                self.setState({
                    CashAccounts : response.data.response
                })
            }
            if (!response.data.status) {
                console.log(response.error);
            }
        }).catch(function(error){
            console.log(error);
        })
    }

    onSelect = (value) => {
        if(!('value' in this.props)){
            this.setState({ value } );
        }
        this.triggerChange({value})
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }

    render() {

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            style: {
                marginBotton: 0,
                paddingBottom : 0
            }
        }

        return (
          <span>
              <Select placeholder="Please select cash account .."
              style={{ width: 500 }}
              onSelect={this.onSelect}>
              {
                  this.state.CashAccounts.map(function(item){
                      return  <Option value={item.csAccountID}
                      key={item.csAccountID}
                      
                      >
                {item.name}
                      </Option>
                  })
              }
              
             
              </Select>
          </span>
            
            )
        }
    }

