import React from 'react';

import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select, Checkbox } from 'antd';
import 'antd/dist/antd.min.css';
import '../../assets/css/style.css';
import CostCenterSelect from '../miniComponents/CostCenterSelect';
import UnitSelect from '../miniComponents/UnitSelect';
const { Option } = Select;

class DocumentReference extends React.Component {
    constructor(props){
        super(props);
        
        this.state = {
            documentTypes : [],
            filteredDocumentTypes : []
        }

        this.onChange = this.onChange.bind(this);
        
    }

    componentDidMount(){
        var self = this;
        var service = new StoreService(this.props.session.token);
        service.GetAllDocumentSerialTypes().then(function(response){
            self.setState({
                documentTypes: response.data,
                filteredDocumentTypes: response.data.filter(function(item){
                    return self.props.documentTypes.indexOf(item.code) != -1;
                })
            })
        })
    }

    onChange = (x,y,z) => {
        var vals = this.props.form.getFieldsValue();
        this.props.setDocumentReference("reference", vals.reference);
        this.props.setDocumentReference("primary", vals.primary);
    }

    onSelect = (value,node,extra)=> {
        this.props.setDocumentReference("typeID",value);


    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {

            //labelCol: { span: 10 },
            //wrapperCol: { span: 14 },
            style: {
                marginBottom: 0,
                paddingBottom: 0
            }
        };
        return (
           
                <Form onChange={this.onChange} > 
                    <h3>Document Reference</h3>
                    <Row gutter={24}>
                        <Col span={12}>
                            <Form.Item
                            {...formItemLayout}
                            >
                                {
                                    getFieldDecorator('typeID', {

                                    })(
                                        <Select placeholder="Select Reference Type.. " 
                                        onSelect={this.onSelect} name="typeID"
                                            style={{ width: "100%" }}
                                        >
                                            {
                                                this.state.filteredDocumentTypes.map(function (item) {
                                                    return <Option value={item.id}>{item.name}</Option>
                                                })
                                            }
                                        </Select>
                                    )
                                }
                            </Form.Item>



                        </Col>
                        <Col span={12}>
                        <Form.Item {...formItemLayout}>
                                {
                                    getFieldDecorator('reference', {})(
                                        <Input name="reference"  />
                                    )
                                }
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                        <Form.Item {...formItemLayout}>{
                                getFieldDecorator('primary', {})(
                                    <Checkbox name="primary" >Primary</Checkbox>
                                )
                            }

                            </Form.Item>

                        </Col>
                    </Row>

                </Form>

              



               

        )
    }
}

export default Form.create()(DocumentReference);