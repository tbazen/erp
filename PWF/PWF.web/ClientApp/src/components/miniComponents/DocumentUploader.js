import React, { Component } from 'react';
import { Button , Row, Col, Form, Input} from 'antd';
import 'antd/dist/antd.min.css';
import swal from 'sweetalert';
const TextArea = Input.TextArea;

export default class DocumentUploader extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        const value = props.value || '';
        this.state = {
            selectedFile : null,
        }
        this.postDocument = this.postDocument.bind(this);

    }

    processFile(e){
        var fileList = document.getElementById('files').files;
        console.log(e.target.value);
        console.log(fileList.length);
        console.log(fileList[0]);
        var doc = {
            ContentType : fileList[0].type,
            Length : fileList[0].size,
            Name : fileList[0].name
        }
        var fileReader = new FileReader();
        if (fileReader && fileList && fileList.length) {
           fileReader.readAsArrayBuffer(fileList[0]);
           fileReader.onload = function () {
              doc.File =Array.from(new Uint8Array(fileReader.result));
           };
        }
        this.props.setFile(doc);
        this.setState({
            selectedFile : doc
        });
        console.log("State set to document");
    }

    postDocument(e){
        var description = '';
        this.props.form.validateFields((err,values) => {
            if(err == null){
               description = values.Description;
            }});
        return description;
    }
    
    render(){

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {

        }

        return(
            <span>
                <Form layout="vertical"
                onSubmit={this.postDocument}

                >
                       
                       <Form.Item  {...formItemLayout} label="Upload Document">

                    <input required type="file" id="files" name="files" onChange={ (e) => this.processFile(e)}  />
  
                    </Form.Item>
                <Form.Item
                {...formItemLayout}
                label="Note"
                className="prop"
                >{
                    getFieldDecorator('Description',{
                        rules : [{ required : true, message: 'Add Description'}]
                    })(
                        <TextArea name="Description"  rows={4} />
                    )
                }

                </Form.Item>
                </Form>
            </span>
        )
    }
}