import React, { Component } from 'react';
import { TreeSelect, Tree, Button, Select, Form } from 'antd';
import 'antd/dist/antd.min.css';
import { PaymentService } from '../../service/payment.service';
import swal from 'sweetalert';

const { Option } = Select;
class EmployeeSelect extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        const value = props.value || '';
        this.state = {
            Employees: [],
            value: value,
        };


    }

    componentDidMount() {
        var self = this;
        var service = new PaymentService("");
        console.log("Calling API ...");
        service.GetAllEmployees().then(function (response) {
            console.log(response);
            if (response.data.status) {
                self.setState({
                    Employees : response.data.response
                })
            }
            if (!response.data.status) {
                swal("",response.data.error,"warning");
            }
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
    }

    onSelect = (value) => {
        if(!('value' in this.props)){
            this.setState({ value } );
        }
        this.triggerChange({value})
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }

    render() {

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            style: {
                marginBotton: 0,
                paddingBottom : 0
            }
        }

        return (
          <span>
              <Select placeholder="Please select employee .."
              showSearch={true}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}

            //  style={{ width: 500 }}
              onSelect={this.onSelect}>
              {
                  this.state.Employees.map(function(item){
                      return  <Option value={item.id}
                      key={item.id}
                      
                      >
                {item.employeeName}
                      </Option>
                  })
              }
              
             
              </Select>
          </span>
            
            )
        }


}

export default Form.create()(EmployeeSelect);