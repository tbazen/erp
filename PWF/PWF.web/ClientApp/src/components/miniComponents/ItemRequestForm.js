import React, { Component } from 'react';
import { TreeSelect, Tree, Button, Select, Input, Row, Col } from 'antd';
import 'antd/dist/antd.min.css';
import { PaymentService } from '../../service/payment.service';

const { Option } = Select;
const { TextArea } = Input;

export default class ItemRequestForm extends Component {

    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props){
        super(props)
        const value = props.value || '';
        this.state = {
            description : '',
            unit : '',
            unitPrice : '',
            quantity :'',
            amount : '',
        }
    }


    handleChange = (event) => {
        console.log(event);
        let target = event.target;
        if(target.name == "unitPrice" || target.name == "quantity" || target.name == "amount" ){
            console.log(target);
            if(!isNaN(target.value)){
                this.setState({
                    [target.name]: target.value
                });
                this.triggerChange({
                    [target.name] : target.value
                });
            }
            // if(this.state.unitPrice != '' && this.state.quantity != ''){
            //     var x = Number(this.state.unitPrice) * Number(this.state.quantity);
            //     console.log(x);
            //     this.setState({
            //         amount : x
            //     });
            //     this.triggerChange({
            //         amount : x
            //     });
            // }
        }
        else if(target.name =="description" || target.name == "unitID"){
            this.setState({
                [target.name]: target.value
            });
            this.triggerChange({
                [target.name] : target.value
            });
        }


        //console.log(target.value);
    }

    handleBlur = (e) => {
        var target = e.target;
        if(target.name == "unitPrice" || target.name == "quantity" || target.name == "amount" ){
            if(this.state.unitPrice != '' && this.state.quantity != ''){
                var x = Number(this.state.unitPrice) * Number(this.state.quantity);
                console.log(x);
                this.setState({
                    amount : x.toFixed(2)
                });
                this.triggerChange({
                    amount : x.toFixed(2)
                });
            }
        }
    }


    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }

    render(){

        const styles = this.props.style != null ? this.props.style : null;
        return(
            <div
            style={styles}
            >
            {/* <Row>
                <span
                style={{width : '33%'}}
                >Description</span>

                <span style={{width : '12.5%'}}>Unit</span>
                <span style={{width : '12.5%'}}>Unit Price</span>
                <span style={{width : '12.5%'}}>Quantity</span>
                <span style={{width : '12.5%'}}>Amount</span>



            </Row> */}


                <Row>
 <TextArea 
                rows={2}
                value={this.state.description}
                name="description"
                onChange={(e) =>
                    this.handleChange(e)
                }
                style={{
                    width: '30%',
                    marginRight : '3%'
                }}
                placeholder={"Please add description"}

                />
                <Input 
                type="text"
                value={this.state.unitID}

                name="unitID"
             onChange={(e) => this.handleChange(e)}
                style={{width : '12%',
                        marginRight : '0.5%'}}
                placeholder={"Unit"}

                
                        />

                <Input 
                type="text"
                value={this.state.unitPrice}
                name="unitPrice"
              onChange={(e) => this.handleChange(e)}
                
                style={{width : '12%',
                marginRight : '0.5%'}}
                placeholder={"Unit Price"}
                onBlur={(e) => this.handleBlur(e)}
                />

                <Input 
                type="text"
                value={this.state.quantity}
                name="quantity"
                onChange={(e) => this.handleChange(e)}
                style={{width : '12%',
                        marginRight : '0.5%'}}
                placeholder={"Quantity"}
                onBlur={(e) => this.handleBlur(e)}
                        />

                <Input
                type="text"
                value={this.state.amount}
                name="amount"
                 onChange={(e) => this.handleChange(e)}
                style={{width : '12%',
                        marginRight : '0.5%'}}
                 placeholder={"Amount"}
                 onBlur={(e) => this.handleBlur(e)}
                 />

                </Row>
               

            </div>

        )
    }

}