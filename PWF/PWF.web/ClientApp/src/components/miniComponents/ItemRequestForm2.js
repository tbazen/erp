import React, { Component } from 'react';
import { Select, Input, Row, Col, Form, Icon, Button, InputNumber} from 'antd';
import ItemSearch from './ItemSearch';
import UnitSelect from './UnitSelect';

import swal from 'sweetalert';
import 'antd/dist/antd.min.css';
import FormItem from 'antd/lib/form/FormItem';

const ButtonGroup = Button.Group;
let itemRowID = 0;
export default class ItemRequestFrom2 extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props){
        super(props);
        const value = props.value || '';
        this.state = {
            code : '',
            quantity : '',
            unitPrice : '',
            unitID : '',
            price : '',
        }
        this.postDocument = this.postDocument.bind(this);
    }

    onSelect = (value) => {
        console.log(value);
        if(!('value' in this.props)){
            this.setState({ value } );
        }
        this.triggerChange({value})
    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        this.triggerChange({
            [target.name] : target.value
        });
        //console.log(target.value);
    }

    postDocument(){
        var finalItems = []
        var self = this;
        this.props.form.validateFields((err,values) => {
            if(err == null){
                if(values.itemKeys.length == 0){
                    swal("Error","Please insert items","warning");
                    self.props.loader.stop();
                }
                else{
                    values.itemKeys.map(function(item){
                        console.log(item);
                        console.log(values.item[item]);
                        if(self.props.showPriceFields){
                            finalItems.push({
                                code : values.item[item].code.value,
                                quantity : values.item[item].quantity,
                                unitPrice : values.item[item].unitPrice,
                                unitID : values.item[item].unitID.value,
                                amount : values.item[item].amount
                            });
                        }
                        else{
                            finalItems.push({
                                code : values.item[item].code.value,
                                quantity : values.item[item].quantity,
                            });
                        }
                        
                        console.log(finalItems);
                    })
                   console.log(finalItems);
                }
            }
        })

        return finalItems;
    }

    addItemRow = () => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        const nextitemKeys = itemKeys.concat(++itemRowID);
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
          itemKeys: nextitemKeys,
        });
      }

      renameItemRow = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const itemKeys = form.getFieldValue('itemKeys');
        // We need at least one passenger
        if (itemKeys.length === 1) {
          return;
        }
    
        // can use data-binding to set
        form.setFieldsValue({
          itemKeys: itemKeys.filter(key => key !== k),
        });
      }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }

      checkCostCenter = (rule,value,callback) => {
          if(value != undefined){
            callback();
            return;
          }
          callback("Select Cost Center");

      }

      checkItem = (rule,value,callback) => {
        if(value != undefined){
            callback();
            return;
          }
          callback("Select Item");
    }

    checkUnit = (rule,value,callback) => {
        if(this.props.showPriceFields){
            console.log(value);
            if(value.value != undefined){
                callback();
                return;
              }
              callback("Select Unit");
              
              
        }
        else{
            callback();
            return;
        }
        
    }

    handleBlur = (e) => {
        if(this.props.showPriceFields){
            var target = e.target;
            var key = target.id.split("[")[1].split("]")[0];
            var type = target.id.split('.')[1];
            var value = target.value;
            if(type == "quantity"){
                var n = `item[${key}].unitPrice`;
                var am =  `item[${key}].amount`;
                console.log(n);
                var v = this.props.form.getFieldValue(n);
                var product = (value * v).toFixed(2);
                this.props.form.setFieldsValue({
                    [am] : product
                });
                
    
            }
            if(type == "unitPrice"){
                var n = `item[${key}].quantity`;
                var am =  `item[${key}].amount`;
                console.log(n);
                var v = this.props.form.getFieldValue(n);
                var product = (value * v).toFixed(2);
                this.props.form.setFieldsValue({
                    [am] : product
                });
            }
        }
       
    }

    render(){
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 0 },
            wrapperCol: { span: 20, offset:4 },
            style : {
              marginBottom : '1px !important',

          }
          };
          const formItemLayoutWithOutLabel = {
            wrapperCol: {
                span : 16, offset: 4
            }
            
        };
          getFieldDecorator('itemKeys',{initialValue:[] });
          const itemKeys = getFieldValue('itemKeys');
          const formLabel =
          <div style={{marginBottom : '-10px'}}>
              {itemKeys.length >= 1 ? (
          <Form.Item
              style={{marginBottom : '-10px'}}
                >

              

            
            <Form.Item
            label="Item"
            style={{display: 'inline-block', width : 'calc(20%)'}}
            >

            </Form.Item>
            <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
       {' '}
      </span>
            <Form.Item
            label="Quantity"
            style={{display: 'inline-block', width : 'calc(10%)'}}
            >

            </Form.Item>
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>
     
          {
              this.props.showPriceFields ?
             
              <Form.Item
              label="Unit Price"
             style={{display: 'inline-block', width : 'calc(10%)'}}
            >

              </Form.Item>
           
               : null
          }
              <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>

      {
              this.props.showPriceFields ?
             
              <Form.Item
            label="Unit"
                        style={{display: 'inline-block', width : 'calc(10%)'}}
                      >

            </Form.Item>
           
               : null
          }

                      
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>

      {
              this.props.showPriceFields ?
             
              <Form.Item
             label="Amount"
               style={{display: 'inline-block', width : 'calc(10%)'}}
             >

               </Form.Item>
           
               : null
          }
            
              
     

               <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>
             
         </Form.Item>  
        ) : null}

            
 

              </div>
          ;
          const formItems = itemKeys.map((k,index) => (
              <div>


  <Form.Item
              style={{marginBottom : '-10px'}}
                >

              

            
            <Form.Item
            style={{display: 'inline-block', width : 'calc(20%)'}}
            >
            {
                getFieldDecorator(`item[${k}].code`,{
                 rules : [{ validator : this.checkItem}]
                })(
                    <ItemSearch
                    {...this.props} />
                )
            }
            </Form.Item>
            <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
       {' '}
      </span>
            <Form.Item
            style={{display: 'inline-block', width : 'calc(10%)'}}
            >
            {
                getFieldDecorator(`item[${k}].quantity`,{
                    rules : [{ required : true, message : 'Add Quantity'}]
                })(
                    <InputNumber
                    min={0}
                    onBlur={(e) => this.handleBlur(e)}
                    {...this.props} />
                )
            }
            </Form.Item>
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>
      {
          this.props.showPriceFields ? 

   <Form.Item
             style={{display: 'inline-block', width : 'calc(10%)'}}
            >
              {
                  getFieldDecorator(`item[${k}].unitPrice`,{
                    initialValue : 0 || '',
                    rules : [{ required : this.props.showPriceFields, message : 'Add Unit Price'}]
                  })(
                      <InputNumber
                      min={0}
                      onBlur={(e) => this.handleBlur(e)}
                      placeholder="Unit Price"/>
                  )
              }
              </Form.Item>
          : null
      }

              <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
              {' '}
      </span>
      {
          this.props.showPriceFields ? 
 <Form.Item
                        style={{display: 'inline-block', width : 'calc(10%)'}}
                      >
                       {
                           getFieldDecorator(`item[${k}].unitID`,{
                            initialValue : 0 || '',
                            rules : [{ validator : this.checkUnit}]
                           })(
                               <UnitSelect  {...this.props}/>
                           )
                       }
            </Form.Item>

          : null
      }
                      
            <span style={{ display: 'inline-block', width: '5px', textAlign: 'center' }}>
            {' '}
      </span>

      {
          this.props.showPriceFields ? 

<Form.Item
               style={{display: 'inline-block', width : 'calc(10%)'}}
             >
             {
                 getFieldDecorator(`item[${k}].amount`,{
                    rules : [{ required : this.props.showPriceFields, message : 'Enter Amount'}]
                 })(
                     <InputNumber  placeholder="Amount"/>
                 )
             }
               </Form.Item>
          : null
      }
             
          
           
               <span style={{ display: 'inline-block', width: '10px', textAlign: 'center' }}>
              {' '}
      </span>
               {itemKeys.length > 1 ? (
          <Icon
            className="dynamic-delete-button"
            type="minus-circle-o"
            disabled={itemKeys.length === 1}
            onClick={() => this.renameItemRow(k)}
            style={{display: 'inline-block', width : '10px'}}
          />
        ) : null}

            
  </Form.Item>

    
              </div>
         
         ));

        return (
            <span>
                <Row>
                  <Form layout="vertical"
                  onSubmit={this.postDocument}>
                  {formLabel}
                  {formItems}
                  <Form.Item {...formItemLayoutWithOutLabel}>

                <Button type="dashed"
                  onClick={this.addItemRow} 
                 //style={{ width: '100%' }}
                 >
                    <Icon type="plus" /> Add Item Field
                </Button>
                
                </Form.Item>
               
                  </Form>
                 
                </Row>
            </span>
        )
    }
}

//export default Form.create()(BudgetTransactionDocument);