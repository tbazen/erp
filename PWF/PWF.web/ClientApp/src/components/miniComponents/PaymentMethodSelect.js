import React, { Component } from 'react';
import { TreeSelect, Tree, Button, Select } from 'antd';
import 'antd/dist/antd.min.css';
import { PaymentService } from '../../service/payment.service';

const { Option } = Select;
export default class PaymentMethodSelect extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        const value = props.value || '';
        this.state = {
            PaymentMethods: [],
            value: value,
        };


    }

    componentDidMount() {
        var self = this;
        var service = new PaymentService(this.props.session.token);
        console.log("Calling API ...");
        service.GetBizNetPaymentMethods().then(function (response) {
            console.log(response);
            if (response.data.status) {
                self.setState({
                    PaymentMethods : response.data.response
                })
            }
            if (!response.data.status) {
                console.log(response.error);
            }
        }).catch(function(error){
            console.log(error);
        })
    }

    onSelect = (value) => {
        if(!('value' in this.props)){
            this.setState({ value } );
        }
        this.triggerChange({value})
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
          onChange(Object.assign({}, this.state, changedValue));
        }
      }

    render() {

        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            style: {
                marginBotton: 0,
                paddingBottom : 0
            }
        }

        return (
          <span>
              <Select placeholder="Please select payment method .."
              style={{ width: 300 }}
              onSelect={this.onSelect}>
              {
                  this.state.PaymentMethods.map(function(item,index){
                      return  <Option value={index}
                      key={index}
                      
                      >
                {item}
                      </Option>
                  })
              }
              
             
              </Select>
          </span>
            
            )
        }
    }

