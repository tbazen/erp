import React, { Component } from 'react';
import { TreeSelect, Tree, Button, Select, Input, Row, Col } from 'antd';
import 'antd/dist/antd.min.css';
import { PaymentService } from '../../service/payment.service';

const { Option } = Select;
export default class PaymentSourceSelect extends Component {
    static getDerivedStateFromProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            return {
                ...(nextProps.value || {}),
            };
        }
        return null;
    }

    constructor(props) {
        super(props);
        const value = props.value || '';
        this.state = {
            BankAccounts: [],
            CashAccounts : [],
            bankTree : [],
            cashTree : [],
            treeData : [],
            value: value,
            amount : '',
        };
       
    }

    componentDidMount() {
        var self = this;
        var banktree = [];
        var cashtree = []
        var service = new PaymentService(this.props.session.token);
        console.log("Calling API ...");
        service.GetAllBankAccounts().then(function (response) {
           // console.log(response);
            if (response.data.status) {
                self.setState({
                    BankAccounts : response.data.response
                })
                response.data.response.map(function(item){
                    banktree.push({
                        title : item.bankBranchAccount,
                        key : `bank_${item.mainCsAccount}`,
                        value : `bank_${item.mainCsAccount}`
                    })
                });
            }
            if (!response.data.status) {
                console.log(response.error);
            }
        }).catch(function(error){
            console.log(error);
        }).then(function(){
            console.log(banktree);
            var bankTree = {
                title : "Bank Accounts",
                value : 'bank',
                key : 'bank',
                disabled : true,
                children : banktree
            }
            self.setState({
                bankTree : [...self.state.bankTree,bankTree]
            });               
            

        }).then(function(){
            self.setState({
                treeData : [...self.state.treeData,...self.state.bankTree]
            })
        });

        service.GetAllCashAccounts().then(function (response) {
           // console.log(response);
            if (response.data.status) {
                self.setState({
                    CashAccounts : response.data.response
                })
                response.data.response.map(function(item){
                    cashtree.push({
                        title : item.name,
                        key : `cash_${item.csAccountID}`,
                        value : `cash_${item.csAccountID}`
                    })
                });
            }
            if (!response.data.status) {
                console.log(response.error);
            }
        }).catch(function(error){
            console.log(error);
        }).then(function(){
            console.log(self.state);
            console.log(cashtree);
            if(self.state.CashAccounts.length > 0){
                var cashTree = {
                    title : "Cash Accounts",
                    value : `cash`,
                    key : 'cash',
                    disabled : true,
                    children : cashtree
                }
                self.setState({
                    cashTree : [...self.state.cashTree,cashTree]
                });
            }
        }).then(function(){
            self.setState({
                treeData : [...self.state.treeData,...self.state.cashTree]
            })
        });
    }

    onSelect = (value) => {
        console.log(value);
        if(!('value' in this.props)){
            this.setState({ value } );
        }
        this.triggerChange({value})
    }

    handleChange = (event) => {
        let target = event.target;
        this.setState({
            [target.name]: target.value
        });
        this.triggerChange({
            [target.name] : target.value
        });
        //console.log(target.value);
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        console.log(this.props);
        const onChange = this.props.onChange;
        if (onChange) {
          console.log(this.state);
          onChange(Object.assign({}, this.state, changedValue));
        }
      }


      
    render() {

        const filterTreeNode = (input,node) => {
            return node.props.title.toLowerCase().indexOf(input.toLowerCase()) >= 0
        }

        return (
          <span>
              <Row gutter={24}>
                  <Col span={14}>
                  <TreeSelect
       // style={{ width: 300 }}
        //value={this.state.value}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        treeData={this.state.treeData}
        showSearch={true}
        
        filterTreeNode={filterTreeNode}
        placeholder="Please select payment source"
        treeDefaultExpandAll={true}
        onChange={this.onSelect}
        required
        />
                  </Col>
                  <Col span={9} offset={1}>
                  <Input required placeholder={"Please enter amount"}
                  name="amount"
                  onChange={(e) => this.handleChange(e)} />
                  </Col>
              </Row>
              

        
          </span>
            
            )
        }


}