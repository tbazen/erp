import React from 'react';

import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select, Checkbox } from 'antd';
import 'antd/dist/antd.min.css';
import '../../assets/css/style.css';
import CostCenterSelect from '../miniComponents/CostCenterSelect';
import UnitSelect from '../miniComponents/UnitSelect';
const { Option } = Select;

class StoreSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            StoreList: []
        }

        this.onSelect = this.onSelect.bind(this);

    }

    componentDidMount() {
        var self = this;
        var service = new StoreService(this.props.session.token);
        service.GetAllStores().then(function (response) {
            self.setState({
                StoreList: response.data,
            })
            console.log(response.data);
        })
        .then(function(){
            console.log(self.state.StoreList[0].costCenterID);
            self.props.setStore(self.props.storeProp, self.state.StoreList[0].costCenterID);
        })
    }


    onSelect = (value, node, extra) => {
        this.props.setStore(this.props.storeProp, value);


    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {

            //labelCol: { span: 10 },
            //wrapperCol: { span: 14 },
            style: {
                marginBottom: 0,
                paddingBottom: 0
            }
        };
        return (

                   

                                   <div>

                                  
                    <Form>
                        <Form.Item label={""}
                        labelCol="10"

                    >
                        {
                            getFieldDecorator("StoreID", {
                                rules: [{
                                    required: true, message: 'Please select store'
                                }],

                               // initialValue: this.state.StoreList[0].costCenterID
                                // initialValue : price[index][0]
                            })(
                                <Select placeholder="Select Store.. "
                                    onSelect={this.onSelect}
                                    style={{ width: "100%" }}
                                //defaultValue={this.state.StoreList[0].costCenterID}
                                >
                                    {
                                        this.state.StoreList.map(function (item, index) {
                                            //return <Option value={865}>{item.description}</Option>

                                            return <Option value={Number(item.costCenterID)}>{item.description}</Option>

                                        })
                                    }
                                </Select>
                            )
                        }
                    </Form.Item>
                    </Form>
                        

            </div>









        )
    }
}

export default Form.create()(StoreSelect);