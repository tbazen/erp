import React, {Component } from 'react';
import {
    Icon, Button, Input, AutoComplete, Select
  } from 'antd';
  import { PaymentService } from '../../service/payment.service';
import { StoreService } from '../../service/store.service';
import swal from 'sweetalert';
  //const Option = AutoComplete.Option;
  
const { Option } = Select;

  
  export default class StoreSelect2 extends React.Component {

    static getDerivedStateFromProps(nextProps) {
      // Should be a controlled component.
      if ('value' in nextProps) {
          return {
              ...(nextProps.value || {}),
          };
      }
      return null;
  }
  
    constructor(props){
        super(props);
        this.state = {
            dataSource: [],
            value : '',
          }
        this.service = new StoreService("");
        this.onSelect = this.onSelect.bind(this);
    }
 
  
    componentDidMount(){
        var self = this;
        var service = new StoreService(self.props.session.token);
        service.GetAllStores().then(function (response) {
            self.setState({
                dataSource : response.data
            });
        }).catch(function(error){
            swal("Error",error.message,"warning");
        })
        
    }

    onSelect = (value, node, extra) => {
        console.log('selected', { value, node, extra });
        if (!('value' in this.props)) {
            this.setState({
                value: value
            })
        }

        this.triggerChange({ value })
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue));
        }
    }


    render(){
        return(
            <div>
                 <Select placeholder="Please select store .."
              showSearch={true}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}

            //  style={{ width: 500 }}
              onSelect={this.onSelect}>
              {
                  this.state.dataSource.map(function(item){
                      return  <Option value={item.costCenterID}
                      key={item.costCenterID}
                      
                      >
                {item.description}
                      </Option>
                  })
              }
              
             
              </Select>
            </div>
        )
    }
}