import React from 'react';

import { StoreService } from '../../service/store.service';
import { Tree, Input, Card, Row, Col, Table, Button, Drawer, Form, Select, DatePicker, Checkbox } from 'antd';

const { Option } = Select;

export default  class UnitSelect extends React.Component {
    constructor(props){
        super(props);
        const value = props.initialValue || '';
        console.log(value);
        this.state = {
            measureUnits : [],
            value : value
        };
        this.triggerChange({value})
    }

    componentDidMount(){
        var self = this;
        var service = new StoreService(self.props.session.token);
        service.GetAllMeasureUnit().then(function (response) {
            self.setState({
                measureUnits: response.data
            });
        })
    }

    onSelect = (value, node, extra) => {
        console.log('selected', { value, node, extra });
        if (!('value' in this.props)) {
            this.setState({
                value: value
            })
        }

        this.triggerChange({ value })
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue));
        }
    }


    render(){
        return(
            <div>
                <Select placeholder="Select Measure Unit ..." onSelect={this.onSelect}
                    dropdownClassName="unitSelect"
                    defaultValue={this.state.value}
                >
                    {

                        this.state.measureUnits.map(function (item) {
                            
                            return <Option value={item.id}>{item.name}</Option>
                        })
                    }
                </Select>
            </div>
        )
    }
}