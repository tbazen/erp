import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/intaps1.png';
import sygnet from '../../assets/img/brand/sygnet.svg';

import AuthService  from '../../service/auth.service';
import swal from 'sweetalert';

const propTypes = {
    children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            session :{}

        };
        this.Service = new AuthService();
        this.Logout = this.Logout.bind(this);
    }
    componentDidMount(){
        this.setState({
            session : this.props.session
        })
    }

    Logout() {
        this.props.auth.logout();
        this.props.history.push('/login');
        // this.Service.Logout().then(function (response) {
        //     if (response.data == true) {
        //         this.props.auth.logout();
        //         this.props.history.push('/login');
        //         window.localStorage.clear();
        //     }
        // }).catch(function (error) {
        //     swal(error.message);
        // });
    }


    render() {

        // eslint-disable-next-line
        const { children, ...attributes } = this.props;
        const logout = () => {
            
        }
        
        return (
            <React.Fragment>
                <AppSidebarToggler className="d-lg-none" display="md" mobile />
                <Nav className="d-md-down-none" navbar>
                    <NavItem className="px-3">
                        <Link to="/"> <AppNavbarBrand
                    full={{ src: logo, width: 89, height: 25, alt: 'Intaps Logo' }}
                    minimized={{ src: logo, width: 30, height: 30, alt: 'Intaps Logo' }}
                    
                /></Link>
                    </NavItem>
                </Nav>
                <AppSidebarToggler className="d-md-down-none" display="lg" />

                {/* <Nav className="d-md-down-none" navbar>
                    <NavItem className="px-3">
                        <NavLink href="/">Dashboard</NavLink>
                    </NavItem>
                </Nav> */}
                <Nav className="ml-auto" navbar>
                    {/* <NavItem className="d-md-down-none">
                        <NavLink href="#"><i className="icon-bell"></i><Badge pill color="danger">5</Badge></NavLink>
                    </NavItem> */}
                    <AppHeaderDropdown direction="down">
                        <DropdownToggle nav>
                            <p className="px-3">{this.state.session.username}</p>
                        </DropdownToggle>
                        <DropdownMenu right style={{ right: 'auto' }}>
                            <DropdownItem onClick={e => this.Logout(e)}><i className="fa fa-lock"></i> Logout</DropdownItem>
                        </DropdownMenu>
                    </AppHeaderDropdown>
                </Nav>
                {/* <AppAsideToggler className="d-md-down-none" /> */}
                {/*<AppAsideToggler className="d-lg-none" mobile />*/}
            </React.Fragment>
        );
    }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
