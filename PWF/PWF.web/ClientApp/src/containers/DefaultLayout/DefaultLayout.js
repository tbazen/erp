import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import { Button } from 'react-bootstrap';
import { Spin } from 'antd'; 
import {
    AppBreadcrumb,
    AppFooter,
    AppHeader,
    AppSidebar,
    AppSidebarFooter,
    AppSidebarForm,
    AppSidebarHeader,
    AppSidebarMinimizer,
    AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
//import navigation from '../_nav';
import { navigation } from '../_nav';
// routes config
import routes from '../routes';
import DefaultHeader from './DefaultHeader';
import DefaultFooter from './DefaultFooter';
import Counter from '../../components/Counter';
import Home from '../../components/Home';
import Authorize from '../../Authorize';

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}

// const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
// const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            navigation : []
        }
    }

    componentWillMount(){
        this.props.loader.stop();
    }


    signOut(e) {
        e.preventDefault()
        this.props.history.push('/login')
    }




    loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

    render() {
        const navs = this.props.authenticated ? navigation[this.props.session.role] : [];
        return (

            <div className="app">
                <AppHeader fixed>       
                    <Suspense  fallback={this.loading()}>
                    <DefaultHeader {...this.props}  />
                  </Suspense>
                </AppHeader>
                <div className="app-body">
                    <AppSidebar fixed display="lg">
                        <AppSidebarHeader />
                        <AppSidebarForm />
                        <Suspense>
                            <AppSidebarNav navConfig={this.props.session ? navigation[this.props.session.role] : navigation[1]} {...this.props} /> 
                            </Suspense>
                           
                   
                        <AppSidebarFooter />
                        <AppSidebarMinimizer />
                    </AppSidebar>
                    <main className="main">
                        <AppBreadcrumb appRoutes={routes} />
                        <Spin
                        spinning={this.props.loading}>
                       
                        <Container fluid>   
                                <Suspense fallback={this.loading}>
                                <Switch>
                                    {routes.map((route, idx) => {
                                    return <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                     
                                        render = {
                                            (props) => (<route.components {...this.props} {...props} />)
                                        }
                                    />
                                    })}
                                </Switch>
                            </Suspense>    
                        </Container>
                        </Spin>
                    </main>
                </div>
                <AppFooter>
                         <Suspense fallback={this.loading()}>
                            <DefaultFooter />
                        </Suspense>
                </AppFooter>

            </div>
        );
    }
}

export default Authorize(DefaultLayout);
