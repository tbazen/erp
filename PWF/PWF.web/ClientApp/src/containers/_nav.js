const employeeNav =  {
    items: [
        {
            name : 'Dashboard',
            url : '/',
            icon : 'icon-speedometer'
    },
    {
        name : 'Store Request',
        url : '/store',
        icon : 'icon-layers'
    },{

        name : "Payment Reqeust",
        url : '/payment',
        icon : 'icon-chart'
    }
    
   
    ]
}

const financeOfficerNav =  {
    items: [
        {
            name : 'Dashboard',
            url : '/',
            icon : 'icon-speedometer'
    },
    {

        name : "Payment Reqeust",
        url : '/payment',
        icon : 'icon-chart'
    }  ,
    {
        name : "Search Workflow",
        url : "/wf/search",
        icon : 'icon-book-open'
    },
    {
        name : "Search Documents",
        url : '/SearchDocument',
        icon : 'icon-drop'
    }
    ]
}

const keeperNav = {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Store Request',
            url: '/store',
            icon : 'icon-layers'
        },
        {
            name : 'Store Deliveries',
            url: '/deliveries',
            icon : 'icon-list',
        },
        {
            name : " Item Browser",
            url : '/item',
            icon : 'icon-drop',
        },
        {
            name : "Purchasing",
            icon: 'icon-wallet',
            url : "/purchase",
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        },
    ]
}

const managerNav = {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Store Request',
            url: '/store',
            icon : 'icon-layers'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        },
        {
            name : "Search Documents",
            url : '/SearchDocument',
            icon : 'icon-drop'
        }
    ]
}

const FinManagerNav =  {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Payment',
            url: '/payment',
            icon : 'icon-chart'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        },
        {
            name : "Search Documents",
            url : '/SearchDocument',
            icon : 'icon-drop'
        }
    ]
}

const OwnerNav =  {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Payment',
            url: '/payment',
            icon : 'icon-chart'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        },
        {
            name : "Search Documents",
            url : '/SearchDocument',
            icon : 'icon-drop'
        }
    ]
}


const CashierNav =  {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Payment',
            url: '/payment',
            icon : 'icon-chart'
        },
        {
            name : "Search Documents",
            url : '/SearchDocument',
            icon : 'icon-drop'
        }
    ]
}

const FinalizerNav =  {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Payment',
            url: '/payment',
            icon : 'icon-chart'
        },
        {
            name : "Search Documents",
            url : '/SearchDocument',
            icon : 'icon-drop'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        }
    ]
}

const Purchaser1Nav = {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Purchasing',
            url: '/purchase',
            icon: 'icon-wallet'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        }
    ]
}

const Purchaser2Nav = {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Purchasing',
            url: '/purchase',
            icon: 'icon-wallet'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        }
    ]
}

const Purchaser3Nav = {
    items: [
        {
            name: 'Dashboard',
            url: '/',
            icon: 'icon-speedometer'
        },
        {
            name: 'Purchasing',
            url: '/purchase',
            icon: 'icon-wallet'
        },{
            name : "Purchase Order",
            url : '/purchaseOrder',
            icon : "icon-drop"
        },
        {
            name : "Search Documents",
            url : '/SearchDocument',
            icon : 'icon-drop'
        },
        {
            name : "Search Workflow",
            url : "/wf/search",
            icon : 'icon-book-open'
        }
    ]
}


const navigation = [null, employeeNav, keeperNav, managerNav, 
    FinManagerNav,OwnerNav, CashierNav, FinalizerNav
    , Purchaser1Nav, Purchaser2Nav, Purchaser3Nav , financeOfficerNav  ];

export { navigation };