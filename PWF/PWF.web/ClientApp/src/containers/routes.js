import DefaultLayout from './DefaultLayout/DefaultLayout';
import {Form} from 'antd';
import React from 'react';
import { Guard } from '../Guard';


const Home = React.lazy(() => import('../components/Home'))
const Counter = React.lazy(() => import('../components/Counter'));
const AddRequest = React.lazy(() => import('../components/Store/Request'));
const ViewRequest = React.lazy(() => import('../components/Store/View'));
const Reorganize = React.lazy(() => import('../components/Store/Reorganize'));
const Review = React.lazy(() => import('../components/Store/Review'));


const item = React.lazy(() => import('../components/Item/ItemBrowser'));
const issue = React.lazy(() => import('../components/Store/Issue'));
const tdItem = React.lazy(() => import('../components/Item/TransactionDocumentItemForm'));
const demo = React.lazy(() => import('../components/miniComponents/ItemRequestForm2'));
const viewPayment = React.lazy(() => import('../components/Payment/View'));
const AddPaymentRequest = React.lazy(() => import('../components/Payment/AddRequest'));
const Check = React.lazy(() => import('../components/Payment/Check'));
const Approve = React.lazy(() => import('../components/Payment/Approve'));
const Execute = React.lazy(() => import('../components/Payment/Execute'));
const Finalize  = React.lazy(() => import('../components/Payment/Finalize'));
const SearchDocuments = React.lazy(() => import('../components/AccountDocument/SearchAccountDocument'));
const Purchasing = React.lazy(() => import('../components/Purchase/View'));
const AddPurchaseRequest = React.lazy(() => import('../components/Purchase/Request'));
const ReviewRequest = React.lazy(() => import('../components/Purchase/ReviewRequest'));
const AddQuotation = React.lazy(() => import('../components/Purchase/AddQuotation'));
const Order = React.lazy(() => import('../components/Purchase/Order'));
const ViewOrders =React.lazy(() => import('../components/Purchase/PurchaseOrder/View'));
const ReviewOrder = React.lazy(() => import('../components/Purchase/PurchaseOrder/ReviewOrder'));
const ViewPurchaseRequest = React.lazy(() => import('../components/Purchase/Viewer/PurchaseRequestViewer'));
const resubmitPurchaseRequest  = React.lazy(() => import('../components/Purchase/Resubmit'));
const ViewPurchaseOrder = React.lazy(() => import('../components/Purchase/Viewer/PurchaseOrderViewer'));
const ViewDeliveries = React.lazy(() => import('../components/Delivery/View'));
const AddDeliveries = React.lazy(() => import('../components/Delivery/AddDeliveries'));
const SubmitVoucher = React.lazy(() => import('../components/Delivery/SubmitVoucher'));
const AccountDocumentViewer = React.lazy(() => import('../components/Document/AccountDocumentViewer'));
const CertifyService = React.lazy(() => import('../components/Delivery/Certify'));
const FinalizePurchase = React.lazy(() => import('../components/Purchase/Finalize'));
const StoreRequestWFViewer = React.lazy(() => import('../components/WorkflowViewer/StoreRequestWF'));

const SearchWF = React.lazy(() => import('../components/WorkflowViewer/SearchWF'));
const ViewWF = React.lazy(() => import('../components/WorkflowViewer/View'));
const Forbidden = React.lazy(() => import('../components/Forbidden'));

const routes = [

    { path: '/', exact: true, name: 'Home', components: Home },
    { path: '/forbidden', name : "Forbidden", components : Forbidden },

    { path: '/store', exact: true, name: 'Store Request', components: Guard(ViewRequest,[1,2,3,5]) },
    { path: '/store/request', name: "Add Store Request", components: Guard(AddRequest,[1]) },
    { path: "/store/reorganize/:id", name: "Reorganize Request", components: Guard(Reorganize,[2]) },
    { path: "/store/review/:id", name : "Review Request", components : Guard(Review,[3,5]) },
    { path: "/store/resubmit/:id", name: "Resubmit Request", components : Guard(AddRequest,[1]) },
    { path : "/store/issue/:id", name : "Issue Items", components: Guard(issue,[2]) },

    { path : "/item", name : "Item Browser", components : item},

    { path : "/payment", exact:true, name : "Payment", components : Guard(viewPayment,[1,4,5,6,7,11]) },
    { path : "/payment/request", name : "New Payment Request", components : Guard(AddPaymentRequest,[1,11]) },
    { path : "/payment/resubmit/:id", name: "Resubmit Request", components : Guard(AddPaymentRequest,[1,11]) },
    { path:  "/payment/check/:id", name : "Check Payment", components : Guard(Check,[4]) },
    { path: "/payment/review/:id", name : "Review Payment", components : Guard(Approve,[5]) },
    { path: "/payment/execute/:id", name: "Execute Payment", components : Guard(Execute,[6]) },
    { path : "/payment/finalize/:id", name : "Finalize Payment", components : Guard(Finalize,[7]) },
    { path: "/SearchDocument", name:"Search Account Document", components: SearchDocuments },

    { path: "/WF/Search", name : "Search Workflow", components : SearchWF},
    { path: "/WF/View/:type/:id", name: " View Workflow", components : ViewWF },

    { path: "/ViewAccountDocument/:id", name: "View Account Document", components: AccountDocumentViewer },

    { path: "/purchase", exact:true, name:"Purchase Request", components: Guard(Purchasing,[2,8,9,10]) },
    { path: "/purchase/request", name:"Add Purchase Request", components : Guard(AddPurchaseRequest,[2,8]) },
    { path: "/purchase/reviewReq/:id", name:"Review Purchase Request", components : Guard(ReviewRequest,[10]) },
    { path: "/purchase/resubmit/:id", name: "Resubmit Purchase Request", components : Guard(resubmitPurchaseRequest,[2,8]) },
    { path: "/purchase/process/:id", name:"Add Quotations", components : Guard(AddQuotation,[8]) },
    { path: "/purchase/order/:id", name:"Purchase Order", components: Guard(Order,[9]) },
    { path: "/purchaseOrder",exact:true, name:"Purchase Orders", components: Guard(ViewOrders,[10])},
    { path: "/purchaseOrder/review/:id", name: "Review Purchase Order", components: Guard(ReviewOrder,[10])},
    { path: "/purchase/view/request/:id", name: "View Purchase Request", components : ViewPurchaseRequest},
    { path: "/purchase/view/order/:id", name : "View Purchase Order", components : ViewPurchaseOrder},
    { path: "/purchase/finalize/:id", name : "Finalize Purchase", components : Guard(FinalizePurchase,[10]) },

    { path: "/deliveries", exact:true,  name: "Store Deliveries", components : Guard(ViewDeliveries,[2]) },
    { path: "/deliveries/add/:id", name : "Add Deliveries", components : Guard(AddDeliveries,[2]) },
    { path: "/deliveries/voucher/:id", name: "Upload Voucher", components : Guard(SubmitVoucher,[2]) },

    { path: "/certify/:id", name: "Certify Service", components : CertifyService },
];

export default routes;