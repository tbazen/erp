const StoreStates = [
    {
        id : 0,
        name : "Filing"
    },
    {
        id : 1,
        name : "Reorganizing"
    },
    {
        id : 2,
        name : "Reviewing"
    }, 
    {
        id : 3,
        name : "Issuance"
    }, 
    {
        id : -1,
        name : "Closed"
    },
    {
        id : -2,
        name : "Cancelled"
    }
];

const PaymentStates = [
    {
        id : 0,
        name : "Filing"
    },
    {
        id: 1,
        name : "Checking",
    },
    {
        id : 2,
        name : "Reviewing"
    },
    {
        id : 3,
        name : "Payment",
    },
    {
        id : 4,
        name : "Final Check"
    },
    {
        id : -1,
        name : "Closed"
    },
    {
        id : -2,
        name : "Cancelled"
    }, {
        id : -3,
        name : "Aborted"
    }
];

const PurchaseState = [
    {
        id : 0,
        name : "Filing"
    },{
        id : 1,
        name : "Reviewing"
    },{
        id : 2,
        name : "Gathering Quotation"
    },{
        id : 3,
        name : "Order"
    },{
        id : 4,
        name : "Finalization",
    },{
        id : -1,
        name : "Closed"
    },{
        id : -2,
        name : "Cancelled"
    }
    
]

const OrderState = [
    {
        id : 0,
        name : "Filing",
    },{
        id : 1,
        name : "Reviewing"
    },{
        id : 2,
        name : "Process"
    },{
        id : -1,
        name : "Closed",
    },{
        id : -2,
        name : "Terminated",
    },{
        id: -3,
        name : "Cancelled"
    }
]

const SRW = 1;
const PW = 2;
const PRW = 1003;
const POW = 1002;

const Workflows = [
    {
        id : SRW,
        name : "Store Request Workflow",
    },
    {
        id : PW,
        name : "Payment Workflow"
    },
    {
        id : PRW,
        name : "Purchase Workflow"
    }, {
        id : POW,
        name : "Purchase Order Workflow"
    }
]

export {StoreStates, PaymentStates, PurchaseState, OrderState, Workflows, SRW, PW, PRW, POW };