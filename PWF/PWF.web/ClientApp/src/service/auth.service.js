import Axios from 'axios';
import { reducer } from '../store/WeatherForecasts';
import { baseUrl } from './urlConfig';

export default class AuthService {


    constructor() {
        this.config = {
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        }
    }

    CheckAuth(username, password){
        var Model = {
            username : username, 
            password : password
        };
        console.log(Model);
        var path =  `/Auth/CheckAuthentication`;
        return Axios.post(baseUrl + path,Model,this.config);
    }

    Logout(){
        return Axios.post(baseUrl + '/Auth/Logout', {},this.config);

    }

    Login(username,password,role){
        var Model = {
            username : username, password : password, role: role
        };
        var path =  `/Auth/Login`;
        return Axios.post(baseUrl + path,Model,this.config);
    }

    

  

}