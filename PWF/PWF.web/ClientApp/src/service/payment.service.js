﻿ import axios from 'axios';
import { reducer } from '../store/WeatherForecasts';
import { baseUrl } from './urlConfig';
import Axios from 'axios';
import { read } from 'fs';
import swal from 'sweetalert';
//mport axios from 'react-axios';

export class PaymentService {


    constructor(tok) {
        this.token = tok;
        this.config = {
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        }
        // this.ax = axios.create({
        //     withCredentials : true,
        //     headers : {
        //         // 'Authorization': 'bearer ' + this.token,
        //         'Content-Type': 'application/json',
        //         'Accept': 'application/json'
        //     }
        // });
        this.axios = axios.create({
            baseURL : baseUrl,
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
            
        })
        this.axios.interceptors.response.use((response) => (response.data), (error) => {
            console.log(error.response.data);
            swal("Error",error.response.data,"warning");
        })
    }

    CreatePaymentRequest(data) {
        const path = baseUrl + `/Payment/CreatePaymentRequest`;
        return axios.post(path, data, this.config);
    }

    ResubmitPaymentRequest(wfid, data){
        const path = baseUrl + `/Payment/ResubmitPaymentRequest/${wfid}`;
        return axios.post(path,data,this.config);
    }

    FinHeadReject(wfid, description) {
        const path = baseUrl + `/Payment/FinHeadReject/${wfid}?description=${description}`;
        return axios.get(path, this.config);
    }

    CheckPaymentRequset(wfid, description, data) {
        const path = baseUrl + `/Payment/CheckPaymentRequest/${wfid}/${description}`;
        return axios.post(path,data,this.config);
    }

    ApprovePaymentRequest(wfid, description) {
        const path = baseUrl + `/Payment/ApprovePaymentRequest/${wfid}?description=${description}`;
        return axios.get(path, this.config);
    }

    OwnerRejectRequest(wfid, description) {
        const path = baseUrl + `/Payment/OwnerRejectRequest/${wfid}?description=${description}`;
        return axios.get(path, this.config);
    }

    CancelPaymentRequest(wfid, description) {
        const path = baseUrl + `/Payment/CancelPaymentRequest/${wfid}?description=${description}`;
        return axios.get(path, this.config);
    }

    ExecutePaymentRequest(wfid, description) {
        const path = baseUrl + `/Payment/ExecutePaymentRequest/${wfid}?description=${description}`;
        return axios.get(path, this.config);
    }

    AddPaymentDocument(wfid, data) {
        const path = baseUrl + `/Payment/AddPaymentDocument/${wfid}`;
        return axios.post(path, data, this.config);
    }

    GetPaymentTypes(){
        const path = baseUrl + `/Payment/GetPaymentTypes`;
        return axios.get(path,this.config);
    }

    GetPaymentRequsetWorkflows(){
        const path = baseUrl +  `/Payment/GetPaymentRequestWorkflows`;
        return axios.get(path,this.config);
    }

    GetLastPaymentWorkitem(wfid){
        const path = baseUrl + `/Payment/GetLastPaymentWorkItem?guid=${wfid}`;
        axios.create({ withCredentials: true, })
        return axios.get(path,this.config);
    }


    GetAllBankAccounts() {
        const path = baseUrl + `/Payment/GetAllBankAccounts`;
        return axios.get(path, this.config);

    }

    GetAllCashAccounts() {
        const path = baseUrl + `/Payment/GetAllCashAccounts`;
        return axios.get(path, this.config);
    }

    GetAllTaxCenters() {
        const path = baseUrl + `/Payment/GetAllTaxCenters`;
        return axios.get(path, this.config);
    }

    GetAllCustomers() {
        const path = baseUrl + `/Payment/GetAllCustomers`;
        return axios.get(path, this.config);
    }

    GetPaymentInstrumentTypes() {
        const path = baseUrl + `/Payment/GetPaymentInstrumentTypes`;
        return axios.get(path, this.config);
    }

    GetBizNetPaymentMethods(){
        const path = baseUrl + `/Payment/GetBIZNETPaymentMethods`;
        return axios.get(path,this.config);
    }

    PostBankWithdrawalDocument(wfid,data){
        const path = baseUrl + `/Payment/PostBankWithdrawalDocument/${wfid}`;
        return axios.post(path,data,this.config);
    }

    PostLabourPaymentDocument(wfid, data){
        const path = baseUrl + `/Payment/PostLabourPaymentDocument/${wfid}`;
        return axios.post(path,data,this.config);
    }

    PostCashierToCashierDocument(wfid,data){
        const path = baseUrl + `/Payment/PostCashierToCashierDocument/${wfid}`;
        return axios.post(path,data,this.config);
    }

    GetAccountDocumentHTML(docID) {
        const path = baseUrl + `/Payment/GetAccountDocumentHTML/${docID}`;
        return axios.get(path, this.config);
    }

    GetAccountDocumentTransactionAmount(docID)
    {
        const path = baseUrl + `/Payment/GetDocumentTransactionAmount/${docID}`;
        return axios.get(path, this.config);
    }

    GetAllDocumentTypes()
    {
        const path = baseUrl + `/Payment/GetAllDocumentTypes`;
        return axios.get(path, this.config);
    }

    SearchDocuments(data){
        const path = baseUrl + `/Payment/SearchDocuments`;
        return axios.post(path,data,this.config);
    }

    AddDocumentFromBrowser(wfid,docID){
        const path = baseUrl + `/Payment/AddPostedDocument/${wfid}?docID=${docID}`;
        return axios.get(path,this.config);
    }

    RemoveDocuments(wfid,docID){
        const path = baseUrl + `/Payment/RemoveDocuments/${wfid}?docID=${docID}`;
        return axios.get(path,this.config);
    }

    GetAllCustomers(){
        const path = baseUrl  + `/Payment/GetAllCustomers`;
        return axios.get(path,this.config);
    }

    GetAllEmployees(){
        const path = baseUrl  + `/Payment/GetAllEmployees`;
        return axios.get(path,this.config);
    }

    GetSerialType(typeID){
        const path = baseUrl + `/Payment/GetSerialType/${typeID}`;
        return axios.get(path,this.config);
    }

    GetTransactionAmount(wfid,docID){
        const path = baseUrl + `/Payment/GetTransactionAmount/${wfid}?docID=${docID}`;
        return axios.get(path,this.config);
    }

    GetTransactionAmounts(docID){
        const path = baseUrl + `/Payment/GetTransactionAmounts/${docID}`;
        return axios.get(path,this.config);
    }

    GetNetTransactionAmount(docID){
        const path = baseUrl + `/Payment/GetNetTransactionAmount/${docID}`;
        return axios.get(path,this.config);
    }

    CheckTransactionAmountForDocument(wfid,docID){
        const path = baseUrl + `/Payment/CheckTransactionAmount/${wfid}?docID=${docID}`;
        return axios.get(path,this.config);
    }

    CheckTransactionAmountForAmount(wfid,amount){
        const path = baseUrl + `/Payment/CheckTransactionAmount/${wfid}?amount=${amount}`;
        return axios.get(path,this.config);

    }

    GetAccountName(id){
        const path = baseUrl + `/Payment/GetAccountName/${id}`;
        return axios.get(path,this.config);
    }

    GetCostCenterName(id){
        const path = baseUrl + `/Payment/GetCostCenterName/${id}`;
        return axios.get(path,this.config);
    }

    AddDocumentAttachment(id,doc){
        console.log(doc);
        const path = baseUrl + `/Payment/AddDocumentAttachment/${id}`;
        return axios.post(path,doc,this.config);
    }

    RemoveAttachedDocument(wfid, docID){
        const path = baseUrl + `/Payment/RemoveAttachedDocument/${wfid}/${docID}`;
        return Axios.get(path,this.config);
    }

    UploadTest(doc){
        console.log(doc);
        const path = baseUrl + `/Payment/CreatePaymentRequest`;
        return axios.post(path,doc,this.config);
    }
    
    GetPaymentWFHistory(wfid){
        const path = baseUrl + `/Payment/GetPaymentWorkflowHistory/${wfid}`;
        return axios.get(path,this.config);
    }

    GetWFHistory(wfid,type){
        const path = baseUrl + `/Payment/GetWorkflowHistory/${wfid}/${type}`;
        return axios.get(path,this.config);
    }

    CloseRequest(wfid, Doc){
        const path = baseUrl + `/Payment/CloseRequest/${wfid}`;
        return axios.post(path,Doc,this.config);
    }
    
    FinalizeRequest(wfid,description){
        const path =baseUrl +  `/Payment/FinalizeRequest/${wfid}?description=${description}`;
        return Axios.get(path,this.config);
    }

    RejectFinalCheck(wfid,description){
        const path =baseUrl + `/Payment/RejectFinalCheck/${wfid}?description=${description}`;
        return axios.get(path,this.config);
    }

    GetPaymentRequestSigners(wfid){
        const path = baseUrl + `/Payment/GetPaymentRequestSigners/${wfid}`;
        return axios.get(path, this.config);
    }

    SearchAccount(query) {
        const path = baseUrl + `/Payment/SearchAccount?query=${query}`;
        return axios.get(path, this.config);
    }

    SearchCostCenter(query) {
        const path = baseUrl + `/Payment/SearchCostCenter?query=${query}`;
        return axios.get(path, this.config);
    }

    GetCurrentEmployee(){
        const path = baseUrl + "/Purchase/GetCurrentEmployee";
        return axios.get(path,this.config);
    }

    GetTotalPaidAmount(wfid){
        const path = baseUrl + `/Payment/GetTotalPaidAmount/${wfid}`;
        return axios.get(path,this.config);
    }

    GetTotalPaidAmountFromSource(wfid){
        const path = baseUrl + `/Payment/GetTotalPaidAmountFromSource/${wfid}`;
        return axios.get(path,this.config);
    }
}