import axios from 'axios';
import { reducer } from '../store/WeatherForecasts';
import { baseUrl } from './urlConfig';
import Axios from 'axios';
import { read } from 'fs';
import AccountSearch from '../components/miniComponents/AccountSearch';
//mport axios from 'react-axios';

export class PurchaseService {


    constructor() {
        this.config = {
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        }

    }

    GetPurchaseWorkflows(){
        const path = baseUrl + `/Purchase/GetUserPurchaseWorkflow`;
        return axios.get(path,this.config);
    }

    GetPurchaseOrderWorkflows(){
        const path = baseUrl + `/Purchase/GetUserPurchaseOrderWorkflow`;
        return axios.get(path,this.config);
    }

    CreatePurchaseWorkflow(data){
        const path = baseUrl + `/Purchase/CreatePurchaseWorkflow`;
        return axios.post(path,data,this.config);
    }

    ResubmitPurchaseRequest(wfid, data){
        const path = baseUrl + `/Purchase/ResubmitPurchaseRequest/${wfid}`;
        return axios.post(path,data,this.config);
    }

    CancelPurchaseRequest(wfid){
        const path = baseUrl + `/Purchase/CancelPurchaseRequest/${wfid}`;
        return axios.get(path,this.config);
    }

    GetLastPurchaseWorkitem(wfid){
        const path = baseUrl + `/Purchase/GetLastPurchaseWorkItem?guid=${wfid}`;
        return axios.get(path,this.config);
    }

    GetItemName(code){
        const path = baseUrl + `/Purchase/GetItemName?code=${code}`;
        return axios.get(path,this.config);
    }

    ApprovePurchaseRequest(wfid,description){
        const path = baseUrl + `/Purchase/ApprovePurchaseRequest?wfid=${wfid}&description=${description}`;
        return axios.get(path,this.config);
    }

    RejectPurchaseRequest(wfid,description){
        const path = baseUrl + `/Purchase/RejectPurchaseRequest?wfid=${wfid}&description=${description}`;
        return axios.get(path,this.config);
    }

    AddPriceQuotation(wfid,quotation){
        const path = baseUrl + `/Purchase/AddPriceQuotation/${wfid}`;
        return axios.post(path,quotation,this.config);
    }

    GetSupplierName(code){
        const path = baseUrl + `/Purchase/GetSupplierName?code=${code}`;
        return axios.get(path,this.config);
    }

    SubmitPriceQuotation(wfid,description){
        const path = baseUrl + `/Purchase/SubmitQuotation/${wfid}?description=${description}`;
        return axios.get(path,this.config);
    }

    IssuePurchaseOrder(wfid,order){
        const path = baseUrl + `/Purchase/IssuePurchaseOrder/${wfid}`;
        return axios.post(path,order,this.config);
    }

    ResubmitPurchaseOrder(wfid,order){
        const path = baseUrl + `/Purchase/ResubmitPurchaseOrder/${wfid}`;
        return axios.post(path,order,this.config);
    }

    CancelPurchaseOrder(parent,child){
        const path = baseUrl + `/Purchase/CancelPurchaseOrder/${parent}/${child}`;
        return axios.get(path,this.config);
    }

    GetLastPurchaseOrderWorkitem(wfid){
        const path = baseUrl + `/Purchase/GetLastPurchaseOrderWorkItem/${wfid}`;
        return axios.get(path,this.config);
    }

    GetPurchaeOrderWorkitemsOf(wfid){
        const path = baseUrl + `/Purchase/GetPurchaseOrderWorkitemsOf/${wfid}`;
        return axios.get(path,this.config);
    }
   
    GetAllowedOrderQuantity(wfid){
        const path = baseUrl + `/Purchase/GetAllowedOrderQuantity/${wfid}`;
        return axios.get(path,this.config);
    }

    ApprovePurchaseOrder(wfid,description){
        const path = baseUrl + `/Purchase/ApprovePurchaseOrder/${wfid}?description=${description}`;
        return axios.get(path, this.config);
    }

    RejectPurchaseOrder(wfid,description){
        const path = baseUrl + `/Purchase/RejectPurchaseOrder/${wfid}?description=${description}`;
        return axios.get(path,this.config);
    }

    RequestPurchasePayment(wfid,data){
        const path = baseUrl + `/Purchase/RequestPurchasePayment/${wfid}`;
        return axios.post(path,data,this.config);
    }

    RequestItemDelivery(wfid,data){
        const path = baseUrl + `/Purchase/RequestItemDelivery/${wfid}`;
        return axios.post(path,data,this.config);
    }    
    
    RequestServiceDelivery(wfid,data){
        const path = baseUrl + `/Purchase/RequestServiceDelivery/${wfid}`;
        return axios.post(path,data,this.config);
    }

    GetStoreDeliveryWorkflows(){
        const path = baseUrl + `/Purchase/GetUserItemDeliveryrWorkflows`;
        return axios.get(path,this.config);
    }


    GetItemDeliveryWorkitem(wfid){
        const path = baseUrl + `/Purchase/GetItemDeliveryWorkItem/${wfid}`;
        return axios.get(path,this.config);
    }

    GetServiceDeliveryWorkItem(wfid){
        const path = baseUrl + `/Purchase/GetServiceDeliveryWorkItem/${wfid}`;
        return axios.get(path,this.config);
    }

    GetUserServiceDeliveryWorkflows(){
        const path = baseUrl + `/Purchase/GetUserServiceDeliveryWorkflows`;
        return axios.get(path,this.config);
    }

    ClosePurchaseOrder(wfid,description){
        const path = baseUrl + `/Purchase/ClosePurchaseOrder/${wfid}?description=${description}`;
        return axios.get(path,this.config);
    }

    CanCloseRequest(wfid){
        const path = baseUrl + `/Purchase/CanCloseRequest/${wfid}`;
        return axios.get(path,this.config);
    }

    CloseRequest(wfid,description){
        const path = baseUrl + `/Purchase/CloseRequest/${wfid}?description=${description}`;
        return axios.get(path,this.config);
    }


    FinalizeRequest(wfid,description){
        const path = baseUrl + `/Purchase/FinalizePurchaseRequest/${wfid}?description=${description}`;
        return axios.get(path,this.config);
    }

    GetUncertifiedService(wfid){
        const path = baseUrl + `/Purchase/GetUncertifiedService/${wfid}`;
        return axios.get(path,this.config);
    }


}