import Axios from 'axios';
import { reducer } from '../store/WeatherForecasts';
import { baseUrl } from './urlConfig'
import { createBrowserHistory } from 'history';
import configureStore from '../store/configureStore';

const base = document.getElementsByTagName('base')[0].baseURI;
const history = createBrowserHistory({ basename: base });

// Get the application-wide store instance, prepopulating with state from the server where available.
const initialState = window.initialReduxState;
const { store , persistor } = configureStore(history, initialState);

export class StoreService {

    constructor(tok) {
        this.token = tok;
        this.config = {
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,

                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        }

        this.axios = Axios.create({
            baseURL : baseUrl,
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
            
        })
        this.axios.interceptors.response.use((response) => (response), (error) => {
            if(error.response){
                if(error.response.status == 401){
                    console.log("401 Caught");
                    store.dispatch({type : "LOGOUT"});
                    console.log(history);
                    window.open(`${base}#/login`);
                    // console.log(base);
                    // history.push('login');
                }
                else{
                    throw error;
                }
            }
        })
        
    }

    

    Who() {
        return Axios.get(baseUrl +'/Auth/Who', {
            withCredentials: true,
            
            headers : {
                "Content-Type": "application/x-www-form-urlencoded",
                "Cache-Control": "no-cache",
            }
        });
    }

    SearchItems(searchTerm){
        return Axios.get(baseUrl +'/Store/SearchItem?term=' + searchTerm, this.config);
    }

    SearchStoreItems(searchTerm){
        return Axios.get(baseUrl +'/Store/SearchStoreItems?term=' + searchTerm, this.config);
    }

    FileRequest(model){
        return Axios.post(baseUrl +'/Store/FileRequest',model,this.config);
    }

    StoreRequestWorkflows(){
        return Axios.get(baseUrl +'/Store/GetStoreRequestWorkflows',this.config);
    }

    SetRole() {
        return Axios.get(baseUrl +'/Auth/SetRole', this.config);
    }

    GetLastWorkItem(guid){
        return Axios.get(baseUrl +'/Store/GetLastWorkitem?guid='+guid,this.config);
    }

    ReorganizeRequest(guid,model){
        return Axios.post(baseUrl +`/Store/ReorganizeRequest/${guid}`,model,this.config);
    }

    GetAccountBalance(itemCode,costCenterID){
        return Axios.get(baseUrl +`/Store/GetItemAccountBalance?itemCode=${itemCode}&costCenterID=${costCenterID}`,this.config);
    }

    GetItemCategories(){
        return Axios.get(baseUrl +'/Store/GetItemCategories',this.config);
    }

    GetItemsInCategory(catID){
        return Axios.get(baseUrl +'/Store/GetItemsInCategory?catID='+catID,this.config);
    }

    SubmitRequestReview(wfid,description,status,items){
        const path = baseUrl +`/Store/SubmitRequestReview/${wfid}/${description}/${status}`;
        return Axios.post(path,items,this.config);
    }

    ResubmitStoreRequest(wfid,model){
        const path = baseUrl + `/Store/ResubmitRequest/${wfid}`;
        return Axios.post(path,model,this.config);
    }

    CancelStoreRequest(wfid,description){
        const path = baseUrl +`/Store/CancelStoreRequest/${wfid}?description=${description}`;
        return Axios.get(path,this.config);
    }

    RegisterItem(item){
        const path = baseUrl +`/Store/RegisterItem`;
        return Axios.post(path,item,this.config);
    }

    CancelReorganization(wfid,description){
        const path = baseUrl +`/Store/CancelReorganization?wfid=${wfid}&description=${description}`;
        return Axios.get(path,this.config);
    }

    RegisterItemCategory(category){
        const path = baseUrl +`/Store/RegisterItemCategory`;
        return Axios.post(path,category,this.config);
    }

    GetAllStores(){
        const path = baseUrl + `/Store/GetAllStores`;
        return Axios.get(path,this.config);
    }

    GetAllCostCenter(parentID){
        const path = baseUrl +`/Store/GetAllCostCenter?parentID=${parentID}`;
        return Axios.get(path,this.config);
    }

    GetChildAccounts(parentID){
        const path = baseUrl +`/Store/GetChildAccounts?parentID=${parentID}`;
        return Axios.get(path,this.config);
    }

    GetAllMeasureUnit(){
        const path = baseUrl + `/Store/GetAllMeasureUnit`;
        return Axios.get(path,this.config);
    }

    GetServiceTypes(){
        const path = baseUrl + `/Store/GetServiceTypes`;
        return Axios.get(path,this.config);
    }

    GetExpenseTypes(){
        const path = baseUrl + `/Store/GetExpenseTypes`;
        return Axios.get(path,this.config);
    }

    GetInventoryTypes(){
        const path = baseUrl + `/Store/GetInventoryTypes`;
        return Axios.get(path,this.config);
    }

    GetGoodTypes(){
        const path = baseUrl +`/Store/GetGoodTypes`;
        return Axios.get(path,this.config);
    }

    GetAllFixedAssetRuleAttributes(){
        const path = baseUrl + `/Store/GetAllFixedAssetRuleAttributes`;
        return Axios.get(path, this.config);
    }
    
    GetAllDocumentSerialTypes(){
        const path = baseUrl + `/Store/GetAllDocumentSerialTypes`;
        return Axios.get(path,this.config);
    }

    PostStoreIssueDocument(data,wfid){
        const path = baseUrl + `/Store/PostStoreIssueDocument/${wfid}`;
        return Axios.post(path,data,this.config);
    }

    GetStoreIssueDocumentSigners(wfid){
        const path = baseUrl + `/Store/GetStoreIssueDocumentSigners?wfid=${wfid}`;
        return Axios.get(path,this.config);
    }

    DeliverItems(wfid,data){
        const path = baseUrl + `/Store/DeliverItems/${wfid}`;
        return Axios.post(path,data,this.config);
    }

    SubmitDeliveryVoucher(wfid,data){
        const path = baseUrl +  `/Store/SubmitDeliveryVoucher/${wfid}`;
        return Axios.post(path,data,this.config);
    }

    GetStoreInfo(costCenterID){
        const path = baseUrl + `/Store/GetStore/${costCenterID}`;
        return Axios.get(path,this.config);
    }

    GetStoreDeliveryDocument(docID){
        const path = baseUrl + `/Store/GetStoreDeliverDocument/${docID}`;
        return Axios.get(path,this.config);
    }

    GetGRVDocumentSigners(wfid){
        const path = baseUrl + `/Store/GetGRVDocSigners/${wfid}`;
        return Axios.get(path,this.config);
    }

    CertifyService(wfid,data){
        const path = baseUrl + `/Store/CertifyItems/${wfid}`;
        return Axios.post(path,data,this.config);
    }

    GetNameForUID(id){
        const path = baseUrl + `/Store/GetNameForUID/${id}`;
        return Axios.get(path,this.config);
    }

    GetRemainingItemOrders(id){
        const path = baseUrl + `/Store/GetRemainingItemOrders/${id}`;
        return Axios.get(path,this.config);
    }

    
    GetRemainingServiceOrders(id){
        const path = baseUrl + `/Store/GetRemainingServiceOrders/${id}`;
        return Axios.get(path,this.config);
    }

    CloseStoreRequest(wfid,data){
        const path =  baseUrl + `/Store/CloseRequest/${wfid}`;
        return Axios.post(path,data,this.config);
    }

    UploadIssueVoucher(wfid,data){
        const path =  baseUrl + `/Store/UploadIssueVoucher/${wfid}`;
        return Axios.post(path,data,this.config);
    }


}