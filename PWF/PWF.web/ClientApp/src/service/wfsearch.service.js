import axios from 'axios';
import { reducer } from '../store/WeatherForecasts';
import { baseUrl } from './urlConfig';
import Axios from 'axios';
import { read } from 'fs';
import AccountSearch from '../components/miniComponents/AccountSearch';
//mport axios from 'react-axios';

export class WFSearchService {


    constructor() {
        this.config = {
            withCredentials: true,
            crossdomain: true,
            headers: {
                // 'Authorization': 'bearer ' + this.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        }

    }

    SearchWorkflow(data){
        const path = baseUrl + `/Workflow/SearchWorkflow`;
        return axios.post(path,data,this.config);
    }

  

}