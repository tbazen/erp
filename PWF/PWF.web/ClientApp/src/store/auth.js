const LoginType = 'LOGIN';
const LogoutType = 'LOGOUT';
const initiailState = {
    authenticated : false,
    session : null
};


export const authActionCreators = {
    login : (session) => ({ type : LoginType , session : session}),
    logout : () => ({ type : LogoutType })
};

export const reducer = (state,action) => {
    state = state || initiailState;

    if(action.type == LoginType){
        //console.log("logged in");
        //console.log(action);
        return { ...state, authenticated : true, session : action.session };
    }
    console.log({...state});
    if(action.type == LogoutType){
        return { ...state, authenticated : false, session : null }
    }

    return state;
}