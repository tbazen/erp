
const StartLoad = "STARTLOAD";
const StopLoad = "STOPLOAD";
const initialState = { loading : false }

export const loadActionCreators = {
    start: () => ({ type : StartLoad}),
    stop : () => ({ type : StopLoad })
}

export const reducer = (state,action) => {
    state = state || initialState;

    if (action.type == StartLoad) {
        console.log("Starting");
        return {...state , loading : true };
    }
    if (action.type == StopLoad) {
        console.log("Stopping");
        return {...state, loading : false };
    }

    return state;
}