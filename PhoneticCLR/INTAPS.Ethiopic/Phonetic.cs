namespace INTAPS.Ethiopic
{
    using System;

    public class Phonetic
    {
        public static string GetAIndex(string txt)
        {
            char[] chArray = txt.ToCharArray();
            for (int i = 0; i < chArray.Length; i++)
            {
                if ((chArray[i] >= 'A') && (chArray[i] <= 'Z'))
                {
                    chArray[i] = (char) (chArray[i] + ' ');
                }
                else
                {
                    switch (((short) chArray[i]))
                    {
                        case 0x1200:
                        case 0x1203:
                        case 0x1210:
                        case 0x1213:
                        case 0x1280:
                        case 0x1283:
                        case 0x12bb:
                            goto Label_022F;

                        case 0x1201:
                        case 0x1211:
                        case 0x1281:
                        case 0x12b9:
                            goto Label_023C;

                        case 0x1202:
                        case 0x1212:
                        case 0x1282:
                        case 0x12ba:
                            goto Label_0249;

                        case 0x1204:
                        case 0x1214:
                        case 0x1284:
                        case 0x12bc:
                            goto Label_0256;

                        case 0x1205:
                        case 0x1215:
                        case 0x1285:
                        case 0x12bd:
                            goto Label_0263;

                        case 0x1206:
                        case 0x1216:
                        case 0x1286:
                        case 0x12be:
                            goto Label_0270;

                        case 0x1220:
                        case 0x1230:
                            chArray[i] = 'ሰ';
                            break;

                        case 0x1221:
                        case 0x1231:
                            chArray[i] = 'ሱ';
                            break;

                        case 0x1222:
                        case 0x1232:
                            chArray[i] = 'ሲ';
                            break;

                        case 0x1223:
                        case 0x1233:
                            chArray[i] = 'ሳ';
                            break;

                        case 0x1224:
                        case 0x1234:
                            chArray[i] = 'ሴ';
                            break;

                        case 0x1225:
                        case 0x1235:
                            chArray[i] = 'ስ';
                            break;

                        case 0x1226:
                        case 0x1236:
                            chArray[i] = 'ሶ';
                            break;

                        case 0x12a0:
                        case 0x12a3:
                        case 0x12d0:
                        case 0x12d3:
                            goto Label_027D;

                        case 0x12a1:
                        case 0x12d1:
                            goto Label_028A;

                        case 0x12a2:
                        case 0x12d2:
                            goto Label_0297;

                        case 0x12a4:
                        case 0x12d4:
                            goto Label_02A4;

                        case 0x12a5:
                        case 0x12d5:
                            goto Label_02B1;

                        case 0x12a6:
                        case 0x12d6:
                            goto Label_02BE;

                        case 0x1338:
                        case 0x1340:
                            chArray[i] = 'ፀ';
                            break;

                        case 0x1339:
                        case 0x1341:
                            chArray[i] = 'ፁ';
                            break;

                        case 0x133a:
                        case 0x1342:
                            chArray[i] = 'ፂ';
                            break;

                        case 0x133b:
                        case 0x1343:
                            chArray[i] = 'ፃ';
                            break;

                        case 0x133c:
                        case 0x1344:
                            chArray[i] = 'ፄ';
                            break;

                        case 0x133d:
                        case 0x1345:
                            chArray[i] = 'ፅ';
                            break;

                        case 0x133e:
                        case 0x1346:
                            chArray[i] = 'ፆ';
                            break;
                    }
                }
                continue;
            Label_022F:
                chArray[i] = 'ሀ';
                continue;
            Label_023C:
                chArray[i] = 'ሁ';
                continue;
            Label_0249:
                chArray[i] = 'ሂ';
                continue;
            Label_0256:
                chArray[i] = 'ሄ';
                continue;
            Label_0263:
                chArray[i] = 'ህ';
                continue;
            Label_0270:
                chArray[i] = 'ሆ';
                continue;
            Label_027D:
                chArray[i] = 'አ';
                continue;
            Label_028A:
                chArray[i] = 'ኡ';
                continue;
            Label_0297:
                chArray[i] = 'ኢ';
                continue;
            Label_02A4:
                chArray[i] = 'ኤ';
                continue;
            Label_02B1:
                chArray[i] = 'እ';
                continue;
            Label_02BE:
                chArray[i] = 'ኦ';
            }
            return new string(chArray);
        }
    }
}

