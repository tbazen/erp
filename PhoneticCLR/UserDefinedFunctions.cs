using INTAPS.Ethiopic;
using Microsoft.SqlServer.Server;
using System;
using System.Data.SqlTypes;

public class UserDefinedFunctions
{
    [SqlFunction(IsDeterministic=true, IsPrecise=true)]
    public static SqlString GetAIndex(SqlString str)
    {
        if (str.IsNull)
        {
            return new SqlString();
        }
        return new SqlString(Phonetic.GetAIndex(str.Value));
    }
}

