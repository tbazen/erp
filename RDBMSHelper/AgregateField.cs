
    using System;
namespace INTAPS.RDBMS
{

    public class AgregateField : DataField
    {
        public string foreignKey;

        public AgregateField() : this("id")
        {
        }

        public AgregateField(string foreignKey)
        {
            this.foreignKey = foreignKey;
        }
    }
}

