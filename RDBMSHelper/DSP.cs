
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
namespace INTAPS.RDBMS
{

    public class DSP : IDisposable
    {
        private IDbDataAdapter m_Adapter;
        private bool m_boolIsDisposed;
        private bool m_boolIsTransactionPending;
        private IDbCommand m_Command;
        private IDbConnection m_Connection;
        private string m_ConnectionString;
        private string m_DataBase;
        private Hashtable m_HashTableAdapter;
        private bool m_IntegratedSecurity;
        private string m_PassWord;
        private DBProvider m_Provider;
        private string m_ServerName;
        private SqlCommandBuilder m_SqlCommandBuilder;
        private string m_strConnectionTimeOut;
        private string m_strLaststatment;
        private string m_strPacketSize;
        private IDbTransaction m_Transaction;
        private bool m_trustedConnection;
        private string m_UserName;

        public DSP() : this(DBProvider.MSSQLSERVER)
        {
        }

        public DSP(DBProvider provider)
        {
            this.m_strConnectionTimeOut = "";
            this.m_strPacketSize = "";
            this.m_strLaststatment = "";
            this.m_boolIsDisposed = false;
            this.m_Provider = provider;
            this.m_boolIsTransactionPending = false;
            this.SetSqlServerParams();
            this.m_HashTableAdapter = new Hashtable();
            this.m_ConnectionString = "";
            this.m_ServerName = "";
            this.m_DataBase = "";
            this.m_UserName = "";
            this.m_PassWord = "";
            this.m_trustedConnection = false;
            this.m_IntegratedSecurity = false;
        }

        private DSP(OdbcDriver odbcDriver)
        {
            this.m_strConnectionTimeOut = "";
            this.m_strPacketSize = "";
            this.m_strLaststatment = "";
            this.m_boolIsDisposed = false;
            this.m_boolIsTransactionPending = false;
        }

        private DSP(OleProvider oleProvider)
        {
            this.m_strConnectionTimeOut = "";
            this.m_strPacketSize = "";
            this.m_strLaststatment = "";
            this.m_boolIsDisposed = false;
            this.m_boolIsTransactionPending = false;
            this.m_HashTableAdapter = new Hashtable();
            this.m_ConnectionString = "";
            this.m_ServerName = "";
            this.m_DataBase = "";
            this.m_UserName = "";
            this.m_PassWord = "";
            this.m_trustedConnection = false;
            this.m_IntegratedSecurity = false;
        }

        private static string AppendCommand(string original, string toAdd)
        {
            return ((original == "") ? toAdd : (original + " ; " + toAdd));
        }

        private static string AppendStringwithAnd(string original, string toAdd)
        {
            return ((original == "") ? toAdd : (original + " AND " + toAdd));
        }

        private static string AppendStringwithComma(string original, string toAdd)
        {
            return ((original == "") ? toAdd : (original + " , " + toAdd));
        }

        public bool BeginTransaction()
        {
            try
            {
                if (this.m_boolIsTransactionPending)
                {
                    return false;
                }
                if (!this.OpenConnection())
                {
                    return false;
                }
                this.m_Transaction = this.m_Connection.BeginTransaction();
                this.m_Command.Connection = this.m_Connection;
                this.m_Command.Transaction = this.m_Transaction;
                this.m_boolIsTransactionPending = true;
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "BeginTransaction", exception));
                return false;
            }
        }

        public bool BeginTransaction(IsolationLevel isolationLevel)
        {
            try
            {
                if (this.m_boolIsTransactionPending)
                {
                    return false;
                }
                if (!this.OpenConnection())
                {
                    return false;
                }
                this.m_Transaction = this.m_Connection.BeginTransaction(isolationLevel);
                this.m_Command.Connection = this.m_Connection;
                this.m_Command.Transaction = this.m_Transaction;
                this.m_boolIsTransactionPending = true;
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "BeginTransaction", exception));
                return false;
            }
        }

        private void BuildConnectionString()
        {
            try
            {
                this.CloseConnection();
                this.m_ConnectionString = "";
                switch (this.m_Provider)
                {
                    case DBProvider.ORACLE:
                        this.m_ConnectionString = this.m_ConnectionString + "Data Source=" + this.m_ServerName + ";";
                        this.m_ConnectionString = this.m_ConnectionString + "Initial Catalog=" + this.m_DataBase + ";";
                        if ((this.m_UserName != null) && (this.m_UserName != ""))
                        {
                            this.m_ConnectionString = this.m_ConnectionString + "UID=" + this.m_UserName + ";";
                        }
                        if ((this.m_PassWord != null) && (this.m_PassWord != ""))
                        {
                            this.m_ConnectionString = this.m_ConnectionString + "PassWord=" + this.m_PassWord + ";";
                        }
                        break;

                    case DBProvider.OLEDB:
                        this.m_ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;";
                        this.m_ConnectionString = this.m_ConnectionString + "Data Source=" + this.m_DataBase;
                        break;

                    case DBProvider.ODBC:
                        this.m_ConnectionString = "Driver={Microsoft Text Driver (*.txt; *.csv)};";
                        this.m_ConnectionString = this.m_ConnectionString + "DBQ=" + this.m_DataBase;
                        break;

                    default:
                        this.m_ConnectionString = this.m_ConnectionString + "Data Source=" + this.m_ServerName + ";";
                        this.m_ConnectionString = this.m_ConnectionString + "Initial Catalog=" + this.m_DataBase + ";";
                        if (this.TrustedConnection)
                        {
                            this.m_ConnectionString = this.m_ConnectionString + "Trusted_Connection=True;";
                        }
                        else
                        {
                            if ((this.m_UserName != null) && (this.m_UserName != ""))
                            {
                                this.m_ConnectionString = this.m_ConnectionString + "User ID=" + this.m_UserName + ";";
                            }
                            if (this.m_PassWord != null)
                            {
                                this.m_ConnectionString = this.m_ConnectionString + "PWD=" + this.m_PassWord + ";";
                            }
                        }
                        break;
                }
                this.m_ConnectionString = this.m_ConnectionString + this.m_strConnectionTimeOut + this.m_strPacketSize;
                this.m_strLaststatment = "m_Connection.ConnectionString=" + this.m_ConnectionString;
                this.m_Connection.ConnectionString = this.m_ConnectionString;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "BuildConnectionString", exception));
            }
        }

        public void ChangeDatabase(string database)
        {
            if (string.Compare(this.m_DataBase, database) != 0)
            {
                this.m_DataBase = database;
                this.BuildConnectionString();
            }
        }

        public void ChangePassword(string password)
        {
            if (string.Compare(this.m_PassWord, password) != 0)
            {
                this.m_PassWord = password;
                this.BuildConnectionString();
            }
        }

        public void ChangeServer(string serverName)
        {
            if (string.Compare(this.m_ServerName, serverName) != 0)
            {
                this.m_ServerName = serverName;
                this.BuildConnectionString();
            }
        }

        public void ChangeUserName(string userName)
        {
            if (string.Compare(this.m_UserName, userName) != 0)
            {
                this.m_UserName = userName;
                this.BuildConnectionString();
            }
        }

        public DSP Clone()
        {
            return new DSP(this.m_Provider) { ConnectionString = this.m_ConnectionString };
        }

        public DSP Clone(string database)
        {
            DSP dsp = new DSP(this.m_Provider);
            dsp.SetConnectionParameters(this.m_ServerName, database, this.m_UserName, this.m_PassWord, this.m_trustedConnection);
            return dsp;
        }

        public void CloseConnection()
        {
            try
            {
                if (this.m_Connection.State != System.Data.ConnectionState.Closed)
                {
                    this.m_strLaststatment = "m_Connection.Close";
                    this.m_Connection.Close();
                }
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "CloseConnection", exception));
            }
        }

        public bool CommitTransaction()
        {
            try
            {
                if (!this.m_boolIsTransactionPending)
                {
                    return false;
                }
                if (((this.ConnectionState() == System.Data.ConnectionState.Closed) || (this.ConnectionState() == System.Data.ConnectionState.Broken)) || (this.m_Transaction == null))
                {
                    return false;
                }
                this.m_strLaststatment = "m_Transaction.Commit()";
                this.m_Transaction.Commit();
                this.m_boolIsTransactionPending = false;
                return true;
            }
            catch (Exception exception)
            {
                this.m_boolIsTransactionPending = false;
                this.ThrowException(new DSPEventArgs("DSP", "CommiTransaction", exception));
                return false;
            }
        }

        public string ConcatenateExpression(params string[] fields)
        {
            int num;
            string str = "";
            switch (this.m_Provider)
            {
                case DBProvider.ORACLE:
                    num = 0;
                    while (num < fields.Length)
                    {
                        str = str + fields[num] + " || ";
                        num++;
                    }
                    break;

                default:
                    for (num = 0; num < fields.Length; num++)
                    {
                        str = str + fields[num] + " + ";
                    }
                    break;
            }
            return str.TrimEnd(new char[] { '+', '|', '|' });
        }

        public System.Data.ConnectionState ConnectionState()
        {
            try
            {
                return this.m_Connection.State;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "ConnectionState", exception));
                return System.Data.ConnectionState.Closed;
            }
        }

        public IDbCommand CreateCommand(string cmd)
        {
            if (!this.OpenConnection())
            {
                throw new Exception("Failed to oppen connection for create command.");
            }
            IDbCommand command = this.m_Connection.CreateCommand();
            command.CommandText = cmd;
            if (this.m_boolIsTransactionPending)
            {
                command.Transaction = this.m_Transaction;
            }
            return command;
        }

        private void DataTableToAdapterMatch(DataSet ds, IDataAdapter adapter)
        {
            try
            {
                this.m_HashTableAdapter.Add(ds, adapter);
            }
            catch
            {
            }
        }

        private void DataTableToAdapterMatch(DataTable table, IDataAdapter adapter)
        {
            try
            {
                this.m_HashTableAdapter.Add(table, adapter);
            }
            catch
            {
            }
        }

        public bool Delete(string tableName, string whereCriteria)
        {
            try
            {
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                string str = "";
                str = "DELETE FROM [";
                str = str + tableName + "] ";
                if (whereCriteria != null)
                {
                    str = str + ((whereCriteria.Length == 0) ? " " : (" WHERE " + whereCriteria));
                }
                this.m_Command.CommandText = str;
                this.m_Command.Connection = this.m_Connection;
                this.m_strLaststatment = str;
                this.OpenConnection();
                this.m_Command.ExecuteNonQuery();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "Delete", exception));
                return false;
            }
        }

        public int DeleteSingleTableRecrod<T>(params object[] key)
        {
            return this.DeleteSingleTableRecrod<T>(key, null);
        }

        public int DeleteSingleTableRecrod<T>(object[] key, string AdditionalCriteria)
        {
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            this.m_Command.Parameters.Clear();
            int index = 0;
            string str = null;
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                System.Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if (field is IDField)
                        {
                            string str4;
                            string str2 = (field.fieldName == null) ? info.Name : field.fieldName;
                            string str3 = "@Par" + index;
                            if (string.IsNullOrEmpty(str))
                            {
                                str4 = str;
                                str = str4 + "[" + str2 + "]=" + str3;
                            }
                            else
                            {
                                str4 = str;
                                str = str4 + " and [" + str2 + "]=" + str3;
                            }
                            object val = key[index];
                            if (!(field.allowNull || (val != null)))
                            {
                                val = TranslateNullValueToDB(val, info.FieldType);
                            }
                            this.m_Command.Parameters.Add(new SqlParameter("@Par" + index, (val == null) ? System.DBNull.Value : val));
                            index++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(str))
            {
                str = "";
            }
            else
            {
                str = " WHERE " + str;
            }
            if (!string.IsNullOrEmpty(AdditionalCriteria))
            {
                if (string.IsNullOrEmpty(str))
                {
                    str = " WHERE " + AdditionalCriteria;
                }
                else
                {
                    str = str + " and (" + AdditionalCriteria + ")";
                }
            }
            this.m_Command.CommandText = "DELETE FROM [" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "] " + str;
            this.OpenConnection();
            this.m_Command.Connection = this.m_Connection;
            if (this.m_boolIsTransactionPending)
            {
                this.m_Command.Transaction = this.m_Transaction;
            }
            return this.m_Command.ExecuteNonQuery();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!this.m_boolIsDisposed && isDisposing)
            {
                if (this.m_Connection != null)
                {
                    if (this.m_Connection.State != System.Data.ConnectionState.Closed)
                    {
                        this.m_Connection.Close();
                    }
                    this.m_Connection.Dispose();
                    this.m_Connection = null;
                }
                if (this.m_Command != null)
                {
                    this.m_Command.Dispose();
                    this.m_Command = null;
                }
                if (this.m_Transaction != null)
                {
                    this.m_Transaction.Dispose();
                    this.m_Transaction = null;
                }
                if (this.m_SqlCommandBuilder != null)
                {
                    this.m_SqlCommandBuilder.Dispose();
                    this.m_SqlCommandBuilder = null;
                }

                this.m_HashTableAdapter = null;
            }
            this.m_boolIsDisposed = true;
        }

        public static string EscapeString(string query)
        {
            return query.Replace("[", "[[]").Replace("%", "[%]").Replace("_", "[_]").Replace("'", "['']");
        }

        public bool ExecuteNonQuery(string query)
        {
            try
            {
                this.OpenConnection();
                this.m_Command.CommandText = query;
                this.m_strLaststatment = query;
                this.m_Command.Connection = this.m_Connection;
                this.m_Command.ExecuteNonQuery();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", " ExecuteNonQuery(string selectquery)", exception));
                return false;
            }
        }

        public IDataReader ExecuteReader(string selectquery)
        {
            try
            {
                this.OpenConnection();
                this.m_Command.CommandText = selectquery;
                this.m_strLaststatment = selectquery;
                this.m_Command.Connection = this.m_Connection;
                return this.m_Command.ExecuteReader();
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "ExecuteReader(string selectquery)", exception));
                return null;
            }
        }

        public IDataReader ExecuteReader(string tableName, string[] ColumnNames, string WhereCriteria)
        {
            try
            {
                int num;
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                for (num = 0; num < ColumnNames.Length; num++)
                {
                    ColumnNames[num] = ColumnNames[num].TrimStart(new char[] { '[' });
                    ColumnNames[num] = ColumnNames[num].TrimEnd(new char[] { ']' });
                }
                string str = "";
                str = "SELECT  [";
                str = str + ColumnNames[0];
                for (num = 1; num < ColumnNames.Length; num++)
                {
                    str = str + "],[" + ColumnNames[num];
                }
                str = str + "] FROM  [" + tableName + "]";
                if ((WhereCriteria != null) && (WhereCriteria != ""))
                {
                    str = str + " WHERE " + WhereCriteria;
                }
                this.m_Command.CommandText = str;
                this.m_strLaststatment = str;
                this.m_Command.Connection = this.m_Connection;
                return this.m_Command.ExecuteReader();
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "ExecuteReader(string tableName,string[] ColumnNames,string WhereCriteria)", exception));
                return null;
            }
        }

        public object ExecuteScalar(string selectquery)
        {
            try
            {
                this.OpenConnection();
                this.m_Command.CommandText = selectquery;
                this.m_strLaststatment = selectquery;
                this.m_Command.Connection = this.m_Connection;
                return this.m_Command.ExecuteScalar();
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "ExecuteScalar", exception));
                return null;
            }
        }

        private IDataAdapter GenerateAdapter()
        {
            return new SqlDataAdapter();
        }

        public T[] GetColumnArray<T>(string sql, int col)
        {
            DataTable dataTable = this.GetDataTable(sql, false);
            T[] localArray = new T[dataTable.Rows.Count];
            int index = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                localArray[index] = (T)row[col];
                index++;
            }
            return localArray;
        }

        public DataSet GetDataSet(string query, bool fillSchema)
        {
            try
            {
                this.m_strLaststatment = query;
                DataSet dataSet = new DataSet();

                this.m_Adapter = new SqlDataAdapter(query, (SqlConnection)this.m_Connection);
                if (fillSchema)
                    ((SqlDataAdapter)this.m_Adapter).FillSchema(dataSet, SchemaType.Source);

                ((SqlDataAdapter)this.m_Adapter).Fill(dataSet);
                this.DataTableToAdapterMatch(dataSet, this.m_Adapter);
                return dataSet;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "GetDataSet", exception));
                return null;
            }
        }

        public DataTable GetDataTable(string query, bool fillSchema)
        {
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(query, (SqlConnection)this.m_Connection)
            {
                SelectCommand = { Transaction = (SqlTransaction)this.m_Transaction }
            };
            dataTable.BeginLoadData();
            if (fillSchema)
            {
                adapter.FillSchema(dataTable, SchemaType.Source);
            }
            adapter.Fill(dataTable);
            dataTable.EndLoadData();
            return dataTable;
        }

        public DataTable GetDataTable(string tableName, string[] ColumnNames, string WhereCriteria, bool fillSchema)
        {
            try
            {
                int num;
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                for (num = 0; num < ColumnNames.Length; num++)
                {
                    ColumnNames[num] = ColumnNames[num].TrimStart(new char[] { '[' });
                    ColumnNames[num] = ColumnNames[num].TrimEnd(new char[] { ']' });
                }
                DataTable dataTable = new DataTable();
                string selectCommandText = "";
                selectCommandText = "SELECT  [";
                selectCommandText = selectCommandText + ColumnNames[0];
                for (num = 1; num < ColumnNames.Length; num++)
                {
                    selectCommandText = selectCommandText + "],[" + ColumnNames[num];
                }
                selectCommandText = selectCommandText + "] FROM  [" + tableName + "]";
                if ((WhereCriteria != null) && (WhereCriteria != ""))
                {
                    selectCommandText = selectCommandText + " WHERE " + WhereCriteria;
                }
                this.m_strLaststatment = selectCommandText;

                this.m_Adapter = new SqlDataAdapter(selectCommandText, (SqlConnection)this.m_Connection);
                this.m_Adapter.SelectCommand.Transaction = this.m_Transaction;
                dataTable.BeginLoadData();
                if (fillSchema)
                {
                    ((SqlDataAdapter)this.m_Adapter).FillSchema(dataTable, SchemaType.Source);
                }
                        ((SqlDataAdapter)this.m_Adapter).Fill(dataTable);
                dataTable.EndLoadData();
                this.DataTableToAdapterMatch(dataTable, this.m_Adapter);
                return dataTable;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "GetDataTable(string tableName,string[] ColumnNames,string WhereCriteria,bool fillSchema)", exception));
                return null;
            }
        }

        public int GetNumberofRecords(string selectquery)
        {
            try
            {
                object obj2 = new object();
                string str = "SELECT COUNT(*) AS Expr1 FROM (" + selectquery + ") DRVTBL";
                this.OpenConnection();
                this.m_Command.CommandText = str;
                this.m_strLaststatment = str;
                this.m_Command.Connection = this.m_Connection;
                return int.Parse(this.m_Command.ExecuteScalar().ToString());
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "GetNumberofRecords", exception));
                return -1;
            }
        }

        public bool GetRow(string tableName, string[] ColumnNames, string whereCriteria, ref object[] Values)
        {
            try
            {
                DataTable table = this.GetDataTable(tableName, ColumnNames, whereCriteria, true);
                if (table == null)
                {
                    return false;
                }
                if (table.Rows.Count == 0)
                {
                    return false;
                }
                DataRow row = table.Rows[0];
                Values = new object[ColumnNames.Length];
                for (int i = 0; i < ColumnNames.Length; i++)
                {
                    if (row[i].ToString() != System.DBNull.Value.ToString())
                    {
                        Values[i] = row[i];
                    }
                    else
                    {
                        Values[i] = null;
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "GetRow", exception));
                return false;
            }
        }

        public DataTable GetSchemaTable(string query)
        {
            try
            {
                DataTable dataTable = new DataTable();
                this.m_strLaststatment = query;
                this.m_Adapter = new SqlDataAdapter(query, (SqlConnection)this.m_Connection);
                ((SqlDataAdapter)this.m_Adapter).FillSchema(dataTable, SchemaType.Source);
                this.DataTableToAdapterMatch(dataTable, this.m_Adapter);
                return dataTable;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "GetSchemaTable", exception));
                return null;
            }
        }

        public DataTable GetSchemaTable(string tableName, string[] ColumnNames)
        {
            try
            {
                int num;
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                for (num = 0; num < ColumnNames.Length; num++)
                {
                    ColumnNames[num] = ColumnNames[num].TrimStart(new char[] { '[' });
                    ColumnNames[num] = ColumnNames[num].TrimEnd(new char[] { ']' });
                }
                DataTable dataTable = new DataTable();
                string selectCommandText = "";
                selectCommandText = "SELECT  [";
                selectCommandText = selectCommandText + ColumnNames[0];
                for (num = 1; num < ColumnNames.Length; num++)
                {
                    selectCommandText = selectCommandText + "],[" + ColumnNames[num];
                }
                selectCommandText = selectCommandText + "] FROM  [" + tableName + "]";
                this.m_strLaststatment = selectCommandText;
                this.m_Adapter = new SqlDataAdapter(selectCommandText, (SqlConnection)this.m_Connection);
                ((SqlDataAdapter)this.m_Adapter).FillSchema(dataTable, SchemaType.Source);
                this.DataTableToAdapterMatch(dataTable, this.m_Adapter);
                return dataTable;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "GetSchemaTable", exception));
                return null;
            }
        }

        public long GetSerialLastValue(string Name)
        {
            long num;
            SqlDataReader reader = (SqlDataReader)this.ExecuteReader("Select Name,LastValue from AutoIncrementFields where Name='" + Name + "'");
            if (reader.Read())
            {
                num = (long)reader["LastValue"];
                reader.Close();
                return num;
            }
            num = 0L;
            reader.Close();
            this.SetSeed(Name, 0L, true);
            return num;
        }

        public long GetSerialNo(string Name)
        {
            long num = this.GetSerialLastValue(Name) + 1L;
            this.m_Command.CommandText = string.Concat(new object[] { "Update AutoIncrementFields  Set LastValue=", num, " where Name='", Name, "'" });
            this.m_Command.Connection = this.m_Connection;
            this.m_Command.ExecuteNonQuery();
            return num;
        }

        public T[] GetSTRArrayByFilter<T>(string filter) where T : new()
        {
            object[][] objArray;
            int num;
            return this.GetSTRArrayByFilter<T>(filter, new string[0], out objArray, 0, -1, out num, null);
        }

        public void GetSTRArrayByFilter<T>(T at, string filter) where T : class, new()
        {
            DataField field;
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string str = null;
            foreach (FieldInfo info in type.GetFields())
            {
                if (info.DeclaringType == type)
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        field = (DataField)customAttributes[0];
                        string str2 = (field.fieldName == null) ? info.Name : field.fieldName;
                        if (string.IsNullOrEmpty(str))
                        {
                            str = "[" + str2 + "]";
                        }
                        else
                        {
                            str = str + ",[" + str2 + "]";
                        }
                    }
                }
            }
            string selectCommandText = "Select " + str + " FROM [" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]" + (string.IsNullOrEmpty(filter) ? "" : (" WHERE " + filter));
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(selectCommandText, (SqlConnection)this.m_Connection);
            this.OpenConnection();
            adapter.Fill(dataTable);
            T[] localArray = new T[dataTable.Rows.Count];
            foreach (DataRow row in dataTable.Rows)
            {
                int num = 0;
                foreach (FieldInfo info in type.GetFields())
                {
                    if (info.DeclaringType == type)
                    {
                        customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                        if ((customAttributes != null) && (customAttributes.Length != 0))
                        {
                            field = (DataField)customAttributes[0];
                            object val = row[num];
                            if (val is System.DBNull)
                            {
                                val = TranslateDBNull(info.FieldType, val);
                            }
                            info.SetValue(at, val);
                            num++;
                        }
                    }
                }
                break;
            }
        }

        public T[] GetSTRArrayByFilter<T>(string filter, string[] AdditionalFields, out object[][] AdditionalData) where T : new()
        {
            int num;
            return this.GetSTRArrayByFilter<T>(filter, AdditionalFields, out AdditionalData, 0, -1, out num, null);
        }

        public T[] GetSTRArrayByFilter<T>(string filter, int index, int pageSize, out int NRecords) where T : new()
        {
            object[][] additionalData = new object[0][];
            return this.GetSTRArrayByFilter<T>(filter, new string[0], out additionalData, index, pageSize, out NRecords, null);
        }

        public T[] GetSTRArrayByFilter<T>(string filter, int index, int pageSize, out int NRecords, IObjectFilter<T> objectFilter) where T : new()
        {
            object[][] additionalData = new object[0][];
            return this.GetSTRArrayByFilter<T>(filter, new string[0], out additionalData, index, pageSize, out NRecords, objectFilter);
        }

        public T[] GetSTRArrayByFilter<T>(string filter, string[] AdditionalFields, out object[][] AdditionalData, int index, int pageSize, out int NRecords, IObjectFilter<T> objectFilter) where T : new()
        {
            int num;
            T[] localArray;
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string str = null;
            for (num = 0; num < AdditionalFields.Length; num++)
            {
                if (string.IsNullOrEmpty(str))
                {
                    str = "[" + AdditionalFields[num] + "]";
                }
                else
                {
                    str = str + ",[" + AdditionalFields[num] + "]";
                }
            }
            List<FieldInfo> list = new List<FieldInfo>();
            List<DataField> list2 = new List<DataField>();
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                System.Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField item = (DataField)customAttributes[0];
                        string str2 = (item.fieldName == null) ? info.Name : item.fieldName;
                        if (string.IsNullOrEmpty(str))
                        {
                            str = "[" + str2 + "]";
                        }
                        else
                        {
                            str = str + ",[" + str2 + "]";
                        }
                        list.Add(info);
                        list2.Add(item);
                    }
                }
            }
            string cmdText = "Select " + str + " FROM [" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]" + (string.IsNullOrEmpty(filter) ? "" : (" WHERE " + filter));
            if (!string.IsNullOrEmpty(attribute.orderBy))
            {
                cmdText = cmdText + " order by " + attribute.orderBy;
            }
            this.OpenConnection();
            SqlCommand command = new SqlCommand(cmdText, (SqlConnection)this.m_Connection)
            {
                Transaction = (SqlTransaction)this.m_Transaction
            };
            SqlDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
                List<T> list3 = new List<T>();
                List<object[]> list4 = new List<object[]>();
                num = 0;
                while (reader.Read())
                {
                    if (((objectFilter == null) && (pageSize != -1)) && ((num < index) || (num >= (index + pageSize))))
                    {
                        num++;
                    }
                    else
                    {
                        T local2 = default(T);
                        T local = (local2 == null) ? Activator.CreateInstance<T>() : (local2 = default(T));
                        object[] additional = new object[AdditionalFields.Length];
                        int num2 = 0;
                        for (int i = 0; i < AdditionalFields.Length; i++)
                        {
                            additional[num2] = reader[num2];
                            num2++;
                        }
                        int num4 = 0;
                        foreach (FieldInfo info in list)
                        {
                            object val = reader[num2];
                            if (list2[num4] is XMLField)
                            {
                                if (string.IsNullOrEmpty(val as string))
                                {
                                    val = null;
                                }
                                else
                                {
                                    val = XmlToObject((string)val, info.FieldType);
                                }
                            }
                            if (val is System.DBNull)
                            {
                                val = TranslateDBNull(info.FieldType, val);
                            }
                            info.SetValue(local, val);
                            num2++;
                            num4++;
                        }
                        if ((objectFilter == null) || objectFilter.Include(local, additional))
                        {
                            if (((pageSize == -1) || (num >= index)) || (num < (index + pageSize)))
                            {
                                list4.Add(additional);
                                list3.Add(local);
                            }
                            num++;
                        }
                    }
                }
                NRecords = num;
                AdditionalData = list4.ToArray();
                localArray = list3.ToArray();
            }
            finally
            {
                if (!((reader == null) || reader.IsClosed))
                {
                    reader.Close();
                }
            }
            return localArray;
        }

        public T[] GetSTRArrayBySQL<T>(string sql) where T : new()
        {
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, (SqlConnection)this.m_Connection);
            this.OpenConnection();
            adapter.Fill(dataTable);
            T[] localArray = new T[dataTable.Rows.Count];
            int num = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                T local2 = default(T);
                T local = (local2 == null) ? Activator.CreateInstance<T>() : (local2 = default(T));
                int num2 = 0;
                foreach (FieldInfo info in type.GetFields())
                {
                    if (info.DeclaringType == type)
                    {
                        customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                        if ((customAttributes != null) && (customAttributes.Length != 0))
                        {
                            DataField field = (DataField)customAttributes[0];
                            string str = (field.fieldName == null) ? info.Name : field.fieldName;
                            object val = row[str];
                            if (val is System.DBNull)
                            {
                                val = TranslateDBNull(info.FieldType, val);
                            }
                            info.SetValue(local, val);
                            num2++;
                        }
                    }
                }
                localArray[num++] = local;
            }
            return localArray;
        }

        public void GetSTRArrayBySQL<T>(T at, string sql) where T : class, new()
        {
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            DataTable dataTable = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, (SqlConnection)this.m_Connection);
            this.OpenConnection();
            adapter.Fill(dataTable);
            T[] localArray = new T[dataTable.Rows.Count];
            foreach (DataRow row in dataTable.Rows)
            {
                int num = 0;
                foreach (FieldInfo info in type.GetFields())
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        string str = (field.fieldName == null) ? info.Name : field.fieldName;
                        object val = row[str];
                        if (val is System.DBNull)
                        {
                            val = TranslateDBNull(info.FieldType, val);
                        }
                        info.SetValue(at, val);
                        num++;
                    }
                }
                break;
            }
        }

        public bool Insert(string tableName, string[] ColumnNames, object[] Values)
        {
            try
            {
                int num;
                if (ColumnNames.Length != Values.Length)
                {
                    return false;
                }
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                for (num = 0; num < ColumnNames.Length; num++)
                {
                    ColumnNames[num] = ColumnNames[num].TrimStart(new char[] { '[' });
                    ColumnNames[num] = ColumnNames[num].TrimEnd(new char[] { ']' });
                }
                string str = "";
                str = "INSERT INTO [";
                str = (str + tableName) + "] ( [" + ColumnNames[0];
                for (num = 1; num < ColumnNames.Length; num++)
                {
                    str = str + "],[" + ColumnNames[num];
                }
                str = str + "] ) VALUES (";

                {
                    SqlParameter parameter = new SqlParameter("@Par0", (Values[0] == null) ? System.DBNull.Value : Values[0]);
                    this.m_Command.Parameters.Clear();
                    this.m_Command.Parameters.Add(parameter);
                    str = str + "@Par0";
                    num = 1;
                    while (num < Values.Length)
                    {
                        parameter = new SqlParameter("@Par" + num.ToString(), (Values[num] == null) ? System.DBNull.Value : Values[num]);
                        str = (str + ",") + "@Par" + num.ToString();
                        this.m_Command.Parameters.Add(parameter);
                        num++;
                    }
                }

                str = str + ")";
                this.m_Command.CommandText = str;
                this.m_Command.Connection = this.m_Connection;
                this.m_strLaststatment = str;
                this.OpenConnection();
                this.m_Command.ExecuteNonQuery();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "Insert", exception));
                return false;
            }
        }

        public long Insert(string tableName, string[] ColumnNames, object[] Values, string IdentityField)
        {
            if (!this.Insert(tableName, ColumnNames, Values))
            {
                throw new Exception("Inserting the object to the database failed");
            }
            return long.Parse(this.ExecuteScalar("Select Max(" + IdentityField + ") From " + tableName).ToString());
        }

        public void InsertColumn<T>(string Table, string[] cols, object[] fvals, T[] colvals)
        {
            if (fvals.Length != (cols.Length - 1))
            {
                throw new Exception("Column count mismatch");
            }
            object[] array = new object[fvals.Length + 1];
            fvals.CopyTo(array, 0);
            foreach (T local in colvals)
            {
                array[fvals.Length] = local;
                this.Insert(Table, cols, array);
            }
        }

        public int InsertSingleTableRecord<T>(T rec)
        {
            return this.InsertSingleTableRecord<T>(rec, new string[0], new object[0]);
        }

        public int InsertSingleTableRecord<T>(T rec, string[] AdditionalFields, object[] AdditionalValues)
        {
            object obj2;
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string str = "(";
            string str2 = "(";
            int num = 0;
            this.m_Command.Parameters.Clear();
            for (int i = 0; i < AdditionalFields.Length; i++)
            {
                if (num > 0)
                {
                    str = str + ",";
                    str2 = str2 + ",";
                }
                str = str + "[" + AdditionalFields[i] + "]";
                str2 = str2 + "@Par" + num;
                obj2 = AdditionalValues[i];
                this.m_Command.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? System.DBNull.Value : obj2));
                num++;
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                System.Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if (num > 0)
                        {
                            str = str + ",";
                            str2 = str2 + ",";
                        }
                        str = str + "[" + ((field.fieldName == null) ? info.Name : field.fieldName) + "]";
                        str2 = str2 + "@Par" + num;
                        obj2 = info.GetValue(rec);
                        if (field is XMLField)
                        {
                            if (obj2 == null)
                            {
                                obj2 = "";
                            }
                            else
                            {
                                obj2 = ObjectToXml(obj2);
                            }
                        }
                        else if (!(field.allowNull || (obj2 != null)))
                        {
                            obj2 = TranslateNullValueToDB(obj2, info.FieldType);
                        }
                        this.m_Command.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? System.DBNull.Value : obj2));
                        num++;
                    }
                }
            }
            this.m_Command.CommandText = "Insert into [" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]" + str + ") values " + str2 + ")";
            this.OpenConnection();
            this.m_Command.Connection = this.m_Connection;
            if (this.m_boolIsTransactionPending)
            {
                this.m_Command.Transaction = this.m_Transaction;
            }
            return this.m_Command.ExecuteNonQuery();
        }

        public string IsNullExpression(string fieldName, object replace)
        {
            fieldName = fieldName.TrimStart(new char[] { '[' });
            fieldName = fieldName.TrimEnd(new char[] { ']' });
            return (" ISNULL([" + fieldName + "]," + this.ToDBString(replace) + ") ");
        }

        public void LoadSeed(SqlConnection con, string Name, string table, string field, string criteria, bool overide)
        {
            string str = "Select Max(" + field + ") from [" + table + ((criteria == "") ? "]" : ("] where " + criteria));
            this.m_Command.CommandText = str;
            this.m_strLaststatment = str;
            this.m_Command.Connection = this.m_Connection;
            object obj2 = this.m_Command.ExecuteScalar();
            if ((obj2 == System.DBNull.Value) || (obj2 == null))
            {
                this.SetSeed(Name, 1L, overide);
            }
            else
            {
                this.SetSeed(Name, (long)(((int)obj2) + 1), overide);
            }
        }

        public static string ObjectToXml(object o)
        {
            XmlSerializer serializer = new XmlSerializer(o.GetType());
            StringWriter writer = new StringWriter();
            serializer.Serialize((TextWriter)writer, o);
            writer.Close();
            return writer.ToString();
        }
        public static string ObjectToXml(object o, Type t)
        {
            XmlSerializer serializer = new XmlSerializer(t);
            StringWriter writer = new StringWriter();
            serializer.Serialize((TextWriter)writer, o);
            writer.Close();
            return writer.ToString();
        }
        private bool OpenConnection()
        {
            try
            {
                if (this.m_Connection.State == System.Data.ConnectionState.Open)
                {
                    return true;
                }
                if ((this.m_Connection.State == System.Data.ConnectionState.Broken) || (this.m_Connection.State == System.Data.ConnectionState.Closed))
                {
                    this.m_Connection.Open();
                    return true;
                }
                this.m_strLaststatment = "m_Connection.Open";
                return false;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "OpenConnection", exception));
                return false;
            }
        }

        public void RemoveDatasetsfromObjectCatch(params DataSet[] datasets)
        {
            try
            {
                foreach (DataSet set in datasets)
                {
                    if (this.m_HashTableAdapter.ContainsKey(set))
                    {
                        this.m_HashTableAdapter.Remove(set);
                    }
                }
            }
            catch (Exception exception)
            {
                this.m_strLaststatment = "";
                this.ThrowException(new DSPEventArgs("DSP", "RemoveDatasetsfromObjectCatch", exception));
            }
        }

        public void RemoveDataTablesfromObjectCatch(params DataTable[] tables)
        {
            try
            {
                foreach (DataTable table in tables)
                {
                    if (this.m_HashTableAdapter.ContainsKey(table))
                    {
                        this.m_HashTableAdapter.Remove(table);
                    }
                }
            }
            catch (Exception exception)
            {
                this.m_strLaststatment = "";
                this.ThrowException(new DSPEventArgs("DSP", "RemoveDataTablesfromObjectCatch", exception));
            }
        }

        public bool RollBackTransaction()
        {
            try
            {
                if (!this.m_boolIsTransactionPending)
                {
                    return false;
                }
                if (((this.ConnectionState() == System.Data.ConnectionState.Closed) || (this.ConnectionState() == System.Data.ConnectionState.Broken)) || (this.m_Transaction == null))
                {
                    return false;
                }
                this.m_Transaction.Rollback();
                this.m_boolIsTransactionPending = false;
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "RollBackTransaction", exception));
                return false;
            }
        }

        public bool RunStoredProcedure(string SP_Name, params IDataParameter[] Parameters)
        {
            try
            {
                this.m_Command.CommandType = CommandType.StoredProcedure;
                this.m_Command.CommandText = SP_Name;
                this.m_Command.Connection = this.m_Connection;
                this.m_Command.Parameters.Clear();
                if (Parameters != null)
                {
                    for (int i = 0; i < Parameters.Length; i++)
                    {
                        this.m_Command.Parameters.Add(Parameters[i]);
                    }
                }
                this.m_strLaststatment = "m_Command.CommandText=" + SP_Name + ";\n m_Command.ExecuteNonQuery();";
                this.OpenConnection();
                this.m_Command.Prepare();
                this.m_Command.ExecuteNonQuery();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "RunStoredProcedure", exception));
                return false;
            }
        }

        public IDataReader RunStoredProcedureReader(string SP_Name, params IDataParameter[] Parameters)
        {
            try
            {
                this.m_Command.CommandType = CommandType.StoredProcedure;
                this.m_Command.CommandText = SP_Name;
                this.m_Command.Connection = this.m_Connection;
                this.m_Command.Parameters.Clear();
                if (Parameters != null)
                {
                    for (int i = 0; i < Parameters.Length; i++)
                    {
                        this.m_Command.Parameters.Add(Parameters[i]);
                    }
                }
                this.m_strLaststatment = "m_Command.CommandText=" + SP_Name + ";\n m_Command.ExecuteReader();";
                this.OpenConnection();
                this.m_Command.Prepare();
                return this.m_Command.ExecuteReader();
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "RunStoredProcedureReader", exception));
                return null;
            }
        }

        public bool SaveDataTable(DataTable dataTable)
        {
            Exception exception;
            bool flag = false;
            try
            {
                if (this.m_Provider == DBProvider.MSSQLSERVER)
                {
                    SqlConnection connection = new SqlConnection(this.ConnectionString);
                    SqlCommand command = new SqlCommand
                    {
                        Connection = connection
                    };
                    string original = "";
                    int num = 0;
                    SqlParameter parameter = new SqlParameter();
                    for (num = 0; num < dataTable.Rows.Count; num++)
                    {
                        string str2;
                        int num2;
                        if (dataTable.Rows[num].RowState == DataRowState.Added)
                        {
                            str2 = "";
                            num2 = 0;
                            while (num2 < dataTable.Columns.Count)
                            {
                                if (!(!(dataTable.Columns[num2].Expression == "") || dataTable.Columns[num2].AutoIncrement))
                                {
                                    str2 = AppendStringwithComma(str2, "@Add" + num.ToString() + dataTable.Columns[num2].ColumnName);
                                    parameter = new SqlParameter("@Add" + num.ToString() + dataTable.Columns[num2].ColumnName, dataTable.Rows[num][num2]);
                                    command.Parameters.Add(parameter);
                                }
                                num2++;
                            }
                            string toAdd = "Insert into " + dataTable.TableName + " Values(" + str2 + ")";
                            original = AppendCommand(original, toAdd);
                        }
                        else
                        {
                            string str4;
                            int num3;
                            if (dataTable.Rows[num].RowState == DataRowState.Modified)
                            {
                                str2 = "";
                                Hashtable hashtable = new Hashtable();
                                for (num2 = 0; num2 < dataTable.Columns.Count; num2++)
                                {
                                    if (dataTable.Columns[num2].Expression == "")
                                    {
                                        str2 = AppendStringwithComma(str2, "[" + dataTable.Columns[num2].ColumnName + "] = @Update" + num.ToString() + dataTable.Columns[num2].ColumnName);
                                        parameter = new SqlParameter("@Update" + num.ToString() + dataTable.Columns[num2].ColumnName, dataTable.Rows[num][num2]);
                                        command.Parameters.Add(parameter);
                                        hashtable[num2] = dataTable.Rows[num][num2];
                                    }
                                }
                                dataTable.Rows[num].RejectChanges();
                                str4 = "";
                                num3 = 0;
                                while (num3 < dataTable.PrimaryKey.Length)
                                {
                                    if (dataTable.PrimaryKey[num3].DataType == typeof(string))
                                    {
                                        str4 = AppendStringwithAnd(str4, "[" + dataTable.PrimaryKey[num3].ColumnName + "] = N'" + dataTable.Rows[num][dataTable.PrimaryKey[num3].ColumnName].ToString().Replace("'", "''") + "'");
                                    }
                                    else
                                    {
                                        str4 = AppendStringwithAnd(str4, "[" + dataTable.PrimaryKey[num3].ColumnName + "] = @UpdatePk" + num.ToString() + dataTable.PrimaryKey[num3].ColumnName);
                                        parameter = new SqlParameter("@UpdatePk" + num.ToString() + dataTable.PrimaryKey[num3].ColumnName, dataTable.Rows[num][dataTable.PrimaryKey[num3].ColumnName]);
                                        command.Parameters.Add(parameter);
                                    }
                                    num3++;
                                }
                                string str5 = ("Update " + dataTable.TableName + " Set " + str2) + ((str4 == "") ? "" : (" where " + str4));
                                original = AppendCommand(original, str5);
                                for (int i = 0; i < dataTable.Columns.Count; i++)
                                {
                                    dataTable.Rows[num][i] = hashtable[i];
                                }
                            }
                            else if (dataTable.Rows[num].RowState == DataRowState.Deleted)
                            {
                                dataTable.Rows[num].RejectChanges();
                                str4 = "";
                                for (num3 = 0; num3 < dataTable.PrimaryKey.Length; num3++)
                                {
                                    if (dataTable.PrimaryKey[num3].DataType == typeof(string))
                                    {
                                        str4 = AppendStringwithAnd(str4, "[" + dataTable.PrimaryKey[num3].ColumnName + "] = N'" + dataTable.Rows[num][dataTable.PrimaryKey[num3].ColumnName].ToString().Replace("'", "''") + "'");
                                    }
                                    else
                                    {
                                        str4 = AppendStringwithAnd(str4, "[" + dataTable.PrimaryKey[num3].ColumnName + "] = @DeletePk" + num.ToString() + dataTable.PrimaryKey[num3].ColumnName);
                                        parameter = new SqlParameter("@DeletePk" + num.ToString() + dataTable.PrimaryKey[num3].ColumnName, dataTable.Rows[num][dataTable.PrimaryKey[num3].ColumnName]);
                                        command.Parameters.Add(parameter);
                                    }
                                }
                                string str6 = "Delete " + dataTable.TableName + " Where " + str4;
                                original = AppendCommand(original, str6);
                                dataTable.Rows[num].Delete();
                            }
                        }
                    }
                    try
                    {
                        command.Connection.Open();
                        command.CommandText = original;
                        flag = Convert.ToBoolean(command.ExecuteNonQuery());
                        dataTable.AcceptChanges();
                        command.Connection.Close();
                    }
                    catch (Exception exception1)
                    {
                        exception = exception1;
                        throw exception;
                    }
                }
                dataTable.AcceptChanges();
                return true;
            }
            catch (Exception exception2)
            {
                exception = exception2;
                this.ThrowException(new DSPEventArgs("DSP", "UpdateDataTable", exception));
                flag = false;
            }
            return flag;
        }

        public object SelectMax(string tableName, string columnName)
        {
            try
            {
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                columnName = columnName.TrimStart(new char[] { '[' });
                columnName = columnName.TrimEnd(new char[] { ']' });
                string selectquery = "SELECT MAX([" + columnName + "]) FROM [" + tableName + "]";
                this.m_strLaststatment = selectquery;
                return this.ExecuteScalar(selectquery);
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "SelectMax", exception));
                return null;
            }
        }

        private bool SetAdapter(DataSet table)
        {
            try
            {
                if (table == null)
                {
                    return false;
                }
                if (!this.m_HashTableAdapter.ContainsKey(table))
                {
                    return false;
                }
                this.m_Adapter = (IDbDataAdapter)this.m_HashTableAdapter[table];
                this.m_SqlCommandBuilder.DataAdapter = (SqlDataAdapter)this.m_Adapter;
                this.m_Adapter.UpdateCommand = this.m_SqlCommandBuilder.GetUpdateCommand();
                this.m_Adapter.InsertCommand = this.m_SqlCommandBuilder.GetInsertCommand();
                this.m_Adapter.DeleteCommand = this.m_SqlCommandBuilder.GetDeleteCommand();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool SetAdapter(DataTable table)
        {
            try
            {
                if (table == null)
                {
                    return false;
                }
                if (!this.m_HashTableAdapter.ContainsKey(table))
                {
                    return false;
                }
                this.m_Adapter = (IDbDataAdapter)this.m_HashTableAdapter[table];
                this.m_SqlCommandBuilder.DataAdapter = (SqlDataAdapter)this.m_Adapter;
                this.m_Adapter.UpdateCommand = this.m_SqlCommandBuilder.GetUpdateCommand();
                this.m_Adapter.InsertCommand = this.m_SqlCommandBuilder.GetInsertCommand();
                this.m_Adapter.DeleteCommand = this.m_SqlCommandBuilder.GetDeleteCommand();
                return true;
            }
            catch (Exception exception)
            {
                this.m_strLaststatment = "SetAdapter()";
                this.ThrowException(new DSPEventArgs("DSP", "SetAdapter", exception));
                return false;
            }
        }

        public void SetConnectionParameters(string serverName, string database, string userName, string password, bool trustedConnection)
        {
            try
            {
                this.ValidateParameter(serverName);
                this.ValidateParameter(database);
                this.ValidateParameter(userName);
                this.ValidateParameter(password);
                this.ValidateParameter(trustedConnection.ToString());
                this.m_ServerName = serverName;
                this.m_DataBase = database;
                this.m_UserName = userName;
                this.m_PassWord = password;
                this.m_trustedConnection = trustedConnection;
                this.BuildConnectionString();
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "SetConnectionParameters", exception));
            }
        }



        public void SetSeed(string Name, long seed, bool Overide)
        {
            this.m_Command.CommandText = "Delete from  AutoIncrementFields  where Name='" + Name + "'";
            this.m_Command.Connection = this.m_Connection;
            this.m_Command.ExecuteNonQuery();
            string str = string.Concat(new object[] { "Insert Into  AutoIncrementFields (Name,LastValue) Values('", Name, "',", seed, ")" });
            this.m_Command.CommandText = str;
            this.m_strLaststatment = str;
            this.m_Command.Connection = this.m_Connection;
            this.m_Command.ExecuteNonQuery();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private void SetSqlServerParams()
        {
            this.m_Connection = new SqlConnection();
            this.m_Command = new SqlCommand();
            this.m_Adapter = new SqlDataAdapter(" ", (SqlConnection)this.m_Connection);
            this.m_SqlCommandBuilder = new SqlCommandBuilder((SqlDataAdapter)this.m_Adapter);
        }

        internal static string StringAdd(string s, int n)
        {
            if (StringIsNullOrEmpty(s))
            {
                return s;
            }
            string str = "";
            for (int i = 0; i < s.Length; i++)
            {
                str = str + ((char)(s[i] + n));
            }
            return str;
        }

        private static bool StringIsNullOrEmpty(string s)
        {
            return ((s == null) || (s == ""));
        }

        internal static string StringMinus(string s, int n)
        {
            if (StringIsNullOrEmpty(s))
            {
                return s;
            }
            string str = "";
            for (int i = 0; i < s.Length; i++)
            {
                str = str + ((char)(s[i] - n));
            }
            return str;
        }

        public bool TestConnection()
        {
            try
            {
                if (this.m_ConnectionString == "")
                {
                    return false;
                }
                this.m_strLaststatment = "ping=OpenConnection()";
                bool flag = this.OpenConnection();
                if (flag)
                {
                    this.m_strLaststatment = "CloseConnection()";
                    this.CloseConnection();
                }
                return flag;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "TestConnection", exception));
                return false;
            }
        }

        private void ThrowException(DSPEventArgs e)
        {
            throw e.Exception;
        }

        private string ToDBString(object obj)
        {
            if (obj == null)
            {
                return "NULL";
            }
            if ((obj.GetType() == typeof(string)) || (obj.GetType() == typeof(DateTime)))
            {
                string str = obj.ToString();
                if (str.Length == 0)
                {
                    return "";
                }
                str.Replace("'", "''");
                return ("N'" + str + "'");
            }
            if (obj.GetType() == typeof(bool))
            {
                if (Convert.ToBoolean(obj))
                {
                    return "1";
                }
                return "0";
            }
            return obj.ToString();
        }

        private static object TranslateDBNull(System.Type t, object val)
        {
            if (t == typeof(DateTime))
            {
                return DateTime.MinValue;
            }
            if (t == typeof(int))
            {
                return -1;
            }
            if (t == typeof(short))
            {
                return (short)(-1);
            }
            if (t == typeof(double))
            {
                return 0.0;
            }
            return null;
        }

        private static object TranslateNullValueToDB(object val, System.Type t)
        {
            if (t == typeof(DateTime))
            {
                return DateTime.MinValue;
            }
            if (t == typeof(int))
            {
                return -1;
            }
            if (t == typeof(short))
            {
                return (short)(-1);
            }
            if (t == typeof(double))
            {
                return 0.0;
            }
            return "";
        }

        private bool TrimNullValue(ref string[] ColumnNames, ref object[] Values)
        {
            int num;
            ArrayList list = new ArrayList();
            StringCollection strings = new StringCollection();
            for (num = 0; num < ColumnNames.Length; num++)
            {
                if (Values[num] != null)
                {
                    strings.Add(ColumnNames[num]);
                    list.Add(Values[num]);
                }
            }
            int count = list.Count;
            if (count == 0)
            {
                return false;
            }
            ColumnNames = new string[count];
            Values = new object[count];
            for (num = 0; num < count; num++)
            {
                Values[num] = list[num];
                ColumnNames[num] = strings[num];
            }
            return true;
        }

        public bool Update(string tableName, string[] ColumnNames, object[] Values, string whereCriteria)
        {
            try
            {
                int num;
                if (ColumnNames.Length != Values.Length)
                {
                    return false;
                }
                tableName = tableName.TrimStart(new char[] { '[' });
                tableName = tableName.TrimEnd(new char[] { ']' });
                for (num = 0; num < ColumnNames.Length; num++)
                {
                    ColumnNames[num] = ColumnNames[num].TrimStart(new char[] { '[' });
                    ColumnNames[num] = ColumnNames[num].TrimEnd(new char[] { ']' });
                }
                string str = "";
                str = "UPDATE [";
                str = (str + tableName + "]  SET [") + ColumnNames[0] + "]=";


                SqlParameter parameter = new SqlParameter("@Par0", (Values[0] == null) ? System.DBNull.Value : Values[0]);
                this.m_Command.Parameters.Clear();
                this.m_Command.Parameters.Add(parameter);
                str = str + "@Par0";
                num = 1;
                while (num < ColumnNames.Length)
                {
                    parameter = new SqlParameter("@Par" + num.ToString(), (Values[num] == null) ? System.DBNull.Value : Values[num]);
                    str = (str + ", [") + ColumnNames[num] + "]=@Par" + num.ToString();
                    this.m_Command.Parameters.Add(parameter);
                    num++;
                }

                if (whereCriteria != null)
                {
                    str = str + ((whereCriteria.Length == 0) ? " " : (" WHERE " + whereCriteria));
                }
                this.m_Command.CommandText = str;
                this.m_Command.Connection = this.m_Connection;
                this.m_strLaststatment = str;
                this.m_strLaststatment = str;
                this.OpenConnection();
                this.m_Command.ExecuteNonQuery();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "Update", this.m_strLaststatment, exception));
                return false;
            }
        }

        public bool UpdateDataSet(DataSet ds)
        {
            try
            {
                if (!this.SetAdapter(ds))
                {
                    return false;
                }
                this.m_strLaststatment = "The following  statement is generated by a command builder of the DataAdapter.\n";
                this.m_strLaststatment = this.m_strLaststatment + "INSERTS:\n";
                this.m_strLaststatment = this.m_strLaststatment + this.m_Adapter.InsertCommand.CommandText + "\n";
                this.m_strLaststatment = this.m_strLaststatment + "UPDATES:\n";
                this.m_strLaststatment = this.m_strLaststatment + this.m_Adapter.UpdateCommand.CommandText + "\n";
                this.m_strLaststatment = this.m_strLaststatment + "DELETES:\n";
                this.m_strLaststatment = this.m_strLaststatment + this.m_Adapter.DeleteCommand.CommandText;
                ((SqlDataAdapter)this.m_Adapter).Update(ds);
                this.m_strLaststatment = "ds.AcceptChanges();";
                ds.AcceptChanges();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "UpdateDataSet", exception));
                return false;
            }
        }

        public bool UpdateDataTable(DataTable dataTable)
        {
            try
            {
                if (!this.SetAdapter(dataTable))
                {
                    return false;
                }
                DataTable changes = dataTable.GetChanges();
                this.m_strLaststatment = "The following  statement is generated by a command builder of the DataAdapter.\n";
                this.m_strLaststatment = this.m_strLaststatment + "INSERTS:\n";
                this.m_strLaststatment = this.m_strLaststatment + this.m_Adapter.InsertCommand.CommandText + "\n";
                this.m_strLaststatment = this.m_strLaststatment + "UPDATES:\n";
                this.m_strLaststatment = this.m_strLaststatment + this.m_Adapter.UpdateCommand.CommandText + "\n";
                this.m_strLaststatment = this.m_strLaststatment + "DELETES:\n";
                this.m_strLaststatment = this.m_strLaststatment + this.m_Adapter.DeleteCommand.CommandText;
                if (changes != null)
                {
                    ((SqlDataAdapter)this.m_Adapter).Update(changes);
                }
                this.m_strLaststatment = "dataTable.AcceptChanges();";
                dataTable.AcceptChanges();
                return true;
            }
            catch (Exception exception)
            {
                this.ThrowException(new DSPEventArgs("DSP", "UpdateDataTable", exception));
                return false;
            }
        }

        public int UpdateSingleTableRecord<T>(T rec)
        {
            return this.UpdateSingleTableRecord<T>(rec, new string[0], new object[0], null);
        }

        public int UpdateSingleTableRecord<T>(T rec, string AdditionalCriteria)
        {
            return this.UpdateSingleTableRecord<T>(rec, new string[0], new object[0], AdditionalCriteria);
        }

        public int UpdateSingleTableRecord<T>(T rec, string[] AdditionalFields, object[] AdditionalValues)
        {
            return this.UpdateSingleTableRecord<T>(rec, AdditionalFields, AdditionalValues, null);
        }

        public int UpdateSingleTableRecord<T>(T rec, string[] AdditionalFields, object[] AdditionalValues, string AdditionalCriteria)
        {
            string str3;
            string str4;
            object obj2;
            string str5;
            System.Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string str = null;
            string str2 = null;
            int num = 0;
            this.m_Command.Parameters.Clear();
            for (int i = 0; i < AdditionalFields.Length; i++)
            {
                str3 = AdditionalFields[i];
                str4 = "@Par" + num;
                if (string.IsNullOrEmpty(str))
                {
                    str = "[" + str3 + "]=" + str4;
                }
                else
                {
                    str5 = str;
                    str = str5 + ",[" + str3 + "]=" + str4;
                }
                obj2 = AdditionalValues[i];
                this.m_Command.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? System.DBNull.Value : obj2));
                num++;
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                System.Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if ((field is IDField) || field.updatable)
                        {
                            str3 = (field.fieldName == null) ? info.Name : field.fieldName;
                            str4 = "@Par" + num;
                            if (field is IDField)
                            {
                                if (string.IsNullOrEmpty(str2))
                                {
                                    str5 = str2;
                                    str2 = str5 + "[" + str3 + "]=" + str4;
                                }
                                else
                                {
                                    str5 = str2;
                                    str2 = str5 + " and [" + str3 + "]=" + str4;
                                }
                            }
                            else if (string.IsNullOrEmpty(str))
                            {
                                str = "[" + str3 + "]=" + str4;
                            }
                            else
                            {
                                str5 = str;
                                str = str5 + ",[" + str3 + "]=" + str4;
                            }
                            obj2 = info.GetValue(rec);
                            if (field is XMLField)
                            {
                                if (obj2 == null)
                                {
                                    obj2 = "";
                                }
                                else
                                {
                                    obj2 = ObjectToXml(obj2);
                                }
                            }
                            else if (!(field.allowNull || (obj2 != null)))
                            {
                                obj2 = TranslateNullValueToDB(obj2, info.FieldType);
                            }
                            this.m_Command.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? System.DBNull.Value : obj2));
                            num++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(str2))
            {
                str2 = "";
            }
            else
            {
                str2 = " WHERE " + str2;
            }
            if (!string.IsNullOrEmpty(AdditionalCriteria))
            {
                if (string.IsNullOrEmpty(str2))
                {
                    str2 = " WHERE " + AdditionalCriteria;
                }
                else
                {
                    str2 = str2 + " and (" + AdditionalCriteria + ")";
                }
            }
            this.m_Command.CommandText = "UPDATE [" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "] SET " + str + str2;
            this.OpenConnection();
            this.m_Command.Connection = this.m_Connection;
            if (this.m_boolIsTransactionPending)
            {
                this.m_Command.Transaction = this.m_Transaction;
            }
            return this.m_Command.ExecuteNonQuery();
        }

        private void ValidateParameter(string par)
        {
            if ((par != null) && (par.IndexOf(';') != -1))
            {
                throw new Exception("Invalid chracter ';' used in Connection string");
            }
        }

        public static object XmlToObject(string xml, System.Type type)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }
            XmlSerializer serializer = new XmlSerializer(type);
            return serializer.Deserialize(new StringReader(xml));
        }

        public IDbConnection Connection
        {
            get
            {
                return this.m_Connection;
            }
        }

        public string ConnectionString
        {
            get
            {
                return this.m_ConnectionString;
            }
            set
            {
                this.m_ConnectionString = value;
                this.CloseConnection();
                this.m_Connection.ConnectionString = this.m_ConnectionString;
            }
        }

        public int ConnectionTimeOut
        {
            get
            {
                return this.m_Connection.ConnectionTimeout;
            }
            set
            {
                this.m_strConnectionTimeOut = " Connection TimeOut=" + value + ";";
                this.BuildConnectionString();
            }
        }

        public string Database
        {
            get
            {
                return this.m_DataBase;
            }
        }

        public string DBNull
        {
            get
            {
                return "NULL";
            }
        }

        public bool IntegratedSecurity
        {
            get
            {
                return this.m_IntegratedSecurity;
            }
            set
            {
                this.m_IntegratedSecurity = value;
                this.m_ConnectionString = "Integrated Security=" + value.ToString() + ";";
                this.m_Connection.ConnectionString = this.m_ConnectionString;
            }
        }

        public string MultipleCharacterWildCard
        {
            get
            {
                return "%";
            }
        }

        public int PacketSize
        {
            get
            {
                if (this.m_Provider == DBProvider.MSSQLSERVER)
                {
                    return ((SqlConnection)this.m_Connection).PacketSize;
                }
                return -1;
            }
            set
            {
                if (this.m_Provider == DBProvider.MSSQLSERVER)
                {
                    this.m_strPacketSize = "Packet Size=" + value + ";";
                    this.BuildConnectionString();
                }
            }
        }

        public string Password
        {
            get
            {
                return this.m_PassWord;
            }
        }

        public string Server
        {
            get
            {
                return this.m_ServerName;
            }
        }

        public string SingleCharacterWildCard
        {
            get
            {
                switch (this.m_Provider)
                {
                    case DBProvider.ORACLE:
                        return "_";

                    case DBProvider.OLEDB:
                        return "?";

                    case DBProvider.ODBC:
                        return "?";
                }
                return "?";
            }
        }

        public IDbTransaction Transaction
        {
            get
            {
                return this.m_Transaction;
            }
        }

        public bool TrustedConnection
        {
            get
            {
                return this.m_trustedConnection;
            }
            set
            {
                if (this.m_trustedConnection != value)
                {
                    this.m_trustedConnection = value;
                    this.BuildConnectionString();
                }
            }
        }

        public string UserName
        {
            get
            {
                return this.m_UserName;
            }
        }
    }
}

