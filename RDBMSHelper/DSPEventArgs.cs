
    using System;
namespace INTAPS.RDBMS
{

    public class DSPEventArgs : EventArgs
    {
        private System.Exception m_Exception;
        private string m_strClass;
        private string m_strLastStatement;
        private string m_strMethod;

        public DSPEventArgs() : this("", "", "", new System.Exception("DSP Default Exception"))
        {
        }

        public DSPEventArgs(string className, string methodName, System.Exception ex) : this(className, methodName, "", ex)
        {
        }

        public DSPEventArgs(string className, string methodName, string lastStatement, System.Exception ex)
        {
            this.m_strClass = className;
            this.m_strMethod = methodName;
            this.m_strLastStatement = lastStatement;
            this.m_Exception = ex;
        }

        public override string ToString()
        {
            return (" Exception on  Class : " + this.m_strClass + "\n Method : " + this.m_strMethod + "\n Last attempted statemnet : " + this.m_strLastStatement + " \n Exception Message :" + this.m_Exception.Message);
        }

        public string ClassName
        {
            get
            {
                return this.m_strClass;
            }
            set
            {
                this.m_strClass = value;
            }
        }

        public System.Exception Exception
        {
            get
            {
                return this.m_Exception;
            }
            set
            {
                this.m_Exception = value;
            }
        }

        public string LastAttemptedStatement
        {
            get
            {
                return this.m_strLastStatement;
            }
            set
            {
                this.m_strLastStatement = value;
            }
        }

        public string MethodName
        {
            get
            {
                return this.m_strMethod;
            }
            set
            {
                this.m_strMethod = value;
            }
        }
    }
}

