
    using System;
namespace INTAPS.RDBMS
{

    public class DataField : Attribute
    {
        public bool allowNull;
        public string fieldName;
        public bool updatable;

        public DataField() : this(null, true, true)
        {
        }

        public DataField(bool allowNull) : this(null, allowNull, true)
        {
        }

        public DataField(string fn) : this(fn, true, true)
        {
        }

        public DataField(bool allowNull, bool updatable) : this(null, allowNull, updatable)
        {
        }

        public DataField(string fn, bool allowNull, bool updatable)
        {
            this.fieldName = fn;
            this.allowNull = allowNull;
            this.updatable = updatable;
        }
    }
}

