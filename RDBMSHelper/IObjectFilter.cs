using System;
namespace INTAPS.RDBMS
{

    public interface IObjectFilter<ObjectType>
    {
        bool Include(ObjectType obj, object[] additional);
    }
}

