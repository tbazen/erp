﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace INTAPS.RDBMS
{
    public class ObjectTable
    {
        static Dictionary<Type, ITypeIndexer> indexers = new Dictionary<Type, ITypeIndexer>();
        static Dictionary<int, Type> typeByID = new Dictionary<int, Type>();
        static Dictionary<Type, int> idByType = new Dictionary<Type, int>();

        public static int getTypeID(Type t, bool throwException)
        {
            object[] atr = t.GetCustomAttributes(typeof(TypeIDAttribute), false);
            TypeIDAttribute tatr;
            if (atr.Length == 0 || (tatr = atr[0] as TypeIDAttribute) == null)
            {
                if (throwException)
                    throw new Exception(string.Format("Type {0} do not have TypeID attribute ", t));
                return -1;
            }
            return tatr.id;
        }
        public static IEnumerable<Type> allIdiedTypes()
        {
            return typeByID.Values;
        }
        public static Type getTypeByID(int typeID)
        {
            Type ret;
            if (typeByID.TryGetValue(typeID, out ret))
                return ret;
            return null;
        }
        public static void loadTypeIDs(System.Reflection.Assembly a)
        {
            foreach (Type t in a.GetTypes())
            {
                int id = ObjectTable.getTypeID(t, false);
                if (id == -1)
                    continue;
                typeByID[id]= t;
                idByType[t]= id;
            }
        }
        

        static SQLHelper helper = null;
        public static void bindConnection(SQLHelper helper)
        {
            ObjectTable.helper = helper;
        }
        public static void loadTypes(Assembly assembly)
        {
            foreach (Type t in assembly.GetTypes())
            {
                Type i = t.GetInterface(typeof(ITypeIndexer).Name);
                if (i == null)
                    continue;
                ITypeIndexer ti = Activator.CreateInstance(t) as ITypeIndexer;
                indexers[ti.dataType] = ti;
                ti.checkAndFixStorage(helper);
            }
        }


        public static ITypeIndexer getIndexer(Type type)
        {
            ITypeIndexer ret;
            if (indexers.TryGetValue(type, out ret))
                return ret;
            return null;
        }
    }
    public abstract class SimpleObjectIndexer<ObjectType, IndexType> 
        where ObjectType :class,new()
        where IndexType : class,new()
    {

        public Type dataType
        {
            get { return typeof(ObjectType); }
        }

        public Type indexDataType
        {
            get { return typeof(IndexType); }
        }

        public void checkAndFixStorage(SQLHelper helper)
        {
            SQLHelper.TypeInformation typeInfo = SQLHelper.getTypeInformation(typeof(IndexType));
            if(typeInfo==null)
                throw new Exception("SingleTableAttribute not set for "+typeof(IndexType));
            string sql = @"SELECT count(*)
 FROM {0}.[INFORMATION_SCHEMA].[TABLES]
 where Table_Type='BASE TABLE' and Table_Name='{1}'";
            if (((int)helper.ExecuteScalar(string.Format(sql, mainDB, typeInfo.sourceTableName))) == 0)
            {
                createTable(helper, typeInfo);
                return;
            }

            sql = @"SELECT Column_Name
 FROM {0}.[INFORMATION_SCHEMA].[Columns]
 where Table_Name='{1}'";
            List<string> existingCols = new List<string>(helper.GetColumnArray<string>(string.Format(sql,indexDB,typeInfo.sourceTableName)));
            List<SQLHelper.TypeFieldInformation> missingFields = new List<SQLHelper.TypeFieldInformation>();
            foreach (SQLHelper.TypeFieldInformation field in typeInfo.fieldInformations)
            {
                if (existingCols.Contains(field.fieldAttribute.fieldName))
                    continue;
                missingFields.Add(field);
            }
            if (missingFields.Count > 0)
                updateTable(helper, typeInfo, missingFields);
        }

        private void updateTable(SQLHelper helper, SQLHelper.TypeInformation typeInfo, List<SQLHelper.TypeFieldInformation> missingFields)
        {
            string sql = ""; 
            foreach (SQLHelper.TypeFieldInformation f in missingFields)
            {
                sql += string.Format("ALTER TABLE {0}.dbo.{1} ADD", indexDB, typeInfo.sourceTableName);
                bool xml = false;
                if (f.fieldAttribute is XMLField)
                {
                    xml = true;
                }

                string fieldName;

                fieldName = "[" + f.fieldAttribute.fieldName + "]";
                sql += "\n" + fieldName + " ";
                if (xml)
                    sql += "ntext null\n";
                else
                {
                    if (f.fieldInfo.FieldType == typeof(int))
                        sql += "int null\n";
                    if (f.fieldInfo.FieldType == typeof(long))
                        sql += "bigint null\n";
                    if (f.fieldInfo.FieldType == typeof(double))
                        sql += "float null\n";
                    if (f.fieldInfo.FieldType == typeof(string))
                        sql += "nvarchar(1000) null\n";
                    if (f.fieldInfo.FieldType == typeof(bool))
                        sql += "bit null\n";
                    if (f.fieldInfo.FieldType == typeof(byte[]))
                        sql += "image null\n";
                    if (f.fieldInfo.FieldType.IsEnum)
                        sql += "int null\n";
                    if (f.fieldInfo.FieldType == typeof(DateTime))
                        sql += "datetime null\n";
                }                
            }
            helper.ExecuteNonQuery(sql);

        }

        private void createTable(SQLHelper helper, SQLHelper.TypeInformation typeInfo)
        {
            string sql ="__objectID varchar(50) not null,";
            foreach (SQLHelper.TypeFieldInformation f in typeInfo.fieldInformations)
            {
                if (f.fieldAttribute is XMLField)
                {
                    throw new Exception("XML field is not supported in object index tables");
                }

                string fieldName;

                fieldName = "[" + f.fieldAttribute.fieldName + "]";
                sql += "\n" + fieldName + " ";
                if (f.fieldInfo.FieldType == typeof(int))
                    sql += "int null,";
                if (f.fieldInfo.FieldType == typeof(long))
                    sql += "bigint null,";
                if (f.fieldInfo.FieldType == typeof(double))
                    sql += "float null,";
                if (f.fieldInfo.FieldType == typeof(string))
                    sql += "nvarchar(1000) null,";
                if (f.fieldInfo.FieldType == typeof(bool))
                    sql += "bit null,";
                if (f.fieldInfo.FieldType == typeof(byte[]))
                    sql += "image null,";
                if (f.fieldInfo.FieldType == typeof(DateTime))
                    sql += "datetime null,";
                if (f.fieldInfo.FieldType.IsEnum)
                    sql += "int null,";
            }
            sql = string.Format(@"CREATE TABLE {0}.[dbo].[{1}] 
(
{2}

CONSTRAINT [PK_{1}] PRIMARY KEY CLUSTERED (__objectID ASC)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]", indexDB,typeInfo.sourceTableName,sql);
            helper.ExecuteNonQuery(sql);
        }
        public virtual IndexType getIndexOf(ObjectType obj)
        {
            return obj as IndexType;
        }
        public void createIndex(SQLHelper helper, object obj)
        {
            string objectID = getObjectID((ObjectType)obj);
            deleteIndex(helper,objectID);
            helper.InsertSingleTableRecord(indexDB, getIndexOf((ObjectType)obj),new string[]{"__objectID"},new object[]{objectID});
        }

        public void deleteIndex(SQLHelper helper, string objID)
        {
            helper.Delete(indexDB, SQLHelper.getTypeInformation(typeof(IndexType)).sourceTableName, "__objectID='" + objID + "'");
        }
        public abstract string getObjectID(ObjectType obj);
        public abstract string mainDB { get; }
        public virtual string indexDB { get { return this.mainDB; } }
    }
}
