
    using System;
namespace INTAPS.RDBMS
{

    public enum OdbcDriver
    {
        Microsoft_Access_Driver = 3,
        Microsoft_Excel_Driver = 4,
        Microsoft_ODBC_for_Oracle = 2,
        Microsoft_Text_Driver = 5,
        SQL_Server = 1
    }
}

