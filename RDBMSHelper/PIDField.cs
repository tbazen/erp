
    using System;
namespace INTAPS.RDBMS
{

    public class PIDField : DataField
    {
        public PIDField() : base(null, false, true)
        {
        }

        public PIDField(bool updatable) : base(null, false, updatable)
        {
        }

        public PIDField(string fn) : base(fn, false, true)
        {
        }
    }
}

