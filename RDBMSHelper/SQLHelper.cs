using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace INTAPS.RDBMS
{
    public enum DataTimeBinding
    {
        LowerBound = 1,
        LowerAndUpperBound = 3
    }

    public class SQLHelper
    {
        internal class TypeFieldInformation
        {
            public System.Reflection.FieldInfo fieldInfo;
            public DataField fieldAttribute;
        }
        internal class TypeInformation
        {
            public SingleTableObjectAttribute tableAttribute;
            public List<TypeFieldInformation> fieldInformations;
            public string selectFieldList;
            public string sourceTableName;
        }
        static Dictionary<Type, TypeInformation> typeCache = new Dictionary<Type, TypeInformation>();
        internal static TypeInformation getTypeInformation(Type type)
        {
            lock(typeCache)
            {
                if (typeCache.ContainsKey(type))
                    return typeCache[type];
                object[] attributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
                if ((attributes == null) || (attributes.Length == 0))
                {
                    typeCache.Add(type, null);
                    return null;
                }
                object[] includeAttributes = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
                SingleTableObjectAttribute tableAttribute = (SingleTableObjectAttribute)attributes[0];
                List<TypeFieldInformation> infos = new List<TypeFieldInformation>();
                string selectFieldList = null;
                foreach (FieldInfo info in type.GetFields())
                {
                    bool includedField = false;
                    Type declaringType = info.DeclaringType;
                    foreach (STOIncludeAttribute incAtr in includeAttributes)
                    {
                        if (incAtr.includeType == declaringType)
                        {
                            includedField = true;
                            break;
                        }
                    }

                    if (includedField || (declaringType == type))
                    {
                        attributes = info.GetCustomAttributes(typeof(DataField), true);
                        if ((attributes != null) && (attributes.Length != 0))
                        {
                            DataField fieldAttribute = (DataField)attributes[0];
                            if (fieldAttribute.fieldName == null)
                                fieldAttribute.fieldName = info.Name;
                            string fieldName = fieldAttribute.fieldName;
                            if (string.IsNullOrEmpty(selectFieldList))
                            {
                                selectFieldList = "[" + fieldName + "]";
                            }
                            else
                            {
                                selectFieldList = selectFieldList + ",[" + fieldName + "]";
                            }
                            infos.Add(new TypeFieldInformation() { fieldInfo = info, fieldAttribute = fieldAttribute });
                        }
                    }
                }
                string sourceTableName = ((tableAttribute.tableName == null) ? type.Name : tableAttribute.tableName);
                TypeInformation ret = new TypeInformation()
                {
                    tableAttribute=tableAttribute,
                    sourceTableName=sourceTableName,
                    fieldInformations=infos,
                    selectFieldList=selectFieldList,
                };
                typeCache.Add(type, ret);
                return ret;
            }
        }
        
        private SqlConnection m_con = null;
        private string m_conStr = null;
        private string m_readDB = null;
        private List<string> m_readerDBStack = new List<string>();
        private int m_stackPosition = 0;
        private SqlTransaction m_tran = null;
        private int m_transactionCounter;
        public SQLHelper(string conStr)
        {
            this.m_conStr = conStr;
            this.m_transactionCounter = 0;
        }

        public void BeginTransaction()
        {
            lock (this.m_con)
            {
                if (this.m_transactionCounter == 0)
                {
                    this.m_tran = this.m_con.BeginTransaction(IsolationLevel.ReadCommitted);
                }
                this.m_transactionCounter++;
            }
        }

        public static object BinaryToObject(byte[] data)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream serializationStream = new MemoryStream(data);
            return formatter.Deserialize(serializationStream);
        }

        private void CheckAndConnect()
        {
            if ((this.m_con == null) || (this.m_con.State != ConnectionState.Open))
            {
                this.m_con = new SqlConnection(this.m_conStr);
                this.m_con.Open();
            }
        }
        public void CheckTransactionIntegrityWithoutReadDB()
        {
            lock (this.m_con)
            {
                if (this.m_transactionCounter != 0)
                {
                    throw new Exception("Tranaction integrity check failed. Transaction Count:" + this.m_transactionCounter);
                }
            }
        }
        public void CheckTransactionIntegrity()
        {
            lock (this.m_con)
            {
                if (this.m_transactionCounter != 0)
                {
                    throw new Exception("Tranaction integrity check failed. Transaction Count:" + this.m_transactionCounter);
                }
                if (this.m_stackPosition != 0)
                {
                    try
                    {
                        throw new Exception();
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Reader DB name stack not cleared. Stack size:" + this.m_stackPosition);
                        Console.WriteLine(exception.StackTrace);
                    }
                }
            }
        }

        public SQLHelper Clone()
        {
            return new SQLHelper(this.m_conStr);
        }

        public void CloseConnection()
        {
            if (this.m_con != null)
            {
                this.m_con.Close();
            }
        }

        public void CommitTransaction()
        {
            lock (this.m_con)
            {
                this.m_transactionCounter--;
                if (this.m_transactionCounter < 0)
                {
                    throw new Exception("No transaction to commit");
                }
                if (this.m_transactionCounter == 0)
                {
                    this.m_tran.Commit();
                    this.m_tran = null;
                }
            }
        }

        internal SqlCommand CreatCommand(string sql)
        {
            SqlCommand cmd = new SqlCommand(sql);
            cmd.CommandTimeout = 0;
            this.SetConnection(cmd);
            return cmd;
        }

        public int Delete(string DB, string tableName, string whereCriteria)
        {
            tableName = tableName.TrimStart(new char[] { '[' });
            tableName = tableName.TrimEnd(new char[] { ']' });
            if (string.IsNullOrEmpty(DB))
            {
                tableName = "[" + tableName + "]";
            }
            else
            {
                tableName = DB + ".dbo.[" + tableName + "]";
            }
            string sql = "";
            sql = "DELETE FROM ";
            sql = sql + tableName + " ";
            if (whereCriteria != null)
            {
                sql = sql + ((whereCriteria.Length == 0) ? " " : (" WHERE " + whereCriteria));
            }
            lock (this.m_con)
            {
                return this.ExecuteNonQuery(sql);
            }
        }

        public int DeleteSingleTableRecrod<T>(string DB, params object[] key)
        {
            return this.DeleteSingleTableRecrod<T>(DB, key, null);
        }

        public int DeleteSingleTableRecrod<T>(string DB, object[] key, string AdditionalCriteria)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            cmd.Parameters.Clear();
            int index = 0;
            string str = null;
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if (field is IDField)
                        {
                            string str5;
                            string str2 = (field.fieldName == null) ? info.Name : field.fieldName;
                            string str3 = "@Par" + index;
                            if (string.IsNullOrEmpty(str))
                            {
                                str5 = str;
                                str = str5 + "[" + str2 + "]=" + str3;
                            }
                            else
                            {
                                str5 = str;
                                str = str5 + " and [" + str2 + "]=" + str3;
                            }
                            object val = key[index];
                            if (!(field.allowNull || (val != null)))
                            {
                                val = TranslateNullValueToDB(val, info.FieldType);
                            }
                            cmd.Parameters.Add(new SqlParameter("@Par" + index, (val == null) ? DBNull.Value : val));
                            index++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(str))
            {
                str = "";
            }
            else
            {
                str = " WHERE " + str;
            }
            if (!string.IsNullOrEmpty(AdditionalCriteria))
            {
                if (string.IsNullOrEmpty(str))
                {
                    str = " WHERE " + AdditionalCriteria;
                }
                else
                {
                    str = str + " and (" + AdditionalCriteria + ")";
                }
            }
            string str4 = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (!string.IsNullOrEmpty(DB))
            {
                str4 = DB + ".dbo." + str4;
            }
            cmd.CommandText = "DELETE FROM " + str4 + " " + str;
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }

        public static string EscapeString(string query)
        {
            return query.Replace("[", "[[]").Replace("%", "[%]").Replace("_", "[_]").Replace("'", "['']");
        }

        public int ExecuteNonQuery(string sql)
        {
            lock (this.m_con)
            {
                return this.CreatCommand(sql).ExecuteNonQuery();
            }
        }

        public SqlDataReader ExecuteReader(string sql)
        {
            return this.CreatCommand(sql).ExecuteReader();
        }

        public object ExecuteScalar(string sql)
        {
            return this.CreatCommand(sql).ExecuteScalar();
        }

        public static string FieldObjectToXml(object o, FieldInfo field, Type classType)
        {
            XmlSerializer serializer;
            object[] customAttributes = field.GetCustomAttributes(typeof(XMLFieldIncludeAttribute), true);
            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                if (customAttributes.Length > 1)
                {
                    throw new Exception("Only one XMLFieldInclude attribute per field is allowed");
                }
                serializer = new XmlSerializer(field.FieldType, ((XMLFieldIncludeAttribute)customAttributes[0]).includeTypes);
            }
            else
            {
                serializer = new XmlSerializer(field.FieldType);
            }
            StringWriter writer = new StringWriter();
            serializer.Serialize((TextWriter)writer, o);
            writer.Close();
            return writer.ToString();
        }

        public static object FieldXmlToObject(string xml, FieldInfo field, Type classType)
        {
            object[] customAttributes = field.GetCustomAttributes(typeof(XMLFieldIncludeAttribute), true);
            if ((customAttributes != null) && (customAttributes.Length > 0))
            {
                if (customAttributes.Length > 1)
                {
                    throw new Exception("Only one XMLFieldInclude attribute per field is allowed");
                }
                return XmlToObject(xml, field.FieldType, ((XMLFieldIncludeAttribute)customAttributes[0]).includeTypes);
            }
            return XmlToObject(xml, field.FieldType, null);
        }
        public T[] GetColumnArray<T>(string sql)
        {
            int N;
            return GetColumnArray<T>(sql, 0, 0, -1, out N);
        }
        public T[] GetColumnArray<T>(string sql, int col)
        {
            int N;
            return GetColumnArray<T>(sql, col, 0, -1, out N);
        }

        public T[] GetColumnArray<T>(string sql, int col, int index, int pageSize, out int N)
        {
            List<T> list = new List<T>();
            SqlDataReader reader = this.ExecuteReader(sql);
            try
            {
                int i = 0;
                while (reader.Read())
                {
                    if ((pageSize == -1) || ((i >= index) && (i < (index + pageSize))))
                    {
                        list.Add((T)reader[col]);
                    }
                    i++;

                }
                N = i;
                return list.ToArray();
            }
            finally
            {
                if (!((reader == null) || reader.IsClosed))
                {
                    reader.Close();
                }
            }
        }

        public DataTable GetDataTable(string sql)
        {
            SqlDataAdapter adapter = new SqlDataAdapter(this.CreatCommand(sql));
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            return dataTable;
        }

        public SQLObjectReader<T> GetObjectReader<T>(string filter) where T : new()
        {
            return this.GetObjectReader<T>(filter, new string[0], null);
        }

        public SQLObjectReader<T> GetObjectReader<T>(string filter, string orderby) where T : new()
        {
            return this.GetObjectReader<T>(filter, new string[0], orderby);
        }

        public SQLObjectReader<T> GetObjectReader<T>(string filter, string[] aditionalFields) where T : new()
        {
            return this.GetObjectReader<T>(filter, aditionalFields, null);
        }

        public SQLObjectReader<T> GetObjectReader<T>(string filter, string[] aditionalFields, string orderby) where T : new()
        {
            return new SQLObjectReader<T>(this, filter, aditionalFields, orderby);
        }

        public SqlConnection getOpenConnection()
        {
            this.CheckAndConnect();
            return this.m_con;
        }
        public T[] GetSTRArrayByFilterVersioned<T>(string filter, long version) where T : new()
        {
            object[][] objArray;
            int num;

            return this.GetSTRArrayByFilter<T>(getVersionDataFilter(version, filter), null,new string[0], out objArray, 0, -1, out num, null);
        }
        public long GetSTRNextVersionDate<T>(string filter, long version) where T : new()
        {

            Type type = typeof(T);
            object[] atr = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((atr == null) || (atr.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objAtr = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)atr[0];

            string sourceTableName = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (this.m_readDB != null)
            {
                sourceTableName = this.m_readDB + ".dbo." + sourceTableName;
            }
            string dateFilter = "ticksFrom>" + version + "";
            string sql = "Select min(ticksFrom) FROM {0} where {1}{2}";
            sql = string.Format(sql, sourceTableName, dateFilter, (string.IsNullOrEmpty(filter) ? "" : (" and " + filter)));
            object ret = ExecuteScalar(sql);
            if (ret is DateTime)
                return (long)ret;
            return -1;
        }
        public long GetSTRPreviousVersionDate<T>(string filter, long version) where T : new()
        {

            Type type = typeof(T);
            object[] atr = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((atr == null) || (atr.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objAtr = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)atr[0];

            string sourceTableName = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (this.m_readDB != null)
            {
                sourceTableName = this.m_readDB + ".dbo." + sourceTableName;
            }
            string dateFilter = "(ticksFrom<" + version + ")";
            string sql = "Select max(ticksFrom) FROM {0} where {1}{2}";
            sql = string.Format(sql, sourceTableName, dateFilter, (string.IsNullOrEmpty(filter) ? "" : (" and " + filter)));
            object ret = ExecuteScalar(sql);
            if (ret is long)
                return (long)ret;
            return -1;
        }
        public T[] GetSTRArrayByFilter<T>(string filter) where T : new()
        {
            object[][] objArray;
            int num;
            return this.GetSTRArrayByFilter<T>(filter,null,new string[0], out objArray, 0, -1, out num, null);
        }
        public T[] GetSTRArrayByFilter<T>(string filter,string orderby) where T : new()
        {
            object[][] objArray;
            int num;
            return this.GetSTRArrayByFilter<T>(filter, orderby, new string[0], out objArray, 0, -1, out num, null);
        }
        public void GetSTRArrayByFilter<T>(T at, string filter) where T : class, new()
        {
            DataField field;
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            string str = null;
            List<FieldInfo> list = new List<FieldInfo>();
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        list.Add(info);
                        field = (DataField)customAttributes[0];
                        string str2 = (field.fieldName == null) ? info.Name : field.fieldName;
                        if (string.IsNullOrEmpty(str))
                        {
                            str = "[" + str2 + "]";
                        }
                        else
                        {
                            str = str + ",[" + str2 + "]";
                        }
                    }
                }
            }
            string str3 = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (this.m_readDB != null)
            {
                str3 = this.m_readDB + ".dbo." + str3;
            }
            string sql = "Select " + str + " FROM " + str3 + " " + (string.IsNullOrEmpty(filter) ? "" : (" WHERE " + filter));
            DataTable dataTable = new DataTable();
            new SqlDataAdapter(this.CreatCommand(sql)).Fill(dataTable);
            T[] localArray = new T[dataTable.Rows.Count];
            foreach (DataRow row in dataTable.Rows)
            {
                int num = 0;
                foreach (FieldInfo info in list)
                {
                    field = (DataField)info.GetCustomAttributes(typeof(DataField), true)[0];
                    object val = row[num];
                    if (val is DBNull)
                    {
                        val = TranslateDBNull(info.FieldType, val);
                    }
                    info.SetValue(at, val);
                    num++;
                }
                break;
            }
        }

        public T[] GetSTRArrayByFilter<T>(string filter, string[] AdditionalFields, out object[][] AdditionalData) where T : new()
        {
            int num;
            return this.GetSTRArrayByFilter<T>(filter, null,AdditionalFields, out AdditionalData, 0, -1, out num, null);
        }

        public T[] GetSTRArrayByFilter<T>(string filter, int index, int pageSize, out int NRecords) where T : new()
        {
            object[][] additionalData = new object[0][];
            return this.GetSTRArrayByFilter<T>(filter, null,new string[0], out additionalData, index, pageSize, out NRecords, null);
        }

        public T[] GetSTRArrayByFilter<T>(string filter, int index, int pageSize, out int NRecords, IObjectFilter<T> objectFilter) where T : new()
        {
            object[][] additionalData = new object[0][];
            return this.GetSTRArrayByFilter<T>(filter, null,new string[0], out additionalData, index, pageSize, out NRecords, objectFilter);
        }

        public T[] GetSTRArrayByFilter<T>(string filter, string orderBy,string[] AdditionalFields, out object[][] AdditionalData, int index, int pageSize, out int NRecords, IObjectFilter<T> objectFilter) where T : new()
        {
            int _nrec;
            TypeInformation typeInfo = getTypeInformation(typeof(T));
            if(typeInfo==null)
                throw new Exception(typeof(T)+ " is not marked SingleTableRecord");
            
            string selectFieldList=typeInfo.selectFieldList;
            for (_nrec = 0; _nrec < AdditionalFields.Length; _nrec++)
            {
                if (string.IsNullOrEmpty(selectFieldList))
                {
                    selectFieldList = "[" + AdditionalFields[_nrec] + "]";
                }
                else
                {
                    selectFieldList = selectFieldList + ",[" + AdditionalFields[_nrec] + "]";
                }
            }

            string sourceTableName = typeInfo.sourceTableName;
            if (this.m_readDB != null)
            {
                sourceTableName = this.m_readDB + ".dbo." + sourceTableName;
            }
            string sql = "Select " + selectFieldList + " FROM " + sourceTableName + (string.IsNullOrEmpty(filter) ? "" : (" WHERE " + filter));
            if (!string.IsNullOrEmpty(orderBy))
            {
                sql = sql + " order by " + orderBy;
            }
            else if (!string.IsNullOrEmpty(typeInfo.tableAttribute.orderBy))
            {
                sql = sql + " order by " + typeInfo.tableAttribute.orderBy;
            }
            
            SqlCommand command = this.CreatCommand(sql);
            SqlDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
                List<T> _ret = new List<T>();
                List<object[]> _additional = new List<object[]>();
                _nrec = 0;
                while (reader.Read())
                {

                    if (((objectFilter == null) && (pageSize != -1)) && ((_nrec < index) || (_nrec >= (index + pageSize))))
                    {
                        _nrec++;
                    }
                    else
                    {
                        T local2 = default(T);
                        T local = (local2 == null) ? Activator.CreateInstance<T>() : (local2 = default(T));
                        object[] additional = new object[AdditionalFields.Length];
                        int readerIndex = 0;
                        
                        foreach (TypeFieldInformation info in typeInfo.fieldInformations)
                        {
                            object val = reader[readerIndex];
                            if (info.fieldAttribute is XMLField)
                            {
                                if (string.IsNullOrEmpty(val as string))
                                {
                                    val = null;
                                }
                                else
                                {
                                    val = FieldXmlToObject((string)val, info.fieldInfo, typeof(T));
                                }
                            }
                            if (val is DBNull)
                            {
                                val = TranslateDBNull(info.fieldInfo.FieldType, val);
                            }
                            info.fieldInfo.SetValue(local, val);
                            readerIndex++;
                        }
                        for (int i = 0; i < AdditionalFields.Length; i++)
                        {
                            additional[i] = reader[readerIndex];
                            readerIndex++;
                        }
                        if ((objectFilter != null) && !objectFilter.Include(local, additional))
                            continue;

                        if (pageSize == -1 || (_nrec >= index && _nrec < (index + pageSize)))
                        {
                            _additional.Add(additional);
                            _ret.Add(local);
                        }
                        _nrec++;

                    }
                }
                NRecords = _nrec;
                AdditionalData = _additional.ToArray();
                return _ret.ToArray();
            }
            finally
            {
                if (!((reader == null) || reader.IsClosed))
                {
                    reader.Close();
                }
            }
        }

        public T[] GetSTRArrayBySQL<T>(string sql) where T : new()
        {
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            DataTable dataTable = new DataTable();
            new SqlDataAdapter(this.CreatCommand(sql)).Fill(dataTable);
            T[] localArray = new T[dataTable.Rows.Count];
            int num = 0;
            foreach (DataRow row in dataTable.Rows)
            {
                T local2 = default(T);
                T local = (local2 == null) ? Activator.CreateInstance<T>() : (local2 = default(T));
                int num2 = 0;
                foreach (FieldInfo info in type.GetFields())
                {
                    if (info.DeclaringType == type)
                    {
                        customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                        if ((customAttributes != null) && (customAttributes.Length != 0))
                        {
                            DataField field = (DataField)customAttributes[0];
                            string str = (field.fieldName == null) ? info.Name : field.fieldName;
                            object val = row[str];
                            if (val is DBNull)
                            {
                                val = TranslateDBNull(info.FieldType, val);
                            }
                            info.SetValue(local, val);
                            num2++;
                        }
                    }
                }
                localArray[num++] = local;
            }
            return localArray;
        }

        public void GetSTRArrayBySQL<T>(T at, string sql) where T : class, new()
        {
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            DataTable dataTable = new DataTable();
            new SqlDataAdapter(this.CreatCommand(sql)).Fill(dataTable);
            T[] localArray = new T[dataTable.Rows.Count];
            foreach (DataRow row in dataTable.Rows)
            {
                int num = 0;
                foreach (FieldInfo info in type.GetFields())
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        string str = (field.fieldName == null) ? info.Name : field.fieldName;
                        object val = row[str];
                        if (val is DBNull)
                        {
                            val = TranslateDBNull(info.FieldType, val);
                        }
                        info.SetValue(at, val);
                        num++;
                    }
                }
                break;
            }
        }

        public object[] GetSystemParameters<KVPType>(KVPType sysPars, string[] fields) where KVPType : class, new()
        {
            object[] objArray = new object[fields.Length];
            FieldInfo[] infoArray = typeof(KVPType).GetFields();
            for (int i = 0; i < fields.Length; i++)
            {
                foreach (FieldInfo info in infoArray)
                {
                    if (fields[i] == info.Name)
                    {
                        objArray[i] = info.GetValue(sysPars);
                        break;
                    }
                }
            }
            return objArray;
        }

        private SqlTransaction getTransaction()
        {
            return this.m_tran;
        }

        public int Insert(string DB, string tableName, string[] ColumnNames, object[] Values)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            if (ColumnNames.Length != Values.Length)
            {
                throw new Exception("Column count doesn't match value count");
            }
            tableName = tableName.TrimStart(new char[] { '[' });
            tableName = tableName.TrimEnd(new char[] { ']' });
            if (string.IsNullOrEmpty(DB))
            {
                tableName = "[" + tableName + "]";
            }
            else
            {
                tableName = DB + ".dbo.[" + tableName + "]";
            }
            int index = 0;
            while (index < ColumnNames.Length)
            {
                ColumnNames[index] = ColumnNames[index].TrimStart(new char[] { '[' });
                ColumnNames[index] = ColumnNames[index].TrimEnd(new char[] { ']' });
                index++;
            }
            string str = "";
            str = "INSERT INTO ";
            str = (str + tableName) + " ( [" + ColumnNames[0];
            for (index = 1; index < ColumnNames.Length; index++)
            {
                str = str + "],[" + ColumnNames[index];
            }
            str = str + "] ) VALUES (";
            SqlParameter parameter = new SqlParameter("@Par0", (Values[0] == null) ? DBNull.Value : Values[0]);
            cmd.Parameters.Clear();
            cmd.Parameters.Add(parameter);
            str = str + "@Par0";
            for (index = 1; index < Values.Length; index++)
            {
                parameter = new SqlParameter("@Par" + index.ToString(), (Values[index] == null) ? DBNull.Value : Values[index]);
                str = (str + ",") + "@Par" + index.ToString();
                cmd.Parameters.Add(parameter);
            }
            str = str + ")";
            cmd.CommandText = str;
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }

        public void InsertColumn<T>(string DB, string Table, string[] cols, object[] fvals, T[] colvals)
        {
            if (fvals.Length != (cols.Length - 1))
            {
                throw new Exception("Column count mismatch");
            }
            object[] array = new object[fvals.Length + 1];
            fvals.CopyTo(array, 0);
            foreach (T local in colvals)
            {
                array[fvals.Length] = local;
                this.Insert(DB, Table, cols, array);
            }
        }

        public int InsertSingleTableRecord<T>(string DB, T rec)
        {
            return this.InsertSingleTableRecord<T>(DB, rec, new string[0], new object[0]);
        }

        public int InsertSingleTableRecord(string DB, object rec, string[] AdditionalFields, object[] AdditionalValues)
        {
            return this.InsertSingleTableRecord(DB, rec, AdditionalFields, AdditionalValues, null);
        }

        public int InsertSingleTableRecord<T>(string DB, T rec, string[] AdditionalFields, object[] AdditionalValues)
        {
            return this.InsertSingleTableRecord<T>(DB, rec, AdditionalFields, AdditionalValues, null);
        }

        public int InsertSingleTableRecord(string DB, object rec, string[] AdditionalFields, object[] AdditionalValues, string insertIntoTableName)
        {
            object parValue;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            Type type = rec.GetType();
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] atr = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string sqlFields = "(";
            string sqlPars = "(";
            int num = 0;
            cmd.Parameters.Clear();
            if (AdditionalFields != null)
            {
                for (int i = 0; i < AdditionalFields.Length; i++)
                {
                    if (num > 0)
                    {
                        sqlFields = sqlFields + ",";
                        sqlPars = sqlPars + ",";
                    }
                    sqlFields = sqlFields + "[" + AdditionalFields[i] + "]";
                    sqlPars = sqlPars + "@Par" + num;
                    parValue = AdditionalValues[i];
                    cmd.Parameters.Add(new SqlParameter("@Par" + num, (parValue == null) ? DBNull.Value : parValue));
                    num++;
                }
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in atr)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if (!(field is IDField) || !((IDField)field).autoGenerated)
                        {
                            if (num > 0)
                            {
                                sqlFields = sqlFields + ",";
                                sqlPars = sqlPars + ",";
                            }
                            sqlFields = sqlFields + "[" + ((field.fieldName == null) ? info.Name : field.fieldName) + "]";
                            sqlPars = sqlPars + "@Par" + num;
                            parValue = info.GetValue(rec);
                            if (field is XMLField)
                            {
                                if (parValue == null)
                                {
                                    parValue = "";
                                }
                                else
                                {
                                    parValue = FieldObjectToXml(parValue, info, type.GetType());
                                }
                            }
                            else if (!(field.allowNull || (parValue != null)))
                            {
                                parValue = TranslateNullValueToDB(parValue, info.FieldType);
                            }
                            cmd.Parameters.Add(new SqlParameter("@Par" + num, (parValue == null) ? DBNull.Value : parValue));
                            num++;
                        }
                    }
                }
            }
            string str3 = (insertIntoTableName == null) ? ("[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]") : insertIntoTableName;
            if (!string.IsNullOrEmpty(DB))
            {
                str3 = DB + ".dbo." + str3;
            }
            cmd.CommandText = "Insert into " + str3 + sqlFields + ") values " + sqlPars + ")";
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }

        public int InsertSingleTableRecord<T>(string DB, T rec, string[] AdditionalFields, object[] AdditionalValues, string insertIntoTableName)
        {
            object obj2;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string str = "(";
            string str2 = "(";
            int num = 0;
            cmd.Parameters.Clear();
            for (int i = 0; i < AdditionalFields.Length; i++)
            {
                if (num > 0)
                {
                    str = str + ",";
                    str2 = str2 + ",";
                }
                str = str + "[" + AdditionalFields[i] + "]";
                str2 = str2 + "@Par" + num;
                obj2 = AdditionalValues[i];
                cmd.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? DBNull.Value : obj2));
                num++;
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if (num > 0)
                        {
                            str = str + ",";
                            str2 = str2 + ",";
                        }
                        str = str + "[" + ((field.fieldName == null) ? info.Name : field.fieldName) + "]";
                        str2 = str2 + "@Par" + num;
                        obj2 = info.GetValue(rec);
                        if (field is XMLField)
                        {
                            if (obj2 == null)
                            {
                                obj2 = "";
                            }
                            else
                            {
                                obj2 = FieldObjectToXml(obj2, info, typeof(T));
                            }
                        }
                        else if (!(field.allowNull || (obj2 != null)))
                        {
                            obj2 = TranslateNullValueToDB(obj2, info.FieldType);
                        }
                        cmd.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? DBNull.Value : obj2));
                        num++;
                    }
                }
            }
            string sql = (insertIntoTableName == null) ? ("[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]") : insertIntoTableName;
            if (!string.IsNullOrEmpty(DB))
            {
                sql = DB + ".dbo." + sql;
            }
            cmd.CommandText = "Insert into " + sql + str + ") values " + str2 + ")";
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }

        public KVPType LoadSystemParameters<KVPType>() where KVPType : class, new()
        {
            KVPType local = Activator.CreateInstance<KVPType>();
            DataTable dataTable = this.GetDataTable(string.Format("Select ParName,ParValue from {0}.dbo.SystemParameter",this.ReadDB));
            FieldInfo[] fields = typeof(KVPType).GetFields();
            foreach (DataRow row in dataTable.Rows)
            {
                string str = row[1] as string;
                string str2 = (string)row[0];
                foreach (FieldInfo info in fields)
                {
                    if (info.Name == str2)
                    {
                        object[] customAttributes = info.FieldType.GetCustomAttributes(typeof(LoadFromSettingAttribute), true);
                        if ((customAttributes != null) && (customAttributes.Length > 0))
                        {
                            if (string.IsNullOrEmpty(str))
                            {
                                info.SetValue(local, null);
                            }
                            else
                            {
                                info.SetValue(local, FieldXmlToObject(str, info, typeof(KVPType)));
                            }
                        }
                        else if (info.FieldType.IsEnum)
                        {
                            info.SetValue(local, Enum.Parse(info.FieldType, str));
                        }
                        else if (info.FieldType.IsEnum)
                        {
                            info.SetValue(local, Enum.Parse(info.FieldType, str));
                        }
                        else
                        {
                            info.SetValue(local, Convert.ChangeType(str, info.FieldType));
                        }
                    }
                }
            }
            return local;
        }

        public void logDeletedData(int DAID, string qualifiedTableName, string criteria)
        {
            this.ExecuteNonQuery(string.Format("insert into {0}_Deleted Select *,{1} as __DAID from {0} where {2}", qualifiedTableName, DAID, criteria));
        }
        public void logDeletedData(int DAID, string DBName, string tableName, string criteria)
        {
            this.ExecuteNonQuery(string.Format("insert into {0}.dbo.[{1}_Deleted] Select *,{2} as __DAID from {0}.dbo.[{1}] where {3}", DBName,tableName, DAID, criteria));
        }
        public static byte[] ObjectToBinary(object o)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream serializationStream = new MemoryStream();
            formatter.Serialize(serializationStream, o);
            byte[] buffer = new byte[serializationStream.Length];
            serializationStream.Seek(0L, SeekOrigin.Begin);
            serializationStream.Read(buffer, 0, buffer.Length);
            return buffer;
        }

        

        public void restoreReadDB()
        {
            if(this.m_stackPosition>0)
                this.m_stackPosition--;
            if ((this.m_stackPosition >= 0) && (this.m_stackPosition < this.m_readerDBStack.Count))
            {
                this.m_readDB = this.m_readerDBStack[this.m_stackPosition];
            }
        }

        public void RollBackTransaction()
        {
            lock (this.m_con)
            {
                this.m_transactionCounter--;
                if (this.m_transactionCounter < 0)
                {
                    throw new Exception("No transaction to rollback");
                }
                if (this.m_transactionCounter == 0)
                {
                    this.m_tran.Rollback();
                    this.m_tran = null;
                }
            }
        }

        private void SetConnection(SqlCommand cmd)
        {
            this.CheckAndConnect();
            cmd.Connection = this.m_con;
            if (this.m_tran != null)
            {
                cmd.Transaction = this.m_tran;
            }
        }

        public void setReadDB(string value)
        {
            if (this.m_stackPosition == this.m_readerDBStack.Count)
            {
                this.m_readerDBStack.Add(this.m_readDB);
                this.m_stackPosition++;
            }
            else
            {
                this.m_readerDBStack[this.m_stackPosition++] = this.m_readDB;
            }
            this.m_readDB = value;
        }

        public void setReadDBUnrestorable(string value)
        {
            this.m_readDB = value;
        }

        public void SetSystemParameters<KVPType>(string DB, KVPType sysPars, string[] fields, object[] vals) where KVPType : class, new()
        {
            this.SetSystemParameters<KVPType>(DB, sysPars, fields, vals, false, -1);
        }

        public void SetSystemParameters<KVPType>(string DB, KVPType sysPars) where KVPType : class, new()
        {
            System.Reflection.FieldInfo[] fi = typeof(KVPType).GetFields();
            string[] fields = new string[fi.Length];
            object[] vals = new object[fi.Length];
            for (int i = 0; i < fi.Length; i++)
            {
                fields[i] = fi[i].Name;
                vals[i] = fi[i].GetValue(sysPars);
            }
            SetSystemParameters<KVPType>(DB, sysPars, fields, vals);
        }
        public void SetSystemParameters<KVPType>(string DB, KVPType sysPars, string[] fields, object[] vals, bool logChange, int AID) where KVPType : class, new()
        {
            Type type = typeof(KVPType);
            foreach (FieldInfo info in type.GetFields())
            {
                int index = -1;
                index = 0;
                while (index < fields.Length)
                {
                    if (fields[index] == info.Name)
                    {
                        break;
                    }
                    index++;
                }
                if (index != fields.Length)
                {
                    object[] customAttributes = info.FieldType.GetCustomAttributes(typeof(LoadFromSettingAttribute), true);
                    bool fieldValueChanged = false;
                    string str = null;
                    if ((customAttributes != null) && (customAttributes.Length > 0))
                    {
                        object o = vals[index];
                        if (o == null)
                        {
                            str = null;
                        }
                        else
                        {
                            str = FieldObjectToXml(o, info, typeof(KVPType));
                        }
                        fieldValueChanged = true;
                    }
                    else //if (!vals[index].Equals(info.GetValue(sysPars)))
                    {
                        str = Convert.ChangeType(vals[index], typeof(string)) as string;
                        fieldValueChanged = true;
                    }
                    if (fieldValueChanged)
                    {
                        if (((int)this.ExecuteScalar("Select count(*) from " + this.ReadDB + ".dbo.SystemParameter where ParName='" + info.Name + "'")) > 0)
                        {
                            if (logChange)
                            {
                                this.logDeletedData(AID, this.ReadDB + ".dbo.SystemParameter", "ParName='" + info.Name + "'");
                                this.Update(DB, "SystemParameter", new string[] { "ParValue", "__AID" }, new object[] { str, AID }, "ParName='" + info.Name + "'");
                            }
                            else
                            {
                                this.Update(DB, "SystemParameter", new string[] { "ParValue" }, new object[] { str }, "ParName='" + info.Name + "'");
                            }
                        }
                        else
                        {
                            if (logChange)
                            {
                                this.Insert(DB, "SystemParameter", new string[] { "ParName", "ParValue", "__AID" }, new object[] { info.Name, str, AID });
                            }
                            else
                            {
                                this.Insert(DB, "SystemParameter", new string[] { "ParName", "ParValue" }, new object[] { info.Name, str });
                            }
                        }
                        info.SetValue(sysPars, vals[index]);
                    }
                }
            }
        }

        public static string ToSQLDateTimeStr(DateTime dt)
        {
            return (dt.ToString("yyy-MM-dd HH:mm:ss") + "." + dt.Millisecond.ToString("000"));
        }
        public static object TranslateDBNull(Type t, object val)
        {
            if (t == typeof(DateTime))
            {
                return INTAPS.DateTools.NullDateValue;
            }
            if (t == typeof(int))
            {
                return -1;
            }
            if (t == typeof(short))
            {
                return (short)(-1);
            }
            if (t == typeof(double))
            {
                return 0.0;
            }
            return null;
        }

        private static object TranslateNullValueToDB(object val, Type t)
        {
            if (t == typeof(DateTime))
            {
                return INTAPS.DateTools.NullDateValue;
            }
            if (t == typeof(int))
            {
                return -1;
            }
            if (t == typeof(short))
            {
                return (short)(-1);
            }
            if (t == typeof(double))
            {
                return 0.0;
            }
            return "";
        }

        public int Update(string DB, string tableName, string[] ColumnNames, object[] Values, string whereCriteria)
        {
            if (ColumnNames.Length != Values.Length)
            {
                throw new Exception("Column count doesn't match value count");
            }
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            tableName = tableName.TrimStart(new char[] { '[' });
            tableName = tableName.TrimEnd(new char[] { ']' });
            if (string.IsNullOrEmpty(DB))
            {
                tableName = "[" + tableName + "]";
            }
            else
            {
                tableName = DB + ".dbo.[" + tableName + "]";
            }
            int index = 0;
            while (index < ColumnNames.Length)
            {
                ColumnNames[index] = ColumnNames[index].TrimStart(new char[] { '[' });
                ColumnNames[index] = ColumnNames[index].TrimEnd(new char[] { ']' });
                index++;
            }
            string str = "";
            str = "UPDATE ";
            str = (str + tableName + "  SET [") + ColumnNames[0] + "]=";
            SqlParameter parameter = new SqlParameter("@Par0", (Values[0] == null) ? DBNull.Value : Values[0]);
            cmd.Parameters.Clear();
            cmd.Parameters.Add(parameter);
            str = str + "@Par0";
            for (index = 1; index < ColumnNames.Length; index++)
            {
                parameter = new SqlParameter("@Par" + index.ToString(), (Values[index] == null) ? DBNull.Value : Values[index]);
                str = (str + ", [") + ColumnNames[index] + "]=@Par" + index.ToString();
                cmd.Parameters.Add(parameter);
            }
            if (whereCriteria != null)
            {
                str = str + ((whereCriteria.Length == 0) ? " " : (" WHERE " + whereCriteria));
            }
            cmd.CommandText = str;
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }

        public int UpdateSingleTableRecord<T>(string DB, T rec)
        {
            return this.UpdateSingleTableRecord<T>(DB, rec, new string[0], new object[0], null);
        }

        public int UpdateSingleTableRecord<T>(string DB, T rec, string AdditionalCriteria)
        {
            return this.UpdateSingleTableRecord<T>(DB, rec, new string[0], new object[0], AdditionalCriteria);
        }

        public int UpdateSingleTableRecord<T>(string DB, T rec, string[] AdditionalFields, object[] AdditionalValues)
        {
            return this.UpdateSingleTableRecord<T>(DB, rec, AdditionalFields, AdditionalValues, null);
        }

        public int UpdateSingleTableRecord<T>(string DB, T rec, string[] AdditionalFields, object[] AdditionalValues, string AdditionalCriteria)
        {
            string str3;
            string str4;
            object obj2;
            string str6;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string setPart = null;
            string criteria = null;
            int num = 0;
            cmd.Parameters.Clear();
            for (int i = 0; i < AdditionalFields.Length; i++)
            {
                str3 = AdditionalFields[i];
                str4 = "@Par" + num;
                if (string.IsNullOrEmpty(setPart))
                {
                    setPart = "[" + str3 + "]=" + str4;
                }
                else
                {
                    str6 = setPart;
                    setPart = str6 + ",[" + str3 + "]=" + str4;
                }
                obj2 = AdditionalValues[i];
                cmd.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? DBNull.Value : obj2));
                num++;
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if ((field is IDField) || field.updatable)
                        {
                            str3 = (field.fieldName == null) ? info.Name : field.fieldName;
                            str4 = "@Par" + num;
                            if (field is IDField)
                            {
                                if (string.IsNullOrEmpty(criteria))
                                {
                                    str6 = criteria;
                                    criteria = str6 + "[" + str3 + "]=" + str4;
                                }
                                else
                                {
                                    str6 = criteria;
                                    criteria = str6 + " and [" + str3 + "]=" + str4;
                                }
                            }
                            else if (string.IsNullOrEmpty(setPart))
                            {
                                setPart = "[" + str3 + "]=" + str4;
                            }
                            else
                            {
                                str6 = setPart;
                                setPart = str6 + ",[" + str3 + "]=" + str4;
                            }
                            obj2 = info.GetValue(rec);
                            if (field is XMLField)
                            {
                                if (obj2 == null)
                                {
                                    obj2 = "";
                                }
                                else
                                {
                                    obj2 = FieldObjectToXml(obj2, info, typeof(T));
                                }
                            }
                            else if (!(field.allowNull || (obj2 != null)))
                            {
                                obj2 = TranslateNullValueToDB(obj2, info.FieldType);
                            }
                            cmd.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? DBNull.Value : obj2));
                            num++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(criteria))
            {
                criteria = "";
            }
            else
            {
                criteria = " WHERE " + criteria;
            }
            if (!string.IsNullOrEmpty(AdditionalCriteria))
            {
                if (string.IsNullOrEmpty(criteria))
                {
                    criteria = " WHERE " + AdditionalCriteria;
                }
                else
                {
                    criteria = criteria + " and (" + AdditionalCriteria + ")";
                }
            }
            string table = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (!string.IsNullOrEmpty(DB))
            {
                table = DB + ".dbo." + table;
            }
            cmd.CommandText = "UPDATE " + table + " SET " + setPart + criteria;
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }
        public int UpdateSingleTableRecord<T>(string DB, object rec, string[] AdditionalFields, object[] AdditionalValues, string AdditionalCriteria)
        {
            string str3;
            string str4;
            object obj2;
            string str6;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            Type type = rec.GetType();
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute)customAttributes[0];
            string str = null;
            string str2 = null;
            int num = 0;
            cmd.Parameters.Clear();
            for (int i = 0; i < AdditionalFields.Length; i++)
            {
                str3 = AdditionalFields[i];
                str4 = "@Par" + num;
                if (string.IsNullOrEmpty(str))
                {
                    str = "[" + str3 + "]=" + str4;
                }
                else
                {
                    str6 = str;
                    str = str6 + ",[" + str3 + "]=" + str4;
                }
                obj2 = AdditionalValues[i];
                cmd.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? DBNull.Value : obj2));
                num++;
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField field = (DataField)customAttributes[0];
                        if ((field is IDField) || field.updatable)
                        {
                            str3 = (field.fieldName == null) ? info.Name : field.fieldName;
                            str4 = "@Par" + num;
                            if (field is IDField)
                            {
                                if (string.IsNullOrEmpty(str2))
                                {
                                    str6 = str2;
                                    str2 = str6 + "[" + str3 + "]=" + str4;
                                }
                                else
                                {
                                    str6 = str2;
                                    str2 = str6 + " and [" + str3 + "]=" + str4;
                                }
                            }
                            else if (string.IsNullOrEmpty(str))
                            {
                                str = "[" + str3 + "]=" + str4;
                            }
                            else
                            {
                                str6 = str;
                                str = str6 + ",[" + str3 + "]=" + str4;
                            }
                            obj2 = info.GetValue(rec);
                            if (field is XMLField)
                            {
                                if (obj2 == null)
                                {
                                    obj2 = "";
                                }
                                else
                                {
                                    obj2 = FieldObjectToXml(obj2, info, typeof(T));
                                }
                            }
                            else if (!(field.allowNull || (obj2 != null)))
                            {
                                obj2 = TranslateNullValueToDB(obj2, info.FieldType);
                            }
                            cmd.Parameters.Add(new SqlParameter("@Par" + num, (obj2 == null) ? DBNull.Value : obj2));
                            num++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(str2))
            {
                str2 = "";
            }
            else
            {
                str2 = " WHERE " + str2;
            }
            if (!string.IsNullOrEmpty(AdditionalCriteria))
            {
                if (string.IsNullOrEmpty(str2))
                {
                    str2 = " WHERE " + AdditionalCriteria;
                }
                else
                {
                    str2 = str2 + " and (" + AdditionalCriteria + ")";
                }
            }
            string str5 = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (!string.IsNullOrEmpty(DB))
            {
                str5 = DB + ".dbo." + str5;
            }
            cmd.CommandText = "UPDATE " + str5 + " SET " + str + str2;
            this.SetConnection(cmd);
            lock (this.m_con)
            {
                return cmd.ExecuteNonQuery();
            }
        }
        public static string ObjectToXml(object o, params Type[] includeTypes)
        {
            
            XmlSerializer serializer;
            serializer= includeTypes.Length == 0 ? new XmlSerializer(o.GetType()) : new XmlSerializer(o.GetType(), includeTypes);
            StringWriter writer = new StringWriter();
            serializer.Serialize((TextWriter)writer, o);
            writer.Close();
            return writer.ToString();
        }
        public static object XmlToObject(string xml, Type type)
        {
            return XmlToObject(xml, type, null);
        }

        public static object XmlToObject(string xml, Type type, Type[] includeTypes)
        {
            XmlSerializer serializer;
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }
            if (includeTypes == null)
            {
                serializer = new XmlSerializer(type);
            }
            else
            {
                serializer = new XmlSerializer(type, includeTypes);
            }
            return serializer.Deserialize(new StringReader(xml));
        }

        public bool InTransaction
        {
            get
            {
                return (this.m_transactionCounter > 0);
            }
        }

        public string ReadDB
        {
            get
            {
                return this.m_readDB;
            }
        }


       

        public static string getVersionDataFilter(long date, string cr)
        {            string filter = "(ticksFrom<={0} and (ticksTo=-1 or ticksTo>{0})) {1}";
            return string.Format(filter, date, string.IsNullOrEmpty(cr) ? "" : string.Format("and ({0})", cr));
        }       
        

        public DataTable GetSchemaTable(string query)
        {
            DataTable dataTable = new DataTable();
            SqlDataAdapter a = new SqlDataAdapter(query, (SqlConnection)this.getOpenConnection());
            ((SqlDataAdapter)a).FillSchema(dataTable, SchemaType.Source);
            return dataTable;
        }

        public DataTable GetSchemaTable(string tableName, string[] ColumnNames)
        {
            int num;
            tableName = tableName.TrimStart(new char[] { '[' });
            tableName = tableName.TrimEnd(new char[] { ']' });
            for (num = 0; num < ColumnNames.Length; num++)
            {
                ColumnNames[num] = ColumnNames[num].TrimStart(new char[] { '[' });
                ColumnNames[num] = ColumnNames[num].TrimEnd(new char[] { ']' });
            }
            DataTable dataTable = new DataTable();
            string selectCommandText = "";
            selectCommandText = "SELECT  [";
            selectCommandText = selectCommandText + ColumnNames[0];
            for (num = 1; num < ColumnNames.Length; num++)
            {
                selectCommandText = selectCommandText + "],[" + ColumnNames[num];
            }
            selectCommandText = selectCommandText + "] FROM  [" + tableName + "]";
            return GetSchemaTable(selectCommandText);
        }

    }
}