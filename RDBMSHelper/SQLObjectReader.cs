using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
namespace INTAPS.RDBMS
{

    public class SQLObjectReader<T> where T: new()
    {
        private SqlDataReader dataReader;
        private List<DataField> includeAtr;
        private List<FieldInfo> includedFlds;
        private string[] m_addtionalFields;
        private object[] m_additionalData;
        private T m_current;
        private string m_orderby;
        private SQLHelper m_parent;

        internal SQLObjectReader(SQLHelper parent, string filter, string[] AdditionalFields, string orderby)
        {
            this.includedFlds = new List<FieldInfo>();
            this.includeAtr = new List<DataField>();
            this.m_orderby = orderby;
            this.m_parent = parent;
            this.m_addtionalFields = AdditionalFields;
            Type type = typeof(T);
            object[] customAttributes = type.GetCustomAttributes(typeof(SingleTableObjectAttribute), true);
            if ((customAttributes == null) || (customAttributes.Length == 0))
            {
                throw new Exception(type + " is not marked SingleTableRecord");
            }
            object[] objArray2 = type.GetCustomAttributes(typeof(STOIncludeAttribute), false);
            SingleTableObjectAttribute attribute = (SingleTableObjectAttribute) customAttributes[0];
            string str = null;
            for (int i = 0; i < AdditionalFields.Length; i++)
            {
                if (string.IsNullOrEmpty(str))
                {
                    str = "[" + AdditionalFields[i] + "]";
                }
                else
                {
                    str = str + ",[" + AdditionalFields[i] + "]";
                }
            }
            foreach (FieldInfo info in type.GetFields())
            {
                bool flag = false;
                Type declaringType = info.DeclaringType;
                foreach (STOIncludeAttribute attribute2 in objArray2)
                {
                    if (attribute2.includeType == declaringType)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag || (declaringType == type))
                {
                    customAttributes = info.GetCustomAttributes(typeof(DataField), true);
                    if ((customAttributes != null) && (customAttributes.Length != 0))
                    {
                        DataField item = (DataField) customAttributes[0];
                        string str2 = (item.fieldName == null) ? info.Name : item.fieldName;
                        if (string.IsNullOrEmpty(str))
                        {
                            str = "[" + str2 + "]";
                        }
                        else
                        {
                            str = str + ",[" + str2 + "]";
                        }
                        this.includedFlds.Add(info);
                        this.includeAtr.Add(item);
                    }
                }
            }
            string str3 = "[" + ((attribute.tableName == null) ? type.Name : attribute.tableName) + "]";
            if (parent.ReadDB != null)
            {
                str3 = parent.ReadDB + ".dbo." + str3;
            }
            string sql = "Select " + str + " FROM " + str3 + (string.IsNullOrEmpty(filter) ? "" : (" WHERE " + filter));
            if (!string.IsNullOrEmpty(this.m_orderby))
            {
                sql = sql + " order by " + this.m_orderby;
            }
            else if (!string.IsNullOrEmpty(attribute.orderBy))
            {
                sql = sql + " order by " + attribute.orderBy;
            }
            this.dataReader = parent.CreatCommand(sql).ExecuteReader();
        }

        public void Close()
        {
            if (this.dataReader != null)
            {
                this.dataReader.Close();
            }
        }

        public bool Read()
        {
            if (!this.dataReader.Read())
            {
                return false;
            }
            this.m_current = new T();
            this.m_additionalData = new object[this.m_addtionalFields.Length];
            int index = 0;
            for (int i = 0; i < this.m_addtionalFields.Length; i++)
            {
                this.m_additionalData[index] = this.dataReader[index];
                index++;
            }
            int num3 = 0;
            foreach (FieldInfo info in this.includedFlds)
            {
                object val = this.dataReader[index];
                if (this.includeAtr[num3] is XMLField)
                {
                    if (string.IsNullOrEmpty(val as string))
                    {
                        val = null;
                    }
                    else
                    {
                        val = SQLHelper.FieldXmlToObject((string) val, info, typeof(T));
                    }
                }
                if (val is DBNull)
                {
                    val = SQLHelper.TranslateDBNull(info.FieldType, val);
                }
                info.SetValue(this.m_current, val);
                index++;
                num3++;
            }
            return true;
        }

        public T Current
        {
            get
            {
                return this.m_current;
            }
        }

        public object[] CurrentAdditionalData
        {
            get
            {
                return this.m_additionalData;
            }
        }

        public bool IsClosed
        {
            get
            {
                if (this.dataReader == null)
                {
                    return false;
                }
                return this.dataReader.IsClosed;
            }
        }
    }
}

