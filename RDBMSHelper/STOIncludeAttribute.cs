
    using System;
namespace INTAPS.RDBMS
{

    public class STOIncludeAttribute : Attribute
    {
        public Type includeType;

        public STOIncludeAttribute(Type t)
        {
            this.includeType = t;
        }
    }
}

