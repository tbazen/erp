
    using System;
namespace INTAPS.RDBMS
{

    public class SingleTableObjectAttribute : Attribute
    {
        public bool inheritAllFields;
        public string orderBy;
        public string tableName;

        public SingleTableObjectAttribute() : this(null, null, false)
        {
        }

        public SingleTableObjectAttribute(bool inheritAllFields) : this(null, null, inheritAllFields)
        {
        }

        public SingleTableObjectAttribute(string tn)
        {
            this.tableName = tn;
            this.orderBy = null;
            this.inheritAllFields = false;
        }

        public SingleTableObjectAttribute(string tn, string orderBy, bool inheritAllFields)
        {
            this.tableName = tn;
            this.orderBy = orderBy;
            this.inheritAllFields = inheritAllFields;
        }
    }
}

