using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
namespace INTAPS.RDBMS
{
    public class TypeIDAttribute : Attribute
    {
        public int id;
        public TypeIDAttribute(int id)
        {
            this.id = id;
        }
    }
    public interface ITypeIndexer
    {
        Type dataType { get; }
        Type indexDataType { get; }
        void checkAndFixStorage(SQLHelper helper);
        void createIndex(SQLHelper helper,object obj);
        void deleteIndex(SQLHelper helper,string objID);
    }
}
