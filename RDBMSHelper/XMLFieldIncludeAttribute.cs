
    using System;
namespace INTAPS.RDBMS
{

    public class XMLFieldIncludeAttribute : Attribute
    {
        public Type[] includeTypes;

        public XMLFieldIncludeAttribute(params Type[] includeTypes)
        {
            this.includeTypes = includeTypes;
        }
    }
}

