namespace INTAPS.ClientServer.Server
{
    using System;
    using System.Configuration;
    using System.Reflection;

    [ConfigurationCollection(typeof(BDETypeElement))]
    public class BDETypeCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new BDETypeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((BDETypeElement) element).BDEName;
        }

        public BDETypeElement this[int idx]
        {
            get
            {
                return (BDETypeElement) base.BaseGet(idx);
            }
        }
    }
}

