namespace INTAPS.ClientServer.Server
{
    using System;
    using System.Configuration;

    public class BDETypeElement : ConfigurationElement
    {
        [ConfigurationProperty("BDEClass", DefaultValue="", IsKey=false, IsRequired=true)]
        public string BDEClass
        {
            get
            {
                return (string) base["BDEClass"];
            }
            set
            {
                base["BDEClass"] = value;
            }
        }

        [ConfigurationProperty("BDEName", DefaultValue="", IsKey=true, IsRequired=true)]
        public string BDEName
        {
            get
            {
                return (string) base["BDEName"];
            }
            set
            {
                base["BDEName"] = value;
            }
        }
    }
}

