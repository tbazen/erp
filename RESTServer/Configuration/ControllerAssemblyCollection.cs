namespace INTAPS.ClientServer.Server
{
    using System;
    using System.Configuration;
    using System.Reflection;

    [ConfigurationCollection(typeof(ControllerAssemblyItem))]
    public class ControllerAssemblyCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ControllerAssemblyItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ControllerAssemblyItem) element).Name;
        }

        public ControllerAssemblyItem this[int idx]
        {
            get
            {
                return (ControllerAssemblyItem) base.BaseGet(idx);
            }
        }
    }
}

