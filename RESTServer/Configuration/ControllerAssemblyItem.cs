namespace INTAPS.ClientServer.Server
{
    using System;
    using System.Configuration;

    public class ControllerAssemblyItem : ConfigurationElement
    {
        [ConfigurationProperty("Name", DefaultValue="", IsKey=true, IsRequired=true)]
        public string Name
        {
            get
            {
                return (string) base["Name"];
            }
            set
            {
                base["Name"] = value;
            }
        }

        [ConfigurationProperty("ControllerAssembly", DefaultValue="", IsKey=false, IsRequired=true)]
        public string ControllerAssembly
        {
            get
            {
                return (string) base["ControllerAssembly"];
            }
            set
            {
                base["ControllerAssembly"] = value;
            }
        }
    }
}

