namespace INTAPS.ClientServer.Server
{
    using System.Configuration;

    public class ServicesSection : ConfigurationSection
    {
        [ConfigurationProperty("BDEClasses")]
        public BDETypeCollection BDEClasses
        {
            get
            {
                return (BDETypeCollection) base["BDEClasses"];
            }
        }

       
        [ConfigurationProperty("ControllerAssemblies")]
        public ControllerAssemblyCollection ControllerAssemblies
        {
            get
            {
                return (ControllerAssemblyCollection) base["ControllerAssemblies"];
            }
        }
    }
}

