namespace INTAPS.ClientServer.Server
{
    using INTAPS.ClientServer;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Serialization;
    public class ErrLogItem
    {
        public string[] Messages;
        public string[] StatckTrace;
        public DateTime time;
        public string[] Types;

        public ErrLogItem()
        {
            this.time = DateTime.Now;
            this.Types = new string[0];
            this.Messages = new string[0];
            this.StatckTrace = new string[0];
        }
        public ErrLogItem(String msg)
        {
            this.Messages = new String[] { msg };
        }
        public ErrLogItem(Exception ex)
        {
            this.time = DateTime.Now;
            List<string> list = new List<string>();
            List<string> list2 = new List<string>();
            List<string> list3 = new List<string>();
            while (ex != null)
            {
                list.Add(ex.GetType().ToString());
                list2.Add(ex.Message);
                list3.Add(ex.StackTrace);
                ex = ex.InnerException;
            }
            this.Types = list.ToArray();
            this.Messages = list2.ToArray();
            this.StatckTrace = list3.ToArray();
        }
    }
    public class ConsolEventLoger : IEventLoger
    {
        private EventLogType s_LogType = ((EventLogType) Enum.Parse(typeof(EventLogType), System.Configuration.ConfigurationManager.AppSettings["LogTypes"]));
        public static ConsolEventLoger TheInstance;

        public ConsolEventLoger()
        {
            TheInstance = this;
        }

        public void Log(string message)
        {
            this.WriteLog(DateTime.Now.ToLongTimeString() + ":" + message);
        }

        public void Log(EventLogType lt, string message)
        {
            Log(lt, message, true);
        }
        void Log(EventLogType lt, string message,bool saveToFile)
        {
            if ((lt & this.s_LogType) == lt)
            {
                ConsoleColor cl = Console.ForegroundColor;
                if (lt == EventLogType.Errors)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    if(saveToFile)
                    {
                        ErrLogItem o = new ErrLogItem(message);
                        XmlSerializer serializer = new XmlSerializer(typeof(ErrLogItem));
                        StreamWriter writer = File.CreateText(Path.Combine(@"log" , o.time.ToString("yyyyMMdd-hhmmss") + Guid.NewGuid().ToString() + ".xml"));
                        serializer.Serialize((TextWriter)writer, o);
                        writer.Close();
                    }
                }
                this.Log(message);
                if (lt == EventLogType.Errors)
                    Console.ForegroundColor = cl;
            }
        }

        public void LogException(string message, Exception ex)
        {
            ErrLogItem o = new ErrLogItem(ex);
            XmlSerializer serializer = new XmlSerializer(typeof(ErrLogItem));
            StreamWriter writer = File.CreateText(Path.Combine(@"log",o.time.ToString("yyyyMMdd-hhmmss") + Guid.NewGuid().ToString() + ".xml"));
            serializer.Serialize((TextWriter) writer, o);
            writer.Close();
            while (ex != null)
            {
                this.Log(EventLogType.Errors, ((message == null) ? "" : (message + "\n")) + ex.Message + "\n" + ex.StackTrace,false);
                ex = ex.InnerException;
            }
        }

        public void TerminateApplication()
        {
        }

        private void WriteLog(string text)
        {
            Console.WriteLine(text);
        }
    }
}

