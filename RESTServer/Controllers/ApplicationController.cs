namespace INTAPS.ClientServer.Server
{
    using INTAPS.ClientServer;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    [Route("api/app/[controller]/[action]")]
    [ApiController]
    public class ServerController: RESTServerControllerBase<ApplicationServer>
    {

        #region CloseSession
        public class CloseSessionPar
        {
            public string SessionID;
        }
        [HttpPost]
        public void CloseSession(CloseSessionPar par)
        {
            ApplicationServer.CloseSession(par.SessionID);
        }
        #endregion

        #region CreateUserSession
        public class CreateUserSessionPar
        {
            public string UserName; public string Password; public string Source;
        }
        [HttpPost]
        public ActionResult CreateUserSession([FromBody]CreateUserSessionPar par)
        {
            try
            {
                return Json(ApplicationServer.CreateUserSession(par.UserName, par.Password, par.Source));
            }
            catch(Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region GetActiveSessions
        [HttpPost]
        public UserSessionInfo[] GetActiveSessions()
        {
            return ApplicationServer.GetActiveSessions();
        }
        #endregion

        #region IsAlive
        public class IsAlivePar
        {
            public string sessionID;
        }
        [HttpPost]
        public bool isAlive(IsAlivePar par)
        {
            return ApplicationServer.isAlive(par.sessionID);
        }
        #endregion

        #region KeepAlivePar
        public class KeepAlivePar
        {
            public string sessionID;
        }
        [HttpPost]
        public ActionResult keepAlive(KeepAlivePar par)
        {
            try
            {
                ApplicationServer.GetSessionObject(par.sessionID, typeof(SecurityService));
                return Ok();
            }
            catch(Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region GetSessionInfo
        public class GetSessionInfoPar
        {
            public string sessionID;
        }
        [HttpPost]
        public UserSessionInfo getSessionInfo(GetSessionInfoPar par)
        {
            return ApplicationServer.getSessionInfo(par.sessionID);
        }
        #endregion

        #region PingServer
        [HttpPost]
        public void PingServer()
        {
        }
        #endregion
        #region GetInstantMessages
        public class GetInstantMessagesPar
        {
           public string sessionID;
        }
        [HttpPost]
        public InstantMessageData[] getInstantMessages(GetInstantMessagesPar par)
        {
            return ApplicationServer.getInstantMessages(par.sessionID);
        }
        #endregion
        #region sendMessage
        public class SendMessagePar
        {
            public string sessionID; public InstantMessageData msg;
        }
        [HttpPost]
        public string sendMessage(SendMessagePar par)
        {
            return ApplicationServer.sendMessage(par.sessionID, par.msg);
        }
        #endregion
        #region SetRead
        public class SetReadPar
        {
            public string sessionID; public string[] messageIDs;
        }
        [HttpPost]
        public void setRead(SetReadPar par)
        {
            ApplicationServer.setRead(par.sessionID, par.messageIDs);
        }
        #endregion
        #region GetLicenseDataPar
        [HttpPost]
        public TLicense.LicenseData getLicenseData()
        {
            return TLicense.LicenseDataLoader.licenseData;
        }
        #endregion
    }
}