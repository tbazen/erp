﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PWF.api.Helpers;
using PWF.domain.Infrastructure;

namespace RESTServer.Controllers
{
    public class BaseController : RESTServerControllerBase<SecurityService>
    {


            public UserSession GetSession()
            {

                var session = HttpContext.Session.GetString("adminSession");
                if (session == null)
                {
                    throw new NotAuthenticatedException("Not Authenticated");
                }
                return JsonConvert.DeserializeObject<UserSession>(session);
            }

            public IActionResult ErrorResponse(Exception ex)
            {

                if(ex is NotAuthenticatedException || ex is INTAPS.ClientServer.SessionExpiredException || ex is INTAPS.ClientServer.AccessDeniedException)
                {
                     return StatusCode(403,ex.Message);
                }
               else
            {
                return StatusCode(501, ex.Message);
            }

            }

            public IActionResult SuccessfulResponse(Object response)
            {
                return Ok(response);

            }




    }
}