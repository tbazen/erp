﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using INTAPS.ClientServer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PWF.domain.Extensions;
using PWF.domain.Infrastructure;

namespace RESTServer.Controllers
{

    [Route("api/app/[Controller]/[action]")]
    [ApiController]
    public class Security2Controller : BaseController
    {
        #region ChangeClass
        public class ChangeClassPar
        {
            public string SessionID; public int ObjectID; public SecurityObjectClass Class;
        }

        [HttpPost]
        public IActionResult ChangeClass(ChangeClassPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);

                ((SecurityService)base.SessionObject).ChangeClass(par.ObjectID, par.Class);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region ChangeOwner
        public class ChangeOwnerPar
        {
            public string SessionID; public int ObjectID; public int NewOwner;
        }
        [HttpPost]
        public IActionResult ChangeOwner(ChangeOwnerPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangeOwner(par.ObjectID, par.NewOwner);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
            ;
        }

        #endregion
        #region ChangePassword
        public class ChangePasswordPar
        {
            public string SessionID; public string newpassword;
        }
        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangePassword(par.newpassword);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region ChangePassword2
        public class ChangePassword2Par
        {
            public string SessionID; public string UserName; public string newpassword;
        }
        [HttpPost]
        public IActionResult ChangePassword2(ChangePassword2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangePassword(par.UserName, par.newpassword);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region CreateObject
        public class CreateObjectPar
        {
            public string SessionID; public int ParentOID; public string Name; public int OwnerID;
        }
        [HttpPost]
        public IActionResult CreateObject(CreateObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).CreateObject(par.ParentOID, par.Name);
                ((SecurityService)base.SessionObject).ChangeOwner(res, par.OwnerID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region CreateUser
        public class CreateUserPar
        {
            public string SessionID; public string ParentUser; public string UserName; public string Password;
        }
        [HttpPost]
        public IActionResult CreateUser(CreateUserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).CreateUser(par.ParentUser, par.UserName, par.Password);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion
        #region DeleteObject
        public class DeleteObjectPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult DeleteObject(DeleteObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).DeleteObject(par.ObjectID);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region DeleteUser
        public class DeleteUserPar
        {
            public string SessionID; public string UserName;
        }
        [HttpPost]
        public IActionResult DeleteUser(DeleteUserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).DeleteUser(par.UserName);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetAllAuditOperations
        public class GetAllAuditOperationsPar
        {
            public string SessionID;
        }
        [HttpPost]
        public IActionResult GetAllAuditOperations(GetAllAuditOperationsPar par)
        {

            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetAllAuditOperations();
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion
        #region GetAllUsers
        public class GetAllUsersPar
        {
            public string SessionID;
        }
        [HttpPost]
        public IActionResult GetAllUsers(GetAllUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetAllUsers();

                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }

        [HttpPost]
        public IActionResult GetAllUsers2(GetAllUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).getAllUsers();
                
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion
        #region GetAudit
        public class GetAuditPar
        {
            public string SessionID; public string[] AuditType; public string[] Users; public bool Date; public DateTime from; public bool DateTo; public DateTime to; public string[] Data1; public int index; public int PageSize;
        }
        public class GetAuditOut
        {
            public int NRecords;
            public DataTable _ret;
        }
        [HttpPost]
        public IActionResult GetAudit(GetAuditPar par)
        {


            try
            {
                var span = new TimeSpan(0, 0, 0);
                var _ret = new GetAuditOut();//add this line
                this.SetSessionObject(par.SessionID);
                par.from = par.from + span;
                par.to = par.to + span;
                //return ((SecurityService)base.SessionObject).GetAudit(AuditType, Users, Date, from, DateTo, to, Data1, index, PageSize, out NRecords);
                _ret._ret = ((SecurityService)base.SessionObject).GetAudit(par.AuditType, par.Users, par.Date, par.from, par.DateTo, par.to, par.Data1, par.index, par.PageSize, out _ret.NRecords);
                return SuccessfulResponse(_ret);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion
        #region GetAuditData2
        public class GetAuditData2Par
        {
            public string SessionID; public int AuditID;
        }
        [HttpPost]
        public IActionResult GetAuditData2(GetAuditData2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetAuditData2(par.AuditID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetChildList
        public class GetChildListPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult GetChildList(GetChildListPar par)
        {

            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetChildList(par.ObjectID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion

        #region GetChildUsers
        public class GetChildUsersPar
        {
            public string SessionID; public string User;
        }
        [HttpPost]
        public IActionResult GetChildUsers(GetChildUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetChildUsers(par.User);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetDeniedEntries
        public class GetDeniedEntriesPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult GetDeniedEntries(GetDeniedEntriesPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetDeniedEntries(par.ObjectID);

                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion 
        #region GetDeniedUsers
        public class GetDeniedUsersPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult GetDeniedUsers(GetDeniedUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetDeniedUsers(par.ObjectID);

                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion
        #region GetNameForID
        public class GetNameForIDPar
        {
            public string SessionID; public int UserID;
        }
        [HttpPost]
        public IActionResult GetNameForID(GetNameForIDPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetNameForID(par.UserID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetObject
        public class GetObjectPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult GetObject(GetObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetObject(par.ObjectID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }

        [HttpPost]
        public IActionResult GetAllObjects(GetObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetAllObjects();
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }


        #endregion
        #region GetObjectFullName
        public class GetObjectFullNamePar
        {
            public string SessionID; public int OID;
        }
        [HttpPost]
        public IActionResult GetObjectFullName(GetObjectFullNamePar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetObjectFullName(par.OID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetObjectID
        public class GetObjectIDPar
        {
            public string SessionID; public string Name;
        }
        [HttpPost]
        public IActionResult GetObjectID(GetObjectIDPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetObjectID(par.Name);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetParentUser
        public class GetParentUserPar
        {
            public string SessionID; public string UserName;
        }
        [HttpPost]
        public IActionResult GetParentUser(GetParentUserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetParentUser(par.UserName);

                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }
        #endregion
        #region GetPermitedUsers
        public class GetPermitedUsersPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult GetPermitedUsers(GetPermitedUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetPermitedUsers(par.ObjectID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetPermittedEntries
        public class GetPermittedEntriesPar
        {
            public string SessionID; public int ObjectID;
        }
        [HttpPost]
        public IActionResult GetPermittedEntries(GetPermittedEntriesPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetPermittedEntries(par.ObjectID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetUIDForName
        public class GetUIDForNamePar
        {
            public string SessionID; public string Name;
        }
        [HttpPost]
        public IActionResult GetUIDForName(GetUIDForNamePar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetUIDForName(par.Name);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetUserPermissions
        public class GetUserPermissionsPar
        {
            public string SessionID; public int UserID;
        }
        [HttpPost]
        public IActionResult GetUserPermissions(GetUserPermissionsPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetUserPermissions(par.UserID);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region GetUserPermissions2
        public class GetUserPermissions2Par
        {
            public string SessionID; public string userName; public string password;
        }
        [HttpPost]
        public IActionResult GetUserPermissions2(GetUserPermissions2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).GetUserPermissions(par.userName, par.password);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region Rename
        public class RenamePar
        {
            public string SessionID; public int ObjectID; public string NewName; public int Owner;
        }
        [HttpPost]
        public IActionResult Rename(RenamePar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).Rename(par.ObjectID, par.NewName);
                ((SecurityService)base.SessionObject).ChangeOwner(par.ObjectID, par.Owner);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region SetPermission
        public class SetPermissionPar
        {
            public string SessionID; public int ObjectID; public string UserName; public PermissionState state;
        }
        [HttpPost]
        public IActionResult SetPermission(SetPermissionPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).SetPermission(par.ObjectID, par.UserName, par.state);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }

        public class SetPermission2Par
        {
            public string SessionID { get; set; }
            public int UserId { get; set; }
            public List<int> Permissions { get; set; }
            public List<int> Denied { get; set; }
        }

        [HttpPost]
        public IActionResult SetPermission2(SetPermission2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var obj = ((SecurityService)base.SessionObject);
                var user = obj.GetUserPermissions(par.UserId);
                var permissions = user.Permissions.ToList();
                var denied = user.Denied.ToList();


                var deletedPermission = permissions.Except(par.Permissions).ToArray();
                var deltedDenied = denied.Except(par.Denied).ToArray();
                var newPermitted = par.Permissions.Except(permissions).ToArray();
                var newDenied = par.Denied.Except(denied).ToArray();

                foreach(var item in deletedPermission)
                {
                    obj.SetPermission(item, user.UserName, PermissionState.Undefined);
                }
                foreach (var item in deltedDenied)
                {
                    obj.SetPermission(item, user.UserName, PermissionState.Undefined);
                }
                foreach (var item in newPermitted)
                {
                    obj.SetPermission(item, user.UserName, PermissionState.Permit);
                }
                foreach (var item in newDenied)
                {
                    obj.SetPermission(item, user.UserName, PermissionState.Deny);
                }
                return SuccessfulResponse(true);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }


        #endregion
        #region User
        public class UserPar
        {
            public string SessionID; public string user;
        }
        [HttpPost]
        public IActionResult User2(UserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).User(par.user);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion
        #region IsPermited
        public class IsPermitedPar
        {
            public string SessionID; public string obj;
        }
        [HttpPost]
        public IActionResult IsPermited(IsPermitedPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                var res = ((SecurityService)base.SessionObject).IsPermited(par.obj);
                return SuccessfulResponse(res);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }

        }
        #endregion


        #region Login
        public class LoginPar
        {
            public string UserName; public string Password;
        }

        [HttpPost]
        public IActionResult Login(LoginPar par)
        {
            try
            {
                var response = INTAPS.ClientServer.ApplicationServer.SecurityBDE.GetUserPermmissions(par.UserName, par.Password);

                var wsissession = INTAPS.ClientServer.ApplicationServer.CreateUserSession(par.UserName, par.Password, "Admin Web");
                var session = new UserSession()
                {
                    Username = par.UserName,
                    CreatedTime = DateTime.Now,
                    LastSeen = DateTime.Now,
                    Id = response.UserID,
                    wsisUserID = response.UserID,
                    Permissions = response,
                    payrollSessionID = wsissession
                };
                var serializedSession = JsonConvert.SerializeObject(session);
                Console.WriteLine("Session");
                return SuccessfulResponse(session);
            }
            catch (Exception ex)
            {

                return ErrorResponse(ex);
            }
        }

        [HttpPost]
        public IActionResult Logout(string sessionID)
        {
            try
            {
                HttpContext.Session.Clear();
                INTAPS.ClientServer.ApplicationServer.CloseSession(sessionID);
                return SuccessfulResponse("");
            }
            catch (Exception ex)
            {
                return SuccessfulResponse("");

            }

        }

        #endregion
    }
}