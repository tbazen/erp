namespace INTAPS.ClientServer.Server
{
    using INTAPS.ClientServer;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Data;


    [Route("api/app/[Controller]/[action]")]
    [ApiController]
    public class SecurityController : RESTServerControllerBase<SecurityService>
    {
        #region ChangeClass
        public class ChangeClassPar
        {
            
            public string SessionID; public int ObjectID; public SecurityObjectClass Class;
        }


        [HttpPost]
        public ActionResult ChangeClass(ChangeClassPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangeClass(par.ObjectID, par.Class);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeOwner
        public class ChangeOwnerPar
        {
            public string SessionID; public int ObjectID; public int NewOwner;
        }

        [HttpPost]
        public ActionResult ChangeOwner(ChangeOwnerPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangeOwner(par.ObjectID, par.NewOwner);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region ChangePassword
        public class ChangePasswordPar
        {
            public string SessionID; public string newpassword;
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangePassword(par.newpassword);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangePassword2
        public class ChangePassword2Par
        {
            public string SessionID; public string UserName; public string newpassword;
        }

        [HttpPost]
        public ActionResult ChangePassword2(ChangePassword2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).ChangePassword(par.UserName, par.newpassword);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateObject
        public class CreateObjectPar
        {
            public string SessionID; public int ParentOID; public string Name;
        }

        [HttpPost]
        public ActionResult CreateObject(CreateObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).CreateObject(par.ParentOID, par.Name));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateUser
        public class CreateUserPar
        {
            public string SessionID; public string ParentUser; public string UserName; public string Password;
        }

        [HttpPost]
        public ActionResult CreateUser(CreateUserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).CreateUser(par.ParentUser, par.UserName, par.Password);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteObject
        public class DeleteObjectPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult DeleteObject(DeleteObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).DeleteObject(par.ObjectID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteUser
        public class DeleteUserPar
        {
            public string SessionID; public string UserName;
        }

        [HttpPost]
        public ActionResult DeleteUser(DeleteUserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).DeleteUser(par.UserName);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllAuditOperations
        public class GetAllAuditOperationsPar
        {
            public string SessionID;
        }
        [HttpPost]
        public ActionResult GetAllAuditOperations(GetAllAuditOperationsPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetAllAuditOperations());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllUsers
        public class GetAllUsersPar
        {
            public string SessionID;
        }

        [HttpPost]
        public ActionResult GetAllUsers(GetAllUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetAllUsers());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAudit
        public class GetAuditPar
        {
            public string SessionID; public string[] AuditType; public string[] Users; public bool Date; public DateTime from; public bool DateTo; public DateTime to; public string[] Data1; public int index; public int PageSize;
        }
        public class GetAuditOut
        {
            public int NRecords;
            public DataTable _ret;
        }

        [HttpPost]
        public ActionResult GetAudit(GetAuditPar par)
        {
            try
            {
                var _ret = new GetAuditOut();//add this line
                this.SetSessionObject(par.SessionID);

                //return ((SecurityService)base.SessionObject).GetAudit(AuditType, Users, Date, from, DateTo, to, Data1, index, PageSize, out NRecords);
                _ret._ret = ((SecurityService)base.SessionObject).GetAudit(par.AuditType, par.Users, par.Date, par.from, par.DateTo, par.to, par.Data1, par.index, par.PageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAuditData2
        public class GetAuditData2Par
        {
            public string SessionID; public int AuditID;
        }

        [HttpPost]
        public ActionResult GetAuditData2(GetAuditData2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetAuditData2(par.AuditID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetChildList
        public class GetChildListPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult GetChildList(GetChildListPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetChildList(par.ObjectID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region GetChildUsers
        public class GetChildUsersPar
        {
            public string SessionID; public string User;
        }

        [HttpPost]
        public ActionResult GetChildUsers(GetChildUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetChildUsers(par.User));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetDeniedEntries
        public class GetDeniedEntriesPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult GetDeniedEntries(GetDeniedEntriesPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetDeniedEntries(par.ObjectID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion 
        #region GetDeniedUsers
        public class GetDeniedUsersPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult GetDeniedUsers(GetDeniedUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetDeniedUsers(par.ObjectID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetNameForID
        public class GetNameForIDPar
        {
            public string SessionID; public int UserID;
        }

        [HttpPost]
        public ActionResult GetNameForID(GetNameForIDPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetNameForID(par.UserID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetObject
        public class GetObjectPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult GetObject(GetObjectPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetObject(par.ObjectID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetObjectFullName
        public class GetObjectFullNamePar
        {
            public string SessionID; public int OID;
        }

        [HttpPost]
        public ActionResult GetObjectFullName(GetObjectFullNamePar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetObjectFullName(par.OID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetObjectID
        public class GetObjectIDPar
        {
            public string SessionID; public string Name;
        }

        [HttpPost]
        public ActionResult GetObjectID(GetObjectIDPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetObjectID(par.Name));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetParentUser
        public class GetParentUserPar
        {
            public string SessionID; public string UserName;
        }

        [HttpPost]
        public ActionResult GetParentUser(GetParentUserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetParentUser(par.UserName));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPermitedUsers
        public class GetPermitedUsersPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult GetPermitedUsers(GetPermitedUsersPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetPermitedUsers(par.ObjectID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPermittedEntries
        public class GetPermittedEntriesPar
        {
            public string SessionID; public int ObjectID;
        }

        [HttpPost]
        public ActionResult GetPermittedEntries(GetPermittedEntriesPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetPermittedEntries(par.ObjectID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUIDForName
        public class GetUIDForNamePar
        {
            public string SessionID; public string Name;
        }

        [HttpPost]
        public ActionResult GetUIDForName(GetUIDForNamePar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetUIDForName(par.Name));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUserPermissions
        public class GetUserPermissionsPar
        {
            public string SessionID; public int UserID;
        }

        [HttpPost]
        public ActionResult GetUserPermissions(GetUserPermissionsPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetUserPermissions(par.UserID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetUserPermissions2
        public class GetUserPermissions2Par
        {
            public string SessionID; public string userName; public string password;
        }

        [HttpPost]
        public ActionResult GetUserPermissions2(GetUserPermissions2Par par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).GetUserPermissions(par.userName, par.password));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region Rename
        public class RenamePar
        {
            public string SessionID; public int ObjectID; public string NewName;
        }

        [HttpPost]
        public ActionResult Rename(RenamePar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).Rename(par.ObjectID, par.NewName);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetPermission
        public class SetPermissionPar
        {
            public string SessionID; public int ObjectID; public string UserName; public PermissionState state;
        }

        [HttpPost]
        public ActionResult SetPermission(SetPermissionPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                ((SecurityService)base.SessionObject).SetPermission(par.ObjectID, par.UserName, par.state);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region User
        public class UserPar
        {
            public string SessionID; public string user;
        }

        [HttpPost]
        public new ActionResult User(UserPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).User(par.user));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region IsPermited
        public class IsPermitedPar
        {
            public string SessionID; public string obj;
        }

        [HttpPost]
        public ActionResult IsPermited(IsPermitedPar par)
        {
            try
            {
                this.SetSessionObject(par.SessionID);
                return Json(((SecurityService)base.SessionObject).IsPermited(par.obj));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
    }
}

