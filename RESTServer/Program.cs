﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace INTAPS.ClientServer.Server
{
    public class Program
    {
        internal static List<Assembly> controllers;

        public static void Main(string[] args)
        {
            controllers = InitializeApplicationServer();
            var host = CreateWebHostBuilder(args)
                .Build();
            host.Run();
            Console.WriteLine("RESTServer exiting");
            ApplicationServer.ShutDown();
        }


        static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("Shutting down application server");
            ApplicationServer.ShutDown();
        }
        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            CurrentDomain_ProcessExit(sender, e);
        }
        private static List<Assembly> InitializeApplicationServer()
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(CurrentDomain_ProcessExit);

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;


            ServicesSection section = (ServicesSection)ConfigurationManager.GetSection("ServiceConfiguration");
            BDETypeCollection bDEClasses = section.BDEClasses;
            var controllerAssemblyEntries = section.ControllerAssemblies;
            string[][] bDETypes = new string[bDEClasses.Count][];
            var controllerAssemblies = new List<Assembly>();
            for (int i = 0; i < bDETypes.Length; i++)
            {
                bDETypes[i] = new string[] { bDEClasses[i].BDEName, bDEClasses[i].BDEClass };
            }
            for (int i = 0; i < controllerAssemblyEntries.Count; i++)
            {
                try
                {
                    controllerAssemblies.Add(Assembly.LoadFile(Path.Combine(Environment.CurrentDirectory,controllerAssemblyEntries[i].ControllerAssembly)));
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error loading controller assembly: " + controllerAssemblyEntries[i].ControllerAssembly);
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                }
            }

            string exp = ConfigurationManager.AppSettings["sessionExpirySeconds"];
            string singleLogin = ConfigurationManager.AppSettings["singleLogin"];
            ApplicationServer.Initialize(bDETypes, new ConsolEventLoger(), string.IsNullOrEmpty(exp) ? 1800 : int.Parse(exp), string.IsNullOrEmpty(singleLogin) ? false : singleLogin.Equals("true", StringComparison.CurrentCultureIgnoreCase));
            RunMaintainanceJob();
            return controllerAssemblies;
        }

        

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var config = new ConfigurationBuilder()
                          .SetBasePath(Directory.GetCurrentDirectory())
                          .AddJsonFile("hosting.json", optional: true, reloadOnChange: true)
                          .Build();

//            var config = new ConfigurationBuilder()
  //                           .AddJsonFile("hosting.json", true)
    //                         .Build();

            var ret = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(config)
                .UseStartup<Startup>()
              ;

            return ret;
        }

        public static void RunMaintainanceJob()
        {
            bool flag;
            string str = ConfigurationManager.AppSettings["MaintainanceJob"];
            if ((!string.IsNullOrEmpty(str) && bool.TryParse(str, out flag)) && flag)
            {
                string str2 = ConfigurationManager.AppSettings["MaintainanceJobClass"];
                if (string.IsNullOrEmpty(str2))
                {
                    ApplicationServer.EventLoger.Log("Job defination not given.");
                }
                else
                {
                    Type type = Type.GetType(str2);
                    if (type == null)
                    {
                        ApplicationServer.EventLoger.Log("Could not find the job class:" + str2);
                    }
                    else
                    {
                        MethodInfo method = type.GetMethod("DoJob", new Type[] { typeof(string) });
                        if (method == null)
                        {
                            ApplicationServer.EventLoger.Log("Could not find DoJob method from:" + str2);
                        }
                        else if (!method.IsStatic)
                        {
                            ApplicationServer.EventLoger.Log("Could not find DoJob method is not static:" + str2);
                        }
                        else
                        {
                            string str3 = ConfigurationManager.AppSettings["MaintainanceJobName"];
                            try
                            {
                                ApplicationServer.EventLoger.Log("Executing " + str2);
                                Console.ForegroundColor = ConsoleColor.Blue;
                                Console.BackgroundColor = ConsoleColor.Yellow;
                                method.Invoke(null, new object[] { str3 });
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.BackgroundColor = ConsoleColor.Black;
                                ApplicationServer.EventLoger.Log("Executing " + str2 + "  done");
                            }
                            catch (Exception exception)
                            {
                                ApplicationServer.EventLoger.LogException("Erorr executng job:" + str2, exception);
                            }
                            return;
                        }
                    }
                }
            }
            ApplicationServer.EventLoger.Log("No maintainance job.");
        }
    }
}

