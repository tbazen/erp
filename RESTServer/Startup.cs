﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PWF.api;
using Sieve.Services;


namespace INTAPS.ClientServer.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }
        
       

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.CookieName = ".ASPNetCoreSession";
                options.CookiePath = "/";
            });

            services.AddAntiforgery(opts =>
            {
                opts.CookieName = ".ASPNetCoreSession";
                opts.CookiePath = "/";
            });
            services.AddMvc().AddJsonOptions(opts =>
            {
                opts.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
                opts.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors();
            foreach (var a in Program.controllers)
            {
                try
                {
                    if (a != null)
                    {
                        var part = new AssemblyPart(a);
                        services.AddMvc()
                            .ConfigureApplicationPartManager(apm => apm.ApplicationParts.Add(part));
                        Console.WriteLine("Added assembly for controller: " + a.FullName);
                    }
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException($"Error loading controller assembly {a.FullName}", ex);
                }
            }

            intaps.cis.api.CIS9App.InitWebApp(services, this.Configuration);
            PWFApp.InitWebApp(services, this.Configuration);
            omms.api.OmmsApp.InitWebApp(services, this.Configuration);

            services.AddScoped<ISieveProcessor, SieveProcessor>();
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var origins=this.Configuration.GetSection("API_Origins").Get<String[]>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            var o =app.UseCors(builder => builder
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                .WithOrigins(origins));
            app.UseSession();
            app.UseMvc();
            

        }
    }
}
