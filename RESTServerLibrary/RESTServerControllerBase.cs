namespace INTAPS.ClientServer.Server
{
    using INTAPS.ClientServer;
    using INTAPS.ClientServer.Server;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;

    public class RESTServerControllerBase<T> : Controller
    {
        protected object SessionObject;
        
        public virtual void SetSessionObject(string SessionID)
        {
            this.SessionObject = ApplicationServer.GetSessionObject(SessionID, typeof(T));
        }

        protected ActionResult Exception(Exception ex)
        {
            try
            {
                var bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                using (var ms = new System.IO.MemoryStream())
                {
                    bf.Serialize(ms, ex);
                    var sex = new ServerException() { type = ex.GetType().ToString() };
                    sex.exception = new byte[ms.Length];
                    sex.message = ex.Message;
                    ms.Seek(0,System.IO.SeekOrigin.Begin);
                    ms.Read(sex.exception, 0,sex.exception.Length);
                    return base.StatusCode(500, sex);
                }
            }
            catch
            {
                ApplicationServer.EventLoger.LogException(null,ex);
                return base.StatusCode(500, new ServerException() { type = ex.GetType().ToString(), exception = null });
            }
        }
    }
}

