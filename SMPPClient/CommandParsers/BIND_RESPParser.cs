using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public abstract class BIND_RESPParser : PDUCommandParser
    {
        int nByte = 0;
        PDUFieldParser currentParser = null;
        IntParser sizeParser = new IntParser();
        COctetParser systemIDParser = new COctetParser();
        TLVParser versionParser = new TLVParser();
        protected virtual uint bindType
        {
            get
            {
                return SMPPCommands.bind_receiver_resp;
            }
        }
        public override PDUCommandParserState processByte(byte b)
        {
            nByte++;
            if (currentParser == null)
            {
                currentParser = sizeParser;

            }
            if (currentParser.processByte(b))
            {
                if (currentParser == sizeParser)
                {
                    currentParser = commandIDParser;
                }
                else if (currentParser == commandIDParser)
                {
                    if (commandIDParser.val != this.bindType)
                        return PDUCommandParserState.Error;
                    currentParser = commandStatusParser;
                }
                else if (currentParser == commandStatusParser)
                {
                    currentParser = sequenseNumberParser;
                }
                else if (currentParser == sequenseNumberParser)
                {
                    currentParser = systemIDParser;
                }
                else if (currentParser == systemIDParser)
                {
                    if (nByte == sizeParser.val)
                        return PDUCommandParserState.Done;
                }
            }
            if (currentParser != sizeParser && nByte == sizeParser.val)
                return PDUCommandParserState.Error;
            return PDUCommandParserState.Continue;
        }
    }

    
    
    
}
