using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public class BIND_TRANSIVER_RESPParser : BIND_RESPParser
    {
        protected override uint bindType
        {
            get
            {

                return SMPPCommands.bind_transceiver_resp;
            }
        }
    }
}
