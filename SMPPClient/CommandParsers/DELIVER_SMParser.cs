﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TSMPPClient;

namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public class DELIVER_SMParser : PDUCommandParserBase
    {
        public enum SMSDeliveryStatus
        {
            Delivered,
            Failed
        }
        public class SMSDeliveryReport
        {
            public string messageID="";
            public SMSDeliveryStatus status = SMSDeliveryStatus.Delivered;
       }
        Int8Parser esm_class;
        OctetWithSizeParser<Int8Parser> msg;
        COctetParser fromField;
        COctetParser toField;
        
        public DELIVER_SMParser()
            : base(SMPPCommands.deliver_sm, 18)
        {
        }
        protected override PDUFieldParser[] getMandatoryFields()
        {
            return  new PDUFieldParser[]{
                base.commandStatusParser//command_status
                ,base.sequenseNumberParser//equence_number
                ,new COctetParser()    //service_type
                
                ,new Int8Parser()   //source_addr_ton
                ,new Int8Parser()   //source_addr_npi
                ,fromField=new COctetParser() //source_addr

                ,new Int8Parser()   //dest_addr_ton
                ,new Int8Parser()   //dest_addr_npi
                ,toField=new COctetParser() //destination_addr

                ,esm_class=new Int8Parser()   //esm_class
                ,new Int8Parser()   //protocol_id
                ,new Int8Parser()   //priority_flag
                ,new COctetParser() //schedule_delivery_time
                ,new COctetParser() //validity_period
                ,new Int8Parser()    //registered_delivery
                ,new Int8Parser()   //replace_if_present_flag
                ,new Int8Parser()   //data_coding
                ,new Int8Parser()   //sm_default_msg_id
                ,msg=new OctetWithSizeParser<Int8Parser>() //sm_length,short_message
            };
        }
        public bool isDeliveryReport
        {
            get
            {
                return (esm_class.val & 0x3C)==0x04;
            }
        }
        public SMSDeliveryReport deliveryReport
        {
            get
            {
                //TLVParser field;
                //if(base.optionalFields.TryGetValue(OptionalParamterTag.user_message_reference,out field))
                //{
                //    return ((Int16Parser)field.value).val.ToString();
                //}
                SMSDeliveryReport  ret=new SMSDeliveryReport();
                
                string[] parts = sms.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string part in parts)
                {
                    string[] kv = part.Split(':');
                    switch (kv[0])
                    {
                        case "stat":
                            if (kv[1].Equals("DELIVRD"))
                                ret.status=SMSDeliveryStatus.Delivered;
                            else
                                ret.status=SMSDeliveryStatus.Failed;
                            break;
                        case "id":
                            ret.messageID = kv[1];
                            break;
                    }
                }
                return ret;
            }
        }
        public string sms
        {
            get
            {
                return this.msg.val;
            }
        }

        public string from
        {
            get
            {
                return fromField.val;
            }
        }
        public string to
        {
            get
            {
                return toField.val;
            }
        }
    }
}
