using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public class GENERIC_NACKParser : PDUCommandParser
    {

        int nByte = 0;
        PDUFieldParser currentParser = null;
        IntParser sizeParser = new IntParser();

        public override PDUCommandParserState processByte(byte b)
        {
            nByte++;
            if (currentParser == null)
            {
                currentParser = sizeParser;

            }
            if (currentParser.processByte(b))
            {
                if (currentParser == sizeParser)
                {
                    currentParser = commandIDParser;
                }
                else if (currentParser == commandIDParser)
                {
                    if (commandIDParser.val != SMPPCommands.generic_nack)
                        return PDUCommandParserState.Error;
                    currentParser = commandStatusParser;
                }
                else if (currentParser == commandStatusParser)
                {
                    currentParser = sequenseNumberParser;
                }
                else if (currentParser == sequenseNumberParser)
                {
                    return PDUCommandParserState.Done;
                }
            }
            if (currentParser != sizeParser && nByte == sizeParser.val)
                return PDUCommandParserState.Error;
            return PDUCommandParserState.Continue;
        }
    }
}
