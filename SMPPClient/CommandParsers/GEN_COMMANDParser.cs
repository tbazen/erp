using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public class GEN_COMMANDParser : PDUCommandParser
    {
        
        PDUFieldParser currentParser = null;
        IntParser sizeParser = new IntParser();
        BinaryDataParser bodyParser;
        public override PDUCommandParserState processByte(byte b)
        {
            
            if (currentParser == null)
            {
                currentParser = sizeParser;

            }
            if (currentParser.processByte(b))
            {
                if (currentParser == sizeParser)
                {
                    currentParser = commandIDParser;
                }
                else if (currentParser == commandIDParser)
                {
                    currentParser = commandStatusParser;
                }
                else if (currentParser == commandStatusParser)
                {
                    currentParser = sequenseNumberParser;
                }
                else if (currentParser == sequenseNumberParser)
                {
                    if(sizeParser.val>16)
                        currentParser = bodyParser = new BinaryDataParser(sizeParser.val-16);
                    else
                        return PDUCommandParserState.Done;
                }
                else if (currentParser == bodyParser)
                {
                    return PDUCommandParserState.Done;
                }
            }
            return PDUCommandParserState.Continue;
        }
        public override string ToString()
        {

            return string.Format("Command ID:{0} , status:{1} ", commandIDParser.val.ToString("X8")
                , SMPPCommandStatus.getStatusMessage(commandStatusParser.val));

        }
    }
}
