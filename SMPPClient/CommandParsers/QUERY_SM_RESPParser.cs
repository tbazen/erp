using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public class QUERY_SM_RESPParser : PDUCommandParser
    {

        int nByte = 0;
        PDUFieldParser currentParser = null;
        IntParser sizeParser = new IntParser();
        COctetParser messageIDParser = new COctetParser();
        COctetParser finalDateParser= new COctetParser();
        Int8Parser messageStatusParser = new Int8Parser();
        Int8Parser errorCodeParser = new Int8Parser();
        public string messageID
        {
            get
            {
                return messageIDParser.val;
            }
        }
        public byte status
        {
            get
            {
                return messageStatusParser.val;
            }
        }
        public override PDUCommandParserState processByte(byte b)
        {
            nByte++;
            if (currentParser == null)
            {
                currentParser = sizeParser;

            }
            if (currentParser.processByte(b))
            {
                if (currentParser == sizeParser)
                {
                    currentParser = commandIDParser;
                }
                else if (currentParser == commandIDParser)
                {
                    if (commandIDParser.val != SMPPCommands.query_sm_resp   )
                        return PDUCommandParserState.Error;
                    currentParser = commandStatusParser;
                }
                else if (currentParser == commandStatusParser)
                {
                    currentParser = sequenseNumberParser;
                }
                else if (currentParser == sequenseNumberParser)
                {
                    currentParser = messageIDParser;
                }
                else if (currentParser == messageIDParser)
                {
                    currentParser = finalDateParser;
                }
                else if (currentParser == finalDateParser)
                {
                    currentParser = messageStatusParser;
                }
                else if (currentParser == messageStatusParser)
                {
                    currentParser = errorCodeParser;
                }
                else if (currentParser == errorCodeParser)
                {
                    return PDUCommandParserState.Done;
                }
            }
            if (currentParser != sizeParser && nByte == sizeParser.val)
                return PDUCommandParserState.Error;
            return PDUCommandParserState.Continue;
        }
    }
}
