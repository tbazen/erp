using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.CommandParsers
{
    [Serializable]
    public class SUBMIT_SM_RESPParser : PDUCommandParser
    {

        int nByte = 0;
        PDUFieldParser currentParser = null;
        IntParser sizeParser = new IntParser();
        COctetParser messageIDParser = new COctetParser();
        public string messageID
        {
            get
            {
                return messageIDParser.val;
            }
        }
        public override PDUCommandParserState processByte(byte b)
        {
            nByte++;
            if (currentParser == null)
            {
                currentParser = sizeParser;

            }
            if (currentParser.processByte(b))
            {
                if (currentParser == sizeParser)
                {
                    currentParser = commandIDParser;
                }
                else if (currentParser == commandIDParser)
                {
                    if (commandIDParser.val != SMPPCommands.submit_sm_resp)
                        return PDUCommandParserState.Error;
                    currentParser = commandStatusParser;
                }
                else if (currentParser == commandStatusParser)
                {
                    currentParser = sequenseNumberParser;
                }
                else if (currentParser == sequenseNumberParser)
                {
                    currentParser = messageIDParser;
                }
                else if (currentParser == messageIDParser)
                {
                    return PDUCommandParserState.Done;
                }
            }
            if (currentParser != sizeParser && nByte == sizeParser.val)
                return PDUCommandParserState.Error;
            return PDUCommandParserState.Continue;
        }

        public uint seqNo { get { return sequenseNumberParser.val; } }
        public uint status
        {
            get
            {
                return commandStatusParser.val;
            }
        }
    }
}
