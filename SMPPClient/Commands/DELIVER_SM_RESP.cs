using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class DELIVER_SM_RESP : PDUOut
    {

        uint send_seq_no;
        public DELIVER_SM_RESP(uint send_seq_no)
        {
            this.send_seq_no = send_seq_no;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                return new PDUField[]
                {
                    new IntField(SMPPCommands.deliver_sm_resp) //command_id
                    ,new IntField(SMPPCommandStatus.ESME_ROK) //command_status
                    ,new IntField(send_seq_no)    //sequence_numbera
                    ,new COctetField("")    //message_id
                };
            }
        }
    }
}
