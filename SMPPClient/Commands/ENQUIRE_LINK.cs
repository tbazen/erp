using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class ENQUIRE_LINK : PDUOut
    {
        uint _seqNo = 0;
        public ENQUIRE_LINK()
        {
            _seqNo = nextSeqNo;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                return new PDUField[]
                {
                    new IntField(SMPPCommands.enquire_link) //command_id
                    ,new IntField(0) //command_status
                    ,new IntField(_seqNo)    //sequence_number
                };
            }
        }
    }
}
