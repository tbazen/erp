using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class ENQUIRE_LINK_RESP : PDUOut
    {
        uint _seqNo = 0;
        public ENQUIRE_LINK_RESP()
        {
            _seqNo = nextSeqNo;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                return new PDUField[]
                {
                    new IntField(SMPPCommands.enquire_link_resp) //command_id
                    ,new IntField(SMPPCommandStatus.ESME_ROK) //command_status
                    ,new IntField(_seqNo)    //sequence_number
                };
            }
        }
    }
}
