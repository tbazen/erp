using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class QUERY_SM : PDUOut
    {
        uint _seqNo = 0;
        string _messageID;
        string _from;
        public QUERY_SM(string messageID,string from)
        {
            _seqNo = nextSeqNo;
            _messageID = messageID;
            _from = from;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                return new PDUField[]
                {
                    new IntField(SMPPCommands.query_sm) //command_id
                    ,new IntField(0) //command_status
                    ,new IntField(_seqNo)    //sequence_number
                    ,new COctetField(_messageID) //message_id
                    ,new Int8Field(0)    //source_addr_ton
                    ,new Int8Field(1)    //source_addr_npi
                    ,new COctetField(_from)    //source_addr address
                };
            }
        }
    }
}
