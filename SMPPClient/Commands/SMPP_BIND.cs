using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public abstract class SMPP_BIND : PDUOut
    {
        string _userName;
        string _passWord;
        uint _seqNo = 0;
        public SMPP_BIND(string userName, string password)
        {
            this._userName = userName;
            this._passWord = password;
            _seqNo = nextSeqNo;
        }
        protected abstract uint bindType
        {
            get;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                return new PDUField[]
                {
                    new IntField(this.bindType) //command_id
                    ,new IntField(0) //command_status
                    ,new IntField(_seqNo)    //sequence_number
                    ,new COctetField(_userName) //system_id
                    ,new COctetField(_passWord) //password
                    ,new COctetField("")    //system_typed
                    ,new Int8Field((byte)0x34) //interface_version
                    ,new Int8Field(0)    //addr_tone
                    ,new Int8Field(0)    //addr_npie
                    ,new COctetField("")    //address_rangee
                };
            }
        }
    }
    
}
