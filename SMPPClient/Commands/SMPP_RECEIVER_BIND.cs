using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class SMPP_RECEIVER_BIND : SMPP_BIND
    {
        public SMPP_RECEIVER_BIND(string userName, string password)
            : base(userName, password)
        {
        }
        protected override uint bindType
        {
            get
            {
                return SMPPCommands.bind_receiver;
            }
        }
    }
}
