using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class SMPP_TRANSIVER_BIND : SMPP_BIND
    {
        public SMPP_TRANSIVER_BIND(string userName, string password)
            : base(userName, password)
        {
        }
        protected override uint bindType
        {
            get
            {
                return SMPPCommands.bind_transceiver;
            }
        }
    }
}
