using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class SUBMIT_SM : PDUOut
    {
        string _message;
        string _from;
        string _to;
        uint _seqNo=0;
        ushort _concRef=0;
        ushort _concNum=0;
        ushort _concSeq=0;
        public string from
        {
            get
            {
                return _from;
            }

        }
        public string to
        {
            get
            {
                return _to;
            }
        }

        public uint seqNo
        {
            get
            {
                return _seqNo;
            }
        }
        public SUBMIT_SM(string from, string to, string message)
            : this(from, to, message, 0, 0, 0)
        {
        }
        public SUBMIT_SM(string from, string to, string message,ushort concRef,ushort concNum,ushort concSeq)
        {
            this._message = message;
            this._from = from;
            this._to = to;
            this._concNum = concNum;
            this._concRef = concRef;
            this._concSeq=concSeq;
            _seqNo = nextSeqNo;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                OctetField msg = new OctetField(_message);
                List<PDUField> fields = new List<PDUField>();
                fields.AddRange(
                new PDUField[]
                {
                    new IntField(SMPPCommands.submit_sm) //command_id
                    ,new IntField(0) //command_status
                    ,new IntField(_seqNo)    //sequence_number
                    ,new COctetField("") //service type
                    ,new Int8Field(0)    //source_addr_ton
                    ,new Int8Field(1)    //source_addr_npi
                    ,new COctetField(_from)    //source_addr address
                    ,new Int8Field(1)    //dest_addr_ton
                    ,new Int8Field(1)    //dest_addr_npi
                    ,new COctetField(_to)    //destination_addr address
                    ,new Int8Field(0x00)    //esm_class
                    ,new Int8Field(0x34)    //protocol_id
                    ,new Int8Field(0)    //priority_flag
                    ,new COctetField("")    //schedule_delivery_time
                    ,new COctetField("")    //validity_period
                    ,new Int8Field(1)    //registered_delivery
                    ,new Int8Field(0)    //replace_if_present_flag
                    ,new Int8Field(0)    //data_coding
                    ,new Int8Field(0)    //sm_default_msg_id
                    ,new Int8Field((byte)msg.size) //sm_length
                    ,msg    //short_message
                    ,new TLVField(OptionalParamterTag.user_message_reference,new Int16Field((ushort)nextSeqNoNoIncr))    //user_message_reference
                });
                if (_concRef > 0)
                {
                    fields.AddRange(new PDUField[]{
                        new TLVField(OptionalParamterTag.sar_msg_ref_num,new Int16Field(_concRef))
                        ,new TLVField(OptionalParamterTag.sar_total_segments,new Int16Field(_concNum))
                        ,new TLVField(OptionalParamterTag.sar_segment_seqnum,new Int16Field(_concSeq))
                    });
                }
                return fields;
            }
        }
    }
}
