using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace TSMPPClient.Commands
{
    [Serializable]
    public class UNBIND : PDUOut
    {
        uint _seqNo;
        public UNBIND()
        {
            _seqNo = nextSeqNo;
        }
        public override IEnumerable<PDUField> fields
        {
            get
            {
                return new PDUField[]
                {
                    new IntField(SMPPCommands.unbind) //command_id
                    ,new IntField(0) //command_status
                    ,new IntField(_seqNo)    //sequence_number
                };
            }
        }
    }
}
