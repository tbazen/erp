using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using TSMPPClient.CommandParsers;
namespace TSMPPClient
{
    [Serializable]
    public class PDUBuf
    {
        byte[] _buf;
        int _index;
        int _length;
        public PDUBuf(byte[] buf, int index, int length)
        {
            this._buf = buf;
            this._index = index;
            this._length = length;
        }
        public byte this[int i]
        {
            get
            {
                return _buf[_index + i];
            }
            set
            {
                _buf[_index + i] = value;
            }
        }
        public int length
        {
            get
            {
                return _length;
            }
        }

    }
    public enum PDUCommandParserState
    {
        Continue,
        Done,
        Error
    }
    static class PDUCommandType
    {

    }
    [Serializable]
    public abstract class PDUCommandParser
    {
        protected IntParser commandIDParser = new IntParser();
        protected IntParser commandStatusParser = new IntParser();
        protected IntParser sequenseNumberParser = new IntParser();

        public abstract PDUCommandParserState processByte(byte b);
        public uint commandID
        {
            get
            {
                return commandIDParser.val;
            }
        }
        public uint commandStatus
        {
            get
            {
                return commandStatusParser.val;
            }
        }
        public uint seqNo
        {
            get
            {
                return sequenseNumberParser.val;
            }
        }
    }
    public class PDUIn
    {
        static List<Type> pduTypes = new List<Type>();
        static PDUIn()
        {
            foreach (Type t in System.Reflection.Assembly.GetCallingAssembly().GetTypes()
                )
            {
                if(t.IsSubclassOf(typeof(PDUCommandParser)) 
                    && !t.IsAbstract
                    && t!=typeof(GEN_COMMANDParser)
                    )
                {
                    pduTypes.Add(t);
                }
            }
        }
        List<PDUCommandParser> activeParsers = new List<PDUCommandParser>();
        GEN_COMMANDParser genParser = new GEN_COMMANDParser();
        public PDUCommandParser command
        {
            get
            {
                if (activeParsers.Count == 0)
                    return null;
                return activeParsers[0];
            }
        }
        public PDUCommandParser parsedCommand=null;
        PDUTransport _transport;
        public PDUIn(PDUTransport transport)
        {
            _transport = transport;
            foreach (Type t in pduTypes)
            {
                activeParsers.Add(Activator.CreateInstance(t) as PDUCommandParser);
            }
        }
        public int processBuffer(PDUBuf buf, out bool done)
        {
            for (int i = 0; i < buf.length; i++)
            {
                for (int j = activeParsers.Count - 1; j >= 0; j--)
                {
                    switch (activeParsers[j].processByte(buf[i]))
                    {
                        case PDUCommandParserState.Done:
                            if (activeParsers.Count > 1)
                                _transport.processor.log(SMPPLogLevel.RawRead, "Ambigous command detected",null);
                            parsedCommand = activeParsers[j];
                            done = true;
                            return i + 1;
                        case PDUCommandParserState.Error:
                            activeParsers.RemoveAt(j);
                            break;
                    }
                }
                switch (genParser.processByte(buf[i]))
                {
                    case PDUCommandParserState.Done:
                        done = true;
                        if (activeParsers.Count == 0)
                            parsedCommand = genParser;
                        else
                            parsedCommand = null;
                        return i + 1;
                    case PDUCommandParserState.Error:
                        done = true;
                        parsedCommand = null;
                        return i + 1;
                }

            }
            done = false;
            return buf.length;
        }
        
    }
    [Serializable]
    public abstract class PDUFieldParser
    {
        public abstract bool processByte(byte b);
    }
    
    public interface IIntValueParser
    {
        uint intVal();
    }
    [Serializable]
    public class Int8Parser : PDUFieldParser,IIntValueParser
    {
        int shift = 24;
        public byte val = 0;
        public override bool processByte(byte b)
        {
            val = b;
            return true;
        }

        public uint intVal()
        {
            return (uint)val;
        }
    }
    [Serializable]
    public class BinaryDataParser : PDUFieldParser
    {
        uint sz;
        uint nbytes = 0;
        public BinaryDataParser(uint size)
        {
            sz = size;
        }
        public override bool processByte(byte b)
        {
            nbytes++;
            return sz == nbytes;
        }
    }
    [Serializable]
    public class IntParser : PDUFieldParser, IIntValueParser
    {
        int shift = 24;
        public uint val = 0;
        public override bool processByte(byte b)
        {
            val +=((uint)b) <<shift;
            shift -= 8;
            return shift < 0;
        }

        public uint intVal()
        {
            return (uint)val;
        }
    }
    [Serializable]
    public class Int16Parser : PDUFieldParser, IIntValueParser
    {
        int shift = 8;
        public ushort val = 0;
        public override bool processByte(byte b)
        {
            val += (ushort)(((uint)b) << shift);
            shift -= 8;
            return shift < 0;
        }
        public uint intVal()
        {
            return (uint)val;
        }
    }
    [Serializable]
    public class COctetParser : PDUFieldParser
    {
        int shift = 24;
        StringBuilder _val= new StringBuilder();
        public string val
        {
            get
            {
                return _val.ToString();
            }
        }
        public override bool processByte(byte b)
        {
            if (b == 0)
                return true;
            _val.Append(((char)b).ToString());
            return false;
        }
    }
    [Serializable]
    public class OctetWithSize : PDUFieldParser 
    {
        int shift = 24;
        StringBuilder _val = new StringBuilder();
        uint size;
        bool parsingSize=true;
        uint nBytes = 0;
        public OctetWithSize(uint size)
        {
            this.size = size;
        }
        public string val
        {
            get
            {
                return _val.ToString();
            }
        }
        public override bool processByte(byte b)
        {
            _val.Append(((char)b).ToString());
            nBytes++;
            return nBytes==size;
        }
    }

    [Serializable]
    public class OctetWithSizeParser<SizeParserType> : PDUFieldParser where SizeParserType : PDUFieldParser, IIntValueParser, new()
    {
        int shift = 24;
        StringBuilder _val = new StringBuilder();
        SizeParserType sizeParser = new SizeParserType();
        bool parsingSize = true;
        uint nBytes = 0;
        public string val
        {
            get
            {
                return _val.ToString();
            }
        }
        public override bool processByte(byte b)
        {
            if (parsingSize)
            {
                if (sizeParser.processByte(b))
                {
                    parsingSize = false;
                    nBytes = 0;
                }
                return false;
            }
            _val.Append(((char)b).ToString());
            nBytes++;
            return nBytes == sizeParser.intVal();
        }
    }
    [Serializable]
    public class TLVParser : PDUFieldParser
    {
        Int16Parser tagParser = new Int16Parser();
        Int16Parser lengthParser = new Int16Parser();
        PDUFieldParser valueParser=null;
        PDUFieldParser currentParser = null;
        public ushort tag
        {
            get
            {
                return tagParser.val;
            }
        }
        public PDUFieldParser value
        {
            get
            {
                return valueParser;
            }
        }
        public override bool processByte(byte b)
        {
            if (currentParser == null)
                currentParser = tagParser;
            if (currentParser.processByte(b))
            {
                if (currentParser == tagParser)
                    currentParser = lengthParser;
                else if (currentParser == lengthParser)
                {
                    if(lengthParser.val==0)
                        return true;
                    valueParser=currentParser = OptionalParamterTag.createParser(tagParser.val,lengthParser.val);
                }
                else
                    return true;
            }
            return false;
        }
    }

    [Serializable]
    public abstract class PDUCommandParserBase : PDUCommandParser
    {
        int nByte = 0;

        IntParser sizeParser = new IntParser();
        TLVParser optParser= null;

        PDUFieldParser[] mandatoryFields;
        int nOpt;
        
        PDUFieldParser currentParser = null;
        int currentIndex = -1;
        bool mandatory = true;
        uint cmdID;
        protected Dictionary<ushort, TLVParser> optionalFields=new Dictionary<ushort,TLVParser>();
        public PDUCommandParserBase(uint cmdID, int nOpt)
        {
            this.cmdID = cmdID;
            this.mandatoryFields = getMandatoryFields();
            this.nOpt = nOpt;
            base.commandIDParser = new IntParser();
        }
        protected abstract PDUFieldParser[] getMandatoryFields();
        public override PDUCommandParserState processByte(byte b)
        {
            nByte++;
            if (currentParser == null)
            {
                currentParser = sizeParser;
                currentIndex = -1;
            }
            if (currentParser.processByte(b))
            {
                if (currentParser == sizeParser)
                {
                    currentParser = commandIDParser;
                }
                else if (currentParser == commandIDParser)
                {
                    if (commandIDParser.val != cmdID)
                        return PDUCommandParserState.Error;
                    if (mandatoryFields.Length > 0)
                    {
                        currentIndex = 0;
                        mandatory = true;
                        currentParser = mandatoryFields[currentIndex];
                    }
                    else if (nByte == sizeParser.val)
                    {
                        return PDUCommandParserState.Done;
                    }
                    else
                        return PDUCommandParserState.Error;

                }
                else if (mandatory)
                {
                    currentIndex++;
                    if (currentIndex == mandatoryFields.Length)
                    {
                        if (nByte == sizeParser.val)
                        {
                            return PDUCommandParserState.Done;
                        }
                        if (nOpt > 0)
                        {
                            currentIndex = 0;
                            mandatory = false;
                            currentParser =optParser= new TLVParser();
                        }
                        else if (nByte == sizeParser.val)
                        {
                            return PDUCommandParserState.Done;
                        }
                        else
                            return PDUCommandParserState.Error;
                    }
                    else
                    {
                        currentParser = mandatoryFields[currentIndex];
                    }
                }
                else//optional fields
                {
                    optionalFields.Add(optParser.tag, optParser);
                    
                    if (nByte == sizeParser.val)
                    {
                        return PDUCommandParserState.Done;
                    }

                    if (optionalFields.Count == nOpt)
                    {
                        return PDUCommandParserState.Error;
                    }
                    else
                    {
                        currentParser = optParser = new TLVParser();
                    }
                }
                
            }
            if (currentParser != sizeParser && nByte == sizeParser.val)
                return PDUCommandParserState.Error;
            return PDUCommandParserState.Continue;
        }
        
    }
}
