﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSMPPClient
{
    public abstract class PDUField
    {
        public abstract uint size { get; }
        public abstract uint write(byte[] buf, uint index);
    }
    [Serializable]
    public abstract class PDUOut
    {
        static uint _seqNo = 0;

        public static uint SeqNo
        {
            get
            { return PDUOut._seqNo; }
            set
            {
                PDUOut._seqNo = value;
            }
        }
        protected static uint nextSeqNo
        {
            get
            {
                _seqNo++;
                return _seqNo;
            }
        }
        protected static uint nextSeqNoNoIncr
        {
            get
            {
                return _seqNo+1;
            }
        }
        public byte[] getData()
        {
            uint size = 0;
            foreach (PDUField f in fields)
            {
                size += f.size;
            }
            uint index = 0;
            IntField szField = new IntField(size + 4);
            byte[] ret = new byte[size+4];
            szField.write(ret, index);
            index += 4;
            foreach (PDUField f in fields)
            {
                f.write(ret, index);
                index += f.size;
            }
            return ret;
        }
        public abstract IEnumerable<PDUField> fields { get; }
        public void getCommonPars(out uint command_id, out uint command_status, out uint seqno)
        {
            IEnumerator<PDUField> e = fields.GetEnumerator();
            command_id = 0;
            command_status = 0;
            seqno = 0;
            if (!e.MoveNext())
                return;
            command_id = ((IntField)e.Current).val;
            if (!e.MoveNext())
                return;
            command_status = ((IntField)e.Current).val;
            if (!e.MoveNext())
                return;
            seqno = ((IntField)e.Current).val;

        }
    }
    [Serializable]
    public class IntField : PDUField
    {
        uint _val;
        public uint val
        {
            get
            {
                return _val;
            }
        }
        public IntField(uint val)
        {
            this._val = val;
        }
        public override uint size
        {
            get { return 4; }
        }

        public override uint write(byte[] buf, uint index)
        {
            buf[index++] = (byte)(_val >> 24);
            buf[index++] = (byte)((_val >> 16) & 0xFF);
            buf[index++] = (byte)((_val >> 8) & 0xFF);
            buf[index++] = (byte)(_val & 0xFF);
            return index;
        }
    }
    [Serializable]
    public class Int16Field : PDUField
    {
        ushort _val;
        public Int16Field(ushort val)
        {
            this._val = val;
        }
        public override uint size
        {
            get { return 2; }
        }

        public override uint write(byte[] buf, uint index)
        {
            buf[index++] = (byte)((_val >> 8) & 0xFF);
            buf[index++] = (byte)(_val & 0xFF);
            return index;
        }
    }
    [Serializable]
    public class Int8Field : PDUField
    {
        byte _val;
        public Int8Field(byte val)
        {
            this._val = val;
        }
        public override uint size
        {
            get { return 1; }
        }

        public override uint write(byte[] buf, uint index)
        {
            buf[index++] = _val;
            return index;
        }
    }
    [Serializable]
    public class COctetField : PDUField
    {
        byte[] bytes;
        public COctetField(string val)
        {
            this.bytes = System.Text.ASCIIEncoding.GetEncoding(0).GetBytes(val);
        }
        public override uint size
        {
            get {
                return (uint)bytes.Length + 1;
            }
        }

        public override uint write(byte[] buf, uint index)
        {
            bytes.CopyTo(buf, index);
            index += (uint)bytes.Length;
            buf[index] = 0;
            index++;
            return index;
        }
    }


    [Serializable]
    public class OctetField : PDUField
    {
        byte[] bytes;
        public OctetField(string val)
        {
            this.bytes = System.Text.ASCIIEncoding.GetEncoding(0).GetBytes(val);
        }
        public override uint size
        {
            get
            {
                return (uint)bytes.Length;
            }
        }

        public override uint write(byte[] buf, uint index)
        {
            bytes.CopyTo(buf, index);
            index += (uint)bytes.Length;
            return index;
        }
    }
    [Serializable]
    public class TLVField : PDUField
    {
        Int16Field _tag;
        Int16Field _size;

        PDUField _f;
        public TLVField(ushort tag, PDUField f)
        {
            _tag = new Int16Field(tag);
            _f = f;
            _size = new Int16Field((ushort)_f.size);
            
        }

        public override uint size
        {
            get
            {
                return _tag.size + _size.size + _f.size;
            }
        }

        public override uint write(byte[] buf, uint index)
        {
            index = _tag.write(buf, index);
            index = _tag.write(buf, index);
            index = _tag.write(buf, index);
            return index;
        }
    }
 
}
