﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace TSMPPClient
{
    
    public interface IPDUProcessor
    {
        void handleConnected();
        void handleDisconnected();
        void processPDU(PDUIn pdu);
        void log(SMPPLogLevel level, string message,object data);
    }
    public enum PDUParserState
    {
        Disconnected,
        Connected,
    }
    public class PDUTransport
    {

        System.Threading.Thread _sendThread;
        System.Threading.Thread _receiveThread;
        TcpClient _socket=null;
        
        IPDUProcessor _processor;
        PDUParserState _state = PDUParserState.Disconnected;
        public PDUParserState State
        {
            get { return _state; }
        }
        string _serverIP;
        int _serverPort;
        bool _run=false;
        Queue<PDUOut> _sendQueue = new Queue<PDUOut>();
        //Semaphore _sem = new Semaphore(0,100);
        int _id;
        public PDUTransport(string serverIP,int serverPort,IPDUProcessor processor,int id)
        {
            this._processor = processor;
            this._serverIP = serverIP;
            this._serverPort = serverPort;
            this._id = id;
        }
        public IPDUProcessor processor
        {
            get
            {
                return _processor;
            }
        }
        
        public void start()
        {
            _run = true;
            _sendThread = new System.Threading.Thread(new System.Threading.ThreadStart(send));
            _sendThread.Start();
            _receiveThread = new System.Threading.Thread(new System.Threading.ThreadStart(receive));
            _receiveThread.Start();

        }
        
        void disconnect()
        {
            try
            {   
                _state = PDUParserState.Disconnected;
                if(_socket.Connected)
                    _socket.Close();
            }
            catch (Exception ex)
            {
                _processor.log(SMPPLogLevel.Exception, "Error trying to close connection.\n" + ex.Message,ex);
            }
            finally
            {
                _processor.handleDisconnected();
            }
        }
        
        public void sendPDU(PDUOut pdu)
        {
            lock (_sendQueue)
            {
                _sendQueue.Enqueue(pdu);
                //_sem.Release();
            }
        }
        static int wait_after_exception= SMPPSystemParamters.WAIT_AFTER_SEND_THREAD_EXCEPTION_MIN;
        void send()
        {
            _processor.log(SMPPLogLevel.Synchronization, string.Format("Send thread {0} starting", _id),_id);
            try
            {
                while (_run)
                {
                    try
                    {
                        _state = PDUParserState.Disconnected;
                        _socket = new TcpClient();
                        IPAddress ip = IPAddress.Parse(_serverIP);
                        _socket.Connect(ip, _serverPort);
                        wait_after_exception = SMPPSystemParamters.WAIT_AFTER_SEND_THREAD_EXCEPTION_MIN;
                        _state = PDUParserState.Connected;
                        _processor.handleConnected();
                        while (_run)
                        {
                            System.Threading.Thread.Sleep(SMPPSystemParamters.SEND_QUEUE_POLL_INTERVAL);
                            lock (_sendQueue)
                            {
                                while (_sendQueue.Count > 0)
                                {
                                    PDUOut pdu = _sendQueue.Dequeue();
                                    byte[] data = pdu.getData();
                                    _socket.GetStream().Write(data, 0, data.Length);
                                    _processor.log(SMPPLogLevel.RawWrite, SMPPClient.byteToString(data, 0, data.Length),data);
                                    _processor.log(SMPPLogLevel.PDUSent, "PDU Sent: "+pdu.ToString(),pdu);
                                }
                            }
                            if (!_socket.Connected)
                                break;
                        }
                        disconnect();
                    }
                    catch (Exception ex)
                    {
                        _processor.log(SMPPLogLevel.Exception, "Error in send thread.\n" + ex.Message,ex);
                        disconnect();
                        if (_run)
                        {
                            System.Threading.Thread.Sleep(wait_after_exception);
                            if(wait_after_exception<SMPPSystemParamters.WAIT_AFTER_SEND_THREAD_EXCEPTION_MAX)
                                wait_after_exception = wait_after_exception * (100+SMPPSystemParamters.WAIT_AFTER_SEND_THREAD_EXCEPTION_INCREASE_PERCENT) / 100;
                            
                        }
                    }
                }
            }
            finally
            {
                _processor.log(SMPPLogLevel.Synchronization, string.Format("Send thread {0} exiting", _id),_id);
            }
        }
        void receive()
        {
            _processor.log(SMPPLogLevel.Synchronization, string.Format("Receive thread {0} starting", _id),_id);
            try
            {
                while (_run)
                {
                    _processor.log(SMPPLogLevel.Synchronization, string.Format("Receive thread {0} polling connection avialability", _id),_id);
                    while (_socket == null || !_socket.Connected)
                    {
                        System.Threading.Thread.Sleep(SMPPSystemParamters.RECEIVE_THREAD_CONNECTION_STATUS_POLL_INTERVAL);
                        if (!_run)
                            return;
                    }
                    _processor.log(SMPPLogLevel.Synchronization, string.Format("Receive thread {0} detected connection", _id),_id);
                    try
                    {
                        byte[] readBuf = new byte[SMPPSystemParamters.RECEIVE_BUFFER_SIZE];
                        PDUIn pdu = null;
                        while (_run)
                        {
                            
                            int readLen = _socket.GetStream().Read(readBuf, 0, readBuf.Length);
                            _processor.log(SMPPLogLevel.RawRead, SMPPClient.byteToString(readBuf, 0, readLen),readBuf);

                            int index = 0;
                            do
                            {
                                if (pdu == null)
                                    pdu = new PDUIn(this);
                                bool done;
                                int plen = pdu.processBuffer(new PDUBuf(readBuf, index, readLen - index), out done);

                                index += plen;
                                if (done)
                                {
                                    _processor.processPDU(pdu);
                                    pdu = null;
                                }
                            }
                            while (index < readLen);
                        }
                    }
                    catch (Exception ex)
                    {
                        _processor.log(SMPPLogLevel.Exception, "Receive thread threw exception.\n" + ex.Message,ex);
                    }
                }
            }
            finally
            {
                _processor.log(SMPPLogLevel.Synchronization, string.Format("Receive thread {0} exiting", _id),_id);
            }
        }

        internal void stop()
        {
            _run = false;
            //_sem.Release();
        }

        public bool threadsAlive
        {
            get
            {
                return (_receiveThread != null && _receiveThread.IsAlive) || (_sendThread != null && _sendThread.IsAlive);
            }
        }

        public int id { get { return _id; } }
    }
}
