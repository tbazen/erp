﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSMPPClient
{
    class Program
    {
        static SMPPClient t;
        
        static void Main(string[] args)
        {
            t = new SMPPClient("10.204.182.92", 8313, "004604", "bishoftu1",0);
            t.clientStatusChanged += new SMPPClientStatusChangedHandler(t_clientStatusChanged);
            t.smsStatusChanged += new SMSStatusChangedHandler(t_smsStatusChanged);
            t.smsReceived += new SMSReceivedHandler(t_smsReceived);
            t.start();
            Console.WriteLine("Press enter to stop");
            Console.ReadLine();
            t.stop(false);
            while (t.Status != SMPPClientStatus.Disconnected)
                System.Threading.Thread.Sleep(1000);

        }

        static void t_smsReceived(SMS sms)
        {
            Console.WriteLine("SMS Received\n\tFrom: {0}\n\tTo: {1}\n\tMessage: {2}", sms.from, sms.to, sms.message);
        }

        static void t_smsStatusChanged(uint seqNo, SMSStatus status, string statusMessage)
        {
            Console.WriteLine("SMS Status seqno:{0} status:{1}", seqNo, statusMessage);
        }

        static void t_clientStatusChanged(SMPPClientStatus status)
        {
            Console.WriteLine(status.ToString());
            if (status == SMPPClientStatus.Connected)
            {
                //t.sendSMS("8029", "0911227220", "Hey");
            }
        }

    }
}
