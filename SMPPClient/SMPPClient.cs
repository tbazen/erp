using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TSMPPClient.CommandParsers;
using TSMPPClient.Commands;
namespace TSMPPClient
{
    [Flags]
    public enum SMPPLogLevel
    {
        Exception=1,
        Synchronization=2,
        StateChange=4,
        PDUSent=8,
        PDUReceived=16,
        RawRead=32,
        RawWrite=64,
        SMSSent=128,
        SMSReceived=256,
        All=Exception|Synchronization|StateChange|PDUSent|PDUReceived|RawRead|RawWrite|SMSSent|SMSReceived
    }

    public enum SMPPClientStatus
    {
        Connecting,
        Binding,
        Connected,
        Disconnecting,
        Disconnected,
    }
    public enum SMSStatus
    {
        Sending,
        Sent,
        SendingFailed,
        Delivered,
        DeliveryFailed
    }
    public interface ISMPPPersistance
    {
        void setMessageID(string messageID, uint seqNo);
        uint getSeqNoForMessageID(string messageID);
        string getMessageIDforSeqNo(uint seqNo);
    }
    public class InMemoryPersistance : ISMPPPersistance
    {
        Dictionary<string, uint> messageIDToSeqNo = new Dictionary<string, uint>();
        public void setMessageID(string messageID, uint seqNo)
        {
            messageIDToSeqNo.Add(messageID, seqNo);
        }

        public uint getSeqNoForMessageID(string messageID)
        {
            return messageIDToSeqNo[messageID];
        }

        public string getMessageIDforSeqNo(uint seqNo)
        {
            throw new NotImplementedException();
        }
    }
    public delegate void SMPPClientStatusChangedHandler(SMPPClientStatus status);
    public delegate void SMSStatusChangedHandler(uint seqNo,SMSStatus status,string statusMessage);
    public delegate void SMSReceivedHandler(SMS sms);
    public delegate void SMSLogHandler(SMPPLogLevel level,string message,object data);
    public delegate void SMSMessageIDSetHandler(int seqNo, string message);
    public class SMS
    {
        public uint seqNo;
        public string from;
        public string to;
        public string message;
    }
    public class SMPPClient : IPDUProcessor
    {
        const int INQUIRE_LINK_INTERVAL = 5000;
        public event SMPPClientStatusChangedHandler clientStatusChanged;
        public event SMSStatusChangedHandler smsStatusChanged;
        public event SMSReceivedHandler smsReceived;
        public event SMSLogHandler smppLog;

        void onStatusChanged()
        {
            log(SMPPLogLevel.StateChange, "State change: "+this.Status.ToString(),this.Status);
            if (clientStatusChanged != null)
                clientStatusChanged(this.Status);
        }
        void onSMSStatusChanged(uint seqNo, SMSStatus status, string statusMessage)
        {
            if (smsStatusChanged != null)
                smsStatusChanged(seqNo, status,statusMessage);
        }
        void onSMSReceived(SMS sms)
        {
            if (smsReceived != null)
                smsReceived(sms);
        }

        string _userName;
        string _passWord;
        PDUTransport t;
        SMPPClientStatus _status = SMPPClientStatus.Disconnected;
        ISMPPPersistance _persistance = new InMemoryPersistance(); //Dictionary<string, uint> messageIDToSeqNo = new Dictionary<string, uint>();

        string _ipAddress;
        int _port;
        static int tid=0;
        public SMPPClientStatus Status
        {
            get { return _status; }
        }
        public SMPPClient(string ipAddress,int port,string userName, string password,uint lastSeqNo)
        {
            this._userName = userName;
            this._passWord = password;
            this._ipAddress=ipAddress;
            this._port=port;
            PDUOut.SeqNo = lastSeqNo;
        }

        public void setPersistance(ISMPPPersistance p)
        {
            _persistance = p;
        }
        
        bool _runCheckLinkLoop = false;
        bool _runClient = false;
        public void start()
        {
            connectAttemptTime = DateTime.Now;
            _runClient = true;
            _status = SMPPClientStatus.Connecting;
            onStatusChanged();
            t = new PDUTransport(_ipAddress, _port, this, tid++);
            t.start();

        }
        public void stop(bool runClient)
        {
            _runCheckLinkLoop = false;
            _runClient = runClient;
            PDUTransport thisT = t;

            thisT.stop();

            log(SMPPLogLevel.Synchronization, string.Format("Waiting for threads for {0} shutdown.", thisT.id),thisT);
            while (thisT.threadsAlive)
            {
                System.Threading.Thread.Sleep(SMPPSystemParamters.THREAD_SHOTDOWN_POLL_INTERVAL);
            }
            log(SMPPLogLevel.Synchronization, string.Format("Threads of {0} shutdown.", thisT.id),thisT);

        }
        public void handleConnected()
        {
            _status = SMPPClientStatus.Binding;
            onStatusChanged();
            t.sendPDU(new SMPP_TRANSIVER_BIND(_userName, _passWord));

        }

        public void handleDisconnected()
        {
            _status = SMPPClientStatus.Disconnected;
            _runCheckLinkLoop = false;
            onStatusChanged();
        }
        public uint sendSMS(string from, string to, string msg)
        {
            if (_status == SMPPClientStatus.Connected)
            {
                SUBMIT_SM cmd=null;
                if (msg.Length <= 160)
                {
                    t.sendPDU(cmd = new SUBMIT_SM(from, to, msg));
                    return cmd.seqNo;
                }
                else
                {
                    int index = 0;
                    ushort concRef=(ushort)PDUOut.SeqNo;
                    ushort concNum = (ushort)(msg.Length / 160 + (msg.Length % 160 > 0 ? 1 : 0));
                    ushort concSeq = 1;
                    while (index < msg.Length)
                    {
                        t.sendPDU(cmd = new SUBMIT_SM(from, to, msg.Substring(index, Math.Min(160, msg.Length - index)),concRef,concNum,concSeq));
                        index += 160;
                        concSeq++;
                    }
                    return cmd.seqNo;
                }
            }
            else
                throw new InvalidOperationException("Can't send sms when disconnected");
        }
        DateTime lastEnq = DateTime.Now;
        DateTime connectAttemptTime;
       
        public void processPDU(PDUIn pdu)
        {
            log(SMPPLogLevel.PDUReceived,"Processing pdu " + (pdu.parsedCommand==null?"null":pdu.parsedCommand.ToString()),pdu);
            if (pdu.parsedCommand is BIND_RESPParser)
            {
                _status = SMPPClientStatus.Connected;
                _runCheckLinkLoop = true;
                lastEnq = DateTime.Now;
                new System.Threading.Thread(new System.Threading.ThreadStart(delegate()
                    {
                        while (_runCheckLinkLoop)
                        {
                            if (DateTime.Now.Subtract(lastEnq).TotalMilliseconds > SMPPSystemParamters.INQUIRE_LINK_RECEIVE_GIVE_UP_INTERVAL)
                            {
                                log(SMPPLogLevel.StateChange, "Link lost. Restarting",null);
                                if (!_runClient)
                                    break;
                                this.stop(true);
                                if (_runClient)
                                    break;
                                else
                                    this.start();
                            }
                            System.Threading.Thread.Sleep(SMPPSystemParamters.INQUIRE_LINK_SEND_INTERVAL);
                            t.sendPDU(new ENQUIRE_LINK());
                        }
                    }
                )).Start();
               
                onStatusChanged();
            }
            else if (pdu.parsedCommand is ENQUIRE_LINK_RESPParser)
            {
                lastEnq = DateTime.Now;
            }
            else if (pdu.parsedCommand is ENQUIRE_LINKParser)
            {
                t.sendPDU(new ENQUIRE_LINK_RESP());
            }
            else if (pdu.parsedCommand is SUBMIT_SM_RESPParser)
            {
                SUBMIT_SM_RESPParser cmd = (SUBMIT_SM_RESPParser)pdu.parsedCommand;
                if (cmd.status == SMPPCommandStatus.ESME_ROK)
                {
                    //lock (messageIDToSeqNo)
                    //{
                    //    this.messageIDToSeqNo.Add(cmd.messageID, cmd.seqNo);
                    //}
                    lock (_persistance)
                    {
                        _persistance.setMessageID(cmd.messageID, cmd.seqNo);
                    }
                    onSMSStatusChanged(cmd.seqNo, SMSStatus.Sent, "Sent");
                }
                else
                    onSMSStatusChanged(cmd.seqNo, SMSStatus.SendingFailed, SMPPCommandStatus.getStatusMessage(cmd.status));
            }
            else if (pdu.parsedCommand is UNBIND_RESPParser)
            {
                t.stop();
            }
            else if (pdu.parsedCommand is DELIVER_SMParser)
            {
                DELIVER_SMParser cmd = (DELIVER_SMParser)pdu.parsedCommand;
                t.sendPDU(new DELIVER_SM_RESP(cmd.seqNo));
                if (cmd.isDeliveryReport)
                {
                    DELIVER_SMParser.SMSDeliveryReport report = cmd.deliveryReport;
                    //lock (messageIDToSeqNo)
                    //{
                    //    if (messageIDToSeqNo.ContainsKey(report.messageID))
                    //    {
                    //        smsStatusChanged(messageIDToSeqNo[report.messageID], report.status == DELIVER_SMParser.SMSDeliveryStatus.Delivered ? SMSStatus.Delivered : SMSStatus.DeliveryFailed, report.status.ToString());
                    //        messageIDToSeqNo.Remove(report.messageID);
                    //    }
                    //}
                    lock (_persistance)
                    {
                        uint seqNo=_persistance.getSeqNoForMessageID(report.messageID);
                        if (seqNo > 0)
                        {
                            smsStatusChanged(seqNo, report.status == DELIVER_SMParser.SMSDeliveryStatus.Delivered ? SMSStatus.Delivered : SMSStatus.DeliveryFailed, report.status.ToString());
                        }
                    }
                }
                else
                {
                    onSMSReceived(new SMS()
                    {
                        seqNo = cmd.seqNo,
                        from = cmd.from,
                        to = cmd.to,
                        message = cmd.sms,
                    });
                }
            }
            else if (pdu.command is QUERY_SM_RESPParser)
            {
                QUERY_SM_RESPParser resp = (QUERY_SM_RESPParser)pdu.command;
                lock (_persistance)
                {
                    uint seqNo = _persistance.getSeqNoForMessageID(resp.messageID);
                    if (seqNo > 0)
                    {
                        switch (resp.status)
                        {
                            case 2:
                            case 6:
                                smsStatusChanged(seqNo, SMSStatus.Delivered, "Delivered");
                                break;
                            case 1:
                                break;
                            default:
                                smsStatusChanged(seqNo, SMSStatus.DeliveryFailed, "Failed");
                                break;
                        }
                    }
                }
            }
            else if (pdu.parsedCommand == null || pdu.parsedCommand is GEN_COMMANDParser)
            {
                t.sendPDU(new GENERIC_NACK());
            }
        }

        public static string byteToString(byte[] bytes, int index, int length)
        {
            StringBuilder ret = new StringBuilder();
            for (int i = 0; i < length; i++)
                ret.Append(bytes[index+i].ToString("X2"));
            return ret.ToString();
        }


        public void log(SMPPLogLevel level, string message,object data)
        {
            if (smppLog != null)
                smppLog(level, message,data);
        }
        public void queryMessageStatus(string messageID,string from)
        {
            if (_status == SMPPClientStatus.Connected)
            {
                t.sendPDU(new QUERY_SM(messageID,from));
            }
            else
                throw new InvalidOperationException("SMPP Client is not connected");
        }
    }
}
