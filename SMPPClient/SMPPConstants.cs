﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSMPPClient
{
    static class SMPPCommands
    {
        public const uint generic_nack = 0x80000000;
        public const uint bind_receiver = 0x00000001;
        public const uint bind_receiver_resp = 0x80000001;
        public const uint bind_transmitter = 0x00000002;
        public const uint bind_transmitter_resp = 0x80000002;
        public const uint query_sm = 0x00000003;
        public const uint query_sm_resp = 0x80000003;
        public const uint submit_sm = 0x00000004;
        public const uint submit_sm_resp = 0x80000004;
        public const uint deliver_sm = 0x00000005;
        public const uint deliver_sm_resp = 0x80000005;
        public const uint unbind = 0x00000006;
        public const uint unbind_resp = 0x80000006;
        public const uint replace_sm = 0x00000007;
        public const uint replace_sm_resp = 0x80000007;
        public const uint cancel_sm = 0x00000008;
        public const uint cancel_sm_resp = 0x80000008;
        public const uint bind_transceiver = 0x00000009;
        public const uint bind_transceiver_resp = 0x80000009;
        public const uint outbind = 0x0000000B;
        public const uint enquire_link = 0x00000015;
        public const uint enquire_link_resp = 0x80000015;
    }
    static class SMPPCommandStatus
    {
        static Dictionary<uint, string> msgs = new Dictionary<uint, string>();
        static SMPPCommandStatus()
        {
            msgs.Add(0x00000000, "Ok");
            msgs.Add(0x00000001, "Message Length is invalid");
            msgs.Add(0x00000002, "Command Length is invalid");
            msgs.Add(0x00000003, "Invalid Command ID");
            msgs.Add(0x00000004, "Incorrect BIND Status for given command");
            msgs.Add(0x00000005, "ESME Already in Bound State");
            msgs.Add(0x00000006, "Invalid Priority Flag");
            msgs.Add(0x00000007, "Invalid Registered Delivery Flag");
            msgs.Add(0x00000008, "System Error");
            msgs.Add(0x0000000A, "Invalid Source Address");
            msgs.Add(0x0000000B, "Invalid Dest Addr");
            msgs.Add(0x0000000C, "Message ID is invalid");
            msgs.Add(0x0000000D, "Bind Failed");
            msgs.Add(0x0000000E, "Invalid Password");
            msgs.Add(0x0000000F, "Invalid System ID");
            msgs.Add(0x00000011, "Cancel SM Failed");
            msgs.Add(0x00000013, "Replace SM Failed");
        }
        public static string getStatusMessage(uint status)
        {
            string ret;
            if (!msgs.TryGetValue(status, out ret))
                return "Uknown status:" + status;
            return ret;
        }
        public const uint ESME_ROK = 0x00000000;
        public const uint ESME_RINVMSGLEN = 0x00000001;
        public const uint ESME_RINVCMDLEN = 0x00000002;
        public const uint ESME_RINVCMDID = 0x00000003;
        public const uint ESME_RINVBNDSTS = 0x00000004;
        public const uint ESME_RALYBND = 0x00000005;
        public const uint ESME_RINVPRTFLG = 0x00000006;
        public const uint ESME_RINVREGDLVFLG = 0x00000007;
        public const uint ESME_RSYSERR = 0x00000008;
        public const uint ESME_RINVSRCADR = 0x0000000A;
        public const uint ESME_RINVDSTADR = 0x0000000B;
        public const uint ESME_RINVMSGID = 0x0000000C;
        public const uint ESME_RBINDFAIL = 0x0000000D;
        public const uint ESME_RINVPASWD = 0x0000000E;
        public const uint ESME_RINVSYSID = 0x0000000F;
        public const uint ESME_RCANCELFAIL = 0x00000011;
        public const uint ESME_RREPLACEFAIL = 0x00000013;
    }
    public static class OptionalParamterTag
    {
        public const ushort dest_addr_subunit = 0x0005;
        public const ushort dest_network_type = 0x0006;
        public const ushort dest_bearer_type = 0x0007;
        public const ushort dest_telematics_id = 0x0008;
        public const ushort source_addr_subunit = 0x000D;
        public const ushort source_network_type = 0x000E;
        public const ushort source_bearer_type = 0x000F;
        public const ushort source_telematics_id = 0x0010;
        public const ushort qos_time_to_live = 0x0017;
        public const ushort payload_type = 0x0019;
        public const ushort additional_status_info_text = 0x001D;
        public const ushort receipted_message_id = 0x001E;
        public const ushort ms_msg_wait_facilities = 0x0030;
        public const ushort privacy_indicator = 0x0201;
        public const ushort source_subaddress = 0x0202;
        public const ushort dest_subaddress = 0x0203;
        public const ushort user_message_reference = 0x0204;
        public const ushort user_response_code = 0x0205;
        public const ushort source_port = 0x020A;
        public const ushort destination_port = 0x020B;
        public const ushort sar_msg_ref_num = 0x020C;
        public const ushort language_indicator = 0x020D;
        public const ushort sar_total_segments = 0x020E;
        public const ushort sar_segment_seqnum = 0x020F;
        public const ushort SC_interface_version = 0x0210;
        public const ushort callback_num_pres_ind = 0x0302;
        public const ushort callback_num_atag = 0x0303;
        public const ushort number_of_messages = 0x0304;
        public const ushort callback_num = 0x0381;
        public const ushort dpf_result = 0x0420;
        public const ushort set_dpf = 0x0421;
        public const ushort ms_availability_status = 0x0422;
        public const ushort network_error_code = 0x0423;
        public const ushort message_payload = 0x0424;
        public const ushort delivery_failure_reason = 0x0425;
        public const ushort more_messages_to_send = 0x0426;
        public const ushort message_state = 0x0427;
        public const ushort ussd_service_op = 0x0501;
        public const ushort display_time = 0x1201;
        public const ushort sms_signal = 0x1203;
        public const ushort ms_validity = 0x1204;
        public const ushort alert_on_message_delivery = 0x130C;
        public const ushort its_reply_type = 0x1380;
        public const ushort its_session_info = 0x1383;
        static Dictionary<ushort,Type> parserType=new Dictionary<ushort,Type>();
        public static PDUFieldParser createParser(ushort tag,uint size)
        {
            Type t;
            if(!parserType.TryGetValue(tag,out t))
                return new Int16Parser();
            if (t == null)
                return null;
            if(t==typeof(OctetWithSize))
                return Activator.CreateInstance(t,new object[]{size}) as PDUFieldParser;
            return Activator.CreateInstance(t) as PDUFieldParser;
        }
        static OptionalParamterTag()
        {
            parserType.Add(dest_addr_subunit, typeof(Int8Parser));
            parserType.Add(dest_network_type, typeof(Int8Parser));
            parserType.Add(dest_bearer_type, typeof(Int8Parser));
            parserType.Add(dest_telematics_id, typeof(Int16Parser));
            parserType.Add(source_addr_subunit, typeof(Int8Parser));
            parserType.Add(source_network_type, typeof(Int8Parser));
            parserType.Add(source_bearer_type, typeof(Int8Parser));
            parserType.Add(source_telematics_id, typeof(Int8Parser));
            parserType.Add(qos_time_to_live, typeof(IntParser));
            parserType.Add(payload_type, typeof(Int8Parser));
            parserType.Add(additional_status_info_text, typeof(COctetParser));
            parserType.Add(receipted_message_id, typeof(COctetParser));
            parserType.Add(ms_msg_wait_facilities, typeof(Int8Parser));
            parserType.Add(privacy_indicator, typeof(Int8Parser));
            parserType.Add(source_subaddress, typeof(OctetWithSize));
            parserType.Add(dest_subaddress, typeof(OctetWithSize));
            parserType.Add(user_message_reference, typeof(Int16Parser));
            parserType.Add(user_response_code, typeof(Int8Parser));
            parserType.Add(source_port, typeof(Int16Parser));
            parserType.Add(destination_port, typeof(Int16Parser));
            parserType.Add(sar_msg_ref_num, typeof(Int16Parser));
            parserType.Add(language_indicator, typeof(Int8Parser));
            parserType.Add(sar_total_segments, typeof(Int8Parser));
            parserType.Add(sar_segment_seqnum, typeof(Int8Parser));
            parserType.Add(SC_interface_version, typeof(Int8Parser));
            parserType.Add(callback_num_pres_ind, typeof(Int8Parser));
            parserType.Add(callback_num_atag, typeof(OctetWithSize));
            parserType.Add(number_of_messages, typeof(Int8Parser));
            parserType.Add(callback_num, typeof(OctetWithSize));
            parserType.Add(dpf_result, typeof(Int8Parser));
            parserType.Add(set_dpf, typeof(Int8Parser));
            parserType.Add(ms_availability_status, typeof(Int8Parser));
            parserType.Add(network_error_code, typeof(OctetWithSize));
            parserType.Add(message_payload, typeof(OctetWithSize));
            parserType.Add(delivery_failure_reason, typeof(Int8Parser));
            parserType.Add(more_messages_to_send, typeof(Int8Parser));
            parserType.Add(message_state, typeof(Int8Parser));
            parserType.Add(ussd_service_op, typeof(OctetWithSize));
            parserType.Add(display_time, typeof(Int8Parser));
            parserType.Add(sms_signal, typeof(Int16Parser));
            parserType.Add(ms_validity, typeof(Int8Parser));
            parserType.Add(alert_on_message_delivery, null);
            parserType.Add(its_reply_type, typeof(Int8Parser));
            parserType.Add(its_session_info, typeof(OctetWithSize));

        }
    }
}
