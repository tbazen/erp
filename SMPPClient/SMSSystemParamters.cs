﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TSMPPClient
{
    public static class SMPPSystemParamters
    {
        public static int INQUIRE_LINK_SEND_INTERVAL = 15000;
        public static int INQUIRE_LINK_RECEIVE_GIVE_UP_INTERVAL = 100 * INQUIRE_LINK_SEND_INTERVAL;
        public static int THREAD_SHOTDOWN_POLL_INTERVAL = 5000;
        public static int SEND_QUEUE_POLL_INTERVAL = 500;
        public static int WAIT_AFTER_SEND_THREAD_EXCEPTION_MIN = 1000;
        public static int WAIT_AFTER_SEND_THREAD_EXCEPTION_MAX = 120000;
        public static int WAIT_AFTER_SEND_THREAD_EXCEPTION_INCREASE_PERCENT = 25;
        public static int RECEIVE_THREAD_CONNECTION_STATUS_POLL_INTERVAL = 1000;
        public static int RECEIVE_BUFFER_SIZE = 1024;

        public static int SMPP_CONNECTION_STATUS_POLL_INTERVAL = 5000;
        public static int SMPP_CONNECTION_STATUS_GIVEUP_POLL_COUNT= 10;
        public static int SMS_OUT_BOX_POLL_INTERVAL = 5000;
        public static int WAIT_BEFORE_ATTEMPTING_TO_CONNECT_SMPP=15000;
    }
}
