using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using INTAPS;
using TSMPPClient;

namespace INTAPS.SMS
{
    public interface ISMSReceiver
    {
        bool onSMSReceived(string phoneNo, string message);
    }
    public enum SMSInterfaceStatus
    {
        Unknown,
        Initializing,
        Ready,
        Sending,
        Error
    }
    public class SMSInterfaceStatusData
    {
        public SMSInterfaceStatus status;
        public string statusMessage;
        public DateTime statusTime;
    }
    public class SMSInterface : ISMPPPersistance
    {

        SMSInterfaceStatus _status = SMSInterfaceStatus.Unknown;
        DateTime _statsDate = DateTime.Now;
        string _statusMessage = null;
        List<ISMSReceiver> _receivers = new List<ISMSReceiver>();
        TSMPPClient.SMPPLogLevel logLevel;
        public void addReciever(ISMSReceiver r)
        {
            _receivers.Add(r);
        }
        public void removeReceiver(ISMSReceiver r)
        {
            _receivers.Remove(r);
        }
        public SMSInterfaceStatus Status
        {
            get
            {
                return _status;
            }
        }
        public SMSInterfaceStatusData StatusData
        {
            get
            {
                return new SMSInterfaceStatusData()
                {
                    status = _status,
                    statusMessage = _statusMessage,
                    statusTime = _statsDate
                };
            }
        }

        public string StatusMessage
        {
            get { return _statusMessage; }
        }
        public DateTime StatsDate
        {
            get { return _statsDate; }
        }

        INTAPS.RDBMS.SQLHelper helper = null;
        INTAPS.RDBMS.SQLHelper helper2 = null;
        INTAPS.RDBMS.SQLHelper helper3 = null;
        private void setStatus(SMSInterfaceStatus s, DateTime t, string m)
        {
            _status = s;
            _statsDate = t;
            _statusMessage = m;

        }
        //SmsClient client;
        String shortCode;
        String systemID;
        String password;
        String serverIP;
        int port;
        const int MAX_OUT_BOX_POLL_COUNT = 20;

        void smsLoop()
        {
            while (true)
            {
                try
                {

                    setStatus(SMSInterfaceStatus.Initializing, DateTime.Now, "Intializing");
                    logLevel = (TSMPPClient.SMPPLogLevel)Enum.Parse(typeof(TSMPPClient.SMPPLogLevel), System.Configuration.ConfigurationManager.AppSettings["sms_logLevel"]);

                    this.client_smppLog(SMPPLogLevel.StateChange, "SMS Interface Connecting to SQL Server", null);

                    string con = System.Configuration.ConfigurationManager.AppSettings["sms_con"];
                    string db = System.Configuration.ConfigurationManager.AppSettings["sms_db"];

                    helper = new INTAPS.RDBMS.SQLHelper(con);
                    helper.getOpenConnection();
                    helper.setReadDB(db);
                    if (helper2 == null)
                    {
                        helper2 = new INTAPS.RDBMS.SQLHelper(con);
                        helper2.getOpenConnection();
                        helper2.setReadDB(db);
                    }
                    if (helper3 == null)
                    {
                        helper3 = new INTAPS.RDBMS.SQLHelper(con);
                        helper3.getOpenConnection();
                        helper3.setReadDB(db);
                    }
                    this.client_smppLog(SMPPLogLevel.StateChange, "SMS Interface Connecting to SQL Server", null);

                    TSMPPClient.SMPPClient client;
                    client = connectClient(systemID, password, serverIP, port);

                    while (true)
                    {
                        int pollCount = 0;
                        while (client.Status != TSMPPClient.SMPPClientStatus.Connected)
                        {
                            if (pollCount == 0)
                            {
                                client_smppLog(SMPPLogLevel.Synchronization, "SQL Server SMS Client waiting for SMPPClient to connect", null);
                                setStatus(SMSInterfaceStatus.Initializing, DateTime.Now, "Intializing");
                            }
                            System.Threading.Thread.Sleep(SMPPSystemParamters.SMPP_CONNECTION_STATUS_POLL_INTERVAL);
                            pollCount++;
                            if (pollCount >= SMPPSystemParamters.SMPP_CONNECTION_STATUS_GIVEUP_POLL_COUNT)
                            {
                                client_smppLog(SMPPLogLevel.Synchronization, "SQL Server SMS Client gave up waiting connection and creating new SMPPClient instance", null);
                                pollCount = 0;
                                disconnectClient(client);
                                System.Threading.Thread.Sleep(SMPPSystemParamters.WAIT_BEFORE_ATTEMPTING_TO_CONNECT_SMPP);
                                client = connectClient(systemID, password, serverIP, port);
                            }


                            //check status of sent sms that were sent before the SMPP was disconnected
                            /*lock (helper)
                            {
                                DataTable sentData;
                                string sql = string.Format("SELECT messageID FROM [SMSOut] where status=2");
                                sentData = helper.GetDataTable(sql);
                                foreach (DataRow row in sentData.Rows)
                                {
                                    string messageID = row[0] as string;
                                    if (!string.IsNullOrEmpty(messageID))
                                        client.queryMessageStatus(messageID,shortCode);
                                }
                            }*/
                        }
                        if (pollCount > 0)
                            client_smppLog(SMPPLogLevel.Synchronization, "SQL Server SMS Client detected SMPPClient to connect", null);

                        DataTable data;

                        //check if there are less than 100 sms being sent
                        {
                            int nSending;
                            string sql;
                            //if there are 100 or more SMS in the sending status we wait until it is below 100 
                            //this is to avoid send too many sms
                            int out_box_pool_count = 0;
                            do
                            {
                                sql = string.Format("SELECT count(*) FROM [SMSOut] where status=1");
                                lock (helper)
                                {
                                    nSending = (int)helper.ExecuteScalar(sql);
                                }
                                if (nSending >= 100)
                                {
                                    out_box_pool_count++;
                                    if (out_box_pool_count >= MAX_OUT_BOX_POLL_COUNT)
                                    {
                                        //if the sms in sending status are stuck at sending status for a long time
                                        //revers stat to 'ready to send'
                                        lock (helper)
                                        {
                                            helper.ExecuteNonQuery("Update SMSOut set status=0 where status=1");
                                            out_box_pool_count = 0;
                                        }
                                    }
                                    System.Threading.Thread.Sleep(SMPPSystemParamters.SMS_OUT_BOX_POLL_INTERVAL);
                                }
                            }
                            while (nSending >= 100);

                            sql = string.Format("SELECT TOP 150 [phoneNo],[message],id FROM [SMSOut] where status=0 or (status=1 and statusTicks<{0})", DateTime.Now.Subtract(new TimeSpan(4, 0, 30)).Ticks);
                            lock (helper)
                            {
                                data = helper.GetDataTable(sql);
                            }
                        }
                        if (data.Rows.Count == 0)
                        {
                            System.Threading.Thread.Sleep(SMPPSystemParamters.SMS_OUT_BOX_POLL_INTERVAL);
                            continue;
                        }
                        setStatus(SMSInterfaceStatus.Sending, DateTime.Now, "Sending sms");
                        this.client_smppLog(SMPPLogLevel.SMSSent, string.Format("{0} messages found", data.Rows.Count), data);
                        int sent = 0;
                        foreach (DataRow row in data.Rows)
                        {
                            string phoneNo = (string)row[0];
                            string sms = (string)row[1];
                            int id = (int)row[2];
                            int seqNo = (int)client.sendSMS(shortCode, phoneNo, sms);
                            lock (helper)
                            {
                                helper.Update(helper.ReadDB, "SMSOut", new string[] { "status", "statusTicks", "statusMessage", "seqNo" }, new object[] { 1, DateTime.Now.Ticks, "Sending", seqNo }, "id=" + id);
                            }
                            sent++;
                            setStatus(SMSInterfaceStatus.Sending, DateTime.Now, string.Format("Proccessed {0}", sent));
                        }
                        setStatus(SMSInterfaceStatus.Ready, DateTime.Now, "Ready");
                    }
                }
                catch (Exception ex)
                {
                    StringBuilder msg = null;
                    while (ex != null)
                    {
                        if (msg == null)
                            msg = new StringBuilder();
                        else
                            msg.Append('\n');
                        msg.Append(ex.Message + "\n" + ex.StackTrace);
                        ex = ex.InnerException;
                    }
                    this.client_smppLog(SMPPLogLevel.Exception, msg.ToString(), msg);
                    setStatus(SMSInterfaceStatus.Error, DateTime.Now, msg.ToString());
                    this.client_smppLog(SMPPLogLevel.Synchronization, "Seelping 5 second befor trying again..", null);
                    System.Threading.Thread.Sleep(SMPPSystemParamters.WAIT_BEFORE_ATTEMPTING_TO_CONNECT_SMPP);
                }
                finally
                {
                    if (helper != null)
                        helper.CloseConnection();
                }
            }
        }

        private TSMPPClient.SMPPClient connectClient(String systemID, String password, String serverIP, int port)
        {

            lock (helper)
            {
                object seqNo = helper.ExecuteScalar(string.Format("Select max(seqNo) from {0}.dbo.SMSOut", helper.ReadDB));
                TSMPPClient.SMPPClient client;
                client = new TSMPPClient.SMPPClient(serverIP, port, systemID, password, seqNo is int ? (uint)(int)seqNo : 0);
                client.smsReceived += client_OnNewSms;
                client.smsStatusChanged += client_smsStatusChanged;
                client.clientStatusChanged += client_clientStatusChanged;
                client.smppLog += client_smppLog;
                client.setPersistance(this);
                client.start();
                return client;
            }
        }
        void disconnectClient(TSMPPClient.SMPPClient client)
        {
            client.stop(false);
            client.smsReceived -= client_OnNewSms;
            client.smsStatusChanged -= client_smsStatusChanged;
            client.clientStatusChanged -= client_clientStatusChanged;
            client.smppLog -= client_smppLog;
        }
        void client_smppLog(SMPPLogLevel level, string message, object data)
        {
            if ((level & SMPPLogLevel.PDUReceived) == SMPPLogLevel.PDUReceived
                || (level & SMPPLogLevel.PDUSent) == SMPPLogLevel.PDUSent)
            {
                try
                {
                    lock (helper3)
                    {
                        bool sendPDU = (level & SMPPLogLevel.PDUSent) == SMPPLogLevel.PDUSent; //send is true
                        PDUOut pduOut = data as PDUOut;
                        PDUIn pduIn = data as PDUIn;
                        uint comandID, comandStatus, seqNo;
                        string sourceAdr = null, destAdr = null;
                        if (sendPDU)
                        {
                            pduOut.getCommonPars(out comandID, out comandStatus, out seqNo);
                            if (pduOut is TSMPPClient.Commands.SUBMIT_SM)
                            {
                                TSMPPClient.Commands.SUBMIT_SM d = (TSMPPClient.Commands.SUBMIT_SM)pduOut;
                                sourceAdr = d.from;
                                destAdr = d.to;
                            }
                        }
                        else
                        {
                            PDUCommandParser cmd = pduIn.command;
                            comandID = cmd.commandID;
                            comandStatus = cmd.commandStatus;
                            seqNo = cmd.seqNo;
                            if (cmd is TSMPPClient.CommandParsers.DELIVER_SMParser)
                            {
                                TSMPPClient.CommandParsers.DELIVER_SMParser d = (TSMPPClient.CommandParsers.DELIVER_SMParser)cmd;
                                sourceAdr = d.from;
                                destAdr = d.to;
                            }
                            data = cmd;
                        }
                        helper3.Insert(helper3.ReadDB, "SMSPDU",
                            new string[] { "ticks", "direction", "command_id", "command_status", "seq_no", "pdu", "source_adr", "dest_adr" }
                        , new object[] {DateTime.Now.Ticks,sendPDU,(long)comandID,(long)comandStatus,(long)seqNo,
                            INTAPS.RDBMS.SQLHelper.ObjectToBinary(data),sourceAdr,destAdr});
                    }
                }
                catch (Exception ex)
                {
                    client_smppLog(SMPPLogLevel.Exception, ex.Message, ex);
                }
            }

            if ((level & logLevel) == level)
            {
                Console.WriteLine("SMPP - {0}: {1}", DateTime.Now.ToString("MM:dd hh:mm:ss"), message);
            }
        }

        void client_clientStatusChanged(TSMPPClient.SMPPClientStatus status)
        {
            switch (status)
            {
                case TSMPPClient.SMPPClientStatus.Connecting:
                    setStatus(SMSInterfaceStatus.Initializing, DateTime.Now, "Intializing");
                    break;
                case TSMPPClient.SMPPClientStatus.Binding:
                    setStatus(SMSInterfaceStatus.Initializing, DateTime.Now, "Intializing");
                    break;
                case TSMPPClient.SMPPClientStatus.Connected:
                    setStatus(SMSInterfaceStatus.Ready, DateTime.Now, "Ready");
                    break;
                case TSMPPClient.SMPPClientStatus.Disconnecting:
                    setStatus(SMSInterfaceStatus.Initializing, DateTime.Now, status.ToString());
                    break;
                case TSMPPClient.SMPPClientStatus.Disconnected:
                    setStatus(SMSInterfaceStatus.Initializing, DateTime.Now, status.ToString());
                    break;
                default:
                    break;
            }
        }

        void client_smsStatusChanged(uint seqNo, TSMPPClient.SMSStatus status, string statusMessage)
        {
            lock (helper)
            {
                int s = 0;
                switch (status)
                {
                    case TSMPPClient.SMSStatus.Sending:
                        s = 1;
                        break;
                    case TSMPPClient.SMSStatus.Sent:
                        s = 2;
                        break;
                    case TSMPPClient.SMSStatus.SendingFailed:
                        s = 3;
                        break;
                    case TSMPPClient.SMSStatus.Delivered:
                        s = 4;
                        break;
                    case TSMPPClient.SMSStatus.DeliveryFailed:
                        s = 5;
                        break;
                }
                helper.Update(helper.ReadDB, "SMSOut", new string[] { "status", "statusTicks", "statusMessage" }, new object[] { s, DateTime.Now.Ticks, statusMessage }, "seqNo=" + seqNo);
            }
        }

        void client_OnNewSms(TSMPPClient.SMS sms)
        {
            lock (helper)
            {
                client_smppLog(SMPPLogLevel.SMSReceived, string.Format("Message Received:\nFrom {0}\n{1}", sms.from, sms.message), sms);
                helper.Insert(helper.ReadDB, "SMSIn", new string[] { "phoneNo", "message", "receivedTicks", "error" }, new object[] {
            sms.from,sms.message,DateTime.Now.Ticks,false
            });
                try
                {
                    foreach (ISMSReceiver r in _receivers)
                        if (r.onSMSReceived(sms.from, sms.message))
                            break;
                }
                catch(Exception ex)
                {
                    this.client_smppLog(SMPPLogLevel.Exception, ex.Message, ex);
                    this.logSMS(-1, sms.from, "The server encountered a problem. Please try later.");
                }
            }
        }
        public void start()
        {

            shortCode = System.Configuration.ConfigurationManager.AppSettings["sms_shortCode"];
            systemID = System.Configuration.ConfigurationManager.AppSettings["sms_systemID"];
            password = System.Configuration.ConfigurationManager.AppSettings["sms_password"];
            serverIP = System.Configuration.ConfigurationManager.AppSettings["sms_serverIP"];
            string strPort = System.Configuration.ConfigurationManager.AppSettings["sms_serverPort"];

            if (string.IsNullOrEmpty(shortCode)
                || string.IsNullOrEmpty(systemID)
                || string.IsNullOrEmpty(password)
                || string.IsNullOrEmpty(strPort)
                || !int.TryParse(strPort, out port))
                throw new INTAPS.ClientServer.ServerUserMessage("SMS Configurations values not set");

            new System.Threading.Thread
            (
                new System.Threading.ThreadStart(smsLoop)
            ).Start();
        }


        public int logSMS(int AID, string phoneNo, string msg)
        {
            lock (helper2)
            {

                helper2.Insert(helper.ReadDB, "SMSOut", new string[] { "phoneNo", "message", "logTicks", "__AID" }, new object[] {
                    phoneNo,msg,DateTime.Now.Ticks,AID
                });
                return (int)helper2.ExecuteScalar(string.Format("Select max(id) from {0}.dbo.SMSOut", helper.ReadDB));
            }
        }

        public int getStatusBySMSID(int smsID)
        {
            lock (helper)
            {
                object _ret = helper.ExecuteScalar(string.Format("Select status from {0}.dbo.SMSOut where id={1}", helper.ReadDB, smsID));
                if (_ret is int)
                    return (int)_ret;
                return -1;
            }
        }

        public void setMessageID(string messageID, uint seqNo)
        {
            lock (helper)
            {
                helper.Update(helper.ReadDB, "SMSOut", new string[] { "messageID" }, new object[] { messageID }, "seqNo=" + seqNo);
            }
        }

        public uint getSeqNoForMessageID(string messageID)
        {
            lock (helper)
            {
                object ret;
                ret = helper.ExecuteScalar(String.Format("Select seqNo from {0}.dbo.SMSOut where messageID='{1}'", helper.ReadDB, messageID));
                if (ret is int)
                    return (uint)(int)ret;
                return 0;
            }
        }

        public string getMessageIDforSeqNo(uint seqNo)
        {
            throw new NotImplementedException();
        }
    }
}
