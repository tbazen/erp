export enum SecurityObjectClass {
    soInherit = 1,
    soNoInherit = 2
}