import { SecurityObjectClass} from './SecurityObjectClass';

export interface SecurityObjectData
{
    Class: SecurityObjectClass;
    FullName: string;
    ID: number;
    Name: string;
    Owner: number;
    OwnerName: string;
    PID: number;

}