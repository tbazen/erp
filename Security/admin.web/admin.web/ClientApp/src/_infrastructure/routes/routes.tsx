import React from 'react';

import { User } from 'src/modules/users/users';
import { Objects } from 'src/modules/objects/objects';
import { Home } from 'src/modules/home/home';
import { ActiveSession } from 'src/modules/session/active_sessions';
import { AuditViewer } from 'src/modules/audit/audit_viewer';



// const User : React.Component<any,any> = React.lazy(() => import('../../modules/users/users'));
// const Objects = React.lazy(() => import('../../modules/objects/objects'));
// const Home = React.lazy(() => import('../../modules/home/home'));


const routes = [
    { path : '/', exact : true, name : "Home", component : Home },
    { path : '/users', exact : true, name : "Users", component : User },
    { path : '/objects', exact : true, name : "Objects", component : Objects },
    { path : '/sessions', exact : true, name : "Sessions", component : ActiveSession },
    { path : '/audit', exact : true, name : "Audit Viewer", component : AuditViewer }
]

export {routes};