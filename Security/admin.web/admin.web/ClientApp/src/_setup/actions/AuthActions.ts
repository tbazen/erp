import { UserSession } from "src/_infrastructure/model/UserSession";

export const enum AuthActions {
  LoginType = "LOGIN",
  LogoutType = "LOGOUT"
}

export const AuthActionCreators = {
  login : (session : UserSession) => ({ type : AuthActions.LoginType , session : session}),
  logout : () => ({ type : AuthActions.LogoutType })
}