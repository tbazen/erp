import { combineReducers } from 'redux'
import authReducer from './authReducer';
import loadReducer from './loadReducer';

import { connectRouter } from 'connected-react-router'
import { AuthState } from 'src/_infrastructure/state/authState';
import LoadState from 'src/_infrastructure/state/LoadState';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
     auth : authReducer,
    loader : loadReducer,
    // routing : routerReducer,
})

export default rootReducer;

// export interface GlobalState {
//   router : History,
//   auth : AuthState,
//   loader : LoadState

// }

// const rootReducer = combineReducers({
//    auth : authReducer,
//    loader : loadReducer

// })

// export default rootReducer
