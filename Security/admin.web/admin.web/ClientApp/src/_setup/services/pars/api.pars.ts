import { SecurityObjectClass } from "src/_infrastructure/model/SecurityObjectClass";

export interface ChangeClassPar
{
    SessionID: string; 
    ObjectID: number; 
    Class: SecurityObjectClass;
}

export interface ChangeOwnerPar
{
    SessionID: string; 
    ObjectID: number; 
    NewOwner: number;
}

export interface ChangePasswordPar
{
    SessionID: string; newpassword: string;
}

export interface ChangePassword2Par
{
    SessionID: string; UserName: string; newpassword: string;
}

export interface CreateObjectPar
{
    SessionID: string; ParentOID: number; Name: string; OwnerID : number;
}

export interface CreateUserPar
{
    SessionID: string; ParentUser: string; UserName: string; Password: string;
}

export interface DeleteObjectPar
{
    SessionID: string; ObjectID: number;
}

export interface DeleteUserPar
{
    SessionID: string; UserName: string;
}

export interface GetAllAuditOperationsPar
{
    SessionID: string;
}

export interface GetAllUsersPar
{
    SessionID: string;
}

export interface GetAuditPar
{
    SessionID: string; AuditType: string[]; Users: string[]; Date: boolean; from: Date | string; DateTo: boolean; to: Date | string; Data1: string[]; index: number; PageSize: number;
}

export interface GetAuditData2Par
{
    SessionID: string; AuditID: number;
}

export interface GetChildListPar
{
    SessionID: string; ObjectID: number;
}

export interface GetChildUsersPar
{
    SessionID: string; User: string;
}

export interface GetDeniedEntriesPar
{
    SessionID: string; ObjectID: number;
}

export interface GetDeniedUsersPar
{
    SessionID: string; ObjectID: number;
}

export interface GetNameForIDPar
{
    SessionID: string; UserID: number;
}

export interface GetObjectPar
{
    SessionID: string; ObjectID: number;
}

export interface GetObjectFullNamePar
{
    SessionID: string; OID: number;
}

export interface GetObjectIDPar
{
    SessionID: string; Name: string;
}

export interface GetParentUserPar
{
    SessionID: string; UserName: string;
}

export interface GetPermitedUsersPar
{
    SessionID: string; ObjectID: string;
}

export interface GetPermittedEntriesPar
{
    SessionID: string; ObjectID: string;
}

export interface GetUIDForNamePar
{
    SessionID: string; Name: string;
}

export interface GetUserPermissionsPar
{
    SessionID: string; UserID: string;
}

export interface GetUserPermissions2Par
{
    SessionID: string; userName: string; password: string;
}

export interface RenamePar
{
    SessionID: string; ObjectID: number; NewName: string; Owner :number;
}

export interface SetPermissionPar
{
    SessionID: string; ObjectID: number; UserName: string; state: PermissionState;
}
export interface UserPar
{
    SessionID: string; user: string;
}

export interface IsPermitedPar
{
    SessionID: string; obj: string;
}

export enum PermissionState {
    Permit = 0,
    Deny = 1,
    Undefined = 2
}

export interface LoginPar
{
    UserName: string; Password: string; 
}

export interface SetPermission2Par
{
    SessionID: string;
    UserId: number;
    Permissions: number[];
    Denied: number[];
}