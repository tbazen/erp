
import { baseUrl } from './urlConfig';
import Axios, { AxiosAdapter, AxiosInstance } from 'axios';
import { createBrowserHistory } from 'history';
import { configureStore } from '../store/configureStore';
import Swal from 'sweetalert2';
import { AuthActions } from '../actions/AuthActions';
import { ChangeClassPar, ChangeOwnerPar, ChangePasswordPar, ChangePassword2Par, CreateObjectPar, CreateUserPar, DeleteObjectPar, GetAllAuditOperationsPar, DeleteUserPar, GetAllUsersPar, GetAuditPar, GetAuditData2Par, GetChildListPar, GetChildUsersPar, GetDeniedEntriesPar, GetDeniedUsersPar, GetNameForIDPar, GetObjectPar, GetObjectFullNamePar, GetParentUserPar, GetPermittedEntriesPar, GetUIDForNamePar, GetUserPermissionsPar, GetUserPermissions2Par, RenamePar, SetPermissionPar, UserPar, IsPermitedPar, GetPermitedUsersPar, LoginPar, SetPermission2Par } from './pars/api.pars';
import { withRouter } from 'react-router';
const base = document.getElementsByTagName('base')[0].baseURI;
const history = createBrowserHistory();

// Get the application-wide store instance, prepopulating with state from the server where available.
const initialState = window.initialReduxState;
const {store,persistor}  = configureStore(history)
import  { Redirect } from 'react-router-dom' 

export default class SecurityService {

    private axios : AxiosInstance;
    private Props : any;
    constructor(props  : any){
        this.Props = props;
        this.axios = Axios.create({
            baseURL : baseUrl + "/app/Security2",
            withCredentials : true,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        })

        this.axios.interceptors.response.use((response) => {
            console.log(response);
            return response.data;}, (error) => {
             console.log(error);
             if(error.message == "Network Error"){
                Swal.fire("Error","Server Is Down !!!","error");
                this.Props.auth.logout();
                console.log(this.Props);
                this.Props.history.push("/login");
            }

            else if(error.response || (error.response && error.response.data == "Access Denied")){
                console.log(error.response.status);
                
                if(error.response.status == 403){
                    Swal.fire("Error",error.response.data,"error");
                    this.Props.auth.logout();
                    console.log(this.Props);
                    this.Props.history.push("/login");
                }
                else{
                    Swal.fire("Error",error.response.data,"error");
                }
            }
        })
    }


    ChangeClass(data : ChangeClassPar){
        var path = "/ChangeClass";
        return this.axios.post(path,data);
    }

    ChangeOwner(data : ChangeOwnerPar){
        var path = "/ChangeOwner";
        return this.axios.post(path,data);
    }

    ChangePassword(data : ChangePasswordPar){
        var path = "/ChangePassword";
        return this.axios.post(path,data);
    }

    ChangePassword2(data: ChangePassword2Par){
        var path = "/ChangePassword2";
        return this.axios.post(path,data);
    }

    CreateObject(data: CreateObjectPar){
        var path = "/CreateObject";
        return this.axios.post(path,data);
    }
    
    CreateUser(data: CreateUserPar){
        var path = "/CreateUser";
        return this.axios.post(path,data);
    }
   
    DeleteObject(data: DeleteObjectPar){
        var path = "/DeleteObject";
        return this.axios.post(path,data);
    }

    DeleteUser(data: DeleteUserPar){
        var path = "/DeleteUser";
        return this.axios.post(path,data);
    }

    GetAllAuditOperations(data: GetAllAuditOperationsPar){
        var path = "/GetAllAuditOperations";
        return this.axios.post(path,data);
    }

    GetAllUsers(data: GetAllUsersPar){
        var path = "/GetAllUsers";
        return this.axios.post(path,data);
    }

    GetAllUsers2(data : GetAllUsersPar){
        var path = "/GetAllUsers2";
        return this.axios.post(path,data);
    }

    GetAudit(data: any){
        var path = "/GetAudit";
        return this.axios.post(path,data);
    }

    GetAuditData2(data: GetAuditData2Par){
        var path = "/GetAuditData2";
        return this.axios.post(path,data);
    }

    GetChildList(data: GetChildListPar){
        var path = "/GetChildList";
        return this.axios.post(path,data);
    } 

    GetChildUsers(data: GetChildUsersPar){
        var path = "/GetChildUsers";
        return this.axios.post(path,data);
    }

    GetDeniedEntries(data: GetDeniedEntriesPar){
        var path = "/GetDeniedEntries";
        return this.axios.post(path,data);
    }

    GetDeniedUsers(data: GetDeniedUsersPar){
        var path = "/GetDeniedUsers";
        return this.axios.post(path,data);
    }

    GetNameForID(data: GetNameForIDPar){
        var path = "/GetNameForID";
        return this.axios.post(path,data);
    }

    GetObject(data: GetObjectPar){
        var path = "/GetObject";
        return this.axios.post(path,data);
    }

    GetAllOjects(data : GetObjectPar){
        var path = "/GetAllObjects";
        return this.axios.post(path,data);
    }

    GetObjectFullName(data: GetObjectFullNamePar){
        var path = "/GetObjectFullName";
        return this.axios.post(path,data);
    }

    GetParentUser(data: GetParentUserPar){
        var path = "/GetParentUser";
        return this.axios.post(path,data);
    }

    GetPermitedUsers(data: GetPermitedUsersPar){
        var path = "/GetPermitedUsers";
        return this.axios.post(path,data);
    }

    GetPermittedEntries(data: GetPermittedEntriesPar){
        var path = "/GetPermittedEntries";
        return this.axios.post(path,data);
    }

    GetUIDForName(data: GetUIDForNamePar){
        var path = "/GetUIDForName";
        return this.axios.post(path,data);
    }

    getUserPermissions(data: GetUserPermissionsPar){
        var path = "/getUserPermissions";
        return this.axios.post(path,data);
    }

    GetUserPermissions2(data: GetUserPermissions2Par){
        var path = "/GetUserPermissions2";
        return this.axios.post(path,data);
    }

    Rename(data: RenamePar){
        var path = "/Rename";
        return this.axios.post(path,data);
    }

    SetPermission(data: SetPermissionPar){
        var path = "/SetPermission";
        return this.axios.post(path,data);
    }

    User(data: UserPar){
        var path = "/User2";
        return this.axios.post(path,data);
    }

    IsPermited(data: IsPermitedPar){
        var path = "/IsPermited";
        return this.axios.post(path,data);
    }

    Login(data: LoginPar){
        var path = "/Login";
        return this.axios.post(path,data);
    }

    Logout(sessionID : string){
        var path = "/Logout";
        return this.axios.post(path,sessionID);
    }

    SetPermission2(data: SetPermission2Par){
        var path = "/SetPermission2";
        return this.axios.post<Boolean>(path,data);
    }
    
}
