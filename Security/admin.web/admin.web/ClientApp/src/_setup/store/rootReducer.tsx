import { combineReducers } from 'redux';
import authReducer from '../reducer/authReducer';
import loadReducer from '../reducer/loadReducer';
import { routerReducer } from 'react-router-redux';

  export const rootReducer = combineReducers({

       auth : authReducer,
      loader : loadReducer,
      route : routerReducer
  })
