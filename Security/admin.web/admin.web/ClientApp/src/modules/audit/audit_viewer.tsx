import { Table, Card, Row, Col, Form, Checkbox, DatePicker, Select, Button } from 'antd';
import React from 'react';
import { PaginationConfig, ColumnProps } from 'antd/lib/table';
import SecurityService from 'src/_setup/services/security.service';
import { GetAuditPar } from 'src/_setup/services/pars/api.pars';
import './audit_viewer.scss';

const Option = Select.Option;
const pageSize = 100;

const columns : any = [
    {
        title : "ID",
        dataIndex : "id",
        key : "id"
    },
    {
        title : "Date",
        dataIndex : "dateTime",
   //     render :  d => d.dateTIme,
    },{
    title: 'User',
    dataIndex: 'user',
  },
  {
    title : "User ID",
    dataIndex : "user ID"
  },
  {
      title : "Operation",
      dataIndex : "operation"
  },{
    title : "Remark",
    dataIndex : "remark"
  }]

  interface state {
      data : any,
      pagination : PaginationConfig,
      loading : boolean,
      DateFromChecked : boolean,
      DateToChecked : boolean,
      Users : any[]
  }

  const config : PaginationConfig = {
    pageSize : pageSize
  }

export class AuditViewer extends React.Component<any,any> {

    private SecurityService : SecurityService;
    private SessionID : string;

    constructor(props : any){
        super(props);
        this.state = {
            data: { },
            pagination: config,
            loading: false,

            DateFromChecked : false,
            DateToChecked : false,
            Users : [],
            Operations : [],
            DateFrom : new Date(),
            DateTo : new Date(),
            CheckedOperations : [],
            CheckedUsers : [],


          };
        this.SecurityService = new SecurityService(this.props);
        this.SessionID = this.props.session.payrollSessionID;
        this.GetData = this.GetData.bind(this);

        this.setUserState = this.setUserState.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.search = this.search.bind(this);
    }
  
    componentDidMount(){
       // this.GetData(0);

       this.setUserState();
    }

    
    setUserState(){
        var self = this;
        this.SecurityService.GetAllUsers2({SessionID : this.props.session.payrollSessionID}).then(function(response){
            if(response){
                self.setState({
                    Users : response
                })
            }
            
        })

        this.SecurityService.GetAllAuditOperations({SessionID : this.props.session.payrollSessionID}).then(function(response){
            if(response){
                self.setState({
                    Operations : response
                })
            }
        })
    }

    GetData(index : number){
        var self = this;
        var data  = {
            AuditType : this.state.CheckedOperations,
            Data1 : [],
            Date : this.state.DateFromChecked,
            from :  this.state.DateFrom,
            DateTo : this.state.DateToChecked,
            PageSize : pageSize,
            index : index,
            SessionID : this.SessionID,
            Users : this.state.CheckedUsers,
            to  : this.state.DateTo

        }
        console.log(data);
        this.setState({ loading: true });
        this.SecurityService.GetAudit(data).then(function(response){
            console.log(response);
            self.setState({
                data : response
            })
        }).then(function(){
            const pagination = {...self.state.pagination};
            pagination.total = self.state.data.nRecords;
            self.setState({
                loading: false,
                pagination,
              });
        })
        
    }

    handleSubmit = (e : React.FormEvent) => {
        var self = this;
        e.preventDefault();
        console.log(this.state);
        this.props.form.validateFieldsAndScroll((err : any, values: any) => {
            if (!err) {
                console.log(values);
               self.setState({
                   DateFrom : self.state.DateFromChecked ? values.dateFrom : new Date(),
                   DateTo : self.state.DateToChecked ? values.dateTo : new Date(),
                   CheckedOperations : values.operations != undefined ? values.operations : [],
                   CheckedUsers : values.users  != undefined ? values.users : [],
               },this.search)
          }
        })
    

      }

      search(){
          this.GetData(0);
      }

  handleTableChange = (pagination : PaginationConfig) => {
    const pager = { ...this.state.pagination };    
    pager.current = pagination.current;
    this.setState({
      pagination: pager,
    });
    this.GetData(Number(pagination.current));

  }

  DateChecked(key : number){
    if(key == 1){
        this.setState({
            DateFromChecked : !this.state.DateFromChecked
        });
        if(this.state.DateToChecked == true){
            this.setState({
                DateToChecked : false
            });
        }
        
    }
    if(key == 2){
        this.setState({
            DateToChecked : !this.state.DateToChecked
        });
    }
}

  render() {

    const { getFieldDecorator } = this.props.form;
  
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    const style = { padding : '0 !important', margin : '0 !important', paddingBotton : '0px', marginBottom : '0px'};


    return (
        <Row>
            <Col span={8}>

            <Card
        title="Audit Viewer"
        style={{minHeight : 400}}
        >
             <Form
              onSubmit={this.handleSubmit}
             >
             <Form.Item
                        labelCol={{span : 16}}
                        style ={style}
                        extra = {
                            <div>
                               <Checkbox onChange={() => this.DateChecked(1)} defaultChecked={false} >Date From</Checkbox>
                            {'   '}
                            <Checkbox 
                            // disabled={!this.state.DateFromChecked}
                            checked = {this.state.DateFromChecked && this.state.DateToChecked}
                            onChange={() => this.DateChecked(2)} defaultChecked={false}>Date To</Checkbox> 
                            </div>
                            
                        }
                        >


                        </Form.Item>
                        {
                            this.state.DateFromChecked ?<Form.Item 
                            {...formItemLayout}
                            style = {style}
                            label = "Date From"
                            >
                            {
                                getFieldDecorator("dateFrom",{
                                    rules: [{
                                        required: this.state.DateFromChecked,
                                        message: 'Please select start date',
                                      }],
                                })(
                                    <DatePicker />
                                )
                            }
    
                            </Form.Item> : null
                        }
                        {
                            this.state.DateToChecked ?
                            <Form.Item 
                            {...formItemLayout}
                            style = {style}
                            label = "Date To"
                            >
                            {
                                getFieldDecorator("dateTo",{
                                    rules: [{
                                        required: this.state.DateToChecked,
                                        message: 'Please select end date',
                                      }],
                                })(
                                    <DatePicker />
                                )
                            }
    
                            </Form.Item> : null
                        }
                        <Form.Item
                        label="Users"
                  //      {...formItemLayout}
                        >
    {
        getFieldDecorator("users",{

        })(
            <Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="Select Users" 
           
            //onChange={handleChange}
          >
            {
                this.state.Users.map(function(item : any){
                    return <Option
                    value={item.userName} key={item.userName}
                    >{item.userName}</Option>
                })
            }
          </Select>,
        )
    }
                        </Form.Item>

                        <Form.Item
                        label="Operations"
                        //{...formItemLayout}
                        >
    {
        getFieldDecorator("operations",{

        })(
            <Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="Select Operations"
            dropdownClassName="searchDropDown"
             //onChange={handleChange}
          >
            {
                this.state.Operations.map(function(item : string){
                    return <Option
                    value={item} key={item}
                    >{item}</Option>
                })
            }
          </Select>,
        )
    }
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit"
            onClick={() => console.log(this)}
            >Search</Button>
          </Form.Item>
            </Form> 
            
             
      </Card>
            </Col>
            <Col span={16}>  
            <Card
            title="Search Result"
            >
                {
                    <h4>{
                        this.state.data.nRecords != null ?
                        `${this.state.data.nRecords} records found` : ""
                    }</h4>
                }
            <Table
        columns={columns}
        rowKey={"id"}
        dataSource={this.state.data._ret}
        pagination={this.state.pagination}
        loading={this.state.loading}
        onChange={this.handleTableChange}
        scroll={ { y : 400}}
      />
      </Card> 
            </Col>
        </Row>

    );
  }
}
