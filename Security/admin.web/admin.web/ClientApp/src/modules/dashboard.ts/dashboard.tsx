import React, { Component, Suspense } from 'react'
import './dashboard.scss';
import {Button, Form, Spin} from 'antd';
import 'antd/dist/antd.css';
//import Login from '../modules/shared/login/login'
import {
  Layout, Menu, Breadcrumb, Icon,
} from 'antd';
import { Link, Switch, Route } from 'react-router-dom';
import { RootState } from 'src/_setup/store/MyTypes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AuthActionCreators } from 'src/_setup/actions/AuthActions';
import { LoadActionCreators } from 'src/_setup/actions/LoadActions';
import SecurityService from 'src/_setup/services/security.service';
import Container from 'reactstrap/lib/Container';
import { routes } from 'src/_infrastructure/routes/routes';
import { url } from 'inspector';

const {
  Header, Content, Footer, Sider,
} = Layout;
const SubMenu = Menu.SubMenu;

class Dash extends React.Component<any,any> {

  private SecurityService : SecurityService; 

    constructor(props : any){
      super(props);
      this.state = {
        collapsed: false,
        pathname : "/"
      };
      this.SecurityService = new SecurityService(this.props);
      this.Logout = this.Logout.bind(this);
    }

  
  componentWillMount(){
    console.log(this.props);
    if(!this.props.authenticated || this.props.session == undefined){
      this.props.history.push('/login');
      this.props.auth.logout();
    }
    else{
      this.setState({
        pathname : this.props.location.pathname
      });
    }
    console.log(this.props);
  }

  componentWillReceiveProps(nextProps :any){
    this.setState({
      pathname : nextProps.location.pathname
    });
  }

  Logout(){
    var self = this;
    self.SecurityService.Logout(self.props.session.payrollSessionID).then(function(response){
      console.log(response);
      self.props.auth.logout();
      self.props.history.push('/login');
    })
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
  
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.toggle}
      >
        <div className="logo">
        <img src="../intaps1.png" className="logo-image" />
        </div>
        <Menu theme="dark" mode="inline"
        defaultSelectedKeys={[this.state.pathname]}
        >
        
          <Menu.Item key="/">
          <Link to={"/"}>
          <Icon type="pie-chart" />
            <span>Dashboard</span>
        </Link>
            
          </Menu.Item>
          <Menu.Item key="/users">
          <Link to={"/users"}>
          <Icon type="user" />
            <span>Users</span>
        </Link>
            
          </Menu.Item>
          
          <Menu.Item key="/objects">
          <Link to={"/objects"}>
          <Icon type="global" />
            <span>Objects</span>
        </Link>
            
          </Menu.Item>
          <Menu.Item key="/sessions">
          <Link to={"/sessions"}>
          <Icon type="wifi" />
            <span>Sessions</span>
        </Link>
            
          </Menu.Item>
          <Menu.Item key="/audit">
          <Link to={"/audit"}>
          <Icon type="scan" />
            <span>Audit</span>
        </Link>
            
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="layout_container">
      <Header style={{ background: '#fff', padding: 0 }}>
           
                 <Menu
        theme="light"
        mode="horizontal"
        style={{ lineHeight: '64px' }}
      >
         <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
        <Menu.Item key="1" className="logout" onClick={this.Logout}>Logout</Menu.Item>
      </Menu>
          </Header>
        <Content style={{ margin: '50px 50px' }} >
        <Spin
                        spinning={false}
                        >
                       
                        <Container fluid>   
                                <Suspense fallback={this.loading}>
                                <Switch>
                                    {routes.map((route, idx) => {
                                    return <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                     
                                        render = {
                                            (props : any) => (<route.component {...this.props} {...props} />)
                                        }
                                    />
                                    })}
                                </Switch>
                            </Suspense>    
                        </Container>
                        </Spin>
        </Content>
        <Footer style={{ textAlign: 'center' }} className="footer_layout">
          Administrator ©2019 Created by INTAPS Consultancy PLC
        </Footer>
      </Layout>
    </Layout>
    );
  }
}

function mapStateToProps(state : RootState ){
  return {
      authenticated : state.auth.authenticated,
      session : state.auth.session,
      loading : state.loader.loading,
  }
}

function mapDispatchToProps(dispatch : any){
  return {
      auth : bindActionCreators(AuthActionCreators,dispatch),
      loader : bindActionCreators(LoadActionCreators, dispatch)
  }
}

const Dashboard = connect(mapStateToProps,mapDispatchToProps)(Form.create()(Dash));
export {Dashboard};