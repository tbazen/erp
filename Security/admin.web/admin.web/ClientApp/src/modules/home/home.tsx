import React from 'react';
import { ActiveSession } from '../session/active_sessions';

export class Home extends React.Component<any,any>{

    render(){
        return(
            <ActiveSession {...this.props} />
        )
    }
}