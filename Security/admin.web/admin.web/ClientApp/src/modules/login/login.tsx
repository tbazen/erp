import React, { Component, FormEvent } from 'react'
import './login.scss';
import {Button, Input, Checkbox, Card, Row, Col} from 'antd';
import 'antd/dist/antd.css';
//import Login from '../modules/shared/login/login'
import {
  Layout, Menu, Breadcrumb, Icon, Form
} from 'antd';
import { AuthState } from 'src/_infrastructure/state/authState';
import LoadState from 'src/_infrastructure/state/LoadState';
import { AuthActionCreators } from 'src/_setup/actions/AuthActions';
import { LoadActionCreators } from 'src/_setup/actions/LoadActions';
import { bindActionCreators } from 'redux';
import {connect } from 'react-redux';
import { RootState } from 'src/_setup/store/MyTypes';
import SecurityService from 'src/_setup/services/security.service';
const FormItem = Form.Item;
const {
  Header, Content, Footer, Sider,
} = Layout;
const SubMenu = Menu.SubMenu;

class L extends React.Component<any,any> {
  state = {
    collapsed: false,
  };
  private SecurityService = new SecurityService(this.props);

  
  
  componentDidMount(){
    console.log(this);
  }

  componentWillMount(){
    if(this.props.authenticated && this.props.session != undefined){
      this.props.history.push('/')
    }
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  load = () => {
    this.props.loader.start();
  }

  stop = () => {
    this.props.loader.stop();
  }


  handleSubmit = (e : FormEvent) => {
    var self = this;
    e.preventDefault();
    this.props.form.validateFields((err:any, values:any) => {
      if (err == undefined) {
          this.SecurityService.Login({UserName : values.userName, Password: values.password}).then(function(response){
            console.log(response);
            self.props.auth.login(response);
            self.props.history.push('/');
          })
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Row>
        <Col span={7} offset={14}>
        <Card
        className="login-form"
      title="Login">
      
      
      <Form onSubmit={this.handleSubmit} >
        <FormItem>
          {getFieldDecorator("userName", {
            rules: [
              { required: true, message: "Please input your username!" },
             
            ]
          })(
            <Input
              prefix={<Icon type="user" style={{ fontSize: 13 }} />}
              placeholder="Username"
            />
          )}
        </FormItem>

        <FormItem>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Input
              prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
              type="password"
              placeholder="Password"
            />
          )}
        </FormItem>

        <FormItem>
         
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Log in
          </Button>
         
        </FormItem>
      </Form>
      </Card>
        </Col>
      </Row>
      
    );
  }
}

function mapStateToProps(state : RootState ){
  return {
      authenticated : state.auth.authenticated,
      session : state.auth.session,
      loading : state.loader.loading,
  }
}

function mapDispatchToProps(dispatch : any){
  return {
      auth : bindActionCreators(AuthActionCreators,dispatch),
      loader : bindActionCreators(LoadActionCreators, dispatch)
  }
}

const Login = connect(mapStateToProps,mapDispatchToProps)(Form.create()(L));
export {Login};
//export { L as Login }