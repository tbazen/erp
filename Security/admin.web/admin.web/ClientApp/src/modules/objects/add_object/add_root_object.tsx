import React from 'react';
import {
    Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete,
  } from 'antd';
import SecurityService from 'src/_setup/services/security.service';
import Swal from 'sweetalert2';
import TextArea from 'antd/lib/input/TextArea';
const Option = Select.Option;
 
  export class AddRootObject extends React.Component<any,any> {
    private SecurityService :SecurityService;
    private sessionID : string;

    constructor(props : any){
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            Users : [],
          };
          this.SecurityService = new SecurityService(this.props);
          this.sessionID = this.props.session.payrollSessionID;
          this.setUserState = this.setUserState.bind(this);

    }

    componentDidMount(){
        this.setUserState();
    }
    

    setUserState(){
        var self = this;
        this.SecurityService.GetAllUsers2({SessionID : this.props.session.payrollSessionID}).then(function(response){
            if(response){
                self.setState({
                    Users : response
                })
            }
            
        })
    }


    handleSubmit = (e : React.FormEvent) => {
      var self = this;
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err : any, values: any) => {
        if (!err) {
            console.log(values);
            self.SecurityService.CreateObject({
                Name : values.name,
                SessionID : self.sessionID,
                ParentOID : -1,
                OwnerID : values.owner
            }).then(function(response){
              if(response != null){
                  Swal.fire("Success","Successfully Created Object","success").then(function(){
                      
                      self.props.update();
                      self.props.toggleModal();
                  })
              }
            })
            
          
        }
      });
    }
  

  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 8,
          },
        },
      };

  
  
      return (
        <Form {...formItemLayout} onSubmit={this.handleSubmit}>
          <Form.Item
            label="Name"
          >
            {getFieldDecorator('name', {
              rules: [ {
                required: true, message: 'Please input object name!',
              }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item
            label="Full Name"
          >
            {getFieldDecorator('fullName', {
              rules: [ {
             //   required: true, message: 'Please Enter FullName!',
              }],
            })(
              <TextArea disabled/>
            )}
          </Form.Item>

          <Form.Item
            label="Owner"
          >
            {getFieldDecorator('owner', {
              rules: [ {
                required: true, message: 'Please select user',
              }],
            })(
              <Select>
                  {
                      this.state.Users.map(function(item: any){
                          return <Option value={item.id} key={item.id}>
                              {item.userName}
                          </Option>
                      })
                  }
              </Select>
            )}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
          {getFieldDecorator('inherited', {
             initialValue : 'cheked',
            valuePropName: 'checked',
          })(
            <Checkbox>Inherited</Checkbox>
          )}
        </Form.Item>
                
        <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">Create</Button>
          </Form.Item>

        </Form>
      );
    }
  }