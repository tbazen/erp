import React from 'react';
import {
    Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete,
  } from 'antd';
import SecurityService from 'src/_setup/services/security.service';
import Swal from 'sweetalert2';
import TextArea from 'antd/lib/input/TextArea';
import { RenamePar } from 'src/_setup/services/pars/api.pars';
const Option = Select.Option;
 
  export default class EditObject extends React.Component<any,any> {
    private SecurityService :SecurityService;
    private sessionID : string;

    constructor(props : any){
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
            Users : [],
            parentID : "",
            parentObject : null,
            defaultOwner : null,
          };
          this.SecurityService = new SecurityService(this.props);
          this.sessionID = this.props.session.payrollSessionID;
          this.setUserState = this.setUserState.bind(this);
          this.getParentObject = this.getParentObject.bind(this);
          this.getOwner = this.getOwner.bind(this);

    }

    componentDidMount(){
        this.setUserState();

        this.setState({
            parentID : this.props.parentID
        },this.getParentObject)
    }

    getOwner(){
        var self = this;
        if(self.state.parentObject != null){
            var data = {SessionID : self.sessionID,
                user : self.state.parentObject.ownerName };
                console.log(self.state.parentObject);
            self.SecurityService.User(
                data).then(function(response){
                        self.setState({
                            defaultOwner : response
                        })
                  })
        }
    }

    getParentObject(){
        var self = this;
        this.SecurityService.GetObject(
            {
                ObjectID : self.state.parentID,
                SessionID : self.sessionID
            }
        ).then(function(response){
            self.setState({
                parentObject : response
            })
        }).then(function(){
            self.getOwner();
        });
    }



    componentWillReceiveProps(nextProps : any){
        this.setState({
            parentID : nextProps.parentID
        },this.getParentObject);
    }
    

    setUserState(){
        var self = this;
        this.SecurityService.GetAllUsers2({SessionID : this.props.session.payrollSessionID}).then(function(response){
            if(response){
                self.setState({
                    Users : response
                })
            }
            
        })
    }


    handleSubmit = (e : React.FormEvent) => {
      var self = this;
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err : any, values: any) => {
        if (!err) {
            console.log(values);
            var data : RenamePar = {
                NewName : values.name,
                SessionID : self.sessionID,
                ObjectID : Number(this.state.parentID),
                Owner : values.owner
            }
            console.log(data);
            self.SecurityService.Rename(data).then(function(response){
              if(response != null){
                  Swal.fire("Success","Successfully Edited Object","success").then(function(){      
                      self.props.update();
                      self.props.toggleModal();
                  })
              }
            })
          
        }
      });
    }
  

  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 8,
          },
        },
      };

  
  
      return (
        <Form {...formItemLayout} onSubmit={this.handleSubmit}>

          <Form.Item
            label="Name"
          >
            {getFieldDecorator('name', {
                 initialValue : this.state.parentObject != null ? this.state.parentObject.name : "",
              rules: [ {
                required: true, message: 'Please input object name!',
              }],
            })(
              <Input />
            )}
          </Form.Item>

          <Form.Item
            label="Full Name"
          >
            {getFieldDecorator('fullName', {
                 initialValue : this.state.parentObject != null ? this.state.parentObject.fullName : "",
              rules: [ {
             //   required: true, message: 'Please Enter FullName!',
              }],
            })(
              <TextArea disabled/>
            )}
          </Form.Item>

          <Form.Item
            label="Owner"
          >
            {getFieldDecorator('owner', {
              initialValue : this.state.defaultOwner != null ? this.state.defaultOwner.userID : "",
              rules: [ {
                required: true, message: 'Please select user',
              }],
            })(
              <Select>
                  {
                      this.state.Users.map(function(item: any){
                          return <Option value={item.id} key={item.id}>
                              {item.userName}
                          </Option>
                      })
                  }
              </Select>
            )}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
          {getFieldDecorator('inherited', {
             initialValue : 'cheked',
            valuePropName: 'checked',
          })(
            <Checkbox>Inherited</Checkbox>
          )}
        </Form.Item>
                
        <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">Edit</Button>
          </Form.Item>

        </Form>
      );
    }
  }