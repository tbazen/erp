import React from 'react';
import { Button, Tree, Row, Col, Card, Icon, Modal } from 'antd';
import SecurityService from 'src/_setup/services/security.service';
import { UserPassword, SecurityObject } from 'src/_infrastructure/model/UserSession';
import { Element } from 'react-scroll';
import "./objects.scss";
import { TreeSelect } from 'antd';
import { AddRootObject } from './add_object/add_root_object';
import AddChildObject from './add_object/add_child_object';
import Swal from 'sweetalert2';
import EditObject from './edit_object/edit_object';

const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const TreeNode = Tree.TreeNode;

function  userToNode(data : SecurityObject[]){
    const arr : any[] = [];
    data.map(function(item){
        if(item.name != "Root"){
            var el = {
                name : item.name,
                id : item.id,
                key : item.id,
                title : item.name,
                value : item.id,
                pid : item.parent != undefined ? item.parent.id : -1,
                selectable : true,
                isLeaf : false,
            }
            arr.push(el);
        }

    })
    return arr;
}

export class Objects extends React.Component<any,any>{

    private SecurityService : SecurityService;
    private sessionID : string ;
    constructor(props : any){
        super(props);
        this.state = {
            Objects : [],
            Users : null,
            SelectedObject : "",

            showAddRootObjectModal : false,
            showAddChidObjectModal : false,
            showEditObjectModal : false,
        }

        this.SecurityService = new SecurityService(this.props);
        this.handleClick = this.handleClick.bind(this);
        this.sessionID = this.props.session.payrollSessionID;

        this.toggleChildObjectModal = this.toggleChildObjectModal.bind(this);
        this.toggleEditObjectModal = this.toggleEditObjectModal.bind(this);
        this.toggleRootObjectModal = this.toggleRootObjectModal.bind(this);
        this.setObjectState = this.setObjectState.bind(this);
        this.DeleteObject = this.DeleteObject.bind(this);
    }

    componentWillMount(){
        console.log(this.props);
    }

    setObjectState(){
        var self = this;
        this.SecurityService.GetAllOjects({ObjectID : -1, SessionID : this.props.session.payrollSessionID}).then(function(response){
            self.setState({
                Objects : response
            })
        })
    }

    componentDidMount(){
        var self = this;
        self.setObjectState();

    }

    toggleRootObjectModal(){
        this.setState({
            showAddRootObjectModal : !this.state.showAddRootObjectModal
        });
    }

    toggleChildObjectModal(){
        this.setState({
            showAddChidObjectModal : !this.state.showAddChidObjectModal
        });
    }

    toggleEditObjectModal(){
        this.setState({
            showEditObjectModal : !this.state.showEditObjectModal
        });
    }

    onChange = (value : string[]) => {
        console.log('onChange ', value);
        this.setState({ value });
      }

      
    handleClick(selected : string[]){
        console.log(selected);
        this.setState({
            SelectedObject : selected[0]
        });
        var self = this;
        this.SecurityService.GetPermitedUsers({ObjectID : selected[0], SessionID : this.sessionID }).then(function(response){
            console.log(response);
            self.setState({
                Users : response
            });
        })
    }

    
    DeleteObject(){
        var self = this;
        Swal.fire("Warning","Are You Sure You Want To Delete This Object?","warning").then(function(val){
            if(val.value != undefined && val.value == true){
                self.SecurityService.DeleteObject({
                    SessionID : self.sessionID,
                    ObjectID : self.state.SelectedObject
                }).then(function(response){
                    if(response != null){
                        Swal.fire("Success","Successfully Deleted Object","success").then(function(){
                            self.setObjectState();
                            self.setState({
                                Users : null,
                                SelectedObject : "",
                            })
                        })
                    }
                })
            }
        })
    }


    list_to_tree = (list : any[]) => {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i; // initialize the map
            list[i].children = []; // initialize the children
        }

        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.pid != -1) {
                // if you have dangling branches check that map[node.parentId] exists
                if (list[map[node.pid]] != undefined){
                    list[map[node.pid]].children.push(node);
                }
                
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    renderTreeNodes = (data : any[]) => {
        return data.map((item) => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item} className="treeNode">
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} dataRef={item} />;
        });
    }


    render(){

        const nodeList = this.state.Objects.length > 0 ? userToNode(this.state.Objects) : [];
        const CatTree = this.list_to_tree(nodeList);
        const TreeNodes = this.renderTreeNodes(CatTree);
        const tProps = {
            treeData : CatTree,
            value: this.state.value,
            onChange: this.onChange,
            treeCheckable: true,
            showCheckedStrategy: SHOW_PARENT,
            searchPlaceholder: 'Please select',
            style: {
              width: 300,
            },
          };


        return(
            <div>

                               {/* Add Object Modal */}
                               <Modal
                        title="Add Root Object"
                        visible={this.state.showAddRootObjectModal}
                        onCancel={this.toggleRootObjectModal}
                        footer={[]}>
                        <AddRootObject {...this.props} 
                        toggleModal={() => this.toggleRootObjectModal()}
                        update={() => this.setObjectState()}
                        />
                        </Modal>

                {/*  */}

                
                               {/* Add Child Object Modal */}
                               <Modal
                        title="Add Child Object"
                        visible={this.state.showAddChidObjectModal}
                        onCancel={this.toggleChildObjectModal}
                        footer={[]}>
                        <AddChildObject {...this.props} 
                        toggleModal={() => this.toggleChildObjectModal()}
                        update={() => this.setObjectState()}
                        parentID={this.state.SelectedObject != null ? this.state.SelectedObject : ""}
                        />
                        </Modal>

                {/*  */}

                                {/*  */}

                
                               {/* Add Child Object Modal */}
                               <Modal
                        title="Edit Object"
                        visible={this.state.showEditObjectModal}
                        onCancel={this.toggleEditObjectModal}
                        footer={[]}>
                        <EditObject {...this.props} 
                        toggleModal={() => this.toggleEditObjectModal()}
                        update={() => this.setObjectState()}
                        parentID={this.state.SelectedObject != null ? this.state.SelectedObject : ""}
                        />
                        </Modal>

                {/*  */}


                <Row>
 
                <Col span={8}>
                    <Card title="Objects"
                    extra={
                        <Button onClick={() => this.toggleRootObjectModal()}>  <Icon type="folder-add" />Add Root Object</Button>
                    }
                    >
                        <Element 
                            name=""
                            className="element" id="containerElement" style={{
                               
                                height: '500px',
                                overflow: 'scroll',
                                }}
                        >
                      <Tree
                                style={{ width: 300 }}
                                // dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                onSelect={this.handleClick}
                            >
                                {TreeNodes}
                            </Tree>
                        </Element>
                        </Card>
                    </Col>
                    <Col span={16}>
                    <Card title="Users"
                    extra={
                        this.state.SelectedObject != "" ?
                        <Button.Group>
                            <Button onClick={() => this.toggleChildObjectModal()}> <Icon type="plus" />Add</Button>
                            <Button onClick={() => this.toggleEditObjectModal()}> <Icon type="edit" />Edit</Button>
                            <Button onClick={() => this.DeleteObject()}> <Icon type="minus" />Delete</Button>
                        </Button.Group> : null
                    }
                    >
                    <Element 
                            name=""
                            className="element" id="containerElement" style={{
                                
                                height: '500px',
                                overflow: 'scroll',
                                }}
                        >
                        {
                            this.state.SelectedObject != "" && this.state.Users != null ?
                                <div>
                                    <h4>Users</h4>
                                    <div className="users">

                                   
                                    {
                                        
                                        this.state.Users.map(function(item : number){
                                            return <h4>{item}</h4>
                                        })
                                    }
                                     </div>
    
                                </div>

                            : null
                        
                        }

                           </Element>
                        </Card>
                    </Col>
       
                </Row>
          
            </div>
            
        )
    }
}