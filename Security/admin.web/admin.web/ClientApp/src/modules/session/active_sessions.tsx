import React from 'react';
import SecurityService from 'src/_setup/services/security.service';
import Axios, { AxiosInstance } from 'axios';
import { baseUrl } from 'src/_setup/services/urlConfig';
import Swal from 'sweetalert2';
import { Row, Col, Card, Table } from 'antd';

interface state {
    sessions : ActiveSession[]
}

export class ActiveSession extends React.Component<any, state>{

    private SecurityService : SecurityService;
    private sessionID : string ;
    private axios : AxiosInstance;
    constructor(props : any){
        super(props);
        this.state = {
            sessions :  []
        }
        this.SecurityService = new SecurityService(this.props);
        this.sessionID = this.props.session.payrollSessionID;
        this.GetActiveSession = this.GetActiveSession.bind(this);
        this.axios = Axios.create({
            baseURL : baseUrl + "/app/Server",
            withCredentials : true,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Headers': '*',
                "Access-Control-Allow-Origin": "*",
                'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            }
        })
    }

    GetActiveSession(){
        var self = this;
        self.axios.post<ActiveSession[]>("/GetActiveSessions").then(function(response){
            if(response.data.length > 0){
                self.setState({
                    sessions : response.data
                });
            }
        }).catch(function(error){
            Swal.fire("Error",error.response.data,"error");
        })
    }

    componentDidMount(){
        this.GetActiveSession();
    }
 
    render(){

        const columns = [
            {
                title : "UserName",
                key : "sessionID",
                dataIndex : "userName"
            },
            {
                title : "Souce",
                dataIndex : "source",
            },{
                title : "Loged In Time",
                dataIndex : "logedInTime"
            },{
                title : "Idle Seconds",
                dataIndex : "idleSeconds"
            }
        ]

        return (
            <Row>
                <Col offset={2}>
                    <Card
                        title="Active Sessions">
                            <Table
                            dataSource={this.state.sessions}
                            columns={columns}
                            />
                        
                        </Card>
                </Col>

            </Row>
        )
    }

}