import React from 'react';
import {
    Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete,
  } from 'antd';
import SecurityService from 'src/_setup/services/security.service';
import Swal from 'sweetalert2';
  
 
  export class AddUser extends React.Component<any,any> {
    private SecurityService :SecurityService;
    private sessionID : string;

    constructor(props : any){
        super(props);
        this.state = {
            confirmDirty: false,
            autoCompleteResult: [],
          };
          this.SecurityService = new SecurityService(this.props);
          this.sessionID = this.props.session.payrollSessionID;

    }

    
    handleSubmit = (e : React.FormEvent) => {
      var self = this;
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err : any, values: any) => {
        if (!err) {
          self.SecurityService.CreateUser({
              UserName : values.username,
              Password : values.password,
              SessionID  : this.sessionID,
              ParentUser : ""
          }).then(function(response){
            if(response != null){
                Swal.fire("Success","Successfully Created User","success").then(function(){
                    
                    self.props.update();
                    self.props.toggleModal();
                })
            }
          })
          
        }
      });
    }
  
    handleConfirmBlur = (e : any) => {
      const value = e.target.value;
      this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }
  
    compareToFirstPassword = (rule : any, value : any, callback :any) => {
      console.log(rule);
      const form = this.props.form;
      if (value && value !== form.getFieldValue('password')) {
        callback('Two passwords that you enter is inconsistent!');
      } else {
        callback();
      }
    }
  
    validateToNextPassword = (rule : any, value : any, callback :any) => {
        console.log(rule);
      const form = this.props.form;
      if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
      }
      callback();
    }
  
  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 8,
          },
        },
      };

  
  
      return (
        <Form {...formItemLayout} onSubmit={this.handleSubmit}>
          <Form.Item
            label="Username"
          >
            {getFieldDecorator('username', {
              rules: [ {
                required: true, message: 'Please input username!',
              }],
            })(
              <Input />
            )}
          </Form.Item>
          <Form.Item
            label="Password"
          >
            {getFieldDecorator('password', {
              rules: [{
                required: true, message: 'Please input your password!',
              }, {
                validator: this.validateToNextPassword,
              }],
            })(
              <Input type="password" />
            )}
          </Form.Item>
          <Form.Item
            label="Confirm Password"
          >
            {getFieldDecorator('confirm', {
              rules: [{
                required: true, message: 'Please confirm your password!',
              }, {
                validator: this.compareToFirstPassword,
              }],
            })(
              <Input type="password" onBlur={this.handleConfirmBlur} />
            )}
          </Form.Item>
        
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">Register</Button>
          </Form.Item>
        </Form>
      );
    }
  }