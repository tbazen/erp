import React from 'react';
import { Button, Tree, Row, Col, Card, Icon, Form, Input } from 'antd';
import SecurityService from 'src/_setup/services/security.service';
import { UserPassword, SecurityObject } from 'src/_infrastructure/model/UserSession';
import { Element } from 'react-scroll';
import "./set_permission.scss";
import { TreeSelect } from 'antd';
import Swal from 'sweetalert2';

const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const TreeNode = Tree.TreeNode;

function  userToNode(data : SecurityObject[]){
    const arr : any[] = [];
    data.map(function(item){
        if(item.name != "Root"){
            var el = {
                name : item.name,
                id : item.id,
                key : item.id,
                title : item.name,
                value : item.id,
                pid : item.parent != undefined ? item.parent.id : -1,
                selectable : true,
                isLeaf : false,
            }
            arr.push(el);
        }

    })
    return arr;
}

export class SetPermission extends React.Component<any,any>{

    private SecurityService : SecurityService;
    private sessionID : string ;
    constructor(props : any){
        super(props);
        this.state = {
            Objects : [],
            Users : null,
            SelectedObject : "",
            value : [],
            value2 : [],
            origValue : [],
            origValue2 : [],
            permission : [],
            denied : [],
            permittedVal : [],
            deniedVal : [],
            username : "",
            userID : "",
        }

        this.SecurityService = new SecurityService(this.props);
        this.handleClick = this.handleClick.bind(this);
        this.sessionID = this.props.session.payrollSessionID;
    }

    componentWillMount(){
        console.log(this.props);
    }


    componentDidMount(){
        var self = this;


        this.SecurityService.GetAllOjects({ObjectID : -1, SessionID : this.props.session.payrollSessionID}).then(function(response){
            self.setState({
                Objects : response
            })
        }).then(function(){
            console.log(self.state);
        })

        this.setState({  
            value : this.props.permissions ,
            value2 : this.props.denied,
            origValue : this.props.permissions,
            origValue2  : this.props.denied,
            username : this.props.username ,
            userID : this.props.userID, })
       
    }

    componentWillReceiveProps(nextProps : any){
        console.log(nextProps);
       this.setState({  
        value : nextProps.permissions ,
        value2 : nextProps.denied,
        origValue : nextProps.permissions,
        origValue2  : nextProps.denied,
        username : nextProps.username,
        userID : nextProps.userID })

     }

     
    onChange = (value : string[]) => {
        console.log('onChange ', value);
        this.setState({ value });
      }


      onChange2 = (value2 : string[]) => {
        console.log('onChange ', value2);
        this.setState({ value2  });
        // this.props.form.setFieldsValue({ denied : value})
      }

      handleSubmit = (e : React.FormEvent) => {
        var self = this;
        e.preventDefault();
        console.log(this.state);
        this.SecurityService.SetPermission2({
            SessionID : self.sessionID,
            UserId : self.state.userID,
            Denied : self.state.value2,
            Permissions : self.state.value
        }).then(function(response){
            console.log(response);
            if(response){
                Swal.fire("Success","Successfully set user permissions","success");
            }
        }).then(function(){
            self.props.toggleModal();
            self.props.update();
        })

      }

      
    handleClick(selected : string[]){
        console.log(selected);
        this.setState({
            SelectedObject : selected[0]
        });
        var self = this;
        this.SecurityService.GetPermitedUsers({ObjectID : selected[0], SessionID : this.sessionID }).then(function(response){
            console.log(response);
            self.setState({
                Users : response
            });
        })
    }

    list_to_tree = (list : any[]) => {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i; // initialize the map
            list[i].children = []; // initialize the children
        }

        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.pid != -1) {
                // if you have dangling branches check that map[node.parentId] exists
                if (list[map[node.pid]] != undefined){
                    list[map[node.pid]].children.push(node);
                }
                
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    renderTreeNodes = (data : any[]) => {
        return data.map((item) => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item} className="treeNode">
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} dataRef={item} />;
        });
    }


    render(){

        const { getFieldDecorator } = this.props.form;
    
        const self = this;
        const formItemLayout = {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
          },
        };
        const tailFormItemLayout = {
          wrapperCol: {
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 8,
            },
          },
        };

        
        const nodeList = this.state.Objects.length > 0 ? userToNode(this.state.Objects) : [];
        const CatTree = this.list_to_tree(nodeList);
        const TreeNodes = this.renderTreeNodes(CatTree);
        const Cat2Tree = CatTree;
        const tProps = {
            treeData : CatTree,
            value: this.state.value,
            
            onChange: this.onChange,
            treeCheckable: true,
            showCheckedStrategy: SHOW_PARENT,
            searchPlaceholder: 'Please select',
            style: {
              width: 300,
            },
          };


        const t1Props = {
            treeData : Cat2Tree,
            value: this.state.value2,      
            onChange: this.onChange2,
            treeCheckable: true,
            showCheckedStrategy: SHOW_PARENT,
            searchPlaceholder: 'Please select',
            style: {
              width: 300,
            },
          };


        return(
            
            <div>
                <Form {...formItemLayout} 
                onSubmit={this.handleSubmit}
                >
          <Form.Item
            label="Username"
          >
            {getFieldDecorator('username', {
              initialValue : this.state.username,
              rules: [ 
                     
                {
              }],
            })(
              <Input disabled/>
            )}
          </Form.Item>
          <Form.Item
            label="Permissions"
          >
        <TreeSelect {...tProps} />
               
          </Form.Item>
          <Form.Item
            label="Denied"
          >
            <TreeSelect  {...t1Props} />

               
      
          </Form.Item>
        
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit"
            onClick={() => console.log(this)}
            >Set Permission</Button>
          </Form.Item>
        </Form>

            </div>
            
        )
    }
}