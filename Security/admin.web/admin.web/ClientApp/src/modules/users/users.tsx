import React from 'react';
import { Button, Tree, Row, Col, Card, Icon, Modal } from 'antd';
import SecurityService from 'src/_setup/services/security.service';
import { UserPassword } from 'src/_infrastructure/model/UserSession';
import { Element } from 'react-scroll';
import './users.scss';
import { AntTreeNodeSelectedEvent } from 'antd/lib/tree';
import { AddUser } from './AddUser/add_user';
import Swal from 'sweetalert2';
import { ChangePassword } from './ChangePassword/change_password';
import { SetPermission } from './SetPermission/set_permission';
const TreeNode = Tree.TreeNode;

function  userToNode(data : UserPassword[]){
    const arr : any[] = [];
    data.map(function(item){
        var el = {
            name : item.userName,
            id : item.id,
            key : item.id,
            title : item.userName,
            value : item.id,
            pid : item.parentId,
            selectable : true,
            isLeaf : true,
        }
        arr.push(el);
    })
    return arr;
}

export class User extends React.Component<any,any>{

    private SecurityService : SecurityService;
    private sessionID : string ;
   // public PermissionChild : any; 
    constructor(props : any){
        super(props);
        this.state = {
            Users : [],
            Objects : [],
            SelectedUser : "",
            UserPermissions : null,
            showAddUserModal : false,
            showChangePasswordModal : false,
            showSetPermissionModal : false,
        }

        this.SecurityService = new SecurityService(this.props);
        this.handleClick = this.handleClick.bind(this);
        this.GetObjectName = this.GetObjectName.bind(this);
        this.toggleAddUserModal = this.toggleAddUserModal.bind(this);
        this.toggleChangePasswordModal = this.toggleChangePasswordModal.bind(this);
        this.toggleSetPermissionModal = this.toggleSetPermissionModal.bind(this);
        this.DeleteUser = this.DeleteUser.bind(this);
        this.sessionID = this.props.session.payrollSessionID;
       // this.PermissionChild = React.createRef();

        this.forceUpdate = this.forceUpdate.bind(this);
        this.setUserState = this.setUserState.bind(this);
        this.setUserPermissionState = this.setUserPermissionState.bind(this);
    }

    componentWillMount(){

    }

    setUserState(){
        var self = this;
        this.SecurityService.GetAllUsers2({SessionID : this.props.session.payrollSessionID}).then(function(response){
            if(response){
                self.setState({
                    Users : response
                })
            }
            
        })
    }

    componentDidMount(){
        var self = this;
        self.setUserState();
        this.SecurityService.GetAllOjects({ObjectID : -1, SessionID : this.props.session.payrollSessionID}).then(function(response){
            if(response){
                self.setState({
                    Objects : response
                })
            }
            
        })

        
    }

    GetObjectName(id : number){
        var obj = this.state.Objects.filter(function(item : any){
            return item.id == id
        });

        return obj[0].fullName;
    }

    toggleAddUserModal(){
        this.setState({
            showAddUserModal : !this.state.showAddUserModal
        });
    }
    toggleChangePasswordModal(){
        this.setState({
            showChangePasswordModal : !this.state.showChangePasswordModal
        });
    }

    toggleSetPermissionModal(){
        // console.log(this.PermissionChild.current);
        this.setState({
            showSetPermissionModal : !this.state.showSetPermissionModal
        })
        // this.PermissionChild.current.props.form.setFieldValues({
        //     permission : this.state.UserPermissions.permissions,
        //     denied : this.state.UserPermissions.denied
        // })
    }

    forceUpdate(){
        this.forceUpdate();
    }

    DeleteUser(){
        var self = this;
        Swal.fire("Warning","Are You Sure You Want To Delete This User?","warning").then(function(val){
            if(val.value != undefined && val.value == true){
                self.SecurityService.DeleteUser({
                    SessionID : self.sessionID,
                    UserName : self.state.UserPermissions.userName
                }).then(function(response){
                    if(response != null){
                        Swal.fire("Success","Successfully Deleted User","success").then(function(){
                            self.setUserState();
                            self.setState({
                                UserPermissions : null,
                                SelectedUser : "",
                            })
                        })
                    }
                })
            }
        })
    }

    list_to_tree = (list : any[]) => {
        var map = {}, node, roots = [], i;
        for (i = 0; i < list.length; i += 1) {
            map[list[i].id] = i; // initialize the map
            list[i].children = []; // initialize the children
        }

        for (i = 0; i < list.length; i += 1) {
            node = list[i];
            if (node.pid != -1) {
                // if you have dangling branches check that map[node.parentId] exists
                if (list[map[node.pid]] != undefined){
                    list[map[node.pid]].children.push(node);
                }
                
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    setUserPermissionState(){
        var self = this;
        this.SecurityService.getUserPermissions({SessionID : this.sessionID, UserID : self.state.SelectedUser}).then(function(response){
            self.setState({
                UserPermissions : response
            });
        })
    }

    handleClick(selected : string[]){
        var self = this;
        this.setState({
            SelectedUser : selected[0]
        },() => self.setUserPermissionState());

        
    }

    renderTreeNodes = (data : any[]) => {
        return data.map((item) => {
            if (item.children) {
                return (
                    <TreeNode title={item.title} key={item.key} dataRef={item} className="treeNode">
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} dataRef={item} />;
        });
    }


    render(){
        var self =this;
        const nodeList = this.state.Users.length > 0 ? userToNode(this.state.Users) : [];
        const CatTree = this.list_to_tree(nodeList);
        const TreeNodes = this.renderTreeNodes(CatTree);



        return(
            <div>
                
               {/* Add User Modal */}
                    <Modal
                        title="Add New User"
                        visible={this.state.showAddUserModal}
                        onCancel={this.toggleAddUserModal}
                        footer={[]}>
                        <AddUser {...this.props} 
                        toggleModal={() => this.toggleAddUserModal()}
                        update = {() =>this.setUserState()}
                        />
                        </Modal>

                {/*  */}


              {/* Change Password Modal */}
              <Modal
                        title="Change Password"
                        onCancel={this.toggleChangePasswordModal}
                        visible={this.state.showChangePasswordModal}
                        footer={[]}>
                          <ChangePassword {...this.props} 
                          username={this.state.UserPermissions != null ? this.state.UserPermissions.userName :""}
                        toggleModal={() => this.toggleChangePasswordModal()}

                        />
                        </Modal>

                {/*  */}

               {/* Set Permission Modal */}

               <Modal
                        title="Set Permission"
                        onCancel={this.toggleSetPermissionModal}
                        visible={this.state.showSetPermissionModal}
                        footer={[]}>
                       <SetPermission {...this.props}
                       
                //        ref={this.PermissionChild}
                       username={this.state.UserPermissions != null ? this.state.UserPermissions.userName :""}
                        permissions={this.state.UserPermissions != null ? this.state.UserPermissions.permissions :""}
                        denied = {this.state.UserPermissions != null ? this.state.UserPermissions.denied :""}
                         toggleModal={() => this.toggleSetPermissionModal()}
                         update={() => this.setUserPermissionState()}
                         userID={this.state.SelectedUser != null ? this.state.SelectedUser : ""}
                       />
                        </Modal>
                {/*  */}



                <Row>
                    <Col span={8}>
                    <Card title="Users"
                    extra={
                        <Button onClick={this.toggleAddUserModal}> <Icon type="user-add" />Add User</Button>
                    }
                    >
                        <Element 
                            name=""
                            className="element" id="containerElement" style={{
                                
                                height: '500px',
                                overflow: 'scroll',
                                }}
                        >
                <Tree
                             onSelect={this.handleClick}
                             style={{ width: 300 }}
                                // dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                //onSelect={this.handleClick}
                            >
                                {TreeNodes}
                            </Tree>
                        </Element>
                        </Card>
                    </Col>
                    <Col span={16}>
                    <Card title="User Information"
                    extra={
                        this.state.SelectedUser != "" ?
                        <Button.Group>
                            <Button onClick={this.toggleChangePasswordModal}> <Icon type="bars" />Change Password</Button>
                            <Button onClick={this.toggleSetPermissionModal}> <Icon type="setting" />Set Permission</Button>
                            <Button onClick={() => this.DeleteUser()}> <Icon type="user-delete" />Delete</Button>
                        </Button.Group> : null
                    }
                    >
                    <Element 
                            name=""
                            className="element" id="containerElement" style={{
                                
                                height: '500px',
                                overflow: 'scroll',
                                }}
                        >
                        {
                            this.state.SelectedUser != "" && this.state.UserPermissions != null ?
                                <div>
                                    <h3>Username : {this.state.UserPermissions.userName}</h3>
                                    <h3>Permission</h3>
                                    <div className="permissions">

                                   
                                    {
                                        
                                        this.state.UserPermissions.permissions.map(function(item : number){
                                            return <h5>{self.GetObjectName(item)}</h5>
                                        })
                                    }
                                     </div>
                                     <h3>Denied</h3>
                                    <div className="permissions">

                                   
                                    {
                                        
                                        this.state.UserPermissions.denied.map(function(item : number){
                                            return <h4>{self.GetObjectName(item)}</h4>
                                        })
                                    }
                                     </div>
                                </div>

                            : null
                        
                        }
                           </Element>
                        </Card>
                    </Col>
                </Row>

            </div>
            
        )
    }
}