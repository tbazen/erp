﻿using System;
using System.Collections.Generic;
using System.Text;
//using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Management;

namespace TLicense
{
    [Serializable]
    public class LicenseValue
    {
        public string description;
        public string key;
        public string value;
        public long expiryTime=-1;
    }
    [Serializable]
    public class LicenseData
    {
        public string id;
        public string licensee;
        public string description;
        public LicenseValue[] data;
    }
    public partial class LicenseDataLoader
    {
        static string getHardDiskSerialNo()
        {
            ManagementObjectSearcher searcher = new
            ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
            string ret = "";
            foreach (ManagementObject wmi_HD in searcher.Get())
            {
                if (!string.IsNullOrEmpty(wmi_HD["SerialNumber"] as string))
                    ret+= wmi_HD["SerialNumber"].ToString();
            }
            return ret;
        }
        public static byte[] generateKey()
        {
            return MD5.Create().ComputeHash(System.Text.Encoding.Unicode.GetBytes(getHardDiskSerialNo()));
        }
        public static byte[] generateIV(int size)
        {
            Random r=new Random();
            byte[] b = new byte[size/8];
            for (int i = 0; i < b.Length; i++)
                b[i] = (byte)(i*100);
            return b;
        }
        public static Dictionary<string, LicenseValue> licenseValues = null;
        public static LicenseData licenseData = null;
        static LicenseDataLoader()
        {
            try
            {
                RijndaelManaged RMCrypto = new RijndaelManaged();
                RMCrypto.Padding = PaddingMode.Zeros;
                byte[] bytes = Convert.FromBase64String(data);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
                System.Security.Cryptography.CryptoStream cs = new System.Security.Cryptography.CryptoStream(ms, RMCrypto.CreateDecryptor(generateKey(), generateIV(RMCrypto.BlockSize)), CryptoStreamMode.Read);
                
                //System.IO.StreamReader reader = new System.IO.StreamReader(cs);
                //string xml=reader.ReadToEnd();
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(LicenseData));
                licenseData = s.Deserialize(cs) as LicenseData;
                cs.Dispose();
                ms.Dispose();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error trying to load licenseData\n{0}",ex.Message);
                licenseData = null;
            }
            if (licenseData != null)
            {
                licenseValues=new Dictionary<string,LicenseValue>();
                foreach(LicenseValue val in licenseData.data)
                {
                    licenseValues.Add(val.key,val);
                }
            }
        }
        
    }
    public class TLicenseClass
    {

        public static string getLicenseValue(string key)
        {
            if (LicenseDataLoader.licenseValues != null)
            {
                LicenseValue ret;
                if (LicenseDataLoader.licenseValues.TryGetValue(key, out ret))
                {
                    if (ret.expiryTime == -1 || ret.expiryTime < DateTime.Now.Ticks)
                        return ret.value;
                }
            }
            return null;
        }
        static LicenseValue getLicenseData(string key)
        {
            if (LicenseDataLoader.licenseValues != null)
            {
                LicenseValue ret;
                if (LicenseDataLoader.licenseValues.TryGetValue(key, out ret))
                {
                    return ret;
                }
            }
            return null;
        }
        
        public static void checkLicense(string key)
        {
            LicenseValue l;
            if ((l = getLicenseData(key)) == null)
                throw new InvalidOperationException("You don't have license for " + key);
            if(!"yes".Equals(l.value,StringComparison.CurrentCultureIgnoreCase))
                throw new InvalidOperationException("You don't have license for " + l.description);
            checkExpiry(l);
        }

        private static void checkExpiry(LicenseValue l)
        {
            if (l.expiryTime!=-1 && DateTime.Now.Ticks > l.expiryTime)
                throw new InvalidOperationException("Your license for " + l.description + " has expired on " + new DateTime(l.expiryTime));
        }
        public static void checkLicense(string key, int minValue)
        {
            LicenseValue l;
            if ((l = getLicenseData(key)) == null)
                throw new InvalidOperationException("You don't have license for " + key);
            if (l.value == null)
                throw new InvalidOperationException("You don't have license for " + l.description);
            checkExpiry(l);
            int val;
            
            if(!int.TryParse(l.value,out val))
                throw new InvalidOperationException("You don't have valid license for " + l.description);
            if(val<minValue)
                throw new InvalidOperationException("You don't have suffient number of license for " + l.description);
        }
    }
}
