﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TLicenseManager
{
    public partial class DatePicker : Form
    {
        public event EventHandler datePicked;
        DateTime _pickedDate = DateTime.Now;

        public DateTime PickedDate
        {
            get { return _pickedDate; }
            set { _pickedDate = value; }
        }

        public DatePicker()
        {
            InitializeComponent();
        }

        private void DatePicker_Deactivate(object sender, EventArgs e)
        {
            _pickedDate = monthCalendar.SelectionStart;
            if(this.Visible)
                this.Hide();
        }
        bool _ignoreChangeEvent = false;
        private void DatePicker_Activated(object sender, EventArgs e)
        {
            _ignoreChangeEvent = true;
            monthCalendar.SelectionStart = _pickedDate;
            monthCalendar.SelectionEnd = _pickedDate;
            _ignoreChangeEvent = false;
        }

        private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            if (_ignoreChangeEvent) return;
            _pickedDate = monthCalendar.SelectionStart;
            if (datePicked != null)
                datePicked(this, null);
                this.Hide();
        }
    }
}
