﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.CodeDom.Compiler;

namespace TLicenseManager
{
    public partial class LicenseGeneratorForm : Form
    {
        DatePicker datePicker;
        public LicenseGeneratorForm()
        {
            InitializeComponent();
            grid.Columns[0].ValueType = typeof(string);
            grid.Columns[1].ValueType = typeof(string);
            grid.Columns[2].ValueType = typeof(bool);
            grid.Columns[3].ValueType = typeof(DateTime);
            datePicker = new DatePicker();
            datePicker.datePicked += new EventHandler(datePicker_datePicked);
        }
        int datePickerRow = -1;
        void datePicker_datePicked(object sender, EventArgs e)
        {
            grid.Rows[datePickerRow].Cells[colExpiryTime.Index].Value = datePicker.PickedDate.Date;
            grid.Rows[datePickerRow].Cells[colExpires.Index].Value = true;
            
        }

        private void grid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            if (e.ColumnIndex == colPickDate.Index)
            {
                datePickerRow = e.RowIndex;
                datePicker.Location = Cursor.Position;
                if (grid.Rows[datePickerRow].Cells[colExpiryTime.Index].Value is DateTime)
                    datePicker.PickedDate = (DateTime)grid.Rows[datePickerRow].Cells[colExpiryTime.Index].Value;
                datePicker.Show();
            }
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            
            string sourceFileName = @"C:\Source\Code\TLicense\License\LicenseDataLoader.cs";
            string sourceFile2 =    @"C:\Source\Code\TLicense\License\TLicenseClass.cs";
            string dataSourceCode = @"namespace TLicense
{
    partial class LicenseDataLoader
    {
        static string data = ""___data___"";
    }
}
";
            TLicense.LicenseData data = new TLicense.LicenseData();
            data.id = textID.Text;
            data.licensee = textName.Text;
            data.description = textDescription.Text;
            List<TLicense.LicenseValue> values = new List<TLicense.LicenseValue>();
            foreach (DataGridViewRow row in grid.Rows)
            {
                if (row.IsNewRow)
                    continue;
                TLicense.LicenseValue l = new TLicense.LicenseValue()
                {
                    key = row.Cells[0].Value as string,
                    description = row.Cells[1].Value as string,
                    expiryTime = (row.Cells[2].Value is bool)
                    && ((bool)row.Cells[2].Value)
                    && (row.Cells[3].Value is DateTime) ? ((DateTime)row.Cells[3].Value).Ticks : (long)-1,
                    value = row.Cells[5].Value as string

                };
                if (string.IsNullOrEmpty(l.key))
                    continue;
                values.Add(l);
            }
            data.data = values.ToArray();
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(TLicense.LicenseData));
            s.Serialize(ms, data);
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            string xml = new System.IO.StreamReader(ms).ReadToEnd();
            
           

            byte[] bytes = new byte[ms.Length];
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            ms.Read(bytes, 0, bytes.Length);
            ms.Dispose();
            byte[] padded = new byte[bytes.Length + 16];
            bytes.CopyTo(padded, 0);
            //byte[] padded = bytes;

            RijndaelManaged RMCrypto = new RijndaelManaged();
            RMCrypto.Padding = PaddingMode.Zeros;
            ms = new System.IO.MemoryStream();
            System.Security.Cryptography.CryptoStream cs = new System.Security.Cryptography.CryptoStream(ms, RMCrypto.CreateEncryptor(TLicense.LicenseDataLoader.generateKey(), TLicense.LicenseDataLoader.generateIV(RMCrypto.BlockSize)), CryptoStreamMode.Write);
            cs.Write(padded, 0, padded.Length);

            byte[] encrypted = new byte[ms.Length];
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            ms.Read(encrypted, 0, encrypted.Length);
            cs.Dispose();
            ms.Dispose();


            dataSourceCode = dataSourceCode.Replace("___data___", Convert.ToBase64String(encrypted));
            System.IO.File.WriteAllText(sourceFileName, dataSourceCode);

            Microsoft.CSharp.CSharpCodeProvider compiler = new Microsoft.CSharp.CSharpCodeProvider();
            string outputFile = textFileName.Text.Trim();
            if (string.IsNullOrEmpty(outputFile))
            {
                FolderBrowserDialog of = new FolderBrowserDialog();
                if (of.ShowDialog(this) != DialogResult.OK)
                    return;
                outputFile = of.SelectedPath + "\\LUtility.dll";
                textFileName.Text = outputFile;
            }
            System.CodeDom.Compiler.CompilerParameters pars=new System.CodeDom.Compiler.CompilerParameters(new string[] 
                        {
                            "System.dll",
                            "System.Core.dll",
                            "System.Xml.dll",
                            "System.Management.dll",
                        }
                , outputFile, true);
            
            //pars.OutputAssembly = "LUtility";
            //pars.GenerateExecutable = true;
            
            System.CodeDom.Compiler.CompilerResults res=compiler.CompileAssemblyFromFile(pars, sourceFileName, sourceFile2);
            if (res.Errors.HasErrors)
            {
                string error = "";
                foreach (CompilerError er in res.Errors)
                {
                    error += "\n" + er.ErrorText;
                }
                MessageBox.Show(error);
            }
            else
                MessageBox.Show("Compile succesfull");

        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;
            if (e.ColumnIndex == colExpires.Index)
            {
                if (!(bool)grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Value)
                    grid.Rows[e.RowIndex].Cells[colExpiryTime.Index].Value = null;
            }
        }

        private void buttonPickFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Multiselect = false;
            of.Filter = "*.dll|*.dll";
            if (of.ShowDialog(this) == DialogResult.OK)
            {
                System.IO.File.Delete("license.xml");
                AppDomain d = AppDomain.CreateDomain("creator");
                d.ExecuteAssembly("TLicenseTest.exe"
                    , new string[]
                {
                    of.FileName,
                    "license.xml"
                }
                    );
                AppDomain.Unload(d);
            }
            if (System.IO.File.Exists("license.xml"))
            {
                System.IO.FileStream fs = System.IO.File.OpenRead("license.xml");
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(TLicense.LicenseData));
                TLicense.LicenseData data = s.Deserialize(fs) as TLicense.LicenseData;
                fs.Close();
                if (data != null)
                {
                    textFileName.Text = of.FileName;
                    textID.Text = data.id;
                    textName.Text = data.licensee;
                    textDescription.Text = data.description;
                    grid.Rows.Clear();
                    foreach (TLicense.LicenseValue val in data.data)
                    {
                        grid.Rows.Add(
                            val.key,
                            val.description,
                            val.expiryTime > 1,
                            val.expiryTime > 1 ? (object)new DateTime(val.expiryTime).Date : null,
                            "",
                            val.value
                            );
                    }
                }
            }
        }
    }
}
