﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace TLicenseTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 1)
            {
                Assembly assembly=System.Reflection.Assembly.LoadFile(args[0]);
                Type t = assembly.GetType("TLicense.LicenseDataLoader");
                if (t == null)
                    Console.WriteLine("Couldn't load license class");
                FieldInfo field = t.GetField("licenseData");
                if (field == null)
                    Console.WriteLine("Couldn't load licenseData field");
                object obj = field.GetValue(null);
                if (obj != null)
                {
                    
                    System.IO.FileStream fs= System.IO.File.Create(args[1]);
                    System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(obj.GetType());
                    s.Serialize(fs,obj);
                    fs.Close();
                }
            }
        }
    }
}
