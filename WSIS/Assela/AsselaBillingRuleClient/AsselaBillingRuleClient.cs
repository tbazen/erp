﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment.Assela
{
    public class AsselaBillingRuleClient:SubscriberManagment.Client.IBillingRuleClient
    {
        public AsselaBillingRuleClient()
        {
            var x = new AssselaBillingRate();
        }
        public void showSettingEditor(Type type, object setting)
        {
            AsselaBillingRateEditor form = new AsselaBillingRateEditor(setting as AssselaBillingRate);
            form.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);   
        }

        public System.Resources.ResourceManager getStringTable(int languageID)
        {
            return StringTable_Or.ResourceManager;
        }


        public void printReceipt(CustomerPaymentReceipt receipt, Client.PrintReceiptParameters pars,bool reprint,int nCopy)
        {
            if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["thermal"], StringComparison.CurrentCultureIgnoreCase))
            {
                String ip = System.Configuration.ConfigurationManager.AppSettings["thermal_ip"];
                String port = System.Configuration.ConfigurationManager.AppSettings["thermal_port"];
                int portn;
                if (string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(port) || !int.TryParse(port, out portn))
                    throw new ClientServer.ServerUserMessage("Please confgure thermal printer ip address and port.");
                new ThermalReceiptPrinter(ip, portn).printNow(receipt, pars, reprint, nCopy);
            }
            else
                new ReceiptPrinter(receipt, pars).PrintNow(reprint, nCopy);
        }
    }
}
