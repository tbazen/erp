﻿namespace INTAPS.WSIS.Job.Assela.REClient

{
    partial class ConnectionMaintenanceFormAssela
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkEstimation = new System.Windows.Forms.CheckBox();
            this.checkRelocateMeter = new System.Windows.Forms.CheckBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.applicationPlaceHolder = new INTAPS.WSIS.Job.Client.ApplicationPlaceHolder();
            this.browserExisting = new INTAPS.UI.HTML.ControlBrowser();
            this.pageConnection = new System.Windows.Forms.TabPage();
            this.pageMeter = new System.Windows.Forms.TabPage();
            this.meterEditor = new INTAPS.WSIS.Job.Client.MeterDataPlaceholder();
            this.label10 = new System.Windows.Forms.Label();
            this.labelDMA = new System.Windows.Forms.Label();
            this.groupLocation = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textParcelCode = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textWMX = new System.Windows.Forms.TextBox();
            this.comboKebele = new System.Windows.Forms.ComboBox();
            this.textLandCertifcate = new System.Windows.Forms.TextBox();
            this.comboPressureZone = new System.Windows.Forms.ComboBox();
            this.comboDMA = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textWMY = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pageConnection.SuspendLayout();
            this.pageMeter.SuspendLayout();
            this.groupLocation.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 560);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(888, 47);
            this.panel1.TabIndex = 3;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(700, 6);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(85, 30);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(791, 6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(85, 30);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.pageConnection);
            this.tabControl.Controls.Add(this.pageMeter);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(888, 560);
            this.tabControl.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkEstimation);
            this.tabPage2.Controls.Add(this.checkRelocateMeter);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(880, 534);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Service Detail";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkEstimation
            // 
            this.checkEstimation.AutoSize = true;
            this.checkEstimation.Location = new System.Drawing.Point(6, 29);
            this.checkEstimation.Name = "checkEstimation";
            this.checkEstimation.Size = new System.Drawing.Size(74, 17);
            this.checkEstimation.TabIndex = 0;
            this.checkEstimation.Text = "Estimation\r\n";
            this.checkEstimation.UseVisualStyleBackColor = true;
            this.checkEstimation.CheckedChanged += new System.EventHandler(this.onServiceDetailChanged);
            // 
            // checkRelocateMeter
            // 
            this.checkRelocateMeter.AutoSize = true;
            this.checkRelocateMeter.Location = new System.Drawing.Point(6, 6);
            this.checkRelocateMeter.Name = "checkRelocateMeter";
            this.checkRelocateMeter.Size = new System.Drawing.Size(99, 17);
            this.checkRelocateMeter.TabIndex = 0;
            this.checkRelocateMeter.Text = "Relocate Meter";
            this.checkRelocateMeter.UseVisualStyleBackColor = true;
            this.checkRelocateMeter.CheckedChanged += new System.EventHandler(this.onServiceDetailChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.applicationPlaceHolder);
            this.tabPage1.Controls.Add(this.browserExisting);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(880, 534);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Customer/Connection Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // applicationPlaceHolder
            // 
            this.applicationPlaceHolder.BackColor = System.Drawing.Color.Khaki;
            this.applicationPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.applicationPlaceHolder.Location = new System.Drawing.Point(3, 3);
            this.applicationPlaceHolder.Name = "applicationPlaceHolder";
            this.applicationPlaceHolder.Size = new System.Drawing.Size(874, 528);
            this.applicationPlaceHolder.TabIndex = 1;
            // 
            // browserExisting
            // 
            this.browserExisting.Location = new System.Drawing.Point(3, 179);
            this.browserExisting.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserExisting.Name = "browserExisting";
            this.browserExisting.Size = new System.Drawing.Size(640, 156);
            this.browserExisting.StyleSheetFile = "jobstyle.css";
            this.browserExisting.TabIndex = 0;
            // 
            // pageConnection
            // 
            this.pageConnection.BackColor = System.Drawing.Color.Khaki;
            this.pageConnection.Controls.Add(this.label10);
            this.pageConnection.Controls.Add(this.labelDMA);
            this.pageConnection.Controls.Add(this.groupLocation);
            this.pageConnection.Controls.Add(this.comboPressureZone);
            this.pageConnection.Controls.Add(this.comboDMA);
            this.pageConnection.Location = new System.Drawing.Point(4, 22);
            this.pageConnection.Name = "pageConnection";
            this.pageConnection.Padding = new System.Windows.Forms.Padding(3);
            this.pageConnection.Size = new System.Drawing.Size(880, 534);
            this.pageConnection.TabIndex = 3;
            this.pageConnection.Text = "Connection Information";
            // 
            // pageMeter
            // 
            this.pageMeter.Controls.Add(this.meterEditor);
            this.pageMeter.Location = new System.Drawing.Point(4, 22);
            this.pageMeter.Name = "pageMeter";
            this.pageMeter.Padding = new System.Windows.Forms.Padding(3);
            this.pageMeter.Size = new System.Drawing.Size(880, 534);
            this.pageMeter.TabIndex = 4;
            this.pageMeter.Text = "Meter Page";
            this.pageMeter.UseVisualStyleBackColor = true;
            // 
            // meterEditor
            // 
            this.meterEditor.DataUpdated = true;
            this.meterEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterEditor.Location = new System.Drawing.Point(3, 3);
            this.meterEditor.Name = "meterEditor";
            this.meterEditor.Size = new System.Drawing.Size(874, 528);
            this.meterEditor.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(419, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 15);
            this.label10.TabIndex = 15;
            this.label10.Text = "Pressure Zone:";
            // 
            // labelDMA
            // 
            this.labelDMA.AutoSize = true;
            this.labelDMA.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDMA.Location = new System.Drawing.Point(8, 14);
            this.labelDMA.Name = "labelDMA";
            this.labelDMA.Size = new System.Drawing.Size(39, 15);
            this.labelDMA.TabIndex = 14;
            this.labelDMA.Text = "DMA:";
            // 
            // groupLocation
            // 
            this.groupLocation.Controls.Add(this.label20);
            this.groupLocation.Controls.Add(this.textWMY);
            this.groupLocation.Controls.Add(this.label2);
            this.groupLocation.Controls.Add(this.label19);
            this.groupLocation.Controls.Add(this.label3);
            this.groupLocation.Controls.Add(this.textParcelCode);
            this.groupLocation.Controls.Add(this.label11);
            this.groupLocation.Controls.Add(this.textAddress);
            this.groupLocation.Controls.Add(this.label4);
            this.groupLocation.Controls.Add(this.textWMX);
            this.groupLocation.Controls.Add(this.comboKebele);
            this.groupLocation.Controls.Add(this.textLandCertifcate);
            this.groupLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupLocation.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupLocation.Location = new System.Drawing.Point(8, 45);
            this.groupLocation.Name = "groupLocation";
            this.groupLocation.Padding = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.groupLocation.Size = new System.Drawing.Size(850, 285);
            this.groupLocation.TabIndex = 18;
            this.groupLocation.TabStop = false;
            this.groupLocation.Text = "Location";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Kebele:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 51);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(142, 15);
            this.label19.TabIndex = 0;
            this.label19.Text = "Water Mater Longitude:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(458, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "House No:";
            // 
            // textParcelCode
            // 
            this.textParcelCode.Location = new System.Drawing.Point(602, 81);
            this.textParcelCode.Name = "textParcelCode";
            this.textParcelCode.Size = new System.Drawing.Size(218, 24);
            this.textParcelCode.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(460, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Parcel No:";
            // 
            // textAddress
            // 
            this.textAddress.Location = new System.Drawing.Point(602, 20);
            this.textAddress.Name = "textAddress";
            this.textAddress.Size = new System.Drawing.Size(218, 24);
            this.textAddress.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(458, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Land Certifcate No:";
            // 
            // textWMX
            // 
            this.textWMX.Location = new System.Drawing.Point(168, 42);
            this.textWMX.Name = "textWMX";
            this.textWMX.Size = new System.Drawing.Size(252, 24);
            this.textWMX.TabIndex = 1;
            // 
            // comboKebele
            // 
            this.comboKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebele.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboKebele.FormattingEnabled = true;
            this.comboKebele.Location = new System.Drawing.Point(168, 16);
            this.comboKebele.Name = "comboKebele";
            this.comboKebele.Size = new System.Drawing.Size(252, 23);
            this.comboKebele.TabIndex = 2;
            // 
            // textLandCertifcate
            // 
            this.textLandCertifcate.Location = new System.Drawing.Point(602, 51);
            this.textLandCertifcate.Name = "textLandCertifcate";
            this.textLandCertifcate.Size = new System.Drawing.Size(218, 24);
            this.textLandCertifcate.TabIndex = 1;
            // 
            // comboPressureZone
            // 
            this.comboPressureZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPressureZone.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboPressureZone.FormattingEnabled = true;
            this.comboPressureZone.Location = new System.Drawing.Point(574, 6);
            this.comboPressureZone.Name = "comboPressureZone";
            this.comboPressureZone.Size = new System.Drawing.Size(207, 23);
            this.comboPressureZone.TabIndex = 16;
            // 
            // comboDMA
            // 
            this.comboDMA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDMA.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDMA.FormattingEnabled = true;
            this.comboDMA.Location = new System.Drawing.Point(163, 6);
            this.comboDMA.Name = "comboDMA";
            this.comboDMA.Size = new System.Drawing.Size(207, 23);
            this.comboDMA.TabIndex = 17;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(96, 81);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 15);
            this.label20.TabIndex = 7;
            this.label20.Text = "Lattiude:";
            // 
            // textWMY
            // 
            this.textWMY.Location = new System.Drawing.Point(168, 76);
            this.textWMY.Name = "textWMY";
            this.textWMY.Size = new System.Drawing.Size(252, 24);
            this.textWMY.TabIndex = 8;
            // 
            // ConnectionMaintenanceFormAssela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 607);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "ConnectionMaintenanceFormAssela";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connection Maintenance";
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.pageConnection.ResumeLayout(false);
            this.pageConnection.PerformLayout();
            this.pageMeter.ResumeLayout(false);
            this.groupLocation.ResumeLayout(false);
            this.groupLocation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private UI.HTML.ControlBrowser browserExisting;
        private INTAPS.WSIS.Job.Client.ApplicationPlaceHolder applicationPlaceHolder;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox checkRelocateMeter;
        private System.Windows.Forms.TabPage pageConnection;
        private System.Windows.Forms.CheckBox checkEstimation;
        private System.Windows.Forms.TabPage pageMeter;
        private Client.MeterDataPlaceholder meterEditor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelDMA;
        private System.Windows.Forms.GroupBox groupLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textParcelCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textWMX;
        private System.Windows.Forms.ComboBox comboKebele;
        private System.Windows.Forms.TextBox textLandCertifcate;
        private System.Windows.Forms.ComboBox comboPressureZone;
        private System.Windows.Forms.ComboBox comboDMA;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textWMY;
    }
}