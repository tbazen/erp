using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
namespace INTAPS.WSIS.Job.Assela.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK)]
    public class CustomerSponsoredExpansionClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new CustomerSponsoredExpansionForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            return "";
        }

    }
}
