﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI;
using System.Collections;

namespace INTAPS.WSIS.Job.Assela.REClient
{
    public partial class EstimationConfigurationAssela : Form
    {
        Assela.AsselaEstimationConfiguration config;
        bool changed=false;
        int typeID;
        bool _ignoreEvents = false;
        public EstimationConfigurationAssela()
        {
            InitializeComponent();
            initMeterGrid(gridDeposit);
            loadDepositGrid();
            buttonOk.Enabled = false;
            typeID = JobManagerClient.getJobTypeByClientHandlerType(typeof(NewLineClientAssela));
            config = JobManagerClient.getConfiguration(typeID, typeof(Assela.AsselaEstimationConfiguration)) as Assela.AsselaEstimationConfiguration;
            if (config == null)
            {
                config = new Assela.AsselaEstimationConfiguration();
                onChanged();
            }
            _ignoreEvents = true;
            try
            {
                itemsPipeline.setItems(config.pipelineItems);
                itemsHDP.setItems(config.hdpPipelineItems);
                itemsWaterMeter.setItems(config.waterMeterItems);
                itemsSurvey.setItems(config.surveyItems);
                setGridValues(gridDeposit, config.meterDeposit);
                itemFormFee.values = config.formFee;
                itemEstimationFee.values = config.estimationFee;
                itemTransitCharge.values = config.transitFee;
                itemTechnicalServiceCharge.values = config.technicalServiceMultiplier;
                itemTechnicalServiceChargeTeir1.values = config.technicalServiceMultiplierTeir1;
                itemTechnicalServiceChargeTeir2.values = config.technicalServiceMultiplierTeir2;
                itemTechnicalServiceChargeTeir3.values = config.technicalServiceMultiplierTeir3;
                itemContractCardFee.values = config.contractCardFee;
                itemFileFolderFee.values = config.fileFolderFee;
                itemPremissionFee.values = config.permissionFee;
                itemServiceCharge.values = config.serviceChargeMultiplier;
                itemOwnershipTransfer.values = config.ownershipTransferFee;
            }
            finally
            {
                _ignoreEvents = false;
            }
        }
        AsselaEstimationConfiguration.ItemTypedValue[] getGridValues(DataGridView grid)
        {
            List<AsselaEstimationConfiguration.ItemTypedValue> _ret = new List<AsselaEstimationConfiguration.ItemTypedValue>();
            foreach (DataGridViewRow row in (IEnumerable)grid.Rows)
            {
                AsselaEstimationConfiguration.ItemTypedValue r = new AsselaEstimationConfiguration.ItemTypedValue();
                r.privateItemCode = (string)row.Tag;
                if (row.Cells[1].Value is double)
                {
                    r.privateValue = (double)row.Cells[1].Value;
                    _ret.Add(r);
                }
                if (row.Cells[2].Value is double)
                {
                    r.institutionalValue = (double)row.Cells[2].Value;
                }
                if (r.institutionalValue != 0 || r.privateValue != 0)
                    _ret.Add(r);
            }
            return _ret.ToArray();
        }
        void setGridValues(DataGridView grid, AsselaEstimationConfiguration.ItemTypedValue[] values)
        {
            foreach (DataGridViewRow row in (IEnumerable)grid.Rows)
            {
                string code = (string)row.Tag;
                for (int i = 0; i < values.Length; i++)
                {
                    if (code.Equals(values[i].privateItemCode))
                    {
                        if (values[i].privateValue != 0)
                            row.Cells[1].Value = values[i].privateValue;
                        if (values[i].institutionalValue != 0)
                            row.Cells[2].Value = values[i].institutionalValue;
                        break;
                    }
                }
            }
        }
        private void initMeterGrid(DataGridView grid)
        {
            int systemParameter = (int)SubscriberManagmentClient.GetSystemParameter("meterMaterialCategory");
            grid.Columns[1].ValueType = typeof(double);
            grid.Columns[2].ValueType = typeof(double);
            if (systemParameter > 1)
            {
                try
                {
                    BIZNET.iERP.TransactionItems[] descriptionArray = BIZNET.iERP.Client.iERPTransactionClient.GetItemsInCategory(systemParameter);
                    if (descriptionArray.Length == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No meter types found.");
                    }
                    foreach (BIZNET.iERP.TransactionItems description in descriptionArray)
                    {
                        object[] values = new object[3];
                        values[0] = description.Name;
                        grid.Rows[grid.Rows.Add(values)].Tag = description.Code;
                    }
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error loading meter types.", exception);
                }
            }
        }
        private void onChanged()
        {
            if (_ignoreEvents)
                return;
            buttonOk.Enabled = true;
            changed = true;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (!changed)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Nothing changed");
                return;
            }
            try
            {
                config.pipelineItems = itemsPipeline.getItems();
                config.hdpPipelineItems = itemsHDP.getItems();
                config.waterMeterItems = itemsWaterMeter.getItems();
                config.surveyItems = itemsSurvey.getItems();
                config.meterDeposit=getGridValues(gridDeposit);

                config.formFee = itemFormFee.values;
                config.estimationFee=itemEstimationFee.values;
                config.transitFee=itemTransitCharge.values;
                config.technicalServiceMultiplier = itemTechnicalServiceCharge.values;
                config.technicalServiceMultiplierTeir1 = itemTechnicalServiceChargeTeir1.values;
                config.technicalServiceMultiplierTeir2 = itemTechnicalServiceChargeTeir2.values;
                config.technicalServiceMultiplierTeir3 = itemTechnicalServiceChargeTeir3.values;
                config.contractCardFee=itemContractCardFee.values;
                config.fileFolderFee = itemFileFolderFee.values;
                config.permissionFee=itemPremissionFee.values;
                config.serviceChargeMultiplier=itemServiceCharge.values;
                config.ownershipTransferFee = itemOwnershipTransfer.values;
                JobManagerClient.saveConfiguration(typeID, config);
                changed = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        
        private void loadDepositGrid()
        {
            int systemParameter = (int)SubscriberManagmentClient.GetSystemParameter("meterMaterialCategory");
            this.gridDeposit.Columns[1].ValueType = typeof(double);
            if (systemParameter > 1)
            {
                try
                {
                    BIZNET.iERP.TransactionItems[] descriptionArray = BIZNET.iERP.Client.iERPTransactionClient.GetItemsInCategory(systemParameter);
                    if (descriptionArray.Length == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No meter types found.");
                    }
                    foreach (BIZNET.iERP.TransactionItems description in descriptionArray)
                    {
                        object[] values = new object[2];
                        values[0] = description.Name;
                        this.gridDeposit.Rows[this.gridDeposit.Rows.Add(values)].Tag = description.Code;
                    }
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error loading meter types.", exception);
                }
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (changed)
                e.Cancel = !INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Do you want to close without saving the changes?");
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void itemsPipeline_Changed(object sender, EventArgs e)
        {
            onChanged();
        }

        private void progressiveRateCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            onChanged();
        }

        private void textCostFactor_TextChanged(object sender, EventArgs e)
        {
            onChanged();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void itemLastMultiplier_ObjectChanged(object sender, EventArgs e)
        {
            onChanged();
        }

        private void itemFormFee_changed(object sender, EventArgs e)
        {
            onChanged();
        }
    }
}
