using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Assela;
namespace INTAPS.WSIS.Job.Assela.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_MAINTENANCE, "Existing Connection Service",priority=1)]
    public class RESConnectionMaintenanceAssela : RESTechnicalServiceAssela<ConntectionMaintenanceAsselaData>, IJobRuleServerHandler
    {
        public RESConnectionMaintenanceAssela(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            Subscription subsc;
            if ((subsc=bde.subscriberBDE.GetSubscription(((ConntectionMaintenanceAsselaData)data).connectionID, date.Ticks)).subscriptionStatus == SubscriptionStatus.Discontinued)
                throw new ServerUserMessage("This connection is discontinued");
            base.createApplication(AID, date, job, worker, data, note);
        }
        
        
        public override int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            ConntectionMaintenanceAsselaData data;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ConntectionMaintenanceAsselaData;
                    if(data.hasApplicationFee)
                        return new int[] { StandardJobStatus.APPLICATION_PAYMENT, StandardJobStatus.CANCELED };
                    else
                        return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_PAYMENT:
                    data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ConntectionMaintenanceAsselaData;
                    if(data.estimation)
                        return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.SURVEY:
                    return new int[] { StandardJobStatus.ANALYSIS, StandardJobStatus.CANCELED };
                case StandardJobStatus.ANALYSIS:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINISHED:
                    return new int[] { };
                default:
                    return new int[0];
            }
        }

        protected override void onForwardToApplicationPaymet(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, AsselaEstimationConfiguration config)
        {
            ConntectionMaintenanceAsselaData data = base.checkAndGetData(job);

            Subscriber customer = bde.getJobCustomer(job);
            JobBillOfMaterial appPaymentBOM = new JobBillOfMaterial();
            appPaymentBOM.jobID = job.id;
            appPaymentBOM.analysisTime = DateTime.Now;
            appPaymentBOM.note = "Application Payment";
            List<JobBOMItem> items = new List<JobBOMItem>();
            if (data.estimation)
                items.AddRange(new JobBOMItem[]{
                        new JobBOMItem(){
                            description="Application Form",
                             itemID=config.formFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.formFee.getValue(customer.subscriberType),
                             calculated=true
                        },new JobBOMItem(){
                            description="Estimation",
                             itemID=config.estimationFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.estimationFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        
                    });
           

            if (data.relocateMeter)
            {
                items.Add(
                        new JobBOMItem(){
                            description="Permission",
                             itemID=config.permissionFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                            unitPrice = config.permissionFee.getValue(customer.subscriberType),
                             calculated=true
                        }
                    );
            }
            _ignoreAID = AID;
            appPaymentBOM.items = items.ToArray();
            appPaymentBOM.id = bde.SetBillofMateial(AID, appPaymentBOM, true);
            bde.generateInvoice(AID, appPaymentBOM.id, worker);
        }
        protected override void onForwardToFinalization(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, AsselaEstimationConfiguration config)
        {
            checkPayment(job.id);
        }
        public override void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            ConntectionMaintenanceAsselaData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            ReadingEntryClerk clerk = bde.subscriberBDE.BWFGetClerkByUserID(worker.userID);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    if (data.changeMeter)
                    {
                        BillPeriod period = bde.subscriberBDE.getPeriodForDate(date);
                        if (period == null)
                            throw new ServerUserMessage("Bill period not defined for time " + date);
                        ReadingReset reset = new ReadingReset();
                        reset.connectionID = data.connectionID;
                        reset.periodID = period.id;
                        reset.reading = data.updatedMeterData.initialMeterReading;
                        reset.ticks = date.Ticks;
                        bde.subscriberBDE.setReadingReset(AID, clerk, reset);
                        bde.subscriberBDE.ChangeSubcriberMeter(AID, data.connectionID, date, data.updatedMeterData);
                    }
                    if (data.relocateMeter)
                    {
                        if (data.updatedSubscription!=null)
                            bde.subscriberBDE.changeSubscription(AID, data.updatedSubscription);
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override void onBOMSet(int AID, JobData job, JobBillOfMaterial bom)
        {
            if (AID == _ignoreAID)
                return;
            ConntectionMaintenanceAsselaData data = bde.getWorkFlowData(job.id, this.getTypeID(), 0, true) as ConntectionMaintenanceAsselaData;
            List<JobBOMItem> meters = getMeterItems(new JobBillOfMaterial[] { bom });
            if (meters.Count > 0)
            {
                if (meters.Count != 1 || meters[0].quantity != 1)
                    throw new ServerUserMessage("Only one meter item should be included in the bill of quantity");
                if (data.updatedMeterData== null)
                    data.updatedMeterData = new MeterData();
                data.updatedMeterData.itemCode = meters[0].itemID;
                data.changeMeter = true;
                bde.setWorkFlowData(AID, data);
            }
            base.onBOMSet(AID, job, bom);
        }
        public override void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ConntectionMaintenanceAsselaData wfData = data as ConntectionMaintenanceAsselaData;
            if (wfData == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfData.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                case StandardJobStatus.SURVEY:
                case StandardJobStatus.ANALYSIS:
                    break;
                case StandardJobStatus.FINALIZATION:                    
                    if (wfData.relocateMeter || wfData.changeMeter)
                        break;
                    else
                        throw new ServerUserMessage("It is not allowed to modify new line data at this stage");
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify new line data at this stage");

            }
            if (!wfData.changeMeter)
                wfData.updatedMeterData = null;
            if (wfData.relocateMeter)
            {
                Subscription existingSubscription = bde.subscriberBDE.GetSubscription(wfData.connectionID, date.Ticks);
                if (existingSubscription == null)
                    throw new ServerUserMessage("There is no subscription for ID:{0} and time:{1}", wfData.connectionID, date);
                wfData.estimation = true;
                if (wfData.updatedSubscription != null)
                {
                    existingSubscription.Kebele = wfData.updatedSubscription.Kebele;
                    existingSubscription.address = wfData.updatedSubscription.address;
                    existingSubscription.waterMeterX = wfData.updatedSubscription.waterMeterX;
                    existingSubscription.waterMeterY = wfData.updatedSubscription.waterMeterY;
                    wfData.updatedSubscription = existingSubscription;
                }
            }
            else
                wfData.updatedSubscription = null;

            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
    }
}
