﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace ApiTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        EKS.IEksAPI GetEKSApi(string eksFirm)
        {
            string asm = "Baylan.EKSAPI";
            string assemblyName = AppDomain.CurrentDomain.BaseDirectory + @"libs\Baylan\" + asm + ".dll";
            if (eksFirm == "baylan")
            {
                asm = "Baylan.EKSAPI";
                assemblyName = AppDomain.CurrentDomain.BaseDirectory + @"libs\Baylan\" + asm + ".dll";

            }
            else if (eksFirm == "elektromed")
            {
                asm = "Elektromed.EksAPI";
                assemblyName = AppDomain.CurrentDomain.BaseDirectory + @"libs\Elektromed\Elektromed.dll";

            }
            Assembly assemblyInstance = Assembly.LoadFrom(assemblyName);
            object t = assemblyInstance.CreateInstance(asm, true, BindingFlags.Public | BindingFlags.InvokeMethod | BindingFlags.Instance, null, null, null, null);
            var eksApi = t as EKS.IEksAPI;
            return eksApi;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            EKS.IEksAPI eksApi = GetEKSApi("baylan");

            if (eksApi.SayacModeliId == 0 || eksApi.SayacModeliId == 255)
            {
                eksApi.SayacModeliId = 1;

            }
            //for (int i = 0; i < 20; i++)
            //{
            //    if (eksApi.KartOku())
            //    {
            //        textBox1.AppendText(i + " - Okuma Basarili, Abone :" + eksApi.KartBilgisi.AboneNo + Environment.NewLine);
            //    }
            //    else
            //    {
            //        textBox1.AppendText("Hata Olustu:" + eksApi.SonHataMesaji);
            //    }
            //}
            if (eksApi.KartOku())
            {
                if (eksApi.KrediYukle(1, 0))
                {
                    if (eksApi.KartOku())
                    {
                        textBox1.AppendText(" kredi :" + eksApi.KartBilgisi.Kredi + Environment.NewLine + " yedek kredi :" + eksApi.KartBilgisi.YedekKredi);
                    }
                }
                else
                    textBox1.AppendText(" Hata :" + eksApi.SonHataMesaji + Environment.NewLine);
            }
            else
                textBox1.AppendText(" Hata :" + eksApi.SonHataMesaji + Environment.NewLine);

        }
    }
}
