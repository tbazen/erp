﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using ApiTest.Properties;
using Baylan.TranslationLibrary;

namespace ApiTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string [] args)
        {
            #region CheckCommandLineArgs
            // Reset application settings and save settings
            foreach (string str in args)
            {              
                if (str.Contains("resetApplicationSettings"))
                {
                    staticClass.settings.languageSetting = languages.turkce;                 
                    staticClass.saveSettings(); 
                    MessageBox.Show(localize.getStr("defaultSettingsLoaded")) ; 
                    staticClass.log(localize.getStr("defaultSettingsLoaded"), priority.error) ; 
                }             
            }
            #endregion CheckCommandLineArgs                    

            #region Check sqlLiteDB                       
            /*
            using (sqLiteMgr mgr = new sqLiteMgr(Directory.GetCurrentDirectory()))
            {
                if (!mgr.try2Connect())
                {
                    MessageBox.Show(localize.getStr("pleaseElevateTheUserPrivilages"), 
                                    localize.getStr("baylanWaterMeters")); 
                    Application.Exit(); 
                }
            }*/
            #endregion Check sqlLiteDB

            staticClass.visualExceptionHandler += staticClass_visualExceptionHandler;            
            // Deserialize settings if exists            
            if (staticClass.readSettings())
            {
                localize.selectedLng = staticClass.settings.languageSetting;
                staticClass.settings.settingsFound = true;
                staticClass.readUsers();
            }
            else
            {
                localize.selectedLng = languages.turkce;
                staticClass.settings.settingsFound = false;
            }

            staticClass.log(localize.getStr("applicationInitialized"), priority.init);       

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            //Application.Run(new Form1());
            //return;
            switch (staticClass.settings.settingsFound)
            {
                case true :
                                if (staticClass.userList.Count > 0)
                                {
                                    Application.Run(new pwdWithUserFrm());
                                }
                                else
                                {
                                    Application.Run(new pwdFrm());
                                }                           
                    break;
                case false :
                    Application.Run(new pwdFrm());
                    break;
                default :
                    Application.Run(new pwdFrm());
                    break;
            }           
        }

        static void staticClass_visualExceptionHandler(object source)
        {
           // MessageBox.Show("Visual alert"); 
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(((Exception)e.ExceptionObject).Message + " " + ((Exception)e.ExceptionObject).StackTrace);
        }


        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(((Exception)e.Exception).Message + " " + ((Exception)e.Exception).StackTrace);            
        }
    }
}
