﻿namespace ApiTest
{
    partial class Utilities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxTBEncryptString = new System.Windows.Forms.TextBox();
            this.uxBTNEncrypt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uxTBEncryptResult = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uxTBDecryptedText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // uxTBEncryptString
            // 
            this.uxTBEncryptString.Location = new System.Drawing.Point(13, 42);
            this.uxTBEncryptString.Name = "uxTBEncryptString";
            this.uxTBEncryptString.Size = new System.Drawing.Size(420, 20);
            this.uxTBEncryptString.TabIndex = 0;
            // 
            // uxBTNEncrypt
            // 
            this.uxBTNEncrypt.Location = new System.Drawing.Point(439, 42);
            this.uxBTNEncrypt.Name = "uxBTNEncrypt";
            this.uxBTNEncrypt.Size = new System.Drawing.Size(75, 23);
            this.uxBTNEncrypt.TabIndex = 1;
            this.uxBTNEncrypt.Text = "Encrypt";
            this.uxBTNEncrypt.UseVisualStyleBackColor = true;
            this.uxBTNEncrypt.Click += new System.EventHandler(this.uxBTNEncrypt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "String To Encrypt :";
            // 
            // uxTBEncryptResult
            // 
            this.uxTBEncryptResult.Location = new System.Drawing.Point(13, 84);
            this.uxTBEncryptResult.Multiline = true;
            this.uxTBEncryptResult.Name = "uxTBEncryptResult";
            this.uxTBEncryptResult.ReadOnly = true;
            this.uxTBEncryptResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.uxTBEncryptResult.Size = new System.Drawing.Size(420, 46);
            this.uxTBEncryptResult.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Encrypted Text :";
            // 
            // uxTBDecryptedText
            // 
            this.uxTBDecryptedText.Location = new System.Drawing.Point(13, 155);
            this.uxTBDecryptedText.Name = "uxTBDecryptedText";
            this.uxTBDecryptedText.ReadOnly = true;
            this.uxTBDecryptedText.Size = new System.Drawing.Size(420, 20);
            this.uxTBDecryptedText.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Decrypted Text :";
            // 
            // Utilities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 262);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.uxTBDecryptedText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.uxTBEncryptResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.uxBTNEncrypt);
            this.Controls.Add(this.uxTBEncryptString);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Utilities";
            this.Text = "Utilities";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox uxTBEncryptString;
        private System.Windows.Forms.Button uxBTNEncrypt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox uxTBEncryptResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox uxTBDecryptedText;
        private System.Windows.Forms.Label label3;
    }
}