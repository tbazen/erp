﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ApiTest
{
    public partial class Utilities : Form
    {
        public Utilities()
        {
            InitializeComponent();
        }

        private void uxBTNEncrypt_Click(object sender, EventArgs e)
        {
            if (uxTBEncryptString.Text == "")
            {
                MessageBox.Show("Sifrelenecek metin giriniz!");
                uxTBEncryptString.Focus();
                return;
            }
            uxTBEncryptResult.Text = Baylan.Parsers.Library.Encrypt(uxTBEncryptString.Text, false);
            uxTBDecryptedText.Text = Baylan.Parsers.Library.Decrypt(uxTBEncryptResult.Text, false);
        }
    }
}
