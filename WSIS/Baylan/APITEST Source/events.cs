﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiTest
{
    public static class clStatic
    {
        public static quitEvent qEvent;
        static clStatic()   // construct
        {
            qEvent = new quitEvent();
        }
    }
    public class quitEvent
    {
        public event EventHandler ehQuitEvent;
        /// <summary>
        /// A valid detection has occured.
        /// </summary>
        private bool quitEventField;
        public bool quitEventProp
        {
            get { return quitEventField; }
            set
            {
                quitEventField = value;
                ehQuitEvent(this, new EventArgs());
            }
        }
    }  
}
