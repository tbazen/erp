﻿using System.Globalization;
namespace ApiTest
{
    partial class mainFrm
    {
        //CultureInfo currentCulture = System.Threading.Thread.CurrentThread.CurrentUICulture;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainFrm));
            this.btRead = new System.Windows.Forms.Button();
            this.txOutput = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.TB_SayacKredi = new System.Windows.Forms.TextBox();
            this.uxBTNEjectCrtReader = new System.Windows.Forms.Button();
            this.uxCBIsiSayaci = new System.Windows.Forms.CheckBox();
            this.uxPNLImageContainer = new System.Windows.Forms.Panel();
            this.uxCBLongAboneNo = new System.Windows.Forms.CheckBox();
            this.btQueryWaterworks = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtKademe6 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.uxIadeEdilecekKredi = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTarife4 = new System.Windows.Forms.TextBox();
            this.txtTarife3 = new System.Windows.Forms.TextBox();
            this.txtKademe4 = new System.Windows.Forms.TextBox();
            this.txtTarife1 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpLoadCredits = new System.Windows.Forms.TabPage();
            this.uxDTPMakbuzNo = new System.Windows.Forms.DateTimePicker();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.uxTBKritikKredi = new System.Windows.Forms.TextBox();
            this.uxLBLAvansKredi = new System.Windows.Forms.Label();
            this.uxTBAvansKredi = new System.Windows.Forms.TextBox();
            this.uxBTNSilVeYukle = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.uxLBYedekKredi = new System.Windows.Forms.Label();
            this.btnKontorSil = new System.Windows.Forms.Button();
            this.txtYedekKredi = new System.Windows.Forms.TextBox();
            this.btnKontorEkle = new System.Windows.Forms.Button();
            this.uxLBAnaKredi = new System.Windows.Forms.Label();
            this.txtAnaKredi = new System.Windows.Forms.TextBox();
            this.tpTest = new System.Windows.Forms.TabPage();
            this.txCardHealth = new System.Windows.Forms.TextBox();
            this.testContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearMenuToolstrip = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultsToolstrip = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.uxRepatedOperationType1 = new System.Windows.Forms.ComboBox();
            this.uxRepatedOperationType2 = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.uxOkumaAdedi = new System.Windows.Forms.NumericUpDown();
            this.btTestCard = new System.Windows.Forms.Button();
            this.tpParameters1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.uxCBDSIKapatma = new System.Windows.Forms.CheckBox();
            this.uxSayacKapatmaTarihi = new System.Windows.Forms.DateTimePicker();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cmpYanginModuSaat = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.cmpYanginModuDakika = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmpKesintisizBitis = new System.Windows.Forms.DateTimePicker();
            this.uxCBSurekli = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cmpKesintisizBaslangic = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.cmpMaxDebiEsikDegeri = new System.Windows.Forms.TextBox();
            this.cmpSayacCapi = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tpParameters2 = new System.Windows.Forms.TabPage();
            this.uxCBZeroTariffs = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.uxBTNCopyCardToClipboard = new System.Windows.Forms.Button();
            this.uxKartStatus = new System.Windows.Forms.TextBox();
            this.uxSendAsByte = new System.Windows.Forms.CheckBox();
            this.uxAK21KartStatus = new System.Windows.Forms.GroupBox();
            this.uxAK21PilKapagiCezasi = new System.Windows.Forms.CheckBox();
            this.uxAK21UstKapakCeza = new System.Windows.Forms.CheckBox();
            this.uxAK21ManyetikCeza = new System.Windows.Forms.CheckBox();
            this.uxAK21RekorCeza = new System.Windows.Forms.CheckBox();
            this.uxAK21HaftaSonuMesaiSayacAcik = new System.Windows.Forms.CheckBox();
            this.uxAK21ResmiTatilVanaAcik = new System.Windows.Forms.CheckBox();
            this.uxAK21TarihdeVanaKapat = new System.Windows.Forms.CheckBox();
            this.uxAK21KesintisizSu = new System.Windows.Forms.CheckBox();
            this.uxAK11KartStatus = new System.Windows.Forms.GroupBox();
            this.uxAK11HaftaSonuMesaiDisiVanaAcik = new System.Windows.Forms.CheckBox();
            this.uxAK11PilUzat = new System.Windows.Forms.CheckBox();
            this.uxAK11AydaIkiVanaAcmaKapama = new System.Windows.Forms.CheckBox();
            this.uxAK11PilKisalt = new System.Windows.Forms.CheckBox();
            this.uxTPParameters3 = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.uxLBLFreq2 = new System.Windows.Forms.Label();
            this.uxLBLFreq1 = new System.Windows.Forms.Label();
            this.uxLBLFreq0 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.uxTBFreq3 = new System.Windows.Forms.TextBox();
            this.uxTBFreq1 = new System.Windows.Forms.TextBox();
            this.uxTBFreq0 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uxLBLTrackBarValue = new System.Windows.Forms.Label();
            this.uxLBLTimeType = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.uxTBTimeData = new System.Windows.Forms.TrackBar();
            this.uxCBTimeType = new System.Windows.Forms.ComboBox();
            this.tpUsers = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btSaveUserList = new System.Windows.Forms.Button();
            this.btDeleteUser = new System.Windows.Forms.Button();
            this.btAddNewUser = new System.Windows.Forms.Button();
            this.lstKullanicilar = new System.Windows.Forms.ListBox();
            this.propGrid = new System.Windows.Forms.PropertyGrid();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.uxGBRFParametreler = new System.Windows.Forms.GroupBox();
            this.label56 = new System.Windows.Forms.Label();
            this.uxTBRFFreq2 = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.uxTBRFFreq1 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.uxTBRFFreq0 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.uxTBRFSure = new System.Windows.Forms.TextBox();
            this.uxCBRFZamanTipi = new System.Windows.Forms.ComboBox();
            this.uxRBRFPasif = new System.Windows.Forms.RadioButton();
            this.uxRBRFAktif = new System.Windows.Forms.RadioButton();
            this.uxCBRFTipi = new System.Windows.Forms.ComboBox();
            this.label52 = new System.Windows.Forms.Label();
            this.uxGBHygenicValveUsage = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.uxTBoxVanaAcilmaSuresi = new System.Windows.Forms.TextBox();
            this.uxAK11IkincilFonksiyonlar = new System.Windows.Forms.CheckBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.uxTBoxSuTuketimMiktari = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.uxTBoxSuTuketimSuresi = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.cbLanguage = new System.Windows.Forms.ComboBox();
            this.uxTPSubAuth = new System.Windows.Forms.TabPage();
            this.uxBTNCreateSubAuthCard = new System.Windows.Forms.Button();
            this.uxNUDAuthNo = new System.Windows.Forms.NumericUpDown();
            this.uxTBAuthNo = new System.Windows.Forms.TrackBar();
            this.label58 = new System.Windows.Forms.Label();
            this.uxTBIdareAdi = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.uxBTNOpenCreditTransaction = new System.Windows.Forms.Button();
            this.uxBTNConvertMeterType = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.uxLBLCurrentMeterType = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.uxCBNewMeterType = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uxDonem = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.uxBTNDec10m3 = new System.Windows.Forms.Button();
            this.BTNLoadNewCardReader = new System.Windows.Forms.Button();
            this.txtTarife2 = new System.Windows.Forms.TextBox();
            this.txtKademe5 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btSeriNoOku = new System.Windows.Forms.Button();
            this.TB_KartAnaKredi = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.TB_KartYedekKredi = new System.Windows.Forms.TextBox();
            this.DTPSayacTarihi = new System.Windows.Forms.DateTimePicker();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.CMBAboneTipi = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtTarife7 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTarife6 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtTarife5 = new System.Windows.Forms.TextBox();
            this.btIade = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.CBSayacModeli = new System.Windows.Forms.ComboBox();
            this.btnSil = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.cmpKritikKredi = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKademe3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtKademe2 = new System.Windows.Forms.TextBox();
            this.btWrite = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKademe1 = new System.Windows.Forms.TextBox();
            this.txtAboneTipi = new System.Windows.Forms.TextBox();
            this.txtSayacNo = new System.Windows.Forms.TextBox();
            this.txtAboneNo = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.label37 = new System.Windows.Forms.Label();
            this.TB_SayacTuketim = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpLoadCredits.SuspendLayout();
            this.tpTest.SuspendLayout();
            this.testContextMenu.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxOkumaAdedi)).BeginInit();
            this.tpParameters1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmpYanginModuSaat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmpYanginModuDakika)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tpParameters2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.uxAK21KartStatus.SuspendLayout();
            this.uxAK11KartStatus.SuspendLayout();
            this.uxTPParameters3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxTBTimeData)).BeginInit();
            this.tpUsers.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.uxGBRFParametreler.SuspendLayout();
            this.uxGBHygenicValveUsage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.uxTPSubAuth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxNUDAuthNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uxTBAuthNo)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btRead
            // 
            this.btRead.BackColor = System.Drawing.Color.DodgerBlue;
            this.btRead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRead.ForeColor = System.Drawing.Color.White;
            this.btRead.Location = new System.Drawing.Point(4, 4);
            this.btRead.Name = "btRead";
            this.btRead.Size = new System.Drawing.Size(83, 23);
            this.btRead.TabIndex = 0;
            this.btRead.Text = "Oku";
            this.btRead.UseVisualStyleBackColor = false;
            this.btRead.Click += new System.EventHandler(this.btRead_Click);
            // 
            // txOutput
            // 
            this.txOutput.BackColor = System.Drawing.Color.GhostWhite;
            this.txOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txOutput.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txOutput.ForeColor = System.Drawing.Color.RoyalBlue;
            this.txOutput.Location = new System.Drawing.Point(0, 0);
            this.txOutput.Multiline = true;
            this.txOutput.Name = "txOutput";
            this.txOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txOutput.Size = new System.Drawing.Size(579, 606);
            this.txOutput.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.TB_SayacTuketim);
            this.panel1.Controls.Add(this.label63);
            this.panel1.Controls.Add(this.TB_SayacKredi);
            this.panel1.Controls.Add(this.uxBTNEjectCrtReader);
            this.panel1.Controls.Add(this.uxCBIsiSayaci);
            this.panel1.Controls.Add(this.uxPNLImageContainer);
            this.panel1.Controls.Add(this.uxCBLongAboneNo);
            this.panel1.Controls.Add(this.btQueryWaterworks);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtKademe6);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.txtTarife4);
            this.panel1.Controls.Add(this.txtTarife3);
            this.panel1.Controls.Add(this.txtKademe4);
            this.panel1.Controls.Add(this.txtTarife1);
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.uxDonem);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.uxBTNDec10m3);
            this.panel1.Controls.Add(this.BTNLoadNewCardReader);
            this.panel1.Controls.Add(this.txtTarife2);
            this.panel1.Controls.Add(this.txtKademe5);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.btSeriNoOku);
            this.panel1.Controls.Add(this.TB_KartAnaKredi);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.TB_KartYedekKredi);
            this.panel1.Controls.Add(this.DTPSayacTarihi);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.CMBAboneTipi);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.txtTarife7);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.txtTarife6);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.txtTarife5);
            this.panel1.Controls.Add(this.btIade);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.CBSayacModeli);
            this.panel1.Controls.Add(this.btnSil);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.cmpKritikKredi);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtKademe3);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtKademe2);
            this.panel1.Controls.Add(this.btWrite);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtKademe1);
            this.panel1.Controls.Add(this.txtAboneTipi);
            this.panel1.Controls.Add(this.txtSayacNo);
            this.panel1.Controls.Add(this.txtAboneNo);
            this.panel1.Controls.Add(this.btRead);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(617, 629);
            this.panel1.TabIndex = 2;
            // 
            // label63
            // 
            this.label63.Location = new System.Drawing.Point(8, 286);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(93, 13);
            this.label63.TabIndex = 82;
            this.label63.Text = "Sayaç Kredi :";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_SayacKredi
            // 
            this.TB_SayacKredi.Location = new System.Drawing.Point(104, 283);
            this.TB_SayacKredi.Name = "TB_SayacKredi";
            this.TB_SayacKredi.Size = new System.Drawing.Size(79, 20);
            this.TB_SayacKredi.TabIndex = 81;
            this.TB_SayacKredi.Text = "0";
            // 
            // uxBTNEjectCrtReader
            // 
            this.uxBTNEjectCrtReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBTNEjectCrtReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.uxBTNEjectCrtReader.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.uxBTNEjectCrtReader.Location = new System.Drawing.Point(510, 299);
            this.uxBTNEjectCrtReader.Name = "uxBTNEjectCrtReader";
            this.uxBTNEjectCrtReader.Size = new System.Drawing.Size(101, 23);
            this.uxBTNEjectCrtReader.TabIndex = 80;
            this.uxBTNEjectCrtReader.Text = "Kartı Çıkart";
            this.uxBTNEjectCrtReader.UseVisualStyleBackColor = true;
            this.uxBTNEjectCrtReader.Click += new System.EventHandler(this.uxBTNEjectCrtReader_Click);
            // 
            // uxCBIsiSayaci
            // 
            this.uxCBIsiSayaci.AutoSize = true;
            this.uxCBIsiSayaci.Location = new System.Drawing.Point(430, 53);
            this.uxCBIsiSayaci.Name = "uxCBIsiSayaci";
            this.uxCBIsiSayaci.Size = new System.Drawing.Size(45, 17);
            this.uxCBIsiSayaci.TabIndex = 79;
            this.uxCBIsiSayaci.Text = "ISI?";
            this.uxCBIsiSayaci.UseVisualStyleBackColor = true;
            this.uxCBIsiSayaci.CheckedChanged += new System.EventHandler(this.uxCBIsiSayaci_CheckedChanged);
            // 
            // uxPNLImageContainer
            // 
            this.uxPNLImageContainer.Location = new System.Drawing.Point(510, 4);
            this.uxPNLImageContainer.Name = "uxPNLImageContainer";
            this.uxPNLImageContainer.Size = new System.Drawing.Size(101, 289);
            this.uxPNLImageContainer.TabIndex = 78;
            // 
            // uxCBLongAboneNo
            // 
            this.uxCBLongAboneNo.AutoSize = true;
            this.uxCBLongAboneNo.Location = new System.Drawing.Point(243, 35);
            this.uxCBLongAboneNo.Name = "uxCBLongAboneNo";
            this.uxCBLongAboneNo.Size = new System.Drawing.Size(32, 17);
            this.uxCBLongAboneNo.TabIndex = 77;
            this.uxCBLongAboneNo.Text = "L";
            this.uxCBLongAboneNo.UseVisualStyleBackColor = true;
            this.uxCBLongAboneNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainFrm_KeyDown);
            // 
            // btQueryWaterworks
            // 
            this.btQueryWaterworks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btQueryWaterworks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btQueryWaterworks.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btQueryWaterworks.Location = new System.Drawing.Point(439, 29);
            this.btQueryWaterworks.Name = "btQueryWaterworks";
            this.btQueryWaterworks.Size = new System.Drawing.Size(64, 23);
            this.btQueryWaterworks.TabIndex = 7;
            this.btQueryWaterworks.Text = "İdare?";
            this.btQueryWaterworks.UseVisualStyleBackColor = true;
            this.btQueryWaterworks.Click += new System.EventHandler(this.btQueryWaterworks_Click);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(247, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Tarife 3:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKademe6
            // 
            this.txtKademe6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtKademe6.Location = new System.Drawing.Point(104, 196);
            this.txtKademe6.Name = "txtKademe6";
            this.txtKademe6.Size = new System.Drawing.Size(65, 20);
            this.txtKademe6.TabIndex = 16;
            this.txtKademe6.Text = "969";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox4.Controls.Add(this.uxIadeEdilecekKredi);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Location = new System.Drawing.Point(242, 266);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(244, 47);
            this.groupBox4.TabIndex = 76;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "İade İşlemi";
            // 
            // uxIadeEdilecekKredi
            // 
            this.uxIadeEdilecekKredi.Location = new System.Drawing.Point(178, 15);
            this.uxIadeEdilecekKredi.Name = "uxIadeEdilecekKredi";
            this.uxIadeEdilecekKredi.Size = new System.Drawing.Size(45, 20);
            this.uxIadeEdilecekKredi.TabIndex = 0;
            this.uxIadeEdilecekKredi.Text = "0";
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(17, 19);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(157, 13);
            this.label36.TabIndex = 59;
            this.label36.Text = "Iade edilecek Kredi:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTarife4
            // 
            this.txtTarife4.Location = new System.Drawing.Point(352, 156);
            this.txtTarife4.Name = "txtTarife4";
            this.txtTarife4.Size = new System.Drawing.Size(138, 20);
            this.txtTarife4.TabIndex = 24;
            this.txtTarife4.Text = "1";
            // 
            // txtTarife3
            // 
            this.txtTarife3.Location = new System.Drawing.Point(352, 135);
            this.txtTarife3.Name = "txtTarife3";
            this.txtTarife3.Size = new System.Drawing.Size(138, 20);
            this.txtTarife3.TabIndex = 23;
            this.txtTarife3.Text = "1";
            // 
            // txtKademe4
            // 
            this.txtKademe4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtKademe4.Location = new System.Drawing.Point(104, 156);
            this.txtKademe4.Name = "txtKademe4";
            this.txtKademe4.Size = new System.Drawing.Size(65, 20);
            this.txtKademe4.TabIndex = 14;
            this.txtKademe4.Text = "969";
            // 
            // txtTarife1
            // 
            this.txtTarife1.Location = new System.Drawing.Point(352, 95);
            this.txtTarife1.Name = "txtTarife1";
            this.txtTarife1.Size = new System.Drawing.Size(138, 20);
            this.txtTarife1.TabIndex = 21;
            this.txtTarife1.Text = "1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpLoadCredits);
            this.tabControl1.Controls.Add(this.tpTest);
            this.tabControl1.Controls.Add(this.tpParameters1);
            this.tabControl1.Controls.Add(this.tpParameters2);
            this.tabControl1.Controls.Add(this.uxTPParameters3);
            this.tabControl1.Controls.Add(this.tpUsers);
            this.tabControl1.Controls.Add(this.tpSettings);
            this.tabControl1.Controls.Add(this.uxTPSubAuth);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 339);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(617, 290);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpLoadCredits
            // 
            this.tpLoadCredits.Controls.Add(this.uxDTPMakbuzNo);
            this.tpLoadCredits.Controls.Add(this.label62);
            this.tpLoadCredits.Controls.Add(this.label61);
            this.tpLoadCredits.Controls.Add(this.uxTBKritikKredi);
            this.tpLoadCredits.Controls.Add(this.uxLBLAvansKredi);
            this.tpLoadCredits.Controls.Add(this.uxTBAvansKredi);
            this.tpLoadCredits.Controls.Add(this.uxBTNSilVeYukle);
            this.tpLoadCredits.Controls.Add(this.label23);
            this.tpLoadCredits.Controls.Add(this.label22);
            this.tpLoadCredits.Controls.Add(this.uxLBYedekKredi);
            this.tpLoadCredits.Controls.Add(this.btnKontorSil);
            this.tpLoadCredits.Controls.Add(this.txtYedekKredi);
            this.tpLoadCredits.Controls.Add(this.btnKontorEkle);
            this.tpLoadCredits.Controls.Add(this.uxLBAnaKredi);
            this.tpLoadCredits.Controls.Add(this.txtAnaKredi);
            this.tpLoadCredits.Location = new System.Drawing.Point(4, 22);
            this.tpLoadCredits.Name = "tpLoadCredits";
            this.tpLoadCredits.Padding = new System.Windows.Forms.Padding(3);
            this.tpLoadCredits.Size = new System.Drawing.Size(609, 264);
            this.tpLoadCredits.TabIndex = 0;
            this.tpLoadCredits.Text = "Kontör İşlemleri";
            this.tpLoadCredits.UseVisualStyleBackColor = true;
            // 
            // uxDTPMakbuzNo
            // 
            this.uxDTPMakbuzNo.Checked = false;
            this.uxDTPMakbuzNo.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.uxDTPMakbuzNo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.uxDTPMakbuzNo.Location = new System.Drawing.Point(163, 111);
            this.uxDTPMakbuzNo.Name = "uxDTPMakbuzNo";
            this.uxDTPMakbuzNo.ShowCheckBox = true;
            this.uxDTPMakbuzNo.Size = new System.Drawing.Size(148, 20);
            this.uxDTPMakbuzNo.TabIndex = 81;
            // 
            // label62
            // 
            this.label62.Location = new System.Drawing.Point(19, 112);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(140, 15);
            this.label62.TabIndex = 40;
            this.label62.Text = "Makbuz No:";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label61
            // 
            this.label61.Location = new System.Drawing.Point(25, 217);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(140, 15);
            this.label61.TabIndex = 39;
            this.label61.Text = "Kritik Kredi:";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBKritikKredi
            // 
            this.uxTBKritikKredi.Location = new System.Drawing.Point(169, 214);
            this.uxTBKritikKredi.Name = "uxTBKritikKredi";
            this.uxTBKritikKredi.Size = new System.Drawing.Size(69, 20);
            this.uxTBKritikKredi.TabIndex = 38;
            this.uxTBKritikKredi.Text = "0";
            // 
            // uxLBLAvansKredi
            // 
            this.uxLBLAvansKredi.Location = new System.Drawing.Point(25, 191);
            this.uxLBLAvansKredi.Name = "uxLBLAvansKredi";
            this.uxLBLAvansKredi.Size = new System.Drawing.Size(140, 15);
            this.uxLBLAvansKredi.TabIndex = 37;
            this.uxLBLAvansKredi.Text = "Avans Kredi:";
            this.uxLBLAvansKredi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBAvansKredi
            // 
            this.uxTBAvansKredi.Location = new System.Drawing.Point(169, 188);
            this.uxTBAvansKredi.Name = "uxTBAvansKredi";
            this.uxTBAvansKredi.Size = new System.Drawing.Size(69, 20);
            this.uxTBAvansKredi.TabIndex = 36;
            this.uxTBAvansKredi.Text = "0";
            // 
            // uxBTNSilVeYukle
            // 
            this.uxBTNSilVeYukle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBTNSilVeYukle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxBTNSilVeYukle.ForeColor = System.Drawing.Color.OrangeRed;
            this.uxBTNSilVeYukle.Location = new System.Drawing.Point(317, 112);
            this.uxBTNSilVeYukle.Name = "uxBTNSilVeYukle";
            this.uxBTNSilVeYukle.Size = new System.Drawing.Size(98, 23);
            this.uxBTNSilVeYukle.TabIndex = 35;
            this.uxBTNSilVeYukle.Text = "Sil ve Yükle";
            this.uxBTNSilVeYukle.UseVisualStyleBackColor = true;
            this.uxBTNSilVeYukle.Click += new System.EventHandler(this.uxBTNSilVeYukle_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(239, 88);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(39, 13);
            this.label23.TabIndex = 29;
            this.label23.Text = "TL/m3";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(239, 59);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(39, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "TL/m3";
            // 
            // uxLBYedekKredi
            // 
            this.uxLBYedekKredi.Location = new System.Drawing.Point(19, 86);
            this.uxLBYedekKredi.Name = "uxLBYedekKredi";
            this.uxLBYedekKredi.Size = new System.Drawing.Size(140, 15);
            this.uxLBYedekKredi.TabIndex = 25;
            this.uxLBYedekKredi.Text = "Yedek Kredi:";
            this.uxLBYedekKredi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnKontorSil
            // 
            this.btnKontorSil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKontorSil.Font = new System.Drawing.Font("Nina", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnKontorSil.ForeColor = System.Drawing.Color.OrangeRed;
            this.btnKontorSil.Location = new System.Drawing.Point(317, 83);
            this.btnKontorSil.Name = "btnKontorSil";
            this.btnKontorSil.Size = new System.Drawing.Size(98, 23);
            this.btnKontorSil.TabIndex = 27;
            this.btnKontorSil.Text = "Kontor Sil";
            this.btnKontorSil.UseVisualStyleBackColor = true;
            this.btnKontorSil.Click += new System.EventHandler(this.btnKontorSil_Click);
            // 
            // txtYedekKredi
            // 
            this.txtYedekKredi.Location = new System.Drawing.Point(163, 83);
            this.txtYedekKredi.Name = "txtYedekKredi";
            this.txtYedekKredi.Size = new System.Drawing.Size(69, 20);
            this.txtYedekKredi.TabIndex = 18;
            this.txtYedekKredi.Text = "0";
            // 
            // btnKontorEkle
            // 
            this.btnKontorEkle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKontorEkle.Font = new System.Drawing.Font("Nina", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKontorEkle.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnKontorEkle.Location = new System.Drawing.Point(317, 54);
            this.btnKontorEkle.Name = "btnKontorEkle";
            this.btnKontorEkle.Size = new System.Drawing.Size(98, 23);
            this.btnKontorEkle.TabIndex = 26;
            this.btnKontorEkle.Text = "Kontor Ekle";
            this.btnKontorEkle.UseVisualStyleBackColor = true;
            this.btnKontorEkle.Click += new System.EventHandler(this.btnKontorEkle_Click);
            // 
            // uxLBAnaKredi
            // 
            this.uxLBAnaKredi.Location = new System.Drawing.Point(36, 60);
            this.uxLBAnaKredi.Name = "uxLBAnaKredi";
            this.uxLBAnaKredi.Size = new System.Drawing.Size(123, 17);
            this.uxLBAnaKredi.TabIndex = 23;
            this.uxLBAnaKredi.Text = "Ana Kredi :";
            this.uxLBAnaKredi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAnaKredi
            // 
            this.txtAnaKredi.Location = new System.Drawing.Point(163, 57);
            this.txtAnaKredi.Name = "txtAnaKredi";
            this.txtAnaKredi.Size = new System.Drawing.Size(69, 20);
            this.txtAnaKredi.TabIndex = 17;
            this.txtAnaKredi.Text = "0";
            // 
            // tpTest
            // 
            this.tpTest.Controls.Add(this.txCardHealth);
            this.tpTest.Controls.Add(this.groupBox3);
            this.tpTest.Controls.Add(this.btTestCard);
            this.tpTest.Location = new System.Drawing.Point(4, 22);
            this.tpTest.Name = "tpTest";
            this.tpTest.Padding = new System.Windows.Forms.Padding(3);
            this.tpTest.Size = new System.Drawing.Size(609, 264);
            this.tpTest.TabIndex = 1;
            this.tpTest.Text = "Test";
            this.tpTest.UseVisualStyleBackColor = true;
            // 
            // txCardHealth
            // 
            this.txCardHealth.BackColor = System.Drawing.Color.Black;
            this.txCardHealth.ContextMenuStrip = this.testContextMenu;
            this.txCardHealth.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txCardHealth.Font = new System.Drawing.Font("Palatino Linotype", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txCardHealth.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.txCardHealth.Location = new System.Drawing.Point(3, 94);
            this.txCardHealth.Multiline = true;
            this.txCardHealth.Name = "txCardHealth";
            this.txCardHealth.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txCardHealth.Size = new System.Drawing.Size(603, 167);
            this.txCardHealth.TabIndex = 69;
            // 
            // testContextMenu
            // 
            this.testContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearMenuToolstrip,
            this.saveResultsToolstrip});
            this.testContextMenu.Name = "testContextMenu";
            this.testContextMenu.Size = new System.Drawing.Size(163, 48);
            // 
            // clearMenuToolstrip
            // 
            this.clearMenuToolstrip.Name = "clearMenuToolstrip";
            this.clearMenuToolstrip.Size = new System.Drawing.Size(162, 22);
            this.clearMenuToolstrip.Text = "Temizle";
            // 
            // saveResultsToolstrip
            // 
            this.saveResultsToolstrip.Name = "saveResultsToolstrip";
            this.saveResultsToolstrip.Size = new System.Drawing.Size(162, 22);
            this.saveResultsToolstrip.Text = "Sonuçları Kaydet";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.uxRepatedOperationType1);
            this.groupBox3.Controls.Add(this.uxRepatedOperationType2);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.uxOkumaAdedi);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(469, 47);
            this.groupBox3.TabIndex = 68;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kart Test İşlemleri";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(442, 21);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(16, 13);
            this.label42.TabIndex = 74;
            this.label42.Text = "->";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(280, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(41, 13);
            this.label41.TabIndex = 73;
            this.label41.Text = "Sonra :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(115, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(39, 13);
            this.label40.TabIndex = 72;
            this.label40.Text = "Önce :";
            // 
            // uxRepatedOperationType1
            // 
            this.uxRepatedOperationType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxRepatedOperationType1.FormattingEnabled = true;
            this.uxRepatedOperationType1.Items.AddRange(new object[] {
            "-Seciniz-",
            "Kart Oku",
            "Kart Sil",
            "Seri No Oku",
            "Kart Oluştur",
            "Okuyucu Yükle",
            "Kredi Yukle"});
            this.uxRepatedOperationType1.Location = new System.Drawing.Point(161, 18);
            this.uxRepatedOperationType1.Name = "uxRepatedOperationType1";
            this.uxRepatedOperationType1.Size = new System.Drawing.Size(110, 21);
            this.uxRepatedOperationType1.TabIndex = 71;
            // 
            // uxRepatedOperationType2
            // 
            this.uxRepatedOperationType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxRepatedOperationType2.FormattingEnabled = true;
            this.uxRepatedOperationType2.Items.AddRange(new object[] {
            "-Seciniz-",
            "Kart Oku",
            "Kart Sil",
            "Seri No Oku",
            "Kart Oluştur",
            "Okuyucu Yükle",
            "Kredi Yukle"});
            this.uxRepatedOperationType2.Location = new System.Drawing.Point(328, 18);
            this.uxRepatedOperationType2.Name = "uxRepatedOperationType2";
            this.uxRepatedOperationType2.Size = new System.Drawing.Size(110, 21);
            this.uxRepatedOperationType2.TabIndex = 70;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(70, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(70, 13);
            this.label38.TabIndex = 69;
            this.label38.Text = "defa";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uxOkumaAdedi
            // 
            this.uxOkumaAdedi.Location = new System.Drawing.Point(10, 18);
            this.uxOkumaAdedi.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.uxOkumaAdedi.Name = "uxOkumaAdedi";
            this.uxOkumaAdedi.Size = new System.Drawing.Size(57, 20);
            this.uxOkumaAdedi.TabIndex = 68;
            this.uxOkumaAdedi.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // btTestCard
            // 
            this.btTestCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTestCard.Font = new System.Drawing.Font("Nina", 8.25F, System.Drawing.FontStyle.Bold);
            this.btTestCard.ForeColor = System.Drawing.Color.SteelBlue;
            this.btTestCard.Location = new System.Drawing.Point(12, 65);
            this.btTestCard.Name = "btTestCard";
            this.btTestCard.Size = new System.Drawing.Size(98, 23);
            this.btTestCard.TabIndex = 67;
            this.btTestCard.Text = "Çalıştır";
            this.btTestCard.UseVisualStyleBackColor = true;
            this.btTestCard.Click += new System.EventHandler(this.btTestCard_Click);
            // 
            // tpParameters1
            // 
            this.tpParameters1.Controls.Add(this.groupBox2);
            this.tpParameters1.Location = new System.Drawing.Point(4, 22);
            this.tpParameters1.Name = "tpParameters1";
            this.tpParameters1.Padding = new System.Windows.Forms.Padding(3);
            this.tpParameters1.Size = new System.Drawing.Size(609, 264);
            this.tpParameters1.TabIndex = 2;
            this.tpParameters1.Text = "Param. 1";
            this.tpParameters1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.cmpMaxDebiEsikDegeri);
            this.groupBox2.Controls.Add(this.cmpSayacCapi);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(486, 252);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parametreler";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox6.Controls.Add(this.uxCBDSIKapatma);
            this.groupBox6.Controls.Add(this.uxSayacKapatmaTarihi);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Location = new System.Drawing.Point(35, 186);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(400, 54);
            this.groupBox6.TabIndex = 75;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Sayaç Kapatma Tarihi";
            // 
            // uxCBDSIKapatma
            // 
            this.uxCBDSIKapatma.AutoSize = true;
            this.uxCBDSIKapatma.Location = new System.Drawing.Point(9, 22);
            this.uxCBDSIKapatma.Name = "uxCBDSIKapatma";
            this.uxCBDSIKapatma.Size = new System.Drawing.Size(95, 17);
            this.uxCBDSIKapatma.TabIndex = 72;
            this.uxCBDSIKapatma.Text = "DSI Kapatma?";
            this.uxCBDSIKapatma.UseVisualStyleBackColor = true;
            this.uxCBDSIKapatma.Visible = false;
            this.uxCBDSIKapatma.CheckedChanged += new System.EventHandler(this.uxCBDSIKapatma_CheckedChanged);
            // 
            // uxSayacKapatmaTarihi
            // 
            this.uxSayacKapatmaTarihi.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.uxSayacKapatmaTarihi.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.uxSayacKapatmaTarihi.Location = new System.Drawing.Point(239, 20);
            this.uxSayacKapatmaTarihi.Name = "uxSayacKapatmaTarihi";
            this.uxSayacKapatmaTarihi.Size = new System.Drawing.Size(150, 20);
            this.uxSayacKapatmaTarihi.TabIndex = 71;
            this.uxSayacKapatmaTarihi.Value = new System.DateTime(2012, 10, 16, 17, 19, 0, 0);
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(26, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(203, 13);
            this.label39.TabIndex = 70;
            this.label39.Text = "Sayaç Kapatma Tarihi:";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.cmpYanginModuSaat);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.cmpYanginModuDakika);
            this.groupBox5.Location = new System.Drawing.Point(35, 25);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(400, 53);
            this.groupBox5.TabIndex = 74;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Yangın Modu";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(329, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 32;
            this.label21.Text = "dakika";
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(23, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(143, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Yangin Modu Suresi :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmpYanginModuSaat
            // 
            this.cmpYanginModuSaat.Location = new System.Drawing.Point(171, 19);
            this.cmpYanginModuSaat.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.cmpYanginModuSaat.Name = "cmpYanginModuSaat";
            this.cmpYanginModuSaat.Size = new System.Drawing.Size(57, 20);
            this.cmpYanginModuSaat.TabIndex = 29;
            this.cmpYanginModuSaat.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(231, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(27, 13);
            this.label20.TabIndex = 30;
            this.label20.Text = "saat";
            // 
            // cmpYanginModuDakika
            // 
            this.cmpYanginModuDakika.Location = new System.Drawing.Point(277, 19);
            this.cmpYanginModuDakika.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.cmpYanginModuDakika.Name = "cmpYanginModuDakika";
            this.cmpYanginModuDakika.Size = new System.Drawing.Size(47, 20);
            this.cmpYanginModuDakika.TabIndex = 31;
            this.cmpYanginModuDakika.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.cmpKesintisizBitis);
            this.groupBox1.Controls.Add(this.uxCBSurekli);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.cmpKesintisizBaslangic);
            this.groupBox1.Location = new System.Drawing.Point(35, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 102);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kesintisiz Su Kullanımı";
            // 
            // cmpKesintisizBitis
            // 
            this.cmpKesintisizBitis.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.cmpKesintisizBitis.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cmpKesintisizBitis.Location = new System.Drawing.Point(199, 62);
            this.cmpKesintisizBitis.Name = "cmpKesintisizBitis";
            this.cmpKesintisizBitis.Size = new System.Drawing.Size(196, 20);
            this.cmpKesintisizBitis.TabIndex = 34;
            this.cmpKesintisizBitis.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // uxCBSurekli
            // 
            this.uxCBSurekli.AutoSize = true;
            this.uxCBSurekli.Location = new System.Drawing.Point(200, 21);
            this.uxCBSurekli.Name = "uxCBSurekli";
            this.uxCBSurekli.Size = new System.Drawing.Size(151, 17);
            this.uxCBSurekli.TabIndex = 72;
            this.uxCBSurekli.Text = "Kesintisiz Su Kullanım Aktif";
            this.uxCBSurekli.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(6, 45);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(189, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "Kesintisiz Su Baslangic:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(6, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(189, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "Kesintisiz Su Bitis:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmpKesintisizBaslangic
            // 
            this.cmpKesintisizBaslangic.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.cmpKesintisizBaslangic.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.cmpKesintisizBaslangic.Location = new System.Drawing.Point(199, 41);
            this.cmpKesintisizBaslangic.Name = "cmpKesintisizBaslangic";
            this.cmpKesintisizBaslangic.Size = new System.Drawing.Size(196, 20);
            this.cmpKesintisizBaslangic.TabIndex = 33;
            this.cmpKesintisizBaslangic.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(272, 158);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(21, 13);
            this.label24.TabIndex = 37;
            this.label24.Text = "m3";
            // 
            // cmpMaxDebiEsikDegeri
            // 
            this.cmpMaxDebiEsikDegeri.Location = new System.Drawing.Point(215, 155);
            this.cmpMaxDebiEsikDegeri.Name = "cmpMaxDebiEsikDegeri";
            this.cmpMaxDebiEsikDegeri.Size = new System.Drawing.Size(51, 20);
            this.cmpMaxDebiEsikDegeri.TabIndex = 36;
            this.cmpMaxDebiEsikDegeri.Text = "3";
            // 
            // cmpSayacCapi
            // 
            this.cmpSayacCapi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmpSayacCapi.FormattingEnabled = true;
            this.cmpSayacCapi.Items.AddRange(new object[] {
            "20",
            "40",
            "50",
            "80",
            "110"});
            this.cmpSayacCapi.Location = new System.Drawing.Point(214, 115);
            this.cmpSayacCapi.Name = "cmpSayacCapi";
            this.cmpSayacCapi.Size = new System.Drawing.Size(100, 21);
            this.cmpSayacCapi.TabIndex = 35;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(90, 164);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(115, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "MAX Debi Esik Degeri:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(147, 119);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 27;
            this.label18.Text = "Sayac Capi:";
            // 
            // tpParameters2
            // 
            this.tpParameters2.Controls.Add(this.uxCBZeroTariffs);
            this.tpParameters2.Controls.Add(this.groupBox9);
            this.tpParameters2.Controls.Add(this.uxAK21KartStatus);
            this.tpParameters2.Controls.Add(this.uxAK11KartStatus);
            this.tpParameters2.Location = new System.Drawing.Point(4, 22);
            this.tpParameters2.Name = "tpParameters2";
            this.tpParameters2.Padding = new System.Windows.Forms.Padding(3);
            this.tpParameters2.Size = new System.Drawing.Size(609, 264);
            this.tpParameters2.TabIndex = 3;
            this.tpParameters2.Text = "Param. 2";
            this.tpParameters2.UseVisualStyleBackColor = true;
            // 
            // uxCBZeroTariffs
            // 
            this.uxCBZeroTariffs.Location = new System.Drawing.Point(499, 15);
            this.uxCBZeroTariffs.Name = "uxCBZeroTariffs";
            this.uxCBZeroTariffs.Size = new System.Drawing.Size(104, 17);
            this.uxCBZeroTariffs.TabIndex = 4;
            this.uxCBZeroTariffs.Text = "0?";
            this.uxCBZeroTariffs.UseVisualStyleBackColor = true;
            this.uxCBZeroTariffs.CheckedChanged += new System.EventHandler(this.uxCBZeroTariffs_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.uxBTNCopyCardToClipboard);
            this.groupBox9.Controls.Add(this.uxKartStatus);
            this.groupBox9.Controls.Add(this.uxSendAsByte);
            this.groupBox9.Location = new System.Drawing.Point(267, 186);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(226, 72);
            this.groupBox9.TabIndex = 65;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Byte Gönderim";
            // 
            // uxBTNCopyCardToClipboard
            // 
            this.uxBTNCopyCardToClipboard.Location = new System.Drawing.Point(50, 41);
            this.uxBTNCopyCardToClipboard.Name = "uxBTNCopyCardToClipboard";
            this.uxBTNCopyCardToClipboard.Size = new System.Drawing.Size(75, 23);
            this.uxBTNCopyCardToClipboard.TabIndex = 63;
            this.uxBTNCopyCardToClipboard.Text = "CLP";
            this.uxBTNCopyCardToClipboard.UseVisualStyleBackColor = true;
            // 
            // uxKartStatus
            // 
            this.uxKartStatus.Location = new System.Drawing.Point(9, 41);
            this.uxKartStatus.Name = "uxKartStatus";
            this.uxKartStatus.Size = new System.Drawing.Size(34, 20);
            this.uxKartStatus.TabIndex = 61;
            // 
            // uxSendAsByte
            // 
            this.uxSendAsByte.AutoSize = true;
            this.uxSendAsByte.Location = new System.Drawing.Point(11, 19);
            this.uxSendAsByte.Name = "uxSendAsByte";
            this.uxSendAsByte.Size = new System.Drawing.Size(162, 17);
            this.uxSendAsByte.TabIndex = 62;
            this.uxSendAsByte.Text = "Kart Durumlarını Byte Gönder";
            this.uxSendAsByte.UseVisualStyleBackColor = true;
            // 
            // uxAK21KartStatus
            // 
            this.uxAK21KartStatus.Controls.Add(this.uxAK21PilKapagiCezasi);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21UstKapakCeza);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21ManyetikCeza);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21RekorCeza);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21HaftaSonuMesaiSayacAcik);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21ResmiTatilVanaAcik);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21TarihdeVanaKapat);
            this.uxAK21KartStatus.Controls.Add(this.uxAK21KesintisizSu);
            this.uxAK21KartStatus.Location = new System.Drawing.Point(6, 6);
            this.uxAK21KartStatus.Name = "uxAK21KartStatus";
            this.uxAK21KartStatus.Size = new System.Drawing.Size(259, 252);
            this.uxAK21KartStatus.TabIndex = 63;
            this.uxAK21KartStatus.TabStop = false;
            this.uxAK21KartStatus.Text = "Kart Durumu";
            // 
            // uxAK21PilKapagiCezasi
            // 
            this.uxAK21PilKapagiCezasi.Location = new System.Drawing.Point(7, 212);
            this.uxAK21PilKapagiCezasi.Name = "uxAK21PilKapagiCezasi";
            this.uxAK21PilKapagiCezasi.Size = new System.Drawing.Size(216, 17);
            this.uxAK21PilKapagiCezasi.TabIndex = 7;
            this.uxAK21PilKapagiCezasi.Text = "Pil Kapagi Cezasi";
            this.uxAK21PilKapagiCezasi.UseVisualStyleBackColor = true;
            // 
            // uxAK21UstKapakCeza
            // 
            this.uxAK21UstKapakCeza.Location = new System.Drawing.Point(7, 186);
            this.uxAK21UstKapakCeza.Name = "uxAK21UstKapakCeza";
            this.uxAK21UstKapakCeza.Size = new System.Drawing.Size(216, 17);
            this.uxAK21UstKapakCeza.TabIndex = 6;
            this.uxAK21UstKapakCeza.Text = "Ust Kapak Ceza";
            this.uxAK21UstKapakCeza.UseVisualStyleBackColor = true;
            // 
            // uxAK21ManyetikCeza
            // 
            this.uxAK21ManyetikCeza.Location = new System.Drawing.Point(7, 160);
            this.uxAK21ManyetikCeza.Name = "uxAK21ManyetikCeza";
            this.uxAK21ManyetikCeza.Size = new System.Drawing.Size(216, 17);
            this.uxAK21ManyetikCeza.TabIndex = 5;
            this.uxAK21ManyetikCeza.Text = "Manyetik Ceza";
            this.uxAK21ManyetikCeza.UseVisualStyleBackColor = true;
            // 
            // uxAK21RekorCeza
            // 
            this.uxAK21RekorCeza.Location = new System.Drawing.Point(7, 134);
            this.uxAK21RekorCeza.Name = "uxAK21RekorCeza";
            this.uxAK21RekorCeza.Size = new System.Drawing.Size(216, 17);
            this.uxAK21RekorCeza.TabIndex = 4;
            this.uxAK21RekorCeza.Text = "Rekor Ceza";
            this.uxAK21RekorCeza.UseVisualStyleBackColor = true;
            // 
            // uxAK21HaftaSonuMesaiSayacAcik
            // 
            this.uxAK21HaftaSonuMesaiSayacAcik.Location = new System.Drawing.Point(7, 108);
            this.uxAK21HaftaSonuMesaiSayacAcik.Name = "uxAK21HaftaSonuMesaiSayacAcik";
            this.uxAK21HaftaSonuMesaiSayacAcik.Size = new System.Drawing.Size(216, 17);
            this.uxAK21HaftaSonuMesaiSayacAcik.TabIndex = 3;
            this.uxAK21HaftaSonuMesaiSayacAcik.Text = "Hafta Sonu Mesai Disi Sayac Acik";
            this.uxAK21HaftaSonuMesaiSayacAcik.UseVisualStyleBackColor = true;
            // 
            // uxAK21ResmiTatilVanaAcik
            // 
            this.uxAK21ResmiTatilVanaAcik.Location = new System.Drawing.Point(7, 56);
            this.uxAK21ResmiTatilVanaAcik.Name = "uxAK21ResmiTatilVanaAcik";
            this.uxAK21ResmiTatilVanaAcik.Size = new System.Drawing.Size(216, 17);
            this.uxAK21ResmiTatilVanaAcik.TabIndex = 2;
            this.uxAK21ResmiTatilVanaAcik.Text = "Resmi Tatil Vana Acik";
            this.uxAK21ResmiTatilVanaAcik.UseVisualStyleBackColor = true;
            // 
            // uxAK21TarihdeVanaKapat
            // 
            this.uxAK21TarihdeVanaKapat.Location = new System.Drawing.Point(7, 82);
            this.uxAK21TarihdeVanaKapat.Name = "uxAK21TarihdeVanaKapat";
            this.uxAK21TarihdeVanaKapat.Size = new System.Drawing.Size(216, 17);
            this.uxAK21TarihdeVanaKapat.TabIndex = 1;
            this.uxAK21TarihdeVanaKapat.Text = "Belirtilen Tarihte Vana Kapat";
            this.uxAK21TarihdeVanaKapat.UseVisualStyleBackColor = true;
            // 
            // uxAK21KesintisizSu
            // 
            this.uxAK21KesintisizSu.Location = new System.Drawing.Point(7, 30);
            this.uxAK21KesintisizSu.Name = "uxAK21KesintisizSu";
            this.uxAK21KesintisizSu.Size = new System.Drawing.Size(216, 17);
            this.uxAK21KesintisizSu.TabIndex = 0;
            this.uxAK21KesintisizSu.Text = "Kesintisiz Su Verme";
            this.uxAK21KesintisizSu.UseVisualStyleBackColor = true;
            // 
            // uxAK11KartStatus
            // 
            this.uxAK11KartStatus.Controls.Add(this.uxAK11HaftaSonuMesaiDisiVanaAcik);
            this.uxAK11KartStatus.Controls.Add(this.uxAK11PilUzat);
            this.uxAK11KartStatus.Controls.Add(this.uxAK11AydaIkiVanaAcmaKapama);
            this.uxAK11KartStatus.Controls.Add(this.uxAK11PilKisalt);
            this.uxAK11KartStatus.Location = new System.Drawing.Point(267, 6);
            this.uxAK11KartStatus.Name = "uxAK11KartStatus";
            this.uxAK11KartStatus.Size = new System.Drawing.Size(226, 174);
            this.uxAK11KartStatus.TabIndex = 62;
            this.uxAK11KartStatus.TabStop = false;
            this.uxAK11KartStatus.Text = "AK11 Sayaç Fonksyonları";
            // 
            // uxAK11HaftaSonuMesaiDisiVanaAcik
            // 
            this.uxAK11HaftaSonuMesaiDisiVanaAcik.Location = new System.Drawing.Point(12, 139);
            this.uxAK11HaftaSonuMesaiDisiVanaAcik.Name = "uxAK11HaftaSonuMesaiDisiVanaAcik";
            this.uxAK11HaftaSonuMesaiDisiVanaAcik.Size = new System.Drawing.Size(210, 20);
            this.uxAK11HaftaSonuMesaiDisiVanaAcik.TabIndex = 3;
            this.uxAK11HaftaSonuMesaiDisiVanaAcik.Text = "Hafta Sonu Mesai Disi Sayac Acik";
            this.uxAK11HaftaSonuMesaiDisiVanaAcik.UseVisualStyleBackColor = true;
            // 
            // uxAK11PilUzat
            // 
            this.uxAK11PilUzat.Location = new System.Drawing.Point(12, 64);
            this.uxAK11PilUzat.Name = "uxAK11PilUzat";
            this.uxAK11PilUzat.Size = new System.Drawing.Size(210, 17);
            this.uxAK11PilUzat.TabIndex = 2;
            this.uxAK11PilUzat.Text = "Pil Ömrü Uzat";
            this.uxAK11PilUzat.UseVisualStyleBackColor = true;
            // 
            // uxAK11AydaIkiVanaAcmaKapama
            // 
            this.uxAK11AydaIkiVanaAcmaKapama.Location = new System.Drawing.Point(12, 100);
            this.uxAK11AydaIkiVanaAcmaKapama.Name = "uxAK11AydaIkiVanaAcmaKapama";
            this.uxAK11AydaIkiVanaAcmaKapama.Size = new System.Drawing.Size(210, 20);
            this.uxAK11AydaIkiVanaAcmaKapama.TabIndex = 1;
            this.uxAK11AydaIkiVanaAcmaKapama.Text = "Ayda Iki Vana Acma/Kapama";
            this.uxAK11AydaIkiVanaAcmaKapama.UseVisualStyleBackColor = true;
            // 
            // uxAK11PilKisalt
            // 
            this.uxAK11PilKisalt.Location = new System.Drawing.Point(12, 28);
            this.uxAK11PilKisalt.Name = "uxAK11PilKisalt";
            this.uxAK11PilKisalt.Size = new System.Drawing.Size(208, 17);
            this.uxAK11PilKisalt.TabIndex = 0;
            this.uxAK11PilKisalt.Text = "Pil Ömrü Kisalt";
            this.uxAK11PilKisalt.UseVisualStyleBackColor = true;
            // 
            // uxTPParameters3
            // 
            this.uxTPParameters3.Controls.Add(this.groupBox11);
            this.uxTPParameters3.Location = new System.Drawing.Point(4, 22);
            this.uxTPParameters3.Name = "uxTPParameters3";
            this.uxTPParameters3.Padding = new System.Windows.Forms.Padding(3);
            this.uxTPParameters3.Size = new System.Drawing.Size(609, 264);
            this.uxTPParameters3.TabIndex = 7;
            this.uxTPParameters3.Text = "Param. 3";
            this.uxTPParameters3.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.uxLBLFreq2);
            this.groupBox11.Controls.Add(this.uxLBLFreq1);
            this.groupBox11.Controls.Add(this.uxLBLFreq0);
            this.groupBox11.Controls.Add(this.label51);
            this.groupBox11.Controls.Add(this.uxTBFreq3);
            this.groupBox11.Controls.Add(this.uxTBFreq1);
            this.groupBox11.Controls.Add(this.uxTBFreq0);
            this.groupBox11.Controls.Add(this.label49);
            this.groupBox11.Controls.Add(this.label12);
            this.groupBox11.Controls.Add(this.label11);
            this.groupBox11.Controls.Add(this.uxLBLTrackBarValue);
            this.groupBox11.Controls.Add(this.uxLBLTimeType);
            this.groupBox11.Controls.Add(this.label50);
            this.groupBox11.Controls.Add(this.uxTBTimeData);
            this.groupBox11.Controls.Add(this.uxCBTimeType);
            this.groupBox11.Location = new System.Drawing.Point(8, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(359, 252);
            this.groupBox11.TabIndex = 55;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "RF Yayın Ayarları";
            // 
            // uxLBLFreq2
            // 
            this.uxLBLFreq2.AutoSize = true;
            this.uxLBLFreq2.Location = new System.Drawing.Point(142, 191);
            this.uxLBLFreq2.Name = "uxLBLFreq2";
            this.uxLBLFreq2.Size = new System.Drawing.Size(24, 13);
            this.uxLBLFreq2.TabIndex = 66;
            this.uxLBLFreq2.Text = "0x0";
            // 
            // uxLBLFreq1
            // 
            this.uxLBLFreq1.AutoSize = true;
            this.uxLBLFreq1.Location = new System.Drawing.Point(142, 165);
            this.uxLBLFreq1.Name = "uxLBLFreq1";
            this.uxLBLFreq1.Size = new System.Drawing.Size(24, 13);
            this.uxLBLFreq1.TabIndex = 65;
            this.uxLBLFreq1.Text = "0x0";
            // 
            // uxLBLFreq0
            // 
            this.uxLBLFreq0.AutoSize = true;
            this.uxLBLFreq0.Location = new System.Drawing.Point(142, 138);
            this.uxLBLFreq0.Name = "uxLBLFreq0";
            this.uxLBLFreq0.Size = new System.Drawing.Size(24, 13);
            this.uxLBLFreq0.TabIndex = 64;
            this.uxLBLFreq0.Text = "0x0";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(63, 110);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(54, 13);
            this.label51.TabIndex = 63;
            this.label51.Text = "DECIMAL";
            // 
            // uxTBFreq3
            // 
            this.uxTBFreq3.Location = new System.Drawing.Point(60, 187);
            this.uxTBFreq3.MaxLength = 3;
            this.uxTBFreq3.Name = "uxTBFreq3";
            this.uxTBFreq3.Size = new System.Drawing.Size(65, 20);
            this.uxTBFreq3.TabIndex = 62;
            this.uxTBFreq3.Text = "0";
            // 
            // uxTBFreq1
            // 
            this.uxTBFreq1.Location = new System.Drawing.Point(60, 161);
            this.uxTBFreq1.MaxLength = 3;
            this.uxTBFreq1.Name = "uxTBFreq1";
            this.uxTBFreq1.Size = new System.Drawing.Size(65, 20);
            this.uxTBFreq1.TabIndex = 61;
            this.uxTBFreq1.Text = "0";
            // 
            // uxTBFreq0
            // 
            this.uxTBFreq0.Location = new System.Drawing.Point(60, 135);
            this.uxTBFreq0.MaxLength = 3;
            this.uxTBFreq0.Name = "uxTBFreq0";
            this.uxTBFreq0.Size = new System.Drawing.Size(65, 20);
            this.uxTBFreq0.TabIndex = 60;
            this.uxTBFreq0.Text = "0";
            this.uxTBFreq0.TextChanged += new System.EventHandler(this.uxTBFreq0_TextChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(9, 191);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(40, 13);
            this.label49.TabIndex = 59;
            this.label49.Text = "Freq 1:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 58;
            this.label12.Text = "Freq 1:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 57;
            this.label11.Text = "Freq 0:";
            // 
            // uxLBLTrackBarValue
            // 
            this.uxLBLTrackBarValue.Location = new System.Drawing.Point(227, 50);
            this.uxLBLTrackBarValue.Name = "uxLBLTrackBarValue";
            this.uxLBLTrackBarValue.Size = new System.Drawing.Size(46, 19);
            this.uxLBLTrackBarValue.TabIndex = 56;
            this.uxLBLTrackBarValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxLBLTimeType
            // 
            this.uxLBLTimeType.Location = new System.Drawing.Point(279, 50);
            this.uxLBLTimeType.Name = "uxLBLTimeType";
            this.uxLBLTimeType.Size = new System.Drawing.Size(46, 19);
            this.uxLBLTimeType.TabIndex = 55;
            this.uxLBLTimeType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(6, 22);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(97, 18);
            this.label50.TabIndex = 53;
            this.label50.Text = "Zaman Tipi:";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBTimeData
            // 
            this.uxTBTimeData.Location = new System.Drawing.Point(9, 50);
            this.uxTBTimeData.Maximum = 63;
            this.uxTBTimeData.Name = "uxTBTimeData";
            this.uxTBTimeData.Size = new System.Drawing.Size(217, 45);
            this.uxTBTimeData.TabIndex = 54;
            this.uxTBTimeData.Scroll += new System.EventHandler(this.uxTBTimeData_Scroll);
            // 
            // uxCBTimeType
            // 
            this.uxCBTimeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxCBTimeType.FormattingEnabled = true;
            this.uxCBTimeType.Items.AddRange(new object[] {
            "",
            "Saniye",
            "Dakika",
            "Saat"});
            this.uxCBTimeType.Location = new System.Drawing.Point(109, 22);
            this.uxCBTimeType.Name = "uxCBTimeType";
            this.uxCBTimeType.Size = new System.Drawing.Size(117, 21);
            this.uxCBTimeType.TabIndex = 52;
            this.uxCBTimeType.SelectedIndexChanged += new System.EventHandler(this.uxCBTimeType_SelectedIndexChanged);
            // 
            // tpUsers
            // 
            this.tpUsers.Controls.Add(this.groupBox7);
            this.tpUsers.Location = new System.Drawing.Point(4, 22);
            this.tpUsers.Name = "tpUsers";
            this.tpUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsers.Size = new System.Drawing.Size(609, 264);
            this.tpUsers.TabIndex = 5;
            this.tpUsers.Text = "Kullanıcılar";
            this.tpUsers.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btSaveUserList);
            this.groupBox7.Controls.Add(this.btDeleteUser);
            this.groupBox7.Controls.Add(this.btAddNewUser);
            this.groupBox7.Controls.Add(this.lstKullanicilar);
            this.groupBox7.Controls.Add(this.propGrid);
            this.groupBox7.Location = new System.Drawing.Point(8, 5);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(486, 253);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Tanımlı Kullanıcılar ve Yetkileri";
            // 
            // btSaveUserList
            // 
            this.btSaveUserList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSaveUserList.Font = new System.Drawing.Font("Nina", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSaveUserList.ForeColor = System.Drawing.Color.DodgerBlue;
            this.btSaveUserList.Location = new System.Drawing.Point(352, 219);
            this.btSaveUserList.Name = "btSaveUserList";
            this.btSaveUserList.Size = new System.Drawing.Size(128, 23);
            this.btSaveUserList.TabIndex = 5;
            this.btSaveUserList.Text = "Kaydet";
            this.btSaveUserList.UseVisualStyleBackColor = true;
            this.btSaveUserList.Click += new System.EventHandler(this.btSaveUserList_Click);
            // 
            // btDeleteUser
            // 
            this.btDeleteUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDeleteUser.Font = new System.Drawing.Font("Nina", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDeleteUser.ForeColor = System.Drawing.Color.DarkRed;
            this.btDeleteUser.Location = new System.Drawing.Point(76, 219);
            this.btDeleteUser.Name = "btDeleteUser";
            this.btDeleteUser.Size = new System.Drawing.Size(65, 23);
            this.btDeleteUser.TabIndex = 4;
            this.btDeleteUser.Text = "Sil";
            this.btDeleteUser.UseVisualStyleBackColor = true;
            this.btDeleteUser.Click += new System.EventHandler(this.btDeleteUser_Click);
            // 
            // btAddNewUser
            // 
            this.btAddNewUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAddNewUser.Font = new System.Drawing.Font("Nina", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAddNewUser.ForeColor = System.Drawing.Color.SteelBlue;
            this.btAddNewUser.Location = new System.Drawing.Point(10, 219);
            this.btAddNewUser.Name = "btAddNewUser";
            this.btAddNewUser.Size = new System.Drawing.Size(65, 23);
            this.btAddNewUser.TabIndex = 3;
            this.btAddNewUser.Text = "Ekle";
            this.btAddNewUser.UseVisualStyleBackColor = true;
            this.btAddNewUser.Click += new System.EventHandler(this.btAddNewUser_Click);
            // 
            // lstKullanicilar
            // 
            this.lstKullanicilar.FormattingEnabled = true;
            this.lstKullanicilar.Location = new System.Drawing.Point(10, 19);
            this.lstKullanicilar.Name = "lstKullanicilar";
            this.lstKullanicilar.Size = new System.Drawing.Size(228, 186);
            this.lstKullanicilar.TabIndex = 2;
            this.lstKullanicilar.SelectedIndexChanged += new System.EventHandler(this.lstKullanicilar_SelectedIndexChanged);
            // 
            // propGrid
            // 
            this.propGrid.BackColor = System.Drawing.Color.DodgerBlue;
            this.propGrid.CategoryForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.propGrid.LargeButtons = true;
            this.propGrid.Location = new System.Drawing.Point(244, 19);
            this.propGrid.Name = "propGrid";
            this.propGrid.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propGrid.Size = new System.Drawing.Size(236, 186);
            this.propGrid.TabIndex = 0;
            this.propGrid.ToolbarVisible = false;
            this.propGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propGrid_PropertyValueChanged);
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.uxGBRFParametreler);
            this.tpSettings.Controls.Add(this.uxGBHygenicValveUsage);
            this.tpSettings.Controls.Add(this.groupBox10);
            this.tpSettings.Location = new System.Drawing.Point(4, 22);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tpSettings.Size = new System.Drawing.Size(609, 264);
            this.tpSettings.TabIndex = 6;
            this.tpSettings.Text = "Ayarlar";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // uxGBRFParametreler
            // 
            this.uxGBRFParametreler.BackColor = System.Drawing.Color.WhiteSmoke;
            this.uxGBRFParametreler.Controls.Add(this.label56);
            this.uxGBRFParametreler.Controls.Add(this.uxTBRFFreq2);
            this.uxGBRFParametreler.Controls.Add(this.label54);
            this.uxGBRFParametreler.Controls.Add(this.uxTBRFFreq1);
            this.uxGBRFParametreler.Controls.Add(this.label53);
            this.uxGBRFParametreler.Controls.Add(this.uxTBRFFreq0);
            this.uxGBRFParametreler.Controls.Add(this.label55);
            this.uxGBRFParametreler.Controls.Add(this.uxTBRFSure);
            this.uxGBRFParametreler.Controls.Add(this.uxCBRFZamanTipi);
            this.uxGBRFParametreler.Controls.Add(this.uxRBRFPasif);
            this.uxGBRFParametreler.Controls.Add(this.uxRBRFAktif);
            this.uxGBRFParametreler.Controls.Add(this.uxCBRFTipi);
            this.uxGBRFParametreler.Controls.Add(this.label52);
            this.uxGBRFParametreler.Location = new System.Drawing.Point(264, 71);
            this.uxGBRFParametreler.Name = "uxGBRFParametreler";
            this.uxGBRFParametreler.Size = new System.Drawing.Size(231, 174);
            this.uxGBRFParametreler.TabIndex = 78;
            this.uxGBRFParametreler.TabStop = false;
            this.uxGBRFParametreler.Text = "RF Ayar Parametreleri";
            this.uxGBRFParametreler.Visible = false;
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(120, 130);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(56, 13);
            this.label56.TabIndex = 39;
            this.label56.Text = "Freq2 :";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBRFFreq2
            // 
            this.uxTBRFFreq2.Location = new System.Drawing.Point(135, 148);
            this.uxTBRFFreq2.Name = "uxTBRFFreq2";
            this.uxTBRFFreq2.Size = new System.Drawing.Size(41, 20);
            this.uxTBRFFreq2.TabIndex = 38;
            this.uxTBRFFreq2.Text = "0";
            // 
            // label54
            // 
            this.label54.Location = new System.Drawing.Point(64, 130);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(56, 13);
            this.label54.TabIndex = 37;
            this.label54.Text = "Freq1 :";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBRFFreq1
            // 
            this.uxTBRFFreq1.Location = new System.Drawing.Point(79, 148);
            this.uxTBRFFreq1.Name = "uxTBRFFreq1";
            this.uxTBRFFreq1.Size = new System.Drawing.Size(41, 20);
            this.uxTBRFFreq1.TabIndex = 36;
            this.uxTBRFFreq1.Text = "0";
            // 
            // label53
            // 
            this.label53.Location = new System.Drawing.Point(8, 130);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(56, 13);
            this.label53.TabIndex = 35;
            this.label53.Text = "Freq0 :";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBRFFreq0
            // 
            this.uxTBRFFreq0.Location = new System.Drawing.Point(23, 148);
            this.uxTBRFFreq0.Name = "uxTBRFFreq0";
            this.uxTBRFFreq0.Size = new System.Drawing.Size(41, 20);
            this.uxTBRFFreq0.TabIndex = 34;
            this.uxTBRFFreq0.Text = "0";
            // 
            // label55
            // 
            this.label55.Location = new System.Drawing.Point(8, 84);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(56, 13);
            this.label55.TabIndex = 33;
            this.label55.Text = "Sure  :";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBRFSure
            // 
            this.uxTBRFSure.Location = new System.Drawing.Point(23, 106);
            this.uxTBRFSure.Name = "uxTBRFSure";
            this.uxTBRFSure.Size = new System.Drawing.Size(41, 20);
            this.uxTBRFSure.TabIndex = 31;
            this.uxTBRFSure.Text = "1";
            // 
            // uxCBRFZamanTipi
            // 
            this.uxCBRFZamanTipi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxCBRFZamanTipi.FormattingEnabled = true;
            this.uxCBRFZamanTipi.Items.AddRange(new object[] {
            "Saniye",
            "Dakika",
            "Saat"});
            this.uxCBRFZamanTipi.Location = new System.Drawing.Point(76, 106);
            this.uxCBRFZamanTipi.Name = "uxCBRFZamanTipi";
            this.uxCBRFZamanTipi.Size = new System.Drawing.Size(121, 21);
            this.uxCBRFZamanTipi.TabIndex = 30;
            // 
            // uxRBRFPasif
            // 
            this.uxRBRFPasif.AutoSize = true;
            this.uxRBRFPasif.Location = new System.Drawing.Point(84, 58);
            this.uxRBRFPasif.Name = "uxRBRFPasif";
            this.uxRBRFPasif.Size = new System.Drawing.Size(65, 17);
            this.uxRBRFPasif.TabIndex = 28;
            this.uxRBRFPasif.Text = "RF Pasif";
            this.uxRBRFPasif.UseVisualStyleBackColor = true;
            // 
            // uxRBRFAktif
            // 
            this.uxRBRFAktif.AutoSize = true;
            this.uxRBRFAktif.Checked = true;
            this.uxRBRFAktif.Location = new System.Drawing.Point(10, 58);
            this.uxRBRFAktif.Name = "uxRBRFAktif";
            this.uxRBRFAktif.Size = new System.Drawing.Size(63, 17);
            this.uxRBRFAktif.TabIndex = 27;
            this.uxRBRFAktif.TabStop = true;
            this.uxRBRFAktif.Text = "RF Aktif";
            this.uxRBRFAktif.UseVisualStyleBackColor = true;
            // 
            // uxCBRFTipi
            // 
            this.uxCBRFTipi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxCBRFTipi.FormattingEnabled = true;
            this.uxCBRFTipi.Items.AddRange(new object[] {
            "8-Byte",
            "16-Byte",
            "T2 Mod",
            "S2 Mod",
            "R Mod"});
            this.uxCBRFTipi.Location = new System.Drawing.Point(83, 31);
            this.uxCBRFTipi.Name = "uxCBRFTipi";
            this.uxCBRFTipi.Size = new System.Drawing.Size(121, 21);
            this.uxCBRFTipi.TabIndex = 26;
            this.uxCBRFTipi.SelectedIndexChanged += new System.EventHandler(this.uxCBRFTipi_SelectedIndexChanged);
            // 
            // label52
            // 
            this.label52.Location = new System.Drawing.Point(8, 34);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(56, 13);
            this.label52.TabIndex = 25;
            this.label52.Text = "RF Tipi :";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxGBHygenicValveUsage
            // 
            this.uxGBHygenicValveUsage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.uxGBHygenicValveUsage.Controls.Add(this.label43);
            this.uxGBHygenicValveUsage.Controls.Add(this.uxTBoxVanaAcilmaSuresi);
            this.uxGBHygenicValveUsage.Controls.Add(this.uxAK11IkincilFonksiyonlar);
            this.uxGBHygenicValveUsage.Controls.Add(this.label47);
            this.uxGBHygenicValveUsage.Controls.Add(this.label44);
            this.uxGBHygenicValveUsage.Controls.Add(this.label48);
            this.uxGBHygenicValveUsage.Controls.Add(this.uxTBoxSuTuketimMiktari);
            this.uxGBHygenicValveUsage.Controls.Add(this.label46);
            this.uxGBHygenicValveUsage.Controls.Add(this.label45);
            this.uxGBHygenicValveUsage.Controls.Add(this.uxTBoxSuTuketimSuresi);
            this.uxGBHygenicValveUsage.Location = new System.Drawing.Point(4, 71);
            this.uxGBHygenicValveUsage.Name = "uxGBHygenicValveUsage";
            this.uxGBHygenicValveUsage.Size = new System.Drawing.Size(254, 133);
            this.uxGBHygenicValveUsage.TabIndex = 77;
            this.uxGBHygenicValveUsage.TabStop = false;
            this.uxGBHygenicValveUsage.Text = "Günlük Su";
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(10, 97);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(149, 13);
            this.label43.TabIndex = 26;
            this.label43.Text = "Vana Açılma Süresi :";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBoxVanaAcilmaSuresi
            // 
            this.uxTBoxVanaAcilmaSuresi.Location = new System.Drawing.Point(164, 94);
            this.uxTBoxVanaAcilmaSuresi.Name = "uxTBoxVanaAcilmaSuresi";
            this.uxTBoxVanaAcilmaSuresi.Size = new System.Drawing.Size(41, 20);
            this.uxTBoxVanaAcilmaSuresi.TabIndex = 22;
            this.uxTBoxVanaAcilmaSuresi.Text = "1";
            // 
            // uxAK11IkincilFonksiyonlar
            // 
            this.uxAK11IkincilFonksiyonlar.AutoSize = true;
            this.uxAK11IkincilFonksiyonlar.Location = new System.Drawing.Point(34, 30);
            this.uxAK11IkincilFonksiyonlar.Name = "uxAK11IkincilFonksiyonlar";
            this.uxAK11IkincilFonksiyonlar.Size = new System.Drawing.Size(136, 17);
            this.uxAK11IkincilFonksiyonlar.TabIndex = 73;
            this.uxAK11IkincilFonksiyonlar.Text = "Günlük su tüketimi aktif";
            this.toolTip1.SetToolTip(this.uxAK11IkincilFonksiyonlar, "AK-11 Gunluk Su tuketim Ikincil Fonksiyonlari - Tarife 1: Suresi, Tarife2:Miktari" +
        " , tarife3: vana acilma suresi");
            this.uxAK11IkincilFonksiyonlar.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(208, 78);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(33, 13);
            this.label47.TabIndex = 28;
            this.label47.Text = "100 lt";
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(10, 77);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(149, 13);
            this.label44.TabIndex = 25;
            this.label44.Text = "Günlük Su Tüketim Miktarı :";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(208, 97);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 13);
            this.label48.TabIndex = 29;
            this.label48.Text = "100 ms";
            // 
            // uxTBoxSuTuketimMiktari
            // 
            this.uxTBoxSuTuketimMiktari.Location = new System.Drawing.Point(164, 74);
            this.uxTBoxSuTuketimMiktari.Name = "uxTBoxSuTuketimMiktari";
            this.uxTBoxSuTuketimMiktari.Size = new System.Drawing.Size(41, 20);
            this.uxTBoxSuTuketimMiktari.TabIndex = 21;
            this.uxTBoxSuTuketimMiktari.Text = "1";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(210, 58);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(23, 13);
            this.label46.TabIndex = 27;
            this.label46.Text = "min";
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(10, 57);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(149, 13);
            this.label45.TabIndex = 24;
            this.label45.Text = "Günlük Su Tüketim Süresi :";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxTBoxSuTuketimSuresi
            // 
            this.uxTBoxSuTuketimSuresi.Location = new System.Drawing.Point(164, 54);
            this.uxTBoxSuTuketimSuresi.Name = "uxTBoxSuTuketimSuresi";
            this.uxTBoxSuTuketimSuresi.Size = new System.Drawing.Size(41, 20);
            this.uxTBoxSuTuketimSuresi.TabIndex = 20;
            this.uxTBoxSuTuketimSuresi.Text = "1";
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox10.Controls.Add(this.cbLanguage);
            this.groupBox10.Location = new System.Drawing.Point(5, 5);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(490, 60);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Dil Tercihi";
            // 
            // cbLanguage
            // 
            this.cbLanguage.FormattingEnabled = true;
            this.cbLanguage.Items.AddRange(new object[] {
            "Türkçe",
            "English",
            "Slovakian",
            "Hungarian"});
            this.cbLanguage.Location = new System.Drawing.Point(9, 23);
            this.cbLanguage.Name = "cbLanguage";
            this.cbLanguage.Size = new System.Drawing.Size(121, 21);
            this.cbLanguage.TabIndex = 0;
            this.cbLanguage.SelectedValueChanged += new System.EventHandler(this.cbLanguage_SelectedValueChanged);
            // 
            // uxTPSubAuth
            // 
            this.uxTPSubAuth.Controls.Add(this.uxBTNCreateSubAuthCard);
            this.uxTPSubAuth.Controls.Add(this.uxNUDAuthNo);
            this.uxTPSubAuth.Controls.Add(this.uxTBAuthNo);
            this.uxTPSubAuth.Controls.Add(this.label58);
            this.uxTPSubAuth.Controls.Add(this.uxTBIdareAdi);
            this.uxTPSubAuth.Controls.Add(this.label57);
            this.uxTPSubAuth.Location = new System.Drawing.Point(4, 22);
            this.uxTPSubAuth.Name = "uxTPSubAuth";
            this.uxTPSubAuth.Padding = new System.Windows.Forms.Padding(3);
            this.uxTPSubAuth.Size = new System.Drawing.Size(609, 264);
            this.uxTPSubAuth.TabIndex = 8;
            this.uxTPSubAuth.Text = "Sub authority";
            this.uxTPSubAuth.UseVisualStyleBackColor = true;
            // 
            // uxBTNCreateSubAuthCard
            // 
            this.uxBTNCreateSubAuthCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBTNCreateSubAuthCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxBTNCreateSubAuthCard.ForeColor = System.Drawing.Color.SteelBlue;
            this.uxBTNCreateSubAuthCard.Location = new System.Drawing.Point(16, 174);
            this.uxBTNCreateSubAuthCard.Name = "uxBTNCreateSubAuthCard";
            this.uxBTNCreateSubAuthCard.Size = new System.Drawing.Size(221, 23);
            this.uxBTNCreateSubAuthCard.TabIndex = 69;
            this.uxBTNCreateSubAuthCard.Text = "Create Sub Auth. Card";
            this.uxBTNCreateSubAuthCard.UseVisualStyleBackColor = true;
            this.uxBTNCreateSubAuthCard.Click += new System.EventHandler(this.uxBTNCreateSubAuthCard_Click);
            // 
            // uxNUDAuthNo
            // 
            this.uxNUDAuthNo.Location = new System.Drawing.Point(116, 100);
            this.uxNUDAuthNo.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.uxNUDAuthNo.Name = "uxNUDAuthNo";
            this.uxNUDAuthNo.Size = new System.Drawing.Size(120, 20);
            this.uxNUDAuthNo.TabIndex = 68;
            this.uxNUDAuthNo.ValueChanged += new System.EventHandler(this.uxNUDAuthNo_ValueChanged);
            // 
            // uxTBAuthNo
            // 
            this.uxTBAuthNo.Location = new System.Drawing.Point(16, 123);
            this.uxTBAuthNo.Maximum = 255;
            this.uxTBAuthNo.Name = "uxTBAuthNo";
            this.uxTBAuthNo.Size = new System.Drawing.Size(517, 45);
            this.uxTBAuthNo.TabIndex = 67;
            this.uxTBAuthNo.ValueChanged += new System.EventHandler(this.uxTBAuthNo_ValueChanged);
            // 
            // label58
            // 
            this.label58.Location = new System.Drawing.Point(13, 102);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(100, 13);
            this.label58.TabIndex = 66;
            this.label58.Text = "Authority Number :";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // uxTBIdareAdi
            // 
            this.uxTBIdareAdi.Location = new System.Drawing.Point(116, 67);
            this.uxTBIdareAdi.MaxLength = 14;
            this.uxTBIdareAdi.Name = "uxTBIdareAdi";
            this.uxTBIdareAdi.Size = new System.Drawing.Size(417, 20);
            this.uxTBIdareAdi.TabIndex = 64;
            // 
            // label57
            // 
            this.label57.Location = new System.Drawing.Point(13, 71);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(100, 13);
            this.label57.TabIndex = 65;
            this.label57.Text = "Authority Name :";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.uxBTNOpenCreditTransaction);
            this.tabPage1.Controls.Add(this.uxBTNConvertMeterType);
            this.tabPage1.Controls.Add(this.label60);
            this.tabPage1.Controls.Add(this.uxLBLCurrentMeterType);
            this.tabPage1.Controls.Add(this.label59);
            this.tabPage1.Controls.Add(this.uxCBNewMeterType);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(609, 264);
            this.tabPage1.TabIndex = 9;
            this.tabPage1.Text = "Tip Değiştir";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // uxBTNOpenCreditTransaction
            // 
            this.uxBTNOpenCreditTransaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBTNOpenCreditTransaction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxBTNOpenCreditTransaction.ForeColor = System.Drawing.Color.SteelBlue;
            this.uxBTNOpenCreditTransaction.Location = new System.Drawing.Point(27, 222);
            this.uxBTNOpenCreditTransaction.Name = "uxBTNOpenCreditTransaction";
            this.uxBTNOpenCreditTransaction.Size = new System.Drawing.Size(138, 23);
            this.uxBTNOpenCreditTransaction.TabIndex = 42;
            this.uxBTNOpenCreditTransaction.Text = "Kredi Yüklemeyi Aç";
            this.uxBTNOpenCreditTransaction.UseVisualStyleBackColor = true;
            this.uxBTNOpenCreditTransaction.Click += new System.EventHandler(this.uxBTNOpenCreditTransaction_Click);
            // 
            // uxBTNConvertMeterType
            // 
            this.uxBTNConvertMeterType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBTNConvertMeterType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxBTNConvertMeterType.ForeColor = System.Drawing.Color.SteelBlue;
            this.uxBTNConvertMeterType.Location = new System.Drawing.Point(169, 94);
            this.uxBTNConvertMeterType.Name = "uxBTNConvertMeterType";
            this.uxBTNConvertMeterType.Size = new System.Drawing.Size(138, 23);
            this.uxBTNConvertMeterType.TabIndex = 41;
            this.uxBTNConvertMeterType.Text = "Sayaç Tipini Çevir";
            this.uxBTNConvertMeterType.UseVisualStyleBackColor = true;
            this.uxBTNConvertMeterType.Click += new System.EventHandler(this.uxBTNConvertMeterType_Click);
            // 
            // label60
            // 
            this.label60.Location = new System.Drawing.Point(8, 70);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(157, 17);
            this.label60.TabIndex = 40;
            this.label60.Text = "Yeni Sayac Modeli:";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxLBLCurrentMeterType
            // 
            this.uxLBLCurrentMeterType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uxLBLCurrentMeterType.ForeColor = System.Drawing.Color.Red;
            this.uxLBLCurrentMeterType.Location = new System.Drawing.Point(171, 40);
            this.uxLBLCurrentMeterType.Name = "uxLBLCurrentMeterType";
            this.uxLBLCurrentMeterType.Size = new System.Drawing.Size(136, 17);
            this.uxLBLCurrentMeterType.TabIndex = 39;
            this.uxLBLCurrentMeterType.Text = "AK-?";
            this.uxLBLCurrentMeterType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.Location = new System.Drawing.Point(8, 40);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(157, 17);
            this.label59.TabIndex = 38;
            this.label59.Text = "Mevcut Sayac Modeli:";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxCBNewMeterType
            // 
            this.uxCBNewMeterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxCBNewMeterType.FormattingEnabled = true;
            this.uxCBNewMeterType.Items.AddRange(new object[] {
            "AK-111",
            "AK-211",
            "Isi Sayaci",
            "AK-211 Ortak Kullanim",
            "GPRSli Kartli Sayac",
            "AK-311",
            "AK-211(BUSKI)",
            "Isi Sayaci Sicak Soguk",
            "AK-311 Ortak Kullanim"});
            this.uxCBNewMeterType.Location = new System.Drawing.Point(169, 67);
            this.uxCBNewMeterType.Name = "uxCBNewMeterType";
            this.uxCBNewMeterType.Size = new System.Drawing.Size(138, 21);
            this.uxCBNewMeterType.TabIndex = 37;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(247, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Tarife 4:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label28.Location = new System.Drawing.Point(8, 160);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 13);
            this.label28.TabIndex = 40;
            this.label28.Text = "Kademe 4:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(247, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Tarife 2:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // uxDonem
            // 
            this.uxDonem.FormattingEnabled = true;
            this.uxDonem.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.uxDonem.Location = new System.Drawing.Point(351, 31);
            this.uxDonem.Name = "uxDonem";
            this.uxDonem.Size = new System.Drawing.Size(73, 21);
            this.uxDonem.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(247, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Tarife 1:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.button1.Location = new System.Drawing.Point(173, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "AzersuTest";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // uxBTNDec10m3
            // 
            this.uxBTNDec10m3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBTNDec10m3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.uxBTNDec10m3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.uxBTNDec10m3.Location = new System.Drawing.Point(173, 105);
            this.uxBTNDec10m3.Name = "uxBTNDec10m3";
            this.uxBTNDec10m3.Size = new System.Drawing.Size(76, 23);
            this.uxBTNDec10m3.TabIndex = 6;
            this.uxBTNDec10m3.Text = "dec10m3";
            this.uxBTNDec10m3.UseVisualStyleBackColor = true;
            this.uxBTNDec10m3.Visible = false;
            this.uxBTNDec10m3.Click += new System.EventHandler(this.uxBTNDec10m3_Click);
            // 
            // BTNLoadNewCardReader
            // 
            this.BTNLoadNewCardReader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BTNLoadNewCardReader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.BTNLoadNewCardReader.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.BTNLoadNewCardReader.Location = new System.Drawing.Point(439, 4);
            this.BTNLoadNewCardReader.Name = "BTNLoadNewCardReader";
            this.BTNLoadNewCardReader.Size = new System.Drawing.Size(64, 23);
            this.BTNLoadNewCardReader.TabIndex = 6;
            this.BTNLoadNewCardReader.Text = "Yenile";
            this.BTNLoadNewCardReader.UseVisualStyleBackColor = true;
            this.BTNLoadNewCardReader.Click += new System.EventHandler(this.BTNLoadNewCardReader_Click);
            // 
            // txtTarife2
            // 
            this.txtTarife2.Location = new System.Drawing.Point(352, 115);
            this.txtTarife2.Name = "txtTarife2";
            this.txtTarife2.Size = new System.Drawing.Size(138, 20);
            this.txtTarife2.TabIndex = 22;
            this.txtTarife2.Text = "1";
            // 
            // txtKademe5
            // 
            this.txtKademe5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtKademe5.Location = new System.Drawing.Point(104, 176);
            this.txtKademe5.Name = "txtKademe5";
            this.txtKademe5.Size = new System.Drawing.Size(65, 20);
            this.txtKademe5.TabIndex = 15;
            this.txtKademe5.Text = "969";
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label27.Location = new System.Drawing.Point(8, 180);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 13);
            this.label27.TabIndex = 42;
            this.label27.Text = "Kademe 5:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btSeriNoOku
            // 
            this.btSeriNoOku.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btSeriNoOku.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSeriNoOku.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSeriNoOku.ForeColor = System.Drawing.Color.SteelBlue;
            this.btSeriNoOku.Location = new System.Drawing.Point(343, 4);
            this.btSeriNoOku.Name = "btSeriNoOku";
            this.btSeriNoOku.Size = new System.Drawing.Size(83, 23);
            this.btSeriNoOku.TabIndex = 1;
            this.btSeriNoOku.Text = "Seri No";
            this.btSeriNoOku.UseVisualStyleBackColor = false;
            this.btSeriNoOku.Click += new System.EventHandler(this.btSeriNoOku_Click);
            // 
            // TB_KartAnaKredi
            // 
            this.TB_KartAnaKredi.Location = new System.Drawing.Point(104, 241);
            this.TB_KartAnaKredi.Name = "TB_KartAnaKredi";
            this.TB_KartAnaKredi.Size = new System.Drawing.Size(79, 20);
            this.TB_KartAnaKredi.TabIndex = 29;
            this.TB_KartAnaKredi.Text = "0";
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(8, 266);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(93, 13);
            this.label34.TabIndex = 57;
            this.label34.Text = "Kart Yedek Kredi :";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label26.Location = new System.Drawing.Point(8, 200);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(93, 13);
            this.label26.TabIndex = 44;
            this.label26.Text = "Kademe 6:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(8, 245);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(93, 13);
            this.label35.TabIndex = 56;
            this.label35.Text = "Kart Ana Kredi :";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_KartYedekKredi
            // 
            this.TB_KartYedekKredi.Location = new System.Drawing.Point(104, 262);
            this.TB_KartYedekKredi.Name = "TB_KartYedekKredi";
            this.TB_KartYedekKredi.Size = new System.Drawing.Size(79, 20);
            this.TB_KartYedekKredi.TabIndex = 30;
            this.TB_KartYedekKredi.Text = "0";
            // 
            // DTPSayacTarihi
            // 
            this.DTPSayacTarihi.CustomFormat = "dd.MM.yyyy HH:mm:ss";
            this.DTPSayacTarihi.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTPSayacTarihi.Location = new System.Drawing.Point(352, 235);
            this.DTPSayacTarihi.Name = "DTPSayacTarihi";
            this.DTPSayacTarihi.Size = new System.Drawing.Size(136, 20);
            this.DTPSayacTarihi.TabIndex = 28;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(247, 238);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(101, 13);
            this.label33.TabIndex = 53;
            this.label33.Text = "Sayaç Tarihi:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(8, 223);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(93, 13);
            this.label32.TabIndex = 51;
            this.label32.Text = "Kart Tipi:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CMBAboneTipi
            // 
            this.CMBAboneTipi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CMBAboneTipi.FormattingEnabled = true;
            this.CMBAboneTipi.Items.AddRange(new object[] {
            "YeniAbone",
            "Zaman",
            "Test",
            "Kontrol",
            "Bos",
            "Yetki",
            "Reset",
            "Ortak Kullanim",
            "Yeni Sayac No",
            "Tuketim Kredi Ayar",
            "Avans Karti (Abseron)",
            "RF Ayar Karti",
            "Avans Reset Karti (Abseron)",
            "Toplayici Kontrol Karti"});
            this.CMBAboneTipi.Location = new System.Drawing.Point(104, 219);
            this.CMBAboneTipi.Name = "CMBAboneTipi";
            this.CMBAboneTipi.Size = new System.Drawing.Size(138, 21);
            this.CMBAboneTipi.TabIndex = 17;
            this.CMBAboneTipi.SelectedIndexChanged += new System.EventHandler(this.CMBAboneTipi_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label29.Location = new System.Drawing.Point(247, 218);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 13);
            this.label29.TabIndex = 50;
            this.label29.Text = "Tarife 7:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTarife7
            // 
            this.txtTarife7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTarife7.Location = new System.Drawing.Point(352, 214);
            this.txtTarife7.Name = "txtTarife7";
            this.txtTarife7.Size = new System.Drawing.Size(138, 20);
            this.txtTarife7.TabIndex = 27;
            this.txtTarife7.Text = "1";
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label30.Location = new System.Drawing.Point(247, 198);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(101, 13);
            this.label30.TabIndex = 48;
            this.label30.Text = "Tarife 6:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTarife6
            // 
            this.txtTarife6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTarife6.Location = new System.Drawing.Point(352, 194);
            this.txtTarife6.Name = "txtTarife6";
            this.txtTarife6.Size = new System.Drawing.Size(138, 20);
            this.txtTarife6.TabIndex = 26;
            this.txtTarife6.Text = "1";
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.label31.Location = new System.Drawing.Point(247, 178);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(101, 13);
            this.label31.TabIndex = 46;
            this.label31.Text = "Tarife 5:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTarife5
            // 
            this.txtTarife5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTarife5.Location = new System.Drawing.Point(352, 174);
            this.txtTarife5.Name = "txtTarife5";
            this.txtTarife5.Size = new System.Drawing.Size(138, 20);
            this.txtTarife5.TabIndex = 25;
            this.txtTarife5.Text = "1";
            // 
            // btIade
            // 
            this.btIade.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btIade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btIade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btIade.ForeColor = System.Drawing.Color.SteelBlue;
            this.btIade.Location = new System.Drawing.Point(257, 4);
            this.btIade.Name = "btIade";
            this.btIade.Size = new System.Drawing.Size(83, 23);
            this.btIade.TabIndex = 3;
            this.btIade.Text = "İade";
            this.btIade.UseVisualStyleBackColor = false;
            this.btIade.Click += new System.EventHandler(this.btIade_Click);
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(247, 76);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(101, 13);
            this.label25.TabIndex = 36;
            this.label25.Text = "Sayac Modeli:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // CBSayacModeli
            // 
            this.CBSayacModeli.BackColor = System.Drawing.Color.White;
            this.CBSayacModeli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBSayacModeli.ForeColor = System.Drawing.SystemColors.InfoText;
            this.CBSayacModeli.FormattingEnabled = true;
            this.CBSayacModeli.Items.AddRange(new object[] {
            "AK-111",
            "AK-211",
            "Isi Sayaci",
            "AK-211 Ortak Kullanim",
            "GPRSli Kartli Sayac",
            "AK-311",
            "AK-211(BUSKI)",
            "Isi Sayaci Sicak Soguk",
            "AK-311 Ortak Kullanim"});
            this.CBSayacModeli.Location = new System.Drawing.Point(352, 72);
            this.CBSayacModeli.Name = "CBSayacModeli";
            this.CBSayacModeli.Size = new System.Drawing.Size(138, 21);
            this.CBSayacModeli.TabIndex = 20;
            this.CBSayacModeli.SelectedValueChanged += new System.EventHandler(this.CBSayacModeli_SelectedValueChanged);
            // 
            // btnSil
            // 
            this.btnSil.BackColor = System.Drawing.Color.Red;
            this.btnSil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSil.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSil.ForeColor = System.Drawing.Color.Yellow;
            this.btnSil.Location = new System.Drawing.Point(172, 4);
            this.btnSil.Name = "btnSil";
            this.btnSil.Size = new System.Drawing.Size(83, 23);
            this.btnSil.TabIndex = 2;
            this.btnSil.Text = "Kart Sil";
            this.btnSil.UseVisualStyleBackColor = false;
            this.btnSil.Click += new System.EventHandler(this.btnSil_Click);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(247, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(101, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Kritik Kredi:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmpKritikKredi
            // 
            this.cmpKritikKredi.Location = new System.Drawing.Point(352, 51);
            this.cmpKritikKredi.Name = "cmpKritikKredi";
            this.cmpKritikKredi.Size = new System.Drawing.Size(72, 20);
            this.cmpKritikKredi.TabIndex = 19;
            this.cmpKritikKredi.Text = "00";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(247, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Dönem:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Kademe 3:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKademe3
            // 
            this.txtKademe3.Location = new System.Drawing.Point(104, 135);
            this.txtKademe3.Name = "txtKademe3";
            this.txtKademe3.Size = new System.Drawing.Size(65, 20);
            this.txtKademe3.TabIndex = 13;
            this.txtKademe3.Text = "969";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Kademe 2:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKademe2
            // 
            this.txtKademe2.Location = new System.Drawing.Point(104, 115);
            this.txtKademe2.Name = "txtKademe2";
            this.txtKademe2.Size = new System.Drawing.Size(65, 20);
            this.txtKademe2.TabIndex = 12;
            this.txtKademe2.Text = "969";
            // 
            // btWrite
            // 
            this.btWrite.BackColor = System.Drawing.Color.Green;
            this.btWrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btWrite.ForeColor = System.Drawing.Color.White;
            this.btWrite.Location = new System.Drawing.Point(88, 4);
            this.btWrite.Name = "btWrite";
            this.btWrite.Size = new System.Drawing.Size(83, 23);
            this.btWrite.TabIndex = 4;
            this.btWrite.Text = "Yaz";
            this.btWrite.UseVisualStyleBackColor = false;
            this.btWrite.Click += new System.EventHandler(this.btWrite_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Kademe 1:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Abone Tipi :";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Sayaç No:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Abone No:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtKademe1
            // 
            this.txtKademe1.Location = new System.Drawing.Point(104, 95);
            this.txtKademe1.Name = "txtKademe1";
            this.txtKademe1.Size = new System.Drawing.Size(65, 20);
            this.txtKademe1.TabIndex = 11;
            this.txtKademe1.Text = "969";
            // 
            // txtAboneTipi
            // 
            this.txtAboneTipi.Location = new System.Drawing.Point(104, 71);
            this.txtAboneTipi.Name = "txtAboneTipi";
            this.txtAboneTipi.Size = new System.Drawing.Size(138, 20);
            this.txtAboneTipi.TabIndex = 10;
            this.txtAboneTipi.Text = "IDARETEST";
            // 
            // txtSayacNo
            // 
            this.txtSayacNo.Location = new System.Drawing.Point(104, 51);
            this.txtSayacNo.Name = "txtSayacNo";
            this.txtSayacNo.Size = new System.Drawing.Size(138, 20);
            this.txtSayacNo.TabIndex = 9;
            this.txtSayacNo.Text = "19135";
            // 
            // txtAboneNo
            // 
            this.txtAboneNo.Location = new System.Drawing.Point(104, 31);
            this.txtAboneNo.Name = "txtAboneNo";
            this.txtAboneNo.Size = new System.Drawing.Size(138, 20);
            this.txtAboneNo.TabIndex = 8;
            this.txtAboneNo.Text = "35363738";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txOutput);
            this.panel2.Controls.Add(this.progressBar1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(617, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(579, 629);
            this.panel2.TabIndex = 3;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar1.Location = new System.Drawing.Point(0, 606);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(579, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 629);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1196, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbStatus
            // 
            this.lbStatus.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.Color.SlateGray;
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(133, 17);
            this.lbStatus.Text = "Baylan EKS Kart Test";
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(8, 307);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(93, 13);
            this.label37.TabIndex = 84;
            this.label37.Text = "Toplam Tüketim :";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_SayacTuketim
            // 
            this.TB_SayacTuketim.Location = new System.Drawing.Point(104, 304);
            this.TB_SayacTuketim.Name = "TB_SayacTuketim";
            this.TB_SayacTuketim.Size = new System.Drawing.Size(79, 20);
            this.TB_SayacTuketim.TabIndex = 83;
            this.TB_SayacTuketim.Text = "0";
            // 
            // mainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 651);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Baylan EKS Kart Test";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainFrm_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpLoadCredits.ResumeLayout(false);
            this.tpLoadCredits.PerformLayout();
            this.tpTest.ResumeLayout(false);
            this.tpTest.PerformLayout();
            this.testContextMenu.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxOkumaAdedi)).EndInit();
            this.tpParameters1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmpYanginModuSaat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmpYanginModuDakika)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tpParameters2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.uxAK21KartStatus.ResumeLayout(false);
            this.uxAK11KartStatus.ResumeLayout(false);
            this.uxTPParameters3.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxTBTimeData)).EndInit();
            this.tpUsers.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.tpSettings.ResumeLayout(false);
            this.uxGBRFParametreler.ResumeLayout(false);
            this.uxGBRFParametreler.PerformLayout();
            this.uxGBHygenicValveUsage.ResumeLayout(false);
            this.uxGBHygenicValveUsage.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.uxTPSubAuth.ResumeLayout(false);
            this.uxTPSubAuth.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxNUDAuthNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uxTBAuthNo)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btRead;
        private System.Windows.Forms.TextBox txOutput;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtKademe1;
        private System.Windows.Forms.TextBox txtAboneTipi;
        private System.Windows.Forms.TextBox txtSayacNo;
        private System.Windows.Forms.TextBox txtAboneNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btWrite;
        private System.Windows.Forms.Label uxLBYedekKredi;
        private System.Windows.Forms.TextBox txtYedekKredi;
        private System.Windows.Forms.Label uxLBAnaKredi;
        private System.Windows.Forms.TextBox txtAnaKredi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtKademe3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtKademe2;
        private System.Windows.Forms.Button btnKontorSil;
        private System.Windows.Forms.Button btnKontorEkle;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox cmpKritikKredi;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox cmpMaxDebiEsikDegeri;
        private System.Windows.Forms.ComboBox cmpSayacCapi;
        private System.Windows.Forms.DateTimePicker cmpKesintisizBitis;
        private System.Windows.Forms.DateTimePicker cmpKesintisizBaslangic;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown cmpYanginModuDakika;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown cmpYanginModuSaat;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnSil;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox CBSayacModeli;
        private System.Windows.Forms.Button btIade;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtTarife7;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtTarife6;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtTarife5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtKademe6;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtKademe5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtKademe4;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox CMBAboneTipi;
        private System.Windows.Forms.DateTimePicker DTPSayacTarihi;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox TB_KartAnaKredi;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox TB_KartYedekKredi;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox uxIadeEdilecekKredi;
        private System.Windows.Forms.TextBox uxKartStatus;
        private System.Windows.Forms.CheckBox uxSendAsByte;
        private System.Windows.Forms.Button btSeriNoOku;
        private System.Windows.Forms.Button BTNLoadNewCardReader;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown uxOkumaAdedi;
        private System.Windows.Forms.Button btTestCard;
        private System.Windows.Forms.DateTimePicker uxSayacKapatmaTarihi;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox uxRepatedOperationType2;
        private System.Windows.Forms.ComboBox uxRepatedOperationType1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.CheckBox uxCBSurekli;
        private System.Windows.Forms.ComboBox uxDonem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtTarife4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTarife3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTarife2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTarife1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpLoadCredits;
        private System.Windows.Forms.TabPage tpTest;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lbStatus;
        private System.Windows.Forms.TabPage tpParameters1;
        private System.Windows.Forms.TabPage tpParameters2;
        private System.Windows.Forms.GroupBox uxAK21KartStatus;
        private System.Windows.Forms.CheckBox uxAK21PilKapagiCezasi;
        private System.Windows.Forms.CheckBox uxAK21UstKapakCeza;
        private System.Windows.Forms.CheckBox uxAK21ManyetikCeza;
        private System.Windows.Forms.CheckBox uxAK21RekorCeza;
        private System.Windows.Forms.CheckBox uxAK21HaftaSonuMesaiSayacAcik;
        private System.Windows.Forms.CheckBox uxAK21ResmiTatilVanaAcik;
        private System.Windows.Forms.CheckBox uxAK21TarihdeVanaKapat;
        private System.Windows.Forms.CheckBox uxAK21KesintisizSu;
        private System.Windows.Forms.GroupBox uxAK11KartStatus;
        private System.Windows.Forms.CheckBox uxAK11HaftaSonuMesaiDisiVanaAcik;
        private System.Windows.Forms.CheckBox uxAK11PilUzat;
        private System.Windows.Forms.CheckBox uxAK11AydaIkiVanaAcmaKapama;
        private System.Windows.Forms.CheckBox uxAK11PilKisalt;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txCardHealth;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TabPage tpUsers;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.ListBox lstKullanicilar;
        private System.Windows.Forms.Button btSaveUserList;
        private System.Windows.Forms.Button btDeleteUser;
        private System.Windows.Forms.Button btAddNewUser;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ContextMenuStrip testContextMenu;
        private System.Windows.Forms.ToolStripMenuItem clearMenuToolstrip;
        private System.Windows.Forms.ToolStripMenuItem saveResultsToolstrip;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox cbLanguage;
        private System.Windows.Forms.Button btQueryWaterworks;
        private System.Windows.Forms.GroupBox uxGBHygenicValveUsage;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox uxTBoxVanaAcilmaSuresi;
        private System.Windows.Forms.CheckBox uxAK11IkincilFonksiyonlar;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox uxTBoxSuTuketimMiktari;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox uxTBoxSuTuketimSuresi;
        private System.Windows.Forms.Button uxBTNCopyCardToClipboard;
        private System.Windows.Forms.TabPage uxTPParameters3;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label uxLBLTimeType;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TrackBar uxTBTimeData;
        private System.Windows.Forms.ComboBox uxCBTimeType;
        private System.Windows.Forms.Label uxLBLTrackBarValue;
        private System.Windows.Forms.CheckBox uxCBLongAboneNo;
        private System.Windows.Forms.Button uxBTNSilVeYukle;
        private System.Windows.Forms.Button uxBTNDec10m3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label uxLBLFreq2;
        private System.Windows.Forms.Label uxLBLFreq1;
        private System.Windows.Forms.Label uxLBLFreq0;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox uxTBFreq3;
        private System.Windows.Forms.TextBox uxTBFreq1;
        private System.Windows.Forms.TextBox uxTBFreq0;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox uxGBRFParametreler;
        private System.Windows.Forms.ComboBox uxCBRFTipi;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.RadioButton uxRBRFPasif;
        private System.Windows.Forms.RadioButton uxRBRFAktif;
        private System.Windows.Forms.ComboBox uxCBRFZamanTipi;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox uxTBRFSure;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox uxTBRFFreq2;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox uxTBRFFreq1;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox uxTBRFFreq0;
        private System.Windows.Forms.Label uxLBLAvansKredi;
        private System.Windows.Forms.TextBox uxTBAvansKredi;
        private System.Windows.Forms.TabPage uxTPSubAuth;
        private System.Windows.Forms.Button uxBTNCreateSubAuthCard;
        private System.Windows.Forms.NumericUpDown uxNUDAuthNo;
        private System.Windows.Forms.TrackBar uxTBAuthNo;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox uxTBIdareAdi;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button uxBTNConvertMeterType;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label uxLBLCurrentMeterType;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ComboBox uxCBNewMeterType;
        private System.Windows.Forms.Panel uxPNLImageContainer;
        private System.Windows.Forms.CheckBox uxCBIsiSayaci;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox uxTBKritikKredi;
        private System.Windows.Forms.CheckBox uxCBZeroTariffs;
        private System.Windows.Forms.Button uxBTNEjectCrtReader;
        private System.Windows.Forms.Button uxBTNOpenCreditTransaction;
        private System.Windows.Forms.CheckBox uxCBDSIKapatma;
        private System.Windows.Forms.DateTimePicker uxDTPMakbuzNo;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox TB_SayacKredi;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox TB_SayacTuketim;
    }
}

