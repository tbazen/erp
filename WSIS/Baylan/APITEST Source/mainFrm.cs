﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Baylan;
using System.Diagnostics;
using System.Threading;
using EKS;
//using EKS_v2;
using System.Globalization;
using Baylan.TranslationLibrary;
using System.Drawing;
using System.Collections.Generic;
using InternalEKS_v1;
using Baylan.Parsers;

namespace ApiTest
{
    public partial class mainFrm : Form
    {
        EKSAPI _EKS;
        EKS_v3.RfZamanTipleri zamantipi;

        BackgroundWorker bw;
        int _padRightSize = 45;
        List<string> _meterTypes = new List<string>();
        Dictionary<string, int> _meterModelsByMeterName = new Dictionary<string, int>();
        Dictionary<int, string> _meterModelsByModelId = new Dictionary<int, string>();              

        IKartBilgisi_v3             _readKartBilgisi = new KartBilgisi();
        IYazilacakEkBilgi           _readYazilacakEkBilgi = new YazilacakEkBilgi();
        IGenelParametreler          _readGenelParametreler = new GenelParametreler();
        string                      _lastReadCardSerialNumber;

        public mainFrm()
        {
            _meterTypes.Add("AK-111");
            _meterModelsByMeterName.Add(_meterTypes[0], 2);
            _meterModelsByModelId.Add(2, _meterTypes[0]);

            _meterTypes.Add("AK-211");
            _meterModelsByMeterName.Add(_meterTypes[1], 1);
            _meterModelsByModelId.Add(1, _meterTypes[1]);

            _meterTypes.Add(localize.getStr("heatMeter"));
            _meterModelsByMeterName.Add(_meterTypes[2], 3);
            _meterModelsByModelId.Add(3, _meterTypes[2]);

            _meterTypes.Add(localize.getStr("AK21CommonUse"));
            _meterModelsByMeterName.Add(_meterTypes[3], 4);
            _meterModelsByModelId.Add(4, _meterTypes[3]);

            _meterTypes.Add(localize.getStr("GPRSmeterWithCard"));
            _meterModelsByMeterName.Add(_meterTypes[4], 5);
            _meterModelsByModelId.Add(5, _meterTypes[4]);

            _meterTypes.Add("AK-311");
            _meterModelsByMeterName.Add(_meterTypes[5], 6);
            _meterModelsByModelId.Add(6, _meterTypes[5]);

            _meterTypes.Add("AK-211(BUSKI)");
            _meterModelsByMeterName.Add(_meterTypes[6], 7);
            _meterModelsByModelId.Add(7, _meterTypes[6]);

            _meterTypes.Add("Isi Sayaci Sicak Soguk");
            _meterModelsByMeterName.Add(_meterTypes[7], 11);
            _meterModelsByModelId.Add(11, _meterTypes[7]);

            _meterTypes.Add("AK-311 ISI Sayaci/Heat Meter");
            _meterModelsByMeterName.Add(_meterTypes[8], 10);
            _meterModelsByModelId.Add(10, _meterTypes[8]);

            _meterTypes.Add(localize.getStr("AK21CommonUse").Replace("21", "311"));
            _meterModelsByMeterName.Add(_meterTypes[9], 9);
            _meterModelsByModelId.Add(9, _meterTypes[9]);

            _meterTypes.Add("AK-311 ISKRA");
            _meterModelsByMeterName.Add(_meterTypes[10], 12);
            _meterModelsByModelId.Add(12, _meterTypes[10]);
           
            _meterTypes.Add("AK-411");
            _meterModelsByMeterName.Add(_meterTypes[11], 20);
            _meterModelsByModelId.Add(20, _meterTypes[11]);

            _meterTypes.Add("US-411");
            _meterModelsByMeterName.Add(_meterTypes[12], 21);
            _meterModelsByModelId.Add(21, _meterTypes[12]);

            _meterModelsByModelId.Add(0, "");          
            InitializeComponent();         
            uxRepatedOperationType2.SelectedIndex = 0;
            uxRepatedOperationType1.SelectedIndex = 0;      
            EKS_v2.Logger.StartTiming("EKSAPI Create Instance");

            uxLBLAvansKredi.Visible = false;
            uxTBAvansKredi.Visible = false;

            DisableEnableSubAuthControls(false);
            uxBTNConvertMeterType.Enabled = false;

            uxCBNewMeterType.Items.Clear();
            uxCBNewMeterType.Enabled = false;

            try
            {
                _EKS = new EKSAPI();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
                staticClass.log("Could not initialize Baylan Card Library !!!!!", priority.error);              
            }
            EKS_v2.Logger.StopTiming();

            bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);

            _EKS.SayacTipi = EKS.SayacTipi.SogukSu;
            _EKS.SayacModeliId = 0;//2; //1:AK-21, 2:AK-11
            CMBAboneTipi.SelectedIndex = 0;
                       
            clStatic.qEvent.ehQuitEvent += new EventHandler(qEvent_ehQuitEvent);
            uxSayacKapatmaTarihi.Value = DateTime.Now.Subtract(new TimeSpan(24, 0, 0));
            if (staticClass.settings.settingsFound)
            {
                _EKS.KartBilgisi_v3.KartTuru_v2 = KartTuru_v2.TestKarti; // KartTuru_v2.TestKarti = 64
            }
            FixGUI();
            authorize(staticClass.loggedInUser);
            cbLanguage.Items.Clear();
            cbLanguage.Items.Add(languages.english);
            cbLanguage.Items.Add(languages.turkce);
            cbLanguage.Items.Add(languages.slovak);
            cbLanguage.Items.Add(languages.hungarian);

            staticClass.visualExceptionHandler += staticClass_visualExceptionHandler;
        }
        string[] GetWaterMeters(string exception)
        {
            List<string> newList = new List<string>();
            foreach (var item in _meterTypes)
            {
                newList.Add(item);
                /*
                if (item != exception)
                {
                    newList.Add(item);
                }
                else
                {
                    continue; 
                }*/
            }
            return newList.ToArray();
        }
        void staticClass_visualExceptionHandler(string exceptionMsg)
        {
            MessageBox.Show("The message received : " + exceptionMsg);
        }
        public void FixGUI()
        {
            this.Text = localize.getStr("baylanCardReaderTestUtility");
            this.btRead.Text = localize.getStr("read");
            this.btWrite.Text = localize.getStr("write");
            this.btnSil.Text = localize.getStr("clear");
            this.btIade.Text = localize.getStr("remand");
            this.BTNLoadNewCardReader.Text = localize.getStr("reload");          
            this.btSeriNoOku.Text = localize.getStr("readSerial");

            this.label1.Text = localize.getStr("consumerNr");
            this.label2.Text = localize.getStr("waterMeterNr");
            this.label3.Text = localize.getStr("consumerType");
            this.label4.Text = localize.getStr("step1");
            this.label5.Text = localize.getStr("step2");
            this.label6.Text = localize.getStr("step3");
            this.label28.Text = localize.getStr("step4");
            this.label27.Text = localize.getStr("step5");
            this.label26.Text = localize.getStr("step6");

            this.label9.Text = localize.getStr("tariff1");
            this.label8.Text = localize.getStr("tariff2");
            this.label7.Text = localize.getStr("tariff3");
            this.label10.Text = localize.getStr("tariff4");
            this.label31.Text = localize.getStr("tariff5");
            this.label30.Text = localize.getStr("tariff6");
            this.label29.Text = localize.getStr("tariff7");
            this.label32.Text = localize.getStr("cardType");
            this.label35.Text = localize.getStr("mainCredits");
            this.label34.Text = localize.getStr("backupCredits");           
            this.label13.Text = localize.getStr("term");
            this.label14.Text = localize.getStr("criticalCredits");
            this.label25.Text = localize.getStr("waterMeterType");
            this.label33.Text = localize.getStr("waterMeterDate");

            groupBox4.Text = localize.getStr("remandProcess");
            this.label36.Text = localize.getStr("creditsToRemand");
            tpLoadCredits.Text = localize.getStr("creditProcess");
            uxLBAnaKredi.Text = localize.getStr("mainCredits");
            uxLBYedekKredi.Text = localize.getStr("backupCredits");
            label22.Text = localize.getStr("currencyPerUnit");
            label23.Text = localize.getStr("currencyPerUnit");
            btnKontorEkle.Text = localize.getStr("addCredits");
            btnKontorSil.Text = localize.getStr("clearCredits");
            tpTest.Text = localize.getStr("test");
            groupBox3.Text = localize.getStr("cardTestProcess");
            label38.Text = localize.getStr("times");
            label40.Text = localize.getStr("first");
            label41.Text = localize.getStr("later");
            btTestCard.Text = localize.getStr("execute");
            uxAK11IkincilFonksiyonlar.Text = localize.getStr("hygenicActive");
            label45.Text = localize.getStr("hygenicPeriod");
            label44.Text = localize.getStr("hygenicAmount");
            label43.Text = localize.getStr("hygenicValvePeriod");
            label46.Text = localize.getStr("min");
            tpParameters1.Text = localize.getStr("parameters1");
            //tabPage5.Text                 = localize.getStr("hygenicProperties");
            groupBox2.Text = localize.getStr("parameters");
            groupBox5.Text = localize.getStr("fireMode");
            label17.Text = localize.getStr("fireModePeriod");
            label20.Text = localize.getStr("hour");
            label21.Text = localize.getStr("min");
            groupBox1.Text = localize.getStr("continuousWaterFlow");
            uxCBSurekli.Text = localize.getStr("continuousWaterFlowActive");
            label16.Text = localize.getStr("continuousWaterFlowStartDate");
            label15.Text = localize.getStr("continuousWaterFlowEndDate");
            groupBox6.Text = localize.getStr("waterMeterClosingDate");
            label39.Text = localize.getStr("waterMeterClosingDate");
            tpParameters2.Text = localize.getStr("parameters2");
            uxAK21KartStatus.Text = localize.getStr("cardStatus");
            uxAK11KartStatus.Text = localize.getStr("ak11CardStatus");
            groupBox9.Text = localize.getStr("sendAsByte");
            uxAK21KesintisizSu.Text = localize.getStr("enableContinuousWaterFlow");
            uxAK21ResmiTatilVanaAcik.Text = localize.getStr("enableHolidays");
            uxAK21TarihdeVanaKapat.Text = localize.getStr("enableCloseValveOnSpecificDate");
            uxAK21HaftaSonuMesaiSayacAcik.Text = localize.getStr("enableWeekendActivity");
            uxAK21RekorCeza.Text = localize.getStr("enableFitting");
            uxAK21ManyetikCeza.Text = localize.getStr("enableMagneticInterference");
            uxAK21UstKapakCeza.Text = localize.getStr("enableMainCoverInterference");
            uxAK21PilKapagiCezasi.Text = localize.getStr("enableBatteryCoverInterference");
            uxAK11PilKisalt.Text = localize.getStr("shortenBatteryLife");
            uxAK11PilUzat.Text = localize.getStr("extendBatteryLife");
            uxAK11AydaIkiVanaAcmaKapama.Text = localize.getStr("closeValveTwiceAMonth");
            uxAK11HaftaSonuMesaiDisiVanaAcik.Text = localize.getStr("enableWeekendActivity");
            uxSendAsByte.Text = localize.getStr("sendCardStatesAsByte");
            tpUsers.Text = localize.getStr("users");
            groupBox7.Text = localize.getStr("usersAndPermissions");
            btAddNewUser.Text = localize.getStr("add");
            btDeleteUser.Text = localize.getStr("delete");
            btSaveUserList.Text = localize.getStr("save");
            btQueryWaterworks.Text = localize.getStr("queryWaterworks");
            tpSettings.Text = localize.getStr("settings");
            groupBox10.Text = localize.getStr("selectedLng");


            CMBAboneTipi.Items.Clear();
            CMBAboneTipi.Items.Add(localize.getStr("newConsumerCard"));
            CMBAboneTipi.Items.Add(localize.getStr("timeCard"));
            CMBAboneTipi.Items.Add(localize.getStr("testCard"));
            CMBAboneTipi.Items.Add(localize.getStr("controlCard"));
            CMBAboneTipi.Items.Add(localize.getStr("emptyCard"));
            CMBAboneTipi.Items.Add(localize.getStr("authorizationCard"));
            CMBAboneTipi.Items.Add(localize.getStr("resetCard"));
            CMBAboneTipi.Items.Add(localize.getStr("commonUseCard"));
            CMBAboneTipi.Items.Add(localize.getStr("meterNrUpdateCard"));
            CMBAboneTipi.Items.Add(localize.getStr("consumptionUpdateCard"));
            CMBAboneTipi.Items.Add("Avans Ayar Karti (Abseron)");
            CMBAboneTipi.Items.Add("RF Ayar Karti");
            CMBAboneTipi.Items.Add("Avans Reset Karti (Abseron)");
            CMBAboneTipi.Items.Add("Toplayici Kontrol Karti");



            CBSayacModeli.Items.Clear();
            CBSayacModeli.Items.AddRange(_meterTypes.ToArray());
            //CBSayacModeli.Items.Add("AK-111");
            //CBSayacModeli.Items.Add("AK-211");
            //CBSayacModeli.Items.Add(localize.getStr("heatMeter"));
            //CBSayacModeli.Items.Add(localize.getStr("AK21CommonUse"));
            //CBSayacModeli.Items.Add(localize.getStr("GPRSmeterWithCard"));
            //CBSayacModeli.Items.Add("AK-311");
            //CBSayacModeli.Items.Add("AK-211(BUSKI)");
            //CBSayacModeli.Items.Add("Isi Sayaci Sicak Soguk");
            //CBSayacModeli.Items.Add("AK-311 ISI Sayaci");
            //CBSayacModeli.Items.Add(localize.getStr("AK21CommonUse").Replace("21", "311"));

            uxRepatedOperationType1.Items.Clear();
            uxRepatedOperationType1.Items.Add(localize.getStr("pleaseSelect"));
            uxRepatedOperationType1.Items.Add(localize.getStr("readCard"));
            uxRepatedOperationType1.Items.Add(localize.getStr("clearCard"));
            uxRepatedOperationType1.Items.Add(localize.getStr("readSerial"));
            uxRepatedOperationType1.Items.Add(localize.getStr("createCard"));
            uxRepatedOperationType1.Items.Add(localize.getStr("reloadReader"));
            uxRepatedOperationType1.Items.Add(localize.getStr("loadCredits"));

            uxRepatedOperationType2.Items.Clear();
            uxRepatedOperationType2.Items.Add(localize.getStr("pleaseSelect"));
            uxRepatedOperationType2.Items.Add(localize.getStr("readCard"));
            uxRepatedOperationType2.Items.Add(localize.getStr("clearCard"));
            uxRepatedOperationType2.Items.Add(localize.getStr("readSerial"));
            uxRepatedOperationType2.Items.Add(localize.getStr("createCard"));
            uxRepatedOperationType2.Items.Add(localize.getStr("reloadReader"));
            uxRepatedOperationType2.Items.Add(localize.getStr("loadCredits"));

            txOutput.Text = localize.getStr("languageChangedEquals") + " " + staticClass.settings.languageSetting;
        }
        void qEvent_ehQuitEvent(object sender, EventArgs e)
        {
            Application.Exit();
        }
       
        public void authorize(user usr)
        {
            btRead.Enabled = usr.yetkiList.Contains(auth.card_read_permission);
            btSeriNoOku.Enabled = usr.yetkiList.Contains(auth.card_read_permission);
            btWrite.Enabled = usr.yetkiList.Contains(auth.card_write_permission);

            tpUsers.Enabled = usr.yetkiList.Contains(auth.settings_access_permission);
            tpSettings.Enabled = usr.yetkiList.Contains(auth.settings_access_permission);

            btIade.Enabled = usr.yetkiList.Contains(auth.refund_card_creation_permission);
            btnSil.Enabled = usr.yetkiList.Contains(auth.clear_card_permission);          
            btnKontorEkle.Enabled = usr.yetkiList.Contains(auth.load_credits_permission);
            btnKontorSil.Enabled = usr.yetkiList.Contains(auth.clear_credits_permission);

            tpParameters1.Enabled = usr.yetkiList.Contains(auth.update_parameters_permission);
            tpParameters2.Enabled = usr.yetkiList.Contains(auth.update_parameters_permission);
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {            
            if (progressBar1.InvokeRequired) {  progressBar1.Invoke((ThreadStart)delegate() { progressBar1.Value = progressBar1.Minimum; });
            }
            try
            {               
                if (_readYazilacakEkBilgi == null) { _readYazilacakEkBilgi = new Baylan.YazilacakEkBilgi(); }
                _readKartBilgisi = null;
                if (_readKartBilgisi == null) { _readKartBilgisi = new KartBilgisi(); }
                if (_readGenelParametreler == null) { _readGenelParametreler = new GenelParametreler(); }
                Stopwatch sw = new Stopwatch();
                sw.Start();              
                {                             
                    if (txOutput.InvokeRequired)
                    {
                        txOutput.Invoke((ThreadStart)delegate()
                        {
                            txOutput.Text = "";
                            if ((_EKS.SayacModeliId == 0 || _EKS.SayacModeliId == 255))
                            {
                                GetOperatorSelectedMeterType();
                            }
                        });
                    }

                    if (_EKS.SonIslemSonuc.Result != Baylan.Common.ResultTypes.Success)
                    {
                        MessageBox.Show(_EKS.SonIslemSonuc.Result.ToString() + " " + _EKS.SonIslemSonuc.ResultDescription);
                        _EKS.SonIslemSonuc.Result = Baylan.Common.ResultTypes.Success;
                    }
                    if (txOutput.InvokeRequired)
                    {
                        txOutput.Invoke((ThreadStart)delegate()
                        {
                            txOutput.AppendText("\n" + localize.getStr("cardReaderInitPeriod") + " " + sw.ElapsedMilliseconds + " ms" + Environment.NewLine);
                            lbStatus.Text = localize.getStr("cardReaderInitPeriod") + " " + sw.ElapsedMilliseconds + " ms";
                        });
                    }
                    else
                    {
                        txOutput.Invoke((ThreadStart)delegate()
                        {
                            txOutput.AppendText("\n" + localize.getStr("cardReaderInitPeriod") + " " + sw.ElapsedMilliseconds + " ms" + Environment.NewLine);
                            lbStatus.Text = localize.getStr("cardReaderInitPeriod") + " " + sw.ElapsedMilliseconds + " ms";
                        });
                    }
                    sw.Stop();
                    sw.Reset();                  
                    string message = "";                 
                    sw.Start();
                    _EKS.IsAK11ExtraFunctionality = uxAK11IkincilFonksiyonlar.Checked && uxAK11IkincilFonksiyonlar.Visible;

                    _EKS.ClearReaderData();
                    _EKS.SayacTipi = SayacTipi.SogukSu; 
                    //_EKS.SayacModeliId = 6; 
                     if (_EKS.KartOku())
                        {
                            if (_EKS.KartBilgisi_v4.SayacNo > 0)
                            {
                                if (this.InvokeRequired)
                                {
                                    this.Invoke((ThreadStart)delegate()
                                    {
                                        uxCBNewMeterType.Items.Clear();
                                        uxCBNewMeterType.Items.AddRange(GetWaterMeters(_meterModelsByModelId[_EKS.SayacModeliId]));
                                        uxCBNewMeterType.Enabled = true;
                                        uxBTNConvertMeterType.Enabled = true;
                                        uxLBLCurrentMeterType.Text = _meterModelsByModelId[_EKS.SayacModeliId];
                                    });
                                }
                                else
                                {
                                    uxCBNewMeterType.Items.Clear();
                                    uxCBNewMeterType.Items.AddRange(GetWaterMeters(_meterModelsByModelId[_EKS.SayacModeliId]));
                                    uxCBNewMeterType.Enabled = true;
                                    uxBTNConvertMeterType.Enabled = true;
                                    uxLBLCurrentMeterType.Text = _meterModelsByModelId[_EKS.SayacModeliId];
                                }
                            }                          
                            _lastReadCardSerialNumber    = _EKS.KartBilgisi.KartSeriNo;
                            _readKartBilgisi.AboneNo     = _EKS.KartBilgisi.AboneNo;
                            _readKartBilgisi.AboneNoLong = _EKS.KartBilgisi_v4.AboneNoLong;
                            _readKartBilgisi.SayacNo = _EKS.KartBilgisi.SayacNo;
                            _readKartBilgisi.Fiyat1 = _EKS.KartBilgisi.Fiyat1;
                            _readKartBilgisi.Fiyat2 = _EKS.KartBilgisi.Fiyat2;
                            _readKartBilgisi.Fiyat3 = _EKS.KartBilgisi.Fiyat3;
                            _readKartBilgisi.Fiyat4 = _EKS.KartBilgisi.Fiyat4;
                            _readKartBilgisi.Fiyat5 = _EKS.KartBilgisi.Fiyat5;
                            _readKartBilgisi.Fiyat6 = _EKS.KartBilgisi_v4.Fiyat6;
                            _readKartBilgisi.Fiyat7 = _EKS.KartBilgisi_v4.Fiyat7;

                            _readKartBilgisi.KademeUstSinir1 = _EKS.KartBilgisi_v4.KademeUstSinir1;
                            _readKartBilgisi.KademeUstSinir2 = _EKS.KartBilgisi_v4.KademeUstSinir2;
                            _readKartBilgisi.KademeUstSinir3 = _EKS.KartBilgisi_v4.KademeUstSinir3;
                            _readKartBilgisi.KademeUstSinir4 = _EKS.KartBilgisi_v4.KademeUstSinir4;
                            _readKartBilgisi.KademeUstSinir5 = _EKS.KartBilgisi_v4.KademeUstSinir5;
                            _readKartBilgisi.KademeUstSinir6 = _EKS.KartBilgisi_v4.KademeUstSinir6;

                            _readKartBilgisi.Kredi = _EKS.KartBilgisi.Kredi;
                            _readKartBilgisi.YedekKredi = _EKS.KartBilgisi.YedekKredi;

                            _readKartBilgisi.KartStatus = _EKS.KartBilgisi_v4.KartStatus;
                            _readKartBilgisi.KartStatus3 = _EKS.KartBilgisi_v4.KartStatus3;
                            _readKartBilgisi.KartDurumExtra = _EKS.KartBilgisi_v4.KartDurumExtra;
                            _readKartBilgisi.KartTipi = _EKS.KartBilgisi_v4.KartTipi;
                            _readKartBilgisi.KartTuru = _EKS.KartBilgisi_v4.KartTuru;
                            _readKartBilgisi.KartTuru_v2 = _EKS.KartBilgisi_v4.KartTuru_v2;

                            _readYazilacakEkBilgi.AboneTipi = _EKS.YazilacakEkBilgi.AboneTipi;
                            _readYazilacakEkBilgi.BayramBaslangici1 = _EKS.YazilacakEkBilgi.BayramBaslangici1;
                            _readYazilacakEkBilgi.BayramBaslangici2 = _EKS.YazilacakEkBilgi.BayramBaslangici2;
                            _readYazilacakEkBilgi.BayramBitisi1 = _EKS.YazilacakEkBilgi.BayramBitisi1;
                            _readYazilacakEkBilgi.BayramBitisi2 = _EKS.YazilacakEkBilgi.BayramBitisi2;

                            _readGenelParametreler.YanginModuSuresi = _EKS.GenelParametreler.YanginModuSuresi;

                            string cezaGiderenKart = (_EKS.KartBilgisi_v3.CezaGiderenYetkiKarti != 0 ? String.Format("Ceza Gideren Yetki Karti : {0:X}", _EKS.KartBilgisi_v3.CezaGiderenYetkiKarti) : "");

                            if (_EKS.KartBilgisi_v3.KartTuru_v2 == KartTuru_v2.YetkiKarti)
                            {
                                cezaGiderenKart = String.Format("Yetki Karti No :{0}", _EKS.KartBilgisi_v3.KartSeriNo.Substring(4));
                            }

                            staticClass.log(localize.getStr("cardReadSucceeded"), priority.ack);
                            staticClass.log(Environment.NewLine + localize.getStr("cardSerialNrEquals") + " " + _EKS.KartBilgisi.KartSeriNo + Environment.NewLine +
                                 cezaGiderenKart + Environment.NewLine
                                + localize.getStr("meterModelIDEquals") + " " + _EKS.SayacModeliId + Environment.NewLine
                                + localize.getStr("consumerNrEquals") + " " + _EKS.KartBilgisi.AboneNo + Environment.NewLine
                                + localize.getStr("waterMeterNrEquals") + " " + _EKS.KartBilgisi.SayacNo + Environment.NewLine

                                + localize.getStr("creditEquals") + " " + _EKS.KartBilgisi.Kredi + Environment.NewLine
                                + localize.getStr("backupCreditsEquals") + " " + _EKS.KartBilgisi.YedekKredi + Environment.NewLine
                                + localize.getStr("recentReadingDateEquals") + " " + _EKS.KartBilgisi.SayacOkumaZamani.ToShortDateString() + " " + _EKS.KartBilgisi.SayacOkumaZamani.ToLongTimeString() + Environment.NewLine
                                + localize.getStr("totalConsumptionEquals") + " " + _EKS.KartBilgisi.ToplamTuketilenKredi + Environment.NewLine
                                + localize.getStr("creditsInMeterEquals") + " " + _EKS.KartBilgisi.SayactakiKredi + Environment.NewLine
                                + localize.getStr("meterDateEquals") + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToLongTimeString() + Environment.NewLine
                                + localize.getStr("debtCreditsEquals") + " " + _EKS.KartBilgisi_v2.BorcKredi + Environment.NewLine
                                + localize.getStr("consumedCreditsEquals") + " " + _EKS.KartBilgisi_v2.TuketilenKredi, priority.ack);

                            sw.Stop();
                            if (_EKS.SonIslemSonuc.Result != Baylan.Common.ResultTypes.Success)
                            {
                                MessageBox.Show(_EKS.SonIslemSonuc.ResultDescription);
                                //break;
                            }

                            message = Environment.NewLine + "ID: " + _EKS.DeviceID + Environment.NewLine;
                            message += localize.getStr("cardSerialNrEquals") + " " + _EKS.KartBilgisi.KartSeriNo + "".PadRight(_padRightSize) + cezaGiderenKart + Environment.NewLine;
                            message +=
                                localize.getStr("consumerNrEquals") + " " + (_EKS.KartBilgisi_v3.AboneNo +", Long:" +_EKS.KartBilgisi.AboneNo) + Environment.NewLine +
                                       localize.getStr("waterMeterNrEquals") + " " + _EKS.KartBilgisi.SayacNo + Environment.NewLine +
                                       GetPaddedString(localize.getStr("tariff1Equals") + " " + _EKS.KartBilgisi.Fiyat1,
                                       localize.getStr("tariff2Equals") + " " + _EKS.KartBilgisi.Fiyat2) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("tariff3Equals") + " " + _EKS.KartBilgisi.Fiyat3 + "", localize.getStr("tariff4Equals") + " " + _EKS.KartBilgisi.Fiyat4) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("tariff5Equals") + " " + _EKS.KartBilgisi.Fiyat5, localize.getStr("tariff6Equals") + " " + _EKS.KartBilgisi_v2.Fiyat6) + Environment.NewLine +
                                       localize.getStr("tariff7Equals") + " " + _EKS.KartBilgisi_v2.Fiyat7 + "".PadRight(_padRightSize) + Environment.NewLine;
                            ;
                            message +=
                                       GetPaddedString(localize.getStr("step1Equals") + " " + _EKS.KartBilgisi.KademeUstSinir1,
                                       localize.getStr("step2Equals") + " " + _EKS.KartBilgisi.KademeUstSinir2) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("step3Equals") + " " + _EKS.KartBilgisi.KademeUstSinir3,
                                       localize.getStr("step4Equals") + " " + _EKS.KartBilgisi.KademeUstSinir4) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("step5Equals") + " " + _EKS.KartBilgisi_v2.KademeUstSinir5,
                                       (Baylan.Parsers.AK21Status3.Isfuture1Aktif == false ?
                                       localize.getStr("step6Equals") + " " + _EKS.KartBilgisi_v2.KademeUstSinir6 : "Hygenic Valve!")) + Environment.NewLine
                            ;
                            message +=
                                       GetPaddedString(localize.getStr("cardTypeEquals") + " " + _EKS.KartBilgisi.KartTuru.ToString(), localize.getStr("baylanCardTypeEquals") + " " + _EKS.KartBilgisi_v4.KartTuru_v3) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("creditEquals") + " " + _EKS.KartBilgisi.Kredi, localize.getStr("backupCreditsEquals") + " " + _EKS.KartBilgisi.YedekKredi) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("creditsToRefundEquals") + " " + _EKS.KartBilgisi.IadeEdilecekKredi, localize.getStr("creditsInMeterEquals") + " " + _EKS.KartBilgisi.SayactakiKredi) + Environment.NewLine
                                ;
                            message +=

                                       GetPaddedString(localize.getStr("batteryLevelEquals") + " " + _EKS.KartBilgisi.PilDurumu, localize.getStr("diameterEquals") + " " + _EKS.KartBilgisi.SayacCapi) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("recentCreditUpdateEquals") + " " + _EKS.KartBilgisi.SayacaYuklemeZamani.ToShortDateString() + " " + _EKS.KartBilgisi.SayacaYuklemeZamani.ToLongTimeString(),
                                       localize.getStr("recentReadingDateEquals") + " " + _EKS.KartBilgisi.SayacOkumaZamani.ToShortDateString()) + Environment.NewLine +
                                       localize.getStr("meterDateEquals") + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToLongTimeString() + Environment.NewLine;

                            message +=
                                       GetPaddedString(localize.getStr("leakageAmountEquals") + " " + _EKS.KartBilgisi.SizintiMiktari, localize.getStr("recentPulseReceiveDateEquals") + " " + _EKS.KartBilgisi.SonPulsZamani.ToShortDateString()) + Environment.NewLine +
                                       GetPaddedString(localize.getStr("totalConsumptionEquals") + " " + _EKS.KartBilgisi.ToplamTuketilenKredi + "(m3) ", localize.getStr("consumptionInTermEquals") + _EKS.KartBilgisi_v2.DonemIciTuketim + " (m3)") + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("termCreditsEquals") + " " + _EKS.KartBilgisi_v2.DonemKredi, localize.getStr("debtCreditsEquals") + " " + _EKS.KartBilgisi_v2.BorcKredi) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("consumedCreditsEquals") + " " + _EKS.KartBilgisi_v2.TuketilenKredi, localize.getStr("leakageRepetitionEquals") + " " + _EKS.KartBilgisi_v2.SizintiTekrarSayisi) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("valveOpenEquals") + " " + _EKS.KartBilgisi.VanaAcik, localize.getStr("valveClosingCountEquals") + " " + _EKS.KartBilgisi.VanaAcmaKapamaSayisi) + Environment.NewLine;

                            message += GetPaddedString(localize.getStr("consumerTypeEquals") + " " + _EKS.YazilacakEkBilgi.AboneTipi, "") + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("monthlyConsumption1Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim1, localize.getStr("monthlyConsumption2Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim2) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("monthlyConsumption3Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim3, localize.getStr("monthlyConsumption4Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim4) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("monthlyConsumption5Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim5, localize.getStr("monthlyConsumption6Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim6) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("monthlyConsumption7Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim7, localize.getStr("monthlyConsumption8Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim8) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("monthlyConsumption9Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim9, localize.getStr("monthlyConsumption10Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim10) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("monthlyConsumption11Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim11, localize.getStr("monthlyConsumption12Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim12) + Environment.NewLine;

                            message += GetPaddedString(localize.getStr("valveClosingDateEquals") + " " + _EKS.KartBilgisi_v2.VanaKapamaTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.VanaKapamaTarihi.ToLongTimeString(), localize.getStr("valveOpeningDateEquals") + " " + _EKS.KartBilgisi_v2.VanaAcmaTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.VanaAcmaTarihi.ToLongTimeString()) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("valveOpeningCountEquals") + " " + _EKS.KartBilgisi_v2.VanaAcmaSayisi, localize.getStr("valveClosingCountEquals") + " " + _EKS.KartBilgisi_v2.VanaKapatmaSayisi) + Environment.NewLine;
                            message += localize.getStr("versionEquals") + " " + _EKS.KartBilgisi_v2.Version + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("highConsumptionEquals") + " " + _EKS.GenelParametreler.AsiriTuketim, localize.getStr("advanceUseEquals") + " " + _EKS.GenelParametreler.AvansKullanimi) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("pulseErrorPeriodEquals") + " " + _EKS.GenelParametreler.GostergeArizaSuresi, localize.getStr("fireModePeriodEquals") + " " + _EKS.GenelParametreler.YanginModuSuresi) + Environment.NewLine;

                            message += GetPaddedString(localize.getStr("holidayStartDateEquals") + " " + _EKS.YazilacakEkBilgi.BayramBaslangici1.ToShortDateString(), localize.getStr("holidayEndDateEquals") + " " + _EKS.YazilacakEkBilgi.BayramBitisi1.ToShortDateString()) + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("waterMeterClosingDateEquals") + " " + _EKS.YazilacakEkBilgi.SayacKapatmaTarihi.ToShortDateString() + " " + _EKS.YazilacakEkBilgi.SayacKapatmaTarihi.ToLongTimeString(), localize.getStr("periodEquals") + " " + _EKS.KartBilgisi.DonemGunSayisi) + Environment.NewLine;

                            message += localize.getStr("interferencesEquals");
                                if (this.InvokeRequired)
                                {
                                    this.Invoke((MethodInvoker)delegate
                                    {
                                        if (CBSayacModeli.Text != _meterModelsByModelId[_EKS.SayacModeliId])
                                        {
                                            CBSayacModeli.Text = _meterModelsByModelId[_EKS.SayacModeliId];
                                            Thread th = new Thread((ThreadStart)delegate {
                                                BlinkBlink(CBSayacModeli);
                                            });
                                            th.Start();
                                            
                                        }
                                    });
                                }
                                else
                                {
                                    if (CBSayacModeli.Text != _meterModelsByModelId[_EKS.SayacModeliId])
                                    {
                                        CBSayacModeli.Text = _meterModelsByModelId[_EKS.SayacModeliId];
                                        Thread th = new Thread((ThreadStart)delegate
                                        {
                                            BlinkBlink(CBSayacModeli);
                                        });
                                        th.Start();
                                    }
                                }                            

                            if (_EKS.KartBilgisi_v2.Cezalar != null && _EKS.KartBilgisi.Cezalar.Length > 0)
                            {
                                foreach (Ceza item in _EKS.KartBilgisi_v2.Cezalar)
                                {
                                    try
                                    {
                                        if (item != null)
                                        {
                                            message += item.Kod.ToString() + Environment.NewLine;
                                        }
                                    }
                                    catch (Exception ex) { EKS_v2.Logger.Log("Ceza hatasi", ex); }
                                }
                            }
                            else { message += Environment.NewLine; }

                            GenerateTexture(_meterModelsByModelId[_EKS.SayacModeliId]);

                            message += localize.getStr("warningsEquals");
                            if (_EKS.KartBilgisi_v2.Uyarilar != null)
                            {
                                foreach (Uyari item in _EKS.KartBilgisi_v2.Uyarilar)
                                {
                                    message += item.Kod.ToString() + Environment.NewLine;
                                }
                            }
                            else { message += Environment.NewLine; }

                            message += GetPaddedString(localize.getStr("cardTypeEquals") + " " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v2.KartTipi }),
                                localize.getStr("cardTypeEquals") + " " + _EKS.KartBilgisi.KartTuru.ToString()) + Environment.NewLine;
                            //message += localize.getStr("baylanCardTypeEquals") + " " + kartIslemleri.KartBilgisi_v2.KartTuru_v2 + Environment.NewLine;
                            message += GetPaddedString(localize.getStr("cardStatusEquals") + " " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v2.KartStatus }), localize.getStr("cardStatusExtraEquals") + " " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v2.KartDurumExtra })) + Environment.NewLine;
                            message += localize.getStr("cardStatus3") + ": " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v3.KartStatus3 }) + Environment.NewLine;
                            message += localize.getStr("maximumFlowEquals") + " " + _EKS.GenelParametreler.MaxDebi + Environment.NewLine;
                            message += GetPaddedString(
                                localize.getStr("debitCreditUpperLevel") + ": " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v3.BorcSinirDegeri }),
                                localize.getStr("criticalCreditsEquals") + " " + _EKS.KartBilgisi.KritikKredi)
                                + Environment.NewLine;
                            message += "Sayac Model ID :" + ": " + _EKS.SayacModeliId + "(" + _meterModelsByModelId[_EKS.SayacModeliId] + ")" + Environment.NewLine;
                            message += "Kart Idare No :" + ": " + _EKS.KartBilgisi_v3.KartIdareNo + Environment.NewLine;
                            message += "Sub Auth. No :" + ": " + _EKS.KartBilgisi_v4.IdareNo + Environment.NewLine;
                            message += Strings.MeterInstallationDate +"/"+Strings.CIUMatchDate +":" +
                                _EKS.KartBilgisi_v4.MeterInstallationDate.ToShortDateString()+ " " + _EKS.KartBilgisi_v4.MeterInstallationDate.ToLongTimeString() + Environment.NewLine;
                            message += Strings.MinTemprature + ":" + _EKS.KartBilgisi_v4.MinTemprature + ", " + 
                                Strings.MaxTemprature + ":" + _EKS.KartBilgisi_v4.MaxTemprature + Environment.NewLine;

                            message += localize.getStr("periodOfProcessEquals") + " " + sw.ElapsedMilliseconds + " ms" + Environment.NewLine;

                            int selectedIndex = 0;
                            int selectedValue = 0;

                            if ((byte)(_EKS.KartBilgisi_v3.RFYayinSuresi) != 0)
                            {
                                string zamanTipi = ((_EKS.KartBilgisi_v3.RFYayinSuresi & 0xC0) == 0 && (byte)(_EKS.KartBilgisi_v3.RFYayinSuresi & 0x3F) == 0
                                    ? "" : (_EKS.KartBilgisi_v3.RFYayinSuresi & 0xC0) == 0x80 ? "Saat" :
                                    (_EKS.KartBilgisi_v3.RFYayinSuresi & 0xC0) == 0x40 ? "Dakika" : "Saniye"
                                    );

                                byte zamanData = (byte)(_EKS.KartBilgisi_v3.RFYayinSuresi & 0x3F);
                                selectedValue = zamanData;

                                if (zamanTipi == "Saniye")
                                {
                                    selectedIndex = 1;
                                }
                                if (zamanTipi == "Dakika")
                                {
                                    selectedIndex = 2;
                                }
                                if (zamanTipi == "Saat")
                                {
                                    selectedIndex = 3;
                                }

                                message += " Rf Yayin Data: Ham:" +
                                    BitConverter.ToString(new byte[] { (byte)(_EKS.KartBilgisi_v3.RFYayinSuresi) }) + " parsed:" +
                                    zamanData +
                                    " " + zamanTipi
                                     + Environment.NewLine;
                            }
                            if (uxCBTimeType.InvokeRequired)
                            {
                                uxCBTimeType.Invoke((ThreadStart)delegate()
                                {
                                    uxCBTimeType.SelectedIndex = selectedIndex;
                                    uxTBTimeData.Value = selectedValue;
                                    uxLBLTrackBarValue.Text = selectedValue + "";
                                });
                            }
                            else
                            {
                                uxCBTimeType.SelectedIndex = selectedIndex;
                                uxTBTimeData.Value = selectedValue;
                                uxLBLTrackBarValue.Text = selectedValue + "";
                            }

                            if (txtAboneNo.InvokeRequired)
                            {
                                txtAboneNo.Invoke((ThreadStart)delegate()
                                {
                                    FillFormFields();

                                });
                            }
                            else
                            {
                                FillFormFields();
                            }
                        }
                        else
                        {
                            message = localize.getStr("failedReadingCard") + Environment.NewLine +
                            _EKS.SonIslemSonuc.Result.ToString() + ":" + _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine + _EKS.SonIslemSonuc.ComResultType.ToString()
                            + Environment.NewLine + (_EKS.SonIslemSonuc.Exception != null ? _EKS.SonIslemSonuc.Exception.Message + " " + _EKS.SonIslemSonuc.Exception.StackTrace : "") +
                            (_EKS.SonIslemSonuc.ExceptionsList != null ? localize.getStr("exceptionCountEquals") + _EKS.SonIslemSonuc.ExceptionsList.Count : "")
                            ;
                        }
                   
                        sw.Reset();
                        if (progressBar1.InvokeRequired)
                        {
                            progressBar1.Invoke((ThreadStart)delegate()
                            {
                                progressBar1.Value = progressBar1.Maximum;
                            });
                        }
                        if (txOutput.InvokeRequired)
                        {
                            txOutput.Invoke((ThreadStart)delegate()
                            {
                                txOutput.AppendText("\n" + localize.getStr("cardContentsDisplayed"));
                                txOutput.AppendText(message);
                                txOutput.ScrollToCaret();
                                lbStatus.Text = localize.getStr("cardContentsDisplayed");
                                lbStatus.ForeColor = Color.DodgerBlue;
                            });
                        }
                        else
                        {
                            lbStatus.Text = localize.getStr("cardContentsDisplayed");
                            lbStatus.ForeColor = Color.DodgerBlue;
                            txOutput.AppendText("\n" + localize.getStr("cardContentsDisplayed"));
                            txOutput.AppendText(message);
                            txOutput.ScrollToCaret();
                        }

                        if (uxIadeEdilecekKredi.InvokeRequired)
                        {
                            uxIadeEdilecekKredi.Invoke((ThreadStart)delegate()
                            {
                                try
                                {
                                    if (Convert.ToDecimal(uxIadeEdilecekKredi.Text) > 0)
                                    {
                                        MessageBox.Show("The card contains refunded credits from the water meter. " +
                                                        "\nThis amount will be inserted in the main credits field once the card is redefined as a consumer card." +
                                                        "\nPlease consider clearing the downloaded credits if new card is not expected to contain this amount !!!",
                                                        "Baylan Water Meters");
                                    }
                                }
                                catch
                                {
                                    ;
                                }
                            });
                        }
                        else
                        {
                            try
                            {
                                if (Convert.ToDecimal(uxIadeEdilecekKredi.Text) > 0)
                                {
                                    MessageBox.Show("The card contains refunded credits from the water meter. " +
                                                    "\nThis amount will be inserted in the main credits field once the card is redefined as a consumer card." +
                                                    "\nPlease consider clearing the downloaded credits if new card is not expected to contain this amount !!!",
                                        "Baylan Water Meters");
                                }
                            }
                            catch
                            {
                                ;
                            }
                        }
                    }                
            }
            catch (Exception ex)
            {
                EKS_v2.Logger.Log("Read Isleminde Hata", ex);
                MessageBox.Show(ex.Message + " " + ex.StackTrace);
            }
        }

        private void GenerateTexture(string modelName)
        {
            if (this.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate { GenerateTexture(modelName); });
            }
            else
            {
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                format.Trimming = StringTrimming.EllipsisCharacter;
                
                Bitmap img = new Bitmap(uxPNLImageContainer.Height, uxPNLImageContainer.Width);
                Graphics G = Graphics.FromImage(img);

                G.Clear(this.BackColor);
                
                Font newFont = new Font(this.Font.FontFamily, this.Font.Size * 4, FontStyle.Bold, GraphicsUnit.Pixel, this.Font.GdiCharSet, this.Font.GdiVerticalFont);


                SolidBrush brush_text = new SolidBrush(Color.Red);
                G.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
                G.DrawString(modelName, newFont, brush_text, new Rectangle(0, 0, img.Width, img.Height), format);
                brush_text.Dispose();

                img.RotateFlip(RotateFlipType.Rotate270FlipNone);

                uxPNLImageContainer.BackgroundImage = img;
            }
        }

        void BlinkBlink(Control control)
        {
            return; 
            if (control.InvokeRequired)
            {
                control.Invoke((MethodInvoker)delegate { BlinkBlink(control); });
            }
            else
            {
                if (control is ComboBox)
                {

                    Color firstColorBack = ((ComboBox)control).BackColor;
                    Color firstColorFore = ((ComboBox)control).ForeColor;
                    FlatStyle firstFlatStyle = ((ComboBox)control).FlatStyle;
                    for (int i = 0; i < 5; i++)
                    {

                        ((ComboBox)control).BackColor = Color.Red;
                        ((ComboBox)control).ForeColor = Color.Yellow;
                        ((ComboBox)control).FlatStyle = FlatStyle.Flat;
                        Application.DoEvents();
                        Thread.Sleep(100);
                        ((ComboBox)control).BackColor = firstColorBack;
                        ((ComboBox)control).ForeColor = firstColorFore;
                        ((ComboBox)control).FlatStyle = firstFlatStyle;
                        Application.DoEvents();
                        Thread.Sleep(100);
                    }
                    ((ComboBox)control).BackColor = firstColorBack;
                    ((ComboBox)control).ForeColor = firstColorFore;
                    ((ComboBox)control).FlatStyle = firstFlatStyle;
                    Application.DoEvents();
                }
            }

        }
        void FillFormFields()
        {
            txtAboneNo.Text = _EKS.KartBilgisi.AboneNo < 0 && _EKS.KartBilgisi_v3.AboneNoLong > 0 ?
                                       _EKS.KartBilgisi_v3.AboneNoLong.ToString() : _EKS.KartBilgisi.AboneNo.ToString();

            txtSayacNo.Text = _EKS.KartBilgisi.SayacNo.ToString();

            txtTarife1.ReadOnly = false;
            txtTarife2.ReadOnly = false;
            txtTarife3.ReadOnly = false;

            if (uxAK11IkincilFonksiyonlar.Checked && uxAK11IkincilFonksiyonlar.Visible && _EKS.SayacModeliId == 2)
            {
                uxTBoxSuTuketimSuresi.Text = _EKS.KartBilgisi_v4.HygenicWaterConsumptionDuration.ToString(); //kartIslemleri.KartBilgisi.Fiyat1.ToString();
                uxTBoxSuTuketimMiktari.Text = _EKS.KartBilgisi_v4.HygenicWaterConsumptionAmout.ToString(); //kartIslemleri.KartBilgisi.Fiyat2.ToString();
                uxTBoxVanaAcilmaSuresi.Text = _EKS.KartBilgisi_v4.HygenicValveOpenDuration.ToString();//kartIslemleri.KartBilgisi.Fiyat3.ToString();

                txtTarife1.ReadOnly = true;
                txtTarife2.ReadOnly = true;
                txtTarife3.ReadOnly = true;

                //txtTarife1.Visible = true;
                //txtTarife2.ReadOnly = true;
                //txtTarife3.ReadOnly = true;


                txtTarife1.Text = txtTarife2.Text = txtTarife3.Text = "Hygenic Valve!";

            }
            else
            {
                txtTarife1.Text = _EKS.KartBilgisi.Fiyat1.ToString();
                txtTarife2.Text = _EKS.KartBilgisi.Fiyat2.ToString();
                txtTarife3.Text = _EKS.KartBilgisi.Fiyat3.ToString();
            }
            txtTarife4.Text = _EKS.KartBilgisi.Fiyat4.ToString();
            txtTarife5.Text = _EKS.KartBilgisi.Fiyat5.ToString();
            txtTarife6.Text = _EKS.KartBilgisi_v2.Fiyat6.ToString();
            txtTarife7.Text = _EKS.KartBilgisi_v2.Fiyat7.ToString();

            txtKademe1.Text = _EKS.KartBilgisi.KademeUstSinir1.ToString();
            txtKademe2.Text = _EKS.KartBilgisi.KademeUstSinir2.ToString();
            txtKademe3.Text = _EKS.KartBilgisi.KademeUstSinir3.ToString();
            txtKademe4.Text = _EKS.KartBilgisi.KademeUstSinir4.ToString();
            txtKademe5.Text = _EKS.KartBilgisi_v2.KademeUstSinir5.ToString();

            if (Baylan.Parsers.AK21Status3.Isfuture1Aktif == false)
            {
                txtKademe6.Text = _EKS.KartBilgisi_v2.KademeUstSinir6.ToString();
            }
            else
            {
                txtKademe6.Text = "Hygenic Valve!";
                //uxAK11IkincilFonksiyonlar.Checked = Baylan.Parsers.AK21Status3.Isfuture1Aktif;
                //byte[] hygenicValues = BitConverter.GetBytes(kartIslemleri.KartBilgisi_v2.KademeUstSinir6);
                //uxTBoxSuTuketimSuresi.Text = hygenicValues[1] + "";
                //uxTBoxVanaAcilmaSuresi.Text = hygenicValues[2] + "";
                //uxTBoxSuTuketimMiktari.Text = hygenicValues[0] + "";
                uxTBoxSuTuketimSuresi.Text = _EKS.KartBilgisi_v4.HygenicWaterConsumptionDuration.ToString(); //kartIslemleri.KartBilgisi.Fiyat1.ToString();
                uxTBoxSuTuketimMiktari.Text = _EKS.KartBilgisi_v4.HygenicWaterConsumptionAmout.ToString(); //kartIslemleri.KartBilgisi.Fiyat2.ToString();
                uxTBoxVanaAcilmaSuresi.Text = _EKS.KartBilgisi_v4.HygenicValveOpenDuration.ToString();//kartIslemleri.KartBilgisi.Fiyat3.ToString();

            }

            txtAboneTipi.Text = _EKS.YazilacakEkBilgi.AboneTipi;
            cmpKritikKredi.Text = _EKS.KartBilgisi.KritikKredi.ToString();
            TB_KartAnaKredi.Text = _EKS.KartBilgisi.Kredi.ToString() + "";
            TB_KartYedekKredi.Text = _EKS.KartBilgisi.YedekKredi.ToString() + "";
            TB_SayacKredi.Text = _EKS.KartBilgisi.SayactakiKredi.ToString() + "";
            uxIadeEdilecekKredi.Text = _EKS.KartBilgisi.IadeEdilecekKredi.ToString() + "";
            TB_SayacTuketim.Text = _EKS.KartBilgisi.ToplamTuketilenKredi.ToString() + "";
        }

        float fixValue(string input)
        {
            float result = 0;
            if (input.Contains(",") || (input.Contains(".")))
            {
                char theSeperator = Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                input = input.Replace(",", theSeperator.ToString());
                input = input.Replace(".", theSeperator.ToString());
            }
            float.TryParse(input, out result);
            return result;
        }


        void SetCardData2Write()
        {
            if (_EKS.KartBilgisi == null)
            {
                _EKS.KartOku();               
            }          

            try { _EKS.KartBilgisi.SayacNo = Convert.ToInt32(txtSayacNo.Text); }
            catch { lbStatus.Text = "Invalid meter number assigned !!!"; return; }
            try
            {               
                {
                    _EKS.KartBilgisi_v3.AboneNoLong = Convert.ToInt64(txtAboneNo.Text);
                    _EKS.KartBilgisi_v3.AboneNo = (int)Convert.ToInt64(txtAboneNo.Text);
                }

            }
            catch { lbStatus.Text = "Invalid consumer number assigned !!!"; return; }

            //MessageBox.Show("Sayac No:" + _EKS.KartBilgisi.SayacNo);

            _EKS.YazilacakEkBilgi.AboneTipi = (txtAboneTipi.Text);
            try { _EKS.KartBilgisi.KademeUstSinir1 = Convert.ToInt16(txtKademe1.Text); }
            catch { lbStatus.Text = "Invalid step1 definition !!!"; return; }
            try { _EKS.KartBilgisi.KademeUstSinir2 = Convert.ToInt16(txtKademe2.Text); }
            catch { lbStatus.Text = "Invalid step2 definition !!!"; return; }
            try { _EKS.KartBilgisi.KademeUstSinir3 = Convert.ToInt16(txtKademe3.Text); }
            catch { lbStatus.Text = "Invalid step3 definition !!!"; return; }
            try { _EKS.KartBilgisi.KademeUstSinir4 = Convert.ToInt16(txtKademe4.Text); }
            catch { lbStatus.Text = "Invalid step4 definition !!!"; }
            try { _EKS.KartBilgisi_v2.KademeUstSinir5 = Convert.ToInt16(txtKademe5.Text); }
            catch { lbStatus.Text = "Invalid step5 definition !!!"; return; }

            _EKS.KartBilgisi.YedekKredi = 0;
            _EKS.KartBilgisi_v2.KartTuru = EKS.KartTuru.AboneKarti;

            switch (CMBAboneTipi.SelectedIndex)
            {
                case 0:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.AboneKarti;
                    break;
                case 1:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.ZamanKarti;
                    _EKS.KartBilgisi_v2.SayacTarihi = DTPSayacTarihi.Value;
                    break;
                case 2:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.TestKarti;
                    break;
                case 3:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.KontrolKarti;
                    break;
                case 4:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.TanimsizKart;
                    break;
                case 5:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.YetkiKarti;
                    break;
                case 6:
                    //Reset KArt tanimlamasi
                    _EKS.KartBilgisi_v2.KartTuru_v2 = (KartTuru_v2.YetkiKarti | KartTuru_v2.KontrolKarti | KartTuru_v2.TestKarti);
                    break;
                //case 7:
                //    kartIslemleri.KartBilgisi_v2.KartTuru_v2 = KartTuru_v2.OrtakKullanimKarti;
                //    break;
                case 8: //Yeni Sayac No
                    _EKS.KartBilgisi_v2.KartTuru_v2 = (KartTuru_v2.YetkiKarti | KartTuru_v2.OrtakKullanimKarti | KartTuru_v2.TestKarti);
                    break;
                case 9://Tuketim ve kredi ayarlama 
                    _EKS.KartBilgisi_v2.KartTuru_v2 = (KartTuru_v2.YetkiKarti | KartTuru_v2.ZamanKarti | KartTuru_v2.TestKarti);
                    decimal anaKredi = 0;
                    decimal yedekKredi = 0;
                    decimal.TryParse(txtAnaKredi.Text, out anaKredi);
                    decimal.TryParse(txtYedekKredi.Text, out yedekKredi);

                    if (yedekKredi == 0)
                    {
                        //if (MessageBox.Show("Ayarlanacak kredi degeri '0' olamaz, Sayac 0 degerini kabul etmez, en dusuk 0.01 olarak ayarlamak istermsiniz? Hayiri secerseniz 1 olarak aya", "Dikkat", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            yedekKredi = 0.01M;
                        }
                    }                 
                    _EKS.KartBilgisi.Kredi = (float)anaKredi;
                    _EKS.KartBilgisi.YedekKredi = (float)yedekKredi;
                    break;
                case 10: //Abseron Avans ayar Karti
                    _EKS.KartBilgisi_v2.KartTuru_v2 = (KartTuru_v2.YetkiKarti | KartTuru_v2.KontrolKarti | KartTuru_v2.ZamanKarti);
                    Int16 tmp = 0;
                    Int16.TryParse(uxTBAvansKredi.Text, out tmp);
                    _EKS.KartBilgisi_v4.AdvanceUsageCredit = tmp;
                    break;
                case 12: //Abseron Avans Reset Karti
                    _EKS.KartBilgisi_v2.KartTuru_v2 = (KartTuru_v2.YetkiKarti | KartTuru_v2.KontrolKarti | KartTuru_v2.ZamanKarti | KartTuru_v2.TestKarti);
                    break;
                case 11: //RF Zaman Ayar Karti
                    _EKS.KartBilgisi_v2.KartTuru_v2 = (KartTuru_v2.KontrolKarti | KartTuru_v2.ZamanKarti);
                    //Kredi alanindan RF Aktif/Pasif(0), RF Yayin Suresi(1), RF Yayin protocolunu gonderiyoruz(3) (3 byte toplam),
                    byte[] rfAyar1 = new byte[4];
                    if (uxRBRFAktif.Checked) //RF Aktif/pasif bytei
                    {
                        rfAyar1[0] = 0x01;
                    }
                    //RF Yayin Suresi
                    byte zaman = 15; //default 15 sn 
                    byte.TryParse(uxTBRFSure.Text, out zaman);
                    rfAyar1[1] = (byte)(uxCBRFZamanTipi.SelectedIndex * 64);
                    rfAyar1[1] |= (byte)(zaman & 0x3F);                  

                    //RF Protokol Tipi
                    rfAyar1[2] = (byte)uxCBRFTipi.SelectedIndex;
                    int rfayarInt = BitConverter.ToInt32(rfAyar1, 0);

                    _EKS.KartBilgisi.Kredi = Convert.ToSingle(rfayarInt);
                    byte[] rfFreqs = new byte[4];
                    byte tempByte = 0;
                    Byte.TryParse(uxTBRFFreq0.Text, out tempByte);
                    rfFreqs[0] = tempByte;

                    Byte.TryParse(uxTBRFFreq1.Text, out tempByte);
                    rfFreqs[1] = tempByte;

                    Byte.TryParse(uxTBRFFreq2.Text, out tempByte);
                    rfFreqs[2] = tempByte;

                    int rfFreqInt = BitConverter.ToInt32(rfFreqs, 0);
                    _EKS.KartBilgisi.YedekKredi = Convert.ToSingle(rfFreqInt);

                    break;
                default:
                    _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.AboneKarti;
                    break;
            }
            if (_EKS.KartBilgisi_v2.IadeEdilecekKredi > 0 && Convert.ToDecimal(uxIadeEdilecekKredi.Text) > 0)
            {
                _EKS.KartBilgisi_v2.IadeEdilecekKredi = (float)Convert.ToDecimal(uxIadeEdilecekKredi.Text);
            }
          
            if (!string.IsNullOrEmpty(uxLBLTimeType.Text))
            {
                _EKS.SetRfYayinZamanData(zamantipi, (byte)uxTBTimeData.Value);
                //kartIslemleri.Se
            }
            float tmpFloat = 0;
            if (uxTBKritikKredi.Text != "0" && string.IsNullOrEmpty(uxTBKritikKredi.Text) == false)
            {
                try
                {
                    tmpFloat = float.Parse(uxTBKritikKredi.Text, CultureInfo.CurrentUICulture);
                    frmPortSettings.Config.CriticalCredit = tmpFloat;
                }
                catch (Exception ex)
                {
                    ;
                }
            }
            _EKS.IsAK11ExtraFunctionality = false;
            //Hijyenik kullanim icin tarife-1, tarife-2 ve 3 parametre degerleri kullanilacak. 
            //EKSAPI.IsAK11ExtraFunctionality true ise bu degerler bu islem icin kullanilir.
            // Makinanın decimal seperatorune bakan metoda bağlı çalışıyor. float.TryParse decimal seperator da yanlış çalışıyor, 10 la çarpıyo veya 0 yazıyo.
            if (uxAK11IkincilFonksiyonlar.Checked)
            {
                if (_EKS.SayacModeliId == 2) //111 icin hijyenik kullanim ise
                {
                    byte tmpInt = 0;

                    byte.TryParse(uxTBoxSuTuketimSuresi.Text, NumberStyles.Any,
                            CultureInfo.InvariantCulture, out tmpInt);
                    _EKS.KartBilgisi_v4.HygenicWaterConsumptionDuration = tmpInt;
                    _EKS.KartBilgisi.Fiyat1 = (float)tmpInt;

                    byte.TryParse(uxTBoxSuTuketimMiktari.Text, NumberStyles.Any,
                            CultureInfo.InvariantCulture, out tmpInt);
                    _EKS.KartBilgisi_v4.HygenicWaterConsumptionAmout = tmpInt;
                    _EKS.KartBilgisi.Fiyat2 = (float)tmpInt;

                    byte.TryParse(uxTBoxVanaAcilmaSuresi.Text, NumberStyles.Any,
                           CultureInfo.InvariantCulture, out tmpInt);
                    _EKS.KartBilgisi_v4.HygenicValveOpenDuration = tmpInt;

                    _EKS.KartBilgisi.Fiyat3 = (float)tmpInt;
                    _EKS.IsAK11ExtraFunctionality = true;
                }
                else if (
                    _EKS.SayacModeliId == 6 ||
                    _EKS.SayacModeliId == 20 ||
                    _EKS.SayacModeliId == 21

                    ) //AK311 icin hijyenik vana aktif
                {
                    byte tmpInt = 0;
                    byte[] hygenicValues = new byte[4];

                    byte.TryParse(uxTBoxSuTuketimMiktari.Text, NumberStyles.Any,
                        CultureInfo.InvariantCulture, out tmpInt);
                    _EKS.KartBilgisi_v4.HygenicWaterConsumptionAmout = tmpInt;
                    //kartIslemleri.KartBilgisi_v2.KademeUstSinir6 = tmpInt; //0.Byte Kullanim Litre
                    hygenicValues[0] = tmpInt;
                    //kartIslemleri.
                    byte.TryParse(uxTBoxSuTuketimSuresi.Text, NumberStyles.Any,
                        CultureInfo.InvariantCulture, out tmpInt);
                    //kartIslemleri.KartBilgisi_v2.KademeUstSinir6 += tmpInt * 0x100; //2.Byte Kullanim Dakika
                    _EKS.KartBilgisi_v4.HygenicWaterConsumptionDuration = tmpInt;
                    hygenicValues[1] = tmpInt;

                    byte.TryParse(uxTBoxVanaAcilmaSuresi.Text, NumberStyles.Any,
                        CultureInfo.InvariantCulture, out tmpInt);
                    //kartIslemleri.KartBilgisi_v2.KademeUstSinir6 += tmpInt * 0x10000; //MSB 3.byte Vana Acik suresi
                    hygenicValues[2] = tmpInt;
                    _EKS.KartBilgisi_v4.HygenicValveOpenDuration = tmpInt;
                    _EKS.KartBilgisi_v2.KademeUstSinir6 = BitConverter.ToInt32(hygenicValues, 0);
                    //((BaylanEks.EKSAPI)kartIslemleri).SetHygenicValues();
                    _EKS.IsAK11ExtraFunctionality = true;

                }
            }
            else
            {
                //float.TryParse(txtTarife1.Text, NumberStyles.Any,
                //CultureInfo.InvariantCulture, out tmpFloat);
                //kartIslemleri.KartBilgisi.Fiyat1 = tmpFloat;        
                _EKS.KartBilgisi.Fiyat1 = fixValue(txtTarife1.Text);


                //float.TryParse(txtTarife2.Text, NumberStyles.Any,
                ///CultureInfo.InvariantCulture, out tmpFloat);
                //kartIslemleri.KartBilgisi.Fiyat2 = tmpFloat;
                _EKS.KartBilgisi.Fiyat2 = fixValue(txtTarife2.Text);


                //float.TryParse(txtTarife3.Text, NumberStyles.Any,
                //CultureInfo.InvariantCulture, out tmpFloat);
                //kartIslemleri.KartBilgisi.Fiyat3 = tmpFloat;
                _EKS.KartBilgisi.Fiyat3 = fixValue(txtTarife3.Text);
                try { _EKS.KartBilgisi_v2.KademeUstSinir6 = Convert.ToInt16(txtKademe6.Text); }
                catch { lbStatus.Text = "Invalid step6 definition !!!"; }
            }

            //float.TryParse(txtTarife4.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat4 = tmpFloat;
            _EKS.KartBilgisi.Fiyat4 = fixValue(txtTarife4.Text);

            //float.TryParse(txtTarife5.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat5 = tmpFloat;
            _EKS.KartBilgisi.Fiyat5 = fixValue(txtTarife5.Text);

            //float.TryParse(txtTarife6.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi_v2.Fiyat6 = tmpFloat;
            _EKS.KartBilgisi_v2.Fiyat6 = fixValue(txtTarife6.Text);

            //float.TryParse(txtTarife7.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi_v2.Fiyat7 = tmpFloat;
            _EKS.KartBilgisi_v2.Fiyat7 = fixValue(txtTarife7.Text);

            float.TryParse(cmpKritikKredi.Text, out tmpFloat);
            _EKS.KartBilgisi.KritikKredi = tmpFloat;

            //int tmp_int = 0;
            //Int32.TryParse(cmpMaxDebiEsikDegeri.Text, out tmp_int);
            //kartIslemleri.GenelParametreler.MaxDebi = tmp_int;
            byte saat = (byte)cmpYanginModuSaat.Value;
            byte dakika = (byte)cmpYanginModuDakika.Value;
            int yanginModuSuresi = (saat * 60) + dakika;

            _EKS.YazilacakEkBilgi.AboneTipi = txtAboneTipi.Text;

            _EKS.GenelParametreler.YanginModuSuresi = yanginModuSuresi;

            _EKS.YazilacakEkBilgi.BayramBaslangici1 = cmpKesintisizBaslangic.Value;
            _EKS.YazilacakEkBilgi.BayramBitisi1 = cmpKesintisizBitis.Value;
            _EKS.YazilacakEkBilgi.SayacKapatmaTarihi = uxSayacKapatmaTarihi.Value;

            _EKS.KartBilgisi.SayacCapi = Convert.ToInt16(cmpSayacCapi.SelectedItem == null || string.IsNullOrEmpty(cmpSayacCapi.SelectedItem.ToString()) ? "20" : cmpSayacCapi.SelectedItem.ToString());
            _EKS.KartBilgisi.DonemGunSayisi = (short)(Convert.ToUInt16(uxDonem.SelectedItem));
            if (uxAK21KartStatus.Visible) //AK-21 kart statusu
            {
                _EKS.SetSayacOzelDurumlar(uxAK21KesintisizSu.Checked,
                    uxAK21ResmiTatilVanaAcik.Checked,
                    uxAK21TarihdeVanaKapat.Checked,
                    uxAK21HaftaSonuMesaiSayacAcik.Checked,
                    uxAK21RekorCeza.Checked, uxAK21ManyetikCeza.Checked,
                    uxAK21UstKapakCeza.Checked, uxAK21PilKapagiCezasi.Checked);
                _EKS.setsayacozeldurumlar_ak211(
                    uxAK21KesintisizSu.Checked,
                    uxAK21ResmiTatilVanaAcik.Checked,
                    uxAK21TarihdeVanaKapat.Checked,
                    uxAK21HaftaSonuMesaiSayacAcik.Checked,
                    uxAK21RekorCeza.Checked,
                    uxAK21ManyetikCeza.Checked,
                    uxAK21UstKapakCeza.Checked,
                    uxAK21PilKapagiCezasi.Checked);

            }
            if (uxAK11KartStatus.Visible) //AK-11 KArt Statusu
            {
                _EKS.SetSayacOzelDurumlar(uxAK11HaftaSonuMesaiDisiVanaAcik.Checked,
                    uxAK11PilKisalt.Checked,
                    uxAK11PilUzat.Checked, uxAK11AydaIkiVanaAcmaKapama.Checked);
            }
            //tmp_int = 0;
            //Int32.TryParse(cmpMaxDebiEsikDegeri.Text, out tmp_int);

            //kartIslemleri.GenelParametreler.MaxDebi = tmp_int;
            if (uxSendAsByte.Checked && !string.IsNullOrEmpty(uxKartStatus.Text))
            {
                _EKS.SetSayacOzelDurumlar(Byte.Parse(uxKartStatus.Text));
            }

            staticClass.log(localize.getStr("valuesPriorToWritingToCard"), priority.ack);
            string msg = Environment.NewLine + localize.getStr("meterModelIDEquals") + " " + _EKS.SayacModeliId + Environment.NewLine
                        + localize.getStr("consumerNrEquals") + " " + _EKS.KartBilgisi.AboneNo + Environment.NewLine
                        + localize.getStr("waterMeterNrEquals") + " " + _EKS.KartBilgisi.SayacNo + Environment.NewLine
                        + localize.getStr("cardTypeEquals") + " " + _EKS.KartBilgisi_v2.KartTuru_v2.ToString() + Environment.NewLine
                        + localize.getStr("creditEquals") + " " + _EKS.KartBilgisi.Kredi + Environment.NewLine
                        + localize.getStr("backupCreditsEquals") + " " + _EKS.KartBilgisi.YedekKredi;

            staticClass.log(msg, priority.ack);
        }
        private void btnSil_Click(object sender, EventArgs e)
        {
            long startTs = Environment.TickCount;
            long startTick = Environment.TickCount;
            if (_EKS.KartSil())
            {
                txOutput.Text = lbStatus.Text = localize.getStr("cardContentsClearedSuccessfully") + (" in " + (Environment.TickCount - startTick) + " ms");
                staticClass.log(localize.getStr("cardContentsClearedSuccessfully"), priority.ack);
                lbStatus.ForeColor = Color.Red;
                FillFormFields();
            }
            else
            {
                lbStatus.Text = localize.getStr("cardClearProcessFailed");
                txOutput.Text = localize.getStr("cardClearProcessFailed");
                txOutput.Text += localize.getStr("errorOccuredWhileClearingCard") + _EKS.SonHataMesaji;
                staticClass.log(localize.getStr("errorOccuredWhileClearingCard"), priority.error);
                staticClass.log(_EKS.SonHataMesaji, priority.error);
            }

            txOutput.AppendText(localize.getStr("periodEquals") + (Environment.TickCount - startTs) + " ms");
        }

        private void btnKontorEkle_Click(object sender, EventArgs e)
        {
            SetCardData2Write();
            decimal anaKredi = 0;
            decimal yedekKredi = 0;

            decimal.TryParse(txtAnaKredi.Text, NumberStyles.Any,
            CultureInfo.CurrentUICulture, out anaKredi);

            decimal.TryParse(txtYedekKredi.Text, NumberStyles.Any,
            CultureInfo.CurrentUICulture, out yedekKredi);

            int aboneNo = 0;
            int sayacNo = 0;
            int.TryParse(txtAboneNo.Text, out aboneNo);
            int.TryParse(txtSayacNo.Text, out sayacNo);

            _EKS.KartBilgisi.AboneNo = aboneNo;
            _EKS.KartBilgisi.SayacNo = sayacNo;
            long startTick = Environment.TickCount;
            if (uxDTPMakbuzNo.Checked)
            {
                _EKS.KartBilgisi_v4.ReceiptDateTime = uxDTPMakbuzNo.Value;
                AK21Status3.Setfuture3Aktif = true;
            }
            if (_EKS.KrediYukle((decimal)anaKredi, (decimal)yedekKredi))
            {
                txOutput.Text = lbStatus.Text = String.Format(localize.getStr("xCreditsXBackupCreditsLoaded"), anaKredi, yedekKredi) + (" in " + (Environment.TickCount - startTick) + " ms");
                lbStatus.ForeColor = Color.DodgerBlue;
            }
            else
            {
                lbStatus.Text = localize.getStr("failedLoadingCredits") + _EKS.SonHataMesaji;
                txOutput.AppendText("\n " + localize.getStr("failedLoadingCredits"));
                txOutput.AppendText("\n " + localize.getStr("failedLoadingCredits") + _EKS.SonHataMesaji);
            }
        }

        private void CBSayacModeli_SelectedValueChanged(object sender, EventArgs e)
        {
            GetOperatorSelectedMeterType();
        }
        private void GetOperatorSelectedMeterType()
        {
            if (CBSayacModeli.SelectedItem != null)
            {
                try
                {
                    uxAK11IkincilFonksiyonlar.Visible = false;
                    if (CBSayacModeli.SelectedItem.Equals("AK-111"))
                    {
                        _EKS.SayacModeliId = 2;
                        uxAK11KartStatus.Visible = true;
                        uxAK21KartStatus.Visible = false;
                        uxAK11IkincilFonksiyonlar.Visible = true;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("AK-211"))
                    {
                        _EKS.SayacModeliId = 1;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("AK-311"))
                    {
                        _EKS.SayacModeliId = 6;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                        uxAK11IkincilFonksiyonlar.Visible = true;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("AK-311 ISKRA"))
                    {
                        _EKS.SayacModeliId = 12;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                        uxAK11IkincilFonksiyonlar.Visible = true;
                        return;
                    }

                    if (CBSayacModeli.SelectedItem.Equals("AK-211(BUSKI)"))
                    {
                        _EKS.SayacModeliId = 7;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                    }
                    if (CBSayacModeli.SelectedItem.Equals(localize.getStr("heatMeter")))
                    {
                        _EKS.SayacModeliId = 3;
                        _EKS.SayacTipi = SayacTipi.SicakSu;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals(localize.getStr("AK21CommonUse")))
                    {
                        _EKS.SayacModeliId = 4;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                        CMBAboneTipi.SelectedIndex = 7;
                    }
                    if (CBSayacModeli.SelectedItem.Equals(localize.getStr("AK21CommonUse").Replace("21", "311")))
                    {
                        _EKS.SayacModeliId = 9;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = true;
                        CMBAboneTipi.SelectedIndex = 7;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals(localize.getStr("GPRSmeterWithCard")))
                    {
                        _EKS.SayacModeliId = 5;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = false;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("Isi Sayaci Sicak Soguk"))
                    {
                        _EKS.SayacModeliId = 11;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = false;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("AK-311 ISI Sayaci"))
                    {
                        _EKS.SayacModeliId = 0x0A;
                        _EKS.SayacTipi = SayacTipi.SicakSu;
                        uxAK11KartStatus.Visible = false;
                        uxAK21KartStatus.Visible = false;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("AK-411"))
                    {
                        _EKS.SayacModeliId = 20;
                        uxAK11KartStatus.Visible = true;
                        uxAK21KartStatus.Visible = false;
                        uxAK11IkincilFonksiyonlar.Visible = true;
                        return;
                    }
                    if (CBSayacModeli.SelectedItem.Equals("US-411"))
                    {
                        _EKS.SayacModeliId = 21;
                        uxAK11KartStatus.Visible = true;
                        uxAK21KartStatus.Visible = false;
                        uxAK11IkincilFonksiyonlar.Visible = true;
                        return;
                    }
                }
                finally
                {
                    DisableEnableSubAuthControls(false);

                    if (_EKS.SayacModeliId == (int)BaylanEks.SayacTipleri.AK_311 ||
                        _EKS.SayacModeliId == (int)BaylanEks.SayacTipleri.AK_311_IsiSayaci ||
                        _EKS.SayacModeliId == (int)BaylanEks.SayacTipleri.AK_311_ISKRA ||
                        _EKS.SayacModeliId == (int)BaylanEks.SayacTipleri.AK_411 ||
                        _EKS.SayacModeliId == (int)BaylanEks.SayacTipleri.AK_411_US
                        )
                    {
                        DisableEnableSubAuthControls(true);
                    }
                }
            }
        }

        void DisableEnableSubAuthControls(bool status)
        {
            foreach (Control item in uxTPSubAuth.Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).ReadOnly = !status;
                }
                if (item is TrackBar)
                {
                    ((TrackBar)item).Enabled = status;
                }
                if (item is NumericUpDown)
                {
                    ((NumericUpDown)item).ReadOnly = !status;
                }
            }
        }

        private void btnKontorSil_Click(object sender, EventArgs e)
        {
            decimal anaKredi = 0;
            decimal yedekKredi = 0;
            //float.TryParse(txtAnaKredi.Text, out anaKredi);
            //float.TryParse(txtYedekKredi.Text, out yedekKredi);
            decimal.TryParse(txtAnaKredi.Text, NumberStyles.Any,
                            CultureInfo.CurrentUICulture, out anaKredi);

            decimal.TryParse(txtYedekKredi.Text, NumberStyles.Any,
                    CultureInfo.CurrentUICulture, out yedekKredi);
            long startTick = Environment.TickCount;
            if (_EKS.KrediSil(anaKredi, yedekKredi))
            {
                txOutput.Text = lbStatus.Text = String.Format(localize.getStr("xCreditsXBackupCreditsDeleted"), anaKredi, yedekKredi) + (" in " + (Environment.TickCount - startTick) + " ms");
                txOutput.Text = String.Format("\n " + localize.getStr("xCreditsXBackupCreditsDeleted"), anaKredi, yedekKredi);
            }
            else
            {
                txOutput.Text = lbStatus.Text = localize.getStr("exceptionOccuredWhileDeletingCredits");
                txOutput.AppendText("\n " + localize.getStr("exceptionOccuredWhileDeletingCredits"));
                txOutput.AppendText("\n " + localize.getStr("exceptionOccuredWhileDeletingCredits") + _EKS.SonHataMesaji);
            }
        }

        private void AK11_StatusCheckChanged(object sender, EventArgs e)
        {
            uxKartStatus.Text = Baylan.Parsers.AK11Status.SetSayacStatus(uxAK11HaftaSonuMesaiDisiVanaAcik.Checked,
                uxAK11PilKisalt.Checked,
                uxAK11PilUzat.Checked,
                uxAK11AydaIkiVanaAcmaKapama.Checked).ToString();
        }

        private void AK21_StatusCheckChanged(object sender, EventArgs e)
        {
            //uxKartStatus.Text = Baylan.Parsers.AK21Status.SetSayacStatus(uxAK11HaftaSonuMesaiDisiVanaAcik.Checked,
            //    uxAK11PilKisalt.Checked,
            //    uxAK11PilUzat.Checked,
            //    uxAK11AydaIkiVanaAcmaKapama.Checked).ToString();
        }

        private void BTNLoadNewCardReader_Click(object sender, EventArgs e)
        {
            int tmpSayacModelId = _EKS.SayacModeliId;
            SayacTipi sayacTipi = _EKS.SayacTipi;

            _EKS.Dispose();
            _EKS = null;

            _EKS = new EKSAPI();
            _EKS.SayacTipi = sayacTipi;
            _EKS.SayacModeliId = tmpSayacModelId; //1:AK-21, 2:AK-11

            txOutput.AppendText(Environment.NewLine + localize.getStr("reloadedCardReaderLibrary"));
        }

        void ProcessSelectedOperation(int selectedOperation, int index)
        {
            string message = "";
            string operationText = localize.getStr("readCard");

            Stopwatch sw = new Stopwatch();
            sw.Start();
            if (selectedOperation == 1)
            {
                if (_EKS.KartOku())
                {
                    message = operationText + localize.getStr("successfull");
                    var x = _EKS.GetCardReaderData().ToString();
                    Baylan.Parsers.Library.HexStringToByteArray(x);
                    MessageBox.Show(x);
                }
                else
                {  
                    message = operationText + localize.getStr("unsuccessfull") + Environment.NewLine +
                              _EKS.SonIslemSonuc.Result.ToString() + ":" +
                              _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine +
                              _EKS.SonIslemSonuc.ComResultType.ToString()
                              + Environment.NewLine + _EKS.SonHataMesaji + Environment.NewLine +
                              (_EKS.SonIslemSonuc.Exception != null
                                   ? _EKS.SonIslemSonuc.Exception.Message + " " +
                                     _EKS.SonIslemSonuc.Exception.StackTrace
                                   : "") +
                              (_EKS.SonIslemSonuc.ExceptionsList != null
                                   ? localize.getStr("exceptionCountEquals") + _EKS.SonIslemSonuc.ExceptionsList.Count
                                   : "")
                        ;
                }
            }
            else if (selectedOperation == 2)
            {
                operationText = localize.getStr("clearCard");

                if (_EKS.KartSil())
                {
                    message = operationText + localize.getStr("successfull");
                }
                else
                {
                    message = operationText + localize.getStr("unsuccessfull") + Environment.NewLine +
                              _EKS.SonIslemSonuc.Result.ToString() + ":" +
                              _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine +
                              _EKS.SonIslemSonuc.ComResultType.ToString()
                              + Environment.NewLine + _EKS.SonHataMesaji + Environment.NewLine +
                              (_EKS.SonIslemSonuc.Exception != null
                                   ? _EKS.SonIslemSonuc.Exception.Message + " " +
                                     _EKS.SonIslemSonuc.Exception.StackTrace
                                   : "") +
                              (_EKS.SonIslemSonuc.ExceptionsList != null
                                   ? localize.getStr("exceptionCountEquals") + _EKS.SonIslemSonuc.ExceptionsList.Count
                                   : "")
                        ;
                }

            }
            else if (selectedOperation == 3)
            {
                operationText = localize.getStr("readSerial");

                if (_EKS.KartSeriNoOku())
                {
                    message = operationText + localize.getStr("successfull");
                }
                else
                {
                    message = operationText + localize.getStr("unsuccessfull") + Environment.NewLine +
                              _EKS.SonIslemSonuc.Result.ToString() + ":" +
                              _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine +
                              _EKS.SonIslemSonuc.ComResultType.ToString()
                              + Environment.NewLine + _EKS.SonHataMesaji + Environment.NewLine +
                              (_EKS.SonIslemSonuc.Exception != null
                                   ? _EKS.SonIslemSonuc.Exception.Message + " " +
                                     _EKS.SonIslemSonuc.Exception.StackTrace
                                   : "") +
                              (_EKS.SonIslemSonuc.ExceptionsList != null
                                   ? localize.getStr("exceptionCountEquals") + _EKS.SonIslemSonuc.ExceptionsList.Count
                                   : "")

                        ;
                }

            }
            else if (selectedOperation == 4)
            {
                operationText = localize.getStr("createCardOrWrite");
                if (_EKS.KartBilgisi.AboneNo == 0 || _EKS.KartBilgisi.SayacNo == 0 || _EKS.KartBilgisi.Fiyat1 == 0 ||
                    _EKS.KartBilgisi.KademeUstSinir1 == 0)
                {
                    return;
                }
                if (_EKS.KartOlustur())
                {
                    message = operationText + localize.getStr("successfull");
                }
                else
                {
                    message = operationText + localize.getStr("unsuccessfull") + Environment.NewLine +
                              _EKS.SonIslemSonuc.Result.ToString() + ":" +
                              _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine +
                              _EKS.SonIslemSonuc.ComResultType.ToString()
                              + Environment.NewLine + _EKS.SonHataMesaji + Environment.NewLine +
                              (_EKS.SonIslemSonuc.Exception != null
                                   ? _EKS.SonIslemSonuc.Exception.Message + " " +
                                     _EKS.SonIslemSonuc.Exception.StackTrace
                                   : "") +
                              (_EKS.SonIslemSonuc.ExceptionsList != null
                                   ? localize.getStr("exceptionCount") + _EKS.SonIslemSonuc.ExceptionsList.Count
                                   : "")
                        ;
                }

            }
            else if (selectedOperation == 5)
            {
                operationText = localize.getStr("reloadReader");
                int tmpSayacModelId = _EKS.SayacModeliId;
                SayacTipi sayacTipi = _EKS.SayacTipi;

                _EKS.Dispose();
                _EKS = null;

                _EKS = new EKSAPI();
                _EKS.SayacTipi = sayacTipi;
                _EKS.SayacModeliId = tmpSayacModelId; //1:AK-21, 2:AK-11
                message = operationText + localize.getStr("successfull");
            }
            else if (selectedOperation == 6)
            {
                operationText = localize.getStr("reloadReader");
                int tmpSayacModelId = _EKS.SayacModeliId;
                SayacTipi sayacTipi = _EKS.SayacTipi;
                if (_EKS == null)
                {
                    _EKS = new EKSAPI();
                }
                float anaKredi = float.Parse(txtAnaKredi.Text);
                float yedekKredi = float.Parse(txtYedekKredi.Text);
                if (_EKS.KrediYukle(anaKredi, yedekKredi))
                {
                    message = operationText + localize.getStr("successfull");
                }
                else
                {
                    message = operationText + localize.getStr("unsuccessfull") + Environment.NewLine +
                              _EKS.SonIslemSonuc.Result.ToString() + ":" +
                              _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine +
                              _EKS.SonIslemSonuc.ComResultType.ToString()
                              + Environment.NewLine + _EKS.SonHataMesaji + Environment.NewLine +
                              (_EKS.SonIslemSonuc.Exception != null
                                   ? _EKS.SonIslemSonuc.Exception.Message + " " +
                                     _EKS.SonIslemSonuc.Exception.StackTrace
                                   : "") +
                              (_EKS.SonIslemSonuc.ExceptionsList != null
                                   ? localize.getStr("exceptionCount") + _EKS.SonIslemSonuc.ExceptionsList.Count
                                   : "")
                        ;
                }
            }
            sw.Stop();
            if (txCardHealth.InvokeRequired)
            {
                txCardHealth.Invoke((ThreadStart)delegate()
                                                  {
                                                      txCardHealth.AppendText("\n" + DateTime.Now.ToLongTimeString() + "." +
                                                                          DateTime.Now.Millisecond + " " +
                                                                          String.Format(
                                                                              operationText + ": {0}. {1} {2} ms" +
                                                                              Environment.NewLine, (index + 1), message,
                                                                              sw.ElapsedMilliseconds));
                                                      txCardHealth.ScrollToCaret();
                                                  });
            }
            else
            {
                txCardHealth.AppendText(DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond + " " +
                                    String.Format(operationText + ": {0}. {1} {2} ms" + Environment.NewLine, (index + 1),
                                                  message, sw.ElapsedMilliseconds));
                txCardHealth.ScrollToCaret();
            }

            staticClass.log(String.Format(localize.getStr("processResult"), operationText, message), priority.ack);

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            staticClass.saveSettings();
            staticClass.log(localize.getStr("applicationShutDown"), priority.final);
            clStatic.qEvent.quitEventProp = true;
        }      
        private void btRead_Click(object sender, EventArgs e)
        {           
            if (bw.IsBusy)
            {
                lbStatus.ForeColor = Color.DarkRed;
                lbStatus.Text = localize.getStr("readingProcessContinuesPleaseWait");
                return;
            }
            staticClass.log(localize.getStr("cardReadInitialized"), priority.ack);
            if (CMBAboneTipi.SelectedIndex == 13)
            {
                 List<Baylan.Models.ISKRAControlCardDataTransfer> data = _EKS.ReadControlCardData();
                 txOutput.Clear();
                 txOutput.AppendText("Okunan Kontrol Kart adedi:" + data.Count + Environment.NewLine);
                 foreach (var item in data)
                 {
                     //txOutput.AppendText("Pil Seviyesi:" + item.BatteryLevel+"\tAbone No:"+item.ConsumerNumber+Environment.NewLine);
                     txOutput.AppendText("Sayac No:" + item.MeterNumber + "\tSayac Tarihi:" + item.MeterDateTime.ToShortDateString() + " " + item.MeterDateTime.ToLongTimeString() + Environment.NewLine);
                     txOutput.AppendText("Sayac tipi:" + item.MeterType + "\tCezalar:" + (item.Interferences != null ? item.Interferences.Length : 0) + Environment.NewLine);
                 }
            }
            else
            {
                bw.RunWorkerAsync();
            }
        }       

        private void btIade_Click(object sender, EventArgs e)
        {
            if (_EKS.IadeIcinHazirla())
            {
                txOutput.Text = lbStatus.Text = localize.getStr("consumersToRefundCard");
                txOutput.AppendText("\n " + localize.getStr("consumerstoRefundCard"));
                staticClass.log(localize.getStr("consumerstoRefundCard"), priority.ack);
            }
            else
            {
                lbStatus.Text = localize.getStr("errorOccuredWhileDefiningRefundCard");
                txOutput.AppendText("\n " + localize.getStr("errorOccuredWhileDefiningRefundCard"));
                txOutput.AppendText("\n " + localize.getStr("exceptionDetails") + _EKS.SonHataMesaji);
                staticClass.log(localize.getStr("consumerstoRefundCard"), priority.error);
                staticClass.log(localize.getStr("exceptionDetails") + _EKS.SonHataMesaji, priority.error);
            }
        }

        private void btSeriNoOku_Click(object sender, EventArgs e)
        {
            long startTick = Environment.TickCount;
            if (_EKS.KartSeriNoOku())
            {                
                txOutput.Text = lbStatus.Text = localize.getStr("cardIDEquals") + _EKS.KartBilgisi.KartSeriNo + (" in " + (Environment.TickCount - startTick) + " ms"); ;
            }
            else
            {
                txOutput.AppendText("\n " + _EKS.SonHataMesaji);
            }
        }

        private void btWrite_Click(object sender, EventArgs e)
        {
            //if (!staticClass.loggedInUser.yetkiList.Contains(auth.create_system_card_permission) &&
            //  CMBAboneTipi.SelectedIndex != 0)
            //{
            //    lbStatus.Text = "Sistem kartı yapma yetkiniz yok !!!" ;
            //    return; 
            //}            
            GetOperatorSelectedMeterType();
            SetCardData2Write();
            staticClass.log(localize.getStr("cardWriteInitialized"), priority.ack);
            long startTick = Environment.TickCount;

            if (_EKS.KartOlustur())
            {
                txOutput.Text = lbStatus.Text = localize.getStr("cardCreatedSuccessfully") + (" in " + (Environment.TickCount - startTick) + " ms");
                staticClass.log(localize.getStr("cardCreatedSuccessfully"), priority.ack);
                uxCBZeroTariffs.Checked = false;

            }
            else
            {
                lbStatus.Text = localize.getStr("cardCreationFailed");
                staticClass.log(localize.getStr("cardCreationFailed"), priority.error);
                staticClass.log(_EKS.SonHataMesaji, priority.error);
                txOutput.Text = localize.getStr("cardCreationFailed") + " : " + _EKS.SonHataMesaji;
            }
        }     
        private void btTestCard_Click(object sender, EventArgs e)
        {
            try
            {
                //-Seciniz- = 0
                //Kart Oku
                //Kart Sil
                //Seri No Oku
                //Kart Oluştur
                //Okuyucu Yükle

                staticClass.log(localize.getStr("cardTestInitialized"), priority.ack);
                int selectedOperation2 = uxRepatedOperationType2.SelectedIndex;
                int selectedOperation1 = uxRepatedOperationType1.SelectedIndex;

                for (int i = 0; i < (int)uxOkumaAdedi.Value; i++)
                {
                    if (selectedOperation1 > 0)
                    {
                        ProcessSelectedOperation(selectedOperation1, i);
                    }
                    if (selectedOperation2 > 0)
                    {
                        ProcessSelectedOperation(selectedOperation2, i);
                    }
                }
            }
            catch (Exception ex)
            {
                if (txCardHealth.InvokeRequired)
                {
                    txCardHealth.Invoke((ThreadStart)delegate()
                    {
                        txCardHealth.AppendText(ex.Message);
                        txCardHealth.ScrollToCaret();
                    });
                }
                else
                {
                    txCardHealth.AppendText(ex.Message);
                    txCardHealth.ScrollToCaret();
                }
            }
            finally { }
        }

        private void btAddNewUser_Click(object sender, EventArgs e)
        {
            using (user usr = new user())
            {
                usr.userName = localize.getStr("newUser");
                usr.password = "baylan";
                usr.yetkiList = new List<auth>();
                lstKullanicilar.Items.Add(usr);
                lstKullanicilar.DisplayMember = "userName";
            }
            propGrid.SelectedObject = lstKullanicilar.Items[lstKullanicilar.Items.Count - 1];
        }

        string GetPaddedString(string param1, string param2)
        {
            int len1 = param1.Length;
            int len2 = param2.Length;
            int totalPadLength = _padRightSize;

            return param1.PadRight((totalPadLength + len1) - (len1), '_') + "" + param2;
        }

        private void propGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (lstKullanicilar.Items.Contains(propGrid.SelectedObject))
            {
                object tmp = propGrid.SelectedObject;
                lstKullanicilar.Items.Remove(propGrid.SelectedObject);
                propGrid.SelectedObject = tmp;
                lstKullanicilar.Items.Add(propGrid.SelectedObject);
                lstKullanicilar.Refresh();
            }
        }

        private void btDeleteUser_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(localize.getStr("selectedUserInfoWillBeDeleted"),
                                localize.getStr("baylanWaterMeters"), MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
            { return; }
            try
            {
                lstKullanicilar.Items.Remove(lstKullanicilar.SelectedItem);
                lstKullanicilar.Refresh();
            }
            catch { ;  }
        }

        private void lstKullanicilar_SelectedIndexChanged(object sender, EventArgs e)
        {
            propGrid.SelectedObject = lstKullanicilar.SelectedItem;
        }

        private void btSaveUserList_Click(object sender, EventArgs e)
        {
            staticClass.userList.Clear();
            bool permission = false;
            foreach (user usr in lstKullanicilar.Items)
            {
                staticClass.userList.Add(usr);
                if (usr.yetkiList.Count > 0) permission = true;
            }
            if (!permission) { lbStatus.Text = localize.getStr("pleaseDefinePermissionsForDefinedUsers"); return; }
            if (!staticClass.saveUsers()) { lbStatus.Text = localize.getStr("uacOrUserPrivilageException"); }
            else
            {
                lbStatus.Text = localize.getStr("userDefinitionSuccessfullySaved");
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 4)
            {
                lstKullanicilar.Items.Clear();
                foreach (user usr in staticClass.userList) { lstKullanicilar.Items.Add(usr); }
                lstKullanicilar.DisplayMember = "userName";
            }
        }

        private void cbLanguage_SelectedValueChanged(object sender, EventArgs e)
        {
            staticClass.settings.languageSetting = (languages)cbLanguage.SelectedItem;
            localize.selectedLng = (languages)cbLanguage.SelectedItem;
            FixGUI();
            if (!staticClass.saveSettings()) { lbStatus.Text = localize.getStr("uacOrUserPrivilageException"); }
            else { lbStatus.Text = localize.getStr("settingsSaved"); }
        }

        private void btQueryWaterworks_Click(object sender, EventArgs e)
        {
            EKSInternal eks = new EKSInternal(_EKS);

            string msg = "";
            if (uxCBSurekli.Checked)
            {
                while (true)
                {
                    txOutput.Text = "Idare taraniyor!";
                    eks.KartOkuyucuIdareDonder();
                    txOutput.Text = eks.LastOperationResult + " Okundu";
                    if (MessageBox.Show("Yeni Kart Koyunuz!", "Devam", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
                    {
                        break;
                    }
                }
            }
            else
            {
                txOutput.Text = "Idare taraniyor!";
                eks.KartOkuyucuIdareDonder();
                txOutput.Text = eks.LastOperationResult + " Okundu";
            }
        }

        private void uxAK11IkincilFonksiyonlar_CheckedChanged(object sender, EventArgs e)
        {
            if (uxAK11IkincilFonksiyonlar.Checked)
            {
                label4.Visible = false; txtKademe1.Visible = false;
                label5.Visible = false; txtKademe2.Visible = false;
                label6.Visible = false; txtKademe3.Visible = false;
                label28.Visible = false; txtKademe4.Visible = false;
                label27.Visible = false; txtKademe5.Visible = false;
                label28.Visible = false; txtKademe6.Visible = false;
                label9.Visible = false; txtTarife1.Visible = false;
                label8.Visible = false; txtTarife2.Visible = false;
                label7.Visible = false; txtTarife3.Visible = false;
                label10.Visible = false; txtTarife4.Visible = false;
                label31.Visible = false; txtTarife5.Visible = false;
                label30.Visible = false; txtTarife6.Visible = false;
                label29.Visible = false; txtTarife7.Visible = false;
                label26.Visible = false;
                uxGBHygenicValveUsage.Enabled = true;
            }
            else
            {
                label4.Visible = true; txtKademe1.Visible = true;
                label5.Visible = true; txtKademe2.Visible = true;
                label6.Visible = true; txtKademe3.Visible = true;
                label28.Visible = true; txtKademe4.Visible = true;
                label27.Visible = true; txtKademe5.Visible = true;
                label28.Visible = true; txtKademe6.Visible = true;
                label9.Visible = true; txtTarife1.Visible = true;
                label8.Visible = true; txtTarife2.Visible = true;
                label7.Visible = true; txtTarife3.Visible = true;
                label10.Visible = true; txtTarife4.Visible = true;
                label31.Visible = true; txtTarife5.Visible = true;
                label30.Visible = true; txtTarife6.Visible = true;
                label29.Visible = true; txtTarife7.Visible = true;
                label26.Visible = true;
                uxGBHygenicValveUsage.Enabled = false;
            }
        }            
      
        private void uxCBTimeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (uxCBTimeType.SelectedIndex == 0)
            {
                uxLBLTimeType.Text = "";
                uxTBTimeData.Value = 0;
                return;
            }
            if (uxCBTimeType.SelectedIndex == 1)
            {
                uxLBLTimeType.Text = "Saniye";
                zamantipi = EKS_v3.RfZamanTipleri.Saniye;
                return;
            }
            if (uxCBTimeType.SelectedIndex == 2)
            {
                uxLBLTimeType.Text = "Dakika";
                zamantipi = EKS_v3.RfZamanTipleri.Dakika;
                return;
            }
            if (uxCBTimeType.SelectedIndex == 3)
            {
                uxLBLTimeType.Text = "Saat";
                zamantipi = EKS_v3.RfZamanTipleri.Saat;
                return;
            }
        }

        private void uxTBTimeData_Scroll(object sender, EventArgs e)
        {
            uxLBLTrackBarValue.Text = uxTBTimeData.Value + "";
        }

        private void uxBTNSilVeYukle_Click(object sender, EventArgs e)
        {
            _EKS.KartOku();

            var yuklenecekKredi = _EKS.KartBilgisi.Kredi;
            var yedekMiktar = _EKS.KartBilgisi.YedekKredi;

            _EKS.KartSil();
            _EKS.KartBilgisi.DonemGunSayisi = 1;

            try { _EKS.KartBilgisi.SayacNo = Convert.ToInt32(txtSayacNo.Text); }
            catch { lbStatus.Text = "Invalid meter number assigned !!!"; return; }
            try
            {              
                {
                    _EKS.KartBilgisi_v3.AboneNoLong = Convert.ToInt64(txtAboneNo.Text);
                    _EKS.KartBilgisi_v3.AboneNo = (int)Convert.ToInt64(txtAboneNo.Text);
                }

            }
            catch { lbStatus.Text = "Invalid consumer number assigned !!!"; return; }
            _EKS.KartBilgisi.Fiyat1 = 1;
            _EKS.KartBilgisi.KademeUstSinir1 = 9999;

            _EKS.KartBilgisi.Kredi = yuklenecekKredi;
            _EKS.KartBilgisi.YedekKredi = yedekMiktar;

            if (!_EKS.KrediYukle(yuklenecekKredi + yedekMiktar, yedekMiktar))
            {
                MessageBox.Show(_EKS.SonHataMesaji);
            }
        }      
        private void uxBTNDec10m3_Click(object sender, EventArgs e)
        {
            if (_EKS.KartBilgisi.YedekKredi > 5)
            {
                float yeniAnaKredi = (_EKS.KartBilgisi.Kredi + _EKS.KartBilgisi.YedekKredi) - 5;

                _EKS.KrediSil(5f, 5f);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
            decimal kredi = 13;
            decimal yedekKredi = 0;
            float orjinalKredi = kartIslemleri.KartBilgisi.Kredi;
            float orjinalYedekKredi = kartIslemleri.KartBilgisi.YedekKredi;

            kartIslemleri.KartBilgisi.Kredi += (float)kredi;
            if (kartIslemleri.KartBilgisi_v2.YedekKredi < 5)
            {
                yedekKredi = 5;
                //kredi -= Config.Data.MinimumYedek;
                kartIslemleri.KartBilgisi.YedekKredi += (float)yedekKredi;
                kartIslemleri.KartBilgisi.Kredi -= (float)yedekKredi;

            }
            kartIslemleri.KartBilgisi_v3.AnaKrediYuklendi = false;
            kartIslemleri.KartBilgisi_v3.YedekKrediYuklendi = false;

            if (EksApi.KartBilgisi.Kredi != orjinalKredi || EksApi.KartBilgisi.YedekKredi != orjinalYedekKredi)
            {
                EksApi.KartOku();
                EksApi.KartBilgisi.Kredi = orjinalKredi;
                EksApi.KartBilgisi.YedekKredi = orjinalYedekKredi;
            }

            float yuklemedenOncekiKredi = EksApi.KartBilgisi.Kredi + EksApi.KartBilgisi.YedekKredi;
            bool krediYuklemeTamam = false;
            //EksApi.SayacModeliId = 6;
            if (yedekKredi > 5)
            {
                yedekKredi = 5;
            }

            if (EksApi.KrediYukle(kredi, yedekKredi))
            {
            }
            else
            {

            }

            */
        }

        private void CMBAboneTipi_SelectedIndexChanged(object sender, EventArgs e)
        {
            uxLBAnaKredi.Text = "Ana Kredi :";
            uxLBYedekKredi.Text = "Yedek Kredi :";
            uxLBAnaKredi.ForeColor = Color.Black;
            uxLBYedekKredi.ForeColor = Color.Black;
            uxGBRFParametreler.Visible = false;

            uxLBLAvansKredi.Visible = false;
            uxTBAvansKredi.Visible = false;

            if (CMBAboneTipi.SelectedIndex == 9)
            {
                uxLBAnaKredi.Text = "Ayarlanacak Kredi :";
                uxLBYedekKredi.Text = "Ayarlanacak Tüketim :";
                uxLBAnaKredi.ForeColor = Color.Red;
                uxLBYedekKredi.ForeColor = Color.Red;
            }
            else if (CMBAboneTipi.SelectedIndex == 11)
            {
                uxGBRFParametreler.Visible = true;
                uxCBRFTipi.SelectedIndex = 0;
                uxCBRFZamanTipi.SelectedIndex = 0;
            }
            else if (CMBAboneTipi.SelectedIndex == 10)
            {
                uxLBLAvansKredi.Visible = true;
                uxTBAvansKredi.Visible = true;
            }
        }

        byte _freq0;
        byte _freq1;
        byte _freq2;

        private void uxTBFreq0_TextChanged(object sender, EventArgs e)
        {
            string txt = ((TextBox)sender).Text;
            string controlName = ((TextBox)sender).Name;
            if (controlName.Contains("Freq0"))
            {
                try
                {
                    _freq0 = Convert.ToByte(txt);
                }
                catch { }
                string formattedValue = String.Format("{0:X}", _freq0);
                uxLBLFreq0.Text = formattedValue;
            }
            else if (controlName.Contains("Freq1"))
            {
                try
                {
                    _freq1 = Convert.ToByte(txt);
                }
                catch { }
                string formattedValue = String.Format("{0:X}", _freq1);
                uxLBLFreq1.Text = formattedValue;
            }
            else
            {
                try
                {
                    _freq2 = Convert.ToByte(txt);
                }
                catch { }

                string formattedValue = String.Format("{0:X}", _freq2);
                uxLBLFreq2.Text = formattedValue;
            }
        }
        byte _rfTipi = 0; //default 8-byte baylan
        private void uxCBRFTipi_SelectedIndexChanged(object sender, EventArgs e)
        {
            _rfTipi = (byte)uxCBRFTipi.SelectedIndex;          
        }       

        private void mainFrm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F10 && e.Control)
            {
                Utilities util = new Utilities();
                util.ShowDialog();
                if (uxCBLongAboneNo.Checked)
                {
                    uxCBLongAboneNo.Checked = !uxCBLongAboneNo.Checked;
                }
            }
        }

        private void uxNUDAuthNo_ValueChanged(object sender, EventArgs e)
        {
            uxTBAuthNo.Value = (int)uxNUDAuthNo.Value;
        }

        private void uxTBAuthNo_ValueChanged(object sender, EventArgs e)
        {
            uxNUDAuthNo.Value = uxTBAuthNo.Value;
        }

        private void uxBTNCreateSubAuthCard_Click(object sender, EventArgs e)
        {
            if (_EKS.SayacModeliId != 6 && _EKS.SayacModeliId != 9)
            {
                lbStatus.Text = localize.getStr("cardCreationFailed");

                return;
            }
            _EKS.KartBilgisi_v4.KartTuru_v2 = (KartTuru_v2.YetkiKarti | KartTuru_v2.KontrolKarti | KartTuru_v2.ZamanKarti | KartTuru_v2.AboneKarti);          
            _EKS.KartBilgisi_v4.IdareNo = uxTBAuthNo.Value;
            _EKS.KartBilgisi_v4.IdareAdi = uxTBIdareAdi.Text;
            if (_EKS.KartOlustur())
            {
                txOutput.Text = lbStatus.Text = localize.getStr("cardCreatedSuccessfully");// +(" in " + (Environment.TickCount - startTick) + " ms");
                staticClass.log(localize.getStr("cardCreatedSuccessfully"), priority.ack);
            }
            else
            {
                lbStatus.Text = localize.getStr("cardCreationFailed");
                staticClass.log(localize.getStr("cardCreationFailed"), priority.error);
                staticClass.log(_EKS.SonHataMesaji, priority.error);
                txOutput.Text = localize.getStr("cardCreationFailed") + " : " + _EKS.SonHataMesaji;
            }           
        }

        private void uxBTNConvertMeterType_Click(object sender, EventArgs e)
        {
            if (_EKS.KartSeriNoOku() && _EKS.KartBilgisi_v4.KartSeriNo != _lastReadCardSerialNumber)
            {
                MessageBox.Show("Tip değiştirilmek istenen kart ile okuyucudaki kart farklı, lütfen önceki kartla tekrar deneyiniz!", "Kartlar Farklı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (_readKartBilgisi.SayacNo == 0)
            {
                MessageBox.Show("Tip değiştirilmek istenen karttaki sayaç no 0 veya kart boş lütfen tanımlı bir kart ile tekrar deneyiniz!", "Kart tanımsız veya boş", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            if (string.IsNullOrEmpty(uxCBNewMeterType.Text) || _meterModelsByMeterName[uxCBNewMeterType.Text] == _EKS.SayacModeliId)
            {
                MessageBox.Show("Sayaç modeli boş veya aynı lütfen doğru model seçtiğinizden emin olunuz!", "model seçim hatası", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (_readKartBilgisi == null)
            {
                return;
            }
            EKSAPI _eks = _EKS;
            //eski model texti gosterim icin _meterModelsByModelId isimli Dictionary<key:int, value:string> den seciliyor
            string eskiModel = _meterModelsByModelId[_eks.SayacModeliId];
            if (_eks.KartSil())
            {
                //Yeni Sayac tipi Sayac tiplerinin oldugu ve secilen ComboBox dan (uxCBNewMeterType) aliniyor - tip bulmak icin _meterModelsByMeterName isimli Dictionary<key:string, value:int> kullaniyor                
                _eks.SayacModeliId = _meterModelsByMeterName[uxCBNewMeterType.Text];
                if (_eks.KartOku())
                {
                    _eks.KartBilgisi.AboneNo = _readKartBilgisi.AboneNo;
                    _eks.KartBilgisi.SayacNo = _readKartBilgisi.SayacNo;
                    _eks.KartBilgisi.Fiyat1 = _readKartBilgisi.Fiyat1;
                    _eks.KartBilgisi.Fiyat2 = _readKartBilgisi.Fiyat2;
                    _eks.KartBilgisi.Fiyat3 = _readKartBilgisi.Fiyat3;
                    _eks.KartBilgisi.Fiyat4 = _readKartBilgisi.Fiyat4;
                    _eks.KartBilgisi.Fiyat5 = _readKartBilgisi.Fiyat5;
                    _eks.KartBilgisi_v4.Fiyat6 = _readKartBilgisi.Fiyat6;
                    _eks.KartBilgisi_v4.Fiyat7 = _readKartBilgisi.Fiyat7;

                    _eks.KartBilgisi_v4.KademeUstSinir1 = _readKartBilgisi.KademeUstSinir1;
                    _eks.KartBilgisi_v4.KademeUstSinir2 = _readKartBilgisi.KademeUstSinir2;
                    _eks.KartBilgisi_v4.KademeUstSinir3 = _readKartBilgisi.KademeUstSinir3;
                    _eks.KartBilgisi_v4.AboneNoLong = _readKartBilgisi.AboneNoLong;
                    _eks.KartBilgisi_v4.KademeUstSinir4 = _readKartBilgisi.KademeUstSinir4;
                    _eks.KartBilgisi_v4.KademeUstSinir5 = _readKartBilgisi.KademeUstSinir5;
                    _eks.KartBilgisi_v4.KademeUstSinir6 = _readKartBilgisi.KademeUstSinir6;

                    _eks.KartBilgisi_v4.Kredi = _readKartBilgisi.Kredi;
                    _eks.KartBilgisi_v4.YedekKredi = _readKartBilgisi.YedekKredi;

                    _eks.KartBilgisi_v4.KartStatus = _readKartBilgisi.KartStatus;
                    _eks.KartBilgisi_v4.KartStatus3 = _readKartBilgisi.KartStatus3;
                    _eks.KartBilgisi_v4.KartDurumExtra = _readKartBilgisi.KartDurumExtra;
                    _eks.KartBilgisi_v4.KartTipi = _readKartBilgisi.KartTipi;
                    _eks.KartBilgisi_v4.KartTuru = _readKartBilgisi.KartTuru;
                    _eks.KartBilgisi_v4.KartTuru_v2 = _readKartBilgisi.KartTuru_v2;

                    _eks.YazilacakEkBilgi.AboneTipi = _readYazilacakEkBilgi.AboneTipi;
                    _eks.YazilacakEkBilgi.BayramBaslangici1 = _readYazilacakEkBilgi.BayramBaslangici1;
                    _eks.YazilacakEkBilgi.BayramBaslangici2 = _readYazilacakEkBilgi.BayramBaslangici2;
                    _eks.YazilacakEkBilgi.BayramBitisi1 = _readYazilacakEkBilgi.BayramBitisi1;
                    _eks.YazilacakEkBilgi.BayramBitisi2 = _readYazilacakEkBilgi.BayramBitisi2;

                    _eks.GenelParametreler.YanginModuSuresi = _readGenelParametreler.YanginModuSuresi;
                    //_eks.SayacModeliId = 10;
                    if (_eks.KartOlustur())
                    {

                        string message = "Yeni tip tanimlamasi yapildi. Eski tip " + eskiModel + ", yeni model:" + uxCBNewMeterType.Text;
                        
                        _eks.SayacModeliId = _meterModelsByMeterName[uxCBNewMeterType.Text];

                        if (_eks.KartOku())
                        {
                            if ((_readKartBilgisi.Kredi > 0 || _readKartBilgisi.YedekKredi > 0))
                            {
                                if (_eks.KrediYukle(_readKartBilgisi.Kredi + _readKartBilgisi.YedekKredi, _readKartBilgisi.YedekKredi))
                                {
                                    message += " karta kredi transferide gerceklesti, ana kredi:" + _readKartBilgisi.Kredi + ", yedek kredi:" + _readKartBilgisi.YedekKredi;
                                }
                                else
                                {
                                    message += " kredi yuklemede hata:" + _eks.SonHataMesaji;
                                }
                            }
                        }
                        MessageBox.Show(message, "Kart tanimlama islem sonucu");
                    }

                    _readGenelParametreler = null;
                    _readKartBilgisi = null;
                    _readYazilacakEkBilgi = null;
                    _lastReadCardSerialNumber = "";

                }
            }
        }

        private void uxCBIsiSayaci_CheckedChanged(object sender, EventArgs e)
        {
            if (uxCBIsiSayaci.Checked) { _EKS.SayacTipi = SayacTipi.SicakSu; } else { _EKS.SayacTipi = SayacTipi.SogukSu; }
        }

        private void uxCBZeroTariffs_CheckedChanged(object sender, EventArgs e)
        {
            frmPortSettings.Config.IsZeroTariffsAllowed = ((CheckBox)sender).Checked;
        }

        private void uxBTNEjectCrtReader_Click(object sender, EventArgs e)
        {
            _EKS.PortuKapat();
        }

        private void uxBTNOpenCreditTransaction_Click(object sender, EventArgs e)
        {
            _EKS.KartBilgisi_v4.KartTuru_v3 = EKS.KartTuru_v3.Acma;
            if (_EKS.KartOlustur())
            {
                txOutput.AppendText("Kredi acma karti olusturma basarili!");
            }
            else
            {
                txOutput.AppendText("Hata : Kredi acma karti olusturma BASARISIZ!" + _EKS.SonHataMesaji);

            }
        }

        private void uxCBDSIKapatma_CheckedChanged(object sender, EventArgs e)
        {
            if (sender is CheckBox)
            {
                AK21Status.XTarihindeVanaKapamaAktif = ((CheckBox)sender).Checked;
            }
        }    
    }
}
