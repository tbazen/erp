﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ApiTest.Properties;
using Baylan.TranslationLibrary;

namespace ApiTest
{
    public partial class pwdFrm : Form
    {
        public pwdFrm()
        {
            InitializeComponent();
            fixUI();
        }

        private int expected = 0;
        mainFrm frm;

        private void fixUI()
        {
            if (
                System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "tr")
            {
                localize.selectedLng = languages.turkce;
            }
            else if (
                System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "en")
            {
                localize.selectedLng = languages.english;
            }
            else if (
               System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "hu")
            {
                localize.selectedLng = languages.hungarian;
            }
            else if (
               System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName == "sk")
            {
                localize.selectedLng = languages.slovak;
            }
            this.label1.Text = localize.getStr("passwordEquals");
            this.btEnter.Text = localize.getStr("enter");
            this.btCancel.Text = localize.getStr("cancel");
            this.prompt.Text = localize.getStr("baylanWaterMeters"); 
        }

        private void btEnter_Click(object sender, EventArgs e)
        {            
            int theDay = DateTime.Now.Day;
            int theMonth = DateTime.Now.Month;

            staticClass.log(String.Format(localize.getStr("loginAttemptDetails"), "admin", "******"),priority.ack); 
            expected = (theMonth * 2) + theDay;

            if (expected.ToString() + DateTime.Now.Year.ToString() == passInput.Text)
            {                
                if (staticClass.userList.Count > 0) 
                {                  
                    prompt.Text = localize.getStr("usersExistAdministratorPasswordDenied");                        
                    staticClass.log(localize.getStr("usersExistAdministratorPasswordDenied"), priority.ack);                                     
                    return;
                }

                staticClass.loggedInUser = new user() { userName = "admin", password = passInput.Text };
                staticClass.loggedInUser.yetkiList = new List<auth>() { auth.settings_access_permission, 
                                                                        auth.refund_card_creation_permission, 
                                                                        auth.card_read_permission,
                                                                        auth.clear_card_permission,
                                                                        auth.define_card_permission,
                                                                        auth.card_write_permission,
                                                                        auth.clear_credits_permission,
                                                                        auth.load_credits_permission,
                                                                        auth.update_parameters_permission } ;

                prompt.Text = String.Format(localize.getStr("passwordVerified"), "admin", passInput.Text); 
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Visible = false;
                frm = new mainFrm();
                frm.Show();                 
            }
            else
            {
                prompt.Text = localize.getStr("passwordDenied");
            }

            #region Log the permissions
            if (staticClass.loggedInUser != null && staticClass.loggedInUser.yetkiList != null && staticClass.loggedInUser.yetkiList.Count > 0)
            {
                foreach (auth ath in staticClass.loggedInUser.yetkiList)
                {
                    staticClass.log(ath.ToString(), priority.ack);
                }
            }
            #endregion Log the permissions
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;          
            this.Visible = false;
            staticClass.log("applicationShutDown", priority.final); 
            Application.Exit();
            this.Close(); 
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btEnter_Click(sender, e);
            else prompt.Text = localize.getStr("baylanWaterMeters"); 
            if (e.KeyCode == (Keys.ControlKey | Keys.F12))
            {
                int theDay = DateTime.Now.Day;
                int theMonth = DateTime.Now.Month;
                expected = (theMonth * 2) + theDay;
                passInput.Text = expected.ToString() + DateTime.Now.Year.ToString();
                //staticClass.log(localize.getStr("shortcutKeyUsed"), priority.ack); 
                btEnter.PerformClick();               
            }
        }      

        private void Form2_Shown(object sender, EventArgs e)
        {
            passInput.Focus(); 
        }
    }
}
