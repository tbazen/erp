﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ApiTest.Properties;
using Baylan.TranslationLibrary;

namespace ApiTest
{
    public partial class pwdWithUserFrm : Form
    {
        public pwdWithUserFrm()
        {
            InitializeComponent();
            fixUI();
        }

        private int expected = 0;
        mainFrm frm;

        private void fixUI()
        {            
            this.label1.Text = localize.getStr("passwordEquals");
            this.label2.Text = localize.getStr("userName");                 
            this.btEnter.Text = localize.getStr("enter");
            this.btCancel.Text = localize.getStr("cancel");
            this.prompt.Text = localize.getStr("baylanWaterMeters"); 
        }

        private void btEnter_Click(object sender, EventArgs e)
        {
            staticClass.log(String.Format(localize.getStr("loginAttemptDetails"), userNameInput.Text, "******"), priority.ack); 

            if (staticClass.userList.Count > 0)
            {
                foreach (user usr in staticClass.userList)
                {
                    if (usr.userName == userNameInput.Text)
                    {
                        if (usr.password == passInput.Text)
                        {
                            staticClass.loggedInUser = usr; 
                            prompt.Text = localize.getStr("passwordVerified");
                            staticClass.log(String.Format(localize.getStr("passwordVerified"), userNameInput.Text, "******"), priority.ack); 
                            this.DialogResult = System.Windows.Forms.DialogResult.OK;
                            this.Visible = false;
                            frm = new mainFrm();
                            frm.Show();
                            return; 
                        }                      
                    }
                    else
                    {
                        prompt.Text = String.Format(localize.getStr("passwordDeniedWithCredentials"),userNameInput.Text, passInput.Text);
                        staticClass.log(String.Format(localize.getStr("passwordDeniedWithCredentials"), userNameInput.Text, passInput.Text), priority.ack); 
                    }
                }
            }
            else
            {
                int theDay = DateTime.Now.Day;
                int theMonth = DateTime.Now.Month;

                expected = (theMonth * 2) + theDay;

                if (expected.ToString() + DateTime.Now.Year.ToString() == passInput.Text)
                {                 
                    prompt.Text = localize.getStr("passwordVerified");
                    staticClass.log(String.Format(localize.getStr("passwordVerified"), userNameInput.Text, passInput.Text), priority.ack); 
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Visible = false;
                    frm = new mainFrm();
                    frm.Show();
                }
                else
                {
                    prompt.Text = localize.getStr("passwordDenied");
                }
            }                    
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;          
            this.Visible = false;
            Application.Exit();
            this.Close(); 
        }           

        private void Form2_Shown(object sender, EventArgs e)
        {
            userNameInput.Focus(); 
        }                    

        private void passInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btEnter_Click(sender, e);
            else prompt.Text = localize.getStr("baylanWaterMeters");        
        }
            
    }
}
