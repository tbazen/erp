﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Baylan.TranslationLibrary;

namespace ApiTest
{
    [Serializable]
    public class settingsClass : IDisposable
    {
        [Description("Yazılım kullanım dili")]
        [Category("Ayarlar")]
        [DisplayName("Dil Ayarı")]
        public languages languageSetting { get; set; }     

        [Description("Yazılım Ayarları Bulundu")]
        [Category("Ayarlar")]
        [DisplayName("Yazılım Ayarları Bulundu")]
        public bool settingsFound { get; set; }

        #region IDisposable Members

        public void Dispose()  { ; }
        #endregion
    }

}
