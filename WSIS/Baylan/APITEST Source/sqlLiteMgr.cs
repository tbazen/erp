﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace ApiTest
{
    public class sqLiteMgr : IDisposable
    {
        #region Fields
        public string connStr;
        #endregion Fields

        public sqLiteMgr(string dbPath)
        {
            connStr = @"Data Source=\" + dbPath + "ApiTest.db;Version=3;New=False;Compress=" + ("False") + ";";  
            //connStr = @"Data Source=\Flash Disk\Baylan\Data\demoT.db;Version=3;New=False;Compress=" + ("False") + ";";            
        }

        public bool try2Connect()
        {
            using (SQLiteConnection conn = new SQLiteConnection(connStr))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    try
                    {
                        conn.Open(); return true;
                    }
                    catch (Exception ex)
                    {
                        #region Try to Create DB                        
                        cmd.Parameters.Clear();            
                        cmd.CommandText = "Create database ApiTest";                                        

                        try
                        {
                           cmd.ExecuteNonQuery();
                        }
                        catch (Exception createEx)
                        {
                           //box.log(ex.Message, priority.fatal, true);
                        }                                              
                        #endregion Try to Create DB
                        return false;
                    }
                    finally { conn.Close(); }
                }
            }
        }

        /*
        void psion_readingLogNeedsUpdateHandler(object source, psionHalDetectionLog theDetectionLog)
        {
            if (stClass.tempLog.recentDetectionDate.HasValue)
            {
                if (stClass.tempLog.recentDetectionDate.Value.Second == theDetectionLog.recentDetectionDate.Value.Second)
                {
                    return;
                }
            }
            lock (this)
            {
                using (SQLiteConnection connect = new SQLiteConnection(connStr))
                {
                    try
                    {
                        connect.Open();
                    }
                    catch (Exception Ex)
                    {
                        box.log("Database could not be opened. Details :" + Ex.Message, priority.fatal, true);
                    }
                    using (SQLiteCommand cmd = new SQLiteCommand(connect))
                    {
                        cmd.CommandText = "insert into readingLogs( deviceNr, recentIndex, detectionDate) values (@param1,@param2,@param3)";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SQLiteParameter("@param1"));
                        cmd.Parameters["@param1"].DbType = DbType.Int32;
                        cmd.Parameters["@param1"].Value = theDetectionLog.deviceNr;

                        cmd.Parameters.Add(new SQLiteParameter("@param2"));
                        cmd.Parameters["@param2"].DbType = DbType.Decimal;
                        cmd.Parameters["@param2"].Value = theDetectionLog.recentIndex;

                        cmd.Parameters.Add(new SQLiteParameter("@param3"));
                        cmd.Parameters["@param3"].DbType = DbType.DateTime;
                        cmd.Parameters["@param3"].Value = theDetectionLog.recentDetectionDate;

                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            box.log(ex.Message, priority.fatal, true);
                        }
                    }
                }
                stClass.tempLog = theDetectionLog;
            }
        }      
        public bool updateAmrDevice(amrDev theDev)
        {
            using (SQLiteConnection connect = new SQLiteConnection(connStr))
            {
                try
                {
                    connect.Open();
                }
                catch (Exception Ex)
                {
                    box.log("Database could not be opened. Details :" + Ex.Message, priority.fatal, true);
                }
                using (SQLiteCommand cmd = new SQLiteCommand(connect))
                {
                    cmd.CommandText = "update abonedata set Yeni_Endeks=@param1,Okuma_Durumu=@param2," +
                                      "Okuma_Tarihi=@param3,Zaman=@param4,sonOkuma_tarihi=@param5 where Modul_No=@param6";

                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SQLiteParameter("@param1"));
                    cmd.Parameters["@param1"].DbType = DbType.Decimal;
                    cmd.Parameters["@param1"].Value = theDev.recentIndex.Value;

                    cmd.Parameters.Add(new SQLiteParameter("@param2"));
                    cmd.Parameters["@param2"].DbType = DbType.Decimal;
                    cmd.Parameters["@param2"].Value = 2;

                    cmd.Parameters.Add(new SQLiteParameter("@param3"));
                    cmd.Parameters["@param3"].DbType = DbType.DateTime;
                    cmd.Parameters["@param3"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SQLiteParameter("@param4"));
                    cmd.Parameters["@param4"].DbType = DbType.DateTime;
                    cmd.Parameters["@param4"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SQLiteParameter("@param5"));
                    cmd.Parameters["@param5"].DbType = DbType.DateTime;
                    cmd.Parameters["@param5"].Value = DateTime.Now;

                    cmd.Parameters.Add(new SQLiteParameter("@param6"));
                    cmd.Parameters["@param6"].DbType = DbType.Int64;
                    cmd.Parameters["@param6"].Value = theDev.deviceNr;

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        box.log(ex.Message, priority.fatal, true);
                    }
                }
            }
            return true;
        }
        public bool getAllAMRdefinitions()
        {
            stClass.psion.searchLst.Clear();
            if (!try2Connect()) return false;
            amrDev theDevice;

            using (SQLiteConnection connect = new SQLiteConnection(connStr))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(connect))
                {
                    using (SQLiteDataAdapter adp = new SQLiteDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        try
                        {
                            cmd.CommandText = "select * from abonedata order by abone_no";
                            adp.Fill(ds, "aboneData");
                            foreach (DataRow row in ds.Tables["abonedata"].Rows)
                            {
                                theDevice = new amrDev();
                                theDevice.adress = row["Adres"].ToString();
                                theDevice.meterOrValve = true;                 // Set default to meter
                                theDevice.moduleOrRepeater = true;             // Set module. 
                                theDevice.name = row["Abone_Adi"].ToString();
                                theDevice.number = row["Abone_No"].ToString();
                                theDevice.priorDetectionDate = new DateTime(2000, 1, 1, 1, 1, 1, 1);
                                try { theDevice.priorIndex = Convert.ToDecimal(row["Eski_Endeks"]); }
                                catch { theDevice.priorIndex = 0; }
                                theDevice.radius = 20;
                                theDevice.receiptPrinted = false;
                                theDevice.recentDetectionDate = new DateTime(2000, 1, 1, 1, 1, 1, 1);
                                theDevice.recentIndex = 0;
                                theDevice.recentManualIndex = 0;
                                try
                                {
                                    theDevice.deviceNr = UInt64.Parse(row["Modul_No"].ToString());
                                }
                                catch
                                {
                                    box.log("Invalid device number detected : " + row["Modul_No"].ToString(), priority.fatal,
                                             true);
                                    continue;
                                }
                                try
                                {
                                    stClass.psion.searchLst.Add(theDevice);
                                }
                                catch (Exception ex)
                                {
                                    box.log("Failed loading consumer definitions. Most probably repeatitive module-water meter number exists in database : " + ex.Message, priority.fatal,
                                             true);
                                    continue;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            box.log("Failed loading consumer definitions. Most probably repeatitive module-water meter number exists in database : " + ex.Message, priority.fatal,
                                            true);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        public bool getAllAMRdefinitions(string specialPurposeQuery)
        {
            stClass.psion.searchLst.Clear();
            if (!try2Connect()) return false;
            amrDev theDevice;

            using (SQLiteConnection connect = new SQLiteConnection(connStr))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(connect))
                {
                    using (SQLiteDataAdapter adp = new SQLiteDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        try
                        {
                            cmd.CommandText = specialPurposeQuery;
                            adp.Fill(ds, "aboneData");
                            foreach (DataRow row in ds.Tables["abonedata"].Rows)
                            {
                                theDevice = new amrDev();
                                theDevice.adress = row["Adres"].ToString();
                                theDevice.meterOrValve = true;                 // Set default to meter
                                theDevice.moduleOrRepeater = true;             // Set module. 
                                theDevice.name = row["Abone_Adi"].ToString();
                                theDevice.number = row["Abone_No"].ToString();
                                theDevice.priorDetectionDate = new DateTime(2000, 1, 1, 1, 1, 1, 1);
                                try { theDevice.priorIndex = Convert.ToDecimal(row["Eski_Endeks"]); }
                                catch { theDevice.priorIndex = 0; }
                                theDevice.radius = 20;
                                theDevice.receiptPrinted = false;
                                theDevice.recentDetectionDate = new DateTime(2000, 1, 1, 1, 1, 1, 1);
                                theDevice.recentIndex = 0;
                                theDevice.recentManualIndex = 0;
                                try
                                {
                                    theDevice.deviceNr = UInt64.Parse(row["Modul_No"].ToString());
                                }
                                catch
                                {
                                    box.log("Invalid device number detected : " + row["Modul_No"].ToString(), priority.fatal,
                                             true);
                                    continue;
                                }

                                try
                                {
                                    stClass.psion.searchLst.Add(theDevice);
                                }
                                catch (Exception ex)
                                {
                                    box.log("Failed loading consumer definitions. Most probably repeatitive module-water meter number exists in database : " + ex.Message, priority.fatal,
                                             true);
                                    continue;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            box.log("Failed loading consumer definitions. Most probably repeatitive module-water meter number exists in database : " + ex.Message, priority.fatal,
                                            true);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        public bool getAllAMRdefinitions(string specialPurposeQuery, out int count)
        {
            stClass.psion.searchLst.Clear();
            if (!try2Connect()) { count = 0; return false; }

            using (SQLiteConnection connect = new SQLiteConnection(connStr))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(connect))
                {
                    try
                    {
                        connect.Open();
                        cmd.CommandText = specialPurposeQuery;
                        count = Convert.ToInt32(cmd.ExecuteScalar());

                    }
                    catch (Exception ex)
                    {
                        box.log("Failed loading consumer definitions. Most probably repeatitive module-water meter number exists in database : " + ex.Message, priority.fatal,
                                        true);
                        count = 0;
                        return false;
                    }
                }
            }
            return true;
        }
        public bool install50KRecord()
        {
            using (SQLiteConnection connect = new SQLiteConnection(connStr))
            {
                try
                {
                    connect.Open();
                }
                catch (Exception Ex)
                {
                    box.log("Database could not be opened. Details :" + Ex.Message, priority.fatal, true);
                }

                using (SQLiteCommand cmd = new SQLiteCommand(connect))
                {
                    cmd.CommandText = "insert into abonedata (Abone_No, Modul_No, Sayac_No, Abone_Adi, Cadde_Adi,Sokak_Adi,Eski_Endeks,Yeni_Endeks,Okuma_Durumu,Adres,Aciklama) " +
                                      " values (@param1,@param2,@param3,@param4,@param5,@param6,@param7,@param8,@param9,@param10,@param11)";

                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SQLiteParameter("@param1"));
                    cmd.Parameters["@param1"].DbType = DbType.Int32;

                    cmd.Parameters.Add(new SQLiteParameter("@param2"));
                    cmd.Parameters["@param2"].DbType = DbType.Int32;
                    cmd.Parameters.Add(new SQLiteParameter("@param3"));
                    cmd.Parameters["@param3"].DbType = DbType.Int32;
                    cmd.Parameters.Add(new SQLiteParameter("@param4"));
                    cmd.Parameters["@param4"].DbType = DbType.String;
                    cmd.Parameters.Add(new SQLiteParameter("@param5"));
                    cmd.Parameters["@param5"].DbType = DbType.String;
                    cmd.Parameters.Add(new SQLiteParameter("@param6"));
                    cmd.Parameters["@param6"].DbType = DbType.String;
                    cmd.Parameters.Add(new SQLiteParameter("@param7"));
                    cmd.Parameters["@param7"].DbType = DbType.Int32;
                    cmd.Parameters.Add(new SQLiteParameter("@param8"));
                    cmd.Parameters["@param8"].DbType = DbType.Int32;
                    cmd.Parameters.Add(new SQLiteParameter("@param9"));
                    cmd.Parameters["@param9"].DbType = DbType.Int32;
                    cmd.Parameters.Add(new SQLiteParameter("@param10"));
                    cmd.Parameters["@param10"].DbType = DbType.String;
                    cmd.Parameters.Add(new SQLiteParameter("@param11"));
                    cmd.Parameters["@param11"].DbType = DbType.String;


                    for (int i = 160276; i < 210276; i++)
                    {
                        cmd.Parameters["@param1"].Value = i - 160270;
                        cmd.Parameters["@param2"].Value = i;
                        cmd.Parameters["@param3"].Value = i - 160270;
                        cmd.Parameters["@param4"].Value = "Abone Adi " + (i - 160270).ToString();
                        cmd.Parameters["@param5"].Value = "Cadde Adi " + (i - 160270).ToString();
                        cmd.Parameters["@param6"].Value = "Sokak Adi " + (i - 160270).ToString();
                        cmd.Parameters["@param7"].Value = 0;
                        cmd.Parameters["@param8"].Value = 0;
                        cmd.Parameters["@param9"].Value = 0;
                        cmd.Parameters["@param10"].Value = "Adres " + (i - 160270).ToString() + " - Abone " + (i - 160270).ToString() + " adresi";
                        cmd.Parameters["@param11"].Value = "Aciklama " + (i - 160270).ToString();
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            box.log(ex.Message, priority.fatal, true);
                        }
                    }
                }
                return true;
            }
        }
        public List<string> retrieveDistinctItems(string field)
        {
            if (!try2Connect()) return null;
            List<string> temp = new List<string>();

            using (SQLiteConnection connect = new SQLiteConnection(connStr))
            {
                using (SQLiteCommand cmd = new SQLiteCommand(connect))
                {
                    using (SQLiteDataAdapter adp = new SQLiteDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        try
                        {
                            cmd.CommandText = "select distinct(" + field + ") from abonedata order by " + field;
                            adp.Fill(ds, "aboneData");
                            foreach (DataRow row in ds.Tables["abonedata"].Rows)
                            {
                                temp.Add(row[field].ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            box.log("Failed retrieving distinct items..." + ex.Message, priority.fatal,
                                            true);
                            return null;
                        }
                    }
                }
            }
            return temp;
        }
        //public bool getAllAMRdetections()
        //{
        //stClass.searchLst.Clear();            
        //if (!connect()) return false ; 
        //  #region Save Reading Record
        //try
        //{
        //    using (SQLiteConnection conn = new SQLiteConnection("DataSource=demoT.db"))
        //    {
        //        conn.Open();
        //        using (IDbTransaction trns = conn.BeginTransaction())
        //        {
        //            try
        //            {
        //                using (IDbCommand cmd = conn.CreateCommand())
        //                {
        //                    cmd.CommandText = "insert into record(content,dt) values('{0}','{1}')";
        //                    /// ÖNEMLİ 
        //                    //cmd.CommandText = string.Format(cmd.CommandText, c.recordContent, c.recentDetection);
        //                    cmd.CommandText = string.Format(cmd.CommandText, vdStr, c.recentDetection);
        //                    cmd.ExecuteNonQuery();
        //                    trns.Commit();
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                //dashBoard.log("Veritabanına yazılamadı: " + ex.Message); 
        //                trns.Rollback();
        //            }
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    dashBoard.log("Veritabanına yazım sırasında hata oluştu. Detaylar : " + ex.Message);
        //}
        //    #endregion Save Reading Record           
        //  return true; 
        //}       
          
         
         */

        #region IDisposable Members
        public void Dispose()
        {
            ; // throw new NotImplementedException();
        }
       
        #endregion
            
    }
}
