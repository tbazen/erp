﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Baylan.TranslationLibrary;

namespace ApiTest
{
    public enum priority
    {
        ack,                        // Acknowledgement
        init,                       // Initialization
        final,                      // Finalization
        error,                      // Error        
        warning                     // Warning
    }

    public static class staticClass
    {
        static staticClass() { settings = new settingsClass(); userList = new List<user>(); }

        public static settingsClass settings;
        public static List<user> userList;
        public static user loggedInUser;
        public static bool loggingEnabled = true ; 
        public delegate void visualException(string exceptionMsg);
        public static event visualException visualExceptionHandler;     

        public static bool saveSettings()
        {
            FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\ApiTest.set", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            try
            {
                bf.Serialize(fs, settings);
                fs.Close();
                staticClass.log(localize.getStr("saveSettingsSucceeded"), priority.ack); 
                return true;
            }
            catch(Exception ex)
            {
                staticClass.log(localize.getStr("saveSettingsFailed"), priority.error) ;
                staticClass.log(ex.Message, priority.error);
                visualExceptionHandler(localize.getStr("saveSettingsFailed"));
                return false;
            }
            finally
            {
                if (fs != null) fs.Close(); 
            }
        }
        public static bool readSettings()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = null;  
            try
            {
                fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\ApiTest.set", FileMode.Open);
                settings = (settingsClass)formatter.Deserialize(fs);
                staticClass.log(localize.getStr("readSettingsSucceeded"), priority.ack); 
                return true;
            }
            catch (Exception ex)
            {
                staticClass.log(localize.getStr("readSettingsFailed"), priority.error);
                staticClass.log(ex.Message, priority.ack);
                visualExceptionHandler(localize.getStr("readSettingsFailed"));
                return false;
            }
            finally
            {
                if (fs != null) fs.Close(); 
            }          
        }

        public static bool readUsers()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = null ; 
            try
            {               
                fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\ApiTest.usr", FileMode.Open);
                userList = (List<user>)formatter.Deserialize(fs);
                staticClass.log(localize.getStr("readUsersSucceeded"), priority.ack); 
                return true;
            }
            catch(Exception ex)
            {
                staticClass.log(localize.getStr("readUsersFailed"),priority.error) ;
                staticClass.log(ex.Message, priority.error);
                visualExceptionHandler(localize.getStr("readUsersFailed"));
                return false; 
            }
            finally
            {
                if (fs != null) fs.Close(); 
            }
        }
        public static bool saveUsers()
        {
            try
            {
                using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"\ApiTest.usr", FileMode.Create))
                {
                    try
                    {
                        if (userList == null) return false;
                        BinaryFormatter bf = new BinaryFormatter();
                        bf.Serialize(fs, userList);
                        return true;
                    }
                    catch
                    { return false; }
                    finally { if (fs != null) fs.Close(); }
                }                
            }
            catch
            {
                visualExceptionHandler(localize.getStr("saveUsersFailed"));
                return false; 
            }
        }
   
        public static void log(string msg, priority baslik)
        {
            if (!loggingEnabled) return;

            try { Directory.CreateDirectory(/*AppDomain.CurrentDomain.BaseDirectory*/AppDomain.CurrentDomain.BaseDirectory + @"\Logs\ApiTest"); } 
            catch (Exception ex) { loggingEnabled = true ; }

            try { Directory.CreateDirectory(Directory.GetCurrentDirectory() + @"\logs") ; }
            catch (Exception ex) { loggingEnabled = false ; }

            string logFile = AppDomain.CurrentDomain.BaseDirectory + @"\logs\ApiTest_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + ".lg";
            string backupFile = AppDomain.CurrentDomain.BaseDirectory + @"\logs\ApiTest\ApiTest_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + ".lg";

            // Try backup under Windows Logs
            #region Windows Backup
            try
            {
                using (TextWriter txBackup = new StreamWriter(backupFile, true))
                {
                    string dt = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "  -> ";
                    dt = dt.PadRight(25, ' ');                   
                    txBackup.Write(dt);
                    switch (baslik)
                    {
                        case priority.ack: txBackup.Write(localize.getStr("acknowledgementEquals")); break;
                        case priority.init: for (int i = 0; i < 50; i++) { txBackup.Write("X"); } txBackup.Write(" "); break;
                        case priority.final: for (int i = 0; i < 50; i++) { txBackup.Write("X"); } txBackup.Write(" "); break;
                        case priority.error: txBackup.Write(localize.getStr("errorEquals")); break;
                        case priority.warning: txBackup.Write(localize.getStr("warningEquals")); break;
                        default: break;
                    }                  
                    txBackup.WriteLine(msg);
                    txBackup.Close(); 
                }
            }
            catch (Exception ex) { visualExceptionHandler(localize.getStr("loggerFailed") + " " + ex.Message); loggingEnabled = true; }
            #endregion Windows Backup
            #region Folder Backup
            try
            {
                using (TextWriter txWrite = new StreamWriter(logFile, true))
                {                  
                        string dt = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "  -> ";
                        dt = dt.PadRight(25, ' ');
                        txWrite.Write(dt);
                        switch (baslik)
                        {
                            case priority.ack: txWrite.Write(localize.getStr("acknowledgementEquals")); break;
                            case priority.init: for (int i = 0; i < 50; i++) { txWrite.Write("X"); } txWrite.Write(" ") ; break;
                            case priority.final: for (int i = 0; i < 50; i++) { txWrite.Write("X"); } txWrite.Write(" ") ; break;
                            case priority.error: txWrite.Write(localize.getStr("errorEquals"));  break;
                            case priority.warning: txWrite.Write(localize.getStr("warningEquals")); break;
                            default: break;
                        }
                        txWrite.WriteLine(msg); txWrite.Close();                                   
                }
            }
            catch (Exception ex)
            {
                visualExceptionHandler(localize.getStr("loggerFailed") + " " + ex.Message) ; 
                loggingEnabled = false;
            }
            #endregion Folder Backup
        }      
    }
}
