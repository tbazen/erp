﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ApiTest
{
    [Serializable]
    public enum auth
    {
        [Description("Read Card")]
        card_read_permission,
        [Description("Write Card")]
        card_write_permission,
        [Description("Clear Card")]
        clear_card_permission,
        [Description("Create Refund Card")]
        refund_card_creation_permission,
        [Description("Define Card")]
        define_card_permission,
        [Description("Load Credits")]
        load_credits_permission,
        [Description("Clear Card Permission")]
        clear_credits_permission,
        [Description("Card Test Screen Permission")]
        card_test_screen_permission,        
        [Description("Update Parameters Permission")]
        update_parameters_permission,
        [Description("Settings Access Permission")]
        settings_access_permission,
        [Description("Create System Card Permission")]
        create_system_card_permission
    }

    [Serializable]
    public class user : IDisposable
    {        
        [Description("Yazılım kullanıcısının adı.")]
        [Category("Kullanıcı Tanımı")]
        [DisplayName("Kullanıcı Adı")]
        public string userName { get; set; }

        [Category("Kullanıcı Tanımı")]
        [PasswordPropertyText(true)]
        [Description("Yazılım kullanıcısının şifresi. Ayarlar ekranına erişim hakkı olan kullanıcı tarafından değiştirilebilir")]
        [DisplayName("Şifre")]
        public string password { get ; set; }

        [Category("Kullanıcı Yetkileri")]
        [Description("Kullanıcının sahip olduğu yetki listesi")]
        [DisplayName("Yetkiler")]

        public List<auth> yetkiList { get; set; }

        #region IDisposable Members

        public void Dispose() { ; }
        #endregion
    }
}