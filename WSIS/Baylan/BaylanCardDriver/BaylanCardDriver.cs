﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INTAPS.SubscriberManagment.Client.PrepaidDriver;

namespace INTAPS.SubscriberManagment.Client.BaylanInterface
{
    public class BaylanCardDriver: INTAPS.SubscriberManagment.Client.PrepaidDriver.IPrepaidCardDriver,CardReader.IProgressMonitor
    {
        CardReader reader;
        public BaylanCardDriver()
        {
            reader = new CardReader(this);
        }

        public bool supportsReadConnectionID => true;

        public void AppendLogText(string p)
        {
            Console.WriteLine(p);
        }

        public void clearFail(string msg)
        {
            throw new NotImplementedException();
        }

        public void clearSuccess()
        {
            throw new NotImplementedException();
        }



        ConnectionIDRead read;
        CardReaderFailed failed;
        public void readConnectionID(ConnectionIDType idType, ConnectionIDRead read, CardReaderFailed failed)
        {
            if (reader.isBusy())
            {
                failed("Card reader is busy",null);
            }
            this.read = read;
            this.failed = failed;
            reader.startRead();
        }

        public void readFailed(string msg)
        {
            this.failed("Card reader failed",null);
        }

        public void setCardData(CardReader.CardData cardData)
        {
            this.read(cardData.connectionID);
        }

        public void setProgress(int val)
        {
            
        }

        public void SetStatusText(string v)
        {
            this.AppendLogText(v);
        }

        public void ShowMessage(string v)
        {
            this.AppendLogText(v);
        }

       

        public void writeFail(string msg)
        {
            throw new NotImplementedException();
        }

        public void writeSuccess(long v)
        {
            throw new NotImplementedException();
        }

        TopupOk topupOk;
        public void topUp(int connectionID,String tag,int meterNo,double cubicMeters, TopupOk ok, CardReaderFailed failed)
        {
            this.topupOk = ok;
            this.failed = failed;
            this.reader.topUp(connectionID, tag, meterNo, 0, (int)cubicMeters, 3);
        }

        public void topUpSuccess()
        {
            this.topupOk();
        }

        public void topUpFail()
        {
            failed("Topup failed", null);
        }
    }
}
