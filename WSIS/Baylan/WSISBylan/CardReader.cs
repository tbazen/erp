﻿using Baylan;
using EKS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTAPS.SubscriberManagment.Client.BaylanInterface
{
    public class CardReader
    {
        public class CardData
        {
            public String rawData;
            public int cbTimeType;
            public int cbTimeValue;
            public string connectionID;
            public string meterNo;
            public string tarife1;
            public string tarife2;
            public string tarife3;
            public string tarife4;
            public string tarife5;
            public string tarife6;
            public string tarife7;
            public string kademe1;
            public string kademe2;
            public string kademe3;
            public string kademe4;
            public string kademe5;
            public string kademe6;
            public string aboneTipi;
            public string kritikKredi;
            public string mainCredit;
            public string reserveCredit;
            public string sayacKredi;
            public string refundValue;
            public string sayacTuketim;
            public string modelName;
            public object modelID;
        }
        public interface IProgressMonitor
        {
            void setProgress(int val);
            void ShowMessage(string v);
            void AppendLogText(string p);
            void SetStatusText(string v);
            void setCardData(CardData cardData);
            void readFailed(string msg);
            void writeSuccess(long v);
            void writeFail(string msg);
            void clearSuccess();
            void clearFail(string msg);
            void topUpSuccess();
            void topUpFail();
        }
        class StringProvider
        {
            public String getStr(string v)
            {
                return v;
            }
        }
        public enum CardMeterType
        {
            AK211,
            AK411
        }

        public bool isBusy()
        {
            return bw.IsBusy;
        }


        BackgroundWorker bw;
        EKSAPI _EKS;

        public void startRead()
        {
            bw.RunWorkerAsync();
        }

        EKS_v3.RfZamanTipleri zamantipi;
        IProgressMonitor _monitor;
        IKartBilgisi_v3 _readKartBilgisi = new KartBilgisi();
        IYazilacakEkBilgi _readYazilacakEkBilgi = new YazilacakEkBilgi();
        IGenelParametreler _readGenelParametreler = new GenelParametreler();
        string _lastReadCardSerialNumber;
        CardMeterType _meterType;
        StringProvider localize = new StringProvider();
        public CardMeterType MeterType {
            get => _meterType;
            set
            {
                _meterType = value;
                switch (_meterType)
                {
                    case CardMeterType.AK211:
                        _EKS.SayacModeliId = 1;
                        break;
                    case CardMeterType.AK411:
                        _EKS.SayacModeliId = 20;
                        break;
                    default:
                        break;
                }
            }

        }

        List<string> _meterTypes = new List<string>();
        Dictionary<string, int> _meterModelsByMeterName = new Dictionary<string, int>();
        Dictionary<int, string> _meterModelsByModelId = new Dictionary<int, string>();
        private int _padRightSize=45;
        public String getMeterName(int modelID)
        {
            if (_meterModelsByModelId.ContainsKey(modelID))
                return _meterModelsByModelId[modelID];
            return "Uknown";
        }
        void initModelTypes()
        {
            _meterTypes.Add("AK-111");
            _meterModelsByMeterName.Add(_meterTypes[0], 2);
            _meterModelsByModelId.Add(2, _meterTypes[0]);

            _meterTypes.Add("AK-211");
            _meterModelsByMeterName.Add(_meterTypes[1], 1);
            _meterModelsByModelId.Add(1, _meterTypes[1]);

            _meterTypes.Add(localize.getStr("heatMeter"));
            _meterModelsByMeterName.Add(_meterTypes[2], 3);
            _meterModelsByModelId.Add(3, _meterTypes[2]);

            _meterTypes.Add(localize.getStr("AK21CommonUse"));
            _meterModelsByMeterName.Add(_meterTypes[3], 4);
            _meterModelsByModelId.Add(4, _meterTypes[3]);

            _meterTypes.Add(localize.getStr("GPRSmeterWithCard"));
            _meterModelsByMeterName.Add(_meterTypes[4], 5);
            _meterModelsByModelId.Add(5, _meterTypes[4]);

            _meterTypes.Add("AK-311");
            _meterModelsByMeterName.Add(_meterTypes[5], 6);
            _meterModelsByModelId.Add(6, _meterTypes[5]);

            _meterTypes.Add("AK-211(BUSKI)");
            _meterModelsByMeterName.Add(_meterTypes[6], 7);
            _meterModelsByModelId.Add(7, _meterTypes[6]);

            _meterTypes.Add("Isi Sayaci Sicak Soguk");
            _meterModelsByMeterName.Add(_meterTypes[7], 11);
            _meterModelsByModelId.Add(11, _meterTypes[7]);

            _meterTypes.Add("AK-311 ISI Sayaci/Heat Meter");
            _meterModelsByMeterName.Add(_meterTypes[8], 10);
            _meterModelsByModelId.Add(10, _meterTypes[8]);

            _meterTypes.Add(localize.getStr("AK21CommonUse").Replace("21", "311"));
            _meterModelsByMeterName.Add(_meterTypes[9], 9);
            _meterModelsByModelId.Add(9, _meterTypes[9]);

            _meterTypes.Add("AK-311 ISKRA");
            _meterModelsByMeterName.Add(_meterTypes[10], 12);
            _meterModelsByModelId.Add(12, _meterTypes[10]);

            _meterTypes.Add("AK-411");
            _meterModelsByMeterName.Add(_meterTypes[11], 20);
            _meterModelsByModelId.Add(20, _meterTypes[11]);

            _meterTypes.Add("US-411");
            _meterModelsByMeterName.Add(_meterTypes[12], 21);
            _meterModelsByModelId.Add(21, _meterTypes[12]);

            _meterModelsByModelId.Add(0, "");
        }
        public CardReader(IProgressMonitor monitor)
        {
            initModelTypes();
            _monitor = monitor;
            EKS_v2.Logger.StartTiming("EKSAPI Create Instance");
            _EKS = new EKSAPI();
            EKS_v2.Logger.StopTiming();
            this.MeterType = CardMeterType.AK211;
            bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
        }
        string GetPaddedString(string param1, string param2)
        {
            int len1 = param1.Length;
            int len2 = param2.Length;
            int totalPadLength = _padRightSize;

            return param1.PadRight((totalPadLength + len1) - (len1), '_') + "" + param2;
        }
        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            _monitor.setProgress(0);
            try
            {
                if (_readYazilacakEkBilgi == null) { _readYazilacakEkBilgi = new Baylan.YazilacakEkBilgi(); }
                _readKartBilgisi = null;
                if (_readKartBilgisi == null) { _readKartBilgisi = new KartBilgisi(); }
                if (_readGenelParametreler == null) { _readGenelParametreler = new GenelParametreler(); }

                Stopwatch sw = new Stopwatch();
                sw.Start();
                {

                    if (_EKS.SonIslemSonuc.Result != Baylan.Common.ResultTypes.Success)
                    {
                        String msg= _EKS.SonIslemSonuc.Result.ToString() + " " + _EKS.SonIslemSonuc.ResultDescription;
                        _monitor.readFailed(msg);
                        _monitor.ShowMessage(msg);
                        _EKS.SonIslemSonuc.Result = Baylan.Common.ResultTypes.Success;
                    }

                   
                    _monitor.AppendLogText("Card initialization time " + sw.ElapsedMilliseconds + " ms" + Environment.NewLine);
                    _monitor.SetStatusText("Card initialization time  " + sw.ElapsedMilliseconds + " ms");
                        
                    sw.Stop();
                    sw.Reset();
                    string message = "";
                    sw.Start();
                    _EKS.IsAK11ExtraFunctionality = false;

                    _EKS.ClearReaderData();
                    _EKS.SayacTipi = SayacTipi.SogukSu;
                    //_EKS.SayacModeliId = 6; 
                    if (_EKS.KartOku())
                    {
                        if (_EKS.KartBilgisi_v4.SayacNo > 0)
                        {
                            
                        }
                        _lastReadCardSerialNumber = _EKS.KartBilgisi.KartSeriNo;
                        _readKartBilgisi.AboneNo = _EKS.KartBilgisi.AboneNo;
                        _readKartBilgisi.AboneNoLong = _EKS.KartBilgisi_v4.AboneNoLong;
                        _readKartBilgisi.SayacNo = _EKS.KartBilgisi.SayacNo;
                        _readKartBilgisi.Fiyat1 = _EKS.KartBilgisi.Fiyat1;
                        _readKartBilgisi.Fiyat2 = _EKS.KartBilgisi.Fiyat2;
                        _readKartBilgisi.Fiyat3 = _EKS.KartBilgisi.Fiyat3;
                        _readKartBilgisi.Fiyat4 = _EKS.KartBilgisi.Fiyat4;
                        _readKartBilgisi.Fiyat5 = _EKS.KartBilgisi.Fiyat5;
                        _readKartBilgisi.Fiyat6 = _EKS.KartBilgisi_v4.Fiyat6;
                        _readKartBilgisi.Fiyat7 = _EKS.KartBilgisi_v4.Fiyat7;

                        _readKartBilgisi.KademeUstSinir1 = _EKS.KartBilgisi_v4.KademeUstSinir1;
                        _readKartBilgisi.KademeUstSinir2 = _EKS.KartBilgisi_v4.KademeUstSinir2;
                        _readKartBilgisi.KademeUstSinir3 = _EKS.KartBilgisi_v4.KademeUstSinir3;
                        _readKartBilgisi.KademeUstSinir4 = _EKS.KartBilgisi_v4.KademeUstSinir4;
                        _readKartBilgisi.KademeUstSinir5 = _EKS.KartBilgisi_v4.KademeUstSinir5;
                        _readKartBilgisi.KademeUstSinir6 = _EKS.KartBilgisi_v4.KademeUstSinir6;

                        _readKartBilgisi.Kredi = _EKS.KartBilgisi.Kredi;
                        _readKartBilgisi.YedekKredi = _EKS.KartBilgisi.YedekKredi;

                        _readKartBilgisi.KartStatus = _EKS.KartBilgisi_v4.KartStatus;
                        _readKartBilgisi.KartStatus3 = _EKS.KartBilgisi_v4.KartStatus3;
                        _readKartBilgisi.KartDurumExtra = _EKS.KartBilgisi_v4.KartDurumExtra;
                        _readKartBilgisi.KartTipi = _EKS.KartBilgisi_v4.KartTipi;
                        _readKartBilgisi.KartTuru = _EKS.KartBilgisi_v4.KartTuru;
                        _readKartBilgisi.KartTuru_v2 = _EKS.KartBilgisi_v4.KartTuru_v2;

                        _readYazilacakEkBilgi.AboneTipi = _EKS.YazilacakEkBilgi.AboneTipi;
                        _readYazilacakEkBilgi.BayramBaslangici1 = _EKS.YazilacakEkBilgi.BayramBaslangici1;
                        _readYazilacakEkBilgi.BayramBaslangici2 = _EKS.YazilacakEkBilgi.BayramBaslangici2;
                        _readYazilacakEkBilgi.BayramBitisi1 = _EKS.YazilacakEkBilgi.BayramBitisi1;
                        _readYazilacakEkBilgi.BayramBitisi2 = _EKS.YazilacakEkBilgi.BayramBitisi2;

                        _readGenelParametreler.YanginModuSuresi = _EKS.GenelParametreler.YanginModuSuresi;

                        string cezaGiderenKart = (_EKS.KartBilgisi_v3.CezaGiderenYetkiKarti != 0 ? String.Format("Ceza Gideren Yetki Karti : {0:X}", _EKS.KartBilgisi_v3.CezaGiderenYetkiKarti) : "");

                        if (_EKS.KartBilgisi_v3.KartTuru_v2 == KartTuru_v2.YetkiKarti)
                        {
                            cezaGiderenKart = String.Format("Yetki Karti No :{0}", _EKS.KartBilgisi_v3.KartSeriNo.Substring(4));
                        }

                        _monitor.AppendLogText(localize.getStr("cardReadSucceeded"));
                        _monitor.ShowMessage(Environment.NewLine + localize.getStr("cardSerialNrEquals") + " " + _EKS.KartBilgisi.KartSeriNo + Environment.NewLine +
                             cezaGiderenKart + Environment.NewLine
                            + localize.getStr("meterModelIDEquals") + " " + _EKS.SayacModeliId + Environment.NewLine
                            + localize.getStr("consumerNrEquals") + " " + _EKS.KartBilgisi.AboneNo + Environment.NewLine
                            + localize.getStr("waterMeterNrEquals") + " " + _EKS.KartBilgisi.SayacNo + Environment.NewLine

                            + localize.getStr("creditEquals") + " " + _EKS.KartBilgisi.Kredi + Environment.NewLine
                            + localize.getStr("backupCreditsEquals") + " " + _EKS.KartBilgisi.YedekKredi + Environment.NewLine
                            + localize.getStr("recentReadingDateEquals") + " " + _EKS.KartBilgisi.SayacOkumaZamani.ToShortDateString() + " " + _EKS.KartBilgisi.SayacOkumaZamani.ToLongTimeString() + Environment.NewLine
                            + localize.getStr("totalConsumptionEquals") + " " + _EKS.KartBilgisi.ToplamTuketilenKredi + Environment.NewLine
                            + localize.getStr("creditsInMeterEquals") + " " + _EKS.KartBilgisi.SayactakiKredi + Environment.NewLine
                            + localize.getStr("meterDateEquals") + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToLongTimeString() + Environment.NewLine
                            + localize.getStr("debtCreditsEquals") + " " + _EKS.KartBilgisi_v2.BorcKredi + Environment.NewLine
                            + localize.getStr("consumedCreditsEquals") + " " + _EKS.KartBilgisi_v2.TuketilenKredi);

                        sw.Stop();
                        if (_EKS.SonIslemSonuc.Result != Baylan.Common.ResultTypes.Success)
                        {
                            _monitor.ShowMessage(_EKS.SonIslemSonuc.ResultDescription);
                            //break;
                        }

                        message = Environment.NewLine + "ID: " + _EKS.DeviceID + Environment.NewLine;
                        message += localize.getStr("cardSerialNrEquals") + " " + _EKS.KartBilgisi.KartSeriNo + "".PadRight(_padRightSize) + cezaGiderenKart + Environment.NewLine;
                        message +=
                            localize.getStr("consumerNrEquals") + " " + (_EKS.KartBilgisi_v3.AboneNo + ", Long:" + _EKS.KartBilgisi.AboneNo) + Environment.NewLine +
                                   localize.getStr("waterMeterNrEquals") + " " + _EKS.KartBilgisi.SayacNo + Environment.NewLine +
                                   GetPaddedString(localize.getStr("tariff1Equals") + " " + _EKS.KartBilgisi.Fiyat1,
                                   localize.getStr("tariff2Equals") + " " + _EKS.KartBilgisi.Fiyat2) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("tariff3Equals") + " " + _EKS.KartBilgisi.Fiyat3 + "", localize.getStr("tariff4Equals") + " " + _EKS.KartBilgisi.Fiyat4) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("tariff5Equals") + " " + _EKS.KartBilgisi.Fiyat5, localize.getStr("tariff6Equals") + " " + _EKS.KartBilgisi_v2.Fiyat6) + Environment.NewLine +
                                   localize.getStr("tariff7Equals") + " " + _EKS.KartBilgisi_v2.Fiyat7 + "".PadRight(_padRightSize) + Environment.NewLine;
                        ;
                        message +=
                                   GetPaddedString(localize.getStr("step1Equals") + " " + _EKS.KartBilgisi.KademeUstSinir1,
                                   localize.getStr("step2Equals") + " " + _EKS.KartBilgisi.KademeUstSinir2) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("step3Equals") + " " + _EKS.KartBilgisi.KademeUstSinir3,
                                   localize.getStr("step4Equals") + " " + _EKS.KartBilgisi.KademeUstSinir4) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("step5Equals") + " " + _EKS.KartBilgisi_v2.KademeUstSinir5,
                                   (Baylan.Parsers.AK21Status3.Isfuture1Aktif == false ?
                                   localize.getStr("step6Equals") + " " + _EKS.KartBilgisi_v2.KademeUstSinir6 : "Hygenic Valve!")) + Environment.NewLine
                        ;
                        message +=
                                   GetPaddedString(localize.getStr("cardTypeEquals") + " " + _EKS.KartBilgisi.KartTuru.ToString(), localize.getStr("baylanCardTypeEquals") + " " + _EKS.KartBilgisi_v4.KartTuru_v3) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("creditEquals") + " " + _EKS.KartBilgisi.Kredi, localize.getStr("backupCreditsEquals") + " " + _EKS.KartBilgisi.YedekKredi) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("creditsToRefundEquals") + " " + _EKS.KartBilgisi.IadeEdilecekKredi, localize.getStr("creditsInMeterEquals") + " " + _EKS.KartBilgisi.SayactakiKredi) + Environment.NewLine
                            ;
                        message +=

                                   GetPaddedString(localize.getStr("batteryLevelEquals") + " " + _EKS.KartBilgisi.PilDurumu, localize.getStr("diameterEquals") + " " + _EKS.KartBilgisi.SayacCapi) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("recentCreditUpdateEquals") + " " + _EKS.KartBilgisi.SayacaYuklemeZamani.ToShortDateString() + " " + _EKS.KartBilgisi.SayacaYuklemeZamani.ToLongTimeString(),
                                   localize.getStr("recentReadingDateEquals") + " " + _EKS.KartBilgisi.SayacOkumaZamani.ToShortDateString()) + Environment.NewLine +
                                   localize.getStr("meterDateEquals") + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.SayacTarihi.ToLongTimeString() + Environment.NewLine;

                        message +=
                                   GetPaddedString(localize.getStr("leakageAmountEquals") + " " + _EKS.KartBilgisi.SizintiMiktari, localize.getStr("recentPulseReceiveDateEquals") + " " + _EKS.KartBilgisi.SonPulsZamani.ToShortDateString()) + Environment.NewLine +
                                   GetPaddedString(localize.getStr("totalConsumptionEquals") + " " + _EKS.KartBilgisi.ToplamTuketilenKredi + "(m3) ", localize.getStr("consumptionInTermEquals") + _EKS.KartBilgisi_v2.DonemIciTuketim + " (m3)") + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("termCreditsEquals") + " " + _EKS.KartBilgisi_v2.DonemKredi, localize.getStr("debtCreditsEquals") + " " + _EKS.KartBilgisi_v2.BorcKredi) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("consumedCreditsEquals") + " " + _EKS.KartBilgisi_v2.TuketilenKredi, localize.getStr("leakageRepetitionEquals") + " " + _EKS.KartBilgisi_v2.SizintiTekrarSayisi) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("valveOpenEquals") + " " + _EKS.KartBilgisi.VanaAcik, localize.getStr("valveClosingCountEquals") + " " + _EKS.KartBilgisi.VanaAcmaKapamaSayisi) + Environment.NewLine;

                        message += GetPaddedString(localize.getStr("consumerTypeEquals") + " " + _EKS.YazilacakEkBilgi.AboneTipi, "") + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("monthlyConsumption1Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim1, localize.getStr("monthlyConsumption2Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim2) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("monthlyConsumption3Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim3, localize.getStr("monthlyConsumption4Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim4) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("monthlyConsumption5Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim5, localize.getStr("monthlyConsumption6Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim6) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("monthlyConsumption7Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim7, localize.getStr("monthlyConsumption8Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim8) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("monthlyConsumption9Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim9, localize.getStr("monthlyConsumption10Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim10) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("monthlyConsumption11Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim11, localize.getStr("monthlyConsumption12Equals") + " " + _EKS.KartBilgisi_v2.AylikTuketim12) + Environment.NewLine;

                        message += GetPaddedString(localize.getStr("valveClosingDateEquals") + " " + _EKS.KartBilgisi_v2.VanaKapamaTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.VanaKapamaTarihi.ToLongTimeString(), localize.getStr("valveOpeningDateEquals") + " " + _EKS.KartBilgisi_v2.VanaAcmaTarihi.ToShortDateString() + " " + _EKS.KartBilgisi_v2.VanaAcmaTarihi.ToLongTimeString()) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("valveOpeningCountEquals") + " " + _EKS.KartBilgisi_v2.VanaAcmaSayisi, localize.getStr("valveClosingCountEquals") + " " + _EKS.KartBilgisi_v2.VanaKapatmaSayisi) + Environment.NewLine;
                        message += localize.getStr("versionEquals") + " " + _EKS.KartBilgisi_v2.Version + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("highConsumptionEquals") + " " + _EKS.GenelParametreler.AsiriTuketim, localize.getStr("advanceUseEquals") + " " + _EKS.GenelParametreler.AvansKullanimi) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("pulseErrorPeriodEquals") + " " + _EKS.GenelParametreler.GostergeArizaSuresi, localize.getStr("fireModePeriodEquals") + " " + _EKS.GenelParametreler.YanginModuSuresi) + Environment.NewLine;

                        message += GetPaddedString(localize.getStr("holidayStartDateEquals") + " " + _EKS.YazilacakEkBilgi.BayramBaslangici1.ToShortDateString(), localize.getStr("holidayEndDateEquals") + " " + _EKS.YazilacakEkBilgi.BayramBitisi1.ToShortDateString()) + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("waterMeterClosingDateEquals") + " " + _EKS.YazilacakEkBilgi.SayacKapatmaTarihi.ToShortDateString() + " " + _EKS.YazilacakEkBilgi.SayacKapatmaTarihi.ToLongTimeString(), localize.getStr("periodEquals") + " " + _EKS.KartBilgisi.DonemGunSayisi) + Environment.NewLine;

                        message += localize.getStr("interferencesEquals");

                        String modelText = _meterModelsByModelId[_EKS.SayacModeliId];
                        

                        if (_EKS.KartBilgisi_v2.Cezalar != null && _EKS.KartBilgisi.Cezalar.Length > 0)
                        {
                            foreach (Ceza item in _EKS.KartBilgisi_v2.Cezalar)
                            {
                                try
                                {
                                    if (item != null)
                                    {
                                        message += item.Kod.ToString() + Environment.NewLine;
                                    }
                                }
                                catch (Exception ex) { EKS_v2.Logger.Log("Ceza hatasi", ex); }
                            }
                        }
                        else { message += Environment.NewLine; }

                        
                        message += localize.getStr("warningsEquals");
                        if (_EKS.KartBilgisi_v2.Uyarilar != null)
                        {
                            foreach (Uyari item in _EKS.KartBilgisi_v2.Uyarilar)
                            {
                                message += item.Kod.ToString() + Environment.NewLine;
                            }
                        }
                        else { message += Environment.NewLine; }

                        message += GetPaddedString(localize.getStr("cardTypeEquals") + " " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v2.KartTipi }),
                            localize.getStr("cardTypeEquals") + " " + _EKS.KartBilgisi.KartTuru.ToString()) + Environment.NewLine;
                        //message += localize.getStr("baylanCardTypeEquals") + " " + kartIslemleri.KartBilgisi_v2.KartTuru_v2 + Environment.NewLine;
                        message += GetPaddedString(localize.getStr("cardStatusEquals") + " " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v2.KartStatus }), localize.getStr("cardStatusExtraEquals") + " " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v2.KartDurumExtra })) + Environment.NewLine;
                        message += localize.getStr("cardStatus3") + ": " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v3.KartStatus3 }) + Environment.NewLine;
                        message += localize.getStr("maximumFlowEquals") + " " + _EKS.GenelParametreler.MaxDebi + Environment.NewLine;
                        message += GetPaddedString(
                            localize.getStr("debitCreditUpperLevel") + ": " + BitConverter.ToString(new byte[] { _EKS.KartBilgisi_v3.BorcSinirDegeri }),
                            localize.getStr("criticalCreditsEquals") + " " + _EKS.KartBilgisi.KritikKredi)
                            + Environment.NewLine;
                        message += "Sayac Model ID :" + ": " + _EKS.SayacModeliId + "(" + _meterModelsByModelId[_EKS.SayacModeliId] + ")" + Environment.NewLine;
                        message += "Kart Idare No :" + ": " + _EKS.KartBilgisi_v3.KartIdareNo + Environment.NewLine;
                        message += "Sub Auth. No :" + ": " + _EKS.KartBilgisi_v4.IdareNo + Environment.NewLine;
                        message += "MeterInstallationDate" + "/" + "CIUMatchDate" + ":" +
                            _EKS.KartBilgisi_v4.MeterInstallationDate.ToShortDateString() + " " + _EKS.KartBilgisi_v4.MeterInstallationDate.ToLongTimeString() + Environment.NewLine;
                        message += "MinTemprature" + ":" + _EKS.KartBilgisi_v4.MinTemprature + ", " +
                            "MaxTemprature" + ":" + _EKS.KartBilgisi_v4.MaxTemprature + Environment.NewLine;

                        message += localize.getStr("periodOfProcessEquals") + " " + sw.ElapsedMilliseconds + " ms" + Environment.NewLine;


                        CardData cardData = new CardData();

                        int selectedIndex = 0;
                        int selectedValue = 0;

                        if ((byte)(_EKS.KartBilgisi_v3.RFYayinSuresi) != 0)
                        {
                            string zamanTipi = ((_EKS.KartBilgisi_v3.RFYayinSuresi & 0xC0) == 0 && (byte)(_EKS.KartBilgisi_v3.RFYayinSuresi & 0x3F) == 0
                                ? "" : (_EKS.KartBilgisi_v3.RFYayinSuresi & 0xC0) == 0x80 ? "Saat" :
                                (_EKS.KartBilgisi_v3.RFYayinSuresi & 0xC0) == 0x40 ? "Dakika" : "Saniye"
                                );

                            byte zamanData = (byte)(_EKS.KartBilgisi_v3.RFYayinSuresi & 0x3F);
                            selectedValue = zamanData;

                            if (zamanTipi == "Saniye")
                            {
                                selectedIndex = 1;
                            }
                            if (zamanTipi == "Dakika")
                            {
                                selectedIndex = 2;
                            }
                            if (zamanTipi == "Saat")
                            {
                                selectedIndex = 3;
                            }

                            message += " Rf Yayin Data: Ham:" +
                                BitConverter.ToString(new byte[] { (byte)(_EKS.KartBilgisi_v3.RFYayinSuresi) }) + " parsed:" +
                                zamanData +
                                " " + zamanTipi
                                 + Environment.NewLine;
                        }
                        cardData.modelName = modelText;
                        cardData.modelID = _EKS.SayacModeliId;
                        cardData.cbTimeType = selectedIndex;
                        cardData.cbTimeValue = selectedValue;
                        
                        cardData.connectionID= _EKS.KartBilgisi.AboneNo < 0 && _EKS.KartBilgisi_v3.AboneNoLong > 0 ?
                                       _EKS.KartBilgisi_v3.AboneNoLong.ToString() : _EKS.KartBilgisi.AboneNo.ToString();
                        cardData.meterNo =_EKS.KartBilgisi.SayacNo.ToString();

                        cardData.tarife1 = _EKS.KartBilgisi.Fiyat1.ToString();
                        cardData.tarife2 = _EKS.KartBilgisi.Fiyat2.ToString();
                        cardData.tarife3 = _EKS.KartBilgisi.Fiyat3.ToString();
                        cardData.tarife4= _EKS.KartBilgisi.Fiyat4.ToString();
                        cardData.tarife5= _EKS.KartBilgisi.Fiyat5.ToString();
                        cardData.tarife6= _EKS.KartBilgisi_v2.Fiyat6.ToString();
                        cardData.tarife7= _EKS.KartBilgisi_v2.Fiyat7.ToString();

                        cardData.kademe1= _EKS.KartBilgisi.KademeUstSinir1.ToString();
                        cardData.kademe2 = _EKS.KartBilgisi.KademeUstSinir2.ToString();
                        cardData.kademe3 = _EKS.KartBilgisi.KademeUstSinir3.ToString();
                        cardData.kademe4 = _EKS.KartBilgisi.KademeUstSinir4.ToString();
                        cardData.kademe5 = _EKS.KartBilgisi_v2.KademeUstSinir5.ToString();
                        cardData.kademe6 = _EKS.KartBilgisi_v2.KademeUstSinir6.ToString();

                        cardData.aboneTipi= _EKS.YazilacakEkBilgi.AboneTipi;
                        cardData.kritikKredi = _EKS.KartBilgisi.KritikKredi.ToString();
                        cardData.mainCredit= _EKS.KartBilgisi.Kredi.ToString() + "";
                        cardData.reserveCredit= _EKS.KartBilgisi.YedekKredi.ToString() + "";
                        cardData.sayacKredi = _EKS.KartBilgisi.SayactakiKredi.ToString() + "";
                        cardData.refundValue= _EKS.KartBilgisi.IadeEdilecekKredi.ToString() + "";
                        cardData.sayacTuketim= _EKS.KartBilgisi.ToplamTuketilenKredi.ToString() + "";
                        _monitor.setCardData(cardData);
                    }
                    else
                    {
                        message = localize.getStr("failedReadingCard") + Environment.NewLine +
                        _EKS.SonIslemSonuc.Result.ToString() + ":" + _EKS.SonIslemSonuc.ResultDescription + Environment.NewLine + _EKS.SonIslemSonuc.ComResultType.ToString()
                        + Environment.NewLine + (_EKS.SonIslemSonuc.Exception != null ? _EKS.SonIslemSonuc.Exception.Message + " " + _EKS.SonIslemSonuc.Exception.StackTrace : "") +
                        (_EKS.SonIslemSonuc.ExceptionsList != null ? localize.getStr("exceptionCountEquals") + _EKS.SonIslemSonuc.ExceptionsList.Count : "")
                        ;
                    }

                    sw.Reset();
                    _monitor.setProgress(100);
                    
                }
            }
            catch (Exception ex)
            {
                EKS_v2.Logger.Log("Read Isleminde Hata", ex);
                _monitor.ShowMessage(ex.Message + " " + ex.StackTrace);
            }
        }
        public void clearCardData()
        {
            long startTs = Environment.TickCount;
            long startTick = Environment.TickCount;
            if (_EKS.KartSil())
            {
                _monitor.clearSuccess();
            }
            else
            {
                String msg = localize.getStr("cardClearProcessFailed");
                _monitor.clearFail(msg);
                _monitor.AppendLogText(_EKS.SonHataMesaji);
            }
           
        }
        public void topUp(int connectionID,String tag,int meterNo, int reserveCredit,int creditAmount,int criticalCredit)
        {
            prepareWriteData(connectionID,tag,meterNo,0,criticalCredit);
            

            _EKS.KartBilgisi.AboneNo = connectionID;
            _EKS.KartBilgisi.SayacNo = meterNo;
            long startTick = Environment.TickCount;
            
            if (_EKS.KrediYukle((decimal)creditAmount, (decimal)reserveCredit))
            {
                _monitor.topUpSuccess();
            }
            else
            {
                _monitor.topUpFail();
                _monitor.AppendLogText(localize.getStr("failedLoadingCredits") + _EKS.SonHataMesaji);
            }
        }
        public void writeCard(int connectionID,String tag,int meterNo, int reserveCredit,int refundAmount,int creditAmount,int criticalCredit)
        {
            prepareWriteData(connectionID, tag, meterNo, refundAmount, criticalCredit);
            long startTick = Environment.TickCount;
            if (_EKS.KartOlustur())
            {
                _monitor.writeSuccess(Environment.TickCount - startTick);
                _monitor.AppendLogText(localize.getStr("cardCreatedSuccessfully") + (" in " + (Environment.TickCount - startTick) + " ms"));
                _monitor.AppendLogText(localize.getStr("cardCreatedSuccessfully"));

            }
            else
            {
                string error = localize.getStr("cardCreationFailed");
                _monitor.writeFail(error);
                _monitor.AppendLogText(error);
            }
        }

        private void prepareWriteData(int connectionID, string tag, int meterNo, int refundAmount, int criticalCredit)
        {
            if (_EKS.KartBilgisi == null)
            {
                _EKS.KartOku();
            }

            try
            {
                _EKS.KartBilgisi.SayacNo = meterNo;
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid meter no.", ex);
            }
            try
            {
                _EKS.KartBilgisi_v3.AboneNoLong = (long)connectionID;
                _EKS.KartBilgisi_v3.AboneNo = connectionID;

            }
            catch (Exception ex)
            {
                throw new Exception("Invalid connection ID!!!", ex);
            }

            //MessageBox.Show("Sayac No:" + _EKS.KartBilgisi.SayacNo);

            _EKS.YazilacakEkBilgi.AboneTipi = tag;
            _EKS.KartBilgisi.KademeUstSinir1 = 9999;
            _EKS.KartBilgisi.KademeUstSinir2 = 9999;
            _EKS.KartBilgisi.KademeUstSinir3 = 9999;
            _EKS.KartBilgisi.KademeUstSinir4 = 0;
            _EKS.KartBilgisi_v2.KademeUstSinir5 = 0;





            _EKS.KartBilgisi.YedekKredi = 0;
            _EKS.KartBilgisi_v2.KartTuru = EKS.KartTuru.AboneKarti;

            _EKS.KartBilgisi_v2.KartTuru_v2 = EKS.KartTuru_v2.AboneKarti; //Card Type:New Customer Card

            _EKS.KartBilgisi_v2.IadeEdilecekKredi = (float)refundAmount;
            _EKS.SetRfYayinZamanData(zamantipi, (byte)0);
            frmPortSettings.Config.CriticalCredit = (float)criticalCredit;
            _EKS.IsAK11ExtraFunctionality = false;

            //float.TryParse(txtTarife1.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat1 = tmpFloat;        
            _EKS.KartBilgisi.Fiyat1 = 1;


            //float.TryParse(txtTarife2.Text, NumberStyles.Any,
            ///CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat2 = tmpFloat;
            _EKS.KartBilgisi.Fiyat2 = 1;


            //float.TryParse(txtTarife3.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat3 = tmpFloat;
            _EKS.KartBilgisi.Fiyat3 = 1;
            _EKS.KartBilgisi_v2.KademeUstSinir6 = 0;

            //float.TryParse(txtTarife4.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat4 = tmpFloat;
            _EKS.KartBilgisi.Fiyat4 = 1;

            //float.TryParse(txtTarife5.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi.Fiyat5 = tmpFloat;
            _EKS.KartBilgisi.Fiyat5 = 0;

            //float.TryParse(txtTarife6.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi_v2.Fiyat6 = tmpFloat;
            _EKS.KartBilgisi_v2.Fiyat6 = 0;

            //float.TryParse(txtTarife7.Text, NumberStyles.Any,
            //CultureInfo.InvariantCulture, out tmpFloat);
            //kartIslemleri.KartBilgisi_v2.Fiyat7 = tmpFloat;
            _EKS.KartBilgisi_v2.Fiyat7 = 0;


            _EKS.KartBilgisi.KritikKredi = (float)criticalCredit;

            //int tmp_int = 0;
            //Int32.TryParse(cmpMaxDebiEsikDegeri.Text, out tmp_int);
            //kartIslemleri.GenelParametreler.MaxDebi = tmp_int;
            byte saat = (byte)30;
            byte dakika = (byte)60;
            int yanginModuSuresi = (saat * 60) + dakika;

            _EKS.YazilacakEkBilgi.AboneTipi = tag;

            _EKS.GenelParametreler.YanginModuSuresi = yanginModuSuresi;

            _EKS.YazilacakEkBilgi.BayramBaslangici1 = new DateTime(2000, 1, 1);
            _EKS.YazilacakEkBilgi.BayramBitisi1 = new DateTime(2000, 1, 1);
            _EKS.YazilacakEkBilgi.SayacKapatmaTarihi = new DateTime(2020, 1, 1);

            _EKS.KartBilgisi.SayacCapi = 20;
            _EKS.KartBilgisi.DonemGunSayisi = 0;

            //tmp_int = 0;
            //Int32.TryParse(cmpMaxDebiEsikDegeri.Text, out tmp_int);



            _monitor.AppendLogText(localize.getStr("valuesPriorToWritingToCard"));
            string msg = Environment.NewLine + localize.getStr("meterModelIDEquals") + " " + _EKS.SayacModeliId + Environment.NewLine
                        + localize.getStr("consumerNrEquals") + " " + _EKS.KartBilgisi.AboneNo + Environment.NewLine
                        + localize.getStr("waterMeterNrEquals") + " " + _EKS.KartBilgisi.SayacNo + Environment.NewLine
                        + localize.getStr("cardTypeEquals") + " " + _EKS.KartBilgisi_v2.KartTuru_v2.ToString() + Environment.NewLine
                        + localize.getStr("creditEquals") + " " + _EKS.KartBilgisi.Kredi + Environment.NewLine
                        + localize.getStr("backupCreditsEquals") + " " + _EKS.KartBilgisi.YedekKredi;

            _monitor.AppendLogText(msg);
            _monitor.AppendLogText(localize.getStr("cardWriteInitialized"));
        }
    }
    
}
