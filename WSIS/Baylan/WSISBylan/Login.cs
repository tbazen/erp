
using INTAPS.Accounting.Client;
using INTAPS.ClientServer.Client;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client.BaylanInterface
{

    public partial class Login : System.Windows.Forms.Form
    {
        private Container components = null;
        public bool logedin;
        private bool m_boolValidUser = false;
        public Login()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["Server"];
                ApplicationClient.Connect(url, this.txtUserName.Text, this.txtPassword.Text);
                SecurityClient.Connect(url);
                SubscriberManagmentClient.Connect(url);
                WSIS.Job.Client.JobManagerClient.Connect(url);
                AccountingClient.Connect(url);
                Payroll.Client.PayrollClient.Connect(url);
                BIZNET.iERP.Client.iERPTransactionClient.Connect(url);
                this.logedin = true;
                base.Close();
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, exception.Message, "Could not login");
                this.txtUserName.Focus();
                this.txtUserName.Text = "";
                this.txtPassword.Text = "";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        public void TryLogin()
        {
            this.logedin = false;
            base.ShowDialog();
        }

        public bool ValidUser
        {
            get
            {
                return this.m_boolValidUser;
            }
        }
    }
}

