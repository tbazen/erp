﻿namespace INTAPS.SubscriberManagment.Client.BaylanInterface
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.textContractNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.browserCust = new INTAPS.UI.HTML.ControlBrowser();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonWriteCustomer = new System.Windows.Forms.Button();
            this.buttonReadCard = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.browserCard = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.buttonLoad);
            this.panel1.Controls.Add(this.textContractNo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 107);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(690, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "WSIS Customer Data";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(408, 39);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(122, 30);
            this.buttonLoad.TabIndex = 2;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // textContractNo
            // 
            this.textContractNo.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContractNo.Location = new System.Drawing.Point(126, 40);
            this.textContractNo.Name = "textContractNo";
            this.textContractNo.Size = new System.Drawing.Size(248, 30);
            this.textContractNo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Contract No:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.browserCust);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.browserCard);
            this.splitContainer1.Size = new System.Drawing.Size(1423, 707);
            this.splitContainer1.SplitterDistance = 690;
            this.splitContainer1.TabIndex = 1;
            // 
            // browserCust
            // 
            this.browserCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserCust.Location = new System.Drawing.Point(0, 107);
            this.browserCust.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserCust.Name = "browserCust";
            this.browserCust.Size = new System.Drawing.Size(690, 600);
            this.browserCust.StyleSheetFile = "jobstyle.css";
            this.browserCust.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.buttonWriteCustomer);
            this.flowLayoutPanel1.Controls.Add(this.buttonReadCard);
            this.flowLayoutPanel1.Controls.Add(this.buttonClear);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 23);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(729, 84);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // buttonWriteCustomer
            // 
            this.buttonWriteCustomer.Location = new System.Drawing.Point(8, 8);
            this.buttonWriteCustomer.Name = "buttonWriteCustomer";
            this.buttonWriteCustomer.Size = new System.Drawing.Size(203, 54);
            this.buttonWriteCustomer.TabIndex = 2;
            this.buttonWriteCustomer.Text = "Write Customer Data";
            this.buttonWriteCustomer.UseVisualStyleBackColor = true;
            this.buttonWriteCustomer.Click += new System.EventHandler(this.buttonWriteCustomer_Click);
            // 
            // buttonReadCard
            // 
            this.buttonReadCard.Location = new System.Drawing.Point(217, 8);
            this.buttonReadCard.Name = "buttonReadCard";
            this.buttonReadCard.Size = new System.Drawing.Size(203, 54);
            this.buttonReadCard.TabIndex = 2;
            this.buttonReadCard.Text = "Read Card";
            this.buttonReadCard.UseVisualStyleBackColor = true;
            this.buttonReadCard.Click += new System.EventHandler(this.buttonReadCard_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(426, 8);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(203, 54);
            this.buttonClear.TabIndex = 2;
            this.buttonClear.Text = "Clear Card";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(729, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "WSIS Customer Data";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // browserCard
            // 
            this.browserCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserCard.Location = new System.Drawing.Point(0, 0);
            this.browserCard.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserCard.Name = "browserCard";
            this.browserCard.Size = new System.Drawing.Size(729, 707);
            this.browserCard.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1423, 707);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Baylan Prepaid Card Manager";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.TextBox textContractNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private UI.HTML.ControlBrowser browserCust;
        private System.Windows.Forms.WebBrowser browserCard;
        private System.Windows.Forms.Button buttonWriteCustomer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonReadCard;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label3;
    }
}