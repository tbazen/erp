﻿using INTAPS.WSIS.Job.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client.BaylanInterface
{
    public partial class MainForm : Form,CardReader.IProgressMonitor
    {
        Subscription connectionData = null;
        CardReader reader = null;
        Dictionary<String, CardReader.CardMeterType> meterTypeMap = new Dictionary<string, CardReader.CardMeterType>();
        void loadMeterTypeMap()
        {
            foreach(String m in System.Configuration.ConfigurationManager.AppSettings["meter_type_map"].Split(new char[] { ';' },StringSplitOptions.RemoveEmptyEntries))
            {
                String[] parts = m.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                meterTypeMap.Add(parts[1], (CardReader.CardMeterType)Enum.Parse(typeof(CardReader.CardMeterType), parts[0]));
            }
        }
        public MainForm()
        {
            InitializeComponent();
            loadMeterTypeMap();
            reader = new CardReader(this);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
        
        private void buttonLoad_Click(object sender, EventArgs e)
        {
            try
            {
                Subscription subsc = SubscriberManagmentClient.GetSubscription(textContractNo.Text, DateTime.Now.Ticks);
                if (subsc == null)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid contract no");
                    textContractNo.Text = "";
                    return;
                }

                setConnectionData(subsc);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Unexpected error", ex);
            }
        }

        private void setConnectionData(Subscription subsc)
        {
            String html = WSIS.Job.Client.CustomerProfileBuilder.buildHTML( subsc.subscriberID, CustomerProfileBuilder.ConnectionProfileOptions.All & ~CustomerProfileBuilder.ConnectionProfileOptions.ShowCustomerRef);
            browserCust.LoadTextPage("customer", html);
            this.connectionData = subsc;
        }

        private void buttonReadCard_Click(object sender, EventArgs e)
        {
            if(reader.isBusy())
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Card reader busy, please wait");
                return;
            }
            reader.startRead();
        }

        public void setProgress(int val)
        {
            
        }

        public void ShowMessage(string v)
        {
            Console.WriteLine(v);
            //INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(v);
        }

        public void AppendLogText(string v)
        {
            ShowMessage(v);
        }

        public void SetStatusText(string v)
        {
            ShowMessage(v);
        }

        public void setCardData(CardReader.CardData cardData)
        {
            int conID = 0;
            int.TryParse(cardData.connectionID, out conID);
            Subscription subsc = SubscriberManagmentClient.GetSubscription(conID, DateTime.Now.Ticks);
            browserCard.DocumentText = $@"
<strong>Connection ID:</strong>{cardData.connectionID+(subsc==null?"[Invalid]":"")}<br/>
<strong>Meter Type:<strong>{cardData.modelName}<br/>
<strong>Meter No:<string>{cardData.meterNo}<br/>
<strong>Main Credit:<string>{cardData.mainCredit}<br/>
<strong>Reseve Credit:<string>{cardData.reserveCredit}<br/>
";
            setConnectionData(subsc);
        }

        public void readFailed(string msg)
        {
            this.AppendLogText("Reading card failed.Err: " + msg);
            browserCard.DocumentText = $@"
<strong>Card couldn't be read</strong>
";
        }

        private void buttonWriteCustomer_Click(object sender, EventArgs e)
        {
            if(connectionData==null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please load a connection data first");
            }
            if(this.reader.isBusy())
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Card reader busy, please wait");
                return;
            }
            if(!meterTypeMap.ContainsKey( this.connectionData.itemCode))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("The meter is not bylan meter");
                return;
            }
            CardReader.CardMeterType type = meterTypeMap[this.connectionData.itemCode];
            if (!this.connectionData.prePaid)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("The meter is not prepaid meter");
                return;
            }
            int meterNo;
            if(!int.TryParse(this.connectionData.serialNo,out meterNo))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Bylan water meter meter no can only be composed of digits");
                return;
            }
            try
            {
                this.reader.MeterType = type;
                this.reader.writeCard(this.connectionData.id, this.connectionData.subscriber.subscriberType.ToString(),
                    meterNo, 0, 0, 0, 3
                    );
            }
            catch(Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to write data", ex);
            }
        }

        public void writeSuccess(long v)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Write succeded");
        }

        public void writeFail(string msg)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Write failed");
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            if(!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to clear the data in this card?"))
            {
                return;
            }
            if (this.reader.isBusy())
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Card reader busy, please wait");
                return;
            }
            try
            {
                this.reader.clearCardData();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to clear card data", ex);
            }
        }

        public void clearSuccess()
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Clear succedded");
            this.browserCard.DocumentText = "";
        }

        public void clearFail(string msg)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Card clear failed");
        }

        public void topUpSuccess()
        {
            throw new NotImplementedException();
        }

        public void topUpFail()
        {
            throw new NotImplementedException();
        }
    }
}
