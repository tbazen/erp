
using INTAPS.UI;
using System;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client.BaylanInterface
{

    internal static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login login = new Login();
            login.TryLogin();
            if (login.logedin)
            {
                if (!INTAPS.ClientServer.Client.SecurityClient.IsPermited("root/Subscriber/Baylan"))
                {
                    System.Windows.Forms.MessageBox.Show("You are not authorized to login to Baylan Prepaid Card Manager");
                    return;
                }
                MainForm main = new MainForm();
                new UIFormApplicationBase(main);
                INTAPS.ClientServer.Client.ApplicationClient.startInstantMessageClient();
                Application.Run(main);
                INTAPS.ClientServer.Client.ApplicationClient.CloseSession();
            }
        }
    }
}

