﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using RazorEngine.Templating;

namespace BelCashTester
{
    public class LoginController : ApiController
    {
        public HttpResponseMessage Get()
        {

            var model = new
            {
                error="",
            };

            string template = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + "\\login.cshtml");
            string templateKey = template.GetHashCode().ToString();

            string html = RazorEngine.Engine.Razor.RunCompile(template, templateKey, null, model);
            
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
            response.Content = new StringContent(html, System.Text.Encoding.UTF8);

            response.Content.Headers.ContentType= new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");

            return response;
        } 
    }
}