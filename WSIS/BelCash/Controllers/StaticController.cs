using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BelCashTester
{
    public class StaticController : ApiController
    {
        static string getMIMEFromExtenstion(string path)
        {
            System.IO.FileInfo fi = new FileInfo(path);
            string mime = null;
            switch (fi.Extension)
            {
                case ".css":
                    mime = "text/css";
                    break;
                case ".jpg":
                    mime = "image/jpeg";
                    break;
                case ".png":
                    mime = "image/png";
                    break;
                case ".htm":
                case ".html":
                    mime = "text/html";
                    break;
                case ".js":
                    mime = "application/javascript";
                    break;
                case ".map":
                    mime = "application/json";
                    break;
                case ".ts":
                    mime = "text/x.typescript";
                    break;
                case ".ico":
                    mime = "image/x-icon";
                    break;
                case ".eot":
                    mime = "application/vnd.ms-fontobject";
                    break;
                case ".woff":
                    mime = "application/font-woff";
                    break;
                case ".ttf":
                    mime = "application/x-font-truetype";
                    break;
                case ".svg":
                    mime = "image/svg+xml";
                    break;
                case ".otf":
                    mime = "application/x-font-opentype";
                    break;
            }
            return mime;
        }
        public HttpResponseMessage Get()
        {
            string path = System.Configuration.ConfigurationManager.AppSettings["self_host_html_root"] + base.ActionContext.Request.RequestUri.AbsolutePath.Substring("api/static/".Length).Replace('/', '\\');
            FileStream content= System.IO.File.OpenRead(path);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, "value");
            response.Content = new StreamContent(content);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(getMIMEFromExtenstion(path));
            return response;

        }
    }
}