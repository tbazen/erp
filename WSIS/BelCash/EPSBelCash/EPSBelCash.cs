﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;
using INTAPS;
namespace EPSBelCash
{
    class EPSBelCashInteface:IEPSInterface
    {
        class BelCashBill
        {
            public int id;
            public string state;
            public string mobilePhone;
            public string customerName;
            public string productDescription;
            public string extraBillInfo;
            public double amount;
            public string batchnr;
            public int billseqnr;
            public int businessId;
            public string bank;
            public string hellocashId;
            public string businessName;
        }
        string sessionID = null;
        System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
        INTAPS.RDBMS.SQLHelper sqlConnection = null;
        PaymentCenter pc;
        public EPSBelCashInteface(PaymentCenter pc)
        {
            this.pc = pc;
        }
        void verifyConnection()
        {
            if (sqlConnection != null)
                return;
            try
            {
                sqlConnection = new INTAPS.RDBMS.SQLHelper(System.Configuration.ConfigurationManager.AppSettings["hellobillConnection"]);
                sqlConnection.getOpenConnection();
            }
            catch
            {
                sqlConnection = null;
                throw;
            }
        }

        class LoginReturn
        {
            public string id;
            public string userId;
        }
        class HeloCashError
        {
            public HellCashErrorData error;
            public class HellCashErrorData
            {
                public string name;
                public string status;
                public string message;
                public string statusCode;
                public string code;
                public string stack;
            }
        }
        string callAPI(string url, object pars,bool put=false)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(pars);
            ApplicationServer.EventLoger.Log(EventLogType.ServerActions,"API Call\nurl: {0}\npars:\n{1}".format(url,json));
            System.Threading.Tasks.Task<HttpResponseMessage> resTask;
            if (put)
                resTask = client.PutAsync(url, new System.Net.Http.StringContent(json, System.Text.Encoding.UTF8, "application/json"));
            else
                resTask = client.PostAsync(url, new System.Net.Http.StringContent(json, System.Text.Encoding.UTF8, "application/json"));
            resTask.Wait();
            System.Threading.Tasks.Task<String> str = resTask.Result.Content.ReadAsStringAsync();
            str.Wait();
            Console.WriteLine("Res: " + str.Result);
            HeloCashError err = Newtonsoft.Json.JsonConvert.DeserializeObject<HeloCashError>(str.Result);
            if (err.error != null)
                throw new Exception(err.error.message);
            return str.Result;
        }
        LoginReturn login()
        {
            INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Connecting to helocash");
            
            LoginReturn ret = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginReturn>(callAPI("https://hellobillapi.belcash.com/api/Businesses/login",
                new
            {
                username = System.Configuration.ConfigurationManager.AppSettings["hellobillUserName"],
                password = System.Configuration.ConfigurationManager.AppSettings["hellobillPassword"]
            }));
            if (ret == null)
                throw new ServerUserMessage("Login failed");
            Console.WriteLine("session ID: " + ret.id);
            return ret;
        }
        public void addBill(EPSBill bill)
        {
            INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Add Bill: " + bill.billID);
            try
            {
                verifyConnection();
                LoginReturn sid = login();
                if (string.IsNullOrEmpty(bill.customerPhoneNo))
                {
                    sqlConnection.Insert(sqlConnection.getOpenConnection().Database, "EPSBillRef", new string[] { "paymentCenterID", "billID", "epsRef" },
                        new object[] { -1, bill.billID, null });
                    INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Bill ID:{0} ignored".format(bill.billID));

                }
                else
                {
                    string id = callAPI("https://hellobillapi.belcash.com/api/Bills?access_token="+sid.id,
                       new
                       {
                           customerName = bill.customerName,
                           mobilePhone = bill.customerPhoneNo,
                           productDescription = bill.remark+"("+bill.billID+")",
                           amount = bill.amount,
                           businessId = sid.userId,
                           state = "unpaid",
                           batchnr = "0",
                           billseqnr = "0",
                           extraBillInfo = bill.customerID + "|" + bill.billID + "|" + bill.billDate,
                       });

                    sqlConnection.Insert(sqlConnection.getOpenConnection().Database, "EPSBillRef", new string[] { "paymentCenterID", "billID", "epsRef" },
                        new object[] { -1, bill.billID, id });
                    INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Bill ID:{0} bill ref:{1}".format(bill.billID, id));
                }
            }
            catch(Exception ex)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("EPS: add bill failed " + bill.billID, ex);
                throw;
            }

        }

        public void deleteBill(int billID)
        {
            INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Delete bill: " + billID);
            try
            {
                verifyConnection();
                LoginReturn sid = login();
                string billRef = sqlConnection.ExecuteScalar("Select epsRef from EPSBillRef where billID=" + billID) as string;
                if (string.IsNullOrEmpty(billRef))
                {
                    INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.Errors, "Synching delete bill ID:{0} ignored because no coresponding bill ref found.".format(billID));
                    return;
                }
                BelCashBill bb = Newtonsoft.Json.JsonConvert.DeserializeObject<BelCashBill>(billRef);
                
                callAPI("https://hellobillapi.belcash.com/api/Bills/{0}?access_token={1}".format(bb.id, sid.id),
                    new
                    {
                        state = "cancelled",
                    },true);

            }
            catch (Exception ex)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("EPS: deleteBill failed " + billID, ex);
            }
        }

        public EPSBill getBillDetail(int billID)
        {
            return null;
        }
        public int[] getBillIDs()
        {
            return null;
        }
        public void replaceBill(EPSBill bill)
        {
            INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Replace bill: " + (bill==null?"null":bill.billID.ToString()));
            try
            {
                verifyConnection();
                LoginReturn sid = login();
                //string billRef = getBillRef(bill);
                
                string billRef = sqlConnection.ExecuteScalar("Select epsRef from EPSBillRef where billID=" + (bill == null ? "null" : bill.billID.ToString())) as string;
                if (string.IsNullOrEmpty(billRef))
                {
                    INTAPS.ClientServer.ApplicationServer.EventLoger.Log(EventLogType.Errors, "Synching delete bill ID:{0} ignored because no coresponding bill ref found.".format(bill==null?"null":bill.billID.ToString()));
                    return;
                }
                BelCashBill bb = Newtonsoft.Json.JsonConvert.DeserializeObject<BelCashBill>(billRef);
                
                string state;
                if(bill.paymentCenterID!=-1)
                {
                    if(bill.paymentCenterID==pc.id)
                        state="paid";
                    else
                        state="paid otherwise";
                }
                else
                    state=bill.status;
                callAPI("https://hellobillapi.belcash.com/api/Bills/{0}?access_token={1}".format(bb.id, sid.id),
                    new
                    {
                        state = state,
                    }, true);
            }
            catch (Exception ex)
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.LogException("EPS: replace failed billID: " + (bill == null ? "null" : bill.billID.ToString()), ex);
                throw;
            }
        }
        class BELCashBill
        {
            public int id;
            public string state;
            public string mobilePhone;
            public string customerName;
            public string productDescription;
            public string extraBillInfo;
            public double amount;
            public string batchnr;
            public int billseqnr;
            public int businessId;
            public string bank;
            public string hellocashId;
            public string businessName;
        }
        private string getBillRef(EPSBill bill)
        {
            string billRef = sqlConnection.ExecuteScalar("Select epsRef from EPSBillRef where billID=" + (bill == null ? "null" : bill.billID.ToString())) as string;
            if (billRef == null)
                return null;
            BELCashBill b= Newtonsoft.Json.JsonConvert.DeserializeObject<BELCashBill>(billRef);
            return b.id.ToString();
        }
    }
}
