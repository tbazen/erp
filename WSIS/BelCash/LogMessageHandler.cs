using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace BelCashTester
{
    public class LogMessageHandler : DelegatingHandler { 
        protected override async Task<HttpResponseMessage> SendAsync ( HttpRequestMessage request, CancellationToken cancellationToken ) { 
            Console.WriteLine("Request {0}",request.ToString());
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken); 
            if (!response.IsSuccessStatusCode) { 
                Console.WriteLine("Error: " + response.StatusCode);
            } 
            return response; 
        } 
    }
}