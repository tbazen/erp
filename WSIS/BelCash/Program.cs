﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Xml.Serialization;
using INTAPS.SubscriberManagment;

namespace BelCashTester
{
    public class APIReturn
    {
        public string error = null;
        public object res = null;
    }
    public class EPSPayment
        {
            public string paymentID;
            public string paymentDate;
            public int customerID;
            public int[] billIDs;
            public int bankAccountID=-1;
            public string bankReference;
            public double amount;
        }
    class Program
    {
        private static HttpSelfHostServer _server;
        static void Main(string[] args)
        {

            initializeSelfHostWebServer();

        }
        static System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
        static string login()
        {
            
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(new { userName = "belcash", passWord = "helocash" });
            System.Threading.Tasks.Task<HttpResponseMessage> resTask = client.PostAsync("http://localhost:8020/subsc/eps/api/login", new System.Net.Http.StringContent(json,System.Text.Encoding.UTF8,"application/json"));
            resTask.Wait();
            System.Threading.Tasks.Task<String> str = resTask.Result.Content.ReadAsStringAsync();
            str.Wait();
            Console.WriteLine("Res: " + str.Result);
            APIReturn ret = Newtonsoft.Json.JsonConvert.DeserializeObject<APIReturn>(str.Result);
            return ret.res.ToString();
        }
        static int recordPayment(string sessionID,EPSPayment payment)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(payment);
            System.Threading.Tasks.Task<HttpResponseMessage> resTask = client.PostAsync("http://localhost:8020/subsc/eps/api/RecordPayment?SessionID=" + sessionID, new System.Net.Http.StringContent(json, System.Text.Encoding.UTF8, "application/json"));
            resTask.Wait();
            System.Threading.Tasks.Task<String> str = resTask.Result.Content.ReadAsStringAsync();
            str.Wait();
            APIReturn ret = Newtonsoft.Json.JsonConvert.DeserializeObject<APIReturn>(str.Result);
            if(ret.error!=null)
                throw new Exception(ret.error.ToString());
            return int.Parse(ret.res.ToString());
        }
        public static void initializeSelfHostWebServer()
        {
            string baseAddress = System.Configuration.ConfigurationManager.AppSettings["wsis_self_host_server"];
            if (string.IsNullOrEmpty(baseAddress))
                return;
            var config = new HttpSelfHostConfiguration(baseAddress);
            config.Routes.MapHttpRoute(
                "API Default", "api/{controller}/{id}/{p1}/{p2}/{p3}/{p4}",
                new
                {
                    id = RouteParameter.Optional,
                    p1 = RouteParameter.Optional,
                    p2 = RouteParameter.Optional,
                    p3 = RouteParameter.Optional,
                    p4 = RouteParameter.Optional,

                });

            //Middleware
            config.MessageHandlers.Add(new LogMessageHandler());
            _server = new HttpSelfHostServer(config);
            try
            {
                _server.OpenAsync().Wait();

            }
            catch (Exception exception)
            {
                throw exception.InnerException;
            }
            Console.Write("Press enter to login.");
            Console.ReadLine();
            string sessionID=login();
            Console.WriteLine("SessionID: " + sessionID);
            Random r=new Random();
            XmlSerializer ser = new XmlSerializer(typeof(EPSBill));
            do
            {
                Console.Write("Press enter to pay: ");
                string code = Console.ReadLine();
                /*List<int> list = new List<int>();
                list.Add(int.Parse(code));
                
                int customerID=-1;
                double amount=0;
                foreach (string f in System.IO.Directory.GetFiles(@"C:\Source\Code\WSIS\ServerBin\db", "*.bill"))
                {
                    using (FileStream fs = System.IO.File.OpenRead(f))
                    {
                        EPSBill bill = ser.Deserialize(fs) as EPSBill;
                        if (bill.customerCode.Equals(code))
                        {
                            list.Add(bill.billID);
                            customerID = bill.customerID;
                            amount+=bill.amount;
                        }
                    }
                }*/
                try
                {

                    int rid = recordPayment(sessionID, new EPSPayment()
                        {
                            paymentID = Guid.NewGuid().ToString(),
                            amount = 10.00,
                            bankAccountID = 470371,
                            bankReference = r.Next(10000000).ToString(),
                            billIDs = new int[] { 19149 },
                            customerID = 21501,
                            paymentDate = DateTime.Now.ToString(),
                        }
                        );
                    Console.WriteLine("rid: " + rid);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            } while (true);
        }


    }
}
