﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using INTAPS.WSIS.CBEBirr.Service;
using Microsoft.AspNetCore.Mvc;

namespace CBEBirr.Controllers
{
    [Route("/[controller]/[action]")]
    [ApiController]
    public class ReportController : Controller
    {
        [HttpGet]
        public IActionResult BillList()
        {
            try
            {
                String body;
                String headers;
                var sid = CBEWSIS.Connect();
                var pid = CBEWSIS.GetCurrentBillingPeriod(sid);
                body= CBEWSIS.EvaluateReport(sid,"SM.BillsList", new object[] { -1,-1,pid,pid}, out headers);
                if (headers == null)
                    headers = "";
                var html = $"<htm><head>{headers}</head><body>{body}</body>";
                return Content(html,"text/html");
            }
            catch(Exception ex)
            {
                return base.Json(new { message = "Failed to load report", detail = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult PaymentList()
        {
            try
            {
                String body;
                String headers;
                var sid = CBEWSIS.Connect();
                int centerID = 8;
                body = CBEWSIS.EvaluateReport(sid, "PaymentCenterReport", new object[] { new DateTime(2019,4,1), DateTime.Now, centerID, -1 }, out headers);
                if (headers == null)
                    headers = "";
                var html = $"<htm><head>{headers}</head><body>{body}</body>";
                return Content(html, "text/html");
            }
            catch (Exception ex)
            {
                return base.Json(new { message = "Failed to load report", detail = ex.Message });
            }
        }
    }
}
