﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INTAPS.WSIS.CBEBirr
{
    public class HostConfiguration
    {
        public String WSISServer { get; set; }
        public String CBEID { get; set; }
        public String LogDb { get; set; }
        public String WsisUser { get; set; }
        public String WsisPassword { get; set; }
    }
}
