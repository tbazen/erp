﻿using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace INTAPS.ClientServer.Client
{
    public class ServerException
    {
        public String type;
        public byte[] exception;
    }
    public class VoidRet
    {

    }
    public class RESTAPIClient
    {
        static RESTAPIClient()
        {
        }
        public static T Call<T>(String path, object obj, params JsonConverter[] converters)
        {
            HttpClient client = new HttpClient();
            client.Timeout = System.Threading.Timeout.InfiniteTimeSpan;
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                var res = client.PostAsJsonAsync(path, obj).Result;
                if (res.IsSuccessStatusCode)
                {
                    if (typeof(T) != typeof(VoidRet))
                    {
                        if (converters.Length > 0)
                        {
                            JsonMediaTypeFormatter f = new JsonMediaTypeFormatter();
                            f.SerializerSettings.Converters = converters;

                            return res.Content.ReadAsAsync<T>(new MediaTypeFormatter[] { f }).Result;

                        }
                        else
                            return res.Content.ReadAsAsync<T>().Result;
                    }
                    else
                        return default(T);
                }
                else
                {
                    if (res.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        ServerException sex = null;
                        try
                        {
                            sex = res.Content.ReadAsAsync<ServerException>().Result;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"Error trying to retreive detailed server error information for API {path}.\n{ex.Message}.", ex);
                        }
                        if (sex == null || String.IsNullOrEmpty(sex.type))
                            throw new Exception("Unknown internal server exception calling API " + path);
                        if (sex.exception == null || sex.exception.Length == 0)
                            throw new Exception($"Server exception of type {sex.type} calling API {path}");
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        using (var ms = new System.IO.MemoryStream(sex.exception))
                        {
                            var ex = bf.Deserialize(ms);
                            if (ex is Exception)
                                throw (Exception)ex;
                        }

                        throw new Exception($"Server exception of type {sex.type} calling API {path}");

                    }
                    else
                    {
                        throw new Exception($"Server exception calling API {path} status code:{res.StatusCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public delegate void RESTReturnWithType<T>(T ret,Exception ex);
        public static void CallAsync<T>(String path, object obj, RESTReturnWithType<T> callback, params JsonConverter[] converters)
        {
            HttpClient client = new HttpClient();
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
                var a = client.PostAsJsonAsync(path, obj).GetAwaiter();

                a.OnCompleted(() =>{
                        var res = a.GetResult();
                        if (res.IsSuccessStatusCode)
                        {
                            if (typeof(T) != typeof(VoidRet))
                            {
                                if (converters.Length > 0)
                                {
                                    JsonMediaTypeFormatter f = new JsonMediaTypeFormatter();
                                    f.SerializerSettings.Converters = converters;

                                    callback(res.Content.ReadAsAsync<T>(new MediaTypeFormatter[] { f }).Result,null);

                                }
                                else
                                    callback(res.Content.ReadAsAsync<T>().Result,null);
                            }
                            else
                                callback(default(T),null);
                        }
                        else
                        {
                            if (res.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                            {
                                ServerException sex = null;
                                try
                                {
                                    sex = res.Content.ReadAsAsync<ServerException>().Result;
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception($"Error trying to retreive detailed server error information for API {path}.\n{ex.Message}.", ex);
                                }
                                if (sex == null || String.IsNullOrEmpty(sex.type))
                                    throw new Exception("Unknown internal server exception calling API " + path);
                                if (sex.exception == null || sex.exception.Length == 0)
                                    throw new Exception($"Server exception of type {sex.type} calling API {path}");
                                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                                using (var ms = new System.IO.MemoryStream(sex.exception))
                                {
                                    var ex = bf.Deserialize(ms);
                                    if (ex is Exception)
                                        throw (Exception)ex;
                                }

                                callback(default(T), new Exception($"Server exception of type {sex.type} calling API {path}"));

                            }
                            else
                            {
                            callback(default(T),new Exception($"Server exception calling API {path} status code:{res.StatusCode}"));
                            }
                        }
                    });

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
