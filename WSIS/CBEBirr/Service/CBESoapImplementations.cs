﻿using INTAPS.ClientServer;
using INTAPS.ClientServer.Client;
using INTAPS.SubscriberManagment;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace INTAPS.WSIS.CBEBirr.Service
{
    public class CBEWSIS : ICBEWSIS
    {

        static Queue<C2BPaymentValidationRequest> paymentQue = new Queue<C2BPaymentValidationRequest>();
        static bool paymentQueOK = true;
        static void pushPaymentRequest(C2BPaymentValidationRequest request)
        {
            lock (paymentQue)
            {
                if (!paymentQueOK)
                    throw new Exception("Payment system is temporarly unavialale, sorry for the inconvicence");
                paymentQue.Enqueue(request);
            }
        }
        static void initPaymentQuerProcessor()
        {
            new System.Threading.Thread(new System.Threading.ThreadStart(paymentRequestProcessor)).Start();
        }
        static void paymentRequestProcessor()
        {
            while(true)
            {
                bool empty = false;
                lock(paymentQue)
                {
                    empty = paymentQue.Count == 0;
                }
                if (empty)
                {
                    System.Threading.Thread.Sleep(EMPTY_QUE_WAIT);
                    continue;
                }
                if (!paymentQueOK)
                {
                    Console.WriteLine($"Payment que is in error state sleeping for {ERROR_QUE_WAIT} miliseconds");
                    System.Threading.Thread.Sleep(ERROR_QUE_WAIT);
                    continue;
                }
                C2BPaymentValidationRequest request = null;
                lock(paymentQue)
                {
                    request = paymentQue.Dequeue();
                }
                Console.WriteLine($"Processing payment from queue. Queue size after this: {paymentQue.Count}");
                Subscriber customer=null;
                String sessionID=null;
                try
                {
                    double requestAmount;

                    if (!double.TryParse(request.TransAmount, out requestAmount))
                        throw new AccessViolationException($"CBEBirr sent invalid transaction amount {request.TransAmount}");
                    ConnectAndGetCustomer(request.BillRefNumber, out sessionID, out customer);

                    var payment = RESTAPIClient.Call<int>($"{Startup.WSIS_Server}/api/erp/subscribermanagment/PayAvialableBills",
                    new { sessionID = sessionID, customerID = customer.id, paymentAmount = requestAmount, paymentDescription = "Payment from CBEBirr" });
                    Console.WriteLine("Processing payment success");
                    continue;
                }
                catch (Exception ex) //payment not accepted by WSIS, attempt deposit
                {
                    Console.WriteLine($"Processing payment failed.\n{ex.Message}.\nAttempting processing as unknown payment.");
                    try
                    {
                        if (sessionID == null)
                            throw new Exception("Connection WSIS server not established");

                        if (customer == null)
                            throw new Exception("Customer information not retrieved");

                        var payment = RESTAPIClient.Call<int>($"{Startup.WSIS_Server}/api/erp/subscribermanagment/RecordUnknownPayment",
                        new { sessionID = sessionID, customerID = customer.id, amount = double.Parse(request.TransAmount), description = "CBEBirr payment that is not reconcilable.\n"+ex.Message});
                        Console.WriteLine("Processing payment as deposit success");
                        continue;
                    }
                    catch (Exception ex2)
                    {
                        lock (paymentQue)
                        {
                            paymentQueOK = false;
                            Console.WriteLine($"Processing payment as deposit failed.\n{ex2.Message}.\nSystem is now placed in failed status. Dumping que");
                            var d = DateTime.Now;
                            var timeStamp = $"{d.Year}-{d.Month}-{d.Day}-{d.Ticks}";
                            var error = ex2.Message + "\n" + ex2.StackTrace;
                            while(ex2.InnerException!=null)
                            {
                                ex2 = ex.InnerException;
                                error+="\n>"+ ex2.Message + "\n" + ex2.StackTrace;
                            }
                            System.IO.File.WriteAllText(Path.Combine("dump", $"{timeStamp}-error.txt"), error);

                            var i = 0;
                            System.IO.File.WriteAllText(Path.Combine("dump", $"{timeStamp}-{i++}.json"), Newtonsoft.Json.JsonConvert.SerializeObject(request));
                            foreach (var r in paymentQue)
                            {
                                System.IO.File.WriteAllText(Path.Combine("dump", $"{timeStamp}-{i++}.json"), Newtonsoft.Json.JsonConvert.SerializeObject(r));
                            }
                        }
                    }
                }
            }
        }
        public static string Connect()
        {
            return RESTAPIClient.Call<String>($"{Startup.WSIS_Server}/api/app/server/CreateUserSession", new { UserName=Startup.WsisUser, Password=Startup.WsisPassword, Source = "CBEWSIS" });
        }
        public class GetSubscriptions2Out
        {
            public int NRecord;
            public SubscriberSearchResult[] _ret;
        }

        public class EvaluateEHTMLOut
        {
            public string headerItems;
            public String _ret;
        }
        public static String EvaluateReport(String sessionID, String reportCode, object[] pars, out String headers)
        {
            var html = RESTAPIClient.Call<EvaluateEHTMLOut>($"{Startup.WSIS_Server}/api/erp/accounting/EvaluateEHTML",
                new { sessionID = sessionID, code = reportCode, parameter = new BinObject(pars) });
            headers = html.headerItems;
            return html._ret;
        }
        public static object[] GetSystemParameters(String sessionID,string path, string[] names)
        {
            return RESTAPIClient.Call<TypeObject[]>(path + "GetSystemParameters", new { sessionID = sessionID, names = names }).Select(x => x.GetConverted()).ToArray();

        }
        public static int GetCurrentBillingPeriod(String sessionID)
        {
            var pars = GetSystemParameters(sessionID, $"{Startup.WSIS_Server}/api/erp/subscribermanagment/", new string[] { "currentPeriod" });
            return (int)pars[0];
        }
        double TotalBills(string sessionID, int customerID)
        {
            double ret = 0.0;
            var bills = RESTAPIClient.Call<INTAPS.SubscriberManagment.CustomerBillRecord[]>($"{Startup.WSIS_Server}/api/erp/subscribermanagment/getBills",
                new { sessionID = sessionID, mainTypeID = -1, customerID = customerID, connectionID = -1, periodID = -1 });
            foreach (var bill in bills)
            {
                if (bill.paymentDiffered || bill.paymentDocumentID > 0)
                    continue;
                var items = RESTAPIClient.Call<INTAPS.SubscriberManagment.BillItem[]>($"{Startup.WSIS_Server}/api/erp/subscribermanagment/getCustomerBillItems",
                    new { sessionID = sessionID, billRecordID = bill.id });
                if (items != null && items.Length > 0)
                    ret += items.Sum(x => x.price - x.settledFromDepositAmount);
            }
            return ret;
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        static ICryptoTransform decryptor;

        const int EMPTY_QUE_WAIT = 2000;
        const int ERROR_QUE_WAIT = 2000;

        static CBEWSIS()
        {
            byte[] key = System.Convert.FromBase64String("WHaWdf0SRxP3oQZPwaDSWgNFl4MV1fgg");
            byte[] iv = StringToByteArray("31323334353630303030303030303030");
            var c = System.Security.Cryptography.Aes.Create();
            c.KeySize = 192;
            c.Mode = System.Security.Cryptography.CipherMode.CBC;
            c.Padding = System.Security.Cryptography.PaddingMode.PKCS7;
            c.Key = key;
            c.IV = iv;
            decryptor = c.CreateDecryptor();
            initPaymentQuerProcessor();
        }
        string DecryptPassword(String password)
        {
            //byte[] key=Cromulent.Encoding.Z85.FromZ85String("WHaWdf0SRxP3oQZPwaDSWgNFl4MV1fgg");
           
           
            
            byte [] data= System.Convert.FromBase64String(password);

            lock (decryptor)
            {
                // Create a memorystream to write the decrypted data in it   
                using (MemoryStream ms = new MemoryStream(data))
                {
                    using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader reader = new StreamReader(cs))
                        {
                            // Reutrn all the data from the streamreader   
                            var ret = reader.ReadToEnd();
                            var passlen = int.Parse(ret.Substring(6, 2));
                            var pass = ret.Substring(22, passlen);
                            return pass;
                        }
                    }
                }
            }

        }
        public ApplyTransactionResponse ApplyTransactionRequest(ApplyTransactionRequest request)
        {
            if (Startup.ValidatePassword)
            {
                var pw = DecryptPassword(request.Header.Password);
                if (!request.Header.LoginID.Equals(Startup.WsisUser, StringComparison.CurrentCultureIgnoreCase)
                    || !pw.Equals(Startup.WsisPassword))
                    throw new AccessViolationException("CBEBirr sent invalid credential");
            }

            var customerCodePar = request.Body.Parameters.Where(x => "BillReferenceNumber".Equals(x.Key)).FirstOrDefault();
            if (customerCodePar == null || String.IsNullOrEmpty(customerCodePar.Value) || customerCodePar.Value.Trim().Length == 0)
                throw new AccessViolationException("CBEBirr didn't send customer code");

            var customerCode = customerCodePar.Value.Trim();
            if (!paymentQueOK)
                throw new AccessViolationException("CBEBirr WSIS link is not avialable because of internal error");

            string sessionID;
            Subscriber customer;
            ConnectAndGetCustomer(customerCode, out sessionID, out customer);

            var amount = TotalBills(sessionID, customer.id);
            if (amount > 0)
                return new ApplyTransactionResponse()
                {
                    ResponseCode = "0",
                    ResponseDesc = "Success",
                    Parameters = new ParameterType[]
                    {
                        new ParameterType(){Key="BillRefNumber",Value=customerCode},
                        new ParameterType(){Key="TransID",Value="NA" },
                        new ParameterType(){Key="UtilityName",Value="Adama TWSSS" },
                        new ParameterType(){Key="CustomerName",Value=customer.name },
                        new ParameterType(){Key="Amount",Value=Math.Round(amount,2).ToString()},
                    }
                };
            else
            {
                return new ApplyTransactionResponse()
                {
                    ResponseCode = "1",
                    ResponseDesc = "The customer has no outstanding bill",
                    Parameters = new ParameterType[]
                    {
                        new ParameterType(){Key="BillRefNumber",Value=customerCode},
                        new ParameterType(){Key="TransID",Value="NA" },
                        new ParameterType(){Key="UtilityName",Value="Adama TWSSS" },
                        new ParameterType(){Key="CustomerName",Value=customer.name },
                        new ParameterType(){Key="Amount",Value=Math.Round(amount,2).ToString()},
                    }
                };
            }
        }

        private static void ConnectAndGetCustomer(string customerCode, out string sessionID, out Subscriber customer)
        {
            sessionID = Connect();
            var _ret = RESTAPIClient.Call<GetSubscriptions2Out>($"{Startup.WSIS_Server}/api/erp/subscribermanagment/GetSubscriptions2",
                new { sessionID = sessionID, query = customerCode, fields = SubscriberSearchField.CustomerCode, multipleVersion = false, kebele = -1, zone = -1, dma = -1, index = 0, page = 10 });
            if (_ret.NRecord == 0)
                throw new InvalidOperationException($"No customer found with customer code {customerCode}");
            if (_ret.NRecord > 1)
                throw new InvalidOperationException($"Multiple customers found with customer code {customerCode}");
            customer = _ret._ret[0].subscriber;
        }

        public C2BPaymentConfirmationResponse C2BPaymentConfirmationRequest(C2BPaymentConfirmationRequest request)
        {
            return new C2BPaymentConfirmationResponse() { C2BPaymentConfirmationResult = "Got it" };
        }

        public C2BPaymentValidationResponse C2BPaymentValidationRequest(C2BPaymentValidationRequest request)
        {
            var t1=DateTime.Now;
            try
            {
                if (!paymentQueOK)
                    throw new AccessViolationException("CBEBirr WSIS link is not avialable because of internal error");

                double requestAmount;

                if (!double.TryParse(request.TransAmount, out requestAmount))
                    throw new AccessViolationException($"CBEBirr sent invalid transaction amount {request.TransAmount}");
                Subscriber customer;
                String sessionID;
                ConnectAndGetCustomer(request.BillRefNumber, out sessionID, out customer);
                var total = TotalBills(sessionID, customer.id);
                if (!Accounting.Account.AmountEqual(total, requestAmount))
                    throw new Exception($"Request amount {requestAmount} is not equal to bill amount {total}");
                var successMessage = "Payment accepted";
                if (customer != null && sessionID != null)
                {
                    lock (paymentQue)
                    {
                        var duplicate = false;
                        foreach(var r in paymentQue)
                        {
                            if(r.BillRefNumber.Equals(request.BillRefNumber) && Accounting.AccountBase.AmountEqual(double.Parse(r.TransAmount), requestAmount))
                            {
                                duplicate = true;
                                break;
                            }
                        }
                        if (duplicate)
                        {
                            successMessage = "This is a duplicate request. The previous request is being processed";
                            Console.WriteLine($"Duplicate request detected. Ignore with success message");
                        }
                        else
                        {
                            paymentQue.Enqueue(request);
                            Console.WriteLine($"Payment queued. Que size {paymentQue.Count}");
                        }
                    }
                }
                else
                    throw new Exception($"Invalid request. {((customer == null && sessionID != null) ? "Customer code is not valid":"")}");
                //var payment = RESTAPIClient.Call<int>($"{Startup.WSIS_Server}/api/erp/subscribermanagment/PayAvialableBills",
                //new { sessionID = sessionID, customerID = customer.id, paymentAmount = requestAmount, paymentDescription = "Payment from CBEBirr" });
                var t2 = DateTime.Now;
                return new C2BPaymentValidationResponse() { ResultCode = "0", ResultDesc = $"{successMessage}. Request processed in {Math.Round(t2.Subtract(t1).TotalSeconds,3)}",ThirdPartyTransID=""};
            }
            catch(Exception ex)
            {
                return new C2BPaymentValidationResponse() { ResultCode = "1", ResultDesc = ex.Message};
            }
        }
    }

}
