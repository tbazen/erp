﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace INTAPS.WSIS.CBEBirr.Service
{
    [XmlRoot(Namespace = "http://cps.huawei.com/cpsinterface/goa")]  
    public class ParameterType
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ParameterTypeRequest
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class C2BPaymentConfirmationRequestKYCInfo
    {
        public string KYCName { get; set; }
        public string KYCValue { get; set; }
    }


    public class C2BPaymentValidationRequestKYCInfo
    {
        public string KYCName { get; set; }
        public string KYCValue { get; set; }
    }
    public class C2BPaymentValidationRequest
    {
        public C2BPaymentValidationRequest()
        {
            BillRefNumber = "not set";
        }
        public string TransType { get; set; }

        public string TransID { get; set; }

        public string TransTime { get; set; }

        public string TransAmount { get; set; }
        public string BusinessShortCode { get; set; }
        public string BillRefNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string MSISDN { get; set; }

        public C2BPaymentValidationRequestKYCInfo[] KYCInfo { get; set; }
    }
    [XmlRoot(ElementName = "C2BPaymentValidationResult", Namespace = "http://cps.huawei.com/cpsinterface/c2bpayment")]
    public class C2BPaymentValidationResponse
    {
        [XmlElement(Namespace = "")]
        public string ResultCode { get; set; }
        [XmlElement(Namespace = "")]
        public string ResultDesc { get; set; }
        [XmlElement(Namespace = "")]
        public string ThirdPartyTransID { get; set; }
    }
    public partial class C2BPaymentConfirmationRequest
    {
        public string TransType { get; set; }
        public string TransID { get; set; }
        public string TransTime { get; set; }
        public string TransAmount { get; set; }
        public string BusinessShortCode { get; set; }
        public string BillRefNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string ThirdPartyTransID { get; set; }
        public string MSISDN { get; set; }
        public C2BPaymentConfirmationRequestKYCInfo[] KYCInfo { get; set; }
    }
    [XmlRoot(ElementName = "C2BPaymentConfirmationResult", Namespace = "http://cps.huawei.com/cpsinterface/c2bpayment")]
    public class C2BPaymentConfirmationResponse
    {
        [XmlElement(Namespace = "")]
        public string C2BPaymentConfirmationResult { get; set; }
    }
    public class HeaderType
    {
        public string CommandID { get; set; }
        public string Version { get; set; }
        public string LoginID { get; set; }
        public string Password { get; set; }
        public string Timestamp { get; set; }
        public string ConversationID { get; set; }
        [XmlArray("HeaderExtension")]
        [XmlArrayItem("Parameter")]
        public ParameterType[] HeaderExtension { get; set; }
    }
    
    public class ApplyTransactionRequestBody
    {
        [XmlArray("Parameters")]
        [XmlArrayItem("Parameter")]
        public ParameterTypeRequest[] Parameters { get; set; }

    }
    public class ApplyTransactionRequest
    {
        public HeaderType Header { get; set; }
        public ApplyTransactionRequestBody Body { get; set; }
    }

    [XmlRoot(ElementName = "ApplyTransactionResponse", Namespace = "http://tempuri.org/")]
    public class ApplyTransactionResponse
    {
        [XmlElement(Namespace = "http://cps.huawei.com/cpsinterface/goa/at")]
        public string ResponseCode { get; set; }
        [XmlElement(Namespace = "http://cps.huawei.com/cpsinterface/goa/at")]
        public string ResponseDesc { get; set; }

        [XmlArray("Parameters", Namespace = "http://cps.huawei.com/cpsinterface/goa/at")]
        [XmlArrayItem("Parameter", Namespace = "http://cps.huawei.com/cpsinterface/goa")]
        public ParameterType[] Parameters { get; set; }
    }

}
