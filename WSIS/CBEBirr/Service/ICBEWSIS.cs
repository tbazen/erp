﻿namespace INTAPS.WSIS.CBEBirr.Service
{
    public interface ICBEWSIS
    {

        C2BPaymentValidationResponse C2BPaymentValidationRequest(C2BPaymentValidationRequest C2BPaymentValidationRequest);

        C2BPaymentConfirmationResponse C2BPaymentConfirmationRequest(C2BPaymentConfirmationRequest C2BPaymentConfirmationRequest);

        ApplyTransactionResponse ApplyTransactionRequest(ApplyTransactionRequest ApplyTransactionRequest);

    }
}