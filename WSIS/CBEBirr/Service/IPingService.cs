﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace INTAPS.WSIS.CBEBirr.Service
{
    public interface IPingService
    {
        string Ping(string msg);
    }
    public class PingService : IPingService
    {
        public string Ping(string msg)
        {
            return string.Join(string.Empty, msg.Reverse());
        }
    }
}
