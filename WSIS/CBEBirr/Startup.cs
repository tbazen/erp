﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using INTAPS.WSIS.CBEBirr.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace INTAPS.WSIS.CBEBirr
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new PingService());
            services.AddSingleton(new CBEWSIS());
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
        }
        static private System.Xml.XmlElement RemovePrefix(System.Xml.XmlDocument doc)
        {
            var root = RemovePrefix(doc, doc.DocumentElement);
            return root;
        }
        static private System.Xml.XmlElement RemovePrefix(System.Xml.XmlDocument doc, System.Xml.XmlElement el)
        {
            var ret = doc.CreateElement(el.LocalName);
            var hasChild = false;
            if (doc.HasChildNodes)
            {
                foreach (var n in el.ChildNodes)
                {
                    if (n is System.Xml.XmlElement)
                    {
                        ret.AppendChild(RemovePrefix(doc, (System.Xml.XmlElement)n));
                        hasChild = true;
                    }
                }
            }

            if (!hasChild)
            {
                ret.InnerText = el.InnerText;
            }

            return ret;
        }

        static Dictionary<String, MessageAction> payloadTypes = new Dictionary<string, MessageAction>();
        static XmlSerializerNamespaces nameSpaces = null;
        static XmlSerializerNamespaces nameSpaces2 = null;
        static XmlWriterSettings xmlSetting;
        public static string WSIS_Server;
        public static string CBEID;
        public static string LogDb;
        public static string WsisUser;
        public static string WsisPassword;
        public static bool ValidatePassword;
        static int logCounter = 1;
        class MessageAction
        {
            public Type type;
            public String method;
        }
        static Startup()
        {
            payloadTypes.Add("ApplyTransactionRequest", new MessageAction { type = typeof(ApplyTransactionRequest), method = "ApplyTransactionRequest" });
            payloadTypes.Add("C2BPaymentValidationRequest", new MessageAction { type = typeof(C2BPaymentValidationRequest), method = "C2BPaymentValidationRequest" });
            payloadTypes.Add("C2BPaymentConfirmationRequest", new MessageAction { type = typeof(C2BPaymentConfirmationRequest), method = "C2BPaymentConfirmationRequest" });
            nameSpaces = new XmlSerializerNamespaces();
            nameSpaces.Add("c2bpayment", "http://cps.huawei.com/cpsinterface/c2bpayment");
            nameSpaces2 = new XmlSerializerNamespaces();
            nameSpaces2.Add("tem", "http://tempuri.org");
            nameSpaces2.Add("at", "http://cps.huawei.com/cpsinterface/goa/at");
            nameSpaces2.Add("goa", "http://cps.huawei.com/cpsinterface/goa");
            xmlSetting = new XmlWriterSettings();
            xmlSetting.OmitXmlDeclaration = true;
        }
        static void LogMessage(DateTime time,String msg,bool request)
        {
            try
            {
                if (!System.IO.Directory.Exists("log"))
                {
                    System.IO.Directory.CreateDirectory("log");
                }
                var fn = $"{time.Year}-{time.Month.ToString("00")}-{time.Day.ToString("00")}-{time.Hour.ToString("00")}-{time.Minute.ToString("00")}-{time.Second.ToString("00")}.{time.Millisecond.ToString("000")}.{(logCounter % 100).ToString("00")}-{(request ? "req" : "res")}.log";
                fn = System.IO.Path.Combine("log", fn);
                System.IO.File.WriteAllText(fn, msg);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Log writing failed");
                Console.Write(ex.Message);
            }
        }
        private static void HandleCBEWSIS(IApplicationBuilder app)
        {
            
            app.Run(async context =>
            {
                var time = DateTime.Now;
                try
                {
                    using (var mstm = new System.IO.MemoryStream((int)context.Request.ContentLength.GetValueOrDefault(1024)))
                    {
                       
                        await context.Request.Body.CopyToAsync(mstm).ConfigureAwait(false);
                        mstm.Seek(0, System.IO.SeekOrigin.Begin);
                        var doc = new System.Xml.XmlDocument();
                        doc.Load(mstm);
                        LogMessage(time, doc.OuterXml, true);
                        var el = RemovePrefix(doc);
                        if (!"Envelope".Equals(el.Name))
                            throw new InvalidOperationException("Envelop should be root element");
                        System.Xml.XmlElement soapBody = null;
                        foreach (System.Xml.XmlNode n in el.ChildNodes)
                        {
                            if (n is System.Xml.XmlElement && "Body".Equals(n.Name, StringComparison.InvariantCultureIgnoreCase))
                            {
                                soapBody = (System.Xml.XmlElement)n;
                                break;
                            }
                        }
                        if (soapBody == null)
                            throw new InvalidOperationException("Soap body not found");
                        var payLoad = soapBody.FirstChild;
                        if (!(payLoad is System.Xml.XmlElement))
                            throw new InvalidOperationException("Soap message payload not found");
                        var payLoadElement = (System.Xml.XmlElement)payLoad;
                        var payloadType = payloadTypes.GetValueOrDefault(payLoadElement.Name);
                        if (payloadType == null)
                            throw new InvalidOperationException($"Unknown message payload type {payLoadElement.Name}");
                        var s = new System.Xml.Serialization.XmlSerializer(payloadType.type);

                        object ret;
                        using (var sr = new System.IO.StringReader(payLoadElement.OuterXml))
                        {
                            var message = s.Deserialize(sr);
                            ret = typeof(CBEWSIS).GetMethod(payloadType.method).Invoke(new CBEWSIS(), new object[] { message });
                        }
                        context.Response.ContentType = payloadType.type.Equals(typeof(ApplyTransactionRequest)) ? "application/soap+xml":"text/xml";
                        string resp;
                        if (ret == null)
                            resp = "<null></null>";
                        else
                        {

                            s = new System.Xml.Serialization.XmlSerializer(ret.GetType(), "http://cps.huawei.com/cpsinterface/c2bpayment");

                            using (var sw = new System.IO.StringWriter())
                            {
                                using (var xw = XmlWriter.Create(sw, xmlSetting))
                                {
                                    var ns = payloadType.type.Equals(typeof(ApplyTransactionRequest)) ? nameSpaces2 : nameSpaces;
                                    s.Serialize(xw, ret, ns);
                                    resp = sw.ToString();
                                    var soap = payloadType.type.Equals(typeof(ApplyTransactionRequest)) ?
    $@"<env:Envelope xmlns:env=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ns1=""http://tempuri.org/"">
   <env:Body>
      {resp}
   </env:Body>
 </env:Envelope>" :
     $@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ns1=""http://cps.huawei.com/cpsinterface/c2bpayment"">
   <SOAP-ENV:Body>
      {resp}
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>";
                                    resp = soap;
                                }
                            }


                        }
                        var b = System.Text.UTF8Encoding.GetEncoding("utf-8").GetBytes(resp);
                        //await context.Response.WriteAsync(resp);
                        await context.Response.Body.WriteAsync(b);
                        LogMessage(time, resp, false);
                    }
                }
                catch(Exception ex)
                {
                    
                    var resp = $"Error processing request\n{ex.Message}\n{ex.StackTrace}";
                    while(ex.InnerException!=null)
                    {
                        ex = ex.InnerException;
                        resp = $"->{ex.Message}\n{ex.StackTrace}"+resp;
                    }
                    await context.Response.WriteAsync(resp);
                    LogMessage(time, resp, false);
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();


            app.UseMvc();
            app.UseStaticFiles();
            loadConfiguration();
            app.Map("/CBEWSIS", HandleCBEWSIS);
        }

        private void loadConfiguration()
        {
            var conf = Configuration.GetSection("HostConfiguration");
            Startup.WSIS_Server = conf.GetValue<String>("WSISServer");
            Startup.CBEID = conf.GetValue<String>("CBEID");
            Startup.LogDb = conf.GetValue<String>("LogDb");
            Startup.WsisUser = conf.GetValue<String>("WsisUser");
            Startup.WsisPassword = conf.GetValue<String>("WsisPassword");
            Startup.ValidatePassword = conf.GetValue<bool>("ValidatePassword");
        }
    }
}
