package et.tewelde.d2dservice;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import et.tewelde.d2dservice.entity.GEOLocation;

public class LocationWrapper
{
    LocationManager _locationManager;
    Location _lastKnownLocation = null;
    LocationListener _locListener = new LocationListener()
    {
        @Override
        public void onLocationChanged(Location location)
        {
            _lastKnownLocation = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            if (status != LocationProvider.AVAILABLE)
                _lastKnownLocation = null;
            else if (status == LocationProvider.AVAILABLE)
            {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    _lastKnownLocation=null;
                    return;
                }
                _lastKnownLocation = _locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }

        @Override
        public void onProviderEnabled(String provider)
        {

        }

        @Override
        public void onProviderDisabled(String provider)
        {
            _lastKnownLocation=null;
        }
    };
    Context context;
    private Object ret;

    public LocationWrapper(Context context)
    {
        this.context=context;
    }
    void initializeLocation()
    {
        _locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        _lastKnownLocation = null;

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        String provider=_locationManager.getBestProvider(new Criteria(),true);
        if(org.apache.commons.lang3.StringUtils.isEmpty(provider))
            provider=LocationManager.GPS_PROVIDER;
        _locationManager.requestLocationUpdates(provider, 1000, 1, _locListener);
        _lastKnownLocation=_locationManager.getLastKnownLocation(provider);
    }

    void releaseLocation()
    {
        _lastKnownLocation=null;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        _locationManager.removeUpdates(_locListener);
    }

    public GEOLocation getLastKnownLocation()
    {
        if(_lastKnownLocation==null)
            return null;
        GEOLocation loc=new GEOLocation();
        loc.lat=_lastKnownLocation.getLatitude();
        loc.lng=_lastKnownLocation.getLongitude();
        loc.alt=_lastKnownLocation.getAltitude();
        return loc;
    }

}
