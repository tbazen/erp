package et.tewelde.d2dservice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import et.tewelde.d2dservice.entity.DataFieldType;
import et.tewelde.d2dservice.entity.DataUpdate;
import et.tewelde.d2dservice.entity.GEOLocation;
import et.tewelde.d2dservice.entity.MRReadingSetting;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.JSONUtils;
import et.tewelde.d2dservice.util.ObjectCursor;

/**
 * Created by Teweldemedhin Aberra on 3/27/2017.
 */
public class MRRepository
{

    private Cursor transfersCurosr;

    public <T> T getSettingObject(String key, Class<T> c) throws IOException
    {
        synchronized (db)
        {
            Cursor cur = db.rawQuery("Select value_blob from Settings where key='" + key + "'", null);
            try
            {
                if (!cur.moveToNext())
                    return null;
                String str = new String(cur.getBlob(0));
                return JSONUtils.fromJson(c, str);
            } finally
            {
                cur.close();
            }
        }
    }

    public <T> void setSettingObject(String key, T value) throws IOException
    {
        synchronized (db)
        {
            Cursor cur = db.rawQuery("Select count(*) from Settings where key='" + key + "'", null);
            try
            {
                cur.moveToNext();
                if (cur.getInt(0) == 0)
                {
                    ContentValues cv = new ContentValues();
                    cv.put("key", key);
                    cv.put("value_blob", JSONUtils.toJson(value).getBytes());
                    db.insert("Settings", null, cv);
                } else
                {
                    ContentValues cv = new ContentValues();
                    cv.put("value_blob", JSONUtils.toJson(value).getBytes());
                    db.update("Settings", cv, "key='" + key + "'", null);
                }
            } finally
            {
                cur.close();
            }
        }
    }

    public int getIntSetting(String key, int defval)
    {
        synchronized (db)
        {
            Cursor cur = db.rawQuery("Select value from Settings where key='" + key + "'", null);
            try
            {
                if (!cur.moveToNext())
                    return defval;
                return Integer.parseInt(cur.getString(0));
            } catch (Exception ex)
            {
                ex.printStackTrace();
                return defval;
            } finally
            {
                cur.close();
            }
        }
    }

    void setValueSetting(String key, ContentValues cv)
    {
        synchronized (db)
        {
            Cursor cur = db.rawQuery("Select count(*) from Settings where key='" + key + "'", null);
            try
            {
                cur.moveToNext();
                if (cur.getInt(0) == 0)
                {
                    cv.put("key", key);
                    db.insert("Settings", null, cv);
                } else
                {
                    db.update("Settings", cv, "key='" + key + "'", null);
                }
            } finally
            {
                cur.close();
            }
        }
    }
    public MRReadingSetting getSetting() throws IOException
    {
        synchronized (db)
        {
            return this.getSettingObject("readingSetting", MRReadingSetting.class);
        }
    }
    public void setSetting(MRReadingSetting setting) throws IOException
    {
        synchronized (db)
        {
            this.setSettingObject("readingSetting", setting);
        }
    }
    public void setIntSetting(String key, int intVal)
    {
        ContentValues cv = new ContentValues();
        cv.put("value", Integer.toString(intVal));
        setValueSetting(key, cv);
    }

    public ReadingSheetRecord getRecord(int connectionID) throws IOException
    {
        synchronized (db)
        {
            Cursor cur = db.rawQuery("Select * from ReadingSheetRow where connectionID=" + connectionID, null);
            try
            {
                if (!cur.moveToNext())
                    return null;
                return createReadingSheetRecord(cur);
            } finally
            {
                cur.close();
            }
        }
    }

    @NonNull
    public static ReadingSheetRecord createReadingSheetRecord(Cursor cur) throws IOException
    {
        ReadingSheetRecord rec = new ReadingSheetRecord();
        rec.connectionID = cur.getInt(cur.getColumnIndex("connectionID"));
        rec.orderN = cur.getInt(cur.getColumnIndex("orderN"));
        rec.periodID = cur.getInt(cur.getColumnIndex("periodID"));
        rec.contractNo = cur.getString(cur.getColumnIndex("contractNo"));
        rec.customerName = cur.getString(cur.getColumnIndex("customerName"));
        rec.meterNo = cur.getString(cur.getColumnIndex("meterNo"));
        rec.meterType = cur.getString(cur.getColumnIndex("meterType"));
        rec.meterTypeID = cur.getString(cur.getColumnIndex("meterTypeID"));
        rec.lat = cur.getDouble(cur.getColumnIndex("lat"));
        rec.lng = cur.getDouble(cur.getColumnIndex("lng"));
        rec.connectionStatus = cur.getInt(cur.getColumnIndex("connectionStatus"));
        rec.phoneNo = cur.getString(cur.getColumnIndex("phoneNo"));
        rec.readerID = cur.getInt(cur.getColumnIndex("readerID"));
        rec.minReadAllowed = cur.getDouble(cur.getColumnIndex("minAllowed"));
        rec.maxReadAllowed = cur.getDouble(cur.getColumnIndex("maxAllowed"));
        rec.status = cur.getInt(cur.getColumnIndex("status"));
        rec.actualReaderID = cur.getInt(cur.getColumnIndex("actualReaderID"));
        rec.reading = cur.getDouble(cur.getColumnIndex("reading"));
        rec.extraInfo = cur.getInt(cur.getColumnIndex("extraInfo"));
        rec.problem = cur.getInt(cur.getColumnIndex("problem"));
        rec.remark = cur.getString(cur.getColumnIndex("remark"));
        rec.readTime = cur.getLong(cur.getColumnIndex("readTime"));
        rec.readingLat = cur.getDouble(cur.getColumnIndex("readingLat"));
        rec.readingLng = cur.getDouble(cur.getColumnIndex("readingLng"));
        rec.canPay = cur.getInt(cur.getColumnIndex("canPay")) != 0;
        rec.billText = cur.getString(cur.getColumnIndex("billText"));
        rec.payStatus = cur.getInt(cur.getColumnIndex("payStatus"));
        rec.payTime = cur.getLong(cur.getColumnIndex("payTime"));
        rec.payConfirmTime = cur.getLong(cur.getColumnIndex("payConfirmTime"));
        byte[] billData=cur.getBlob(cur.getColumnIndex("bill"));
        if(billData!=null && billData.length>0)
            rec.bill=JSONUtils.getAdapter(ReadingSheetRecord.MRBill.class).fromJson(new String(billData));
        else
            rec.bill=null;
        rec.billTotal=cur.getDouble(cur.getColumnIndex("billTotal"));
        rec.rowKey=cur.getString(cur.getColumnIndex("rowKey"));
        rec.customerType=cur.getInt(cur.getColumnIndex("customerType"));
        rec.connectionType=cur.getInt(cur.getColumnIndex("connectionType"));
        return rec;
    }

    public void invalidateBill(int connectionID) throws IOException
    {
        synchronized (db)
        {
            ReadingSheetRecord rec = getRecord(connectionID);
            if(rec==null)
                return;
            if (rec.payStatus != ReadingSheetRecord.PAYMENT_STATUS_ACTIVE)
                return;
            ContentValues cv = new ContentValues();
            cv.put("payStatus", ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_INVALIDATED);
            db.update("ReadingSheetRow", cv, "connectionID=" + connectionID, null);
        }
    }

    void updateRecord(ReadingSheetRecord rec) throws IOException
    {
        synchronized (db)
        {
            ReadingSheetRecord old=getRecord(rec.connectionID);
            ContentValues cv = getContentValues(rec,true);
            db.update("ReadingSheetRow", cv, "connectionID=" + rec.connectionID, null);
            sum.updateBy(old, rec, getSetting());
        }
    }
    public ReadingSheetRecord collectPayment(int conID) throws IOException
    {
        synchronized (db)
        {
            ReadingSheetRecord rec = this.getRecord(conID);
            if (rec == null)
                throw new IllegalArgumentException("Invalid connectionID: " + conID);
            if (!rec.paymentAllowed())
                throw new IllegalArgumentException("Payment not allowed contract no: " + rec.contractNo);
            rec.payStatus = ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID;
            rec.payTime = new Date().getTime();
            this.updateRecord(rec);
            return rec;
        }
    }

    public List<ReadingSheetRecord> getTransferList() throws IOException
    {
        ArrayList<ReadingSheetRecord> ret=new ArrayList<>();

        Cursor cur=db.rawQuery("Select * from ReadingSheetRow where status=" + ReadingSheetRecord.READ_STAT_READ
                + " or payStatus="+ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID,null);
        while(cur.moveToNext())
        {
            ret.add(createReadingSheetRecord(cur));
        }
        return ret;
    }

    public void confirmPayment(int connectionID) throws IOException
    {
        synchronized (db)
        {
            ReadingSheetRecord rec=getRecord(connectionID);
            MRReadingSetting rs=getSetting();
            if (rec == null)
                throw new IllegalArgumentException("Invalid connectionID: " + connectionID);
            rec.payStatus = ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED;
            rec.payConfirmTime= new Date().getTime();
            this.updateRecord(rec);
        }
    }
    public void confirmPayment(int [] bills) throws IOException
    {
        synchronized (db)
        {
            Cursor cur=db.rawQuery("Select * from ReadingSheetRow where payStatus=" + ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID
                    +" or payStatus=" + ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_INVALIDATED , null);
            while(cur.moveToNext())
            {
                ReadingSheetRecord rec=createReadingSheetRecord(cur);
                if(rec.bill!=null && rec.bill.idEquals(bills))
                    confirmPayment(rec.connectionID);
            }
        }
    }

    public ReadingSheetCursor getPaymentListCursor()
    {
        synchronized (db)
        {
            String filter="payStatus="+ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID
                    +" or payStatus="+ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED;
            Cursor rscur=db.rawQuery("Select count(*) from ReadingSheetRow where "+ filter,null);
            int nrect;
            try
            {
                rscur.moveToNext();
                nrect=rscur.getInt(0);
            }
            finally
            {
                rscur.close();
            }
            rscur = db.rawQuery("Select * from ReadingSheetRow where "+ filter +" order by orderN", null);
            return new ReadingSheetCursor(rscur,nrect);
        }
    }

    public void recordReading(int connectionID, double reading, String remark,int problem,GEOLocation location) throws IOException
    {
        synchronized (db)
        {
            MRReadingSetting setting = getSetting();
            ReadingSheetRecord record = getRecord(connectionID);
            if (record.status == ReadingSheetRecord.READ_STAT_READ_UPLOADED)
                throw new IllegalArgumentException("Already read");
            if (record.status == ReadingSheetRecord.READ_STAT_INITIAL && record.readerID != -1 && record.readerID != setting.readerID)
                throw new IllegalArgumentException("This meter is not assigned to you");
            if (setting.readingDistanceTolerance > 0)
            {
                if (location == null)
                    throw new IllegalArgumentException("It is not allowed to read without GPS");
                if (record.hasLocation())
                {
                    double dist = GEOLocation.sphericalDistance(location.lat, location.lng, record.lat, record.lng);
                    if (dist > setting.readingDistanceTolerance)
                        throw new IllegalArgumentException("You are too far from the location of the water meter.");
                }
            }
            record.readTime = new Date().getTime();
            record.extraInfo = 2;
            record.reading = reading;
            record.problem = problem;
            record.remark = remark;
            record.actualReaderID=setting.readerID;
            if (!StringUtils.isEmpty(remark))
                record.extraInfo |= 4;
            if (location != null)
            {
                record.readingLat = location.lat;
                record.readingLng = location.lng;
                record.extraInfo |= 1;
            }
            record.status = ReadingSheetRecord.READ_STAT_READ;
            updateRecord(record);

        }
    }

    public void clearReading(int connectionID) throws IOException
    {
        synchronized (db)
        {
            MRReadingSetting setting = getSetting();
            ReadingSheetRecord record = getRecord(connectionID);
            if (record.status == ReadingSheetRecord.READ_STAT_READ_UPLOADED)
                throw new IllegalArgumentException("Reading is uploaded to the server. It can't be cleared.");
            if (record.status != ReadingSheetRecord.READ_STAT_READ)
                throw new IllegalArgumentException("This meter is not read");
            record.readTime = new Date().getTime();
            record.extraInfo = 0;
            record.reading = 0;
            record.problem = 0;
            record.remark = "";
            record.actualReaderID=-1;
            record.status = ReadingSheetRecord.READ_STAT_INITIAL;
            updateRecord(record);
        }
    }

    public void confirmReading(int connectionID) throws IOException
    {
        synchronized (db)
        {
            ReadingSheetRecord rec=getRecord(connectionID);
            if(rec.status==ReadingSheetRecord.READ_STAT_READ)
            {
                rec.status = ReadingSheetRecord.READ_STAT_READ_UPLOADED;
                this.updateRecord(rec);
            }

        }

    }

    public void setProfileDataUploaded(int connectionID)
    {
        synchronized (db)
        {
            ContentValues cv=new ContentValues();
            cv.put("status",1);
            db.update("DataUpdate",cv,"connectionID="+connectionID,null);
        }
    }

    public void recordOutgoingPayment(ReadingSheetSync.MobileChangeLog.Change change)
    {
        synchronized (db)
        {
            ContentValues cv=new ContentValues();
            cv.put("payTime",change.paymentTime);
            cv.put("utilityPayment",change.utilityPayment);
            cv.put("systemPayment",change.systemPayment);
            db.insert("Payments", null, cv);
        }
    }

    public void clearReadingUploadFlags() throws IOException
    {
        synchronized (db)
        {
            db.execSQL("update ReadingSheetRow set status="+ReadingSheetRecord.READ_STAT_READ
            +" where status="+ReadingSheetRecord.READ_STAT_READ_UPLOADED
            );
            sum=new ViewModel.WorkSummary();
            updateSummary();
        }
    }

    public String executeQuery(String sql) {
        synchronized (db)
        {
            Cursor rscur=db.rawQuery(sql,null);
            try
            {

                if(rscur.moveToNext()) {
                    String str = rscur.getString(0);
                    if(str==null)
                        return  "<null>";
                    return str;
                }
                return  "Empty result";
            }
            finally
            {
                rscur.close();
            }
        }
    }

    public static class DatabaseHandler extends SQLiteOpenHelper
    {
        // All Static variables
        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "mreading";

        Context context;
        String[] tables;
        String[] createQueries;

        public DatabaseHandler(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;

            tables = new String[]{"ReadingSheetRow", "DataUpdate", "Settings","Payments"};
            createQueries = new String[]{
                    this.context.getResources().getString(R.string.sql_create_reading_sheet_row)
                    , this.context.getResources().getString(R.string.sql_create_data_update)
                    , this.context.getResources().getString(R.string.sql_create_settings)
                    ,this.context.getResources().getString(R.string.sql_create_payment)
            };
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db)
        {

            int i = 0;
            for (String t : tables)
            {
                db.execSQL(createQueries[i]);
                i++;
            }
        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            // Drop older table if existed

            for (String t : tables)
            {
                db.execSQL("DROP TABLE IF EXISTS " + t);
            }
            // Create tables again
            onCreate(db);
        }

        public void checkAndCreateIndexes()
        {

        }
    }

    final String DATA_FILE = "config.json";
    SQLiteDatabase db;
    static int count = 0;

    ViewModel.WorkSummary sum;
    HashMap<Integer,GEOLocation> coordinateCache=new HashMap<>();
    boolean coordinateCacheValid=false;
    public MRRepository(Context c) throws IOException
    {
        count++;
        if (count > 1)
            throw new IllegalStateException("only one instance of repository is allowed");
        db = new DatabaseHandler(c).getReadableDatabase();
        MRReadingSetting stg=getSetting();
        if(stg!=null)
            DataFieldType.initType(stg);
        updateSummary();
        //cacheCoordinates();
    }
    private void cacheCoordinates()
    {

        synchronized (db) {
            Cursor cur = db.rawQuery("Select lat,lng,connectionID from ReadingSheetRow", null);
            try {
                coordinateCache.clear();
                coordinateCacheValid=false;
                while (cur.moveToNext()) {
                    GEOLocation rec = new GEOLocation();
                    rec.lat = cur.getDouble(cur.getColumnIndex("lat"));
                    rec.lng = cur.getDouble(cur.getColumnIndex("lng"));
                    int connectionID = cur.getInt(cur.getColumnIndex("connectionID"));
                    if (rec.isValid()) {
                        coordinateCache.put(connectionID, rec);
                    }
                }
                coordinateCacheValid=true;
            } finally {
                cur.close();
            }
        }
    }
    private void invalidateCoordinateCache()
    {
        synchronized (db)
        {
            coordinateCacheValid=false;
        }
    }
    private void checkAndCacheCoordinates()
    {
        synchronized (db)
        {
            if(coordinateCacheValid)
                return;
            cacheCoordinates();
        }
    }

    private void updateSummary() throws IOException
    {
        sum=getWorkSummaryDB();
    }

    public void shutDown()
    {
        synchronized (db)
        {
            db.close();
            count--;
        }
    }
    ViewModel.WorkSummary sumBeforTran=null;
    public void beginTransaction()
    {
        synchronized (db)
        {
            db.beginTransaction();
            if(sum==null)
                sumBeforTran=null;
            else
                sumBeforTran=sum.clone();
        }
    }

    public void rollbackTransaction()
    {
        synchronized (db)
        {
            db.endTransaction();
            sum=sumBeforTran;
        }
    }

    public void commitTransaction()
    {
        synchronized (db)
        {
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }

    public void clearReadingSheetRecord()
    {
        synchronized (db)
        {
            db.execSQL("delete from ReadingSheetRow");
            sum=new ViewModel.WorkSummary();
            invalidateCoordinateCache();
        }
    }
    public void clearDataUpdate()
    {
        synchronized (db)
        {
            db.execSQL("delete from DataUpdate");
        }
    }
    public void clearDataUpdateSyncFlag()
    {
        synchronized (db)
        {
            db.execSQL("Update DataUpdate set status=0");
        }
    }
    public void clearOutgingPaymentRecord() throws IOException
    {
        synchronized (db)
        {
            db.execSQL("delete from Payments");
            updateSummary();
        }
    }
    public void saveRecord(ReadingSheetRecord rec) throws IOException
    {
        synchronized (db)
        {
            ContentValues cv = getContentValues(rec, false);
            db.insert("ReadingSheetRow", null, cv);
            sum.updateBy(null, rec, getSetting());
        }
    }
    DataUpdate getProfileUpdateData(int connectionID) throws IOException
    {
        synchronized (db)
        {
            Cursor cur=db.rawQuery("Select updateData from DataUpdate where dataClass=1 and connectionID=" + connectionID, null);
            try
            {
                if(!cur.moveToNext())
                    return  null;
                return createUpdateFromCursor(cur);

            }
            finally
            {
                cur.close();
            }

        }
    }

    @Nullable
    private DataUpdate createUpdateFromCursor(Cursor cur) throws IOException
    {
        byte[] billData=cur.getBlob(cur.getColumnIndex("updateData"));
        if(billData!=null && billData.length>0)
        {
            DataUpdate du = JSONUtils.getAdapter(DataUpdate.class).fromJson(new String(billData));
            for(DataUpdate.DataUpdateField f:du.updatedFields)
            {
                DataUpdate.FieldMetaData metaData=DataUpdate.FieldMetaData.getMetaDataForFieldName(f.fieldName);
                if(metaData!=null)
                {
                    if(metaData.type.getDataClass().equals(Integer.class) && (f.value instanceof Double))
                    {
                        Double dv=(Double)f.value;
                        f.value=dv.intValue();
                    }
                    else if(metaData.type instanceof DataFieldType.CoordinateType)
                    {
                        if(f.value==null)
                            f.value=null;
                        else
                        {
                            String json=JSONUtils.toJson(f.value);
                            f.value = JSONUtils.getAdapter(GEOLocation.class).fromJson(json);
                        }
                    }
                }
            }
            return du;
        }
        else
            return  null;
    }

    public ArrayList<DataUpdate>    getUnsyncedDataUpdate() throws IOException
    {
        synchronized (db)
        {
            ArrayList<DataUpdate> ret = new ArrayList<>();
            Cursor cur = db.rawQuery("Select * from DataUpdate where status=0", null);
            try
            {
                while (cur.moveToNext())
                {
                    ret.add(createUpdateFromCursor(cur));
                }
                return ret;
            }
            finally
            {
                cur.close();
            }
        }
    }
    public void saveProfileUpdateData(int connectionID,DataUpdate update) throws IOException
    {
        synchronized (db)
        {
            ContentValues cv=new ContentValues();
            DataUpdate upd=getProfileUpdateData(connectionID);
            cv.put("dataClass", (int) 1);
            cv.put("updateData", JSONUtils.toJson(update).getBytes());
            cv.put("status",0);
            if(upd==null)
            {
                cv.put("connectionID",connectionID);
                db.insert("DataUpdate",null,cv);
            }
            else
            {
                db.update("DataUpdate",cv,"connectionID="+connectionID,null);
            }
        }
    }
    @NonNull
    private ContentValues getContentValues(ReadingSheetRecord rec,boolean forUpdate)
    {
        ContentValues cv = new ContentValues();
        if(!forUpdate)
            cv.put("connectionID", rec.connectionID);
        cv.put("orderN", rec.orderN);
        cv.put("periodID", rec.periodID);
        cv.put("contractNo", rec.contractNo);
        cv.put("customerName", rec.customerName);
        cv.put("meterNo", rec.meterNo);

        cv.put("meterType", rec.meterType);
        cv.put("meterTypeID", rec.meterTypeID);
        cv.put("lat", rec.lat);
        cv.put("lng", rec.lng);
        cv.put("connectionStatus", rec.connectionStatus);
        cv.put("phoneNo", rec.phoneNo);
        cv.put("readerID", rec.readerID);
        cv.put("minAllowed", rec.minReadAllowed);

        cv.put("maxAllowed", rec.maxReadAllowed);
        cv.put("status", rec.status);
        cv.put("actualReaderID", rec.actualReaderID);
        cv.put("reading", rec.reading);
        cv.put("extraInfo", rec.extraInfo);
        cv.put("problem", rec.problem);
        cv.put("remark", rec.remark);
        cv.put("readTime", rec.readTime);
        cv.put("readingLat", rec.readingLat);
        cv.put("readingLng", rec.readingLng);
        cv.put("canPay", rec.canPay);
        cv.put("billText", rec.billText);
        cv.put("payStatus", rec.payStatus);
        cv.put("payTime", rec.payTime);
        cv.put("payConfirmTime", rec.payConfirmTime);
        cv.put("bill", JSONUtils.toJson(rec.bill).getBytes());
        cv.put("billTotal", rec.billTotal);
        cv.put("rowKey",rec.rowKey);
        cv.put("connectionType",rec.connectionType);
        cv.put("customerType",rec.customerType);
        return cv;
    }


    public static class ReadingSheetCursor extends ObjectCursor<ReadingSheetRecord>
    {

        public ReadingSheetCursor(Cursor c,int n)
        {
            super(c,n);
        }

        @Override
        protected ReadingSheetRecord createObject() throws IOException
        {
            return createReadingSheetRecord(cur);
        }

        public int getNRec()
        {
            return super.n;
        }
    }
    public ReadingSheetRecord[] searchByRadius(GEOLocation location,double rad,int limitTo) throws IOException
    {
        if(location==null || limitTo==0)
            return new ReadingSheetRecord[0];
        synchronized (db) {
            TreeSet<ReadingSheetRecord> ss = new TreeSet<>(new Comparator() {
                @Override
                public int compare(Object lhs, Object rhs) {
                    return Double.compare(((ReadingSheetRecord) lhs).distance, ((ReadingSheetRecord) rhs).distance);
                }
            });
            //Cursor cur = db.rawQuery("Select * from ReadingSheetRow", null);
            //while (cur.moveToNext())
            checkAndCacheCoordinates();
            for (Map.Entry<Integer, GEOLocation> loc : coordinateCache.entrySet()) {

                GEOLocation rec = loc.getValue();
                double distance = GEOLocation.sphericalDistance(location.lat, location.lng, rec.lat, rec.lng);
                if (distance <= rad) {

                    if (ss.size() == limitTo) {
                        ReadingSheetRecord last = ss.last();
                        if (ss.last().distance > distance) {
                            ss.remove(ss.last());
                        } else
                            continue;
                    }
                    ReadingSheetRecord rrec = getRecord(loc.getKey());
                    rrec.distance = distance;
                    ss.add(rrec);
                }
            }

            return ss.toArray(new ReadingSheetRecord[0]);
        }
    }
    public ReadingSheetCursor getReadingSheetCursor(int type,Object q) throws IOException
    {
        synchronized (db)
        {
            MRReadingSetting rs=getSetting();
            Cursor rscur;
            String cr;
            switch (type)
            {
                case ReadingSheetRecord.LIST_READING_SHEET:
                    cr=" where actualReaderID>0 or readerID="+rs.readerID +" order by readTime,orderN";
                    break;
                case ReadingSheetRecord.LIST_READING_CONTRACT_NO_SEARCH:
                    cr=" where contractNo='"+StringUtils.replace((String)q,"*","C") +"'";
                    break;
                case ReadingSheetRecord.LIST_READING_METER_NO_SEARCH:
                    cr=" where meterNo='"+q+"'";
                    break;
                case ReadingSheetRecord.LIST_READING_METER_NO_FUZZY_SEARCH:
                    cr=" where meterNo like '%"+q+"%' order by meterNo";
                    break;
                case ReadingSheetRecord.LIST_READING_NAME_SEARCH:
                    cr=" where customerName like '%"+q+"%' order by customerName";
                    break;
                default:
                    cr=" order by orderN";

                    break;
            }
            rscur = db.rawQuery("Select count(*) from ReadingSheetRow"+cr, null);
            int nrect;
            try
            {
                rscur.moveToNext();
                nrect=rscur.getInt(0);
            }
            finally
            {
                rscur.close();
            }
            rscur = db.rawQuery("Select * from ReadingSheetRow"+cr, null);
            return new ReadingSheetCursor(rscur,nrect);
        }
    }
    public ViewModel.WorkSummary getWorkSummary()
    {
        synchronized (db)
        {
            return this.sum.clone();
        }
    }
    public ViewModel.WorkSummary getWorkSummaryDB() throws IOException
    {

        synchronized (db)
        {
            MRReadingSetting rs=getSetting();
            ViewModel.WorkSummary summary=new ViewModel.WorkSummary();

            Cursor rscur=db.rawQuery("Select count(*), sum(case when status=" + ReadingSheetRecord.READ_STAT_READ + " then 1 else 0 end) as rd"
                    + ", sum(case when status=" + ReadingSheetRecord.READ_STAT_READ_UPLOADED + " then 1 else 0 end) as rdu"
                    + ", sum(case when payStatus=" + ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID + " then 1 else 0 end) as pd"
                    + ", sum(case when payStatus=" + ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID + " then billTotal else 0 end) as pd_a"
                    + ", sum(case when payStatus=" + ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED + " then 1 else 0 end) as pdu"
                    + ", sum(case when payStatus=" + ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED + " then billTotal else 0 end) as pdu_a"
                    + ", sum(case when readerID=" + (rs == null ? -1 : rs.readerID) + " then 1 else 0 end) as aloc"
                    + " from ReadingSheetRow", null);
            try
            {
                if(rscur.moveToNext())
                {
                    summary.setTotalConnections(rscur.getInt(0));
                    summary.setRead(rscur.getInt(1) + rscur.getInt(2));
                    summary.setUploaded(rscur.getInt(2));
                    summary.setNCollected(rscur.getInt(3) + rscur.getInt(5));
                    summary.setCollected(rscur.getDouble(4) + rscur.getDouble(6));
                    summary.setNCollectedConfirmed(rscur.getInt(5));
                    summary.setCollectedConfirmed(rscur.getDouble(6));
                    summary.setTotalAllocated(rscur.getInt(7));
                    summary.setServiceCharge(rs == null ? 0 : summary.getNCollected() * rs.commissionAmount);
                    summary.setSystemFee(rs==null?0:summary.getNCollected()*rs.systemShare);
                    summary.setUtilityShare(rs==null?0:summary.getNCollected()*rs.utilityShare);
                }
            }
            finally
            {
                rscur.close();
            }
            rscur=db.rawQuery("Select sum(utilityPayment),sum(systemPayment) from Payments",null);
            try
            {
                if(rscur.moveToNext())
                {
                    summary.setUtilityPaid(rscur.getDouble(0));
                    summary.setSystemFeePaid(rscur.getDouble(1));
                }
            }
            finally
            {
                rscur.close();
            }
            return summary;
        }
    }
    public ReadingSheetRecord[] searchReadingSheet(int searchField,String q) throws IOException
    {
        synchronized (db)
        {
            String cr;
            switch (searchField)
            {
                case ReadingSheetRecord.FIELD_CONTRACT_NO:
                    cr="contractNo='"+q+"'";
                    break;
                case ReadingSheetRecord.FIELD_METER_NO:
                    cr="meterNo='"+q+"'";
                    break;
                case ReadingSheetRecord.FIELD_NAME:
                    cr="customerName like '"+q+"%'";
                    break;
                    default:
                        return new ReadingSheetRecord[0];
            }
            ArrayList<ReadingSheetRecord> ret=new ArrayList<>();
            try(Cursor rs=db.rawQuery("Select * from ReadingSheetRow where "+cr+" LIMIT 100",null))
            {
                while (rs.moveToNext())
                {
                    ret.add(createReadingSheetRecord(rs));
                }
            }
            return ret.toArray(new ReadingSheetRecord[0]);
        }
    }
}
