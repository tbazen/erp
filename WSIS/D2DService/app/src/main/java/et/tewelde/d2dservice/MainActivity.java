package et.tewelde.d2dservice;

import android.Manifest;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import et.tewelde.d2dservice.ui.CustomerProfilePageController;
import et.tewelde.d2dservice.ui.DashboardController;
import et.tewelde.d2dservice.ui.DataEditorPageController;
import et.tewelde.d2dservice.ui.MRDialog;
import et.tewelde.d2dservice.ui.PageController;
import et.tewelde.d2dservice.ui.PaymentListPageController;
import et.tewelde.d2dservice.ui.PaymentPageController;
import et.tewelde.d2dservice.ui.ReadingEntryPageController;
import et.tewelde.d2dservice.ui.ReadingRecordAdapterBase;
import et.tewelde.d2dservice.ui.ReadingSheetController;
import et.tewelde.d2dservice.ui.SearchResultController;
import et.tewelde.d2dservice.ui.SettingPageController;
import et.tewelde.d2dservice.util.VelocityEvaluate;

public class MainActivity extends AppCompatActivity
{

    public void showReadingSheetPage()
    {
        pushView(R.layout.reading_sheet_list, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {
                return new ReadingSheetController(MainActivity.this, MainActivity.this.service, v);
            }
        });
    }

    public void showPaymentListPage()
    {
        pushView(R.layout.payment_list, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {
                return new PaymentListPageController(MainActivity.this, MainActivity.this.service, v);
            }
        });
    }

    public void showReadingEntryPage(final int conID)
    {
        pushView(R.layout.reading_entry, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {
                return new ReadingEntryPageController(MainActivity.this, MainActivity.this.service, v,conID);
            }
        });
    }

    public void showCustomerProfile(final int connectionID)
    {
        pushView(R.layout.customer_profile, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {

                return new CustomerProfilePageController(MainActivity.this, MainActivity.this.service, v, connectionID);

            }
        });
    }

    public void showDataEditor(final int connectionID)
    {
        pushView(R.layout.data_editor, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {

                return new DataEditorPageController(MainActivity.this, MainActivity.this.service, v, connectionID);

            }
        });
    }

    public void showConfirmationMessage(String msg, final View.OnClickListener onYes)
    {
        showConfirmationMessage(msg, onYes, null);
    }

    public void showPaymentPage(final int conID)
    {
        pushView(R.layout.payment, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {

                return new PaymentPageController(MainActivity.this, MainActivity.this.service, v, conID);

            }
        });
    }


    public void returnToHomePage()
    {
        returnToPage(R.layout.activity_main);
    }

    public void returnToPage(int page)
    {
        int index = -1;
        for (Map.Entry<Integer, MainActivity.ViewPage> kv : viewStack)
        {
            index++;
            if (kv.getKey() == page)
                break;
        }
        if (index == -1)
            throw new IllegalArgumentException("invalid page:" + page);

        if (viewStack.size() == 1)
        {
            return;
        }
        ViewPage vout = viewStack.get(0).getValue();


        ViewPage vin = viewStack.get(index).getValue();
        vin.controller.activate();
        vout.v.startAnimation(createAnimation(getInvAnim(vout.animationIn), vout.v));
        vin.v.startAnimation(createAnimation(getInvAnim(vout.animationOut), vout.v));
        setContentView(vin.v);
        for (int i = 0; i < index; i++)
        {
            vout = viewStack.get(0).getValue();
            vout.controller.deactivate();
            vout.controller.unload();
            viewStack.remove(0);
        }
    }

    public void showSearchResultPage(final MRRepository.ReadingSheetCursor ret)
    {
        pushView(R.layout.search_result, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {
                return new SearchResultController(MainActivity.this, MainActivity.this.service, v, ret);
            }
        });
    }
    public void showSearchResultPage(final ReadingRecordAdapterBase adapter)
    {
        pushView(R.layout.search_result, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {
                return new SearchResultController(MainActivity.this, MainActivity.this.service, v,adapter);
            }
        });
    }
    static interface ControllerCreator
    {
        public PageController createController(View v);
    }

    static class ViewPage
    {
        public View v;
        public int animationIn;
        public int animationOut;
        public PageController controller;

        public ViewPage(View view, int animationIn, int animationOut, PageController controller)
        {
            this.v = view;
            this.animationIn = animationIn;
            this.animationOut = animationOut;
            this.controller = controller;
        }
    }

    DashboardController dashBoard;
    MobiReadService service;
    View currentView = null;
    View mainPage = null;
    ArrayList<HashMap.Entry<Integer, ViewPage>> viewStack = new ArrayList<>();
    final static int ANIM_IN_LEFT = 1;
    final static int ANIM_OUT_RIGHT = 2;
    final static int ANIM_IN_RIGHT = 3;
    final static int ANIM_OUT_LEFT = 4;

    Animation createAnimation(int a, View v)
    {
        Animation ret = null;
        switch (a)
        {
            case ANIM_IN_LEFT:
                ret = new TranslateAnimation(Animation.RELATIVE_TO_SELF, -1, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
                break;
            case ANIM_OUT_RIGHT:
                ret = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
                break;
            case ANIM_IN_RIGHT:
                ret = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
                break;
            case ANIM_OUT_LEFT:
                ret = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -1, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
                break;
        }
        if (ret != null)
            ret.setDuration(200);
        return ret;
    }

    int getOutAnim(int a)
    {
        switch (a)
        {
            case ANIM_IN_LEFT:
                return ANIM_OUT_RIGHT;
            case ANIM_IN_RIGHT:
                return ANIM_OUT_LEFT;
        }
        return -1;
    }

    int getInvAnim(int a)
    {
        switch (a)
        {
            case ANIM_IN_LEFT:
                return ANIM_OUT_LEFT;
            case ANIM_IN_RIGHT:
                return ANIM_OUT_RIGHT;
            case ANIM_OUT_LEFT:
                return ANIM_IN_LEFT;
            case ANIM_OUT_RIGHT:
                return ANIM_IN_RIGHT;
        }
        return -1;
    }

    View pushView(int res, int animationIn, int animationOut, ControllerCreator creator)
    {
        if (viewStack.size() > 0)
        {
            ViewPage vout = viewStack.get(0).getValue();
            vout.controller.deactivate();
        }

        int i = 0;
        ViewPage v = null;
        for (HashMap.Entry<Integer, ViewPage> e : viewStack)
        {
            if (e.getKey() == res)
            {
                viewStack.remove(i);
                v = e.getValue();
                break;
            }
        }

        if (v == null)
        {
            View inflated = super.getLayoutInflater().inflate(res, null);
            v = new ViewPage(inflated, animationIn, animationOut, creator.createController(inflated));
        } else
        {
            v.controller.activate();
            v.animationIn = animationIn;
            v.animationOut = animationOut;
        }
        HashMap.Entry<Integer, ViewPage> e = new AbstractMap.SimpleEntry<Integer, ViewPage>(res, v);
        viewStack.add(0, e);
        if (viewStack.size() == 1 || animationIn == 0)
            setContentView(v.v);
        else
        {
            View out = viewStack.get(1).getValue().v;
            out.startAnimation(createAnimation(animationOut, v.v));
            v.v.startAnimation(createAnimation(animationIn, v.v));
            setContentView(v.v);
        }

        return v.v;
    }

    void initializeAlertDialog()
    {
        final AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
        final AlertDialog alertDialog = b.create();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        VelocityEvaluate.initialize(this);
        try
        {
            service=new MobiReadService(new MRRepository(this),new LocationWrapper(this));
            View v=pushView(R.layout.activity_main, 0, 0, new ControllerCreator()
            {
                @Override
                public PageController createController(View v)
                {
                    return new DashboardController(MainActivity.this,MainActivity.this.service,v);
                }
            });
            dashBoard=new DashboardController(this,service,v);
            service.start();

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.ACCESS_FINE_LOCATION}, 1);



        } catch (IOException e)
        {
            e.printStackTrace();
            showErrorMessage("The system couldn't initialize. Apologies for the inconvenience. Please report the problem\n"+e.getMessage());
        }

    }



    public void showSettingPage()
    {
        pushView(R.layout.setting_page, ANIM_IN_RIGHT, ANIM_OUT_LEFT, new ControllerCreator()
        {
            @Override
            public PageController createController(View v)
            {
                return new SettingPageController(MainActivity.this, MainActivity.this.service, v);
            }
        });
    }
        @Override
    public void onBackPressed()
    {
        if(viewStack.size()==1)
        {
            super.onBackPressed();
            return;
        }
        ViewPage vout= viewStack.get(0).getValue();
        vout.controller.unload();
        vout.controller.deactivate();

        ViewPage vin= viewStack.get(1).getValue();
        vin.controller.activate();
        vout.v.startAnimation(createAnimation(getInvAnim(vout.animationIn),vout.v));
        vin.v.startAnimation(createAnimation(getInvAnim(vout.animationOut), vout.v));
        setContentView(vin.v);
        viewStack.remove(0);
    }
    public void showErrorMessage(String message)
    {
        final MRDialog md=  new MRDialog();
        md.show(this, message, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                md.hide();
            }
        }, null, false, false);
    }


    public void showInformationMessage(String message)
    {
        new MRDialog ().show(this, message, null, null, false, false);
    }
    public void showConfirmationMessage(String msg,final View.OnClickListener onYes,final View.OnClickListener onNo)
    {
        final MRDialog  md=  new MRDialog ("Yes","No");
        md.show(this, msg, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(onYes!=null)
                    onYes.onClick(v);
                md.hide();
            }
        }, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(onNo!=null)
                    onNo.onClick(v);
                md.hide();
            }
        }, false, false);
    }
    public void showInputBox(String prompt, final Object tag,final MRDialog.InputDone done)
    {
        final MRDialog  md=  new MRDialog ();
        md.show(this, prompt, new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if (md.text == null || md.text.length() == 0)
                        {
                            return;
                        }
                        done.done(tag, md.text, false);
                        md.hide();
                    }
                },
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        done.done(tag, null, true);
                        md.hide();
                    }
                }, true, true);
    }

    @Override
    protected void onDestroy()
    {
        try
        {
            service.shotDown();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
            showErrorMessage("Error trying to shutdown synchronization loop.\n"+e.getMessage());
        }
        super.onDestroy();
    }
}
