package et.tewelde.d2dservice;

import android.app.Activity;
import android.telephony.SmsManager;
import android.util.Base64;

import java.io.IOException;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.List;

import et.tewelde.d2dservice.entity.ConnectionSetting;
import et.tewelde.d2dservice.entity.CustomField;
import et.tewelde.d2dservice.entity.DataUpdate;
import et.tewelde.d2dservice.entity.GEOLocation;
import et.tewelde.d2dservice.entity.MRReadingSetting;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.JSONUtils;
import et.tewelde.d2dservice.util.MRGlobals;

/**
 * Created by Teweldemedhin Aberra on 3/25/2017.
 */
public class MobiReadService
{
    <T> void doAsyncWithRet(final Activity context, final AsyncInterfaces.DelegateWithRet<T> method, final AsyncInterfaces.DoneWithReturn<T> done )
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    method.run();

                    if (context == null)
                        done.done(null,method.res);
                    else
                        context.runOnUiThread(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        done.done(null, method.res);
                                    }
                                });
                } catch (final Exception ex)
                {
                    if (context == null)
                        done.done(ex,null);
                    else
                        context.runOnUiThread(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        done.done(ex,null);
                                    }
                                });
                }
            }
        }).start();
    }
    void doAsyncVoidRet(final Activity context, final AsyncInterfaces.DelegateNoRet method, final AsyncInterfaces.DoneNoReturn done )
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    method.run();

                    if (context == null)
                        done.done(null);
                    else
                        context.runOnUiThread(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        done.done(null);
                                    }
                                });
                } catch (final Exception ex)
                {
                    if (context == null)
                        done.done(ex);
                    else
                        context.runOnUiThread(
                                new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        done.done(ex);
                                    }
                                });
                }
            }
        }).start();
    }
    public void getReadingSheetCursorAsync(final MainActivity context,final int type,final Object q, final AsyncInterfaces.DoneWithReturn<MRRepository.ReadingSheetCursor> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet<MRRepository.ReadingSheetCursor>()
        {
            @Override
            public void run() throws IOException
            {
                super.res = repository.getReadingSheetCursor(type, q);
            }
        }, done);
    }

    public void searchByRadiusAsync(final MainActivity context,final GEOLocation location,final double rad,final int limitTo, final AsyncInterfaces.DoneWithReturn<ReadingSheetRecord[]> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet<ReadingSheetRecord[]>()
        {
            @Override
            public void run() throws IOException
            {
                super.res = repository.searchByRadius(location, rad, limitTo);
            }
        }, done);
    }

    public void getPaymentListAsync(MainActivity activity, AsyncInterfaces.DoneWithReturn<MRRepository.ReadingSheetCursor> done)
    {
        doAsyncWithRet(activity, new AsyncInterfaces.DelegateWithRet<MRRepository.ReadingSheetCursor>()
        {
            @Override
            public void run()
            {
                super.res=repository.getPaymentListCursor();
            }
        },done);
    }

    public boolean locationAvialable()
    {
        return locWrapper.getLastKnownLocation()!=null;
    }

    public void recordReadingAsync(Activity context, final int connectionID, final double reading, final String remark, final int problem,AsyncInterfaces.DoneNoReturn done)
    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                repository.recordReading(connectionID,reading,remark,problem,locWrapper.getLastKnownLocation());
            }
        },done);
    }

    public GEOLocation getLastKnownLocation()
    {
        return locWrapper.getLastKnownLocation();
    }

    public void resendSMSAsync(Activity context, final int connectionID,AsyncInterfaces.DoneNoReturn done)
    {


    }

    public void clearReadingUploadFlagsAsync(Activity context, AsyncInterfaces.DoneNoReturn done)
    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                repository.clearReadingUploadFlags();

            }
        }, done);

    }

    public void executeQuerySync(Activity context,final String sql, AsyncInterfaces.DoneWithReturn<String> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet<String>()
        {
            @Override
            public void run() throws IOException
            {
                super.res=repository.executeQuery(sql);
            }
        }, done);

    }
    public void clearDataUpdateSyncFlag(Activity context, AsyncInterfaces.DoneNoReturn done)
    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                repository.clearDataUpdateSyncFlag();

            }
        }, done);

    }
    public void forceCurrentReadingCycleAync(Activity context, AsyncInterfaces.DoneNoReturn done)
    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                readingSheetSync.run();
            }
        }, done);

    }
    public void getReadingSettingViewAync(Activity context,AsyncInterfaces.DoneWithReturn<String> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet<String>()
        {
            @Override
            public void run() throws IOException
            {
                MRReadingSetting s=repository.getSetting();
                if(s==null)
                    super.res="Reading sheet setting not set.";
                else
                super.res=s.periodID+","+s.periodName+",\n"+s.sheetHash;
            }
        }, done);

    }

    public static enum SystemStatusType
    {
        Ok,
        Error,
        Waiting,
        Warning,
        Information,
    }

    public static interface SystemStatusChangeListener
    {
        public void logEvent(Object source, String event, SystemStatusType type,Object data);
        public void setStatus(Object source, String status, SystemStatusType type,Object data);
    }
    final ArrayList<SystemStatusChangeListener> systemStatusListeners =new ArrayList<>();

    MRRepository repository;
    ReadingSheetSync readingSheetSync;
    Runnable runWhenSyncShutsDown=null;
    public Object status_msg_source=null;
    public String status_msg_text=null;
    public SystemStatusType status_msg_type= SystemStatusType.Ok;

    LocationWrapper locWrapper;
    public MobiReadService(MRRepository repo,LocationWrapper loc)
    {
        this.locWrapper=loc;
        this.locWrapper.initializeLocation();
        this.repository=repo;
        this.readingSheetSync=new ReadingSheetSync(this.repository, new SystemStatusChangeListener()
        {
            @Override
            public void logEvent(Object source,String event, SystemStatusType type,Object data)
            {

            }

            @Override
            public void setStatus(Object source,String status, SystemStatusType type,Object data)
            {
                status_msg_source = source;
                status_msg_text = status;
                status_msg_type = type;
                if (runWhenSyncShutsDown != null && !readingSheetSync.isRunning())
                    runWhenSyncShutsDown.run();
                synchronized (systemStatusListeners)
                {
                    for (SystemStatusChangeListener l : systemStatusListeners)
                    {
                        l.setStatus(source, status, type,data);
                    }
                }
            }
        });

    }
    public void shotDown() throws InterruptedException
    {
        synchronized (this)
        {
            synchronized (readingSheetSync)
            {
                readingSheetSync.stopBlocking();
                repository.shutDown();
                locWrapper.releaseLocation();
                updateRunStatus();
            }
        }
    }
    public void start()
    {
        this.readingSheetSync.run();
    }
    void updateRunStatus()
    {
        synchronized (this)
        {
            boolean shutDown= !readingSheetSync.isRunning();
            systemStatusChange("System shutdown", SystemStatusType.Ok,null);
        }
    }

    public void addSystemStatusListener(SystemStatusChangeListener listener)
    {
        synchronized (systemStatusListeners)
        {
            if(systemStatusListeners.contains(listener))
                return;
            systemStatusListeners.add(listener);
        }

    }
    public void removeSystemStatusListener(SystemStatusChangeListener listener)
    {
        synchronized (systemStatusListeners)
        {
            if(systemStatusListeners.contains(listener))
                return;
            systemStatusListeners.remove(listener);
        }
    }
    void systemStatusChange(String msg,SystemStatusType type,Object data)
    {
        synchronized (systemStatusListeners)
        {
            for (SystemStatusChangeListener l : systemStatusListeners)
            {
                l.setStatus(this, msg,type,data);
            }
        }
    }
    public void clearReadingAsync(Activity context, final int connectionID,AsyncInterfaces.DoneNoReturn done)
    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                repository.clearReading(connectionID);
            }
        },done);
    }
    public List<CustomField> getSearchableCustomFields() throws IOException
    {
        List<CustomField> ret=new ArrayList<>();
        MRReadingSetting rs=repository.getSetting();
        if(rs==null)
            return ret;
        for(CustomField cf:rs.customFields)
            ret.add(cf);
        return ret;
    }
    public void getSearchableCustomFieldsAsync(Activity context,AsyncInterfaces.DoneWithReturn<List<CustomField>> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet()
        {
            @Override
            public void run() throws IOException
            {
                super.res = getSearchableCustomFields();
            }
        }, done);
    }
    public void setConnectionParameterAsync(final ConnectionSetting setting,Runnable then)
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    setConnectionParameter(setting);
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void setConnectionParameter(ConnectionSetting setting) throws IOException
    {
        synchronized (this.repository.db)
        {
            this.repository.setSettingObject("conSetting",setting);
            synchronized (this.readingSheetSync)
            {
                if (this.readingSheetSync.isRunning())
                {
                    this.readingSheetSync.stop();

                    this.runWhenSyncShutsDown=new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            readingSheetSync.run();
                        }
                    };
                }
                else
                    this.readingSheetSync.run();
            }
        }
    }
    public ConnectionSetting getConnectionParameter() throws IOException
    {
        return this.repository.getSettingObject("conSetting", ConnectionSetting.class);
    }

    public void getWorkSummaryAsync(Activity context,AsyncInterfaces.DoneWithReturn<ViewModel.WorkSummary> done )
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet()
        {
            @Override
            public void run()
            {
                super.res = repository.getWorkSummary();
            }
        }, done);
    }
    public void getReadingSheetRecordAsync(Activity context,final int connectionID,AsyncInterfaces.DoneWithReturn<ReadingSheetRecord> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet()
        {
            @Override
            public void run() throws IOException
            {
                super.res = repository.getRecord(connectionID);
            }
        }, done);
    }

    public void getReaderSettingAsync(Activity context,final int connectionID,AsyncInterfaces.DoneWithReturn<MRReadingSetting> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet()
        {
            @Override
            public void run() throws IOException
            {
                super.res = repository.getSetting();
            }
        }, done);
    }
    public  void searchReadingSheetAsync(Activity context,final int searchField,final String q,AsyncInterfaces.DoneWithReturn<ReadingSheetRecord[]> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet()
        {
            @Override
            public void run() throws IOException
            {
                super.res = repository.searchReadingSheet(searchField, q);
            }
        }, done);
    }

    public void getProfileUpdateDataAsync(Activity context,final int connectionID,AsyncInterfaces.DoneWithReturn<DataUpdate> done)
    {
        doAsyncWithRet(context, new AsyncInterfaces.DelegateWithRet()
        {
            @Override
            public void run() throws IOException
            {
                super.res = repository.getProfileUpdateData(connectionID);
            }
        }, done);
    }

    public String[] getProblemTypes()
    {
        return new String[]{"No Reading Problem", "Nobody Home", "Owners not Cooperative", "Meter Defective", "Meter Inaccessible", "Other"};
    }
    public int getProblemIDByIndex(int problem)
    {
        return  problem;
    }
    public void collectPaymentAsync(Activity context,final int conID,AsyncInterfaces.DoneNoReturn done)

    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                ReadingSheetRecord rec= repository.collectPayment(conID);
                try
                {
                    MobiReadService.this.systemStatusChange("Payment made", SystemStatusType.Information,null);
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                sendPaymentSMS(rec,false);
            }
        }, done);
    }

    private void sendPaymentSMS(ReadingSheetRecord rec,boolean re_send) throws Exception
    {
        try
        {
            if (rec.validPhoneNo())
            {
                String msg = "A total amount of " + MRGlobals.moneyFormat.format(rec.getTotalWithCommission()) + " is received.\nCode: " + rec.rowKey
                +"\nContractNo: " + rec.contractNo;
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(rec.normalizePhoneNo(), null, msg, null, null);
                String msg2=(re_send?"RE:HWSSA":"HWSSA")+"|"+rec.rowKey+"|"+rec.contractNo+"|"+repository.getSetting().readerID+"|"+rec.getTotalBill();

                msg= Base64.encodeToString(msg2.getBytes(), Base64.DEFAULT);
                smsManager.sendTextMessage("0983042943", null, msg2, null, null);
            }
        }catch (Exception ex)
        {
            throw new Exception("Payment settlement is registered but sms receipt was not sent.\nPlease inform the customer.\nError:"+ex.getMessage());
        }
    }

    public void saveProfileUpdateDataAsync(Activity context,final int conID,final DataUpdate update, AsyncInterfaces.DoneNoReturn done)
    {
        doAsyncVoidRet(context, new AsyncInterfaces.DelegateNoRet()
        {
            @Override
            public void run() throws Exception
            {
                repository.saveProfileUpdateData(conID, update);
            }
        }, done);
    }

}
