package et.tewelde.d2dservice;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import org.apache.commons.lang.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import et.tewelde.d2dservice.entity.ConnectionSetting;
import et.tewelde.d2dservice.entity.DataFieldType;
import et.tewelde.d2dservice.entity.DataUpdate;
import et.tewelde.d2dservice.entity.MRReadingSetting;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.JSONUtils;
import et.tewelde.d2dservice.util.RESTAPIClient;
import et.tewelde.d2dservice.util.WSISClient;

/**
 * Created by Teweldemedhin Aberra on 3/27/2017.
 */
public class ReadingSheetSync
{

    class MobileChangeLogRet extends JSONUtils.JSONRet<MobileChangeLog>{}

    private static final int CONNECTION_RETRY = 2000;
    private static final int CONNECTION_RETRY_SLEEP = 10000;
    private static final int N_CONNECTION_RETRY_BEFORE_SLEEP = 10;
    private static final int N_RECS_PER_SEVER_CALL = 10;
    private static final int NO_CHANGE_SLEEP = 10000;
    private static final int N_SYN_PER_SEVER_CALL = 100;
    MRRepository repo;
    ConnectionSetting conSetting=null;
    MobiReadService.SystemStatusChangeListener listener;
    MainActivity activity;
    public ReadingSheetSync(MRRepository repo, MobiReadService.SystemStatusChangeListener listener)
    {
        this.repo=repo;
        this.listener=listener;
        this.activity=activity;
    }

    void changeStatus(String status,MobiReadService.SystemStatusType statusType,Object data)
    {
        if(this.listener==null)
            return;
        listener.setStatus(this,status,statusType,data);
    }
    private RESTAPIClient.ConnectionStatus connectionStatus1=RESTAPIClient.ConnectionStatus.DISCONNECTED;
    private RESTAPIClient.ConnectionStatus connectionStatus2=RESTAPIClient.ConnectionStatus.DISCONNECTED;

    public boolean isRunning()
    {
        return runSync;
    }

    public void stop()
    {
        stopRequested =true;

    }
    public void stopBlocking() throws InterruptedException
    {
        stopRequested =true;
        if(syncLoop!=null && syncLoop.isAlive())
        {
            syncLoop.interrupt();
            syncLoop.join();
        }

    }

    public static class MobileChangeLog
    {
        public class Change
        {
            public int connectionID=-1;
            public boolean billModified = false;
            public int[] paymentConfirmed = null;
            public long paymentTime=-1;
            public double utilityPayment=0;
            public double systemPayment=0;
        }
        public int from;
        public int upto;
        public Change[] changes;

    }
    private boolean runSync=false;
    private boolean stopRequested =false;
    Thread syncLoop=null;
    public void run()
    {
        synchronized (this)
        {
            if(runSync)
                throw new IllegalStateException("Sync already running");
        }

        runSync=true;
        stopRequested =false;
        changeStatus("Reading sync: starting", MobiReadService.SystemStatusType.Ok,null);
        connectionStatus1=RESTAPIClient.ConnectionStatus.DISCONNECTED;
        connectionStatus2=RESTAPIClient.ConnectionStatus.DISCONNECTED;
        try
        {
            this.conSetting = repo.getSettingObject("conSetting", ConnectionSetting.class);
            if(this.conSetting==null)
            {
                changeStatus("Reading sync: connection setting not set", MobiReadService.SystemStatusType.Error,null);
                runSync=false;
                return;
            }
        }
        catch (Exception ex)
        {
            changeStatus("Reading sync: couldn't load settings\n"+ex.getMessage(), MobiReadService.SystemStatusType.Error,null);
            runSync=false;
            return;
        }
        syncLoop=new Thread(new Runnable()
        {
            @Override
            public void run()
            {

                try
                {
                    //connect
                    int nConRetry = N_CONNECTION_RETRY_BEFORE_SLEEP;
                    while (!stopRequested)
                    {
                        WSISClient con=null;
                        //connection loop
                        while (!stopRequested)
                        {
                            con = new WSISClient(conSetting.wsisURL1);
                            try
                            {
                                changeStatus("Reading sync: Trying to connect with main url", MobiReadService.SystemStatusType.Ok,null);
                                con.login(conSetting.wsisUserName, conSetting.wsisPassword);
                                connectionStatus1 = RESTAPIClient.ConnectionStatus.CONNECTED;
                                changeStatus("Reading sync: Connected with main url", MobiReadService.SystemStatusType.Ok,null);
                            } catch (Exception ex)
                            {
                                con=null;
                                connectionStatus1 = RESTAPIClient.ConnectionStatus.ERROR;
                                changeStatus("Error\n" + ex.getMessage(), MobiReadService.SystemStatusType.Error,null);
                            }


                            if (stopRequested) break;
                            if (connectionStatus1 != RESTAPIClient.ConnectionStatus.CONNECTED && !StringUtils.isEmpty(conSetting.wsisURL2))
                            {
                                con = new WSISClient(conSetting.wsisURL2);

                                try
                                {
                                    changeStatus("Reading sync: Trying to connect with second url", MobiReadService.SystemStatusType.Ok,null);
                                    con.login(conSetting.wsisUserName, conSetting.wsisPassword);
                                    connectionStatus2 = RESTAPIClient.ConnectionStatus.CONNECTED;
                                    changeStatus("Reading sync: Connected with second url", MobiReadService.SystemStatusType.Ok,null);
                                } catch (Exception ex)
                                {
                                    con=null;
                                    connectionStatus2 = RESTAPIClient.ConnectionStatus.ERROR;
                                    changeStatus("Error\n" + ex.getMessage(), MobiReadService.SystemStatusType.Error,null);

                                }
                                if (stopRequested) break;
                            }

                            if (connectionStatus1 != RESTAPIClient.ConnectionStatus.CONNECTED && connectionStatus2 != RESTAPIClient.ConnectionStatus.CONNECTED)
                            {
                                Thread.sleep(CONNECTION_RETRY);
                                if (stopRequested) break;

                                nConRetry--;

                                if (nConRetry == 0)
                                {
                                    nConRetry = N_CONNECTION_RETRY_BEFORE_SLEEP;
                                    changeStatus("Reading sync: sleeping", MobiReadService.SystemStatusType.Error,null);
                                    Thread.sleep(CONNECTION_RETRY_SLEEP);
                                }
                                if (stopRequested) break;
                                continue;
                            }
                            break;
                        }

                        boolean dataChanged=false;
                        try
                        {
                            MRReadingSetting existingSetting=repo.getSettingObject("readingSetting", MRReadingSetting.class);
                            if(existingSetting!=null)
                            {
                                dataChanged |= syncChanges(con);
                            }

                            changeStatus("Reading sync: receiving setting info.", MobiReadService.SystemStatusType.Ok,null);

                            JSONUtils.StringRet settingHash=con.invokeSessionAPI("subsc/metermap/api/getReadingSettingHashCode", null, JSONUtils.StringRet.class);
                            if(settingHash.error!=null) throw new IllegalArgumentException(settingHash.error);

                            if(existingSetting==null || !settingHash.res.equals(existingSetting.sheetHash))
                            {
                                dataChanged |= downloadReadingSheet(con);
                            }
                            if(existingSetting!=null)
                            {
                                dataChanged |= uploadReadingAndPayment(con);
                                dataChanged |= uploadDataChanges(con);
                            }
                            if(con!=null)
                            {
                                try
                                {
                                    con.logout();
                                }
                                catch (Exception ex)
                                {
                                    changeStatus("Reading sync: trying to logout from wsis.\n"+ex.getMessage(), MobiReadService.SystemStatusType.Error,null);
                                }
                            }

                            if(!dataChanged)
                            {
                                changeStatus("Reading sync: everything is synced. Sleeping", MobiReadService.SystemStatusType.Ok,null);
                                Thread.sleep(NO_CHANGE_SLEEP);
                            }

                        } catch (Exception e)
                        {
                            e.printStackTrace();
                            dataChanged=true;
                            String msg;
                            StringWriter sw=new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            changeStatus(String.format("Error syncing data with the server.\n" + e.getMessage() + "\n"
                                    + sw.toString()), MobiReadService.SystemStatusType.Error, null);
                            Thread.sleep(NO_CHANGE_SLEEP);
                        }
                    }
                    runSync=false;
                    stopRequested =false;
                    changeStatus("Reading sync: successfully shutdown.", MobiReadService.SystemStatusType.Ok,null);
                }
                catch (InterruptedException ex)
                {
                    runSync=false;
                    stopRequested =false;
                    changeStatus("Reading sync: interrupted. "+ex.getMessage(), MobiReadService.SystemStatusType.Error,null);
                }
            }
        });
        syncLoop.start();
    }

    private boolean uploadReadingAndPayment(WSISClient con) throws Exception
    {
        boolean dataChanged=false;
        List<ReadingSheetRecord> cur= repo.getTransferList();
        if(cur.size()>0)
        {
            synchronized (repo.db)
            {
                for(ReadingSheetRecord rec:cur)
                {
                    repo.beginTransaction();
                    try
                    {
                        if(rec.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID)
                        {
                            JSONUtils.IntegerRet ret=con.invokeSessionAPI("subsc/metermap/api/paybill?conID="+rec.connectionID+"&date="+rec.payTime,
                                    rec.bill, JSONUtils.IntegerRet.class);
                            if(ret.error!=null)
                                throw new Exception(ret.error);
                            dataChanged=true;
                            repo.confirmPayment(rec.connectionID);
                        }
                        if(rec.status==ReadingSheetRecord.READ_STAT_READ)
                        {
                            JSONUtils.IntegerRet ret=con.invokeSessionAPI("subsc/metermap/api/setReading2",
                                    rec, JSONUtils.IntegerRet.class);
                            if(ret.error!=null)
                            {
                                //throw new Exception(ret.error);
                                continue;
                            }
                            repo.confirmReading(rec.connectionID);
                            dataChanged=true;
                        }
                        changeStatus("Some local data changed",  MobiReadService.SystemStatusType.Information,new SyncEvent());
                        repo.commitTransaction();
                    } catch (Exception ex)
                    {
                        repo.rollbackTransaction();
                        throw ex;
                    }
                }
            }
        }
        return dataChanged;
    }


    private boolean uploadDataChanges(WSISClient con) throws Exception
    {
        List<DataUpdate> cur = repo.getUnsyncedDataUpdate();
        if (cur.size() > 0)
        {
            synchronized (repo.db)
            {
                boolean dataChanged = false;
                int prog=0;
                for (DataUpdate rec : cur)
                {
                    repo.beginTransaction();
                    try
                    {
                        JSONUtils.VoidRet ret = con.invokeSessionAPI("subsc/metermap/api/save_data?conID=" + rec.connectionID + "&data_class=1",
                                rec, JSONUtils.VoidRet.class);
                        if (ret.error != null)
                            throw new Exception(ret.error);
                        dataChanged = true;
                        repo.setProfileDataUploaded(rec.connectionID);
                        prog++;
                        changeStatus("Uploaded data upadate " + (prog) + "/" + (cur.size()), MobiReadService.SystemStatusType.Information, new SyncEvent());
                        repo.commitTransaction();
                    } catch (Exception ex)
                    {
                        repo.rollbackTransaction();
                        throw ex;
                    }
                }
                return dataChanged;
            }
        }
        return false;
    }
    private boolean downloadReadingSheet(WSISClient con) throws Exception
    {
        ViewModel.WorkSummary ws=repo.getWorkSummary();
        if(ws.getUploaded()<ws.getRead())
            throw new IllegalStateException("Can't download readingsheet while There are still readings not uploaded.");

        boolean dataChanged;
        JSONUtils.MRReadingSettingRet serverSetting=con.invokeSessionAPI("subsc/metermap/api/getReadingSetting", null, JSONUtils.MRReadingSettingRet.class);
        if(serverSetting.error!=null) throw new IllegalArgumentException(serverSetting.error);

        dataChanged=false;
        //new setting
        changeStatus("Reading sync: setting changed on server. Download new readingsheet.", MobiReadService.SystemStatusType.Ok,new SyncEvent());
        synchronized (repo.db)
        {
            try
            {
                repo.beginTransaction();
                repo.setSettingObject("readingSetting", serverSetting.res);
                DataFieldType.initType(serverSetting.res);
                repo.clearReadingSheetRecord();
                repo.clearDataUpdate();
                repo.clearOutgingPaymentRecord();
                JSONUtils.IntegerRet intRet = con.invokeSessionAPI("subsc/metermap/api/getReadingSheetSize?periodID=" + serverSetting.res.periodID, null, JSONUtils.IntegerRet.class);
                if (intRet.error != null)
                    throw new IllegalArgumentException(intRet.error);
                int nRec = intRet.res;

                for (int i = 0; i < nRec; i += N_RECS_PER_SEVER_CALL)
                {
                    dataChanged=true;
                    JSONUtils.ReadingSheetRecordRet recRet = con.invokeSessionAPI("subsc/metermap/api/getReadingSheetRows2?periodID=" + serverSetting.res.periodID
                            + "&index=" + i
                            + "&n=" + Math.min(N_RECS_PER_SEVER_CALL, nRec - i), null, JSONUtils.ReadingSheetRecordRet.class);
                    ReadingSheetRecord[] recs = recRet.res;
                    if (recRet.error != null)
                        throw new IllegalArgumentException(recRet.error);
                    for (ReadingSheetRecord rec : recs)
                    {
                        repo.saveRecord(rec);
                    }
                    changeStatus("Downloading reading sheet " + i + "/" + (nRec), MobiReadService.SystemStatusType.Information,null);
                }

                repo.setIntSetting("syncN", serverSetting.res.syncedTo);
                changeStatus(nRec + " records saved",  MobiReadService.SystemStatusType.Information,new SyncEvent());
                repo.commitTransaction();
            } catch (Exception ex)
            {
                repo.rollbackTransaction();
                throw ex;
            }
        }
        return dataChanged;
    }

    private boolean syncChanges(WSISClient con) throws Exception
    {
        boolean dataChanged=false;
        changeStatus("Reading sync: checking changes.", MobiReadService.SystemStatusType.Ok,null);
        int syncN = repo.getIntSetting("syncN", -1);
        JSONUtils.IntegerRet serverSyncN=con.invokeSessionAPI("subsc/metermap/api/getSyncN", null, JSONUtils.IntegerRet.class);
        if(serverSyncN.error!=null) throw new IllegalArgumentException(serverSyncN.error);

        for(int n=syncN;n<serverSyncN.res;n+=N_SYN_PER_SEVER_CALL)
        {
            MobileChangeLogRet changes = con.invokeSessionAPI("subsc/metermap/api/getSyncData?from=" + (n + 1)
                    + "&upto=" + Math.min(n + N_SYN_PER_SEVER_CALL, serverSyncN.res)
                    , null, MobileChangeLogRet.class);
            if (changes.error != null)
                throw new IllegalArgumentException(changes.error);

            synchronized (repo.db)
            {
                try
                {
                    repo.beginTransaction();
                    repo.setIntSetting("syncN", changes.res.upto);
                    for (MobileChangeLog.Change c : changes.res.changes)
                    {
                        dataChanged = true;
                        if(c.billModified)
                            repo.invalidateBill(c.connectionID);
                        if(c.paymentConfirmed!=null && c.paymentConfirmed.length>0)
                        {
                            repo.confirmPayment(c.paymentConfirmed);
                        }
                        if(c.utilityPayment>0 || c.systemPayment>0)
                        {
                            repo.recordOutgoingPayment(c);
                        }
                    }
                    repo.commitTransaction();
                } catch (Exception ex)
                {
                    repo.rollbackTransaction();
                    throw ex;
                }
            }
            changeStatus("Syncing " + (n - syncN) + "/" + (serverSyncN.res-syncN), MobiReadService.SystemStatusType.Information,new SyncEvent());
        }
        return dataChanged;
    }

    public RESTAPIClient.ConnectionStatus getConnectionStatus1()
    {
        return connectionStatus1;
    }

    public RESTAPIClient.ConnectionStatus getConnectionStatus2()
    {
        return connectionStatus2;
    }

}
