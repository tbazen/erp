package et.tewelde.d2dservice.entity;

public class ConnectionSetting
{
    public String wsisURL1;
    public String wsisURL2;
    public String wsisUserName;
    public String wsisPassword;
    public String paymentRouterURL;
    public String paymentRouterKey;
}
