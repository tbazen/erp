package et.tewelde.d2dservice.entity;

/**
 * Created by Teweldemedhin Aberra on 3/25/2017.
 */
public class CustomField
{
    public String description;
    public String fieldName;
    public boolean searchable=true;
    public CustomField(String name, String desc, boolean searchable)
    {
        this.description=desc;
        this.fieldName=name;
        this.searchable=searchable;
    }

}
