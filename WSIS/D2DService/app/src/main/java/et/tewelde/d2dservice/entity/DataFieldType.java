package et.tewelde.d2dservice.entity;

import org.apache.commons.collections.FastHashMap;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;

import et.tewelde.d2dservice.util.JSONUtils;

/**
 * Created by Teweldemedhin Aberra on 4/7/2017.
 */
public abstract class DataFieldType
{


    private static Arrays dataClass;

    public abstract Class getDataClass();

    static class ParseResult<T>
    {
        public boolean success=false;
        public T val;
        public ParseResult()
        {
            success=false;
        }
        public ParseResult(T v)
        {
            success=true;
            this.val=v;
        }
    }


    public abstract Object parse(Object str);
    public abstract boolean isValid(Object str);
    public Object dataToString(Object data)
    {
        if(data==null)
            return "";
        return data.toString();
    }
    public static class FloatType extends DataFieldType
    {
        static Pattern p=Pattern.compile("^-?\\d*(\\.\\d+)?$");
        public boolean limitMin;
        public double  min;
        public boolean limitMax;
        public double  max;

        public FloatType(boolean limitMin,double  min,boolean limitMax,double max )
        {

            this.limitMin=limitMin;
            this.limitMax=limitMax;
            this.min=min;
            this.max=max;
        }
        public FloatType()
        {
            this(false,0,false,0);
        }
        public FloatType(boolean limitLower,double limit)
        {
            this(limitLower,limit,!limitLower,limit);
        }

        @Override
        public Class getDataClass()
        {
            return Double.class;
        }

        @Override
        public Object parse(Object str)
        {
            ParseResult<Double> res= parseVal((String) str);
            if(!res.success)
                throw new IllegalArgumentException("Not a valid value: "+str);
            return res.val;
        }
        public ParseResult<Double> parseVal(String str)
        {
            if(str==null||str.length()==0)
                return new ParseResult<>();
            if (!p.matcher(str).matches())
                return new ParseResult<>();
            double val = Double.parseDouble(str);
            if (limitMin && val < min)
                return new ParseResult<>();
            if (limitMax && val > max)
                return new ParseResult<>();
            return new ParseResult<>(val);
        }
        @Override
        public boolean isValid(Object str)
        {
            return parseVal((String) str).success;
        }

    }
    public static class IntType extends DataFieldType
    {
        static Pattern p=Pattern.compile("^(\\+|-)?\\d+$\n");
        public boolean limitMin;
        public int min;
        public boolean limitMax;
        public int max;

        public IntType(boolean limitMin,int min,boolean limitMax,int max )
        {

            this.limitMin=limitMin;
            this.limitMax=limitMax;
            this.min=min;
            this.max=max;
        }
        public IntType()
        {
            this(false,0,false,0);
        }
        public IntType(boolean limitLower,int limit)
        {
            this(limitLower,limit,!limitLower,limit);
        }
        @Override
        public Object parse(Object str)
        {
            ParseResult<Integer> res=parseInt((String) str);
            if(!res.success)
                throw new IllegalArgumentException("Not a valid value: "+str);
            return res.val;
        }
        public ParseResult<Integer> parseInt(String str)
        {
            if (!p.matcher(str).matches())
                return new ParseResult<>();
            int val = Integer.parseInt(str);
            if (limitMin && val < min)
                return new ParseResult<>();
            if (limitMax && val > max)
                return new ParseResult<>();
            return new ParseResult<>(val);
        }
        @Override
        public boolean isValid(Object str)
        {
            return parseInt((String) str).success;
        }
        @Override
        public Class getDataClass()
        {
            return Integer.class;
        }

    }

    public static class TextType extends DataFieldType
    {
        Pattern p;
        public TextType(String pattern)
        {
            if(pattern!=null && pattern.length()>0)
                p=Pattern.compile(pattern);
            else
                p=null;
        }
        public TextType()
        {
            this(null);
        }
        @Override
        public Object parse(Object str)
        {
            ParseResult<String> res= parseStr((String) str);
            if(!res.success)
                throw new IllegalArgumentException("Not a valid value: "+str);
            return res.val;
        }
        public ParseResult<String> parseStr(String str)
        {
            if(p==null)
                return  new ParseResult<String>(str);
            if(p.matcher(str).matches())
                return  new ParseResult<String>(str);
            return  new ParseResult<String>();
        }
        @Override
        public boolean isValid(Object str)
        {
            return parseStr((String) str).success;
        }

        @Override
        public Class getDataClass()
        {
            return String.class;
        }

    }
   public abstract static class LookUpType<ValType> extends DataFieldType
   {
       Map.Entry<ValType,String>[] entries;
       public LookUpType()
       {
           entries=null;
       }
       public LookUpType(Map.Entry<ValType,String>[] entries)
       {
           this.entries=entries;
       }
       public LookUpType(ValType[] keys,String[] values)
       {
           this.entries=new Map.Entry[keys.length];
           for(int i=0;i<keys.length;i++)
               this.entries[i]=new AbstractMap.SimpleEntry<ValType, String>(keys[i],values[i]);
       }
       public int indexOfValue(ValType value)
       {
           for(int i=0;i<entries.length;i++)
               if(entries[i].getKey().equals(value))
                   return i;
           return -1;
       }

       public String getDescriptionForValue(ValType val)
       {
           return getDescriptionForValue(val,false);
       }
       public String getDescriptionForValue(ValType val,boolean throwExceptionForInvalid)
       {
           for (Map.Entry<ValType, String> e : entries)
           {
               if (e.getKey().equals(val))
                   return e.getValue();
           }
           if (throwExceptionForInvalid)
               throw new IllegalArgumentException("Invalid value for enumeration: " + val);
           return "";
       }

       @Override
       public Object parse(Object str)
       {
           if(str instanceof Integer)
           {
               int index=(Integer)str;
               if(index<0 || index>=entries.length)
                   throw new IllegalArgumentException("Out of bound enumeration index");
               return entries[index].getKey();
           }
           throw new IllegalArgumentException("Invalid enumeration index");
       }

       @Override
       public boolean isValid(Object str)
       {
           if(str instanceof Integer)
           {
               int index=(Integer)str;
               if(index<0 || index>=entries.length)
                   return false;
               return true;
           }
           return false;
       }

       public Map.Entry<ValType, String>[] getEnumeration()
       {
           return entries;
       }



   }
    public static class WSISLookupType extends LookUpType<Integer>
    {
        public WSISLookupType(MRReadingSetting.WSISEnumerationItem[] items)
        {

            entries=new Map.Entry[items==null?0:items.length];
            for(int i=0;i<entries.length;i++)
                entries[i]=new AbstractMap.SimpleEntry<Integer, String>(items[i].value,items[i].description);
        }
        @Override
        public Class getDataClass()
        {
            return Integer.class;
        }
    }
    public static class WSISLookupStringValueType extends LookUpType<String>
    {
        public WSISLookupStringValueType (MRReadingSetting.WSISEnumerationItemStringValue[] items)
        {
            entries=new Map.Entry[items.length];
            for(int i=0;i<items.length;i++)
                entries[i]=new AbstractMap.SimpleEntry<String, String>(items[i].value,items[i].description);
        }
        @Override
        public Class getDataClass()
        {
            return String.class;
        }

    }
    public static class CoordinateType extends DataFieldType
    {
        static Pattern pattern=Pattern.compile("^([-+]?\\d{1,2}([.]\\d+)?),\\s*([-+]?\\d{1,3}([.]\\d+)?)$");
        public Class getDataClass()
        {
            return GEOLocation.class;
        }
        public ParseResult<GEOLocation> parseCoordinate(String str)
        {
            if (!pattern.matcher(str).matches())
                return new ParseResult<>();
            String[] parts=str.split(",");
            return new ParseResult<>(new GEOLocation(Double.parseDouble(parts[0]),Double.parseDouble(parts[1])));

        }

        @Override
        public Object parse(Object str)
        {
            ParseResult<GEOLocation> res= parseCoordinate((String) str);
            if(!res.success)
                throw new IllegalArgumentException("Not a valid value: "+str);
            return res.val;
        }

        @Override
        public boolean isValid(Object str)
        {
            return parseCoordinate((String) str).success;
        }
    }
    public static IntType Type_PositiveInteger=new IntType(true,1,false,0);
    public static IntType Type_Integer=new IntType();
    public static FloatType Type_Money=new FloatType(true,1,false,0);
    public static TextType Type_Text=new TextType(null);
    public static TextType Type_PhoneNo=new TextType(null);
    public static LookUpType<String> Type_MeterType;
    public static LookUpType<Integer> Type_CustomerType;
    public static LookUpType<Integer> Type_ConnectionType;
    public static LookUpType<Integer> Type_ConnectionStatus;
    public static CoordinateType Type_Coordinate;
    public static void initType(MRReadingSetting stg)
    {
        Type_MeterType=new WSISLookupStringValueType(stg.meterTypes);
        Type_CustomerType=new WSISLookupType(stg.customerTypes);
        Type_ConnectionType=new WSISLookupType(stg.connectionTypes);
        Type_ConnectionStatus=new WSISLookupType(stg.connectionStatusTypes);
        Type_Coordinate=new CoordinateType();
    }

}

