package et.tewelde.d2dservice.entity;

import java.util.Objects;

public class DataUpdate
{
    public static class FieldMetaData
    {
        public String fieldDescription;
        public DataFieldType type;

        public static FieldMetaData customerName=new FieldMetaData("Customer Name",DataFieldType.Type_Text);
        public static FieldMetaData phoneNo=new FieldMetaData("Phone No.",DataFieldType.Type_PhoneNo);
        public static FieldMetaData meterNo=new FieldMetaData("Meter No.",DataFieldType.Type_Text);
        public static FieldMetaData meterTypeID=new FieldMetaData("Meter Type", DataFieldType.Type_MeterType);
        public static FieldMetaData customerType=new FieldMetaData("Customer Type", DataFieldType.Type_CustomerType);
        public static FieldMetaData connectionType=new FieldMetaData("Connection Type",DataFieldType.Type_ConnectionType);
        public static FieldMetaData connectionStatus=new FieldMetaData("Connection Status",DataFieldType.Type_ConnectionStatus);
        public static FieldMetaData meterCoordinate=new FieldMetaData("Location",DataFieldType.Type_Coordinate);

        public FieldMetaData(String desc, DataFieldType type )
        {
            this.fieldDescription=desc;
            this.type=type;
        }
        public static FieldMetaData getMetaDataForFieldName(String fieldName)
        {
            switch (fieldName)
            {
                case "customerName":return customerName;
                case "phoneNo":return phoneNo;
                case "meterNo":return meterNo;
                case "meterTypeID":return meterTypeID;
                case "customerType":return customerType;
                case "connectionType":return connectionType;
                case "connectionStatus":return connectionStatus;
                case "coordinate":return meterCoordinate;
            }
            return null;
        }
    }

    public static class DataUpdateField
    {
        public String fieldName;
        public Object value;
        public DataUpdateField(
                String fieldName,
                Object value)
        {
            this.fieldName=fieldName;
            this.value=value;
        }
    }
    public int connectionID;
    public DataUpdateField[] updatedFields;

}
