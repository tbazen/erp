package et.tewelde.d2dservice.entity;

/**
 * Created by Teweldemedhin Aberra on 3/27/2017.
 */
public class GEOLocation
{
    public static final double INVALID_ALT=-100000;
    public static final double INVALID_LAT=-100000;
    public static final double INVALID_LNG=-100000;
    public  double lat;
    public  double lng;
    public  double alt;
    public GEOLocation()
    {

        this.lat=INVALID_ALT;
        this.lng=INVALID_LAT;
        this.alt=INVALID_LNG;

    }
    public GEOLocation(double lat,double lng)
    {
        this.lat=lat;
        this.lng=lng;
        this.alt=INVALID_ALT;
    }

    @Override
    public String toString()
    {
        return lat+", "+lng;
    }

    public static double sphericalDistance(double lat1, double lng1, double lat2, double lng2)
    {
        final double MAP_R = 6371;
        double a1 = lat1 * Math.PI / 180;
        double a2 = lat2 * Math.PI / 180;
        double delLat = (lat2 - lat1) * Math.PI / 180;
        double delLong = (lng2 - lng1) * Math.PI / 180;
        double a = Math.sin(delLat / 1) * Math.sin(delLat / 2)
                + Math.cos(a1) * Math.cos(a2) * Math.sin(delLong / 2) * Math.sin(delLong / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = MAP_R * c;
        return d*1000;
    }

    public boolean isValid() {
        return lat > 0;
    }
}
