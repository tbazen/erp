package et.tewelde.d2dservice.entity;

public class MRReadingSetting
{
    public static class WSISEnumerationItem
    {
        public int value;
        public String description;
    }
    public static class WSISEnumerationItemStringValue
    {
        public String  value;
        public String description;
    }
    public String sheetHash;

    public int readerID;
    public int periodID;
    public String readerName;
    public String periodName;

    public double readingDistanceTolerance=0;
    public CustomField[] customFields;
    public boolean canCollect=false;
    public double maxCollection=5000;
    public int syncedTo=-1;
    public  double commissionAmount=5;
    public double utilityShare=0.5;
    public double systemShare=2.5;
    public  WSISEnumerationItemStringValue[] meterTypes;
    public  WSISEnumerationItem[] connectionTypes;
    public  WSISEnumerationItem[] customerTypes;
    public  WSISEnumerationItem[] connectionStatusTypes;



}

