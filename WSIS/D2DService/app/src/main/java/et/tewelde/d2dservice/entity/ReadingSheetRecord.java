package et.tewelde.d2dservice.entity;

import et.tewelde.d2dservice.MRRepository;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.MRGlobals;

public class ReadingSheetRecord
{
    public final static int PAYMENT_STATUS_ACTIVE=0;
    public final static int PAYMENT_STATUS_ACTIVE_PAID=1;
    public final static int PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED=2;
    public final static int PAYMENT_STATUS_ACTIVE_INVALIDATED=3;

    public final static int FIELD_CONTRACT_NO=1;
    public final static int FIELD_METER_NO=2;
    public final static int FIELD_NAME=4;

    public  final static int READ_STAT_INITIAL=0;
    public  final static int READ_STAT_READ=1;
    public  final static int READ_STAT_READ_UPLOADED=3;

    public static final int LIST_READING_ALL = 1;
    public static final int LIST_READING_SHEET = 2;
    public static final int LIST_READING_PAID = 3;
    public static final int LIST_READING_CONTRACT_NO_SEARCH = 4;
    public static final int LIST_READING_METER_NO_SEARCH = 5;
    public static final int LIST_READING_NAME_SEARCH = 6;
    public static final int LIST_READING_METER_NO_FUZZY_SEARCH = 7;
    public static final int LIST_READING_LOCATION_SEARCH = 8;


    private String connectionsStatusStr;
    private String billStatusText;
    private double totalBill;

    public boolean canRead(MRReadingSetting stg)
    {
        return (connectionStatus==2 && status!=READ_STAT_READ_UPLOADED) &&
                (stg.readerID==-1||stg.readerID==stg.readerID);
    }

    public String getReadingStatusText()
    {
        switch (status)
        {
            case READ_STAT_INITIAL:
                return "Not read";
            case READ_STAT_READ:
                if(reading<0)
                    return "Visited";
                return "Read";
            case READ_STAT_READ_UPLOADED:
                return "Uploaded";
            default:
                return "Unknown status! (Your application might be updated)";
        }
    }
    public String getBillStatusText(MRReadingSetting stg)
    {
        switch (payStatus)
        {
            case PAYMENT_STATUS_ACTIVE:
                return "Not Paid";
            case PAYMENT_STATUS_ACTIVE_INVALIDATED:
                return "Bill invalidated";
            case PAYMENT_STATUS_ACTIVE_PAID:
                if(stg==null)
                    return "Paid";
                return "Paid ("+ MRGlobals.moneyFormat.format(this.getTotalBill()+ stg.commissionAmount)+" Birr)";
            case PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED:
                if(stg==null)
                    return "Paid Confirmed";
                return "Paid Confirmed ("+ MRGlobals.moneyFormat.format(this.getTotalBill()+ stg.commissionAmount)+" Birr)";
            default:
                return "Unknown status! (Your application might be updated)";
        }
    }

    public boolean paymentAllowed()
    {
        return canPay && payStatus==PAYMENT_STATUS_ACTIVE && getTotalBill()>0.01;
    }

    public double getTotalBill()
    {
        double totalBill=0;
        if(this.bill!=null && this.bill.items!=null)
            for(MRBillItem item :this.bill.items)
                totalBill+=item.amount;
        return totalBill;
    }

    public double getTotalWithCommission()
    {
        return getTotalBill()+5;
    }

    public boolean isPaid()
    {
        return payStatus != PAYMENT_STATUS_ACTIVE;
    }

    public boolean isRead()
    {
        return status != READ_STAT_INITIAL;
    }

    public boolean hasLocation()
    {
        return lat>0;
    }

    public GEOLocation getMeterLocation()
    {
        return new GEOLocation(this.lat,this.lng);
    }


    public static class MRBillItem
    {
        public int billID;
        public String description;
        public double amount;

        public String getDescription()
        {
            return description;
        }

        public double getAmount()
        {
            return amount;
        }
    }
    public static class MRBill
    {
        public MRBillItem [] items;

        public MRBillItem[] getItems()
        {
            return items;
        }

        public boolean idEquals(int[] billIDs)
        {
            if(billIDs==null || billIDs.length!=items.length)
                return false;
            for(int i=0;i<billIDs.length;i++)
                if(billIDs[i]!=items[i].billID)
                    return false;
            return true;
        }
    }
    //record index
    public int connectionID;
    public int orderN;
    public int periodID;

    public String getConnectionsStatusStr()
    {
        switch (connectionStatus)
        {
            case 1: return "Pending";
            case 2: return "Active";
            case 3: return "Disconnected";
            case 4: return "Discontinued";
            default:return "Unknown";

        }
    }
    //connection info
    public String contractNo;
    public String  customerName;
    public String meterNo;
    public String meterType;
    public String meterTypeID;
    public double lat;
    public double lng;
    public int connectionStatus;
    public String phoneNo;
    public int customerType;
    public int connectionType;

    public String rowKey;

    //setting
    public int readerID;
    public double minReadAllowed;
    public double maxReadAllowed;

    //reading data
    public int status;
    public int actualReaderID;
    public double reading;
    public int extraInfo;
    public int problem;
    public String remark;
    public long readTime;
    public double readingLat;
    public double readingLng;

    //billing
    public boolean canPay;
    public MRBill bill;
    public double billTotal;
    public String billText;
    public int payStatus;
    public long payTime;
    public long payConfirmTime;

    //double
    public double distance=-1;
    public boolean validPhoneNo()
    {

        if (phoneNo == null || phoneNo.length() < 9)
            return false;
        int j=-1;
        for(int i=phoneNo.length()-1;i>=0;i--)
        {
            if(Character.isDigit(phoneNo.charAt(i)))
            {
                j++;
                if (j == 8)
                    return phoneNo.charAt(i) == '9';
            }
        }
        return false;
    }
    public String normalizePhoneNo()
    {
        char[] digs=new char[10];
        if (phoneNo == null || phoneNo.length() < 9)
           throw new IllegalStateException("Invalid phone no:"+phoneNo);
        int j=-1;
        for(int i=phoneNo.length()-1;i>=0;i--)
        {
            if(Character.isDigit(phoneNo.charAt(i)))
            {
                j++;
                if (j == 9)
                    break;
                digs[9-j] = phoneNo.charAt(i);
            }
        }
        digs[0]='0';
        return new String(digs);
    }
    public static  final int MeterReadingProblem_NoProblem=0;
    public static  final int MeterReadingProblem_VisistedNoAttendant=1;
    public static  final int MeterReadingProblem_VisistedNotCooperative=2;
    public static  final int MeterReadingProblem_VisistedDefectiveMeter=3;
    public static  final int MeterReadingProblem_VisistedMeterPhysicallyInaccessible=4;
    public static  final int MeterReadingProblem_Other=6;

    public String getReadingProblemString()
    {
        switch (problem)
        {
            case MeterReadingProblem_NoProblem:
                return "";
            case MeterReadingProblem_VisistedNoAttendant:
                return "Nobody Home";
            case MeterReadingProblem_VisistedNotCooperative:
                return "Household Not Cooperative";
            case MeterReadingProblem_VisistedDefectiveMeter:
                return "Defective Meter";
            case MeterReadingProblem_VisistedMeterPhysicallyInaccessible:
                return "Inaccessible Meter";
            case MeterReadingProblem_Other:
                return "General Problem";
            default:
                return "";
        }
    }
}
