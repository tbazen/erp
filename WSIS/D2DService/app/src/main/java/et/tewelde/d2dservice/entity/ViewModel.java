package et.tewelde.d2dservice.entity;

import android.provider.ContactsContract;

import java.util.Map;

import et.tewelde.d2dservice.ReadingSheetSync;
import et.tewelde.d2dservice.util.JSONUtils;

/**
 * Created by Teweldemedhin Aberra on 3/29/2017.
 */
public class ViewModel
{
    public static class WorkSummary
    {
        private int totalAllocated=0;
        private int totalConnections=0;
        private int read=0;
        private int uploaded=0;
        private int nCollected=0;
        private double collected=0;
        private int nCollectedConfirmed=0;
        private double collectedConfirmed=0;

        private double serviceCharge=0;
        private double utilityShare;
        private double utilityPaid;
        private double systemFee;
        private double systemFeePaid;

        public WorkSummary clone()
        {
            WorkSummary ret=new WorkSummary();
            ret.totalAllocated=this.totalAllocated;
            ret.totalConnections=this.totalConnections;
            ret.read=this.read;
            ret.uploaded=this.uploaded;
            ret.nCollected=this.nCollected;
            ret.collected=this.collected;
            ret.serviceCharge=this.serviceCharge;
            ret.nCollectedConfirmed=this.nCollectedConfirmed;
            ret.collectedConfirmed=this.collectedConfirmed;

            ret.utilityShare=utilityShare;
            ret.utilityPaid=utilityPaid;
            ret.systemFee=systemFee;
            ret.systemFeePaid=systemFeePaid;
            return ret;
        }

        public double getUtilityBalance()
        {
            return collected+utilityShare-utilityPaid;
        }
        public double getSystemFeeBalance()
        {
            return systemFee-systemFeePaid;
        }
        public String getWaterCompany()
        {
            return "HWSSA";
        }
        public int getRead()
        {
            return read;
        }

        public void setRead(int read)
        {
            this.read = read;
        }

        public int getTotalAllocated()
        {
            return totalAllocated;
        }

        public void setTotalAllocated(int totalAllocated)
        {
            this.totalAllocated = totalAllocated;
        }

        public int getTotalConnections()
        {
            return totalConnections;
        }

        public void setTotalConnections(int totalConnections)
        {
            this.totalConnections = totalConnections;
        }

        public int getUploaded()
        {
            return uploaded;
        }

        public void setUploaded(int uploaded)
        {
            this.uploaded = uploaded;
        }

        public int getNCollected()
        {
            return nCollected;
        }

        public void setNCollected(int nCollected)
        {
            this.nCollected = nCollected;
        }

        public double getCollected()
        {
            return collected;
        }

        public void setCollected(double collected)
        {
            this.collected = collected;
        }

        public double getCommission()
        {
            return serviceCharge-utilityShare-systemFee;
        }

        public int getNCollectedConfirmed()
        {
            return nCollectedConfirmed;
        }

        public void setNCollectedConfirmed(int nCollectedConfirmed)
        {
            this.nCollectedConfirmed = nCollectedConfirmed;
        }

        public double getCollectedConfirmed()
        {
            return collectedConfirmed;
        }

        public void setCollectedConfirmed(double collectedConfirmed)
        {
            this.collectedConfirmed = collectedConfirmed;
        }
        public void updateBy(ReadingSheetSync.MobileChangeLog.Change change)
        {
            if (change == null)
                return;
            this.utilityPaid += change.utilityPayment;
            this.systemFeePaid+= change.systemPayment;
        }
        public void updateBy(ReadingSheetRecord old,ReadingSheetRecord rec,MRReadingSetting st)
        {
            if(old!=null)
            {
                totalConnections--;
                if(old.readerID==st.readerID)
                    totalAllocated--;
                if(old.status==ReadingSheetRecord.READ_STAT_READ)
                    read--;
                if(old.status==ReadingSheetRecord.READ_STAT_READ_UPLOADED)
                {
                    read--;
                    uploaded--;
                }
                if(old.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID)
                {
                    nCollected--;
                    collected-=old.billTotal;
                    serviceCharge-=st.commissionAmount;
                    utilityShare-=st.utilityShare;
                    systemFee-=st.systemShare;
                }
                if(old.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED)
                {
                    nCollected--;
                    collected-=old.billTotal;
                    serviceCharge-=st.commissionAmount;
                    utilityShare-=st.utilityShare;
                    systemFee-=st.systemShare;

                    nCollectedConfirmed--;
                    collectedConfirmed-=old.billTotal;
                }
            }

            if(rec!=null)
            {
                totalConnections++;
                if(rec.readerID==st.readerID)
                    totalAllocated++;
                if(rec.status==ReadingSheetRecord.READ_STAT_READ)
                    read++;
                if(rec.status==ReadingSheetRecord.READ_STAT_READ_UPLOADED)
                {
                    read++;
                    uploaded++;
                }
                if(rec.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID)
                {
                    nCollected++;
                    collected+=rec.billTotal;
                    serviceCharge+=st.commissionAmount;
                    utilityShare+=st.utilityShare;
                    systemFee+=st.systemShare;
                }
                if(rec.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED)
                {
                    nCollected++;
                    collected+=rec.billTotal;
                    serviceCharge+=st.commissionAmount;
                    utilityShare+=st.utilityShare;
                    systemFee+=st.systemShare;

                    nCollectedConfirmed++;
                    collectedConfirmed+=rec.billTotal;
                }
            }
        }

        public double getTotalPayment()
        {
            return this.serviceCharge+this.collected;
        }

        public double getServiceCharge()
        {
            return serviceCharge;
        }

        public void setServiceCharge(double serviceCharge)
        {
            this.serviceCharge = serviceCharge;
        }

        public double getUtility()
        {
            return collected+utilityShare;
        }



        public double getUtilityPaid()
        {
            return utilityPaid;
        }

        public void setUtilityPaid(double utilityPaid)
        {
            this.utilityPaid = utilityPaid;
        }

        public double getSystemFee()
        {
            return systemFee;
        }

        public void setSystemFee(double systemFee)
        {
            this.systemFee = systemFee;
        }

        public double getSystemFeePaid()
        {
            return systemFeePaid;
        }

        public void setSystemFeePaid(double systemFeePaid)
        {
            this.systemFeePaid = systemFeePaid;
        }

        public double getUtilityShare()
        {
            return utilityShare;
        }

        public void setUtilityShare(double utilityShare)
        {
            this.utilityShare = utilityShare;
        }
    }

    public static class CustomerProfile
    {

        private String customerName;
        private String contractNo;
        private int connectionID;
        private String conStatStr;
        private int conStat;
        private String meterNo;
        private String meterSize;
        private String phoneNo;
        private String reading;
        private String readingStatus;
        private String problemStr;
        private boolean allowReading;
        private boolean allowClearReading;
        private String bill;
        private String billStatus;
        private boolean allowCollect;
        private String coordinates;
        private String lat;
        private String lng;
        private String allocation;
        private String customerType;
        private String connectionType;
        private boolean allowResend;
        public CustomerProfile(ReadingSheetRecord rec,MRReadingSetting stg,WorkSummary ws)
        {
            this.customerName=rec.customerName;
            this.contractNo=rec.contractNo;
            this.connectionID=rec.connectionID;
            this.conStatStr=rec.getConnectionsStatusStr();
            this.conStat=rec.connectionStatus;
            this.meterNo=rec.meterNo;
            this.meterSize=rec.meterType;
            this.phoneNo=rec.phoneNo;
            if(rec.status==ReadingSheetRecord.READ_STAT_INITIAL|| rec.reading<0)
                this.reading="";
            else
                this.reading=Double.toString(rec.reading);
            this.readingStatus=rec.getReadingStatusText();
            this.problemStr= rec.getReadingProblemString();
            this.allowReading=rec.canRead(stg);
            this.allowClearReading=rec.status==ReadingSheetRecord.READ_STAT_READ;
            this.bill=rec.billText;
            this.billStatus=rec.getBillStatusText(stg);
            this.allowCollect=rec.paymentAllowed()&& ws.getUtilityBalance()<stg.maxCollection;
            this.coordinates=rec.lat+", "+rec.lng;
            this.lat=Double.toString(rec.lat);
            this.lng=Double.toString(rec.lng);
            this.allocation=rec.readerID==-1?"Not Allocated":(rec.readerID==stg.readerID?"My Meter":"Others meter");
            this.customerType=DataFieldType.Type_CustomerType.getDescriptionForValue(rec.customerType);
            this.connectionType=DataFieldType.Type_ConnectionType.getDescriptionForValue(rec.connectionType);
            this.allowResend=rec.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID
                    ||rec.payStatus==ReadingSheetRecord.PAYMENT_STATUS_ACTIVE_PAID_CONFIRMED;
        }
        public String getCustomerName()
        {
            return customerName;
        }

        public void setCustomerName(String customerName)
        {
            this.customerName = customerName;
        }

        public String getContractNo()
        {
            return contractNo;
        }

        public void setContractNo(String contractNo)
        {
            this.contractNo = contractNo;
        }

        public int getConnectionID()
        {
            return connectionID;
        }

        public void setConnectionID(int connectionID)
        {
            this.connectionID = connectionID;
        }

        public String getConStatStr()
        {
            return conStatStr;
        }

        public int getConStat()
        {
            return conStat;
        }

        public String getMeterNo()
        {
            return meterNo;
        }

        public String getMeterSize()
        {
            return meterSize;
        }

        public String getPhoneNo()
        {
            return phoneNo;
        }

        public String getReading()
        {
            return reading;
        }

        public boolean isAllowReading()
        {
            return allowReading;
        }

        public boolean isAllowClearReading()
        {
            return allowClearReading;
        }

        public String getBill()
        {
            return bill;
        }

        public String getBillStatus()
        {
            return billStatus;
        }

        public boolean isAllowCollect()
        {
            return allowCollect;
        }

        public String getCoordinates()
        {
            return coordinates;
        }

        public String getLat()
        {
            return lat;
        }

        public String getLng()
        {
            return lng;
        }

        public String getReadingStatus()
        {
            return readingStatus;
        }


        public String getAllocation()
        {
            return allocation;
        }

        public String getCustomerType()
        {
            return customerType;
        }

        public String getConnectionType()
        {
            return connectionType;
        }

        public String getProblemStr()
        {
            return problemStr;
        }

        public boolean isAllowResend()
        {
            return allowResend;
        }
    }
    public static class PaymentPageModel
    {
        private String customerName;
        private String contractNo;
        private ReadingSheetRecord.MRBill bill;
        private double totalPrice;
        private double commission;
        private double totalPayment;
        public PaymentPageModel(ReadingSheetRecord rec)
        {
            this.customerName=rec.customerName;
            this.contractNo=rec.contractNo;
            this.bill=rec.bill;
            this.totalPrice=rec.getTotalBill();
            this.commission=5.0;
            this.totalPayment=this.totalPrice+this.commission;
        }

        public String getCustomerName()
        {
            return customerName;
        }

        public String getContractNo()
        {
            return contractNo;
        }

        public ReadingSheetRecord.MRBill getBill()
        {
            return bill;
        }

        public double getTotalPrice()
        {
            return totalPrice;
        }

        public double getCommission()
        {
            return commission;
        }

        public double getTotalPayment()
        {
            return totalPayment;
        }

    }
    public static class DataEditorModel
    {
        public static class EditField
        {
            private String fieldDescription;
            private String fieldName;
            private String[] options;
            private String value;
            private boolean changed;
            private boolean multiLine=false;
            public EditField(DataUpdate.DataUpdateField field,boolean changed,boolean multiLine)
            {
                DataUpdate.FieldMetaData meta= DataUpdate.FieldMetaData.getMetaDataForFieldName(field.fieldName);
                this.fieldDescription=meta.fieldDescription;
                this.fieldName=field.fieldName;
                this.changed=changed;
                this.multiLine=multiLine;
                if(meta.type instanceof DataFieldType.WSISLookupType)
                {
                    DataFieldType.WSISLookupType lu = (DataFieldType.WSISLookupType) meta.type;
                    options=new String[lu.entries.length];
                    for(int i=0;i<options.length;i++)
                    {
                        options[i]=lu.entries[i].getValue();
                    }
                    this.value=Integer.toString(lu.indexOfValue((Integer)field.value)+1);
                }else if(meta.type instanceof DataFieldType.WSISLookupStringValueType)
                {
                    DataFieldType.WSISLookupStringValueType lu = (DataFieldType.WSISLookupStringValueType) meta.type;
                    options=new String[lu.entries.length];
                    for(int i=0;i<options.length;i++)
                    {
                        options[i]=lu.entries[i].getValue();
                    }
                    this.value=Integer.toString(lu.indexOfValue((String) field.value)+1);
                }
                else
                {
                    options = null;
                    this.value=(String)meta.type.dataToString(field.value);
                }
            }

            public String getFieldName()
            {
                return fieldName;
            }

            public String[] getOptions()
            {
                return options;
            }

            public String getValue()
            {
                return value;
            }

            public String getFieldDescription()
            {
                return fieldDescription;
            }

            public boolean isChanged()
            {
                return changed;
            }

            public boolean isMultiLine()
            {
                return multiLine;
            }
        }
        private EditField[] data;
        public DataEditorModel(DataUpdate.DataUpdateField[] fields,DataUpdate.DataUpdateField[] updated)
        {
            data=new EditField[fields.length];
            int i=0;
            for (DataUpdate.DataUpdateField f:fields)
            {

                if (updated != null)
                {
                    boolean replaced=false;
                    for (DataUpdate.DataUpdateField u : updated)
                    {
                        if (f.fieldName.equals(u.fieldName))
                        {
                            replaced=true;
                            data[i++] = new EditField(u, true,f.fieldName.equals("coordinate"));
                            break;
                        }
                    }
                    if(replaced)
                        continue;
                }
                data[i++]=new EditField(f,false,f.fieldName.equals("coordinate"));
            }
        }

        public EditField[]  getData()
        {
            return data;
        }
    }
}
