package et.tewelde.d2dservice.ui;

import android.content.Context;

import et.tewelde.d2dservice.entity.ReadingSheetRecord;

public class ArrayReadingSheetRecordAdapter extends ReadingRecordAdapterBase
{
    ReadingSheetRecord[]rec;
    public ArrayReadingSheetRecordAdapter(Context context,ReadingSheetRecord []recs)
    {
        super(context);
        this.rec=recs;

    }
    @Override
    protected ReadingSheetRecord getRec(int position)
    {
        return rec[position];
    }

    @Override
    public int getCount()
    {
        return rec.length;
    }


}
