package et.tewelde.d2dservice.ui;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.MRReadingSetting;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.VelocityEvaluate;

public class CustomerProfilePageController extends WebPageController
{
    public CustomerProfilePageController(final MainActivity context, final MobiReadService service, View contentView, final int connectionID)
    {
        super(context, service, contentView, connectionID,R.id.customer_profile_webview);
        updatePage(context, service, connectionID);
    }

    private void updatePage(final MainActivity context, final MobiReadService service, final int connectionID)
    {
        service.getReaderSettingAsync(activity, this.connectionID
                , new AsyncInterfaces.DoneWithReturn<MRReadingSetting>()
        {
            @Override
            public void done(Exception ex, MRReadingSetting ret)
            {
                final MRReadingSetting stg=ret;
                if (ex == null)
                {
                    service.getReadingSheetRecordAsync(context, connectionID, new AsyncInterfaces.DoneWithReturn<ReadingSheetRecord>()
                    {
                        @Override
                        public void done(Exception ex, ReadingSheetRecord ret)
                        {
                            if (ex == null)
                            {
                                final ReadingSheetRecord rrec = ret;
                                service.getWorkSummaryAsync(activity, new AsyncInterfaces.DoneWithReturn<ViewModel.WorkSummary>()
                                {
                                    @Override
                                    public void done(Exception ex, ViewModel.WorkSummary ret)
                                    {
                                        if (ex == null)
                                        {
                                            String html = VelocityEvaluate.evaluateTemplate(R.raw.customer_profile, new ViewModel.CustomerProfile(rrec, stg, ret));
                                            //activity.showInformationMessage(html);
                                            web.loadDataWithBaseURL("same://u", html, "text/html", "UTF-8", null);
                                            //web.loadData(html, "text/html", "UTF-8");
                                        } else
                                            activity.showErrorMessage("Error trying to load customer profile.\n" + ex.getMessage());
                                    }
                                });
                            } else
                                activity.showErrorMessage("Error trying to load customer profile.\n" + ex.getMessage());
                        }
                    });

                } else
                    activity.showErrorMessage("Error trying to load customer profile.\n" + ex.getMessage());
            }
        });
    }

    @Override
    protected boolean processURL(Uri uri)
    {
        //reading_page
        if (uri.getPath().equals("/reading_page"))
        {
            activity.showReadingEntryPage(Integer.parseInt(uri.getQueryParameter("conID")));
            return true;
        }

        //clear_reading
        if (uri.getPath().equals("/clear_reading"))
        {
            activity.showConfirmationMessage("Are you sure you want to clear this reading?"
                    , new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            service.clearReadingAsync(activity, connectionID, new AsyncInterfaces.DoneNoReturn()
                            {
                                @Override
                                public void done(Exception ex)
                                {
                                    if (ex != null)
                                        activity.showErrorMessage(ex.getMessage());
                                    updatePage(activity, service, connectionID);
                                }
                            });
                        }
                    }
            );
            return true;
        }
        //bill_pay
        if (uri.getPath().equals("/bill_pay"))
        {
            activity.showPaymentPage(Integer.parseInt(uri.getQueryParameter("conID")));
            return true;
        }
        //update_prof
        if (uri.getPath().equals("/update_prof"))
        {
            activity.showDataEditor(Integer.parseInt(uri.getQueryParameter("conID")));
            return true;
        }
        //navigate
        if (uri.getPath().equals("/navigate"))
        {
            service.getReadingSheetRecordAsync(activity, connectionID, new AsyncInterfaces.DoneWithReturn<ReadingSheetRecord>()
            {
                @Override
                public void done(Exception ex, ReadingSheetRecord ret)
                {
                    String uri="google.navigation:q="+ret.lat+"%2C"+ret.lng;
                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(uri));
                    activity.startActivity(intent);
                }
            });
            return true;
        }
        //navigate
        if (uri.getPath().equals("/resend_sms"))
        {
            activity.showConfirmationMessage("Are you sure you want to resend receipt SMS?",
                    new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            service.resendSMSAsync(activity, connectionID, new AsyncInterfaces.DoneNoReturn()
                            {

                                @Override
                                public void done(Exception ex)
                                {
                                    if(ex==null)
                                    {
                                        activity.showInformationMessage("Message saved for sending");
                                    }
                                    else
                                    {
                                        activity.showErrorMessage("Re-send receipt sms failed\n"+ex.getMessage());
                                    }
                                }
                            });
                        }
                    }
            );
            return true;
        }return false;
    }
}
