package et.tewelde.d2dservice.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import et.tewelde.d2dservice.MRRepository;
import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.SyncEvent;
import et.tewelde.d2dservice.entity.CustomField;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.VelocityEvaluate;

/**
 * Created by Teweldemedhin Aberra on 3/25/2017.
 */
public class DashboardController extends PageController
{
    private MRKeyboard kb;

    public static class ReadingSheetRowAdapter extends BaseAdapter
    {
        @Override
        public int getCount()
        {
            return 0;
        }

        @Override
        public Object getItem(int position)
        {
            return null;
        }

        @Override
        public long getItemId(int position)
        {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            return null;
        }
    }
    //controls
    Spinner spinnerQueryType;
    WebView webviewStat;
    ImageButton buttonSetting;
    EditText editQuery;
    TextView textSplash;
    AlertDialog alertDialog;

    List<CustomField> customFieldList;

    MainActivity activity;
    MobiReadService service;
    public DashboardController(MainActivity context,MobiReadService service,View contentView)
    {
        this.activity=context;
        this.service=service;
        //initialize controls
        initializeControls(context, contentView);
        this.service.addSystemStatusListener(new MobiReadService.SystemStatusChangeListener()
        {
            @Override
            public void logEvent(Object source, String event, MobiReadService.SystemStatusType type, Object data)
            {

            }

            @Override
            public void setStatus(Object source, String status, MobiReadService.SystemStatusType type, Object data)
            {
                if (data instanceof SyncEvent)
                {
                    DashboardController.this.activity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            DashboardController.this.updateDashboard();
                        }
                    });
                }
            }
        });
    }
    void updateQueryHint()
    {
        boolean enable = true;
        boolean showKB = false;
        switch (spinnerQueryType.getSelectedItemPosition())
        {
            case 0://Contract no
                editQuery.setHint("Enter Contract No");
                editQuery.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_TEXT);
                editQuery.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                editQuery.selectAll();
                enable = false;
                showKB = true;
                break;
            case 1:
                editQuery.setHint("Enter Meter No");
                editQuery.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_DATETIME);
                editQuery.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                editQuery.selectAll();
                enable = false;
                showKB = true;
                break;
            case 2:
                editQuery.setHint("Enter Customer Name");
                editQuery.setInputType(InputType.TYPE_CLASS_TEXT);
                editQuery.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                editQuery.selectAll();
                break;
            case 3:
                enable = false;
                editQuery.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                editQuery.setHint("Wait for the GPS");
                break;
            case 4:
                enable = false;
                editQuery.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                editQuery.setHint("Select Connection From List");
                break;
            default:
                CustomField cf = customFieldList.get(spinnerQueryType.getSelectedItemPosition() - 5);
                editQuery.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                editQuery.setHint("Enter " + cf.description);
                break;
        }
        if (editQuery.isEnabled() != enable)
            editQuery.setEnabled(enable);
        if(showKB)
            kb.setVisibility(View.VISIBLE);
        else
            kb.setVisibility(View.GONE);
        showSoftKeyboard(!showKB);
    }
    void showSoftKeyboard(boolean show)
    {

        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(show)
        {
            if(editQuery.isEnabled())
            {
                editQuery.requestFocus();
                imm.showSoftInput(editQuery, 0);
            }
        }
        else
            imm.hideSoftInputFromWindow(editQuery.getWindowToken(), 0);

    }
    private void initializeControls(final Context context, View contentView)
    {
        editQuery=(EditText)contentView.findViewById(R.id.main_text_query);
        spinnerQueryType=(Spinner)contentView.findViewById(R.id.main_spin_query_type);
        webviewStat =(WebView)contentView.findViewById(R.id.main_web_readstat);
        buttonSetting=(ImageButton)contentView.findViewById(R.id.main_button_setting);
        alertDialog= new AlertDialog.Builder(context).create();
        textSplash=(TextView)contentView.findViewById(R.id.main_splash);
        kb=(MRKeyboard)contentView.findViewById(R.id.main_kb);
        kb.setEditText(editQuery);
        kb.setListener(new MRKeyboardListner()
        {
            @Override
            public void typeText(String text)
            {

            }

            @Override
            public void backSpace()
            {

            }

            @Override
            public void confirm()
            {
                searchCustomers();
            }
        });
        configureSearchTypeSpinner(context);

        //setting button
        buttonSetting.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DashboardController.this.activity.showSettingPage();
            }
        });

        configureDashboardWebView();
        configureQueryTextBox();
        
    }
    void showSplashScreen(String text)
    {
        textSplash.setText(text);
        textSplash.setVisibility(View.VISIBLE);
    }
    void hideSplashScreen()
    {
        textSplash.setVisibility(View.INVISIBLE);
    }
    private void configureQueryTextBox()
    {
        editQuery.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if(event.getAction()==KeyEvent.ACTION_DOWN && event.getKeyCode()==KeyEvent.KEYCODE_ENTER)
                {
                    searchCustomers();
                    return true;
                }
                return false;
            }
        });
    }

    private void searchCustomers()
    {
        int type;
        switch (spinnerQueryType.getSelectedItemPosition())
        {
            case 1:type= ReadingSheetRecord.LIST_READING_METER_NO_SEARCH;break;
            case 2:type=ReadingSheetRecord.LIST_READING_NAME_SEARCH;break;
            default:type=ReadingSheetRecord.LIST_READING_CONTRACT_NO_SEARCH;break;
        }
        service.getReadingSheetCursorAsync(activity, type, editQuery.getText().toString(), new AsyncInterfaces.DoneWithReturn<MRRepository.ReadingSheetCursor>()
        {
            @Override
            public void done(Exception ex, MRRepository.ReadingSheetCursor ret)
            {
                if(ret.getNRec()==0)
                    activity.showErrorMessage("No record found matching your query");
                else if(ret.getNRec()==1)
                {
                    try
                    {
                        ret.move(1);
                        ReadingSheetRecord rec=ret.getObject();
                        activity.showCustomerProfile(rec.connectionID);
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                        activity.showErrorMessage("Error loading data\n" + e.getMessage());
                    }
                }
                else
                {
                    activity.showSearchResultPage(ret);
                }
            }
        });
    }

    private void configureDashboardWebView()
    {
        webviewStat.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                try
                {
                    URL u = new URL(url);
                    if (u.getPath().equals("/readstat"))
                    {
                        activity.showReadingSheetPage();
                        return true;
                    } else if (u.getPath().equals("/paystat"))
                    {
                        activity.showPaymentListPage();
                        return true;
                    }

                } catch (MalformedURLException e)
                {

                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        updateDashboard();
    }

    private void updateDashboard()
    {
        service.getWorkSummaryAsync(activity, new AsyncInterfaces.DoneWithReturn<ViewModel.WorkSummary>()
        {
            @Override
            public void done(Exception ex, ViewModel.WorkSummary ret)
            {
                if(ex==null)
                    webviewStat.loadDataWithBaseURL("same://u", VelocityEvaluate.evaluateTemplate(R.raw.dashboard, ret),"text/html","UTF-8",null);
                else
                    DashboardController.this.activity.showErrorMessage("Errir trying to load dashboard.\n"+ex.getMessage());
            }
        });
    }

    private void configureSearchTypeSpinner(final Context context)
    {
        //EditText et;

        service.getSearchableCustomFieldsAsync(activity, new AsyncInterfaces.DoneWithReturn<List<CustomField>>()
        {
            @Override
            public void done(Exception ex, List<CustomField> ret)
            {
                final ArrayList<String> items = new ArrayList<>();
                items.add("Search by Contract No.");   //0
                items.add("Search by Meter No.");      //1
                items.add("Search by Name");           //2
                items.add("Search by Location");       //3
                items.add("Open Reading Sheet");         //4
                final ArrayAdapter arr = new ArrayAdapter<CharSequence>(context, R.layout.search_option_item);
                arr.addAll(items);
                if (ex == null)
                {
                    for (CustomField cf : customFieldList = ret)
                        arr.add(cf.description + " Search");
                } else
                    alertDialog.setMessage("Error trying to load custom field setting.\n" + ex.getMessage());
                spinnerQueryType.setAdapter(arr);
                spinnerQueryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {
                        updateQueryHint();
                        switch (position)
                        {
                            case 4:
                                activity.showReadingSheetPage();
                                break;
                            case 3:
                                if(service.locationAvialable())
                                {
                                    showSplashScreen("Searching by location");
                                    service.searchByRadiusAsync(activity, service.getLastKnownLocation(),20,100, new AsyncInterfaces.DoneWithReturn<ReadingSheetRecord[]>()
                                    {
                                        @Override
                                        public void done(Exception ex, ReadingSheetRecord[] ret)
                                        {
                                            if(ex==null)
                                            {
                                                spinnerQueryType.setSelection(0);
                                                hideSplashScreen();
                                                if (ret.length == 0)
                                                    activity.showErrorMessage("No record found matching your query");
                                                else if (ret.length == 1)
                                                {
                                                    activity.showCustomerProfile(ret[0].connectionID);
                                                } else
                                                {
                                                    activity.showSearchResultPage(new ArrayReadingSheetRecordAdapter(activity, ret));
                                                }
                                            }
                                            else
                                                activity.showErrorMessage("Error searching:"+ex.getMessage());

                                        }
                                    });
                                }
                                else
                                {
                                    spinnerQueryType.setSelection(0);
                                    activity.showErrorMessage("Location is not available");
                                }
                                break;
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent)
                    {

                    }
                });
                updateQueryHint();

            }
        });
    }

    @Override
    public void activate()
    {
        updateDashboard();
        if(spinnerQueryType.getSelectedItemPosition()>2)
            spinnerQueryType.setSelection(0);
        super.activate();
    }

    @Override
    public void deactivate()
    {

        super.deactivate();
    }
}
