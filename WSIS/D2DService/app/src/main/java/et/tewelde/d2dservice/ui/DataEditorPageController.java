package et.tewelde.d2dservice.ui;

import android.net.Uri;
import android.view.View;
import android.webkit.ValueCallback;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.DataFieldType;
import et.tewelde.d2dservice.entity.DataUpdate;
import et.tewelde.d2dservice.entity.GEOLocation;
import et.tewelde.d2dservice.entity.MRReadingSetting;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.VelocityEvaluate;

public class DataEditorPageController extends WebPageController
{
    public DataEditorPageController(final MainActivity context, final MobiReadService service, View contentView, final int connectionID)
    {
        super(context, service, contentView, connectionID,R.id.data_editor_webview);
        updatePage(context, service, connectionID);
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setAllowContentAccess(true);
    }
    ViewModel.DataEditorModel data;
    DataUpdate.DataUpdateField[] fields;
    void loadNew(final DataUpdate updated)
    {
        service.getReaderSettingAsync(activity, this.connectionID
                , new AsyncInterfaces.DoneWithReturn<MRReadingSetting>()
        {
            @Override
            public void done(Exception ex, MRReadingSetting ret)
            {
                final MRReadingSetting stg=ret;
                if (ex == null)
                {

                    service.getReadingSheetRecordAsync(activity, connectionID, new AsyncInterfaces.DoneWithReturn<ReadingSheetRecord>()
                    {
                        @Override
                        public void done(Exception ex, ReadingSheetRecord ret)
                        {
                            if (ex == null)
                            {
                                fields = new DataUpdate.DataUpdateField[]{
                                        new DataUpdate.DataUpdateField("customerName", ret.customerName),
                                        new DataUpdate.DataUpdateField("phoneNo", ret.phoneNo),
                                        new DataUpdate.DataUpdateField("meterNo", ret.meterNo),
                                        new DataUpdate.DataUpdateField("meterTypeID", ret.meterTypeID),
                                        new DataUpdate.DataUpdateField("customerType", ret.customerType),
                                        new DataUpdate.DataUpdateField("connectionType", ret.connectionType),
                                        new DataUpdate.DataUpdateField("connectionStatus", ret.connectionStatus),
                                        new DataUpdate.DataUpdateField("coordinate", ret.getMeterLocation()),
                                };

                                data = new ViewModel.DataEditorModel(fields, updated == null ? null : updated.updatedFields);
                                String html = VelocityEvaluate.evaluateTemplate(R.raw.data_editor, data);
                                web.loadDataWithBaseURL("same://u", html, "text/html", "UTF-8", null);
                            } else
                                activity.showErrorMessage("Error trying to load customer profile.\n" + ex.getMessage());
                        }
                    });

                } else
                    activity.showErrorMessage("Error trying to load customer profile.\n" + ex.getMessage());
            }
        });
    }

    private void updatePage(final MainActivity context, final MobiReadService service, final int connectionID)
    {
        service.getProfileUpdateDataAsync(context, connectionID, new AsyncInterfaces.DoneWithReturn<DataUpdate>()
        {
            @Override
            public void done(Exception ex, DataUpdate ret)
            {
                if (ex == null)
                {
                    loadNew(ret);
                } else
                    activity.showErrorMessage("Error trying to load customer profile.\n" + ex.getMessage());
                }
        });

    }

    @Override
    protected boolean processURL(Uri uri)
    {
        if(uri.getPath().equals("/coord"))
        {
            GEOLocation location=service.getLastKnownLocation();
            if(location==null)
                activity.showErrorMessage("Location is not avialable");
            else
                web.loadUrl("javascript: updateLocation('" + location.toString() + "')");

            return true;
        }
        else
        {
            //reading_page
            ArrayList<DataUpdate.DataUpdateField> updateFields = new ArrayList<>();
            try
            {
                int i = -1;
                for (ViewModel.DataEditorModel.EditField f : this.data.getData())
                {
                    i++;
                    String strVal = uri.getQueryParameter("fld_" + f.getFieldName());
                    if (StringUtils.isEmpty(strVal)) continue;
                    Object val;
                    DataUpdate.FieldMetaData meta = DataUpdate.FieldMetaData.getMetaDataForFieldName(f.getFieldName());
                    if (f.getOptions() == null)
                    {
                        if (!meta.type.isValid(strVal))
                            throw new IllegalArgumentException("Value " + strVal + " is not valid for " + meta.fieldDescription);
                        val = meta.type.parse(strVal);
                    } else
                    {
                        int index = Integer.parseInt(strVal) - 1;
                        if (index == -1)
                            throw new IllegalArgumentException("Please select a value for " + meta.fieldDescription);
                        if (meta.type instanceof DataFieldType.WSISLookupType)
                            val = ((DataFieldType.WSISLookupType) meta.type).getEnumeration()[index].getKey();
                        else
                            val = ((DataFieldType.WSISLookupStringValueType) meta.type).getEnumeration()[index].getKey();
                    }
                    if (fields[i].value.equals(val))
                        continue;
                    ;
                    DataUpdate.DataUpdateField data = new DataUpdate.DataUpdateField(f.getFieldName(), val);
                    updateFields.add(data);
                }
                if (updateFields.size() == 0)
                {
                    activity.showInformationMessage("No data changed");
                    return true;
                }
                DataUpdate update = new DataUpdate();
                update.connectionID = connectionID;
                update.updatedFields = updateFields.toArray(new DataUpdate.DataUpdateField[0]);
                service.saveProfileUpdateDataAsync(activity, connectionID, update, new AsyncInterfaces.DoneNoReturn()
                {
                    @Override
                    public void done(Exception ex)
                    {
                        if (ex == null)
                        {
                            activity.showInformationMessage("Data saved successfully.");
                            activity.onBackPressed();
                        } else
                            activity.showErrorMessage("Error saving the data change.\n" + ex.getMessage());
                    }
                });
            } catch (Exception ex)
            {
                activity.showErrorMessage("Error processing your data.\n" + ex.getMessage());
            }
            return true;
        }
    }
}
