package et.tewelde.d2dservice.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class KBButton extends Button
{

    public KBButton(Context context) {
        super(context);
    }

    public KBButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KBButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

}
