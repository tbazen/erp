package et.tewelde.d2dservice.ui;

import android.app.Dialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.R;


public class MRDialog
{
    public MRDialog()
    {

    }
    String okLabel="Ok";
    String cancelLabel="Cancel";
    public MRDialog(String okLabel,String cancelLabel)
    {
        this.okLabel=okLabel;
        this.cancelLabel=cancelLabel;
    }
    public static interface InputDone
    {
        public void done(Object tag, String value, boolean canceled);
    }
    Dialog dialog=null;
    public String text;
    public void show(MainActivity ctxt,String title
    , final OnClickListener okListener,final OnClickListener cancelListener,boolean showInput, boolean acceptNumberOnly)
    {
        dialog = new Dialog(ctxt);
        // Include dialog.xml file
        dialog.setContentView(R.layout.mr_dialog);
        // Set dialog title
        dialog.setTitle(title);

        // set values for custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.textDialog);
        text.setText(title);
        final EditText edit=(EditText)dialog.findViewById(R.id.dialog_textEdit);
        if(!showInput)
        {
            edit.setVisibility(View.GONE);
        }
        else
        {
/*            if(acceptNumberOnly)
                edit.setInputType(EditorInfo.TYPE_CLASS_NUMBER|EditorInfo.TYPE_NUMBER_FLAG_DECIMAL);
            else
                edit.setInputType(EditorInfo.TYPE_CLASS_TEXT)|EditorInfo.TYPE_T;*/
            edit.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {

                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    MRDialog.this.text=edit.getText().toString();
                }
            });
        }
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.declineButton);

        if(cancelListener==null)
            declineButton.setVisibility(View.GONE);
        else
        {
            declineButton.setText(this.cancelLabel);
            declineButton.setOnClickListener(cancelListener);
        }

        Button acceptButton= (Button) dialog.findViewById(R.id.acceptButton);

        if(okListener==null)
            acceptButton.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    MRDialog.this.hide();
                }
            });
        else
        {
            acceptButton.setText(this.okLabel);
            acceptButton.setOnClickListener(okListener);
        }
    }


    public void hide()
    {
        if(this.dialog!=null)
            this.dialog.hide();
    }
}