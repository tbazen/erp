package et.tewelde.d2dservice.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import et.tewelde.d2dservice.R;

/**
 * Created by Teweldemedhin Aberra on 7/28/2014.
 */
public class MRKeyboard extends FrameLayout
{
    final String NUM_MODE_LABEL="AB/12";
    final String ALPHA_MODEL_LABEL="AB/12";
    static String[] NUM_MODE_KEYS=new String[]{"1","2","3","4","5","6","7","8","9",".","0","-"};
    static String[] ALPHA_MODE_KEYS=new String[]{"ABCD","EFGH","IJKL","MNOP","QRST","UVWX","YZ@_","-+/\\","1234","5678","90()","?!'/\""};
    ArrayList<Button> _typeButtons= new ArrayList<Button>();
    Button _okButton;
    ImageButton _backSpaceButton;
    Button _modeButton;
    MRKeyboardListner _l;
    //TextView _t=null;
    //EditText _e=null;
    String[] _currentMode =null;
    String _multiKey=null;
    interface KBTarget
    {
        public void backSpace();
        public void typeText(String txt);
    }
    KBTarget _t;
    void setMode(boolean num)
    {
        if(num)
        {
            _currentMode = NUM_MODE_KEYS;
            _modeButton.setText(NUM_MODE_LABEL);
        }

        else
        {
            _currentMode = ALPHA_MODE_KEYS;
            _modeButton.setText(ALPHA_MODEL_LABEL);
        }
    }
    void resetKeys()
    {
        int i;
        if(_multiKey==null)
            for (i=0;i<_typeButtons.size();i++)
                _typeButtons.get(i).setText(_currentMode[i]);
        else
        {
            for (i = 0; i < _multiKey.length(); i++)
                _typeButtons.get(i).setText(_multiKey.substring(i, i+1));
            for (;i<_typeButtons.size();i++)
                _typeButtons.get(i).setText("");
        }
    }
    public void setListener(MRKeyboardListner l)
    {
        _l=l;
    }
    public void setTextView(final TextView t)
    {
        _t=new KBTarget()
        {
            TextView tv=t;
            @Override
            public void backSpace()
            {
                String txt = tv.getText().toString();
                if (txt.length() > 0)
                {
                    tv.setText(txt.substring(0, txt.length() - 1));
                }
            }

            @Override
            public void typeText(String txt)
            {
                tv.setText(tv.getText() + txt);
            }
        };

    }
    public void setEditText(final EditText t)
    {
        _t=new KBTarget()
        {
            EditText tv=t;
            @Override
            public void backSpace()
            {
                String txt = tv.getText().toString();
                if(tv.getSelectionEnd()-tv.getSelectionStart()>0)
                {
                    tv.setText(txt.substring(0,tv.getSelectionStart())+txt.substring(tv.getSelectionEnd()));
                }
                else
                {
                    if (txt.length() > 0)
                    {
                        tv.setText(txt.substring(0, txt.length() - 1));
                    }
                }
            }

            @Override
            public void typeText(String txt)
            {
                if(tv.getSelectionEnd()-tv.getSelectionStart()>0)
                {
                    String oldTxt=tv.getText().toString();
                    tv.setText(oldTxt.substring(0,tv.getSelectionStart())+oldTxt.substring(tv.getSelectionEnd()));
                }
                tv.setText(tv.getText() + txt);
            }
        };

    }
    void typeText(String text)
    {
        if (_t != null)
            _t.typeText(text);

        if(_l!=null)
            _l.typeText(text);
    }

    void initButtons(Context c)
    {
        LayoutInflater inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        _okButton = (Button)inflater.inflate(R.layout.kb_cont,null);
        _okButton.setText("Ok");
_okButton.setOnClickListener(new OnClickListener()
{
    @Override
    public void onClick(View view)
    {
        if(_l!=null)
            _l.confirm();
    }
});

        _backSpaceButton = (ImageButton)inflater.inflate(R.layout.kb_backspace,null);
        _backSpaceButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (_l != null)
                    _l.backSpace();
                if (_t != null)
                {
                    _t.backSpace();
                }
            }
        });
        _modeButton = (Button)inflater.inflate(R.layout.kb_cont,null);
        setMode(true);
        _multiKey=null;
        resetKeys();
        _modeButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (_currentMode == ALPHA_MODE_KEYS)
                    setMode(true);
                else
                    setMode(false);
                _multiKey=null;
                resetKeys();
            }
        });

        String[] typeChars=new String[]{"1","2","3","4","5","6","7","8","9",".","0","-"};

        for (int i = 0; i < typeChars.length; i++)
        {
            Button b = (Button)inflater.inflate(R.layout.num_key,null);
            b.setText(typeChars[i]);
            this.addView(b);
            switch (i)
            {
                case 2:
                    addView(_backSpaceButton);
                    break;
                case 5:
                    addView(_okButton);
                    break;
                case 8:
                    addView(_modeButton);
                    break;
            }
            b.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    String keyText=((Button)view).getText().toString();
                    if(keyText.length()==0)
                    {
                        return;
                    }
                    if(keyText.length()>1)
                    {
                        _multiKey = keyText;
                        resetKeys();
                    }
                    else
                    {
                        typeText(keyText);
                        if(_multiKey!=null)
                        {
                            _multiKey=null;
                            resetKeys();
                        }
                    }
                }
            });
            _typeButtons.add(b);
        }





    }

    public MRKeyboard(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initButtons(context);

        /*Button db=new Button(context);
        db.setText("Dyna");
        //db.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
        this.addView(db);
        db=new Button(context);
        db.setText("ss");
        db.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
        this.addView(db);*/
    }



    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom)
    {
        super.onLayout(changed, left, top, right, bottom);
        int c=getChildCount();
        int w=right-left;
        int h=bottom -top;
        for(int i=0;i<c;i++)
        {
            int x = (i % 4) * w / 4;
            int y = (i / 4) * h / 4;

            getChildAt(i).layout(x,y,x+w/4,y+h/4);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int w= MeasureSpec.getSize(widthMeasureSpec);
        int h=w*6/10;
        int c=getChildCount();
        for(int i=0;i<c;i++)
            getChildAt(i).setLayoutParams(new LayoutParams(w/4, h/4));
        super.onMeasure(widthMeasureSpec,heightMeasureSpec);
        setMeasuredDimension(w, h);
    }
}
