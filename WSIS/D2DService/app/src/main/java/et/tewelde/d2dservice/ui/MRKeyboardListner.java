package et.tewelde.d2dservice.ui;

public  interface  MRKeyboardListner
{
    void typeText(String text);
    void backSpace();
    void confirm();
}
