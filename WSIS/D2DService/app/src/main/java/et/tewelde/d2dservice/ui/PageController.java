package et.tewelde.d2dservice.ui;

/**
 * Created by Teweldemedhin Aberra on 3/25/2017.
 */
public abstract class PageController
{
    protected boolean active=true;
    public void activate(){active=true;};
    public void deactivate(){active=false;};
    public void unload(){active=false;};
}
