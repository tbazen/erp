package et.tewelde.d2dservice.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.MRGlobals;
import et.tewelde.d2dservice.util.VelocityEvaluate;

public class PaymentPageController extends WebPageController
{
    ReadingSheetRecord rec=null;
    public PaymentPageController(MainActivity context, MobiReadService service, View contentView, int connectionID)
    {
        super(context, service, contentView, connectionID,R.id.payment_page_webview);
        service.getReadingSheetRecordAsync(activity, this.connectionID
                , new AsyncInterfaces.DoneWithReturn<ReadingSheetRecord>()
        {
            @Override
            public void done(Exception ex, ReadingSheetRecord ret)
            {
                if (ex == null)
                {
                    PaymentPageController.this.rec=ret;
                    web.loadDataWithBaseURL("same://u",VelocityEvaluate.evaluateTemplate(R.raw.payment_page, new ViewModel.PaymentPageModel(ret)), "text/html", "UTF-8",null);
                } else
                    activity.showErrorMessage("Error trying to load payment page.\n" + ex.getMessage());
            }
        });
    }

    @Override
    protected boolean processURL(Uri uri)
    {

        //bill_pay
        if (uri.getPath().equals("/collect_payment"))
        {

            int permissionCheck = ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.SEND_SMS);
            if(permissionCheck!=PackageManager.PERMISSION_GRANTED)
            {
                activity.showErrorMessage("Payment can't be collected because you didn't grant permission");
                return true;
            }
            activity.showConfirmationMessage("Are you sure you want to proceed with the payment, this process is irreversible? Have you received "
                            + MRGlobals.moneyFormat.format(rec.getTotalWithCommission()) + " birr?",
                    new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            service.collectPaymentAsync(activity,connectionID, new AsyncInterfaces.DoneNoReturn()
                            {

                                        @Override
                                        public void done(Exception ex)
                                        {
                                            if(ex==null)
                                            {
                                                activity.returnToHomePage();
                                            }
                                            else
                                                activity.showErrorMessage("Error settling payment.\n"+ex.getMessage());
                                        }
                                    }
                            );
                        }
                    });
            return true;
        }
       return false;
    }
}
