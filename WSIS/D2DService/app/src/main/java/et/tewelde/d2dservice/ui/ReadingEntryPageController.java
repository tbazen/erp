package et.tewelde.d2dservice.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.RequiresPermission;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.CustomField;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.VelocityEvaluate;

/**
 * Created by Teweldemedhin Aberra on 3/25/2017.
 */
public class ReadingEntryPageController extends PageController
{

    //controls
    Spinner spinnerProblem;
    EditText editReading;
    EditText editRemark;
    Button buttonSubmit;
    MainActivity activity;
    MobiReadService service;
    int connectionID;
    public ReadingEntryPageController(MainActivity context, MobiReadService service, View contentView,int conID)
    {
        this.connectionID=conID;
        this.activity=context;
        this.service=service;
        //initialize controls
        initializeControls(context, contentView);
    }


    private void initializeControls(final Context context, View contentView)
    {
        editReading =(EditText)contentView.findViewById(R.id.reading_entry_edit_reading);
        editRemark=(EditText)contentView.findViewById(R.id.reading_entry_edit_remark);
        spinnerProblem =(Spinner)contentView.findViewById(R.id.reading_entry_spinner_problem);
        buttonSubmit=(Button)contentView.findViewById(R.id.reading_entry_button_submit);

        configureProblemSpinner(context);
        configureSubmitButton();
    }
    void recordReading(ReadingSheetRecord rec, final double reading,String remark,int problem)
    {
        service.recordReadingAsync(activity, connectionID,reading, remark, problem,  new AsyncInterfaces.DoneNoReturn()
                {
                    @Override
                    public void done(Exception ex)
                    {
                        if(ex==null)
                        {
                            activity.returnToHomePage();
                        }
                        else
                            activity.showErrorMessage("Error recording reading.\n"+ex.getMessage());
                    }
                }
        );
    }
    void checkRangeAndRecordReading(final double reading, final String remark, final int problem)
    {
        service.getReadingSheetRecordAsync(activity, connectionID, new AsyncInterfaces.DoneWithReturn<ReadingSheetRecord>()
        {
            @Override
            public void done(Exception ex, final ReadingSheetRecord ret)
            {
                if(ret.minReadAllowed!=0 || ret.maxReadAllowed!=0) {
                    if (reading < ret.minReadAllowed || reading > ret.maxReadAllowed) {
                        activity.showConfirmationMessage("You reading is abnormal, do you want to save it?",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        recordReading(ret, reading, remark, problem);
                                    }
                                }
                        );
                    }
                    else
                        recordReading(ret, reading,remark,problem);
                }
                else
                    recordReading(ret, reading,remark,problem);
            }
        });
    }
    private void configureSubmitButton()
    {
        buttonSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String text = editReading.getText().toString().trim();
                boolean hasReading = !StringUtils.isEmpty(text);
                double reading=-1;
                if (hasReading)
                {
                    if (!NumberUtils.isDigits(text))
                    {
                        activity.showErrorMessage("Please enter valid reading");
                        return;
                    }
                    reading=Double.parseDouble(text);
                    if(reading<0)
                    {
                        activity.showErrorMessage("Negative reading not allowed");
                        return;
                    }
                }


                final String remark = editRemark.getText().toString().trim();
                int problem = spinnerProblem.getSelectedItemPosition();
                problem=service.getProblemIDByIndex(problem);
                if (problem == ReadingSheetRecord.MeterReadingProblem_NoProblem && !hasReading)
                {
                    activity.showErrorMessage("Please enter reading");
                    return;
                }

                if (problem == ReadingSheetRecord.MeterReadingProblem_Other && StringUtils.isEmpty(text.trim()))
                {
                    activity.showErrorMessage("Please enter remark");
                    return;
                }

                if(service.locationAvialable())
                {
                    checkRangeAndRecordReading(reading,text,problem);
                }else
                {
                    final double reading_final=reading;
                    final int problem_final=problem;
                    activity.showConfirmationMessage("Location data is not available, are you sure you want to try to save reding without lcoation?"
                            , new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    checkRangeAndRecordReading(reading_final,text,problem_final);
                                }
                            }
                    );
                }

            }
        });
    }


    private void configureProblemSpinner(final Context context)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, R.layout.search_option_item, service.getProblemTypes());
        adapter.setDropDownViewResource(R.layout.search_option_item);
        spinnerProblem.setAdapter(adapter);
    }

    @Override
    public void activate()
    {
        super.activate();
    }

    @Override
    public void deactivate()
    {
        super.deactivate();
    }
}
