package et.tewelde.d2dservice.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.IOException;

import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;

/**
 * Created by Teweldemedhin Aberra on 3/29/2017.
 */
public abstract class ReadingRecordAdapterBase extends BaseAdapter
{
    LayoutInflater mInflater;
    ReadingRecordAdapterBase(Context context)
    {
        mInflater=LayoutInflater.from(context);
    }
    protected abstract ReadingSheetRecord getRec(int position) throws IOException;

    private @NonNull
    View createViewFromResource(@NonNull LayoutInflater inflater, int position,
                                @Nullable View convertView, @NonNull ViewGroup parent) throws IOException
    {
        final View view;

        if (convertView == null) {
            view = inflater.inflate(R.layout.reading_sheet_record, parent, false);
        } else {
            view = convertView;
        }

        ReadingSheetRecord rec=getRec(position);
        String text="<Null>";
        if(rec!=null) text= (position+1)+". "+rec.customerName;
        TextView textView=(TextView)view.findViewById(R.id.reading_sheet_record_text1);
        textView.setText(text);

        if(rec!=null) text=rec.contractNo;
        textView=(TextView)view.findViewById(R.id.reading_sheet_record_text2);
        textView.setText(text);

        if(rec!=null) text=rec.isPaid()? rec.getBillStatusText(null):"";
        textView=(TextView)view.findViewById(R.id.reading_sheet_record_text3);
        textView.setText(text);

        if(rec!=null) text=rec.isRead()? rec.getReadingStatusText():"";
        textView=(TextView)view.findViewById(R.id.reading_sheet_record_text4);
        textView.setText(text);
        return view;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        try
        {
            return createViewFromResource(mInflater, position, convertView, parent);
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public Object getItem(int position)
    {
        try
        {
            return getRec(position);
        } catch (IOException e)
        {
            e.printStackTrace();
            return  null;
        }
    }
    @Override
    public long getItemId(int position)
    {
        ReadingSheetRecord rec= null;
        try
        {
            rec = getRec(position);
        } catch (IOException e)
        {
            e.printStackTrace();
            rec=null;
        }
        if(rec==null)
            return -1;
        return rec.connectionID;
    }
    public void releaseResource()
    {

    }
}
