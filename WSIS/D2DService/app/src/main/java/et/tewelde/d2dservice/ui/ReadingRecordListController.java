package et.tewelde.d2dservice.ui;

import android.view.View;
import android.widget.ListView;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;

/**
 * Created by Teweldemedhin Aberra on 3/29/2017.
 */
public abstract class ReadingRecordListController extends PageController
{
    ListView listView;
    MainActivity activity;
    MobiReadService service;
    protected ReadingRecordAdapterBase adapter=null;
    abstract protected void connectToData();

    public ReadingRecordListController(MainActivity context, MobiReadService service, View contentView,int listViewID)
    {
        this.activity = context;
        this.service = service;
        initializeControls(contentView,listViewID);
    }
    void initializeControls(View v,int listViewID)
    {
        listView=(ListView)v.findViewById(listViewID);
        connectToData();
    }
    @Override
    public void unload()
    {
        listView.setAdapter(null);
        if(adapter!=null)
            adapter.releaseResource();
        super.unload();
    }
}
