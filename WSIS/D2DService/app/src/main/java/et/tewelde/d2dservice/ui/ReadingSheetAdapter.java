package et.tewelde.d2dservice.ui;

import android.content.Context;

import java.io.IOException;

import et.tewelde.d2dservice.MRRepository;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;

public class ReadingSheetAdapter extends ReadingRecordAdapterBase
{

    int lastPosition=-1;
    MRRepository.ReadingSheetCursor cursor;

    ReadingSheetAdapter(Context context, MRRepository.ReadingSheetCursor cursor)
    {
        super(context);
        this.cursor=cursor;

    }

    @Override
    public int getCount()
    {
        return cursor.getNRec();
    }



    @Override
    protected ReadingSheetRecord getRec(int position) throws IOException
    {
        if(position!=lastPosition)
        {
            cursor.move(position - lastPosition);
            lastPosition = position;
        }
        if(position<0 || position>=cursor.getNRec())
            return null;
        return cursor.getObject();
    }


    @Override
    public void releaseResource()
    {
        cursor.close();
    }
}
