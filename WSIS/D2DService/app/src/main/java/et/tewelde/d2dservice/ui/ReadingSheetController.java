package et.tewelde.d2dservice.ui;

import android.view.View;
import android.widget.AdapterView;

import et.tewelde.d2dservice.MRRepository;
import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.util.AsyncInterfaces;

public class ReadingSheetController  extends ReadingRecordListController
{

    public ReadingSheetController(MainActivity context, MobiReadService service, View contentView)
    {
        super(context, service, contentView, R.id.reading_sheet_list_list);
    }
    @Override
    protected void connectToData()
    {
        service.getReadingSheetCursorAsync(activity,ReadingSheetRecord.LIST_READING_SHEET,null, new AsyncInterfaces.DoneWithReturn<MRRepository.ReadingSheetCursor>()
        {
            @Override
            public void done(Exception ex, final MRRepository.ReadingSheetCursor ret)
            {
                if (ex == null)
                {
                    adapter = new ReadingSheetAdapter(activity, ret);
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
                        {
                            Object obj = adapter.getItem(position);
                            if (obj == null)
                                return;
                            activity.showCustomerProfile(((ReadingSheetRecord) obj).connectionID);


                        }

                    });
                } else
                {
                    adapter = null;
                    activity.showErrorMessage("Error loading reading sheet.\n" + ex.getMessage());
                    activity.returnToHomePage();
                }
            }
        });
    }



}
