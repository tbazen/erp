package et.tewelde.d2dservice.ui;

import android.view.View;
import android.widget.AdapterView;

import et.tewelde.d2dservice.MRRepository;
import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.util.AsyncInterfaces;

public class SearchResultController extends ReadingRecordListController
{

    ReadingRecordAdapterBase adapter;
    public SearchResultController(MainActivity context, MobiReadService service, View contentView,MRRepository.ReadingSheetCursor cur)
    {
        super(context, service, contentView, R.id.search_result_list);
        this.adapter = new ReadingSheetAdapter(activity, cur);
        connectToData();
    }
    public SearchResultController(MainActivity context, MobiReadService service, View contentView,ReadingRecordAdapterBase adapter)
    {
        super(context, service, contentView, R.id.search_result_list);
        this.adapter = adapter;
        connectToData();
    }
    @Override
    protected void connectToData()
    {
        if(this.adapter==null)
            return;

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Object obj = adapter.getItem(position);
                if (obj == null)
                    return;
                activity.showCustomerProfile(((ReadingSheetRecord) obj).connectionID);
            }
        });
    }

}
