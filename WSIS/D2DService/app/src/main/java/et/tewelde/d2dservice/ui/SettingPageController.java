package et.tewelde.d2dservice.ui;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.ReadingSheetSync;
import et.tewelde.d2dservice.entity.ConnectionSetting;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.RESTAPIClient;

/**
 * Created by Teweldemedhin Aberra on 3/25/2017.
 */
public class SettingPageController extends PageController
{
    MainActivity activity;
    MobiReadService service;
    TextView textStatus;
    EditText editUrl1, editUrl2, editUN, editPW;
    Button buttonUpdateConPar;
    Button buttonMaintenance;

    public SettingPageController(MainActivity context, MobiReadService service, View contentView)
    {
        this.activity = context;
        this.service = service;
        initializeControls(context, contentView);
    }

    void showSystemStatus(String msg, MobiReadService.SystemStatusType type)
    {

        textStatus.setText(msg);
        switch (type)
        {
            case Ok:
                textStatus.setTextColor(Color.argb(255, 0, 150, 0));
                break;
            case Error:
                textStatus.setTextColor(Color.RED);
                break;
            default:
                textStatus.setTextColor(Color.DKGRAY);
        }
    }

    private void initializeControls(final Context context, View contentView)
    {
        textStatus = (TextView) contentView.findViewById(R.id.setting_text_status);
        editUrl1 = (EditText) contentView.findViewById(R.id.setting_url1);
        editUrl2 = (EditText) contentView.findViewById(R.id.setting_url2);
        editUN = (EditText) contentView.findViewById(R.id.setting_text_username);
        editPW = (EditText) contentView.findViewById(R.id.setting_text_password);

        buttonUpdateConPar = (Button) contentView.findViewById(R.id.setting_button_update_par);
        buttonUpdateConPar.setEnabled(false);
        buttonUpdateConPar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    ConnectionSetting con = new ConnectionSetting();
                    con.wsisUserName = editUN.getText().toString();
                    con.wsisPassword = editPW.getText().toString();
                    con.wsisURL1 = editUrl1.getText().toString();
                    con.wsisURL2 = editUrl2.getText().toString();
                    service.setConnectionParameter(con);
                    buttonUpdateConPar.setEnabled(false);
                } catch (Exception ex)
                {
                    showSystemStatus("Error saving connection parameters.\n" + ex.getMessage(), MobiReadService.SystemStatusType.Error);
                }
            }
        });
        setConParEdit(false);
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    final ConnectionSetting con = service.getConnectionParameter();
                    activity.runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (con != null)
                            {
                                editUN.setText(con.wsisUserName);
                                editPW.setText(con.wsisPassword);
                                editUrl1.setText(con.wsisURL1);
                                editUrl2.setText(con.wsisURL2);
                            }
                            setConParEdit(true);
                            buttonUpdateConPar.setEnabled(false);
                        }
                    });
                } catch (final Exception ex)
                {
                    activity.runOnUiThread(
                            new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    showSystemStatus("Error trying to load connection setting.\n" + ex.getMessage(), MobiReadService.SystemStatusType.Error);
                                    ex.printStackTrace();
                                }
                            });
                }
            }
        }).start();


        TextWatcher editorListener = new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                buttonUpdateConPar.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        };
        editUN.addTextChangedListener(editorListener);
        editPW.addTextChangedListener(editorListener);
        editUrl1.addTextChangedListener(editorListener);
        editUrl2.addTextChangedListener(editorListener);
        ;


        showSystemStatus(service.status_msg_text, service.status_msg_type);
        service.addSystemStatusListener(new MobiReadService.SystemStatusChangeListener()
        {
            @Override
            public void logEvent(Object source, String event, MobiReadService.SystemStatusType type, Object data)
            {

            }

            @Override
            public void setStatus(final Object source, final String status, final MobiReadService.SystemStatusType type, Object data)
            {

                activity.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        showSystemStatus(status, type);
                        if (source instanceof ReadingSheetSync)
                        {
                            ReadingSheetSync sync = (ReadingSheetSync) source;
                            if (sync.getConnectionStatus1() == RESTAPIClient.ConnectionStatus.CONNECTED)
                                editUrl1.setBackgroundColor(Color.GREEN);
                            else if (sync.getConnectionStatus1() == RESTAPIClient.ConnectionStatus.DISCONNECTED)
                                editUrl1.setBackgroundColor(Color.BLUE);
                            else if (sync.getConnectionStatus1() == RESTAPIClient.ConnectionStatus.ERROR)
                                editUrl1.setBackgroundColor(Color.RED);

                            if (sync.getConnectionStatus2() == RESTAPIClient.ConnectionStatus.CONNECTED)
                                editUrl2.setBackgroundColor(Color.GREEN);
                            else if (sync.getConnectionStatus2() == RESTAPIClient.ConnectionStatus.DISCONNECTED)
                                editUrl2.setBackgroundColor(Color.BLUE);
                            else if (sync.getConnectionStatus2() == RESTAPIClient.ConnectionStatus.ERROR)
                                editUrl2.setBackgroundColor(Color.RED);

                        }
                    }
                });
            }
        });

        buttonMaintenance = (Button) contentView.findViewById(R.id.setting_button_maintenance);
        buttonMaintenance.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    activity.showInputBox("Please enter maintenance passcode", "",
                            new MRDialog.InputDone()
                            {
                                @Override
                                public void done(Object tag, String value, boolean canceled)
                                {
                                    if(!"spatniOnly".equals(value))
                                    {
                                        activity.showErrorMessage("Invalid passcode");
                                        return;
                                    }
                                    processMaintenance();
                                }
                            });
                } catch (Exception ex)
                {
                    showSystemStatus("Error.\n" + ex.getMessage(), MobiReadService.SystemStatusType.Error);
                }
            }
        });
    }

    private void processMaintenance()
    {
        activity.showInputBox("Enter maintenance code", "", new MRDialog.InputDone()
        {
            @Override
            public void done(Object tag, String value, boolean canceled)
            {
                if(value==null)
                    return;

                parseMaintenanceCode(value);
            }

        });
    }

    private void parseMaintenanceCode(String code)
    {
        if(code.startsWith("q:"))
        {
            String sql=code.substring("q:".length());
            try
            {
                service.executeQuerySync(activity,sql, new AsyncInterfaces.DoneWithReturn<String>()
                {
                    @Override
                    public void done(Exception ex, String ret)
                    {
                        if(ex==null)
                        {
                            activity.showInformationMessage(ret);
                        }
                        else
                            activity.showErrorMessage("Error running sql:"+ ex.getMessage());
                    }
                });
            }
            catch (Exception ex)
            {
                activity.showErrorMessage("Sql error: " +ex.getMessage() +"\n"+ ex.getClass().toString());
            }
            return;

        }
        switch (code)
        {
            case "clr_rd_ul":
                service.clearReadingUploadFlagsAsync(activity, new AsyncInterfaces.DoneNoReturn()
                {
                    @Override
                    public void done(Exception ex)
                    {
                        if(ex==null)
                            activity.showInformationMessage("Upload flags successfully cleared.");
                        else
                            activity.showErrorMessage("Error clearing upload flags:"+ ex.getMessage());
                    }
                });
                break;
            case "force_current":
                service.forceCurrentReadingCycleAync(activity, new AsyncInterfaces.DoneNoReturn()
                {
                    @Override
                    public void done(Exception ex)
                    {
                        if(ex==null)
                        {
                            activity.showInformationMessage("Reading sheet set to the current period.");

                        }
                        else
                            activity.showErrorMessage("Error setting reading sheet to the current period.:"+ ex.getMessage());
                    }
                });
                break;
            case "show_stg":
                service.getReadingSettingViewAync(activity, new AsyncInterfaces.DoneWithReturn<String>()
                {
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
                    @Override
                    public void done(Exception ex, String ret)
                    {
                        if(ex==null)
                        {
                            activity.showInformationMessage(ret);
                            ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("d2d setting", ret);
                            clipboard.setPrimaryClip(clip);
                        }
                        else
                            activity.showErrorMessage("Error retrieving reading sheet setting:"+ ex.getMessage());
                    }
                });
                break;
            case "rst_data_sync":
                service.clearDataUpdateSyncFlag(activity, new AsyncInterfaces.DoneNoReturn()
                {
                    @Override
                    public void done(Exception ex)
                    {
                        if(ex==null)
                            activity.showInformationMessage("Data update sync reset.");
                        else
                            activity.showErrorMessage("Error clearing upload flags:"+ ex.getMessage());
                    }
                });
                break;
            default:
                activity.showErrorMessage("Can't process maintenance command: " + code);
        }
    }

    private void setConParEdit(boolean enabled)
    {
        editUN.setEnabled(enabled);
        editPW.setEnabled(enabled);
        editUrl1.setEnabled(enabled);
        editUrl2.setEnabled(enabled);
    }
}
