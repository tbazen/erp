package et.tewelde.d2dservice.ui;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.MobiReadService;
import et.tewelde.d2dservice.R;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;
import et.tewelde.d2dservice.entity.ViewModel;
import et.tewelde.d2dservice.util.AsyncInterfaces;
import et.tewelde.d2dservice.util.JSONUtils;
import et.tewelde.d2dservice.util.VelocityEvaluate;

/**
 * Created by Teweldemedhin Aberra on 4/2/2017.
 */
public class WebPageController extends PageController
{
    protected WebView web;
    final protected MainActivity activity;
    protected MobiReadService service;
    protected int connectionID;
    private  int webViewID;
    public WebPageController(MainActivity context, MobiReadService service, View contentView,int connectionID,int webvieID)
    {
        this.activity = context;
        this.service = service;
        this.connectionID=connectionID;
        this.webViewID=webvieID;
        initializeControls(context, contentView);
    }
    protected boolean processURL(android.net.Uri uri)
    {
        return false;
    }
    protected void initializeControls(final Context context, View contentView)
    {
        web = (WebView) contentView.findViewById(webViewID);
        web.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                try
                {
                    android.net.Uri u = android.net.Uri.parse(url);
                    if(processURL(u))
                        return true;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    activity.showErrorMessage(e.getMessage());
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        });

    }
}
