package et.tewelde.d2dservice.util;

import java.io.IOException;

/**
 * Created by Teweldemedhin Aberra on 3/29/2017.
 */
public class AsyncInterfaces
{
    public abstract static class DelegateWithRet<T>
    {
        public T res;
        public abstract void run() throws IOException;
    }
    public abstract static class DelegateNoRet
    {
        public abstract void run() throws Exception;


    }
    public static interface DoneWithReturn<T>
    {
        public void done(Exception ex, T ret);
    }
    public static interface DoneNoReturn
    {
        public void done(Exception ex);
    }
}
