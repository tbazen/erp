package et.tewelde.d2dservice.util;

import android.net.UrlQuerySanitizer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;


import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import et.tewelde.d2dservice.entity.MRReadingSetting;
import et.tewelde.d2dservice.entity.ReadingSheetRecord;

/**
 * Created by Teweldemedhin Aberra on 3/27/2017.
 */
public class JSONUtils
{
    public static String getUrlPar(URL u, String name) throws URISyntaxException
    {
        android.net.Uri ur=android.net.Uri.parse(u.toString());
        return ur.getQueryParameter(name);
    }
    public static String getUrlPar(String u, String name) throws URISyntaxException
    {
        android.net.Uri ur=android.net.Uri.parse(u);
        return ur.getQueryParameter(name);
    }
    public static class JSONRet<RetType>
    {
        public String error;
        public RetType res;
    }

    public static class StringRet extends JSONRet<String>
    {

    }
    public static class IntegerRet extends JSONRet<Integer>
    {

    }
    public static class ReadingSheetRecordRet extends JSONRet<ReadingSheetRecord[]>
    {

    }
    public static class MRReadingSettingRet extends JSONRet<MRReadingSetting>
    {

    }
    public static class VoidRet
    {
        public String error;
        public String res;
    }
    public final static Gson gson=new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();;
    final static HashMap<Class,Object> adapters=new HashMap<Class, Object>();
    public static <T> TypeAdapter<T> getAdapter(Class<T> c)
    {
        synchronized (gson)
        {
            if (adapters.containsKey(c))
                return (TypeAdapter<T>) adapters.get(c);
            TypeAdapter<T> ret = gson.getAdapter(c);
            adapters.put(c, ret);
            return ret;
        }
    }
    public static String toJson(Object obj)
    {
        synchronized (JSONUtils.gson)
        {
            return JSONUtils.gson.toJson(obj);
        }
    }
    public static <T> T fromJson(Class<T> c,String str) throws IOException
    {
        synchronized (JSONUtils.gson)
        {
            return getAdapter(c).fromJson(str);
        }
    }
}
