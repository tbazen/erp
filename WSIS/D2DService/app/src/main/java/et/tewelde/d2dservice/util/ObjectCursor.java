package et.tewelde.d2dservice.util;

import android.database.Cursor;

import java.io.IOException;

/**
 * Created by Teweldemedhin Aberra on 3/26/2017.
 */
public abstract class ObjectCursor<T>
{
    protected Cursor cur;
    protected int n;
    public ObjectCursor(Cursor c,int n)
    {
        this.cur=c;
        this.n=n;
    }
    protected abstract T createObject() throws IOException;
    public T getObject() throws IOException
    {
        int p=cur.getPosition();
        if(p<0 || p>=n)
            return null;
        return createObject();
    }
    public boolean move(int ofs)
    {
        return cur.move(ofs);
    }
    public void close()
    {
        cur.close();
    }

}
