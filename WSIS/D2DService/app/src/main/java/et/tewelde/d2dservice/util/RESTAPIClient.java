package et.tewelde.d2dservice.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Teweldemedhin Aberra on 3/27/2017.
 */
public class RESTAPIClient
{
    public enum ConnectionStatus
    {
        DISCONNECTED,
        CONNECTED,
        ERROR,
    }
    String _url;
    public static class UP
    {
        private String UserName;
        private String Password;
        public String getUserName()
        {
            return UserName;
        }
        public void setUserName(String value)
        {
            UserName =value;
        }
        public String getPassword()
        {
            return Password;
        }
        public  void  setPassword(String value)
        {
            Password =value;
        }
    }

    public RESTAPIClient(String url)
    {
        this._url=url;
    }
    public <RetType>  RetType invokeJSON(String pathAndQuery,Object post,Class<RetType> c) throws Exception
    {
        URL url = new URL(_url + pathAndQuery);
        return invokeJSON(url,post,c);
    }
    public <RetType>  RetType invokeJSON(URL url,Object post,Class<RetType> c) throws Exception
    {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try
        {
            if (post == null)
            {
                urlConnection.setDoOutput(false);
                urlConnection.setRequestMethod("GET");
                urlConnection.setChunkedStreamingMode(0);
            }
            else
            {
                urlConnection.setDoOutput(true);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
            }

            if (post != null)
            {
                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());

                String jsonString = JSONUtils.toJson(post);
                out.write(jsonString.getBytes());
                out.close();
            }

            InputStream inCon=urlConnection.getInputStream();
            InputStream in = new BufferedInputStream(inCon);
            String retStr = new String(org.apache.commons.io.IOUtils.toByteArray(in));
            return JSONUtils.fromJson(c,retStr);

        } finally
        {
            urlConnection.disconnect();
        }
    }

}
