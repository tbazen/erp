package et.tewelde.d2dservice.util;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;

import et.tewelde.d2dservice.MainActivity;
import et.tewelde.d2dservice.R;

/**
 * Created by Teweldemedhin Aberra on 3/29/2017.
 */
public class VelocityEvaluate
{
    static VelocityContext context;
    static MainActivity activity;

    public static void initialize(MainActivity activity)
    {
        Velocity.init();
        VelocityEvaluate.activity = activity;

    }

    public static String evaluateTemplate(int template, Object model)
    {
        VelocityContext context = new VelocityContext();
        context.put("model", model);
        context.put("numberTool", MRGlobals.moneyFormat);
        String style;

        try(InputStream reader= activity.getResources().openRawResource(R.raw.style)
        )
        {
            style=org.apache.commons.io.IOUtils.toString(reader, "UTF-8");
            context.put("style",style);
        }catch (IOException e)
        {
            e.printStackTrace();
            return "Error evaluating template\n" + e.getMessage();
        }
        InputStreamReader reader=null;
        try
        {
            reader= new InputStreamReader(activity.getResources().openRawResource(template));
            StringWriter sw = new StringWriter();
            Velocity.evaluate(context, sw, "temp_template", reader);
            return sw.toString();
        }
        finally
        {
            if(reader!=null)
                try
                {
                    reader.close();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
        }
    }

}
