package et.tewelde.d2dservice.util;

import java.net.URL;

public  class WSISClient extends RESTAPIClient
{
    String sessionID;
    public WSISClient(String url)
    {
        super(url);
    }
    public void login(String userName,String password) throws Exception
    {
        UP un=new UP();
        un.setUserName(userName);
        un.setPassword(password);
        JSONUtils.StringRet ret=invokeJSON("subsc/metermap/login", un, JSONUtils.StringRet.class);
        if(ret.error==null)
            sessionID=ret.res;
        else
            throw new IllegalArgumentException(ret.error);
    }
    public <RetType>  RetType invokeSessionAPI(String pathAndQuery,Object post,Class<RetType> c) throws Exception
    {
        URL url=new URL(super._url+pathAndQuery);
        if(org.apache.commons.lang.StringUtils.isEmpty(url.getQuery()))
        {
            return invokeJSON(pathAndQuery+"?sessionID="+sessionID,post,c);
        }
        return invokeJSON(pathAndQuery+"&sessionID="+sessionID,post,c);
    }

    public void logout() throws Exception
    {
        invokeJSON("subsc/metermap/login?sid="+this.sessionID, null, JSONUtils.VoidRet.class);
    }
}
