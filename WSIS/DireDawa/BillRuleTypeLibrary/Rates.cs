﻿using System;
using System.Collections.Generic;

using System.Text;

namespace INTAPS.SubscriberManagment.DD
{
    [Serializable]
    public class SolidWateDispoalSpecialRates
    {
		public SubscriberManagment.SubscriberType subscType;
        public int subTypeID;
        public double rate;
        
	}
    [Serializable]
    public class SolidWasteDisposalRate
    {
        public INTAPS.UI.ProgressiveRate privateRate = new UI.ProgressiveRate(0);
        public INTAPS.UI.ProgressiveRate institutionalRate = new UI.ProgressiveRate(0);
        public double communityPercentage=0;
        public SolidWateDispoalSpecialRates[] specialSubType = new SolidWateDispoalSpecialRates[0];
        public double waterCompanyShare = 0;
        public int incomeAccountID=-1;
        public int payableAccountID=-1;
        public int incomeAccountSummaryID = -1;
        public int payableAccountSummaryID = -1;
        public bool enable = true;
    }
    
    [Serializable]
    public class DDBillingRate
    {
        public string penalityFormula = "if(D=1,25, if(D=2,40,55))";
    }
}
