﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;

namespace INTAPS.SubscriberManagment.DD
{
    [Serializable]
    [INTAPS.RDBMS.TypeID(101)]
    public class SolidWasteDisposalBillDocument:PeriodicBill
    {
        public BillItem[] billItems;
        public override BillItem[] items
        {
            get { return billItems; }
        }
    }
}
