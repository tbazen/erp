﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.Evaluator;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.WSIS.Job.DD;
using INTAPS.WSIS.Job;

namespace INTAPS.SubscriberManagment.DD
{
    public class BillSymbolProvider : ISymbolProvider
    {
        private double m_eval_A = 0.0;
        private double m_eval_D = 0.0;
        private double m_eval_B = 0.0;
        private double m_eval_R = 0.0;
        private double m_evel_P = 0.0;
        SubscriberManagmentBDE m_parent;
        Subscription _connection;
        public BillSymbolProvider(SubscriberManagmentBDE bde, Subscription connection, double rent, double mainBill)
        {
            this.m_parent = bde;
            _connection = connection;
            this.m_eval_R = rent;
            this.m_eval_B = mainBill;

        }
        #region ISymbolProvider
        public FunctionDocumentation[] GetAvialableFunctions()
        {
            return new FunctionDocumentation[0];
        }

        public EData GetData(URLIden iden)
        {
            return this.GetData(iden.Element.ToUpper());
        }

        public EData GetData(string symbol)
        {
            switch (symbol.ToUpper())
            {
                case "P":
                    return new EData(DataType.Float, this.m_evel_P);

                case "A":
                    return new EData(DataType.Float, this.m_eval_A);
                case "B":
                    return new EData(DataType.Float, this.m_eval_B);

                case "R":
                    return new EData(DataType.Float, this.m_eval_R);

                case "D":
                    return new EData(DataType.Float, this.m_eval_D);

                case "STYPE":
                    return new EData(DataType.Int, (int)this._connection.subscriber.subscriberType);
                case "SCTYPE":
                    return new EData(DataType.Int, (int)this._connection.subscriptionType);
            }
            return EData.Empty;
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            return null;
        }

        public IFunction GetFunction(URLIden iden)
        {
            return this.GetFunction(iden.Element);
        }

        public IFunction GetFunction(string symbol)
        {
            if (CalcGlobal.Functions.Contains(symbol.ToUpper()))
            {
                return (IFunction)CalcGlobal.Functions[symbol.ToUpper()];
            }
            return null;
        }

        public bool SymbolDefined(string Name)
        {
            return (this.GetData(Name).Type != DataType.Empty);
        }
        #endregion

    }
    public class PenalitySymbolProvider : ISymbolProvider
    {
        int _nBills = 0;
        public PenalitySymbolProvider(int nBills)
        {
            this._nBills = nBills;
        }
        #region ISymbolProvider
        public FunctionDocumentation[] GetAvialableFunctions()
        {
            return new FunctionDocumentation[0];
        }

        public EData GetData(URLIden iden)
        {
            return this.GetData(iden.Element.ToUpper());
        }

        public EData GetData(string symbol)
        {
            switch (symbol.ToUpper())
            {
                case "D":
                    return new EData(DataType.Int, _nBills);
            }
            return EData.Empty;
        }

        public FunctionDocumentation GetDocumentation(IFunction f)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(URLIden iden)
        {
            return null;
        }

        public FunctionDocumentation GetDocumentation(string Symbol)
        {
            return null;
        }

        public IFunction GetFunction(URLIden iden)
        {
            return this.GetFunction(iden.Element);
        }

        public IFunction GetFunction(string symbol)
        {
            if (CalcGlobal.Functions.Contains(symbol.ToUpper()))
            {
                return (IFunction)CalcGlobal.Functions[symbol.ToUpper()];
            }
            return null;
        }

        public bool SymbolDefined(string Name)
        {
            return (this.GetData(Name).Type != DataType.Empty);
        }
        #endregion
    }

    public class DDBillingRule : IBillingRule
    {
        SubscriberManagmentBDE m_parent;
        INTAPS.WSIS.Job.JobManagerBDE _bdeJob = null;
        INTAPS.WSIS.Job.JobManagerBDE bdeJob
        {
            get
            {
                if (_bdeJob == null)
                {
                    _bdeJob = INTAPS.ClientServer.ApplicationServer.GetBDE("JobManager") as INTAPS.WSIS.Job.JobManagerBDE;
                }
                return _bdeJob;
            }
        }



        public DDBillingRule(SubscriberManagmentBDE bde)
        {
            this.m_parent = bde;
            foreach (KeyValuePair<int, IReportProcessor> pair in bde.Accounting.ReportProcessors)
            {
                ((ReportProcessorBase)pair.Value).funcs.Add(new IsSWDSpecialCustomerEFunction(bde,bde.Accounting));
            }
        }
        private SingeRateSetting[] GateRates(Subscription sub, BillPeriod p)
        {
            return this.m_parent.GetRates(sub, p);
        }

        bool buildConnectionBills(Subscription connection, BillPeriod period, BWFMeterReading reading, out BillItem[] items, out string msg)
        {

                SingeRateSetting[] settingArray = this.GateRates(connection, period);
                items = null;
                if (settingArray.Length == 0 && reading != null)
                {
                    msg = "No rate that applies to the subscriber found.";
                    return false;
                }
                int i;
                if (settingArray.Length > 1)
                {
                    msg = "More than one rates " + settingArray[0].name;
                    for (i = 1; i < settingArray.Length; i++)
                    {
                        msg = msg + "," + settingArray[i].name;
                    }
                    return false;
                }

                SingeRateSetting setting = settingArray.Length == 0 ? null : settingArray[0];
                INTAPS.RDBMS.SQLHelper dspReader = m_parent.GetReaderHelper();
                try
                {
                    double mainBill = 0.0;
                    double rent = 0.0;
                    if (reading != null && setting != null)
                    {
                        if (reading.consumption < 0.0)
                        {
                            mainBill = 0.0;
                        }
                        else
                        {
                            mainBill = setting.Evaluate(reading.consumption);
                            if (mainBill < 0.0)
                            {
                                mainBill = 0.0;
                            }
                        }
                    }
                    if (setting != null && !string.IsNullOrEmpty(connection.itemCode))
                    {
                        string itemCode = m_parent.GetSubscription(connection.id, period.toDate.Ticks).itemCode;
                        rent = setting.EvaluateRent(itemCode);
                    }

                    List<BillItem> _items = new List<BillItem>();
                    if (!Accounting.AccountBase.AmountEqual(mainBill, 0))
                    {
                        string readingDesc = "";
                        switch (reading.readingType)
                        {
                            case MeterReadingType.MeterReset:
                                readingDesc = "Current Reading: " + Math.Round(reading.reading, 0) + " Use: " + Math.Round(reading.consumption, 0) + " (Meter reset)";
                                break;
                            case MeterReadingType.Average:
                                readingDesc = "Current Reading: " + Math.Round(reading.reading, 0) + " Use: " + Math.Round(reading.consumption, 0) + " (Average)";
                                break;
                            default:
                                readingDesc = Accounting.AccountBase.AmountLess(reading.consumption, 0) ? "(Negative)" :
                                                ("Previous Reading: " + Math.Round(reading.reading-reading.consumption, 0) +
                                                "\nCurrent Reading: " + Math.Round(reading.reading, 0) + " Use: " + Math.Round(reading.consumption, 0));
                                break;
                        }
                        BillItem bi = new BillItem();
                        bi.itemTypeID = 0;
                        bi.hasUnitPrice = false;
                        bi.description =
                            "Water Bill for Contract No: " + connection.contractNo
                            + "\nMonth: " + period.name
                            + "\n" + readingDesc
                            ;
                        bi.price = mainBill;
                        bi.incomeAccountID = m_parent.SysPars.incomeAccount;
                        _items.Add(bi);
                    }
                    if (!Accounting.AccountBase.AmountEqual(rent, 0))
                    {
                        BillItem bi = new BillItem();
                        bi.itemTypeID = 1;
                        bi.hasUnitPrice = false;
                        bi.description = "Rent " + period.name;
                        bi.price = rent;
                        bi.incomeAccountID = m_parent.SysPars.rentIncomeAccount;
                        _items.Add(bi);
                    }
                    msg = null;
                    items = _items.ToArray();
                return true;
            }
            finally
            {
                m_parent.ReleaseHelper(dspReader);
            }
        }

        bool addSolidWasteDisposalBill(Subscriber customer,
           DateTime time,
            BillPeriod period,
            double consumption,
            double mainBill,

            List<CustomerBillRecord> _bills,
           List<CustomerBillDocument> _docs,
           List<BillItem[]> _items,
            out string msg
           )
        {
            if (consumption<0)
                consumption = 0;
            msg = null;
            if(period.fromDate<new DateTime(2014,7,7))
                return true;
            SolidWasteDisposalRate swdRate = this.m_parent.getBillingRuleSetting(typeof(SolidWasteDisposalRate)) as SolidWasteDisposalRate;
            if (!swdRate.enable)
                return true;
            double solidWasteDisposalFee = 0;
            if (swdRate != null)
            {

                double specialRate = 0;
                bool speciallyBilled = false;
                foreach (SolidWateDispoalSpecialRates sr in swdRate.specialSubType)
                {
                    if (sr.subscType == customer.subscriberType && sr.subTypeID == customer.subTypeID)
                    {
                        specialRate = sr.rate;
                        speciallyBilled = true;
                        break;
                    }
                }
                if (speciallyBilled)
                {
                    solidWasteDisposalFee = specialRate;
                }
                else
                {
                    UI.ProgressiveRate prate=null;
                    switch (customer.subscriberType)
                    {
                        case SubscriberType.CommercialInstitution:
                        case SubscriberType.GovernmentInstitution:
                        case SubscriberType.Industry:
                        case SubscriberType.NGO:
                        case SubscriberType.Other:
                        case SubscriberType.Unknown:
                        case SubscriberType.RelegiousInstitution:
                            if (customer.subscriberType == SubscriberType.CommercialInstitution &&
                                customer.subTypeID == 2)
                            {
                                prate = swdRate.privateRate;
                            }
                            else
                                prate = swdRate.institutionalRate;
                            break;
                        case SubscriberType.Private:
                            prate = swdRate.privateRate;
                            break;
                        case SubscriberType.Community:
                            solidWasteDisposalFee = mainBill * swdRate.communityPercentage / 100;
                            break;
                        default:
                            break;
                    }
                    if (prate != null)
                    {
                        bool fnd;
                        solidWasteDisposalFee = prate.findRate(consumption, out fnd);
                        if (!fnd)
                        {
                            msg = "No solid waste disposal rate found";
                            return false;
                        }
                    }
                }
            }
            if (!AccountBase.AmountEqual(solidWasteDisposalFee, 0))
            {
                int billTypeID = m_parent.Accounting.GetDocumentTypeByType(typeof(SolidWasteDisposalBillDocument)).id;
                SolidWasteDisposalBillDocument[] existingSWDBills = m_parent.getBillDocuments(billTypeID, customer.id, -1, period.id, false) as SolidWasteDisposalBillDocument[];
                if (existingSWDBills == null)
                    existingSWDBills = new SolidWasteDisposalBillDocument[0];

                bool generated = false;
                foreach (SolidWasteDisposalBillDocument e in existingSWDBills)
                {
                    if (e.period.id == period.id)
                    {
                        generated = true;
                        break;
                    }
                }
                if (generated)
                {
                    return true;
                }
                CustomerBillRecord billRecord = new CustomerBillRecord();
                billRecord.billDocumentTypeID = billTypeID;
                billRecord.connectionID = -1;
                billRecord.customerID = customer.id;
                billRecord.periodID = period.id;
                billRecord.billDate = time;
                SolidWasteDisposalBillDocument doc = new SolidWasteDisposalBillDocument();
                doc.period = period;
                doc.customer = customer;

                doc.ShortDescription = "Solid Waste Disposal Bill for Customer " + customer.customerCode + " period:" + doc.period.name;
                BillItem bi1 = new BillItem();
                bi1.itemTypeID = 1;
                bi1.hasUnitPrice = false;
                bi1.description = "Solid Waste Disposal Fee for " + period.name;
                bi1.price = solidWasteDisposalFee * (100 - swdRate.waterCompanyShare) / 100;
                bi1.incomeAccountID = swdRate.payableAccountID;
                bi1.accounting = BillItemAccounting.Cash;


                BillItem bi2 = new BillItem();
                bi2.itemTypeID = 2;
                bi2.hasUnitPrice = false;
                bi2.description = "Solid Waste Disposal WSSA Share for " + period.name;
                bi2.price = solidWasteDisposalFee * (swdRate.waterCompanyShare) / 100;
                bi2.incomeAccountID = swdRate.incomeAccountID;
                bi2.accounting = BillItemAccounting.Cash;
                doc.billItems = new BillItem[] { bi1, bi2 };
                _docs.Add(doc);
                _bills.Add(billRecord);
                _items.Add(doc.billItems);
            }
            return true;
        }

        bool addPenalityBill(Subscriber customer,
            Subscription connection,
           DateTime time,
            BillPeriod period,
            double consumption,
            double mainBill,

            List<CustomerBillRecord> _bills,
           List<CustomerBillDocument> _docs,
           List<BillItem[]> _items,
            out string msg)
        {
            msg = null;
            CustomerBillRecord[] bills = m_parent.getBills(-1, customer.id, connection.id, -1);
            List<int> unitLateBillPeriods = new List<int>();
            double existingPenality = 0;
            foreach (CustomerBillRecord bill in bills)
            {
                PeriodicBill pb = m_parent.Accounting.GetAccountDocument(bill.id, true) as PeriodicBill;
                if (pb == null)
                    continue;
                if (pb is PenalityBillDocument && !bill.isPayedOrDiffered)
                {
                        existingPenality += pb.total;
                    continue;
                }
                if (bill.isPayedOrDiffered)
                    continue;
                if (pb.period.fromDate < period.fromDate)
                {
                    if (!unitLateBillPeriods.Contains(pb.period.id))
                        unitLateBillPeriods.Add(pb.period.id);
                }
            }
            if (unitLateBillPeriods.Count == 0)
                return true;
            DDBillingRate penalityRate = this.m_parent.getBillingRuleSetting(typeof(DDBillingRate)) as DDBillingRate;

            
            if (penalityRate==null)
            {
                msg= "Penality formula not set";
                return false;
            }
            
            Symbolic symbolic = new Symbolic();
            symbolic.SetSymbolProvider(new PenalitySymbolProvider(unitLateBillPeriods.Count));
            symbolic.Expression = penalityRate.penalityFormula;
            EData data = symbolic.Evaluate();
            if (data.Type == DataType.Error)
            {
                msg= "Error evaluation penality " + data.Value;
                return false;
            }
            if (data.Type != DataType.Float)
            {
                msg= "Error evaluation penality didn't return a numeric value.";
                return false;
            }
            double calculatedPenality = Math.Ceiling((double)data.Value);
            if (AccountBase.AmountLess(existingPenality, calculatedPenality))
            {
                int billTypeID = m_parent.Accounting.GetDocumentTypeByType(typeof(PenalityBillDocument)).id;
                CustomerBillRecord billRecord = new CustomerBillRecord();
                billRecord.billDocumentTypeID = billTypeID;
                billRecord.connectionID = connection.id;
                billRecord.customerID = customer.id;
                billRecord.periodID = period.id;
                billRecord.billDate = time;
                PenalityBillDocument doc = new PenalityBillDocument();
                doc.period = period;
                doc.customer = customer;

                doc.ShortDescription = "Penality Bill for contract no " + connection.contractNo+ " period:" + doc.period.name;
                BillItem bi1 = new BillItem();
                bi1.itemTypeID = 1;
                bi1.hasUnitPrice = false;
                bi1.description = "Penality for " + period.name;
                bi1.price = calculatedPenality - existingPenality;
                bi1.price = Math.Ceiling(bi1.price * 100) / 100;
                bi1.incomeAccountID = m_parent.SysPars.billPenalityIncomeAccount;
                bi1.accounting = BillItemAccounting.Invoice;

                doc.theItems = new BillItem[] { bi1 };
                _docs.Add(doc);
                _bills.Add(billRecord);
                _items.Add(doc.items);
            }
            else if (AccountBase.AmountGreater(existingPenality, calculatedPenality))
            {
                INTAPS.ClientServer.ApplicationServer.EventLoger.Log(ClientServer.EventLogType.Errors, "The penality bills that are already generated are greater than the calculated penality. Contract no:"+connection.contractNo);
            }
            return true;
        }
        
        public bool buildMonthlyBill(Subscriber customer,DateTime time, BillPeriod period, out CustomerBillDocument[] billDocs, out CustomerBillRecord[] bills, out BillItem[][] items, out string msg)
        {
            billDocs = null;
            bills = null;
            items = null;
            List<CustomerBillRecord> _bills     = new List<CustomerBillRecord>();
            List<CustomerBillDocument> _docs    = new List<CustomerBillDocument>();
            List<BillItem[]> _items             = new List<BillItem[]>();
            CustomerBillRecord[] existing;
            //generate main bill
            double totalConsumption=0;
            double totalMainBill=0;
            int billTypeID = m_parent.Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
            Subscription[] connections = m_parent.GetSubscriptions(customer.id, period.toDate.Ticks);
            foreach (Subscription connection in connections)
            {
                connection.subscriber = customer;
                existing = m_parent.getBills(billTypeID, -1, connection.id, period.id);
                if (existing.Length == 0)
                {
                    if (m_parent.GetSubscriptionStatusAtDate(connection.id, period.toDate) == SubscriptionStatus.Active)
                    {
                        BWFMeterReading reading = m_parent.BWFGetMeterReadingByPeriod(connection.id, period.id);
                        if (reading != null && reading.bwfStatus == BWFStatus.Unread)
                            reading = null;
                        if(reading!=null)
                            totalConsumption+=reading.consumption;
                        CustomerBillRecord mainBill = new CustomerBillRecord();
                        mainBill.billDocumentTypeID = billTypeID;
                        mainBill.connectionID = connection.id;
                        mainBill.customerID = connection.subscriberID;
                        mainBill.periodID = period.id;
                        mainBill.billDate = time;
                        BillItem[] thisItems;
                        if (!connection.prePaid)
                        {
                            bool r = this.buildConnectionBills(connection, period, reading, out thisItems, out msg);
                            if (!r)
                                return false;
                            if (thisItems.Length > 0)
                            {
                                WaterBillDocument doc = new WaterBillDocument();
                                doc.period = period;
                                doc.customer = customer;
                                doc.subscription = connection;
                                doc.reading = reading;
                                doc.waterBillItems = thisItems;
                                doc.ShortDescription = "Water bill for connection " + doc.subscription.contractNo + " period:" + doc.period.name;


                                _items.Add(thisItems);
                                _bills.Add(mainBill);
                                _docs.Add(doc);

                                foreach (BillItem bi in thisItems)
                                    totalMainBill += bi.price;
                            }
                        }
                    }
                }
                else
                {
                    foreach(CustomerBillRecord e in existing)
                    {
                        foreach(BillItem bi in m_parent.getCustomerBillItems(e.id))
                            totalMainBill+=bi.price;
                    }
                }
                if (!addPenalityBill(customer,connection, time, period, totalConsumption, totalMainBill, _bills, _docs, _items, out msg))
                    return false;
            }
            //add solid waste disposal bill
            billTypeID = m_parent.Accounting.GetDocumentTypeByType(typeof(SolidWasteDisposalBillDocument)).id;
            existing = m_parent.getBills(billTypeID, customer.id, -1, period.id);
            if (existing.Length == 0)
            {
                if (!addSolidWasteDisposalBill(customer, time, period, totalConsumption, totalMainBill, _bills, _docs, _items, out msg))
                    return false;
            }

            
            billDocs = _docs.ToArray();
            bills = _bills.ToArray();
            items = _items.ToArray();
            msg = null;
            return true;
        }

        public double getExageratedReading(int connectionID)
        {
            return 0;
        }


        public BillingRuleSettingType[] getSettingTypes()
        {
            return new BillingRuleSettingType[]{
                new BillingRuleSettingType(){name="Solid Waste Disposal",type=typeof(SolidWasteDisposalRate)}
                ,new BillingRuleSettingType(){name="Dire Dawa Penality Rate",type=typeof(DDBillingRate)}
            };
        }




        public void fixSubscriptionDataForSave(Subscription subsc)
        {
            subsc.prePaid = false;
            INTAPS.WSIS.Job.DD.DDEstimationConfiguration config = bdeJob.getConfiguration(StandardJobTypes.NEW_LINE, typeof(DDEstimationConfiguration)) as DDEstimationConfiguration;
            if (config != null)
            {
                if(!string.IsNullOrEmpty(subsc.itemCode))
                    subsc.prePaid= bdeJob.contains(bdeJob.expandItemsList(config.preaPaidWaterMeterItems), subsc.itemCode);
            }
        }
    }
}