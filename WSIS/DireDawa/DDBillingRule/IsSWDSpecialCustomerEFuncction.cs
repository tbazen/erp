using INTAPS.Accounting;
using INTAPS.Evaluator;
using INTAPS.Accounting.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using System;
using System.Data;
using INTAPS.SubscriberManagment.BDE;
namespace INTAPS.SubscriberManagment.DD
{

    public class IsSWDSpecialCustomerEFunction : IFunction
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public IsSWDSpecialCustomerEFunction(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            int customerID = (int)Pars[0].Value;
            Subscriber subsc= m_subsc.GetSubscriber(customerID);
            SolidWasteDisposalRate rate=m_subsc.getBillingRuleSetting(typeof(SolidWasteDisposalRate)) as SolidWasteDisposalRate;
            if (rate == null)
                throw new ClientServer.ServerUserMessage("Solid Waste Disposal Rate not Set");
            bool result = false;
            foreach(SolidWateDispoalSpecialRates sr in rate.specialSubType)
            {
                if (sr.subTypeID == subsc.subTypeID && sr.subscType == subsc.subscriberType)
                {
                    result = true;
                    break;
                }
            }

            return new EData(DataType.Bool, result);
        }

        public string Name
        {
            get
            {
                return "Is Solid Waste Disposal Special Customer";
            }
        }

        public int ParCount
        {
            get
            {
                return 1;
            }
        }

        public string Symbol
        {
            get
            {
                return "SWDIsSpecial";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}