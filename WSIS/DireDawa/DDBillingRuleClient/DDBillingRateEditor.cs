﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.DD
{
    public partial class DDBillingRateEditor : Form
    {

        public DDBillingRateEditor()
        {
            InitializeComponent();
        }

        public DDBillingRateEditor(DDBillingRate rate)
            : this()
        {
            if (rate == null)
                return;
            textFormula.Text = rate.penalityFormula;
        }


        

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                DDBillingRate rate = new DDBillingRate();
                rate.penalityFormula = textFormula.Text.Trim();

                SubscriberManagment.Client.SubscriberManagmentClient.setBillingRuleSetting(typeof(DDBillingRate), rate);
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
    }
}
