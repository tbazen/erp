﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment.DD
{
    public class DDBillingRuleClient:SubscriberManagment.Client.IBillingRuleClient
    {
        public DDBillingRuleClient(ISubscriberManagmentService client, string sessionID)
        {
        }
        public void showSettingEditor(Type type, object setting)
        {
            if (type == typeof(SolidWasteDisposalRate))
            {
                SolidWasteDisposalRateEditor form = new SolidWasteDisposalRateEditor(setting as SolidWasteDisposalRate);
                form.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
            else
            {
                DDBillingRateEditor form = new DDBillingRateEditor(setting as DDBillingRate);
                form.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
        }

        public System.Resources.ResourceManager getStringTable(int languageID)
        {
            return StringTable.ResourceManager;
        }


        public void printReceipt(CustomerPaymentReceipt receipt, Client.PrintReceiptParameters pars,bool reprint,int nCopies)
        {
            if (nCopies != 3) //three copies for customer service bills
                new ReceiptPrinter(receipt, pars).PrintNow(reprint, 1);
            else
                new ReceiptPrinter(receipt, pars).PrintNow(reprint, 2);
        }
    }
   
}
