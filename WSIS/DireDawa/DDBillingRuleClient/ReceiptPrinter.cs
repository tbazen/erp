
using INTAPS.Accounting;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using System.Collections.Generic;
using INTAPS.SubscriberManagment.Client;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment.DD
{
    public partial class ReceiptPrinter 
    {
        class StringDrawItem
        {
            public Font f;
            public string text;
            public StringAlignment alignment;
            public float width;
            public float height;
            public float drawWidth;
            public bool placeHolder = false;
            public StringDrawItem(Graphics g, Font f, string text, StringAlignment a, float width)
            {
                SizeF ef = g.MeasureString(text, f,new SizeF(width,1000));
                this.width   = ef.Width;
                this.height = ef.Height;
                this.f = f;
                this.text = text;
                this.alignment = a;
                this.drawWidth = width;

            }
            public StringDrawItem(Graphics g, Font f, string text, StringAlignment a, float width,bool placeHolder)
                :this(g,f,text,a,width)
            {
                this.placeHolder = placeHolder;
            }
        }
        
        const float TOP_MARGIN = 0.08f;
        const float BOTTOM_MARGIN = 0.03f;
        const float LEFT_MARGIN = 0.05f;
        const float RIGHT_MARGIN = 0.1f;
        const float VERTICAL_MARGIN_SMALL = 0.01f;
        const float VERTICAL_MARGIN_LARGE = 0.03f;

        private static Font HeaderTitleFont1 = new Font("Arial", 12f, FontStyle.Bold);
        private static Font HeaderTitleFont2 = new Font("Arial", 12f, FontStyle.Bold);
        private static Font HeaderTitleFont3 = new Font("Arial", 10f);
        private static Font SerialFont = new Font("Arial", 12f, FontStyle.Bold);
        private static Font DateFont = new Font("Arial", 12f, FontStyle.Bold);
        private static Font TimeFont = new Font("Arial", 8f, FontStyle.Bold);

        private static Font FooterTextFont1 = new Font("Arial", 8f);
        private static Font FooterTextFont2 = new Font("Arial", 8f, FontStyle.Italic);
        private static Font CasherNameFont = new Font("Arial", 10f);
        private static Font CasherLabelFont=new Font("Arial",10f,FontStyle.Bold);

        private static Font CustomerCodeFont = new Font("Arial", 10f);
        private static Font NameFont = new Font("Arial", 10f);
        

        private static Font ItemGridHeaderFont = new Font("Arial", 10f, FontStyle.Bold);
        private static Font ItemFont = new Font("Arial", 8f, FontStyle.Italic);
        private static Font AmountFont = new Font("Arial", 8f);
        private static Font TotalFont = new Font("Arial", 10f, FontStyle.Bold);
        private static Font TotalLabelFont = new Font("Arial", 10f, FontStyle.Bold);

        private static Font PageFont = new Font("Arial", 10f, FontStyle.Bold);

        static Pen LinePen = new Pen(Color.Black, 1f);
        
        private PrintDocument m_printDoc = new PrintDocument();
        private CustomerPaymentReceipt m_receipt;
        bool quantityColumn = false;
        PrintReceiptParameters _pars;
        public int m_NumberOfCopies = 0;

        class DrawData:ReceiptPrinter
        {
            public StringDrawItem itemMainTitle;
            public StringDrawItem itemSubTitile;
            public StringDrawItem itemReprint;
            public StringDrawItem itemCopyHeader;
            public StringDrawItem itemCustomerCode;
            public StringDrawItem itemCustomerName;

            public StringDrawItem itemFooterOne;
            public StringDrawItem itemFooterTwo;
            public StringDrawItem itemCasheirLabel;
            public StringDrawItem itemCasheirName;
            public StringDrawItem itemSignature;

            public StringDrawItem itemSerialNo;
            public StringDrawItem itemDate;
            public StringDrawItem itemTime;
            public List<List<List<StringDrawItem>>> pages;
            public float hres;
            public float vres;
            public float printWidth;
            public float halfPrintWidth;
            public float printHeight;
            public bool prePrinted=false;
            public ReceiptPrinter _printer;
            public DrawData(ReceiptPrinter printer, PrintPageEventArgs e, CustomerPaymentReceipt receipt,bool reprint)
            {
                _printer = printer;
                string casheirName = _printer._pars.casheirName;
                
                hres = e.PageSettings.PrinterResolution.X;
                vres = e.PageSettings.PrinterResolution.Y;
                Console.WriteLine(string.Format("Printer name:{0} h:{1} v:{2}", e.PageSettings.PrinterSettings.PrinterName, e.PageSettings.PrinterResolution.X, e.PageSettings.PrinterResolution.Y));
                if (vres == 0)
                    vres = hres;
                Console.WriteLine(string.Format("Printer name:{0} h:{1} v:{2}", e.PageSettings.PrinterSettings.PrinterName, e.PageSettings.PrinterResolution.X, e.PageSettings.PrinterResolution.Y));

                float _pageWidth = (float)e.PageBounds.Width / (float)hres;
                float _pageHeight = (float)e.PageBounds.Height / (float)vres;

                printWidth = _pageWidth -  LEFT_MARGIN-RIGHT_MARGIN;
                halfPrintWidth = printWidth / 2;
                printHeight = _pageHeight - TOP_MARGIN - BOTTOM_MARGIN;
                prePrinted = "true".Equals( System.Configuration.ConfigurationManager.AppSettings["PrePrintedReceipt"],StringComparison.CurrentCultureIgnoreCase);
                string mainTitle = printer._pars.mainTitle;
                string subTitle = StringTable.ResourceManager.GetString("Receipt_Title2")+(reprint?"\n(Reprint)":"");
                string reprintReceipt = reprint ? "(Reprint)" : "";
                string copyHeader = m_NumberOfCopies == 0 ? "(Original)" : String.Format("(Copy {0})", m_NumberOfCopies);
                string mainFooter = StringTable.ResourceManager.GetString("Receipt_Footer1");
                string subFooter = StringTable.ResourceManager.GetString("Receipt_Footer2");

                itemMainTitle = new StringDrawItem(e.Graphics, HeaderTitleFont1, mainTitle, StringAlignment.Center, printWidth * hres,prePrinted);
                itemSubTitile = new StringDrawItem(e.Graphics, HeaderTitleFont2, subTitle, StringAlignment.Center, printWidth * hres,prePrinted);
                itemReprint = new StringDrawItem(e.Graphics, HeaderTitleFont2, reprintReceipt, StringAlignment.Center, printWidth * hres, false);
                if (!reprint)
                    itemCopyHeader = new StringDrawItem(e.Graphics, HeaderTitleFont2, copyHeader, StringAlignment.Center, printWidth * hres, false);

                itemCustomerCode = new StringDrawItem(e.Graphics, CustomerCodeFont, StringTable.ResourceManager.GetString("Customer Code") + ": " + receipt.customer.customerCode, StringAlignment.Near, printWidth * hres);
                itemCustomerName = new StringDrawItem(e.Graphics, CustomerCodeFont, StringTable.ResourceManager.GetString("Name") + ": " + receipt.customer.name, StringAlignment.Near, printWidth * hres);

                itemFooterOne = new StringDrawItem(e.Graphics, FooterTextFont1, mainFooter, StringAlignment.Near, printWidth * hres,prePrinted);
                itemFooterTwo = new StringDrawItem(e.Graphics, FooterTextFont2, subFooter, StringAlignment.Near, printWidth * hres, prePrinted);
                itemCasheirLabel = new StringDrawItem(e.Graphics, CasherLabelFont, StringTable.ResourceManager.GetString("Casheir") + ": ", StringAlignment.Near, printWidth * hres / 2);
                itemCasheirName = new StringDrawItem(e.Graphics, CasherNameFont, casheirName, StringAlignment.Near, printWidth * hres - itemCasheirLabel.width);
                itemSignature = new StringDrawItem(e.Graphics, CasherLabelFont, StringTable.ResourceManager.GetString("Signature") + ": ", StringAlignment.Near, printWidth * hres);

                itemSerialNo = new StringDrawItem(e.Graphics, SerialFont, StringTable.ResourceManager.GetString("No") + ": " + (receipt.receiptNumber == null ? "0000000" : receipt.receiptNumber.reference), StringAlignment.Far, printWidth * hres);
                itemDate = new StringDrawItem(e.Graphics, SerialFont, StringTable.ResourceManager.GetString("Date") + ": " + INTAPS.Ethiopic.EtGrDate.ToEth(receipt.DocumentDate).ToString(), StringAlignment.Far, printWidth * hres);
                itemTime = new StringDrawItem(e.Graphics, TimeFont, StringTable.ResourceManager.GetString("Time") + ": " + (receipt.DocumentDate).ToString("hh:MM tt"), StringAlignment.Far, printWidth * hres);


                List<StringDrawItem> headerRow = new List<StringDrawItem>();
                
                headerRow.Add(new StringDrawItem(e.Graphics, ItemGridHeaderFont, StringTable.ResourceManager.GetString("Item"), StringAlignment.Near, printWidth * hres / 4));
                headerRow.Add(new StringDrawItem(e.Graphics, ItemGridHeaderFont, StringTable.ResourceManager.GetString("Unit Amount"), StringAlignment.Near, printWidth * hres / 4, !_printer.quantityColumn));
                headerRow.Add(new StringDrawItem(e.Graphics, ItemGridHeaderFont, StringTable.ResourceManager.GetString("Quantity"), StringAlignment.Near, printWidth * hres / 4, !_printer.quantityColumn));
                headerRow.Add(new StringDrawItem(e.Graphics, ItemGridHeaderFont, StringTable.ResourceManager.GetString("Amount"), StringAlignment.Far, printWidth * hres / 4));
                float headerHeight = 0;
                foreach (StringDrawItem item in headerRow)
                    if (headerHeight < item.height)
                        headerHeight = item.height;
                float copyHeaderHeight = !reprint ? itemCopyHeader.height - VERTICAL_MARGIN_SMALL * vres : 0;
                float gridHeight = printHeight * vres
                    - itemMainTitle.height - VERTICAL_MARGIN_SMALL * vres
                    - itemSubTitile.height - VERTICAL_MARGIN_SMALL * vres
                    - itemReprint.height - VERTICAL_MARGIN_SMALL * vres
                    - copyHeaderHeight
                    - itemSerialNo.height - VERTICAL_MARGIN_SMALL * vres
                    - itemDate.height - VERTICAL_MARGIN_SMALL * vres
                    - itemTime.height - VERTICAL_MARGIN_SMALL * vres
                    - itemCustomerCode.height - VERTICAL_MARGIN_SMALL * vres
                    - itemCustomerName.height - VERTICAL_MARGIN_LARGE * vres

                    - Math.Max(itemCasheirLabel.height, itemCasheirName.height) - VERTICAL_MARGIN_LARGE * vres
                    - itemSignature.height - VERTICAL_MARGIN_SMALL * vres
                    - itemFooterOne.height - VERTICAL_MARGIN_SMALL * vres
                    - itemFooterTwo.height - VERTICAL_MARGIN_SMALL * vres
                    - headerHeight - VERTICAL_MARGIN_SMALL * vres;






                pages = new List<List<List<StringDrawItem>>>();
                List<List<StringDrawItem>> currentPage = null;
                float pageHeight = 0;
                double total = 0;
                int itemNo = 1;
                foreach (BillItem item in receipt.billItems)
                {
                    string desc = item.description;
                    desc += "\nBill amount:" + AccountBase.FormatAmount(item.price);
                    if (item.partiallySettledByDebosit)
                        desc += "\nSettled from Deposit:" + AccountBase.FormatAmount(item.settledFromDepositAmount);
                    StringDrawItem descItem = new StringDrawItem(e.Graphics, ItemFont, itemNo + ". " + desc, StringAlignment.Near, printWidth * hres);
                    itemNo++;
                    List<StringDrawItem> itemRow = new List<StringDrawItem>();
                    itemRow.Add(new StringDrawItem(e.Graphics, AmountFont, item.hasUnitPrice ? AccountBase.FormatAmount(item.unitPrice) : "", StringAlignment.Far, printWidth * hres / 3));
                    itemRow.Add(new StringDrawItem(e.Graphics, AmountFont, item.hasUnitPrice ? item.quantity.ToString("0.0") : "", StringAlignment.Far, printWidth * hres / 3));
                    itemRow.Add(new StringDrawItem(e.Graphics, AmountFont, AccountBase.FormatAmount(item.netPayment), StringAlignment.Far, printWidth * hres / 3));
                    total += item.netPayment;
                    double itemHeight = 0;
                    foreach (StringDrawItem it in itemRow)
                    {
                        if (itemHeight < it.height)
                            itemHeight = it.height;
                    }
                    double thisRowHeight = descItem.height + itemHeight + VERTICAL_MARGIN_SMALL * this.vres;
                    if (thisRowHeight + pageHeight > gridHeight)
                    {
                        if (currentPage != null)
                            pages.Add(currentPage);
                        currentPage = null;
                    }
                    if (currentPage == null)
                    {
                        currentPage = new List<List<StringDrawItem>>();
                        currentPage.Add(headerRow);
                        pageHeight = headerHeight;
                    }
                    currentPage.Add(new List<StringDrawItem>(new StringDrawItem[] { descItem }));
                    currentPage.Add(itemRow);
                    pageHeight += (float)thisRowHeight;
                }

                StringDrawItem totalItem = new StringDrawItem(e.Graphics, TotalFont, StringTable.ResourceManager.GetString("Total") + ": " + AccountBase.FormatAmount(total), StringAlignment.Far, printWidth * hres);
                if (totalItem.height + VERTICAL_MARGIN_SMALL * this.vres + pageHeight > gridHeight)
                {
                    if (currentPage != null)
                        pages.Add(currentPage);
                    currentPage = null;
                }
                if (currentPage == null)
                {
                    currentPage = new List<List<StringDrawItem>>();
                    currentPage.Add(headerRow);
                    pageHeight = headerHeight;
                }
                currentPage.Add(new List<StringDrawItem>(new StringDrawItem[] { totalItem }));
                pages.Add(currentPage);
            }
        }
        
        public ReceiptPrinter()
        {

        }
        public ReceiptPrinter(CustomerPaymentReceipt receipt,PrintReceiptParameters pars)
        {
            this.m_receipt = receipt;
            _pars = pars;
            this.m_printDoc.PrintPage += new PrintPageEventHandler(this.PrintPage);
            this.m_printDoc.EndPrint += new PrintEventHandler(this.m_printDoc_EndPrint);
            this.m_printDoc.DefaultPageSettings.Margins.Left = 30;
            this.m_printDoc.DefaultPageSettings.Margins.Right = 30;
            this.m_printDoc.DefaultPageSettings.Margins.Bottom = 30;
            this.m_printDoc.DefaultPageSettings.Margins.Top = 30;
            this.quantityColumn = false;
            foreach (BillItem item in this.m_receipt.billItems)
            {
                if(item.hasUnitPrice)
                {
                    this.quantityColumn = true;
                    break;
                }
            }
        }

        private void m_printDoc_EndPrint(object sender, PrintEventArgs e)
        {
        }

       
        public void PrintNow(bool reprint,int nCoopies)
        {
            _reprint = reprint;

            if (reprint)
                nCoopies = 1;

            if (!this.m_printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Invalid printer setting.");
            }
            for (int i = 0; i < nCoopies; i++)
            {
                pageNumber = 0;
                this.m_NumberOfCopies = i;
                this.m_printDoc.Print();
            }
        }
       
        void drawLine(Graphics g, float y)
        {
            try
            {
                g.DrawLine(LinePen, LEFT_MARGIN * data.hres, y, LEFT_MARGIN * data.hres + data.printWidth * data.hres, y);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Draw Line: y:{0} \nError{1}",y, ex.Message));
            }
        }
        void drawStringItem(Graphics g, StringDrawItem item,float x,float y)
        {
            StringFormat sf = new StringFormat();
            sf.Alignment = item.alignment;
            if(!item.placeHolder)
                try
                {
                    g.DrawString(item.text, item.f, Brushes.Black, new RectangleF(x, y, item.drawWidth, data.printHeight * data.vres), sf);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(string.Format("Draw string:x{0} y:{1}\n h:{2} v:{3}\nText:{4} \nError{5}",x,y,data.printHeight,data.vres,item.text,ex.Message));
                }
            //DEBUG Code
            //g.DrawRectangle(Pens.Black, new Rectangle((int)x, (int)y, (int)item.drawWidth, (int)item.height));
        }
        DrawData data = null;
        int pageNumber = 0;
        bool _reprint;
        private void PrintPage(object sender, PrintPageEventArgs e)
        {
            if (data == null)
                data = new DrawData(this,e, m_receipt,_reprint);

            
            float x=LEFT_MARGIN*data.hres;
            float y=TOP_MARGIN*data.hres;
            
            //headers
            drawStringItem(e.Graphics, data.itemMainTitle, x, y);
            y += data.itemMainTitle.height + VERTICAL_MARGIN_SMALL * data.vres;

            drawStringItem(e.Graphics, data.itemSubTitile, x, y);
            y += data.itemSubTitile.height + VERTICAL_MARGIN_SMALL * data.vres / 2;

            drawStringItem(e.Graphics, data.itemReprint, x, y);
            y += data.itemReprint.height + VERTICAL_MARGIN_SMALL * data.vres / 2;

            if (!_reprint)
            {
                if (m_NumberOfCopies > 0)
                    data.itemCopyHeader.text = String.Format("(Copy {0})", m_NumberOfCopies);

                drawStringItem(e.Graphics, data.itemCopyHeader, x, y);
                y += data.itemCopyHeader.height + VERTICAL_MARGIN_SMALL * data.vres / 2;
            }
            if(!data.prePrinted)
                drawLine(e.Graphics, y);
            y += VERTICAL_MARGIN_SMALL * data.vres / 2;

            drawStringItem(e.Graphics, data.itemSerialNo, x, y);
            y += data.itemSerialNo.height + VERTICAL_MARGIN_SMALL * data.vres;

            drawStringItem(e.Graphics, data.itemDate, x, y);
            y += data.itemDate.height + VERTICAL_MARGIN_SMALL * data.vres;

            drawStringItem(e.Graphics, data.itemTime, x, y);
            y += data.itemTime.height + VERTICAL_MARGIN_SMALL * data.vres;

            drawStringItem(e.Graphics, data.itemCustomerCode, x, y);
            y += data.itemCustomerCode.height + VERTICAL_MARGIN_SMALL * data.vres;

            drawStringItem(e.Graphics, data.itemCustomerName, x, y);
            y += data.itemCustomerName.height + VERTICAL_MARGIN_LARGE * data.vres;
            
            //items
            List<List<StringDrawItem>> page = data.pages[pageNumber];
            int rowIndex=0;
            foreach (List<StringDrawItem> row in page)
            {
                float itemHeight = 0;
                foreach (StringDrawItem cell in row)
                {
                    if (itemHeight < cell.height)
                        itemHeight = cell.height;
                }
                float itemX = x;
                foreach (StringDrawItem cell in row)
                {
                    drawStringItem(e.Graphics, cell, itemX, y + (itemHeight - cell.height));
                    itemX += cell.drawWidth;
                }
                y += itemHeight + VERTICAL_MARGIN_SMALL*data.vres/2;
                if((pageNumber<data.pages.Count-1 || rowIndex<page.Count-1) && rowIndex%2==0)
                    drawLine(e.Graphics, y);
                y += VERTICAL_MARGIN_SMALL * data.vres / 2;
                rowIndex++;
            }

            //footers
            y = data.printHeight* data.vres;
            y -= data.itemFooterTwo.height;
            drawStringItem(e.Graphics, data.itemFooterTwo, x, y);

            y -= data.itemFooterOne.height + VERTICAL_MARGIN_SMALL * data.vres;
            drawStringItem(e.Graphics, data.itemFooterOne, x, y);

            y -= VERTICAL_MARGIN_SMALL * data.vres / 2;
            if(!data.prePrinted)
                drawLine(e.Graphics, y);

            y -= data.itemSignature.height + VERTICAL_MARGIN_SMALL * data.vres / 2;
            drawStringItem(e.Graphics, data.itemSignature, x, y);

            float casherHeight = Math.Max(data.itemCasheirLabel.height, data.itemCasheirName.height);
            y -= casherHeight + VERTICAL_MARGIN_SMALL * data.vres;
            drawStringItem(e.Graphics, data.itemCasheirLabel, x, y + (casherHeight - data.itemCasheirLabel.height));
            drawStringItem(e.Graphics, data.itemCasheirName, x + data.itemCasheirLabel.width, y + (casherHeight - data.itemCasheirName.height));
            if (data.pages.Count > 1)
            {
                StringDrawItem itemPage = new StringDrawItem(e.Graphics, PageFont, "Page " + (pageNumber + 1) + " of " + data.pages.Count, StringAlignment.Far, data.printWidth * data.hres);
                y = data.printHeight * data.vres;
                y -= itemPage.height;
                drawStringItem(e.Graphics, itemPage, x, y);
            }
            
            e.HasMorePages = pageNumber<data.pages.Count-1;
            pageNumber++;
        }

        private void ppv_Click(object sender, EventArgs e)
        {

        }
    }
}

