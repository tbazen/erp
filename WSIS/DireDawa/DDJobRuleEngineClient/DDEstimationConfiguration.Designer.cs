﻿namespace INTAPS.WSIS.Job.DD.REClient
{
    partial class DDEstimationConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.itemLWDOther = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.label4 = new System.Windows.Forms.Label();
            this.itemLWD = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.label2 = new System.Windows.Forms.Label();
            this.itemLastMultiplier = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.label5 = new System.Windows.Forms.Label();
            this.labelFixedMaitenance = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.itemMaintenanceFixed = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemNewLineFixed = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsSurvey = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemPrepaidWaterMeters = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsWaterMeter = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridDeposit = new System.Windows.Forms.DataGridView();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeposit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.itemHydrantGov = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.label7 = new System.Windows.Forms.Label();
            this.itemHydrantOther = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(406, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(87, 30);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(510, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 30);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(605, 371);
            this.tabControl.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.itemHydrantOther);
            this.tabPage1.Controls.Add(this.itemLWDOther);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.itemHydrantGov);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.itemLWD);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.itemLastMultiplier);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.labelFixedMaitenance);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.itemMaintenanceFixed);
            this.tabPage1.Controls.Add(this.itemNewLineFixed);
            this.tabPage1.Controls.Add(this.itemsSurvey);
            this.tabPage1.Controls.Add(this.itemPrepaidWaterMeters);
            this.tabPage1.Controls.Add(this.itemsWaterMeter);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(597, 345);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // itemLWDOther
            // 
            this.itemLWDOther.anobject = null;
            this.itemLWDOther.Location = new System.Drawing.Point(305, 246);
            this.itemLWDOther.Name = "itemLWDOther";
            this.itemLWDOther.Size = new System.Drawing.Size(283, 20);
            this.itemLWDOther.TabIndex = 7;
            this.itemLWDOther.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(19, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Liquid Waste Disposal Item (Others):";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // itemLWD
            // 
            this.itemLWD.anobject = null;
            this.itemLWD.Location = new System.Drawing.Point(305, 220);
            this.itemLWD.Name = "itemLWD";
            this.itemLWD.Size = new System.Drawing.Size(283, 20);
            this.itemLWD.TabIndex = 7;
            this.itemLWD.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(19, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(270, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Liquid Waste Disposal Item (Residential ):";
            this.label2.Click += new System.EventHandler(this.label4_Click);
            // 
            // itemLastMultiplier
            // 
            this.itemLastMultiplier.anobject = null;
            this.itemLastMultiplier.Location = new System.Drawing.Point(305, 194);
            this.itemLastMultiplier.Name = "itemLastMultiplier";
            this.itemLastMultiplier.Size = new System.Drawing.Size(283, 20);
            this.itemLastMultiplier.TabIndex = 7;
            this.itemLastMultiplier.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(19, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Material Multiplier:";
            this.label5.Click += new System.EventHandler(this.label4_Click);
            // 
            // labelFixedMaitenance
            // 
            this.labelFixedMaitenance.AutoSize = true;
            this.labelFixedMaitenance.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.labelFixedMaitenance.ForeColor = System.Drawing.Color.White;
            this.labelFixedMaitenance.Location = new System.Drawing.Point(19, 157);
            this.labelFixedMaitenance.Name = "labelFixedMaitenance";
            this.labelFixedMaitenance.Size = new System.Drawing.Size(171, 17);
            this.labelFixedMaitenance.TabIndex = 0;
            this.labelFixedMaitenance.Text = "Maintenance Fixed Items:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(19, 123);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "New Line Fixed Items:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(19, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Survey Items:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(19, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prepaid Water Meter Items:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(19, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Water Meter Items:";
            // 
            // itemMaintenanceFixed
            // 
            this.itemMaintenanceFixed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemMaintenanceFixed.Location = new System.Drawing.Point(263, 151);
            this.itemMaintenanceFixed.Name = "itemMaintenanceFixed";
            this.itemMaintenanceFixed.Size = new System.Drawing.Size(325, 28);
            this.itemMaintenanceFixed.TabIndex = 1;
            this.itemMaintenanceFixed.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemNewLineFixed
            // 
            this.itemNewLineFixed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemNewLineFixed.Location = new System.Drawing.Point(263, 117);
            this.itemNewLineFixed.Name = "itemNewLineFixed";
            this.itemNewLineFixed.Size = new System.Drawing.Size(325, 28);
            this.itemNewLineFixed.TabIndex = 1;
            this.itemNewLineFixed.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsSurvey
            // 
            this.itemsSurvey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsSurvey.Location = new System.Drawing.Point(263, 83);
            this.itemsSurvey.Name = "itemsSurvey";
            this.itemsSurvey.Size = new System.Drawing.Size(325, 28);
            this.itemsSurvey.TabIndex = 1;
            this.itemsSurvey.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemPrepaidWaterMeters
            // 
            this.itemPrepaidWaterMeters.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemPrepaidWaterMeters.Location = new System.Drawing.Point(263, 49);
            this.itemPrepaidWaterMeters.Name = "itemPrepaidWaterMeters";
            this.itemPrepaidWaterMeters.Size = new System.Drawing.Size(325, 28);
            this.itemPrepaidWaterMeters.TabIndex = 1;
            this.itemPrepaidWaterMeters.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsWaterMeter
            // 
            this.itemsWaterMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsWaterMeter.Location = new System.Drawing.Point(263, 15);
            this.itemsWaterMeter.Name = "itemsWaterMeter";
            this.itemsWaterMeter.Size = new System.Drawing.Size(325, 28);
            this.itemsWaterMeter.TabIndex = 1;
            this.itemsWaterMeter.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridDeposit);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(597, 286);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Deposit Rule";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridDeposit
            // 
            this.gridDeposit.AllowUserToAddRows = false;
            this.gridDeposit.AllowUserToDeleteRows = false;
            this.gridDeposit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colType,
            this.colDeposit});
            this.gridDeposit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDeposit.Location = new System.Drawing.Point(3, 3);
            this.gridDeposit.Name = "gridDeposit";
            this.gridDeposit.Size = new System.Drawing.Size(591, 280);
            this.gridDeposit.TabIndex = 0;
            // 
            // colType
            // 
            this.colType.HeaderText = "Meter Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 79;
            // 
            // colDeposit
            // 
            this.colDeposit.HeaderText = "Desposit Amount";
            this.colDeposit.Name = "colDeposit";
            this.colDeposit.Width = 103;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 371);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(605, 36);
            this.panel1.TabIndex = 8;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Meter Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 79;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Desposit Amount";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 103;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(19, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(284, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Hydrant Supply Service Item (Government):";
            this.label6.Click += new System.EventHandler(this.label4_Click);
            // 
            // itemHydrantGov
            // 
            this.itemHydrantGov.anobject = null;
            this.itemHydrantGov.Location = new System.Drawing.Point(305, 272);
            this.itemHydrantGov.Name = "itemHydrantGov";
            this.itemHydrantGov.Size = new System.Drawing.Size(283, 20);
            this.itemHydrantGov.TabIndex = 7;
            this.itemHydrantGov.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(19, 301);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(243, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Hydrant Supply Service Item (Others):";
            this.label7.Click += new System.EventHandler(this.label4_Click);
            // 
            // itemHudrantOther
            // 
            this.itemHydrantOther.anobject = null;
            this.itemHydrantOther.Location = new System.Drawing.Point(305, 298);
            this.itemHydrantOther.Name = "itemHudrantOther";
            this.itemHydrantOther.Size = new System.Drawing.Size(283, 20);
            this.itemHydrantOther.TabIndex = 7;
            this.itemHydrantOther.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // DDEstimationConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 407);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Name = "DDEstimationConfigurationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estimation Configuration";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView gridDeposit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeposit;
        private System.Windows.Forms.TabPage tabPage1;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemLastMultiplier;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelFixedMaitenance;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private Client.ItemListPlaceHolder itemMaintenanceFixed;
        private Client.ItemListPlaceHolder itemNewLineFixed;
        private Client.ItemListPlaceHolder itemsSurvey;
        private Client.ItemListPlaceHolder itemsWaterMeter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label1;
        private Client.ItemListPlaceHolder itemPrepaidWaterMeters;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemLWD;
        private System.Windows.Forms.Label label2;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemLWDOther;
        private System.Windows.Forms.Label label4;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemHydrantOther;
        private System.Windows.Forms.Label label7;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemHydrantGov;
        private System.Windows.Forms.Label label6;
    }
}