using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.DD.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.HYDRANT_SERVICE)]
    public class HydrantWaterSupplyClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new HydrantWaterSupplyForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            HydrantWaterSupplyData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.HYDRANT_SERVICE, 0, true) as HydrantWaterSupplyData;
            string html;
            if (data == null)
                html = "";
            else
            {
                html = string.Format("<br/><span class='jobSection1'>Volume (Cubic Meters): {0}</span><br/>",data.workAmount);
            }
            return html;
        }


    }

    public class HydrantWaterSupplyForm : HomeDeliveryServiceFormBase<HydrantWaterSupplyData>
    {
        public HydrantWaterSupplyForm(int jobID, Subscriber customer)
            : base(jobID, customer)
        {
            labelWorkVolume.Text = "Volume (Cubic Meters)";
            this.Text = "Hydrant Supply Service";
        }
        public override int JobTypeID
        {
            get { return StandardJobTypes.HYDRANT_SERVICE; }
        }
    }
}
