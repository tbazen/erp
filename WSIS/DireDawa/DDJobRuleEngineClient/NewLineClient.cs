﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;

namespace INTAPS.WSIS.Job.DD.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.NEW_LINE,"Configure Estimation Parameters",true)]
    public class DDNewLineClient : StandardNewLineClientBase
    {
        public override System.Windows.Forms.Form getConfigurationForm()
        {
            return new DDEstimationConfigurationForm();
        }
    }

}
