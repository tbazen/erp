﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.DD.REServer
{
    public class LiquidWasteDisposalActiveListEFunction : IFunction
    {
        public JobManagerBDE bde;
        public LiquidWasteDisposalActiveListEFunction(JobManagerBDE bde)
        {
            this.bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            JobData[] jobs=bde.getActiveJobs( new int[] { StandardJobTypes.LIQUID_WASTE_DISPOSAL });
            ListData table = new ListData(jobs.Length);
            int i=0;

            foreach (JobData j in jobs)
            {
                LiquidWasteDisposalData data = bde.getWorkFlowData(j.id, j.applicationType, 0, true) as LiquidWasteDisposalData;
                ListData row = new ListData(9);
                Subscriber customer=bde.getJobCustomer(j);
                row[0] = new EData(DataType.Text, (i + 1).ToString());
                row[1] = new EData(DataType.Text, j.jobNo);
                row[2] = new EData(DataType.Text, j.startDate);
                row[3] = new EData(DataType.Text,customer.customerCode==null?"": customer.customerCode);
                row[4] = new EData(DataType.Text, customer.name == null ? "" : customer.name);
                row[5] = new EData(DataType.Text, data.phoneNo);
                row[6] = new EData(DataType.Text, data.address);
                row[7] = new EData(DataType.Text, data.contactPerson);
                row[8] = new EData(DataType.Text, data.workAmount.ToString());
                table[i] = new EData(DataType.ListData, row);
                i++;
            }
            return new EData(DataType.ListData, table);
        }

        public string Name
        {
            get { return "Active Liquid Waste Disposal Job"; }
        }

        public int ParCount
        {
            get { return 0; }
        }

        public string Symbol
        {
            get { return "JOBActiveLWD"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.Infix; }
        }
    }
}
