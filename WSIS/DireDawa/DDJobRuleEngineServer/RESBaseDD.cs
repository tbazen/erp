using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using BIZNET.iERP;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job.DD.REServer
{
    public abstract class RESBaseDD : RESBase
    {
        public RESBaseDD(JobManagerBDE bde)
            : base(bde)
        {
        }

        public bool isSingleItemService(JobBillOfMaterial[] boms, string itemCode, double quantity)
        {
            if (string.IsNullOrEmpty(itemCode))
                return false;
            if (boms == null)
                return false;
            if (boms.Length != 1)
                return false;
            JobBillOfMaterial bom = boms[0];
            if (bom == null)
                return false;
            if(bom.items.Length!=1)
                return false;
            if (!bom.items[0].itemID.Equals(itemCode, StringComparison.CurrentCultureIgnoreCase))
                return false;
            if (quantity < 0)
                return true;
            return bom.items[0].quantity == quantity;
        }

        public virtual void analyzeBillOfQuantity(Subscriber customer,JobData job, JobBillOfMaterial bom
            ,  out List<SummaryItem> summary
            , out List<MaterialItem> materials
            , out List<ServiceItem> services
           )
        {
            INTAPS.WSIS.Job.DD.DDEstimationConfiguration config = bde.getConfiguration(StandardJobTypes.NEW_LINE, typeof(DDEstimationConfiguration)) as DDEstimationConfiguration;
            if (config == null)
                throw new ServerUserMessage("Estimation configuration is not set");
            SubscriberType customerType = customer.subscriberType;
            summary = new List<SummaryItem>();
            materials = new List<MaterialItem>();
            services = new List<ServiceItem>();
            
            double totalMaterialFee = 0;
            double totalService = 0;
            double totalMaterialCost = 0;
            MaterialItem meterItem = null;

            TransactionItems[] waterMeterItems = bde.expandItemsList(config.waterMeterItems);
            bool hasWaterItem=false;
            foreach (JobBOMItem item in bom.items)
            {
                BIZNET.iERP.TransactionItems titem = bde.bERP.GetTransactionItems(item.itemID);
                if (titem == null)
                    continue;
                

                BIZNET.iERP.MeasureUnit mu = bde.bERP.GetMeasureUnit(titem.MeasureUnitID);
                string mustr = mu == null ? "" : mu.Name;
                if (titem.GoodOrService == BIZNET.iERP.GoodOrService.Good)
                {
                    string desc;
                    if (string.IsNullOrEmpty(item.description))
                        desc = titem.Name;
                    else
                        desc = item.description;
                    bool isWaterMeter = bde.contains(waterMeterItems, titem.Code);
                    if(isWaterMeter)
                        hasWaterItem=true;  
                    MaterialItem mi = new MaterialItem();
                    mi.itemCode = item.itemID;
                    mi.itemName = desc;
                    mi.orderNo = materials.Count + 1;
                    mi.quantity = item.quantity;
                    mi.standardUnitCost = titem.FixedUnitPrice;
                    mi.customerUnitCost = item.referenceUnitPrice;
                    mi.inSourced = item.inSourced;
                    mi.unit = mustr;
                    mi.salesAccountID = titem.salesAccountID;
                    materials.Add(mi);


                    double materialCost = 0;
                    if (mi.inSourced)
                    {

                        totalMaterialFee += item.quantity * titem.FixedUnitPrice;
                        materialCost = item.quantity * titem.FixedUnitPrice;

                    }
                    else
                    {
                        if (isWaterMeter)
                            throw new ServerUserMessage("Water meters can't be sourced from customers");
                        if (item.referenceUnitPrice == -1)
                            materialCost = item.quantity * titem.FixedUnitPrice;
                        else
                            materialCost = item.quantity * Math.Max(titem.FixedUnitPrice, item.referenceUnitPrice);

                    }
                    mi.materialCost = materialCost;

                    if (isWaterMeter)
                    {
                        if (meterItem != null)
                            throw new ServerUserMessage("Only one water meter item is permited");
                        meterItem = mi;
                    }
                    totalMaterialCost += materialCost;
                }
                else
                {

                    string desc;
                    if (string.IsNullOrEmpty(item.description))
                        desc = titem.Name;
                    else
                        desc = item.description;
                    ServiceItem si = new ServiceItem();
                    si.itemCode = titem.Code;
                    si.itemName = desc;
                    si.orderNo = services.Count + 1;
                    si.quantity = item.quantity;
                    si.unitCost = titem.FixedUnitPrice;
                    si.unit = mustr;
                    si.salesAccountID = titem.salesAccountID;
                    services.Add(si);
                    totalService += si.unitCost * si.quantity;
                }
            }
            double grandTotal = 0;
            summary.Add(new SummaryItem("", "Service", totalService, false, -1, 0));
            grandTotal += totalService;
            
            summary.Add(new SummaryItem("", "Material Fee", totalMaterialFee, false, -1, 0));
            grandTotal += totalMaterialFee;

            //Deposit
            if (job.applicationType == StandardJobTypes.NEW_LINE && meterItem!=null)
            {
                for (int k = 0; k < config.depositMeterType.Length; k++)
                {
                    if (config.depositMeterType[k].Equals(meterItem.itemCode))
                    {
                        summary.Add(new SummaryItem("", "Deposit", config.deposit[k], true, customer.depositAccountID, 0));
                        grandTotal += config.deposit[k];
                        break;
                    }
                }
            }

            bool addLastMultiplier = true;
            if (job.applicationType != StandardJobTypes.NEW_LINE)
            {
                if (bom.items.Length == 1 && hasWaterItem)
                    addLastMultiplier = false;
                
            }
            if (!string.IsNullOrEmpty(config.lastMultiplier) && addLastMultiplier)
            {
                BIZNET.iERP.TransactionItems t = bde.bERP.GetTransactionItems(config.lastMultiplier);
                JobItemSetting itemSetting=bde.getJobItem(t.Code);
                if(itemSetting==null)
                    throw new ServerUserMessage("Please set the multiplier value for the final multipler");
                double finalCharge = totalMaterialCost * itemSetting.value;
                summary.Add(new SummaryItem(t.Code, t.Name, finalCharge, true, t.salesAccountID, totalMaterialCost));
                grandTotal += finalCharge;
            }
        }
        
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            JobBillDocument bill = new JobBillDocument();

            List<SummaryItem> summary;
            List<MaterialItem> materials;
            List<ServiceItem> services;

            analyzeBillOfQuantity(customer, job, billOfMaterial, out summary, out materials, out services);
            bill.summaryItems = summary.ToArray();
            bill.materialItems = materials.ToArray();
            bill.serviceItems = services.ToArray();

            
            List<BillItem> items = new List<BillItem>();
            foreach (MaterialItem mi in materials)
            {
                if (!mi.inSourced)
                    continue;
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.standardUnitCost;
                i.price = mi.quantity * mi.standardUnitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;

                items.Add(i);
            }
            foreach (ServiceItem mi in services)
            {
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.unitCost;
                i.price = mi.quantity * mi.unitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);

            }
            foreach (SummaryItem mi in summary)
            {
                if (!mi.add)
                    continue;
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.saleAccountID;
                i.description = mi.description;
                i.hasUnitPrice = false;
                i.price = mi.amount;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);
            }
            bill.job = job;
            bill.customer = customer;
            bill.bom = billOfMaterial;
            bill.DocumentDate = DateTime.Now;
            bill.jobItems = items.ToArray();
            bill.ShortDescription = billOfMaterial.note;
            bill.draft = false;
            return bill;
        }

        public override string generateInvoiceHTML(JobBillDocument document)
        {
            JobData job = document.job; ;
            Subscriber customer = document.customer;
            INTAPS.Evaluator.EData jobNo = new Evaluator.EData(Evaluator.DataType.Text, job.jobNo);
            INTAPS.Evaluator.EData appDate = new Evaluator.EData(Evaluator.DataType.Text, job.startDate.ToString("MMM dd,yyyy"));
            INTAPS.Evaluator.EData customerName = new Evaluator.EData(Evaluator.DataType.Text, customer.name);
            INTAPS.Evaluator.EData custCode = new Evaluator.EData(Evaluator.DataType.Text, customer.customerCode);
            INTAPS.Evaluator.EData kebele = new Evaluator.EData(Evaluator.DataType.Text, document.customer.name);
            INTAPS.Evaluator.EData houseNo = new Evaluator.EData(Evaluator.DataType.Text, customer.address);
            INTAPS.Evaluator.EData phoneNo = new Evaluator.EData(Evaluator.DataType.Text, customer.phoneNo);
            INTAPS.Evaluator.EData customerType = new Evaluator.EData(Evaluator.DataType.Text, Subscriber.EnumToName(customer.subscriberType));
            INTAPS.Payroll.Employee prepareEmployee = bde.BdePayroll.GetEmployeeByLoginName(document.bom.preparedBy);
            INTAPS.Evaluator.EData approvedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData approvedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData preparedBy = new Evaluator.EData(Evaluator.DataType.Text, prepareEmployee == null ? "" : prepareEmployee.employeeName);
            INTAPS.Evaluator.EData preparedByDate = new Evaluator.EData(Evaluator.DataType.Text, document.bom.analysisTime.ToString("MMM dd,yyyy"));


            INTAPS.Evaluator.ListData list;
            double totalMaterialValue = 0;
            double totalMaterialFeeValue=0;
            double totalServiceValue=0;
            double grandTotal = 0;
            int i;

            list = new Evaluator.ListData(document.materialItems.Length);
            i = 0;
            foreach (MaterialItem mi in document.materialItems)
            {
                double materialValue = mi.materialCost;
                double materialFee = (mi.inSourced ? mi.standardUnitCost : 0) * mi.quantity;
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(11);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, mi.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, mi.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, mi.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, mi.standardUnitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (mi.standardUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[7] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost==-1 ? "" : mi.customerUnitCost.ToString("#,#00.00"));
                row.elements[8] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost == -1 ? "" : (mi.customerUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[9] = new Evaluator.EData(Evaluator.DataType.Text, mi.materialCost);
                row.elements[10] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced ? materialFee.ToString("#,#00.00") : "");
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal+=materialFee;
                totalMaterialValue += materialValue;
                totalMaterialFeeValue += materialFee;
                i++;
            }
            INTAPS.Evaluator.EData materialList = new Evaluator.EData(Evaluator.DataType.ListData, list);

            list = new Evaluator.ListData(document.serviceItems.Length);
            i = 0;
            foreach (ServiceItem si in document.serviceItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(7);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, si.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, si.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, si.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, si.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, si.unitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (si.unitCost * si.quantity).ToString("#,#00.00"));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal+=si.unitCost*si.quantity;
                totalServiceValue += si.unitCost * si.quantity;
                i++;
            }
            INTAPS.Evaluator.EData labourList = new Evaluator.EData(Evaluator.DataType.ListData, list);


            i=0;
            list= new Evaluator.ListData(document.summaryItems.Length);
            foreach (SummaryItem si in document.summaryItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(3);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.description);
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.amount));
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.basePrice));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                if (si.add)
                    grandTotal += si.amount;
                i++;
            }
            INTAPS.Evaluator.EData feeSummaryData = new Evaluator.EData(Evaluator.DataType.ListData, list);



            INTAPS.Evaluator.EData totalMaterial = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalMaterialFee = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialFeeValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalLabour = new Evaluator.EData(Evaluator.DataType.Text, totalServiceValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalSummary = new Evaluator.EData(Evaluator.DataType.Text, grandTotal.ToString("#,#00.00"));

            string headers;
            string ret = bde.BdeAccounting.EvaluateEHTML(bde.SysPars.jobInvoiceReportID, new object[]{
                 "jobNo",jobNo
                ,"appDate",appDate
                ,"customerName",customerName
                ,"custCode",custCode
                ,"kebele",kebele
                ,"houseNo",houseNo
                ,"phoneNo",phoneNo
                ,"customerType",customerType
                ,"feeSummaryData",feeSummaryData
                ,"materialList",materialList
                ,"labourList",labourList
                ,"approvedBy",approvedBy
                ,"approvedByDate",approvedByDate
                ,"checkedBy",checkedBy
                ,"checkedByDate",checkedByDate
                ,"preparedBy",preparedBy
                ,"preparedByDate",preparedByDate
                ,"totalSummary",totalSummary
                ,"totalMaterial",totalMaterial
                ,"totalMaterialFee",totalMaterialFee
                ,"totalLabour",totalLabour
                },out headers);
            return ret;
        }
        
        public override JobStatusType[] getAllStatusTypes()
        {
            return new JobStatusType[]{ 
                        StandardJobStatus.Application
                        ,StandardJobStatus.Survey
                        ,StandardJobStatus.Analysis
                        ,StandardJobStatus.WorkApproval
                        ,StandardJobStatus.WorkPayment
                        ,StandardJobStatus.Contract
                        ,StandardJobStatus.TechnicalWork
                        ,StandardJobStatus.Finalization
                        ,StandardJobStatus.Canceled
                        ,StandardJobStatus.Finished
            };
        }
    }
}
