using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job.DD.REServer
{
    [JobRuleServerHandler(StandardJobTypes.LIQUID_WASTE_DISPOSAL, "Liquid Waste Disposal")]
    public class RESLiquidWasteDisposal : RESHomeDeliveryServiceBase<LiquidWasteDisposalData>, IJobRuleServerHandler
    {
        public RESLiquidWasteDisposal(JobManagerBDE bde)
            : base(bde)
        {
        }

        public override JobBillOfMaterial getBOM(Subscriber customer,JobData job,DDEstimationConfiguration config)
        {
            string itemCode = customer.subscriberType == SubscriberType.Private ? config.liquidWasteDisposalItem : config.liquidWasteDisposalItemOther;
            if (string.IsNullOrEmpty(itemCode))
                throw new ServerUserMessage("Liquid waste disposal sales item not configured");
            BIZNET.iERP.TransactionItems item = bde.bERP.GetTransactionItems(itemCode);
            if (item == null)
                throw new ServerUserMessage("Liquid waste disposal item is not valid. It is probably deleted");
            if (AccountBase.AmountEqual(item.FixedUnitPrice, 0))
                throw new ServerUserMessage("Liquid waste disposal item " + item.Code + " doesn't have fixed unit price");

            //build the bom
            JobBillOfMaterial bom = new JobBillOfMaterial();
            bom.jobID = job.id;
            bom.note = "Liquid Waste Disposal";
            JobBOMItem ji = new JobBOMItem();

            ji.itemID = itemCode;
            ji.inSourced = true;
            ji.description = "Liquid Waste Disposal Fee";
            ji.quantity = checkAndGetData(job).workAmount;

            ji.unitPrice = item.FixedUnitPrice;
            bom.items = new JobBOMItem[] { ji };
            return bom;
        }
    }
    [JobRuleServerHandler(StandardJobTypes.HYDRANT_SERVICE, "Hydrant Water Supply")]
    public class RESHydrantWaterSupply: RESHomeDeliveryServiceBase<HydrantWaterSupplyData>, IJobRuleServerHandler
    {
        public RESHydrantWaterSupply(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override JobBillOfMaterial getBOM(Subscriber customer, JobData job, DDEstimationConfiguration config)
        {
            string itemCode = customer.subscriberType == SubscriberType.GovernmentInstitution ? config.hydrantGovermental: config.hydrantPrivate;
            if (string.IsNullOrEmpty(itemCode))
                throw new ServerUserMessage("Hydrant water supply sales item not configured");
            BIZNET.iERP.TransactionItems item = bde.bERP.GetTransactionItems(itemCode);
            if (item == null)
                throw new ServerUserMessage("Hydrant water supply item is not valid. It is probably deleted");
            if (AccountBase.AmountEqual(item.FixedUnitPrice, 0))
                throw new ServerUserMessage("Hydrant water supply item " + item.Code + " doesn't have fixed unit price");

            //build the bom
            JobBillOfMaterial bom = new JobBillOfMaterial();
            bom.jobID = job.id;
            bom.note = "Hydrant Water Supply";
            JobBOMItem ji = new JobBOMItem();

            ji.itemID = itemCode;
            ji.inSourced = true;
            ji.description = "Hydrant Water Supply Fee";
            ji.quantity = checkAndGetData(job).workAmount;
            ji.unitPrice = item.FixedUnitPrice;
            bom.items = new JobBOMItem[] { ji };
            return bom;
        }
    }

    public abstract class RESHomeDeliveryServiceBase<DataType> : RESBaseDD, IJobRuleServerHandler where DataType:HomeDeliveryServiceDataBase
    {
        public RESHomeDeliveryServiceBase(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            DataType newLine = data as DataType;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DataType newLine = data as DataType;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DataType wfdata = data as DataType;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:                    
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }
        public abstract JobBillOfMaterial getBOM(Subscriber customer, JobData job, DDEstimationConfiguration config);
       
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (nextState)
            {
                case StandardJobStatus.TECHNICAL_WORK:
                    checkPayment(job.id);
                    if (job.completionDocumentID != -1)
                    {
                        bde.BdeAccounting.DeleteAccountDocument(AID, job.completionDocumentID, false);
                        job.completionDocumentID = -1;
                        bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                        bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    }
                    break;
                case StandardJobStatus.WORK_PAYMENT:

                    if (job.customerID < 1)
                    {
                        if (job.newCustomer == null)
                            throw new ServerUserMessage("New customer not set");
                        if (job.newCustomer.id == -1)
                        {
                            job.newCustomer.id = bde.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                            job.newCustomer = bde.subscriberBDE.GetSubscriber(job.newCustomer.id);
                            bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, job);
                        }
                    }
                    Subscriber customer = bde.getJobCustomer(job);
                    //validate configuration
                    DDEstimationConfiguration config = bde.getConfiguration<DDEstimationConfiguration>(StandardJobTypes.NEW_LINE);
                    if (config == null)
                        throw new ServerUserMessage("Please set estimation configuration");

                    //delete any existing BOM
                    foreach (JobBillOfMaterial bm in bde.GetJobBOM(job.id))
                        bde.DeleteBOM(AID, bm.id,true);
                    JobBillOfMaterial bom = getBOM(customer,job,config);
                    bom.id = bde.SetBillofMateial(AID, bom);
                    bde.generateInvoice(AID, bom.id, worker);
                    break;
            }
        }


        protected DataType checkAndGetData(JobData job)
        {
            DataType data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as DataType;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);
            DataType data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    checkPayment(job.id);
                    data = checkAndGetData(job);
                    if (data == null)
                        throw new ServerUserMessage("No work data found");
                    
                    CompletedJobDocument completed = new CompletedJobDocument();
                    completed.job = job;
                    completed.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, completed);
                    job.completionDocumentID = completed.AccountDocumentID;
                    bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                    bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);

                    bde.setWorkFlowData(AID, data);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

    }
}
