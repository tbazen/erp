﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.WSIS.Job.DD
{
    [Serializable]
    public class DDEstimationConfiguration
    {
        public ItemsListItem[] waterMeterItems = new ItemsListItem[0];//ITEM CODE REF
        public ItemsListItem[] newLineFixedServiceItems = new ItemsListItem[0];//ITEM CODE REF
        public ItemsListItem[] maintenanceFixedServiceItems = new ItemsListItem[0];//ITEM CODE REF
        public ItemsListItem[] surveyItems = new ItemsListItem[0];//ITEM CODE REF
        public ItemsListItem[] preaPaidWaterMeterItems = new ItemsListItem[0];//ITEM CODE REF
        public string lastMultiplier = null;//ITEM CODE REF
        public string liquidWasteDisposalItem = null;//ITEM CODE REF
        public string liquidWasteDisposalItemOther = null;//ITEM CODE REF
        public string hydrantGovermental = null;//ITEM CODE REF
        public string hydrantPrivate = null;//ITEM CODE REF
        public double[] deposit = new double[0];
        public string[] depositMeterType = new string[0];//ITEM CODE REF
    }
}