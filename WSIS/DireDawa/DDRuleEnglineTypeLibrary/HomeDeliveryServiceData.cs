using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job.DD
{
    [Serializable]
    public class HomeDeliveryServiceDataBase : WorkFlowData
    {
        public Subscriber customer;
        public int kebele;
        public string houseNo;
        public string address;
        public string phoneNo;
        public double workAmount;
        public string contactPerson;
    }
    [Serializable]
    public class LiquidWasteDisposalData : HomeDeliveryServiceDataBase
    {
    }
    [Serializable]
    public class HydrantWaterSupplyData : HomeDeliveryServiceDataBase
    {
    }
}
