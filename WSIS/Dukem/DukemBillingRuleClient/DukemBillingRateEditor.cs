﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Dukem
{
    public partial class DukemBillingRateEditor : Form
    {

        public DukemBillingRateEditor()
        {
            InitializeComponent();
            billPeriodSelector.LoadData(SubscriberManagment.Client.SubscriberManagmentClient.CurrentYear);
        }

        public DukemBillingRateEditor(DukemBillingRate rate)
            : this()
        {
            if (rate == null)
                return;
            textFixed.Text = rate.fixedPenality.ToString();
            textProp.Text = rate.proportionalPenality.ToString();
            if(rate.periodFrom!=-1)            
                billPeriodSelector.SelectedPeriod = SubscriberManagment.Client.SubscriberManagmentClient.GetBillPeriod(rate.periodFrom);
        }        

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                DukemBillingRate rate = new DukemBillingRate();
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textProp, "Please enter proportional rate",false,true,out rate.proportionalPenality))
                    return;
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textFixed, "Please enter fixed rate", out rate.fixedPenality))
                    return;
                rate.periodFrom = billPeriodSelector.SelectedPeriod.id;
                SubscriberManagment.Client.SubscriberManagmentClient.setBillingRuleSetting(typeof(DukemBillingRate), rate);
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
    }
}
