using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
namespace INTAPS.WSIS.Job.Dukem.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CONNECTION_MAINTENANCE,priority=1)]
    public class ConnectionMaintenanceClientDukem : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ConnectionMaintenanceFormDukem(jobID, connection);
        }

        public string buildDataHTML(JobData job)
        {
            ConntectionMaintenanceDukemData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_MAINTENANCE, 0, true) as ConntectionMaintenanceDukemData;
            if (data == null)
                return "";

            Subscription subsc = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
            Subscriber newCustomer = job.customerID == -1 ? job.newCustomer : SubscriberManagmentClient.GetSubscriber(job.customerID);
            var conTable = createConnectionTable(subsc);
            string meterhtml="";
            if (data.changeMeter)
            {
                meterhtml += string.Format("<br/><span class='jobSection1'>New Meter Detail</span><br/>{0}", createMeterHTML(data.updatedMeterData));
            }

            string updatedConnectionDetail = "";
            if (data.relocateMeter)
            {
                updatedConnectionDetail =
                    System.Web.HttpUtility.HtmlEncode("Relocated Connection Detail")
                    +(data.updatedSubscription==null?"New connection information not saved": BIZNET.iERP.bERPHtmlBuilder.ToString(createConnectionTable( data.updatedSubscription)));
            }
            return string.Format("<span class='jobSection1'>{0}</span>{1}{2}"
                , System.Web.HttpUtility.HtmlEncode("Connection Detail")
                , BIZNET.iERP.bERPHtmlBuilder.ToString(conTable)
                ,meterhtml
                ,updatedConnectionDetail
                );
        }


    }

     
}

