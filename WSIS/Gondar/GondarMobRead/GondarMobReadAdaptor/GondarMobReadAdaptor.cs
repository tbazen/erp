﻿using INTAPS.ClientServer;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment.BDE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace INTAPS.SubscriberManagment.Gondar
{
    public class GondarMobReadAdaptor:IReadingAdapter
    {
        SubscriberManagmentBDE _bde;
        string m_connString;
        public GondarMobReadAdaptor(SubscriberManagmentBDE bde)
        {
            _bde = bde;
        }
        public void initialize()
        {
            foreach (var c in System.Configuration.ConfigurationManager.AppSettings)
            {
                if (c.Equals("WSSDB"))
                {
                    m_connString = System.Configuration.ConfigurationManager.AppSettings["WSSDB"];
                    break;
                }
            }
        }
        public void generateReadingPeriod()
        {
            return;
        }
        public void UploadReading(int wsisPeriodID)
        {
            DateTime engDate = DateTime.Now;
            SQLHelper wssHelper = new SQLHelper(m_connString);
            wssHelper.getOpenConnection();
            lock (wssHelper)
            {

                try
                {
                    wssHelper.BeginTransaction();
                    SQLHelper dspReader = _bde.GetReaderHelper();
                    try
                    {
                        string sql = String.Format(@"SELECT [subscriptionID],[reading],readingTime
                         FROM [Subscriber].[dbo].[BWFMeterReading] where periodID={0} and reading<>0 and bwfStatus=1", wsisPeriodID);
                        DataTable readTable = dspReader.GetDataTable(sql);
                        int hwssPeriodID = GetWSSPeriodID(wsisPeriodID, dspReader);
                        _bde.Accounting.PushProgress("Uploading reading for " + _bde.GetBillPeriod(wsisPeriodID).name);
                        int step = 0;
                        foreach (DataRow row in readTable.Rows)
                        {

                            int subscriptionID = (int)row[0];

                            // Subscription[] ss = Program.DBSubscriberConnection.GetSTRArrayByFilter<Subscription>("id=" + subscriptionID);
                            Subscription sub = _bde.GetSubscription(subscriptionID, DateTime.Now.Ticks);
                            object previousRead = wssHelper.ExecuteScalar(String.Format("Select ActualReading from WSS.dbo.bill_Consumption where ForMonth={0} and CusIndex={1}", hwssPeriodID - 1, int.Parse(sub.contractNo)));

                            double previousReading = 0;
                            if (previousRead != null)
                                previousReading = double.Parse(previousRead.ToString());
                            double newReading = double.Parse(row[1].ToString());
                            if (newReading < previousReading)
                                newReading = previousReading;
                            double newConsumption = newReading - previousReading;
                            //1. Calculate amount based on tariff consumption break down and insert or update amount for bill_Consumption
                            //2. Insert consumption breakdown calculated above to bill_ConsumptionBreakdown
                            //3. update co_Customer table lastReading with the actualreading and update lastMonthIndex to the current month

                            object _custType = wssHelper.ExecuteScalar(string.Format("Select CusType from WSS.dbo.co_Customers where CusIndex={0}", int.Parse(sub.contractNo)));
                            int cusType;
                            if (_custType == null)
                            {
                                cusType = -1;
                                Console.WriteLine("Customer type null for customer {0}", sub.contractNo);
                            }
                            else if (_custType is DBNull)
                            {
                                cusType = -1;
                                Console.WriteLine("Customer type DBNull for customer {0}", sub.contractNo);
                            }
                            else
                                cusType = int.Parse(_custType.ToString());

                            DataTable tariffTbl = wssHelper.GetDataTable(string.Format("SELECT * FROM [WSS].[dbo].[bill_Tariff] where CusType={0} order by TarrifID", cusType));
                            double amount = calculateBillAmount(tariffTbl, newConsumption);

                            if (!readingExists(sub.contractNo, wssHelper, hwssPeriodID))
                            {
                                //insert reading
                                string sqlMeter = "select MeterNo from WSS.dbo.co_Customers where CusIndex=" + int.Parse(sub.contractNo);
                                string sqlMeterSize = "select MeterSize from WSS.dbo.co_Customers where CusIndex=" + int.Parse(sub.contractNo);
                                Object meterSize = wssHelper.ExecuteScalar(sqlMeterSize);
                                if (meterSize == null || meterSize is DBNull)
                                    meterSize = 0;
                                string sqlMeterRent = "select Amount from WSS.dbo.bill_StgMeterSize where Pk=" + meterSize;

                                DataTable data = wssHelper.GetDataTable(sqlMeter);
                                double meterRent = (double)wssHelper.ExecuteScalar(sqlMeterRent);
                                string meterNo = (string)data.Rows[0][0];
                                DateTime d = (DateTime)row[2];
                                DateTime d2 = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
                                DateTime readDate = DateTime.Parse(d2.ToString("yyyy-MM-dd HH:mm:ss"));
                                engDate = readDate;
                                string ethReadDate = INTAPS.Ethiopic.EtGrDate.ToEth(readDate).ToString();
                                string wssDBName = wssHelper.getOpenConnection().Database;
                                wssHelper.Insert(wssDBName, "bill_Consumption"
                                    , new string[] { "MeterNo", "CusIndex", "ForMonth", "ReDate", "ReDateA", "LastReading", "ActualReading", "Consumtion", "MeterRent", "Amount", "Penality", "Total", "TotalSold", "BillTotal", "Processed", "Printed", "Posted" }
                                    , new object[] { meterNo, int.Parse(sub.contractNo), hwssPeriodID, readDate, ethReadDate, previousReading, newReading, newConsumption, meterRent, amount, 0d, amount + meterRent, 0d, 0d, 0, 0, 0 });
                                int conID = (int)wssHelper.ExecuteScalar(string.Format("select ConID FROM [WSS].[dbo].[bill_Consumption] where ForMonth={0} and CusIndex={1}", hwssPeriodID, int.Parse(sub.contractNo)));
                                insertConsumptionBreakdown(tariffTbl, newConsumption, conID, wssHelper);
                                wssHelper.ExecuteScalar(string.Format("Update WSS.dbo.co_Customers set LastReading={0},LastMonthIndex={1} where CusIndex={2}", newReading, hwssPeriodID, int.Parse(sub.contractNo)));
                            }
                            step++;
                            _bde.Accounting.SetProgress((double)step / (double)readTable.Rows.Count);
                        }
                        _bde.Accounting.PopProgress();
                    }
                    finally
                    {
                        _bde.ReleaseHelper(dspReader);
                    }
                    wssHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    wssHelper.RollBackTransaction();
                    ApplicationServer.EventLoger.LogException("Error Date:" + engDate.ToString() + "\n" + ex.Message, ex);
                    throw ex;
                }
            }
        }

        public void transferCustomerData(int AID, int wsisPeriodID)
        {
            addReadersToCurrentPeriod(wsisPeriodID);
            transferNewCustomers(AID);
            syncPreviousReadings(AID, wsisPeriodID);
            importBlocksToCurrentPeriod(AID, wsisPeriodID);
        }

        public string[] getCustomFields()
        {
            return new string[0];
        }

        public string getOverdue(int periodID, DateTime date, int customerID, int connectionID)
        {
            return string.Empty;
        }

        public string[] getcustomFieldValues(int periodID, int customerID, int connectionID)
        {
           return new string[] { };
        }


        public void prepareReadingCycleData()
        {
            return;
        }

        private int GetWSSPeriodID(int periodID, SQLHelper helper)
        {
            string sql = "select WSSBillPeriodID from Subscriber.dbo.BillPeriodMap where WSISBillPeriodID=" + periodID;
            int wssPeriodID = (int)helper.ExecuteScalar(sql);
            return wssPeriodID;
        }
        private bool readingExists(string contractNo, SQLHelper helper, int hwssPeriodID)
        {
            string sql = String.Format("Select count(*) from WSS.dbo.[bill_Consumption] where ForMonth={0} AND CusIndex={1}", hwssPeriodID, int.Parse(contractNo));
            int count = (int)helper.ExecuteScalar(sql);
            return count > 0;
        }
        private void insertConsumptionBreakdown(DataTable tariffTbl, double newConsumption, int conID, SQLHelper helper)
        {
            double sumDifference = 0;
            foreach (DataRow row in tariffTbl.Rows)
            {
                int diff = (int)row[4];
                int tariff = (int)row[0];
                double amount = (double)row[3];
                if (newConsumption - sumDifference >= diff)
                {
                    helper.Insert(helper.getOpenConnection().Database, "bill_ConsumptionBreakdown"
                    , new string[] { "ConID", "Tarrif", "Amount" }
                    , new object[] { conID, tariff, diff });

                    sumDifference += diff;
                }
                else if (newConsumption - sumDifference < diff)
                {
                    helper.Insert(helper.getOpenConnection().Database, "bill_ConsumptionBreakdown"
                   , new string[] { "ConID", "Tarrif", "Amount" }
                   , new object[] { conID, tariff, newConsumption - sumDifference });
                    sumDifference += (newConsumption - sumDifference);
                }
                else if (newConsumption - sumDifference == 0)
                {
                    helper.Insert(helper.getOpenConnection().Database, "bill_ConsumptionBreakdown"
                   , new string[] { "ConID", "Tarrif", "Amount" }
                   , new object[] { conID, tariff, 0 });
                }

            }
        }

        private double calculateBillAmount(DataTable tariffTbl, double newConsumption)
        {
            double sumDifference = 0;
            double sumAmount = 0;
            foreach (DataRow row in tariffTbl.Rows)
            {
                int diff = (int)row[4];
                double amount = (double)row[3];
                if (newConsumption - sumDifference >= diff)
                {
                    sumAmount += amount * diff;
                    sumDifference += diff;
                }
                else
                {
                    sumAmount += amount * (newConsumption - sumDifference);
                    break;
                }

            }
            return sumAmount;
        }

        private void addReadersToCurrentPeriod(int periodID)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    //If readers are not added to current period add them
                    ReadingEntryClerk[] clerks = _bde.GetAllReadingEntryClerk();
                    string description = "Transferring readers to period " + _bde.GetBillPeriod(periodID).name;
                    this._bde.Accounting.PushProgress(description); //initialize progress
                    int step = 0;
                    foreach (ReadingEntryClerk clerk in clerks)
                    {
                        MeterReaderEmployee reader = _bde.GetMeterReaderEmployees(periodID, clerk.employeeID);
                        if (reader == null)
                        {
                            //Add reader to MeterReaderEmployee
                            _bde.CreateMeterReaderEmployee(new MeterReaderEmployee() { periodID = periodID, employeeID = clerk.employeeID });
                        }
                        step++;
                        this._bde.Accounting.SetProgress((double)step / (double)(clerks.Length)); //Step progress
                    }
                    this._bde.Accounting.PopProgress(); //announce progress termination
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;
                }
            }
        }
        private void transferNewCustomers(int AID)
        {
            SQLHelper helper = new SQLHelper(m_connString);
            DataTable wssCustomers;
            //1. get all customers from WSS
            try
            {
                string custSql = "SELECT * FROM [WSS].[dbo].[co_Customers]";
                wssCustomers = helper.GetDataTable(custSql);
            }
            finally
            {
                _bde.ReleaseHelper(helper);
            }
            _bde.WriterHelper.setReadDB(_bde.DBName);
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.BeginTransaction();

                    object sub = _bde.WriterHelper.ExecuteScalar("Select max(id) from Subscriber.dbo.Subscriber");
                    int maxSubscriberID = sub is DBNull ? 0 : (int)sub;
                    object conn = _bde.WriterHelper.ExecuteScalar("Select max(id) from Subscriber.dbo.Subscription");
                    int maxSubscriptionID = conn is DBNull ? 0 : (int)conn;
                    _bde.Accounting.PushProgress("Transferring/updating customers");

                    int step = 0;
                    foreach (DataRow row in wssCustomers.Rows)
                    {
                        string customerCode = ((int)row["CusIndex"]).ToString();
                        Subscriber oldSubscriber = _bde.GetSubscriber(customerCode);
                        if (oldSubscriber == null) //new customer found
                        {
                            //transfer the new subscriber

                            Subscriber subscriber = new Subscriber();
                            subscriber.id = maxSubscriberID + 1;
                            subscriber.name = row["CustName"] as string;
                            subscriber.subscriberType = GetSubscriberType((int)row["CusType"], _bde.WriterHelper);
                            subscriber.Kebele = GetKebele(row["Ketena"] as string, _bde.WriterHelper);
                            subscriber.address = row["HNo"] as string;
                            subscriber.amharicName = "N/A";
                            subscriber.customerCode = ((int)row["CusIndex"]).ToString();
                            subscriber.phoneNo = row["Tele"] as string;
                            subscriber.email = "N/A";
                            subscriber.nameOfPlace = GetKebeleName(subscriber.Kebele, _bde.WriterHelper);
                            subscriber.status = CustomerStatus.Active;
                            subscriber.statusDate = row["RegDate"] is DBNull ? DateTime.Now.Date : (DateTime)row["RegDate"];
                            subscriber.accountID = -1;
                            subscriber.depositAccountID = -1;
                            subscriber.subTypeID = -1;
                            _bde.WriterHelper.InsertSingleTableRecord<Subscriber>(_bde.DBName, subscriber, new string[] { "__AID" }, new object[] { AID });

                            Subscription subscription = new Subscription();
                            subscription.id = maxSubscriptionID + 1;
                            subscription.ticksFrom = 635641776000000000;
                            subscription.ticksTo = -1;
                            subscription.timeBinding = DataTimeBinding.LowerBound;
                            subscription.subscriberID = subscriber.id;
                            subscription.contractNo = subscriber.customerCode;
                            subscription.subscriptionType = SubscriptionType.Tap;
                            subscription.subscriptionStatus = GetSubscriptionStatus(row["Status"] as string, _bde.WriterHelper);
                            subscription.Kebele = subscriber.Kebele;
                            subscription.address = subscriber.address;
                            subscription.initialMeterReading = 0;
                            subscription.prePaid = false;
                            subscription.parcelNo = "N/A";
                            subscription.connectionType = ConnectionType.Unknown;
                            subscription.supplyCondition = SupplyCondition.Unknown;
                            subscription.remark = "New Customer Transfer from HAWASA WSS System";
                            subscription.dataStatus = DataStatus.Unknown;
                            subscription.meterPosition = 0;
                            subscription.waterMeterX = 0;
                            subscription.waterMeterY = 0;
                            subscription.houseConnectionX = 0;
                            subscription.houseConnectionY = 0;
                            subscription.serialNo = row["MeterNo"] as string;
                            subscription.modelNo = subscription.serialNo;
                            subscription.opStatus = MaterialOperationalStatus.Working;
                            subscription.householdSize = 0;
                            _bde.WriterHelper.InsertSingleTableRecord<Subscription>(_bde.DBName, subscription, new string[] { "__AID" }, new object[] { AID });

                            maxSubscriberID++;
                            maxSubscriptionID++;
                        }
                        else
                        {
                            //update Subscriber
                            oldSubscriber.name = row["CustName"] as string;
                            oldSubscriber.subscriberType = GetSubscriberType((int)row["CusType"], _bde.WriterHelper);
                            oldSubscriber.Kebele = GetKebele(row["Ketena"] as string, _bde.WriterHelper);
                            oldSubscriber.address = row["HNo"] as string;
                            oldSubscriber.customerCode = ((int)row["CusIndex"]).ToString();
                            oldSubscriber.phoneNo = row["Tele"] as string;
                            oldSubscriber.nameOfPlace = GetKebeleName(oldSubscriber.Kebele, _bde.WriterHelper);
                            oldSubscriber.statusDate = row["RegDate"] is DBNull ? DateTime.Now.Date : (DateTime)row["RegDate"];
                            _bde.WriterHelper.logDeletedData(AID, string.Format("{0}.dbo.Subscriber", _bde.DBName), "id=" + oldSubscriber.id);
                            _bde.WriterHelper.UpdateSingleTableRecord<Subscriber>(_bde.DBName, oldSubscriber, new string[] { "__AID" }, new object[] { AID });
                            //Update subscription
                            Subscription oldSubscription = _bde.WriterHelper.GetSTRArrayByFilter<Subscription>("subscriberID=" + oldSubscriber.id)[0];
                            oldSubscription.subscriptionStatus = GetSubscriptionStatus(row["Status"] as string, _bde.WriterHelper);
                            oldSubscription.Kebele = oldSubscriber.Kebele;
                            oldSubscription.address = oldSubscriber.address;
                            oldSubscription.serialNo = row["MeterNo"] as string;
                            _bde.WriterHelper.logDeletedData(AID, string.Format("{0}.dbo.Subscription", _bde.DBName), "id=" + oldSubscription.id);
                            _bde.WriterHelper.UpdateSingleTableRecord<Subscription>(_bde.DBName, oldSubscription, new string[] { "__AID" }, new object[] { AID });
                        }
                        step++;
                        _bde.Accounting.SetProgress((double)step / (double)wssCustomers.Rows.Count);
                    }
                    _bde.Accounting.PopProgress();
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;

                }
                finally
                {
                    _bde.WriterHelper.restoreReadDB();
                }
            }
        }
        private void syncPreviousReadings(int AID, int periodID)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    object parameter = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='lastPrevousReadingPeriodID'");
                    object wssEmployeeID = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='wssSystemEmployeeID'");
                    IDataReader reader = null;
                    _bde.WriterHelper.setReadDB(_bde.DBName);
                    _bde.WriterHelper.BeginTransaction();
                    if (wssEmployeeID == null)
                    {
                        throw new Exception("WSS System employee id not found in Subscriber system parameters");
                    }
                    int wssEmpID = int.Parse(wssEmployeeID.ToString());
                    ReadingEntryClerk clerk = _bde.BWFGetClerk(wssEmpID);
                    if (parameter == null)
                        throw new Exception("Last previous reading period id not found in Subscriber system parameters!");
                    else
                    {
                        int lastPreviousReadingPeriodID = int.Parse(parameter.ToString());
                        SQLHelper wssHelper = new SQLHelper(m_connString);
                        try
                        {
                            SQLHelper wssClone1 = wssHelper.Clone();
                            SQLHelper wssClone2 = wssHelper.Clone();
                            try
                            {
                                for (int i = periodID - 1; i <= periodID - 1; i++)
                                {
                                    try
                                    {
                                        int wssPeriodID = GetWSSPeriodID(i, _bde.WriterHelper);
                                        //Create wss system block for readings that are recorded manually in the particular period(readings not read by mobile) and transfer them
                                        string systemBlockName = "WSS System Block";
                                        BWFReadingBlock[] blocks = _bde.GetBlock(i, systemBlockName);
                                        if (blocks.Length > 0)
                                        {
                                            //if block already exists delete the block and the readings transferred under that block
                                            deleteBlockWithReadingsNoLog(AID, clerk, blocks[0].id, _bde.WriterHelper);
                                        }
                                        BWFReadingBlock wssBlock = new BWFReadingBlock();
                                        wssBlock.readerID = wssEmpID;
                                        wssBlock.periodID = i;
                                        wssBlock.readingStart = DateTime.Now.Date;
                                        wssBlock.readingEnd = DateTime.Now.Date;
                                        wssBlock.blockName = systemBlockName;
                                        wssBlock.entryClerkID = wssEmpID;
                                        wssBlock.id = _bde.BWFCreateReadingBlock(AID, null, wssBlock);
                                        string sql = String.Format("Select * from WSS.dbo.bill_Consumption where ForMonth ={0}", wssPeriodID);
                                        int count = (int)wssClone2.ExecuteScalar(String.Format("Select count(*) from WSS.dbo.bill_Consumption where ForMonth ={0}", wssPeriodID));
                                        reader = wssClone1.ExecuteReader(sql);
                                        _bde.Accounting.PushProgress("Syncing readings from " + _bde.GetBillPeriod(i).name);
                                        int step = 0;
                                        while (reader.Read())
                                        {
                                            string contractNo = ((int)reader["CusIndex"]).ToString();
                                            double newReading = double.Parse(reader["ActualReading"].ToString());
                                            double newConsumption = double.Parse(reader["Consumtion"].ToString());
                                            DateTime readDate = (DateTime)reader["ReDate"];
                                            // update if reading exists for particular period in WSIS
                                            int status = (int)_bde.WriterHelper.ExecuteScalar(String.Format("Select subscriptionStatus from Subscriber.dbo.Subscription where contractNo='{0}'", contractNo));
                                            SubscriptionStatus curStatus = (SubscriptionStatus)status;
                                            if (curStatus == SubscriptionStatus.Active)
                                            {
                                                int subscriptionID = (int)_bde.WriterHelper.ExecuteScalar("select id from Subscriber.dbo.Subscription where contractNo='" + contractNo + "'");
                                                UpdateSubscriptionReading(AID, subscriptionID, i, newReading, newConsumption, _bde.WriterHelper);
                                                //If there are readings not read by mobile transfer them to wss system block
                                                if ((int)_bde.WriterHelper.ExecuteScalar(string.Format("select count(*) from Subscriber.dbo.BWFMeterReading where subscriptionID={0} and periodID={1}", subscriptionID, i)) == 0)
                                                    importWSSSystemBlockReading(AID, wssBlock.id, i, subscriptionID, newReading, newConsumption, readDate, clerk, _bde.WriterHelper);
                                            }
                                            step++;
                                            _bde.Accounting.SetProgress((double)step / (double)count);
                                        }
                                        _bde.Accounting.PopProgress();
                                    }
                                    finally
                                    {
                                        if (!((reader == null) || reader.IsClosed))
                                        {
                                            reader.Close();
                                        }

                                    }
                                }
                            }
                            finally
                            {
                                //if (!((reader == null) || reader.IsClosed))
                                //{
                                //    reader.Close();
                                //}
                                wssClone1.CloseConnection();
                                wssClone2.CloseConnection();
                            }
                        }
                        finally
                        {
                            _bde.ReleaseHelper(wssHelper);
                        }
                    }
                    _bde.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    _bde.WriterHelper.restoreReadDB();
                }
            }
        }

        private void deleteBlockWithReadingsNoLog(int AID, ReadingEntryClerk clerk, int blockID, SQLHelper helper)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.DeleteSingleTableRecrod<BWFReadingBlock>(_bde.DBName, new object[] { blockID });
                    string sql = "Delete from Subscriber.dbo.BWFMeterReading where readingBlockID=" + blockID;
                    _bde.WriterHelper.ExecuteScalar(sql);
                }
                catch (Exception exception)
                {
                    throw new Exception("Error on deleting reading block", exception);
                }
            }
        }


        private void UpdateSubscriptionReading(int AID, int subscriptionID, int periodID, double newReading, double newConsumption, SQLHelper helper)
        {
            //int subscriptionID = (int)helper.ExecuteScalar(String.Format("Select id from Subscriber.dbo.Subscription where contractNo='{0}'", contractNo));
            string sql = String.Format("Update Subscriber.dbo.BWFMeterReading set reading={0},consumption={1},__AID={2},bwfStatus=1 where subscriptionID={3} and periodID={4}", newReading, newConsumption, AID, subscriptionID, periodID);
            _bde.WriterHelper.logDeletedData(AID, string.Format("{0}.dbo.BWFMeterReading", _bde.DBName), "subscriptionID=" + subscriptionID + " AND periodID=" + periodID);
            helper.ExecuteScalar(sql);
        }
        private void importWSSSystemBlockReading(int AID, int block, int period, int subscriptionID, double newReading, double newConsumption, DateTime readDate, ReadingEntryClerk clerk, SQLHelper helper)
        {

            object order = helper.ExecuteScalar("select max(orderN) from Subscriber.dbo.BWFMeterReading where periodID=" + period);
            int lastOrderN = order is DBNull ? 0 : (int)order;
            Subscription sub = helper.GetSTRArrayByFilter<Subscription>("id=" + subscriptionID)[0];
            BWFMeterReading reading = new BWFMeterReading();
            reading.readingBlockID = block;
            reading.subscriptionID = subscriptionID;
            reading.bwfStatus = BWFStatus.Read;
            reading.readingType = MeterReadingType.Normal;
            reading.averageMonths = 0;
            reading.reading = newReading;
            reading.consumption = newConsumption;
            reading.entryDate = readDate;
            reading.orderN = lastOrderN + 1;
            reading.billDocumentID = -1;
            reading.periodID = period;
            reading.readingProblem = MeterReadingProblem.NoProblem;
            reading.extraInfo = ReadingExtraInfo.None;
            reading.readingRemark = "Reading imported WSS System Employee";
            reading.readingTime = reading.entryDate;
            reading.readingX = sub.waterMeterX;
            reading.readingY = sub.waterMeterY;
            reading.method = ReadingMethod.Unknown;
            _bde.BWFAddMeterReading(AID, clerk, reading, false);
        }
        private void importBlocksToCurrentPeriod(int AID, int periodID)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.WriterHelper.setReadDB(_bde.DBName);
                    string sql = "SELECT [id] FROM [Subscriber].[dbo].[BWFReadingBlock] where periodID=" + _bde.GetPreviousBillPeriod(periodID).id;
                    DataTable tblBlocks = _bde.WriterHelper.GetDataTable(sql);
                    foreach (DataRow row in tblBlocks.Rows)
                    {
                        int blockID = (int)row[0];

                        if (skipIfSystemBlock(blockID, _bde.WriterHelper))
                            continue;
                        int num3;
                        BWFReadingBlock block = _bde.BWFGetReadingBlockInternal(_bde.WriterHelper, blockID);
                        BWFReadingBlock[] blocks = _bde.GetBlock(periodID, block.blockName);
                        if (blocks.Length > 0)
                        {
                            //if block already exists delete the block and the readings transferred under that block
                            deleteBlockWithReadingsNoLog(AID, null, blocks[0].id, _bde.WriterHelper);
                        }

                        BWFMeterReading[] readingArray = _bde.GetReadings(blockID, 0, -1, out num3);
                        block.id = -1;
                        block.periodID = periodID;
                        block.id = _bde.BWFCreateReadingBlock(AID, null, block);
                        _bde.Accounting.PushProgress(String.Format("Importing reading for {0} ({1} Readings)", block.blockName, readingArray.Length));
                        int step = 0;
                        foreach (BWFMeterReading reading in readingArray)
                        {
                            reading.readingBlockID = block.id;
                            reading.periodID = block.periodID;
                            reading.bwfStatus = BWFStatus.Unread;
                            reading.reading = 0.0;
                            reading.readingX = 0d;
                            reading.readingY = 0d;
                            reading.extraInfo = ReadingExtraInfo.None;
                            reading.readingRemark = "";
                            reading.readingType = MeterReadingType.Normal;
                            reading.readingProblem = MeterReadingProblem.NoProblem;
                            _bde.BWFAddMeterReading(AID, null, reading, false);
                            step++;
                            _bde.Accounting.SetProgress((double)step / (double)readingArray.Length);
                        }
                        _bde.Accounting.PopProgress();
                    }
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    _bde.WriterHelper.restoreReadDB();
                }
            }
        }

        private bool skipIfSystemBlock(int blockID, SQLHelper dsp)
        {
            object wssEmployeeID = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='wssSystemEmployeeID'");
            if (wssEmployeeID == null)
            {
                throw new Exception("WSS System employee id not found in Subscriber system parameters");
            }
            int wssEmpID = int.Parse(wssEmployeeID.ToString());
            string sql = string.Format("Select count(*) from {0}.dbo.[BWFReadingBlock] where id=" + blockID + " and readerID=" + wssEmpID, _bde.DBName);
            if ((int)dsp.ExecuteScalar(sql) > 0)
                return true;
            return false;
        }

      

        private static SubscriberType GetSubscriberType(int custType, SQLHelper sQLHelper)
        {
            string sql = "select WSISSubscriberType From Subscriber.dbo.CustTypeToSubscribeTypeMap where WSSACustType=" + custType;
            int subscriberType = (int)sQLHelper.ExecuteScalar(sql);
            return (SubscriberType)subscriberType;
        }
        private static SubscriptionStatus GetSubscriptionStatus(string status, SQLHelper sQLHelper)
        {
            string sql = String.Format("Select WSISSubscriptionStatus from Subscriber.dbo.CustStatusToSubscriptionStatusMap where WSSCustStatus='{0}'", status);
            int s = (int)sQLHelper.ExecuteScalar(sql);
            return (SubscriptionStatus)s;
        }

        private static string GetKebeleName(int kebele, SQLHelper sQLHelper)
        {
            string sql = "Select name from Subscriber.dbo.Kebele where id=" + kebele;
            string name = (string)sQLHelper.ExecuteScalar(sql);
            return name;
        }
        private static int GetKebele(string kebele, SQLHelper sQLHelper)
        {
            string sql = String.Format("Select id from Subscriber.dbo.Kebele where name='{0}'", kebele);
            int id = (int)sQLHelper.ExecuteScalar(sql);
            return id;
        }
    }
}
