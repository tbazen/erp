﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Harar
{
    public partial class HararBillingRateEditor : Form
    {

        public HararBillingRateEditor()
        {
            InitializeComponent();
            lblFormula.Text = "D: The number of period that passed since the bill produced\r\n\nT: Numerical value that stands for a particular customer type identified as below:\r\t 0 - Unknown\r\t 1 - Private\r\t 2 - Government\r\t 3 - Commercial\r\t 4 - NGO\r\t 5 - Religious Institution\r\t 6 - Community\r\t 7 - Industry\r\t 8 - Others";
        }

        public HararBillingRateEditor(DDBillingRate rate)
            : this()
        {
            if (rate == null)
                return;
            textFormula.Text = rate.penalityFormula;
        }


        

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                DDBillingRate rate = new DDBillingRate();
                rate.penalityFormula = textFormula.Text.Trim();

                SubscriberManagment.Client.SubscriberManagmentClient.setBillingRuleSetting(typeof(DDBillingRate), rate);
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
    }
}
