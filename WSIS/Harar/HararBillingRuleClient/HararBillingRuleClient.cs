﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment.Harar
{
    public class HararBillingRuleClient:SubscriberManagment.Client.IBillingRuleClient
    {
        public HararBillingRuleClient()
        {
        }
        public void showSettingEditor(Type type, object setting)
        {
            if (type == typeof(SolidWasteDisposalRate))
            {
                SolidWasteDisposalRateEditor form = new SolidWasteDisposalRateEditor(setting as SolidWasteDisposalRate);
                form.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
            else
            {
                HararBillingRateEditor form = new HararBillingRateEditor(setting as DDBillingRate);
                form.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm);
            }
        }

        public System.Resources.ResourceManager getStringTable(int languageID)
        {
            return StringTable.ResourceManager;
        }


        public void printReceipt(CustomerPaymentReceipt receipt, Client.PrintReceiptParameters pars,bool reprint,int nCopies)
        {
            new ReceiptPrinter(receipt, pars).PrintNow(reprint,nCopies);
        }
    }
   
}
