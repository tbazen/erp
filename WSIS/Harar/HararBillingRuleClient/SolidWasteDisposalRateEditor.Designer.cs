﻿namespace INTAPS.SubscriberManagment.Harar
{
    partial class SolidWasteDisposalRateEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonAddSpecial = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridSpecialRate = new System.Windows.Forms.DataGridView();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeposit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.accountPayable = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.accountingIncome = new INTAPS.Accounting.Client.AccountPlaceHolder();
            this.textRevenueShare = new System.Windows.Forms.TextBox();
            this.textCommunityRate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rateInstitutionalRate = new INTAPS.UI.ProgressiveRateEditor();
            this.ratePrivate = new INTAPS.UI.ProgressiveRateEditor();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkEnableSolidWasteDiposalBill = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSpecialRate)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateInstitutionalRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratePrivate)).BeginInit();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonAddSpecial);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 432);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(612, 36);
            this.panel1.TabIndex = 9;
            // 
            // buttonAddSpecial
            // 
            this.buttonAddSpecial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAddSpecial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAddSpecial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddSpecial.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonAddSpecial.ForeColor = System.Drawing.Color.White;
            this.buttonAddSpecial.Location = new System.Drawing.Point(7, 3);
            this.buttonAddSpecial.Name = "buttonAddSpecial";
            this.buttonAddSpecial.Size = new System.Drawing.Size(145, 30);
            this.buttonAddSpecial.TabIndex = 4;
            this.buttonAddSpecial.Text = "&Add Special Rate";
            this.buttonAddSpecial.UseVisualStyleBackColor = false;
            this.buttonAddSpecial.Click += new System.EventHandler(this.buttonAddSpecial_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(413, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(87, 30);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(517, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 30);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridSpecialRate);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(604, 359);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Special Rates";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridSpecialRate
            // 
            this.gridSpecialRate.AllowUserToAddRows = false;
            this.gridSpecialRate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridSpecialRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSpecialRate.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colType,
            this.colDeposit});
            this.gridSpecialRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSpecialRate.Location = new System.Drawing.Point(3, 3);
            this.gridSpecialRate.Name = "gridSpecialRate";
            this.gridSpecialRate.Size = new System.Drawing.Size(598, 353);
            this.gridSpecialRate.TabIndex = 0;
            // 
            // colType
            // 
            this.colType.HeaderText = "Customer Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 95;
            // 
            // colDeposit
            // 
            this.colDeposit.HeaderText = "Fee";
            this.colDeposit.Name = "colDeposit";
            this.colDeposit.Width = 50;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.chkEnableSolidWasteDiposalBill);
            this.tabPage3.Controls.Add(this.accountPayable);
            this.tabPage3.Controls.Add(this.accountingIncome);
            this.tabPage3.Controls.Add(this.textRevenueShare);
            this.tabPage3.Controls.Add(this.textCommunityRate);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.rateInstitutionalRate);
            this.tabPage3.Controls.Add(this.ratePrivate);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(604, 406);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "General";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // accountPayable
            // 
            this.accountPayable.account = null;
            this.accountPayable.AllowAdd = false;
            this.accountPayable.Location = new System.Drawing.Point(163, 314);
            this.accountPayable.Name = "accountPayable";
            this.accountPayable.OnlyLeafAccount = false;
            this.accountPayable.Size = new System.Drawing.Size(284, 20);
            this.accountPayable.TabIndex = 10;
            this.accountPayable.Tag = "PermanentPayrollExpenseAccount";
            // 
            // accountingIncome
            // 
            this.accountingIncome.account = null;
            this.accountingIncome.AllowAdd = false;
            this.accountingIncome.Location = new System.Drawing.Point(163, 292);
            this.accountingIncome.Name = "accountingIncome";
            this.accountingIncome.OnlyLeafAccount = false;
            this.accountingIncome.Size = new System.Drawing.Size(284, 20);
            this.accountingIncome.TabIndex = 10;
            this.accountingIncome.Tag = "PermanentPayrollExpenseAccount";
            // 
            // textRevenueShare
            // 
            this.textRevenueShare.Location = new System.Drawing.Point(163, 262);
            this.textRevenueShare.Name = "textRevenueShare";
            this.textRevenueShare.Size = new System.Drawing.Size(284, 20);
            this.textRevenueShare.TabIndex = 2;
            // 
            // textCommunityRate
            // 
            this.textCommunityRate.Location = new System.Drawing.Point(163, 236);
            this.textCommunityRate.Name = "textCommunityRate";
            this.textCommunityRate.Size = new System.Drawing.Size(284, 20);
            this.textCommunityRate.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 321);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Payable Account:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "WSSA Revenue Share(%)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Institutional Rate";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 295);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Income Account:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 239);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Community Rate (%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Private Rate";
            // 
            // rateInstitutionalRate
            // 
            this.rateInstitutionalRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rateInstitutionalRate.HorizontalEnterNavigationMode = true;
            this.rateInstitutionalRate.Location = new System.Drawing.Point(10, 135);
            this.rateInstitutionalRate.Name = "rateInstitutionalRate";
            this.rateInstitutionalRate.Size = new System.Drawing.Size(591, 90);
            this.rateInstitutionalRate.SuppessEnterKey = false;
            this.rateInstitutionalRate.TabIndex = 0;
            this.rateInstitutionalRate.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.progressiveRateEditor1_CellContentClick);
            // 
            // ratePrivate
            // 
            this.ratePrivate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ratePrivate.HorizontalEnterNavigationMode = true;
            this.ratePrivate.Location = new System.Drawing.Point(10, 26);
            this.ratePrivate.Name = "ratePrivate";
            this.ratePrivate.Size = new System.Drawing.Size(591, 90);
            this.ratePrivate.SuppessEnterKey = false;
            this.ratePrivate.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(612, 432);
            this.tabControl.TabIndex = 8;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Customer Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 95;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Fee";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 50;
            // 
            // chkEnableSolidWasteDiposalBill
            // 
            this.chkEnableSolidWasteDiposalBill.AutoSize = true;
            this.chkEnableSolidWasteDiposalBill.Location = new System.Drawing.Point(13, 347);
            this.chkEnableSolidWasteDiposalBill.Name = "chkEnableSolidWasteDiposalBill";
            this.chkEnableSolidWasteDiposalBill.Size = new System.Drawing.Size(189, 17);
            this.chkEnableSolidWasteDiposalBill.TabIndex = 11;
            this.chkEnableSolidWasteDiposalBill.Text = "Generate Solid Waste Disposal Bill";
            this.chkEnableSolidWasteDiposalBill.UseVisualStyleBackColor = true;
            // 
            // SolidWasteDisposalRateEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 468);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Name = "SolidWasteDisposalRateEditor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solid Wate Disposal Rates";
            this.panel1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSpecialRate)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateInstitutionalRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ratePrivate)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView gridSpecialRate;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private UI.ProgressiveRateEditor rateInstitutionalRate;
        private UI.ProgressiveRateEditor ratePrivate;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Button buttonAddSpecial;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeposit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textCommunityRate;
        private System.Windows.Forms.TextBox textRevenueShare;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private Accounting.Client.AccountPlaceHolder accountPayable;
        private Accounting.Client.AccountPlaceHolder accountingIncome;
        private System.Windows.Forms.CheckBox chkEnableSolidWasteDiposalBill;
    }
}