﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Harar
{
    public partial class SolidWasteDisposalRateEditor : Form
    {

        public SolidWasteDisposalRateEditor()
        {
            InitializeComponent();
            tabControl_SelectedIndexChanged(null, null);
        }

        public SolidWasteDisposalRateEditor(SolidWasteDisposalRate solidWasteDisposalRate)
            : this()
        {
            ratePrivate.initGrid();
            rateInstitutionalRate.initGrid();

            if (solidWasteDisposalRate == null)
                return;

            ratePrivate.setRate(solidWasteDisposalRate.privateRate);
            rateInstitutionalRate.setRate(solidWasteDisposalRate.institutionalRate);
            textCommunityRate.Text = solidWasteDisposalRate.communityPercentage.ToString();
            textRevenueShare.Text=solidWasteDisposalRate.waterCompanyShare.ToString();
            foreach (SolidWateDispoalSpecialRates r in solidWasteDisposalRate.specialSubType)
            {
                addSpecialRate(r);
            }
            accountingIncome.SetByID(solidWasteDisposalRate.incomeAccountID);
            accountPayable.SetByID(solidWasteDisposalRate.payableAccountID);
            chkEnableSolidWasteDiposalBill.Checked = solidWasteDisposalRate.enable;
        }

        private void addSpecialRate(SolidWateDispoalSpecialRates r)
        {
            int rowIndex = gridSpecialRate.Rows.Add(Subscriber.EnumToName(r.subscType) + " - " + SubscriberManagment.Client.SubscriberManagmentClient.getCustomerSubtype(r.subscType, r.subTypeID).name, r.rate);
            gridSpecialRate.Rows[rowIndex].Tag = r;
        }

        private void progressiveRateEditor1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                SolidWasteDisposalRate rate = new SolidWasteDisposalRate();
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textCommunityRate, "Please enter percentage for community water points", out rate.communityPercentage))
                    return;
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textRevenueShare, "Please enter revenue share percentage for the water company", out rate.waterCompanyShare))
                    return;
                string msg;
                if (!ratePrivate.validateInput(out  rate.privateRate, out msg))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(msg);
                    return;
                }
                if (!rateInstitutionalRate.validateInput(out  rate.institutionalRate, out msg))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(msg);
                    return;
                }
                rate.specialSubType = new SolidWateDispoalSpecialRates[gridSpecialRate.Rows.Count];
                int i = 0;
                foreach (DataGridViewRow row in gridSpecialRate.Rows)
                {
                    SolidWateDispoalSpecialRates sr = row.Tag as SolidWateDispoalSpecialRates;
                    sr.rate = Convert.ToDouble( row.Cells[1].Value);
                    rate.specialSubType[i] = sr;
                    i++;
                }
                rate.incomeAccountID = accountingIncome.GetAccountID();
                rate.payableAccountID = accountPayable.GetAccountID();
                if (rate.incomeAccountID == -1)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select income account");
                    return;
                }
                rate.enable = chkEnableSolidWasteDiposalBill.Checked;
                if (rate.payableAccountID== -1)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select payable account");
                    return;
                }
                SubscriberManagment.Client.SubscriberManagmentClient.setBillingRuleSetting(typeof(SolidWasteDisposalRate), rate);
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonAddSpecial.Visible = tabControl.SelectedIndex == 1;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddSpecial_Click(object sender, EventArgs e)
        {
            SpecialRateForm sr = new SpecialRateForm();
            if (sr.ShowDialog(this) == DialogResult.OK)
            {
                addSpecialRate(sr.rate);
            }
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }
    }
}
