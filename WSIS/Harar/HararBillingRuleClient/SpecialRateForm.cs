﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.SubscriberManagment.Harar
{
    public partial class SpecialRateForm : Form
    {
        public SolidWateDispoalSpecialRates rate;
        public SpecialRateForm()
        {
            InitializeComponent();
            this.comboType.Items.AddRange(Enum.GetNames(typeof(SubscriberType)));
            if (this.comboType.Items.Count > 0)
            {
                this.comboType.SelectedIndex = 0;
            }
            
        }
        void listSubtype()
        {
            comboSubtype.Items.Clear();
            SubscriberType type = (SubscriberType)Enum.Parse(typeof(SubscriberType), (string)this.comboType.SelectedItem);
            comboSubtype.Items.AddRange(SubscriberManagmentClient.getCustomerSubtypes(type));
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            listSubtype();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (comboSubtype.SelectedIndex == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select customer type");
                return;
            }
            SolidWateDispoalSpecialRates rate=new SolidWateDispoalSpecialRates();
            if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textFee, "Please enter valid fee", out rate.rate))
                return;
            rate.subscType = (SubscriberType)Enum.Parse(typeof(SubscriberType), (string)this.comboType.SelectedItem);
            rate.subTypeID = ((CustomerSubtype)comboSubtype.SelectedItem).id;
            this.rate = rate;
            this.DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
