﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INTAPS.SubscriberManagment.Harar {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class StringTable {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal StringTable() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("INTAPS.SubscriberManagment.Harar.StringTable", typeof(StringTable).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Amount.
        /// </summary>
        internal static string Amount {
            get {
                return ResourceManager.GetString("Amount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap backward1 {
            get {
                object obj = ResourceManager.GetObject("backward1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bill.
        /// </summary>
        internal static string Bill {
            get {
                return ResourceManager.GetString("Bill", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Casheir.
        /// </summary>
        internal static string Casheir {
            get {
                return ResourceManager.GetString("Casheir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contract No.
        /// </summary>
        internal static string Contract_No {
            get {
                return ResourceManager.GetString("Contract No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer Code.
        /// </summary>
        internal static string Customer_Code {
            get {
                return ResourceManager.GetString("Customer Code", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date.
        /// </summary>
        internal static string Date {
            get {
                return ResourceManager.GetString("Date", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap forward {
            get {
                object obj = ResourceManager.GetObject("forward", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap forward1 {
            get {
                object obj = ResourceManager.GetObject("forward1", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Item.
        /// </summary>
        internal static string Item {
            get {
                return ResourceManager.GetString("Item", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Month.
        /// </summary>
        internal static string Month {
            get {
                return ResourceManager.GetString("Month", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        internal static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No..
        /// </summary>
        internal static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Others.
        /// </summary>
        internal static string Others {
            get {
                return ResourceManager.GetString("Others", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Outstanding Bill.
        /// </summary>
        internal static string Outstanding_Bill {
            get {
                return ResourceManager.GetString("Outstanding Bill", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity.
        /// </summary>
        internal static string Quantity {
            get {
                return ResourceManager.GetString("Quantity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reading.
        /// </summary>
        internal static string Reading {
            get {
                return ResourceManager.GetString("Reading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Water Supply Information System.
        /// </summary>
        internal static string Receipt_Footer1 {
            get {
                return ResourceManager.GetString("Receipt_Footer1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created by INTAPS.
        /// </summary>
        internal static string Receipt_Footer2 {
            get {
                return ResourceManager.GetString("Receipt_Footer2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dire Dawa Water Supply and Sewerage Authority.
        /// </summary>
        internal static string Receipt_Title1 {
            get {
                return ResourceManager.GetString("Receipt_Title1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cash Receipt.
        /// </summary>
        internal static string Receipt_Title2 {
            get {
                return ResourceManager.GetString("Receipt_Title2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rent.
        /// </summary>
        internal static string Rent {
            get {
                return ResourceManager.GetString("Rent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Signature.
        /// </summary>
        internal static string Signature {
            get {
                return ResourceManager.GetString("Signature", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time.
        /// </summary>
        internal static string Time {
            get {
                return ResourceManager.GetString("Time", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Total.
        /// </summary>
        internal static string Total {
            get {
                return ResourceManager.GetString("Total", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unit Amount.
        /// </summary>
        internal static string Unit_Amount {
            get {
                return ResourceManager.GetString("Unit Amount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use.
        /// </summary>
        internal static string Use {
            get {
                return ResourceManager.GetString("Use", resourceCulture);
            }
        }
    }
}
