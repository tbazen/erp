using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CONNECTION_MAINTENANCE,priority=1)]
    public class ConnectionMaintenanceClientHarar : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ConnectionMaintenanceFormHarar(jobID, connection);
        }

        public string buildDataHTML(JobData job)
        {
            Harar.ConntectionMaintenanceHararData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_MAINTENANCE, 0, true) as Harar.ConntectionMaintenanceHararData;
            if (data == null)
                return "";

            Subscription subsc = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
            Subscriber newCustomer = job.customerID == -1 ? job.newCustomer : SubscriberManagmentClient.GetSubscriber(job.customerID);
            var conTable = createConnectionTable(subsc);
            string meterhtml;
            if (data.changeMeter)
            {
                meterhtml = string.Format("<br/><span class='jobSection1'>New Meter Detail</span><br/>{0}", createMeterHTML(data.meterData));
            }
            else
                meterhtml = "";
            return string.Format("<span class='jobSection1'>{0}</span>{1}{2}"
                , System.Web.HttpUtility.HtmlEncode("Connection Detail")
                , BIZNET.iERP.bERPHtmlBuilder.ToString(conTable)
                ,meterhtml
                );
        }


    }

     
}

