using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Harar.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CONNECTION_PENALITY)]
    public class ConnectionPenalityClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ConnectionPenalityForm(jobID, customer,connection);
        }

        public string buildDataHTML(JobData job)
        {
            ConnectionPenalityData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_PENALITY, 0, true) as ConnectionPenalityData;
            string html;
            if (data == null)
                html = "";
            else
            {
                html = string.Format("<br/><span class='jobSection1'>Penality Type: {0}</span><br/>",PenalityInstance.penalityText(data.penalityType));
                html += string.Format("<br/><span class='jobSection1'>Instance {0}</span><br/>", data.instanceCount);
            }
            return html;
        }


    }
}
