﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Harar.REClient
{
    public partial class ConnectionPenalityForm: NewApplicationForm
    {
        int _jobID;
        JobData _job;
        ApplicationPlaceHolder applicationPlaceHolder;
        Subscriber _customer = null;
        Subscription _connection = null;
        void addInstance(PenalityInstance inst)
        {
            int rowIndex=gridPenality.Rows.Add(
                new object[]
                {
                    new DateTime(inst.date).ToString("MMM dd,yyy")
                    ,PenalityInstance.penalityText(inst.type)
                    ,inst.description
                });
            gridPenality.Rows[rowIndex].Tag = inst;
        }
        public ConnectionPenalityForm(int jobID, Subscriber customer,Subscription connection)
        {
            InitializeComponent();
            applicationPlaceHolder = new ApplicationPlaceHolder();
            tabCustomer.Controls.Add(applicationPlaceHolder);
            applicationPlaceHolder.Dock = DockStyle.Fill;

            _customer = customer;
            _connection = connection;

            _jobID = jobID;
            if (_jobID == -1)
            {
                _job = null;
                comboType.SelectedIndex = 0;
                if (customer == null)
                    applicationPlaceHolder.setCustomer(null, true);
                else
                {
                    applicationPlaceHolder.setCustomer(customer, false);
                    applicationPlaceHolder.Enabled = false;
                }
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);

                ConnectionPenalityData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_PENALITY, 0, true) as ConnectionPenalityData;
                _job = job;
                textAmount.Text = data.otherAmount.ToString("#,#0.00");
                textDescription.Text = data.description;
                comboType.SelectedIndex = (int)data.penalityType;
                foreach (PenalityInstance inst in data.pastPenalities)
                {
                    addInstance(inst);
                }
                if(job.customerID==-1)
                    applicationPlaceHolder.setCustomer(job.newCustomer, true);
                else
                    applicationPlaceHolder.setCustomer(SubscriberManagmentClient.GetSubscriber(job.customerID), false);
            }      
        }
        
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CONNECTION_PENALITY;
                }
                else
                    app = _job;
                if (applicationPlaceHolder.isNewCustomerMode())
                {
                    if (!applicationPlaceHolder.validateCustomerData())
                        return;
                    app.newCustomer = applicationPlaceHolder.getCustomer();
                }
                else
                    app.customerID = _customer.id;

                ConnectionPenalityData data = new ConnectionPenalityData();
                data.penalityType = (PenalityType) comboType.SelectedIndex;
                data.pastPenalities = new PenalityInstance[gridPenality.Rows.Count];
                data.customer = _customer;
                data.connection = _connection;
                if (data.penalityType == PenalityType.Others)
                {
                    if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textAmount, "Please enter valid amount",out data.otherAmount))
                        return;
                }
                else
                    data.otherAmount = 0;
                int i = 0;
                foreach (DataGridViewRow row in gridPenality.Rows)
                {
                    data.pastPenalities[i++] = row.Tag as PenalityInstance;
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridPenality_CurrentCellChanged(object sender, EventArgs e)
        {
            buttonRemove.Enabled = gridPenality.CurrentRow != null
                && gridPenality.CurrentRow.Index > -1 && gridPenality.CurrentRow.Index < gridPenality.Rows.Count;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            PenalityInstanceForm f = new PenalityInstanceForm();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                addInstance(f.inst);
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            for(int i=gridPenality.SelectedRows.Count-1;i>=0;i--)
            {
                gridPenality.Rows.Remove(gridPenality.SelectedRows[i]);
            }
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (comboType.SelectedIndex == (int)PenalityType.Others)
            {
                textAmount.Enabled = true;
            }
            else
            {
                textAmount.Enabled = false;
                textAmount.Text = "";
            }
        }
    }

}
