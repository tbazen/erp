﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.WSIS.Job.Harar.REClient
{
    public partial class PenalityInstanceForm : Form
    {
        public PenalityInstance inst=null;
        public PenalityInstanceForm()
        {
            InitializeComponent();
            foreach(PenalityType typ in Enum.GetValues(typeof(PenalityType)))
            {
                comboType.Items.Add(PenalityInstance.penalityText(typ));
            }
            comboType.SelectedIndex = 0;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            inst = new PenalityInstance()
            {
                type=(PenalityType)comboType.SelectedIndex,
                description=textDescription.Text,
                date=date.DateTime.Date.Ticks,

            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
