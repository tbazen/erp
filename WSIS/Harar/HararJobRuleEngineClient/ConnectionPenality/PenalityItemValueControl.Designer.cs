﻿namespace INTAPS.WSIS.Job.Harar.REClient
{
    partial class PenalityItemValueControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.itemPrivate = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.itemInstitutional = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.itemOther = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.textPrivate1st = new System.Windows.Forms.TextBox();
            this.textPrivate2nd = new System.Windows.Forms.TextBox();
            this.textPrivate3rd = new System.Windows.Forms.TextBox();
            this.textComercial1st = new System.Windows.Forms.TextBox();
            this.textComercial2nd = new System.Windows.Forms.TextBox();
            this.textComercial3rd = new System.Windows.Forms.TextBox();
            this.textOther1st = new System.Windows.Forms.TextBox();
            this.textOther2nd = new System.Windows.Forms.TextBox();
            this.textOther3rd = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(14, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Private:";
            // 
            // itemPrivate
            // 
            this.itemPrivate.anobject = null;
            this.itemPrivate.Location = new System.Drawing.Point(123, 10);
            this.itemPrivate.Name = "itemPrivate";
            this.itemPrivate.Size = new System.Drawing.Size(201, 20);
            this.itemPrivate.TabIndex = 8;
            this.itemPrivate.ObjectChanged += new System.EventHandler(this.itemPrivate_ObjectChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Institutional:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Other:";
            // 
            // itemInstitutional
            // 
            this.itemInstitutional.anobject = null;
            this.itemInstitutional.Location = new System.Drawing.Point(123, 46);
            this.itemInstitutional.Name = "itemInstitutional";
            this.itemInstitutional.Size = new System.Drawing.Size(201, 20);
            this.itemInstitutional.TabIndex = 8;
            this.itemInstitutional.ObjectChanged += new System.EventHandler(this.itemPrivate_ObjectChanged);
            // 
            // itemOther
            // 
            this.itemOther.anobject = null;
            this.itemOther.Location = new System.Drawing.Point(123, 82);
            this.itemOther.Name = "itemOther";
            this.itemOther.Size = new System.Drawing.Size(201, 20);
            this.itemOther.TabIndex = 8;
            this.itemOther.ObjectChanged += new System.EventHandler(this.itemPrivate_ObjectChanged);
            // 
            // textPrivate1st
            // 
            this.textPrivate1st.Location = new System.Drawing.Point(350, 10);
            this.textPrivate1st.Name = "textPrivate1st";
            this.textPrivate1st.Size = new System.Drawing.Size(100, 20);
            this.textPrivate1st.TabIndex = 9;
            this.textPrivate1st.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textPrivate2nd
            // 
            this.textPrivate2nd.Location = new System.Drawing.Point(456, 10);
            this.textPrivate2nd.Name = "textPrivate2nd";
            this.textPrivate2nd.Size = new System.Drawing.Size(100, 20);
            this.textPrivate2nd.TabIndex = 9;
            this.textPrivate2nd.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textPrivate3rd
            // 
            this.textPrivate3rd.Location = new System.Drawing.Point(562, 10);
            this.textPrivate3rd.Name = "textPrivate3rd";
            this.textPrivate3rd.Size = new System.Drawing.Size(100, 20);
            this.textPrivate3rd.TabIndex = 9;
            this.textPrivate3rd.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textComercial1st
            // 
            this.textComercial1st.Location = new System.Drawing.Point(350, 46);
            this.textComercial1st.Name = "textComercial1st";
            this.textComercial1st.Size = new System.Drawing.Size(100, 20);
            this.textComercial1st.TabIndex = 9;
            this.textComercial1st.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textComercial2nd
            // 
            this.textComercial2nd.Location = new System.Drawing.Point(456, 46);
            this.textComercial2nd.Name = "textComercial2nd";
            this.textComercial2nd.Size = new System.Drawing.Size(100, 20);
            this.textComercial2nd.TabIndex = 9;
            this.textComercial2nd.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textComercial3rd
            // 
            this.textComercial3rd.Location = new System.Drawing.Point(562, 46);
            this.textComercial3rd.Name = "textComercial3rd";
            this.textComercial3rd.Size = new System.Drawing.Size(100, 20);
            this.textComercial3rd.TabIndex = 9;
            this.textComercial3rd.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textOther1st
            // 
            this.textOther1st.Location = new System.Drawing.Point(350, 82);
            this.textOther1st.Name = "textOther1st";
            this.textOther1st.Size = new System.Drawing.Size(100, 20);
            this.textOther1st.TabIndex = 9;
            this.textOther1st.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // textOther2nd
            // 
            this.textOther2nd.Location = new System.Drawing.Point(456, 82);
            this.textOther2nd.Name = "textOther2nd";
            this.textOther2nd.Size = new System.Drawing.Size(100, 20);
            this.textOther2nd.TabIndex = 9;
            this.textOther2nd.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textOther3rd
            // 
            this.textOther3rd.Location = new System.Drawing.Point(562, 82);
            this.textOther3rd.Name = "textOther3rd";
            this.textOther3rd.Size = new System.Drawing.Size(100, 20);
            this.textOther3rd.TabIndex = 9;
            this.textOther3rd.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // PenalityItemValueControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.textOther3rd);
            this.Controls.Add(this.textComercial3rd);
            this.Controls.Add(this.textPrivate3rd);
            this.Controls.Add(this.textOther2nd);
            this.Controls.Add(this.textOther1st);
            this.Controls.Add(this.textComercial2nd);
            this.Controls.Add(this.textComercial1st);
            this.Controls.Add(this.textPrivate2nd);
            this.Controls.Add(this.textPrivate1st);
            this.Controls.Add(this.itemOther);
            this.Controls.Add(this.itemInstitutional);
            this.Controls.Add(this.itemPrivate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "PenalityItemValueControl";
            this.Size = new System.Drawing.Size(685, 109);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemPrivate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemInstitutional;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemOther;
        private System.Windows.Forms.TextBox textPrivate1st;
        private System.Windows.Forms.TextBox textPrivate2nd;
        private System.Windows.Forms.TextBox textPrivate3rd;
        private System.Windows.Forms.TextBox textComercial1st;
        private System.Windows.Forms.TextBox textComercial2nd;
        private System.Windows.Forms.TextBox textComercial3rd;
        private System.Windows.Forms.TextBox textOther1st;
        private System.Windows.Forms.TextBox textOther2nd;
        private System.Windows.Forms.TextBox textOther3rd;
    }
}
