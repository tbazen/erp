﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Harar.REClient
{
    public partial class PenalityItemValueControl : UserControl
    {
        public PenalityItemValueControl()
        {
            InitializeComponent();
        }
        public event EventHandler changed = null;
        void OnChanged()
        {
            if (changed != null)
                changed(this, null);
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        TextBox[][] getTextBoxes()
        {
            return new TextBox[][]
            
            {
            new TextBox[]{textPrivate1st,textPrivate2nd,textPrivate3rd }
            ,new TextBox[]{textComercial1st,textComercial2nd,textComercial3rd}
            ,new TextBox[]{textOther1st,textOther2nd,textOther3rd}
            };
        }
        BIZNET.iERP.Client.TransactionItemPlaceHolder[] getItemPlaceHodlers()
        {
            return new BIZNET.iERP.Client.TransactionItemPlaceHolder[]
            {
                itemPrivate,itemInstitutional,itemOther
            };
        }
        public HararEstimationConfiguration.PenalityItemValue value
        {
            get
            {
                HararEstimationConfiguration.PenalityItemValue ret = new HararEstimationConfiguration.PenalityItemValue();
                if (itemPrivate.anobject != null)
                {
                    ret.privateValue.itemCode = itemPrivate.anobject.Code;
                    if (!double.TryParse(textPrivate1st.Text, out ret.privateValue.incedenseValues[0]))
                        ret.privateValue.incedenseValues[0] = 0;
                    if (!double.TryParse(textPrivate2nd.Text, out ret.privateValue.incedenseValues[1]))
                        ret.privateValue.incedenseValues[1] = 0;
                    if (!double.TryParse(textPrivate3rd.Text, out ret.privateValue.incedenseValues[2]))
                        ret.privateValue.incedenseValues[2] = 0;

                }
                if (itemInstitutional.anobject != null)
                {
                    ret.institutionalValue.itemCode = itemInstitutional.anobject.Code;
                    if (!double.TryParse(textComercial1st.Text, out ret.institutionalValue.incedenseValues[0]))
                        ret.institutionalValue.incedenseValues[0] = 0;
                    if (!double.TryParse(textComercial2nd.Text, out ret.institutionalValue.incedenseValues[1]))
                        ret.institutionalValue.incedenseValues[1] = 0;
                    if (!double.TryParse(textComercial3rd.Text, out ret.institutionalValue.incedenseValues[2]))
                        ret.institutionalValue.incedenseValues[2] = 0;
                }

                if (itemOther.anobject != null)
                {
                    ret.otherValue.itemCode = itemOther.anobject.Code;
                    if (!double.TryParse(textOther1st.Text, out ret.otherValue.incedenseValues[0]))
                        ret.otherValue.incedenseValues[0] = 0;
                    if (!double.TryParse(textOther2nd.Text, out ret.otherValue.incedenseValues[1]))
                        ret.otherValue.incedenseValues[1] = 0;
                    if (!double.TryParse(textOther3rd.Text, out ret.otherValue.incedenseValues[2]))
                        ret.otherValue.incedenseValues[2] = 0;
                }
                return ret;
            }
            set
            {
                if (value == null)
                {
                    foreach (TextBox[] tt in this.getTextBoxes())
                        foreach (TextBox t in tt)
                            t.Text = "";
                    foreach (BIZNET.iERP.Client.TransactionItemPlaceHolder it in getItemPlaceHodlers())
                        it.SetByID(null);
                }
                if (value.privateValue == null)
                {
                    foreach (TextBox t in getTextBoxes()[0])
                        t.Text = "";
                }
                else if (!string.IsNullOrEmpty(value.privateValue.itemCode))
                {
                    itemPrivate.SetByID(value.privateValue.itemCode);
                    TextBox[] t = getTextBoxes()[0];
                    for (int i = 0; i < 3; i++)
                    {
                        if (value.privateValue.incedenseValues[i] == 0)
                            t[i].Text= "";
                        else
                            t[i].Text = value.privateValue.incedenseValues[i].ToString("#,#0.00");
                    }
                }

                if (value.institutionalValue == null)
                {
                    foreach (TextBox t in getTextBoxes()[1])
                        t.Text = "";
                }
                else if (!string.IsNullOrEmpty(value.institutionalValue.itemCode))
                {
                    itemInstitutional.SetByID(value.institutionalValue.itemCode);
                    TextBox[] t = getTextBoxes()[1];
                    for (int i = 0; i < 3; i++)
                    {
                        if (value.institutionalValue.incedenseValues[i] == 0)
                            t[i].Text = "";
                        else
                            t[i].Text = value.institutionalValue.incedenseValues[i].ToString("#,#0.00");
                    }
                }

                if (value.otherValue == null)
                {
                    foreach (TextBox t in getTextBoxes()[2])
                        t.Text = "";
                }
                else if (!string.IsNullOrEmpty(value.otherValue.itemCode))
                {
                    itemOther.SetByID(value.otherValue.itemCode);
                    TextBox[] t = getTextBoxes()[2];
                    for (int i = 0; i < 3; i++)
                    {
                        if (value.otherValue.incedenseValues[i] == 0)
                            t[i].Text = "";
                        else
                            t[i].Text = value.otherValue.incedenseValues[i].ToString("#,#0.00");
                    }
                }
            }

        }

        private void itemPrivate_ObjectChanged(object sender, EventArgs e)
        {
            OnChanged();
        }
        private void textValueChanged(object sender, EventArgs e)
        {
            OnChanged();
        }
    }
}
