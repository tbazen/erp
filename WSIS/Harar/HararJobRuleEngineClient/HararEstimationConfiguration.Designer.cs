﻿namespace INTAPS.WSIS.Job.Harar.REClient
{
    partial class HararEstimationConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HararEstimationConfigurationForm));
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.itemsSurvey = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsWaterMeter = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridDeposit = new System.Windows.Forms.DataGridView();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDepositPrivate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDepositCommercial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridPrice = new System.Windows.Forms.DataGridView();
            this.colMeterType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPricePrivate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPriceCommercial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPenality = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typedItemLWDOutOfTown = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemLWDRegular = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemBWPublicFountain = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemBWSelfDelivery = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemBWOutOfTown = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemBWRegular = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemMeterDiag = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemOwnershipTransfer = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemTechnicalService = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemMatMargin = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemApproval = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemMeterTransfer = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemFolder = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemStampDuty = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemContractCard = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemApplicationForm = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemPremission = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemEstimation = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.typedItemOtherPenality = new INTAPS.WSIS.Job.Harar.REClient.ItemTypedValuesControl();
            this.penalityUnauthorizedUse = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.penalityIllegalConnection = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.penalityDamagedMeter = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.penalityOverPipeConstruction = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.penalityMeterReversing = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.penalityUnauthorizedRelocation = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.penalityMeterBypass = new INTAPS.WSIS.Job.Harar.REClient.PenalityItemValueControl();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPrice)).BeginInit();
            this.tabPenality.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(559, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(87, 30);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(663, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 30);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPenality);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(758, 511);
            this.tabControl.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.typedItemLWDOutOfTown);
            this.tabPage1.Controls.Add(this.typedItemLWDRegular);
            this.tabPage1.Controls.Add(this.typedItemBWPublicFountain);
            this.tabPage1.Controls.Add(this.typedItemBWSelfDelivery);
            this.tabPage1.Controls.Add(this.typedItemBWOutOfTown);
            this.tabPage1.Controls.Add(this.typedItemBWRegular);
            this.tabPage1.Controls.Add(this.typedItemMeterDiag);
            this.tabPage1.Controls.Add(this.typedItemOwnershipTransfer);
            this.tabPage1.Controls.Add(this.typedItemTechnicalService);
            this.tabPage1.Controls.Add(this.typedItemMatMargin);
            this.tabPage1.Controls.Add(this.typedItemApproval);
            this.tabPage1.Controls.Add(this.typedItemMeterTransfer);
            this.tabPage1.Controls.Add(this.typedItemFolder);
            this.tabPage1.Controls.Add(this.typedItemStampDuty);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.typedItemContractCard);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.typedItemApplicationForm);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.typedItemPremission);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.typedItemEstimation);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.itemsSurvey);
            this.tabPage1.Controls.Add(this.itemsWaterMeter);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(750, 485);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(19, 2935);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(173, 62);
            this.label29.TabIndex = 0;
            this.label29.Text = "Liquid Waste Disposal  Fee per Truck Load (Out of Town)";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(19, 2851);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(184, 62);
            this.label28.TabIndex = 0;
            this.label28.Text = "Liquid Waste Disposal Fee per Truck Load (Regular)";
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(19, 2753);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(173, 56);
            this.label32.TabIndex = 0;
            this.label32.Text = "Bulk Water Fee per Meter Cube (Community Water Point)";
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(19, 2661);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(173, 56);
            this.label27.TabIndex = 0;
            this.label27.Text = "Bulk Water Fee per Meter Cube (Self Delivery)";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(18, 2574);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(173, 56);
            this.label17.TabIndex = 0;
            this.label17.Text = "Bulk Water Fee per Meter Cube (Out of Town)";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(18, 2482);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(173, 56);
            this.label15.TabIndex = 0;
            this.label15.Text = "Bulk Water Fee per Meter Cube (Regular)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(19, 2396);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Water Meter Checking Fee";
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(19, 2201);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(151, 48);
            this.label31.TabIndex = 0;
            this.label31.Text = "Technical Servier (% of Mateiral Cost)";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(18, 2109);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(176, 17);
            this.label30.TabIndex = 0;
            this.label30.Text = "Material Supply Margin (%)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(19, 2304);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(153, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ownership Transfer Fee";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(19, 2017);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Letter of Approval Fee";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(19, 1925);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(164, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Water Meter Transfer Fee";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(19, 1825);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "File Folder Cover Fee";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(18, 1731);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Stamp Duty";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(19, 1638);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Contract Card Fee";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(19, 1546);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Application Form Fee";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(19, 1460);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Permission Fee";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(19, 1368);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Estimation Fee";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(19, 1323);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Survey Items:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(19, 1289);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Water Meter Items:";
            // 
            // itemsSurvey
            // 
            this.itemsSurvey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsSurvey.Location = new System.Drawing.Point(263, 1317);
            this.itemsSurvey.Name = "itemsSurvey";
            this.itemsSurvey.Size = new System.Drawing.Size(446, 28);
            this.itemsSurvey.TabIndex = 1;
            this.itemsSurvey.Changed += new System.EventHandler(this.valueChanged);
            // 
            // itemsWaterMeter
            // 
            this.itemsWaterMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsWaterMeter.Location = new System.Drawing.Point(263, 1283);
            this.itemsWaterMeter.Name = "itemsWaterMeter";
            this.itemsWaterMeter.Size = new System.Drawing.Size(446, 28);
            this.itemsWaterMeter.TabIndex = 1;
            this.itemsWaterMeter.Changed += new System.EventHandler(this.valueChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridDeposit);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(750, 485);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Deposit Rule";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridDeposit
            // 
            this.gridDeposit.AllowUserToAddRows = false;
            this.gridDeposit.AllowUserToDeleteRows = false;
            this.gridDeposit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colType,
            this.colDepositPrivate,
            this.colDepositCommercial});
            this.gridDeposit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDeposit.Location = new System.Drawing.Point(3, 3);
            this.gridDeposit.Name = "gridDeposit";
            this.gridDeposit.Size = new System.Drawing.Size(744, 479);
            this.gridDeposit.TabIndex = 0;
            this.gridDeposit.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridCellValueChanged);
            // 
            // colType
            // 
            this.colType.HeaderText = "Meter Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 86;
            // 
            // colDepositPrivate
            // 
            this.colDepositPrivate.HeaderText = "Desposit Amount (Private)";
            this.colDepositPrivate.Name = "colDepositPrivate";
            this.colDepositPrivate.Width = 141;
            // 
            // colDepositCommercial
            // 
            this.colDepositCommercial.HeaderText = "Deposit Amount (Commercial)";
            this.colDepositCommercial.Name = "colDepositCommercial";
            this.colDepositCommercial.Width = 155;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridPrice);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(750, 485);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "Water Meter Prices";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridPrice
            // 
            this.gridPrice.AllowUserToAddRows = false;
            this.gridPrice.AllowUserToDeleteRows = false;
            this.gridPrice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridPrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPrice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMeterType,
            this.colPricePrivate,
            this.colPriceCommercial});
            this.gridPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPrice.Location = new System.Drawing.Point(3, 3);
            this.gridPrice.Name = "gridPrice";
            this.gridPrice.Size = new System.Drawing.Size(744, 479);
            this.gridPrice.TabIndex = 1;
            this.gridPrice.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridCellValueChanged);
            // 
            // colMeterType
            // 
            this.colMeterType.HeaderText = "Meter Type";
            this.colMeterType.Name = "colMeterType";
            this.colMeterType.ReadOnly = true;
            this.colMeterType.Width = 86;
            // 
            // colPricePrivate
            // 
            this.colPricePrivate.HeaderText = "Price (Private)";
            this.colPricePrivate.Name = "colPricePrivate";
            this.colPricePrivate.Width = 98;
            // 
            // colPriceCommercial
            // 
            this.colPriceCommercial.HeaderText = "Price (Commercial)";
            this.colPriceCommercial.Name = "colPriceCommercial";
            this.colPriceCommercial.Width = 109;
            // 
            // tabPenality
            // 
            this.tabPenality.AutoScroll = true;
            this.tabPenality.BackColor = System.Drawing.Color.Gray;
            this.tabPenality.Controls.Add(this.typedItemOtherPenality);
            this.tabPenality.Controls.Add(this.label26);
            this.tabPenality.Controls.Add(this.label22);
            this.tabPenality.Controls.Add(this.label21);
            this.tabPenality.Controls.Add(this.label20);
            this.tabPenality.Controls.Add(this.label18);
            this.tabPenality.Controls.Add(this.label16);
            this.tabPenality.Controls.Add(this.label14);
            this.tabPenality.Controls.Add(this.label25);
            this.tabPenality.Controls.Add(this.label24);
            this.tabPenality.Controls.Add(this.label23);
            this.tabPenality.Controls.Add(this.label19);
            this.tabPenality.Controls.Add(this.label1);
            this.tabPenality.Controls.Add(this.penalityUnauthorizedUse);
            this.tabPenality.Controls.Add(this.penalityIllegalConnection);
            this.tabPenality.Controls.Add(this.penalityDamagedMeter);
            this.tabPenality.Controls.Add(this.penalityOverPipeConstruction);
            this.tabPenality.Controls.Add(this.penalityMeterReversing);
            this.tabPenality.Controls.Add(this.penalityUnauthorizedRelocation);
            this.tabPenality.Controls.Add(this.penalityMeterBypass);
            this.tabPenality.Location = new System.Drawing.Point(4, 22);
            this.tabPenality.Name = "tabPenality";
            this.tabPenality.Padding = new System.Windows.Forms.Padding(3);
            this.tabPenality.Size = new System.Drawing.Size(750, 485);
            this.tabPenality.TabIndex = 6;
            this.tabPenality.Text = "Detailed Penalities Rates";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(16, 34);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(139, 38);
            this.label26.TabIndex = 10;
            this.label26.Text = "Other Penality Income Items:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(16, 1032);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(254, 17);
            this.label22.TabIndex = 2;
            this.label22.Text = "Unauthorized Water Use Penality Rates";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(16, 895);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(220, 17);
            this.label21.TabIndex = 2;
            this.label21.Text = "Illegal Connection Penality Rates";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(16, 751);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(365, 17);
            this.label20.TabIndex = 2;
            this.label20.Text = "Damaged Meter Penality Rates (% of Water Meter Cost)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(16, 607);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(251, 17);
            this.label18.TabIndex = 2;
            this.label18.Text = "Over Pipe Construction Penality Rates";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(16, 458);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(202, 17);
            this.label16.TabIndex = 2;
            this.label16.Text = "Meter Reversing Penality Rates";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(16, 315);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(300, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "Unauthorized Meter Relocation Penality Rates";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(603, 148);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(28, 17);
            this.label25.TabIndex = 2;
            this.label25.Text = "3rd";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(492, 148);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 17);
            this.label24.TabIndex = 2;
            this.label24.Text = "2nd";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(398, 148);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 17);
            this.label23.TabIndex = 2;
            this.label23.Text = "1st";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(158, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(131, 17);
            this.label19.TabIndex = 2;
            this.label19.Text = "Income Item Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(16, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Meter Bypass Penality Rates";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 511);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(758, 36);
            this.panel1.TabIndex = 8;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Meter Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 79;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Desposit Amount";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 103;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Deposit Amount (Commercial)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 155;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Meter Type";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 79;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Price (Private)";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Price (Commercial)";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 109;
            // 
            // typedItemLWDOutOfTown
            // 
            this.typedItemLWDOutOfTown.Location = new System.Drawing.Point(263, 2935);
            this.typedItemLWDOutOfTown.Name = "typedItemLWDOutOfTown";
            this.typedItemLWDOutOfTown.Size = new System.Drawing.Size(470, 86);
            this.typedItemLWDOutOfTown.TabIndex = 8;
            this.typedItemLWDOutOfTown.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemLWDOutOfTown.values")));
            this.typedItemLWDOutOfTown.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemLWDRegular
            // 
            this.typedItemLWDRegular.Location = new System.Drawing.Point(263, 2851);
            this.typedItemLWDRegular.Name = "typedItemLWDRegular";
            this.typedItemLWDRegular.Size = new System.Drawing.Size(470, 86);
            this.typedItemLWDRegular.TabIndex = 8;
            this.typedItemLWDRegular.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemLWDRegular.values")));
            this.typedItemLWDRegular.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemBWPublicFountain
            // 
            this.typedItemBWPublicFountain.Location = new System.Drawing.Point(262, 2753);
            this.typedItemBWPublicFountain.Name = "typedItemBWPublicFountain";
            this.typedItemBWPublicFountain.Size = new System.Drawing.Size(470, 86);
            this.typedItemBWPublicFountain.TabIndex = 8;
            this.typedItemBWPublicFountain.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemBWPublicFountain.values")));
            this.typedItemBWPublicFountain.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemBWSelfDelivery
            // 
            this.typedItemBWSelfDelivery.Location = new System.Drawing.Point(262, 2661);
            this.typedItemBWSelfDelivery.Name = "typedItemBWSelfDelivery";
            this.typedItemBWSelfDelivery.Size = new System.Drawing.Size(470, 86);
            this.typedItemBWSelfDelivery.TabIndex = 8;
            this.typedItemBWSelfDelivery.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemBWSelfDelivery.values")));
            this.typedItemBWSelfDelivery.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemBWOutOfTown
            // 
            this.typedItemBWOutOfTown.Location = new System.Drawing.Point(262, 2574);
            this.typedItemBWOutOfTown.Name = "typedItemBWOutOfTown";
            this.typedItemBWOutOfTown.Size = new System.Drawing.Size(470, 86);
            this.typedItemBWOutOfTown.TabIndex = 8;
            this.typedItemBWOutOfTown.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemBWOutOfTown.values")));
            this.typedItemBWOutOfTown.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemBWRegular
            // 
            this.typedItemBWRegular.Location = new System.Drawing.Point(262, 2482);
            this.typedItemBWRegular.Name = "typedItemBWRegular";
            this.typedItemBWRegular.Size = new System.Drawing.Size(470, 86);
            this.typedItemBWRegular.TabIndex = 8;
            this.typedItemBWRegular.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemBWRegular.values")));
            this.typedItemBWRegular.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemMeterDiag
            // 
            this.typedItemMeterDiag.Location = new System.Drawing.Point(263, 2396);
            this.typedItemMeterDiag.Name = "typedItemMeterDiag";
            this.typedItemMeterDiag.Size = new System.Drawing.Size(470, 86);
            this.typedItemMeterDiag.TabIndex = 8;
            this.typedItemMeterDiag.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemMeterDiag.values")));
            this.typedItemMeterDiag.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemOwnershipTransfer
            // 
            this.typedItemOwnershipTransfer.Location = new System.Drawing.Point(263, 2304);
            this.typedItemOwnershipTransfer.Name = "typedItemOwnershipTransfer";
            this.typedItemOwnershipTransfer.Size = new System.Drawing.Size(470, 86);
            this.typedItemOwnershipTransfer.TabIndex = 8;
            this.typedItemOwnershipTransfer.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemOwnershipTransfer.values")));
            this.typedItemOwnershipTransfer.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemTechnicalService
            // 
            this.typedItemTechnicalService.Location = new System.Drawing.Point(263, 2201);
            this.typedItemTechnicalService.Name = "typedItemTechnicalService";
            this.typedItemTechnicalService.Size = new System.Drawing.Size(470, 86);
            this.typedItemTechnicalService.TabIndex = 8;
            this.typedItemTechnicalService.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemTechnicalService.values")));
            this.typedItemTechnicalService.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemMatMargin
            // 
            this.typedItemMatMargin.Location = new System.Drawing.Point(262, 2109);
            this.typedItemMatMargin.Name = "typedItemMatMargin";
            this.typedItemMatMargin.Size = new System.Drawing.Size(470, 86);
            this.typedItemMatMargin.TabIndex = 8;
            this.typedItemMatMargin.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemMatMargin.values")));
            this.typedItemMatMargin.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemApproval
            // 
            this.typedItemApproval.Location = new System.Drawing.Point(263, 2017);
            this.typedItemApproval.Name = "typedItemApproval";
            this.typedItemApproval.Size = new System.Drawing.Size(470, 86);
            this.typedItemApproval.TabIndex = 8;
            this.typedItemApproval.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemApproval.values")));
            this.typedItemApproval.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemMeterTransfer
            // 
            this.typedItemMeterTransfer.Location = new System.Drawing.Point(263, 1925);
            this.typedItemMeterTransfer.Name = "typedItemMeterTransfer";
            this.typedItemMeterTransfer.Size = new System.Drawing.Size(470, 86);
            this.typedItemMeterTransfer.TabIndex = 8;
            this.typedItemMeterTransfer.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemMeterTransfer.values")));
            this.typedItemMeterTransfer.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemFolder
            // 
            this.typedItemFolder.Location = new System.Drawing.Point(263, 1825);
            this.typedItemFolder.Name = "typedItemFolder";
            this.typedItemFolder.Size = new System.Drawing.Size(470, 86);
            this.typedItemFolder.TabIndex = 8;
            this.typedItemFolder.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemFolder.values")));
            this.typedItemFolder.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemStampDuty
            // 
            this.typedItemStampDuty.Location = new System.Drawing.Point(262, 1731);
            this.typedItemStampDuty.Name = "typedItemStampDuty";
            this.typedItemStampDuty.Size = new System.Drawing.Size(470, 86);
            this.typedItemStampDuty.TabIndex = 8;
            this.typedItemStampDuty.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemStampDuty.values")));
            this.typedItemStampDuty.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemContractCard
            // 
            this.typedItemContractCard.Location = new System.Drawing.Point(263, 1638);
            this.typedItemContractCard.Name = "typedItemContractCard";
            this.typedItemContractCard.Size = new System.Drawing.Size(470, 86);
            this.typedItemContractCard.TabIndex = 8;
            this.typedItemContractCard.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemContractCard.values")));
            this.typedItemContractCard.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemApplicationForm
            // 
            this.typedItemApplicationForm.Location = new System.Drawing.Point(263, 1546);
            this.typedItemApplicationForm.Name = "typedItemApplicationForm";
            this.typedItemApplicationForm.Size = new System.Drawing.Size(470, 86);
            this.typedItemApplicationForm.TabIndex = 8;
            this.typedItemApplicationForm.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemApplicationForm.values")));
            this.typedItemApplicationForm.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemPremission
            // 
            this.typedItemPremission.Location = new System.Drawing.Point(263, 1460);
            this.typedItemPremission.Name = "typedItemPremission";
            this.typedItemPremission.Size = new System.Drawing.Size(470, 86);
            this.typedItemPremission.TabIndex = 8;
            this.typedItemPremission.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemPremission.values")));
            this.typedItemPremission.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemEstimation
            // 
            this.typedItemEstimation.Location = new System.Drawing.Point(263, 1368);
            this.typedItemEstimation.Name = "typedItemEstimation";
            this.typedItemEstimation.Size = new System.Drawing.Size(470, 86);
            this.typedItemEstimation.TabIndex = 8;
            this.typedItemEstimation.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemEstimation.values")));
            this.typedItemEstimation.changed += new System.EventHandler(this.valueChanged);
            // 
            // typedItemOtherPenality
            // 
            this.typedItemOtherPenality.Location = new System.Drawing.Point(161, 34);
            this.typedItemOtherPenality.Name = "typedItemOtherPenality";
            this.typedItemOtherPenality.Size = new System.Drawing.Size(534, 86);
            this.typedItemOtherPenality.TabIndex = 11;
            this.typedItemOtherPenality.values = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.ItemTypedValue)(resources.GetObject("typedItemOtherPenality.values")));
            this.typedItemOtherPenality.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityUnauthorizedUse
            // 
            this.penalityUnauthorizedUse.BackColor = System.Drawing.Color.Gray;
            this.penalityUnauthorizedUse.ForeColor = System.Drawing.Color.White;
            this.penalityUnauthorizedUse.Location = new System.Drawing.Point(36, 1052);
            this.penalityUnauthorizedUse.Name = "penalityUnauthorizedUse";
            this.penalityUnauthorizedUse.Size = new System.Drawing.Size(685, 109);
            this.penalityUnauthorizedUse.TabIndex = 9;
            this.penalityUnauthorizedUse.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityUnauthorizedUse.value")));
            this.penalityUnauthorizedUse.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityIllegalConnection
            // 
            this.penalityIllegalConnection.BackColor = System.Drawing.Color.Gray;
            this.penalityIllegalConnection.ForeColor = System.Drawing.Color.White;
            this.penalityIllegalConnection.Location = new System.Drawing.Point(36, 915);
            this.penalityIllegalConnection.Name = "penalityIllegalConnection";
            this.penalityIllegalConnection.Size = new System.Drawing.Size(685, 109);
            this.penalityIllegalConnection.TabIndex = 8;
            this.penalityIllegalConnection.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityIllegalConnection.value")));
            this.penalityIllegalConnection.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityDamagedMeter
            // 
            this.penalityDamagedMeter.BackColor = System.Drawing.Color.Gray;
            this.penalityDamagedMeter.ForeColor = System.Drawing.Color.White;
            this.penalityDamagedMeter.Location = new System.Drawing.Point(36, 771);
            this.penalityDamagedMeter.Name = "penalityDamagedMeter";
            this.penalityDamagedMeter.Size = new System.Drawing.Size(685, 109);
            this.penalityDamagedMeter.TabIndex = 7;
            this.penalityDamagedMeter.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityDamagedMeter.value")));
            this.penalityDamagedMeter.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityOverPipeConstruction
            // 
            this.penalityOverPipeConstruction.BackColor = System.Drawing.Color.Gray;
            this.penalityOverPipeConstruction.ForeColor = System.Drawing.Color.White;
            this.penalityOverPipeConstruction.Location = new System.Drawing.Point(36, 627);
            this.penalityOverPipeConstruction.Name = "penalityOverPipeConstruction";
            this.penalityOverPipeConstruction.Size = new System.Drawing.Size(685, 109);
            this.penalityOverPipeConstruction.TabIndex = 5;
            this.penalityOverPipeConstruction.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityOverPipeConstruction.value")));
            this.penalityOverPipeConstruction.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityMeterReversing
            // 
            this.penalityMeterReversing.BackColor = System.Drawing.Color.Gray;
            this.penalityMeterReversing.ForeColor = System.Drawing.Color.White;
            this.penalityMeterReversing.Location = new System.Drawing.Point(36, 478);
            this.penalityMeterReversing.Name = "penalityMeterReversing";
            this.penalityMeterReversing.Size = new System.Drawing.Size(685, 109);
            this.penalityMeterReversing.TabIndex = 4;
            this.penalityMeterReversing.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityMeterReversing.value")));
            this.penalityMeterReversing.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityUnauthorizedRelocation
            // 
            this.penalityUnauthorizedRelocation.BackColor = System.Drawing.Color.Gray;
            this.penalityUnauthorizedRelocation.ForeColor = System.Drawing.Color.White;
            this.penalityUnauthorizedRelocation.Location = new System.Drawing.Point(36, 335);
            this.penalityUnauthorizedRelocation.Name = "penalityUnauthorizedRelocation";
            this.penalityUnauthorizedRelocation.Size = new System.Drawing.Size(685, 109);
            this.penalityUnauthorizedRelocation.TabIndex = 3;
            this.penalityUnauthorizedRelocation.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityUnauthorizedRelocation.value")));
            this.penalityUnauthorizedRelocation.changed += new System.EventHandler(this.valueChanged);
            // 
            // penalityMeterBypass
            // 
            this.penalityMeterBypass.BackColor = System.Drawing.Color.Gray;
            this.penalityMeterBypass.ForeColor = System.Drawing.Color.White;
            this.penalityMeterBypass.Location = new System.Drawing.Point(36, 188);
            this.penalityMeterBypass.Name = "penalityMeterBypass";
            this.penalityMeterBypass.Size = new System.Drawing.Size(685, 109);
            this.penalityMeterBypass.TabIndex = 0;
            this.penalityMeterBypass.value = ((INTAPS.WSIS.Job.Harar.HararEstimationConfiguration.PenalityItemValue)(resources.GetObject("penalityMeterBypass.value")));
            this.penalityMeterBypass.changed += new System.EventHandler(this.valueChanged);
            // 
            // HararEstimationConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 547);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Name = "HararEstimationConfigurationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estimation Configuration";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPrice)).EndInit();
            this.tabPenality.ResumeLayout(false);
            this.tabPenality.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView gridDeposit;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
        private Client.ItemListPlaceHolder itemsSurvey;
        private Client.ItemListPlaceHolder itemsWaterMeter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDepositPrivate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDepositCommercial;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView gridPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMeterType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPricePrivate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPriceCommercial;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.TabPage tabPenality;
        private PenalityItemValueControl penalityMeterBypass;
        private System.Windows.Forms.Label label1;
        private PenalityItemValueControl penalityUnauthorizedRelocation;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private PenalityItemValueControl penalityMeterReversing;
        private System.Windows.Forms.Label label18;
        private PenalityItemValueControl penalityOverPipeConstruction;
        private PenalityItemValueControl penalityDamagedMeter;
        private System.Windows.Forms.Label label20;
        private PenalityItemValueControl penalityIllegalConnection;
        private System.Windows.Forms.Label label21;
        private PenalityItemValueControl penalityUnauthorizedUse;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private ItemTypedValuesControl typedItemEstimation;
        private System.Windows.Forms.Label label26;
        private ItemTypedValuesControl typedItemMeterDiag;
        private ItemTypedValuesControl typedItemOwnershipTransfer;
        private ItemTypedValuesControl typedItemApproval;
        private ItemTypedValuesControl typedItemMeterTransfer;
        private ItemTypedValuesControl typedItemStampDuty;
        private ItemTypedValuesControl typedItemContractCard;
        private System.Windows.Forms.Label label11;
        private ItemTypedValuesControl typedItemApplicationForm;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private ItemTypedValuesControl typedItemPremission;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private ItemTypedValuesControl typedItemBWSelfDelivery;
        private ItemTypedValuesControl typedItemBWOutOfTown;
        private ItemTypedValuesControl typedItemBWRegular;
        private ItemTypedValuesControl typedItemFolder;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private ItemTypedValuesControl typedItemLWDOutOfTown;
        private ItemTypedValuesControl typedItemLWDRegular;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private ItemTypedValuesControl typedItemTechnicalService;
        private ItemTypedValuesControl typedItemMatMargin;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label17;
        private ItemTypedValuesControl typedItemBWPublicFountain;
        private System.Windows.Forms.Label label32;
        private ItemTypedValuesControl typedItemOtherPenality;
    }
}