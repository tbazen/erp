﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI;
using System.Collections;

namespace INTAPS.WSIS.Job.Harar.REClient
{
    public partial class HararEstimationConfigurationForm : Form
    {
        HararEstimationConfiguration config;
        bool changed=false;
        int typeID;
        bool _ignoreEvents = false;
        public HararEstimationConfigurationForm()
        {
            _ignoreEvents = true;
            try
            {
            InitializeComponent();
            initMeterGrid(gridPrice);
            initMeterGrid(gridDeposit);
            //initMeterGrid(gridMeterService);
            buttonOk.Enabled = false;
            typeID = JobManagerClient.getJobTypeByClientHandlerType(typeof(HararNewLineClient));
            config = JobManagerClient.getConfiguration(typeID, typeof(HararEstimationConfiguration)) as HararEstimationConfiguration;
            if (config == null)
            {
                config = new HararEstimationConfiguration();
                onChanged();
            }
            
                itemsWaterMeter.setItems(config.waterMeterItems);
                itemsSurvey.setItems(config.surveyItems);

                typedItemEstimation.values= config.estimationValues;
                typedItemPremission.values= config.premissionValues;
                typedItemApplicationForm.values= config.applicationFormValues;
                typedItemContractCard.values= config.contractCardValues;
                setGridValues(gridPrice,config.meterPrice);        
                typedItemMeterTransfer.values= config.waterMeterTransferValues;
                typedItemApproval.values= config.letterOfApprovalValues;
                typedItemOwnershipTransfer.values= config.ownershipTransferValues;
                typedItemMeterDiag.values= config.waterMeterDiagnosisValues;   
                setGridValues(gridDeposit,config.meterDeposit);           
                //setGridValues(gridMeterService,config.meterServiceValues);
                
                typedItemMatMargin.values= config.materialProfitMargin;         
                typedItemTechnicalService.values= config.technicalServiceMultiplier;
                
                typedItemFolder.values= config.fileFolderCostValues;
                typedItemStampDuty.values= config.stampDutyValues;              
                
                typedItemBWRegular.values= config.bwRegularValues;
                typedItemBWOutOfTown.values= config.bwOutOfTownValues;
                typedItemBWSelfDelivery.values= config.bwSelfDeliveryValues;
                typedItemBWPublicFountain.values = config.bwPublicFountain;

                typedItemLWDRegular.values= config.lwdRegularValues;
                typedItemLWDOutOfTown.values = config.lwdOutOfTownValues;
                
                typedItemOtherPenality.values=config.otherPenalityItem;
                penalityMeterBypass.value = config.penalityMeterByPass;
                penalityUnauthorizedRelocation.value = config.penalityMeterRelocation;
                penalityMeterReversing.value = config.penalityMeterReversing;
                penalityOverPipeConstruction.value = config.penalityOverPipeConstruction;
                penalityDamagedMeter.value = config.penalityMeterDamege;
                penalityIllegalConnection.value = config.penalityIllegalConnection;
                penalityUnauthorizedUse.value = config.penalityIllegalWaterUse;

            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void onChanged()
        {
            if (_ignoreEvents)
                return;
            buttonOk.Enabled = true;
            changed = true;
        }
        
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (!changed)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Nothing changed");
                return;
            }
            try
            {
                HararEstimationConfiguration config = new HararEstimationConfiguration();
                config.waterMeterItems = itemsWaterMeter.getItems();
                config.surveyItems = itemsSurvey.getItems();
                config.estimationValues = typedItemEstimation.values;
                config.premissionValues = typedItemPremission.values;
                config.applicationFormValues = typedItemApplicationForm.values;
                config.contractCardValues = typedItemContractCard.values;
                config.meterPrice = getGridValues(gridPrice);
                config.waterMeterTransferValues = typedItemMeterTransfer.values;
                config.letterOfApprovalValues = typedItemApproval.values;
                config.ownershipTransferValues = typedItemOwnershipTransfer.values;
                config.waterMeterDiagnosisValues = typedItemMeterDiag.values;
                config.meterDeposit = getGridValues(gridDeposit);
                //config.meterServiceValues = getGridValues(gridMeterService);

                config.materialProfitMargin = typedItemMatMargin.values;
                config.technicalServiceMultiplier = typedItemTechnicalService.values;

                config.fileFolderCostValues = typedItemFolder.values;
                config.stampDutyValues = typedItemStampDuty.values;

                config.bwRegularValues = typedItemBWRegular.values;
                config.bwOutOfTownValues = typedItemBWOutOfTown.values;
                config.bwSelfDeliveryValues = typedItemBWSelfDelivery.values;
                config.bwPublicFountain = typedItemBWPublicFountain.values;

                config.lwdRegularValues = typedItemLWDRegular.values;
                config.lwdOutOfTownValues = typedItemLWDOutOfTown.values;

                config.otherPenalityItem = typedItemOtherPenality.values;

                config.penalityMeterByPass = penalityMeterBypass.value;
                config.penalityMeterRelocation = penalityUnauthorizedRelocation.value;
                config.penalityMeterReversing = penalityMeterReversing.value;
                config.penalityOverPipeConstruction = penalityOverPipeConstruction.value;
                config.penalityMeterDamege = penalityDamagedMeter.value;
                config.penalityIllegalConnection = penalityIllegalConnection.value;
                config.penalityIllegalWaterUse = penalityUnauthorizedUse.value;
                
                JobManagerClient.saveConfiguration(typeID, config);
                changed = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        HararEstimationConfiguration.ItemTypedValue[] getGridValues(DataGridView grid)
        {
            List<HararEstimationConfiguration.ItemTypedValue> _ret = new List<HararEstimationConfiguration.ItemTypedValue>();
            foreach (DataGridViewRow row in (IEnumerable)grid.Rows)
            {
                HararEstimationConfiguration.ItemTypedValue r = new HararEstimationConfiguration.ItemTypedValue();
                r.privateItemCode = (string)row.Tag;
                if (row.Cells[1].Value is double)
                {
                    r.privateValue = (double)row.Cells[1].Value;
                    _ret.Add(r);
                }
                if (row.Cells[2].Value is double)
                {
                    r.institutionalValue = (double)row.Cells[2].Value;
                }
                if (r.institutionalValue != 0 || r.privateValue != 0)
                    _ret.Add(r);
            }
            return _ret.ToArray();
        }
        void setGridValues(DataGridView grid,HararEstimationConfiguration.ItemTypedValue[] values)
        {
            foreach (DataGridViewRow row in (IEnumerable)grid.Rows)
            {
                string code = (string)row.Tag;
                for (int i = 0; i < values.Length; i++)
                {
                    if (code.Equals(values[i].privateItemCode))
                    {
                        if(values[i].privateValue!=0)
                            row.Cells[1].Value = values[i].privateValue;
                        if (values[i].institutionalValue != 0)
                            row.Cells[2].Value = values[i].institutionalValue;
                        break;
                    }
                }
            }
        }
        private void initMeterGrid(DataGridView grid)
        {
            int systemParameter = (int)SubscriberManagmentClient.GetSystemParameter("meterMaterialCategory");
            grid.Columns[1].ValueType = typeof(double);
            grid.Columns[2].ValueType = typeof(double);
            if (systemParameter > 1)
            {
                try
                {
                    BIZNET.iERP.TransactionItems[] descriptionArray = BIZNET.iERP.Client.iERPTransactionClient.GetItemsInCategory(systemParameter);
                    if (descriptionArray.Length == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No meter types found.");
                    }
                    foreach (BIZNET.iERP.TransactionItems description in descriptionArray)
                    {
                        object[] values = new object[3];
                        values[0] = description.Name;
                        grid.Rows[grid.Rows.Add(values)].Tag = description.Code;
                    }
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error loading meter types.", exception);
                }
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (changed)
                e.Cancel = !INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Do you want to close without saving the changes?");
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void valueChanged(object sender, EventArgs e)
        {
            this.onChanged();
        }

        private void gridCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.onChanged();
        }
        
    }
}
