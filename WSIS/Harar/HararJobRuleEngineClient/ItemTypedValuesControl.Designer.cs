﻿namespace INTAPS.WSIS.Job.Harar.REClient
{
    partial class ItemTypedValuesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemPrivate = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.textPrivateValue = new System.Windows.Forms.TextBox();
            this.itemInstitutional = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.textInstitutionalValue = new System.Windows.Forms.TextBox();
            this.itemOther = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.textOtherValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // itemPrivate
            // 
            this.itemPrivate.anobject = null;
            this.itemPrivate.Location = new System.Drawing.Point(76, 3);
            this.itemPrivate.Name = "itemPrivate";
            this.itemPrivate.Size = new System.Drawing.Size(250, 20);
            this.itemPrivate.TabIndex = 8;
            this.itemPrivate.ObjectChanged += new System.EventHandler(this.itemPrivate_ObjectChanged);
            // 
            // textPrivateValue
            // 
            this.textPrivateValue.Location = new System.Drawing.Point(332, 3);
            this.textPrivateValue.Name = "textPrivateValue";
            this.textPrivateValue.Size = new System.Drawing.Size(114, 20);
            this.textPrivateValue.TabIndex = 9;
            this.textPrivateValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textPrivateValue.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // itemInstitutional
            // 
            this.itemInstitutional.anobject = null;
            this.itemInstitutional.Location = new System.Drawing.Point(76, 29);
            this.itemInstitutional.Name = "itemInstitutional";
            this.itemInstitutional.Size = new System.Drawing.Size(250, 20);
            this.itemInstitutional.TabIndex = 8;
            this.itemInstitutional.ObjectChanged += new System.EventHandler(this.itemPrivate_ObjectChanged);
            // 
            // textInstitutionalValue
            // 
            this.textInstitutionalValue.Location = new System.Drawing.Point(332, 29);
            this.textInstitutionalValue.Name = "textInstitutionalValue";
            this.textInstitutionalValue.Size = new System.Drawing.Size(114, 20);
            this.textInstitutionalValue.TabIndex = 9;
            this.textInstitutionalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textInstitutionalValue.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // itemOther
            // 
            this.itemOther.anobject = null;
            this.itemOther.Location = new System.Drawing.Point(76, 55);
            this.itemOther.Name = "itemOther";
            this.itemOther.Size = new System.Drawing.Size(250, 20);
            this.itemOther.TabIndex = 8;
            this.itemOther.ObjectChanged += new System.EventHandler(this.itemPrivate_ObjectChanged);
            // 
            // textOtherValue
            // 
            this.textOtherValue.Location = new System.Drawing.Point(332, 55);
            this.textOtherValue.Name = "textOtherValue";
            this.textOtherValue.Size = new System.Drawing.Size(114, 20);
            this.textOtherValue.TabIndex = 9;
            this.textOtherValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textOtherValue.TextChanged += new System.EventHandler(this.textValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Private:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Institutional:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Other:";
            // 
            // ItemTypedValuesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textOtherValue);
            this.Controls.Add(this.textInstitutionalValue);
            this.Controls.Add(this.itemOther);
            this.Controls.Add(this.itemInstitutional);
            this.Controls.Add(this.textPrivateValue);
            this.Controls.Add(this.itemPrivate);
            this.Name = "ItemTypedValuesControl";
            this.Size = new System.Drawing.Size(470, 86);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemPrivate;
        private System.Windows.Forms.TextBox textPrivateValue;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemInstitutional;
        private System.Windows.Forms.TextBox textInstitutionalValue;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemOther;
        private System.Windows.Forms.TextBox textOtherValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}
