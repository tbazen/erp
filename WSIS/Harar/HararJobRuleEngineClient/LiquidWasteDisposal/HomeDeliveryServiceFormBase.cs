﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS;
namespace INTAPS.WSIS.Job.Harar.REClient
{
    public abstract partial class HomeDeliveryServiceFormBase<DataType> : NewApplicationForm where DataType:HomeDeliveryServiceDataBase,new()
    {
        int _jobID;
        JobData _job;
        ApplicationPlaceHolder applicationPlaceHolder;
        Subscriber _customer = null;
        DeliveryType[] _deliveryTypes;
        public HomeDeliveryServiceFormBase(int jobID, Subscriber customer)
        {
            InitializeComponent();
            foreach (DeliveryType dt in _deliveryTypes=this.deliveryTypes)
            {
                comboDeliveryType.Items.Add(HomeDeliveryServiceDataBase.getDeliveryTypeString(dt));
            }
            comboDeliveryType.SelectedIndex = 0;
            labelWorkVolume.Text = "Volume ({0}):".format(this.measuringUnit);
            applicationPlaceHolder = new ApplicationPlaceHolder();
            tabPage2.Controls.Add(applicationPlaceHolder);
            applicationPlaceHolder.Dock = DockStyle.Fill;

            _customer = customer;
            
            _jobID = jobID;
            if (_jobID == -1)
            {
                _job = null;
                textWorkAmount.Text = "1";
                if (customer != null)
                {
                    textAddress.Text = SubscriberManagmentClient.getCustomerAddressString(customer);
                    textPhoneNumber.Text = customer.phoneNo;
                    textContactPerson.Text = customer.name;
                }
                if (customer == null)
                    applicationPlaceHolder.setCustomer(null, true);
                else
                {
                    applicationPlaceHolder.setCustomer(customer, false);
                    applicationPlaceHolder.Enabled = false;
                }
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);

                DataType data = JobManagerClient.getWorkFlowData(job.id, this.JobTypeID, 0, true) as DataType;
                _job = job;
                textWorkAmount.Text = data.workAmount.ToString();
                textAddress.Text = data.address;
                textPhoneNumber.Text = data.phoneNo;
                textContactPerson.Text = data.contactPerson;
                comboDeliveryType.SelectedIndex = ArrayExtension.indexOf(_deliveryTypes, data.deliveryType);
                if(job.customerID==-1)
                    applicationPlaceHolder.setCustomer(job.newCustomer, true);
                else
                    applicationPlaceHolder.setCustomer(SubscriberManagmentClient.GetSubscriber(job.customerID), false);
            }      
        }
        public abstract int JobTypeID { get; }
        protected abstract DeliveryType[] deliveryTypes
        {
            get;
        }
        public abstract string measuringUnit
        {
            get;
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = JobTypeID;
                }
                else
                    app = _job;
                if (applicationPlaceHolder.isNewCustomerMode())
                {
                    if (!applicationPlaceHolder.validateCustomerData())
                        return;
                    app.newCustomer = applicationPlaceHolder.getCustomer();
                }
                else
                    app.customerID = _customer.id;

                DataType data = new DataType();
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textWorkAmount, "Please enter valid work amount", out data.workAmount))
                    return;
                if (!INTAPS.UI.Helper.ValidateNonEmptyTextBox(textAddress, "Please enter valid address", out data.address))
                    return;
                if (!INTAPS.UI.Helper.ValidateNonEmptyTextBox(textContactPerson, "Please enter valid contact person", out data.contactPerson))
                    return;
                if (string.IsNullOrEmpty(textPhoneNumber.Text.Trim()))
                {
                    if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to save without phone number"))
                        return;
                }
                data.phoneNo = textPhoneNumber.Text.Trim();
                data.deliveryType = _deliveryTypes[comboDeliveryType.SelectedIndex];
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}
