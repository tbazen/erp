using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Harar.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.LIQUID_WASTE_DISPOSAL)]
    public class LiquidWasteDisposalClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new LiquidWasteDisposalForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            LiquidWasteDisposalData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.LIQUID_WASTE_DISPOSAL, 0, true) as LiquidWasteDisposalData;
            string html;
            if (data == null)
                html = "";
            else
            {
                html = string.Format("<br/><span class='jobSection1'>Volume (Truck Load): {0}</span><br/>",data.workAmount);
            }
            return html;
        }


    }
    public class LiquidWasteDisposalForm : HomeDeliveryServiceFormBase<LiquidWasteDisposalData>
    {
        public LiquidWasteDisposalForm(int jobID, Subscriber customer)
            : base(jobID, customer)
        {
        }

        public override int JobTypeID
        {
            get { return StandardJobTypes.LIQUID_WASTE_DISPOSAL; }
        }

        protected override DeliveryType[] deliveryTypes
        {
            get { return new DeliveryType[] {DeliveryType.Regular,DeliveryType.OutOfTown }; }
        }

        public override string measuringUnit
        {
            get { return "Truck Load"; }
        }
    }
}
