using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Harar;
namespace INTAPS.WSIS.Job.Harar.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_MAINTENANCE, "Existing Connection Service",priority=1)]
    public class RESConnectionMaintenanceHarar :RESTechnicalServiceHarar<ConntectionMaintenanceHararData>, IJobRuleServerHandler
    {
        public RESConnectionMaintenanceHarar(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            if (bde.subscriberBDE.GetSubscription(((ConntectionMaintenanceHararData)data).connectionID, date.Ticks).subscriptionStatus == SubscriptionStatus.Discontinued)
                throw new ServerUserMessage("This connection is discontinued");
            base.createApplication(AID, date, job, worker, data, note);
        }
        public override int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            Harar.ConntectionMaintenanceHararData data;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as Harar.ConntectionMaintenanceHararData;
                    if(data.hasApplicationFee)
                        return new int[] { StandardJobStatus.APPLICATION_PAYMENT, StandardJobStatus.CANCELED };
                    else
                        return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_PAYMENT:
                    data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as Harar.ConntectionMaintenanceHararData;
                    if(data.estimation)
                        return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                     if(data.meterDiagnosis||data.relocateMeter)
                        return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                     return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.SURVEY:
                    return new int[] { StandardJobStatus.ANALYSIS, StandardJobStatus.CANCELED };
                case StandardJobStatus.ANALYSIS:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINISHED:
                    return new int[] { };
                default:
                    return new int[0];
            }
        }

        protected override void onForwardToApplicationPaymet(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, HararEstimationConfiguration config)
        {
            ConntectionMaintenanceHararData data = base.checkAndGetData(job);

            Subscriber customer = bde.getJobCustomer(job);
            JobBillOfMaterial appPaymentBOM = new JobBillOfMaterial();
            appPaymentBOM.jobID = job.id;
            appPaymentBOM.analysisTime = DateTime.Now;
            appPaymentBOM.note = "Application Payment";
            List<JobBOMItem> items = new List<JobBOMItem>();
            if (data.estimation)
                items.AddRange(new JobBOMItem[]{
                        new JobBOMItem(){
                            description="Estimation",
                             itemID=config.estimationValues.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.estimationValues.getValue(customer.subscriberType),
                             calculated=true
                        }
                        ,new JobBOMItem(){
                            description="Application Form",
                             itemID=config.applicationFormValues.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.applicationFormValues.getValue(customer.subscriberType),
                             calculated=true
                        }
                    });
            if (data.meterDiagnosis)
            {
                items.Add(
                new JobBOMItem()
                {
                    description = "Meter Diagnosis",
                    itemID = config.waterMeterDiagnosisValues.getItemCode(customer.subscriberType),
                    inSourced = true,
                    quantity = 1,
                    referenceUnitPrice = 0,
                    unitPrice = config.waterMeterDiagnosisValues.getValue(customer.subscriberType),
                    calculated = true
                });
            }

            if (data.relocateMeter)
            {
                items.Add(
                new JobBOMItem()
                {
                    description = "Meter Relocation",
                    itemID = config.waterMeterTransferValues.getItemCode(customer.subscriberType),
                    inSourced = true,
                    quantity = 1,
                    referenceUnitPrice = 0,
                    unitPrice = config.waterMeterTransferValues.getValue(customer.subscriberType),
                    calculated = true
                });
            }
            if (data.supportLetter)
            {
                items.Add(
                new JobBOMItem()
                {
                    description = "Support Letter",
                    itemID = config.letterOfApprovalValues.getItemCode(customer.subscriberType),
                    inSourced = true,
                    quantity = 1,
                    referenceUnitPrice = 0,
                    unitPrice = config.letterOfApprovalValues.getValue(customer.subscriberType),
                    calculated = true
                });
            }
            _ignoreAID = AID;
            appPaymentBOM.items = items.ToArray();
            appPaymentBOM.id = bde.SetBillofMateial(AID, appPaymentBOM, true);
            bde.generateInvoice(AID, appPaymentBOM.id, worker);
        }
        protected override void onForwardToFinalization(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, HararEstimationConfiguration config)
        {
            checkPayment(job.id);
        }
        public override void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            ConntectionMaintenanceHararData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            ReadingEntryClerk clerk = bde.subscriberBDE.BWFGetClerkByUserID(worker.userID);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    if (data.changeMeter)
                    {
                        BillPeriod period = bde.subscriberBDE.getPeriodForDate(date);
                        if (period == null)
                            throw new ServerUserMessage("Bill period not defined for time " + date);
                        ReadingReset reset = new ReadingReset();
                        reset.connectionID = data.connectionID;
                        reset.periodID = period.id;
                        reset.reading = data.meterData.initialMeterReading;
                        reset.ticks = date.Ticks;
                        bde.subscriberBDE.setReadingReset(AID, clerk, reset);
                        bde.subscriberBDE.ChangeSubcriberMeter(AID, data.connectionID, date, data.meterData);
                    }
                    
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override void onBOMSet(int AID, JobData job, JobBillOfMaterial bom)
        {
            if (AID == _ignoreAID)
                return;
            Harar.ConntectionMaintenanceHararData data = bde.getWorkFlowData(job.id, this.getTypeID(), 0, true) as Harar.ConntectionMaintenanceHararData;
            List<JobBOMItem> meters = getMeterItems(new JobBillOfMaterial[] { bom });
            if (meters.Count > 0)
            {
                if (meters.Count != 1 || meters[0].quantity != 1)
                    throw new ServerUserMessage("Only one meter item should be included in the bill of quantity");
                if (data.meterData == null)
                    data.meterData = new MeterData();
                data.meterData.itemCode = meters[0].itemID;
                data.changeMeter = true;
                bde.setWorkFlowData(AID, data);
            }
            base.onBOMSet(AID, job, bom);
        }
    }
}
