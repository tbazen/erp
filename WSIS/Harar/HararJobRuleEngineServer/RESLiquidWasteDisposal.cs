using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job.Harar.REServer
{
    [JobRuleServerHandler(StandardJobTypes.LIQUID_WASTE_DISPOSAL, "Liquid Waste Disposal")]
    public class RESLiquidWasteDisposal : RESHomeDeliveryServiceBase<LiquidWasteDisposalData>, IJobRuleServerHandler
    {
        public RESLiquidWasteDisposal(JobManagerBDE bde)
            : base(bde)
        {
        }

        public override JobBillOfMaterial getBOM(Subscriber customer,JobData job,HararEstimationConfiguration config)
        {
            LiquidWasteDisposalData data = checkAndGetData(job);

            JobBillOfMaterial bom = new JobBillOfMaterial();
            bom.jobID = job.id;
            bom.note = "Liquid Waste Disposal Fee";
            JobBOMItem ji = new JobBOMItem();
            HararEstimationConfiguration.ItemTypedValue setting = null;
            switch (data.deliveryType)
            {
                case DeliveryType.Regular:
                    setting = config.lwdRegularValues;
                    break;
                case DeliveryType.OutOfTown:
                    setting = config.lwdOutOfTownValues;
                    break;
            }
            if (setting == null)
                throw new ServerUserMessage("Please configure liquid waste disposal rates properly");
            string itemCode = setting.getItemCode(customer.subscriberType);
            if (string.IsNullOrEmpty(itemCode))
                throw new ServerUserMessage("Please configure liquid waste disposal rates properly");
            
            BIZNET.iERP.TransactionItems item = bde.bERP.GetTransactionItems(itemCode);
            if (item == null)
                throw new ServerUserMessage("Liquid waste disposal item is not valid. It is probably deleted");
            ji.itemID = itemCode;
            ji.inSourced = true;
            ji.description = item.Name;
            ji.quantity = data.workAmount;
            ji.unitPrice = setting.getValue(customer.subscriberType);
            ji.calculated = true;

            bom.items = new JobBOMItem[] { ji };
            return bom;
        }
    }
    [JobRuleServerHandler(StandardJobTypes.HYDRANT_SERVICE, "Bulk Water Supply")]
    public class RESHydrantWaterSupply: RESHomeDeliveryServiceBase<HydrantWaterSupplyData>, IJobRuleServerHandler
    {
        public RESHydrantWaterSupply(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override JobBillOfMaterial getBOM(Subscriber customer, JobData job, HararEstimationConfiguration config)
        {

            HydrantWaterSupplyData data=checkAndGetData(job);
            
            JobBillOfMaterial bom = new JobBillOfMaterial();
            bom.jobID = job.id;
            bom.note = "Bulk Water Supply Fee";
            JobBOMItem ji = new JobBOMItem();
            HararEstimationConfiguration.ItemTypedValue setting=null;
            switch (data.deliveryType)
            {
                case DeliveryType.Regular:
                    setting = config.bwRegularValues;
                    break;
                case DeliveryType.OutOfTown:
                    setting=config.bwOutOfTownValues;
                    break;
                case DeliveryType.CommunitySupply:
                    setting = config.bwPublicFountain;
                    break;
                case DeliveryType.SelfDelivery:
                    setting = config.bwSelfDeliveryValues;
                    break;
            }
            if (setting == null)
                throw new ServerUserMessage("Please configure bulk water supply rates properly");
            string itemCode = setting.getItemCode(customer.subscriberType);
            if (string.IsNullOrEmpty(itemCode))
                throw new ServerUserMessage("Please configure bulk water supply rates properly");
            BIZNET.iERP.TransactionItems item = bde.bERP.GetTransactionItems(itemCode);
            if (item == null)
                throw new ServerUserMessage("Hydrant water supply item is not valid. It is probably deleted");
            ji.itemID = itemCode;
            ji.inSourced = true;
            ji.description = item.Name;
            ji.quantity = data.workAmount;
            ji.unitPrice = setting.getValue(customer.subscriberType);
            ji.calculated = true;
            
            bom.items = new JobBOMItem[] { ji };
            return bom;
        }
    }

    public abstract class RESHomeDeliveryServiceBase<DataType> : RESBaseHarar, IJobRuleServerHandler where DataType:HomeDeliveryServiceDataBase
    {
        public RESHomeDeliveryServiceBase(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            DataType newLine = data as DataType;
            addApplicationData(AID, date, job, worker, data);
            if (job.customerID != -1)
                bde.verifyAccountingClearance(AID, job.customerID, date, job);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DataType newLine = data as DataType;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DataType wfdata = data as DataType;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:                    
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }
        public abstract JobBillOfMaterial getBOM(Subscriber customer, JobData job, HararEstimationConfiguration config);
       
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (nextState)
            {
                case StandardJobStatus.TECHNICAL_WORK:
                    if (job.status != StandardJobStatus.WORK_PAYMENT)
                        throw new ServerUserMessage("Invalid state change");
                    checkPayment(job.id);
                    
                    if (job.completionDocumentID != -1)
                    {
                        bde.BdeAccounting.DeleteAccountDocument(AID, job.completionDocumentID, false);
                        job.completionDocumentID = -1;
                        bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                        bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    }
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                    if (job.status != StandardJobStatus.APPLICATION)
                        throw new ServerUserMessage("Invalid state change");
                    bde.registerNewCustomer(AID, job, date);
                    Subscriber customer = bde.getJobCustomer(job);
                    //validate configuration
                    HararEstimationConfiguration config = bde.getConfiguration<HararEstimationConfiguration>(StandardJobTypes.NEW_LINE);
                    if (config == null)
                        throw new ServerUserMessage("Please set estimation configuration");

                    //delete any existing BOM
                    foreach (JobBillOfMaterial bm in bde.GetJobBOM(job.id))
                        bde.DeleteBOM(AID, bm.id,true);
                    JobBillOfMaterial bom = getBOM(customer,job,config);
                    bom.id = bde.SetBillofMateial(AID, bom);
                    bde.generateInvoice(AID, bom.id, worker);
                    break;
            }
        }


        protected DataType checkAndGetData(JobData job)
        {
            DataType data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as DataType;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        protected override double getUnitPrice(HararEstimationConfiguration config, SubscriberType type, JobBOMItem bomItem, BIZNET.iERP.TransactionItems titem)
        {
            return bomItem.unitPrice;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);
            DataType data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    checkPayment(job.id);
                    data = checkAndGetData(job);
                    if (data == null)
                        throw new ServerUserMessage("No work data found");
                    
                    CompletedJobDocument completed = new CompletedJobDocument();
                    completed.job = job;
                    completed.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, completed);
                    job.completionDocumentID = completed.AccountDocumentID;
                    bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                    bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);

                    bde.setWorkFlowData(AID, data);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public override bool jobAppliesTo(DateTime date, JobData job, JobWorker worker)
        {
            List<int> list = new List<int>();
            foreach (JobWorkerRole role in worker.roles)
            {
                switch (role)
                {
                    case JobWorkerRole.Casheir:
                        list.Add(StandardJobStatus.APPLICATION);
                        list.Add(StandardJobStatus.WORK_PAYMENT);
                        break;
                    case JobWorkerRole.Technician:
                        list.Add(StandardJobStatus.TECHNICAL_WORK);
                        break;

                }
            }
            return list.Contains(job.status) && new List<int>(worker.jobTypes).Contains(job.applicationType);
        }
    }
}
