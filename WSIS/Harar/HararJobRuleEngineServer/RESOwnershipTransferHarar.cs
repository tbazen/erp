﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Harar.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER, "Ownership Transfer", priority = 1)]
    public class RESOwnershipTransferHarar : RESOwnershipTransfer, IJobRuleServerHandler
    {
        public RESOwnershipTransferHarar(JobManagerBDE bde)
            : base(bde)
        {
        }
        protected override JobBOMItem getOwnerShipTransferItem(JobData job, Subscriber customer)
        {
            INTAPS.WSIS.Job.Harar.HararEstimationConfiguration config = bde.getConfiguration(StandardJobTypes.NEW_LINE, typeof(HararEstimationConfiguration)) as HararEstimationConfiguration;
            if (config == null)
                throw new ServerUserMessage("Estimation configuration is not set");
            JobBOMItem item = new JobBOMItem();
            string code = config.ownershipTransferValues.getItemCode(customer.subscriberType);
            item.calculated = true;
            item.description = bde.bERP.GetTransactionItems(code).Name;
            item.inSourced = true;
            item.itemID = code;
            item.quantity = 1;
            item.referenceUnitPrice = -1;
            item.tag = null;
            item.unitPrice = config.ownershipTransferValues.getValue(customer.subscriberType);
            return item;
        }
    }
}