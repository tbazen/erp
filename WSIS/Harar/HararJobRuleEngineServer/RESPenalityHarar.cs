using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Harar;
using INTAPS;
namespace INTAPS.WSIS.Job.Harar.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_PENALITY, "Penality")]
    public class RESPenalityHarar : RESBaseHarar, IJobRuleServerHandler
    {
        public RESPenalityHarar(JobManagerBDE bde)
            : base(bde)
        {
        }

        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            ConnectionPenalityData penality = data as ConnectionPenalityData;
            if (penality != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ConnectionPenalityData newLine = data as ConnectionPenalityData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ConnectionPenalityData wfData = data as ConnectionPenalityData;
            if (wfData == null)
                throw new ServerUserMessage("Empty data not allowed");

            wfData.typeID = getTypeID();
            wfData.setID(job.id, base.getTypeID());
            wfData.instanceCount = calculateInstanceCount(wfData);
            foreach (PenalityInstance inst in wfData.pastPenalities)
            {
                inst.customerID = wfData.customer.id;
                inst.connectionID = wfData.connection == null ? -1 : wfData.connection.id;
            }
            bde.setWorkFlowData(AID, wfData);
        }

        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.FINALIZATION};
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED };
                default:
                    return new int[0];
            }
        }
        
        int getIncedenceCount(int customerID,int connectionID, PenalityType type)
        {
            INTAPS.RDBMS.SQLHelper reader = bde.GetReaderHelper();
            try
            {
                return (int)reader.ExecuteScalar("Select count(*) from {0}.dbo.PenalityInstance where type={1} and customerID={2} and connectionID={3}".format( bde.DBName, (int)type
                    ,customerID,connectionID
                    ));
            }
            finally
            {
                bde.ReleaseHelper(reader);
            }
        }
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            ConnectionPenalityData data;
            HararEstimationConfiguration config = bde.getConfiguration<HararEstimationConfiguration>(StandardJobTypes.NEW_LINE);
            if (config == null)
                throw new ServerUserMessage("Configure estimation parameters");
            data = checkAndGetData(job);
            switch (job.status)
            {
                case StandardJobStatus.WORK_PAYMENT:
                    if (nextState != StandardJobStatus.FINALIZATION)
                        foreach (JobBillOfMaterial b in bde.GetJobBOM(job.id))
                            bde.DeleteBOM(AID, b.id, true);
                    break;
            }

            switch (nextState)
            {
                case StandardJobStatus.APPLICATION_APPROVAL:
                    bde.registerNewCustomer(AID, job, date);
                    data.customer = bde.getJobCustomer(job);
                    bde.setWorkFlowData(AID, data);
                    break;
                case StandardJobStatus.FINALIZATION:
                    checkPayment(job.id);
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                    Subscriber customer = bde.getJobCustomer(job);
                    JobBillOfMaterial penalityBOM = new JobBillOfMaterial();
                    penalityBOM.jobID = job.id;
                    penalityBOM.note = "Penality Payment";
                    penalityBOM.analysisTime = DateTime.Now;
                    double unitPrice;
                    string itemCode;
                    if (data.penalityType == PenalityType.Others)
                    {
                        itemCode= config.otherPenalityItem.getItemCode(customer.subscriberType);
                        unitPrice = data.otherAmount;
                    }
                    else
                    {
                        HararEstimationConfiguration.PenalityItemSingleTypeValue penalityConfig
                            = config.getPenalityConfig(data.penalityType).getConfguration(customer.subscriberType);
                        itemCode = penalityConfig.itemCode;
                        unitPrice = penalityConfig.incedenseValues[data.instanceCount < 3 ? data.instanceCount - 1 : 2];
                    }

                    BIZNET.iERP.TransactionItems titem = bde.bERP.GetTransactionItems(itemCode);
                    
                    data.instanceCount = calculateInstanceCount(data);

                    penalityBOM.items = new JobBOMItem[]{
                        new JobBOMItem(){
                                description=titem.Name+"(Instance {0})".format(data.instanceCount),
                                itemID=titem.Code,
                                inSourced=true,
                                quantity=1,
                                referenceUnitPrice=0,
                                unitPrice=unitPrice
                        }
                    };
                    penalityBOM.id = bde.SetBillofMateial(AID, penalityBOM);
                    bde.generateInvoice(AID, penalityBOM.id, worker);
                    bde.setWorkFlowData(AID, data);
                    break;

            }
        }

        private int calculateInstanceCount(ConnectionPenalityData data)
        {
            int incedenceCount = getIncedenceCount(data.customer.id,data.connection==null?-1:data.connection.ObjectIDVal, data.penalityType);
            foreach (PenalityInstance inst in data.pastPenalities)
                if (inst.type == data.penalityType)
                    incedenceCount++;
            incedenceCount++;
            return incedenceCount;
        }

        ConnectionPenalityData checkAndGetData(JobData job)
        {
            ConnectionPenalityData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ConnectionPenalityData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            ConnectionPenalityData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            ReadingEntryClerk clerk = bde.subscriberBDE.BWFGetClerkByUserID(worker.userID);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    foreach(PenalityInstance inst in data.pastPenalities)
                    {
                        inst.customerID = data.customer.id;
                        inst.connectionID = data.connection == null ? -1 : data.connection.id;
                        bde.WriterHelper.InsertSingleTableRecord(bde.DBName, inst, new string[] { "__AID" }, new object[] { AID });
                    }
                    bde.WriterHelper.InsertSingleTableRecord(bde.DBName,
                        new PenalityInstance()
                        {
                            customerID=data.customer.id,
                            connectionID=data.connection==null?-1:data.connection.id,
                            date=date.Ticks,
                            description=data.description??(job.description??""),
                            type=data.penalityType
                        },new string[]{"__AID"},new object[]{AID}
                        );
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        protected override double getUnitPrice(HararEstimationConfiguration config, SubscriberType type, JobBOMItem bomItem, BIZNET.iERP.TransactionItems titem)
        {
            return bomItem.unitPrice;
        }
    }
}
