﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Harar
{
    [Serializable]
    public class ConntectionMaintenanceHararData : WorkFlowData
    {
        public bool changeMeter;
        public bool chargeForMeter;
        public bool estimation;
        public bool relocateMeter;
        public bool meterDiagnosis;
        public bool supportLetter;

        public int connectionID;
        public MeterData meterData;

        public ConntectionMaintenanceHararData()
        {
        }

        public bool hasApplicationFee { get { return estimation || relocateMeter || meterDiagnosis || supportLetter; } }
    }
}
