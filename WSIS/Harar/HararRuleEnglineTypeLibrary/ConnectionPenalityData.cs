using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
using INTAPS.RDBMS;
namespace INTAPS.WSIS.Job.Harar
{
    [Serializable]
    public class ConnectionPenalityData : WorkFlowData
    {
        public Subscriber customer;
        public Subscription connection;
        public PenalityInstance[] pastPenalities;
        public PenalityType penalityType;
        public int instanceCount;
        public double otherAmount = 0;
        public string description = "";
    }
    public enum PenalityType
    {
        MeterByPass=0,
        MeterRelcoation=1,
        MeterReversing=2,
        OverPipeConstruction=3,
        MeterDamage=4,
        IllegalConnection=5,
        IllegalWateruse=6,
        Others=7
    }
    [Serializable]
    [SingleTableObject]
    public class PenalityInstance
    {
        [IDField]
        public int customerID;
        [DataField]
        public int connectionID;
        [IDField]
        public long date;
        [IDField]
        public PenalityType type;
        [DataField]
        public string description;
        public static string penalityText(PenalityType type)
        {
            switch (type)
            {
                case PenalityType.MeterByPass:
                    return "Meter Bypass";
                case PenalityType.MeterRelcoation:
                    return "Meter Relocation";
                case PenalityType.MeterReversing:
                    return "Meter Reversing";
                case PenalityType.OverPipeConstruction:
                    return "Over Pipeline Construction";
                case PenalityType.MeterDamage:
                    return "Meter Damage";
                case PenalityType.IllegalConnection:
                    return "Illegal Connection";
                case PenalityType.IllegalWateruse:
                    return "Illegal Wateruse";
                case PenalityType.Others:
                    return "Others";
                default:
                    return "Unknown type";
            }
        }
    }
}
