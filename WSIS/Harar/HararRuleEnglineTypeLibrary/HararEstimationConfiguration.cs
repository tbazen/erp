﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.WSIS.Job.Harar
{
    [Serializable]
    public class HararEstimationConfiguration
    {
        [Serializable]
        public class ItemTypedValue
        {
            public string privateItemCode=null;
            public string institutionalItemCode=null;
            public string otherItemCode=null;
            public double privateValue=0;
            public double institutionalValue=0;
            public double otherValue;
            public string getItemCode(SubscriberManagment.SubscriberType customerType)
            {
                switch (customerType)
                {
                    case INTAPS.SubscriberManagment.SubscriberType.Private:
                        return this.privateItemCode;
                    case INTAPS.SubscriberManagment.SubscriberType.CommercialInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Community:
                    case INTAPS.SubscriberManagment.SubscriberType.GovernmentInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Industry:
                    case INTAPS.SubscriberManagment.SubscriberType.NGO:
                    case INTAPS.SubscriberManagment.SubscriberType.Other:
                    case INTAPS.SubscriberManagment.SubscriberType.RelegiousInstitution:
                        return this.institutionalItemCode;
                    case INTAPS.SubscriberManagment.SubscriberType.Unknown:
                        return this.institutionalItemCode;
                    default:
                        return this.institutionalItemCode;
                }
            }
            public double getValue(SubscriberManagment.SubscriberType customerType)
            {
                switch (customerType)
                {
                    case INTAPS.SubscriberManagment.SubscriberType.Private:
                        return this.privateValue;
                    case INTAPS.SubscriberManagment.SubscriberType.CommercialInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Community:
                    case INTAPS.SubscriberManagment.SubscriberType.GovernmentInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Industry:
                    case INTAPS.SubscriberManagment.SubscriberType.NGO:
                    case INTAPS.SubscriberManagment.SubscriberType.Other:
                    case INTAPS.SubscriberManagment.SubscriberType.RelegiousInstitution:
                        return this.institutionalValue;
                    case INTAPS.SubscriberManagment.SubscriberType.Unknown:
                        return this.institutionalValue;
                    default:
                        return this.institutionalValue;
                }
            }
        }
        [Serializable]
        public class PenalityItemSingleTypeValue
        {
            public string itemCode=null;
            public double [] incedenseValues=new double[3];
        }
        [Serializable]
        public class PenalityItemValue
        {
            public PenalityItemSingleTypeValue privateValue = new PenalityItemSingleTypeValue();
            public PenalityItemSingleTypeValue institutionalValue = new PenalityItemSingleTypeValue();
            public PenalityItemSingleTypeValue otherValue = new PenalityItemSingleTypeValue();
            public PenalityItemSingleTypeValue getConfguration(SubscriberManagment.SubscriberType customerType)
            {
                switch (customerType)
                {
                    case INTAPS.SubscriberManagment.SubscriberType.Private:
                        return this.privateValue;
                    case INTAPS.SubscriberManagment.SubscriberType.CommercialInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Community:
                    case INTAPS.SubscriberManagment.SubscriberType.GovernmentInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Industry:
                    case INTAPS.SubscriberManagment.SubscriberType.NGO:
                    case INTAPS.SubscriberManagment.SubscriberType.Other:
                    case INTAPS.SubscriberManagment.SubscriberType.RelegiousInstitution:
                        return this.institutionalValue;
                    case INTAPS.SubscriberManagment.SubscriberType.Unknown:
                        return this.institutionalValue;
                    default:
                        return this.institutionalValue;
                }
            }



            
        }
        public ItemsListItem[]  waterMeterItems                  = new ItemsListItem[0];//ITEM CODE REF
        public ItemsListItem[]  surveyItems                      = new ItemsListItem[0];//ITEM CODE REF


        public ItemTypedValue   estimationValues                = new ItemTypedValue(); 
        public ItemTypedValue   premissionValues                = new ItemTypedValue(); 
        public ItemTypedValue   applicationFormValues           = new ItemTypedValue(); 
        public ItemTypedValue   contractCardValues              = new ItemTypedValue(); 
        public ItemTypedValue[] meterPrice                      = new ItemTypedValue[0];
        public ItemTypedValue   waterMeterTransferValues        = new ItemTypedValue();
        public ItemTypedValue   letterOfApprovalValues          = new ItemTypedValue();
        public ItemTypedValue   ownershipTransferValues         = new ItemTypedValue();//*
        public ItemTypedValue   waterMeterDiagnosisValues       = new ItemTypedValue();
        public ItemTypedValue[] meterDeposit                    = new ItemTypedValue[0];
        public ItemTypedValue[] meterServiceValues              = new ItemTypedValue[0];
        
        public ItemTypedValue   materialProfitMargin            = new ItemTypedValue();
        public ItemTypedValue   technicalServiceMultiplier      = new ItemTypedValue();

        public ItemTypedValue   fileFolderCostValues            = new ItemTypedValue();
        public ItemTypedValue   stampDutyValues                 = new ItemTypedValue();

        public ItemTypedValue   bwRegularValues                  = new ItemTypedValue();//*
        public ItemTypedValue   bwOutOfTownValues               = new ItemTypedValue();//*
        public ItemTypedValue   bwSelfDeliveryValues            = new ItemTypedValue();//*
        public ItemTypedValue   bwPublicFountain                = new ItemTypedValue();//*

        public ItemTypedValue   lwdRegularValues                = new ItemTypedValue();//*
        public ItemTypedValue   lwdOutOfTownValues              = new ItemTypedValue();//*



        public ItemTypedValue otherPenalityItem = null;
        public PenalityItemValue penalityMeterByPass = new PenalityItemValue();
        public PenalityItemValue penalityMeterRelocation = new PenalityItemValue();
        public PenalityItemValue penalityMeterReversing = new PenalityItemValue();
        public PenalityItemValue penalityOverPipeConstruction = new PenalityItemValue();
        public PenalityItemValue penalityMeterDamege = new PenalityItemValue();
        public PenalityItemValue penalityIllegalConnection = new PenalityItemValue();
        public PenalityItemValue penalityIllegalWaterUse = new PenalityItemValue();

        public HararEstimationConfiguration.ItemTypedValue[] getMaintainanceNewLineItems()
        {
            return new HararEstimationConfiguration.ItemTypedValue[]
                {
                };

        }
        public HararEstimationConfiguration.ItemTypedValue[] getNewLineFixedItems()
        {
            return new HararEstimationConfiguration.ItemTypedValue[]
                {
                    premissionValues,
                    contractCardValues,
                    stampDutyValues,
                };

        }
        public bool isFixedItemCode(string code, SubscriberManagment.SubscriberType type, int incedence, out double value)
        {
            foreach (System.Reflection.FieldInfo fi in typeof(HararEstimationConfiguration).GetFields())
            {
                object val = fi.GetValue(this);

                if (val is ItemTypedValue)
                {
                    ItemTypedValue setting = (ItemTypedValue)val;
                    foreach (string thisCode in new string[] { setting.institutionalItemCode, setting.otherItemCode, setting.privateItemCode })
                        if (code.Equals(thisCode, StringComparison.CurrentCultureIgnoreCase))
                        {
                            value = setting.getValue(type);
                            return true;
                        }
                }

                else if (val is PenalityItemValue)
                {
                    PenalityItemValue setting = (PenalityItemValue)val;

                    foreach (string thisCode in new string[] { setting.institutionalValue.itemCode, setting.institutionalValue.itemCode, setting.privateValue.itemCode })
                        if (code.Equals(thisCode, StringComparison.CurrentCultureIgnoreCase))
                        {
                            value = setting.getConfguration(type).incedenseValues[incedence < 3 ? incedence - 1 : 2];
                            return true;
                        }

                }
            }
            value = 0;
            return false;
        }

        public PenalityItemValue getPenalityConfig(PenalityType penalityType)
        {
            switch (penalityType)
            {
                case PenalityType.MeterByPass:
                    return this.penalityMeterByPass;
                case PenalityType.MeterRelcoation:
                    return this.penalityMeterRelocation;
                case PenalityType.MeterReversing:
                    return this.penalityMeterReversing;
                case PenalityType.OverPipeConstruction:
                    return this.penalityOverPipeConstruction;
                case PenalityType.MeterDamage:
                    return this.penalityMeterDamege;
                case PenalityType.IllegalConnection:
                    return this.penalityIllegalConnection;
                case PenalityType.IllegalWateruse:
                    return this.penalityIllegalWaterUse;
                default:
                    return null;
            }
        }
    }
}