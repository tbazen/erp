using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
using INTAPS.RDBMS;
namespace INTAPS.WSIS.Job.Harar
{


    public enum DeliveryType
    {
        Regular,
        OutOfTown,
        SelfDelivery,
        CommunitySupply,

    }
    [Serializable]
    public class HomeDeliveryServiceDataBase : WorkFlowData
    {
        public DeliveryType deliveryType=DeliveryType.Regular;
        public Subscriber customer;
        public int kebele;
        public string houseNo;
        public string address;
        public string phoneNo;
        public double workAmount;
        public string contactPerson;
        public static string getDeliveryTypeString(DeliveryType type)
        {
            switch (type)
            {
                case DeliveryType.Regular:
                    return "Regular";
                case DeliveryType.OutOfTown:
                    return "Out of Town";
                case DeliveryType.SelfDelivery:
                    return "Self Delivery";
                case DeliveryType.CommunitySupply:
                    return "Community Water Point";
                default:
                    return type.ToString();
            }
        }
    }
    [Serializable]
    public class LiquidWasteDisposalData : HomeDeliveryServiceDataBase
    {
    }
    [Serializable]
    public class HydrantWaterSupplyData : HomeDeliveryServiceDataBase
    {
    }
}
