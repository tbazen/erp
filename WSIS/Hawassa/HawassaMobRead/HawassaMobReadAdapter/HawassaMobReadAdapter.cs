﻿using INTAPS.ClientServer;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment.BDE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace INTAPS.SubscriberManagment.Hawassa
{
    public class HawassaMobReadAdapter : IReadingAdapter
    {
        SubscriberManagmentBDE _bde;
        string m_connString = null;
        public HawassaMobReadAdapter(SubscriberManagmentBDE bde)
        {
            _bde = bde;
        }
        public void initialize()
        {
            foreach (var c in System.Configuration.ConfigurationManager.AppSettings)
            {
                if (c.Equals("WSSDB"))
                {
                    m_connString = System.Configuration.ConfigurationManager.AppSettings["WSSDB"];
                    break;
                }
            }
        }
        public void generateReadingPeriod()
        {
            if (m_connString != null)
            {
                int year = INTAPS.Ethiopic.EtGrDate.ToEth(DateTime.Now).Year;
                int month = INTAPS.Ethiopic.EtGrDate.ToEth(DateTime.Now).Month;
                if (month == 13) //Pagume
                    month = 12;
                lock (_bde.WriterHelper)
                {

                    SQLHelper wssHelper = new SQLHelper(m_connString);
                    wssHelper.getOpenConnection();
                    try
                    {

                        string sql = string.Format("Select cast(PerNo as nvarchar(20)) from {0}.dbo.GA_AccountPeriod where cast([Year] as int)=" + year + " and cast(MonthID as int)=" + month, wssHelper.getOpenConnection().Database);
                        object result = wssHelper.ExecuteScalar(sql);

                        if (!(result is string) || (result is int) || (result is long))
                            throw new ServerUserMessage("Accounting Period is not generated for Year=" + year + " and Month=" + month + " on Hawassa WSIS database");
                        int perNo = int.Parse(result.ToString());
                        Console.WriteLine($"Hawassa reading adapter: Period No:{perNo}");
                        //check if period exists in subscriber bill period map
                        string sqlCheck = string.Format("select count(*) from {0}.dbo.[BillPeriodMap] where WSSBillPeriodID=" + perNo, _bde.DBName);
                        if ((int)_bde.WriterHelper.ExecuteScalar(sqlCheck) == 0) // no period found
                        {
                            string monthAmh = getMonthAmhDescription(month);
                            if (monthAmh == null)
                                throw new ServerUserMessage("Period not found for month " + month + " in WSIS subscriber database");
                            string periodName = monthAmh + " " + year;
                            string sqlPeriod = string.Format("Select id from {0}.dbo.BillPeriod where name='" + periodName + "'", _bde.DBName);
                            object p = _bde.WriterHelper.ExecuteScalar(sqlPeriod);
                            if (p is DBNull)
                                throw new ServerUserMessage("Period not found in WSIS BillPeriod for " + monthAmh);
                            _bde.WriterHelper.Insert(_bde.DBName, "BillPeriodMap",
                                new string[] { "WSISBillPeriodID", "WSSBillPeriodID" },
                                new object[] { (int)p, perNo });
                        }

                    }
                    finally
                    {
                        _bde.ReleaseHelper(wssHelper);
                    }

                }
            }
        }
        private string getMonthAmhDescription(int month)
        {
            string monthName = null;
            switch (month)
            {
                case 1:
                    monthName = "Meskerem";
                    break;
                case 2:
                    monthName = "Tikimt";
                    break;
                case 3:
                    monthName = "Hidar";
                    break;
                case 4:
                    monthName = "Tahisas";
                    break;
                case 5:
                    monthName = "Tir";
                    break;
                case 6:
                    monthName = "Yekatit";
                    break;
                case 7:
                    monthName = "Megabit";
                    break;
                case 8:
                    monthName = "Miyazia";
                    break;
                case 9:
                    monthName = "Ginbot";
                    break;
                case 10:
                    monthName = "Sene";
                    break;
                case 11:
                    monthName = "Hamle";
                    break;
                case 12:
                    monthName = "Nehase";
                    break;

                default:
                    break;
            }
            return monthName;
        }
        public void UploadReading(int wsisPeriodID)
        {
            DateTime engDate = DateTime.Now;
            SQLHelper wssHelper = new SQLHelper(m_connString);
            wssHelper.getOpenConnection();
            lock (wssHelper)
            {

                try
                {
                    wssHelper.BeginTransaction();
                    SQLHelper dspReader = _bde.GetReaderHelper();
                    try
                    {
                        string sql = String.Format(@"SELECT [subscriptionID],[reading],readingTime,bwfStatus,readerID
                         FROM [Subscriber].[dbo].[BWFMeterReading] INNER JOIN Subscriber.dbo.BWFReadingBlock on BWFMeterReading.readingBlockID=BWFReadingBlock.id where BWFMeterReading.periodID={0}", wsisPeriodID);
                        DataTable readTable = dspReader.GetDataTable(sql);
                        int hwssPeriodID = GetWSSPeriodID(wsisPeriodID, dspReader);
                        int prevPeriod = (int)dspReader.ExecuteScalar(string.Format("Select WSSBillPeriodID from {0}.dbo.BillPeriodMap where WSISBillPeriodID=" + (wsisPeriodID - 1), _bde.DBName));
                        string wssDBName = wssHelper.getOpenConnection().Database;
                        _bde.Accounting.PushProgress("Uploading reading for " + _bde.GetBillPeriod(wsisPeriodID).name);
                        CachedObject<int, DataTable> tarrifTables = new CachedObject<int, DataTable>(
    cusType => wssHelper.GetDataTable(string.Format("SELECT * FROM {0}.[dbo].[CS_LUTariff] where CusGroupPk={1} order by TarrifPk", wssHelper.getOpenConnection().Database, cusType))
    );

                        int step = 0;
                        foreach (DataRow row in readTable.Rows)
                        {
                            int subscriptionID = (int)row[0];
                            string empID = _bde.bdePayroll.GetEmployee((int)row[4]).employeeID;

                            // Subscription[] ss = Program.DBSubscriberConnection.GetSTRArrayByFilter<Subscription>("id=" + subscriptionID);
                            Subscription sub = _bde.GetSubscription(subscriptionID, DateTime.Now.Ticks);
                            if (sub.subscriptionStatus == SubscriptionStatus.Active)
                            {
                                object previousRead = wssHelper.ExecuteScalar(String.Format("Select ActReading from {0}.dbo.CS_Reading where PerNo={1} and CusCode='{2}'", wssHelper.getOpenConnection().Database, prevPeriod, sub.contractNo));

                                double previousReading = 0;
                                if (previousRead != null)
                                    previousReading = double.Parse(previousRead.ToString());
                                double newReading = double.Parse(row[1].ToString());
                                BWFStatus bwfStatus = (BWFStatus)(int)row[3];
                                if (newReading < previousReading || bwfStatus == BWFStatus.Unread)
                                    newReading = previousReading;
                                double newConsumption = newReading - previousReading;
                                //1. Calculate amount based on tariff consumption break down and insert or update amount for bill_Consumption
                                //2. Insert consumption breakdown calculated above to bill_ConsumptionBreakdown
                                //3. update co_Customer table lastReading with the actualreading and update lastMonthIndex to the current month
                                object _custType = wssHelper.ExecuteScalar(string.Format("Select CusGroup from {0}.dbo.CS_Customer where CusCode='{1}'", wssHelper.getOpenConnection().Database, sub.contractNo));
                                int cusType;

                                if (_custType == null)
                                {
                                    cusType = -1;
                                    Console.WriteLine("Customer type null for customer {0}", sub.contractNo);
                                }
                                else if (_custType is DBNull)
                                {
                                    cusType = -1;
                                    Console.WriteLine("Customer type DBNull for customer {0}", sub.contractNo);
                                }
                                else
                                    cusType = int.Parse(_custType.ToString());
                                //DataTable tariffTbl = wssHelper.GetDataTable(traifsql = string.Format("SELECT * FROM {0}.[dbo].[CS_LUTariff] where CusGroupPk={1} order by TarrifPk", wssHelper.getOpenConnection().Database, cusType));
                                DataTable tariffTbl = tarrifTables[cusType];
                                if (tariffTbl.Rows.Count == 0)
                                {
                                    Console.WriteLine("Tarif not found for customer {0}.\n", sub.contractNo);
                                }
                                // double amount = calculateBillAmount(tariffTbl, newConsumption);

                                if (!readingExists(sub.contractNo, wssHelper, hwssPeriodID))
                                {
                                    //insert reading
                                    //string sqlMeter = "select MeterNo from WSS.dbo.co_Customers where CusIndex=" + int.Parse(sub.contractNo);
                                    string sqlMeterSize = string.Format("select MeterSize from {0}.dbo.CS_Customer where CusCode='" + sub.contractNo + "'", wssHelper.getOpenConnection().Database);
                                    string meterSize = (string)wssHelper.ExecuteScalar(sqlMeterSize);
                                    string meterRentAccNo = (string)wssHelper.ExecuteScalar(string.Format("select AccNoRent from {0}.dbo.CS_LUMeterSize where MeterSize='" + meterSize + "'", wssHelper.getOpenConnection().Database));
                                    string sqlMeterRent = string.Format("select Rent from {0}.dbo.CS_LUMeterSize where MeterSize='" + meterSize + "'", wssHelper.getOpenConnection().Database);
                                    string receivableAccNo = (string)wssHelper.ExecuteScalar(string.Format("select AccNo from {0}.dbo.[Sys_FormChartAccAssociation] where SubModule like '%ReadingMgt.aspx%'", wssHelper.getOpenConnection().Database));
                                    //double penalityRate = (double)wssHelper.ExecuteScalar("select Penality from WSS.dbo.bill_Penality");
                                    //int length = (int)wssHelper.ExecuteScalar("SELECT count(*) FROM WSS.dbo.bill_Consumption WHERE Printed = 0 AND CusIndex =" + int.Parse(sub.contractNo));
                                    //DataTable data = wssHelper.GetDataTable(sqlMeter);
                                    object _meterRent = wssHelper.ExecuteScalar(sqlMeterRent);
                                    double meterRent = _meterRent is double ? (double)_meterRent : 0;
                                    //string meterNo = (string)data.Rows[0][0];
                                    //double penality = length == 1 ? penalityRate : penalityRate / (double)(length - 1);
                                    DateTime d = (DateTime)row[2];
                                    DateTime d2 = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
                                    DateTime readDate = DateTime.Parse(d2.ToString("yyyy-MM-dd HH:mm:ss"));
                                    engDate = readDate;
                                    string ethReadDate = INTAPS.Ethiopic.EtGrDate.ToEth(readDate).ToString();
                                    wssHelper.Insert(wssDBName, "CS_Reading"
                                        , new string[] { "CusCode", "PerNo", "PrvReading", "ActReading", "Consumption", "ReadDateAh", "ReadDateEn", "ProcessStatus", "PostStatus", "SalesStatus", "CreatedBy" }
                                        , new object[] { sub.contractNo, hwssPeriodID, previousReading, newReading, newConsumption, ethReadDate, readDate, "Pending", "Pending", "Pending", empID });
                                    int readCode = (int)wssHelper.ExecuteScalar(string.Format("select ReadCode FROM {0}.[dbo].[CS_Reading] where PerNo={1} and CusCode='{2}'", wssHelper.getOpenConnection().Database, hwssPeriodID, sub.contractNo));
                                    insertConsumptionBreakdown(tariffTbl, newConsumption, readCode, meterRentAccNo, meterRent, receivableAccNo, wssHelper);
                                    //wssHelper.ExecuteScalar(string.Format("Update WSS.dbo.co_Customers set LastReading={0},LastMonthIndex={1} where CusIndex={2}", newReading, hwssPeriodID, int.Parse(sub.contractNo)));
                                }
                                else
                                    Console.WriteLine($"Reading exists for connection {sub.contractNo}. Skipped");
                            }
                            step++;
                            _bde.Accounting.SetProgress((double)step / (double)readTable.Rows.Count);
                        }
                        _bde.Accounting.PopProgress();

                        step = 0;
                        string sqlUnallocated = string.Format("Select contractNo from {0}.dbo.Subscription where SubscriptionStatus=" + (int)SubscriptionStatus.Active + " and id not in (Select subscriptionID from {0}.dbo.BWFMeterReading where periodID=" + wsisPeriodID + ")", _bde.DBName);
                        DataTable dtUnallocated = dspReader.GetDataTable(sqlUnallocated);
                        _bde.Accounting.PushProgress("Uploading Unallocated readings for " + _bde.GetBillPeriod(wsisPeriodID).name + " (" + dtUnallocated.Rows.Count + ")");
                        object wssEmployeeID = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='wssSystemEmployeeID'");
                        if (wssEmployeeID == null)
                            throw new ServerUserMessage("wssSystemEmployeeID not set in system parameters");
                        string sysEmployeeID = _bde.bdePayroll.GetEmployee(int.Parse((string)wssEmployeeID)).employeeID;
                        foreach (DataRow r in dtUnallocated.Rows)
                        {
                            string cNo = (string)r[0];
                            object pRead = wssHelper.ExecuteScalar(String.Format("Select ActReading from {0}.dbo.CS_Reading where PerNo={1} and CusCode='{2}'", wssDBName, prevPeriod, cNo));
                            double previousReading = 0;
                            if (pRead != null)
                                previousReading = double.Parse(pRead.ToString());
                            DateTime d2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                            DateTime readDate = DateTime.Parse(d2.ToString("yyyy-MM-dd HH:mm:ss"));
                            if ((int)wssHelper.ExecuteScalar(string.Format("select count(*) from {0}.dbo.CS_Customer where CusCode='{1}'", wssDBName, cNo)) > 0)
                            {
                                int cusType = int.Parse((string)wssHelper.ExecuteScalar(string.Format("Select CusGroup from {0}.dbo.CS_Customer where CusCode='{1}'", wssDBName, cNo)));
                                DataTable tariffTbl = wssHelper.GetDataTable(string.Format("SELECT * FROM {0}.[dbo].[CS_LUTariff] where CusGroupPk={1} order by TarrifPk", wssDBName, cusType));
                                string sqlMeterSize = string.Format("select MeterSize from {0}.dbo.CS_Customer where CusCode='" + cNo + "'", wssDBName);
                                string meterSize = (string)wssHelper.ExecuteScalar(sqlMeterSize);
                                string meterRentAccNo = (string)wssHelper.ExecuteScalar(string.Format("select AccNoRent from {0}.dbo.CS_LUMeterSize where MeterSize='" + meterSize + "'", wssDBName));
                                string sqlMeterRent = string.Format("select Rent from {0}.dbo.CS_LUMeterSize where MeterSize='" + meterSize + "'", wssDBName);
                                string receivableAccNo = (string)wssHelper.ExecuteScalar(string.Format("select AccNo from {0}.dbo.[Sys_FormChartAccAssociation] where SubModule like '%ReadingMgt.aspx%'", wssDBName));

                                if (!readingExists(cNo, wssHelper, hwssPeriodID))
                                {
                                    wssHelper.Insert(wssDBName, "CS_Reading"
            , new string[] { "CusCode", "PerNo", "PrvReading", "ActReading", "Consumption", "ReadDateAh", "ReadDateEn", "ProcessStatus", "PostStatus", "SalesStatus", "CreatedBy" }
            , new object[] { cNo, hwssPeriodID, previousReading, previousReading, 0, INTAPS.Ethiopic.EtGrDate.ToEth(DateTime.Now).ToString(), readDate, "Pending", "Pending", "Pending", sysEmployeeID });
                                    int readCode = (int)wssHelper.ExecuteScalar(string.Format("select ReadCode FROM {0}.[dbo].[CS_Reading] where PerNo={1} and CusCode='{2}'", wssDBName, hwssPeriodID, cNo));
                                    double meterRent = (double)wssHelper.ExecuteScalar(sqlMeterRent);
                                    insertConsumptionBreakdown(tariffTbl, 0, readCode, meterRentAccNo, meterRent, receivableAccNo, wssHelper);
                                }
                            }
                            step++;
                            _bde.Accounting.SetProgress((double)step / (double)dtUnallocated.Rows.Count);

                        }
                        _bde.Accounting.PopProgress();

                    }
                    finally
                    {
                        _bde.ReleaseHelper(dspReader);
                    }
                    wssHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    wssHelper.RollBackTransaction();
                    ApplicationServer.EventLoger.LogException("Error Date:" + engDate.ToString() + "\n" + ex.Message, ex);
                    throw ex;
                }
            }
        }
        public void transferCustomerData(int AID, int wsisPeriodID)
        {
            addReadersToCurrentPeriod(wsisPeriodID);
            transferNewCustomers(AID);
            syncPreviousReadings(AID, wsisPeriodID);
            importBlocksToCurrentPeriod(AID, wsisPeriodID);
        }
        private int GetWSSPeriodID(int periodID, SQLHelper helper)
        {
            string sql = "select WSSBillPeriodID from Subscriber.dbo.BillPeriodMap where WSISBillPeriodID=" + periodID;
            int wssPeriodID = (int)helper.ExecuteScalar(sql);
            return wssPeriodID;
        }
        private bool readingExists(string contractNo, SQLHelper helper, int hwssPeriodID)
        {
            string sql = String.Format("Select count(*) from {0}.dbo.[CS_Reading] where PerNo={1} AND CusCode='{2}'", helper.getOpenConnection().Database, hwssPeriodID, contractNo);
            int count = (int)helper.ExecuteScalar(sql);
            return count > 0;
        }
        private void insertConsumptionBreakdown(DataTable tariffTbl, double newConsumption, int readCode, string meterRentAccNo, double meterRent, string receivableAccNo, SQLHelper helper)
        {
            double sumDifference = 0;
            double totalAmount = 0;
            string tariffAccNo = "";
            foreach (DataRow row in tariffTbl.Rows)
            {
                if (row[2] == null || row[2] is DBNull)
                    throw new ServerUserMessage("Null tarif configuration entery. Please check Billing system tarrif configuration.");
                if (row[3] == null || row[3] is DBNull)
                    throw new ServerUserMessage("Null tarif configuration entery. Please check Billing system tarrif configuration.");
                double diff = Convert.ToDouble(row[2]);
                double tariff = Convert.ToDouble(row[3]);

                tariffAccNo = row[5] as string;
                if (newConsumption - sumDifference >= diff)
                {
                    helper.Insert(helper.getOpenConnection().Database, "CS_ReadingBreakdown"
                    , new string[] { "RedCode", "Cons", "Tarrif", "Amount" }
                    , new object[] { readCode, diff, tariff, diff * tariff });

                    sumDifference += diff;
                    double amt = diff * tariff;
                    totalAmount += amt;

                }
                else if (newConsumption - sumDifference < diff)
                {
                    double cons = newConsumption - sumDifference;
                    if (cons == 0)
                        break;
                    helper.Insert(helper.getOpenConnection().Database, "CS_ReadingBreakdown"
                   , new string[] { "RedCode", "Cons", "Tarrif", "Amount" }
                   , new object[] { readCode, cons, tariff, cons * tariff });
                    sumDifference += cons;
                    double amt = cons * tariff;
                    totalAmount += amt;

                }
                //else if (newConsumption - sumDifference == 0)
                //{
                //    break;
                //    // helper.Insert(helper.getOpenConnection().Database, "bill_ConsumptionBreakdown"
                //    //, new string[] { "RedCode", "Cons", "Tarrif", "Amount" }
                //    //, new object[] { readCode, 0, tariff, 0 });
                //}

            }
            if (totalAmount > 0)
            {
                helper.Insert(helper.getOpenConnection().Database, "CS_ReadingDetail", new string[] { "ReadCode", "GLAccNo", "Dr", "Cr", "Status" }
                    , new object[] { readCode, tariffAccNo, 0, totalAmount, "-" });
                helper.Insert(helper.getOpenConnection().Database, "CS_ReadingDetail", new string[] { "ReadCode", "GLAccNo", "Dr", "Cr", "Status" }
                    , new object[] { readCode, meterRentAccNo, 0, meterRent, "-" });
                helper.Insert(helper.getOpenConnection().Database, "CS_ReadingDetail", new string[] { "ReadCode", "GLAccNo", "Dr", "Cr", "Status" }
                    , new object[] { readCode, receivableAccNo, totalAmount + meterRent, 0, "-" });
            }
            if (newConsumption == 0) //insert rent amount on CS_ReadingDetail
            {
                helper.Insert(helper.getOpenConnection().Database, "CS_ReadingDetail", new string[] { "ReadCode", "GLAccNo", "Dr", "Cr", "Status" }
    , new object[] { readCode, meterRentAccNo, 0, meterRent, "-" });
                helper.Insert(helper.getOpenConnection().Database, "CS_ReadingDetail", new string[] { "ReadCode", "GLAccNo", "Dr", "Cr", "Status" }
                    , new object[] { readCode, receivableAccNo, totalAmount + meterRent, 0, "-" });
            }

        }
        private double calculateBillAmount(DataTable tariffTbl, double newConsumption)
        {
            double sumDifference = 0;
            double sumAmount = 0;
            foreach (DataRow row in tariffTbl.Rows)
            {
                int diff = (int)row[4];
                double amount = (double)row[3];
                if (newConsumption - sumDifference >= diff)
                {
                    sumAmount += amount * diff;
                    sumDifference += diff;
                }
                else
                {
                    sumAmount += amount * (newConsumption - sumDifference);
                    break;
                }

            }
            return sumAmount;
        }
        private void addReadersToCurrentPeriod(int periodID)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    //If readers are not added to current period add them
                    ReadingEntryClerk[] clerks = _bde.GetAllReadingEntryClerk();
                    string description = "Transferring readers to period " + _bde.GetBillPeriod(periodID).name;
                    this._bde.Accounting.PushProgress(description); //initialize progress
                    int step = 0;
                    foreach (ReadingEntryClerk clerk in clerks)
                    {
                        MeterReaderEmployee reader = _bde.GetMeterReaderEmployees(periodID, clerk.employeeID);
                        if (reader == null)
                        {
                            //Add reader to MeterReaderEmployee
                            _bde.CreateMeterReaderEmployee(new MeterReaderEmployee() { periodID = periodID, employeeID = clerk.employeeID });
                        }
                        step++;
                        this._bde.Accounting.SetProgress((double)step / (double)(clerks.Length)); //Step progress
                    }
                    this._bde.Accounting.PopProgress(); //announce progress termination
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;
                }
            }
        }
        private void transferNewCustomers(int AID)
        {
            SQLHelper helper = new SQLHelper(m_connString);
            DataTable wssCustomers;
            //1. get all customers from WSS
            try
            {
                string custSql = string.Format("SELECT * FROM {0}.[dbo].[CS_Customer]", helper.getOpenConnection().Database);
                wssCustomers = helper.GetDataTable(custSql);
            }
            finally
            {
                _bde.ReleaseHelper(helper);
            }
            _bde.WriterHelper.setReadDB(_bde.DBName);
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.BeginTransaction();

                    object sub = _bde.WriterHelper.ExecuteScalar("Select max(id) from Subscriber.dbo.Subscriber");
                    int maxSubscriberID = sub is DBNull ? 0 : (int)sub;
                    object conn = _bde.WriterHelper.ExecuteScalar("Select max(id) from Subscriber.dbo.Subscription");
                    int maxSubscriptionID = conn is DBNull ? 0 : (int)conn;
                    _bde.Accounting.PushProgress("Transferring/updating customers");

                    int step = 0;
                    foreach (DataRow row in wssCustomers.Rows)
                    {
                        string customerCode = row["CusCode"] as string;
                        Subscriber oldSubscriber = _bde.GetSubscriber(customerCode);
                        if (oldSubscriber == null) //new customer found
                        {
                            //transfer the new subscriber

                            Subscriber subscriber = new Subscriber();
                            subscriber.id = maxSubscriberID + 1;
                            subscriber.name = row["CusName"] as string;
                            subscriber.subscriberType = GetSubscriberType(int.Parse(row["CusGroup"] as string), _bde.WriterHelper);
                            subscriber.Kebele = GetKebele(row["Kebelle"] as string, _bde.WriterHelper);
                            subscriber.address = row["HNo"] as string;
                            subscriber.amharicName = "N/A";
                            subscriber.customerCode = row["CusCode"] as string;
                            subscriber.phoneNo = row["Tel"] is DBNull ? "" : row["Tel"] as string;
                            subscriber.email = "N/A";
                            subscriber.nameOfPlace = GetKebeleName(subscriber.Kebele, _bde.WriterHelper);
                            subscriber.status = CustomerStatus.Active;
                            subscriber.statusDate = row["RegDateEn"] is DBNull ? DateTime.Now.Date : (DateTime)row["RegDateEn"];
                            subscriber.accountID = -1;
                            subscriber.depositAccountID = -1;
                            subscriber.subTypeID = -1;
                            _bde.WriterHelper.InsertSingleTableRecord<Subscriber>(_bde.DBName, subscriber, new string[] { "__AID" }, new object[] { AID });

                            Subscription subscription = new Subscription();
                            subscription.id = maxSubscriptionID + 1;
                            subscription.ticksFrom = 635641776000000000;
                            subscription.ticksTo = -1;
                            subscription.timeBinding = DataTimeBinding.LowerBound;
                            subscription.subscriberID = subscriber.id;
                            subscription.contractNo = subscriber.customerCode;
                            subscription.subscriptionType = SubscriptionType.Tap;
                            subscription.subscriptionStatus = GetSubscriptionStatus(row["Status"] as string, _bde.WriterHelper);
                            subscription.Kebele = subscriber.Kebele;
                            subscription.address = subscriber.address;
                            subscription.initialMeterReading = 0;
                            subscription.prePaid = false;
                            subscription.parcelNo = "N/A";
                            subscription.connectionType = ConnectionType.Unknown;
                            subscription.supplyCondition = SupplyCondition.Unknown;
                            subscription.remark = "New Customer Transfer from HAWASA WSS System";
                            subscription.dataStatus = DataStatus.Unknown;
                            subscription.meterPosition = 0;
                            subscription.waterMeterX = 0;
                            subscription.waterMeterY = 0;
                            subscription.houseConnectionX = 0;
                            subscription.houseConnectionY = 0;
                            subscription.serialNo = row["MeterNo"] as string;
                            subscription.modelNo = subscription.serialNo;
                            subscription.opStatus = MaterialOperationalStatus.Working;
                            subscription.householdSize = 0;
                            _bde.WriterHelper.InsertSingleTableRecord<Subscription>(_bde.DBName, subscription, new string[] { "__AID" }, new object[] { AID });

                            maxSubscriberID++;
                            maxSubscriptionID++;
                        }
                        else
                        {
                            //update Subscriber
                            oldSubscriber.name = row["CusName"] as string;
                            oldSubscriber.subscriberType = GetSubscriberType(int.Parse(row["CusGroup"] as string), _bde.WriterHelper);
                            oldSubscriber.Kebele = GetKebele(row["Kebelle"] as string, _bde.WriterHelper);
                            oldSubscriber.address = row["HNo"] as string;
                            oldSubscriber.customerCode = row["CusCode"] as string;
                            oldSubscriber.phoneNo = row["Tel"] is DBNull ? "" : row["Tel"] as string;
                            oldSubscriber.nameOfPlace = GetKebeleName(oldSubscriber.Kebele, _bde.WriterHelper);
                            oldSubscriber.statusDate = row["RegDateEn"] is DBNull ? DateTime.Now.Date : (DateTime)row["RegDateEn"];
                            _bde.WriterHelper.logDeletedData(AID, string.Format("{0}.dbo.Subscriber", _bde.DBName), "id=" + oldSubscriber.id);
                            _bde.WriterHelper.UpdateSingleTableRecord<Subscriber>(_bde.DBName, oldSubscriber, new string[] { "__AID" }, new object[] { AID });
                            //Update subscription
                            Subscription oldSubscription = _bde.WriterHelper.GetSTRArrayByFilter<Subscription>("subscriberID=" + oldSubscriber.id)[0];
                            oldSubscription.subscriptionStatus = GetSubscriptionStatus(row["Status"] as string, _bde.WriterHelper);
                            oldSubscription.Kebele = oldSubscriber.Kebele;
                            oldSubscription.address = oldSubscriber.address;
                            oldSubscription.serialNo = row["MeterNo"] as string;
                            _bde.WriterHelper.logDeletedData(AID, string.Format("{0}.dbo.Subscription", _bde.DBName), "id=" + oldSubscription.id);
                            _bde.WriterHelper.UpdateSingleTableRecord<Subscription>(_bde.DBName, oldSubscription, new string[] { "__AID" }, new object[] { AID });
                        }
                        step++;
                        _bde.Accounting.SetProgress((double)step / (double)wssCustomers.Rows.Count);
                    }
                    _bde.Accounting.PopProgress();
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;

                }
                finally
                {
                    _bde.WriterHelper.restoreReadDB();
                }
            }
        }
        private void syncPreviousReadings(int AID, int periodID)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    object parameter = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='lastPrevousReadingPeriodID'");
                    object wssEmployeeID = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='wssSystemEmployeeID'");
                    IDataReader reader = null;
                    _bde.WriterHelper.setReadDB(_bde.DBName);
                    _bde.WriterHelper.BeginTransaction();
                    if (wssEmployeeID == null)
                    {
                        throw new Exception("WSS System employee id not found in Subscriber system parameters");
                    }
                    int wssEmpID = int.Parse(wssEmployeeID.ToString());
                    ReadingEntryClerk clerk = _bde.BWFGetClerk(wssEmpID);
                    if (parameter == null)
                        throw new Exception("Last previous reading period id not found in Subscriber system parameters!");
                    else
                    {
                        int lastPreviousReadingPeriodID = int.Parse(parameter.ToString());
                        SQLHelper wssHelper = new SQLHelper(m_connString);
                        try
                        {
                            SQLHelper wssClone1 = wssHelper.Clone();
                            SQLHelper wssClone2 = wssHelper.Clone();
                            try
                            {
                                for (int i = periodID - 1; i <= periodID - 1; i++)
                                {
                                    try
                                    {
                                        int wssPeriodID = GetWSSPeriodID(i, _bde.WriterHelper);
                                        //Create wss system block for readings that are recorded manually in the particular period(readings not read by mobile) and transfer them
                                        string systemBlockName = "WSS System Block";
                                        BWFReadingBlock[] blocks = _bde.GetBlock(i, systemBlockName);
                                        if (blocks.Length > 0)
                                        {
                                            //if block already exists delete the block and the readings transferred under that block
                                            deleteBlockWithReadingsNoLog(AID, clerk, blocks[0].id, _bde.WriterHelper);
                                        }
                                        BWFReadingBlock wssBlock = new BWFReadingBlock();
                                        wssBlock.readerID = wssEmpID;
                                        wssBlock.periodID = i;
                                        wssBlock.readingStart = DateTime.Now.Date;
                                        wssBlock.readingEnd = DateTime.Now.Date;
                                        wssBlock.blockName = systemBlockName;
                                        wssBlock.entryClerkID = wssEmpID;
                                        wssBlock.id = _bde.BWFCreateReadingBlock(AID, null, wssBlock);
                                        string sql = String.Format("Select * from {0}.dbo.CS_Reading where PerNo ={1}", wssClone1.getOpenConnection().Database, wssPeriodID);
                                        int count = (int)wssClone2.ExecuteScalar(String.Format("Select count(*) from {0}.dbo.CS_Reading where PerNo ={1}", wssClone2.getOpenConnection().Database, wssPeriodID));
                                        reader = wssClone1.ExecuteReader(sql);
                                        _bde.Accounting.PushProgress("Syncing readings from " + _bde.GetBillPeriod(i).name);
                                        int step = 0;
                                        while (reader.Read())
                                        {
                                            string contractNo = reader["CusCode"] as string;
                                            double newReading = double.Parse(reader["ActReading"].ToString());
                                            double newConsumption = double.Parse(reader["Consumption"].ToString());
                                            DateTime readDate;
                                            object _readDate = reader["ReadDateEn"];
                                            if (_readDate is DateTime)
                                                readDate = (DateTime)_readDate;
                                            else
                                                readDate = DateTime.Now;
                                            // update if reading exists for particular period in WSIS
                                            object _status = _bde.WriterHelper.ExecuteScalar(String.Format("Select subscriptionStatus from Subscriber.dbo.Subscription where contractNo='{0}'", contractNo));

                                            int status = _status is int ? (int)_status : (int)(SubscriptionStatus.Discontinued);
                                            SubscriptionStatus curStatus = (SubscriptionStatus)status;
                                            if (curStatus == SubscriptionStatus.Active)
                                            {
                                                int subscriptionID = (int)_bde.WriterHelper.ExecuteScalar("select id from Subscriber.dbo.Subscription where contractNo='" + contractNo + "'");
                                                UpdateSubscriptionReading(AID, subscriptionID, i, newReading, newConsumption, _bde.WriterHelper);
                                                //If there are readings not read by mobile transfer them to wss system block
                                                if ((int)_bde.WriterHelper.ExecuteScalar(string.Format("select count(*) from Subscriber.dbo.BWFMeterReading where subscriptionID={0} and periodID={1}", subscriptionID, i)) == 0)
                                                    importWSSSystemBlockReading(AID, wssBlock.id, i, subscriptionID, newReading, newConsumption, readDate, clerk, _bde.WriterHelper);
                                            }
                                            step++;
                                            _bde.Accounting.SetProgress((double)step / (double)count);
                                        }
                                        _bde.Accounting.PopProgress();
                                    }
                                    finally
                                    {
                                        if (!((reader == null) || reader.IsClosed))
                                        {
                                            reader.Close();
                                        }

                                    }
                                }
                            }
                            finally
                            {
                                //if (!((reader == null) || reader.IsClosed))
                                //{
                                //    reader.Close();
                                //}
                                wssClone1.CloseConnection();
                                wssClone2.CloseConnection();
                            }
                        }
                        finally
                        {
                            _bde.ReleaseHelper(wssHelper);
                        }
                    }
                    _bde.WriterHelper.CommitTransaction();

                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    _bde.WriterHelper.restoreReadDB();
                }
            }
        }
        private void deleteBlockWithReadingsNoLog(int AID, ReadingEntryClerk clerk, int blockID, SQLHelper helper)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.DeleteSingleTableRecrod<BWFReadingBlock>(_bde.DBName, new object[] { blockID });
                    string sql = "Delete from Subscriber.dbo.BWFMeterReading where readingBlockID=" + blockID;
                    _bde.WriterHelper.ExecuteScalar(sql);
                }
                catch (Exception exception)
                {
                    throw new Exception("Error on deleting reading block", exception);
                }
            }
        }
        private void UpdateSubscriptionReading(int AID, int subscriptionID, int periodID, double newReading, double newConsumption, SQLHelper helper)
        {
            //int subscriptionID = (int)helper.ExecuteScalar(String.Format("Select id from Subscriber.dbo.Subscription where contractNo='{0}'", contractNo));
            string sql = String.Format("Update Subscriber.dbo.BWFMeterReading set reading={0},consumption={1},__AID={2},bwfStatus=1 where subscriptionID={3} and periodID={4}", newReading, newConsumption, AID, subscriptionID, periodID);
            _bde.WriterHelper.logDeletedData(AID, string.Format("{0}.dbo.BWFMeterReading", _bde.DBName), "subscriptionID=" + subscriptionID + " AND periodID=" + periodID);
            helper.ExecuteScalar(sql);
        }
        private void importWSSSystemBlockReading(int AID, int block, int period, int subscriptionID, double newReading, double newConsumption, DateTime readDate, ReadingEntryClerk clerk, SQLHelper helper)
        {

            object order = helper.ExecuteScalar("select max(orderN) from Subscriber.dbo.BWFMeterReading where periodID=" + period);
            int lastOrderN = order is DBNull ? 0 : (int)order;
            Subscription sub = helper.GetSTRArrayByFilter<Subscription>("id=" + subscriptionID)[0];
            BWFMeterReading reading = new BWFMeterReading();
            reading.readingBlockID = block;
            reading.subscriptionID = subscriptionID;
            reading.bwfStatus = BWFStatus.Read;
            reading.readingType = MeterReadingType.Normal;
            reading.averageMonths = 0;
            reading.reading = newReading;
            reading.consumption = newConsumption;
            reading.entryDate = readDate;
            reading.orderN = lastOrderN + 1;
            reading.billDocumentID = -1;
            reading.periodID = period;
            reading.readingProblem = MeterReadingProblem.NoProblem;
            reading.extraInfo = ReadingExtraInfo.None;
            reading.readingRemark = "Reading imported WSS System Employee";
            reading.readingTime = reading.entryDate;
            reading.readingX = sub.waterMeterX;
            reading.readingY = sub.waterMeterY;
            reading.method = ReadingMethod.Unknown;
            _bde.BWFAddMeterReading(AID, clerk, reading, false);
        }
        private void importBlocksToCurrentPeriod(int AID, int periodID)
        {
            lock (_bde.WriterHelper)
            {
                try
                {
                    _bde.WriterHelper.BeginTransaction();
                    _bde.WriterHelper.setReadDB(_bde.DBName);
                    string sql = "SELECT [id] FROM [Subscriber].[dbo].[BWFReadingBlock] where periodID=" + _bde.GetPreviousBillPeriod(periodID).id;
                    DataTable tblBlocks = _bde.WriterHelper.GetDataTable(sql);
                    foreach (DataRow row in tblBlocks.Rows)
                    {
                        int blockID = (int)row[0];

                        if (skipIfSystemBlock(blockID, _bde.WriterHelper))
                            continue;
                        int num3;
                        BWFReadingBlock block = _bde.BWFGetReadingBlockInternal(_bde.WriterHelper, blockID);
                        BWFReadingBlock[] blocks = _bde.GetBlock(periodID, block.blockName);
                        if (blocks.Length > 0)
                        {
                            //if block already exists delete the block and the readings transferred under that block
                            deleteBlockWithReadingsNoLog(AID, null, blocks[0].id, _bde.WriterHelper);
                        }

                        BWFMeterReading[] readingArray = _bde.GetReadings(blockID, 0, -1, out num3);
                        block.id = -1;
                        block.periodID = periodID;
                        block.id = _bde.BWFCreateReadingBlock(AID, null, block);
                        _bde.Accounting.PushProgress(String.Format("Importing reading for {0} ({1} Readings)", block.blockName, readingArray.Length));
                        int step = 0;
                        foreach (BWFMeterReading reading in readingArray)
                        {
                            reading.readingBlockID = block.id;
                            reading.periodID = block.periodID;
                            reading.bwfStatus = BWFStatus.Unread;
                            reading.reading = 0.0;
                            reading.readingX = 0d;
                            reading.readingY = 0d;
                            reading.extraInfo = ReadingExtraInfo.None;
                            reading.readingRemark = "";
                            reading.readingType = MeterReadingType.Normal;
                            reading.readingProblem = MeterReadingProblem.NoProblem;
                            _bde.BWFAddMeterReading(AID, null, reading, false);
                            step++;
                            _bde.Accounting.SetProgress((double)step / (double)readingArray.Length);
                        }
                        _bde.Accounting.PopProgress();
                    }
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    _bde.WriterHelper.RollBackTransaction();
                    throw ex;
                }
                finally
                {
                    _bde.WriterHelper.restoreReadDB();
                }
            }
        }
        private bool skipIfSystemBlock(int blockID, SQLHelper dsp)
        {
            object wssEmployeeID = _bde.WriterHelper.ExecuteScalar("select ParValue from Subscriber.dbo.SystemParameter where ParName='wssSystemEmployeeID'");
            if (wssEmployeeID == null)
            {
                throw new Exception("WSS System employee id not found in Subscriber system parameters");
            }
            int wssEmpID = int.Parse(wssEmployeeID.ToString());
            string sql = string.Format("Select count(*) from {0}.dbo.[BWFReadingBlock] where id=" + blockID + " and readerID=" + wssEmpID, _bde.DBName);
            if ((int)dsp.ExecuteScalar(sql) > 0)
                return true;
            return false;
        }
        private static SubscriberType GetSubscriberType(int custType, SQLHelper sQLHelper)
        {
            string sql = "select WSISSubscriberType From Subscriber.dbo.CustTypeToSubscribeTypeMap where WSSACustType=" + custType;
            int subscriberType = (int)sQLHelper.ExecuteScalar(sql);
            return (SubscriberType)subscriberType;
        }
        private static SubscriptionStatus GetSubscriptionStatus(string status, SQLHelper sQLHelper)
        {
            string sql = String.Format("Select WSISSubscriptionStatus from Subscriber.dbo.CustStatusToSubscriptionStatusMap where WSSCustStatus='{0}'", status);
            int s = (int)sQLHelper.ExecuteScalar(sql);
            return (SubscriptionStatus)s;
        }
        private static string GetKebeleName(int kebele, SQLHelper sQLHelper)
        {
            string sql = "Select name from Subscriber.dbo.Kebele where id=" + kebele;
            object kname = sQLHelper.ExecuteScalar(sql);
            if (kname is string)
                return (string)kname;
            return "Unknown Kebele";
        }
        private static int GetKebele(string kebele, SQLHelper sQLHelper)
        {
            string sql = String.Format("Select id from Subscriber.dbo.Kebele where name='{0}'", kebele);
            object kid = sQLHelper.ExecuteScalar(sql);
            if (kid is int)
                return (int)kid;
            return -1;
        }
        public string[] getCustomFields()
        {
            return new string[] { "File Number" };
        }
        public string getOverdue(int periodID, DateTime date, int customerID, int connectionID)
        {
            SQLHelper reader = _bde.GetReaderHelper();
            string customerCode = null;
            int perNo;
            try
            {
                perNo = GetWSSPeriodID(periodID, reader);
                string cust = string.Format("Select contractNo from {0}.dbo.Subscription where id=" + connectionID + " and timeBinding=1", _bde.DBName);
                customerCode = (string)reader.ExecuteScalar(cust);
            }
            finally
            {
                _bde.ReleaseHelper(reader);
            }

            SQLHelper helper = new SQLHelper(m_connString);
            try
            {

                string sql = string.Format(@"SELECT Sum(Cr)
  FROM {0}.[dbo].[CS_ReadingCollectionView_tmp] where SalesStatus='Pending' and CusCode='" + customerCode + "' and PerNo<>" + perNo, helper.getOpenConnection().Database);

                object result = helper.ExecuteScalar(sql);
                double amount = result is DBNull ? 0 : (double)result;
                return Accounting.AccountBase.FormatAmount(amount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                helper.CloseConnection();
            }
        }
        public string[] getcustomFieldValues(int periodID, int customerID, int connectionID)
        {
            SQLHelper reader = _bde.GetReaderHelper();
            string customerCode = null;
            int perNo;
            try
            {
                perNo = GetWSSPeriodID(periodID, reader);
                string cust = string.Format("Select contractNo from {0}.dbo.Subscription where id=" + connectionID + " and timeBinding=1", _bde.DBName);
                customerCode = (string)reader.ExecuteScalar(cust);
            }
            finally
            {
                _bde.ReleaseHelper(reader);
            }

            SQLHelper helper = new SQLHelper(m_connString);
            try
            {
                string sql = string.Format(@"SELECT FileNo
  FROM {0}.[dbo].[CS_Customer] where CusCode='" + customerCode + "'", helper.getOpenConnection().Database);

                object fileNo = helper.ExecuteScalar(sql);
                string fileNumber = fileNo is DBNull ? "" : (string)fileNo;
                return new string[] { fileNumber == null ? "" : fileNumber };
            }
            finally
            {
                helper.CloseConnection();
            }
        }
        public void prepareReadingCycleData()
        {
            SQLHelper helper = new SQLHelper(m_connString);
            lock (helper)
            {
                try
                {
                    string database = helper.getOpenConnection().Database;
                    helper.BeginTransaction();

                    string sql1 = string.Format(@"IF OBJECT_ID('dbo.CS_ReadingCollectionView_tmp', 'U') IS NOT NULL
                  DROP TABLE {0}.dbo.CS_ReadingCollectionView_tmp;", database);

                    string sql2 = string.Format("Select * into CS_ReadingCollectionView_tmp from {0}.[dbo].[CS_ReadingCollectionView]", database);
                    string sql3 = string.Format(@"CREATE NONCLUSTERED INDEX [IX_CS_ReadingCollectionView_tmp] ON {0}.[dbo].[CS_ReadingCollectionView_tmp]
                (
	                [SalesStatus] ASC,
	                [CusCode] ASC,
	                [PerNo] ASC
                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", database);
                    helper.ExecuteScalar(sql1);
                    helper.ExecuteScalar(sql2);
                    helper.ExecuteScalar(sql3);
                    helper.CommitTransaction();
                }

                catch (Exception ex)
                {
                    helper.RollBackTransaction();
                    throw ex;
                }
            }
        }
    }
}
