﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;
using BIZNET.iERP;

namespace INTAPS.WSIS.Job
{
    public class CompletedJobHandler:IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if (_bdeSubsc == null)
                    _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        JobManagerBDE _bde;
        JobManagerBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;
                return _bde;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public CompletedJobHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    CompletedJobDocument doc = (CompletedJobDocument)_doc;
                    if (doc.AccountDocumentID != -1)
                        throw new ServerUserMessage("Completed job document can't be update");
                    
                    doc.jobNumber = bde.getJobAccountingTypedDocumentReference(doc.job,false); 
                    doc.boms = bde.GetJobBOM(doc.job.id);
                    doc.customer = bde.getJobCustomer(doc.job);

                    List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                    doc.customer.accountID = bdeSubsc.getOrCreateCustomerAccountID(AID, doc.customer);
                    int csaIDCustomer = _accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, doc.customer.accountID).id;
                    

                    foreach (JobBillOfMaterial bom in doc.boms)
                    {
                        CustomerBillRecord bill = bdeSubsc.getCustomerBillRecord(bom.invoiceNo);
                        if (bill == null)
                            throw new ServerUserMessage("All invoices must be settled to complete a job");
                        if (bill.paymentDocumentID != -1)
                        {
                            INTAPS.SubscriberManagment.CustomerPaymentReceipt receipt = bdeSubsc.getCustomerPaymentReceipt(bill.paymentDocumentID);
                            foreach (BillItem it in receipt.billItems)
                            {
                                if (it.accounting == BillItemAccounting.Advance)
                                {
                                    trans.Add(new TransactionOfBatch(csaIDCustomer, it.price, it.description));
                                    trans.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, it.incomeAccountID).id, -it.price, it.description));
                                }
                            }
                        }

                    }
                    doc.ShortDescription = "Job completion transaction Job No:" + doc.job.jobNo;
                    doc.AccountDocumentID = _accounting.RecordTransaction(AID, doc, trans.ToArray(), doc.jobNumber);
                    _accounting.WriterHelper.CommitTransaction();
                    return doc.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public string GetHTML(AccountDocument _doc)
        {
            return "";
        }
        public void delteJobCompletionDocument(int AID, int docID)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    _accounting.DeleteAccountDocument(AID, docID, false);
                    _accounting.WriterHelper.CommitTransaction();
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;  
                }
            }
        }
        public void DeleteDocument(int AID, int docID)
        {
            throw new ServerUserMessage("It is not allowed to delete a job completion document manually");
        }

        public void ReverseDocument(int AID, int docID)
        {
            throw new NotImplementedException();
        }
    }
}
