﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.WSIS.Job
{
    public class ActiveJobList : IFunction
    {
        Job.JobManagerBDE _bde;
        public ActiveJobList(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            int jobTypeID = (int)Pars[0].Value;
            JobData[] jobs = _bde.getActiveJobs(jobTypeID == -1 ? null : new int[] { jobTypeID });
            ListData ld = new ListData(jobs.Length);
            for (int i = 0; i < jobs.Length; i++)
            {
                Job.JobData data = jobs[i];
                ListData row = new ListData(9);//0:id,1:logDate,2:jobNo,3:Job type,4:applicant,5:Phone No.,6:status,7:time since last status
                row[0] = new EData(DataType.Int, data.id);
                row[1] = new EData(DataType.Text, (i+1).ToString());
                row[2] = new EData(DataType.Text, data.jobNo);
                row[3] = new EData(DataType.Text, data.logDate.ToString("MMM dd,yyyy hh:mm tt"));
                row[4] = new EData(DataType.Text, _bde.getJobType(data.applicationType).name);
                SubscriberManagment.Subscriber custmer = _bde.getJobCustomer(data);
                row[5] = new EData(DataType.Text, custmer==null?"": custmer.name);
                row[6] = new EData(DataType.Text, custmer == null ? "" : custmer.phoneNo);
                row[7] = new EData(DataType.Text, _bde.getJobStatus(data.status).name);
                row[8] = new EData(DataType.Text, JobProcessRules.GetDurationString(DateTime.Now.Subtract(data.statusDate)));
                ld[i] = new EData(DataType.ListData, row);
            }
            return new EData(DataType.ListData, ld);

        }

        public string Name
        {
            get { return "Active Job List"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "ActiveJobList"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class ProcessedJobSummary : IFunction
    {
        class ProccessdJobItem
        {
            public int count = 0;
            public double totalSeconds=0;
        }
        Job.JobManagerBDE _bde;
        public ProcessedJobSummary(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            INTAPS.RDBMS.SQLObjectReader<JobStatusHistory> jreader = null;
            INTAPS.RDBMS.SQLHelper reader = _bde.GetReaderHelper();
            try
            {
                jreader= reader.GetObjectReader<JobStatusHistory>(string.Format("changeDate>='{0}' and changeDate<'{1}'",Pars[0].Value,Pars[1].Value));
                int currentJob = -1;
                Dictionary<string, ProccessdJobItem> summary = new Dictionary<string, ProccessdJobItem>();
                Dictionary<string,double> thisProcessed=null;
                DateTime prevTime = DateTime.Now;
                string prevAgent=null;
                while (jreader.Read())
                {
                    
                    JobStatusHistory h = jreader.Current;
                    if (currentJob != h.jobID)
                    {
                        summerize(summary, thisProcessed);
                        thisProcessed = new Dictionary<string, double>();
                        currentJob = h.jobID;
                        prevTime = h.changeDate;
                        prevAgent = h.agent;
                        continue;
                    }
                    double delta = h.changeDate.Subtract(prevTime).TotalSeconds;
                    if (thisProcessed.ContainsKey(h.agent))
                        thisProcessed[h.agent] = thisProcessed[h.agent] + delta;
                    else
                        thisProcessed.Add(h.agent, delta);
                    prevTime = h.changeDate;
                    prevAgent = h.agent;
                }
                summerize(summary, thisProcessed);
                
                ListData ld = new ListData(summary.Count);
                int i = 0;
                foreach (KeyValuePair<string, ProccessdJobItem> kv in summary)
                {
                    ListData row = new ListData(4);//0:angent,1:Agent Name,2:Number of Jobs Processed, 3: Average Time
                    row[0] = new EData(DataType.Text, kv.Key);
                    INTAPS.Payroll.Employee e = _bde.subscriberBDE.bdePayroll.GetEmployeeByLoginName(kv.Key);
                    string employeeName = e == null ? kv.Key : e.employeeName;
                    row[1] = new EData(DataType.Text, employeeName);
                    row[2] = new EData(DataType.Int, kv.Value.count);
                    if (kv.Value.count == 0)
                        row[3] = new EData(DataType.Text, "");
                    else
                    {
                        double avg = kv.Value.totalSeconds / kv.Value.count;
                        TimeSpan ts = new TimeSpan((long)(avg * 10 * 1000000));
                        row[3] = new EData(DataType.Text, JobProcessRules.GetDurationString(ts));
                    }
                    ld[i] = new EData(DataType.ListData, row);
                    i++;
                }
                return new EData(DataType.ListData, ld);
            }
            finally
            {
                if (jreader != null && !jreader.IsClosed)
                    jreader.Close();
                _bde.ReleaseHelper(reader);
            }
        }

        private static void summerize(Dictionary<string, ProccessdJobItem> summary, Dictionary<string, double> thisProcessed)
        {
            if (thisProcessed != null)
            {
                foreach (KeyValuePair<string, double> kv in thisProcessed)
                {
                    ProccessdJobItem item;
                    if (summary.ContainsKey(kv.Key))
                        item = summary[kv.Key];
                    else
                    {
                        item = new ProccessdJobItem();
                        summary.Add(kv.Key, item);
                    }
                    item.count++;
                    item.totalSeconds += kv.Value;
                }
            }
        }

        public string Name
        {
            get { return "Job Summary by Worker"; }
        }

        public int ParCount
        {
            get { return 2; }
        }

        public string Symbol
        {
            get { return "JobPeriformance"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class GetJobData : IFunction
    {
        Job.JobManagerBDE _bde;
        public GetJobData(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {

            int jobID = Convert.ToInt32(Pars[0].Value);
            JobData job = _bde.GetJob(jobID);
            if (job == null)
                return new FSError("Job not found id:" + jobID+" - GetJobData").ToEData();
            ListData fields = (ListData)Pars[1].Value;
            ListData ret=new ListData(fields.elements.Length);
            int i = -1;
            foreach (EData dat in fields.elements)
            {
                i++;
                string[] fieldParts = dat.Value.ToString().Split('.');
                object obj = job;
                bool skip = false;
                foreach (string f in fieldParts)
                {
                    if (obj == null)
                        break;
                    System.Reflection.FieldInfo fi = obj.GetType().GetField(f.ToString());
                    if (fi == null)
                    {
                        ret[i] = new FSError("Infalid field name:" + f + " - GetJobData").ToEData();
                        skip = true;
                        break;
                    }
                    obj = fi.GetValue(obj);
                }
                if (skip)
                    continue;
                if (obj == null)
                    ret[i] = new EData(DataType.Empty, null);
                else if (obj is bool)
                {
                    ret[i] = new EData(DataType.Bool, (bool)obj);
                }
                else if (obj is int)
                {
                    ret[i] = new EData(DataType.Int, (int)obj);
                }
                else if (obj is short)
                {
                    ret[i] = new EData(DataType.Int, (int)(short)obj);
                }
                else if (obj is double)
                {
                    ret[i] = new EData(DataType.Float, (double)obj);
                }
                else if (obj is float)
                {
                    ret[i] = new EData(DataType.Float, (double)(float)obj);
                }
                else if (obj is string)
                {
                    ret[i] = new EData(DataType.Text, (string)obj);
                }
                else
                    ret[i] = new EData(DataType.Text, obj.ToString());
            }
            return new EData(DataType.ListData, ret);
        }

        public string Name
        {
            get { return "Get Job Fields"; }
        }

        public int ParCount
        {
            get { return 2; }
        }

        public string Symbol
        {
            get { return "GetJobData"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }

    public class GetApplicationType : IFunction
    {
        Job.JobManagerBDE _bde;
        public GetApplicationType(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            int typeID;
            typeID = Convert.ToInt32(Pars[0].Value);
            JobTypeInfo jt = _bde.getJobType(typeID);
            return new EData(DataType.Text, jt == null ? "" : jt.name);
        }

        public string Name
        {
            get { return "get Job type"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "GetJobType"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }

    public class GetStatusType : IFunction
    {
        Job.JobManagerBDE _bde;
        public GetStatusType(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {

            int typeID;
            typeID = Convert.ToInt32(Pars[0].Value);
            JobStatusType jt = _bde.getJobStatus(typeID);
            return new EData(DataType.Text, jt == null ? "" : jt.name);

        }

        public string Name
        {
            get { return "get status type"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "GetStatusType"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }

    public class GetDurationText: IVarParamCountFunction
    {
        int _npar;
        Job.JobManagerBDE _bde;
        public GetDurationText(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            if (Pars.Length == 2)
            {
                return new EData(DataType.Text,
                    JobProcessRules.GetDurationString(((DateTime)Pars[1].Value).Subtract((DateTime)Pars[0].Value))
                    );
            }

            return new EData(DataType.Text,
    JobProcessRules.GetDurationString(new TimeSpan( (long)(((double)Pars[0].Value)*1e7)))
    );

        }

        public string Name
        {
            get { return "Get Duration Text"; }
        }

        public int ParCount
        {
            get { return _npar; }
        }

        public string Symbol
        {
            get { return "GetDurationText"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }

        public IVarParamCountFunction Clone()
        {
            GetDurationText ret = new GetDurationText(_bde);
            ret._npar = _npar;
            return ret;
        }

        public bool SetParCount(int n)
        {
            _npar = n;
            return _npar == 1 || _npar == 2;
        }
    }

    public class DateDiffSeconds : IFunction
    {
        
        Job.JobManagerBDE _bde;
        public DateDiffSeconds(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            return new EData(DataType.Float,
                ((DateTime)Pars[1].Value).Subtract((DateTime)Pars[0].Value).TotalSeconds
                );
        }

        public string Name
        {
            get { return "Date Difference in Seconds"; }
        }

        public int ParCount
        {
            get { return 2; }
        }

        public string Symbol
        {
            get { return "DateDiffSeconds"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class GetJobCustomer : IFunction
    {
        Job.JobManagerBDE _bde;
        public GetJobCustomer(Job.JobManagerBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            int jobid = (int)Pars[0].Value;
            JobData jobData = _bde.GetJob(jobid);
            SubscriberManagment.Subscriber subscriber = _bde.getJobCustomer(jobData);
            return new EData(DataType.Text, subscriber == null ? "" : subscriber.name);
        }

        public string Name
        {
            get { return "Get Job Customer"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "GetJobCustomer"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
    public class GetBatchDisconnectData : IFunction
    {
        Job.JobManagerBDE _bde;
        INTAPS.SubscriberManagment.BDE.SubscriberManagmentBDE subscriber = (INTAPS.SubscriberManagment.BDE.SubscriberManagmentBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("Subscriber");
        public GetBatchDisconnectData(Job.JobManagerBDE bde)
        {
            _bde = bde;
            
        }
        public EData Evaluate(EData[] Pars)
        {
            int jobID = (int)Pars[0].Value;
            BatchDisconnectData data = (BatchDisconnectData)_bde.getWorkFlowData(jobID, 12, 0, true);
           string contractNos= _GetConnectionsSeparatedByComma(data.connections);
            return new EData(DataType.Text, contractNos);
        }
        private string _GetConnectionsSeparatedByComma(ClientServer.VersionedID[] versionedID)
        {
            string contracts = "";
            int i = 0;
            foreach (ClientServer.VersionedID vid in versionedID)
            {
              string contractno=  subscriber.GetSubscription(vid.id, vid.version).contractNo;
              if (i == 0)
                  contracts += contractno;
              else
                  contracts += ", " + contractno;
              i++;
            }
            return contracts;
        }

        public string Name
        {
            get { return "Get Batch Disconnect Data"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "GetDisconnectedConnection"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }


}
