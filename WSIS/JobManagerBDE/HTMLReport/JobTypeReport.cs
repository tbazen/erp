using INTAPS.Accounting.BDE;
using INTAPS.Evaluator;
using System;
namespace INTAPS.WSIS.Job
{
    public class JobTypeReport : ReportProcessorBase
    {
        public override object[] GetDefaultPars()
        {
            return new object[] { -1 };
        }

        public override void PrepareEvaluator(EHTML data, params object[] paramter)
        {
            int jobTypeID = Convert.ToInt32(paramter[0]);
            data.AddVariable("typeID", new EData(DataType.Int, jobTypeID));
            base.PrepareEvaluator(data, paramter);
        }
    }
}

