﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job
{
    public interface IJobRuleServerHandler
    {
        void createApplication(int AID, DateTime date, JobData job,JobWorker worker, WorkFlowData data, string note);
        void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data);
        void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data);
        int[] getPossibleNextState(DateTime date,JobWorker worker,JobData job);
        void changeState(int AID, DateTime date,JobWorker worker, JobData job, int nextState,string note);
        void finishJob(int AID, DateTime date,JobWorker worker, JobData job,string note);
        bool jobAppliesTo(DateTime date, JobData job, JobWorker worker);
        void deleteApplication(int AID, DateTime date, JobData job, JobWorker worker);
        JobStatusType[] getAllStatusTypes();

        string getCommandString(int status);
        bool selfPayment { get; }
        int doPayment(int AID, DateTime date, JobData job, int bomID, JobWorker worker);
        JobBillDocument generateInvoice(int AID,Subscriber customer, JobData job, JobBillOfMaterial billOfMaterialt);
        string generateInvoiceHTML(JobBillDocument bill);

        Type[] getDataExtraTypes();
        void onBOMSet(int AID, JobData job, JobBillOfMaterial bom);
    }
    public class JobRuleServerHandlerAttribute : Attribute
    {
        public int priority = 0;
        public int typeID;
        public string name;
        public JobTypeInfo getJobTypeInfo()
        {
            JobTypeInfo ret = new JobTypeInfo();
            ret.id = typeID;
            ret.name = name;
            return ret;
        }
        public JobRuleServerHandlerAttribute(int typeID
            ,string name
            )
        {
            this.typeID = typeID;
            this.name = name;
        }
    }
}
