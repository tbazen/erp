
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using INTAPS.Accounting.BDE;
namespace INTAPS.WSIS.Job
{

    public partial class JobManagerBDE : BDEBase
    {
       
        public int RegisterSubscription(int AID, int jobID, Subscription data)
        {
            lock (base.dspWriter)
            {
                JobData job = this.GetJob(jobID);
                if (job == null)
                {
                    throw new Exception("Job for setting appoinment is not in the database");
                }
                if (job.status != StandardJobStatus.CONTRACT)
                {
                    throw new Exception("The job should be on contract stage");
                }
                JobBillOfMaterial[] jobBOM = this.GetJobBOM(jobID);
                if (jobBOM.Length == 0)
                {
                    throw new Exception("No payment made");
                }
                foreach (JobBillOfMaterial material in jobBOM)
                {
                    if(!bdeSubsriber.isBillPaid(material.invoiceNo))
                    {
                        throw new Exception("All bills must be settled before registering subscription");
                    }
                }
                base.dspWriter.BeginTransaction();
                try
                {
                    data.subscriberID = this.bdeSubsriber.RegisterSubscriber(AID, data.subscriber);
                    data.contractNo = this.GetNextContractNumber(AID,data);
                    data.subscriptionStatus = SubscriptionStatus.Active;
                    int subscriptionID = this.bdeSubsriber.AddSubscription(AID, data);
                    //job.subscriptionID = this.bdeSubsriber.AddSubscription(AID, data);
                    //job.newContract = data;
                    //base.dspWriter.UpdateSingleTableRecord<JobData>(base.DBName, job);
                    base.dspWriter.CommitTransaction();
                    return subscriptionID;
                }
                catch 
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void RemoveJob(int AID, int jobHandle)
        {
            if (this.GetJob(jobHandle) == null)
            {
                throw new Exception("Invalid job ID:" + jobHandle);
            }
            JobStatusHistory[] jobHistory = this.GetJobHistory(jobHandle);
            if (jobHistory.Length > 1)
            {
                throw new Exception("Only jobs that are not processed can be deleted, consider canceling the job instead.");
            }
            JobAppointment[] jobAppointments = this.GetJobAppointments(jobHandle);
            if (jobAppointments.Length > 1)
            {
                throw new Exception("Only jobs that are not processed can be deleted, consider canceling the job instead.");
            }
            if (jobAppointments.Length == 1)
            {
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobAppointment", "id=" + jobAppointments[0].id);
                base.dspWriter.DeleteSingleTableRecrod<JobAppointment>(base.DBName, new object[] { jobAppointments[0].id });
            }

            lock (base.dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobStatusHistory", "id=" + jobHistory[0].id);
                    base.dspWriter.DeleteSingleTableRecrod<JobStatusHistory>(base.DBName, new object[] { jobHistory[0].id });
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobData", "id=" + jobHandle);
                    base.dspWriter.DeleteSingleTableRecrod<JobData>(base.DBName, new object[] { jobHandle });
                    changeNo++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void AddAppointment(int AID, JobAppointment appointment)
        {
            lock (base.dspWriter)
            {
                JobData job = this.GetJob(appointment.jobID);
                if (job == null)
                {
                    throw new Exception("Job for setting appoinment is not in the database");
                }
                JobAppointment latestAppointment = this.GetLatestAppointment(appointment.jobID);
                if ((latestAppointment != null) && latestAppointment.active)
                {
                    throw new Exception("There is already an active appointment for this job");
                }
                if (appointment.appointmentDate < job.startDate)
                {
                    throw new Exception("Appointment date set before job starting date");
                }
                base.dspWriter.BeginTransaction();
                try
                {
                    if (appointment.id < 1)
                    {
                        appointment.id = AutoIncrement.GetKey("JobManager.AppointmentID");
                    }
                    appointment.active = true;
                    appointment.actionDate = DateTime.Now;
                    base.dspWriter.InsertSingleTableRecord<JobAppointment>(base.DBName, appointment, new string[] { "__AID" }, new object[] { AID });
                    changeNo++;
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public int AddJob(int AID, JobData job, JobWorker agent,WorkFlowData data)
        {
            lock (base.dspWriter)
            {
                /*if (job.customerID == -1)
                {
                    if (job.newCustomer == null)
                        throw new ServerUserMessage("Customer not set");
                }
                else
                {
                    if (bdeSubsriber.GetSubscriber(job.customerID)== null)
                        throw new ServerUserMessage("Invalid customer ID:" + job.customerID);
                }*/
                if (job.customerID != -1)
                {
                    JobData[] jobs=getCustomerJobs(job.customerID,true);
                    if (job.applicationType != StandardJobTypes.EXEMPT_BILL_ITEMS)
                    {
                        if (jobs.Length > 0)
                            throw new ServerUserMessage("Please finish or cancel job " + jobs[0].jobNo + " first.");
                    }
                }
                IJobRuleServerHandler re = getJobRuleHandler(job.applicationType, true);
                base.dspWriter.BeginTransaction();
                try
                {
                    if (job.id == -1)
                    {
                        job.id = AutoIncrement.GetKey("JobManager.JobID");
                    }
                    if (string.IsNullOrEmpty(job.jobNo))
                    {
                        job.jobNo = this.GetNextJobNumber(AID);
                    }
                    job.status = StandardJobStatus.APPLICATION;
                    job.logDate = DateTime.Now;
                    job.statusDate = job.startDate;


                    base.dspWriter.InsertSingleTableRecord<JobData>(base.DBName, job, new string[] { "__AID" }, new object[] { AID });
                    JobStatusHistory hist = new JobStatusHistory();
                    hist.jobID = job.id;
                    hist.newStatus = job.status;
                    hist.oldStatus =StandardJobStatus.NONE;
                    hist.changeDate = job.statusDate;
                    hist.agent = agent.userID;
                    hist.note = "Job creation";
                    this.AddJobHistory(AID, hist);
                    this.CheckHistoryRecord(job.id, "Error trying to save job data");
                    changeNo++;
                    re.createApplication(AID, job.statusDate, job, agent, data, "");
                    base.dspWriter.CommitTransaction();
                    return job.id;
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        private void AddJobHistory(int AID, JobStatusHistory hist)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.BeginTransaction();
                try
                {
                    hist.id = AutoIncrement.GetKey("JobManager.HistoryID");
                    hist.actionDate = DateTime.Now;
                    base.dspWriter.InsertSingleTableRecord<JobStatusHistory>(base.DBName, hist, new string[] { "__AID" }, new object[] { AID });
                    changeNo++;
                    base.dspWriter.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base.dspWriter.RollBackTransaction();
                    throw new Exception("Error adding job history", exception);
                }
            }
        }
        // BDE exposed
        public int getPreviousHistory(int jobID)
        {
            JobStatusHistory[] ret = GetJobHistory(jobID);
            return ret[ret.Length - 1].oldStatus;
        }

        public void ChangeJobStatus(int AID, int jobID, DateTime date, int newStatus, string note, JobWorker agent)
        {
            JobData job = this.GetJob(jobID);
            if (job == null)
            {
                throw new Exception("Job not found in the database");
            }

            IJobRuleServerHandler re = getJobRuleHandler(job.applicationType, true);
            lock (base.dspWriter)
            {
                JobBillOfMaterial[] jobBOM;
                if (date < job.statusDate)
                {
                    throw new Exception("The date is not valid because it is before the date of the last record for this job");
                }
                JobStatusHistory[] jobhist = GetJobHistory(jobID);
                bool goBack = false;
                for (int i = 0; i < jobhist.Length - 1; i++)
                {
                    if (newStatus == jobhist[i].newStatus)
                    {
                        goBack = true;
                        break;
                    }
                }
                jobBOM = this.GetJobBOM(jobID);
                if (!goBack)
                {
                    if (JobProcessRules.searchArray<int>(re.getPossibleNextState(date, agent, job), newStatus) == -1)
                    {
                        throw new Exception(string.Format("The requested {0} status is not possible for this job", this.getJobStatus(newStatus).name));
                    }
                }
                if (!re.jobAppliesTo(date, job, agent))
                    throw new ServerUserMessage("You are not allowed to work on this job");

                
                base.dspWriter.BeginTransaction();
                try
                {

                    if (newStatus == StandardJobStatus.FINISHED)
                    {
                        re.finishJob(AID, date, agent, job, note);
                    }
                    else
                        re.changeState(AID, date, agent, job, newStatus, note);
                    JobStatusHistory hist = new JobStatusHistory();
                    hist.agent = agent.userID;
                    hist.jobID = job.id;
                    hist.newStatus = newStatus;
                    hist.note = note;
                    hist.oldStatus = job.status;
                    hist.changeDate = date;
                    this.AddJobHistory(AID, hist);
                    job.statusDate = date;
                    job.status = newStatus;
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobData", "id=" + job.id);
                    base.dspWriter.UpdateSingleTableRecord<JobData>(base.DBName, job, new string[] { "__AID" }, new object[] { AID });
                    this.CheckHistoryRecord(jobID, "Error changing status");
                    changeNo++;
                    base.dspWriter.CommitTransaction();
                }
                catch 
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void verifyAccountingClearance(int AID, int customerID, DateTime date, JobData job)
        {
            if(customerID<1)
                return;           

            int accountID = subscriberBDE.GetSubscriber(customerID).accountID;
            if (accountID < 1)
                return;
            double balance;
            balance = bdeAccounting.GetNetBalanceAsOf(
                bdeAccounting.GetOrCreateCostCenterAccount(AID, bdeBERP.SysPars.mainCostCenterID, accountID).id, 0, date);
            if (balance>0.2)
            {
                string errorMsg = null;
                string msg = string.Format("This customer has an unsettled balance of {0}", balance.ToString("#,#0.0000"));
                errorMsg = errorMsg == null ? msg : errorMsg + "\n" + msg;
                throw new ServerUserMessage(errorMsg);
            }
        }

        private void finshOtherConnectionServices(int AID, DateTime date, JobData job)
        {
            verifyAccountingClearance(AID, job.customerID, date, job);
            //lock (dspWriter)
            //{
            //    try
            //    {
            //        dspWriter.BeginTransaction();

            //        Subscriber oldCustomer = bdeSubsriber.GetSubscriber(job.customerID);
            //        if (oldCustomer == null)
            //            throw new ServerUserMessage("Customer not selected");
            //        Subscription connection = bdeSubsriber.GetSubscription(job.subscriptionID);
            //        if (connection == null)
            //            throw new ServerUserMessage("The connection not set");
            //        bool disconnect = JobProcessRules.searchServieType(new ServiceType[] { ServiceType.Meter_Return }
            //            , job.serviceTypes);
            //        bool reconnect= JobProcessRules.searchServieType(new ServiceType[] { ServiceType.Contract_Renewal}
            //            , job.serviceTypes);
            //        if (disconnect && reconnect)
            //            throw new ServerUserMessage("Meter return and connection renewal can't be periformed in one job");
            //        if (disconnect)
            //        {
            //            if(connection.subscriptionStatus==SubscriptionStatus.Active)
            //                bdeSubsriber.ChangeSubcriptionStatus(AID, connection.id, date, SubscriptionStatus.Diconnected);
            //        }
            //        else if (reconnect)
            //        {
            //            if (connection.subscriptionStatus != SubscriptionStatus.Active)
            //                bdeSubsriber.ChangeSubcriptionStatus(AID, connection.id, date, SubscriptionStatus.Active);
            //        }

                    
            //        dspWriter.CommitTransaction();
            //    }
            //    catch
            //    {
            //        dspWriter.RollBackTransaction();
            //        throw;
            //    }
            //}
        }
        private void finshConnectionModficiation(int AID, DateTime date, JobData job)
        {
            //verifyAccountingClearance(AID,job.customerID,date);
            //lock (dspWriter)
            //{
            //    try
            //    {
            //        dspWriter.BeginTransaction();

            //        Subscriber oldCustomer = bdeSubsriber.GetSubscriber(job.customerID);
            //        if (oldCustomer == null)
            //            throw new ServerUserMessage("Customer not selected");
            //        Subscription connection = bdeSubsriber.GetSubscription(job.subscriptionID);
            //        if (connection == null)
            //            throw new ServerUserMessage("The connection that will be modified is not set");
            //        if (job.newContract == null)
            //            throw new ServerUserMessage("Updated connection information not set");

            //        job.newContract.contractNo = connection.contractNo;
            //        job.newContract.id = connection.id;
            //        job.newContract.meter = connection.meter;
            //        job.newContract.subscriberID = connection.subscriberID;
            //        bdeSubsriber.UpdateSubscription(AID, job.newContract);
            //        dspWriter.CommitTransaction();
            //    }
            //    catch
            //    {
            //        dspWriter.RollBackTransaction();
            //        throw;
            //    }
            //}
        }

        private void finshOwnershipTransfer(int AID, DateTime date, JobData job)
        {
            verifyAccountingClearance(AID, job.customerID, date, job);
            //lock (dspWriter)
            //{
            //    try
            //    {
            //        dspWriter.BeginTransaction();
            //        Subscriber oldCustomer = bdeSubsriber.GetSubscriber(job.customerID);
            //        if (job.newCustomer == null)
            //            throw new ServerUserMessage("New customer not set");
            //        if (job.newCustomer.id == -1)
            //        {
            //            job.newCustomer.id = bdeSubsriber.RegisterSubscriber(AID, job.newCustomer);
            //            job.newCustomer = bdeSubsriber.GetSubscriber(job.newCustomer.id);
            //            dspWriter.UpdateSingleTableRecord(this.DBName, job);
            //        }
            //        Subscription connection = bdeSubsriber.GetSubscription(job.subscriptionID);
            //        if (connection == null)
            //            throw new ServerUserMessage("The connection that will be transfered is not set");
            //        connection.remark = "Transfered to customer " + job.newCustomer.customerCode;
            //        int meterID = connection.meter;
            //        connection.meter = -1;
            //        bdeSubsriber.UpdateSubscription(AID, connection);
            //        bdeSubsriber.ChangeSubcriptionStatus(AID, connection.id, date, SubscriptionStatus.Discontinued);
            //        connection.subscriptionStatus = SubscriptionStatus.Active;
            //        connection.subscriptionStatusDate = date;
            //        connection.id = -1;
            //        connection.contractNo = null;
            //        connection.subscriberID = job.newCustomer.id;
            //        connection.contractNo = this.GetNextContractNumber(AID,connection.Kebele);
            //        connection.remark = "Transfered from customer " + oldCustomer.customerCode;
            //        bdeSubsriber.AddSubscription(AID, connection);
            //        dspWriter.CommitTransaction();
            //    }
            //    catch
            //    {
            //        dspWriter.RollBackTransaction();
            //        throw;
            //    }
            //}
        }

        

        public void ChangeWorkerStatus(int AID, string userID, bool active)
        {
            lock (base.dspWriter)
            {
                JobWorker rec = this.GetWorker(userID);
                if (rec == null)
                {
                    throw new Exception("Worker not found");
                }
                if (rec.active != active)
                {

                    rec.active = active;
                    dspWriter.BeginTransaction();
                    try
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobWorker", "userID='" + rec.userID + "'");
                        base.dspWriter.UpdateSingleTableRecord<JobWorker>(base.DBName, rec, new string[] { "__AID" }, new object[] { AID });
                        changeNo++;
                        dspWriter.CommitTransaction();
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
            }
        }

        public void CheckAndFixConfiguration(int AID)
        {
            lock (base.dspWriter)
            {
                int num;
                SerialBatch batch;
                dspWriter.BeginTransaction();
                try
                {

                    if (this.m_sysPars.invoiceNoSerialBatch < 1)
                    {
                        batch = new SerialBatch();
                        batch.note = "Invoice Number";
                        batch.serialFrom = 1;
                        batch.serialTo = 0xf4240;
                        batch.active = true;
                        batch.batchDate = DateTime.Now;
                        batch.expiryDate = DateTime.Now.AddYears(100);
                        num = this.bdeAccounting.CreateBatch(AID, batch);
                        this.SetSystemParameters(AID, new string[] { "invoiceNoSerialBatch" }, new object[] { num });
                        this.m_sysPars.invoiceNoSerialBatch = num;
                    }
                    if (this.m_sysPars.jobNoSerialBatch < 1)
                    {
                        batch = new SerialBatch();
                        batch.note = "Job Number";
                        batch.serialFrom = 1;
                        batch.serialTo = 0xf4240;
                        batch.active = true;
                        batch.batchDate = DateTime.Now;
                        batch.expiryDate = DateTime.Now.AddYears(100);
                        num = this.bdeAccounting.CreateBatch(AID, batch);
                        this.SetSystemParameters(AID, new string[] { "jobNoSerialBatch" }, new object[] { num });
                        this.m_sysPars.jobNoSerialBatch = num;
                    }
                    if (this.m_sysPars.contractNoSerialBatch < 1)
                    {
                        batch = new SerialBatch();
                        batch.note = "Contract Number";
                        batch.serialFrom = 1;
                        batch.serialTo = 0xf4240;
                        batch.active = true;
                        batch.batchDate = DateTime.Now;
                        batch.expiryDate = DateTime.Now.AddYears(100);
                        num = this.bdeAccounting.CreateBatch(AID, batch);
                        this.SetSystemParameters(AID, new string[] { "contractNoSerialBatch" }, new object[] { num });
                        this.m_sysPars.contractNoSerialBatch = num;
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }

        }
        public void CloseAppointment(int AID, int appointmentID)
        {
            lock (base.dspWriter)
            {
                JobAppointment rec = this.GetAppointment(appointmentID);
                if (rec == null)
                {
                    throw new Exception("Appointment don't exist in database");
                }
                if (!rec.active)
                {
                    throw new Exception("The appointment is already closed");
                }
                rec.active = false;
                rec.appointmentCloseDate = DateTime.Now;
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobAppointment", "id=" + rec.id);
                    base.dspWriter.UpdateSingleTableRecord<JobAppointment>(base.DBName, rec, new string[] { "__AID" }, new object[] { AID });
                    changeNo++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void CreateJobWorker(int AID, JobWorker worker)
        {
            assertIndependentProcessOwner(worker);
            fixWorkerUserID(worker);
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    if (base.dspWriter.GetSTRArrayByFilter<JobWorker>("userID='" + worker.userID + "'").Length > 0)
                    {
                        throw new Exception("Worker is allready registered with this user name");
                    }
                    if (base.dspWriter.GetSTRArrayByFilter<JobWorker>("employeeID='" + worker.employeeID + "'").Length > 0)
                    {
                        throw new Exception("The employee is already registered as a technical worker");
                    }
                    base.dspWriter.InsertSingleTableRecord<JobWorker>(base.DBName, worker, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }

        public void DeleteBOM(int AID, int bomID)
        {
            DeleteBOM(AID, bomID, false);
        }

        public void DeleteBOM(int AID, int bomID,bool internalCommand)
        {
            lock (base.dspWriter)
            {
                JobBillOfMaterial billOfMaterial = this.GetBillOfMaterial(bomID);
                if (billOfMaterial == null)
                {
                    throw new Exception("Bill of quantity not found in the database:" + bomID);
                }
                if (subscriberBDE.isBillPaid(billOfMaterial.invoiceNo))
                {
                    throw new Exception("Bill of quantity can't be deleted because payment is collected");
                }
                if (!internalCommand)
                {
                    if (this.GetJob(billOfMaterial.jobID).status != StandardJobStatus.SURVEY)
                    {
                        throw new Exception("Bill of quanity can be modified only in survey stage");
                    }

                    AuditInfo ai = ApplicationServer.SecurityBDE.getAuditInfo(AID);
                    if (!billOfMaterial.preparedBy.Equals(ai.userName))
                        throw new ServerUserMessage("This bill of item can only be deleted by " + billOfMaterial.preparedBy);
                }
                base.dspWriter.BeginTransaction();
                try
                {
                    if (billOfMaterial.invoiceNo != -1)
                    {
                        bdeSubsriber.deleteCustomerBill(AID, billOfMaterial.invoiceNo);
                        //BdeAccounting.DeleteGenericDocument(AID, billOfMaterial.invoiceNo);
                    }
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobBillOfMaterial", "id=" + billOfMaterial.id);
                    base.dspWriter.DeleteSingleTableRecrod<JobBillOfMaterial>(base.DBName, new object[] { bomID });
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void DeleteWorker(int AID, string userName)
        {
            lock (base.dspWriter)
            {
                if (((int)base.dspWriter.ExecuteScalar("Select count(*) from " + base.DBName + ".dbo.JobStatusHistory where agent='" + userName + "'")) > 0)
                {
                    throw new Exception("This worker can not be deleted because there are records refering him/her in the system");
                }
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobWorker", "userID='" + userName + "'");
                    base.dspWriter.DeleteSingleTableRecrod<JobWorker>(base.DBName, new object[] { userName });
                    changeNo++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void SetSystemParameters(int AID, string[] fields, object[] vals)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                try
                {
                    changeNo++;
                    base.dspWriter.SetSystemParameters<JobManagerSystemParameters>(base.m_DBName, this.m_sysPars, fields, vals, true, AID);
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }
        public void UpdateAppointment(int AID, JobAppointment appointment)
        {
            lock (base.dspWriter)
            {
                base.dspWriter.BeginTransaction();
                try
                {
                    JobAppointment appointment2 = this.GetAppointment(appointment.id);
                    if (appointment2 == null)
                    {
                        throw new Exception("Appointment doesn't exist");
                    }
                    //if (appointment2.agent.ToUpper() != appointment.agent.ToUpper())
                    //{
                    //    throw new Exception("Only " + appointment2.agent + " can change this data");
                    //}
                    if (!appointment2.active)
                    {
                        throw new Exception("It is not allowed to delete closed appointment");
                    }
                    appointment2.appointmentDate = appointment.appointmentDate;
                    appointment2.note = appointment.note;
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobAppointment", "id=" + appointment.id);
                    base.dspWriter.DeleteSingleTableRecrod<JobAppointment>(base.DBName, new object[] { appointment.id });
                    this.AddAppointment(AID, appointment2);
                    changeNo++;
                    base.dspWriter.CommitTransaction();
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        public void UpdateHistoryEntry(int AID, JobStatusHistory history)
        {
            lock (base.dspWriter)
            {
                JobData job = this.GetJob(history.jobID);
                if (job == null)
                {
                    throw new Exception("Job not found in the database");
                }
                JobStatusHistory[] jobHistory = this.GetJobHistory(history.jobID);
                for (int i = 0; i < jobHistory.Length; i++)
                {
                    if (jobHistory[i].id == history.id)
                    {
                        JobStatusHistory history2 = jobHistory[i];
                        //if (history2.agent != history.agent)
                        //{
                        //    throw new Exception("The data can be changed only by " + history.agent);
                        //}
                        base.dspWriter.BeginTransaction();
                        try
                        {
                            if (i == (jobHistory.Length - 1))
                            {
                                job.status = history.newStatus;
                                job.statusDate = history.changeDate;
                                dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobData", "id=" + job.id);
                                base.dspWriter.UpdateSingleTableRecord<JobData>(base.DBName, job, new string[] { "__AID" }, new object[] { AID });
                            }
                            else
                            {
                                jobHistory[i + 1].oldStatus = job.status;
                                dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobStatusHistory", "id=" + jobHistory[i + 1].id);
                                base.dspWriter.UpdateSingleTableRecord<JobStatusHistory>(base.DBName, jobHistory[i + 1], new string[] { "__AID" }, new object[] { AID });
                            }
                            dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobStatusHistory", "id=" + history.id);
                            base.dspWriter.UpdateSingleTableRecord<JobStatusHistory>(base.DBName, history, new string[] { "__AID" }, new object[] { AID });
                            this.CheckHistoryRecord(history.jobID, "Error updating history entry");
                            changeNo++;
                            base.dspWriter.CommitTransaction();
                        }
                        catch (Exception exception)
                        {
                            base.dspWriter.RollBackTransaction();
                            throw new Exception("Error updating history entry", exception);
                        }
                        goto Label_018D;
                    }
                }
                throw new Exception("History record not found in the database");
            Label_018D: ;
            }
        }

        public void UpdateJob(int AID, JobData job, JobWorker agent, WorkFlowData data)
        {
            IJobRuleServerHandler re = getJobRuleHandler(job.applicationType,true);
            lock (base.dspWriter)
            {
                JobData rec = this.GetJob(job.id);
                if (rec == null)
                {
                    throw new Exception(string.Format("Job {0} doesn't exist in database", job.jobNo));
                }

                JobStatusHistory[] jobHistory = this.GetJobHistory(job.id);
                //if (string.Compare(agent.userID ,jobHistory[0].agent,true)!=0)
                //{
                //    throw new Exception("Data for this job can only be updated by " + jobHistory[0].agent);
                //}
                dspWriter.BeginTransaction();
                try
                {

                    rec.newCustomer = job.newCustomer;
                    rec.customerID = job.customerID;
                    rec.description = job.description;
                    rec.serviceTypes = job.serviceTypes;
                    
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobData", "id=" + rec.id);
                    if(data!=null)
                        re.updateApplicationData(AID, job.startDate, job, agent, data);
                    base.dspWriter.UpdateSingleTableRecord<JobData>(base.DBName, rec, new string[] { "__AID" }, new object[] { AID });
                    changeNo++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        void fixWorkerUserID(JobWorker w)
        {
            if (string.IsNullOrEmpty(bdePayroll.GetEmployee(w.employeeID).loginName))
                throw new ServerUserMessage("Please set loging ID for the employee");
            w.userID=bdePayroll.GetEmployee(w.employeeID).loginName;
        }
        void assertIndependentProcessOwner(JobWorker worker)
        {
            bool hasNoPO = false;
            bool hasPO = false;
            HashSet<int> exceptions = null;
            String config = System.Configuration.ConfigurationManager.AppSettings["job_independent_po_rule_exception"];
            if(!String.IsNullOrEmpty(config))
            {
                exceptions = new HashSet<int>();
                foreach(String c in config.Split(new char[] { ',' },StringSplitOptions.RemoveEmptyEntries))
                {
                    exceptions.Add(int.Parse(c));
                }
            }
            if(exceptions!=null && exceptions.Count>0)
            {
                bool byPass = true;
                foreach(var t in worker.jobTypes)
                {
                    if(!exceptions.Contains(t))
                    {
                        byPass = false;
                        break;
                    }
                }
                if (byPass)
                    return;
            }
            foreach (JobWorkerRole role in worker.roles)
            {
                if(role==JobWorkerRole.Process_Owner)
                {
                    hasPO = true;
                }
                if (role != JobWorkerRole.Process_Owner)
                {
                    hasNoPO= true;
                }
                if (hasPO && hasNoPO)
                    throw new ServerUserMessage("It is not allowed to combile process owner role with other roles");
            }
        }
        public void UpdateJobWorker(int AID, JobWorker worker)
        {
            assertIndependentProcessOwner(worker);
            fixWorkerUserID(worker);
            lock (base.dspWriter)
            {
                base.dspWriter.setReadDB(base.DBName);
                dspWriter.BeginTransaction();
                try
                {
                    JobWorker[] sTRArrayByFilter = base.dspWriter.GetSTRArrayByFilter<JobWorker>("userID='" + worker.userID + "'");
                    if (sTRArrayByFilter.Length <= 0)
                    {
                        throw new Exception("Worker not found");
                    }
                    if (!sTRArrayByFilter[0].active)
                    {
                        throw new Exception("The worker is not active");
                    }
                    if (worker.employeeID != sTRArrayByFilter[0].employeeID)
                    {
                        throw new Exception("You can't chage the employee for a worker");
                    }
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobWorker", "userID='" + worker.userID + "'");
                    base.dspWriter.UpdateSingleTableRecord<JobWorker>(base.DBName, worker, new string[] { "__AID" }, new object[] { AID });
                    changeNo++;
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
                finally
                {
                    base.dspWriter.restoreReadDB();
                }
            }
        }
        // BDE exposed
        public int getChangeNo()
        {
            return changeNo;
        }
        public void VoidJobReceipt(int AID, int bomID)
        {
            throw new Exception("Method deprecated");
            //lock (dspWriter)
            //{
            //    JobBillOfMaterial bom = GetBillOfMaterial(bomID);
            //    if (bom == null)
            //        throw new Exception("Bill of quantity not found id:" + bomID);
            //    if (!subscriberBDE.isBillPaid(bom.invoiceNo))
            //        throw new Exception("Bill of quantity not paid");
            //    JobData job = GetJob(bom.jobID);
            //    if (StandardJobStatus.isInactiveState(job.status))
            //        throw new ServerUserMessage("Receipts of inactive jobs can't be void");
            //    dspWriter.BeginTransaction();
            //    try
            //    {
            //        bdeAccounting.DeleteAccountDocument(AID, bom.receiptNo,false);
            //        bom.receiptNo = -1;
            //        dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobBillOfMaterial", "id=" + bom.id);
            //        dspWriter.UpdateSingleTableRecord(this.DBName, bom, new string[] { "__AID" }, new object[] { AID });
            //        dspWriter.CommitTransaction();
            //    }
            //    catch
            //    {
            //        dspWriter.RollBackTransaction();
            //        throw;
            //    }
            //}
        }

    }
}

