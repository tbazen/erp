
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using INTAPS.Accounting.BDE;
using System.Reflection;
namespace INTAPS.WSIS.Job
{
    public partial class JobManagerBDE : BDEBase
    {
        Dictionary<int, JobRuleServerHandlerAttribute> _serverHandlerInfo;
        Dictionary<int, IJobRuleServerHandler> _serverHandlers;
        Dictionary<int, JobStatusType> _jobStatusTypes;
        public void loadJobRuleHandlers()
        {
            _serverHandlerInfo = new Dictionary<int, JobRuleServerHandlerAttribute>();
            _serverHandlers = new Dictionary<int, IJobRuleServerHandler>();
            _jobStatusTypes = new Dictionary<int, JobStatusType>();
            string assemblies = System.Configuration.ConfigurationManager.AppSettings["jobRuleAssemblies"];
            if (assemblies == null)
                return;
            foreach (string an in assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    Assembly a = null;

                    if (a == null)
                    {
                        try
                        {
                            a = System.Reflection.Assembly.Load(an);
                        }
                        catch (System.IO.FileNotFoundException)
                        {
                            a = null;
                        }
                    }

                    if (a == null)
                    {
                        try
                        {
                            a = System.Reflection.Assembly.LoadFile(System.IO.Path.Combine(Environment.CurrentDirectory, an));
                        }
                        catch (System.IO.FileNotFoundException)
                        {
                            a = null;
                        }
                    }



                    if (a == null)
                    {
                        ApplicationServer.EventLoger.Log(EventLogType.Errors, $"Couldn't load job rule assembly:{an}");
                        continue;
                    }
                    int count = 0;
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.GetInterface("INTAPS.Evaluator.IFunction") != null)
                        {
                            System.Reflection.ConstructorInfo c = t.GetConstructor(new Type[] { typeof(JobManagerBDE) });
                            if (c != null)
                            {
                                foreach (KeyValuePair<int, IReportProcessor> pair in this.bdeAccounting.ReportProcessors)
                                {
                                    ((ReportProcessorBase)pair.Value).funcs.Add(c.Invoke(new object[] { this }) as INTAPS.Evaluator.IFunction);
                                }
                            }

                        }
                        object[] atr = t.GetCustomAttributes(typeof(JobRuleServerHandlerAttribute), true);
                        if (atr == null || atr.Length == 0)
                            continue;
                        if (atr.Length > 1)
                        {
                            ApplicationServer.EventLoger.Log(EventLogType.Errors, "Multiple job JobRuleServerHandlerAttribute found for type ");
                            continue;
                        }

                        JobRuleServerHandlerAttribute jatr = atr[0] as JobRuleServerHandlerAttribute;
                        ConstructorInfo ci = t.GetConstructor(new Type[] { typeof(JobManagerBDE) });
                        if (ci == null)
                        {
                            ApplicationServer.EventLoger.Log(EventLogType.Errors, "Constructor not found for type ");
                            continue;
                        }
                        try
                        {
                            IJobRuleServerHandler o = ci.Invoke(new object[] { this }) as IJobRuleServerHandler;
                            if (o == null)
                            {
                                ApplicationServer.EventLoger.Log(EventLogType.Errors, "Server rule handler " + t + " doesn't implement IJobRuleServerHandler");
                                continue;
                            }

                            if (_serverHandlerInfo.ContainsKey(jatr.typeID))
                            {
                                if (_serverHandlerInfo[jatr.typeID].priority > jatr.priority)
                                    continue;
                                if (_serverHandlerInfo[jatr.typeID].priority == jatr.priority)
                                    throw new ServerUserMessage("Job server rule handler repeated. ID {0}", jatr.typeID);
                                _serverHandlerInfo[jatr.typeID] = jatr;
                                _serverHandlers[jatr.typeID] = o;
                            }
                            else
                            {
                                _serverHandlerInfo.Add(jatr.typeID, jatr);
                                _serverHandlers.Add(jatr.typeID, o);
                            }
                            foreach (JobStatusType st in o.getAllStatusTypes())
                            {
                                if (!_jobStatusTypes.ContainsKey(st.id))
                                {
                                    _jobStatusTypes.Add(st.id, st);
                                }
                            }
                            count++;
                        }
                        catch (Exception tex)
                        {
                            ApplicationServer.EventLoger.LogException("Error trying to instantiate server rule handler " + t, tex);
                        }
                    }
                    if (count > 0)
                    {
                        ApplicationServer.EventLoger.Log(string.Format("{0} job rule server handler(s) loaded from {1}", count, an));
                    }
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException("Error trying to load server rule handler assembly " + an, ex);
                }
            }
        }
        IJobRuleServerHandler getJobRuleHandler(int typeID, bool throwException)
        {
            if (!_serverHandlers.ContainsKey(typeID))
            {
                if (throwException)
                    throw new ServerUserMessage("Invalid server handler :" + typeID);
                return null;
            }
            return _serverHandlers[typeID];

        }
        // BDE exposed
        public WorkFlowData getWorkFlowData(int jobID, int typeID, int key, bool fullData)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string cr = string.Format("jobID={0} and typeID={1} and dataKey={2}", jobID, typeID, key);
                WorkFlowData[] ret = dspReader.GetSTRArrayByFilter<WorkFlowData>(cr);
                if (ret.Length == 0)
                    return null;
                if (!fullData)
                    return ret[0];
                string xml = dspReader.ExecuteScalar(string.Format("Select jobData from  {0}.dbo.WorkFlowData where {1}", this.DBName, cr)) as string;
                if (string.IsNullOrEmpty(xml))
                    return null;
                IJobRuleServerHandler rule = getJobRuleHandler(typeID, true);
                WorkFlowData r = WorkFlowData.DeserializeXML(xml, Type.GetType(ret[0].dataType), rule.getDataExtraTypes());
                r.copyData(ret[0]);
                return r;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void deleteWorkData(int AID, int jobID, int typeID, int key)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    string cr = string.Format("jobID={0} and typeID={1} and dataKey={2}", jobID, typeID, key);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.WorkFlowData", cr);
                    dspWriter.DeleteSingleTableRecrod<WorkFlowData>(this.DBName, jobID, typeID, key);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public int getJobTypeIDByHandlerType(Type type)
        {
            foreach (KeyValuePair<int, IJobRuleServerHandler> kv in _serverHandlers)
                if (kv.Value.GetType().Equals(type))
                    return kv.Key;
            return -1;
        }
        // BDE exposed
        public void setWorkFlowData(int AID, WorkFlowData data)
        {
            if (data == null)
                throw new ServerUserMessage("Empty data not allowed");
            WorkFlowData old = getWorkFlowData(data.jobID, data.typeID, data.dataKey, false);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (old != null)
                    {
                        deleteWorkData(AID, data.jobID, data.typeID, data.dataKey);
                    }
                    data.dataType = data.GetType().FullName + "," + data.GetType().Assembly.GetName().Name;
                    dspWriter.InsertSingleTableRecord(this.DBName, data, new string[] { "jobData", "__AID" }, new object[] { data.ToXML(getJobRuleHandler(data.typeID, true).getDataExtraTypes()), AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        internal string GetCommandString(int jobID, int status)
        {
            JobData job = this.GetJob(jobID);
            IJobRuleServerHandler re = getJobRuleHandler(job.applicationType, true);
            return re.getCommandString(status);
        }
        // BDE exposed
        public JobStatusType getJobStatus(int status)
        {
            if (_jobStatusTypes.ContainsKey(status))
                return _jobStatusTypes[status];
            return null;
        }

        // BDE exposed
        public int[] getPossibleNextStatus(DateTime date, int jobID)
        {
            JobData job = this.GetJob(jobID);
            IJobRuleServerHandler re = getJobRuleHandler(job.applicationType, true);
            return re.getPossibleNextState(date, null, job);
        }
        // BDE exposed
        public JobTypeInfo getJobType(int typeID)
        {
            if (_serverHandlerInfo.ContainsKey(typeID))
                return _serverHandlerInfo[typeID].getJobTypeInfo();
            return null;
        }
        // BDE exposed
        public JobTypeInfo[] getAllJobTypes()
        {
            JobTypeInfo[] ret = new JobTypeInfo[_serverHandlerInfo.Count];
            int i = 0;
            foreach (KeyValuePair<int, JobRuleServerHandlerAttribute> kv in _serverHandlerInfo)
            {
                ret[i] = kv.Value.getJobTypeInfo();
                i++;
            }
            return ret;
        }

        // BDE exposed
        public void saveConfiguration(int AID, int typeID, object config)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    string xml = SQLHelper.ObjectToXml(config);
                    if ((int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.JobRuleConfiguration where applicationType={1}", this.DBName, typeID)) > 0)
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobRuleConfiguration", "applicationType=" + typeID);
                        dspWriter.Update(this.DBName, "JobRuleConfiguration", new string[] { "configData", "__AID" }, new object[] { xml, AID }
                            , "applicationType=" + typeID);
                    }
                    else
                        dspWriter.Insert(this.DBName, "JobRuleConfiguration", new string[] { "applicationType", "configData", "__AID" }, new object[] { typeID, xml, AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public object getConfiguration(int typeID, Type type)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                object val = dspWriter.ExecuteScalar(string.Format("Select configData from {0}.dbo.JobRuleConfiguration where applicationType={1}", this.DBName, typeID));
                if (val == null || DBNull.Value.Equals(val))
                    return null;
                return SQLHelper.XmlToObject((string)val, type);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public T getConfiguration<T>(int typeID) where T : class
        {
            return getConfiguration(typeID, typeof(T)) as T;
        }
    }
}

