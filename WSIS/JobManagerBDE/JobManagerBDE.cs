using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using INTAPS.Accounting.BDE;
using BIZNET.iERP;
namespace INTAPS.WSIS.Job
{
    public partial class JobManagerBDE : BDEBase
    {
        private AccountingBDE bdeAccounting;

        public AccountingBDE BdeAccounting
        {
            get { return bdeAccounting; }

        }
        private PayrollBDE bdePayroll;

        public PayrollBDE BdePayroll
        {
            get { return bdePayroll; }

        }

        private SubscriberManagmentBDE bdeSubsriber;
        private BIZNET.iERP.Server.iERPTransactionBDE bdeBERP;
        public BIZNET.iERP.Server.iERPTransactionBDE bERP
        {
            get
            {
                return bdeBERP;
            }
        }

        private JobManagerSystemParameters m_sysPars;
        public int changeNo = 0;
        public JobManagerBDE(string BDEName, string DBName, SQLHelper writer, string readSqlCon)
            : base(BDEName, DBName, writer, readSqlCon)
        {
            this.bdeSubsriber = (SubscriberManagmentBDE)ApplicationServer.GetBDE("Subscriber");
            this.bdeAccounting = (AccountingBDE)ApplicationServer.GetBDE("Accounting");
            this.bdePayroll = (PayrollBDE)ApplicationServer.GetBDE("Payroll");
            this.bdeBERP = (BIZNET.iERP.Server.iERPTransactionBDE)ApplicationServer.GetBDE("iERP");
            this.LoadSystemParameters();
            this.InitializeReporting();
            this.loadJobRuleHandlers();
        }
        public SubscriberManagmentBDE subscriberBDE
        {
            get
            {
                return bdeSubsriber;
            }
        }
        private void InitializeReporting()
        {
            foreach (KeyValuePair<int, IReportProcessor> pair in this.bdeAccounting.ReportProcessors)
            {
                ((ReportProcessorBase)pair.Value).vars.Add("conJob", ReportProcessorBase.CreateConnection("JobManager"));
                //((ReportProcessorBase)pair.Value).funcs.Add(new ActiveJobList(this));
                //((ReportProcessorBase)pair.Value).funcs.Add(new ProcessedJobSummary(this));
                //((ReportProcessorBase)pair.Value).funcs.Add(new GetJobData(this));
                //((ReportProcessorBase)pair.Value).funcs.Add(new GetApplicationType(this));
                //((ReportProcessorBase)pair.Value).funcs.Add(new GetDurationText(this));
                //((ReportProcessorBase)pair.Value).funcs.Add(new DateDiffSeconds());
                //((ReportProcessorBase)pair.Value).funcs.Add(new GetStatusType(this));
            }
        }



        private void CheckHistoryRecord(int jobID, string message)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                JobData job = this.GetJob(jobID);
                if (job == null)
                {
                    throw new Exception(message + "\nJob doesn't exist in the database");
                }
                JobStatusHistory[] jobHistory = this.GetJobHistory(job.id);
                if (jobHistory.Length == 0)
                {
                    throw new Exception(message + "\nNo job history record");
                }
                JobStatusHistory history = jobHistory[0];
                if (history.oldStatus != StandardJobStatus.NONE)
                {
                    throw new Exception(message + "\nThe old status record should always be node");
                }
                for (int i = 1; i < jobHistory.Length; i++)
                {
                    if (history.newStatus != jobHistory[i].oldStatus)
                    {
                        throw new Exception(string.Concat(new object[] { message, "\nOld status for record ", i, " don't match with status for the previos entry" }));
                    }
                    if (jobHistory[i].changeDate < history.changeDate)
                    {
                        throw new Exception(string.Concat(new object[] { message, "\nThe date for record ", i, " is earlier thatn the status for the previos entry" }));
                    }
                    history = jobHistory[i];
                }
                if ((job.status != history.newStatus) || (job.statusDate != history.changeDate))
                {
                    throw new Exception(message + "\nThe last history record and the job data don't match");
                }
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }


        JobBillDocument getJobBillDocument(int AID,JobData job, JobBillOfMaterial billOfMaterial)
        {
            if (billOfMaterial.invoiceNo > 0)
                return bdeAccounting.GetAccountDocument(billOfMaterial.invoiceNo, true) as JobBillDocument;
            return this.getJobRuleHandler(job.applicationType, true).generateInvoice(AID,getJobCustomer(job), job, billOfMaterial);
        }

        // BDE exposed
        public double getBOMTotal(int bomID)
        {
            JobBillOfMaterial billOfMaterial = this.GetBillOfMaterial(bomID);
            if (billOfMaterial == null)
                throw new ServerUserMessage("Bill of material not found for this job");
            JobData job = this.GetJob(billOfMaterial.jobID);
            if (job == null)
                throw new ServerUserMessage("Job not found in the database");
            return getJobBillDocument(-1,job, billOfMaterial).total;
        }
        public void deleteBOMInvoice(int AID, int bomID)
        {
            JobBillOfMaterial billOfMaterial = this.GetBillOfMaterial(bomID);
            if (billOfMaterial == null)
                throw new ServerUserMessage("Bill of material not found for this job");
            if (billOfMaterial.invoiceNo != -1)
            {
                if (this.subscriberBDE.isBillPaid(billOfMaterial.invoiceNo))
                    throw new ServerUserMessage("Job can't be canceled because payment is already made.");
                this.subscriberBDE.deleteCustomerBill(AID, billOfMaterial.invoiceNo);
                billOfMaterial.invoiceNo = -1;
                base.dspWriter.UpdateSingleTableRecord<JobBillOfMaterial>(base.DBName, billOfMaterial, new string[] { "__AID" }, new object[] { AID });
            }
        }
        public void generateInvoice(int AID, int bomID, JobWorker agent)
        {
            JobBillOfMaterial billOfMaterial = this.GetBillOfMaterial(bomID);
            if (billOfMaterial == null)
                throw new ServerUserMessage("Bill of material not found for this job");
            if (billOfMaterial.invoiceNo != -1)
                throw new ServerUserMessage("Invoice already produced");
            JobData job = this.GetJob(billOfMaterial.jobID);
            if (job == null)
                throw new ServerUserMessage("Job not found in the database");

            JobBillDocument bill = this.getJobRuleHandler(job.applicationType, true).generateInvoice(AID,getJobCustomer(job), job, billOfMaterial);
            bill.job = job;
            bill.customer = getJobCustomer(job);
            base.dspWriter.BeginTransaction();
            try
            {
                bill.AccountDocumentID = bdeSubsriber.addBill(AID, bill, bill.jobItems);
                bdeAccounting.addDocumentSerials(AID, bill, getJobAccountingTypedDocumentReference(job, true));
                billOfMaterial.invoiceNo = bill.AccountDocumentID;
                billOfMaterial.invoiceDate = DateTime.Now;
                base.dspWriter.UpdateSingleTableRecord<JobBillOfMaterial>(base.DBName, billOfMaterial, new string[] { "__AID" }, new object[] { AID });
                base.dspWriter.CommitTransaction();
            }
            catch
            {
                base.dspWriter.RollBackTransaction();
                throw;
            }

        }

        public Subscriber getJobCustomer(JobData job)
        {
            if (job.customerID == -1)
                return job.newCustomer;
            return bdeSubsriber.GetSubscriber(job.customerID);
        }

        public List<JobData> GetAllJobs()
        {
            List<JobData> list2;
            List<JobData> list = new List<JobData>();
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                list.AddRange(readerHelper.GetSTRArrayByFilter<JobData>(null));
                list2 = list;
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return list2;
        }

        public JobWorker[] GetAllWorkers()
        {
            JobWorker[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobWorker>(null);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        // BDE exposed
        public JobAppointment GetAppointment(int id)
        {
            JobAppointment appointment;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                JobAppointment[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobAppointment>("id=" + id);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                appointment = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return appointment;
        }

        public JobBillOfMaterial GetBillOfMaterial(int bomID)
        {
            JobBillOfMaterial material;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                JobBillOfMaterial[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobBillOfMaterial>("id=" + bomID);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                material = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return material;
        }
        // BDE exposed
        public bool jobAppliesTo(DateTime date, int jobID, JobWorker worker)
        {
            JobData job = this.GetJob(jobID);
            return getJobRuleHandler(job.applicationType, true).jobAppliesTo(date, job, worker);
        }
        public JobData[] GetFilteredJobs(DateTime date, JobWorker worker)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                List<int> list = new List<int>();
                list.Add(StandardJobStatus.FINISHED);
                list.Add(StandardJobStatus.CANCELED);
                if (list.Count == 0)
                {
                    return new JobData[0];
                }
                string filter = " status not in (" + ((int)list[0]);
                for (int i = 1; i < list.Count; i++)
                {
                    filter = filter + "," + ((int)list[i]);
                }
                filter = filter + ")";
                JobData[] jobs = readerHelper.GetSTRArrayByFilter<JobData>(filter);
                List<JobData> ret = new List<JobData>();
                foreach (JobData j in jobs)
                {
                    var rule = getJobRuleHandler(j.applicationType, false);
                    if(rule==null)
                    {
                        ApplicationServer.EventLoger.Log($"Rule handler not loaded for appication type :{j.applicationType}");
                        continue;
                    }
                    if (rule.jobAppliesTo(date, j, worker))
                    {
                        ret.Add(j);
                    }
                }
                ret.Sort(new ActiveJobSorter(this));
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }

        }

        public string GetInvoicePreview(JobBillOfMaterial bom)
        {
            JobData job = GetJob(bom.jobID);
            Subscriber customer = getJobCustomer(job);
            JobBillDocument bill = getJobBillDocument(-1,job, bom);
            return this.getJobRuleHandler(job.applicationType, true).generateInvoiceHTML(bill);
        }



        public JobData GetJob(int jobID)
        {
            JobData data;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                JobData[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobData>("id=" + jobID);
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                data = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return data;
        }

        public JobAppointment[] GetJobAppointments(int jobHandle)
        {
            JobAppointment[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobAppointment>("jobID=" + jobHandle);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public JobBillOfMaterial[] GetJobBOM(int jobID)
        {
            JobBillOfMaterial[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobBillOfMaterial>("jobID=" + jobID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public string GetJobCard(int jobID)
        {
            throw new ServerUserMessage("This method is depreciated");
        }

        public JobStatusHistory[] GetJobHistory(int jobID)
        {
            JobStatusHistory[] sTRArrayByFilter;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobStatusHistory>("jobID=" + jobID);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return sTRArrayByFilter;
        }

        public JobAppointment GetLatestAppointment(int jobID)
        {
            JobAppointment appointment;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                object obj2 = readerHelper.ExecuteScalar(string.Format("Select max(id) from {0}.dbo.{1} where jobID={2}", base.DBName, typeof(JobAppointment).Name, jobID));
                if (!(obj2 is int))
                {
                    return null;
                }
                appointment = this.GetAppointment((int)obj2);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return appointment;
        }

        public JobStatusHistory GetLatestHistory(int jobID)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                throw new Exception("BDE Note implemented");
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }

        public string GetNextContractNumber(int AID, Subscription connection)
        {
            UsedSerial ser = new UsedSerial();
            ser.val = this.bdeAccounting.GetNextSerial(this.m_sysPars.contractNoSerialBatch);
            ser.batchID = this.m_sysPars.contractNoSerialBatch;
            ser.date = DateTime.Now;
            ser.isVoid = false;
            this.bdeAccounting.UseSerial(AID, ser);
            string contractNo = bdeSubsriber.getRule().formatContractNo(connection, ser.val);
            if(contractNo == null)
                return "C" + (connection.Kebele.ToString("00") + ser.val.ToString(this.m_sysPars.contractNoFormat));
            return contractNo;
        }

        private string GetNextInvoiceNumber(int AID)
        {
            UsedSerial ser = new UsedSerial();
            ser.val = this.bdeAccounting.GetNextSerial(this.m_sysPars.invoiceNoSerialBatch);
            ser.batchID = this.m_sysPars.invoiceNoSerialBatch;
            ser.date = DateTime.Now;
            ser.isVoid = false;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    this.bdeAccounting.UseSerial(AID, ser);
                    dspWriter.CommitTransaction();
                    return ser.val.ToString(this.m_sysPars.invoiceNoFormat);
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        private string GetNextJobNumber(int AID)
        {
            UsedSerial ser = new UsedSerial();
            ser.val = this.bdeAccounting.GetNextSerial(this.m_sysPars.jobNoSerialBatch);
            ser.batchID = this.m_sysPars.jobNoSerialBatch;
            ser.date = DateTime.Now;
            ser.isVoid = false;
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    this.bdeAccounting.UseSerial(AID, ser);
                    dspWriter.CommitTransaction();
                    return ser.val.ToString(this.m_sysPars.jobNoFormat);
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }


        public object[] GetSystemParameters(string[] fields)
        {
            object[] systemParameters;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                systemParameters = readerHelper.GetSystemParameters<JobManagerSystemParameters>(this.m_sysPars, fields);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return systemParameters;
        }

        public JobWorker GetWorker(string userID)
        {
            JobWorker worker;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                JobWorker[] sTRArrayByFilter = readerHelper.GetSTRArrayByFilter<JobWorker>("userID='" + userID + "'");
                if (sTRArrayByFilter.Length == 0)
                {
                    return null;
                }
                worker = sTRArrayByFilter[0];
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return worker;
        }

        private void LoadSystemParameters()
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                this.m_sysPars = readerHelper.LoadSystemParameters<JobManagerSystemParameters>();
            }
            catch (Exception exception)
            {
                ApplicationServer.EventLoger.LogException("Error loading system parameters.", exception);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }




        public JobData[] SearchJob(string query, int index, int pageSize, out int NRecords)
        {
            JobData[] dataArray;
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                dataArray = readerHelper.GetSTRArrayByFilter<JobData>("jobNo like '{0}%'".format(query), index, pageSize, out NRecords);
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
            return dataArray;
        }

        public List<JobData> SearchJob(string query, bool byDate, DateTime fromDate, DateTime toDate, int status)
        {
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                throw new Exception("BDE Note implemented");
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }
        }
        public int SetBillofMateial(int AID, JobBillOfMaterial bom)
        {
            return SetBillofMateial(AID, bom, false);
        }
        public int SetBillofMateial(int AID, JobBillOfMaterial bom,bool dontValidateEditable)
        {
            if (bdeAccounting.DocumentExists(bom.invoiceNo))
                throw new ServerUserMessage("This bill of quantities can't be edited because invoice is already prepared");
            int id;
            List<string> usedItems = new List<string>();

            foreach (JobBOMItem item in bom.items)
            {
                BIZNET.iERP.TransactionItems titem;
                if ((titem = bdeBERP.GetTransactionItems(item.itemID)) == null)
                    throw new ServerUserMessage("Invalid item code " + item.itemID);
                if (usedItems.Contains(item.itemID))
                    throw new ServerUserMessage("It is not allowed to repeat items");
                usedItems.Add(item.itemID);
                if (AccountBase.AmountLess(titem.FixedUnitPrice, 0))
                    throw new ServerUserMessage("Please configure fixerd sales price for item " + titem.NameCode + " in bERP");

            }
            lock (base.dspWriter)
            {
                JobBillOfMaterial billOfMaterial;
                JobData job = this.GetJob(bom.jobID);
                if (job == null)
                {
                    throw new Exception("Job not in database");
                }

                if (bom.id == -1)
                {
                    billOfMaterial = null;
                }
                else
                {
                    billOfMaterial = this.GetBillOfMaterial(bom.id);
                    if (subscriberBDE.isBillPaid(bom.invoiceNo))
                    {
                        throw new Exception("Bill of material can't be edited because payment is collected based on it");
                    }
                    if (!dontValidateEditable && !StandardJobStatus.isBOQEditable(job.applicationType,job.status))
                        throw new ServerUserMessage("It is not allowed to edit bill of quantity at this stage");
                }
                base.dspWriter.BeginTransaction();
                try
                {
                    IJobRuleServerHandler rule = this.getJobRuleHandler(job.applicationType, false);
                    if (rule != null)
                        rule.onBOMSet(AID, job, bom);
                    if (bom.id == -1)
                    {
                        bom.id = AutoIncrement.GetKey("JM.BomID");
                        bom.preparedBy = ApplicationServer.SecurityBDE.getAuditInfo(AID).userName;
                        base.dspWriter.InsertSingleTableRecord<JobBillOfMaterial>(base.DBName, bom, new string[] { "__AID" }, new object[] { AID });
                    }
                    else
                    {
                        base.dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobBillOfMaterial", "id=" + bom.id);
                        base.dspWriter.UpdateSingleTableRecord<JobBillOfMaterial>(base.DBName, bom, new string[] { "__AID" }, new object[] { AID });
                    }
                    base.dspWriter.CommitTransaction();
                    id = bom.id;
                }
                catch
                {
                    base.dspWriter.RollBackTransaction();
                    throw;
                }
            }
            return id;
        }

        // BDE exposed
        public void setBillofMaterialCustomerReference(int AID, JobBillOfMaterial bom)
        {
            JobBillOfMaterial existing = GetBillOfMaterial(bom.id);
            if (existing == null)
                throw new ServerUserMessage("Invalid bill of material id " + bom.id);
            int i = 0;
            foreach (JobBOMItem item in existing.items)
            {
                if (!item.inSourced)
                {
                    item.referenceUnitPrice = bom.items[i].referenceUnitPrice;
                }
                i++;
            }
            JobData job = this.GetJob(bom.jobID);
            if (job == null)
            {
                throw new Exception("Job not in database");
            }


            if (subscriberBDE.isBillPaid(bom.invoiceNo))
            {
                throw new Exception("Bill of material can't be edited because payment is collected based on it");
            }

            base.dspWriter.BeginTransaction();
            try
            {
                base.dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobBillOfMaterial", "id=" + existing.id);
                base.dspWriter.UpdateSingleTableRecord<JobBillOfMaterial>(base.DBName, existing, new string[] { "__AID" }, new object[] { AID });
                base.dspWriter.CommitTransaction();
            }
            catch
            {
                base.dspWriter.RollBackTransaction();
                throw;
            }
        }

        public JobManagerSystemParameters SysPars
        {
            get
            {
                return this.m_sysPars;
            }
        }

        private class ActiveJobSorter : IComparer<JobData>
        {
            public JobManagerBDE _parent;

            public ActiveJobSorter(JobManagerBDE parent)
            {
                this._parent = parent;
            }

            public int Compare(JobData x, JobData y)
            {
                DateTime maxValue;
                DateTime appointmentDate;
                JobAppointment latestAppointment = this._parent.GetLatestAppointment(x.id);
                if (!((latestAppointment != null) && latestAppointment.active))
                {
                    maxValue = DateTime.MaxValue;
                }
                else
                {
                    maxValue = latestAppointment.appointmentDate;
                }
                latestAppointment = this._parent.GetLatestAppointment(y.id);
                if (!((latestAppointment != null) && latestAppointment.active))
                {
                    appointmentDate = DateTime.MaxValue;
                }
                else
                {
                    appointmentDate = latestAppointment.appointmentDate;
                }
                int num = maxValue.CompareTo(appointmentDate);
                if (num != 0)
                {
                    return num;
                }
                return y.statusDate.CompareTo(x.statusDate);
            }
        }

        private class JobFilter : IObjectFilter<JobData>
        {
            private string q;

            public JobFilter(string query)
            {
                this.q = query;
            }

            public bool Include(JobData obj, object[] additional)
            {
                return ((obj.jobNo.IndexOf(this.q) == 0));
            }
        }

        // BDE exposed
        public object GetSystemParameter(string p)
        {
            return GetSystemParameters(new string[] { p })[0];
        }


        // BDE exposed
        public JobItemSetting[] getAllJobItems()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<JobItemSetting>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public JobItemSetting getJobItem(string itemCode)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                JobItemSetting[] ret = dspReader.GetSTRArrayByFilter<JobItemSetting>("itemCode='" + itemCode + "'");
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        // BDE exposed
        public void deleteJobItem(int AID, string itemCode)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.DeleteSingleTableRecrod<JobItemSetting>(this.DBName, itemCode);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }

        // BDE exposed
        public void setJobItem(int AID, JobItemSetting setting)
        {
            JobItemSetting test = getJobItem(setting.itemCode);

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    if (test == null)
                    {
                        dspWriter.InsertSingleTableRecord(this.DBName, setting, new string[] { "__AID" }, new object[] { AID });
                    }
                    else
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.JobItemSetting", "itemCode='" + setting.itemCode + "'");
                        dspWriter.UpdateSingleTableRecord(this.DBName, setting, new string[] { "__AID"},new object[]{AID});
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void registerNewCustomer(int AID, JobData job, DateTime date)
        {
            if (job.customerID < 1)
            {
                if (job.newCustomer == null)
                    throw new ServerUserMessage("New customer not set");
                if (job.newCustomer.id == -1)
                {
                    job.newCustomer.status = CustomerStatus.Active;
                    job.newCustomer.statusDate = date;
                    job.customerID=job.newCustomer.id = this.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                    job.newCustomer = this.subscriberBDE.GetSubscriber(job.newCustomer.id);
                    this.WriterHelper.UpdateSingleTableRecord(this.DBName, job);
                }
            }
        }
        public bool contains(IEnumerable<BIZNET.iERP.TransactionItems> list, string anItem)
        {
            foreach (BIZNET.iERP.TransactionItems i in list)
                if (string.Compare(i.Code, anItem, true) == 0)
                    return true;
            return false;
        }
        public BIZNET.iERP.TransactionItems[] expandItemsList(ItemsListItem[] list)
        {
            List<BIZNET.iERP.TransactionItems> items = new List<BIZNET.iERP.TransactionItems>();
            foreach (ItemsListItem ti in list)
            {
                if (ti.isCategory)
                {
                    items.AddRange(bERP.GetItemsInCategoryRecursive(ti.category.ID));
                }
                else
                    if (!contains(items, ti.titem.Code))
                        items.Add(ti.titem);
            }
            return items.ToArray();
        }

        public DocumentTypedReference getJobAccountingTypedDocumentReference(JobData jobData, bool primary)
        {
            DocumentSerialType st = bdeAccounting.getDocumentSerialType("JOB");
            if (st == null)
                throw new ServerUserMessage("Configure JOB document reference type");
            return new DocumentTypedReference(st.id, jobData.jobNo, primary);
        }
        // BDE exposed
        public CashHandoverDocument getLastCashHandoverDocument(int cashAccountID)
        {
            INTAPS.RDBMS.SQLHelper reader = bdeAccounting.GetReaderHelper();
            try
            {
                PaymentCenter ba = bdeSubsriber.GetPaymentCenterByCashAccount(cashAccountID);
                if (ba == null)
                    throw new ServerUserMessage("Invalid payment center ID:" + cashAccountID);
                int[] allDocs = reader.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where tranTicks<={1} and DocumentTypeID={2} order by tranTicks desc", bdeAccounting.DBName, DateTime.Now.Ticks, bdeAccounting.GetDocumentTypeByType(typeof(CashHandoverDocument)).id), 0);
                foreach (int docId in allDocs)
                {
                    CashHandoverDocument bdoc = bdeAccounting.GetAccountDocument(docId, true) as CashHandoverDocument;
                    if (bdoc.data.sourceCashAccountID != cashAccountID)
                        continue;
                    return bdoc;
                }
                return null;
                /*int[] jobIDs = reader.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.JobData where [applicationType]={1} and [status]={2} order by [statusDate] desc", this.DBName, StandardJobTypes.CASH_HANDOVER, StandardJobStatus.FINISHED));
                int typeID=this.getJobTypeIDByHandlerType(typeof(RESCashHandover));
                foreach(int jobID in jobIDs)
                {
                    CashHandoverData d = this.getWorkFlowData(jobID, typeID, 0, true) as CashHandoverData;
                    if (d != null)
                        if (d.sourceCashAccountID == cashAccountID)
                            return bdeAccounting.GetAccountDocument<CashHandoverDocument>(d.handoverDocumentID);
                }
                return null;*/
            }
            finally
            {
                bdeAccounting.ReleaseHelper(reader);
            }
        }

        // BDE exposed
        public PaymentInstrumentItem[] getPaymentCenterPaymentInstruments(int cashAccountID, DateTime from, DateTime to)
        {
            int n;
            AccountTransaction[] tran = bdeAccounting.GetTransactions(new int[] { cashAccountID }, 0, from, to, 0, -1, out n);
            double totalDebit = 0;
            List<PaymentInstrumentItem> ret = new List<PaymentInstrumentItem>();
            PaymentInstrumentItem cashInstrument = new PaymentInstrumentItem();
            cashInstrument.instrumentTypeItemCode = bERP.SysPars.cashInstrumentCode;
            cashInstrument.amount = 0;

            foreach (AccountTransaction t in tran)
            {
                AccountDocument doc = bdeAccounting.GetAccountDocument(t.documentID, true);
                PaymentInstrumentItem[] insts = null;
                if (doc is CustomerPaymentReceipt)
                {
                    CustomerPaymentReceipt custr = (CustomerPaymentReceipt)doc;
                    insts = ArrayExtension.mergeArray(insts, custr.paymentInstruments);
                }
                else if (doc is CustomerPaymentReceiptSummaryDocument)
                {
                    if (doc is CustomerPaymentReceiptSummaryDocument)
                    {
                        CustomerPaymentReceiptSummaryDocument sum = (CustomerPaymentReceiptSummaryDocument)doc;
                        foreach (int receiptID in sum.receiptIDs)
                        {
                            CustomerPaymentReceipt custr = (CustomerPaymentReceipt)bdeAccounting.GetAccountDocument(receiptID, true);
                            insts = ArrayExtension.mergeArray(insts, custr.paymentInstruments);
                        }
                    }
                }
                else if (doc is CashHandoverDocument)
                {
                    CashHandoverDocument hdoc = (CashHandoverDocument)doc;
                    if (hdoc.data.sourceCashAccountID == cashAccountID)
                        continue;
                    if (hdoc.data.destinationAccountID != cashAccountID)
                    {
                        throw new ServerUserMessage("Unexpected cash handover transaction ref:{0} ca:{1}\n{2}",hdoc.PaperRef,cashAccountID,
                        hdoc.ToXML());
                    }
                    insts = hdoc.data.instruments;
                }
                if (insts != null)
                {
                    foreach (PaymentInstrumentItem inst in insts)
                    {
                        if (!inst.instrumentTypeItemCode.Equals(bERP.SysPars.cashInstrumentCode))
                        {
                            ret.Add(inst);
                        }
                        else
                            cashInstrument.amount += inst.amount;
                    }

                }
                totalDebit += t.Amount;
            }
            if (AccountBase.AmountGreater(cashInstrument.amount, 0))
                ret.Add(cashInstrument);
            return ret.ToArray();
        }
        public JobData[] getActiveJobs(int[] types)
        {
            string cr = null;
            if (types != null)
            {
                foreach (int t in types)
                    cr = INTAPS.StringExtensions.AppendOperand(cr, ",", t.ToString());
                if (cr != null)
                    cr = "(applicationType in (" + cr + "))";
            }
            string act = null;
            foreach(int s in StandardJobStatus.getActiveJobStatuses())
                act = INTAPS.StringExtensions.AppendOperand(act, ",",s.ToString());
            if (act != null)
            {
                cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", "(status not in (" + act + "))");
            }
            
            SQLHelper readerHelper = base.GetReaderHelper();
            try
            {
                JobData[] ret= readerHelper.GetSTRArrayByFilter<JobData>(cr);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(readerHelper);
            }                    
        }

        // BDE exposed
        public JobData[] getCustomerJobs(int customerID, bool onlyAcitve)
        {
            SQLHelper reader = base.GetReaderHelper();
            try
            {
                string cr = "customerID=" + customerID;
                if (onlyAcitve)
                {
                    string strStates = null;
                    foreach (int state in StandardJobStatus.getInactiveStates())
                        strStates = INTAPS.StringExtensions.AppendOperand(strStates, ",", state.ToString());
                    cr += " AND status not in (" + strStates + ")";
                }
                JobData[] ret = reader.GetSTRArrayByFilter<JobData>(cr);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }
    }
}
