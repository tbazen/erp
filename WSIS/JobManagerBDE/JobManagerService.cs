
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
namespace INTAPS.WSIS.Job
{

    public class JobManagerService : SessionObjectBase<JobManagerBDE>
    {
        private JobWorker _agent;
        private JobManagerBDE bdeJobManager;

        public JobManagerService(UserSessionData session)
            : base(session, (JobManagerBDE)ApplicationServer.GetBDE("JobManager"))
        {
            this.bdeJobManager = base._bde;
            this._agent = base._bde.GetWorker(session.Permissions.UserName);

        }
        void checkConfigurePremission()
        {
            if (!base.m_Session.Permissions.IsPermited("Root/Job/Configure"))
                throw new AccessDeniedException("You can't change configuration");
        }
        public void AddAppointment(JobAppointment appointment)
        {
            this.BlockNoneAgent();
            this.FixAgent(appointment);
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("AddAppointment Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.AddAppointment(AID, appointment);
                    base.WriteExecuteAudit("AddAppointment  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("AddAppointment  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }

        }

        public int AddJob(JobData job, WorkFlowData data)
        {
            this.BlockNoneAgent();
            job.startDate = job.statusDate = DateTime.Now;

            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("AddJob Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    int ret = base._bde.AddJob(AID, job, this._agent, data);
                    base.WriteExecuteAudit("AddJob  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("AddJob  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }

        }

        private void BlockNoneAgent()
        {
            this.BlockNoneAgent("Sorry, you are not a registered and active customer serivce worker");
        }

        private void BlockNoneAgent(string msg)
        {
            if (this._agent == null || !this._agent.active)
            {
                throw new Exception(msg);
            }
        }

        private void BlockNoneProcessOwner()
        {
            this.BlockNoneAgent();
            foreach (JobWorkerRole role in this._agent.roles)
            {
                if (role == JobWorkerRole.Process_Owner)
                {
                    return;
                }
            }
            throw new Exception("Only process owner can execute this task");
        }

        public void ChangeJobStatus(int jobID, DateTime date, int newStatus, string node)
        {
            this.BlockNoneAgent();
            date = DateTime.Now;
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("ChangeJobStatus Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.ChangeJobStatus(AID, jobID, date, newStatus, node, this._agent);
                    base.WriteExecuteAudit("ChangeJobStatus  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ChangeJobStatus  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void ChangeWorkerStatus(string userID, bool active)
        {
            if (!m_Session.Permissions.IsPermited("root/JOB/ManageWorker"))
                throw new Exception("You are not authorized to manage worker data");

            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("ChangeWorkerStatus Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.ChangeWorkerStatus(AID, userID, active);
                    base.WriteExecuteAudit("ChangeWorkerStatus  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("ChangeWorkerStatus  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void CheckAndFixConfiguration()
        {
            lock (base._bde.WriterHelper)
            {
                int aID = base.WriteExecuteAudit("CheckAndFixConfiguration Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.CheckAndFixConfiguration(aID);
                    base.WriteExecuteAudit("CheckAndFixConfiguration  Success", aID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CheckAndFixConfiguration  Failure", exception.Message, exception.StackTrace, aID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void ClearJobs()
        {
        }

        public void CloseAppointment(int appointmentID)
        {
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("CloseAppointment Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.CloseAppointment(AID, appointmentID);
                    base.WriteExecuteAudit("CloseAppointment  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CloseAppointment  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void CreateJobWorker(JobWorker worker)
        {
            if (!m_Session.Permissions.IsPermited("root/JOB/ManageWorker"))
                throw new Exception("You are not authorized to manage worker data");

            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("CreateJobWorker Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.CreateJobWorker(AID, worker);
                    base.WriteExecuteAudit("CreateJobWorker  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("CreateJobWorker  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteBOM(int bomID)
        {
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeleteBOM Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.DeleteBOM(AID, bomID);
                    base.WriteExecuteAudit("DeleteBOM  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteBOM  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void DeleteWorker(string userName)
        {
            if (!m_Session.Permissions.IsPermited("root/JOB/ManageWorker"))
                throw new Exception("You are not authorized to manage worker data");

            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("DeleteWorker Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.DeleteWorker(AID, userName);
                    if ((this._agent != null) && (userName.ToUpper() == this._agent.userID.ToUpper()))
                    {
                        this._agent = null;
                    }
                    base.WriteExecuteAudit("DeleteWorker  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("DeleteWorker  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        private void FixAgent(AgentActionData data)
        {
            data.agent = base.m_Session.Permissions.UserName;
        }

        private void FixAgent(JobBillOfMaterial bom)
        {
            bom.preparedBy = base.m_Session.Permissions.UserName;
        }

        public void GenerateReceipt(int jobID)
        {
            /*JobReceipt receipt2;
            this.BlockNoneAgent();
            int AID = base.WriteAddAudit("Generate Receipt Attempt", jobID.ToString(), -1);
            lock (base._bde.WriterHelper)
            {
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    JobReceipt receipt = this.bdeJobManager.GenerateReceipt(AID, jobID, this._agent);
                    base.WriteAddAudit("Generate Receipt Success", jobID.ToString(), AID);
                    base._bde.WriterHelper.CommitTransaction();
                    receipt2 = receipt;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Generate Receipt Failure", jobID.ToString(), exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
            return receipt2;
             */
        }

        public List<JobData> GetAllJobs()
        {
            return base._bde.GetAllJobs();
        }

        public JobWorker[] GetAllWorkers()
        {
            return this.bdeJobManager.GetAllWorkers();
        }

        public JobBillOfMaterial GetBillOfMaterial(int jobID)
        {
            return this.bdeJobManager.GetBillOfMaterial(jobID);
        }

        public JobData[] GetFilteredJobs(DateTime date)
        {
            this.BlockNoneAgent();
            return this.bdeJobManager.GetFilteredJobs(date, this._agent);
        }

        public string GetInvoicePreview(JobBillOfMaterial bom)
        {
            return this.bdeJobManager.GetInvoicePreview(bom);
        }

        public JobData GetJob(int jobID)
        {
            return this.bdeJobManager.GetJob(jobID);
        }

        public JobAppointment[] GetJobAppointments(int jobHandle)
        {
            return this.bdeJobManager.GetJobAppointments(jobHandle);
        }

        public JobBillOfMaterial[] GetJobBOM(int jobID)
        {
            return this.bdeJobManager.GetJobBOM(jobID);
        }

        public string GetJobCard(int jobID)
        {
            return this.bdeJobManager.GetJobCard(jobID);
        }

        public JobStatusHistory[] GetJobHistory(int jobID)
        {
            return this.bdeJobManager.GetJobHistory(jobID);
        }

        public JobStatusHistory GetLatestHistory(int jobID)
        {
            return this.bdeJobManager.GetLatestHistory(jobID);
        }


        public JobWorker GetWorker(string userID)
        {
            return this.bdeJobManager.GetWorker(userID);
        }

        public int RegisterSubscription(int jobID, Subscription sub)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteAddAudit("Register Subscription Attempt", jobID.ToString(), -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    int subscriptionID = this.bdeJobManager.RegisterSubscription(AID, jobID, sub);
                    base.WriteAddAudit("Register Subscription Success", jobID.ToString(), AID);
                    base._bde.WriterHelper.CommitTransaction();
                    return subscriptionID;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("Register Subscription Failure", jobID.ToString(), exception.Message + "\n" + exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void RemoveJob(int jobHandle)
        {
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("RemoveJob Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    base._bde.RemoveJob(AID, jobHandle);
                    base.WriteExecuteAudit("RemoveJob  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("RemoveJob  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public JobData[] SearchJob(string query, int index, int pageSize, out int NRecords)
        {
            return this.bdeJobManager.SearchJob(query, index, pageSize, out NRecords);
        }

        public List<JobData> SearchJob(string query, bool byDate, DateTime fromDate, DateTime toDate, int status)
        {
            return this.bdeJobManager.SearchJob(query, byDate, fromDate, toDate, status);
        }

        public int SetBillofMateial(JobBillOfMaterial bom)
        {
            this.BlockNoneAgent();
            this.FixAgent(bom);
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("SetBillofMateial Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    int ret = this.bdeJobManager.SetBillofMateial(AID, bom);
                    base.WriteExecuteAudit("SetBillofMateial  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                    return ret;
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SetBillofMateial  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }

        }

        public void UpdateAppointment(JobAppointment appointment)
        {
            this.BlockNoneAgent();
            this.FixAgent(appointment);
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdateAppointment Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.UpdateAppointment(AID, appointment);
                    base.WriteExecuteAudit("UpdateAppointment  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateAppointment  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateHistoryEntry(JobStatusHistory history)
        {
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdateHistoryEntry Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.UpdateHistoryEntry(AID, history);
                    base.WriteExecuteAudit("UpdateHistoryEntry  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateHistoryEntry  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateJob(JobData _job, WorkFlowData data)
        {
            this.BlockNoneAgent();
            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdateJob Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.UpdateJob(AID, _job, this._agent, data);
                    base.WriteExecuteAudit("UpdateJob  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateJob  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void UpdateJobWorker(JobWorker worker)
        {
            if (!m_Session.Permissions.IsPermited("root/JOB/ManageWorker"))
                throw new Exception("You are not authorized to manage worker data");

            lock (base._bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("UpdateJobWorker Attempt", -1);
                try
                {
                    base._bde.WriterHelper.BeginTransaction();
                    this.bdeJobManager.UpdateJobWorker(AID, worker);
                    if ((this._agent != null) && (worker.userID.ToUpper() == this._agent.userID.ToUpper()))
                    {
                        this._agent = worker;
                    }

                    base.WriteExecuteAudit("UpdateJobWorker  Success", AID);
                    base._bde.WriterHelper.CommitTransaction();
                }
                catch (Exception exception)
                {
                    base._bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("UpdateJobWorker  Failure", exception.Message, exception.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(exception.Message, exception);
                    throw;
                }
                finally
                {
                    base._bde.WriterHelper.CheckTransactionIntegrity();
                }
            }
        }

        public void VoidJobReceipt(int bomID)
        {
            if (!m_Session.Permissions.IsPermited("root/JOB/VoidReceipt"))
                throw new Exception("You are not authorized to void job payments receipts");
            int AID = base.WriteExecuteAudit("VoidJobReceipt Attempt", -1);
            try
            {
                base._bde.WriterHelper.BeginTransaction();
                this.bdeJobManager.VoidJobReceipt(AID, bomID);
                base.WriteExecuteAudit("VoidJobReceipt Success", AID);
                base._bde.WriterHelper.CommitTransaction();
            }
            catch (Exception exception)
            {
                base._bde.WriterHelper.RollBackTransaction();
                base.WriteAudit("VoidJobReceipt Failure", exception.Message, exception.StackTrace, AID);
                ApplicationServer.EventLoger.LogException(exception.Message, exception);
                throw;
            }
            finally
            {
                base._bde.WriterHelper.CheckTransactionIntegrity();
            }
        }

        public INTAPS.WSIS.Job.JobAppointment GetAppointment(int id)
        {
            return _bde.GetAppointment(id);
        }

        public double getBOMTotal(int bomID)
        {
            return _bde.getBOMTotal(bomID);
        }

        public int getPreviousHistory(int jobID)
        {
            return _bde.getPreviousHistory(jobID);
        }

        public object GetSystemParameter(string p)
        {
            return _bde.GetSystemParameter(p);
        }

        public INTAPS.WSIS.Job.JobItemSetting[] getAllJobItems()
        {
            return _bde.getAllJobItems();
        }

        public INTAPS.WSIS.Job.JobItemSetting getJobItem(string itemCode)
        {
            return _bde.getJobItem(itemCode);
        }

        public void deleteJobItem(string itemCode)
        {
            checkConfigurePremission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("deleteJobItem Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.deleteJobItem(AID, itemCode);
                    base.WriteExecuteAudit("deleteJobItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("deleteJobItem Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void setJobItem(INTAPS.WSIS.Job.JobItemSetting setting)
        {
            checkConfigurePremission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setJobItem Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setJobItem(AID, setting);
                    base.WriteExecuteAudit("setJobItem Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setJobItem Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public int getChangeNo()
        {
            return _bde.getChangeNo();
        }

        public string GetCommandString(int jobID, int status)
        {
            return _bde.GetCommandString(jobID, status);
        }

        public int[] getPossibleNextStatus(System.DateTime date, int jobID)
        {
            return _bde.getPossibleNextStatus(date, jobID);
        }

        public INTAPS.WSIS.Job.WorkFlowData getWorkFlowData(int jobID, int typeID, int key, bool fullData)
        {
            return _bde.getWorkFlowData(jobID, typeID, key, fullData);
        }

        public INTAPS.WSIS.Job.JobTypeInfo getJobType(int typeID)
        {
            return _bde.getJobType(typeID);
        }

        public INTAPS.WSIS.Job.JobStatusType getJobStatus(int status)
        {
            return _bde.getJobStatus(status);
        }

        public void SetSystemParameters(string[] fields, object[] vals)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("SetSystemParameters Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.SetSystemParameters(AID, fields, vals);
                    base.WriteExecuteAudit("SetSystemParameters Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("SetSystemParameters Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void saveConfiguration(int typeID, object config)
        {
            checkConfigurePremission();
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("saveConfiguration Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.saveConfiguration(AID, typeID, config);
                    base.WriteExecuteAudit("saveConfiguration Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("saveConfiguration Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public object getConfiguration(int typeID, System.Type type)
        {
            return _bde.getConfiguration(typeID, type);
        }

        public void setBillofMaterialCustomerReference(INTAPS.WSIS.Job.JobBillOfMaterial bom)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setBillofMaterialCustomerReference Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setBillofMaterialCustomerReference(AID, bom);
                    base.WriteExecuteAudit("setBillofMaterialCustomerReference Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setBillofMaterialCustomerReference Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public void setWorkFlowData(INTAPS.WSIS.Job.WorkFlowData data)
        {
            lock (_bde.WriterHelper)
            {
                int AID = base.WriteExecuteAudit("setWorkFlowData Attempt", -1);
                _bde.WriterHelper.BeginTransaction();
                try
                {
                    _bde.setWorkFlowData(AID, data);
                    base.WriteExecuteAudit("setWorkFlowData Success", AID);
                    _bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    _bde.WriterHelper.RollBackTransaction();
                    base.WriteAudit("setWorkFlowData Filure", ex.Message, ex.StackTrace, AID);
                    ApplicationServer.EventLoger.LogException(ex.Message, ex);
                    throw;
                }
                finally
                {
                    _bde.CheckTransactionIntegrity();
                }
            }
        }

        public bool jobAppliesTo(System.DateTime date, int jobID)
        {
            return _bde.jobAppliesTo(date, jobID, _agent);
        }

        public INTAPS.WSIS.Job.JobTypeInfo[] getAllJobTypes()
        {
            return _bde.getAllJobTypes();
        }

        public INTAPS.WSIS.Job.CashHandoverDocument getLastCashHandoverDocument(int cashAccountID)
        {
            return _bde.getLastCashHandoverDocument(cashAccountID);
        }

        public BIZNET.iERP.PaymentInstrumentItem[] getPaymentCenterPaymentInstruments(int cashAccountID, System.DateTime from, System.DateTime to)
        {
            return _bde.getPaymentCenterPaymentInstruments(cashAccountID, from, to);
        }

        public INTAPS.WSIS.Job.JobData[] getCustomerJobs(int customerID, bool onlyAcitve)
        {
            return _bde.getCustomerJobs(customerID, onlyAcitve);
        }

    }
}

