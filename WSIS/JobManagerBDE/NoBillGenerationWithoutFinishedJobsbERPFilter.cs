﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using BIZNET.iERP.Server;

namespace INTAPS.WSIS.Job
{
    //[BDEChangeFilter(typeof(CustomerBillDocument))]
    class NoBillGenerationWithoutFinishedJobsbERPFilter : iERPChangeFilterBase
    {
        JobManagerBDE _jobBde = null;
        protected JobManagerBDE jobBde
        {
            get
            {
                if (_jobBde == null)
                    _jobBde = (JobManagerBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("JobManager");
                return _jobBde;
            }
        }
        public override bool CanCreate(int AID, object objectData, out string userMessage)
        {
            /*CustomerBillDocument doc = (CustomerBillDocument)objectData;
            JobData[] activJobs=_jobBde.getCustomerJobs(doc.customer.id, true);
            if (activJobs.Length > 0)
            {
                string jobsList = null;
                foreach (JobData j in activJobs)
                {
                    ActiveJobList
                }
            }*/
            userMessage = null;
            return true;
        }
        public bool CanDelete(int AID, Type objectType, out string userMessage, params object[] keyValues)
        {
            userMessage = null;
            return true;
        }

        public bool CanUpdate(int AID, object objectData, out string userMessage)
        {
            return CanCreate(AID, objectData, out userMessage);
        }
    }
}
