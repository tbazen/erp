﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.Accounting;
using BIZNET.iERP;

namespace INTAPS.WSIS.Job
{
    public class CashHandoverDocumentHandler : INTAPS.Accounting.IDocumentServerHandler
    {
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if (_bdeSubsc == null)
                    _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        AccountingBDE _accounting;
        JobManagerBDE _bde;
        JobManagerBDE bde
        {
            get
            {
                if (_bde == null)
                    _bde = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;
                return _bde;
            }
        }
        public CashHandoverDocumentHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }
        public bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, UserSessionData userSession)
        {
            return true;
        }
        void deleteExisting(int AID, int docID, bool forUpdate)
        {
            CashHandoverDocument bill = _accounting.GetAccountDocument(docID, true) as CashHandoverDocument;
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    _accounting.DeleteAccountDocument(AID, docID, forUpdate);
                    _accounting.WriterHelper.CommitTransaction();
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public void DeleteDocument(int AID, int docID)
        {
            deleteExisting(AID, docID, false);
        }

        public string GetHTML(Accounting.AccountDocument _doc)
        {
            CashHandoverDocument doc = (CashHandoverDocument)_doc;
            return doc.BuildHTML();
        }
        void postReceiptSummary(int AID, long ticks, int assetAccountID, params DocumentTypedReference []tref)
        {
            CustomerPaymentReceiptSummaryDocument prev = null;
            int[] allDocs = _accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where tranTicks<={1} and DocumentTypeID={2} order by tranTicks desc", _accounting.DBName, ticks, _accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceiptSummaryDocument)).id), 0);
            foreach (int docId in allDocs)
            {
                CustomerPaymentReceiptSummaryDocument bdoc = _accounting.GetAccountDocument(docId, true) as CustomerPaymentReceiptSummaryDocument;
                if (bdoc.assetAccountID!=assetAccountID)
                    continue;
                prev = bdoc;
                break;
            }
            long lastTicks = -1;
            if (prev != null)
                lastTicks = prev.tranTicks;
            int n;
            AccountBalance bal;
            AccountTransaction[] tran = _accounting.GetTransactions(assetAccountID, 0, true,prev == null ? INTAPS.DateTools.NullDateValue : prev.DocumentDate, new DateTime(ticks)
                ,new int[]{_accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id}
                ,0, -1, out n,out bal);
            TransactionSummerizer summerizer = new TransactionSummerizer(this._accounting, _bdeSubsc.bERP.SysPars.summaryRoots);
            long maxTicks = -1;
            List<int> receiptIDs = new List<int>();
            foreach (AccountTransaction t in tran)
            {
                CustomerPaymentReceipt receipt = _accounting.GetAccountDocument(t.documentID, true) as CustomerPaymentReceipt;
                if (receipt != null)
                {
                    if (!receipt.summerizeTransaction && receipt.billBatchID == -1)
                    {
                        if (maxTicks == -1)
                            maxTicks = receipt.tranTicks;
                        else
                            if (maxTicks < receipt.tranTicks)
                                maxTicks = receipt.tranTicks;
                        AccountTransaction[] trans = _accounting.GetTransactionsOfDocument(t.documentID);
                        foreach (AccountTransaction tt in trans)
                            if(tt.ItemID==0)
                                summerizer.addTransaction(AID, new TransactionOfBatch(tt));
                        if(receiptIDs.Contains(receipt.AccountDocumentID))
                        {
                            throw new ServerUserMessage($"Duplicate receipt ID:{receipt.AccountDocumentID} ref:{receipt.PaperRef} found while summerizing receipts for asset id:{assetAccountID}");
                        }
                        receiptIDs.Add(receipt.AccountDocumentID);
                    }
                }
            }
            if (summerizer.summary.Count > 0)
            {
                CustomerPaymentReceiptSummaryDocument sumDoc = new CustomerPaymentReceiptSummaryDocument()
                {
                    ShortDescription = "Summary on cash hand over",
                    tranTicks = maxTicks,
                    transactions = summerizer.getSummary(),
                    PaperRef = tref[0].reference,
                    assetAccountID = assetAccountID,
                    receiptIDs = receiptIDs.ToArray()
                };
                sumDoc.AccountDocumentID = _accounting.PostGenericDocument(AID, sumDoc);
                _accounting.addDocumentSerials(AID, sumDoc, tref);
            }

        }
        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    CashHandoverDocument handoverDoc = (CashHandoverDocument)_doc;
                    AuditInfo ai = ApplicationServer.SecurityBDE.getAuditInfo(AID);
                    //PaymentCenter center = bdeSubsc.GetPaymentCenter(ai.userName);
                    PaymentCenter sourceCenter = bdeSubsc.GetPaymentCenterByCashAccount(handoverDoc.data.sourceCashAccountID);
                    PaymentCenter destinationPaymentCenter = null;
                    if (handoverDoc.data.bankDespoit)
                    {
                        if (bdeSubsc.bERP.GetBankAccount(handoverDoc.data.destinationAccountID) == null)
                            throw new ServerUserMessage("Invalid bank account ID " + handoverDoc.data.destinationAccountID);
                    }
                    else
                    {
                        if (bdeSubsc.bERP.GetCashAccount(handoverDoc.data.destinationAccountID) == null)
                            throw new ServerUserMessage("Invalid cash account ID " + handoverDoc.data.destinationAccountID);
                        destinationPaymentCenter = bdeSubsc.GetPaymentCenterByCashAccount(handoverDoc.data.destinationAccountID);
                    }
                    if (handoverDoc.data.sourceCashAccountID == handoverDoc.data.destinationAccountID)
                        throw new ServerUserMessage("It is not allowed to register cash handover between same accounts");
                    if (handoverDoc.AccountDocumentID != -1)
                        deleteExisting(AID, handoverDoc.AccountDocumentID, true);
                    //if (handoverDoc.data.sourceCashAccountID != center.casherAccountID)
                    //    throw new ServerUserMessage("You must login with the account that made the sale");

                    DocumentTypedReference tref = bde.getJobAccountingTypedDocumentReference(bde.GetJob(handoverDoc.data.jobID), true);
                    

                    if (string.IsNullOrEmpty(handoverDoc.data.receiptNumber))
                    {
                        string rtype = handoverDoc.data.bankDespoit ? "BD" : "CH";
                        DocumentSerialType st = _accounting.getDocumentSerialType(rtype);
                        if (st == null)
                            throw new ServerUserMessage("Configure "+rtype+" document reference type");
                        handoverDoc.voucher = _accounting.getNextTypedReference(st.id, _doc.DocumentDate);

                    }
                    else
                    {
                        DocumentSerialType st = _accounting.getDocumentSerialType("CRV");
                        if (st == null)
                            throw new ServerUserMessage("Configure CRV document reference type");
                        handoverDoc.voucher = new DocumentTypedReference(st.id, handoverDoc.data.receiptNumber, true);
                    }
                    handoverDoc.PaperRef = handoverDoc.voucher.reference;
                    
                    postReceiptSummary(AID, _doc.tranTicks, sourceCenter.casherAccountID, handoverDoc.voucher, tref);

                    CashHandoverDocument prev = null;
                    int[] allDocs = _accounting.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where tranTicks<={1} and DocumentTypeID={2} order by tranTicks desc", _accounting.DBName, handoverDoc.tranTicks, _accounting.GetDocumentTypeByType(typeof(CashHandoverDocument)).id), 0);
                    foreach (int docId in allDocs)
                    {
                        CashHandoverDocument bdoc = _accounting.GetAccountDocument(docId, true) as CashHandoverDocument;
                        if (bdoc.data.sourceCashAccountID != handoverDoc.data.sourceCashAccountID)
                            continue;
                        prev = bdoc;
                        break;
                    }

                    if (prev != null)
                    {
                        //if (prev.DocumentDate>= handoverDoc.DocumentDate) BETSU NOTE: I've changed this line with the below code since this validation always fails because of time difference
                        if (!"true".Equals(System.Configuration.ConfigurationManager.AppSettings["chAllowMultiple"]))
                        {
                            if (prev.DocumentDate.Date >= handoverDoc.DocumentDate.Date)
                                throw new ServerUserMessage("Only one cash handover is allowed per casheir");
                        }
                        handoverDoc.previousHandoverDocumentID = prev.AccountDocumentID;
                    }
                    else
                        handoverDoc.previousHandoverDocumentID = -1;

                    
                    Dictionary<string, int> noneCashInstruments = new Dictionary<string, int>();
                    
                    int n;
                    AccountTransaction[] tran = _accounting.GetTransactions(new int[] { handoverDoc.data.sourceCashAccountID }, 0, prev == null ? INTAPS.DateTools.NullDateValue : prev.DocumentDate, handoverDoc.DocumentDate, 0, -1, out n);
                    double totalDebit = 0;                    
                    
                    foreach (AccountTransaction t in tran)
                    {
                        AccountDocument doc = _accounting.GetAccountDocument(t.documentID, true);
                        PaymentInstrumentItem[] insts = null;
                        int delta = 0;
                        if (doc is CustomerPaymentReceiptSummaryDocument)
                        {
                            CustomerPaymentReceiptSummaryDocument sum = (CustomerPaymentReceiptSummaryDocument)doc;
                            foreach (int receiptID in sum.receiptIDs)
                            {
                                CustomerPaymentReceipt custr = (CustomerPaymentReceipt)_accounting.GetAccountDocument(receiptID, true);
                                insts = ArrayExtension.mergeArray(insts, custr.paymentInstruments);
                                delta = 1;
                            }
                        }
                        else if (doc is CashHandoverDocument)
                        {
                            CashHandoverDocument hdoc = (CashHandoverDocument)doc;
                            if (hdoc.data.sourceCashAccountID == handoverDoc.data.sourceCashAccountID)
                                continue;
                            if (hdoc.data.destinationAccountID != handoverDoc.data.sourceCashAccountID)
                                throw new ServerUserMessage("Unexpected transaction ref:{0}. Source {1} Destionation {2}", hdoc.PaperRef, handoverDoc.data.sourceCashAccountID, hdoc.data.destinationAccountID);
                            insts = hdoc.data.instruments;
                            delta = 1;
                        }
                        else if (doc is ReverseCustomerPaymentReceiptDocument)
                        {
                            ReverseCustomerPaymentReceiptDocument rev = (ReverseCustomerPaymentReceiptDocument)doc;
                            CustomerPaymentReceipt revRec = _accounting.GetAccountDocument<CustomerPaymentReceipt>(rev.receiptID);
                            insts = revRec.paymentInstruments;
                            delta = -1;
                        }
                        else
                        {
                            if (!"true".Equals(System.Configuration.ConfigurationManager.AppSettings["chAllowInvalidDoc"]))
                                throw new ServerUserMessage("Unexpected transaction type: " + doc.GetType());
                            continue;
                            
                        }
                        if (insts != null)
                        {
                            foreach (PaymentInstrumentItem inst in insts)
                            {
                                if (!inst.instrumentTypeItemCode.Equals(bdeSubsc.bERP.SysPars.cashInstrumentCode))
                                {
                                    if (noneCashInstruments.ContainsKey(inst.instrumentTypeItemCode))
                                        noneCashInstruments[inst.instrumentTypeItemCode]+=delta;
                                    else
                                        noneCashInstruments.Add(inst.instrumentTypeItemCode, delta);
                                }
                            }
                        }
                        totalDebit += t.Amount;
                    }
                    List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                    foreach (KeyValuePair<string, int> kv in noneCashInstruments)
                    {
                        int count = 0;
                        foreach (PaymentInstrumentItem item in handoverDoc.data.instruments)
                        {
                            if (item.instrumentTypeItemCode.Equals(kv.Key))
                                count++;
                        }
                        if (count > kv.Value)
                            throw new ServerUserMessage(string.Format("Only {0} {1}s are avialable", kv.Value, bdeSubsc.bERP.getPaymentInstrumentType(kv.Key).name));
                        if (count < kv.Value)
                        {
                            TransactionItems titem = _bdeSubsc.bERP.GetTransactionItems(kv.Key);
                            trans.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, sourceCenter.storeCostCenterID, titem.inventoryAccountID).id, TransactionItem.MATERIAL_QUANTITY, -(kv.Value - count), "Payment Instrument Difference"));
                            trans.Add(new TransactionOfBatch(
                                    _accounting.GetOrCreateCostCenterAccount(AID, _bdeSubsc.bERP.SysPars.mainCostCenterID, _bdeSubsc.bERP.SysPars.materialInFlowAccountID).id
                                    , TransactionItem.MATERIAL_QUANTITY, (kv.Value - count), "Payment Instrument Difference"));
                        }
                    }

                    
                    double totalInstruments = 0;
                    foreach (BIZNET.iERP.PaymentInstrumentItem item in handoverDoc.data.instruments)
                    {
                        if (item.despositToBankAccountID != -1)
                        {
                            continue;
                        }
                        totalInstruments += item.amount;
                        if (item.instrumentTypeItemCode.Equals(_bdeSubsc.bERP.SysPars.cashInstrumentCode))
                            continue;
                        TransactionItems titem = _bdeSubsc.bERP.GetTransactionItems(item.instrumentTypeItemCode);
                        if (titem == null)
                            throw new ServerUserMessage("Transaction item code is not valid for payment instrument");
                        if (!titem.IsInventoryItem)
                            throw new ServerUserMessage("Transaction item set for payment instrument " + item.instrumentTypeItemCode + " is not inventory item");
                        trans.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, sourceCenter.storeCostCenterID, titem.inventoryAccountID).id, TransactionItem.MATERIAL_QUANTITY, -1, ""));
                        if (destinationPaymentCenter == null)
                        {
                            trans.Add(new TransactionOfBatch(
                                    _accounting.GetOrCreateCostCenterAccount(AID, _bdeSubsc.bERP.SysPars.mainCostCenterID, _bdeSubsc.bERP.SysPars.materialInFlowAccountID).id
                                    , TransactionItem.MATERIAL_QUANTITY, 1, ""));
                        }
                        else
                        {
                            trans.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, destinationPaymentCenter.storeCostCenterID, titem.inventoryAccountID).id, TransactionItem.MATERIAL_QUANTITY, 1, ""));
                        }                        
                    }
                    handoverDoc.data.delta = totalInstruments - totalDebit;

                    trans.Add(new TransactionOfBatch(sourceCenter.summaryAccountID, -totalInstruments, "Cash handed out"));
                    trans.Add(new TransactionOfBatch(handoverDoc.data.destinationAccountID, totalInstruments, "Cash received from other cashiers on cash handover"));
                    
                    handoverDoc.AccountDocumentID = _accounting.RecordTransaction(AID,
                        handoverDoc, trans.ToArray(), handoverDoc.voucher, tref);


                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(handoverDoc.AccountDocumentID, handoverDoc));

                    _accounting.WriterHelper.CommitTransaction();
                    return handoverDoc.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public void ReverseDocument(int AID, int docID)
        {
            throw new NotImplementedException();
        }
    }
}
