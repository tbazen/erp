using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using BIZNET.iERP;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job
{
    public class RESBase
    {
        BIZNET.iERP.Server.iERPTransactionBDE _berpBDE = null;
        protected BIZNET.iERP.Server.iERPTransactionBDE berpBDE 
        {
            get
            {
                if (_berpBDE == null)
                    _berpBDE = (BIZNET.iERP.Server.iERPTransactionBDE)INTAPS.ClientServer.ApplicationServer.GetBDE("iERP");
                return _berpBDE;
            }
        }
        protected JobManagerBDE bde;
        public RESBase(JobManagerBDE bde)
        {
            this.bde = bde;
        }
        public int getTypeID()
        {
            return bde.getJobTypeIDByHandlerType(this.GetType());
        }
        public virtual string getCommandString(int status)
        {
            JobStatusType js = bde.getJobStatus(status);
            return js.commandName;
        }
        public void checkPayment(int jobID)
        {
            JobBillOfMaterial[] jobBOM;
            checkPayment(jobID, out jobBOM);
        }
        public void checkPayment(int jobID, out JobBillOfMaterial[] jobBOM)
        {
            bool hasPayment = true;
            jobBOM = bde.GetJobBOM(jobID);
            foreach (JobBillOfMaterial material in jobBOM)
            {
                if (!bde.subscriberBDE.isBillPaid(material.invoiceNo))
                {
                    hasPayment = false;
                    break;
                }
            }
            if (!(hasPayment && (jobBOM.Length != 0)))
            {
                throw new Exception("Payment should be made first.");
            }
        }
        protected bool hasApplicationPaymentItems(int jobID)
        {
            JobBillOfMaterial[] boms = bde.GetJobBOM(jobID);
            if (boms.Length == 0)
                return false;
            JobBOMItem item = boms[0].findItem(bde.SysPars.servicePaymentItem);
            if (item == null)
                return false;
            return true;
        }
        protected bool hasPaymentItems(int jobID,string itemCode)
        {
            JobBillOfMaterial[] boms = bde.GetJobBOM(jobID);
            if (boms.Length == 0)
                return false;
            JobBOMItem item = boms[0].findItem(itemCode);
            if (item == null)
                return false;
            return true;
        }
        public List<JobBOMItem> getMeterItems(JobBillOfMaterial[] bom)
        {
            List<JobBOMItem> ret = new List<JobBOMItem>();
            int meterCatCode = bde.subscriberBDE.SysPars.meterMaterialCategory;
            foreach(JobBillOfMaterial b in bom)
                foreach (JobBOMItem i in b.items)
                {
                    TransactionItems item = berpBDE.GetTransactionItems(i.itemID);
                    if (item == null)
                        continue;
                    if (item.categoryID == meterCatCode)
                        ret.Add(i);
                }
            return ret;
        }
        public void verifyOneMeter(JobBillOfMaterial[] bom)
        {
            List<JobBOMItem> meters=getMeterItems(bom);
            if(meters.Count==0)
                throw new ServerUserMessage("Water meter is not provided in the material list");
            if(meters.Count>1)
                throw new ServerUserMessage("Only one meter should be provided in the material list");
            JobBOMItem item=meters[0];
            if(!AccountBase.AmountEqual(item.quantity,1))
                throw new ServerUserMessage("Only one meter should be provided in the material list");
        }
        public void verifyNoMeter(JobBillOfMaterial[] bom,string message)
        {
            List<JobBOMItem> meters=getMeterItems(bom);
            if(meters.Count>0)
                throw new ServerUserMessage(message);
        }

        protected virtual void cancelJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            JobBillOfMaterial[] bom = bde.GetJobBOM(job.id);
            lock (bde.WriterHelper)
            {
                bde.WriterHelper.BeginTransaction();
                try
                {
                    foreach (JobBillOfMaterial b in bom)
                    {
                        if (b.invoiceNo != -1)
                        {
                            bde.deleteBOMInvoice(AID, b.id);
                        }
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }

            }
        }
        
        public virtual bool jobAppliesTo(DateTime date, JobData job, JobWorker worker)
        {
            List<int> list = new List<int>();
            foreach (JobWorkerRole role in worker.roles)
            {
                switch (role)
                {
                    case JobWorkerRole.Receiption:
                        list.Add(StandardJobStatus.APPLICATION);
                        list.Add(StandardJobStatus.CONTRACT);
                        list.Add(StandardJobStatus.FINALIZATION);
                        break;
                    case JobWorkerRole.Analaysis:
                        list.Add(StandardJobStatus.ANALYSIS);
                        break;
                    case JobWorkerRole.Survey:
                        list.Add(StandardJobStatus.SURVEY);
                        break;
                    case JobWorkerRole.Process_Owner:
                        list.Add(StandardJobStatus.APPLICATION_APPROVAL);
                        list.Add(StandardJobStatus.WORK_APPROVAL);
                        break;
                    case JobWorkerRole.Technician:
                        list.Add(StandardJobStatus.TECHNICAL_WORK);
                        break;

                    case JobWorkerRole.Casheir:
                        list.Add(StandardJobStatus.APPLICATION_PAYMENT);
                        list.Add(StandardJobStatus.WORK_PAYMENT);
                        break;
                }
            }
            return list.Contains(job.status) && new List<int>(worker.jobTypes).Contains(job.applicationType);
        }

        public void deleteApplication(int AID, DateTime date, JobData job, JobWorker worker)
        {
            throw new NotImplementedException();
        }

        public virtual JobStatusType[] getAllStatusTypes()
        {
            return new JobStatusType[]{ 
                        StandardJobStatus.Application
                        ,StandardJobStatus.ApplicationApproval
                        ,StandardJobStatus.ApplicationPayment
                        ,StandardJobStatus.Survey
                        ,StandardJobStatus.Analysis
                        ,StandardJobStatus.WorkApproval
                        ,StandardJobStatus.WorkPayment
                        ,StandardJobStatus.Contract
                        ,StandardJobStatus.TechnicalWork
                        ,StandardJobStatus.Finalization
                        ,StandardJobStatus.Canceled
                        ,StandardJobStatus.Finished
            };
        }
        public virtual bool selfPayment { get { return false; } }
        public virtual int doPayment(int AID, DateTime date, JobData job, int bomID, JobWorker worker) { return -1; }

        public virtual void analyzeBillOfQuantity(JobData job,JobBillOfMaterial bom, out List<SummaryItem> summary, out List<MaterialItem> materials, out List<ServiceItem> services)
        {
            throw new NotImplementedException();
        }
        public virtual string generateInvoiceHTML(JobBillDocument document)
        {
            throw new NotImplementedException();
        }
       
        public virtual JobBillDocument generateInvoice(int AID,Subscriber customer, JobData job, JobBillOfMaterial billOfMaterialt)
        {
            throw new NotImplementedException();
        }
        public virtual Type[] getDataExtraTypes() { return new Type[0]; }
        public Type[] getAllCustomerBillDocumentTypes()
        {
            List<Type> ret = new List<Type>();
            foreach (DocumentType dt in bde.BdeAccounting.GetAllDocumentTypes())
            {
                Type t = dt.GetTypeObject();
                if (t!=null && t.IsSubclassOf(typeof(CustomerBillDocument)))
                {
                    ret.Add(t);
                }
            }
            return ret.ToArray();
        }
        public virtual void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            if (nextState == StandardJobStatus.CANCELED)
                cancelJob(AID, date, worker, job, note);
        }
        public virtual void onBOMSet(int AID, JobData job, JobBillOfMaterial bom)
        {
        }
    }
}
