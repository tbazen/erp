using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.BATCH_DISCONNECT, "Disconnect Connections")]
    public class RESBatchDisconnect : RESBase, IJobRuleServerHandler
    {
        public RESBatchDisconnect(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            BatchDisconnectData newLine = data as BatchDisconnectData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            BatchDisconnectData newLine = data as BatchDisconnectData;
            addApplicationData(AID, date, job, worker, data);
        }
        void validateData(BatchDisconnectData data)
        {
            INTAPS.RDBMS.SQLHelper dspReader = bde.subscriberBDE.GetReaderHelper();
            try
            {
                if (data.connections.Length== 0)
                    throw new ServerUserMessage("No connection data provided");
                foreach (VersionedID con in data.connections)
                {
                    Subscription test = bde.subscriberBDE.GetSubscription(con.id, con.version);
                    if (test == null)
                        throw new ServerUserMessage("Connection id " + con+ " no longer exists.");
                    if(test.subscriptionStatus!=SubscriptionStatus.Active)
                        throw new ServerUserMessage("Connection " + con+ " is not active.");
                }
            }
            finally
            {
                bde.subscriberBDE.ReleaseHelper(dspReader);
            }

        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            BatchDisconnectData wfdata = data as BatchDisconnectData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            validateData(wfdata);

            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINISHED};
                default:
                    return new int[0];
            }
        }


        BatchDisconnectData checkAndGetData(JobData job)
        {
            BatchDisconnectData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as BatchDisconnectData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            lock (bde.WriterHelper)
            {
                bde.subscriberBDE.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    BatchDisconnectData wfdata = checkAndGetData(job);
                    validateData(wfdata);

                    foreach (VersionedID con in wfdata.connections)
                    {
                        bde.subscriberBDE.ChangeSubcriptionStatus(AID, con.id, date,
                            wfdata.discontinue?SubscriptionStatus.Discontinued:
                            SubscriptionStatus.Diconnected
                            ,false);
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    bde.subscriberBDE.WriterHelper.restoreReadDB();
                }
            }
        }

    }

    
}
