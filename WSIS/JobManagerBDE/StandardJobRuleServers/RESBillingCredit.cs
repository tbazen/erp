using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.BILLING_CERDIT, "Billing Credit")]
    public class RESBillingCredit : RESBase, IJobRuleServerHandler
    {
        public RESBillingCredit(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            BillCreditData newLine = data as BillCreditData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            BillCreditData newLine = data as BillCreditData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            BillCreditData wfdata = data as BillCreditData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscriber customer = bde.subscriberBDE.GetSubscriber(job.customerID);
            if (customer == null)
                throw new ServerUserMessage("Invalid customer ID");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        BillCreditData checkAndGetData(JobData job)
        {
            BillCreditData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as BillCreditData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            BillCreditData wfdata = checkAndGetData(job);
            lock (bde.WriterHelper)
            {
                bde.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    int i = 0;
                    int[] docIDs = new int[wfdata.creditedBills.Length];
                    Subscriber customer = bde.getJobCustomer(job);
                    foreach (CustomerBillDocument bill in wfdata.creditedBills)
                    {
                        docIDs[i] = bill.AccountDocumentID;
                        i++;
                    }
                    wfdata.schedule.issueDate = date;
                    wfdata.schedule.customerID = bde.getJobCustomer(job).id;
                    if(docIDs.Length==0)
                        bde.subscriberBDE.addNoneBillCreditScheme(AID, wfdata.schedule);
                    else
                        bde.subscriberBDE.differePayment(AID, docIDs, wfdata.schedule);

                    BillPeriod startPayPeriod = bde.subscriberBDE.GetBillPeriod(wfdata.schedule.startPeriodID);
                    BillPeriod currentPeriod = bde.subscriberBDE.GetBillPeriod(bde.subscriberBDE.SysPars.currentPeriod);
                    if (currentPeriod.id == startPayPeriod.id)
                    {
                        CustomerBillDocument[] docs;
                        CustomerBillRecord[] bills;
                        BillItem[][] items;
                        string msg;
                        bool res = bde.subscriberBDE.generateMonthlyBill(date, currentPeriod, customer, out docs, out bills, out items, out msg);
                        if (!res)
                            throw new ServerUserMessage(msg);
                        for (int j = 0; j < bills.Length; j++)
                        {
                            if (docs[j] is CreditBillDocument)
                            {
                                docs[j].draft = false;
                                bills[j].draft = false;
                                docs[j].AccountDocumentID=bde.subscriberBDE.addBill(AID, docs[j], bills[j], items[j]);
                                bde.BdeAccounting.addDocumentSerials(AID,docs[j],bde.getJobAccountingTypedDocumentReference(job,false)); 
                            }
                        }
                    }
                    bde.WriterHelper.CommitTransaction();

                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    bde.WriterHelper.restoreReadDB();
                    throw;
                }
            }
        }






        public void analyzeBillOfQuantity(int bomID, out List<SummaryItem> summary, out List<MaterialItem> materials, out List<ServiceItem> services, out double totalSummary)
        {
            throw new NotImplementedException();
        }
        public override Type[] getDataExtraTypes()
        {
            return getAllCustomerBillDocumentTypes();
        }

    }
}
