using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.BILLING_DEPOSIT, "Billing Desposit")]
    public class RESBillingDesposit : RESBase, IJobRuleServerHandler
    {
        public RESBillingDesposit(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            BillDepositData newLine = data as BillDepositData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            BillDepositData newLine = data as BillDepositData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            BillDepositData wfdata = data as BillDepositData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscriber customer = bde.subscriberBDE.GetSubscriber(job.customerID);
            if (customer == null)
                throw new ServerUserMessage("Invalid customer ID");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (nextState)
            {
                case StandardJobStatus.APPLICATION:
                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        bde.DeleteBOM(AID, bom.id);
                    }
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                    BillDepositData wfdata = checkAndGetData(job);
                    BIZNET.iERP.TransactionItems depItem = bde.bERP.GetTransactionItems(bde.SysPars.depositPaymentItem);
                    if (depItem == null)
                        throw new ServerUserMessage("Configure billing deposit payment item");
                    JobBOMItem item = new JobBOMItem();
                    item.itemID = bde.SysPars.depositPaymentItem;
                    item.quantity = 1;
                    item.unitPrice = wfdata.deposit;
                    item.inSourced = true;
                    item.referenceUnitPrice = 0;
                    item.description = "Billing Deposit";
                    JobBillOfMaterial depositbom = new JobBillOfMaterial();
                    depositbom.items = new JobBOMItem[] { item };
                    depositbom.jobID = job.id;
                    depositbom.analysisTime = date;
                    depositbom.invoiceNo = -1;
                    depositbom.note = "Billing Deposit";
                    depositbom.preparedBy = worker.userID;
                    depositbom.surveyCollectionTime = date;
                    depositbom.id = bde.SetBillofMateial(AID, depositbom);
                    bde.generateInvoice(AID,depositbom.id, worker);
                    break;
            }
        }
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            BillDepositData wfdata = checkAndGetData(job);
            JobBillDocument bill = new JobBillDocument();
            bill.bom = billOfMaterial;
            bill.customer = bde.getJobCustomer(job);
            bill.draft = false;
            BillItem i = new BillItem();
            i.itemTypeID = 0;
            i.incomeAccountID = -1;
            i.description = billOfMaterial.items[0].description;
            i.hasUnitPrice = false;
            i.price = wfdata.deposit;
            i.accounting = BillItemAccounting.Advance;
            bill.jobItems = new BillItem[] { i };
            bill.ShortDescription = "Deposit from customer " + bill.customer.customerCode;
            return bill;
        }

        BillDepositData checkAndGetData(JobData job)
        {
            BillDepositData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as BillDepositData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            checkPayment(job.id);
        }
        public override bool selfPayment
        {
            get
            {
                return true;
            }
        }
        public override int doPayment(int AID, DateTime date,JobData job, int bomID,JobWorker worker)
        {
            throw new ServerUserMessage("Not upgraded");

            //BillDepositData wfdata = checkAndGetData(job);
            //lock (bde.WriterHelper)
            //{
            //    try
            //    {
            //        bde.WriterHelper.BeginTransaction();
            //        wfdata.deposit.DocumentDate = date;
            //        PaymentCenter pc= bde.subscriberBDE.GetPaymentCenter(worker.userID);
            //        wfdata.deposit.cashAccount = pc.casherAccountID;
            //        SubscriptionDeposit ret=bde.subscriberBDE.RegisterDiposit(AID, wfdata.connectionID, wfdata.deposit);
            //        bde.Wri                   terHelper.CommitTransaction();
            //        return ret.AccountDocumentID;
            //    }
            //    catch
            //    {
            //        bde.WriterHelper.RollBackTransaction();
            //        throw;
            //    }
            //}

        }
        public override string generateInvoiceHTML(JobBillDocument document)
        {
            
            JobData job = document.job; ;
            Subscriber customer = document.customer;
            INTAPS.Evaluator.EData jobNo = new Evaluator.EData(Evaluator.DataType.Text, job.jobNo);
            INTAPS.Evaluator.EData appDate = new Evaluator.EData(Evaluator.DataType.Text, job.startDate.ToString("MMM dd,yyyy"));
            INTAPS.Evaluator.EData customerName = new Evaluator.EData(Evaluator.DataType.Text, customer.name);
            INTAPS.Evaluator.EData custCode = new Evaluator.EData(Evaluator.DataType.Text, customer.customerCode);
            INTAPS.Evaluator.EData kebele = new Evaluator.EData(Evaluator.DataType.Text, document.customer.name);
            INTAPS.Evaluator.EData houseNo = new Evaluator.EData(Evaluator.DataType.Text, customer.address);
            INTAPS.Evaluator.EData phoneNo = new Evaluator.EData(Evaluator.DataType.Text, customer.phoneNo);
            INTAPS.Evaluator.EData customerType = new Evaluator.EData(Evaluator.DataType.Text, Subscriber.EnumToName(customer.subscriberType));
            INTAPS.Payroll.Employee prepareEmployee = bde.BdePayroll.GetEmployeeByLoginName(document.bom.preparedBy);
            INTAPS.Evaluator.EData approvedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData approvedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData preparedBy = new Evaluator.EData(Evaluator.DataType.Text, prepareEmployee == null ? "" : prepareEmployee.employeeName);
            INTAPS.Evaluator.EData preparedByDate = new Evaluator.EData(Evaluator.DataType.Text, document.bom.analysisTime.ToString("MMM dd,yyyy"));


            INTAPS.Evaluator.ListData list;
            double deposit = document.jobItems[0].price;
            INTAPS.Evaluator.EData materialList = new Evaluator.EData(Evaluator.DataType.ListData, new Evaluator.ListData(0));

            list = new Evaluator.ListData(0);
            BIZNET.iERP.TransactionItems itemDeposit=bde.bERP.GetTransactionItems(bde.SysPars.depositPaymentItem);
            INTAPS.Evaluator.EData labourList = new Evaluator.EData(Evaluator.DataType.ListData, list);


            list = new Evaluator.ListData(1);
            
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(3);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, "Desposit Amount");
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(deposit));
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, 0);
                list[0] = new Evaluator.EData(Evaluator.DataType.ListData, row);
            INTAPS.Evaluator.EData feeSummaryData = new Evaluator.EData(Evaluator.DataType.ListData, list);



            INTAPS.Evaluator.EData totalMaterial = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData totalMaterialFee = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData totalLabour = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData totalSummary = new Evaluator.EData(Evaluator.DataType.Text, deposit.ToString("#,#00.00"));
            string htmlHeaders;
            string ret = bde.BdeAccounting.EvaluateEHTML(bde.SysPars.jobInvoiceReportID, new object[]{
                 "jobNo",jobNo
                ,"appDate",appDate
                ,"customerName",customerName
                ,"custCode",custCode
                ,"kebele",kebele
                ,"houseNo",houseNo
                ,"phoneNo",phoneNo
                ,"customerType",customerType
                ,"feeSummaryData",feeSummaryData
                ,"materialList",materialList
                ,"labourList",labourList
                ,"approvedBy",approvedBy
                ,"approvedByDate",approvedByDate
                ,"checkedBy",checkedBy
                ,"checkedByDate",checkedByDate
                ,"preparedBy",preparedBy
                ,"preparedByDate",preparedByDate
                ,"totalSummary",totalSummary
                ,"totalMaterial",totalMaterial
                ,"totalMaterialFee",totalMaterialFee
                ,"totalLabour",totalLabour
                }
                , out htmlHeaders);
            return ret;
        }
    }


    
}
