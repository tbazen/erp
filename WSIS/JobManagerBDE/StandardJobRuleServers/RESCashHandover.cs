using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.CASH_HANDOVER, "Cash Handover")]
    public class RESCashHandover : RESBase, IJobRuleServerHandler
    {
        public RESCashHandover(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            CashHandoverData newLine = data as CashHandoverData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            CashHandoverData newLine = data as CashHandoverData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            CashHandoverData wfdata = data as CashHandoverData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            if (!wfdata.bankDespoit)
            {
                PaymentCenter pc = bde.subscriberBDE.GetPaymentCenter(worker.userID);
                if (pc == null)
                    throw new ServerUserMessage("You must be a cashier to execute this transaction");
                wfdata.destinationAccountID = pc.summaryAccountID;
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        CashHandoverData checkAndGetData(JobData job)
        {
            CashHandoverData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as CashHandoverData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            CashHandoverData wfdata = checkAndGetData(job);
            
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    CashHandoverDocument handoverDocument = new CashHandoverDocument();
                    handoverDocument.data = wfdata;
                    handoverDocument.ShortDescription = "Cash handover";
                    handoverDocument.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, handoverDocument);
                    wfdata.handoverDocumentID = handoverDocument.AccountDocumentID;
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

    }
}
