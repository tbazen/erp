using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.CLOSE_CREDIT_SCHEME, "Close Credit Schemes")]
    public class RESCloseCreditScheme : RESBase, IJobRuleServerHandler
    {
        public RESCloseCreditScheme(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            CloseCreditSchemeData newLine = data as CloseCreditSchemeData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            CloseCreditSchemeData newLine = data as CloseCreditSchemeData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            CloseCreditSchemeData wfdata = data as CloseCreditSchemeData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscriber customer = bde.subscriberBDE.GetSubscriber(job.customerID);
            if (customer == null)
                throw new ServerUserMessage("Invalid customer ID");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        CloseCreditSchemeData checkAndGetData(JobData job)
        {
            CloseCreditSchemeData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as CloseCreditSchemeData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            CloseCreditSchemeData wfdata = checkAndGetData(job);
            lock (bde.WriterHelper)
            {
                bde.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    int i = 0;
                    Subscriber customer = bde.getJobCustomer(job);
                    double[] allBilling;
                    CreditScheme[] allCredits= bde.subscriberBDE.getCustomerCreditSchemes(job.customerID,out allBilling);
                    wfdata.schemes=new CreditScheme[wfdata.schemeIDs.Length];
                    wfdata.billed=new double[wfdata.schemeIDs.Length];
                    for(i=0;i<wfdata.schemeIDs.Length;i++)
                    {
                        int index=-1;
                        for(int j=0;j<allCredits.Length;j++)
                        {
                            if(allCredits[j].id==wfdata.schemeIDs[i])
                            {
                                index=j;
                                break;
                            }
                        }
                        if(index==-1)
                            throw new ServerUserMessage("Invalid scheme ID {0}",wfdata.schemeIDs[i]);
                        wfdata.schemes[i] = allCredits[index];
                        wfdata.billed[i] = allBilling[index];
                        bde.subscriberBDE.closeScheme(AID,wfdata.schemeIDs[i]);
                        i++;
                    }
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();

                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    bde.WriterHelper.restoreReadDB();
                }
                    
            }
        }

        public void analyzeBillOfQuantity(int bomID, out List<SummaryItem> summary, out List<MaterialItem> materials, out List<ServiceItem> services, out double totalSummary)
        {
            throw new NotImplementedException();
        }
    }
}
