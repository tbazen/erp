using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.BATCH_DELETE_BILLS, "Delete Bills")]
    public class RESDeleteBills : RESBase, IJobRuleServerHandler
    {
        public RESDeleteBills(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            DeleteBillsData newLine = data as DeleteBillsData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DeleteBillsData newLine = data as DeleteBillsData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DeleteBillsData wfdata = data as DeleteBillsData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscriber customer = bde.subscriberBDE.GetSubscriber(job.customerID);
            if (customer == null)
                throw new ServerUserMessage("Invalid customer ID");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        DeleteBillsData checkAndGetData(JobData job)
        {
            DeleteBillsData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as DeleteBillsData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            DeleteBillsData wfdata = checkAndGetData(job);
            lock (bde.WriterHelper)
            {
                bde.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    int i = 0;
                    int[] docIDs = new int[wfdata.deletedBills.Length];
                    Subscriber customer = bde.getJobCustomer(job);
                    Array.Sort<CustomerBillDocument>(wfdata.deletedBills,
                        delegate(CustomerBillDocument b1, CustomerBillDocument b2)
                        {
                            int p1 = 1;
                            foreach (DocumentSerial s in bde.BdeAccounting.getDocumentSerials(b1.AccountDocumentID))
                            {
                                if (s.primaryDocument)
                                {
                                    p1 = -1;
                                    break;
                                }
                            }
                            int p2 = 1;
                            foreach (DocumentSerial s in bde.BdeAccounting.getDocumentSerials(b2.AccountDocumentID))
                            {
                                if (s.primaryDocument)
                                {
                                    p2 = -1;
                                    break;
                                }
                            }
                            return p1.CompareTo(p2);
                        }
                );
                    foreach (CustomerBillDocument bill in wfdata.deletedBills)
                    {
                        wfdata.deletedBills[i] = bde.BdeAccounting.GetAccountDocument(bill.AccountDocumentID, true) as CustomerBillDocument;
                        if (wfdata.deletedBills[i] == null)
                            throw new ServerUserMessage("One or more of the bills selected for deletion don't exist anymore");
                        CustomerBillRecord rec = bde.subscriberBDE.getCustomerBillRecord(wfdata.deletedBills[i].AccountDocumentID);
                        if (rec.paymentDocumentID != -1)
                            throw new ServerUserMessage("One or more of the bills selected for deletion are paid. Void the payment first.");
                        if (rec.paymentDiffered)
                            throw new ServerUserMessage("One or more of the bills selected for deletion are used in credit scheme.");
                        bde.subscriberBDE.deleteCustomerBill(AID, rec.id, bde.getJobAccountingTypedDocumentReference(job, true));
                        i++;
                    }
                    bde.WriterHelper.CommitTransaction();

                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    bde.WriterHelper.restoreReadDB();
                }
                    
            }
        }






        public void analyzeBillOfQuantity(int bomID, out List<SummaryItem> summary, out List<MaterialItem> materials, out List<ServiceItem> services, out double totalSummary)
        {
            throw new NotImplementedException();
        }
        public override Type[] getDataExtraTypes()
        {
            return getAllCustomerBillDocumentTypes();
        }
    }
}
