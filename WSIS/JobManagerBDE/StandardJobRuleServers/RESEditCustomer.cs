using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using System.Diagnostics;

namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.EDIT_CUTOMER_DATA, "Edit Customer Data")]
    public class RESEditCustomer : RESBase, IJobRuleServerHandler
    {
        public RESEditCustomer(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            EditCustomerData newLine = data as EditCustomerData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            EditCustomerData newLine = data as EditCustomerData;
            addApplicationData(AID, date, job, worker, data);
        }
        void validateData(EditCustomerData data)
        {
            INTAPS.RDBMS.SQLHelper dspReader = bde.subscriberBDE.GetReaderHelper();
            try
            {
                foreach (DiffObject<Subscription> con in data.connections)
                {
                    switch (con.diffType)
                    {
                        case GenericDiffType.Delete:
                        case GenericDiffType.Replace:
                            long version = dspReader.GetSTRNextVersionDate<Subscription>("id=" + con.data.id, con.data.ticksFrom);
                            if (version > 0)
                                throw new ServerUserMessage("Connection " + con.data.contractNo + " can't be updated because there is already a future version of it the database");
                            break;
                        default:
                            break;
                    }
                }

            }
            finally
            {
                bde.subscriberBDE.ReleaseHelper(dspReader);
            }

        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            EditCustomerData wfdata = data as EditCustomerData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            validateData(wfdata);

            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }

            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);

        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        EditCustomerData checkAndGetData(JobData job)
        {
            EditCustomerData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as EditCustomerData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            lock (bde.WriterHelper)
            {
                bde.subscriberBDE.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    EditCustomerData wfdata = checkAndGetData(job);
                    validateData(wfdata);
                    Subscriber customer = bde.getJobCustomer(job);
                    
                    if (wfdata.customerDiff != null)
                    {
                        Console.WriteLine("Customer diff exists");
                        switch (wfdata.customerDiff.diffType)
                        {
                            case GenericDiffType.Add:
                                wfdata.customerDiff.data.id = bde.subscriberBDE.RegisterSubscriber(AID, wfdata.customerDiff.data);
                                Console.WriteLine("Customer added:"+ wfdata.customerDiff.data.id);
                                job.customerID = wfdata.customerDiff.data.id;
                                break;
                            case GenericDiffType.Delete:
                                wfdata.oldVersion = bde.subscriberBDE.GetSubscriber(wfdata.customerDiff.data.id);
                                bde.subscriberBDE.DeleteSubscriber(AID, wfdata.customerDiff.data.id);
                                job.customerID = wfdata.customerDiff.data.id;
                                break;
                            case GenericDiffType.Replace:
                                wfdata.oldVersion = bde.subscriberBDE.GetSubscriber(wfdata.customerDiff.data.id);
                                wfdata.customerDiff.data.accountID = wfdata.oldVersion.accountID;
                                wfdata.oldVersion.accountID = -1;
                                bde.subscriberBDE.UpdateSubscriber(AID, wfdata.customerDiff.data, null);
                                job.customerID = wfdata.customerDiff.data.id;
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        Console.WriteLine("Customer diff null");
                    wfdata.oldVersions = new Subscription[wfdata.connections.Length];
                    int i = -1;
                    foreach (DiffObject<Subscription> con in wfdata.connections)
                    {
                        if(job.customerID<1)
                            job.customerID = con.data.subscriberID;
                        Console.WriteLine($"Processing connection: difftype:{con.diffType}");
                        i++;
                        switch (con.diffType)
                        {
                            case GenericDiffType.Delete:
                                if (wfdata.customerDiff != null && wfdata.customerDiff.diffType != GenericDiffType.Replace)
                                    throw new ServerUserMessage("BUG: Delete connection operation not valid for a new customer or delete customer operation. ");
                                wfdata.oldVersions[i] = bde.subscriberBDE.GetSubscriptionInternal(bde.WriterHelper, con.data.id, con.data.ticksFrom, false);
                                if (wfdata.oldVersions[i] == null)
                                    throw new ServerUserMessage("Trying to delete a none existent connection ID:" + con.data.id);
                                bde.subscriberBDE.DeleteSubscription(AID, con.data.id);
                                break;
                            case GenericDiffType.Replace:
                                if (wfdata.customerDiff != null)
                                {
                                    if (wfdata.customerDiff.diffType != GenericDiffType.Replace)
                                        throw new ServerUserMessage("BUG: Update connection operation not valid for a new customer or delete customer operation.");
                                    if (con.data.subscriberID != wfdata.customerDiff.data.id)
                                        throw new ServerUserMessage("BUG: Updated connections refer a diffferent customer");
                                }

                                long prevVersion = bde.subscriberBDE.WriterHelper.GetSTRPreviousVersionDate<Subscription>("id=" + con.data.id, con.data.ticksFrom);
                                if (prevVersion < 1)
                                    throw new ServerUserMessage("Trying to update a connection with no previos version ID:" + con.data.id);
                                wfdata.oldVersions[i] = bde.subscriberBDE.GetSubscriptionInternal(bde.WriterHelper, con.data.id, prevVersion, false);
                                if (wfdata.oldVersions[i] == null)
                                    throw new ServerUserMessage("Trying to update a none existent connection ID:" + con.data.id);
                                bde.subscriberBDE.changeSubscription(AID, con.data);
                                break;
                            case GenericDiffType.Add:
                                if (customer==null || con.data.subscriberID == customer.id)
                                {
                                    if (wfdata.customerDiff != null)
                                    {
                                        con.data.subscriberID = wfdata.customerDiff.data.id;
                                        con.data.subscriber = wfdata.customerDiff.data;
                                    }
                                    if (con.data.contractNo == null || string.IsNullOrEmpty(con.data.contractNo.Trim()))
                                    {
                                        con.data.contractNo = bde.GetNextContractNumber(AID, con.data);
                                    }
                                    else
                                        con.data.contractNo = con.data.contractNo.Trim();
                                    con.data.subscriptionStatus = SubscriptionStatus.Active;
                                    con.data.id = bde.subscriberBDE.AddSubscription(AID, con.data);
                                }
                                else
                                {
                                    con.data.subscriberID = customer.id;
                                    con.data.ticksFrom = date.Ticks;
                                    bde.subscriberBDE.changeSubscription(AID, con.data);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    bde.UpdateJob(AID, job, worker, null);
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    bde.subscriberBDE.WriterHelper.restoreReadDB();
                }
            }
        }
    }
}