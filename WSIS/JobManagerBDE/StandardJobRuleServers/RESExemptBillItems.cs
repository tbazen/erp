using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.EXEMPT_BILL_ITEMS, "Exempt Bill Items")]
    public class RESExemptBillItems : RESBase, IJobRuleServerHandler
    {
        public RESExemptBillItems(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            ExemptBillItemsData newLine = data as ExemptBillItemsData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ExemptBillItemsData newLine = data as ExemptBillItemsData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ExemptBillItemsData wfdata = data as ExemptBillItemsData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscriber customer = bde.subscriberBDE.GetSubscriber(job.customerID);
            if (customer == null)
                throw new ServerUserMessage("Invalid customer ID");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            if (wfdata.exemptedItems.Length == 0)
                throw new ServerUserMessage("At least one bill item should be exempted");
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        ExemptBillItemsData checkAndGetData(JobData job)
        {
            ExemptBillItemsData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ExemptBillItemsData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            ExemptBillItemsData wfdata = checkAndGetData(job);
            lock (bde.WriterHelper)
            {
                bde.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    int i = 0;
                    Subscriber customer = bde.getJobCustomer(job);
                    wfdata.exemptedBill = bde.subscriberBDE.getCustomerBillRecord(wfdata.exemptedBill.id);
                    if (wfdata.exemptedBill.isPayedOrDiffered)
                        throw new ServerUserMessage("The bill is settled");
                    CustomerBillDocument doc = bde.BdeAccounting.GetAccountDocument<CustomerBillDocument>(wfdata.exemptedBill.id);
                    wfdata.originalItems = doc.itemsLessExemption.ToArray();
                    for(int k=0;k<wfdata.exemptedItems.Length;k++)
                    {
                        bool found=false;
                        foreach (BillItem bi in wfdata.originalItems)
                        {
                            if (bi.itemTypeID == wfdata.exemptedItems[k])
                            {
                                if (AccountBase.AmountGreater(wfdata.exemptedAmounts[k], bi.price))
                                    throw new ServerUserMessage("It is not allowed to exempt more than the value of the item. Item {0} attempted exemption {1}", bi.description, wfdata.exemptedAmounts[k]);
                                found = true;
                                break;
                            }
                        }
                        if(!found)
                            throw new ServerUserMessage("Invalid bill item {0}", wfdata.exemptedItems[k]);
                    }
                    
                    doc.exemptedItems = ArrayExtension.mergeArray(doc.exemptedItems, wfdata.exemptedItems);
                    doc.exemptedAmounts =ArrayExtension.mergeArray(doc.exemptedAmounts,  wfdata.exemptedAmounts);
                    bde.subscriberBDE.deleteCustomerBill(AID, wfdata.exemptedBill.id,date, bde.getJobAccountingTypedDocumentReference(job, true));
                    
                    doc.AccountDocumentID = doc.billBatchID==-1?doc.AccountDocumentID:-1;
                    doc.billBatchID = -1;
                    doc.summerizeTransaction = true;
                    
                    doc.DocumentDate = date;
                    doc.ShortDescription += " (with exemption)";
                    bde.subscriberBDE.addBill(AID, doc, wfdata.exemptedBill, doc.itemsLessExemption.ToArray());
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();

                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
                finally
                {
                    bde.WriterHelper.restoreReadDB();
                }
                    
            }
        }






        public void analyzeBillOfQuantity(int bomID, out List<SummaryItem> summary, out List<MaterialItem> materials, out List<ServiceItem> services, out double totalSummary)
        {
            throw new NotImplementedException();
        }
        public override Type[] getDataExtraTypes()
        {
            return getAllCustomerBillDocumentTypes();
        }
    }
}
