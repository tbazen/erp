﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Bishoftu.REServer
{
    [JobRuleServerHandler(StandardJobTypes.GENERAL_SELL, "General Paid Service")]
    public class RESGeneralSell :RESBase , IJobRuleServerHandler
    {
        public RESGeneralSell(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override string generateInvoiceHTML(JobBillDocument document)
        {
            JobData job = document.job; ;
            Subscriber customer = document.customer;
            INTAPS.Evaluator.EData jobNo = new Evaluator.EData(Evaluator.DataType.Text, job.jobNo);
            INTAPS.Evaluator.EData appDate = new Evaluator.EData(Evaluator.DataType.Text, job.startDate.ToString("MMM dd,yyyy"));
            INTAPS.Evaluator.EData customerName = new Evaluator.EData(Evaluator.DataType.Text, customer.name);
            INTAPS.Evaluator.EData custCode = new Evaluator.EData(Evaluator.DataType.Text, customer.customerCode);
            INTAPS.Evaluator.EData kebele = new Evaluator.EData(Evaluator.DataType.Text, document.customer.name);
            INTAPS.Evaluator.EData houseNo = new Evaluator.EData(Evaluator.DataType.Text, customer.address);
            INTAPS.Evaluator.EData phoneNo = new Evaluator.EData(Evaluator.DataType.Text, customer.phoneNo);
            INTAPS.Evaluator.EData customerType = new Evaluator.EData(Evaluator.DataType.Text, Subscriber.EnumToName(customer.subscriberType));
            INTAPS.Payroll.Employee prepareEmployee = bde.BdePayroll.GetEmployeeByLoginName(document.bom.preparedBy);
            INTAPS.Evaluator.EData approvedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData approvedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData preparedBy = new Evaluator.EData(Evaluator.DataType.Text, prepareEmployee == null ? "" : prepareEmployee.employeeName);
            INTAPS.Evaluator.EData preparedByDate = new Evaluator.EData(Evaluator.DataType.Text, document.bom.analysisTime.ToString("MMM dd,yyyy"));


            INTAPS.Evaluator.ListData list;
            double totalMaterialValue = 0;
            double totalMaterialFeeValue = 0;
            double totalServiceValue = 0;
            double grandTotal = 0;
            int i;

            list = new Evaluator.ListData(document.materialItems.Length);
            i = 0;
            foreach (MaterialItem mi in document.materialItems)
            {
                double materialValue = mi.materialCost;
                double materialFee = (mi.inSourced ? mi.standardUnitCost : 0) * mi.quantity;
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(11);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, mi.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, mi.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, mi.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, mi.standardUnitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (mi.standardUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[7] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost == -1 ? "" : mi.customerUnitCost.ToString("#,#00.00"));
                row.elements[8] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost == -1 ? "" : (mi.customerUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[9] = new Evaluator.EData(Evaluator.DataType.Text, mi.materialCost);
                row.elements[10] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced ? materialFee.ToString("#,#00.00") : "");
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal += materialFee;
                totalMaterialValue += materialValue;
                totalMaterialFeeValue += materialFee;
                i++;
            }
            INTAPS.Evaluator.EData materialList = new Evaluator.EData(Evaluator.DataType.ListData, list);

            list = new Evaluator.ListData(document.serviceItems.Length);
            i = 0;
            foreach (ServiceItem si in document.serviceItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(7);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, si.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, si.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, si.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, si.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, si.unitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (si.unitCost * si.quantity).ToString("#,#00.00"));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal += si.unitCost * si.quantity;
                totalServiceValue += si.unitCost * si.quantity;
                i++;
            }
            INTAPS.Evaluator.EData labourList = new Evaluator.EData(Evaluator.DataType.ListData, list);


            i = 0;
            list = new Evaluator.ListData(document.summaryItems.Length);
            foreach (SummaryItem si in document.summaryItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(3);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.description);
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.amount));
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.basePrice));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                if (si.add)
                    grandTotal += si.amount;
                i++;
            }
            INTAPS.Evaluator.EData feeSummaryData = new Evaluator.EData(Evaluator.DataType.ListData, list);



            INTAPS.Evaluator.EData totalMaterial = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalMaterialFee = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialFeeValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalLabour = new Evaluator.EData(Evaluator.DataType.Text, totalServiceValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalSummary = new Evaluator.EData(Evaluator.DataType.Text, grandTotal.ToString("#,#00.00"));

            string htmlHeaders;
            var repID = bde.BdeAccounting.GetReportDefID("Job.PerformaInvoice");
            string ret = bde.BdeAccounting.EvaluateEHTML(repID, new object[]{
                 "jobNo",jobNo
                ,"appDate",appDate
                ,"customerName",customerName
                ,"custCode",custCode
                ,"kebele",kebele
                ,"houseNo",houseNo
                ,"phoneNo",phoneNo
                ,"customerType",customerType
                ,"feeSummaryData",feeSummaryData
                ,"materialList",materialList
                ,"labourList",labourList
                ,"approvedBy",approvedBy
                ,"approvedByDate",approvedByDate
                ,"checkedBy",checkedBy
                ,"checkedByDate",checkedByDate
                ,"preparedBy",preparedBy
                ,"preparedByDate",preparedByDate
                ,"totalSummary",totalSummary
                ,"totalMaterial",totalMaterial
                ,"totalMaterialFee",totalMaterialFee
                ,"totalLabour",totalLabour
                }
                , out htmlHeaders
                );
            return ret;
        }
        public virtual JobStatusType[] getAllStatusTypes()
        {
            return new JobStatusType[]{ 
                        StandardJobStatus.Application
                        ,StandardJobStatus.Analysis
                        ,StandardJobStatus.WorkApproval
                        ,StandardJobStatus.WorkPayment
                        ,StandardJobStatus.Canceled
                        ,StandardJobStatus.Finished
            };
        }

        public virtual void analyzeBillOfQuantity(Subscriber customer, JobData job, JobBillOfMaterial bom
    , out List<SummaryItem> summary
    , out List<MaterialItem> materials
    , out List<ServiceItem> services
   )
        {
            SubscriberType customerType = customer.subscriberType;
            summary = new List<SummaryItem>();
            materials = new List<MaterialItem>();
            services = new List<ServiceItem>();

            double totalMaterialFee = 0;
            double totalService = 0;
            double totalMaterialCost = 0;

            foreach (JobBOMItem item in bom.items)
            {
                BIZNET.iERP.TransactionItems titem = bde.bERP.GetTransactionItems(item.itemID);
                if (titem == null)
                    continue;


                BIZNET.iERP.MeasureUnit mu = bde.bERP.GetMeasureUnit(titem.MeasureUnitID);
                string mustr = mu == null ? "" : mu.Name;
                if (titem.GoodOrService == BIZNET.iERP.GoodOrService.Good)
                {
                    string desc;
                    if (string.IsNullOrEmpty(item.description))
                        desc = titem.Name;
                    else
                        desc = item.description;
                    if (!item.inSourced)
                        throw new Exception("Sourcing material from customer for general service is not supported."); 
                    MaterialItem mi = new MaterialItem();
                    mi.itemCode = item.itemID;
                    mi.itemName = desc;
                    mi.orderNo = materials.Count + 1;
                    mi.quantity = item.quantity;
                    mi.standardUnitCost = titem.FixedUnitPrice;
                    mi.customerUnitCost = item.referenceUnitPrice;
                    mi.inSourced = item.inSourced;
                    mi.unit = mustr;
                    mi.salesAccountID = titem.salesAccountID;
                    materials.Add(mi);
                    double materialCost;
                    totalMaterialFee += item.quantity * titem.FixedUnitPrice;
                    materialCost = item.quantity * titem.FixedUnitPrice;
                    mi.materialCost = materialCost;
                    totalMaterialCost += materialCost;
                }
                else
                {

                    string desc;
                    if (string.IsNullOrEmpty(item.description))
                        desc = titem.Name;
                    else
                        desc = item.description;
                    ServiceItem si = new ServiceItem();
                    si.itemCode = titem.Code;
                    si.itemName = desc;
                    si.orderNo = services.Count + 1;
                    si.quantity = item.quantity;
                    si.unitCost = titem.FixedUnitPrice;
                    si.unit = mustr;
                    si.salesAccountID = titem.salesAccountID;
                    services.Add(si);
                    totalService += si.unitCost * si.quantity;
                }
            }
            double grandTotal = 0;
            summary.Add(new SummaryItem("", "Service", totalService, false, -1, 0));
            grandTotal += totalService;

            summary.Add(new SummaryItem("", "Material Fee", totalMaterialFee, false, -1, 0));
            grandTotal += totalMaterialFee;
        }
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            JobBillDocument bill = new JobBillDocument();

            List<SummaryItem> summary;
            List<MaterialItem> materials;
            List<ServiceItem> services;

            analyzeBillOfQuantity(customer, job, billOfMaterial, out summary, out materials, out services);
            bill.summaryItems = summary.ToArray();
            bill.materialItems = materials.ToArray();
            bill.serviceItems = services.ToArray();


            List<BillItem> items = new List<BillItem>();
            foreach (MaterialItem mi in materials)
            {
                if (!mi.inSourced)
                    continue;
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.standardUnitCost;
                i.price = mi.quantity * mi.standardUnitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;

                items.Add(i);
            }
            foreach (ServiceItem mi in services)
            {
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.unitCost;
                i.price = mi.quantity * mi.unitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);

            }
            foreach (SummaryItem mi in summary)
            {
                if (!mi.add)
                    continue;
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.saleAccountID;
                i.description = mi.description;
                i.hasUnitPrice = false;
                i.price = mi.amount;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);
            }
            bill.job = job;
            bill.customer = customer;
            bill.bom = billOfMaterial;
            bill.DocumentDate = DateTime.Now;
            bill.jobItems = items.ToArray();
            bill.ShortDescription = billOfMaterial.note;
            bill.draft = false;
            return bill;
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);
            GeneralSellsData newLine = data as GeneralSellsData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            GeneralSellsData newLine = data as GeneralSellsData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            GeneralSellsData wfData = data as GeneralSellsData;
            if (wfData == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfData.typeID = getTypeID();
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }
        public void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION_APPROVAL:
                    if (nextState != StandardJobStatus.CANCELED)
                    {
                        if (job.newCustomer != null)
                            bde.registerNewCustomer(AID, job, date);
                    }
                    break;
            }
            GeneralSellsData data;
            JobBillOfMaterial[] boms = null;
            switch (nextState)
            {
                case StandardJobStatus.FINALIZATION:
                    checkPayment(job.id);
                    break;
                case StandardJobStatus.ANALYSIS:
                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length != 1)
                        throw new ServerUserMessage("Exactly one bill of material is required to send this job to analysis");
                    foreach (JobBOMItem item in boms[0].items)
                        if (item.referenceUnitPrice != -1)
                            throw new ServerUserMessage("Reference price is not supported for general service");
                    data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as GeneralSellsData;
                    if (data == null)
                    {
                        data = new GeneralSellsData();
                        data.jobID = job.id;
                        data.typeID = getTypeID();
                    }
                    bde.setWorkFlowData(AID, data);
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                case StandardJobStatus.WORK_APPROVAL:
                    bde.verifyAccountingClearance(AID, job.customerID, date, job);
                    if (job.newCustomer != null)
                        bde.registerNewCustomer(AID, job, date);
                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        if (bom.invoiceNo < 1)
                            bde.generateInvoice(AID, bom.id, worker);
                        foreach (JobBOMItem item in bom.items)
                            if (!item.inSourced || item.referenceUnitPrice != -1)
                                throw new ServerUserMessage("Sourcing items from customers not supported for general services");
                    }
                    break;

            }
        }
        GeneralSellsData checkAndGetData(JobData job)
        {
            GeneralSellsData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as GeneralSellsData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            GeneralSellsData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();

                    if (job.customerID < 1)
                    {
                        if (job.newCustomer == null)
                            throw new ServerUserMessage("New customer not set");
                        if (job.newCustomer.id == -1)
                        {
                            job.customerID = job.newCustomer.id = bde.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                            job.newCustomer = bde.subscriberBDE.GetSubscriber(job.newCustomer.id);
                            bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, job);
                        }
                    }
                    bde.setWorkFlowData(AID, data);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
    }
}
