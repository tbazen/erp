using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using BIZNET.iERP;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER, "Ownership Transfer")]
    public class RESOwnershipTransfer : RESBase, IJobRuleServerHandler
    {
        public RESOwnershipTransfer(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            OwnerShipTransferData wfData = data as OwnerShipTransferData;
            Subscription subsc = bde.subscriberBDE.GetSubscription(wfData.connectionID, date.Ticks);
            bde.verifyAccountingClearance(AID, subsc.subscriberID, date, job);
            Subscriber oldCustomer = bde.subscriberBDE.GetSubscriber(subsc.subscriberID);
            wfData.oldCustomer = oldCustomer;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            OwnerShipTransferData newLine = data as OwnerShipTransferData;
            addApplicationData(AID, date, job, worker, data);
        }
        public override string generateInvoiceHTML(JobBillDocument document)
        {
            JobData job = document.job; ;
            Subscriber customer = document.customer;
            INTAPS.Evaluator.EData jobNo = new Evaluator.EData(Evaluator.DataType.Text, job.jobNo);
            INTAPS.Evaluator.EData appDate = new Evaluator.EData(Evaluator.DataType.Text, job.startDate.ToString("MMM dd,yyyy"));
            INTAPS.Evaluator.EData customerName = new Evaluator.EData(Evaluator.DataType.Text, customer.name);
            INTAPS.Evaluator.EData custCode = new Evaluator.EData(Evaluator.DataType.Text, customer.customerCode);
            INTAPS.Evaluator.EData kebele = new Evaluator.EData(Evaluator.DataType.Text, document.customer.name);
            INTAPS.Evaluator.EData houseNo = new Evaluator.EData(Evaluator.DataType.Text, customer.address);
            INTAPS.Evaluator.EData phoneNo = new Evaluator.EData(Evaluator.DataType.Text, customer.phoneNo);
            INTAPS.Evaluator.EData customerType = new Evaluator.EData(Evaluator.DataType.Text, Subscriber.EnumToName(customer.subscriberType));
            INTAPS.Payroll.Employee prepareEmployee = bde.BdePayroll.GetEmployeeByLoginName(document.bom.preparedBy);
            INTAPS.Evaluator.EData approvedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData approvedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData preparedBy = new Evaluator.EData(Evaluator.DataType.Text, prepareEmployee == null ? "" : prepareEmployee.employeeName);
            INTAPS.Evaluator.EData preparedByDate = new Evaluator.EData(Evaluator.DataType.Text, document.bom.analysisTime.ToString("MMM dd,yyyy"));


            INTAPS.Evaluator.ListData list;
            double totalMaterialValue = 0;
            double totalMaterialFeeValue = 0;
            double totalServiceValue = 0;
            double grandTotal = 0;
            int i;

            list = new Evaluator.ListData(document.materialItems.Length);
            i = 0;
            foreach (MaterialItem mi in document.materialItems)
            {
                double materialValue = mi.materialCost;
                double materialFee = (mi.inSourced ? mi.standardUnitCost : 0) * mi.quantity;
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(11);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, mi.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, mi.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, mi.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, mi.standardUnitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (mi.standardUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[7] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost == -1 ? "" : mi.customerUnitCost.ToString("#,#00.00"));
                row.elements[8] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost == -1 ? "" : (mi.customerUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[9] = new Evaluator.EData(Evaluator.DataType.Text, mi.materialCost);
                row.elements[10] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced ? materialFee.ToString("#,#00.00") : "");
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal += materialFee;
                totalMaterialValue += materialValue;
                totalMaterialFeeValue += materialFee;
                i++;
            }
            INTAPS.Evaluator.EData materialList = new Evaluator.EData(Evaluator.DataType.ListData, list);

            list = new Evaluator.ListData(document.serviceItems.Length);
            i = 0;
            foreach (ServiceItem si in document.serviceItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(7);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, si.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, si.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, si.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, si.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, si.unitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (si.unitCost * si.quantity).ToString("#,#00.00"));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal += si.unitCost * si.quantity;
                totalServiceValue += si.unitCost * si.quantity;
                i++;
            }
            INTAPS.Evaluator.EData labourList = new Evaluator.EData(Evaluator.DataType.ListData, list);


            i = 0;
            list = new Evaluator.ListData(document.summaryItems.Length);
            foreach (SummaryItem si in document.summaryItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(3);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.description);
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.amount));
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.basePrice));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                if (si.add)
                    grandTotal += si.amount;
                i++;
            }
            INTAPS.Evaluator.EData feeSummaryData = new Evaluator.EData(Evaluator.DataType.ListData, list);



            INTAPS.Evaluator.EData totalMaterial = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalMaterialFee = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialFeeValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalLabour = new Evaluator.EData(Evaluator.DataType.Text, totalServiceValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalSummary = new Evaluator.EData(Evaluator.DataType.Text, grandTotal.ToString("#,#00.00"));
            string htmlHeaders;
            string ret = bde.BdeAccounting.EvaluateEHTML(bde.SysPars.jobInvoiceReportID, new object[]{
                 "jobNo",jobNo
                ,"appDate",appDate
                ,"customerName",customerName
                ,"custCode",custCode
                ,"kebele",kebele
                ,"houseNo",houseNo
                ,"phoneNo",phoneNo
                ,"customerType",customerType
                ,"feeSummaryData",feeSummaryData
                ,"materialList",materialList
                ,"labourList",labourList
                ,"approvedBy",approvedBy
                ,"approvedByDate",approvedByDate
                ,"checkedBy",checkedBy
                ,"checkedByDate",checkedByDate
                ,"preparedBy",preparedBy
                ,"preparedByDate",preparedByDate
                ,"totalSummary",totalSummary
                ,"totalMaterial",totalMaterial
                ,"totalMaterialFee",totalMaterialFee
                ,"totalLabour",totalLabour
                }
                , out htmlHeaders);
            return ret;
        }
        
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            JobBillDocument bill = new JobBillDocument();
            bill.summaryItems = new SummaryItem[1];
            bill.materialItems = new MaterialItem[0];
            bill.serviceItems = new ServiceItem[billOfMaterial.items.Length];
            double totalService = 0;
            for (int i = 0; i < billOfMaterial.items.Length; i++)
            {
                JobBOMItem item = billOfMaterial.items[i];
                BIZNET.iERP.TransactionItems titem = bde.bERP.GetTransactionItems(item.itemID);
                if (titem == null)
                    throw new ServerUserMessage("Invalid item " + item.itemID);
                if (titem.GoodOrService != GoodOrService.Service)
                    throw new ServerUserMessage("Only service items are allowed");
                BIZNET.iERP.MeasureUnit mu = bde.bERP.GetMeasureUnit(titem.MeasureUnitID);
                string mustr = mu == null ? "" : mu.Name;
                string desc;
                if (string.IsNullOrEmpty(item.description))
                    desc = titem.Name;
                else
                    desc = item.description;
                ServiceItem si = new ServiceItem();
                si.itemCode = titem.Code;
                si.itemName = desc;
                si.orderNo = i + 1;
                si.quantity = 1;
                si.unitCost = item.unitPrice;
                si.unit = mustr;
                si.salesAccountID = titem.salesAccountID;
                bill.serviceItems[i] = si;
                totalService += si.unitCost * si.quantity;
            }
            bill.summaryItems[0] = new SummaryItem("", "Service", totalService, false, -1, 0);
            List<BillItem> items = new List<BillItem>();
            int itemTypeID = 1;
            foreach (ServiceItem mi in bill.serviceItems)
            {
                BillItem i = new BillItem();
                i.itemTypeID = itemTypeID++;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.unitCost;
                i.price = mi.quantity * mi.unitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);

            }
            bill.job = job;
            bill.customer = customer;
            bill.bom = billOfMaterial;
            bill.DocumentDate = DateTime.Now;
            bill.jobItems = items.ToArray();
            bill.ShortDescription = billOfMaterial.note;
            bill.draft = false;
            return bill;
        }

        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            OwnerShipTransferData wfdata = data as OwnerShipTransferData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscription s = bde.subscriberBDE.GetSubscription(wfdata.connectionID, date.Ticks);
            if (s == null)
                throw new ServerUserMessage("Invalid connection ID:" + wfdata.connectionID);
            if (s.subscriber.id == job.customerID)
                throw new ServerUserMessage("It is not allowed to transfer to the same owner");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                case StandardJobStatus.CONTRACT:
                case StandardJobStatus.ANALYSIS:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }

        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (nextState)
            {
                case StandardJobStatus.APPLICATION_APPROVAL:
                    if (job.status != StandardJobStatus.APPLICATION)
                        throw new ServerUserMessage("Invalid state change");
                    break;
                case StandardJobStatus.FINALIZATION:
                    if (job.status != StandardJobStatus.WORK_PAYMENT)
                        throw new ServerUserMessage("Invalid state change");

                    checkPayment(job.id);
                    CompletedJobDocument completed = new CompletedJobDocument();
                    completed.job = job;
                    completed.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, completed);
                    job.completionDocumentID = completed.AccountDocumentID;
                    bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                    bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);

                    break;
                case StandardJobStatus.WORK_PAYMENT:
                    if (job.status != StandardJobStatus.APPLICATION_APPROVAL)
                        throw new ServerUserMessage("Invalid state change");
                    OwnerShipTransferData wfdata= checkAndGetData(job);
                    Subscription subsc = bde.subscriberBDE.GetSubscription(wfdata.connectionID, date.Ticks);
                    bde.verifyAccountingClearance(AID, subsc.subscriberID, date, job);

                    bde.registerNewCustomer(AID, job, date);
                    
                    //if (!hasPaymentItems(job.id, bde.SysPars.ownershipTransferPaymentItem))
                    //{
                        JobBOMItem item =getOwnerShipTransferItem(job,subsc.subscriber);
                        
                        JobBillOfMaterial bom = new JobBillOfMaterial();
                        bom.items = new JobBOMItem[] { item };
                        bom.jobID = job.id;
                        bom.analysisTime = date;
                        bom.invoiceNo = -1;
                        bom.note = "Ownership Tranfer Payment";
                        bom.preparedBy = worker.userID;
                        bom.surveyCollectionTime = date;
                        bde.SetBillofMateial(AID, bom);
                        bde.generateInvoice(AID, bom.id, worker);
                    //}
                    break;
                default:
                    throw new ServerUserMessage("Invalid state change");
            }
        }

        protected virtual JobBOMItem getOwnerShipTransferItem(JobData job, Subscriber customer)
        {
            JobBOMItem item = new JobBOMItem();
            item.itemID = bde.SysPars.ownershipTransferPaymentItem;
            item.quantity = 1;
            item.inSourced = true;
            item.referenceUnitPrice = 0;
            item.calculated = true;
            item.unitPrice = bde.bERP.GetTransactionItems(bde.SysPars.ownershipTransferPaymentItem).FixedUnitPrice;
            return item;
        }

        OwnerShipTransferData checkAndGetData(JobData job)
        {
            OwnerShipTransferData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as OwnerShipTransferData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.subscriberBDE.WriterHelper.setReadDB(bde.subscriberBDE.DBName);
                    bde.WriterHelper.BeginTransaction();

                    OwnerShipTransferData wfdata = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as OwnerShipTransferData;
                    if (wfdata == null)
                        throw new ServerUserMessage("The connection that will be transfered is not set");
                    Subscription connection = bde.subscriberBDE.GetSubscription(wfdata.connectionID, date.Ticks);
                    if (connection == null)
                        throw new ServerUserMessage("The connection that will be transfered is not set");
                    Subscriber jobCustomer = bde.getJobCustomer(job);
                    Subscriber oldCustomer = bde.subscriberBDE.GetSubscriber(connection.subscriberID);
                    wfdata.oldCustomer = oldCustomer;
                    connection.remark = "Transfered from customer " + oldCustomer.customerCode;
                    connection.subscriptionStatus = SubscriptionStatus.Active;
                    connection.ticksFrom = date.Ticks;
                    connection.subscriberID = jobCustomer.id;
                    bde.subscriberBDE.changeSubscription(AID, connection); 
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();
                }                                                                   
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

    }
}
