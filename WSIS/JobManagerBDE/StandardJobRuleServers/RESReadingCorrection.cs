using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.READING_CORRECTION, "Reading Correction")]
    public class RESReadingCorrection : RESBase, IJobRuleServerHandler
    {
        public RESReadingCorrection(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            ReadingCorrectionData newLine = data as ReadingCorrectionData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ReadingCorrectionData newLine = data as ReadingCorrectionData;
            addApplicationData(AID, date, job, worker, data);
        }
        void verifyData(ReadingCorrectionData wfdata, out CustomerBillRecord[] bill)
        {
            INTAPS.RDBMS.SQLHelper dspReader = bde.subscriberBDE.GetReaderHelper();
            try
            {
                verifyData(dspReader, wfdata, out bill);
            }
            finally
            {
                bde.subscriberBDE.ReleaseHelper(dspReader);
            }
        }
        void verifyData(INTAPS.RDBMS.SQLHelper reader,ReadingCorrectionData wfdata,out CustomerBillRecord[] bill)
        {
            BWFMeterReading test = bde.subscriberBDE.BWFGetMeterReadingByPeriodID(wfdata.newReading.subscriptionID, wfdata.newReading.periodID);
            if (test == null || test.bwfStatus!=BWFStatus.Read)
                throw new ServerUserMessage("No existing reading to edit");
            wfdata.oldReading = test;

            wfdata.newReading.bwfStatus = BWFStatus.Read;
            wfdata.newReading.readingBlockID = bde.subscriberBDE.BWFGetReadingBlockID(wfdata.newReading.subscriptionID, wfdata.newReading.periodID);
            bde.subscriberBDE.BWFFixReading(reader, wfdata.newReading);
            bill = bde.subscriberBDE.getBills(bde.BdeAccounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id, -1, test.subscriptionID, test.periodID);
            foreach (CustomerBillRecord b in bill)
            {
                if (b.paymentDocumentID != -1)
                    throw new ServerUserMessage("It is not allowed to correct readings which are used in paid pills");
            }
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ReadingCorrectionData wfdata = data as ReadingCorrectionData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            if(wfdata.newReading.readingType==MeterReadingType.MeterReset)
            {
                AuditInfo audit= ApplicationServer.SecurityBDE.getAuditInfo(AID);
                if (!ApplicationServer.SecurityBDE.GetUserPermissions(ApplicationServer.SecurityBDE.GetUIDForName(audit.userName)).IsPermited("root/Subscriber/ReadingReset"))
                    throw new ServerUserMessage("You don't have permission for reseting readings");
            }
            wfdata.typeID = getTypeID();
            Subscriber oldSubscriber = bde.subscriberBDE.GetSubscriber(job.customerID);
            if (oldSubscriber == null)
                throw new ServerUserMessage("Invalid customer ID");
            ReadingEntryClerk clerk = bde.subscriberBDE.BWFGetClerkByUserID(worker.userID);
            if (clerk == null)
                throw new ServerUserMessage(worker.userID + " must be registerd in billing system as reading entry clerk");
            
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            CustomerBillRecord[] bill;

            verifyData(wfdata, out bill);
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        ReadingCorrectionData checkAndGetData(JobData job)
        {
            ReadingCorrectionData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ReadingCorrectionData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            //bde.verifyAccountingClearance(AID, job.customerID, date, job);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    ReadingCorrectionData wfdata = checkAndGetData(job);
                    CustomerBillRecord[] bills;
                    verifyData(wfdata,out bills);
                    foreach (CustomerBillRecord bill in bills)
                    {
                        if (bill.isPayedOrDiffered)
                            throw new ServerUserMessage("It is not allowed to correct readings which are used in settled bills");
                        bde.subscriberBDE.deleteCustomerBill(AID, bill.id);
                    }
                    JobStatusHistory h = bde.GetJobHistory(job.id)[0];
                    ReadingEntryClerk clerk = bde.subscriberBDE.BWFGetClerkByUserID(h.agent);
                    wfdata.newReading.bwfStatus = BWFStatus.Read;
                    wfdata.newReading.entryDate = date;
                    bde.subscriberBDE.BWFUpdateMeterReading(AID, clerk, wfdata.newReading);
                    if (bills != null && bills.Length != 0)
                    {
                        CustomerBillRecord[] recs;
                        BillItem[][] items;
                        CustomerBillDocument[] docs;
                        string msg;
                        BillPeriod prd=bde.subscriberBDE.GetBillPeriod(wfdata.newReading.periodID);
                        bool res=bde.subscriberBDE.generateMonthlyBill(date, prd, bde.subscriberBDE.GetSubscription(wfdata.newReading.subscriptionID,prd.toDate.Ticks).subscriber, out docs, out recs, out items, out msg);
                        if (!res)
                            throw new ServerUserMessage(msg);
                        for(int k=0;k<recs.Length;k++)
                        {
                            if (recs[k].connectionID == wfdata.newReading.subscriptionID && recs[k].billDocumentTypeID==bde.BdeAccounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id)
                            {
                                docs[k].draft = false;
                                recs[k].draft = false;
                                docs[k].summerizeTransaction = bde.subscriberBDE.SysPars.summerizeTransactions;
                                docs[k].billBatchID = -1;
                                int id=bde.subscriberBDE.addBill(AID,docs[k],recs[k],items[k]);
                            }
                        }
                    }
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

    }

    
}
