using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using BIZNET.iERP;

namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.RETURN_METER, "Return Meter")]
    public class RESReturnMeter : RESBase, IJobRuleServerHandler
    {
        public RESReturnMeter(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            ReturnMeterData newLine = data as ReturnMeterData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ReturnMeterData newLine = data as ReturnMeterData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ReturnMeterData wfdata = data as ReturnMeterData;
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            Subscription s = bde.subscriberBDE.GetSubscription(wfdata.connectionID,date.Ticks);
            if (s == null)
                throw new ServerUserMessage("Invalid connection ID:" + wfdata.connectionID);
            if (s.subscriber.id != job.customerID)
                throw new ServerUserMessage("The connection must belong to the applicant");
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                case StandardJobStatus.FINALIZATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }
        
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (nextState)
            {
                case StandardJobStatus.FINALIZATION:
                    checkPayment(job.id);
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                    ReturnMeterData wfdata = checkAndGetData(job);
                    if (wfdata == null)
                        throw new ServerUserMessage("Meter return application form not filled");
                    string itemCode = wfdata.permanent ? bde.SysPars.permanentMeterReturnPaymentItem : bde.SysPars.temporaryMeterReturnPaymentItem;
                    if (!hasPaymentItems(job.id, itemCode))
                    {
                        JobBOMItem item = new JobBOMItem();
                        item.itemID = itemCode;
                        item.quantity = 1;
                        item.inSourced = true;
                        item.referenceUnitPrice = 0;
                        JobBillOfMaterial bom = new JobBillOfMaterial();
                        bom.items = new JobBOMItem[] { item };
                        bom.jobID = job.id;
                        bom.analysisTime = date;
                        bom.invoiceNo = -1;
                        bom.note = berpBDE.GetTransactionItems(itemCode).Name;
                        bom.preparedBy = worker.userID;
                        bom.surveyCollectionTime = date;
                        bde.SetBillofMateial(AID,bom);
                    }
                    break;
                case StandardJobStatus.SURVEY:
                    if (!hasApplicationPaymentItems(job.id))
                        throw new ServerUserMessage("Application payment should be made");
                    checkPayment(job.id);
                    break;
            }
        }

        ReturnMeterData checkAndGetData(JobData job)
        {
            ReturnMeterData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ReturnMeterData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    ReturnMeterData wfdata = checkAndGetData(job);
                    Subscription connection = bde.subscriberBDE.GetSubscription(wfdata.connectionID,date.Ticks);
                    if (connection == null)
                        throw new ServerUserMessage("Connection not selected");
                    if (wfdata.permanent)
                        bde.subscriberBDE.ChangeSubcriptionStatus(AID, connection.id, date, SubscriptionStatus.Discontinued, false);
                    else
                        bde.subscriberBDE.ChangeSubcriptionStatus(AID, connection.id, date, SubscriptionStatus.Diconnected, false);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }



        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            JobBillDocument bill = new JobBillDocument();
            bill.summaryItems = new SummaryItem[1];
            bill.materialItems = new MaterialItem[0];
            bill.serviceItems = new ServiceItem[billOfMaterial.items.Length];
            double totalService = 0;
            for (int i = 0; i < billOfMaterial.items.Length; i++)
            {
                JobBOMItem item = billOfMaterial.items[i];
                BIZNET.iERP.TransactionItems titem = bde.bERP.GetTransactionItems(item.itemID);
                if (titem == null)
                    throw new ServerUserMessage("Invalid item " + item.itemID);
                if (titem.GoodOrService != GoodOrService.Service)
                    throw new ServerUserMessage("Only service items are allowed");
                BIZNET.iERP.MeasureUnit mu = bde.bERP.GetMeasureUnit(titem.MeasureUnitID);
                string mustr = mu == null ? "" : mu.Name;
                string desc;
                if (string.IsNullOrEmpty(item.description))
                    desc = titem.Name;
                else
                    desc = item.description;
                ServiceItem si = new ServiceItem();
                si.itemCode = titem.Code;
                si.itemName = desc;
                si.orderNo = i + 1;
                si.quantity = 1;
                si.unitCost = item.unitPrice;
                si.unit = mustr;
                si.salesAccountID = titem.salesAccountID;
                bill.serviceItems[i] = si;
                totalService += si.unitCost * si.quantity;
            }
            bill.summaryItems[0] = new SummaryItem("", "Service", totalService, false, -1, 0);
            List<BillItem> items = new List<BillItem>();
            int itemTypeID = 1;
            foreach (ServiceItem mi in bill.serviceItems)
            {
                BillItem i = new BillItem();
                i.itemTypeID = itemTypeID++;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.unitCost;
                i.price = mi.quantity * mi.unitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);

            }
            bill.job = job;
            bill.customer = customer;
            bill.bom = billOfMaterial;
            bill.DocumentDate = DateTime.Now;
            bill.jobItems = items.ToArray();
            bill.ShortDescription = billOfMaterial.note;
            bill.draft = false;
            return bill;
        }
    }
    
}
