using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
namespace INTAPS.WSIS.Job
{
    [JobRuleServerHandler(StandardJobTypes.VOID_RECEIPT, "Void Receipt")]
    public class RESVoidReceipt : RESBase, IJobRuleServerHandler
    {
        public RESVoidReceipt(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            VoidReceiptData newLine = data as VoidReceiptData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            VoidReceiptData newLine = data as VoidReceiptData;
            addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            VoidReceiptData wfdata = data as VoidReceiptData;

            lock (bde.WriterHelper)
            {
                try
                {
                    string sql = "Select Top 1 customerID from Subscriber.dbo.CustomerBill where paymentDocumentID=" + wfdata.receitDocumentID;
                    int customerID = (int)bde.WriterHelper.ExecuteScalar(sql);
                    job.customerID = customerID;
                    string update = String.Format("Update JobManager.dbo.JobData SET customerID={0} where id={1}", customerID, job.id);
                    bde.WriterHelper.ExecuteScalar(update);
                }
                catch
                {
                    throw;
                }
            }
            if (wfdata == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfdata.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                default:
                    return new int[0];
            }
        }

        

        VoidReceiptData checkAndGetData(JobData job)
        {
            VoidReceiptData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as VoidReceiptData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }

        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            VoidReceiptData wfdata = checkAndGetData(job);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    CustomerPaymentReceipt receipt = bde.BdeAccounting.GetAccountDocument(wfdata.receitDocumentID, true) as CustomerPaymentReceipt;
                    if (receipt == null)
                        throw new ServerUserMessage("Invalid receipt ID " + wfdata.receitDocumentID);
                    //check if there is cash hand over document posted at the date of receipt issuance and prevent voiding receipt
                    bde.BdeAccounting.WriterHelper.setReadDB(bde.BdeAccounting.DBName);
                    AccountDocument[] handover = bde.BdeAccounting.GetAccountingDocumentByFilter(bde.BdeAccounting.WriterHelper, "DocumentTypeID=175 and tranTicks >=" + date.Date.Ticks, true);
                    foreach (CashHandoverDocument cashHandOver in handover)
                    {
                        if (cashHandOver.data.sourceCashAccountID == receipt.assetAccountID)
                        {
                            throw new Exception("Cannot void receipt after cash hand over document is posted!");
                        }
                    }

                    wfdata.receiptHTML = bde.BdeAccounting.GetDocumentHTML(receipt);
                    int [] docs= bde.BdeAccounting.getDocumentListByTypedReference(receipt.receiptNumber);
                    foreach (int docID in docs)
                    {
                        bde.BdeAccounting.removeDocumentSerial(AID, docID, receipt.receiptNumber);
                    }
                    if (receipt.billBatchID == -1)
                        bde.BdeAccounting.DeleteGenericDocument(AID, receipt.AccountDocumentID);
                    else
                    {
                        ReverseCustomerPaymentReceiptDocument reverse = new ReverseCustomerPaymentReceiptDocument();
                        reverse.ShortDescription = "Reversal of " + receipt.ShortDescription;
                        DocumentTypedReference tref = bde.getJobAccountingTypedDocumentReference(job, true);
                        reverse.PaperRef = tref.reference;
                        reverse.receiptID = receipt.AccountDocumentID;
                        bde.BdeAccounting.PostGenericDocument(AID, reverse);
                        bde.BdeAccounting.addDocumentSerials(AID, reverse, tref);
                    }
                    bde.BdeAccounting.voidSerialNo(AID, date, receipt.receiptNumber, note);
                    bde.setWorkFlowData(AID, wfdata);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

    }
}
