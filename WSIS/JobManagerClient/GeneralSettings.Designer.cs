﻿namespace INTAPS.WSIS.Job.Client
{
    partial class GeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.applicationServiceItem = new INTAPS.WSIS.Job.Client.ItemPlaceHolder();
            this.itemPlaceHolder1 = new INTAPS.WSIS.Job.Client.ItemPlaceHolder();
            this.label2 = new System.Windows.Forms.Label();
            this.itemPlaceHolder2 = new INTAPS.WSIS.Job.Client.ItemPlaceHolder();
            this.label3 = new System.Windows.Forms.Label();
            this.itemPlaceHolder3 = new INTAPS.WSIS.Job.Client.ItemPlaceHolder();
            this.label4 = new System.Windows.Forms.Label();
            this.itemPlaceHolder4 = new INTAPS.WSIS.Job.Client.ItemPlaceHolder();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.itemPlaceHolder5 = new INTAPS.WSIS.Job.Client.ItemPlaceHolder();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(452, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 32;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(369, 6);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 33;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 17);
            this.label1.TabIndex = 35;
            this.label1.Text = "Application Fee Item:";
            // 
            // applicationServiceItem
            // 
            this.applicationServiceItem.anobject = null;
            this.applicationServiceItem.Location = new System.Drawing.Point(275, 12);
            this.applicationServiceItem.Name = "applicationServiceItem";
            this.applicationServiceItem.Size = new System.Drawing.Size(242, 20);
            this.applicationServiceItem.TabIndex = 34;
            this.applicationServiceItem.Tag = "servicePaymentItem";
            // 
            // itemPlaceHolder1
            // 
            this.itemPlaceHolder1.anobject = null;
            this.itemPlaceHolder1.Location = new System.Drawing.Point(275, 38);
            this.itemPlaceHolder1.Name = "itemPlaceHolder1";
            this.itemPlaceHolder1.Size = new System.Drawing.Size(242, 20);
            this.itemPlaceHolder1.TabIndex = 34;
            this.itemPlaceHolder1.Tag = "ownershipTransferPaymentItem";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(13, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 17);
            this.label2.TabIndex = 35;
            this.label2.Text = "Ownership Transfer Fee Item:";
            // 
            // itemPlaceHolder2
            // 
            this.itemPlaceHolder2.anobject = null;
            this.itemPlaceHolder2.Location = new System.Drawing.Point(275, 89);
            this.itemPlaceHolder2.Name = "itemPlaceHolder2";
            this.itemPlaceHolder2.Size = new System.Drawing.Size(242, 20);
            this.itemPlaceHolder2.TabIndex = 34;
            this.itemPlaceHolder2.Tag = "temporaryMeterReturnPaymentItem";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(13, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(227, 17);
            this.label3.TabIndex = 35;
            this.label3.Text = "Permanent Meter Return Fee Item:";
            // 
            // itemPlaceHolder3
            // 
            this.itemPlaceHolder3.anobject = null;
            this.itemPlaceHolder3.Location = new System.Drawing.Point(275, 64);
            this.itemPlaceHolder3.Name = "itemPlaceHolder3";
            this.itemPlaceHolder3.Size = new System.Drawing.Size(242, 20);
            this.itemPlaceHolder3.TabIndex = 34;
            this.itemPlaceHolder3.Tag = "permanentMeterReturnPaymentItem";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(13, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(223, 17);
            this.label4.TabIndex = 35;
            this.label4.Text = "Temporary Meter Return Fee Item:";
            // 
            // itemPlaceHolder4
            // 
            this.itemPlaceHolder4.anobject = null;
            this.itemPlaceHolder4.Location = new System.Drawing.Point(275, 115);
            this.itemPlaceHolder4.Name = "itemPlaceHolder4";
            this.itemPlaceHolder4.Size = new System.Drawing.Size(242, 20);
            this.itemPlaceHolder4.TabIndex = 34;
            this.itemPlaceHolder4.Tag = "depositPaymentItem";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(13, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 17);
            this.label5.TabIndex = 35;
            this.label5.Text = "Billing Deposit Payment Item:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 196);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(529, 37);
            this.panel1.TabIndex = 36;
            // 
            // itemPlaceHolder5
            // 
            this.itemPlaceHolder5.anobject = null;
            this.itemPlaceHolder5.Location = new System.Drawing.Point(275, 141);
            this.itemPlaceHolder5.Name = "itemPlaceHolder5";
            this.itemPlaceHolder5.Size = new System.Drawing.Size(242, 20);
            this.itemPlaceHolder5.TabIndex = 34;
            this.itemPlaceHolder5.Tag = "meterDiagnosisItem";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(13, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(146, 17);
            this.label6.TabIndex = 35;
            this.label6.Text = "Meter Diagnosis Item:";
            // 
            // GeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 233);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.itemPlaceHolder5);
            this.Controls.Add(this.itemPlaceHolder4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.itemPlaceHolder3);
            this.Controls.Add(this.itemPlaceHolder2);
            this.Controls.Add(this.itemPlaceHolder1);
            this.Controls.Add(this.applicationServiceItem);
            this.MaximizeBox = false;
            this.Name = "GeneralSettings";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "General Settings";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private ItemPlaceHolder applicationServiceItem;
        private System.Windows.Forms.Label label1;
        private ItemPlaceHolder itemPlaceHolder1;
        private System.Windows.Forms.Label label2;
        private ItemPlaceHolder itemPlaceHolder2;
        private System.Windows.Forms.Label label3;
        private ItemPlaceHolder itemPlaceHolder3;
        private System.Windows.Forms.Label label4;
        private ItemPlaceHolder itemPlaceHolder4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private ItemPlaceHolder itemPlaceHolder5;
        private System.Windows.Forms.Label label6;
    }
}