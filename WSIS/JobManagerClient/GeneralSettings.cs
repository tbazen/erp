﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.WSIS.Job.Client
{
    public partial class GeneralSettings : Form
    {
        public GeneralSettings()
        {
            InitializeComponent();
            foreach (Control c in this.Controls)
            {
                ItemPlaceHolder p = c as ItemPlaceHolder;
                if (p == null)
                    continue;
                string key = c.Tag as string;
                if (string.IsNullOrEmpty(key))
                    continue;

                string itemCode = JobManagerClient.GetSystemParameter(key) as string;
                p.SetByID(itemCode);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> changedSettings = new Dictionary<string, object>();
            foreach (Control c in this.Controls)
            {
                ItemPlaceHolder p = c as ItemPlaceHolder;
                if (p == null)
                    continue;
                string key = c.Tag as string;
                if (string.IsNullOrEmpty(key))
                    continue;
                if (p.Changed)
                {
                    changedSettings.Add(key, p.GetObjectID());
                }
            }
            if (changedSettings.Count > 0)
            {
                try
                {
                    string[] fields=new string[changedSettings.Count];
                    object[] vals=new object[changedSettings.Count];
                    changedSettings.Keys.CopyTo(fields,0);
                    changedSettings.Values.CopyTo(vals,0);
                    JobManagerClient.SetSystemParameters(fields, vals);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to update settings", ex);
                }
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
