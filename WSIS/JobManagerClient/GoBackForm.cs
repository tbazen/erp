﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;

namespace INTAPS.WSIS.Job.Client
{
    public partial class GoBackForm : Form,IHTMLBuilder
    {HtmlTable t = new HtmlTable();
        public GoBackForm(JobData job,JobStatusHistory [] hist)
        {
            InitializeComponent();
            List<int> unique = new List<int>();
            for (int i = hist.Length - 1; i >= 0; i--)
            {
                if (!unique.Contains(hist[i].newStatus) && !StandardJobStatus.isInactiveState(hist[i].newStatus) && job.status!=hist[i].newStatus)
                    unique.Add(hist[i].newStatus);
            }
            
            foreach (int s in unique)
            {
                HtmlTableRow row = new HtmlTableRow();
                HtmlTableCell cell = new HtmlTableCell();
                row.Cells.Add(cell);
                cell.InnerHtml = string.Format("<a class='commandLinkJobProcess' href='changeJobStatus?jobID={0}&newStatus={1}' >{2}</a>"
                        , job.id
                        , (int)s
                        , JobManagerClient.GetCommandString(job.id,s)
                        );
                t.Rows.Add(row);
            }
            browser.LoadControl( this);
        }

        public int oldStatus;

        public void Build()
        {
            
        }

        public string GetOuterHtml()
        {
            return HTMLBuilder.ControlToString(t);
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            switch (path)
            {
                case "changeJobStatus":
                    this.BeginInvoke(new ProcessTowParameter<int, int>(processChangeJobStatus), int.Parse((string)query["jobID"]), int.Parse((string)query["newStatus"]));
                    return true;
                default:
                    return false;
            }
        }
        void processChangeJobStatus(int jobID, int newStatus)
        {
            oldStatus = newStatus;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        public void SetHost(IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Go Back"; }
        }
    }
}
