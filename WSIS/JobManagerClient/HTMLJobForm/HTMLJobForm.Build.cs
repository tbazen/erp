﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    partial class HTMLJobForm
    {
        HtmlTable _outerTable;  
        public void Build()
        {
            _outerTable = new HtmlTable();
            _outerTable.Attributes.Add("class", "outerTable");
            HtmlTable jobTable = createJobTable();
            HtmlTableRow row;
            HtmlTableCell cell;
            IJobRuleClientHandler client=JobManagerClient.getJobRuleEngineClientHandler(_job.applicationType);

            bool jobEditable = true;// JobProcessRules.searchArray<JobWorkerRole>(new JobWorkerRole[] { JobWorkerRole.Receiption, JobWorkerRole.Analaysis }, JobManagerClient.GetWorker(INTAPS.ClientServer.Client.ApplicationClient.UserName).roles);

            bool jobDeletable = _job.status == StandardJobStatus.APPLICATION;
            //date edit command row
            {
                string editLink = jobEditable ? "<a class='commandLinkJobProcess' href='editJob?jobID=" + _job.id + "' >Edit Application</a>" : "";
                string deleteLink = jobDeletable ? "<a class='commandLinkJobProcess' href='deleteJob?jobID=" + _job.id + "' >Delete Job</a>" : "";
                row = new HtmlTableRow();
                cell = HTMLBuilder.CreateHtmlCell(deleteLink + editLink);
                cell.Style.Add("text-align", "right");
                row.Cells.Add(cell);
                _outerTable.Rows.Add(row);
            }
            //process command row
            {
                string processCmds =
                    _statusHistory.Length>1 && !StandardJobStatus.isInactiveState(_job.status)
                    ? string.Format("<a class='commandLinkJobProcessDarkBlue' href='goBack?jobID={0}' >Go to Previous</a>", _job.id)
                    :"";
                foreach (int status in JobManagerClient.getPossibleNextStatus(DateTime.Now,this._job.id))
                {
                    string link = string.Format("<a class='{3}' href='changeJobStatus?jobID={0}&newStatus={1}' >{2}</a>"
                        , _job.id
                        , (int)status
                        , JobManagerClient.GetCommandString(this._job.id,status)
                        , status == StandardJobStatus.CANCELED? "commandLinkJobProcessRed" : "commandLinkJobProcess"
                        );
                    processCmds += link;
                }
                string newBOMLink = (this._job.status == StandardJobStatus.SURVEY)
                    || (this._job.status == StandardJobStatus.APPLICATION && this._job.applicationType == StandardJobTypes.GENERAL_SELL)
                    ? string.Format("<a class='commandLinkJobProcessDarkBlue' href='addBOM?jobID={0}'>Add Bill of Quantity</a>", _job.id) : "";
                row = new HtmlTableRow();
                cell = HTMLBuilder.CreateHtmlCell(processCmds+newBOMLink);
                cell.Style.Add("text-align", "left");
                row.Cells.Add(cell);
                _outerTable.Rows.Add(row);
            }
            //job table
            row = new HtmlTableRow();
            cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span><a class='commandLink3' href='viewHistory?jobID={2}'>History</a>{1}"
                , HttpUtility.HtmlEncode("Job Detail")
                , HTMLBuilder.ControlToString(jobTable)
                ,_job.id
                ));

            row.Cells.Add(cell);
            _outerTable.Rows.Add(row);

            if (_job.customerID > 0 && (_job.newCustomer == null || _job.newCustomer.id != _job.customerID))
            {
                //existing customer detail
                Subscriber customer = SubscriberManagmentClient.GetSubscriber(_job.customerID);
                if (customer == null)
                {
                    row = new HtmlTableRow();
                    cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span>"
                        , HttpUtility.HtmlEncode("Customer Information Doesn't Exist or deleted")
                        ));
                    row.Cells.Add(cell);
                    _outerTable.Rows.Add(row);
                }
                else
                {
                    var custTable = createApplicationTable(customer);
                    row = new HtmlTableRow();
                    cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span>{1}"
                        , HttpUtility.HtmlEncode("Customer Detail")
                        , BIZNET.iERP.bERPHtmlBuilder.ToString(custTable)));
                    row.Cells.Add(cell);
                    _outerTable.Rows.Add(row);
                }
            }
            if (_job.newCustomer != null)
            {
                //new customer detail
                var newCustTable = createApplicationTable(_job.newCustomer);
                row = new HtmlTableRow();
                cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span>{1}"
                    , HttpUtility.HtmlEncode("New Customer Detail")

                    , BIZNET.iERP.bERPHtmlBuilder.ToString(newCustTable)));
                row.Cells.Add(cell);
                _outerTable.Rows.Add(row);
            }
            //if (_job.subscriptionID != -1 && (_job.newContract == null || _job.newContract.id != _job.subscriptionID))
            //{
            //    //existing connection detail
            //    HtmlTable conTable = createConnectionTable(SubscriberManagmentClient.GetSubscription(_job.subscriptionID));
            //    row = new HtmlTableRow();
            //    cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span>{1}"
            //        , HttpUtility.HtmlEncode("Connection Detail")
            //        , HTMLBuilder.ControlToString(conTable)));
            //    row.Cells.Add(cell);
            //    _outerTable.Rows.Add(row);
            //}
            //if (_job.newContract != null)
            //{
            //    //new connection detail
            //    HtmlTable newConTable = createConnectionTable(_job.newContract);
            //    row = new HtmlTableRow();
            //    cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span>{1}"
            //        , HttpUtility.HtmlEncode("New Connection Detail")

            //        , HTMLBuilder.ControlToString(newConTable)));
            //    row.Cells.Add(cell);
            //    _outerTable.Rows.Add(row);
            //}
            row = new HtmlTableRow();
            string subHtml;
            try
            {
                subHtml = client.buildDataHTML(_job);
            }
            catch (Exception ex)
            {
                subHtml = BIZNET.iERP.bERPHtmlBuilder.HtmlEncode("Error generating more job information\n"+ex.Message);
            }
            cell = HTMLBuilder.CreateHtmlCell(subHtml);
            _outerTable.Rows.Add(row);
            row.Cells.Add(cell);
            JobBillOfMaterial [] bom= JobManagerClient.GetJobBOM(_job.id);
            if (bom.Length > 0)
            {
                //bill of materials
                HtmlTable bomTable = createBOMList(bom);
                row = new HtmlTableRow();
                cell = HTMLBuilder.CreateHtmlCell(string.Format("<span class='jobSection1'>{0}</span>{1}"
                    , HttpUtility.HtmlEncode("Bill of Quantities")
                    , HTMLBuilder.ControlToString(bomTable)));
                row.Cells.Add(cell);
                _outerTable.Rows.Add(row);
            }
        }
        HtmlTable createBOMList(JobBillOfMaterial [] boms)
        {
            HtmlTable bomTable = new HtmlTable();
            bomTable.ID = "bomList";
            HTMLBuilder.CreateHtmlRow(bomTable
                ,"tableHeader1"
                ,HTMLBuilder.CreateTextCell("No.")
                , HTMLBuilder.CreateTextCell("Description")
                ,HTMLBuilder.CreateTextCell("Amount")
                , HTMLBuilder.CreateTextCell("Receipt No")
                , HTMLBuilder.CreateTextCell("Store Issue No")
                ,HTMLBuilder.CreateTextCell("")
                );
            int no=1;
            foreach(JobBillOfMaterial b in boms)
            {
                CustomerBillRecord billrec = SubscriberManagmentClient.getCustomerBillRecord(b.invoiceNo);
                string receiptNo = null;
                bool isPaid = billrec!=null && billrec.isPayedOrDiffered;
                if(isPaid)
                {
                    if (billrec.paymentDiffered)
                        receiptNo = "Differed";
                    else 
                        receiptNo = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(billrec.paymentDocumentID, false).PaperRef;
                }

                string viewInvoice = "<a class='commandLink2' href='viewInvoice?bomID=" + b.id + "' >View</a>" ;
                string editLink = !isPaid? "<a class='commandLink2' href='editBOM?bomID=" + b.id+ "' >Edit</a>" : "";
                string deleteLink = !isPaid ? "<a class='commandLink2' href='deleteBOM?bomID=" + b.id + "' >Delete</a>" : "";
                string printLink="";
                string storeIssueLink = "";
                if (StandardJobStatus.IsPaymentStatus(this._job.status) && !isPaid)
                    printLink = !isPaid ? "<a class='commandLink2' href='printReceipt?bomID=" + b.id + "' >Print Receipt</a>" : "";
                string storeIssueRef="";
                if(b.storeIssueID>0)
                    storeIssueRef = INTAPS.Accounting.Client.AccountingClient.GetAccountDocument(b.storeIssueID, false).PaperRef;
                if (this._job.status==StandardJobStatus.STORE_ISSUE && b.storeIssueID!=-1)
                    storeIssueLink = "<a class='commandLink2' href='storeIssue?bomID=" + b.id + "' >Store Issue</a>";

                double total = 0;
                try
                {
                    total = JobManagerClient.getBOMTotal(b.id);
                }
                catch
                {
                    total = -1;
                }
                HTMLBuilder.AddAlternatingStyleRow(bomTable,HTMLBuilder.CreateHtmlRow(
                    
                    HTMLBuilder.CreateTextCell(no.ToString())
                    ,HTMLBuilder.CreateTextCell(b.note)
                    ,HTMLBuilder.CreateTextCell(total==-1?"error":total.ToString("#,#0.00"))
                    , HTMLBuilder.CreateTextCell(!isPaid ? "" : receiptNo)
                    , HTMLBuilder.CreateTextCell(storeIssueRef)
                    ,HTMLBuilder.CreateHtmlCell(viewInvoice+editLink+deleteLink+ printLink+storeIssueLink)
                    ),new string[]{"evenRow1","oddRow1"}
                    );
                no++;
            }
            bomTable.Attributes.Add("class", "innerTable");
            return bomTable;

        }
        
        public const string DATE_TIME_FORMAT="MMM dd,yyyy hh:mm tt";
        private BIZNET.iERP.bERPHtmlTable createApplicationTable(Subscriber customer)
        {
            var appTable = new BIZNET.iERP.bERPHtmlTable();
            appTable.createBodyGroup();
            appTable.id = "custInfo";
            string services=null;
            foreach(ServiceType s in _job.serviceTypes)
            {
                if(services==null)
                    services=JobProcessRules.GetServiceTypeName(s,false);
                else
                    services+=", "+JobProcessRules.GetServiceTypeName(s,false);
            }
            JobProcessRules.buildTwoColumnDataTable(appTable.groups[0]
                ,"Application Date:",_job.startDate.ToString(DATE_TIME_FORMAT)
                ,"Customer Code:",customer.customerCode
                ,"Customer Name:",customer.name
                , "Customer Type:", Subscriber.EnumToName(customer.subscriberType)
                , "Kebele:", customer.Kebele<1?"":SubscriberManagmentClient.GetKebele(customer.Kebele).ToString()
                ,"House No:",customer.address
                ,"Name of Place:",customer.nameOfPlace
                );
            appTable.css= "innerTable";
            return appTable;
        }

        private HtmlTable createJobTable()
        {
            HtmlTable jobTable = new HtmlTable();
            jobTable.ID = "jobInfo";
            string applicationText = JobManagerClient.getJobType(_job.applicationType).name;
            
            HTMLBuilder.CreateHtmlRow(jobTable
                , HTMLBuilder.CreateTextCell("Job Number:", "dataLabel1"), HTMLBuilder.CreateTextCell(_job.jobNo, "dataValue1")
                , HTMLBuilder.CreateTextCell(applicationText, "applicationType"));
            JobAppointment apo = (_appointments.Length > 0) ? _appointments[_appointments.Length - 1] : null;
            if (apo == null || !apo.active)
            {
                HTMLBuilder.CreateHtmlRow(jobTable
                    , HTMLBuilder.CreateTextCell("Appointment:", "dataLabel1"), HTMLBuilder.CreateTextCell("No Appointment", "dataValue1")
                    , HTMLBuilder.CreateHtmlCell(string.Format("<a class='commandLink1' href='setAppointment?jobID={0}'>Set Appointment</a>", _job.id))
                    );
            }
            else
            {
                HTMLBuilder.CreateHtmlRow(jobTable
                    , HTMLBuilder.CreateTextCell("Appointment:", "dataLabel1"), HTMLBuilder.CreateHtmlCell(string.Format("{0}<br/>{1}",
                    HttpUtility.HtmlEncode( apo.appointmentDate.ToString("MMM dd,yyyy hh:mm tt")),HttpUtility.HtmlEncode(apo.note)), "dataValue1")
                    , HTMLBuilder.CreateHtmlCell(string.Format("<a class='commandLink1' href='closeAppointment?apID={0}'>Close Appointment</a><a class='commandLink1' href='changeAppointment?apID={0}'>Change Appointment</a>", apo.id))
                    );
            }
            JobStatusHistory h= _statusHistory[_statusHistory.Length - 1];
            string statusstring;
            if (string.IsNullOrEmpty(h.note))
                statusstring = JobManagerClient.getJobStatus(_job.status).name;
            else
            {
                JobWorker worker = JobManagerClient.GetWorker(h.agent);

                INTAPS.Payroll.Employee emp=worker==null?null:Payroll.Client.PayrollClient.GetEmployee(worker.employeeID);
                string employeeName = emp == null ? "[Invalid employee]" : emp.employeeName;
                statusstring = JobManagerClient.getJobStatus(_job.status).name + "<br/>" + HttpUtility.HtmlEncode("Remark by " + employeeName+ ": " + h.note);
            }
            HTMLBuilder.CreateHtmlRow(jobTable
                    , HTMLBuilder.CreateTextCell("Status:", "dataLabel1")
                    , HTMLBuilder.CreateHtmlCell(statusstring, "dataValue1")
                    , HTMLBuilder.CreateHtmlCell(string.Format("<a class='commandLink1' href='showJobCard?jobID={0}'>Job Card</a>", _job.id))
                    );
            jobTable.Attributes.Add("class", "innerTable");
            return jobTable;
        }

        
    }
}
