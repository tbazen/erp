﻿namespace INTAPS.WSIS.Job.Client
{
    partial class HTMLJobForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.browserToolbar = new INTAPS.UI.HTML.BowserController();
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 25);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(918, 548);
            this.browser.StyleSheetFile = "jobstyle.css";
            this.browser.TabIndex = 0;
            // 
            // browserToolbar
            // 
            this.browserToolbar.Location = new System.Drawing.Point(0, 0);
            this.browserToolbar.Name = "browserToolbar";
            this.browserToolbar.ShowBackForward = true;
            this.browserToolbar.ShowRefresh = true;
            this.browserToolbar.Size = new System.Drawing.Size(918, 25);
            this.browserToolbar.TabIndex = 1;
            this.browserToolbar.Text = "bowserController1";
            // 
            // HTMLJobForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 573);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.browserToolbar);
            this.Name = "HTMLJobForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.HTML.ControlBrowser browser;
        private UI.HTML.BowserController browserToolbar;
    }
}