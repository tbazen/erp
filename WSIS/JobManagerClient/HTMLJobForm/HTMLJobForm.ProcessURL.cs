﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Client
{

    partial class HTMLJobForm
    {
        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            try
            {
                switch (path)
                {
                    case "goBack":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processGoBack), int.Parse((string)query["jobID"]));
                        return true;
                    case "viewHistory":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processViewHistory), int.Parse((string)query["jobID"]));
                        return true;
                    case "editBOM":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processEditBom), int.Parse((string)query["bomID"]));
                        return true;
                    case "deleteBOM":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processDeleteBom), int.Parse((string)query["bomID"]));
                        return true;
                    case "voidReceipt":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processVoidReceipt), int.Parse((string)query["bomID"]));
                        return true;
                    case "printReceipt":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processPrintReceipt), int.Parse((string)query["bomID"]));
                        return true;
                    case "viewInvoice":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processViewInvoice), int.Parse((string)query["bomID"]));
                        return true;
                    case "addBOM":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processAddBOM), int.Parse((string)query["jobID"]));
                        return true;

                    case "changeJobStatus":
                        this.BeginInvoke(new ProcessTowParameter<int,int>(processChangeJobStatus), int.Parse((string)query["jobID"]), int.Parse((string)query["newStatus"]));
                        return true;

                    case "deleteJob":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processDeleteJob), int.Parse((string)query["jobID"]));
                        return true;

                    case "editJob":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processEditJob), int.Parse((string)query["jobID"]));
                        return true;
                    case "showJobCard":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processShowJobCard), int.Parse((string)query["jobID"]));
                        return true;
                    case "aproveApplication":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processApproveApplication), int.Parse((string)query["jobID"]));
                        return true;
                    case "setAppointment":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processSetAppointment), int.Parse((string)query["jobID"]));
                        return true;
                    case "closeAppointment":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processCloseAppointment), int.Parse((string)query["apID"]));
                        return true;
                    case "changeAppointment":
                        this.BeginInvoke(new ProcessSingleParameter<int>(processChangeApointment), int.Parse((string)query["apID"]));
                        return true;
                    default:
                        return JobManagerClient.getJobRuleEngineClientHandler(_job.applicationType).processURL(this, path, query);
                }
            }
            catch (Exception ex)
            {
                this.BeginInvoke(new ShowErrorMesssageDelegate(showErrorMessage), ex);
                return true;
            }
        }
        void processEditBom(int bomID)
        {
            try
            {
                BOMEditor editor = new BOMEditor(this._job.id, bomID, _job.status == StandardJobStatus.ANALYSIS ? EditableFields.ReferencePrice : EditableFields.ItemQuantity);
                if (editor.ShowDialog(this) == DialogResult.OK)
                {
                    loadData(_job.id);
                    browser.RefreshPage();
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        void processViewHistory(int jobID)
        {
            try
            {
                JobHistoryViewer jh = new JobHistoryViewer(_statusHistory, _appointments);
                jh.Show(this);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        void processDeleteBom(int bomID)
        {
            try
            {
                if (INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected bill of quantity?"))
                {
                    JobManagerClient.DeleteBOM(bomID);
                    loadData(_job.id);
                    browser.RefreshPage();
                }
            }
            catch (Exception exception)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to delete bill of quantity", exception);
            }
        }
        void processViewInvoice(int bomID)
        {
            JobInvoiceViewer iv = new JobInvoiceViewer(bomID);
            iv.Show(this);
        }
        public Subscriber getJobCustomer(JobData job)
        {
            if (job.customerID <1)
                return job.newCustomer;
            return SubscriberManagment.Client.SubscriberManagmentClient.GetSubscriber(job.customerID);
        }


        private string[] JobLines(JobData data)
        {
            Subscriber customer=getJobCustomer(_job);
            return new string[] { 
                    "Applicant Name:" + customer.name
                    ,"Customer Code:" + (string.IsNullOrEmpty(customer.customerCode)?"[New Customer]":customer.customerCode)
                    , "Job No.:" + data.jobNo
            };
        }
        void processPrintReceipt(int bomID)
        {
            try
            {
                JobBillOfMaterial bom= JobManagerClient.GetBillOfMaterial(bomID);
                Cash cash=new Cash(Program.salesPoint, new CustomerBillDocument[]{ AccountingClient.GetAccountDocument(bom.invoiceNo,true) as CustomerBillDocument},3);
                if (cash.ShowDialog(this) == DialogResult.OK)
                {
                    loadData(_job.id);
                    browser.RefreshPage();
                }
            }
            catch (Exception exception)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error generating receipt", exception);
            }
        }
        void processVoidReceipt(int bomID)
        {
            try
            {
                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to void this receipt?"))
                    return;
                JobManagerClient.VoidJobReceipt(bomID);
                loadData(_job.id);
                browser.RefreshPage();

            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to void receipt", ex);
            }
        }
        void processGoBack(int jobID)
        {
            GoBackForm gbf = new GoBackForm(_job, _statusHistory);
            if (gbf.ShowDialog(this) == DialogResult.OK)
            {
                changeStatus(jobID, gbf.oldStatus);
            }
        }
        void processAddBOM(int jobID)
        {
            BOMEditor editor = new BOMEditor(this._job.id, -1, _job.status == StandardJobStatus.ANALYSIS ? EditableFields.ReferencePrice : EditableFields.ItemQuantity);
            if (editor.ShowDialog(this) == DialogResult.OK)
            {
                loadData(_job.id);
                browser.RefreshPage();
            }
        }
        void changeStatus(int jobID, int newStatus)
        {
            ChangeStatusForm form = new ChangeStatusForm(this._job, newStatus);
            if (form.ShowDialog(this) == DialogResult.OK)
            {
                INTAPS.WSIS.Job.Client.MainForm.MainButtonGrid.UpdateLayoutInPlace();
                if (newStatus == StandardJobStatus.FINISHED || JobManagerClient.jobAppliesTo(DateTime.Now, jobID))
                {
                    loadData(_job.id);
                    browser.RefreshPage();
                }
                else
                    base.Close();
            }
        }
        void processChangeJobStatus(int jobID,int newStatus)
        {
            changeStatus(jobID, newStatus);
        }
        void showErrorMessage(Exception ex)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error", ex);
        }
        void processApproveApplication(int jobID)
        {
            
        }

        void processEditJob(int jobID)
        {

            IJobRuleClientHandler client = JobManagerClient.getJobRuleEngineClientHandler(_job.applicationType);

            NewApplicationForm appForm = client.getNewApplicationForm(jobID,SubscriberManagmentClient.GetSubscriber(_job.customerID),null);
            if (appForm.ShowDialog(this) == DialogResult.OK)
            {
                loadData(_job.id);
                browser.RefreshPage();
            }
        }

        void processDeleteJob(int jobID)
        {
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this job?"))
                return;
            try
            {
                JobManagerClient.RemoveJob(jobID);
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to delete job", ex);
            }
        }
        void processShowJobCard(int jobID)
        {
            try
            {
                string jobCard = JobManagerClient.GetJobCard(this._job.id);
                HTMLBuilder.ViewInApplication(ApplicationType.Word, "Job Card:" + this._job.jobNo, jobCard);
            }
            catch (Exception exception)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to produce job card", exception);
            }
        }
        void processSetAppointment(int jobID)
        {
            ApointmentForm af = new ApointmentForm(DateTime.Now);
            if (af.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    af.appointment.jobID = jobID;
                    JobManagerClient.AddAppointment(af.appointment);
                    reloadJobTable();
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to set appointment",ex);
                }
            }
        }
        void processCloseAppointment(int apID)
        {
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure about closing this appointment?"))
                return;
            try
            {
                JobManagerClient.CloseAppointment(apID);
                reloadJobTable();
            }
            catch (Exception exception)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error adjusting appoitnemnt", exception);
            }

        }
        void processChangeApointment(int apID)
        {
            ApointmentForm form = new ApointmentForm(JobManagerClient.GetAppointment(apID), this._job.startDate);
            if (form.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    form.appointment.id = apID;
                    form.appointment.active = true;
                    JobManagerClient.UpdateAppointment(form.appointment);
                    reloadJobTable();
                }
                catch (Exception exception)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error updating appointment", exception);
                }

            }
        }

        private void reloadJobTable()
        {
            loadData(_job.id);
            HtmlTable table = createJobTable();
            _outerTable.Rows[0].Cells[0].Controls.Clear();
            _outerTable.Rows[0].Cells[0].Controls.Add(table);
            browser.UpdateElement("jobInfo", table);
        }

        
    }
}
