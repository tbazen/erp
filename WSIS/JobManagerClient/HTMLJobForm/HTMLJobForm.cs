﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.HTML;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public partial class HTMLJobForm : Form,IHTMLBuilder
    {
        public class CustomerJobFormProfilePlugin : ICustomerProfilePlugin
        {
            public bool ProcessUrl(Form parent, string path, System.Collections.Hashtable query)
            {
                switch (path)
                {
                    case "showJob":
                        parent.BeginInvoke(new ProcessSingleParameter<int>(
                            delegate(int jobID)
                            {
                                HTMLJobForm j = new HTMLJobForm(jobID);
                                j.Show(parent);
                            }
                            ), int.Parse((string)query["jobID"]));
                        return true;
                    default:
                        return false;
                }
            }
        }

        

        JobData _job;
        JobStatusHistory[] _statusHistory;
        JobAppointment[] _appointments;

        
        public HTMLJobForm()
        {
            InitializeComponent();
            browserToolbar.SetBrowser(browser);
        }

        public HTMLJobForm(int jobID):this()
        {
            loadData(jobID);
            this.Build();
            browser.LoadControl(this);
        }

        private void loadData(int jobID)
        {
            _job = JobManagerClient.GetJob(jobID);
            _statusHistory = JobManagerClient.GetJobHistory(jobID);
            _appointments = JobManagerClient.GetJobAppointments(jobID);
        }

        public string GetOuterHtml()
        {
            return HTMLBuilder.ControlToString(_outerTable);
        }

        

        public void SetHost(IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Job"; }
        }
    }
}
