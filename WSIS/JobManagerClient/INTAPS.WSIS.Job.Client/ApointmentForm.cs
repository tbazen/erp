
    using INTAPS.Ethiopic;
    using INTAPS.UI;
    using INTAPS.WSIS.Job;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public partial class ApointmentForm : Form
    {
        private DateTime _after;
        private EtGrPickerPair _date;
        public JobAppointment appointment;
        
        private IContainer components;
         
        public ApointmentForm(DateTime after)
        {
            this.components = null;
            this.InitializeComponent();
            this._date = new EtGrPickerPair(this.dateAppointment, this.timeAppointment);
        }

        public ApointmentForm(JobAppointment ap, DateTime after) : this(after)
        {
            this._date.SetValue(ap.appointmentDate);
            this.textRemark.Text = ap.note;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (this._date.GetValue() < this._after)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError("The date and time should be after " + JobProcessRules.TimeString(this._after));
            }
            else
            {
                this.appointment = new JobAppointment();
                this.appointment.note = this.textRemark.Text;
                this.appointment.appointmentDate = this._date.GetValue();
                base.DialogResult = DialogResult.OK;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

