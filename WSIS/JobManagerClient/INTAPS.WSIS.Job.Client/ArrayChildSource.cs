
    using INTAPS.UI.ButtonGrid;
    using System;
    using System.Collections;
    using System.Collections.Generic;
namespace INTAPS.WSIS.Job.Client
{

    public class ArrayChildSource : IButtonChildSource, IEnumerable<ButtonGridItem>, IEnumerable
    {
        private INTAPS.UI.ButtonGrid.ButtonGrid _grid;
        private List<ButtonGridItem> _items = new List<ButtonGridItem>();
        private ButtonGridItem _parentButton;

        public ArrayChildSource(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parentButton, params ButtonGridItem[] items)
        {
            this._items.AddRange(items);
            this._grid = grid;
            this._parentButton = parentButton;
            foreach (ButtonGridItem item in items)
            {
                this._grid.RelateWithParent(parentButton, item);
            }
        }

        public IEnumerator<ButtonGridItem> GetEnumerator()
        {
            return this._items.GetEnumerator();
        }

        public void SetQuery(string query)
        {
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool Searchable
        {
            get
            {
                return false;
            }
        }


        public string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }
}

