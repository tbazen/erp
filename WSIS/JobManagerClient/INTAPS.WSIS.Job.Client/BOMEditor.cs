
using INTAPS.Accounting;
using INTAPS.Ethiopic;
using INTAPS.Payroll.Client;
using INTAPS.UI;
using INTAPS.WSIS.Job;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Client
{
    public enum EditableFields
    {
        ItemQuantity = 1,
        ReferencePrice = 2,
        All = ItemQuantity | ReferencePrice
    }
    public partial class BOMEditor : Form
    {
        private JobBillOfMaterial _bom;
        private JobData _job;

        private IContainer components = null;
        EditableFields _fields;
        public BOMEditor(int jobID, int bomID,EditableFields fields)
        {
            this.InitializeComponent();
            _fields = fields;
            this._job = JobManagerClient.GetJob(jobID);
            if (bomID == -1)
            {
                this._bom = null;
                this.textNote.Text = "Work Payment";
            }
            else
            {
                this._bom = JobManagerClient.GetBillOfMaterial(bomID);
            }
            this.InitDataGrid();
            this.LoadData();
        }
        public bool setReferenceMode
        {
            get
            {
                return _fields == EditableFields.ReferencePrice;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
                        try
            {
                
                JobBillOfMaterial bom = new JobBillOfMaterial();
                bom.jobID = this._job.id;
                bom.analysisTime = DateTime.Now;
                bom.note = this.textNote.Text;
                bom.surveyCollectionTime = this.dateCollectionTime.Value;
                List<JobBOMItem> list = new List<JobBOMItem>();
                foreach (DataGridViewRow row in (IEnumerable)this.grid.Rows)
                {
                    if (!row.IsNewRow)
                    {
                        BIZNET.iERP.TransactionItems item = row.Tag as BIZNET.iERP.TransactionItems;
                        bool hasItem = item != null;
                        if (!hasItem)
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserError("Item code not selected for item no:" + (row.Index + 1));
                            row.Selected = true;
                            return;
                        }
                        bool hasQuantity = ((row.Cells[this.colQuantity.Index].Value is double) && (((double)row.Cells[this.colQuantity.Index].Value) > 0.0)) && Account.AmountGreater((double)row.Cells[this.colQuantity.Index].Value, 0.0);
                        bool hasReferencePrice = (row.Cells[this.colReferencePrice.Index].Value is double) && Account.AmountGreater((double)row.Cells[this.colReferencePrice.Index].Value, 0.0);
                        bool customerSourced = (row.Cells[this.colInSource.Index].Value is bool) && ((bool)row.Cells[this.colInSource.Index].Value);

                        if ((!hasItem || (!hasQuantity && (!AccountBase.AmountEqual(item.FixedUnitPrice, 0)))) || (customerSourced && !hasReferencePrice && setReferenceMode))
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserError("Data not complete for item no:" + (row.Index + 1));
                            row.Selected = true;
                            return;
                        }
                        if (customerSourced && !hasReferencePrice && setReferenceMode)
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserError("Reference price not set for item no:" + (row.Index + 1));
                            row.Selected = true;
                            return;
                        }
                        if (!customerSourced && hasReferencePrice)
                        {
                            UIFormApplicationBase.CurrentAppliation.ShowUserError("Reference price can not be set for item no:" + (row.Index + 1));
                            row.Selected = true;
                            return;
                        }
                        JobBOMItem jobItem = new JobBOMItem();
                        jobItem.itemID = ((BIZNET.iERP.TransactionItems)row.Tag).Code;
                        if (!AccountBase.AmountEqual(item.FixedUnitPrice, 0))
                        {
                            jobItem.quantity = (double)row.Cells[this.colQuantity.Index].Value;
                        }
                        else
                        {
                            jobItem.quantity = -1.0;
                        }
                        jobItem.inSourced = !customerSourced;
                        if (hasReferencePrice)
                        {
                            jobItem.referenceUnitPrice = (double)row.Cells[this.colReferencePrice.Index].Value;
                        }
                        else
                        {
                            jobItem.referenceUnitPrice = -1;
                        }
                        list.Add(jobItem);
                    }
                }
                bom.items = list.ToArray();
                if (this._bom != null)
                {
                    bom.id = this._bom.id;
                }
                bom.id = JobManagerClient.SetBillofMateial(bom);
                this._bom = bom;
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Data Saved");
                base.DialogResult = DialogResult.OK;
                base.Close();
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error saving data", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FormatRow(DataGridViewRow row)
        {
            row.Cells[this.colItemNo.Index].Value = (row.Index + 1).ToString();
            BIZNET.iERP.TransactionItems item = row.Tag as BIZNET.iERP.TransactionItems;
            if (item == null)
            {
                string code = row.Cells[this.colItemCode.Index].Value as string;
                if (code == null)
                {
                    item = null;
                }
                else
                {
                    item = BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(code);
                }
            }
            if (item == null)
            {
                row.Cells[this.colItemName.Index].Value = null;
                row.Cells[this.colUnit.Index].Value = null;
                row.Cells[this.colWSSAPrice.Index].Value = null;
                row.Cells[this.colStock.Index].Value = null;
            }
            else
            {
                row.Cells[this.colItemCode.Index].Value = item.Code;
                row.Cells[this.colItemName.Index].Value = item.Name;
                BIZNET.iERP.MeasureUnit u = BIZNET.iERP.Client.iERPTransactionClient.GetMeasureUnit(item.MeasureUnitID);
                row.Cells[this.colUnit.Index].Value = u == null ? "":u.Name ;
                
                row.Cells[this.colWSSAPrice.Index].Value = (AccountBase.AmountEqual(item.FixedUnitPrice,0)) ? null : ((object)item.FixedUnitPrice);
                int storeID = Convert.ToInt32(JobManagerClient.GetSystemParameter("materialStoreID"));
                if (storeID == -1)
                    row.Cells[this.colStock.Index].Value = 0d;
                else
                {
                    int store = (int)JobManagerClient.GetSystemParameter("materialStoreID");
                    CostCenterAccount csa = AccountingClient.GetCostCenterAccount(store, item.inventoryAccountID);
                    if(csa!=null)
                        row.Cells[this.colStock.Index].Value = AccountingClient.GetNetBalanceAsOf(csa.id, TransactionItem.MATERIAL_QUANTITY, DateTime.Now);
                }
                row.Tag = item;
            }
            row.Cells[this.colDelete.Index].Value = "X";
        }

        private void grid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (((e.RowIndex != -1) && (e.ColumnIndex == this.colDelete.Index)) && !this.grid.Rows[e.RowIndex].IsNewRow)
            {
                this.grid.Rows.RemoveAt(e.RowIndex);
            }
        }

        private void grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.RowIndex != -1) && (e.ColumnIndex == this.colItemCode.Index))
            {
                this.grid.Rows[e.RowIndex].Tag = null;
                this.FormatRow(this.grid.Rows[e.RowIndex]);
            }
        }

        private void InitDataGrid()
        {

            colReferencePrice.ReadOnly = !setReferenceMode;
            foreach (DataGridViewColumn column in this.grid.Columns)
            {
                if (setReferenceMode)
                    if (column != colReferencePrice)
                        column.ReadOnly = true;
                if (column.ReadOnly)
                {
                    column.DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            grid.AllowUserToAddRows = grid.AllowUserToDeleteRows = !setReferenceMode;
            colDelete.Visible = !setReferenceMode;
            foreach (DataGridViewColumn column in new DataGridViewColumn[] { this.colWSSAPrice, this.colReferencePrice })
            {
                column.ValueType = typeof(double);
                column.DefaultCellStyle.Format = "#,#0.00";
            }
            this.colInSource.ValueType = typeof(bool);
            foreach (DataGridViewColumn column in new DataGridViewColumn[] { this.colQuantity, this.colStock })
            {
                column.ValueType = typeof(double);
                column.DefaultCellStyle.Format = "0.00";
            }
        }


        private void LoadData()
        {
            this.grid.Rows.Clear();
            if (this._bom != null)
            {
                this.textNote.Text = this._bom.note;
                this.dateCollectionTime.Value = this._bom.surveyCollectionTime;
                foreach (JobBOMItem item in this._bom.items)
                {
                    BIZNET.iERP.TransactionItems materialDescription = BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(item.itemID);
                    DataGridViewRow row = this.grid.Rows[this.grid.Rows.Add()];
                    row.Tag = materialDescription;
                    row.Cells[this.colQuantity.Index].Value = (item.quantity > 0.0) ? ((object)item.quantity) : ((object)DBNull.Value);
                    if (Account.AmountEqual(item.referenceUnitPrice, -1))
                    {
                        row.Cells[this.colReferencePrice.Index].Value = null;
                    }
                    else
                    {
                        row.Cells[this.colReferencePrice.Index].Value = item.referenceUnitPrice;
                    }
                    row.Cells[this.colInSource.Index].Value = !item.inSourced;
                    this.FormatRow(row);
                }
            }
        }

        public JobBillOfMaterial Bom
        {
            get
            {
                return this._bom;
            }
            set
            {
                this._bom = value;
            }
        }
    }
}

