
    using INTAPS.Accounting.Client;
    using INTAPS.UI.ButtonGrid;
    using System;
    using System.Drawing;
namespace INTAPS.WSIS.Job.Client
{

    internal class ButtonGridHelper
    {
        public const int BUTTON_CMD_HEIGHT = 60;
        public const int BUTTON_HEIGHT = 120;
        public const int BUTTON_WIDTH = 100;
        private static Pen ButtonFocusPen = new Pen(Color.DarkBlue, 2f);
        public static Font font_normal_button = new Font("Arial Rounded MT", 10f);
        public static Font font_small_text= new Font("Arial Rounded MT", 8f);
        public static Font font_title = new Font("Arial Rounded MT", 18f);
        public static Font font_title_Bold = new Font("Arial Rounded MT", 18f, FontStyle.Bold);
        public const int JUMBO_BUTTON_HEIGHT = 240;
        public const int JUMBO_BUTTON_WIDTH = 200;
        public static void AddCornerText(ButtonGridItem item, string cornerText)
        {
            AddCornerText(item, cornerText, true);
        }


        public static void AddCornerText(ButtonGridItem item, string cornerText,bool right)
        {
            ButtonElement<string> element = null;
            if (item is ButtonGridBodyButtonItem)
            {
                element = ((ButtonGridBodyButtonItem) item).ApperanceBody.AddText(cornerText);
            }
            else
            {
                element = ((ButtonGridGroupButtonItem) item).Apperance.AddText(cornerText);
            }
            StringAlignment halign = right ? StringAlignment.Far : StringAlignment.Near;
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, halign));
            element.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, halign));
            element.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, halign));
            element.DefaultStyle.Add(StyleType.Font, font_normal_button);
            element.SnapStyle.Add(StyleType.Font, font_normal_button);
            element.SelectStyle.Add(StyleType.Font, font_normal_button);
        }
        public static void AddSmallCornerText(ButtonGridItem item, string cornerText, bool right)
        {
            ButtonElement<string> element = null;
            if (item is ButtonGridBodyButtonItem)
            {
                element = ((ButtonGridBodyButtonItem)item).ApperanceBody.AddText(cornerText);
            }
            else
            {
                element = ((ButtonGridGroupButtonItem)item).Apperance.AddText(cornerText);
            }
            StringAlignment halign = right ? StringAlignment.Far : StringAlignment.Near;
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, halign));
            element.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, halign));
            element.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, halign));
            element.DefaultStyle.Add(StyleType.Font, font_small_text);
            element.SnapStyle.Add(StyleType.Font, font_small_text);
            element.SelectStyle.Add(StyleType.Font, font_small_text);
        }

        public static ButtonElement<Image> AddLargeImage(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                if (item is ButtonGridBodyButtonItem)
                {
                    ButtonGridBodyButtonItem item2 = (ButtonGridBodyButtonItem) item;
                    return item2.ApperanceBody.AddImage(img);
                }
                if (item is ButtonGridGroupButtonItem)
                {
                    ButtonGridGroupButtonItem item3 = (ButtonGridGroupButtonItem) item;
                    return item3.Apperance.AddImage(img);
                }
            }
            return null;
        }

        public static void AddLargeImageLeftTop(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                ButtonElement<Image> element = AddLargeImage(item, img);
                element.DefaultStyle.Add(StyleType.ImageAlignment, ImageAlignment.LeftTop);
                element.SelectStyle.Add(StyleType.ImageAlignment, ImageAlignment.LeftTop);
                element.SnapStyle.Add(StyleType.ImageAlignment, ImageAlignment.LeftTop);
            }
        }

        public static void AddLargeImageRightBottom(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                AddLargeImage(item, img).DefaultStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightBottom);
            }
        }

        public static void AddLargeImageRightTop(ButtonGridItem item, Image img)
        {
            if (img != null)
            {
                ButtonElement<Image> element = AddLargeImage(item, img);
                element.DefaultStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightTop);
                element.SelectStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightTop);
                element.SnapStyle.Add(StyleType.ImageAlignment, ImageAlignment.RightTop);
            }
        }

        public static void AddMiddleText(ButtonGridBodyButtonItem item, string middleText)
        {
            ButtonElement<string> element = null;
            element = item.ApperanceBody.AddText(middleText);
            element.DefaultStyle.Add(StyleType.Font, font_normal_button);
            element.SnapStyle.Add(StyleType.Font, font_normal_button);
            element.SelectStyle.Add(StyleType.Font, font_normal_button);
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
            element.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
            element.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
        }

        public static ButtonGridBodyButtonItem CreateButton(string key, string txt)
        {
            ButtonGridBodyButtonItem item = new KeyedButton(key);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonWidth, 100);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, 120);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonWidth, 100);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonHeight, 120);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonWidth, 100);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonHeight, 120);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.BroderPen, ButtonFocusPen);
            ButtonElement<string> element = item.ApperanceBody.AddText(txt);
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            element.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            element.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            element.DefaultStyle.Add(StyleType.Font, font_normal_button);
            element.SnapStyle.Add(StyleType.Font, font_normal_button);
            element.SelectStyle.Add(StyleType.Font, font_normal_button);
            element = item.ApperanceHeader.AddText(txt);
            element.SnapStyle.Add(StyleType.FontBrush, Brushes.LightBlue);
            element.SelectStyle.Add(StyleType.FontBrush, Brushes.White);
            element.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Center));
            return item;
        }

        public static ButtonGridBodyButtonItem CreateButton(string key, string txt, string cornerText)
        {
            ButtonGridBodyButtonItem item = CreateButton(key, txt);
            AddCornerText(item, cornerText);
            return item;
        }

        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color)
        {
            ButtonGridBodyButtonItem item = CreateButton(key, txt);
            SolidBrush brush = new SolidBrush(color);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FillBrush, brush);
            return item;
        }

        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string cornerText)
        {
            ButtonGridBodyButtonItem item = CreateButtonWithColor(key, txt, color);
            AddCornerText(item, cornerText);
            return item;
        }
        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string leftText,string rightText)
        {
            ButtonGridBodyButtonItem item = CreateButtonWithColor(key, txt, color);
            AddSmallCornerText(item, leftText,false);
            AddSmallCornerText(item, rightText, true);
            return item;
        }

        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string cornerText, Image img)
        {
            ButtonGridBodyButtonItem item = CreateButtonWithColor(key, txt, color, cornerText);
            AddLargeImage(item, img);
            return item;
        }

        public static ButtonGridBodyButtonItem CreateButtonWithColor(string key, string txt, Color color, string cornerText, int img)
        {
            return CreateButtonWithColor(key, txt, color, cornerText, LoadImageFromServer(img, 100, 120));
        }

        public static ButtonGridBodyButtonItem CreateButtonWithImage(string key, string txt, Image img)
        {
            ButtonGridBodyButtonItem item = CreateButton(key, txt);
            AddLargeImage(item, img);
            return item;
        }

        public static ButtonGridBodyButtonItem CreateButtonWithImage(string key, string txt, int img)
        {
            return CreateButtonWithImage(key, txt, LoadImageFromServer(img, 100, 120));
        }

        internal static ButtonGridBodyButtonItem CreateCommandButton(string key, string txt)
        {
            ButtonGridBodyButtonItem item = CreateButton(key, txt);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, 60);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonHeight, 60);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonHeight, 60);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FontBrush, Brushes.Black);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.FontBrush, Brushes.Blue);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.FontBrush, Brushes.DarkBlue);

            return item;
        }

        internal static ButtonGridBodyButtonItem CreateCommandButton(string key, string txt, string cornerText)
        {
            ButtonGridBodyButtonItem item = CreateCommandButton(key, txt);
            AddCornerText(item, cornerText);
            return item;
        }

        public static ButtonGridGroupButtonItem CreateGroupButton(string text)
        {
            ButtonGridGroupButtonItem item = new ButtonGridGroupButtonItem();
            ButtonElement<string> element = item.Apperance.AddText(text);
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormatVert(StringAlignment.Near));
            element.DefaultStyle.Add(StyleType.Font, font_title);
            return item;
        }

        public static ButtonGridGroupButtonItem CreateGroupButton(string text, int pictureID)
        {
            ButtonGridGroupButtonItem item = CreateGroupButton(text);
            AddLargeImageRightBottom(item, LoadImageFromServer(pictureID, 100, 120));
            return item;
        }

        internal static ButtonGridBodyButtonItem CreateJumboButton(string key, string title)
        {
            ButtonGridBodyButtonItem item = new KeyedButton(key);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonWidth, 200);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, 240);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonWidth, 200);
            item.ApperanceBody.StylesCollection.SelectStyle.Add(StyleType.ButtonHeight, 240);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonWidth, 200);
            item.ApperanceBody.StylesCollection.SnapStyle.Add(StyleType.ButtonHeight, 240);
            ButtonElement<string> element = item.ApperanceBody.AddText(title);
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            element.SnapStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            element.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Far, StringAlignment.Center));
            Font font = new Font("Arial Rounded MT", 14f);
            element.DefaultStyle.Add(StyleType.Font, font);
            element.SnapStyle.Add(StyleType.Font, font);
            element.SelectStyle.Add(StyleType.Font, font);
            return item;
        }

        public static ButtonGridGroupButtonItem CreateModuleGroupButton(string number, string text)
        {
            ButtonGridGroupButtonItem item = new ButtonGridGroupButtonItem();
            item.Apperance.StylesCollection.DefaultStyle.Add(StyleType.CornerRadius, 0);
            ButtonElement<string> element = item.Apperance.AddText(text);
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormatVert(StringAlignment.Near));
            element.DefaultStyle.Add(StyleType.Font, font_title);
            element = item.Apperance.AddText(number);
            element.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Near, StringAlignment.Near));
            element.DefaultStyle.Add(StyleType.Font, font_title_Bold);
            return item;
        }

        public static Image LoadImageFromServer(int pictureID, int width, int height)
        {
            if (pictureID != -1)
            {
                try
                {
                    return INTAPS.Accounting.Client.AccountingClient.LoadImageFormated(pictureID, 1, false, width, height);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(string.Format("Error loading image {0} returning null\n{1}", pictureID, exception.Message));
                    return null;
                }
            }
            return null;
        }

        internal static void ReformatAsNoneCommand(ButtonGridItem item, Image image)
        {
            StylesCollection stylesCollection;
            if (item is ButtonGridBodyButtonItem)
            {
                stylesCollection = ((ButtonGridBodyButtonItem) item).ApperanceBody.StylesCollection;
            }
            else
            {
                stylesCollection = ((ButtonGridGroupButtonItem) item).Apperance.StylesCollection;
            }
            stylesCollection.DefaultStyle.Add(StyleType.ButtonHeight, 120);
            stylesCollection.SnapStyle.Add(StyleType.ButtonHeight, 120);
            stylesCollection.SelectStyle.Add(StyleType.ButtonHeight, 120);
            AddLargeImage(item, image);
        }

        internal static void ReformatAsNoneCommand(ButtonGridItem item, int image)
        {
            ReformatAsNoneCommand(item, LoadImageFromServer(image, 100, 120));
        }

        internal static void SetButtonBackColor(ButtonGridBodyButtonItem item, Color color)
        {
            SolidBrush brush = new SolidBrush(color);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FillBrush, brush);
        }

        public static void SetHeaderModeText(ButtonGridBodyButtonItem item, string text)
        {
            item.ApperanceHeader.Texts.Clear();
            item.ApperanceHeader.AddText(text);
        }
    }
}

