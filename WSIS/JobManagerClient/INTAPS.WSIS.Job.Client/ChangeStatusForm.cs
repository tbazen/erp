
    using INTAPS.Ethiopic;
    using INTAPS.UI;
    using INTAPS.WSIS.Job;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public partial class ChangeStatusForm : Form
    {
        private EtGrPickerPair _date;
        private JobData _job;
        private int _status;
         
        private IContainer components = null;
         
        public ChangeStatusForm(JobData job, int status)
        {
            this.InitializeComponent();
            this.Text = JobManagerClient.GetCommandString(job.id,status);
            this._status = status;
            this._job = job;
            this._date = new EtGrPickerPair(this.dateStatus, this.timeStatus);
            this._date.SetValue(job.statusDate);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobManagerClient.ChangeJobStatus(this._job.id, this._date.GetValue(), this._status, this.textRemark.Text);
                base.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error changing job status", ex);
                if (!(ex is ClientServer.ServerUserMessage))
                {
                    string msg = "";
                    while (ex != null)
                    {
                        msg += "\n" + ex.Message + "\n" + ex.StackTrace;
                        ex = ex.InnerException;
                    }
                    MessageBox.Show(msg);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

