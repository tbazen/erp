
    using INTAPS.UI.ButtonGrid;
    using System;
    using System.Collections;
    using System.Collections.Generic;
namespace INTAPS.WSIS.Job.Client
{

    internal class CombinedButtonList : IButtonChildSource, IEnumerable<ButtonGridItem>, IEnumerable, IEnumerator<ButtonGridItem>, IDisposable, IEnumerator
    {
        private string _query = null;
        private IButtonChildSource[] _sources;
        private IEnumerator<ButtonGridItem> currentSource = null;
        private int sourceIndex = -1;

        public CombinedButtonList(params IButtonChildSource[] sources)
        {
            this._sources = sources;
        }

        public void Dispose()
        {
        }

        public IEnumerator<ButtonGridItem> GetEnumerator()
        {
            this.sourceIndex = -1;
            return this;
        }

        public bool MoveNext()
        {
            if (this.sourceIndex == -1)
            {
                this.sourceIndex = 0;
                this.currentSource = this._sources[this.sourceIndex].GetEnumerator();
            }
            while (!this.currentSource.MoveNext())
            {
                this.sourceIndex++;
                if (this.sourceIndex < this._sources.Length)
                {
                    this.currentSource = this.currentSource = this._sources[this.sourceIndex].GetEnumerator();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public void Reset()
        {
            this.sourceIndex = -1;
        }

        public void SetQuery(string query)
        {
            this._query = query;
            foreach (IButtonChildSource source in this._sources)
            {
                source.SetQuery(this._query);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public ButtonGridItem Current
        {
            get
            {
                return this.currentSource.Current;
            }
        }

        public bool Searchable
        {
            get
            {
                foreach (IButtonChildSource source in this._sources)
                {
                    if (source.Searchable)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }


        public string searchLabel
        {
            get {
                
                foreach (IButtonChildSource source in this._sources)
                {
                    if (source.Searchable)
                    {
                        return source.searchLabel;
                    }
                }
                return "Search";
            }
        }
    }
}

