
using INTAPS.UI;
using System;
using System.Windows.Forms;
using INTAPS.Accounting;
namespace INTAPS.WSIS.Job.Client
{

    public class DocumentActivator : IButtonGridItemActivator
    {
        private IGenericDocumentClientHandler handler;

        public DocumentActivator(IGenericDocumentClientHandler h)
        {
            this.handler = h;
        }

        public DialogResult Activate()
        {
            object editor = this.handler.CreateEditor(false);
            this.handler.SetEditorDocument(editor, null);
            if (editor is Form)
            {
                ((Form)editor).Show(UIFormApplicationBase.MainForm);
            }
            return DialogResult.OK;
        }
    }
}

