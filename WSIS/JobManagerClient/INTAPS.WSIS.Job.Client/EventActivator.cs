
    using INTAPS.UI.ButtonGrid;
    using System;
    using System.Threading;
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public class EventActivator<ObjectType> : IButtonGridItemActivator
    {
        private ObjectType _data;
        private ButtonGridBodyButtonItem _item;

        public event GenericEventHandler<ButtonGridBodyButtonItem, ObjectType> Clicked;

        public EventActivator(ButtonGridBodyButtonItem item, ObjectType data)
        {
            this._item = item;
            this._item.Tag = this;
            this._data = data;
        }

        public DialogResult Activate()
        {
            if (this.Clicked != null)
            {
                this.Clicked(this._item, this._data);
            }
            return DialogResult.OK;
        }
    }
}

