
    using System;
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public class FormActivator : IButtonGridItemActivator
    {
        private System.Type formType;
        private bool m_dialog;
        private Form owner;

        public FormActivator(Form formOwner, System.Type formType, bool dialog)
        {
            this.owner = formOwner;
            this.formType = formType;
            this.m_dialog = dialog;
        }

        public DialogResult Activate()
        {
            if (this.m_dialog)
            {
                return ((Form) Activator.CreateInstance(this.formType)).ShowDialog(this.owner);
            }
            ((Form) Activator.CreateInstance(this.formType)).Show(this.owner);
            return DialogResult.OK;
        }
    }
}

