﻿namespace INTAPS.WSIS.Job.Client
{
    public partial class HTMLViewer : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controlBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.browserToolbar = new INTAPS.UI.HTML.BowserController();
            base.SuspendLayout();
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(0, 0x19);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(730, 0x228);
            this.controlBrowser.StyleSheetFile = "StyleSheet.css";
            this.controlBrowser.TabIndex = 4;
            this.browserToolbar.Location = new System.Drawing.Point(0, 0);
            this.browserToolbar.Name = "browserToolbar";
            this.browserToolbar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.browserToolbar.ShowBackForward = false;
            this.browserToolbar.ShowRefresh = false;
            this.browserToolbar.Size = new System.Drawing.Size(730, 0x19);
            this.browserToolbar.TabIndex = 5;
            this.browserToolbar.Text = "bowserController1";
            base.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            base.ClientSize = new System.Drawing.Size(730, 0x241);
            base.Controls.Add(this.controlBrowser);
            base.Controls.Add(this.browserToolbar);
            base.Name = "HTMLViewer";
            base.ShowIcon = false;
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HTMLViewer";
            base.ResumeLayout(false);
            base.PerformLayout();
        }
        #endregion
        private INTAPS.UI.HTML.ControlBrowser controlBrowser;
        private INTAPS.UI.HTML.BowserController browserToolbar;
    }
}