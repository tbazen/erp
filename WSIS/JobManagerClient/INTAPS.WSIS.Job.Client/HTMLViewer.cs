
using INTAPS.UI.HTML;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public partial class HTMLViewer : Form
    {

        private IContainer components = null;


        public HTMLViewer(string title, string html)
        {
            this.InitializeComponent();
            this.browserToolbar.SetBrowser(this.controlBrowser);
            this.controlBrowser.LoadTextPage("", html);
            this.Text = title;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}

