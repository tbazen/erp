
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public interface IButtonGridItemActivator
    {
        DialogResult Activate();
    }
}

