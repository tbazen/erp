
using INTAPS.UI.ButtonGrid;
using INTAPS.WSIS.Job;
using System;
using System.Collections.Generic;
using System.Drawing;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job.Client
{

    internal class JobList : ObjectBGList<JobData>
    {
        public JobList(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parent) : base(grid, parent)
        {
        }

        static Dictionary<int, Subscriber> cache_getCustomer = new Dictionary<int, Subscriber>();
        Subscriber getCustomer(int customerID)
        {
            if (!cache_getCustomer.ContainsKey(customerID))
            {
                Subscriber customer = INTAPS.SubscriberManagment.Client.SubscriberManagmentClient.GetSubscriber(customerID);
                cache_getCustomer.Add(customerID, customer);
                return customer;
            }
            return cache_getCustomer[customerID];
        }
        static Dictionary<int, JobStatusType> cache_getJobStatus = new Dictionary<int, JobStatusType>();
        JobStatusType getJobStatus(int status)
        {
            if (!cache_getJobStatus.ContainsKey(status))
            {
                JobStatusType ret = JobManagerClient.getJobStatus(status);
                cache_getJobStatus.Add(status, ret);
                return ret;
            }
            return cache_getJobStatus[status];
        }
        public override ButtonGridItem[] CreateButton(JobData job)
        {
            Subscriber customer = job.customerID < 1 ? job.newCustomer : getCustomer(job.customerID);
            ButtonGridBodyButtonItem item = ButtonGridHelper.CreateButtonWithColor("", customer == null ? "[Internal]" : customer.name, Color.DarkBlue, getJobStatus(job.status).name, job.jobNo);
            item.Tag = new ObjectEditorActivator<int, HTMLJobForm>(job.id);
            return new ButtonGridItem[] { item };
        }

        static Dictionary<String, List<JobData>> cache_loadData = new Dictionary<string, List<JobData>>();
        public override List<JobData> LoadData(string query)
        {

            List<JobData> list = new List<JobData>();
            try
            {
                int NRec;
                if (string.IsNullOrEmpty(query))
                {
                    list.AddRange(JobManagerClient.GetFilteredJobs(DateTime.Now));
                    return list;
                }
                if (!cache_loadData.ContainsKey(query))
                {
                    list.AddRange(JobManagerClient.SearchJob(query, 0, 30, out NRec));
                    cache_loadData.Add(query, list);
                }
                else
                    list = cache_loadData[query];

            }
            catch (Exception exception)
            {
                Console.WriteLine("Error getting data:" + exception.Message);
            }
            return list;
        }

        public override bool Searchable
        {
            get
            {
                return true;
            }
        }

        public override string searchLabel
        {
            get { return "Job No:"; }
        }

    }
}

