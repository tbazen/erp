
    using INTAPS.ClientServer.Client;
    using INTAPS.Payroll.Client;
    using INTAPS.UI;
    using INTAPS.WSIS.Job;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public partial class JobWorkerForm : Form
    {
        
        private IContainer components;
        public JobWorkerForm()
        {
            this._jw = null;
            this.components = null;
            this.InitializeComponent();
            this.buttonActivate.Enabled = false;
            this.buttonDelete.Enabled = false;
            initalizeAppTypes();
        }

        public JobWorkerForm(JobWorker jw) : this()
        {
            this._jw = jw;
            if (jw != null)
            {
                this.buttonActivate.Enabled = true;
                this.buttonActivate.Text = jw.active ? "Deactivate" : "Activate";
                this.buttonDelete.Enabled = true;
                this.employee.SetByID(jw.employeeID);
                foreach (CheckBox box in this.groupRoles.Controls)
                {
                    if (jw.roles != null)
                        foreach (JobWorkerRole role in jw.roles)
                        {
                            if (box.Tag.ToString() == role.ToString())
                            {
                                box.Checked = true;
                                break;
                            }
                        }
                }
                foreach (CheckBox box in this.flowAppTypes.Controls)
                {
                    if (jw.jobTypes != null)
                    {
                        if (jw.jobTypes != null)
                            foreach (int app in jw.jobTypes)
                            {
                                if ((int)box.Tag == app)
                                {
                                    box.Checked = true;
                                    break;
                                }
                            }
                    }
                }
            }
        }
        void initalizeAppTypes()
        {
            foreach (JobTypeInfo jt in JobManagerClient.getAllJobTypes())
            {
                CheckBox chk = new CheckBox();
                chk.Text = jt.name;
                chk.Tag = jt.id;
                chk.AutoSize = true;
                flowAppTypes.Controls.Add(chk);
            }
        }

        private void buttonActivate_Click(object sender, EventArgs e)
        {
            if (UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to change the status of this worker?"))
            {
                try
                {
                    JobManagerClient.ChangeWorkerStatus(this._jw.userID, !this._jw.active);
                    INTAPS.WSIS.Job.Client.MainForm.MainButtonGrid.UpdateLayout();
                    base.Close();
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error saving worker data", exception);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete this worker?"))
            {
                try
                {
                    JobManagerClient.DeleteWorker(this._jw.userID);
                    INTAPS.WSIS.Job.Client.MainForm.MainButtonGrid.UpdateLayout();
                    base.Close();
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error saving worker data", exception);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (this.employee.GetEmployeeID() == -1)
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select an employee");
            }
            else
            {
                List<JobWorkerRole> list = new List<JobWorkerRole>();
                List<int> appTypes = new List<int>();
                foreach (CheckBox box in this.groupRoles.Controls)
                {
                    if (box.Checked)
                    {
                        list.Add((JobWorkerRole)Enum.Parse(typeof(JobWorkerRole), (string)box.Tag));
                    }
                }
                if (list.Count == 0)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Please assign a role for this worker");
                    return;
                }
                foreach (CheckBox box in this.flowAppTypes.Controls)
                {
                    if (box.Checked)
                        appTypes.Add((int)box.Tag);
                }
                if (appTypes.Count == 0)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserError("Please assign a process for this worker");
                    return;
                }
                try
                {
                    JobWorker worker = new JobWorker();
                    worker.employeeID = this.employee.GetEmployeeID();
                    worker.active = true;
                    worker.roles = list.ToArray();
                    worker.jobTypes = appTypes.ToArray();
                    if (this._jw == null)
                    {
                        JobManagerClient.CreateJobWorker(worker);
                    }
                    else
                    {
                        JobManagerClient.UpdateJobWorker(worker);
                    }
                    this._jw = worker;
                    INTAPS.WSIS.Job.Client.MainForm.MainButtonGrid.UpdateLayout();
                    base.Close();
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error saving worker data", exception);
                }
            }

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void flowAppTypes_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupTypes_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.DarkOrange, Color.DarkOrange);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Clear text and border
                g.Clear(this.BackColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }
    }
}

