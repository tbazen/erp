
    using INTAPS.UI.ButtonGrid;
    using System;
namespace INTAPS.WSIS.Job.Client
{

    internal class KeyedButton : ButtonGridBodyButtonItem, IEquatable<KeyedButton>
    {
        public string key;

        public KeyedButton(string key) : base(false)
        {
            this.key = key;
        }

        public bool Equals(KeyedButton other)
        {
            if (string.IsNullOrEmpty(this.key) || string.IsNullOrEmpty(other.key))
            {
                return (this == other);
            }
            return this.key.Equals(other.key);
        }

        public override bool Equals(object obj)
        {
            return ((obj is ButtonGridBodyButtonItem) && this.Equals((KeyedButton) obj));
        }
    }
}

