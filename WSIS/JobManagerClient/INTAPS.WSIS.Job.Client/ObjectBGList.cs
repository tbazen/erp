
    using INTAPS.UI.ButtonGrid;
    using System;
    using System.Collections;
    using System.Collections.Generic;
namespace INTAPS.WSIS.Job.Client
{

    internal abstract class ObjectBGList<ItemType> : ObjectBGListBase, IButtonChildSource, IEnumerable<ButtonGridItem>, IEnumerable
    {
        private string _query;

        public ObjectBGList(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parentItem) : base(grid, parentItem)
        {
        }

        public abstract ButtonGridItem[] CreateButton(ItemType obj);
        protected virtual void CreateButtonItems()
        {
            base._items = new List<ButtonGridItem>();
            base._grid.SuspendPaint();
            try
            {
                foreach (ItemType local in this.LoadData(this._query))
                {
                    ButtonGridItem[] itemArray;
                    try
                    {
                        itemArray = this.CreateButton(local);
                    }
                    catch (Exception exception)
                    {
                        itemArray = new ButtonGridItem[] { ButtonGridHelper.CreateButton("", exception.Message) };
                    }
                    base.AddToParent(itemArray);
                    base._items.AddRange(itemArray);
                }
            }
            finally
            {
                base._grid.ResumePaint();
            }
        }

        public IEnumerator<ButtonGridItem> GetEnumerator()
        {
            this.CreateButtonItems();
            return base._items.GetEnumerator();
        }

        public abstract List<ItemType> LoadData(string query);
        public void SetQuery(string query)
        {
            this._query = query;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return base._items.GetEnumerator();
        }

        public abstract bool Searchable { get; }
        public abstract string searchLabel{get;}

    }
}

