
    using INTAPS.UI.ButtonGrid;
    using System;
    using System.Collections.Generic;
namespace INTAPS.WSIS.Job.Client
{

    internal abstract class ObjectBGListBase
    {
        protected INTAPS.UI.ButtonGrid.ButtonGrid _grid;
        protected List<ButtonGridItem> _items = null;
        protected ButtonGridItem _parentItem;

        public ObjectBGListBase(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parentItem)
        {
            this._grid = grid;
            this._parentItem = parentItem;
        }

        protected void AddToParent(params ButtonGridItem[] items)
        {
            foreach (ButtonGridItem item in items)
            {
                if (this._parentItem == null)
                {
                    this._grid.RelateWithParent(this._grid.rootButtonItem, item);
                }
                else
                {
                    this._grid.RelateWithParent(this._parentItem, item);
                }
            }
        }
    }
}

