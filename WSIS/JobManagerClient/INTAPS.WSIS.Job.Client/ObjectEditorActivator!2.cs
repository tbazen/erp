
    using System;
    using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    internal class ObjectEditorActivator<ObjectType, FormType> : IButtonGridItemActivator where FormType: Form
    {
        private ObjectType _obj;

        public ObjectEditorActivator(ObjectType obj)
        {
            this._obj = obj;
        }

        public DialogResult Activate()
        {
            Form form = (Form) typeof(FormType).GetConstructor(new System.Type[] { typeof(ObjectType) }).Invoke(new object[] { this._obj });
            return form.ShowDialog();
        }
    }
}

