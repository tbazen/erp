
    using INTAPS.SubscriberManagment;
    using INTAPS.SubscriberManagment.Client;
    using INTAPS.UI;
    using INTAPS.UI.ButtonGrid;
    using INTAPS.WSIS.Job;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    internal class SubscriberList : ObjectBGList<Subscriber>
    {
        class SubscriberService
        {
            public Subscriber subscription;
            public int applicationType;
            public SubscriberService(Subscriber s, int type)
            {
                this.subscription = s;
                this.applicationType = type;
            }
        }

        int  _serviceType;
        public SubscriberList(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parent, int serviceType)
            : base(grid, parent)
        {
            _serviceType = serviceType;
        }

        public override ButtonGridItem[] CreateButton(Subscriber obj)
        {
            ButtonGridBodyButtonItem item = ButtonGridHelper.CreateButton("", obj.name, obj.customerCode);
            ButtonGridHelper.SetButtonBackColor(item, Color.RosyBrown);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FontBrush,Brushes.Black);
            new EventActivator<SubscriberService>(item, new SubscriberService(obj,_serviceType)).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, SubscriberService>(this.SubscriberList_Clicked);
            return new ButtonGridItem[] { item };
        }

        public override List<Subscriber> LoadData(string query)
        {
            List<Subscriber> list = new List<Subscriber>();
            if (!string.IsNullOrEmpty(query))
            {
                int num;
                SubscriberSearchResult[] resultArray = SubscriberManagmentClient.GetCustomers(query, -1, 0, 30, out num);
                foreach (SubscriberSearchResult result in resultArray)
                {
                    if (result.subscriber != null)
                    {
                        list.Add(result.subscriber);
                    }
                }
            }
            return list;
        }

        private void SubscriberList_Clicked(ButtonGridBodyButtonItem t, SubscriberService z)
        {
            try
            {
                IJobRuleClientHandler client = JobManagerClient.getJobRuleEngineClientHandler(z.applicationType);
                if (client == null)
                    throw new ClientServer.ServerUserMessage("Job rule client handler " + z.applicationType + " not found");
                NewApplicationForm f = client.getNewApplicationForm(-1, z.subscription, null);
                if (f.ShowDialog(UIFormApplicationBase.MainForm) != System.Windows.Forms.DialogResult.OK)
                    return;
                new HTMLJobForm(f.createJobID).Show(UIFormApplicationBase.MainForm);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                string msg = "";
                while (ex != null)
                {
                    msg += "\n" + ex.Message + "\n" + ex.StackTrace;
                    ex = ex.InnerException;
                }
                MessageBox.Show(msg);

            }
        }

        public override bool Searchable
        {
            get
            {
                return true;
            }
        }

        public override string searchLabel
        {
            get { return "Customer Code:"; }
        }

    }
    internal class SubscriptionList : ObjectBGList<Subscription>
    {
        class SubscriptionService
        {
            public Subscription subscription;
            public int applicationType;
            public SubscriptionService(Subscription s, int type)
            {
                this.subscription = s;
                this.applicationType = type;
            }
        }

        int _serviceType;
        public SubscriptionList(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parent, int serviceType)
            : base(grid, parent)
        {
            _serviceType = serviceType;
        }

        public override ButtonGridItem[] CreateButton(Subscription obj)
        {
            ButtonGridBodyButtonItem item = ButtonGridHelper.CreateButton("", obj.subscriber.name, obj.contractNo);
            ButtonGridHelper.SetButtonBackColor(item, Color.RosyBrown);
            item.ApperanceBody.StylesCollection.DefaultStyle.Add(StyleType.FontBrush, Brushes.Black);
            new EventActivator<SubscriptionService>(item, new SubscriptionService(obj, _serviceType)).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, SubscriptionService>(this.SubscriberList_Clicked);
            return new ButtonGridItem[] { item };
        }

        public override List<Subscription> LoadData(string query)
        {
            List<Subscription> list = new List<Subscription>();
            if (!string.IsNullOrEmpty(query))
            {
                int num;
                SubscriberSearchResult[] resultArray = SubscriberManagmentClient.GetSubscriptions(query, SubscriberSearchField.ConstactNo | SubscriberSearchField.CustomerName,false,-1,-1,-1, 0, 30, out num);
                foreach (SubscriberSearchResult result in resultArray)
                {
                    if (result.subscription!= null)
                    {
                        result.subscription.subscriber = result.subscriber;
                        list.Add(result.subscription);
                    }
                }
            }
            return list;
        }

        private void SubscriberList_Clicked(ButtonGridBodyButtonItem t, SubscriptionService z)
        {
            IJobRuleClientHandler client = JobManagerClient.getJobRuleEngineClientHandler(z.applicationType);
            if (client == null)
                throw new ClientServer.ServerUserMessage("Job rule client handler " + z.applicationType + " not found");
            NewApplicationForm f = client.getNewApplicationForm(-1, z.subscription.subscriber, z.subscription);
            if (f.ShowDialog(UIFormApplicationBase.MainForm) != System.Windows.Forms.DialogResult.OK)
                return;
            new HTMLJobForm(f.createJobID).Show(UIFormApplicationBase.MainForm);
        }

        public override bool Searchable
        {
            get
            {
                return true;
            }
        }

        public override string searchLabel
        {
            get { return "Contract No:"; }
        }
    }

}

