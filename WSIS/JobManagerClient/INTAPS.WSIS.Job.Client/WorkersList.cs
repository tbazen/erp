
    using INTAPS.Payroll;
    using INTAPS.Payroll.Client;
    using INTAPS.UI.ButtonGrid;
    using INTAPS.WSIS.Job;
    using System;
    using System.Collections.Generic;
namespace INTAPS.WSIS.Job.Client
{

    internal class WorkersList : ObjectBGList<object>
    {
        public WorkersList(INTAPS.UI.ButtonGrid.ButtonGrid grid, ButtonGridItem parent) : base(grid, parent)
        {
        }

        private void ActivateClicked(ButtonGridBodyButtonItem t, JobWorker z)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ButtonGridItem[] CreateButton(object obj)
        {
            ButtonGridBodyButtonItem item;
            if (obj == null)
            {
                item = ButtonGridHelper.CreateButton("WL_NewItem", "New Worker");
                item.Tag = new ObjectEditorActivator<JobWorker, JobWorkerForm>(null);
                return new ButtonGridItem[] { item };
            }
            List<ButtonGridItem> list = new List<ButtonGridItem>();
            JobWorker worker = obj as JobWorker;
            Employee employee = PayrollClient.GetEmployee(worker.employeeID);
            item = ButtonGridHelper.CreateButton("", employee.employeeName);
            ButtonGridHelper.AddLargeImageRightBottom(item, ButtonGridHelper.LoadImageFromServer(employee.picture, 120, 120));
            list.Add(item);
            item.Tag = new ObjectEditorActivator<JobWorker, JobWorkerForm>(worker);
            return list.ToArray();
        }

        public override List<object> LoadData(string query)
        {
            List<object> list = new List<object>();
            list.AddRange((object[]) JobManagerClient.GetAllWorkers());
            list.Add(null);
            return list;
        }

        private void WorkersList_Clicked(ButtonGridBodyButtonItem t, JobWorker z)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override bool Searchable
        {
            get
            {
                return false;
            }
        }

        public override string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }
}

