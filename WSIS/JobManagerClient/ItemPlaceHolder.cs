﻿using System;
using System.Collections.Generic;
using System.Text;
using BIZNET.iERP;

namespace INTAPS.WSIS.Job.Client
{
    public class ItemPlaceHolder : INTAPS.UI.ObjectPlaceHolderTypedID<TransactionItems,string>
    {
        public ItemPlaceHolder()
            : base("")
        {
        }

        protected override TransactionItems GetObjectBYID(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;
            return BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(id);
        }

        protected override string GetObjectID(TransactionItems obj)
        {
            return obj.Code;
        }

        protected override string GetObjectString(TransactionItems obj)
        {
            if (obj == null)
                return "";
            return obj.Name+" - "+obj.Code;
        }

        protected override TransactionItems PickObject()
        {
            throw new NotImplementedException();
        }

        protected override TransactionItems[] SearchObject(string query)
        {
            int n;
            return BIZNET.iERP.Client.iERPTransactionClient.SearchTransactionItems(0, 10, new object[] { query }, new string[] { "Code" }, out n);
        }
        public override bool pickable
        {
            get { return false; }
        }
    }
}
