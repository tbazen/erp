﻿namespace INTAPS.WSIS.Job.Client
{
    partial class JobHistoryViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.browserToolBar = new INTAPS.UI.HTML.BowserController();
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser.Location = new System.Drawing.Point(0, 25);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(749, 628);
            this.browser.StyleSheetFile = "jobstyle.css";
            this.browser.TabIndex = 0;
            // 
            // browserToolBar
            // 
            this.browserToolBar.BackColor = System.Drawing.SystemColors.Menu;
            this.browserToolBar.Location = new System.Drawing.Point(0, 0);
            this.browserToolBar.Name = "browserToolBar";
            this.browserToolBar.ShowBackForward = true;
            this.browserToolBar.ShowRefresh = true;
            this.browserToolBar.Size = new System.Drawing.Size(749, 25);
            this.browserToolBar.TabIndex = 1;
            this.browserToolBar.Text = "bowserController1";
            // 
            // JobHistoryViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 653);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.browserToolBar);
            this.Name = "JobHistoryViewer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Invoice";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.HTML.ControlBrowser browser;
        private UI.HTML.BowserController browserToolBar;
    }
}