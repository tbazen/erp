﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Web.UI.HtmlControls;
using INTAPS.UI.HTML;

namespace INTAPS.WSIS.Job.Client
{
    public partial class JobHistoryViewer : Form
    {
        public JobHistoryViewer(JobStatusHistory[] jobHistory, JobAppointment [] jobAppointments)
        {
            InitializeComponent();
            browserToolBar.SetBrowser(browser);


            HtmlTable historyTable = new HtmlTable();

            HTMLBuilder.CreateHtmlRow(historyTable
                , "tableHeader1"
                , HTMLBuilder.CreateTextCell("No.")
                , HTMLBuilder.CreateTextCell("Date")
                , HTMLBuilder.CreateTextCell("Worker")
                , HTMLBuilder.CreateTextCell("Stage")
                , HTMLBuilder.CreateTextCell("Forwared to")
                , HTMLBuilder.CreateTextCell("Note")
                , HTMLBuilder.CreateTextCell("Duration")
                );
            int no = 1;
            for (int i = 0; i < jobHistory.Length; i++)
            {
                if (i == 0)
                    continue;
                string durationString = i == 0 ? "" : JobProcessRules.GetDurationString(jobHistory[i].changeDate.Subtract(jobHistory[i-1].changeDate));
                HTMLBuilder.AddAlternatingStyleRow(historyTable, HTMLBuilder.CreateHtmlRow(
                   HTMLBuilder.CreateTextCell(no.ToString())
                   , HTMLBuilder.CreateTextCell(jobHistory[i].changeDate.ToString(HTMLJobForm.DATE_TIME_FORMAT))
                   , HTMLBuilder.CreateTextCell(Payroll.Client.PayrollClient.GetEmployee(JobManagerClient.GetWorker(jobHistory[i].agent).employeeID).employeeName)
                   , HTMLBuilder.CreateTextCell(JobManagerClient.getJobStatus(jobHistory[i].oldStatus).name)
                   , HTMLBuilder.CreateTextCell(JobManagerClient.getJobStatus(jobHistory[i].newStatus).name)
                   , HTMLBuilder.CreateTextCell(jobHistory[i].note)
                   , HTMLBuilder.CreateTextCell(durationString)
                   ), new string[] { "evenRow1", "oddRow1" }
                   );
                no++;
            }

            HtmlTable appointmentHistoryTable = new HtmlTable();
            HTMLBuilder.CreateHtmlRow(appointmentHistoryTable
                , "tableHeader1"
                , HTMLBuilder.CreateTextCell("No.")
                , HTMLBuilder.CreateTextCell("Date")
                , HTMLBuilder.CreateTextCell("Worker")
                , HTMLBuilder.CreateTextCell("Stage")
                , HTMLBuilder.CreateTextCell("Forwared to")
                , HTMLBuilder.CreateTextCell("Note")
                , HTMLBuilder.CreateTextCell("Duration")
                );
            no = 1;
            for (int i = 0; i < jobHistory.Length; i++)
            {
                if (i == 0)
                    continue;
                string durationString = i == 0 ? "" : JobProcessRules.GetDurationString(jobHistory[i].changeDate.Subtract(jobHistory[i - 1].changeDate));
                HTMLBuilder.AddAlternatingStyleRow(appointmentHistoryTable, HTMLBuilder.CreateHtmlRow(
                   HTMLBuilder.CreateTextCell(no.ToString())
                   , HTMLBuilder.CreateTextCell(jobHistory[i].changeDate.ToString(HTMLJobForm.DATE_TIME_FORMAT))
                   , HTMLBuilder.CreateTextCell(Payroll.Client.PayrollClient.GetEmployee(JobManagerClient.GetWorker(jobHistory[i].agent).employeeID).employeeName)
                   , HTMLBuilder.CreateTextCell(JobManagerClient.getJobStatus(jobHistory[i].oldStatus).name)
                   , HTMLBuilder.CreateTextCell(JobManagerClient.getJobStatus(jobHistory[i].newStatus).name)
                   , HTMLBuilder.CreateTextCell(jobHistory[i].note)
                   , HTMLBuilder.CreateTextCell(durationString)
                   ), new string[] { "evenRow1", "oddRow1" }
                   );
                no++;
            }
            browser.LoadTextPage("History",HTMLBuilder.ControlToString(historyTable));
        }
    }
}
