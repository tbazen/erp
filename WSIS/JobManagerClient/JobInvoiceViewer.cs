﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.HTML;

namespace INTAPS.WSIS.Job.Client
{
    public partial class JobInvoiceViewer : Form,IHTMLBuilder
    {
        string _html;
        int _bomID;
        public JobInvoiceViewer(int bomID)
        {
            InitializeComponent();
            browserToolBar.SetBrowser(browser);
            _bomID = bomID;
            Build();
            browser.LoadControl(this);
        }

        public void Build()
        {
            JobBillOfMaterial bom = JobManagerClient.GetBillOfMaterial(_bomID);
            _html=JobManagerClient.GetInvoicePreview(bom);
        }

        public string GetOuterHtml()
        {
            return _html;
        }

        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return false;
        }

        public void SetHost(IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "BOM"; }
        }
    }
}
