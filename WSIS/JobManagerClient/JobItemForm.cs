﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BIZNET.iERP;
using BIZNET.iERP.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class JobItemForm : Form
    {
        public JobItemSetting jobItem;
        TransactionItems item = null;
        public JobItemForm(JobItemSetting jobItem)
        {
            InitializeComponent();
            if (jobItem != null)
            {
                item = iERPTransactionClient.GetTransactionItems(jobItem.itemCode);
                textItemCode.Text = item.Code;
                textItemName.Text = item.Name;
                textAmount.Text = (jobItem.value*100).ToString("0.0");
                textItemCode.ReadOnly = true;
                if (jobItem.type == JobItemType.MaterialCostMultiplier)
                    comboType.SelectedIndex = 0;
                else
                    comboType.SelectedIndex = 1;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (item == null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter item");
                return;
            }
            double val;
            if(!INTAPS.UI.Helper.ValidateDoubleTextBox(textAmount,"Please enter value",out val))
                return;
            try
            {
                jobItem = new JobItemSetting();
                jobItem.itemCode = item.Code;
                jobItem.type = JobItemType.MaterialCostMultiplier;
                jobItem.value = val / 100;

                if (comboType.SelectedIndex == 0)
                    jobItem.type=JobItemType.MaterialCostMultiplier;
                else
                    jobItem.type = JobItemType.TotalServiceMultiplier;
                JobManagerClient.setJobItem(jobItem);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to create multiplier item",ex);
            }

        }
        private void textItemCode_Validated(object sender, EventArgs e)
        {
            updateItem();
        }

        private void updateItem()
        {
            TransactionItems titem = iERPTransactionClient.GetTransactionItems(textItemCode.Text);
            if (titem != null)
                item = titem;
            textItemCode.Text = item == null ? "" : item.Code;
            textItemName.Text = item == null ? "" : item.Name;
        }

        private void textItemCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                updateItem();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
