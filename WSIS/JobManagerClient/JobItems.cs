﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public partial class JobItems : SimpleObjectList<JobItemSetting>
    {
        public JobItems()
        {
            this.Height = 450;   
        }
        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }
        protected override JobItemSetting EditObject(JobItemSetting obj)
        {
            JobItemForm jif = new JobItemForm(obj);
            if (jif.ShowDialog(this) == DialogResult.OK)
            {
                return jif.jobItem;
            }
            return null;
        }
        protected override JobItemSetting CreateObject()
        {
            JobItemForm jif = new JobItemForm(null);
            if (jif.ShowDialog(this) == DialogResult.OK)
            {
                return jif.jobItem;
            }
            return null;
        }

        protected override void DeleteObject(JobItemSetting obj)
        {
            JobManagerClient.deleteJobItem(obj.itemCode);
        }

        protected override JobItemSetting[] GetObjects()
        {
            return JobManagerClient.getAllJobItems();
        }

        protected override string GetObjectString(JobItemSetting obj)
        {
            BIZNET.iERP.TransactionItems item=BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(obj.itemCode);
            return item.Code+":"+item.Name+ "-" + (obj.value*100).ToString("")+"%";
        }

        protected override string ObjectTypeName
        {
            get { return "Bill of Quantity Multipliers"; }
        }
    }
}
