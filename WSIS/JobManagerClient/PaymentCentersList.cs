﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.UI.ButtonGrid;

namespace INTAPS.WSIS.Job.Client
{
     class PaymentCentersList : ObjectBGList<PaymentCenter>
    {
         public PaymentCentersList(ButtonGrid grid, ButtonGridItem parent)
             : base(grid, parent)
         {
         }
         double getCasherBalance(PaymentCenter _center)
         {
             DateTime d = JobManagerClient.getLastCashHandoverDocument(_center.summaryAccountID).DocumentDate;
             return AccountingClient.GetNetBalanceAsOf(_center.casherAccountID, DateTime.Now.Date.AddDays(1))
                - AccountingClient.GetNetBalanceAsOf(_center.casherAccountID,d);
         }
        public override UI.ButtonGrid.ButtonGridItem[] CreateButton(PaymentCenter obj)
        {
            ButtonGridBodyButtonItem item = ButtonGridHelper.CreateButtonWithColor("", obj.centerName, System.Drawing.Color.DarkBlue);
            EventActivator<PaymentCenter> a=new EventActivator<PaymentCenter>(item, obj);
            item.Tag =a;
            a.Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, PaymentCenter>(PaymentCentersList_Clicked);
            return new ButtonGridItem[] { item };
        }

        void PaymentCentersList_Clicked(ButtonGridBodyButtonItem t, PaymentCenter z)
        {
            IJobRuleClientHandler client = JobManagerClient.getJobRuleEngineClientHandler(StandardJobTypes.CASH_HANDOVER);
            if (client == null)
                throw new ClientServer.ServerUserMessage("Job rule client handler " + StandardJobTypes.CASH_HANDOVER + " not found");
            NewApplicationForm f = client.getApplicationFormType().GetConstructor(new Type[] { typeof(int) }).Invoke(new object[]{z.casherAccountID}) as NewApplicationForm;
            if (f.ShowDialog(INTAPS.UI.UIFormApplicationBase.MainForm) != System.Windows.Forms.DialogResult.OK)
                return;
            new HTMLJobForm(f.createJobID).Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
        
        public override List<PaymentCenter> LoadData(string query)
        {

            return new List<PaymentCenter>(INTAPS.SubscriberManagment.Client.SubscriberManagmentClient.GetAllPaymentCenters());
        }

        public override bool Searchable
        {
            get { return false; }
        }

        public override string searchLabel
        {
            get { throw new NotImplementedException(); }
        }
    }
}
