
using INTAPS.ClientServer.Client;
using INTAPS.Payroll.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.WSIS.Job.Client.Properties;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public partial class Login : BIZNET.iERP.Client.LoginWithUpdate
    {

        private Container components = null;

        public bool logedin;
        private bool m_boolValidUser = false;



        public Login()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string url = ConfigurationManager.AppSettings["Server"];
                ApplicationClient.Connect(url, this.txtUserName.Text, this.txtPassword.Text);
                SecurityClient.Connect(url);
                INTAPS.Accounting.Client.AccountingClient.Connect(url);
                PayrollClient.Connect(url);
                BIZNET.iERP.Client.iERPTransactionClient.Connect(url);
                SubscriberManagmentClient.Connect(url);
                JobManagerClient.Connect(url);
                Settings.Default.User = this.txtUserName.Text;
                Settings.Default.Save();
                this.logedin = true;
                checkClientUpdate();
                base.Close();
            }
            catch (Exception exception)
            {
                String msg = exception.Message;
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                    msg += "\n->" + exception.Message;
                }
                MessageBox.Show(this, msg, "Could not login", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.txtUserName.Focus();
                this.txtUserName.Text = "";
                this.txtPassword.Text = "";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        protected override void OnLoad(EventArgs e)
        {
            this.txtUserName.Text = Settings.Default.User;
            base.OnLoad(e);
        }

        public void TryLogin()
        {
            this.logedin = false;
            base.ShowDialog();
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
        }

        public bool ValidUser
        {
            get
            {
                return this.m_boolValidUser;
            }
        }
    }
}

