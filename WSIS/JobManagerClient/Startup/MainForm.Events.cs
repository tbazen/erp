﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{
    partial class MainForm
    {
        private void NewLineApplicationClicked(ButtonGridBodyButtonItem t, int applicationType)
        {
            
            JobCustomerSelector selector = new JobCustomerSelector(true);
            if (selector.ShowDialog(this) != DialogResult.OK)
                return;
            IJobRuleClientHandler client=JobManagerClient.getJobRuleEngineClientHandler(applicationType);
            NewApplicationForm f = client.getNewApplicationForm(-1,selector.selectedCustomer,null);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                new HTMLJobForm(f.createJobID).Show(this);
            }
        }
        private void VoidReceiptClicked(ButtonGridBodyButtonItem t, int applicationType)
        {
            IJobRuleClientHandler client = JobManagerClient.getJobRuleEngineClientHandler(applicationType);
            NewApplicationForm f = client.getNewApplicationForm(-1, null, null);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                new HTMLJobForm(f.createJobID).Show(this);
            }
        }
        
        private void buttonGrid_LeafButtonClicked(INTAPS.UI.ButtonGrid.ButtonGrid t, ButtonGridItem z)
        {
            ButtonGridBodyButtonItem item = z as ButtonGridBodyButtonItem;
            if (item != null)
            {
                IButtonGridItemActivator tag = item.Tag as IButtonGridItemActivator;
                if (tag != null)
                {
                    try
                    {
                        tag.Activate();
                    }
                    catch (Exception exception)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error activating this item", exception);
                    }
                }
            }
        }
    }
}
