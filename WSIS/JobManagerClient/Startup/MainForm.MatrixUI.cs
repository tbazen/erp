﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.UI.ButtonGrid;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{
    partial class MainForm
    {

        private ButtonGridGroupButtonItem _newJobGroup;
        private ButtonGridBodyButtonItem _browseCustomers;
        private ButtonGridBodyButtonItem _newLineApplication;
        private ButtonGridBodyButtonItem _connectionModification;
        private ButtonGridBodyButtonItem _customerSponsoredExpansion = null;
        
        private ButtonGridBodyButtonItem _ownershipTransfer;
        private ButtonGridBodyButtonItem _generalSell;
        private ButtonGridBodyButtonItem _deleteBills;
        private ButtonGridBodyButtonItem _closeCreditSchemes;
        private ButtonGridBodyButtonItem _exemptBillItems;
        private ButtonGridBodyButtonItem _billingCredit;
        private ButtonGridBodyButtonItem _billingDeposit;
        private ButtonGridBodyButtonItem _returnMeter;
        private ButtonGridBodyButtonItem _readingCorrection;
        private ButtonGridBodyButtonItem _liquidWasteDisposal=null;
        private ButtonGridBodyButtonItem _hydrantSupplyService = null;
        private ButtonGridBodyButtonItem _ruleViolation = null;
        private ButtonGridBodyButtonItem _customerRuleViolation = null;
        private ButtonGridBodyButtonItem _internalServices;
        private ButtonGridBodyButtonItem _otherServices;
        private ButtonGridBodyButtonItem _workers;
        
        private ButtonGridGroupButtonItem _activeItemsGroup;
        private INTAPS.UI.ButtonGrid.ButtonGrid buttonGrid;

        public static INTAPS.UI.ButtonGrid.ButtonGrid MainButtonGrid = null;

        private void InitializeButtonGrid()
        {
            MainButtonGrid = this.buttonGrid = CreateButtonGrid();

            base.Controls.Add(this.buttonGrid);
            this.buttonGrid.BringToFront();
            this.buttonGrid.LeafButtonClicked += new GenericEventHandler<INTAPS.UI.ButtonGrid.ButtonGrid, ButtonGridItem>(this.buttonGrid_LeafButtonClicked);
            this._newJobGroup = ButtonGridHelper.CreateModuleGroupButton("1", "Tools");
            this._activeItemsGroup = ButtonGridHelper.CreateModuleGroupButton("2", "Active Jobs");

            this._newLineApplication = ButtonGridHelper.CreateCommandButton("", "New Line");
            new EventActivator<int>(this._newLineApplication, StandardJobTypes.NEW_LINE).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);

            this._browseCustomers = ButtonGridHelper.CreateCommandButton("", "Customers List");
            new EventActivator<int>(this._browseCustomers, 0).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(customerList_Clicked);

            this._ownershipTransfer = ButtonGridHelper.CreateCommandButton("", "Transfer Connection Ownership");
            this.buttonGrid.AddButtonGridItem(this._ownershipTransfer, new SubscriptionList(this.buttonGrid, this._ownershipTransfer, StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER));

            var jobType = JobManagerClient.getJobType(StandardJobTypes.CONNECTION_MAINTENANCE);
            if (jobType != null)
            {
                this._connectionModification = ButtonGridHelper.CreateCommandButton("", jobType.name);
                this.buttonGrid.AddButtonGridItem(this._connectionModification, new SubscriptionList(this.buttonGrid, this._connectionModification, StandardJobTypes.CONNECTION_MAINTENANCE));
            }
            else
                this._connectionModification = null;
            
            
            this._readingCorrection = ButtonGridHelper.CreateCommandButton("", "Reading Correction");
            this.buttonGrid.AddButtonGridItem(this._readingCorrection, new SubscriptionList(this.buttonGrid, this._readingCorrection, StandardJobTypes.READING_CORRECTION));

            
            this._billingCredit= ButtonGridHelper.CreateCommandButton("", "Billing Credit");
            this.buttonGrid.AddButtonGridItem(this._billingCredit, new SubscriberList(this.buttonGrid, this._billingCredit, StandardJobTypes.BILLING_CERDIT));

            this._billingDeposit = ButtonGridHelper.CreateCommandButton("", "Billing Deposit");
            this.buttonGrid.AddButtonGridItem(this._billingDeposit, new SubscriberList(this.buttonGrid, this._billingDeposit, StandardJobTypes.BILLING_DEPOSIT));

            this._deleteBills = ButtonGridHelper.CreateCommandButton("", "Delete Bills");
            this.buttonGrid.AddButtonGridItem(this._deleteBills, new SubscriberList(this.buttonGrid, this._deleteBills, StandardJobTypes.BATCH_DELETE_BILLS));

            this._closeCreditSchemes = ButtonGridHelper.CreateCommandButton("", "Close Credit Schemes");
            this.buttonGrid.AddButtonGridItem(this._closeCreditSchemes, new SubscriberList(this.buttonGrid, this._closeCreditSchemes, StandardJobTypes.CLOSE_CREDIT_SCHEME));

            this._exemptBillItems = ButtonGridHelper.CreateCommandButton("", "Exempt Bill Items");
            this.buttonGrid.AddButtonGridItem(this._exemptBillItems, new SubscriberList(this.buttonGrid, this._exemptBillItems, StandardJobTypes.EXEMPT_BILL_ITEMS));
            
            this._returnMeter = ButtonGridHelper.CreateCommandButton("", "Return Meter");
            this.buttonGrid.AddButtonGridItem(this._returnMeter, new SubscriptionList(this.buttonGrid, this._returnMeter, StandardJobTypes.RETURN_METER));

            intializeInternalServiceButtonGridItems();
            initializeOtherServices();
            
            this._workers = ButtonGridHelper.CreateCommandButton("", "Workers List");
            this.buttonGrid.AddButtonGridItem(this._workers, new WorkersList(this.buttonGrid, this._workers));

            List<ButtonGridItem> items = new List<ButtonGridItem>();
            if (this._newJobGroup != null) items.Add(_newJobGroup);
            if(this._browseCustomers!=null) items.Add(_browseCustomers);
            if(this._newLineApplication!=null) items.Add(_newLineApplication);
            if(this._ownershipTransfer!=null) items.Add(_ownershipTransfer);
            if(this._connectionModification!=null) items.Add(_connectionModification);
            if(this._readingCorrection!=null) items.Add(_readingCorrection);
            if(this._billingCredit!=null) items.Add(_billingCredit);
            if (this._closeCreditSchemes != null) items.Add(_closeCreditSchemes);            
            if(this._billingDeposit!=null) items.Add(_billingDeposit);
            if(this._deleteBills!=null) items.Add(_deleteBills);
            if (this._exemptBillItems != null) items.Add(_exemptBillItems);
            if(this._returnMeter!=null) items.Add(_returnMeter);
            if (this._otherServices != null) items.Add(_otherServices);
            if(this._internalServices!=null) items.Add(_internalServices);
            if (this._workers != null) items.Add(_workers);
            if (this._activeItemsGroup != null) items.Add(_activeItemsGroup);
            this.buttonGrid.AddButtonGridItem(this.buttonGrid.rootButtonItem
                , new CombinedButtonList(new IButtonChildSource[] { new ArrayChildSource
                    (this.buttonGrid, this.buttonGrid.rootButtonItem, items.ToArray())
                , new JobList(this.buttonGrid, this.buttonGrid.rootButtonItem)}
                )
                );
            this.buttonGrid.setFilterBox();
            this.buttonGrid.UpdateLayout();
        }

        private void intializeInternalServiceButtonGridItems()
        {
            ButtonGridBodyButtonItem voidReceipt;
            ButtonGridBodyButtonItem cashHandover;
            ButtonGridBodyButtonItem editCustomerData;
            ButtonGridBodyButtonItem disconnectLine;
            ButtonGridBodyButtonItem reconnect;

            cashHandover = ButtonGridHelper.CreateCommandButton("", "Cash Handover");
            this.buttonGrid.AddButtonGridItem(cashHandover, new PaymentCentersList(this.buttonGrid, cashHandover));

            editCustomerData = ButtonGridHelper.CreateCommandButton("", "Edit Customer Data");
            new EventActivator<int>(editCustomerData, StandardJobTypes.EDIT_CUTOMER_DATA).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);

            voidReceipt = ButtonGridHelper.CreateCommandButton("", "Void Receipt");
            new EventActivator<int>(voidReceipt, StandardJobTypes.VOID_RECEIPT).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.VoidReceiptClicked);

            disconnectLine = ButtonGridHelper.CreateCommandButton("", "Disconnet Lines");
            new EventActivator<int>(disconnectLine, StandardJobTypes.BATCH_DISCONNECT).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.VoidReceiptClicked);

            reconnect= ButtonGridHelper.CreateCommandButton("", "Reconnect a Line");
            new EventActivator<int>(reconnect, StandardJobTypes.BATCH_RECONNECT).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.VoidReceiptClicked);

            this._internalServices = ButtonGridHelper.CreateCommandButton("", "Internal Services");
            this.buttonGrid.AddButtonGridItem(this._internalServices, new ArrayChildSource(this.buttonGrid, _internalServices,
                editCustomerData,
                voidReceipt,
                cashHandover,
                disconnectLine,
                reconnect
                ));
        }

        private void initializeOtherServices()
        {

            if (JobManagerClient.getJobRuleEngineClientHandler(StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK) != null)
            {
                this._customerSponsoredExpansion = ButtonGridHelper.CreateCommandButton("", "Customer Sponsored Network Expansion");
                new EventActivator<int>(this._customerSponsoredExpansion, StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);
            }

            if (JobManagerClient.getJobRuleEngineClientHandler(StandardJobTypes.LIQUID_WASTE_DISPOSAL) != null)
            {
                this._liquidWasteDisposal = ButtonGridHelper.CreateCommandButton("", JobManagerClient.getJobType(StandardJobTypes.LIQUID_WASTE_DISPOSAL).name);
                new EventActivator<int>(this._liquidWasteDisposal, StandardJobTypes.LIQUID_WASTE_DISPOSAL).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);
            }

            if (JobManagerClient.getJobRuleEngineClientHandler(StandardJobTypes.HYDRANT_SERVICE) != null)
            {
                this._hydrantSupplyService = ButtonGridHelper.CreateCommandButton("", JobManagerClient.getJobType(StandardJobTypes.HYDRANT_SERVICE).name);
                new EventActivator<int>(this._hydrantSupplyService, StandardJobTypes.HYDRANT_SERVICE).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);
            }

            if (JobManagerClient.getJobRuleEngineClientHandler(StandardJobTypes.CONNECTION_PENALITY) != null)
            {
                this._ruleViolation = ButtonGridHelper.CreateCommandButton("", "Connection Rule Violations");
                this.buttonGrid.AddButtonGridItem(this._ruleViolation, new SubscriptionList(this.buttonGrid, this._ruleViolation, StandardJobTypes.CONNECTION_PENALITY));

                this._customerRuleViolation = ButtonGridHelper.CreateCommandButton("", "Customer Rule Violations");
                new EventActivator<int>(this._customerRuleViolation, StandardJobTypes.CONNECTION_PENALITY).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);

            }
            this._generalSell = ButtonGridHelper.CreateCommandButton("", "General Paid Service");
            new EventActivator<int>(this._generalSell, StandardJobTypes.GENERAL_SELL).Clicked += new GenericEventHandler<ButtonGridBodyButtonItem, int>(this.NewLineApplicationClicked);

            this._otherServices = ButtonGridHelper.CreateCommandButton("", "Other Services");
            List<ButtonGridBodyButtonItem> buttons = new List<ButtonGridBodyButtonItem>();

            if (_customerSponsoredExpansion != null) buttons.Add(_customerSponsoredExpansion);
            if (_liquidWasteDisposal != null) buttons.Add(_liquidWasteDisposal);
            if (_hydrantSupplyService != null) buttons.Add(_hydrantSupplyService);
            if (_ruleViolation != null) buttons.Add(_ruleViolation);
            if (_customerRuleViolation != null) buttons.Add(_customerRuleViolation);
            if (_generalSell != null) buttons.Add(_generalSell);

            this.buttonGrid.AddButtonGridItem(this._otherServices, new ArrayChildSource(this.buttonGrid, _otherServices,
                buttons.ToArray()
                ));
        }

        void customerList_Clicked(ButtonGridBodyButtonItem t, int z)
        {
            INTAPS.SubscriberManagment.Client.SubscriptionList sl = new SubscriberManagment.Client.SubscriptionList();
            sl.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }

    }
}
