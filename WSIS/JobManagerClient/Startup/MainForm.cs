using INTAPS.UI;
using INTAPS.UI.ButtonGrid;
using INTAPS.WSIS.Job;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using System.Threading;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
namespace INTAPS.WSIS.Job.Client
{

    public partial class MainForm : Form
    {

        int _changeNo = -1;
        public MainForm()
        {
            this.InitializeComponent();
            this.Text = "Technical Job Manager:" + INTAPS.ClientServer.Client.ApplicationClient.UserName;
            this.InitializeButtonGrid();
            INTAPS.Accounting.Client.AccountingClient.PopulateReportSelector(-1, new ToolStripDrowDownReportSelector(this.dropDownReport));
            _changeNo = JobManagerClient.getChangeNo();
            new Thread(new ThreadStart(pollChangeNo)).Start();
            initSettingButtons();
        }
        void initSettingButtons()
        {
            int y = buttonGeneralSettings.Top;
            y -= buttonGeneralSettings.Height+10;
            foreach (System.Collections.Generic.KeyValuePair<int, JobRuleClientHandlerAttribute> kv in JobManagerClient.getClientHandlerAttributes)
            {
                if (kv.Value.hasConfiguration)
                {
                    Button btn = new Button();
                    btn.Text = kv.Value.configurationName;
                    btn.Size = buttonGeneralSettings.Size;
                    btn.Top = y;
                    btn.Left = buttonGeneralSettings.Left;
                    btn.Tag = kv.Key;
                    btn.Click += new EventHandler(configurationButtonClicked);
                    btn.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
                    buttonGeneralSettings.Parent.Controls.Add(btn);
                }
            }
        }

        void configurationButtonClicked(object sender, EventArgs e)
        {
            try
            {
                int type = (int)((Control)sender).Tag;
                Form f = JobManagerClient.getJobRuleEngineClientHandler(type).getConfigurationForm();
                f.Show(this);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

       

        public static INTAPS.UI.ButtonGrid.ButtonGrid CreateButtonGrid()
        {
            INTAPS.UI.ButtonGrid.ButtonGrid grid = new INTAPS.UI.ButtonGrid.ButtonGrid();
            grid.Dock = DockStyle.Fill;
            grid.ShowLeafNode = false;
            grid.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.FillBrush, Brushes.LightGray);
            grid.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Near, StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));
            grid.RootStylesCollectionHeaderButton.DefaultStyle.Add(StyleType.Font, new Font("Arial", 8f));
            grid.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.ButtonWidth, 200);
            grid.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.Font, new Font("Arial", 10f, FontStyle.Bold));
            grid.RootStylesCollectionHeaderButton.SelectStyle.Add(StyleType.StringFormat, StringFormatExtensions.GetStringFormat(StringAlignment.Center, StringAlignment.Near, StringFormatFlags.NoWrap, StringTrimming.EllipsisCharacter));
            grid.FilterBoxBackColor = Color.LightGray;
            grid.FilterLabel.ForeColor = Color.White;
            grid.FilterBox.Width = 400;
            grid.RootStyleHeaderButton.Add(StyleType.FontBrush, Brushes.White);
            grid.BackColor = Color.FromArgb(0x22, 0x7b, 0xad);
            grid.RootStylesCollectionBodyButton.DefaultStyle.Add(StyleType.FontBrush, Brushes.White);
            return grid;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void FixConfigClicked(ButtonGridBodyButtonItem t, int z)
        {
            try
            {
                JobManagerClient.CheckAndFixConfiguration();
                UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Configuration succesful");
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error fixing config", exception);
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            ApplicationClient.Disconnect();
            base.OnClosing(e);
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error updating\n" + exception.Message);
            }
        }

        private void toolReport_Click(object sender, EventArgs e)
        {
            this.buttonGrid.UpdateLayoutInPlace();
        }

        private void buttonMultipliers_Click(object sender, EventArgs e)
        {
            //JobItems ji = new JobItems();
            //ji.Show(this);
            BIZNET.iERP.Client.EmployeeManager m = new BIZNET.iERP.Client.EmployeeManager();
            m.Show(this);
        }
        void refreshJobs()
        {
            buttonGrid.UpdateLayoutInPlace();
        }
        void pollChangeNo()
        {
            do
            {
                Thread.Sleep(1000);
                try
                {
                    int chno = JobManagerClient.getChangeNo();
                
                if (chno != _changeNo)
                {
                    _changeNo = chno;
                    this.BeginInvoke(new ProcessParameterless(refreshJobs));
                }
                }
                catch
                {
                    return;
                }
            }
            while (true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //GeneralSettings gs = new GeneralSettings();
            //gs.Show(this);
            BIZNET.iERP.Client.ItemManager m = new BIZNET.iERP.Client.ItemManager();
            m.Show(this);
        }
    }
}

