using INTAPS.UI;
using System;
using System.Configuration;
using System.Windows.Forms;
using INTAPS.SubscriberManagment.Client;
namespace INTAPS.WSIS.Job.Client
{

    internal static class Program
    {
        public static bool PosPrinterMode;
        public static INTAPS.SubscriberManagment.Client.PosPrinter thePostPrinter;
        static SalePointBDE _salesPoint=null;
        public static SalePointBDE salesPoint
        {
            get
            {
                if (_salesPoint == null)
                {
                    try
                    {
                        _salesPoint = new SalePointBDE(null,null);
                        _salesPoint.Connect(ConnectionMode.Connected,false);
                    }
                    catch
                    {
                        _salesPoint = null;
                        throw;
                    }
                }
                return _salesPoint;
            }
        }
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login login = new Login();
            login.TryLogin();
            if (login.logedin)
            {
                if (!INTAPS.ClientServer.Client.SecurityClient.IsPermited("root/Job/Login"))
                {
                    System.Windows.Forms.MessageBox.Show("You are not authorized to login to customer service application");
                    return;
                }


                if (!bool.TryParse(ConfigurationManager.AppSettings["PosPrinterMode"], out PosPrinterMode))
                {
                    PosPrinterMode = false;
                }
                if (PosPrinterMode)
                {
                    thePostPrinter = new INTAPS.SubscriberManagment.Client.PosPrinter();
                }



                MainForm main = new MainForm();
                new UIFormApplicationBase(main);
                Application.Run(main);
            }
        }
    }
}

