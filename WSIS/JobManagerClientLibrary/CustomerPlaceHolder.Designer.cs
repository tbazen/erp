﻿namespace INTAPS.WSIS.Job.Client
{
    partial class ApplicationPlaceHolder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelNewCustomer = new System.Windows.Forms.Panel();
            this.textNewCustomerEmail = new System.Windows.Forms.TextBox();
            this.textNewCustomerPhone = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textNewCustomerName = new System.Windows.Forms.TextBox();
            this.textNewCustomerNameOfPlace = new System.Windows.Forms.TextBox();
            this.textNewCustomerHouseNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textNewCustomerAmharicName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboNewCustomerKebele = new System.Windows.Forms.ComboBox();
            this.comboNewCustomerType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateApplication = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.timeApplication = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textDescription = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.panelNewCustomer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelNewCustomer
            // 
            this.panelNewCustomer.Controls.Add(this.browser);
            this.panelNewCustomer.Controls.Add(this.textNewCustomerEmail);
            this.panelNewCustomer.Controls.Add(this.textNewCustomerPhone);
            this.panelNewCustomer.Controls.Add(this.label18);
            this.panelNewCustomer.Controls.Add(this.label12);
            this.panelNewCustomer.Controls.Add(this.label3);
            this.panelNewCustomer.Controls.Add(this.textNewCustomerName);
            this.panelNewCustomer.Controls.Add(this.textNewCustomerNameOfPlace);
            this.panelNewCustomer.Controls.Add(this.textNewCustomerHouseNo);
            this.panelNewCustomer.Controls.Add(this.label7);
            this.panelNewCustomer.Controls.Add(this.label5);
            this.panelNewCustomer.Controls.Add(this.label4);
            this.panelNewCustomer.Controls.Add(this.textNewCustomerAmharicName);
            this.panelNewCustomer.Controls.Add(this.label8);
            this.panelNewCustomer.Controls.Add(this.comboNewCustomerKebele);
            this.panelNewCustomer.Controls.Add(this.comboNewCustomerType);
            this.panelNewCustomer.Controls.Add(this.label9);
            this.panelNewCustomer.Location = new System.Drawing.Point(52, 90);
            this.panelNewCustomer.Name = "panelNewCustomer";
            this.panelNewCustomer.Size = new System.Drawing.Size(691, 359);
            this.panelNewCustomer.TabIndex = 0;
            // 
            // textNewCustomerEmail
            // 
            this.textNewCustomerEmail.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textNewCustomerEmail.Location = new System.Drawing.Point(150, 307);
            this.textNewCustomerEmail.Name = "textNewCustomerEmail";
            this.textNewCustomerEmail.Size = new System.Drawing.Size(248, 21);
            this.textNewCustomerEmail.TabIndex = 62;
            // 
            // textNewCustomerPhone
            // 
            this.textNewCustomerPhone.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textNewCustomerPhone.Location = new System.Drawing.Point(150, 276);
            this.textNewCustomerPhone.Name = "textNewCustomerPhone";
            this.textNewCustomerPhone.Size = new System.Drawing.Size(248, 21);
            this.textNewCustomerPhone.TabIndex = 63;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(23, 309);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 15);
            this.label18.TabIndex = 60;
            this.label18.Text = "Email:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(23, 278);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 15);
            this.label12.TabIndex = 61;
            this.label12.Text = "Phone No (mobile):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(23, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 15);
            this.label3.TabIndex = 48;
            this.label3.Text = "Name:";
            // 
            // textNewCustomerName
            // 
            this.textNewCustomerName.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textNewCustomerName.Location = new System.Drawing.Point(150, 52);
            this.textNewCustomerName.Name = "textNewCustomerName";
            this.textNewCustomerName.Size = new System.Drawing.Size(248, 21);
            this.textNewCustomerName.TabIndex = 53;
            // 
            // textNewCustomerNameOfPlace
            // 
            this.textNewCustomerNameOfPlace.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textNewCustomerNameOfPlace.Location = new System.Drawing.Point(150, 139);
            this.textNewCustomerNameOfPlace.Name = "textNewCustomerNameOfPlace";
            this.textNewCustomerNameOfPlace.Size = new System.Drawing.Size(248, 21);
            this.textNewCustomerNameOfPlace.TabIndex = 58;
            // 
            // textNewCustomerHouseNo
            // 
            this.textNewCustomerHouseNo.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textNewCustomerHouseNo.Location = new System.Drawing.Point(150, 174);
            this.textNewCustomerHouseNo.Multiline = true;
            this.textNewCustomerHouseNo.Name = "textNewCustomerHouseNo";
            this.textNewCustomerHouseNo.Size = new System.Drawing.Size(248, 96);
            this.textNewCustomerHouseNo.TabIndex = 59;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(23, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 15);
            this.label7.TabIndex = 57;
            this.label7.Text = "Name of Place:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 83);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 16);
            this.label5.TabIndex = 50;
            this.label5.Text = "አማርኛ ስም:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(23, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 15);
            this.label4.TabIndex = 56;
            this.label4.Text = "House No:";
            // 
            // textNewCustomerAmharicName
            // 
            this.textNewCustomerAmharicName.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textNewCustomerAmharicName.Location = new System.Drawing.Point(150, 80);
            this.textNewCustomerAmharicName.Name = "textNewCustomerAmharicName";
            this.textNewCustomerAmharicName.Size = new System.Drawing.Size(248, 21);
            this.textNewCustomerAmharicName.TabIndex = 52;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(23, 113);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 15);
            this.label8.TabIndex = 54;
            this.label8.Text = "Kebele:";
            // 
            // comboNewCustomerKebele
            // 
            this.comboNewCustomerKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNewCustomerKebele.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboNewCustomerKebele.FormattingEnabled = true;
            this.comboNewCustomerKebele.Location = new System.Drawing.Point(150, 111);
            this.comboNewCustomerKebele.Name = "comboNewCustomerKebele";
            this.comboNewCustomerKebele.Size = new System.Drawing.Size(248, 23);
            this.comboNewCustomerKebele.TabIndex = 55;
            // 
            // comboNewCustomerType
            // 
            this.comboNewCustomerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboNewCustomerType.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboNewCustomerType.FormattingEnabled = true;
            this.comboNewCustomerType.Location = new System.Drawing.Point(150, 17);
            this.comboNewCustomerType.Name = "comboNewCustomerType";
            this.comboNewCustomerType.Size = new System.Drawing.Size(248, 23);
            this.comboNewCustomerType.TabIndex = 47;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(23, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 15);
            this.label9.TabIndex = 46;
            this.label9.Text = "Customer Type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 17);
            this.label1.TabIndex = 30;
            this.label1.Text = "Application Date:";
            // 
            // dateApplication
            // 
            this.dateApplication.Location = new System.Drawing.Point(145, 4);
            this.dateApplication.Name = "dateApplication";
            this.dateApplication.Size = new System.Drawing.Size(144, 20);
            this.dateApplication.Style = INTAPS.Ethiopic.ETDateStyle.Ethiopian;
            this.dateApplication.TabIndex = 31;
            // 
            // timeApplication
            // 
            this.timeApplication.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeApplication.Location = new System.Drawing.Point(378, 4);
            this.timeApplication.Name = "timeApplication";
            this.timeApplication.Size = new System.Drawing.Size(143, 20);
            this.timeApplication.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.Location = new System.Drawing.Point(331, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 17);
            this.label2.TabIndex = 32;
            this.label2.Text = "Time:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textDescription);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dateApplication);
            this.panel1.Controls.Add(this.timeApplication);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(862, 75);
            this.panel1.TabIndex = 34;
            // 
            // textDescription
            // 
            this.textDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textDescription.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F);
            this.textDescription.Location = new System.Drawing.Point(118, 29);
            this.textDescription.Multiline = true;
            this.textDescription.Name = "textDescription";
            this.textDescription.Size = new System.Drawing.Size(701, 36);
            this.textDescription.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label10.Location = new System.Drawing.Point(6, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 17);
            this.label10.TabIndex = 34;
            this.label10.Text = "Job Description:";
            // 
            // browser
            // 
            this.browser.Location = new System.Drawing.Point(452, 98);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(250, 250);
            this.browser.StyleSheetFile = "jobstyle.css";
            this.browser.TabIndex = 35;
            // 
            // ApplicationPlaceHolder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.Controls.Add(this.panelNewCustomer);
            this.Controls.Add(this.panel1);
            this.Name = "ApplicationPlaceHolder";
            this.Size = new System.Drawing.Size(862, 452);
            this.panelNewCustomer.ResumeLayout(false);
            this.panelNewCustomer.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelNewCustomer;
        private System.Windows.Forms.TextBox textNewCustomerEmail;
        private System.Windows.Forms.TextBox textNewCustomerPhone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textNewCustomerName;
        private System.Windows.Forms.TextBox textNewCustomerNameOfPlace;
        private System.Windows.Forms.TextBox textNewCustomerHouseNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textNewCustomerAmharicName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboNewCustomerKebele;
        private System.Windows.Forms.ComboBox comboNewCustomerType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private INTAPS.Ethiopic.ETDateTimeDropDown dateApplication;
        private System.Windows.Forms.DateTimePicker timeApplication;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private INTAPS.UI.HTML.ControlBrowser browser;
        private System.Windows.Forms.TextBox textDescription;
        private System.Windows.Forms.Label label10;
    }
}
