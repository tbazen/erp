﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ApplicationPlaceHolder : UserControl
    {
        bool _newCustomerMode = false;
        List<SubscriberType> _subTypes;
        EtGrPickerPair _dateStart;
        bool _showCustomer = true;
        public ApplicationPlaceHolder()
        {
            InitializeComponent();

            setCustomer(null, true);
            panelNewCustomer.Dock=DockStyle.Fill;
            browser.Dock=DockStyle.Fill;
            this._dateStart = new EtGrPickerPair(this.dateApplication, this.timeApplication);
            this._subTypes = new List<SubscriberType>();
            this._subTypes.AddRange((SubscriberType[])Enum.GetValues(typeof(SubscriberType)));
            int i = 0;
            int selectIndex = -1;
            foreach (SubscriberType type in this._subTypes)
            {
                this.comboNewCustomerType.Items.Add(Subscriber.EnumToName(type));
                if (type == SubscriberType.Private)
                    selectIndex = i;
                i++;
            }
            this.comboNewCustomerType.SelectedIndex = selectIndex;

            if (INTAPS.ClientServer.Client.ApplicationClient.isConnected())
            {
                INTAPS.ClientServer.Client.ApplicationClient.PowerGeez.AddControl(this.textNewCustomerAmharicName);
                this.comboNewCustomerKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
                if (this.comboNewCustomerKebele.Items.Count > 0)
                {
                    this.comboNewCustomerKebele.SelectedIndex = 0;
                }

            }
        }
        private void setCustomerToControls(Subscriber customer)
        {
            if (customer == null)
                return;
            textNewCustomerName.Text = customer.name;
            this.textNewCustomerHouseNo.Text = customer.address;
            foreach (Kebele k in comboNewCustomerKebele.Items)
                if (k.id == customer.Kebele)
                {
                    comboNewCustomerKebele.SelectedItem = k;
                    break;
                }
            int index = _subTypes.IndexOf(customer.subscriberType);
            comboNewCustomerType.SelectedIndex = index;
            this.textNewCustomerAmharicName.Text = customer.amharicName;
            this.textNewCustomerPhone.Text = customer.phoneNo;
            textNewCustomerEmail.Text = customer.email;
            textNewCustomerNameOfPlace.Text = customer.nameOfPlace;
        }
        public void setCustomer(Subscriber customer,bool newCustomer)
        {
            _newCustomerMode = newCustomer;
            panelNewCustomer.Visible = _newCustomerMode;
            browser.Visible = !_newCustomerMode;
            if (newCustomer)
            {
                setCustomerToControls(customer);
            }
            else
            {
                if(customer==null)
                    browser.LoadTextPage("Customer", "");
                else
                    browser.LoadTextPage("Customer", JobManagerClient.buildCustomerHTML(customer));
                    
            }
        }
        public Subscriber getCustomer()
        {
            Subscriber customer = new Subscriber();
            customer.name = textNewCustomerName.Text;
            customer.address = this.textNewCustomerHouseNo.Text;
            customer.Kebele = ((Kebele)this.comboNewCustomerKebele.SelectedItem).id;
            customer.subscriberType = (SubscriberType)_subTypes[comboNewCustomerType.SelectedIndex];
            customer.amharicName = this.textNewCustomerAmharicName.Text;
            customer.phoneNo = this.textNewCustomerPhone.Text;
            customer.email = textNewCustomerEmail.Text;
            customer.nameOfPlace = textNewCustomerNameOfPlace.Text;
            return customer;
        }
        public bool validateCustomerData()
        {
            if (textNewCustomerName.Text.Trim().Equals(""))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter customer name");
                return false;
            }
            if (((Kebele)this.comboNewCustomerKebele.SelectedItem).id == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select customer kebele");
                return false;
            }
            if (this.comboNewCustomerType.SelectedIndex== -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select customer type");
                return false;
            }
            if (textNewCustomerPhone.Text != "")
            {
                if (!Subscriber.isValidPhoneNumber(textNewCustomerPhone.Text))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select valid phone no or leave empty");
                    return false;
                }
            }
            if (textNewCustomerEmail.Text != "")
            {
                if (!Subscriber.isValidEmailAddress(textNewCustomerEmail.Text))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select valid emial address or leave empty");
                    return false;
                }
            }
            return true;
        }
        public void getApplicationInfo(out DateTime appTime, out string note)
        {
            note = textDescription.Text;
            appTime = _dateStart.GetValue();
        }
        public void setApplictionInfo(DateTime appTime, string note)
        {
            textDescription.Text = note;
            _dateStart.SetValue(appTime);
        }
        public void hideCustomer()
        {
            _showCustomer = false;
            browser.Hide();
            panelNewCustomer.Hide();
        }

        public bool isNewCustomerMode()
        {
            return _newCustomerMode;
        }
    }
}
