﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using BIZNET.iERP;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using BIZNET.iERP.Client;
using INTAPS.ClientServer.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class CustomerProfile : Form,INTAPS.UI.HTML.IHTMLBuilder
    {
        
        string _html = "";
        public void Build()
        {
            _html = CustomerProfileBuilder.buildHTML(_customerID, CustomerProfileBuilder.ConnectionProfileOptions.All & ~CustomerProfileBuilder.ConnectionProfileOptions.ShowCustomerRef);
        }

        public string GetOuterHtml()
        {
            Build();
            return _html;
        }

     
    }
    
}
