﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.UI.HTML;
using INTAPS.SubscriberManagment.Client;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public partial class CustomerProfile : Form,INTAPS.UI.HTML.IHTMLBuilder
    {
        public bool ProcessUrl(string path, System.Collections.Hashtable query)
        {
            return CustomerProfileBuilder.ProcessUrl(this, path, query);
        }
     
    }
}
