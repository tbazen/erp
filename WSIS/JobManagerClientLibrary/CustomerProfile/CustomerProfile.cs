﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public partial class CustomerProfile : Form,INTAPS.UI.HTML.IHTMLBuilder
    {
        int _customerID;
        public CustomerProfile(int customerID)
        {
            InitializeComponent();
            bowserController1.SetBrowser(browser);
            _customerID = customerID;
            browser.LoadControl(this);
        }

        public void SetHost(UI.HTML.IHTMLDocumentHost host)
        {
            
        }

        public string WindowCaption
        {
            get { return "Customer Profile"; }
        }
    }
}
