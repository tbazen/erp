using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using BIZNET.iERP;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using BIZNET.iERP.Client;
using INTAPS.UI.HTML;
using INTAPS.ClientServer.Client;
using System.Reflection;
namespace INTAPS.WSIS.Job.Client
{
    public interface ICustomerProfilePlugin
    {
        bool ProcessUrl(Form parent, string path, System.Collections.Hashtable query);
    }
    public static class CustomerProfileBuilder
    {
        static List<ICustomerProfilePlugin> plugins = new List<ICustomerProfilePlugin>();
        public static void registerPlugin(ICustomerProfilePlugin plugin)
        {
            plugins.Add(plugin);
        }
        static CustomerProfileBuilder()
        {
            string assemblies = System.Configuration.ConfigurationManager.AppSettings["profilePlugins"];
            if (assemblies == null)
                return;
            foreach (string an in assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    Assembly a = System.Reflection.Assembly.Load(an);
                    int count = 0;
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.GetInterface(typeof(ICustomerProfilePlugin).Name) == null)
                            continue;
                        try
                        {
                            ICustomerProfilePlugin o = Activator.CreateInstance(t) as ICustomerProfilePlugin;
                            if (o == null)
                            {
                                Console.WriteLine("Client rule handler " + t + " doesn't implement IJobRuleServerHandler");
                                continue;
                            }
                            registerPlugin(o);
                            count++;
                        }
                        catch (Exception tex)
                        {
                            Console.WriteLine("Error trying to instantiate customer profile plugin " + t + "\n" + tex.Message);
                        }
                    }
                    if (count > 0)
                    {
                        Console.WriteLine(string.Format("{0} instantiate customer profile plugin(s) loaded from {1}", count, an));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error trying to load instantiate customer profile plugin assembly " + an + "\n" + ex.Message);
                }
            }
        }
        [Flags]
        public enum ConnectionProfileOptions
        {
            ShowMeterInfo=1,
            ShowReadingChart=2,
            ShowVersionLink=4,
            ShowMapLink=8,
            ShowLedgerLink=16,
            ShowCustomerRef=32,
            All = ShowMeterInfo | ShowReadingChart | ShowVersionLink | ShowMapLink | ShowLedgerLink | ShowCustomerRef,
            Minimal = (((All ^ ShowReadingChart) ^ ShowVersionLink) ^ ShowMapLink)^ShowLedgerLink,
            NoLink = ShowMeterInfo | ShowReadingChart
        }
        public static string buildHTML(int customerID, ConnectionProfileOptions options)
        {
            Subscriber customer = SubscriberManagmentClient.GetSubscriber(customerID);
            Subscription[] connection = SubscriberManagmentClient.GetSubscriptions(customerID, DateTime.Now.Ticks);
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            //customer name
            sb.Append("<tr><td class='customerProflieName'>");
            sb.Append(customer.name);
            sb.Append("</td></tr>");

            //customer primary detail
            sb.Append("<tr><td>");
            createCustomerTable(customer,options).build(sb);
            sb.Append("</td></tr>");

            //connection section title
            string sectionTitle;
            if (connection.Length == 0)
                sectionTitle = "Customer Has no Connections";
            else
                sectionTitle = "Connections ("+connection.Length+")";
            sb.Append("<tr><td class='customerProflieName'>");
            sb.Append(sectionTitle);
            sb.Append("</td></tr>");

            //connections detail
            int i = 1;
            foreach (Subscription con in connection)
            {
                sb.Append("<tr><td>");
                createConnectionTable(connection.Length > 1 ? i.ToString() + ". " : "", con, options).build(sb);
                sb.Append("</td></tr>");
                i++;
            }

            //Bill Section Title
            CustomerBillDocument[] bills = SubscriberManagmentClient.getBillDocuments(-1, customerID, -1, -1, true);
            if (bills.Length == 0)
                sectionTitle = "Customer has No Outstanding Bills";
            else
                sectionTitle = "Outstanding Bills";
            sb.Append("<tr><td class='customerProflieName'>");
            sb.Append(sectionTitle);
            sb.Append("</td></tr>");

            //Bill detail
            if (bills.Length > 0)
            {
                sb.Append("<tr><td>");
                createBillTable(bills).build(sb);
                sb.Append("</td></tr>");
            }

            //Credit scheme Section Title
            double[] settlements;
            CreditScheme[] schemes = SubscriberManagmentClient.getCustomerCreditSchemes(customerID, out settlements);
            if (schemes.Length == 0)
                sectionTitle = "Customer has no credit scheme";
            else
                sectionTitle = "Credit schemes";
            sb.Append("<tr><td class='customerProflieName'>");
            sb.Append(sectionTitle);
            sb.Append("</td></tr>");

            //Bill detail
            if (schemes.Length > 0)
            {

                sb.Append("<tr><td>");
                createCreditSchemeTable(schemes, settlements).build(sb);
                sb.Append("</td></tr>");
            }

            //Customer Service History
            JobData[] jobs=JobManagerClient.getCustomerJobs(customer.id,false);
            Array.Sort<JobData>(jobs, new Comparison<JobData>(delegate(JobData x, JobData y)
                {
                    return y.startDate.CompareTo(x.startDate);
                }));
            if (jobs.Length > 0)
            {
                sb.Append("<tr><td class='customerProflieName'>");
                sb.Append("Customer Service History");
                sb.Append("</td></tr>");
                sb.Append("<tr><td>");
                createJobTable(jobs).build(sb);
                sb.Append("</td></tr>");
            }
            
            return sb.ToString();
        }
        public static bERPHtmlTable createCustomerTable(Subscriber customer, ConnectionProfileOptions options)
        {
            bERPHtmlTableCell cell;
            bERPHtmlTable ret = new bERPHtmlTable();
            bERPHtmlTableRowGroup group = ret.createBodyGroup();
            if (customer != null)
            {
                bERPHtmlBuilder.htmlAddRow(group
                    , new bERPHtmlTableCell(TDType.body, "Name", "customerProfileLabel1")
                    , new bERPHtmlTableCell(TDType.body, customer.name, "customerProfileValue1")
                    , new bERPHtmlTableCell(TDType.body, "Customer Code", "customerProfileLabel1")
                    , cell = new bERPHtmlTableCell(TDType.body, customer.customerCode, "customerProfileValue1")
                    );
                CustomerSubtype subType = SubscriberManagmentClient.getCustomerSubtype(customer.subscriberType, customer.subTypeID);
                string subtypeStr = subType == null ? "" : " (" + subType.name + ")";
                bERPHtmlBuilder.htmlAddRow(group
                    , new bERPHtmlTableCell(TDType.body, "Type", "customerProfileLabel2")
                    , new bERPHtmlTableCell(TDType.body, Subscriber.EnumToName(customer.subscriberType) + subtypeStr, "customerProfileValue2")
                    , new bERPHtmlTableCell(TDType.body, "", "customerProfileLabel2")
                    , new bERPHtmlTableCell(TDType.body, "", "customerProfileValue2")
                    );

                Kebele theK = null;
                foreach (Kebele k in SubscriberManagmentClient.GetAllKebeles())
                {
                    if (k.id == customer.Kebele)
                    {
                        theK = k;
                        break;
                    }
                }
                bERPHtmlBuilder.htmlAddRow(group
                    , new bERPHtmlTableCell(TDType.body, "Address", "customerProfileLabel2")
                    , new bERPHtmlTableCell(TDType.body, (theK == null ? "" : theK.name) + (string.IsNullOrEmpty(customer.address) ? "" : ", House No.:" + customer.address) + (string.IsNullOrEmpty(customer.nameOfPlace) ? "" : ", " + customer.nameOfPlace), "customerProfileValue2")
                    , new bERPHtmlTableCell(TDType.body, "Email", "customerProfileLabel2")
                    , new bERPHtmlTableCell(TDType.body, customer.email, "customerProfileValue2")
                    );

                bool hasAccount = false;
                double balance = 0;
                int accountID = 1;
                if (customer.accountID > 0)
                {
                    CostCenterAccount csa = AccountingClient.GetCostCenterAccount(BIZNET.iERP.Client.iERPTransactionClient.mainCostCenterID, customer.accountID);
                    if (csa != null)
                    {
                        hasAccount = true;
                        balance = AccountingClient.GetNetBalanceAsOf(accountID = csa.id, 0, DateTime.Now);
                    }
                }

                bERPHtmlBuilder.htmlAddRow(group
                    , new bERPHtmlTableCell(TDType.body, "Phone No.", "customerProfileLabel2")
                    , new bERPHtmlTableCell(TDType.body, customer.phoneNo, "customerProfileValue2")
                    , new bERPHtmlTableCell(TDType.body, "Account Balance:", "customerProfileLabel2")
                    , cell = new bERPHtmlTableCell(TDType.body, "", "customerProfileValue3")
                    );

                cell.innerHtml = bERPHtmlBuilder.HtmlEncode(AccountBase.FormatAmount(balance))
                    + (hasAccount && ((options & ConnectionProfileOptions.ShowLedgerLink) == ConnectionProfileOptions.ShowLedgerLink)

                    ? string.Format("<a class='customerProfileLink1' href='showLedger?acid={0}'>ledger</a>", accountID) : "");
            }
            return ret;
        }
        public static bERPHtmlTable createConnectionTable(string prefix, Subscription connection, ConnectionProfileOptions options)
        {
            bERPHtmlTableCell cell;
            bERPHtmlTable ret = new bERPHtmlTable();
            bERPHtmlTableRowGroup group = ret.createBodyGroup();
            bERPHtmlBuilder.htmlAddRow(group
                , new bERPHtmlTableCell(TDType.body, prefix + "Connection Code", "customerProfileLabel1")
                , cell = new bERPHtmlTableCell(TDType.body, connection.contractNo, "customerProfileValue1")
                , new bERPHtmlTableCell(TDType.body, "Status", "customerProfileLabel2")
                , new bERPHtmlTableCell(TDType.body, Subscriber.EnumToName(connection.subscriptionStatus), "customerProfileValue2")
                );
            if ((options & ConnectionProfileOptions.ShowCustomerRef) == ConnectionProfileOptions.ShowCustomerRef)
            {
                Subscriber subsc = SubscriberManagmentClient.GetSubscriber(connection.subscriberID);
                bERPHtmlBuilder.htmlAddRow(group
                    , new bERPHtmlTableCell(TDType.body, "Customer Code", "customerProfileLabel1")
                    , cell = new bERPHtmlTableCell(TDType.body, subsc == null ? "Costomer ID: {0} not in database".format(connection.subscriberID) : subsc.customerCode, "customerProfileValue1")
                    , new bERPHtmlTableCell(TDType.body, "Customer Name:", "customerProfileLabel2")
                    , new bERPHtmlTableCell(TDType.body, subsc == null ? "" : subsc.name, "customerProfileValue2")
                    );
            }
            bERPHtmlBuilder.htmlAddRow(group
                , new bERPHtmlTableCell(TDType.body, "Date:", "customerProfileLabel1")
                , cell = new bERPHtmlTableCell(TDType.body, "", "customerProfileValue1")
                , new bERPHtmlTableCell(TDType.body, "Connection Type", "customerProfileLabel2")
                , new bERPHtmlTableCell(TDType.body, Subscriber.EnumToName(connection.subscriptionType), "customerProfileValue2")
                );

            if ((options & ConnectionProfileOptions.ShowVersionLink) == ConnectionProfileOptions.ShowVersionLink)
                cell.innerHtml = string.Format("{0} <a href='conversions?conID={1}'>Old Versions</a>", System.Web.HttpUtility.HtmlEncode(AccountBase.FormatDate(new DateTime(connection.ticksFrom))), connection.id);
            else
                cell.innerHtml = System.Web.HttpUtility.HtmlEncode(AccountBase.FormatDate(new DateTime(connection.ticksFrom)));
            bERPHtmlBuilder.htmlAddRow(group
                , new bERPHtmlTableCell(TDType.body, "House Hold Size", "customerProfileLabel1")
                , cell = new bERPHtmlTableCell(TDType.body, connection.householdSize.ToString(), "customerProfileValue1")
                , new bERPHtmlTableCell(TDType.body, "Location", "customerProfileLabel2")
                , cell = new bERPHtmlTableCell(TDType.body, "", "customerProfileValue2")
                );
            if ((options & ConnectionProfileOptions.ShowMapLink) == ConnectionProfileOptions.ShowMapLink)
            {
                cell.innerHtml = connection.hasCoordinate ?
                    string.Format("Latitude {0}, longitude {1}<a class='customerProfileLink1' href='showMap?conID={2}'>Show on map</a><br/><a class='customerProfileLink1' href='show_reading_map?conID={2}'>Reading Map</a>", connection.waterMeterX, connection.waterMeterY, connection.id)
                    : "";
            }

            Kebele keb = SubscriberManagmentClient.GetKebele(connection.Kebele);
            string kstr = keb == null ? "" : keb.name;
            bERPHtmlBuilder.htmlAddRow(group
                , new bERPHtmlTableCell(TDType.body, "Kebele", "customerProfileLabel1")
                , cell = new bERPHtmlTableCell(TDType.body, kstr, "customerProfileValue1")
                , new bERPHtmlTableCell(TDType.body, "Address", "customerProfileLabel2")
                , cell = new bERPHtmlTableCell(TDType.body, connection.address,"customerProfileValue2")
                );

            DistrictMeteringZone dma = SubscriberManagmentClient.getDMA(connection.dma);
            PressureZone zone = SubscriberManagmentClient.getPressureZone(connection.pressureZone);
            bERPHtmlBuilder.htmlAddRow(group
                , new bERPHtmlTableCell(TDType.body, "DMA", "customerProfileLabel1")
                , cell = new bERPHtmlTableCell(TDType.body, dma == null ? "" : dma.ToString(), "customerProfileValue1")
                , new bERPHtmlTableCell(TDType.body, "Pressure Zone", "customerProfileLabel2")
                , cell = new bERPHtmlTableCell(TDType.body, zone == null ? "" : zone.ToString(), "customerProfileValue2")
                );

            TransactionItems meterItem = BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(connection.itemCode);
            bERPHtmlBuilder.htmlAddRow(group
                , new bERPHtmlTableCell(TDType.body, "Meter No.", "customerProfileLabel2")
                , new bERPHtmlTableCell(TDType.body, connection.serialNo, "customerProfileValue2")
                , new bERPHtmlTableCell(TDType.body, "Meter Type", "customerProfileLabel2")
                , new bERPHtmlTableCell(TDType.body,
                    (meterItem == null ? "" : meterItem.Name)
                    + (string.IsNullOrEmpty(connection.modelNo) ? "" : " Model: " + connection.modelNo)
                    + (connection.prePaid ? "[Prepaid]" : "")
                    , "customerProfileValue2")
                );
            if ((options & ConnectionProfileOptions.ShowReadingChart) == ConnectionProfileOptions.ShowReadingChart)
            {
                bERPHtmlBuilder.htmlAddRow(group, new bERPHtmlTableCell(TDType.body, "Water use (meter cubes)", 4));
                bERPHtmlBuilder.htmlAddRow(group, cell = new bERPHtmlTableCell(TDType.body, "", 4));
                int n;
                BWFMeterReading[] readings = connection.id < 1 ? new BWFMeterReading[0] : SubscriberManagmentClient.BWFGetMeterReadings(-1, connection.id, -1, -1, 0, 10, out n);
                if (readings.Length == 0)
                    cell.innerText = "No readings";
                else
                {
                    string[] mark = new string[readings.Length];
                    double[] value = new double[readings.Length];
                    for (int i = 0; i < readings.Length; i++)
                    {
                        mark[i] = SubscriberManagmentClient.GetBillPeriod(readings[i].periodID).name;
                        value[i] = readings[i].consumption;
                    }
                    cell.innerHtml = CustomerProfileBuilder.buildHTMLBarChart(mark, value, "200px", "readingBar", "readingBarLabel", "readingBarValue");
                }
            }
            return ret;
        }
        public static bERPHtmlTable createCreditSchemeTable(CreditScheme[] schemes,double[] settled)
        {
            bERPHtmlTable ret = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "Date", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Credited Amount", "cpHeaderCellCurrency")
                , new bERPHtmlTableCell(TDType.ColHeader, "Monthly Payment", "cpHeaderCellCurrency")
                , new bERPHtmlTableCell(TDType.ColHeader, "Billed Amount", "cpHeaderCellCurrency")
                , new bERPHtmlTableCell(TDType.ColHeader, "Status", "cpHeaderCellCurrency")
                );

            bERPHtmlTableRowGroup group = ret.createBodyGroup();
            ret.groups.Add(header);
            int i = 0;
            foreach (CreditScheme scheme in schemes)
            {
                string css = "cpBodyCell";
                string status=null;
                if(AccountBase.AmountEqual(scheme.creditedAmount,settled[i]))
                    status="Fully Billed";
                else if (AccountBase.AmountLess(scheme.creditedAmount, settled[i]))
                        status = "Over Billed";

                if (!scheme.active)
                    status = INTAPS.StringExtensions.AppendOperand(status, ", ", "Closed");
                bERPHtmlBuilder.htmlAddRow(group
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatDate(scheme.issueDate),css)
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatAmount(scheme.creditedAmount), css+" currencyCell")
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatAmount(scheme.paymentAmount), css + " currencyCell")
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatAmount(settled[i]), css + " currencyCell")
                                    , new bERPHtmlTableCell(TDType.body, status, css + " currencyCell")
                                    );
                i++;
            }
            return ret;
        }
        public static bERPHtmlTable createJobTable(JobData[] jobs)
        {
            bERPHtmlTable ret = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "Date", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Job Number", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Service Type", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Status ", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Billed Amount", "cpHeaderCellCurrency")
                , new bERPHtmlTableCell(TDType.ColHeader, "Paid Amount", "cpHeaderCellCurrency")
                );

            bERPHtmlTableRowGroup group = ret.createBodyGroup();
            ret.groups.Add(header);
            int i = 0;
            foreach (JobData job in jobs)
            {
                string css = "cpBodyCell";
                string status = JobManagerClient.getJobStatus(job.status).name;
                double invoice = 0;
                double paid = 0;
                foreach (JobBillOfMaterial b in JobManagerClient.GetJobBOM(job.id))
                {
                    if (b.invoiceNo > 0)
                    {
                        CustomerBillRecord bill = SubscriberManagmentClient.getCustomerBillRecord(b.invoiceNo);
                        if (bill != null)
                        {
                            foreach (BillItem bi in SubscriberManagmentClient.getCustomerBillItems(b.invoiceNo))
                            {
                                invoice += bi.netPayment;
                            }
                            if (bill.paymentDocumentID > 0)
                            {
                                CustomerPaymentReceipt receipt = AccountingClient.GetAccountDocument(bill.paymentDocumentID, true)
                                    as CustomerPaymentReceipt;
                                paid += receipt.totalInstrument;
                            }
                        }
                    }
                }
                bERPHtmlTableCell cell=new bERPHtmlTableCell(TDType.body, "", css );
                cell.innerHtml = "<a href='showJob?jobID={0}'>{1}</a>".format(job.id, System.Web.HttpUtility.HtmlEncode(job.jobNo));
                bERPHtmlBuilder.htmlAddRow(group
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatDate(job.startDate), css)
                                    , cell
                                    , new bERPHtmlTableCell(TDType.body, System.Web.HttpUtility.HtmlEncode(JobManagerClient.getJobType(job.applicationType).name), css)
                                    , new bERPHtmlTableCell(TDType.body, System.Web.HttpUtility.HtmlEncode(status), css)
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatAmount(invoice), css + " currencyCell")
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatAmount(paid), css + " currencyCell")
                                    );
                i++;
            }
            return ret;
        }
        public static bERPHtmlTable createBillTable(CustomerBillDocument[] bills)
        {
            bERPHtmlTable ret = new bERPHtmlTable();
            bERPHtmlTableRowGroup header = new bERPHtmlTableRowGroup(TableRowGroupType.header);
            bERPHtmlBuilder.htmlAddRow(header
                , new bERPHtmlTableCell(TDType.ColHeader, "Type", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Invoice No.", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Period", "cpHeaderCell")
                , new bERPHtmlTableCell(TDType.ColHeader, "Amount", "cpHeaderCellCurrency")
                );

            bERPHtmlTableRowGroup group = ret.createBodyGroup();
            ret.groups.Add(header);

            foreach (CustomerBillDocument bill in bills)
            {
                if (bill==null || bill.draft)
                    continue;
                
                PeriodicBill p = bill as PeriodicBill;
                string css = "cpBodyCell";
                bERPHtmlBuilder.htmlAddRow(group
                                    , new bERPHtmlTableCell(TDType.body, AccountingClient.GetDocumentTypeByType(bill.GetType()).name,css)
                                    , new bERPHtmlTableCell(TDType.body, bill.invoiceNumber.reference,css)
                                    , new bERPHtmlTableCell(TDType.body, p == null ? "" : p.period.name,css)
                                    , new bERPHtmlTableCell(TDType.body, AccountBase.FormatAmount(bill.total),css+" currencyCell")
                                    );
            }
            return ret;
        }
        public static string buildHTMLBarChart(string[] vars, double[] vals,string height,string barClass,string labelStyle,string valueStyle)
        {
            if(vars==null || vars.Length==0)
                return "No Data";
            
            double max = 0;
            double min= 0;
            for (int i = 0; i < vars.Length; i++)
            {
                if (i == 0)
                {
                    min = max = vals[i];
                }
                else
                {
                    if (vals[i] < min)
                        min = vals[i];
                    if (vals[i] > max)
                        max = vals[i];
                }
            }
            if (min > 0)
                min = 0;
            double range = max - min;
            string chartRow = "";
            string labelRow = "";
            string valueRow = "";
            for (int i = vars.Length-1; i >=0; i--)
            {
                double barheight = AccountBase.AmountEqual(range, 0) ? 0 : Math.Round((vals[i] - min) / range * 100, 2);
                double baroffset = 100 - barheight;
                chartRow += string.Format("<td style='width:{0}%'><div style='height:{2}%'></div><div style='height:{1}%' class='{3}'></div></td>"
                    , Math.Round((double)1 / (double)vars.Length * 100, 2)
                    , barheight, baroffset
                    ,barClass);
                labelRow += string.Format("<td class='{1}'>{0}</td>", vars[i],labelStyle);
                valueRow += string.Format("<td class='{1}'>{0}</td>", vals[i], labelStyle);
            }
            string ret = string.Format("<table width='100%' style='height:{0}'><tr>{3}</tr><tr>{1}</tr><tr>{2}</tr></table>", height, chartRow, labelRow, valueRow);
            return ret;
        }

        public static string buildReadingHTML(BWFMeterReading reading)
        {
            if (reading == null)
                return "";
            if (reading.bwfStatus != BWFStatus.Read)
                return "Unread";
            string html = string.Format("<b>Reading for {2}: </b>{0}<br/><b>Consumption:</b> {1}", reading.reading.ToString("0"), reading.consumption.ToString("0"), SubscriberManagmentClient.GetBillPeriod(reading.periodID));

            switch (reading.readingType)
            {
                case MeterReadingType.MeterReset:
                    html += "<br/>Meter is reset";
                    break;
                case MeterReadingType.Average:
                    html += "<br/>Reading is estimated by averaging last "+reading.averageMonths +" months reading";
                    break;
                default:
                    break;
            }
            if ((reading.extraInfo & ReadingExtraInfo.Coordinate) == ReadingExtraInfo.Coordinate)
            {
                html += string.Format("<br/><b>Coordinates</b><br/><b>Latitude:</b>{0}<b>Longitude:</b>{1}", reading.readingX, reading.readingY);
            }
            if ((reading.extraInfo & ReadingExtraInfo.Coordinate) == ReadingExtraInfo.Coordinate)
            {
                html += string.Format("<br/><b>Read on</b>{0}", reading.readingTime);
            }
            if ((reading.extraInfo & ReadingExtraInfo.Remark) == ReadingExtraInfo.Remark)
            {
                html += string.Format("<br/><b>Remark</b>{0}", reading.readingRemark);
            }
            return html;
        }


        public static bool ProcessUrl(Form parent, string path, System.Collections.Hashtable query)
        {
            try
            {
                switch (path)
                {
                    case "conversions":
                        parent.BeginInvoke(new ProcessSingleParameter<int>(processShowVersions), int.Parse((string)query["conID"]));
                        return true;
                    case "showLedger":
                        parent.BeginInvoke(new ProcessSingleParameter<int>(processViewLedger), int.Parse((string)query["acid"]));
                        return true;
                    case "showMap":
                        parent.BeginInvoke(new ProcessTowParameter<int,bool>(processShowOnMap), int.Parse((string)query["conID"]),false);
                        return true;
                    case "show_reading_map":
                        parent.BeginInvoke(new ProcessTowParameter<int, bool>(processShowOnMap), int.Parse((string)query["conID"]),true);
                        return true;
                    default:
                        foreach (ICustomerProfilePlugin p in plugins)
                            if (p.ProcessUrl(parent, path, query))
                                return true;
                        return false;
                }
            }
            catch (Exception ex)
            {
                parent.BeginInvoke(new ShowErrorMesssageDelegate(showErrorMessage), ex);
                return true;
            }
        }
        static void processShowVersions(int connectionID)
        {
            Subscription[] s = SubscriberManagmentClient.GetSubscriptionHistory(connectionID);
            StringBuilder html = new StringBuilder();
            int index = 1;
            foreach (Subscription ss in s)
            {
                if (index>1)
                    html.Append( "</br>");
                CustomerProfileBuilder.createConnectionTable( "Version " + (s.Length - index + 1) + ": ", ss, ConnectionProfileOptions.Minimal).build(html);
                index++;
            }
            SimpleHTMLViewer sh = new SimpleHTMLViewer("Connection History", html.ToString(), "jobstyle.css");
            sh.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
        }
        static void processShowOnMap(int connectionID,bool reading)
        {
            SubscriberManagmentClient.locateWaterMeter(connectionID,reading);
        }
        static void processViewLedger(int accountID)
        {
            Type t = Type.GetType("BIZNET.iERP.Client.AccountExplorer,iERPTransactionClient");
            if (t == null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Could not load account explorer");
                return;
            }
            try
            {
                Form f = t.GetConstructor(new Type[] { typeof(int) }).Invoke(new object[] { accountID }) as Form;
                f.Show(INTAPS.UI.UIFormApplicationBase.MainForm);
                f.WindowState = FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }

        }
        static void showErrorMessage(Exception ex)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error", ex);
        }
    
    
    }
    
}
