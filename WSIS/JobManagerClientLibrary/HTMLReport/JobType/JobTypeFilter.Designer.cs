﻿namespace INTAPS.WSIS.Job.Client
{
    public partial class JobTypeFilter : System.Windows.Forms.UserControl
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.comboJobType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(6, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Period:";
            // 
            // comboJobType
            // 
            this.comboJobType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboJobType.FormattingEnabled = true;
            this.comboJobType.Location = new System.Drawing.Point(81, 5);
            this.comboJobType.Name = "comboJobType";
            this.comboJobType.Size = new System.Drawing.Size(264, 21);
            this.comboJobType.TabIndex = 12;
            // 
            // JobTypeFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboJobType);
            this.Controls.Add(this.label3);
            this.Name = "JobTypeFilter";
            this.Size = new System.Drawing.Size(366, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboJobType;
        
    }
}