using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{
    public partial class JobTypeFilter : UserControl
    {
        INTAPS.WSIS.Job.JobTypeInfo[] allJobTypes;
        public JobTypeFilter()
        {
            this.InitializeComponent();
            allJobTypes = JobManagerClient.getAllJobTypes();
            Array.Sort<JobTypeInfo>(allJobTypes, delegate(JobTypeInfo j1, JobTypeInfo j2)
            {
                return j1.name.CompareTo(j2.name);
            }
        );
            comboJobType.Items.Add("All Types");
            
            foreach (INTAPS.WSIS.Job.JobTypeInfo jt in allJobTypes)
            {
                comboJobType.Items.Add(jt.name);
            }
        }


        public int jobTypeID
        {
            get
            {
                if(comboJobType.SelectedIndex<1)
                {
                    return -1;
                }
                return allJobTypes[comboJobType.SelectedIndex - 1].id;
            }
        }
    }
}

