using INTAPS.Accounting.Client;
using System;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{

    public class RFSinglePeriod : EHTMLForm
    {
        protected JobTypeFilter m_filter = new JobTypeFilter();

        public RFSinglePeriod()
        {
            base.Controls.Add(this.m_filter);
            this.m_filter.BringToFront();
            this.m_filter.Dock = DockStyle.Top;
            base.SetTabDock(DockStyle.Fill);
        }

        public override object[] GetParamters()
        {
            return new object[] {m_filter.jobTypeID};
        }
    }
}

