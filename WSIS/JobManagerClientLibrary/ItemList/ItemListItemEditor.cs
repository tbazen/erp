﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ItemsListItemEditor : Form
    {
        public ItemsListItem theItem=null;
        public ItemsListItemEditor()
        {
            InitializeComponent();
        }
        public ItemsListItemEditor(ItemsListItem item):this()
        {
            if (item.isCategory)
            {
                categoryPlaceHolder.SetByID(item.category.ID);
            }
            else
            {
                itemPlaceholder.SetByID(item.titem.Code);
            }

        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            ItemsListItem item = new ItemsListItem();
            item.category=categoryPlaceHolder.anobject;
            item.titem = itemPlaceholder.anobject;
            if (item.titem==null &&  item.category==null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter an item or item category");
                return;
            }
            if (item.titem!= null && item.category != null)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("It is not allowed to set both item and item category");
                return;
            }
            item.isCategory = item.category != null;
            theItem = item;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
    
}
