﻿namespace INTAPS.WSIS.Job.Client
{
    partial class ItemListPlaceHolder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPick = new System.Windows.Forms.Button();
            this.labelPreview = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonPick
            // 
            this.buttonPick.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonPick.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonPick.ForeColor = System.Drawing.Color.White;
            this.buttonPick.Location = new System.Drawing.Point(517, 0);
            this.buttonPick.Name = "buttonPick";
            this.buttonPick.Size = new System.Drawing.Size(30, 28);
            this.buttonPick.TabIndex = 0;
            this.buttonPick.Text = "...";
            this.buttonPick.UseVisualStyleBackColor = false;
            this.buttonPick.Click += new System.EventHandler(this.buttonPick_Click);
            // 
            // labelPreview
            // 
            this.labelPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelPreview.Location = new System.Drawing.Point(0, 0);
            this.labelPreview.Name = "labelPreview";
            this.labelPreview.Size = new System.Drawing.Size(517, 28);
            this.labelPreview.TabIndex = 1;
            this.labelPreview.Text = "###";
            this.labelPreview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ItemListPlaceHolder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelPreview);
            this.Controls.Add(this.buttonPick);
            this.Name = "ItemListPlaceHolder";
            this.Size = new System.Drawing.Size(547, 28);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonPick;
        private System.Windows.Forms.Label labelPreview;
    }
}
