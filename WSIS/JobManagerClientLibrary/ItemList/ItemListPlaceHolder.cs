﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ItemListPlaceHolder : UserControl
    {
        public event EventHandler Changed;
        List<ItemsListItem> _items = new List<ItemsListItem>();
        public ItemsListItem[] getItems()
        {
            return _items.ToArray();
        }
        public ItemListPlaceHolder()
        {
            InitializeComponent();
            updateText();
        }
        public void setItems(ItemsListItem[] items)
        {
            _items.AddRange(items);
            updateText();
        }
        void updateText()
        {
            string txt="";
            foreach (ItemsListItem i in _items)
            {
                if (string.IsNullOrEmpty(txt))
                    txt = i.ToString();
                else
                    txt=txt+", "+i;
            }
            labelPreview.Text=txt;
        }
        private void buttonPick_Click(object sender, EventArgs e)
        {
            ItemsListEditor editor=new ItemsListEditor(_items);
            editor.ReloadList();
            editor.ShowDialog(this);
            if (editor.changed)
            {
                updateText();
                if (Changed != null)
                    Changed(this, null);
            }
        }
    }
    
}
