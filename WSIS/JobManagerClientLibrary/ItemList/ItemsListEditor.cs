using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{
    public class ItemsListEditor : INTAPS.UI.SimpleObjectList<ItemsListItem>
    {
        public List<ItemsListItem> _items;
        public bool changed=false;
        public ItemsListEditor(List<ItemsListItem> items)
        {
            _items = items;
        }
        protected override ItemsListItem CreateObject()
        {
            ItemsListItemEditor e = new ItemsListItemEditor();
            if (e.ShowDialog(this) == DialogResult.OK)
            {
                changed = true;
                _items.Add(e.theItem);
                return e.theItem;
            }
            return null;
        }
        protected override bool loadInConstructor
        {
            get
            {
                return false;
            }
        }
        protected override void DeleteObject(ItemsListItem obj)
        {
            changed = true;
            _items.Remove(obj);
        }

        protected override ItemsListItem[] GetObjects()
        {
            return _items.ToArray();
        }

        protected override string GetObjectString(ItemsListItem obj)
        {
            return obj.ToString();
        }

        protected override string ObjectTypeName
        {
            get { return "Items"; }
        }
        protected override bool AllowEdit
        {
            get
            {
                return true;
            }
        }
        protected override ItemsListItem EditObject(ItemsListItem obj)
        {
            ItemsListItemEditor e = new ItemsListItemEditor(obj);
            if (e.ShowDialog(this) == DialogResult.OK)
            {
                changed = true;
                _items[_items.IndexOf(obj)] = e.theItem;
                return e.theItem;
            }
            return null;

        }
    }
}
