﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class JobCustomerSelector : Form
    {
        public Subscriber selectedCustomer = null;
        public JobCustomerSelector(string title,string newButtonCaption,string existingButtonCapation,bool allowNew)
        {
            InitializeComponent();
            listCustomer_SelectedIndexChanged(null, null);
            this.Text = title;
            buttonNewCustomer.Visible = allowNew;
            buttonNewCustomer.Text = newButtonCaption;
            buttonSelect.Text = existingButtonCapation;

        }
        public JobCustomerSelector()
            : this("Select Customer", "New Customer", "Select",true)
        {
        }
        public JobCustomerSelector(bool allowNew)
            : this("Select Customer", "New Customer", "Select", allowNew)
        {
        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            try
            {
                listCustomer.Items.Clear();
                int N;
                foreach (SubscriberSearchResult sr in SubscriberManagmentClient.GetCustomers(textQuery.Text, -1, 0, 30, out N))
                {
                    ListViewItem li = new ListViewItem(sr.subscriber.customerCode);
                   li.SubItems.Add(sr.subscriber.name);
                   Kebele k=SubscriberManagmentClient.GetKebele(sr.subscriber.Kebele);
                    li.SubItems.Add(k==null?"":k.ToString());
                   li.Tag = sr.subscriber;
                   listCustomer.Items.Add(li);
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error loading search result", ex);
            }
        }

        private void listCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonSelect.Enabled = listCustomer.SelectedItems.Count > 0;
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            if (listCustomer.SelectedItems.Count == 0)
                return;
            this.selectedCustomer= listCustomer.SelectedItems[0].Tag as Subscriber;
            this.DialogResult = DialogResult.OK;
        }

        private void buttonNewCustomer_Click(object sender, EventArgs e)
        {
            this.selectedCustomer = null;
            this.DialogResult = DialogResult.OK;   
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void listCustomer_DoubleClick(object sender, EventArgs e)
        {
            buttonSelect_Click(null, null);
        }

        private void textQuery_TextChanged(object sender, EventArgs e)
        {

        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
                buttonSearch_Click(null, null);
        }
    }
}
