
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using INTAPS.ClientServer.Client;
using System.Reflection;
using System.Web.UI.HtmlControls;
using System.Text;
using Newtonsoft.Json.Linq;
using INTAPS.ClientServer;

namespace INTAPS.WSIS.Job.Client
{
    public static class JobManagerClient
    {
        static Dictionary<int, JobRuleClientHandlerAttribute> _jreClientHandlerAttributes;
        static Dictionary<int, IJobRuleClientHandler> _jreClientHandlers;
        public static Dictionary<int, JobRuleClientHandlerAttribute> getClientHandlerAttributes
        {
            get
            {
                return _jreClientHandlerAttributes;
            }
        }
        

        public static void AddAppointment(JobAppointment appointment)
        {
            //JobServer.AddAppointment(ApplicationClient.SessionID, appointment);
            RESTAPIClient.Call<VoidRet>(path + "AddAppointment", new { sessionID = ApplicationClient.SessionID , appointment = appointment });
        }

        public static int AddJob(JobData job, WorkFlowData data)
        {
            //return JobServer.AddJob(ApplicationClient.SessionID, job, data);
            return RESTAPIClient.Call<int>(path + "AddJob", new { sessionID = ApplicationClient.SessionID, job= job, data= new BinObject(data) });
        }

        public static void ChangeJobStatus(int jobID, DateTime date, int newStatus, string node)
        {
           // JobServer.ChangeJobStatus(ApplicationClient.SessionID, jobID, date, newStatus, node);
           RESTAPIClient.Call<VoidRet>(path + "ChangeJobStatus", new {sessionID = ApplicationClient.SessionID, jobID = jobID , newStatus = newStatus , node = node });
        }

        public static void ChangeWorkerStatus(string userID, bool active)
        {
            //JobServer.ChangeWorkerStatus(ApplicationClient.SessionID, userID, active);
            RESTAPIClient.Call<VoidRet>(path + "ChangeWorkerStatus", new {sessionID = ApplicationClient.SessionID, userID = userID , active = active });
        }

        public static void CheckAndFixConfiguration()
        {
            //JobServer.CheckAndFixConfiguration(ApplicationClient.SessionID);
            RESTAPIClient.Call<VoidRet>(path + "CheckAndFixConfiguration", new {sessionID = ApplicationClient.SessionID}); 
        }

        public static void ClearJobs()
        {
            //JobServer.ClearJobs(ApplicationClient.SessionID);
            RESTAPIClient.Call<VoidRet>(path + "ClearJobs", new {sessionID = ApplicationClient.SessionID});
        }

        public static void CloseAppointment(int appointmentID)
        {
            //JobServer.CloseAppointment(ApplicationClient.SessionID, appointmentID);
            RESTAPIClient.Call<VoidRet>(path + "CloseAppointment", new {sessionID = ApplicationClient.SessionID, appointmentID = appointmentID });
        }
        public static String path
        {
            get
            {
                return ApplicationClient.Server+ "/api/erp/jobmanager/";
            }
        }
        public static void Connect(string url)
        {
            //_JobServer = (IJobManager)RemotingServices.Connect(typeof(IJobManager), url + "/JobManagerServer");
            loadJubRuleEngineClientHandler();
        }

        public static void CreateJobWorker(JobWorker worker)
        {
            //JobServer.CreateJobWorker(ApplicationClient.SessionID, worker);
            RESTAPIClient.Call<VoidRet>(path + "CreateJobWorker", new {sessionID = ApplicationClient.SessionID, worker = worker }); 
        }

        public static void DeleteBOM(int bomID)
        {
            //JobServer.DeleteBOM(ApplicationClient.SessionID, bomID);
            RESTAPIClient.Call<VoidRet>(path + "DeleteBOM", new {sessionID = ApplicationClient.SessionID, bomID = bomID }); 
        }

        public static void DeleteWorker(string userName)
        {
            //JobServer.DeleteWorker(ApplicationClient.SessionID, userName);
            RESTAPIClient.Call<VoidRet>(path + "DeleteWorker", new {sessionID = ApplicationClient.SessionID, userName = userName });
        }


        public static List<JobData> GetAllJobs()
        {
           // return JobServer.GetAllActiveJobs(ApplicationClient.SessionID);
           return RESTAPIClient.Call<List<JobData>>(path + "GetAllActiveJobs", new { sessionID = ApplicationClient.SessionID });
        }

        public static JobWorker[] GetAllWorkers()
        {
            //return JobServer.GetAllWorkers(ApplicationClient.SessionID);
            return RESTAPIClient.Call<JobWorker[]>(path + "GetAllWorkers", new { sessionID = ApplicationClient.SessionID });
        }

        public static JobBillOfMaterial GetBillOfMaterial(int jobID)
        {
            //return JobServer.GetBillOfMaterial(ApplicationClient.SessionID, jobID);
            return RESTAPIClient.Call<JobBillOfMaterial>(path + "GetBillOfMaterial", new { sessionID = ApplicationClient.SessionID, jobID= jobID });
        }

        public static JobData[] GetFilteredJobs(DateTime date)
        {
            //return JobServer.GetFilteredJobs(ApplicationClient.SessionID, date);
            return RESTAPIClient.Call<JobData[]>(path + "GetFilteredJobs", new { sessionID = ApplicationClient.SessionID, date= date });
        }

        public static string GetInvoicePreview(JobBillOfMaterial bom)
        {
           // return JobServer.GetInvoicePreview(ApplicationClient.SessionID, bom);
           return RESTAPIClient.Call<string>(path + "GetInvoicePreview", new { sessionID = ApplicationClient.SessionID, bom= bom });
        }

        public static JobData GetJob(int jobID)
        {
            //return JobServer.GetJob(ApplicationClient.SessionID, jobID);
            return RESTAPIClient.Call<JobData>(path + "GetJob", new { sessionID = ApplicationClient.SessionID, jobID= jobID }); 
        }

        public static JobAppointment[] GetJobAppointments(int jobHandle)
        {
            //return JobServer.GetJobAppointments(ApplicationClient.SessionID, jobHandle);
            return RESTAPIClient.Call<JobAppointment[]>(path + "GetJobAppointments", new { sessionID = ApplicationClient.SessionID, jobHandle= jobHandle }); 
        }

        public static JobBillOfMaterial[] GetJobBOM(int jobID)
        {
            //return JobServer.GetJobBOM(ApplicationClient.SessionID, jobID);
            return RESTAPIClient.Call<JobBillOfMaterial[]>(path + "GetJobBOM", new {sessionID = ApplicationClient.SessionID, jobID= jobID });
        }

        public static string GetJobCard(int jobID)
        {
            //return JobServer.GetJobCard(ApplicationClient.SessionID, jobID);
            return RESTAPIClient.Call<string>(path + "GetJobCard", new {sessionID = ApplicationClient.SessionID, jobID= jobID });
        }

        public static JobStatusHistory[] GetJobHistory(int jobID)
        {
            //return JobServer.GetJobHistory(ApplicationClient.SessionID, jobID);
            return RESTAPIClient.Call<JobStatusHistory[]>(path + "GetJobHistory", new { sessionID = ApplicationClient.SessionID, jobID= jobID });
        }

        public static JobStatusHistory GetLatestHistory(int jobID)
        {
            //return JobServer.GetLatestHistory(ApplicationClient.SessionID, jobID);
            return RESTAPIClient.Call<JobStatusHistory>(path + "GetLatestHistory", new { sessionID = ApplicationClient.SessionID, jobID= jobID });
        }


        public static JobWorker GetWorker(string userID)
        {
            //return JobServer.GetWorker(ApplicationClient.SessionID, userID);
            return RESTAPIClient.Call<JobWorker>(path + "GetWorker", new { sessionID = ApplicationClient.SessionID, userID= userID });
        }

        public static int RegisterSubscription(int jobID, Subscription sub)
        {
           // return JobServer.RegisterSubscription(ApplicationClient.SessionID, jobID, sub);
           return RESTAPIClient.Call<int>(path + "RegisterSubscription", new {sessionID = ApplicationClient.SessionID, jobID= jobID, sub= sub }); 
        }

        public static void RemoveJob(int jobHandle)
        {
            //JobServer.RemoveJob(ApplicationClient.SessionID, jobHandle);
            RESTAPIClient.Call<VoidRet>(path + "RemoveJob", new {sessionID = ApplicationClient.SessionID, jobHandle = jobHandle });
        }
        public class SearchJobOut
        {
            public int NRecords;
            public JobData[] _ret;
        }
        public static JobData[] SearchJob(string query, int index, int pageSize, out int NRecords)
        {
            //return JobServer.SearchJob(ApplicationClient.SessionID, query, index, pageSize, out NRecords);
            var _ret =RESTAPIClient.Call<SearchJobOut>(path + "SearchJob", new { sessionID = ApplicationClient.SessionID, query = query , index = index , pageSize = pageSize });
            NRecords = _ret.NRecords;
            return _ret._ret;
        }

        public static List<JobData> SearchJob(string query, bool byDate, DateTime fromDate, DateTime toDate, int status)
        {
           // return JobServer.SearchJob(ApplicationClient.SessionID, query, byDate, fromDate, toDate, status);
           return RESTAPIClient.Call<List<JobData>>(path + "SearchJob2", new { sessionID = ApplicationClient.SessionID, query= query, byDate= byDate, fromDate= fromDate, toDate= toDate, status= status });
        }

        public static int SetBillofMateial(JobBillOfMaterial bom)
        {
           // return JobServer.SetBillofMateial(ApplicationClient.SessionID, bom);
           return RESTAPIClient.Call<int>(path + "SetBillofMateial", new {sessionID = ApplicationClient.SessionID, bom= bom });
        }

        public static void UpdateAppointment(JobAppointment appointment)
        {
           // JobServer.UpdateAppointment(ApplicationClient.SessionID, appointment);
           RESTAPIClient.Call<VoidRet>(path + "UpdateAppointment", new { sessionID = ApplicationClient.SessionID, appointment = appointment });
        }

        public static void UpdateHistoryEntry(JobStatusHistory history)
        {
           // JobServer.(ApplicationClient.SessionID, history);
           RESTAPIClient.Call<VoidRet>(path + "UpdateHistoryEntry", new {sessionID = ApplicationClient.SessionID, history = history });
        }

        public static void UpdateJob(JobData _job, WorkFlowData data)
        {
           // JobServer.UpdateJob(ApplicationClient.SessionID, _job, data);
           RESTAPIClient.Call<VoidRet>(path + "UpdateJob", new {sessionID = ApplicationClient.SessionID, _job = _job, data= new BinObject(data) });
        }

        public static void UpdateJobWorker(JobWorker worker)
        {
           // JobServer.UpdateJobWorker(ApplicationClient.SessionID, worker);
           RESTAPIClient.Call<VoidRet>(path + "UpdateJobWorker", new {sessionID = ApplicationClient.SessionID, worker = worker });
        }
        public static void VoidJobReceipt(int bomID)
        {
            //JobServer.VoidJobReceipt(ApplicationClient.SessionID, bomID);
            RESTAPIClient.Call<VoidRet>(path + "VoidJobReceipt", new {sessionID = ApplicationClient.SessionID, bomID = bomID});
        }

        public static INTAPS.WSIS.Job.JobAppointment GetAppointment(int id)
        {
            //return JobServer.GetAppointment(ApplicationClient.SessionID, id);
            return RESTAPIClient.Call<INTAPS.WSIS.Job.JobAppointment>(path + "GetAppointment", new { sessionID = ApplicationClient.SessionID, id= id });
        }

        public static double getBOMTotal(int bomID)
        {
          //  return JobServer.getBOMTotal(ApplicationClient.SessionID, bomID);
          return RESTAPIClient.Call<double>(path + "getBOMTotal", new { sessionID = ApplicationClient.SessionID, bomID= bomID });
        }

        public static int getPreviousHistory(int jobID)
        {
          //  return JobServer.getPreviousHistory(ApplicationClient.SessionID, jobID);
          return RESTAPIClient.Call<int>(path + "getPreviousHistory", new { sessionID = ApplicationClient.SessionID, jobID= jobID });
        }

        public static object GetSystemParameter(string p)
        {
            //return JobServer.GetSystemParameter(ApplicationClient.SessionID, p);
            return RESTAPIClient.Call<object>(path + "GetSystemParameter", new {sessionID = ApplicationClient.SessionID, p= p }); 
        }

        public static INTAPS.WSIS.Job.JobItemSetting[] getAllJobItems()
        {
            //return JobServer.getAllJobItems(ApplicationClient.SessionID);
            return RESTAPIClient.Call<INTAPS.WSIS.Job.JobItemSetting[]>(path + "getAllJobItems", new { sessionID = ApplicationClient.SessionID });
        }

        public static INTAPS.WSIS.Job.JobItemSetting getJobItem(string itemCode)
        {
           // return JobServer.getJobItem(ApplicationClient.SessionID, itemCode);
           return RESTAPIClient.Call<INTAPS.WSIS.Job.JobItemSetting>(path + "getJobItem", new {sessionID = ApplicationClient.SessionID, itemCode= itemCode });
        }

        public static void deleteJobItem(string itemCode)
        {
            //JobServer.deleteJobItem(ApplicationClient.SessionID, itemCode);
            RESTAPIClient.Call<VoidRet>(path + "deleteJobItem", new {sessionID = ApplicationClient.SessionID, itemCode = itemCode });
        }

        public static void setJobItem(INTAPS.WSIS.Job.JobItemSetting setting)
        {
           // JobServer.setJobItem(ApplicationClient.SessionID, setting);
           RESTAPIClient.Call<VoidRet>(path + "setJobItem", new {sessionID = ApplicationClient.SessionID, setting = setting });
        }

        public static int getChangeNo()
        {
            //return JobServer.getChangeNo(ApplicationClient.SessionID);
            return RESTAPIClient.Call<int>(path + "getChangeNo", new {  sessionID = ApplicationClient.SessionID});
        }



        public static string GetCommandString(int jobID, int status)
        {
            //return JobServer.GetCommandString(ApplicationClient.SessionID, jobID, status);
            return RESTAPIClient.Call<string>(path + "GetCommandString", new {sessionID = ApplicationClient.SessionID, jobID= jobID, status= status });
        }
        public static int getJobTypeByClientHandlerType(Type t)
        {
            foreach (KeyValuePair<int, IJobRuleClientHandler> h in _jreClientHandlers)
            {
                if (h.Value.GetType().Equals(t))
                    return h.Key;
            }
            return -1;
        }

        public static void loadJubRuleEngineClientHandler()
        {

            _jreClientHandlerAttributes = new Dictionary<int, JobRuleClientHandlerAttribute>();

            _jreClientHandlers = new Dictionary<int, IJobRuleClientHandler>();
            string assemblies = System.Configuration.ConfigurationManager.AppSettings["ruleAssemblies"];
            if (assemblies == null)
                return;
            foreach (string an in assemblies.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    Assembly a = System.Reflection.Assembly.Load(an);
                    int count = 0;
                    foreach (Type t in a.GetTypes())
                    {
                        object[] atr = t.GetCustomAttributes(typeof(JobRuleClientHandlerAttribute), true);
                        if (atr == null || atr.Length == 0)
                            continue;
                        if (atr.Length > 1)
                        {
                            Console.WriteLine("Multiple job JobRuleServerHandlerAttribute found for type ");
                            continue;
                        }

                        JobRuleClientHandlerAttribute jatr = atr[0] as JobRuleClientHandlerAttribute;
                        ConstructorInfo ci = t.GetConstructor(new Type[] { });
                        if (ci == null)
                        {
                            Console.WriteLine("Constructor not found for type ");
                            continue;
                        }
                        try
                        {
                            IJobRuleClientHandler o = ci.Invoke(new object[] { }) as IJobRuleClientHandler;
                            if (o == null)
                                if (ci == null)
                                {
                                    Console.WriteLine("Client rule handler " + t + " doesn't implement IJobRuleServerHandler");
                                    continue;
                                }
                            if (_jreClientHandlers.ContainsKey(jatr.typeID))
                            {
                                Console.WriteLine("Client rule handler " + t + " is using an ID that is already used " + jatr.typeID);
                                continue;
                            }
                            if (_jreClientHandlerAttributes.ContainsKey(jatr.typeID))
                            {
                                if (_jreClientHandlerAttributes[jatr.typeID].priority > jatr.priority)
                                    continue;
                                if (_jreClientHandlerAttributes[jatr.typeID].priority == jatr.priority)
                                    throw new ClientServer.ServerUserMessage("Client rule handler is repeated. ID {0}", jatr.typeID);
                                _jreClientHandlerAttributes[jatr.typeID] = jatr;
                                _jreClientHandlers[jatr.typeID] = o;
                            }
                            else
                            {
                                _jreClientHandlerAttributes.Add(jatr.typeID, jatr);
                                _jreClientHandlers.Add(jatr.typeID, o);
                            }
                            count++;
                        }
                        catch (Exception tex)
                        {
                            Console.WriteLine("Error trying to instantiate client rule handler " + t + "\n" + tex.Message);
                        }
                    }
                    if (count > 0)
                    {
                        Console.WriteLine(string.Format("{0} job rule client handler(s) loaded from {1}", count, an));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error trying to load client rule handler assembly " + an + "\n" + ex.Message);
                }

            }
        }

        public static IJobRuleClientHandler getJobRuleEngineClientHandler(int typeID)
        {
            if (_jreClientHandlers.ContainsKey(typeID))
                return _jreClientHandlers[typeID];
            return null;
        }

        public static int[] getPossibleNextStatus(System.DateTime date, int jobID)
        {
           // return JobServer.getPossibleNextStatus(ApplicationClient.SessionID, date, jobID);
           return RESTAPIClient.Call<int[]>(path + "getPossibleNextStatus", new { sessionID = ApplicationClient.SessionID, date= date, jobID= jobID });
        }
        public static string buildCustomerHTML(INTAPS.SubscriberManagment.Subscriber customer)
        {
            var appTable = new BIZNET.iERP.bERPHtmlTable();
            appTable.createBodyGroup();
            appTable.id = "custInfo";
            JobProcessRules.buildTwoColumnDataTable(appTable.groups[0]
                , "Customer Code:", customer.customerCode
                , "Customer Name:", customer.name
                , "Customer Type:", INTAPS.SubscriberManagment.Subscriber.EnumToName(customer.subscriberType)
                , "Kebele:", customer.Kebele < 1 ? "" : INTAPS.SubscriberManagment.Client.SubscriberManagmentClient.GetKebele(customer.Kebele).ToString()
                , "House No:", customer.address
                , "Name of Place:", customer.nameOfPlace
                );
            string ret = BIZNET.iERP.bERPHtmlBuilder.ToString(appTable);
            return ret;
        }

        public static INTAPS.WSIS.Job.WorkFlowData getWorkFlowData(int jobID, int typeID, int key, bool fullData)
        {
           // return JobServer.getWorkFlowData(ApplicationClient.SessionID, jobID, typeID, key, fullData);
           return RESTAPIClient.Call<BinObject>(path + "getWorkFlowData", new {sessionID = ApplicationClient.SessionID, jobID= jobID, typeID= typeID, key= key, fullData= fullData }).Deserialized() as WorkFlowData;
        }

        public static INTAPS.WSIS.Job.JobTypeInfo getJobType(int typeID)
        {
            //return JobServer.getJobType(ApplicationClient.SessionID, typeID);
            return RESTAPIClient.Call<INTAPS.WSIS.Job.JobTypeInfo>(path + "getJobType", new { sessionID = ApplicationClient.SessionID, typeID= typeID });
        }

        public static INTAPS.WSIS.Job.JobStatusType getJobStatus(int status)
        {
           // return JobServer.getJobStatus(ApplicationClient.SessionID, status);
           return RESTAPIClient.Call<INTAPS.WSIS.Job.JobStatusType>(path + "getJobStatus", new { sessionID = ApplicationClient.SessionID, status= status });
        }

        public static void SetSystemParameters(string[] fields, object[] vals)
        {
            //JobServer.SetSystemParameters(ApplicationClient.SessionID, fields, vals);
            RESTAPIClient.Call<VoidRet>(path + "SetSystemParameters", new {sessionID = ApplicationClient.SessionID, fields = fields , vals = vals });
        }

        public static void saveConfiguration(int typeID, object config)
        {
            //JobServer.saveConfiguration(ApplicationClient.SessionID, typeID, config);
            var t = config.GetType();
            RESTAPIClient.Call<VoidRet>(path + "saveConfiguration", new {sessionID = ApplicationClient.SessionID, typeID = typeID , config = config,typeName=t.FullName+","+t.Assembly.FullName});
        }

        public static object getConfiguration(int typeID, System.Type type)
        {
            // return JobServer.getConfiguration(ApplicationClient.SessionID, typeID, type);
            var ret = RESTAPIClient.Call<BinObject>(path + "getConfiguration", new { sessionID = ApplicationClient.SessionID, typeID = typeID, type = type.AssemblyQualifiedName });
            return ret.Deserialized();
        }

        public static void setBillofMaterialCustomerReference(INTAPS.WSIS.Job.JobBillOfMaterial bom)
        {
          //  JobServer.setBillofMaterialCustomerReference(ApplicationClient.SessionID, bom);
          RESTAPIClient.Call<VoidRet>(path + "setBillofMaterialCustomerReference", new { sessionID = ApplicationClient.SessionID, bom = bom });
        }

        public static void setWorkFlowData(INTAPS.WSIS.Job.WorkFlowData data)
        {
           // JobServer.setWorkFlowData(ApplicationClient.SessionID, data);
           RESTAPIClient.Call<VoidRet>(path + "setWorkFlowData", new {sessionID = ApplicationClient.SessionID, data = new BinObject(data) });
        }

        public static bool jobAppliesTo(System.DateTime date, int jobID)
        {
           // return JobServer.jobAppliesTo(ApplicationClient.SessionID, date, jobID);
           return RESTAPIClient.Call<bool>(path + "jobAppliesTo", new { sessionID = ApplicationClient.SessionID, date= date, jobID= jobID });
        }

        public static INTAPS.WSIS.Job.JobTypeInfo[] getAllJobTypes()
        {
           // return JobServer.getAllJobTypes(ApplicationClient.SessionID);
           return RESTAPIClient.Call<INTAPS.WSIS.Job.JobTypeInfo[]>(path + "getAllJobTypes", new { sessionID = ApplicationClient.SessionID });
        }

        public static INTAPS.WSIS.Job.CashHandoverDocument getLastCashHandoverDocument(int cashAccountID)
        {
            //return JobServer.getLastCashHandoverDocument(ApplicationClient.SessionID, cashAccountID);
            return RESTAPIClient.Call<INTAPS.WSIS.Job.CashHandoverDocument>(path + "getLastCashHandoverDocument", new { sessionID = ApplicationClient.SessionID, cashAccountID = cashAccountID });
        }

        public static BIZNET.iERP.PaymentInstrumentItem[] getPaymentCenterPaymentInstruments(int cashAccountID, System.DateTime from, System.DateTime to)
        {
            //return JobServer.getPaymentCenterPaymentInstruments(ApplicationClient.SessionID, cashAccountID, from, to);
            return RESTAPIClient.Call<BIZNET.iERP.PaymentInstrumentItem[]>(path + "getPaymentCenterPaymentInstruments", new { sessionID = ApplicationClient.SessionID, cashAccountID= cashAccountID, from= from, to= to });
        }

        public static INTAPS.WSIS.Job.JobData[] getCustomerJobs(int customerID, bool onlyAcitve)
        {
           // return JobServer.getCustomerJobs(ApplicationClient.SessionID, customerID, onlyAcitve);
           return RESTAPIClient.Call<INTAPS.WSIS.Job.JobData[]>(path + "getCustomerJobs", new { sessionID = ApplicationClient.SessionID, customerID= customerID, onlyAcitve= onlyAcitve });
        }
    }
}

