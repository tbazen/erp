﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public class JobRuleClientHandlerAttribute:Attribute
    {
        public int priority = 0;
        public int typeID;
        public string configurationName;
        public bool hasConfiguration;
        public JobRuleClientHandlerAttribute(int typeID,string configName,bool hasConfig)
        {
            this.typeID = typeID;
            this.configurationName = configName;
            this.hasConfiguration = hasConfig;
        }
        public JobRuleClientHandlerAttribute(int typeID)
            :this(typeID,null,false)
        {
            
        }
    }
    public interface IJobRuleClientHandler
    {
        Type getApplicationFormType();
        NewApplicationForm getNewApplicationForm(int jobID,Subscriber customer,Subscription connection);
        string buildDataHTML(JobData job);
        Form getConfigurationForm();
        bool processURL(Form parent,string path, System.Collections.Hashtable query);

    }
}
