using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.BILLING_CERDIT)]
    public class BillCreditClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new BillingCreditForm(jobID,customer);
        }

        public string buildDataHTML(JobData job)
        {
            BillCreditData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BILLING_CERDIT, 0, true) as BillCreditData;
            Subscriber newCustomer = job.customerID == -1 ? job.newCustomer : SubscriberManagmentClient.GetSubscriber(job.customerID);
            string creditDetail;
            if (data == null)
                creditDetail = "";
            else
            {
                creditDetail = "<br/><span class='jobSection1'>Credit Detail</span><br/>";
                if (data.creditedBills.Length > 0)
                {
                    creditDetail = "<br/><span class='jobSection2'>Bills</span><br/><table>";
                    foreach (CustomerBillDocument record in data.creditedBills)
                    {
                        string name = record.ShortDescription;
                        creditDetail+=string.Format("<tr><td>{0}</td><td>{1}</td></tr>"
                            ,HttpUtility.HtmlEncode(name),HttpUtility.HtmlEncode( record.total.ToString("#,#0.00")));
                    }
                    creditDetail += "</table>";
                }
                if (data.schedule != null)
                {
                    creditDetail+= string.Format("<br/>Payment Amount: {0} birr every month starting from {1}"
                        ,HttpUtility.HtmlEncode(data.schedule.paymentAmount.ToString("#,#0.00"))
                        , HttpUtility.HtmlEncode(SubscriberManagmentClient.GetBillPeriod(data.schedule.startPeriodID).name)
                        );
                }
            }
            return creditDetail;
        }


    }
}
