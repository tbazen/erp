﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class BillingCreditForm : NewApplicationForm
    {
        int _jobID;
        JobData _job;
        Subscriber _customer;
        public BillingCreditForm(int jobID,Subscriber customer)
        {
            InitializeComponent();
            _customer=customer;

            billPeriodSelector.LoadData(SubscriberManagment.Client.SubscriberManagmentClient.CurrentYear);
            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _job = null;
                loadBills(null);
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                applicationPlaceHolder.setApplictionInfo(job.startDate, job.description);
                BillCreditData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BILLING_CERDIT, 0, true) as BillCreditData;
                _job = job;
                loadBills(data);

                if (data.schedule != null)
                {
                    this.textAmount.Text = data.schedule.paymentAmount.ToString("0,0.00");
                    this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.GetBillPeriod(data.schedule.startPeriodID);
                }
            }             
        }
        void updateAmount()
        {
            double total = 0;
            foreach (ListViewItem li in listView.CheckedItems)
            {
                total+=((CustomerBillDocument)li.Tag).total;
            }
            labelAmount.Text = INTAPS.Accounting.AccountBase.FormatAmount(total);
            if (total == 0)
            {
                textTotalAmount.Enabled = false;
                int csaid=-1;
                if(_customer.accountID>0)
                {
                    INTAPS.Accounting.CostCenterAccount csa=AccountingClient.GetCostCenterAccount(BIZNET.iERP.Client.iERPTransactionClient.mainCostCenterID,_customer.accountID);
                    if(csa!=null)
                        csaid=csa.id;
                }
                if (csaid == -1)
                    textTotalAmount.Text = "";
                else
                    textTotalAmount.Text = INTAPS.Accounting.AccountBase.FormatAmount(AccountingClient.GetNetBalanceAsOf(csaid, DateTime.Now));
                textTotalAmount.Enabled = true;
            }
            else
            {
                textTotalAmount.Enabled = false;
                textTotalAmount.Text = INTAPS.Accounting.AccountBase.FormatAmount(total);
            }
        }

        private void loadBills(BillCreditData data)
        {
            listView.Items.Clear();
            CustomerBillDocument[] existingCreditBills = data == null ? new CustomerBillDocument[0] : data.creditedBills;
            if (_job == null || !StandardJobStatus.isInactiveState(_job.status))
            {
                CustomerBillDocument[] bills = SubscriberManagmentClient.getBillDocuments(-1,_customer.id,-1,-1,true);
                foreach (CustomerBillDocument record in bills)
                {
                    if (record.draft)
                        continue;
                    PeriodicBill p=record as PeriodicBill;
                    ListViewItem item = new ListViewItem();
                    string name = AccountingClient.GetDocumentTypeByType(record.GetType()).name;
                    if(p!=null)
                        name+=" "+SubscriberManagmentClient.GetBillPeriod(p.period.id).name;
                        item = new ListViewItem(name);
                    
                    item.Tag = record;
                    item.SubItems.Add(INTAPS.Accounting.AccountBase.FormatDate(record.DocumentDate));
                    item.SubItems.Add(record.total.ToString("#,#0.00"));
                    listView.Items.Add(item);
                    foreach (CustomerBillDocument e in existingCreditBills)
                    {
                        if (e.AccountDocumentID == record.AccountDocumentID)
                        {
                            item.Checked = true;
                            break;
                        }
                    }
                    

                }
            }
            else
            {
                foreach (CustomerBillDocument record in existingCreditBills)
                {
                    PeriodicBill p = record as PeriodicBill;
                    ListViewItem item = new ListViewItem();
                    string name = AccountingClient.GetDocumentTypeByType(record.GetType()).name;
                    if (p != null)
                        name += " " + SubscriberManagmentClient.GetBillPeriod(p.period.id).name;
                    
                    item = new ListViewItem(name);
                    item.Tag = record;
                    item.SubItems.Add(INTAPS.Accounting.AccountBase.FormatDate(record.DocumentDate));
                    item.SubItems.Add(record.total.ToString("#,#0.00"));
                    listView.Items.Add(item);
                    item.Checked = true;
                }
            }
            updateAmount();
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.BILLING_CERDIT;
                }
                else
                    app = _job;
                app.customerID = _customer.id;
                
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                BillCreditData data = new BillCreditData();
                data.creditedBills = new CustomerBillDocument[listView.CheckedItems.Count];
                int i = 0;
                foreach (ListViewItem li in listView.CheckedItems)
                {
                    data.creditedBills[i++] = li.Tag as CustomerBillDocument;
                }


                CreditScheme credit = new CreditScheme();
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(this.textAmount, "Please enter valid amount", out credit.paymentAmount))
                    return;
                if (textAmount.Enabled)
                {
                    if (!INTAPS.UI.Helper.ValidateDoubleTextBox(this.textTotalAmount, "Please enter valid amount", out credit.creditedAmount))
                        return;
                }
                credit.startPeriodID = this.billPeriodSelector.SelectedPeriod.id;
                data.schedule = credit;
                /*if (data.creditedBills.Length == 0)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select at least one bill");
                    return;
                }*/
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void listView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            updateAmount();
        }
    }
}
