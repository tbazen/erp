using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.BILLING_DEPOSIT)]
    public class BillDepositClient : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new BillingDepositForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            BillDepositData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BILLING_DEPOSIT, 0, true) as BillDepositData;
            Subscriber newCustomer = SubscriberManagmentClient.GetSubscriber(job.customerID);
            string depositDetail;
            if (data == null)
                depositDetail = "";
            else
            {
                depositDetail = string.Format("<br/><span class='dataLabel1'>Deposit Amount:</span><span class='dataValue1'>{0}</span><br/>"
                    , HttpUtility.HtmlEncode(data.deposit.ToString("#,#0.00"))
                    )
                    ;
            }
            return string.Format("<span class='jobSection1'>{0}</span>{1}"
                , System.Web.HttpUtility.HtmlEncode("Detail")
                , depositDetail
                );
        }


    }
}
