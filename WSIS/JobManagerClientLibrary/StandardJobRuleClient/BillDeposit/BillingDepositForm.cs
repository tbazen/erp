﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class BillingDepositForm : NewApplicationForm
    {
        Subscriber _customer;
        int _jobID;
        JobData _job;
        public BillingDepositForm(int jobID, Subscriber connection)
        {
            InitializeComponent();
            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _customer = connection;
                _job = null;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                BillDepositData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BILLING_DEPOSIT, 0, true) as BillDepositData;
                _customer = SubscriberManagmentClient.GetSubscriber(job.customerID);
                _job = job;
                if (data!= null)
                {
                    this.textAmount.Text = data.deposit.ToString("0,0.00");
                }
            }
            setConnection();
            
        }

    
        void setConnection()
        {
            applicationPlaceHolder.setCustomer(_customer, false);
            browser.LoadTextPage("Connection",
                JobRuleClientBase.createConciseCustString(_customer)
                );
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.BILLING_DEPOSIT;
                }
                else
                    app = _job;

                app.customerID = _customer.id;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                BillDepositData data = new BillDepositData();
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textAmount, "Please enter deposit amount", out data.deposit))
                    return;

                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }

                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }
    }
}
