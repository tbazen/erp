using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
using BIZNET.iERP;
using INTAPS.Accounting;
using INTAPS.Payroll.Client;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CASH_HANDOVER)]
    public class CashHandoverClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new CashHandoverForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            CashHandoverData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CASH_HANDOVER, 0, true) as CashHandoverData;
            AccountDocument doc=AccountingClient.GetAccountDocument(data.handoverDocumentID,false);
            string date=doc==null?"":AccountBase.FormatDate(doc.DocumentDate);
            string reference=doc==null?"":string.Format("{0}<a href='print?docID={1}'>Print Receipt</a>", HttpUtility.HtmlEncode(doc.PaperRef+"  "),doc.AccountDocumentID);
            string html;
            if (data == null)
                html = "<span class='error'>Empty data</span>";
            else
            {
                if (data.bankDespoit)
                {
                    html = "<br/><span class='jobSection1'>Bank Deposit Detail</span><br/>";
                    html += string.Format(@"<b>Bank deposit by </b>{0} to {1} <b>
                                                <br/><b>Date: </b>{2}<br/><b>Reference: </b>{3}<br/><b>Bank Reference:</b>{4}<br/><b>Deposit Date:</b>{5}",
                        PayrollClient.GetEmployee(SubscriberManagmentClient.GetPaymentCenterByCashAccount(data.sourceCashAccountID).casheir).employeeName
                        ,BIZNET.iERP.Client.iERPTransactionClient.GetBankAccount(data.destinationAccountID).BankBranchAccount
                        , HttpUtility.HtmlEncode(date)
                        , reference
                        , HttpUtility.HtmlEncode(data.bankDespositReference)
                        ,HttpUtility.HtmlEncode(AccountBase.FormatDate(data.bankDespositDate))
                        );
                }
                else
                {
                    html = "<br/><span class='jobSection1'>Cash Handover Detail</span><br/>";
                    PaymentCenter pcSource = SubscriberManagmentClient.GetPaymentCenterByCashAccount(data.sourceCashAccountID);
                    PaymentCenter pcDest = SubscriberManagmentClient.GetPaymentCenterByCashAccount(data.destinationAccountID);
                    html += string.Format("<b>Handover of cash from </b>{0} to {1}<br/><b>Date: </b>{2}<br/><b>Reference: </b>{3}",
                        PayrollClient.GetEmployee(pcSource.casheir).employeeName,
                        PayrollClient.GetEmployee(pcDest.casheir).employeeName
                        , HttpUtility.HtmlEncode(date)
                        , reference
                        );
                }
                double total = 0;
                string table = null;
                foreach (PaymentInstrumentItem item in data.instruments)
                {
                    if (table == null)
                    {
                        table = "<table><thead><tr><td>Payment Instrument</td><td>Bank</td><td>Number</td><td>Date</td><td>Amount<td><tbody>";
                    }
                    PaymentInstrumentType type = BIZNET.iERP.Client.iERPTransactionClient.getPaymentInstrumentType(item.instrumentTypeItemCode);
                    BankBranchInfo branch =type.isBankDocument? BIZNET.iERP.Client.iERPTransactionClient.getBankBranchInfo(item.bankBranchID):null;
                    BankInfo bank = type.isBankDocument && branch!=null ? BIZNET.iERP.Client.iERPTransactionClient.getBankInfo(branch.bankID) : null;

                    string bankString =
                    table += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                        type.name,
                        type.isTransferFromAccount ? bank.name + "-" + branch.name : "",
                        type.isBankDocument ? item.documentReference : "",
                        type.isBankDocument ? AccountBase.FormatDate(item.documentDate) : "",
                        AccountBase.FormatAmount(item.amount)
                        );

                    total += item.amount;
                }
                if (table != null)
                {
                    table += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td></tr>",
                        "Total", "", "","", AccountBase.FormatAmount(total)
                );
                    html += table;
                }
                else
                    html += "<span class='error'>No payment instrument found </span>";
            }
            return html;
        }
        public override bool processURL(System.Windows.Forms.Form parent, string path, System.Collections.Hashtable query)
        {
            switch (path)
            {
                case "print":
                    parent.Invoke(new ProcessTowParameter<System.Windows.Forms.Form,int>(processPrintReceipt),parent, int.Parse((string)query["docID"]));
                    return true;
            }
            return false;
        }
        void processPrintReceipt(System.Windows.Forms.Form parent,int docID)
        {
            StandardJobRuleClient.CashHandover.CashHandoverReceiptData data = new StandardJobRuleClient.CashHandover.CashHandoverReceiptData();
            CashHandoverDocument receipt = AccountingClient.GetAccountDocument(docID, true) as CashHandoverDocument;
            CompanyProfile prof= BIZNET.iERP.Client.iERPTransactionClient.GetCompanyProfile();
           JobStatusHistory[] hist= JobManagerClient.GetJobHistory(receipt.data.jobID); 
            string preparedBy="";
            string approvedBy="";
            string preparedDate="";
            string approvedDate="";
            string note="";
            foreach(JobStatusHistory h in hist)
            {
                if(h.newStatus==StandardJobStatus.APPLICATION_APPROVAL)
                {
                    preparedBy=PayrollClient.GetEmployeeByLoginName(h.agent ).employeeName;
                    preparedDate=AccountBase.FormatDate(h.actionDate);
                    note = h.note;
                }
                if(h.newStatus==StandardJobStatus.FINISHED)
                {
                    approvedBy=PayrollClient.GetEmployeeByLoginName(h.agent ).employeeName;
                    approvedDate=AccountBase.FormatDate(h.actionDate);
                }
            }
            INTAPS.WSIS.Job.Client.StandardJobRuleClient.CashHandover.CashHandoverReceiptData.headerRow hr= data.header.AddheaderRow(prof.Name
                ,prof.Logo
                ,AccountBase.FormatDate(receipt.DocumentDate)
                ,receipt.PaperRef
                ,SubscriberManagmentClient.GetPaymentCenterEmployeeByCashAccountID(receipt.data.sourceCashAccountID).employeeName
                ,receipt.data.bankDespoit?BIZNET.iERP.Client.iERPTransactionClient.GetBankAccount(receipt.data.destinationAccountID).BankBranchAccount :SubscriberManagmentClient.GetPaymentCenterEmployeeByCashAccountID(receipt.data.destinationAccountID).employeeName
                ,preparedBy
                ,approvedBy
                ,AccountBase.FormatAmount( receipt.data.total)
                ,prof.Telephone
                ,preparedDate
                ,approvedDate
                ,note
                ,""
                ,receipt.data.bankDespoit?"Deposited To":"Received By"
                ,receipt.data.bankDespoit ? "Bank Deposit Receipt" : "Cash Handover Receipt"
                ,receipt.data.bankDespoit ? "Deposited By" : "Paid By"
                );
            
            string cashItem=BIZNET.iERP.Client.iERPTransactionClient.GetSystemParamter("cashInstrumentCode") as string;
            foreach (PaymentInstrumentItem item in receipt.data.instruments)
            {
                string numLabel = item.instrumentTypeItemCode.Equals(cashItem) ? "" : "Number";
                data.detail.AdddetailRow(hr, BIZNET.iERP.Client.iERPTransactionClient.getPaymentInstrumentType(item.instrumentTypeItemCode).name, numLabel, item.documentReference,AccountBase.FormatAmount(item.amount));
            }
            CashHandoverReceiptViewer v = new CashHandoverReceiptViewer(receipt, data);
            v.Show(parent);
        }
        public override Type getApplicationFormType()
        {
            return typeof(CashHandoverForm);
        }
    }
}
