﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using System.Threading;
using BIZNET.iERP;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.WSIS.Job.Client;
using BIZNET.iERP.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class CashHandoverForm : NewApplicationForm
    {
        int _jobID=-1;
        Thread _updateThread = null;
        bool _updateReport = false;
        string _html = null;
        CashHandoverDocument _prevHandoverDocument=null;
        PaymentCenter _center=null;
        bool _ignoreEvent = false;
        public CashHandoverForm()
        {
            InitializeComponent();
            initializeGrid();
        }
        public CashHandoverForm(int cashAccountID):this()//new job
        {
            setCashAccount(cashAccountID);
            checkDesposit_CheckedChanged(null, null);
        }
        public CashHandoverForm(int jobID, Subscriber customer):this()//update job
        {
            _ignoreEvent =true;
            _jobID = jobID;

            if (_jobID!= -1)
            {
                CashHandoverData data = JobManagerClient.getWorkFlowData(_jobID, StandardJobTypes.CASH_HANDOVER, 0, true) as CashHandoverData;
                setCashAccount(data.sourceCashAccountID);
                checkDesposit.Checked = data.bankDespoit;
                if (data.bankDespoit)
                {
                    bankAccountPlaceholder.mainCsAccountID = data.destinationAccountID;
                    textDepositReference.Text = data.bankDespositReference;
                    dateDeposit.DateTime = data.bankDespositDate;
                }
                string cashInst = iERPTransactionClient.GetSystemParamter("cashInstrumentCode") as string;
                double cashAmount = 0;
                foreach (PaymentInstrumentItem i in data.instruments)
                {
                    if (i.instrumentTypeItemCode.Equals(cashInst))
                    {
                        cashAmount = i.amount;
                        break;
                    }
                }
                if (AccountBase.AmountEqual(cashAmount, 0))
                    textCashAmount.Text = "";
                else 
                    textCashAmount.Text = AccountBase.FormatAmount(cashAmount);

                foreach (DataGridViewRow row in gridControl.Rows)
                {
                    PaymentInstrumentItem item = row.Tag as PaymentInstrumentItem;
                    bool found=false;
                    foreach (PaymentInstrumentItem i in data.instruments)
                    {
                        if (i.instrumentTypeItemCode.Equals(item.instrumentTypeItemCode) && i.accountNumber.Equals(item.accountNumber) && i.documentReference.Equals(item.documentReference))
                        {
                            found = true;
                            break;
                        }
                    }
                    row.Cells[0].Value = found;
                }
                updateTotal();
            }
            _ignoreEvent = false;
        }
        void setCashAccount(int cashAccountID)
        {
            _center = SubscriberManagmentClient.GetPaymentCenterByCashAccount(cashAccountID);
            _prevHandoverDocument = JobManagerClient.getLastCashHandoverDocument(_center.summaryAccountID);
            labelPreviosHandover.Text = _prevHandoverDocument == null ? "" : "Handover from: "+AccountBase.FormatDate(_prevHandoverDocument.DocumentDate);
            labelAsOf.Text ="Handover up to: "+ AccountBase.FormatDate(DateTime.Today);
            
            this.Text = "Cash handover for " + _center.centerName;
            loadLedger();
        }
        private void startUpdateThread()
        {
            _updateReport = false;
            _updateThread = new Thread(delegate()
                {
                    _html = buildHTML();
                });
            _updateThread.Start();
            timer.Enabled = true;
        }

        private void updatePage()
        {
            controlBrowser.LoadTextPage("Bank Reconciliation", buildHTML());
        }

        void loadLedger()
        {
            _ignoreEvent = true;
            gridControl.Rows.Clear();
            string cashInst = iERPTransactionClient.GetSystemParamter("cashInstrumentCode") as string;
            PaymentInstrumentItem[] instruments1 = JobManagerClient.getPaymentCenterPaymentInstruments(_center.casherAccountID, _prevHandoverDocument==null? INTAPS.DateTools.NullDateValue:_prevHandoverDocument.DocumentDate,DateTime.Now);
            PaymentInstrumentItem[] instruments2 = JobManagerClient.getPaymentCenterPaymentInstruments(_center.summaryAccountID, _prevHandoverDocument == null ? INTAPS.DateTools.NullDateValue : _prevHandoverDocument.DocumentDate, DateTime.Now);
            PaymentInstrumentItem[] instruments = INTAPS.ArrayExtension.mergeArray(instruments1, instruments2);
            double cashAmount = 0;
            double noneCashAmount = 0;
            foreach (PaymentInstrumentItem inst in instruments)
            {
                if (inst.instrumentTypeItemCode.Equals(cashInst))
                {
                    cashAmount += inst.amount;
                    textCashAmount.Text = AccountBase.FormatAmount(inst.amount);
                }
                else
                {
                    int index = gridControl.Rows.Add(true, iERPTransactionClient.getPaymentInstrumentType(inst.instrumentTypeItemCode).name, inst.documentReference, AccountBase.FormatDate(inst.documentDate), AccountBase.FormatAmount(inst.amount)
                        ,inst.bankBranchID==-1? null: iERPTransactionClient.getBankBranchInfo(inst.bankBranchID).name, inst.accountNumber);
                    gridControl.Rows[index].Tag = inst;
                    noneCashAmount += inst.amount;
                }
            }
            labelCashDebit.Text = AccountBase.FormatAmount(cashAmount);
            labelNoneCaseh.Text = AccountBase.FormatAmount(noneCashAmount);
            updateTotal();
            _ignoreEvent = false;
        }
        public string buildHTML()
        {
            try
            {
                
                
                StringBuilder builder = new StringBuilder();
                generateTitle(builder);
                return builder.ToString();
            }
            catch (Exception ex)
            {
                return System.Web.HttpUtility.HtmlEncode(ex.Message + "\n" + ex.StackTrace);
            }
        }
        

        void initializeGrid()
        {
        }

        

        

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!_updateThread.IsAlive)
            {
                timer.Enabled = false;
                controlBrowser.LoadTextPage("Cash handover", _html);
                if (_updateReport)
                    startUpdateThread();
            }

        }

        private void textBalance_Validated(object sender, EventArgs e)
        {
            updatePage();
        }
        private void generateTitle(StringBuilder builder)
        {
            builder.Append("<h2>Cash Handover Document</h2>");
            builder.Append("<h2>" + System.Web.HttpUtility.HtmlEncode(_center.userName) + "</h2>");
            builder.Append(string.Format("<span class='SubTitle'><b>As of</b> <u>{0}</u></span>", AccountBase.FormatDate(DateTime.Now)));
        }

        private void textBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                updatePage();
            }
        }

        

        private void controlBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = _jobID==-1;
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CASH_HANDOVER;
                }
                else
                    app = JobManagerClient.GetJob(_jobID);
                CashHandoverData data = new CashHandoverData();
                data.receiptNumber = null;
                int destinationAccountID=-1;
                if (checkDesposit.Checked)
                {
                    if (!INTAPS.UI.Helper.ValidateNonEmptyTextBox(textDepositReference, "Please enter bank deposit slip number.", out data.bankDespositReference))
                        return;
                    if (dateDeposit.DateTime > DateTime.Now)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid deposit date.");
                        return;
                    }
                    if(bankAccountPlaceholder.mainCsAccountID==-1)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please deposit select bank account.");
                        return;
                    }
                    destinationAccountID = bankAccountPlaceholder.mainCsAccountID;
                }
                
                double cashAmount = 0;
                if (!string.IsNullOrEmpty(textCashAmount.Text))
                {
                    if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textCashAmount, "Please enter valid cash amount.", out cashAmount))
                        return;
                }
                List<PaymentInstrumentItem> instrumentItem = new List<PaymentInstrumentItem>();
                foreach (DataGridViewRow row in gridControl.Rows)
                {
                    PaymentInstrumentItem inst = row.Tag as PaymentInstrumentItem;
                    if ((bool)row.Cells[0].Value)
                        instrumentItem.Add(inst);
                }
                if (AccountBase.AmountGreater(cashAmount, 0))
                {
                    string cashInst = iERPTransactionClient.GetSystemParamter("cashInstrumentCode") as string;
                    PaymentInstrumentItem cashItem = new PaymentInstrumentItem();
                    cashItem.instrumentTypeItemCode = cashInst;
                    cashItem.amount = cashAmount;
                    instrumentItem.Add(cashItem);
                }
                data.instruments = instrumentItem.ToArray();
                data.sourceCashAccountID = _center.summaryAccountID;
                data.destinationAccountID = destinationAccountID;
                data.bankDespoit = checkDesposit.Checked;
                data.bankDespositDate = dateDeposit.DateTime;
                data.bankDespositReference = data.bankDespoit ? textDepositReference.Text.Trim() : "";

                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void checkDesposit_CheckedChanged(object sender, EventArgs e)
        {
            panelBankDeposit.Enabled = checkDesposit.Checked;
        }
        void updateTotal()
        {
            double total;
            if (!double.TryParse(textCashAmount.Text, out total))
                total = 0;
            foreach (DataGridViewRow row in gridControl.Rows)
            {
                PaymentInstrumentItem inst = row.Tag as PaymentInstrumentItem;
                if ((bool)row.Cells[0].Value)
                    total += inst.amount;
            }
            labelTotal.Text = AccountBase.FormatAmount(total);
        }

        private void textCashAmount_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            updateTotal();
        }

        private void gridControl_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (_ignoreEvent) return;
            updateTotal();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        
    }

}