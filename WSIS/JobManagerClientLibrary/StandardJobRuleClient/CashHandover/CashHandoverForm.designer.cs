﻿namespace INTAPS.WSIS.Job.Client
{
    partial class CashHandoverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelPreviosHandover = new System.Windows.Forms.Label();
            this.gridControl = new System.Windows.Forms.DataGridView();
            this.colFond = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReference = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBank = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAccount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEdit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.labelAsOf = new System.Windows.Forms.Label();
            this.controlBrowser = new INTAPS.UI.HTML.ControlBrowser();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonOk = new System.Windows.Forms.Button();
            this.panelBankDeposit = new System.Windows.Forms.Panel();
            this.dateDeposit = new INTAPS.Ethiopic.DualCalendar();
            this.bankAccountPlaceholder = new INTAPS.SubscriberManagment.Client.BankAccountPlaceholder();
            this.label4 = new System.Windows.Forms.Label();
            this.textDepositReference = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.checkDesposit = new System.Windows.Forms.CheckBox();
            this.textCashAmount = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelNoneCaseh = new System.Windows.Forms.Label();
            this.labelCashDebit = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelBankDeposit.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelPreviosHandover
            // 
            this.labelPreviosHandover.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPreviosHandover.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPreviosHandover.ForeColor = System.Drawing.Color.Black;
            this.labelPreviosHandover.Location = new System.Drawing.Point(789, 41);
            this.labelPreviosHandover.Name = "labelPreviosHandover";
            this.labelPreviosHandover.Size = new System.Drawing.Size(193, 20);
            this.labelPreviosHandover.TabIndex = 9;
            this.labelPreviosHandover.Text = "##";
            // 
            // gridControl
            // 
            this.gridControl.AllowUserToAddRows = false;
            this.gridControl.AllowUserToDeleteRows = false;
            this.gridControl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFond,
            this.colType,
            this.colReference,
            this.colDate,
            this.colAmount,
            this.colBank,
            this.colAccount,
            this.colEdit});
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.Location = new System.Drawing.Point(0, 0);
            this.gridControl.Name = "gridControl";
            this.gridControl.Size = new System.Drawing.Size(994, 192);
            this.gridControl.TabIndex = 4;
            this.gridControl.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridControl_CellEndEdit);
            // 
            // colFond
            // 
            this.colFond.HeaderText = "";
            this.colFond.Name = "colFond";
            this.colFond.Width = 30;
            // 
            // colType
            // 
            this.colType.HeaderText = "Type";
            this.colType.Name = "colType";
            // 
            // colReference
            // 
            this.colReference.HeaderText = "Number";
            this.colReference.Name = "colReference";
            // 
            // colDate
            // 
            this.colDate.HeaderText = "Date";
            this.colDate.Name = "colDate";
            // 
            // colAmount
            // 
            this.colAmount.HeaderText = "Amount";
            this.colAmount.Name = "colAmount";
            // 
            // colBank
            // 
            this.colBank.HeaderText = "Bank Name";
            this.colBank.Name = "colBank";
            // 
            // colAccount
            // 
            this.colAccount.HeaderText = "Account";
            this.colAccount.Name = "colAccount";
            // 
            // colEdit
            // 
            this.colEdit.HeaderText = "";
            this.colEdit.Name = "colEdit";
            this.colEdit.Width = 30;
            // 
            // labelAsOf
            // 
            this.labelAsOf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelAsOf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelAsOf.ForeColor = System.Drawing.Color.SeaGreen;
            this.labelAsOf.Location = new System.Drawing.Point(789, 70);
            this.labelAsOf.Name = "labelAsOf";
            this.labelAsOf.Size = new System.Drawing.Size(193, 20);
            this.labelAsOf.TabIndex = 1;
            // 
            // controlBrowser
            // 
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(0, 0);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(994, 122);
            this.controlBrowser.StyleSheetFile = "berp.css";
            this.controlBrowser.TabIndex = 0;
            this.controlBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.controlBrowser_DocumentCompleted);
            // 
            // timer
            // 
            this.timer.Interval = 200;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 224);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.gridControl);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.controlBrowser);
            this.splitContainer1.Size = new System.Drawing.Size(994, 318);
            this.splitContainer1.SplitterDistance = 192;
            this.splitContainer1.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Khaki;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panelBankDeposit);
            this.panel1.Controls.Add(this.checkDesposit);
            this.panel1.Controls.Add(this.textCashAmount);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.labelNoneCaseh);
            this.panel1.Controls.Add(this.labelCashDebit);
            this.panel1.Controls.Add(this.labelTotal);
            this.panel1.Controls.Add(this.labelAsOf);
            this.panel1.Controls.Add(this.labelPreviosHandover);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(994, 224);
            this.panel1.TabIndex = 5;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.DarkOrange;
            this.panel2.Controls.Add(this.buttonOk);
            this.panel2.Location = new System.Drawing.Point(0, 179);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(994, 45);
            this.panel2.TabIndex = 63;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.Location = new System.Drawing.Point(856, 9);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(126, 30);
            this.buttonOk.TabIndex = 62;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // panelBankDeposit
            // 
            this.panelBankDeposit.Controls.Add(this.dateDeposit);
            this.panelBankDeposit.Controls.Add(this.bankAccountPlaceholder);
            this.panelBankDeposit.Controls.Add(this.label4);
            this.panelBankDeposit.Controls.Add(this.textDepositReference);
            this.panelBankDeposit.Controls.Add(this.label5);
            this.panelBankDeposit.Controls.Add(this.label3);
            this.panelBankDeposit.Location = new System.Drawing.Point(12, 41);
            this.panelBankDeposit.Name = "panelBankDeposit";
            this.panelBankDeposit.Size = new System.Drawing.Size(760, 58);
            this.panelBankDeposit.TabIndex = 13;
            // 
            // dateDeposit
            // 
            this.dateDeposit.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.dateDeposit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateDeposit.Location = new System.Drawing.Point(453, 6);
            this.dateDeposit.Name = "dateDeposit";
            this.dateDeposit.ShowEthiopian = true;
            this.dateDeposit.ShowGregorian = true;
            this.dateDeposit.ShowTime = false;
            this.dateDeposit.Size = new System.Drawing.Size(264, 46);
            this.dateDeposit.TabIndex = 12;
            this.dateDeposit.VerticalLayout = true;
            // 
            // bankAccountPlaceholder
            // 
            this.bankAccountPlaceholder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bankAccountPlaceholder.FormattingEnabled = true;
            this.bankAccountPlaceholder.Location = new System.Drawing.Point(149, 5);
            this.bankAccountPlaceholder.Name = "bankAccountPlaceholder";
            this.bankAccountPlaceholder.Size = new System.Drawing.Size(209, 21);
            this.bankAccountPlaceholder.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(3, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Deposit Slip:";
            // 
            // textDepositReference
            // 
            this.textDepositReference.Location = new System.Drawing.Point(149, 32);
            this.textDepositReference.Name = "textDepositReference";
            this.textDepositReference.Size = new System.Drawing.Size(208, 20);
            this.textDepositReference.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(388, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Bank Account:";
            // 
            // checkDesposit
            // 
            this.checkDesposit.AutoSize = true;
            this.checkDesposit.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkDesposit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkDesposit.Location = new System.Drawing.Point(12, 14);
            this.checkDesposit.Name = "checkDesposit";
            this.checkDesposit.Size = new System.Drawing.Size(111, 21);
            this.checkDesposit.TabIndex = 12;
            this.checkDesposit.Text = "Bank Deposit";
            this.checkDesposit.UseVisualStyleBackColor = true;
            this.checkDesposit.CheckedChanged += new System.EventHandler(this.checkDesposit_CheckedChanged);
            // 
            // textCashAmount
            // 
            this.textCashAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textCashAmount.Location = new System.Drawing.Point(162, 146);
            this.textCashAmount.Name = "textCashAmount";
            this.textCashAmount.Size = new System.Drawing.Size(208, 20);
            this.textCashAmount.TabIndex = 11;
            this.textCashAmount.TextChanged += new System.EventHandler(this.textCashAmount_TextChanged);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(399, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "None Cash Amount:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(14, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "Cash Amount:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(496, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Total:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(14, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Handed Over Cash:";
            // 
            // labelNoneCaseh
            // 
            this.labelNoneCaseh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelNoneCaseh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNoneCaseh.ForeColor = System.Drawing.Color.SeaGreen;
            this.labelNoneCaseh.Location = new System.Drawing.Point(540, 112);
            this.labelNoneCaseh.Name = "labelNoneCaseh";
            this.labelNoneCaseh.Size = new System.Drawing.Size(232, 20);
            this.labelNoneCaseh.TabIndex = 1;
            // 
            // labelCashDebit
            // 
            this.labelCashDebit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCashDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCashDebit.ForeColor = System.Drawing.Color.SeaGreen;
            this.labelCashDebit.Location = new System.Drawing.Point(162, 112);
            this.labelCashDebit.Name = "labelCashDebit";
            this.labelCashDebit.Size = new System.Drawing.Size(207, 20);
            this.labelCashDebit.TabIndex = 1;
            // 
            // labelTotal
            // 
            this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelTotal.ForeColor = System.Drawing.Color.SeaGreen;
            this.labelTotal.Location = new System.Drawing.Point(540, 150);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(232, 20);
            this.labelTotal.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Type";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Number";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Date";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Bank Name";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Account";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // CashHandoverForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(994, 542);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "CashHandoverForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cash Handover";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panelBankDeposit.ResumeLayout(false);
            this.panelBankDeposit.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelAsOf;
        private INTAPS.UI.HTML.ControlBrowser controlBrowser;
        private System.Windows.Forms.DataGridView gridControl;
        private System.Windows.Forms.Label labelPreviosHandover;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textCashAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.CheckBox checkDesposit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Panel panelBankDeposit;
        private SubscriberManagment.Client.BankAccountPlaceholder bankAccountPlaceholder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textDepositReference;
        private System.Windows.Forms.Label label3;
        private Ethiopic.DualCalendar dateDeposit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colFond;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colReference;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBank;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAccount;
        private System.Windows.Forms.DataGridViewButtonColumn colEdit;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelNoneCaseh;
        private System.Windows.Forms.Label labelCashDebit;
        private System.Windows.Forms.Panel panel2;
    }
}