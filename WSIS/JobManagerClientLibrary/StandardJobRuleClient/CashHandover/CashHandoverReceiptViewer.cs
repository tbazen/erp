﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Drawing.Imaging;

namespace INTAPS.WSIS.Job.Client
{
    public partial class CashHandoverReceiptViewer : Form
    {
       
        List<System.IO.Stream> _streams = new List<System.IO.Stream>();
        public CashHandoverReceiptViewer(CashHandoverDocument doc, StandardJobRuleClient.CashHandover.CashHandoverReceiptData data)
        {
            InitializeComponent();
            printDocument.DefaultPageSettings.PrinterSettings.PrinterName = System.Configuration.ConfigurationManager.AppSettings["A4Printer"];
            LocalReport lr = new LocalReport();
            lr.ReportPath = Application.StartupPath + "\\rdlc\\CashHandoverReceiptReport.rdlc";

            lr.DataSources.Add(new ReportDataSource("d1", (DataTable)data.header));
            lr.DataSources.Add(new ReportDataSource("d2", (DataTable)data.detail));
            string deviceInfo =
          @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.5in</PageWidth>
                <PageHeight>11in</PageHeight>
                <MarginTop>0.25in</MarginTop>
                <MarginLeft>0.25in</MarginLeft>
                <MarginRight>0.25in</MarginRight>
                <MarginBottom>0.25in</MarginBottom>
            </DeviceInfo>";
            Warning [] w;
            string[] copies =
                doc.data.bankDespoit?new string[] { "(Depositor Copy)", "(File Copy)" }:
                new string[] { "(Payer Copy)", "(Receiver Copy)", "(File Copy)" };
            foreach (string copy in copies)
            {
                data.header[0].copy = copy;
                lr.Refresh();
                lr.Render("Image", deviceInfo, delegate(string name, string ext, Encoding enc, string mime, bool seek)
                {
                    MemoryStream s = new MemoryStream();
                    _streams.Add(s);
                    return s;
                }, out w);
            }
            foreach (MemoryStream s in _streams)
                s.Position = 0;

        }
      

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            _pageIndex = 0;
            printDocument.Print();
        }
        int _pageIndex = 0;
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            _streams[_pageIndex].Position = 0;
            Metafile pageImage = new Metafile(_streams[_pageIndex]);
            Rectangle rec = new Rectangle(e.PageBounds.Left - (int)e.PageSettings.HardMarginX
                , e.PageBounds.Top - (int)e.PageSettings.HardMarginY
                , e.PageBounds.Width
                , e.PageBounds.Height);
            e.Graphics.FillRectangle(Brushes.White, rec);
            e.Graphics.DrawImage(pageImage, rec);
            _pageIndex++;
            e.HasMorePages = _pageIndex < _streams.Count;
        }

        private void printDocument_EndPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           }

        private void CashHandoverReceiptViewer_Load(object sender, EventArgs e)
        {

        }

        private void CashHandoverReceiptViewer_Resize(object sender, EventArgs e)
        {
            
        }

        private void printPreviewControl_Click(object sender, EventArgs e)
        {

        }
    }
}


        