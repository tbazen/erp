using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CLOSE_CREDIT_SCHEME)]
    public class CloseCreditSchemeClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new CloseCreditSchemeForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            CloseCreditSchemeData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CLOSE_CREDIT_SCHEME, 0, true) as CloseCreditSchemeData;
            string html;
            if (data == null)
                html = "";
            else
            {
                int i = 1;
                html="";
                foreach (int schemeID in data.schemeIDs)
                {
                    html += string.Format("<br/><span class='jobSection1'>Credit scheme {0} </span><br/>", data.schemeIDs.Length > 1 ? i + "/" + data.schemeIDs.Length : "");
                    CreditScheme scheme = SubscriberManagmentClient.getCreditScheme(schemeID);
                    html += "<strong>Date :</strong>{0}<br><strong>Amount :</strong>{1}<br><strong>Monthly Payment Amount :</strong>{2}"
                        .format(INTAPS.Accounting.AccountBase.FormatDate(scheme.issueDate),
                        AccountBase.FormatAmount(scheme.creditedAmount),
                        AccountBase.FormatAmount(scheme.paymentAmount));
                    i++;
                }
            }
            return html;
        }


    }
}
