﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class CloseCreditSchemeForm : NewApplicationForm
    {
        int _jobID;
        JobData _job;
        Subscriber _customer;
        public CloseCreditSchemeForm(int jobID, Subscriber customer)
        {
            InitializeComponent();

            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _customer = customer;
                _job = null;
                loadBills(null);
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                _customer = SubscriberManagmentClient.GetSubscriber(job.customerID);
                CloseCreditSchemeData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CLOSE_CREDIT_SCHEME, 0, true) as CloseCreditSchemeData;
                _job = job;
                loadBills(data);
            }
            setCustomer();
        }


        void setCustomer()
        {
            string html = JobRuleClientBase.createConciseCustString(_customer);
            browser.LoadTextPage("Connection", html);
        }
        private void loadBills(CloseCreditSchemeData data)
        {
            listView.Items.Clear();

            CreditScheme[] existingSchemes;
            if (data == null)
                existingSchemes = new CreditScheme[0];
            else
            {
                existingSchemes = new CreditScheme[data.schemeIDs.Length];
                for(int i=0;i<existingSchemes.Length;i++)
                    existingSchemes[i]=SubscriberManagmentClient.getCreditScheme(data.schemeIDs[i]);
            }
            if (_job == null || !StandardJobStatus.isInactiveState(_job.status))
            {
                double[] billed;
                CreditScheme[] allSchemes = SubscriberManagmentClient.getCustomerCreditSchemes(_customer.id,out billed);
                int i=0;
                foreach (CreditScheme scheme in allSchemes)
                {
                    if (!scheme.active)
                        continue;
                    ListViewItem item = new ListViewItem();
                    item = new ListViewItem(INTAPS.Accounting.AccountBase.FormatDate(scheme.issueDate));
                    
                    item.Tag = scheme;
                    item.SubItems.Add(scheme.creditedAmount.ToString("#,#0.00"));
                    item.SubItems.Add(scheme.paymentAmount.ToString("#,#0.00"));
                    listView.Items.Add(item);
                    foreach (CreditScheme e in existingSchemes)
                    {
                        if (e.id == scheme.id)
                        {
                            item.Checked = true;
                            break;
                        }
                    }                    
                }
            }
            else
            {
                foreach (CreditScheme scheme in existingSchemes)
                {
                    ListViewItem item = new ListViewItem();
                    item = new ListViewItem(INTAPS.Accounting.AccountBase.FormatDate(scheme.issueDate));
                    item.SubItems.Add(scheme.creditedAmount.ToString("#,#0.00"));
                    item.SubItems.Add(scheme.paymentAmount.ToString("#,#0.00"));
                    item.Checked = true;
                    listView.Items.Add(item);
                }
            }
            listView_ItemChecked(null, null);
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CLOSE_CREDIT_SCHEME;
                }
                else
                    app = _job;
                app.customerID = _customer.id;
                
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                CloseCreditSchemeData data = new CloseCreditSchemeData();
                data.schemeIDs= new int[listView.CheckedItems.Count];
                int i = 0;
                foreach (ListViewItem li in listView.CheckedItems)
                {
                    data.schemeIDs[i++] = (li.Tag as CreditScheme).id;
                }


                if (data.schemeIDs.Length == 0)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select at least one crdit scheme");
                    return;
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void listView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            bool allSelected = true;
            bool allNotSelected = true;
            foreach(ListViewItem li in listView.Items)
                if(li.Checked)
                    allNotSelected=false;
                else
                    allSelected=false;
            buttonSelect.Enabled = !allSelected;
            buttonClear.Enabled = !allNotSelected;
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = true;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = false;
        }

        
    }
}
