﻿namespace INTAPS.WSIS.Job.Client
{
    partial class ConnectionMaintenanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.applicationPlaceHolder = new INTAPS.WSIS.Job.Client.ApplicationPlaceHolder();
            this.browserExisting = new INTAPS.UI.HTML.ControlBrowser();
            this.pageMeter = new System.Windows.Forms.TabPage();
            this.meterDataPlaceholder = new INTAPS.WSIS.Job.Client.MeterDataPlaceholder();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pageMeter.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 447);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 47);
            this.panel1.TabIndex = 3;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(502, 6);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(85, 30);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(593, 6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(85, 30);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.pageMeter);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(690, 447);
            this.tabControl.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.applicationPlaceHolder);
            this.tabPage1.Controls.Add(this.browserExisting);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(682, 421);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Customer/Connection Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // applicationPlaceHolder
            // 
            this.applicationPlaceHolder.BackColor = System.Drawing.Color.Khaki;
            this.applicationPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.applicationPlaceHolder.Location = new System.Drawing.Point(3, 3);
            this.applicationPlaceHolder.Name = "applicationPlaceHolder";
            this.applicationPlaceHolder.Size = new System.Drawing.Size(676, 415);
            this.applicationPlaceHolder.TabIndex = 1;
            // 
            // browserExisting
            // 
            this.browserExisting.Location = new System.Drawing.Point(3, 179);
            this.browserExisting.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserExisting.Name = "browserExisting";
            this.browserExisting.Size = new System.Drawing.Size(640, 156);
            this.browserExisting.StyleSheetFile = "jobstyle.css";
            this.browserExisting.TabIndex = 0;
            // 
            // pageMeter
            // 
            this.pageMeter.Controls.Add(this.meterDataPlaceholder);
            this.pageMeter.Location = new System.Drawing.Point(4, 22);
            this.pageMeter.Name = "pageMeter";
            this.pageMeter.Padding = new System.Windows.Forms.Padding(3);
            this.pageMeter.Size = new System.Drawing.Size(682, 421);
            this.pageMeter.TabIndex = 1;
            this.pageMeter.Text = "Meter Information";
            this.pageMeter.UseVisualStyleBackColor = true;
            // 
            // meterDataPlaceholder
            // 
            this.meterDataPlaceholder.BackColor = System.Drawing.Color.Khaki;
            this.meterDataPlaceholder.DataUpdated = true;
            this.meterDataPlaceholder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterDataPlaceholder.Location = new System.Drawing.Point(3, 3);
            this.meterDataPlaceholder.Name = "meterDataPlaceholder";
            this.meterDataPlaceholder.Size = new System.Drawing.Size(676, 415);
            this.meterDataPlaceholder.TabIndex = 0;
            // 
            // ConnectionMaintenanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 494);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "ConnectionMaintenanceForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connection Maintenance";
            this.panel1.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pageMeter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private UI.HTML.ControlBrowser browserExisting;
        private System.Windows.Forms.TabPage pageMeter;
        private MeterDataPlaceholder meterDataPlaceholder;
        private INTAPS.WSIS.Job.Client.ApplicationPlaceHolder applicationPlaceHolder;
    }
}