﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ConnectionMaintenanceForm : NewApplicationForm
    {
        Subscription _connection;
        int _jobID;
        JobData _job;
        public ConnectionMaintenanceForm(int jobID, Subscription connection)
        {
            InitializeComponent();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _connection = connection;
                _job = null;
                setMeterChanage(false);
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                ConntectionMaintenanceData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_MAINTENANCE, 0, true) as ConntectionMaintenanceData;
                _connection = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
                _job = job;
                setMeterChanage(data.changeMeter);
                if (data.changeMeter)
                {
                    meterDataPlaceholder.setMeter(data.meterData);
                }
            }
            setConnection();
        }
        void setMeterChanage(bool change)
        {
            if (change && !tabControl.TabPages.Contains(pageMeter))
            {
                tabControl.TabPages.Add(pageMeter);
            }
            else if (!change && tabControl.TabPages.Contains(pageMeter))
            {
                tabControl.TabPages.Remove(pageMeter);
            }

        }
        void setConnection()
        {
            applicationPlaceHolder.setCustomer(_connection.subscriber,false);
            browserExisting.LoadTextPage("Connection","<h2>Connection Detail</h1>" + BIZNET.iERP.bERPHtmlBuilder.ToString(JobRuleClientBase.createConnectionTable(_connection))
                );
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CONNECTION_MAINTENANCE;
                }
                else
                    app = _job;

                app.customerID = _connection.subscriber.id;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                ConntectionMaintenanceData data = new ConntectionMaintenanceData();
                data.connectionID = _connection.id;
                if (tabControl.TabPages.Contains(pageMeter))
                {
                    if (!meterDataPlaceholder.validateControlData())
                        return;
                    data.meterData = meterDataPlaceholder.getMeterData();
                    data.changeMeter = true;
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }

                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

    }
}
