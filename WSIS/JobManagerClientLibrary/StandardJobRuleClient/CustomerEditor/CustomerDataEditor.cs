using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.EDIT_CUTOMER_DATA)]
    public class EditCustomerDataClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new SubscriberEditor(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            EditCustomerData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.EDIT_CUTOMER_DATA, 0, true) as EditCustomerData;
            string html;
            if (data == null)
                html = "";
            else
            {
                html = "";
                StringBuilder sb;
                if (data.customerDiff != null)
                {
                    if (data.customerDiff.diffType == ClientServer.GenericDiffType.Delete)
                    {
                        Subscriber s = data.oldVersion == null ? SubscriberManagmentClient.GetSubscriber(data.customerDiff.data.id) : data.oldVersion;
                        html = "<br/><span class='jobSection1'>Deleted Customer detail</span><br/>";
                        sb = new StringBuilder();
                        CustomerProfileBuilder.createCustomerTable(s,CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                        html += sb.ToString();
                    }
                    else if (data.customerDiff.diffType == ClientServer.GenericDiffType.Replace)
                    {
                        Subscriber s = data.oldVersion == null ? SubscriberManagmentClient.GetSubscriber(data.customerDiff.data.id) : data.oldVersion;
                        html = "<br/><span class='jobSection1'>Original Customer detail</span><br/>";
                        sb = new StringBuilder();
                        CustomerProfileBuilder.createCustomerTable(s, CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                        html += sb.ToString();

                        html += "<br/><span class='jobSection1'>Edited Customer detail</span><br/>";
                        sb = new StringBuilder();
                        CustomerProfileBuilder.createCustomerTable(data.customerDiff.data, CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                        html += sb.ToString();
                    }
                    else if (data.customerDiff.diffType == ClientServer.GenericDiffType.Add)
                    {
                        html = "<br/><span class='jobSection1'>New Customer Detail</span><br/>";
                        sb = new StringBuilder();
                        CustomerProfileBuilder.createCustomerTable(data.customerDiff.data, CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                        html += sb.ToString();
                    }
                }
                else
                {
                    Subscriber customer = null;
                    foreach (DiffObject<Subscription> con in data.connections)
                    {
                        customer = SubscriberManagmentClient.GetSubscriber(con.data.subscriberID);
                        break;
                    }
                    if (customer != null)
                    {
                        html += "<br/><span class='jobSection1'>Customer Detail</span><br/>";
                        sb = new StringBuilder();
                        CustomerProfileBuilder.createCustomerTable(customer, CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                        html += sb.ToString();
                    }
                }
                int index = -1;
                foreach (DiffObject<Subscription> con in data.connections)
                {
                    index++;
                    Subscription oldData;
                    switch (con.diffType)
                    {
                        case GenericDiffType.Add:
                            if (con.data.subscriberID == -1)
                                html += "<br/><span class='jobSection1'>New Connection</span><br/>";
                            else
                                html += string.Format("<br/><span class='jobSection1'>New Connection Moved from Another Customer</span><br/>");

                            sb = new StringBuilder();
                            CustomerProfileBuilder.createConnectionTable( "", con.data, INTAPS.WSIS.Job.Client.CustomerProfileBuilder.ConnectionProfileOptions.Minimal).build(sb);
                            html += sb.ToString();

                            break;
                        case GenericDiffType.Delete:
                            oldData = data.oldVersions == null || data.oldVersions.Length == 0 || data.oldVersions[index] == null ? SubscriberManagmentClient.GetSubscription(con.data.id, con.data.ticksFrom) : data.oldVersions[index];
                            html += "<br/><span class='jobSection1'>Deleted Connection</span><br/>";
                            sb = new StringBuilder();
                            CustomerProfileBuilder.createConnectionTable("", con.data, INTAPS.WSIS.Job.Client.CustomerProfileBuilder.ConnectionProfileOptions.Minimal).build(sb);
                            html += sb.ToString();

                            break;
                        case GenericDiffType.Replace:
                            oldData = data.oldVersions == null || data.oldVersions.Length == 0 || data.oldVersions[index] == null ? SubscriberManagmentClient.GetSubscription(con.data.id, con.data.ticksFrom) : data.oldVersions[index];
                            html += "<br/><span class='jobSection1'>Original Connection Detail</span><br/>";
                            sb = new StringBuilder();
                            CustomerProfileBuilder.createConnectionTable("", oldData, INTAPS.WSIS.Job.Client.CustomerProfileBuilder.ConnectionProfileOptions.Minimal).build(sb);
                            html += sb.ToString();
                            html += "<br/><span class='jobSection1'>Edited Connection detail</span><br/>";
                            sb = new StringBuilder();
                            CustomerProfileBuilder.createConnectionTable("", con.data, INTAPS.WSIS.Job.Client.CustomerProfileBuilder.ConnectionProfileOptions.Minimal).build(sb);
                            html += sb.ToString();
                            break;
                        case GenericDiffType.NoChange:
                            break;
                        default:
                            break;
                    }
                }
            }
            return html;
        }

        public override bool processURL(System.Windows.Forms.Form parent, string path, System.Collections.Hashtable query)
        {
            return CustomerProfileBuilder.ProcessUrl(parent, path, query);
        }
    }
}
