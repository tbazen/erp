﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using INTAPS.ClientServer.Client;
using BIZNET.iERP.Client;
using BIZNET.iERP;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class MeterDataPlaceholder : UserControl
    {
        public event EventHandler MeterDataChanged;
        bool _dataUpdated = false;
        bool _ignoreEvents = false;
        public bool DataUpdated
        {
            get { return _dataUpdated; }
            set { _dataUpdated = value; }
        }
        void raiseChangedEvent()
        {
            _dataUpdated = true;
            if (MeterDataChanged != null)
                MeterDataChanged(this, null);
        }
        public MeterDataPlaceholder()
        {
            InitializeComponent();
            loadMeterTypes();

            this.cmbOperationalStatus.Items.AddRange(Enum.GetNames(typeof(MaterialOperationalStatus)));
            if (this.cmbOperationalStatus.Items.Count > 0)
            {
                this.cmbOperationalStatus.SelectedIndex = 0;
            }
        }
        public void setMeter(MeterData mat)
        {
            if (mat == null)
                return;
            _ignoreEvents = true;
            try
            {
                this.cmbOperationalStatus.SelectedItem = mat.opStatus.ToString();
                this.txtSerialNo.Text = (mat.serialNo == null) ? "" : mat.serialNo;
                this.txtModel.Text = (mat.modelNo == null) ? "" : mat.modelNo;
                this.textInitialReading.Text = mat.initialMeterReading.ToString();
                foreach (TransactionItems desc in this.comboMeterTypes.Items)
                {
                    if (desc.Code.Equals(mat.itemCode))
                    {
                        this.comboMeterTypes.SelectedItem = desc;
                        break;
                    }
                }
            }
            finally
            {
                _ignoreEvents=false;
            }
        }
        
        private void loadMeterTypes()
        {
            if (!ApplicationClient.isConnected())
                return;
            int systemParameter = Convert.ToInt32(SubscriberManagmentClient.GetSystemParameter("meterMaterialCategory"));
            if (systemParameter > 1)
            {
                try
                {
                    TransactionItems[] descriptionArray = iERPTransactionClient.GetItemsInCategory(systemParameter);
                    if (descriptionArray.Length == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No meter types found.");
                    }
                    foreach (TransactionItems description in descriptionArray)
                    {
                        this.comboMeterTypes.Items.Add(description);
                    }
                    this.comboMeterTypes.SelectedIndex = 0;
                }
                catch (Exception exception)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error loading material types.", exception);
                }
            }

        }

        public MeterData getMeterData()
        {
            MeterData mat = new MeterData();
            if (!string.IsNullOrEmpty(this.cmbOperationalStatus.SelectedItem as string))
            {
                mat.opStatus = (MaterialOperationalStatus)Enum.Parse(typeof(MaterialOperationalStatus), (string)this.cmbOperationalStatus.SelectedItem);
            }
            if (!double.TryParse(textInitialReading.Text, out mat.initialMeterReading))
                mat.initialMeterReading = 0;
            mat.serialNo = this.txtSerialNo.Text;
            mat.modelNo = this.txtModel.Text;
            mat.itemCode = ((TransactionItems)comboMeterTypes.SelectedItem).Code;

            return mat;
        }



        public bool validateControlData()
        {
            if (txtSerialNo.Text.Trim() == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter meter serial no");
                return false;
            }
            if (comboMeterTypes.SelectedIndex == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select meter type");
                return false;
            }
            double i;
            if (!double.TryParse(textInitialReading.Text, out i))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid initial reading");
                return false;
            }
            return true;
        }

        private void comboMeterTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return;
            raiseChangedEvent();
        }

        private void txtModel_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return; 
            raiseChangedEvent();
        }

        private void txtSerialNo_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return; 
            raiseChangedEvent();
        }

        private void cmbOperationalStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents) return; 
            raiseChangedEvent();
        }
    }
}

