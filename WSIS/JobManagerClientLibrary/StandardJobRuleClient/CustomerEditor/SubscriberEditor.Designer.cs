﻿namespace INTAPS.WSIS.Job.Client
{
    public partial class SubscriberEditor 
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubscriberEditor));
            this.label1 = new System.Windows.Forms.Label();
            this.textName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textAddress = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.textEmail = new System.Windows.Forms.TextBox();
            this.textPhone = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textCustomerCode = new System.Windows.Forms.TextBox();
            this.textNameOfPlace = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textAmharicName = new System.Windows.Forms.TextBox();
            this.comboType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboKebele = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupSubscriptions = new System.Windows.Forms.GroupBox();
            this.comboSubtype = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonDeleteCustomer = new System.Windows.Forms.Button();
            this.buttonMerge = new System.Windows.Forms.Button();
            this.buttonAddSubcription = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(7, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // textName
            // 
            this.textName.BackColor = System.Drawing.Color.White;
            this.textName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textName.Location = new System.Drawing.Point(148, 48);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(263, 20);
            this.textName.TabIndex = 1;
            this.textName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(7, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "House No:";
            // 
            // textAddress
            // 
            this.textAddress.BackColor = System.Drawing.Color.White;
            this.textAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAddress.Location = new System.Drawing.Point(148, 154);
            this.textAddress.Multiline = true;
            this.textAddress.Name = "textAddress";
            this.textAddress.Size = new System.Drawing.Size(263, 23);
            this.textAddress.TabIndex = 5;
            this.textAddress.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(670, 7);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(65, 28);
            this.btnOk.TabIndex = 27;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(741, 7);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(65, 28);
            this.btnCancel.TabIndex = 26;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // textEmail
            // 
            this.textEmail.BackColor = System.Drawing.Color.White;
            this.textEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textEmail.Location = new System.Drawing.Point(148, 262);
            this.textEmail.Name = "textEmail";
            this.textEmail.Size = new System.Drawing.Size(263, 20);
            this.textEmail.TabIndex = 9;
            this.textEmail.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // textPhone
            // 
            this.textPhone.BackColor = System.Drawing.Color.White;
            this.textPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textPhone.Location = new System.Drawing.Point(148, 228);
            this.textPhone.Name = "textPhone";
            this.textPhone.Size = new System.Drawing.Size(263, 20);
            this.textPhone.TabIndex = 10;
            this.textPhone.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(7, 265);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 17);
            this.label18.TabIndex = 7;
            this.label18.Text = "Email:";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(7, 231);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(134, 17);
            this.label12.TabIndex = 8;
            this.label12.Text = "Phone No (mobile):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(7, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Customer Code:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // textCustomerCode
            // 
            this.textCustomerCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
            this.textCustomerCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textCustomerCode.Location = new System.Drawing.Point(148, 14);
            this.textCustomerCode.Name = "textCustomerCode";
            this.textCustomerCode.ReadOnly = true;
            this.textCustomerCode.Size = new System.Drawing.Size(263, 20);
            this.textCustomerCode.TabIndex = 1;
            this.textCustomerCode.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // textNameOfPlace
            // 
            this.textNameOfPlace.BackColor = System.Drawing.Color.White;
            this.textNameOfPlace.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textNameOfPlace.Location = new System.Drawing.Point(148, 191);
            this.textNameOfPlace.Multiline = true;
            this.textNameOfPlace.Name = "textNameOfPlace";
            this.textNameOfPlace.Size = new System.Drawing.Size(263, 23);
            this.textNameOfPlace.TabIndex = 5;
            this.textNameOfPlace.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(7, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Name of Place:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(7, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "አማርኛ ስም:";
            // 
            // textAmharicName
            // 
            this.textAmharicName.BackColor = System.Drawing.Color.White;
            this.textAmharicName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAmharicName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAmharicName.Location = new System.Drawing.Point(148, 82);
            this.textAmharicName.Name = "textAmharicName";
            this.textAmharicName.Size = new System.Drawing.Size(263, 23);
            this.textAmharicName.TabIndex = 1;
            this.textAmharicName.TextChanged += new System.EventHandler(this.textName_TextChanged);
            // 
            // comboType
            // 
            this.comboType.BackColor = System.Drawing.Color.White;
            this.comboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboType.FormattingEnabled = true;
            this.comboType.Location = new System.Drawing.Point(148, 296);
            this.comboType.Name = "comboType";
            this.comboType.Size = new System.Drawing.Size(263, 21);
            this.comboType.TabIndex = 3;
            this.comboType.SelectedIndexChanged += new System.EventHandler(this.comboType_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(7, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Kebele:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(7, 300);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Customer Type:";
            // 
            // comboKebele
            // 
            this.comboKebele.BackColor = System.Drawing.Color.White;
            this.comboKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebele.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboKebele.FormattingEnabled = true;
            this.comboKebele.Location = new System.Drawing.Point(148, 117);
            this.comboKebele.Name = "comboKebele";
            this.comboKebele.Size = new System.Drawing.Size(263, 23);
            this.comboKebele.TabIndex = 3;
            this.comboKebele.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 450);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(809, 38);
            this.panel1.TabIndex = 29;
            // 
            // groupSubscriptions
            // 
            this.groupSubscriptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupSubscriptions.Location = new System.Drawing.Point(431, 6);
            this.groupSubscriptions.Name = "groupSubscriptions";
            this.groupSubscriptions.Size = new System.Drawing.Size(377, 362);
            this.groupSubscriptions.TabIndex = 30;
            this.groupSubscriptions.TabStop = false;
            this.groupSubscriptions.Text = "Connection";
            // 
            // comboSubtype
            // 
            this.comboSubtype.BackColor = System.Drawing.Color.White;
            this.comboSubtype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSubtype.FormattingEnabled = true;
            this.comboSubtype.Location = new System.Drawing.Point(148, 323);
            this.comboSubtype.Name = "comboSubtype";
            this.comboSubtype.Size = new System.Drawing.Size(263, 21);
            this.comboSubtype.TabIndex = 3;
            this.comboSubtype.SelectedIndexChanged += new System.EventHandler(this.comboSubtype_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(7, 327);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Customer Sub-Type:";
            // 
            // buttonDeleteCustomer
            // 
            this.buttonDeleteCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonDeleteCustomer.ForeColor = System.Drawing.Color.White;
            this.buttonDeleteCustomer.Image = global::INTAPS.WSIS.Job.Client.Properties.Resources.Delete1;
            this.buttonDeleteCustomer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonDeleteCustomer.Location = new System.Drawing.Point(9, 405);
            this.buttonDeleteCustomer.Name = "buttonDeleteCustomer";
            this.buttonDeleteCustomer.Size = new System.Drawing.Size(132, 39);
            this.buttonDeleteCustomer.TabIndex = 31;
            this.buttonDeleteCustomer.Text = "Delete Customer";
            this.buttonDeleteCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonDeleteCustomer.UseVisualStyleBackColor = false;
            this.buttonDeleteCustomer.Click += new System.EventHandler(this.buttonDeleteCustomer_Click);
            // 
            // buttonMerge
            // 
            this.buttonMerge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMerge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonMerge.ForeColor = System.Drawing.Color.White;
            this.buttonMerge.Image = global::INTAPS.WSIS.Job.Client.Properties.Resources.AddFile_32x32;
            this.buttonMerge.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonMerge.Location = new System.Drawing.Point(513, 405);
            this.buttonMerge.Name = "buttonMerge";
            this.buttonMerge.Size = new System.Drawing.Size(165, 39);
            this.buttonMerge.TabIndex = 31;
            this.buttonMerge.Text = "Add Existing Connection";
            this.buttonMerge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonMerge.UseVisualStyleBackColor = false;
            this.buttonMerge.Click += new System.EventHandler(this.buttonMerge_Click);
            // 
            // buttonAddSubcription
            // 
            this.buttonAddSubcription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAddSubcription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonAddSubcription.ForeColor = System.Drawing.Color.White;
            this.buttonAddSubcription.Image = global::INTAPS.WSIS.Job.Client.Properties.Resources.EditTask_32x32;
            this.buttonAddSubcription.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddSubcription.Location = new System.Drawing.Point(683, 405);
            this.buttonAddSubcription.Name = "buttonAddSubcription";
            this.buttonAddSubcription.Size = new System.Drawing.Size(124, 39);
            this.buttonAddSubcription.TabIndex = 31;
            this.buttonAddSubcription.Text = "Add Connection";
            this.buttonAddSubcription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonAddSubcription.UseVisualStyleBackColor = false;
            this.buttonAddSubcription.Click += new System.EventHandler(this.buttonAddSubcription_Click);
            // 
            // SubscriberEditor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(809, 488);
            this.Controls.Add(this.buttonDeleteCustomer);
            this.Controls.Add(this.buttonMerge);
            this.Controls.Add(this.buttonAddSubcription);
            this.Controls.Add(this.groupSubscriptions);
            this.Controls.Add(this.textEmail);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textPhone);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboKebele);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboSubtype);
            this.Controls.Add(this.comboType);
            this.Controls.Add(this.textCustomerCode);
            this.Controls.Add(this.textAmharicName);
            this.Controls.Add(this.textNameOfPlace);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textAddress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "SubscriberEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Data Editor";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textAddress;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboKebele;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textAmharicName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textCustomerCode;
        private System.Windows.Forms.TextBox textNameOfPlace;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textEmail;
        private System.Windows.Forms.TextBox textPhone;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupSubscriptions;
        private System.Windows.Forms.Button buttonAddSubcription;
        private System.Windows.Forms.ComboBox comboSubtype;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonMerge;
        private System.Windows.Forms.Button buttonDeleteCustomer;

    }
}