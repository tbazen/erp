
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.SubscriberManagment.Client;
using INTAPS.ClientServer;
using INTAPS.WSIS.Job.Client;
namespace INTAPS.WSIS.Job.Client
{

    public partial class SubscriberEditor : NewApplicationForm
    {
        private bool m_ignoreEvents=false;
        private bool m_subscriberModfied=false;
        int _customerID;
        int _jobID;
        JobData _job;
        public SubscriberEditor(int jobID, Subscriber customer)
        {
            this.InitializeComponent();
            this.buttonMerge.Enabled = "true".Equals(System.Configuration.ConfigurationManager.AppSettings["enable_add_existing"]);
            INTAPS.ClientServer.Client.ApplicationClient.PowerGeez.AddControl(this.textAmharicName);
            listKebelesAndCustomerTypes();
            buttonDeleteCustomer.Tag = false;
            _jobID = jobID;
            if (_jobID == -1)
            {
                _job = null;
                if (customer == null)
                {
                    textCustomerCode.ReadOnly = false;
                    _customerID = -1;
                }
                else
                {
                    _customerID = customer.id;
                    setCustomer(customer);
                    foreach (Subscription con in SubscriberManagmentClient.GetSubscriptions(_customerID, DateTime.Now.Ticks))
                    {
                        addConnection(new DiffObject<Subscription>(con, GenericDiffType.NoChange));
                    }
                }

            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);

                EditCustomerData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.EDIT_CUTOMER_DATA, 0, true) as EditCustomerData;
                bool hasChanges = false;
                if (data.customerDiff == null)
                {
                    foreach (DiffObject<Subscription> con in data.connections)
                    {
                        setCustomer(SubscriberManagmentClient.GetSubscriber(con.data.subscriberID));
                        break;
                    }
                    m_subscriberModfied = false;
                }
                else
                {
                    setCustomer(data.customerDiff.data);
                    m_subscriberModfied = data.customerDiff.diffType == GenericDiffType.Add || data.customerDiff.diffType == GenericDiffType.Replace;
                    if (m_subscriberModfied)
                        hasChanges = true;

                }
                foreach (DiffObject<Subscription> con in data.connections)
                {
                    addConnection(con);
                    hasChanges = true;
                }

                if (hasChanges)
                    buttonDeleteCustomer.Visible = false;
                {
                    if (data.customerDiff != null && data.customerDiff.diffType == GenericDiffType.Delete)
                        setDeletedState(true);
                    else
                        setDeletedState(false);
                }
                _job = job;
            }

            
        }
        void setDeletedState(bool deleted)
        {
            buttonDeleteCustomer.Tag=deleted;
            if (deleted)
                buttonDeleteCustomer.Text = "Undo Delete Customer";
            else
                buttonDeleteCustomer.Text = "Delete Customer";
            foreach (Control c in this.Controls)
            {
                bool exclude = false ;
                foreach (Control e in new Control[] { panel1, buttonDeleteCustomer })
                {
                    if (e == c)
                    {
                        exclude = true;
                        break;
                    }
                }
                if (exclude)
                    continue;
                c.Enabled = !deleted;
            }
        }
        const int BUTTON_WIDTH=40;
        const int ROW_HEIGHT=30;
        const int CONT_PER_ROW = 3;
        void addConnection(DiffObject<Subscription> con)
        {
            int y = groupSubscriptions.Controls.Count / CONT_PER_ROW * ROW_HEIGHT;

            Button del = new Button();
            del.Text = "Delete";
            del.BackColor = Color.Red;
            del.Click += new EventHandler(del_Click);
            del.Tag = con;
            del.Enabled = con.diffType != GenericDiffType.Delete;
            del.Size = new Size(BUTTON_WIDTH, ROW_HEIGHT);
            
            
            Button edit = new Button();
            edit.Text = con.diffType==GenericDiffType.Delete?"Undelete":"Edit";
            edit.BackColor = Color.Blue;
            edit.Click += new EventHandler(edit_Click);
            edit.Tag = con;
            edit.Size = new Size(BUTTON_WIDTH, ROW_HEIGHT);
            

            Label l = new Label();
            l.AutoSize = false;
            l.Left = 0;
            
            l.Size = new Size(groupSubscriptions.Width - BUTTON_WIDTH * 2, ROW_HEIGHT);
            setColor(l, con);
            l.Left = 0;
            edit.Left = l.Right;
            del.Left = edit.Right;
            edit.Top=del.Top= l.Top = y;
            groupSubscriptions.Controls.Add(l);
            groupSubscriptions.Controls.Add(edit);
            groupSubscriptions.Controls.Add(del);

        }
        void listSubtype()
        {
            comboSubtype.Items.Clear();
            SubscriberType type=(SubscriberType)Enum.Parse(typeof(SubscriberType), (string)this.comboType.SelectedItem);
            comboSubtype.Items.AddRange(SubscriberManagmentClient.getCustomerSubtypes(type));
        }
        void setColor(Label l, DiffObject<Subscription> con)
        {
            switch (con.diffType)
            {
                case GenericDiffType.Add:
                    l.BackColor = Color.White;
                    break;
                case GenericDiffType.Delete:
                    l.BackColor = Color.Red;
                    break;
                case GenericDiffType.Replace:
                    l.BackColor = Color.Blue;
                    break;
                case GenericDiffType.NoChange:
                    l.BackColor = Color.LightGray;
                    break;
                default:
                    break;
            }
            string desc = con.data.itemCode;
            l.Text = "Contract No:" + con.data.contractNo + ", " + desc;
        }
        void removeConnection(int index)
        {
            int c = groupSubscriptions.Controls.Count;
            for (int i = (index + 1) * CONT_PER_ROW; i < c; i++)
            {
                groupSubscriptions.Controls.RemoveAt(i);
            }
        }
        void del_Click(object sender, EventArgs e)
        {
            Button btn=(Button)sender;
            DiffObject<Subscription> con = (DiffObject<Subscription>)btn.Tag;
            int index = groupSubscriptions.Controls.IndexOf(btn) / CONT_PER_ROW;
            switch (con.diffType)
            {
                case GenericDiffType.Delete:
                    break;
                case GenericDiffType.Add:
                    removeConnection(index);
                    break;
                case GenericDiffType.Replace:
                case GenericDiffType.NoChange:
                    btn.Enabled = false;
                    con.diffType = GenericDiffType.Delete;
                    setColor((Label)groupSubscriptions.Controls[index * CONT_PER_ROW], con);
                    break;
                default:
                    break;
            }
        }

        void edit_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            DiffObject<Subscription> con = (DiffObject<Subscription>)btn.Tag;
            int index = groupSubscriptions.Controls.IndexOf(btn) / CONT_PER_ROW;
            SubscriptionEditorForm editor = new SubscriptionEditorForm(con.data);
            if (editor.ShowDialog(this) != DialogResult.OK)
                return;
            if(con.diffType!=GenericDiffType.Add)        
                con.diffType = GenericDiffType.Replace;
            con.data = editor.connection;
            con.data.ticksFrom = DateTime.Now.Ticks;
            setColor((Label)groupSubscriptions.Controls[index * CONT_PER_ROW], con);
        }

        public void setCustomer(Subscriber s)
        {
            this.m_ignoreEvents = true;
            try
            {
                this.textName.Text = s.name;
                this.textCustomerCode.Text = s.customerCode;
                foreach (Kebele kebele in this.comboKebele.Items)
                {
                    if (kebele.id == s.Kebele)
                    {
                        this.comboKebele.SelectedItem = kebele;
                        break;
                    }
                }
                this.comboType.SelectedItem = s.subscriberType.ToString();
                listSubtype();
                foreach(CustomerSubtype st in this.comboSubtype.Items)
                    if (st.id == s.subTypeID)
                    {
                        this.comboSubtype.SelectedItem = st;
                        break;
                    }
                
                this.textAddress.Text = (s.address == null) ? "" : s.address;
                this.textAmharicName.Text = s.amharicName;

                this.textPhone.Text = s.phoneNo;
                this.textEmail.Text = s.email == null ? "" : s.email;
                this.textNameOfPlace.Text = s.nameOfPlace;

            }
            finally
            {
                this.m_ignoreEvents = false;
            }
        }

        private void listKebelesAndCustomerTypes()
        {
            this.m_ignoreEvents = true;
            try
            {
                this.comboKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());

                Kebele item = new Kebele();
                item.id = -1;
                item.name = "Unknown";
                this.comboKebele.Items.Add(item);
                this.comboKebele.SelectedIndex = 0;

                this.comboType.Items.AddRange(Enum.GetNames(typeof(SubscriberType)));
                this.comboType.SelectedItem = SubscriberType.Private.ToString();

            }
            finally
            {
                this.m_ignoreEvents = false;
            }
        }
        

       
        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            EditCustomerData edit = new EditCustomerData();
            if (m_subscriberModfied)
            {
                edit.customerDiff = new DiffObject<Subscriber>();
                Subscriber subscriber = new Subscriber();
                subscriber.id = _customerID;
                edit.customerDiff = new DiffObject<Subscriber>(subscriber, _customerID <1 ? GenericDiffType.Add : GenericDiffType.Replace);
                subscriber.customerCode = textCustomerCode.Text;
                subscriber.name = this.textName.Text;
                if (subscriber.name == "")
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter the name of the susbcriber.");
                    this.textName.Focus();
                    this.textName.SelectAll();
                    return;
                }
                subscriber.address = this.textAddress.Text;
                subscriber.Kebele = ((Kebele)this.comboKebele.SelectedItem).id;
                subscriber.subscriberType = (SubscriberType)Enum.Parse(typeof(SubscriberType), (string)this.comboType.SelectedItem);
                subscriber.subTypeID = comboSubtype.SelectedIndex == -1 ? -1 : ((CustomerSubtype)comboSubtype.SelectedItem).id;
                subscriber.amharicName = this.textAmharicName.Text;
                subscriber.phoneNo = this.textPhone.Text;
                subscriber.email = textEmail.Text;
                subscriber.nameOfPlace = textNameOfPlace.Text;
            }
            else
            {
                if (_customerID != -1 && (bool)buttonDeleteCustomer.Tag)
                {
                    edit.customerDiff = new DiffObject<Subscriber>();
                    edit.customerDiff.data = SubscriberManagmentClient.GetSubscriber(_customerID);
                    edit.customerDiff.data.id = _customerID;
                    edit.customerDiff.diffType = GenericDiffType.Delete;
                }
            }
            List<DiffObject<Subscription>> list = new List<DiffObject<Subscription>>();
            for (int i = 0; i < groupSubscriptions.Controls.Count / CONT_PER_ROW; i++)
            {
                DiffObject<Subscription> con = groupSubscriptions.Controls[i * CONT_PER_ROW + 1].Tag as DiffObject<Subscription>;
                if (con.diffType != GenericDiffType.NoChange)
                    list.Add(con);
            }
            edit.connections = list.ToArray();

            JobData app;
            bool newJob;
            newJob = (_jobID==-1);
            if (newJob)
            {
                app = new JobData();
                app.applicationType = StandardJobTypes.EDIT_CUTOMER_DATA;
            }
            else
                app = _job;
            if (edit.customerDiff == null || edit.customerDiff.diffType == GenericDiffType.NoChange)
            {
                if (edit.connections.Length == 0)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("No data changed");
                    return;
                }
            }
            app.customerID = _customerID;
            try
            {
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, edit);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, edit);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                m_subscriberModfied = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }

        }

        

        private void comboKebele_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_ignoreEvents)
            {
                customerModified();
            }
        }

        private void customerModified()
        {
            m_subscriberModfied = true;
            buttonDeleteCustomer.Tag = false;
            buttonDeleteCustomer.Visible=false;
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.m_ignoreEvents)
                return;
            customerModified();
            listSubtype();

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void label4_Click(object sender, EventArgs e)
        {
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.m_subscriberModfied )
            {
                e.Cancel = !UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to close this subsriber without saving the changes?");
            }
        }

        private void se_SubscripionUpdate(object sender, EventArgs e)
        {
        }



        private void textAddress_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_ignoreEvents)
            {
                customerModified();
            }
        }

        private void textName_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_ignoreEvents)
            {
                customerModified();
            }
        }



        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void buttonAddSubcription_Click(object sender, EventArgs e)
        {
            SubscriptionEditorForm f = new SubscriptionEditorForm(_customerID);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                
                addConnection(new DiffObject<Subscription>(f.connection, GenericDiffType.Add));
            }
        }

        private void comboSubtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.m_ignoreEvents)
                return;
            customerModified();
        }

        private void buttonMerge_Click(object sender, EventArgs e)
        {
            SubscriberManagment.Client.SubscriptionList list = new SubscriptionList();
            if (list.PickSubscription() == DialogResult.OK)
            {
                if (list.pickedSubscription != null)
                {
                    addConnection(new DiffObject<Subscription>(list.pickedSubscription, GenericDiffType.Add));
                }
            }
        }

        private void buttonDeleteCustomer_Click(object sender, EventArgs e)
        {
            buttonDeleteCustomer.Tag = !(bool)buttonDeleteCustomer.Tag;
            setDeletedState((bool)buttonDeleteCustomer.Tag);
        }
    }
}

