﻿namespace INTAPS.WSIS.Job.Client
{
    public partial class SubscriptionEditor : System.Windows.Forms.UserControl
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textContractNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboKebele = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textAddress = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textLandCertifcate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textStatus = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboSubscriptionType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textParcelCode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.comboConnectionType = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboSupplyCondition = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboDataStatus = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textRemark = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboMeterLocation = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textWMX = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textWMY = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.textHCX = new System.Windows.Forms.TextBox();
            this.textHCY = new System.Windows.Forms.TextBox();
            this.groupLocation = new System.Windows.Forms.GroupBox();
            this.groupStatistics = new System.Windows.Forms.GroupBox();
            this.textHouseHold = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupMeter = new System.Windows.Forms.GroupBox();
            this.meterDataPlaceholder = new INTAPS.WSIS.Job.Client.MeterDataPlaceholder();
            this.dtpDate = new INTAPS.Ethiopic.ETDateTimeDropDown();
            this.comboDMA = new System.Windows.Forms.ComboBox();
            this.labelDMA = new System.Windows.Forms.Label();
            this.comboPressureZone = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupLocation.SuspendLayout();
            this.groupStatistics.SuspendLayout();
            this.groupMeter.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Contract No:";
            // 
            // textContractNo
            // 
            this.textContractNo.Location = new System.Drawing.Point(174, 30);
            this.textContractNo.Name = "textContractNo";
            this.textContractNo.Size = new System.Drawing.Size(252, 20);
            this.textContractNo.TabIndex = 1;
            this.textContractNo.TextChanged += new System.EventHandler(this.textCode_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Kebele:";
            // 
            // comboKebele
            // 
            this.comboKebele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboKebele.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboKebele.FormattingEnabled = true;
            this.comboKebele.Location = new System.Drawing.Point(168, 16);
            this.comboKebele.Name = "comboKebele";
            this.comboKebele.Size = new System.Drawing.Size(252, 23);
            this.comboKebele.TabIndex = 2;
            this.comboKebele.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(458, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "House No:";
            // 
            // textAddress
            // 
            this.textAddress.Location = new System.Drawing.Point(602, 20);
            this.textAddress.Name = "textAddress";
            this.textAddress.Size = new System.Drawing.Size(218, 24);
            this.textAddress.TabIndex = 6;
            this.textAddress.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(458, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Land Certifcate No:";
            // 
            // textLandCertifcate
            // 
            this.textLandCertifcate.Location = new System.Drawing.Point(602, 101);
            this.textLandCertifcate.Name = "textLandCertifcate";
            this.textLandCertifcate.Size = new System.Drawing.Size(218, 24);
            this.textLandCertifcate.TabIndex = 1;
            this.textLandCertifcate.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Date:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(453, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "Status:";
            // 
            // textStatus
            // 
            this.textStatus.Location = new System.Drawing.Point(608, 9);
            this.textStatus.Name = "textStatus";
            this.textStatus.ReadOnly = true;
            this.textStatus.Size = new System.Drawing.Size(207, 20);
            this.textStatus.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Type:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // comboSubscriptionType
            // 
            this.comboSubscriptionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSubscriptionType.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboSubscriptionType.FormattingEnabled = true;
            this.comboSubscriptionType.Location = new System.Drawing.Point(174, 59);
            this.comboSubscriptionType.Name = "comboSubscriptionType";
            this.comboSubscriptionType.Size = new System.Drawing.Size(252, 23);
            this.comboSubscriptionType.TabIndex = 9;
            this.comboSubscriptionType.SelectedIndexChanged += new System.EventHandler(this.comboSubscriptionType_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 103);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Parcel No:";
            // 
            // textParcelCode
            // 
            this.textParcelCode.Location = new System.Drawing.Point(168, 96);
            this.textParcelCode.Name = "textParcelCode";
            this.textParcelCode.Size = new System.Drawing.Size(252, 24);
            this.textParcelCode.TabIndex = 6;
            this.textParcelCode.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 15);
            this.label13.TabIndex = 0;
            this.label13.Text = "Connection Type:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // comboConnectionType
            // 
            this.comboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConnectionType.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboConnectionType.FormattingEnabled = true;
            this.comboConnectionType.Location = new System.Drawing.Point(159, 50);
            this.comboConnectionType.Name = "comboConnectionType";
            this.comboConnectionType.Size = new System.Drawing.Size(218, 23);
            this.comboConnectionType.TabIndex = 2;
            this.comboConnectionType.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 88);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Supply Condition:";
            // 
            // comboSupplyCondition
            // 
            this.comboSupplyCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSupplyCondition.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboSupplyCondition.FormattingEnabled = true;
            this.comboSupplyCondition.Location = new System.Drawing.Point(159, 80);
            this.comboSupplyCondition.Name = "comboSupplyCondition";
            this.comboSupplyCondition.Size = new System.Drawing.Size(218, 23);
            this.comboSupplyCondition.TabIndex = 2;
            this.comboSupplyCondition.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(12, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 15);
            this.label15.TabIndex = 0;
            this.label15.Text = "Data Status:";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // comboDataStatus
            // 
            this.comboDataStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDataStatus.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDataStatus.FormattingEnabled = true;
            this.comboDataStatus.Location = new System.Drawing.Point(159, 20);
            this.comboDataStatus.Name = "comboDataStatus";
            this.comboDataStatus.Size = new System.Drawing.Size(218, 23);
            this.comboDataStatus.TabIndex = 2;
            this.comboDataStatus.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label16.Location = new System.Drawing.Point(16, 456);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 17);
            this.label16.TabIndex = 0;
            this.label16.Text = "Remarks:";
            // 
            // textRemark
            // 
            this.textRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textRemark.Location = new System.Drawing.Point(174, 456);
            this.textRemark.Multiline = true;
            this.textRemark.Name = "textRemark";
            this.textRemark.Size = new System.Drawing.Size(682, 47);
            this.textRemark.TabIndex = 1;
            this.textRemark.TextChanged += new System.EventHandler(this.textCode_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 118);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 15);
            this.label17.TabIndex = 0;
            this.label17.Text = "Meter Location:";
            // 
            // comboMeterLocation
            // 
            this.comboMeterLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMeterLocation.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboMeterLocation.FormattingEnabled = true;
            this.comboMeterLocation.Location = new System.Drawing.Point(159, 110);
            this.comboMeterLocation.Name = "comboMeterLocation";
            this.comboMeterLocation.Size = new System.Drawing.Size(218, 23);
            this.comboMeterLocation.TabIndex = 2;
            this.comboMeterLocation.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 51);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(142, 15);
            this.label19.TabIndex = 0;
            this.label19.Text = "Water Mater Longitude:";
            // 
            // textWMX
            // 
            this.textWMX.Location = new System.Drawing.Point(168, 42);
            this.textWMX.Name = "textWMX";
            this.textWMX.Size = new System.Drawing.Size(252, 24);
            this.textWMX.TabIndex = 1;
            this.textWMX.TextChanged += new System.EventHandler(this.textCode_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(458, 52);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "Lattiude:";
            // 
            // textWMY
            // 
            this.textWMY.Location = new System.Drawing.Point(602, 47);
            this.textWMY.Name = "textWMY";
            this.textWMY.Size = new System.Drawing.Size(218, 24);
            this.textWMY.TabIndex = 1;
            this.textWMY.TextChanged += new System.EventHandler(this.textCode_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(10, 77);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(136, 15);
            this.label21.TabIndex = 0;
            this.label21.Text = "Connection Longitude:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(458, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 15);
            this.label22.TabIndex = 0;
            this.label22.Text = "Lattiude:";
            // 
            // textHCX
            // 
            this.textHCX.Location = new System.Drawing.Point(168, 69);
            this.textHCX.Name = "textHCX";
            this.textHCX.Size = new System.Drawing.Size(252, 24);
            this.textHCX.TabIndex = 1;
            this.textHCX.TextChanged += new System.EventHandler(this.textCode_TextChanged);
            // 
            // textHCY
            // 
            this.textHCY.Location = new System.Drawing.Point(602, 74);
            this.textHCY.Name = "textHCY";
            this.textHCY.Size = new System.Drawing.Size(218, 24);
            this.textHCY.TabIndex = 1;
            this.textHCY.TextChanged += new System.EventHandler(this.textCode_TextChanged);
            // 
            // groupLocation
            // 
            this.groupLocation.Controls.Add(this.label2);
            this.groupLocation.Controls.Add(this.label19);
            this.groupLocation.Controls.Add(this.label3);
            this.groupLocation.Controls.Add(this.label21);
            this.groupLocation.Controls.Add(this.label20);
            this.groupLocation.Controls.Add(this.label22);
            this.groupLocation.Controls.Add(this.textParcelCode);
            this.groupLocation.Controls.Add(this.label11);
            this.groupLocation.Controls.Add(this.textAddress);
            this.groupLocation.Controls.Add(this.label4);
            this.groupLocation.Controls.Add(this.textWMX);
            this.groupLocation.Controls.Add(this.textHCX);
            this.groupLocation.Controls.Add(this.textWMY);
            this.groupLocation.Controls.Add(this.textHCY);
            this.groupLocation.Controls.Add(this.comboKebele);
            this.groupLocation.Controls.Add(this.textLandCertifcate);
            this.groupLocation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupLocation.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupLocation.Location = new System.Drawing.Point(18, 106);
            this.groupLocation.Name = "groupLocation";
            this.groupLocation.Padding = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.groupLocation.Size = new System.Drawing.Size(850, 128);
            this.groupLocation.TabIndex = 13;
            this.groupLocation.TabStop = false;
            this.groupLocation.Text = "Location";
            this.groupLocation.Paint += new System.Windows.Forms.PaintEventHandler(this.groupLocation_Paint);
            this.groupLocation.Enter += new System.EventHandler(this.groupLocation_Enter);
            // 
            // groupStatistics
            // 
            this.groupStatistics.Controls.Add(this.comboConnectionType);
            this.groupStatistics.Controls.Add(this.label13);
            this.groupStatistics.Controls.Add(this.label17);
            this.groupStatistics.Controls.Add(this.label15);
            this.groupStatistics.Controls.Add(this.label14);
            this.groupStatistics.Controls.Add(this.comboMeterLocation);
            this.groupStatistics.Controls.Add(this.textHouseHold);
            this.groupStatistics.Controls.Add(this.label8);
            this.groupStatistics.Controls.Add(this.comboDataStatus);
            this.groupStatistics.Controls.Add(this.comboSupplyCondition);
            this.groupStatistics.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupStatistics.Location = new System.Drawing.Point(461, 244);
            this.groupStatistics.Name = "groupStatistics";
            this.groupStatistics.Padding = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.groupStatistics.Size = new System.Drawing.Size(407, 180);
            this.groupStatistics.TabIndex = 14;
            this.groupStatistics.TabStop = false;
            this.groupStatistics.Text = "Survey Data";
            this.groupStatistics.Paint += new System.Windows.Forms.PaintEventHandler(this.groupStatistics_Paint);
            // 
            // textHouseHold
            // 
            this.textHouseHold.Location = new System.Drawing.Point(159, 140);
            this.textHouseHold.Name = "textHouseHold";
            this.textHouseHold.Size = new System.Drawing.Size(220, 24);
            this.textHouseHold.TabIndex = 6;
            this.textHouseHold.TextChanged += new System.EventHandler(this.textAddress_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(14, 149);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Household Size:";
            // 
            // groupMeter
            // 
            this.groupMeter.Controls.Add(this.meterDataPlaceholder);
            this.groupMeter.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupMeter.Location = new System.Drawing.Point(18, 244);
            this.groupMeter.Name = "groupMeter";
            this.groupMeter.Size = new System.Drawing.Size(437, 180);
            this.groupMeter.TabIndex = 15;
            this.groupMeter.TabStop = false;
            this.groupMeter.Text = "Meter Detail";
            this.groupMeter.Paint += new System.Windows.Forms.PaintEventHandler(this.groupMeter_Paint);
            // 
            // meterDataPlaceholder
            // 
            this.meterDataPlaceholder.DataUpdated = false;
            this.meterDataPlaceholder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.meterDataPlaceholder.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.meterDataPlaceholder.Location = new System.Drawing.Point(3, 20);
            this.meterDataPlaceholder.Name = "meterDataPlaceholder";
            this.meterDataPlaceholder.Size = new System.Drawing.Size(431, 157);
            this.meterDataPlaceholder.TabIndex = 0;
            this.meterDataPlaceholder.MeterDataChanged += new System.EventHandler(this.meterDataPlaceholder_MeterDataChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(174, 4);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(252, 20);
            this.dtpDate.Style = INTAPS.Ethiopic.ETDateStyle.Ethiopian;
            this.dtpDate.TabIndex = 10;
            this.dtpDate.Changed += new System.EventHandler(this.dtpDate_Changed);
            // 
            // comboDMA
            // 
            this.comboDMA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDMA.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDMA.FormattingEnabled = true;
            this.comboDMA.Location = new System.Drawing.Point(608, 35);
            this.comboDMA.Name = "comboDMA";
            this.comboDMA.Size = new System.Drawing.Size(207, 23);
            this.comboDMA.TabIndex = 2;
            this.comboDMA.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // labelDMA
            // 
            this.labelDMA.AutoSize = true;
            this.labelDMA.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDMA.Location = new System.Drawing.Point(453, 43);
            this.labelDMA.Name = "labelDMA";
            this.labelDMA.Size = new System.Drawing.Size(39, 15);
            this.labelDMA.TabIndex = 0;
            this.labelDMA.Text = "DMA:";
            this.labelDMA.Click += new System.EventHandler(this.label9_Click);
            // 
            // comboPressureZone
            // 
            this.comboPressureZone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPressureZone.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboPressureZone.FormattingEnabled = true;
            this.comboPressureZone.Location = new System.Drawing.Point(608, 64);
            this.comboPressureZone.Name = "comboPressureZone";
            this.comboPressureZone.Size = new System.Drawing.Size(207, 23);
            this.comboPressureZone.TabIndex = 2;
            this.comboPressureZone.SelectedIndexChanged += new System.EventHandler(this.comboKebele_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(453, 67);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Pressure Zone:";
            this.label10.Click += new System.EventHandler(this.label9_Click);
            // 
            // SubscriptionEditor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Khaki;
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labelDMA);
            this.Controls.Add(this.groupStatistics);
            this.Controls.Add(this.groupLocation);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.comboSubscriptionType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textStatus);
            this.Controls.Add(this.textRemark);
            this.Controls.Add(this.textContractNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupMeter);
            this.Controls.Add(this.comboPressureZone);
            this.Controls.Add(this.comboDMA);
            this.Name = "SubscriptionEditor";
            this.Size = new System.Drawing.Size(885, 515);
            this.groupLocation.ResumeLayout(false);
            this.groupLocation.PerformLayout();
            this.groupStatistics.ResumeLayout(false);
            this.groupStatistics.PerformLayout();
            this.groupMeter.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textContractNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboKebele;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textLandCertifcate;
        private INTAPS.Ethiopic.ETDateTimeDropDown dtpDate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboSubscriptionType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textParcelCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboConnectionType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboSupplyCondition;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboDataStatus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textRemark;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboMeterLocation;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textWMX;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textWMY;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textHCX;
        private System.Windows.Forms.TextBox textHCY;
        private System.Windows.Forms.GroupBox groupLocation;
        private System.Windows.Forms.GroupBox groupStatistics;
        private System.Windows.Forms.TextBox textHouseHold;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupMeter;
        private MeterDataPlaceholder meterDataPlaceholder;
        private System.Windows.Forms.ComboBox comboDMA;
        private System.Windows.Forms.Label labelDMA;
        private System.Windows.Forms.ComboBox comboPressureZone;
        private System.Windows.Forms.Label label10;

    }
}