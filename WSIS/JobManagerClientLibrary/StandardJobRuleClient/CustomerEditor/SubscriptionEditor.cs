
using INTAPS.Accounting;
using INTAPS.Accounting.Client;
using INTAPS.Ethiopic;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using INTAPS.SubscriberManagment.Client;
namespace INTAPS.WSIS.Job.Client
{

    public partial class SubscriptionEditor : UserControl
    {
        public Subscription formData;
        private bool m_IgnoreEvent = false;
        public event EventHandler SubscripiontUpdated;
        bool _dataUpdated = false;

        public bool DataUpdated
        {
            get { return _dataUpdated; }
            set
            {
                _dataUpdated = value;
            }



        }
        public SubscriptionEditor()
        {
            this.InitializeComponent();
        }



        private void chkPrepaid_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        private void comboKebele_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        private void comboSubscriptionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void dtpDate_Changed(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }
        public bool validateControlData()
        {
            double WMx = 0, WMy = 0, HCx = 0, HCy = 0;
            if (comboSubscriptionType.SelectedIndex < 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select connection type");
                return false;
            }
            if (!textWMX.Text.Equals("") || !textWMY.Text.Equals(""))
            {
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textWMX, "Please enter valid water meter x coordinate", out WMx))
                    return false;
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textWMY, "Please enter valid water meter y coordinate", out WMy))
                    return false;
            }
            if (!textHCX.Text.Equals("") || !textHCY.Text.Equals(""))
            {
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textHCX, "Please enter valid house connection x coordinate", out HCx))
                    return false;
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textHCY, "Please enter valid house connection y coordinate", out HCy))
                    return false;
            }
            if (!meterDataPlaceholder.validateControlData())
                return false;
            int hz;
            if (!int.TryParse(textHouseHold.Text, out hz) || hz<0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid household size");
                return false;
            }
            return true;
        }
        public Subscription GetSubscription()
        {
            Subscription ret = new Subscription();
            ret.contractNo = textContractNo.Text;
            ret.meterData = meterDataPlaceholder.getMeterData();
            ret.landCertificateNo = this.textLandCertifcate.Text.Trim();
            ret.Kebele = ((Kebele)this.comboKebele.SelectedItem).id;
            ret.dma = this.comboDMA.SelectedItem is DistrictMeteringZone?((DistrictMeteringZone)this.comboDMA.SelectedItem).id:-1;
            ret.pressureZone = this.comboPressureZone.SelectedItem is PressureZone?((PressureZone)this.comboPressureZone.SelectedItem).id:-1;

            ret.landCertificateNo = this.textLandCertifcate.Text;
            ret.subscriptionType = (SubscriptionType)Enum.Parse(typeof(SubscriptionType), (string)this.comboSubscriptionType.SelectedItem);
            ret.address = this.textAddress.Text;
            ret.ticksFrom= this.dtpDate.Value.Ticks;
            ret.subscriptionStatus = SubscriptionStatus.Active;
            
            ret.parcelNo = this.textParcelCode.Text;
            ret.connectionType = (ConnectionType)this.comboConnectionType.SelectedItem;
            ret.supplyCondition = (SupplyCondition)this.comboSupplyCondition.SelectedItem;
            ret.dataStatus = (DataStatus)this.comboDataStatus.SelectedItem;
            ret.meterPosition = (MeterPositioning)this.comboMeterLocation.SelectedItem;
            ret.remark = this.textRemark.Text;
            
            if(!double.TryParse(textWMX.Text,out ret.waterMeterX )) ret.waterMeterX=0;
            if (!double.TryParse(textWMY.Text, out ret.waterMeterY)) ret.waterMeterY = 0;
            if (!double.TryParse(textHCX.Text, out ret.houseConnectionX)) ret.houseConnectionX = 0;
            if (!double.TryParse(textHCY.Text, out ret.houseConnectionY)) ret.houseConnectionY = 0;

            int.TryParse(textHouseHold.Text, out ret.householdSize);
            if (formData != null)
            {
                ret.id = formData.id;
                ret.subscriberID = formData.subscriberID;
                //ret.contractNo = formData.contractNo;
                ret.subscriptionStatus = formData.subscriptionStatus;
                ret.ticksFrom= formData.ticksFrom;
            }
            return ret;
        }




        
        public void LoadData()
        {
            this.formData = new Subscription();
            this.formData.subscriberID = -1;

            this.comboKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
            Kebele item = new Kebele();
            item.id = -1;
            item.name = "Unknown";
            this.comboKebele.Items.Add(item);

            this.comboKebele.SelectedIndex = 0;
            
            this.comboDMA.Items.AddRange(SubscriberManagmentClient.getAllDMAs());
            DistrictMeteringZone dma = new DistrictMeteringZone();
            dma.id = -1;
            dma.name = "Unknown";
            this.comboDMA.Items.Add(dma);
            this.comboDMA.SelectedIndex = 0;

            this.comboPressureZone.Items.AddRange(SubscriberManagmentClient.getAllPressureZones());
            PressureZone zone = new PressureZone();
            zone.id = -1;
            zone.name = "Unknown";
            this.comboPressureZone.Items.Add(zone);
            this.comboPressureZone.SelectedIndex = 0;

            this.comboSubscriptionType.Items.AddRange(Enum.GetNames(typeof(SubscriptionType)));
            this.comboSubscriptionType.SelectedItem = SubscriptionType.Tap.ToString();

            foreach (ConnectionType type in Enum.GetValues(typeof(ConnectionType)))
            {
                this.comboConnectionType.Items.Add(type);
            }

            foreach (SupplyCondition condition in Enum.GetValues(typeof(SupplyCondition)))
            {
                this.comboSupplyCondition.Items.Add(condition);
            }

            foreach (DataStatus status in Enum.GetValues(typeof(DataStatus)))
            {
                this.comboDataStatus.Items.Add(status);
            }

            foreach (MeterPositioning positioning in Enum.GetValues(typeof(MeterPositioning)))
            {
                this.comboMeterLocation.Items.Add(positioning);
            }

            this.m_IgnoreEvent = true;
            try
            {
                this.comboConnectionType.SelectedIndex = 0;
                this.comboSupplyCondition.SelectedIndex = 0;
                this.comboDataStatus.SelectedIndex = 0;
                this.comboMeterLocation.SelectedIndex = 0;
            }
            finally
            {
                this.m_IgnoreEvent = false;
            }
        }

        private void materialMeter_MaterialChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        private void OnSubscriptionUpdated()
        {
            _dataUpdated = true;
            if (this.SubscripiontUpdated != null)
            {
                this.SubscripiontUpdated(this, null);
            }
        }

        public void SetAutoContractNo()
        {
            this.textContractNo.Enabled = false;
        }
        
        public void SetSubscription(Subscription se)
        {
            this.m_IgnoreEvent = true;
            try
            {
                this.dtpDate.Value = new DateTime(se.ticksFrom);
                this.textStatus.Text = Subscriber.EnumToName(se.subscriptionStatus);
                this.textContractNo.Text = se.contractNo;
                int i = 0;
                foreach (Kebele kebele in this.comboKebele.Items)
                {
                    if (kebele.id == se.Kebele)
                    {
                        this.comboKebele.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                i = 0;
                foreach (DistrictMeteringZone dma in this.comboDMA.Items)
                {
                    if (dma.id == se.dma)
                    {
                        this.comboDMA.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                i = 0;
                foreach (PressureZone zone in this.comboPressureZone.Items)
                {
                    if (zone.id == se.pressureZone)
                    {
                        this.comboPressureZone.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                this.textLandCertifcate.Text = (se.landCertificateNo == null) ? "" : se.landCertificateNo;
                this.comboSubscriptionType.SelectedItem = se.subscriptionType.ToString();
                this.textAddress.Text = (se.address == null) ? "" : se.address;
                meterDataPlaceholder.setMeter(se.meterData);
                this.textParcelCode.Text = se.parcelNo;
                //this.textInitialReading.Text = se.initialMeterReading.ToString();
                this.formData = se;

                this.comboConnectionType.SelectedItem = se.connectionType;
                this.comboSupplyCondition.SelectedItem = se.supplyCondition;
                this.comboDataStatus.SelectedItem = se.dataStatus;
                this.comboMeterLocation.SelectedItem = se.meterPosition;
                this.textWMX.Text = Account.AmountEqual(se.waterMeterX, 0) ? "" : se.waterMeterX.ToString("0.00000000");
                this.textWMY.Text = Account.AmountEqual(se.waterMeterY, 0) ? "" : se.waterMeterY.ToString("0.00000000");
                this.textHCX.Text = Account.AmountEqual(se.houseConnectionX, 0) ? "" : se.houseConnectionX.ToString("0.00000000");
                this.textHCY.Text = Account.AmountEqual(se.houseConnectionY, 0) ? "" : se.houseConnectionY.ToString("0.00000000");
                this.textRemark.Text = se.remark;
                this.textHouseHold.Text = se.householdSize.ToString();
                _dataUpdated = false;
            }
            finally
            {
                this.m_IgnoreEvent = false;
            }
        }

        private void textAddress_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        private void textCode_TextChanged(object sender, EventArgs e)
        {
            if (!this.m_IgnoreEvent)
            {
                this.OnSubscriptionUpdated();
            }
        }

        public string ContractNo
        {
            get
            {
                return this.textContractNo.Text;
            }
        }

        

        

        

      

        private void textInitialReading_Validating(object sender, CancelEventArgs e)
        {
            
        }

        

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void groupLocation_Enter(object sender, EventArgs e)
        {

        }

        private void meterDataPlaceholder_MeterDataChanged(object sender, EventArgs e)
        {
            OnSubscriptionUpdated();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void groupLocation_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.DarkOrange, Color.DarkOrange);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color textColor, Color borderColor)
        {
            if (box != null)
            {
                Brush textBrush = new SolidBrush(textColor);
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Clear text and border
                g.Clear(this.BackColor);

                // Draw text
                g.DrawString(box.Text, box.Font, textBrush, box.Padding.Left, 0);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void groupMeter_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.DarkOrange, Color.DarkOrange);

        }

        private void groupStatistics_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.DarkOrange, Color.DarkOrange);

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}