﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public partial class SubscriptionEditorForm : Form
    {
        public Subscription connection = null;
        int _subscriberID = -1;
        public SubscriptionEditorForm(int subscriberID)
        {
            InitializeComponent();
            _subscriberID = subscriberID;
            editor.LoadData();
        }
        public SubscriptionEditorForm(Subscription con)
            : this(con.subscriberID)
        {
            connection = con;
            editor.SetSubscription(con);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!editor.validateControlData())
                return;
            Subscription newCon = editor.GetSubscription();
            newCon.id = connection != null ? connection.id : -1;
            newCon.subscriberID = _subscriberID;
            connection = newCon;
            this.DialogResult = DialogResult.OK;
            this.Close();
              
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
