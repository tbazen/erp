using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.BATCH_DELETE_BILLS)]
    public class DeleteBillsClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new DeleteBillsForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            DeleteBillsData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BATCH_DELETE_BILLS, 0, true) as DeleteBillsData;
            string html;
            if (data == null)
                html = "";
            else
            {
                int i = 1;
                html="";
                foreach (CustomerBillDocument doc in data.deletedBills)
                {
                    html += string.Format("<br/><span class='jobSection1'>Deleted Bill{0}</span><br/>", data.deletedBills.Length > 1 ? i + "/" + data.deletedBills.Length : "");
                    html += AccountingClient.GetDocumentHTML(doc.AccountDocumentID);
                    i++;
                }
            }
            return html;
        }


    }
}
