﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class DeleteBillsForm : NewApplicationForm
    {
        int _jobID;
        JobData _job;
        Subscriber _customer;
        public DeleteBillsForm(int jobID, Subscriber customer)
        {
            InitializeComponent();

            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _customer = customer;
                _job = null;
                loadBills(null);
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                applicationPlaceHolder.setApplictionInfo(job.startDate, job.description);
                _customer = SubscriberManagmentClient.GetSubscriber(job.customerID);
                DeleteBillsData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BATCH_DELETE_BILLS, 0, true) as DeleteBillsData;
                _job = job;
                loadBills(data);
            }
            setCustomer();
        }


        void setCustomer()
        {
            string html = JobRuleClientBase.createConciseCustString(_customer);
            browser.LoadTextPage("Connection", html);
        }
        private void loadBills(DeleteBillsData data)
        {
            listView.Items.Clear();
            CustomerBillDocument[] existingDeletedBills = data == null ? new CustomerBillDocument[0] : data.deletedBills;
            if (_job == null || !StandardJobStatus.isInactiveState(_job.status))
            {
                CustomerBillDocument[] bills = SubscriberManagmentClient.getBillDocuments(-1,_customer.id,-1,-1,true);
                foreach (CustomerBillDocument record in bills)
                {
                    if (record.draft)
                        continue;
                    PeriodicBill p=record as PeriodicBill;
                    ListViewItem item = new ListViewItem();
                    string name = AccountingClient.GetDocumentTypeByType(record.GetType()).name;
                    if(p!=null)
                        name+=" "+SubscriberManagmentClient.GetBillPeriod(p.period.id).name;
                        item = new ListViewItem(name);
                    
                    item.Tag = record;
                    item.SubItems.Add(INTAPS.Accounting.AccountBase.FormatDate(record.DocumentDate));
                    item.SubItems.Add(record.total.ToString("#,#0.00"));
                    listView.Items.Add(item);
                    foreach (CustomerBillDocument e in existingDeletedBills)
                    {
                        if (e.AccountDocumentID == record.AccountDocumentID)
                        {
                            item.Checked = true;
                            break;
                        }
                    }
                    

                }
            }
            else
            {
                foreach (CustomerBillDocument doc in existingDeletedBills)
                {
                    PeriodicBill p = doc as PeriodicBill;
                    ListViewItem item = new ListViewItem();
                    string name = AccountingClient.GetDocumentTypeByType(doc.GetType()).name;
                    if (p != null)
                        name += " " + SubscriberManagmentClient.GetBillPeriod(p.period.id).name;
                    
                    item = new ListViewItem(name);
                    item.Tag = doc;
                    item.SubItems.Add(INTAPS.Accounting.AccountBase.FormatDate(doc.DocumentDate));
                    item.SubItems.Add(doc.total.ToString("#,#0.00"));
                    listView.Items.Add(item);
                    item.Checked = true;
                }
            }
            listView_ItemChecked(null, null);
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.BATCH_DELETE_BILLS;
                }
                else
                    app = _job;
                app.customerID = _customer.id;
                
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                DeleteBillsData data = new DeleteBillsData();
                data.deletedBills = new CustomerBillDocument[listView.CheckedItems.Count];
                int i = 0;
                foreach (ListViewItem li in listView.CheckedItems)
                {
                    data.deletedBills[i++] = li.Tag as CustomerBillDocument;
                }


                if (data.deletedBills.Length == 0)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select at least one bill");
                    return;
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void listView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            bool allSelected = true;
            bool allNotSelected = true;
            foreach(ListViewItem li in listView.Items)
                if(li.Checked)
                    allNotSelected=false;
                else
                    allSelected=false;
            buttonSelect.Enabled = !allSelected;
            buttonClear.Enabled = !allNotSelected;
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = true;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = false;
        }

        
    }
}
