using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
using INTAPS.ClientServer;
namespace INTAPS.WSIS.Job.Client
{

    [JobRuleClientHandlerAttribute(StandardJobTypes.BATCH_DISCONNECT)]
    public class BatchDisconnectClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new DisconnectClientForm(jobID, customer, StandardJobTypes.BATCH_DISCONNECT);
        }

        public string buildDataHTML(JobData job)
        {
            BatchDisconnectData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.BATCH_DISCONNECT, 0, true) as BatchDisconnectData;
            string html;
            if (data == null)
                html = "";
            else
            {
                html="";
                if (data.discontinue)
                    html= string.Format("<h2>{0}</h2>", HttpUtility.HtmlEncode("Contract Termination!"));

                if (data.connections.Length > 10)
                    html += string.Format("<br/><span class='jobSection1'>{0} lines selected for disconnection</span><br/>", data.connections.Length);
                else
                {
                    int index = 1;
                    foreach (VersionedID con in data.connections)
                    {
                        StringBuilder sb=new StringBuilder();
                        CustomerProfileBuilder.createConnectionTable(data.connections.Length == 1 ? "" : index + ".", SubscriberManagmentClient.GetSubscription(con.id, con.version), CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                        html += sb.ToString();
                        index++;
                    }
                }

                
            }
            return html;
        }


    }
}
