﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.ClientServer;

namespace INTAPS.WSIS.Job.Client
{
    public partial class DisconnectClientForm : NewApplicationForm
    {
        int _jobID;
        JobData _job;
        int _appType;
        public DisconnectClientForm(int jobID, Subscriber customer,int applicationType)
        {
            InitializeComponent();
            this.Text = applicationType == StandardJobTypes.BATCH_DISCONNECT ? "Disconnect Lines" : "Reconnect Lines";
            _jobID = jobID;
            _appType = applicationType;
            if (_jobID == -1)
            {
                _job = null;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                BatchDisconnectData data = JobManagerClient.getWorkFlowData(job.id, _appType, 0, true) as BatchDisconnectData;
                _job = job;
                foreach (VersionedID co in data.connections)
                {
                    Subscription subsc = SubscriberManagmentClient.GetSubscription(co.id, co.version);
                    addSubscription(subsc);
                }
                checkDiscontinue.Checked = data.discontinue;
            }        
        }
        void addConnection()
        {
            try
            {
                string r = textContractNo.Text.Trim();
                Subscription subsc = SubscriberManagmentClient.GetSubscription(r, DateTime.Now.Ticks);
                if (subsc == null)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid contract no:" + subsc.contractNo);
                    return;
                }
                if (_appType == StandardJobTypes.BATCH_DISCONNECT)
                {
                    if (subsc.subscriptionStatus != SubscriptionStatus.Active)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Connection for contract no:" + subsc.contractNo + " is not active");
                        return;
                    }
                }
                else
                {
                    if (subsc.subscriptionStatus == SubscriptionStatus.Active)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Connection for contract no:" + subsc.contractNo + " is already active");
                        return;
                    }
                }
                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    if ((row.Tag as VersionedID).id == subsc.id)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Already added");
                        return;
                    }
                }
                addSubscription(subsc);
                textContractNo.Text = "";
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
           

        }
        void addSubscription(Subscription subsc)
        {
            CustomerBillDocument[] docs = SubscriberManagmentClient.getBillDocuments(-1, -1, subsc.id, -1, true);
            double total = 0;
            foreach (CustomerBillDocument d in docs)
                total += d.total;
            int index=dataGrid.Rows.Add(subsc.contractNo, subsc.subscriber.name, subsc.subscriber.customerCode, docs.Length.ToString(), AccountBase.FormatAmount(total));
            dataGrid.Rows[index].Tag = subsc.versionedID;

        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if(checkDiscontinue.Checked)
            {
                if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to discontinue these connections, it is not possible to reverse this process?"))                    return;
            }
            if (dataGrid.Rows.Count == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("No connection is selected");
                return;
            }
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = _appType;
                }
                else
                    app = _job;

                BatchDisconnectData data = _appType == StandardJobTypes.BATCH_DISCONNECT ? new BatchDisconnectData() : new BatchReconnectData();
                data.connections = new VersionedID[dataGrid.Rows.Count];
                data.discontinue = checkDiscontinue.Checked;
                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    data.connections[row.Index] = row.Tag as VersionedID;
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            addConnection();
        }

        private void textReceiptNumber_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }

}
