using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.EXEMPT_BILL_ITEMS)]
    public class ExcemptBillItemsClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ExcemptBillItemsForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            ExemptBillItemsData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.EXEMPT_BILL_ITEMS, 0, true) as ExemptBillItemsData;
            string html;
            if (data == null)
                html = "";
            else
            {
                int i = 1;
                CustomerBillDocument doc = AccountingClient.GetAccountDocument(data.exemptedBill.id, true) as CustomerBillDocument;
                doc.exemptedAmounts =ArrayExtension.mergeArray(doc.exemptedAmounts, data.exemptedAmounts);
                doc.exemptedItems = ArrayExtension.mergeArray(doc.exemptedItems, data.exemptedItems);
                html = "<br/><span class='jobSection1'>Exemption</span><br/>";
                html += AccountingClient.GetDocumentHTML(doc.AccountDocumentID);
                i++;
            }
            return html;
        }


    }
}
