﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ExcemptBillItemsForm : NewApplicationForm
    {
        int _jobID;
        JobData _job;
        Subscriber _customer;
        class BillIndex
        {
            public CustomerBillDocument doc;
            public override string ToString()
            {
                if (doc is PeriodicBill)
                {
                    PeriodicBill pb = (PeriodicBill)doc;
                    return "{0} Period: {1} Amount: {2}".format(AccountingClient.GetDocumentTypeByID(doc.DocumentTypeID).name, pb.period.name, AccountBase.FormatAmount(doc.total));
                }
                return "{0} Amount: {1}".format(AccountingClient.GetDocumentTypeByID(doc.DocumentTypeID).name, AccountBase.FormatAmount(doc.total));
            }
        }
        public ExcemptBillItemsForm(int jobID, Subscriber customer)
        {
            InitializeComponent();

            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _customer = customer;
                _job = null;
                loadBills();
                if (comboBills.Items.Count > 0)
                    comboBills.SelectedIndex = 0;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                _customer = SubscriberManagmentClient.GetSubscriber(job.customerID);
                ExemptBillItemsData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.EXEMPT_BILL_ITEMS, 0, true) as ExemptBillItemsData;
                _job = job;
                loadBills();
                int i=0;
                foreach (BillIndex ind in comboBills.Items)
                {
                    if (data.exemptedBill.id == ind.doc.AccountDocumentID)
                    {
                        comboBills.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                loadItems(data);
            }
            setCustomer();
        }
        void loadBills()
        {
            comboBills.Items.Clear();
            foreach (CustomerBillDocument doc in SubscriberManagmentClient.getBillDocuments(-1, _customer.id, -1, -1, true))
                comboBills.Items.Add(new BillIndex() { doc = doc });
        }

        void setCustomer()
        {
            string html = JobRuleClientBase.createConciseCustString(_customer);
            browser.LoadTextPage("Connection", html);
        }
        private void loadItems(ExemptBillItemsData data)
        {
            listView.Items.Clear();
            List<BillItem> existingExemption = new List<BillItem>();
            CustomerBillDocument doc = null;
            if (data != null)
            {
                doc = AccountingClient.GetAccountDocument(data.exemptedBill.id, true) as CustomerBillDocument;
                for (int i = 0; i < data.exemptedItems.Length; i++)
                {
                    foreach (BillItem item in doc.items)
                    {
                        if (item.itemTypeID == data.exemptedItems[i])
                        {
                            existingExemption.Add(item);
                            break;
                        }
                    }
                }
            }
            foreach (ListViewItem item in listView.Items)
            {
                BillItem record = (BillItem)item.Tag;
                foreach (BillItem e in existingExemption)
                {
                    if (e.itemTypeID == record.itemTypeID)
                    {
                        item.Checked = true;
                        break;
                    }
                }
            }
            listView_ItemChecked(null, null);
        }

       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.EXEMPT_BILL_ITEMS;
                }
                else
                    app = _job;
                app.customerID = _customer.id;
                
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                ExemptBillItemsData data = new ExemptBillItemsData();
                data.exemptedBill = SubscriberManagmentClient.getCustomerBillRecord(((BillIndex)comboBills.SelectedItem).doc.AccountDocumentID);
                data.exemptedItems= new int[listView.CheckedItems.Count];
                data.exemptedAmounts= new double[listView.CheckedItems.Count];
                int i = 0;
                foreach (ListViewItem li in listView.CheckedItems)
                {
                    BillItem item=li.Tag as BillItem;
                    data.exemptedAmounts[i] = item.price;
                    data.exemptedItems[i++] =item.itemTypeID ;
                }


                if (data.exemptedItems.Length == 0)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select at least one item");
                    return;
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void listView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            bool allSelected = true;
            bool allNotSelected = true;
            foreach(ListViewItem li in listView.Items)
                if(li.Checked)
                    allNotSelected=false;
                else
                    allSelected=false;
            buttonSelect.Enabled = !allSelected;
            buttonClear.Enabled = !allNotSelected;
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = true;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem li in listView.Items)
                li.Checked = false;
        }

        private void comboBills_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<BillItem> items = ((BillIndex)comboBills.SelectedItem).doc.itemsLessExemption;
            listView.Items.Clear();
            foreach (BillItem record in items)
            {
                ListViewItem item = new ListViewItem(record.description);
                item.Tag = record;
                item.SubItems.Add(INTAPS.Accounting.AccountBase.FormatAmount(record.price));
                listView.Items.Add(item);
            }
        }
    }
}
