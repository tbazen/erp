﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI;
using INTAPS.WSIS.Job.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class GeneralSellApplicationForm : NewApplicationForm
    {
        JobData _application;
        int _applicationType;
        Subscriber _customer=null;
        
        public GeneralSellApplicationForm()
        {
            InitializeComponent();
            _application = null;
        }

        public void initWitJobID(int jobID)
        {
            _application = JobManagerClient.GetJob(jobID);
            _applicationType = _application.applicationType;
            if (_application.customerID < 1)
            {
                _customer = _application.newCustomer;
                applicationPlaceHolder.setCustomer(_customer, true);
            }
            else
            {
                _customer = SubscriberManagmentClient.GetSubscriber(_application.customerID);
                applicationPlaceHolder.setCustomer(_customer, false);
            }
            GeneralSellsData data = JobManagerClient.getWorkFlowData(jobID, StandardJobTypes.GENERAL_SELL, 0,true) as GeneralSellsData;
            applicationPlaceHolder.setApplictionInfo(_application.startDate, _application.description);
            applicationPlaceHolder.Enabled = _application.status == StandardJobStatus.APPLICATION || _application.status == StandardJobStatus.FINALIZATION;
        }

        public GeneralSellApplicationForm(int jobID, Subscriber customer)
            : this()
        {
            if (jobID == -1)
            {
                _application = null;
                _customer = customer;
                if(customer==null)
                    applicationPlaceHolder.setCustomer(null, true);
                else
                    applicationPlaceHolder.setCustomer(customer, false);
            }
            else
            {
                initWitJobID(jobID);
            }

        }
        

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_application == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.GENERAL_SELL;
                }
                else
                    app = _application;
                if (applicationPlaceHolder.isNewCustomerMode())
                {
                    if (!applicationPlaceHolder.validateCustomerData())
                        return;
                    app.newCustomer = applicationPlaceHolder.getCustomer();
                }
                else
                    app.customerID = _customer.id;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                GeneralSellsData data=null;
                    data=new GeneralSellsData();
                
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    if (applicationPlaceHolder.Enabled)
                        JobManagerClient.UpdateJob(app, data);
                    else
                    {
                        data.jobID = _application.id;
                        data.typeID = StandardJobTypes.GENERAL_SELL;
                        JobManagerClient.setWorkFlowData(data);
                    }
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                
                _application = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void textCustomerCode_TextChanged(object sender, EventArgs e)
        {

        }

        
    }
}
