using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Windows.Forms;
namespace INTAPS.WSIS.Job.Client
{
    public class JobRuleClientBase
    {
        public static BIZNET.iERP.bERPHtmlTable createConnectionTable(Subscription connection)
        {
            var appTable = new BIZNET.iERP.bERPHtmlTable();
            appTable.createBodyGroup();
            appTable.id = "conInfo";
            string meterString;

            BIZNET.iERP.TransactionItems item = connection.meterData == null ? null : BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(connection.meterData.itemCode);
            meterString = item == null ? "" : item.Name + ", "
                + connection.meterData.detailDesc;
            JobProcessRules.buildTwoColumnDataTable(appTable.groups[0]
                , "Contract No:", string.IsNullOrEmpty(connection.contractNo) ? "" : connection.contractNo
                , "Meter Type:", meterString
                , "Meter No:", string.IsNullOrEmpty(connection.serialNo)?"":connection.serialNo
                , "Kebele:", connection.Kebele < 1 ? "" : SubscriberManagmentClient.GetKebele(connection.Kebele).ToString()
                , "House No:", connection.address
                );
            return appTable;
        }
        public static string createConciseCustConnString(Subscriber customer, Subscription connection)
        {
            var appTable = new BIZNET.iERP.bERPHtmlTable();
            appTable.createBodyGroup();
            appTable.id = "conInfo";
            string meterString;

            BIZNET.iERP.TransactionItems item = connection.meterData == null ? null : BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(connection.meterData.itemCode);
            meterString = item == null ? "" : item.Name + ", "
                + connection.meterData.detailDesc;
            JobProcessRules.buildTwoColumnDataTable(appTable.groups[0]
                , "Customer Name:", string.IsNullOrEmpty(customer.name) ? "" : customer.name
                , "Contract No:", string.IsNullOrEmpty(connection.contractNo) ? "" : connection.contractNo
                , "Connection Kebele:", connection.Kebele < 1 ? "" : SubscriberManagmentClient.GetKebele(connection.Kebele).ToString()
                );
            return BIZNET.iERP.bERPHtmlBuilder.ToString(appTable);
        }
        public static string createConciseCustString(Subscriber customer)
        {
            var appTable = new BIZNET.iERP.bERPHtmlTable();
            appTable.createBodyGroup();
            appTable.id = "custInfo";
            JobProcessRules.buildTwoColumnDataTable(appTable.groups[0]
                , "Customer Name:", string.IsNullOrEmpty(customer.name) ? "" : customer.name
                , "Customer Code:", string.IsNullOrEmpty(customer.customerCode) ? "" : customer.customerCode
                , "Kebele:", customer.Kebele < 1 ? "" : SubscriberManagmentClient.GetKebele(customer.Kebele).ToString()
                );
            return BIZNET.iERP.bERPHtmlBuilder.ToString(appTable);
        }
        public static string createMeterHTML(MeterData meter)
        {
            var appTable = new BIZNET.iERP.bERPHtmlTable();
            appTable.createBodyGroup();
            appTable.id = "conInfo";
            string meterString;

            BIZNET.iERP.TransactionItems item = meter == null ? null : BIZNET.iERP.Client.iERPTransactionClient.GetTransactionItems(meter.itemCode);
            meterString = item == null ? "" : item.Name;
            JobProcessRules.buildTwoColumnDataTable(appTable.groups[0]
                , "Meter Type:", meterString
                , "Meter No.:", meter.serialNo
                , "Meter Model.", meter.modelNo
                );
            return BIZNET.iERP.bERPHtmlBuilder.ToString(appTable);
        }

        public static void printReceipt(CustomerPaymentReceipt receipt)
        {

        }

        public virtual System.Windows.Forms.Form getConfigurationForm() { throw new NotImplementedException("Not implented"); }
        public virtual bool processURL(Form parent, string path, System.Collections.Hashtable query)
        {
            return false;
        }
        public virtual Type getApplicationFormType()
        {
            return null;
        }

    }
}
