﻿namespace INTAPS.WSIS.Job.Client
{
    partial class NewLineApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.pageContract = new System.Windows.Forms.TabPage();
            this.connectionEditor = new INTAPS.WSIS.Job.Client.SubscriptionEditor();
            this.pageGeneral = new System.Windows.Forms.TabPage();
            this.applicationPlaceHolder = new INTAPS.WSIS.Job.Client.ApplicationPlaceHolder();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.panel1.SuspendLayout();
            this.pageContract.SuspendLayout();
            this.pageGeneral.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 498);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(882, 47);
            this.panel1.TabIndex = 1;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Image = global::INTAPS.WSIS.Job.Client.Properties.Resources.ok3;
            this.buttonOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonOk.Location = new System.Drawing.Point(700, 7);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(79, 30);
            this.buttonOk.TabIndex = 0;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Image = global::INTAPS.WSIS.Job.Client.Properties.Resources.Close_32x32;
            this.buttonCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCancel.Location = new System.Drawing.Point(785, 6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(85, 30);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // pageContract
            // 
            this.pageContract.Controls.Add(this.connectionEditor);
            this.pageContract.Location = new System.Drawing.Point(4, 22);
            this.pageContract.Name = "pageContract";
            this.pageContract.Padding = new System.Windows.Forms.Padding(3);
            this.pageContract.Size = new System.Drawing.Size(874, 472);
            this.pageContract.TabIndex = 2;
            this.pageContract.Text = "New Connection Detail";
            this.pageContract.UseVisualStyleBackColor = true;
            // 
            // connectionEditor
            // 
            this.connectionEditor.BackColor = System.Drawing.Color.Khaki;
            this.connectionEditor.DataUpdated = false;
            this.connectionEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.connectionEditor.Location = new System.Drawing.Point(3, 3);
            this.connectionEditor.Name = "connectionEditor";
            this.connectionEditor.Size = new System.Drawing.Size(868, 466);
            this.connectionEditor.TabIndex = 0;
            // 
            // pageGeneral
            // 
            this.pageGeneral.Controls.Add(this.applicationPlaceHolder);
            this.pageGeneral.Location = new System.Drawing.Point(4, 22);
            this.pageGeneral.Name = "pageGeneral";
            this.pageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.pageGeneral.Size = new System.Drawing.Size(874, 472);
            this.pageGeneral.TabIndex = 3;
            this.pageGeneral.Text = "Application Detail";
            this.pageGeneral.UseVisualStyleBackColor = true;
            // 
            // applicationPlaceHolder
            // 
            this.applicationPlaceHolder.BackColor = System.Drawing.Color.Khaki;
            this.applicationPlaceHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.applicationPlaceHolder.Location = new System.Drawing.Point(3, 3);
            this.applicationPlaceHolder.Name = "applicationPlaceHolder";
            this.applicationPlaceHolder.Size = new System.Drawing.Size(868, 466);
            this.applicationPlaceHolder.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.pageGeneral);
            this.tabControl.Controls.Add(this.pageContract);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(882, 498);
            this.tabControl.TabIndex = 0;
            // 
            // NewLineApplicationForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(882, 545);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Name = "NewLineApplicationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Job Application Form";
            this.panel1.ResumeLayout(false);
            this.pageContract.ResumeLayout(false);
            this.pageGeneral.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabPage pageContract;
        private System.Windows.Forms.TabPage pageGeneral;
        private System.Windows.Forms.TabControl tabControl;
        private SubscriptionEditor connectionEditor;
        private INTAPS.WSIS.Job.Client.ApplicationPlaceHolder applicationPlaceHolder;
    }
}