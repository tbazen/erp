﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;

namespace INTAPS.WSIS.Job.Client
{
    
    public class StandardNewLineClientBase : JobRuleClientBase,IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new NewLineApplicationForm(jobID,customer);
        }

        public virtual string buildDataHTML(JobData job)
        {
            NewLineData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.NEW_LINE, 0, true) as NewLineData;
            if (data == null)
                return "";

            var newConTable = data.connectionData==null?null:createConnectionTable(data.connectionData);
            if(newConTable!=null)
                newConTable.css="innerTable";
            return string.Format("<span class='jobSection1'>{0}</span>{1}"
                , System.Web.HttpUtility.HtmlEncode("New Connection Detail")
                , newConTable==null?"":BIZNET.iERP.bERPHtmlBuilder.ToString(newConTable));
        }
    }


    


}
