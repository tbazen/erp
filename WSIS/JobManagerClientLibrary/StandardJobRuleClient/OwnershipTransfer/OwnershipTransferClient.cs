using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER)]
    public class OwnershipTransferClient : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new OwnershipTransferForm(jobID, connection);
        }

        public string buildDataHTML(JobData job)
        {
            OwnerShipTransferData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER, 0, true) as OwnerShipTransferData;
            if (data == null)
                return "";

            Subscription subsc = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
            var conTable = createConnectionTable(subsc);
            string ret= string.Format("<span class='jobSection1'>{0}</span>{1}<span class='jobSection1'>{2}</span>{3}"
                , System.Web.HttpUtility.HtmlEncode("Old Customer Detial")
                , data.oldCustomer==null?"<br/>No old customer detail.<br/>":JobManagerClient.buildCustomerHTML(data.oldCustomer)
                , System.Web.HttpUtility.HtmlEncode("Connection Detail")
                , BIZNET.iERP.bERPHtmlBuilder.ToString(conTable)
                );
            if (data.newConnectionID != -1)
            {
                StringBuilder sb=new StringBuilder(ret);
                sb.Append("<span class='jobSection1'>New Connection Detail</span>");
                Subscription connection = SubscriberManagmentClient.GetSubscription(data.newConnectionID, DateTime.Now.Ticks);
                if (connection != null)
                {
                    CustomerProfileBuilder.createConnectionTable( "", connection, CustomerProfileBuilder.ConnectionProfileOptions.All).build(sb);
                    ret = sb.ToString();
                }
                else
                    ret = "Empty";
            }
            return ret;
        }

    }
}
