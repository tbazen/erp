﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class OwnershipTransferForm : NewApplicationForm
    {
        int _jobID = -1;
        Subscription _connection;
        JobData _application;
        int _newCustomerID=-1;
        public OwnershipTransferForm(int jobID,Subscription connection)
        {
            InitializeComponent();
            _jobID = jobID;
            _connection = connection;
        }

        void setConnection()
        {
            browserExisting.LoadTextPage("Customer", "<h2>Customer Detail</h1>"+JobManagerClient.buildCustomerHTML(_connection.subscriber)
                + "<h2>Connection Detail</h1>" + BIZNET.iERP.bERPHtmlBuilder.ToString( JobRuleClientBase.createConnectionTable(_connection))
                );
        }

        protected override void OnLoad(EventArgs e)
        {
            if (_jobID == -1)
            {
                JobCustomerSelector s = new JobCustomerSelector("Select New Owner", "New Customer", "Select Customer",true);
                if (s.ShowDialog() != DialogResult.OK)
                {
                    this.Close();
                    return;
                }
                if (s.selectedCustomer == null)
                    applicationPlaceHolder.setCustomer(null, true);
                else
                {
                    _newCustomerID = s.selectedCustomer.id;
                    applicationPlaceHolder.setCustomer(s.selectedCustomer, false);
                }
                _application = null;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                if (job.customerID==-1)
                {
                    applicationPlaceHolder.setCustomer(job.newCustomer, true);
                }
                else
                    applicationPlaceHolder.setCustomer(SubscriberManagmentClient.GetSubscriber(job.customerID), false);
                OwnerShipTransferData data=JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER, 0, true) as OwnerShipTransferData;
                _connection = SubscriberManagmentClient.GetSubscription(data.connectionID,job.statusDate.Ticks);
                _application = job;
            }
            setConnection();

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_application == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER;
                }
                else
                    app = _application;
                if (applicationPlaceHolder.isNewCustomerMode())
                {
                    if (!applicationPlaceHolder.validateCustomerData())
                        return;
                    app.newCustomer = applicationPlaceHolder.getCustomer();
                }
                else
                    app.customerID = _newCustomerID;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                OwnerShipTransferData data = new OwnerShipTransferData();
                data.connectionID = _connection.id;
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _application = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }

        }
    }
}
