using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.READING_CORRECTION)]
    public class ReadingCorrectionClient : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ReadingCorrectionForm(jobID, connection);
        }

        public string buildDataHTML(JobData job)
        {
            ReadingCorrectionData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.READING_CORRECTION, 0, true) as ReadingCorrectionData;
            Subscriber newCustomer = SubscriberManagmentClient.GetSubscriber(job.customerID);
            if (data == null)
                return "";

            BWFMeterReading oldReading = data.oldReading == null ? SubscriberManagmentClient.BWFGetMeterReadingByPeriodID(data.newReading.subscriptionID, data.newReading.periodID) : data.oldReading;
            string html = string.Format("<span class='jobSection1'>{0}</span><br/>{1}"
             , System.Web.HttpUtility.HtmlEncode("Old Reading")
             , CustomerProfileBuilder.buildReadingHTML(oldReading)
             );
            html += string.Format("<br/><span class='jobSection1'>{0}</span><br/>{1}"
             , System.Web.HttpUtility.HtmlEncode("New Reding")
             , CustomerProfileBuilder.buildReadingHTML(data.newReading)
             );
            
            return html;
        }


    }
}
