﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ReadingCorrectionForm : NewApplicationForm
    {
        Subscription _connection;
        int _jobID;
        JobData _job;
        BWFMeterReading[] averageReadings = null;
        bool _ignoreEvents = false;
        public ReadingCorrectionForm(int jobID, Subscription connection)
        {
            InitializeComponent();
            _ignoreEvents = true;
            this.comboType.Items.Add(MeterReadingType.Normal);
            //this.comboType.Items.Add(MeterReadingType.Average);
            this.comboType.Items.Add(MeterReadingType.MeterReset);

            this.comboType.SelectedItem = MeterReadingType.Normal;
            this.billPeriodSelector.LoadData(SubscriberManagmentClient.CurrentYear);


            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _connection = connection;
                _job = null;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                ReadingCorrectionData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.READING_CORRECTION, 0, true) as ReadingCorrectionData;
                billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.GetBillPeriod(data.newReading.periodID);
                _connection = SubscriberManagmentClient.GetSubscription(data.newReading.subscriptionID, billPeriodSelector.SelectedPeriod.toDate.Ticks);
                _job = job;
                applicationPlaceHolder.setApplictionInfo(job.startDate, job.description);
                if (data!= null)
                {
                    this.billPeriodSelector.SelectedPeriod = SubscriberManagmentClient.GetBillPeriod(data.newReading.periodID);
                    this.billPeriodSelector.Enabled = false;
                    this.textReading.Text = data.newReading.readingType==MeterReadingType.Average?data.newReading.averageMonths.ToString():data.newReading.reading.ToString();
                    this.textConsumption.Text = data.newReading.consumption.ToString();
                    this.comboType.SelectedItem = data.newReading.readingType;
                    setAverageReadings(data.averageReadings);

                }
                
            }
            setConnection();
            _ignoreEvents = false;
        }

        void setConnection()
        {
            applicationPlaceHolder.setCustomer(_connection.subscriber, false);
            string html = JobRuleClientBase.createConciseCustConnString(_connection.subscriber, _connection);

            BWFMeterReading reading = null;
            if (billPeriodSelector.SelectedPeriod != null)
                reading = SubscriberManagmentClient.BWFGetMeterReadingByPeriodID(_connection.id, billPeriodSelector.SelectedPeriod.id);
            if (reading != null)
            {
                html += "<br/><b>Readings</b><br/>";
                html += CustomerProfileBuilder.buildReadingHTML(reading);
            }
            browser.LoadTextPage("Connection", html);

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.READING_CORRECTION;
                }
                else
                    app = _job;

                app.customerID = _connection.subscriberID;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                ReadingCorrectionData data = new ReadingCorrectionData();
                data.newReading = new BWFMeterReading();
                data.newReading.periodID = this.billPeriodSelector.SelectedPeriod.id;
                data.newReading.subscriptionID = _connection.id;
                double entry;
                if (!double.TryParse(this.textReading.Text, out entry))
                {
                    if (entry < 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter non negative reading.");
                        return;
                    }
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter correct value.");
                    return;
                }

                data.newReading.readingType = (MeterReadingType)this.comboType.SelectedItem;
                switch (data.newReading.readingType)
                {
                    case MeterReadingType.Normal:                        
                        data.newReading.reading = entry;
                        break;
                    case MeterReadingType.MeterReset:
                        data.newReading.reading = entry;
                        if (!double.TryParse(this.textConsumption.Text, out data.newReading.consumption) || data.newReading.consumption<0)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Please enter consumption.");
                            return;
                        }
                        break;
                    //case MeterReadingType.Average:
                    //    if((int)entry<1)
                    //    {
                    //        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Number of average months must be greater than 0.");
                    //        return;
                    //    }
                    //    data.newReading.averageMonths = (int)entry;
                    //    break;
                    default:
                        break;
                }

                if ((data.newReading.readingType != MeterReadingType.Normal) && !double.TryParse(this.textConsumption.Text, out data.newReading.consumption))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter consumption.");
                    return;
                }
                

                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }

                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void comboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((MeterReadingType)comboType.SelectedItem)
            {
                case MeterReadingType.Normal:
                    labelReading.Text = "Reading:";
                    textConsumption.Enabled = false;
                    break;
                case MeterReadingType.MeterReset:
                    labelReading.Text = "Reading:";
                    textConsumption.Enabled= true;
                    break;
                //case MeterReadingType.Average:
                //    labelReading.Text = "Number of Months:";
                //    textConsumption.Enabled = false;
                //    break;
            }
        }

        private void billPeriodSelector_Load(object sender, EventArgs e)
        {
                
        }

        private void billPeriodSelector_SelectionChanged(object sender, EventArgs e)
        {
            if (_ignoreEvents)
                return;
            setConnection();
        }
        void setAverageReadings(BWFMeterReading[] readings)
        {
            this.averageReadings = readings;
            if (readings == null || readings.Length == 0)
            {
                buttonMonths.Text = "Calculate Average";
                textReading.Enabled = true;
                textConsumption.Enabled = ((MeterReadingType)comboType.SelectedItem) == MeterReadingType.MeterReset;
                this.averageReadings = null;
            }
            else
            {
                buttonMonths.Text = $"Average of {readings.Length} readings";

                textConsumption.Enabled = false;
                double average = 0;
                foreach (var r in readings)
                    average += r.consumption;
                average = Math.Round(average / readings.Length, 0);
                this.textConsumption.Text = average.ToString();

                switch ((MeterReadingType)comboType.SelectedItem)
                {
                    case MeterReadingType.Normal:
                        var p = this.billPeriodSelector.SelectedPeriod;
                        if (p != null)
                        {

                            var r = SubscriberManagmentClient.BWFGetMeterPreviousReading(_connection.id, p.id);
                            this.textReading.Text = Math.Round((r == null ? 0 : r.reading) + average).ToString();
                            textReading.Enabled = false;
                        }
                        break;
                    case MeterReadingType.MeterReset:
                        textReading.Enabled = true;
                        if (textReading.Text.Trim().Equals(""))
                            textReading.Text = "0";
                        break;

                }

            }
        }
        private void buttonMonths_Click(object sender, EventArgs e)
        {
            var p = this.billPeriodSelector.SelectedPeriod;
            if (p == null)
                return;
            var f = new ReadingList(_connection.id, averageReadings,p);
            if(f.ShowDialog(this)==DialogResult.OK)
            {
                setAverageReadings(f.selected);                
            }
        }
    }
}
