﻿using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ReadingList : Form
    {
        public BWFMeterReading[] selected = null;
        public ReadingList(int connectionID,BWFMeterReading[] readings,BillPeriod period)
        {
            InitializeComponent();
            int N;
            var history = SubscriberManagmentClient.BWFGetMeterReadings(new MeterReadingFilter()
            {
                subscriptions=new int[] { connectionID}
            }, 0, -1, out N);
            foreach(var r in history)
            {
                var p = SubscriberManagmentClient.GetBillPeriod(r.periodID);
                if(p.fromDate>=period.fromDate)
                    continue;

                var item = new ListViewItem();
                item.Tag = r;
                item.Text = p.name;
                item.SubItems.Add(r.reading.ToString());
                item.SubItems.Add(r.consumption.ToString());
                switch (r.readingType)
                {
                    case MeterReadingType.Normal:
                        item.SubItems.Add("Normal");
                        break;
                    case MeterReadingType.MeterReset:
                        item.SubItems.Add("Meter Reset");
                        break;
                    case MeterReadingType.Average:
                        item.SubItems.Add("Average");
                        break;
                    default:
                        break;
                }
                listView1.Items.Add(item);
            }
            if (readings != null)
            {
                var h = new Dictionary<int,BWFMeterReading>();
                foreach (var r in readings)
                {
                    h.Add(r.periodID, r);
                }
                foreach (ListViewItem item in listView1.Items)
                {
                    item.Checked = h.ContainsKey(((BWFMeterReading)item.Tag).periodID);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (listView1.CheckedItems.Count == 0)
            {
                selected = null;
            }
            else
            {
                selected = new BWFMeterReading[listView1.CheckedItems.Count];
                int i = 0;
                foreach (ListViewItem item in listView1.CheckedItems)
                {
                    selected[i++] = (BWFMeterReading)item.Tag;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();   
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
