using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.RETURN_METER)]
    public class ReturnMeterClient : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ReturnMeterForm(jobID, connection);
        }

        public string buildDataHTML(JobData job)
        {
            ReturnMeterData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.RETURN_METER, 0, true) as ReturnMeterData;

            Subscription subsc = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
            Subscriber newCustomer = job.customerID == -1 ? job.newCustomer : SubscriberManagmentClient.GetSubscriber(job.customerID);
            var conTable = createConnectionTable(subsc);
            string returnDetail;
            if (data == null)
                returnDetail = "";
            else
            {
                returnDetail = data.permanent ? "<br/>Return meter permanently" : "<br/>Return meter temporarily";

            }
            return string.Format("<span class='jobSection1'>{0}</span>{1}{2}"
                , System.Web.HttpUtility.HtmlEncode("Connection Detail")
                , BIZNET.iERP.bERPHtmlBuilder.ToString(conTable)
                , returnDetail
                );
        }


    }
}
