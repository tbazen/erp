﻿namespace INTAPS.WSIS.Job.Client
{
    partial class ReturnMeterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.browser = new INTAPS.UI.HTML.ControlBrowser();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.checkPermanent = new System.Windows.Forms.CheckBox();
            this.applicationPlaceHolder = new INTAPS.WSIS.Job.Client.ApplicationPlaceHolder();
            this.cmbOperationalStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // browser
            // 
            this.browser.Dock = System.Windows.Forms.DockStyle.Top;
            this.browser.Location = new System.Drawing.Point(0, 0);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(672, 95);
            this.browser.StyleSheetFile = "jobstyle.css";
            this.browser.TabIndex = 0;
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(493, 9);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(85, 30);
            this.buttonOk.TabIndex = 58;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(584, 9);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(85, 30);
            this.buttonCancel.TabIndex = 57;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // checkPermanent
            // 
            this.checkPermanent.AutoSize = true;
            this.checkPermanent.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.checkPermanent.Location = new System.Drawing.Point(151, 225);
            this.checkPermanent.Name = "checkPermanent";
            this.checkPermanent.Size = new System.Drawing.Size(152, 21);
            this.checkPermanent.TabIndex = 59;
            this.checkPermanent.Text = "Return Permanently";
            this.checkPermanent.UseVisualStyleBackColor = true;
            this.checkPermanent.CheckedChanged += new System.EventHandler(this.checkPermanent_CheckedChanged);
            // 
            // applicationPlaceHolder
            // 
            this.applicationPlaceHolder.BackColor = System.Drawing.Color.Khaki;
            this.applicationPlaceHolder.Location = new System.Drawing.Point(0, 101);
            this.applicationPlaceHolder.Name = "applicationPlaceHolder";
            this.applicationPlaceHolder.Size = new System.Drawing.Size(672, 74);
            this.applicationPlaceHolder.TabIndex = 60;
            // 
            // cmbOperationalStatus
            // 
            this.cmbOperationalStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbOperationalStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOperationalStatus.Font = new System.Drawing.Font("Microsoft JhengHei UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOperationalStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(99)))), ((int)(((byte)(99)))));
            this.cmbOperationalStatus.FormattingEnabled = true;
            this.cmbOperationalStatus.Location = new System.Drawing.Point(151, 186);
            this.cmbOperationalStatus.Name = "cmbOperationalStatus";
            this.cmbOperationalStatus.Size = new System.Drawing.Size(226, 22);
            this.cmbOperationalStatus.TabIndex = 61;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(4, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 17);
            this.label2.TabIndex = 62;
            this.label2.Text = "Operational Status:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 263);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(672, 51);
            this.panel1.TabIndex = 63;
            // 
            // ReturnMeterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(672, 314);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cmbOperationalStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.applicationPlaceHolder);
            this.Controls.Add(this.checkPermanent);
            this.Controls.Add(this.browser);
            this.Name = "ReturnMeterForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Return Meter";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.HTML.ControlBrowser browser;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.CheckBox checkPermanent;
        private INTAPS.WSIS.Job.Client.ApplicationPlaceHolder applicationPlaceHolder;
        private System.Windows.Forms.ComboBox cmbOperationalStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
    }
}