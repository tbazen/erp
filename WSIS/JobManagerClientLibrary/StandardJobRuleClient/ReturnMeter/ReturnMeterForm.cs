﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class ReturnMeterForm : NewApplicationForm
    {


        Subscription _connection;
        int _jobID;
        JobData _job;
        public ReturnMeterForm(int jobID, Subscription connection)
        {
            InitializeComponent();
            this.cmbOperationalStatus.Items.AddRange(Enum.GetNames(typeof(MaterialOperationalStatus)));
            if (this.cmbOperationalStatus.Items.Count > 0)
            {
                this.cmbOperationalStatus.SelectedIndex = 0;
            }

            applicationPlaceHolder.hideCustomer();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _connection = connection;
                _job = null;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                ReturnMeterData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.RETURN_METER, 0, true) as ReturnMeterData;
                _connection = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
                _job = job;
                if (data!= null)
                {
                    this.checkPermanent.Checked = data.permanent;
                    this.cmbOperationalStatus.SelectedItem = data.opStatus.ToString();
                }
            }
            setConnection();
            
        }

    
        void setConnection()
        {
            applicationPlaceHolder.setCustomer(_connection.subscriber, false);
            browser.LoadTextPage("Connection", 
                JobRuleClientBase.createConciseCustConnString(_connection.subscriber, _connection)
                + JobRuleClientBase.createMeterHTML(_connection.meterData)
                );
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.RETURN_METER;
                }
                else
                    app = _job;

                app.customerID = _connection.subscriber.id;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                ReturnMeterData data = new ReturnMeterData();
                data.connectionID = _connection.id;
                data.permanent = checkPermanent.Checked;
                data.opStatus = (MaterialOperationalStatus)Enum.Parse(typeof(MaterialOperationalStatus), (string)cmbOperationalStatus.SelectedItem);
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }

                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void checkPermanent_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
