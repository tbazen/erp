﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using INTAPS.Payroll;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Client
{
    public partial class PickReceipt : Form
    {
        public PickReceipt(List<CustomerPaymentReceipt> rs)
        {
            InitializeComponent();
            foreach(CustomerPaymentReceipt r in rs)
            {
                ListViewItem li = new ListViewItem(r.PaperRef);
                li.SubItems.Add(r.DocumentDate.ToString("MMM dd,yyy"));
                String casheir="";
                PaymentCenter pc=SubscriberManagment.Client.SubscriberManagmentClient.GetPaymentCenterByCashAccount(r.assetAccountID);
                if(pc!=null)
                {
                    casheir="Center Name: "+pc.centerName;
                    Employee e= Payroll.Client.PayrollClient.GetEmployee(pc.casheir);
                    if(e!=null)
                    {
                        casheir=e.employeeName;
                    }
                }
                li.SubItems.Add(casheir);
                li.SubItems.Add(r.totalInstrument.ToString("#,#0.00"));
                li.Tag = r.AccountDocumentID;
                listView.Items.Add(li);
            }
            updateButtonStatus();
        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateButtonStatus();
        }
        void updateButtonStatus()
        {
            buttonOk.Enabled = listView.SelectedItems.Count == 1;
            buttonCancel.Enabled= listView.SelectedItems.Count != 1;
        }
        public int documentID=-1;
        private void buttonOk_Click(object sender, EventArgs e)
        {
            documentID = (int)listView.SelectedItems[0].Tag;
            this.DialogResult = DialogResult.OK;
        }
    }
}
