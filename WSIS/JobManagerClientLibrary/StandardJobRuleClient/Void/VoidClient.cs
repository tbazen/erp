using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
using System.Web;
using INTAPS.Accounting.Client;
namespace INTAPS.WSIS.Job.Client
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.VOID_RECEIPT)]
    public class VoidClient : JobRuleClientBase, IJobRuleClientHandler
    {
        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new VoidReceiptForm(jobID, customer);
        }

        public string buildDataHTML(JobData job)
        {
            VoidReceiptData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.VOID_RECEIPT, 0, true) as VoidReceiptData;
            string html;
            if (data == null)
                html = "";
            else
            {
                html = "<br/><span class='jobSection1'>Receipt Detail</span><br/>";
                if(!string.IsNullOrEmpty(data.receiptHTML))
                {
                    html += data.receiptHTML;
                }
                else if (data.receitDocumentID!=-1)
                {
                    html += AccountingClient.GetDocumentHTML(data.receitDocumentID);
                }
                
            }
            return html;
        }


    }
}
