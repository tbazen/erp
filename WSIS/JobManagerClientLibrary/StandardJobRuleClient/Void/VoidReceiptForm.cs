﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;

namespace INTAPS.WSIS.Job.Client
{
    public partial class VoidReceiptForm : NewApplicationForm
    {
        int _jobID;
        JobData _job;
        public VoidReceiptForm(int jobID, Subscriber customer)
        {
            InitializeComponent();
            _jobID = jobID;
            if (_jobID == -1)
            {
                _job = null;
                textReceiptNumber.Tag = -1;
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                textNote.Text = job.description;
                VoidReceiptData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.VOID_RECEIPT, 0, true) as VoidReceiptData;
                _job = job;
                textReceiptNumber.Tag = data.receitDocumentID;
                textReceiptNumber.Text = AccountingClient.GetAccountDocument(data.receitDocumentID, false).PaperRef;
            }        
        }
        void validateReceiptNumber()
        {
            bool reset = true;
            try
            {
                string r = textReceiptNumber.Text.Trim();
                DocumentSerialType st = AccountingClient.getDocumentSerialType("BSN");
                if (st == null)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please configure BSN reference type");
                    return;
                }
                int[] docs = AccountingClient.getDocumentListByTypedReference(new DocumentTypedReference(st.id, r, false));
                List<CustomerPaymentReceipt> receipts = new List<CustomerPaymentReceipt>();
                int receiptDocumentTypeID = AccountingClient.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id;
                foreach (int d in docs)
                {
                    AccountDocument doc;
                    if ((doc=AccountingClient.GetAccountDocument(d, true)).DocumentTypeID != receiptDocumentTypeID)
                        continue;
                    receipts.Add(doc as CustomerPaymentReceipt);
                }

                if (receipts.Count==0)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Receipt not found");
                    return;
                }
                int selectedID;
                if (receipts.Count > 1)
                {
                    PickReceipt pr = new PickReceipt(receipts);
                    if (pr.ShowDialog(this) == DialogResult.OK)
                        selectedID = pr.documentID;
                    else
                        return;
                }
                else
                    selectedID = receipts[0].AccountDocumentID;
                try
                {
                    browser.LoadTextPage("Receipt", AccountingClient.GetDocumentHTML(selectedID));
                    textReceiptNumber.Tag = selectedID;
                    reset = false;
                }
                catch (Exception ex)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                }
            }
            finally
            {
                if (reset)
                    textReceiptNumber.Text = "";
            }

        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.VOID_RECEIPT;
                }
                else
                    app = _job;
                VoidReceiptData data = new VoidReceiptData();
                data.receitDocumentID = (int)textReceiptNumber.Tag;
                app.description = textNote.Text;
                if (data.receitDocumentID == -1)
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select a receipt to void");
                    return;
                }

                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            validateReceiptNumber();
        }

        private void textReceiptNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                validateReceiptNumber();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }

}
