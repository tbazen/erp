using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.ClientServer.Server;
using INTAPS.SubscriberManagment;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
namespace INTAPS.WSIS.Job
{


    public class AccountDocumentConverter : JsonConverter
    {
        public Accounting.Service.AccountingService accounting { set; get; }


        public override bool CanConvert(Type objectType)
        {
            return typeof(AccountDocument).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader,
            Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;
            JObject item = JObject.Load(reader);
            var typeID = item["documentTypeID"].Value<int>();
            var type = accounting.GetDocumentTypeByID(typeID).GetTypeObject();
            return item.ToObject(type);
        }
        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }


    [Route("api/erp/[controller]/[action]")]
    [ApiController]
    public class JobManagerController : RESTServerControllerBase<JobManagerService>
    {
        #region AddAppointment
        public class AddAppointmentPar
        {
            public string sessionID; public JobAppointment appointment;
        }

        [HttpPost]
        public ActionResult AddAppointment(AddAppointmentPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).AddAppointment(par.appointment);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region AddJob
        public class AddJobPar
        {
            public string sessionID; public JobData job; public BinObject data;
        }

        [HttpPost]
        public ActionResult AddJob(AddJobPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).AddJob(par.job, par.data.Deserialized() as WorkFlowData));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region AddJobWeb

        public class AddJobWebPar
        {
            public string sessionID; public JobData job; public object data; public String dataType;
        }
        [HttpPost]
        public ActionResult AddJobWeb(AddJobWebPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                var service = (JobManagerService)base.SessionObject;
                var type = Type.GetType(par.dataType);
                var obj = par.data != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(par.data.ToString(), type
                    , new AccountDocumentConverter()
                    {
                        accounting = (Accounting.Service.AccountingService)ApplicationServer.GetSessionObject(par.sessionID, typeof(Accounting.Service.AccountingService))
                    }) : null;
                return AddJob(new AddJobPar { sessionID = par.sessionID, job = par.job, data = new BinObject(obj) });
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region ChangeJobStatus
        public class ChangeJobStatusPar
        {
            public string sessionID; public int jobID; public DateTime date; public int newStatus; public string node;
        }

        [HttpPost]
        public ActionResult ChangeJobStatus(ChangeJobStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).ChangeJobStatus(par.jobID, par.date, par.newStatus, par.node);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ChangeWorkerStatus
        public class ChangeWorkerStatusPar
        {
            public string sessionID; public string userID; public bool active;
        }

        [HttpPost]
        public ActionResult ChangeWorkerStatus(ChangeWorkerStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).ChangeWorkerStatus(par.userID, par.active);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CheckAndFixConfiguration
        public class CheckAndFixConfigurationPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult CheckAndFixConfiguration(CheckAndFixConfigurationPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).CheckAndFixConfiguration();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region ClearJobs
        public class ClearJobsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult ClearJobs(ClearJobsPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).ClearJobs();

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CloseAppointment 
        public class CloseAppointmentPar
        {
            public string sessionID; public int appointmentID;
        }

        [HttpPost]
        public ActionResult CloseAppointment(CloseAppointmentPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).CloseAppointment(par.appointmentID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region CreateJobWorker
        public class CreateJobWorkerPar
        {
            public string sessionID; public JobWorker worker;
        }

        [HttpPost]
        public ActionResult CreateJobWorker(CreateJobWorkerPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).CreateJobWorker(par.worker);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteBOM
        public class DeleteBOMPar
        {
            public string sessionID; public int bomID;
        }

        [HttpPost]
        public ActionResult DeleteBOM(DeleteBOMPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).DeleteBOM(par.bomID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteWorker
        public class DeleteWorkerPar
        {
            public string sessionID; public string userName;
        }

        [HttpPost]
        public ActionResult DeleteWorker(DeleteWorkerPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).DeleteWorker(par.userName);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion
        #region GetAllActiveJobs
        public class GetAllActiveJobsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllActiveJobs(GetAllActiveJobsPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetAllJobs());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllWorkers
        public class GetAllWorkersPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult GetAllWorkers(GetAllWorkersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetAllWorkers());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBillOfMaterial
        public class GetBillOfMaterialPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult GetBillOfMaterial(GetBillOfMaterialPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetBillOfMaterial(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetFilteredJobs
        public class GetFilteredJobsPar
        {
            public string sessionID; public DateTime date;
        }

        [HttpPost]
        public ActionResult GetFilteredJobs(GetFilteredJobsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetFilteredJobs(par.date));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetInvoicePreview
        public class GetInvoicePreviewPar
        {
            public string sessionID; public JobBillOfMaterial bom;
        }

        [HttpPost]
        public ActionResult GetInvoicePreview(GetInvoicePreviewPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetInvoicePreview(par.bom));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJob
        public class GetJobPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult GetJob(GetJobPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetJob(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJobAppointments
        public class GetJobAppointmentsPar
        {
            public string sessionID; public int jobHandle;
        }

        [HttpPost]
        public ActionResult GetJobAppointments(GetJobAppointmentsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetJobAppointments(par.jobHandle));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJobBOM
        public class GetJobBOMPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult GetJobBOM(GetJobBOMPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetJobBOM(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJobCard
        public class GetJobCardPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult GetJobCard(GetJobCardPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetJobCard(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJobHistory
        public class GetJobHistoryPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult GetJobHistory(GetJobHistoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetJobHistory(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLatestHistory
        public class GetLatestHistoryPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult GetLatestHistory(GetLatestHistoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetLatestHistory(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetWorker
        public class GetWorkerPar
        {
            public string sessionID; public string userID;
        }

        [HttpPost]
        public ActionResult GetWorker(GetWorkerPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetWorker(par.userID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RegisterSubscription
        public class RegisterSubscriptionPar
        {
            public string sessionID; public int jobID; public Subscription sub;
        }

        [HttpPost]
        public ActionResult RegisterSubscription(RegisterSubscriptionPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).RegisterSubscription(par.jobID, par.sub));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region RemoveJob
        public class RemoveJobPar
        {
            public string sessionID; public int jobHandle;
        }

        [HttpPost]
        public ActionResult RemoveJob(RemoveJobPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).RemoveJob(par.jobHandle);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchJob
        public class SearchJobPar
        {
            public string sessionID; public string query; public int index; public int pageSize;
        }
        public class SearchJobOut
        {
            public int NRecords;
            public JobData[] _ret;
        }

        [HttpPost]
        public ActionResult SearchJob(SearchJobPar par)
        {
            try
            {
                var _ret = new SearchJobOut();
                this.SetSessionObject(par.sessionID);
                _ret._ret = ((JobManagerService)base.SessionObject).SearchJob(par.query, par.index, par.pageSize, out _ret.NRecords);
                return Json(_ret);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SearchJob2
        public class SearchJob2Par
        {
            public string sessionID; public string query; public bool byDate; public DateTime fromDate; public DateTime toDate; public int status;
        }

        [HttpPost]
        public ActionResult SearchJob2(SearchJob2Par par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).SearchJob(par.query, par.byDate, par.fromDate, par.toDate, par.status));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetBillofMateial
        public class SetBillofMateialPar
        {
            public string sessionID; public JobBillOfMaterial bom;
        }

        [HttpPost]
        public ActionResult SetBillofMateial(SetBillofMateialPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).SetBillofMateial(par.bom));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateAppointment
        public class UpdateAppointmentPar
        {
            public string sessionID; public JobAppointment appointment;
        }

        [HttpPost]
        public ActionResult UpdateAppointment(UpdateAppointmentPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).UpdateAppointment(par.appointment);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateHistoryEntry
        public class UpdateHistoryEntryPar
        {
            public string sessionID; public JobStatusHistory history;
        }

        [HttpPost]
        public ActionResult UpdateHistoryEntry(UpdateHistoryEntryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).UpdateHistoryEntry(par.history);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region UpdateJob
        public class UpdateJobPar
        {
            public string sessionID; public JobData _job; public BinObject data;
        }

        [HttpPost]
        public ActionResult UpdateJob(UpdateJobPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).UpdateJob(par._job, par.data.Deserialized() as WorkFlowData);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region UpdateJobWeb
        public class UpdateJobWebPar
        {
            public string sessionID; public JobData job; public object data; public String dataType;
        }

        [HttpPost]
        public ActionResult UpdateJobWeb(UpdateJobWebPar par)
        {
            try
            {
                base.SetSessionObject(par.sessionID);
                var service = (JobManagerService)base.SessionObject;
                var type = Type.GetType(par.dataType);
                var obj = par.data != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(par.data.ToString(), type) : null;
                return UpdateJob(new UpdateJobPar { sessionID = par.sessionID, _job = par.job, data = new BinObject(obj) });

            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion



        #region UpdateJobWorker
        public class UpdateJobWorkerPar
        {
            public string sessionID; public JobWorker worker;
        }

        [HttpPost]
        public ActionResult UpdateJobWorker(UpdateJobWorkerPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).UpdateJobWorker(par.worker);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region VoidJobReceipt
        public class VoidJobReceiptPar
        {
            public string sessionID; public int bomID;
        }

        [HttpPost]
        public ActionResult VoidJobReceipt(VoidJobReceiptPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).VoidJobReceipt(par.bomID);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAppointment
        public class GetAppointmentPar
        {
            public string sessionID; public int id;
        }

        [HttpPost]
        public ActionResult GetAppointment(GetAppointmentPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetAppointment(par.id));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetBOMTotal
        public class GetBOMTotalPar
        {
            public string sessionID; public int bomID;
        }

        [HttpPost]
        public ActionResult getBOMTotal(GetBOMTotalPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getBOMTotal(par.bomID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPreviousHistory
        public class GetPreviousHistoryPar
        {
            public string sessionID; public int jobID;
        }

        [HttpPost]
        public ActionResult getPreviousHistory(GetPreviousHistoryPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getPreviousHistory(par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSystemParameter
        public class GetSystemParameterPar
        {
            public string sessionID; public string p;
        }

        [HttpPost]
        public ActionResult GetSystemParameter(GetSystemParameterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(new BinObject(((JobManagerService)base.SessionObject).GetSystemParameter(par.p)));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetSystemParametrWeb
        [HttpPost]
        public ActionResult GetSystemParameterWeb(GetSystemParameterPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                var response = ((JobManagerService)base.SessionObject).GetSystemParameter(par.p);
                return Json(response);
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

        #region GetAllJobItems
        public class GetAllJobItemsPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllJobItems(GetAllJobItemsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getAllJobItems());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJobItem
        public class GetJobItemPar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult getJobItem(GetJobItemPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getJobItem(par.itemCode));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region DeleteJobItem
        public class DeleteJobItemPar
        {
            public string sessionID; public string itemCode;
        }

        [HttpPost]
        public ActionResult deleteJobItem(DeleteJobItemPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).deleteJobItem(par.itemCode);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetJobItem
        public class SetJobItemPar
        {
            public string sessionID; public INTAPS.WSIS.Job.JobItemSetting setting;
        }

        [HttpPost]
        public ActionResult setJobItem(SetJobItemPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).setJobItem(par.setting);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetChangeNo
        public class GetChangeNoPar
        {
            public string sessionID;
        }

        public int getChangeNo(GetChangeNoPar par)
        {
            this.SetSessionObject(par.sessionID);
            return ((JobManagerService)base.SessionObject).getChangeNo();
        }
        #endregion
        #region GetCommandString
        public class GetCommandStringPar
        {
            public string sessionID; public int jobID; public int status;
        }

        [HttpPost]
        public ActionResult GetCommandString(GetCommandStringPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).GetCommandString(par.jobID, par.status));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPossibleNextStatus
        public class GetPossibleNextStatusPar
        {
            public string sessionID; public System.DateTime date; public int jobID;
        }

        [HttpPost]
        public ActionResult getPossibleNextStatus(GetPossibleNextStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getPossibleNextStatus(par.date, par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetWorkFlowData
        public class GetWorkFlowDataPar
        {
            public string sessionID; public int jobID; public int typeID; public int key; public bool fullData;
        }

        [HttpPost]
        public ActionResult getWorkFlowData(GetWorkFlowDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(new BinObject(((JobManagerService)base.SessionObject).getWorkFlowData(par.jobID, par.typeID, par.key, par.fullData)));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }



        public class GetWorkFlowDataWebPar
        {
            public string sessionID; public int jobID; public int typeID; public int key; public bool fullData;
        }

        [HttpPost]
        public ActionResult GetWorkFlowDataWeb(GetWorkFlowDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(new BinObject(((JobManagerService)base.SessionObject).getWorkFlowData(par.jobID, par.typeID, par.key, par.fullData)).Deserialized());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }





        #endregion
        #region GetJobType
        public class GetJobTypePar
        {
            public string sessionID; public int typeID;
        }

        [HttpPost]
        public ActionResult getJobType(GetJobTypePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getJobType(par.typeID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetJobStatus
        public class GetJobStatusPar
        {
            public string sessionID; public int status;
        }

        [HttpPost]
        public ActionResult getJobStatus(GetJobStatusPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getJobStatus(par.status));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetSystemParameters
        public class SetSystemParametersPar
        {
            public string sessionID; public string[] fields; public BinObject vals;
        }

        [HttpPost]
        public ActionResult SetSystemParameters(SetSystemParametersPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).SetSystemParameters(par.fields, (object[])par.vals.Deserialized());

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SaveConfiguration
        public class SaveConfigurationPar
        {
            public string sessionID; public int typeID; public object config; public String typeName;
        }

        [HttpPost]
        public ActionResult saveConfiguration(SaveConfigurationPar par)
        {
            try
            {
                if (par.config is JObject)
                {
                    var jobj = par.config as JObject;
                    var t = Type.GetType(par.typeName);
                    if (t == null)
                        throw new ClientServer.ServerUserMessage("Couldn't load configuration type:" + par.typeName);
                    par.config = jobj.ToObject(t);
                }
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).saveConfiguration(par.typeID, par.config);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetConfiguration
        public class GetConfigurationPar
        {
            public string sessionID; public int typeID; public String type;
        }

        [HttpPost]
        public ActionResult getConfiguration(GetConfigurationPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(new INTAPS.ClientServer.BinObject(((JobManagerService)base.SessionObject).getConfiguration(par.typeID, Type.GetType(par.type))));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }




        public class GetConfigurationWebPar
        {
            public string sessionID; public int typeID; public String type;
        }

        [HttpPost]
        public ActionResult GetConfigurationWeb(GetConfigurationWebPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                Type assemblyType = Type.GetType(par.type);
                GetConfigurationPar configurationPar = new GetConfigurationPar
                {
                    sessionID = par.sessionID,
                    type = assemblyType.AssemblyQualifiedName,
                    typeID = par.typeID
                };
                var binaryResult = new INTAPS.ClientServer.BinObject(((JobManagerService)base.SessionObject).getConfiguration(par.typeID, Type.GetType(par.type)));
                return Json(binaryResult.Deserialized());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }






        #endregion
        #region SetBillofMaterialCustomerReference
        public class SetBillofMaterialCustomerReferencePar
        {
            public string sessionID; public INTAPS.WSIS.Job.JobBillOfMaterial bom;
        }

        [HttpPost]
        public ActionResult setBillofMaterialCustomerReference(SetBillofMaterialCustomerReferencePar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).setBillofMaterialCustomerReference(par.bom);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetWorkFlowData
        public class SetWorkFlowDataPar
        {
            public string sessionID; public BinObject data;
        }

        [HttpPost]
        public ActionResult setWorkFlowData(SetWorkFlowDataPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                ((JobManagerService)base.SessionObject).setWorkFlowData(par.data.Deserialized() as WorkFlowData);

                return Ok();
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region SetWorkflowDataWeb
        public class SetWorkFlowDataWebPar
        {
            public string sessionID; public object data; public string dataType;
        }

        [HttpPost]
        public ActionResult SetWorkFlowDataWeb(SetWorkFlowDataWebPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);

                var service = (JobManagerService)base.SessionObject;
                var type = Type.GetType(par.dataType);
                var obj = par.data != null ? Newtonsoft.Json.JsonConvert.DeserializeObject(par.data.ToString(), type) : null;
                return setWorkFlowData(new SetWorkFlowDataPar { sessionID = par.sessionID, data = new BinObject(obj) });
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }

        #endregion


        #region JobAppliesTo
        public class JobAppliesToPar
        {
            public string sessionID; public System.DateTime date; public int jobID;
        }

        [HttpPost]
        public ActionResult jobAppliesTo(JobAppliesToPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).jobAppliesTo(par.date, par.jobID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetAllJobTypes
        public class GetAllJobTypesPar
        {
            public string sessionID;
        }

        [HttpPost]
        public ActionResult getAllJobTypes(GetAllJobTypesPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getAllJobTypes());
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetLastCashHandoverDocument
        public class GetLastCashHandoverDocumentPar
        {
            public string sessionID; public int cashAccountID;
        }

        [HttpPost]
        public ActionResult getLastCashHandoverDocument(GetLastCashHandoverDocumentPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getLastCashHandoverDocument(par.cashAccountID));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetPaymentCenterPaymentInstruments
        public class GetPaymentCenterPaymentInstrumentsPar
        {
            public string sessionID; public int cashAccountID; public System.DateTime from; public System.DateTime to;
        }

        [HttpPost]
        public ActionResult getPaymentCenterPaymentInstruments(GetPaymentCenterPaymentInstrumentsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getPaymentCenterPaymentInstruments(par.cashAccountID, par.from, par.to));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion
        #region GetCustomerJobs
        public class GetCustomerJobsPar
        {
            public string sessionID; public int customerID; public bool onlyAcitve;
        }

        [HttpPost]
        public ActionResult getCustomerJobs(GetCustomerJobsPar par)
        {
            try
            {
                this.SetSessionObject(par.sessionID);
                return Json(((JobManagerService)base.SessionObject).getCustomerJobs(par.customerID, par.onlyAcitve));
            }
            catch (Exception ex)
            {
                return base.Exception(ex);
            }
        }
        #endregion

    }
}

