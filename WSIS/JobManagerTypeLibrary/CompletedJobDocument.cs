﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using INTAPS.Accounting;
using INTAPS.ClientServer;

namespace INTAPS.WSIS.Job
{
    [Serializable]
    [INTAPS.RDBMS.TypeID(1002)]
    public class CompletedJobDocument:Accounting.AccountDocument
    {
        [DocumentTypedReferenceField("JOB")]
        public DocumentTypedReference jobNumber;
        public JobData job;
        public JobBillOfMaterial[] boms;

        public Subscriber customer;
    }
}
