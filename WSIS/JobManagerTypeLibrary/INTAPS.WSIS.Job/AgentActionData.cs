
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable, SingleTableObject]
    public class AgentActionData
    {
        [IDField]
        public int id;
        [DataField]
        public DateTime actionDate = DateTime.Now;
        [DataField]
        public string agent;
        [DataField]
        public int jobID;
        [DataField]
        public string note;
    }
}

