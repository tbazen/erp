
    using INTAPS.SubscriberManagment;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable]
    public class ApplicantData
    {
        public SubscriberType applicantType;
        public string name;
        public int kebele;
        public string houseNo;
        public string phoneNumber;
        public string nameOfPlace;
        public string email;
    }
}

