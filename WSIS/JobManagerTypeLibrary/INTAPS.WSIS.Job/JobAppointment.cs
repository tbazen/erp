
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable, SingleTableObject, STOInclude(typeof(AgentActionData))]
    public class JobAppointment : AgentActionData
    {
        [DataField]
        public bool active;
        [DataField]
        public DateTime appointmentCloseDate = DateTime.Now;
        [DataField]
        public DateTime appointmentDate = DateTime.Now;
    }
}

