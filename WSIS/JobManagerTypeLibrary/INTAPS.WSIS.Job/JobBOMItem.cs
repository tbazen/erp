
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable]
    public class JobBOMItem
    {
        public string itemID;
        public bool inSourced;
        public double quantity;
        public double referenceUnitPrice=-1;
        public double unitPrice;
        public string description = null;
        public bool calculated= false;
        public string tag = null;
        
    }
}

