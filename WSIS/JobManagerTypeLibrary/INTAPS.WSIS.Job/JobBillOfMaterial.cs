
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable, SingleTableObject]
    public class JobBillOfMaterial
    {
        [IDField]
        public int id = -1;
        [DataField]
        public int jobID;
        [XMLField]
        public JobBOMItem[] items;
        [DataField]
        public DateTime surveyCollectionTime = DateTime.Now;
        [DataField]
        public DateTime analysisTime = DateTime.Now;
        [DataField]
        public string note;
        [DataField]
        public int invoiceNo = -1;
        [DataField]
        public DateTime invoiceDate = DateTime.Now;
        [DataField]
        public string preparedBy;
        [DataField]
        public int storeIssueID = -1;

        public JobBOMItem findItem(string code)
        {
            foreach (JobBOMItem item in items)
            {
                if (string.Compare(code, item.itemID, true) == 0)
                    return item;
            }
            return null;
        }
    }
}

