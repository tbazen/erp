
    using INTAPS.RDBMS;
    using INTAPS.SubscriberManagment;
    using System;
namespace INTAPS.WSIS.Job
{
    [SingleTableObject, Serializable]
    public class JobStatusType
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
        [DataField]
        public string commandName;
    }
    
    [SingleTableObject, Serializable]
    public class JobTypeInfo
    {
        [IDField]
        public int id;
        [DataField]
        public string name;
    }

    [Serializable, SingleTableObject]
    public class JobData
    {
        [IDField]
        public int id;
        [DataField]
        public string jobNo;
        [XMLField]
        public Subscriber newCustomer;
        [DataField]
        public string description;
        [XMLField]
        public ServiceType[] serviceTypes=new ServiceType[0];
        [DataField]
        public DateTime logDate;
        [DataField]
        public DateTime startDate;
        [DataField]
        public DateTime statusDate;
        [DataField]
        public int status;
        [DataField]
        public int applicationType;

        [DataField]
        public int customerID=-1 ;

        [DataField]
        public int completionDocumentID = -1;
        public JobData()
        {
            this.id = -1;
            this.newCustomer = null;
            this.serviceTypes = new ServiceType[0];
            this.logDate = DateTime.Now;
            this.startDate = DateTime.Now;
            this.statusDate = DateTime.Now;
        }

        public JobData(string Description)
        {
            this.id = -1;
            this.newCustomer = null;
            this.serviceTypes = new ServiceType[0];
            this.logDate = DateTime.Now;
            this.startDate = DateTime.Now;
            this.statusDate = DateTime.Now;
            this.description = Description;
        }

        public override string ToString()
        {
            return this.description;
        }
    }
}

