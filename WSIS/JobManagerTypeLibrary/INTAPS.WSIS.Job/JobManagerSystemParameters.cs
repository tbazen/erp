using System;
namespace INTAPS.WSIS.Job
{

    [Serializable]
    public class JobManagerSystemParameters
    {
        public string contractNoFormat="000000";
        public int contractNoSerialBatch = 2061;
        public string invoiceNoFormat="000000";
        public int invoiceNoSerialBatch;
        public string jobNoFormat="00000";
        public int jobNoSerialBatch = 3;
        public int materialStoreID=-1;

        public int jobInvoiceReportID = 109;
        public string servicePaymentItem = "060100";//ITEM CODE REF

        public string ownershipTransferPaymentItem;//ITEM CODE REF
        public string permanentMeterReturnPaymentItem;//ITEM CODE REF
        public string temporaryMeterReturnPaymentItem;//ITEM CODE REF

        public string depositPaymentItem;//ITEM CODE REF
        public string meterDiagnosisItem;//ITEM CODE REF
    }
}

