
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable, SingleTableObject]
    public class JobParticipant
    {
        [DataField]
        public bool active;
        [DataField]
        public DateTime date;
        [DataField]
        public DateTime deactivateDate;
        [IDField]
        public int employeeID;
        [IDField]
        public int jobID;
    }
}

