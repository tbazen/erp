using System;
namespace INTAPS.WSIS.Job
{

    public static class StandardJobStatus
    {
        public const int NONE = 0;
        public const int APPLICATION = 1;
        public const int APPLICATION_APPROVAL = 2;
        public const int WORK_APPROVAL = 3;
        public const int SURVEY = 4;
        public const int ANALYSIS = 5;
        public const int APPLICATION_PAYMENT = 6;
        public const int WORK_PAYMENT = 7;
        public const int CANCELED = 8;
        public const int FINISHED = 9;
        public const int INACTIVE = 10;
        public const int TECHNICAL_WORK = 11;
        public const int WORK_COMPLETED = 12;
        public const int CONTRACT = 13;
        public const int FINALIZATION = 14;
        public const int STORE_ISSUE = 15;
        public const int STORE_ISSUED = 17;
        public static JobStatusType Application = new JobStatusType { id = StandardJobStatus.APPLICATION, name = "Application", commandName = "Apply" };
        public static JobStatusType ApplicationApproval = new JobStatusType { id = StandardJobStatus.APPLICATION_APPROVAL, name = "Application Approval", commandName = "Send to Application Approval" };
        public static JobStatusType ApplicationPayment = new JobStatusType { id = StandardJobStatus.APPLICATION_PAYMENT, name = "Application Payment", commandName = "Send to Application Payment" };
        public static JobStatusType Survey = new JobStatusType { id = StandardJobStatus.SURVEY, name = "Survey", commandName = "Send to Survey" };
        public static JobStatusType Analysis = new JobStatusType { id = StandardJobStatus.ANALYSIS, name = "Analysis", commandName = "Send to Analysis" };
        public static JobStatusType WorkApproval = new JobStatusType { id = StandardJobStatus.WORK_APPROVAL, name = "Work Approval", commandName = "Send to Work Approval" };
        public static JobStatusType WorkPayment = new JobStatusType { id = StandardJobStatus.WORK_PAYMENT, name = "Work Payment", commandName = "Send to Work Payment" };
        public static JobStatusType Contract = new JobStatusType { id = StandardJobStatus.CONTRACT, name = "Contract", commandName = "Send to Contract" };
        public static JobStatusType TechnicalWork = new JobStatusType { id = StandardJobStatus.TECHNICAL_WORK, name = "Technical Work", commandName = "Send to Technical Work" };
        public static JobStatusType Finalization = new JobStatusType { id = StandardJobStatus.FINALIZATION, name = "Finalization", commandName = "Send to Finalization" };
        public static JobStatusType Canceled = new JobStatusType { id = StandardJobStatus.CANCELED, name = "Canceled", commandName = "Cancel" };
        public static JobStatusType Finished = new JobStatusType { id = StandardJobStatus.FINISHED, name = "Finished", commandName = "Finish" };
        public static JobStatusType StoreIssue = new JobStatusType { id = StandardJobStatus.STORE_ISSUE, name = "Store Issue", commandName = "Send to Store" };
        public static JobStatusType StoreIssued = new JobStatusType { id = StandardJobStatus.STORE_ISSUED, name = "Stores Issued", commandName = "Store Issue" };


        public static bool IsPaymentStatus(int status)
        {
            switch (status)
            {
                case StandardJobStatus.APPLICATION_PAYMENT:
                case StandardJobStatus.WORK_PAYMENT:
                    return true;
            }
            return false;
        }
        public static int[] getInactiveStates()
        {
            return new int[]{
                StandardJobStatus.NONE,
                StandardJobStatus.CANCELED,
                StandardJobStatus.FINISHED,
                StandardJobStatus.INACTIVE,
                StandardJobStatus.WORK_COMPLETED
            };
        }
        public static bool isInactiveState(int jobStatus)
        {
            switch (jobStatus)
            {
                case StandardJobStatus.NONE:
                case StandardJobStatus.CANCELED:
                case StandardJobStatus.FINISHED:
                case StandardJobStatus.INACTIVE:
                case StandardJobStatus.WORK_COMPLETED:
                    return true;
                default:
                    return false;
            }
        }

        public static int[] getActiveJobStatuses()
        {
            return new int[]{
                StandardJobStatus.NONE,
                StandardJobStatus.CANCELED,
                StandardJobStatus.FINISHED,
                StandardJobStatus.INACTIVE,
                StandardJobStatus.WORK_COMPLETED,
            };
        }


        public static bool isBOQEditable(int status)
        {
            return status == StandardJobStatus.ANALYSIS
                || status == StandardJobStatus.SURVEY;
        }
        public static bool isBOQEditable(int jobType, int status)
        {
            if (jobType == StandardJobTypes.GENERAL_SELL)
                return status == StandardJobStatus.APPLICATION;
            return status == StandardJobStatus.ANALYSIS
                || status == StandardJobStatus.SURVEY;
        }
    }
}