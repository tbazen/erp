
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable, SingleTableObject(orderBy="jobID,id"), STOInclude(typeof(AgentActionData))]
    public class JobStatusHistory : AgentActionData
    {
        [DataField]
        public DateTime changeDate;
        [DataField]
        public int newStatus;
        [DataField]
        public int oldStatus;
    }
}

