
    using INTAPS.RDBMS;
    using System;
namespace INTAPS.WSIS.Job
{

    [Serializable, SingleTableObject(orderBy = "active desc")]
    public class JobWorker
    {
        [IDField]
        public string userID;
        [DataField]
        public bool active;
        [DataField]
        public int employeeID;
        [XMLField]
        public JobWorkerRole[] roles=new JobWorkerRole[0];
        [XMLField]
        public int[] jobTypes=new int[0];
    }
}

