
    using System;
namespace INTAPS.WSIS.Job
{

    public enum ServiceType
    {
        Connection_Transfer = 5,
        Connection_Upgrading = 3,
        Contract_Renewal = 12,
        Defective_Meter_Diagnosis = 8,
        
        Hydrant_Service = 13,
        Leak_Diagnosis = 2,
        Meter_Temporary_Return=3,
        Meter_Return = 9,
        New_Line = 1,
        Ownership_Transfer = 4,
        Replacement_of_Brokendown_Meter = 6,
        Replacement_of_Meter = 7,
        Sewer_Service = 10
    }
}

