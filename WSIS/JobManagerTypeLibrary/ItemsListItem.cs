using System;
using System.Collections.Generic;
using System.Text;
using BIZNET.iERP;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class ItemsListItem:IComparable<ItemsListItem>
    {
        public bool isCategory;
        public ItemCategory category;//CATEGORY CODE REF
        public TransactionItems titem;//ITEM CODE REF
        public override string ToString()
        {
            if (isCategory)
            {
                if (category == null)
                    return "[Empty Category]";
                return "Category:"+category.NameCode;
            }
            if(titem==null)
                return "[Empty Item]";
            return "Item:"+titem.NameCode;
        }
        public int CompareTo(ItemsListItem other)
        {
            if (other == null)
                return 1;
            if (this.isCategory == other.isCategory)
            {
                if (this.isCategory)
                    return this.category.Code.CompareTo(other.category.Code);
                return this.titem.Code.CompareTo(other.titem.Code);
            }
            if (this.isCategory)
                return 1;
            return -1;
        }
    }
}
