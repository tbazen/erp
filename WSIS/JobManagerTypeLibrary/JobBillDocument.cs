﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using INTAPS.ClientServer;

namespace INTAPS.WSIS.Job
{
    [Serializable]
    [INTAPS.RDBMS.TypeID(1001)]
    public class JobBillDocument:CustomerBillDocument
    {
        public JobData job;
        public JobBillOfMaterial bom;
        public BillItem[] jobItems;
        public MaterialItem[] materialItems;
        public ServiceItem[] serviceItems;
        public SummaryItem[] summaryItems;
        public override BillItem[] items
        {
            get { return jobItems; }
        }
    }
}
