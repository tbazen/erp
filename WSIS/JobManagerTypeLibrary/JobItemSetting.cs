﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.IO;
using System.Xml.Serialization;

namespace INTAPS.WSIS.Job
{
    [SingleTableObject,Serializable]
    public class WorkFlowData
    {
        [IDField]
        public int jobID=-1;
        [IDField]
        public int typeID=-1;
        [IDField]
        public int dataKey=0;
        [DataField]
        public string dataType;
        //serialization
        public  string ToXML(Type[] extraTypes)
        {
            System.Xml.Serialization.XmlSerializer s = new XmlSerializer(this.GetType(),extraTypes);
            StringWriter sw = new StringWriter();
            s.Serialize(sw, this);
            return sw.ToString();
        }
        public static WorkFlowData DeserializeXML(string xml, Type SubType,Type[] extraTypes)
        {
            System.Xml.Serialization.XmlSerializer s = new XmlSerializer(SubType,extraTypes);
            StringReader sr = new StringReader(xml);
            return (WorkFlowData)s.Deserialize(sr);
        }

        public void copyData(WorkFlowData workFlowData)
        {
            this.jobID = workFlowData.jobID;
            this.typeID = workFlowData.typeID;
            this.dataKey = workFlowData.dataKey;
            this.dataType = workFlowData.dataType;
        }

        public void setID(int jobID, int typeID)
        {
            this.jobID = jobID;
            this.typeID = typeID;
            this.dataKey = 0;
        }
    }
    public enum JobItemType
    {
        MaterialCostMultiplier,
        FixedUnitCost,
        TotalServiceMultiplier
    }
    [Serializable]
    [SingleTableObject]
    public  class JobItemSetting
    {
        [IDField]
        public string itemCode;
        [DataField]
        public JobItemType type;
        [DataField]
        public double value;
    }
    [Serializable]
    public class SummaryItem
    {
        public string itemCode;
        public string description;
        public double amount;
        public bool add;
        public int saleAccountID;
        public double basePrice;
        public SummaryItem()
        {
        }
        public SummaryItem(string code,string d, double a,bool add,int sa,double bp)
        {
            saleAccountID = sa;
            itemCode = code;
            this.description = d;
            this.amount = a;
            this.add = add;
            this.basePrice = bp;
        }
    }
    [Serializable]
    public class MaterialItem
    {
        public int orderNo;
        public string itemName;
        public string itemCode;
        public double quantity;
        public string unit;
        public double standardUnitCost;
        public double customerUnitCost;
        public bool inSourced;
        public int salesAccountID;
        public double materialCost;
        public bool freeInSourcedMaterial = false;
    }
    [Serializable]
    public class ServiceItem
    {
        public int orderNo;
        public string itemName;
        public string itemCode;
        public double quantity;
        public string unit;
        public double unitCost;
        public int salesAccountID;
    }
}
