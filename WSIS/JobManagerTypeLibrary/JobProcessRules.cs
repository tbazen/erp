using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace INTAPS.WSIS.Job
{
    public class JobProcessRules
    {
        public static List<ServiceType> GetAllServiceTypes()
        {
            List<ServiceType> list = new List<ServiceType>();
            foreach (ServiceType type in Enum.GetValues(typeof(ServiceType)))
            {
                list.Add(type);
            }
            return list;
        }

        public static string GetDurationString(TimeSpan span)
        {
            object obj2;
            string str = "";
            if (span.Days > 0)
            {
                str = span.Days + " days";
            }
            if ((span.Days > 0) || (span.Hours > 0))
            {
                obj2 = str;
                str = string.Concat(new object[] { obj2, " ", span.Hours, " hours" });
            }
            if (((span.Days > 0) || (span.Hours > 0)) || (span.Minutes > 0))
            {
                obj2 = str;
                str = string.Concat(new object[] { obj2, " ", span.Minutes, " minutes" });
            }
            if (((span.Days > 0) || (span.Hours > 0)) || (span.Minutes > 0) || (span.Seconds > 0))
            {
                obj2 = str;
                str = string.Concat(new object[] { obj2, " ", span.Seconds, " seconds" });
            }
            return str;
        }


        public static string GetServiceTypeName(ServiceType type, bool amharic)
        {
            return type.ToString().Replace('_', ' ');
        }

        

        public static ServiceType[] ServiceTypeSetting(int status, out bool[] setting, out bool[] enabled)
        {
            //int index;
            List<ServiceType> servieTypeList = new List<ServiceType>();
            servieTypeList.AddRange((ServiceType[])Enum.GetValues(typeof(ServiceType)));
            setting = new bool[] { };
            enabled = new bool[] { };
            return new ServiceType[0];
            //List<bool> settingsList = new List<bool>();
            //for (int i = 0; i < servieTypeList.Count; i++)
            //{
            //    settingsList.Add(false);
            //}
            //switch (status)
            //{
            //    case JobStatus.New_Line_Application:
            //        setting = new bool[] { true };
            //        enabled = new bool[] { false };
            //        return new ServiceType[] { ServiceType.New_Line };

            //    case JobStatus.Subscriber_Service_Application:
            //        setting = new bool[] { false, false, false, false };
            //        enabled = new bool[] { true, true, true, true };
            //        return new ServiceType[] { 
            //            ServiceType.Leak_Diagnosis,
            //            ServiceType.Contract_Renewal,
            //            ServiceType.Defective_Meter_Diagnosis,
            //            ServiceType.Meter_Return};

            //    case JobStatus.Nonesubscriber_Service_Application:
            //        setting = new bool[] { false, false, false, false };
            //        enabled = new bool[] { true, true, true, true };
            //        return new ServiceType[] { 
            //            ServiceType.Hydrant_Service,
            //            ServiceType.Sewer_Service,
            //            };

            //    case JobStatus.Ownership_Transfer_Application:
            //        setting = new bool[] { true };
            //        enabled = new bool[] { false };
            //        return new ServiceType[] { ServiceType.Ownership_Transfer };
            //    case JobStatus.Connection_Modification_Application:
            //        setting = new bool[] { false, false };
            //        enabled = new bool[] { true, true };
            //        return new ServiceType[] { ServiceType.Connection_Upgrading, ServiceType.Connection_Transfer };
            //    default:
            //        throw new INTAPS.ClientServer.ServerUserMessage("Invalid status:" + status);
            //}
        }

        public static string TimeString(DateTime time)
        {
            return time.ToString("MMM dd,yyyy hh:mm tt");
        }
        public static bool searchArray<Type>(Type[] searchItems, Type[] array)
        {
            foreach (Type st in searchItems)
            {
                foreach (Type so in array)
                {
                    if (st.Equals(so))
                        return true;
                }
            }
            return false;
        }
        public static int searchArray<Type>(Type[] array, Type searchItem)
        {
            int i = 0;
            foreach (Type so in array)
            {
                if (so.Equals(searchItem))
                    return i;
                i++;
            }
            return -1;
        }
        public static bool isJobPastStatus(int status, JobStatusHistory[] array)
        {
            foreach (JobStatusHistory so in array)
            {
                if (so.newStatus == status)
                    return true;
            }

            return false;
        }
        public static bool searchServieType(ServiceType[] servieTypeList, ServiceType[] searchOptions)
        {
            return searchArray<ServiceType>(servieTypeList, searchOptions);
        }
        
        

        public static void buildTwoColumnDataTable(BIZNET.iERP.bERPHtmlTableRowGroup group, params string[] items)
        {
            int remainder;
            switch (items.Length % 4)
            {
                case 0:
                    remainder = 0;
                    break;
                case 2:
                    remainder = 1;
                    break;
                default:
                    throw new ClientServer.ServerUserMessage("Only even number of items supported");
            }

            for (int row = 0; row < items.Length / 4+remainder; row++)
            {
                var cell1 = new BIZNET.iERP.bERPHtmlTableCell(items[row * 4], "dataLabel1");
                var cell2 = new BIZNET.iERP.bERPHtmlTableCell(items[row * 4 + 1], "dataValue1");
                var cell3 = new BIZNET.iERP.bERPHtmlTableCell(row * 4 + 2 < items.Length ? items[row * 4 + 2] : "", "dataLabel1");
                var cell4 = new BIZNET.iERP.bERPHtmlTableCell(row * 4 + 3 < items.Length ? items[row * 4 + 3] : "", "dataValue1");
                group.rows.Add(new BIZNET.iERP.bERPHtmlTableRow(cell1, cell2, cell3, cell4));
            }
        }
    }
}

