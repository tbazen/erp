using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    [XmlInclude(typeof(WaterBillDocument))]
    [XmlInclude(typeof(PenalityBillDocument))]
    [XmlInclude(typeof(InterestBillDocument))]
    [XmlInclude(typeof(JobBillDocument))]
    [XmlInclude(typeof(UnclassifiedPayment))]
    [XmlInclude(typeof(MobilePaymentServiceCharge))]
    [XmlInclude(typeof(CreditBillDocument))]
    public class BillCreditData : WorkFlowData
    {
        public CreditScheme schedule;
        public CustomerBillDocument[] creditedBills = new CustomerBillDocument[0];
    }    
}
