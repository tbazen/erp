using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class BillDepositData : WorkFlowData
    {
        public double deposit;
    }
}
