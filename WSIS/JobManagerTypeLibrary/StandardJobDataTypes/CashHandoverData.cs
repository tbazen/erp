using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
using INTAPS.Accounting;
using BIZNET.iERP;
using INTAPS.RDBMS;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    [TypeID(1003)]
    public class CashHandoverDocument : AccountDocument
    {
        [DocumentTypedReferenceField("CRV","CH","BD")]
        public DocumentTypedReference voucher;
        public int previousHandoverDocumentID=-1;
        public CashHandoverData data;
        public override string BuildHTML()
        {
            return data.reportHTML;
        }
    }
    [Serializable]
    public class CashHandoverData : WorkFlowData
    {
        public string receiptNumber;
        public int sourceCashAccountID;
        public bool bankDespoit=false;
        public string bankDespositReference;
        public DateTime bankDespositDate;
        public int destinationAccountID;
        
        public string reportHTML = null;
        public PaymentInstrumentItem[] instruments;
        public double delta;
        public int handoverDocumentID=-1;
        public double total
        {
            get
            {
                double ret = 0;
                foreach (PaymentInstrumentItem i in instruments)
                    ret += i.amount;
                return ret;
            }
        }
    }
}
