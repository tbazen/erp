using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class CloseCreditSchemeData : WorkFlowData
    {
        public int[] schemeIDs;
        public CreditScheme[] schemes=null;
        public double[] billed=null;
    }    
}
