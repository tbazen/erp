using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class ConntectionMaintenanceData : WorkFlowData
    {
        public int connectionID;
        public bool changeMeter = false;
        public MeterData meterData=null;
    }
}
