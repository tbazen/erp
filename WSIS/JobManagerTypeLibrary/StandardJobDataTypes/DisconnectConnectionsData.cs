﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using INTAPS.ClientServer;

namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class BatchDisconnectData : WorkFlowData
    {
        public bool discontinue=false;
        public VersionedID[] connections = new VersionedID[0];
    }
    [Serializable]
    public class BatchReconnectData : BatchDisconnectData
    {
    }
    public class DeleteBillData : WorkFlowData
    {
        public int billDocumentID;
    }
}
