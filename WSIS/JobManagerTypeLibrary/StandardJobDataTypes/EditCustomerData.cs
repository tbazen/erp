using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
using INTAPS.ClientServer;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class EditCustomerData : WorkFlowData
    {
        public Subscriber oldVersion=null;
        public DiffObject<Subscriber> customerDiff=null;
        public Subscription [] oldVersions=new Subscription[0];
        public DiffObject<Subscription>[] connections=new DiffObject<Subscription>[0];
    }
}
