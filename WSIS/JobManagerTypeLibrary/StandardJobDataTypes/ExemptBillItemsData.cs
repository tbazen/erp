using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class ExemptBillItemsData : WorkFlowData
    {
        public CustomerBillRecord exemptedBill;
        public BillItem[] originalItems;
        public int[] exemptedItems;
        public double[] exemptedAmounts;
    }
}
