﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class NewLineData:WorkFlowData
    {
        public Subscription connectionData=null;
        public NewLineData()
        {
        }
        public NewLineData(Subscription subscription)
        {
            this.connectionData = subscription;
        }
    }
    [Serializable]
    public class GeneralSellsData:WorkFlowData
    {

    }
}
