using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class OwnerShipTransferData : WorkFlowData
    {
        public int connectionID;
        public int newConnectionID = -1;
        public Subscriber oldCustomer = null;
    }
}
