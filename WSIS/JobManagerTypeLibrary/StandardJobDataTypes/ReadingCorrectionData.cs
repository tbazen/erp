using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class ReadingCorrectionData : WorkFlowData
    {
        public int customerID;
        public WaterBillDocument oldBill=null;
        public WaterBillDocument newBill=null;
        public BWFMeterReading oldReading=null;
        public BWFMeterReading newReading=null;
        public BWFMeterReading[] averageReadings = null;
    }
}
