using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class ReturnMeterData : WorkFlowData
    {
        public int connectionID;
        public MaterialOperationalStatus opStatus;
        public bool permanent=false;
        public string remark;  
    }
    
}
