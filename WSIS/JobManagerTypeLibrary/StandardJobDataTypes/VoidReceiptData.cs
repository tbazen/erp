using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment;
using System.Xml.Serialization;
namespace INTAPS.WSIS.Job
{
    [Serializable]
    public class VoidReceiptData : WorkFlowData
    {
        public int receitDocumentID=-1;
        public string receiptHTML = null;
    }
}
