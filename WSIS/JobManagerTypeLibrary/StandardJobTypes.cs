using System;
namespace INTAPS.WSIS.Job
{
    public static class StandardJobTypes
    {
        public const int NEW_LINE = 1;
        public const int LINE_MAINTENANCE = 2;
        public const int CONNECTION_OWNERSHIP_TRANSFER = 3;
        public const int CONNECTION_MAINTENANCE = 4;
        public const int RETURN_METER = 5;
        public const int EDIT_CUTOMER_DATA = 6;
        public const int READING_CORRECTION = 7;
        public const int BILLING_DEPOSIT = 8;
        public const int BILLING_CERDIT = 9;
        public const int VOID_RECEIPT = 10;
        public const int CASH_HANDOVER = 11;
        public const int BATCH_DISCONNECT = 12;
        public const int BATCH_RECONNECT = 13;
        public const int BATCH_DELETE_BILLS = 14;
        public const int LIQUID_WASTE_DISPOSAL = 15;
        public const int HYDRANT_SERVICE = 16;
        public const int CUSTOMER_SPONSORED_NETWORK_WORK = 17;
        public const int CLOSE_CREDIT_SCHEME = 18;
        public const int EXEMPT_BILL_ITEMS = 19;
        public const int CONNECTION_PENALITY = 20;
        public const int GENERAL_SELL = 97;
        public const int OTHER_NONE_TECHNICAL_SERVICES = 98;
        public const int OTHER_TECHNICAL_SERVICES = 99;
        
    }
}
