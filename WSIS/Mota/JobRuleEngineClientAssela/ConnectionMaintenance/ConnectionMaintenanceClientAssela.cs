using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;
using System.Web.UI.HtmlControls;
using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI.HTML;
namespace INTAPS.WSIS.Job.Mota.REClient
{
    [JobRuleClientHandlerAttribute(StandardJobTypes.CONNECTION_MAINTENANCE,priority=1)]
    public class ConnectionMaintenanceClientAssela : JobRuleClientBase, IJobRuleClientHandler
    {

        public NewApplicationForm getNewApplicationForm(int jobID, Subscriber customer, Subscription connection)
        {
            return new ConnectionMaintenanceFormAssela(jobID, connection);
        }

        public string buildDataHTML(JobData job)
        {
             ConntectionMaintenanceAsselaData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_MAINTENANCE, 0, true) as ConntectionMaintenanceAsselaData;
            if (data == null)
                return "";

            Subscription subsc = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
            Subscriber newCustomer = job.customerID == -1 ? job.newCustomer : SubscriberManagmentClient.GetSubscriber(job.customerID);
            HtmlTable conTable = createConnectionTable(subsc);
            string meterhtml="";
            if (data.changeMeter)
            {
                meterhtml += string.Format("<br/><span class='jobSection1'>New Meter Detail</span><br/>{0}", createMeterHTML(data.updatedMeterData));
            }

            string updatedConnectionDetail = "";
            if (data.relocateMeter)
            {
                updatedConnectionDetail =
                    System.Web.HttpUtility.HtmlEncode("Relocated Connection Detail")
                    +(data.updatedSubscription==null?"":HTMLBuilder.ControlToString(createConnectionTable( data.updatedSubscription)));
            }
            return string.Format("<span class='jobSection1'>{0}</span>{1}{2}"
                , System.Web.HttpUtility.HtmlEncode("Connection Detail")
                , HTMLBuilder.ControlToString(conTable)
                ,meterhtml
                ,updatedConnectionDetail
                );
        }


    }

     
}

