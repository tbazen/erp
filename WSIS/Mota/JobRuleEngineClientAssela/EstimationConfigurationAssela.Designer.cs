﻿namespace INTAPS.WSIS.Job.Mota.REClient
{
    partial class EstimationConfigurationAssela
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EstimationConfigurationAssela));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.itemServiceCharge = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.itemPremissionFee = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.itemFileFolderFee = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.itemContractCardFee = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.itemTechnicalServiceChargeTeir1 = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.itemTechnicalServiceChargeTeir2 = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.itemTechnicalServiceChargeTeir3 = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.label11 = new System.Windows.Forms.Label();
            this.itemTransitCharge = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.itemEstimationFee = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.itemFormFee = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.itemsSurvey = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsWaterMeter = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsPipeline = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsHDP = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridDeposit = new System.Windows.Forms.DataGridView();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDepositPrivate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDepositCommercial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.itemOwnershipTransfer = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.label13 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.itemTechnicalServiceCharge = new INTAPS.WSIS.Job.Mota.REClient.ItemTypedValuesControl();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pipeline Items:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(18, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "HDP Pipeline Items:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(18, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Water Meter Items:";
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(536, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(87, 30);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(640, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 30);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(735, 624);
            this.tabControl.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.itemServiceCharge);
            this.tabPage1.Controls.Add(this.itemPremissionFee);
            this.tabPage1.Controls.Add(this.itemFileFolderFee);
            this.tabPage1.Controls.Add(this.itemContractCardFee);
            this.tabPage1.Controls.Add(this.itemTechnicalServiceChargeTeir1);
            this.tabPage1.Controls.Add(this.itemTechnicalServiceChargeTeir2);
            this.tabPage1.Controls.Add(this.itemTechnicalServiceCharge);
            this.tabPage1.Controls.Add(this.itemTechnicalServiceChargeTeir3);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.itemTransitCharge);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.itemEstimationFee);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.itemFormFee);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.itemsSurvey);
            this.tabPage1.Controls.Add(this.itemsWaterMeter);
            this.tabPage1.Controls.Add(this.itemsPipeline);
            this.tabPage1.Controls.Add(this.itemsHDP);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(727, 598);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // itemServiceCharge
            // 
            this.itemServiceCharge.Location = new System.Drawing.Point(207, 1086);
            this.itemServiceCharge.Name = "itemServiceCharge";
            this.itemServiceCharge.Size = new System.Drawing.Size(470, 86);
            this.itemServiceCharge.TabIndex = 2;
            this.itemServiceCharge.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemServiceCharge.values")));
            this.itemServiceCharge.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // itemPremissionFee
            // 
            this.itemPremissionFee.Location = new System.Drawing.Point(207, 994);
            this.itemPremissionFee.Name = "itemPremissionFee";
            this.itemPremissionFee.Size = new System.Drawing.Size(470, 86);
            this.itemPremissionFee.TabIndex = 2;
            this.itemPremissionFee.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemPremissionFee.values")));
            this.itemPremissionFee.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // itemFileFolderFee
            // 
            this.itemFileFolderFee.Location = new System.Drawing.Point(202, 895);
            this.itemFileFolderFee.Name = "itemFileFolderFee";
            this.itemFileFolderFee.Size = new System.Drawing.Size(470, 86);
            this.itemFileFolderFee.TabIndex = 2;
            this.itemFileFolderFee.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemFileFolderFee.values")));
            this.itemFileFolderFee.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // itemContractCardFee
            // 
            this.itemContractCardFee.Location = new System.Drawing.Point(202, 803);
            this.itemContractCardFee.Name = "itemContractCardFee";
            this.itemContractCardFee.Size = new System.Drawing.Size(470, 86);
            this.itemContractCardFee.TabIndex = 2;
            this.itemContractCardFee.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemContractCardFee.values")));
            this.itemContractCardFee.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // itemTechnicalServiceChargeTeir1
            // 
            this.itemTechnicalServiceChargeTeir1.Location = new System.Drawing.Point(202, 414);
            this.itemTechnicalServiceChargeTeir1.Name = "itemTechnicalServiceChargeTeir1";
            this.itemTechnicalServiceChargeTeir1.Size = new System.Drawing.Size(470, 86);
            this.itemTechnicalServiceChargeTeir1.TabIndex = 2;
            this.itemTechnicalServiceChargeTeir1.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemTechnicalServiceChargeTeir1.values")));
            this.itemTechnicalServiceChargeTeir1.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // itemTechnicalServiceChargeTeir2
            // 
            this.itemTechnicalServiceChargeTeir2.Location = new System.Drawing.Point(202, 506);
            this.itemTechnicalServiceChargeTeir2.Name = "itemTechnicalServiceChargeTeir2";
            this.itemTechnicalServiceChargeTeir2.Size = new System.Drawing.Size(470, 86);
            this.itemTechnicalServiceChargeTeir2.TabIndex = 2;
            this.itemTechnicalServiceChargeTeir2.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemTechnicalServiceChargeTeir2.values")));
            this.itemTechnicalServiceChargeTeir2.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // itemTechnicalServiceChargeTeir3
            // 
            this.itemTechnicalServiceChargeTeir3.Location = new System.Drawing.Point(202, 598);
            this.itemTechnicalServiceChargeTeir3.Name = "itemTechnicalServiceChargeTeir3";
            this.itemTechnicalServiceChargeTeir3.Size = new System.Drawing.Size(470, 86);
            this.itemTechnicalServiceChargeTeir3.TabIndex = 2;
            this.itemTechnicalServiceChargeTeir3.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemTechnicalServiceChargeTeir3.values")));
            this.itemTechnicalServiceChargeTeir3.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(23, 1086);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Service Charge:";
            // 
            // itemTransitCharge
            // 
            this.itemTransitCharge.Location = new System.Drawing.Point(202, 322);
            this.itemTransitCharge.Name = "itemTransitCharge";
            this.itemTransitCharge.Size = new System.Drawing.Size(470, 86);
            this.itemTransitCharge.TabIndex = 2;
            this.itemTransitCharge.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemTransitCharge.values")));
            this.itemTransitCharge.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(23, 994);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Permission Fee:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(18, 895);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "File Folder Fee:";
            // 
            // itemEstimationFee
            // 
            this.itemEstimationFee.Location = new System.Drawing.Point(202, 230);
            this.itemEstimationFee.Name = "itemEstimationFee";
            this.itemEstimationFee.Size = new System.Drawing.Size(470, 86);
            this.itemEstimationFee.TabIndex = 2;
            this.itemEstimationFee.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemEstimationFee.values")));
            this.itemEstimationFee.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(18, 414);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(174, 42);
            this.label15.TabIndex = 0;
            this.label15.Text = "Technical Service Charge (pipelines: 0-5 pcs):";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(18, 803);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Contract Card Fee:";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(18, 506);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(174, 43);
            this.label14.TabIndex = 0;
            this.label14.Text = "Technical Service Charge (pipelines: 6-10 pcs):";
            // 
            // itemFormFee
            // 
            this.itemFormFee.Location = new System.Drawing.Point(202, 138);
            this.itemFormFee.Name = "itemFormFee";
            this.itemFormFee.Size = new System.Drawing.Size(470, 86);
            this.itemFormFee.TabIndex = 2;
            this.itemFormFee.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemFormFee.values")));
            this.itemFormFee.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(18, 598);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 50);
            this.label7.TabIndex = 0;
            this.label7.Text = "Technical Service Charge (pipelines: >10 pcs):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(18, 322);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Transit Charge:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(18, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Estimation Fee:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(18, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Form Fee:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(19, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Survey Items:";
            // 
            // itemsSurvey
            // 
            this.itemsSurvey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsSurvey.Location = new System.Drawing.Point(247, 92);
            this.itemsSurvey.Name = "itemsSurvey";
            this.itemsSurvey.Size = new System.Drawing.Size(342, 28);
            this.itemsSurvey.TabIndex = 1;
            this.itemsSurvey.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsWaterMeter
            // 
            this.itemsWaterMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsWaterMeter.Location = new System.Drawing.Point(246, 58);
            this.itemsWaterMeter.Name = "itemsWaterMeter";
            this.itemsWaterMeter.Size = new System.Drawing.Size(342, 28);
            this.itemsWaterMeter.TabIndex = 1;
            this.itemsWaterMeter.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsPipeline
            // 
            this.itemsPipeline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsPipeline.Location = new System.Drawing.Point(246, 5);
            this.itemsPipeline.Name = "itemsPipeline";
            this.itemsPipeline.Size = new System.Drawing.Size(342, 28);
            this.itemsPipeline.TabIndex = 1;
            this.itemsPipeline.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsHDP
            // 
            this.itemsHDP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsHDP.Location = new System.Drawing.Point(246, 31);
            this.itemsHDP.Name = "itemsHDP";
            this.itemsHDP.Size = new System.Drawing.Size(342, 28);
            this.itemsHDP.TabIndex = 1;
            this.itemsHDP.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridDeposit);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(727, 598);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Deposit Rule";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridDeposit
            // 
            this.gridDeposit.AllowUserToAddRows = false;
            this.gridDeposit.AllowUserToDeleteRows = false;
            this.gridDeposit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colType,
            this.colDepositPrivate,
            this.colDepositCommercial});
            this.gridDeposit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDeposit.Location = new System.Drawing.Point(3, 3);
            this.gridDeposit.Name = "gridDeposit";
            this.gridDeposit.Size = new System.Drawing.Size(721, 592);
            this.gridDeposit.TabIndex = 1;
            // 
            // colType
            // 
            this.colType.HeaderText = "Meter Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 79;
            // 
            // colDepositPrivate
            // 
            this.colDepositPrivate.HeaderText = "Desposit Amount (Private)";
            this.colDepositPrivate.Name = "colDepositPrivate";
            this.colDepositPrivate.Width = 141;
            // 
            // colDepositCommercial
            // 
            this.colDepositCommercial.HeaderText = "Deposit Amount (Commercial)";
            this.colDepositCommercial.Name = "colDepositCommercial";
            this.colDepositCommercial.Width = 155;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Gray;
            this.tabPage2.Controls.Add(this.itemOwnershipTransfer);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(727, 598);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "Other Rates";
            // 
            // itemOwnershipTransfer
            // 
            this.itemOwnershipTransfer.Location = new System.Drawing.Point(196, 18);
            this.itemOwnershipTransfer.Name = "itemOwnershipTransfer";
            this.itemOwnershipTransfer.Size = new System.Drawing.Size(470, 86);
            this.itemOwnershipTransfer.TabIndex = 4;
            this.itemOwnershipTransfer.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemOwnershipTransfer.values")));
            this.itemOwnershipTransfer.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(12, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 17);
            this.label13.TabIndex = 3;
            this.label13.Text = "Ownership Transfer Fee:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 624);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(735, 36);
            this.panel1.TabIndex = 8;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(18, 690);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(174, 50);
            this.label16.TabIndex = 0;
            this.label16.Text = "Technical Service Charge (Fittings):";
            // 
            // itemTechnicalServiceChargeTeir
            // 
            this.itemTechnicalServiceCharge.Location = new System.Drawing.Point(202, 690);
            this.itemTechnicalServiceCharge.Name = "itemTechnicalServiceChargeTeir";
            this.itemTechnicalServiceCharge.Size = new System.Drawing.Size(470, 86);
            this.itemTechnicalServiceCharge.TabIndex = 2;
            this.itemTechnicalServiceCharge.values = ((INTAPS.WSIS.Job.Mota.AsselaEstimationConfiguration.ItemTypedValue)(resources.GetObject("itemTechnicalServiceChargeTeir.values")));
            this.itemTechnicalServiceCharge.changed += new System.EventHandler(this.itemFormFee_changed);
            // 
            // EstimationConfigurationAssela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(735, 660);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Name = "EstimationConfigurationAssela";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estimation Configuration";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Client.ItemListPlaceHolder itemsPipeline;
        private Client.ItemListPlaceHolder itemsHDP;
        private Client.ItemListPlaceHolder itemsWaterMeter;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
        private Client.ItemListPlaceHolder itemsSurvey;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView gridDeposit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDepositPrivate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDepositCommercial;
        private System.Windows.Forms.Label label4;
        private ItemTypedValuesControl itemFormFee;
        private ItemTypedValuesControl itemEstimationFee;
        private System.Windows.Forms.Label label5;
        private ItemTypedValuesControl itemTechnicalServiceChargeTeir3;
        private ItemTypedValuesControl itemTransitCharge;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private ItemTypedValuesControl itemPremissionFee;
        private ItemTypedValuesControl itemContractCardFee;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private ItemTypedValuesControl itemServiceCharge;
        private System.Windows.Forms.Label label11;
        private ItemTypedValuesControl itemFileFolderFee;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage2;
        private ItemTypedValuesControl itemOwnershipTransfer;
        private System.Windows.Forms.Label label13;
        private ItemTypedValuesControl itemTechnicalServiceChargeTeir1;
        private ItemTypedValuesControl itemTechnicalServiceChargeTeir2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private ItemTypedValuesControl itemTechnicalServiceCharge;
        private System.Windows.Forms.Label label16;
    }
}