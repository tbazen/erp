using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using BIZNET.iERP;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS;
namespace INTAPS.WSIS.Job.Mota.REServer
{
    public abstract class RESBaseAssela : RESBase
    {
        public RESBaseAssela(JobManagerBDE bde)
            : base(bde)
        {
        }
        public bool isSingleItemService(JobBillOfMaterial[] boms, string itemCode, double quantity)
        {
            if (string.IsNullOrEmpty(itemCode))
                return false;
            if (boms == null)
                return false;
            if (boms.Length != 1)
                return false;
            JobBillOfMaterial bom = boms[0];
            if (bom == null)
                return false;
            if(bom.items.Length!=1)
                return false;
            if (!bom.items[0].itemID.Equals(itemCode, StringComparison.CurrentCultureIgnoreCase))
                return false;
            if (quantity < 0)
                return true;
            return bom.items[0].quantity == quantity;
        }
        protected virtual double getUnitPrice(AsselaEstimationConfiguration config, SubscriberType type, JobBOMItem bomItem, TransactionItems titem,out bool fixedItem)
        {
            double unitCost;
            if (!config.isFixedItemCode(titem.Code, type, 1, out unitCost))
            {
                fixedItem = false;
                if (titem.FixedUnitPrice > 0)
                {
                    return titem.FixedUnitPrice;
                }
                else
                    throw new ServerUserMessage("Unit price not set form item {0}",bomItem.itemID);
            }
            fixedItem = true;
            return unitCost;
        }
        public virtual void analyzeBillOfQuantity(Subscriber customer,JobData job, JobBillOfMaterial bom
            ,  out List<SummaryItem> summary
            , out List<MaterialItem> materials
            , out List<ServiceItem> services
           )
        {
            AsselaEstimationConfiguration config = bde.getConfiguration(StandardJobTypes.NEW_LINE, typeof(AsselaEstimationConfiguration)) as AsselaEstimationConfiguration;
            if (config == null)
                throw new ServerUserMessage("Estimation configuration is not set");
            SubscriberType customerType = customer.subscriberType;
            summary = new List<SummaryItem>();
            materials = new List<MaterialItem>();
            services = new List<ServiceItem>();
            
            double totalMaterialFee = 0;
            double totalService = 0;
            double totalTechnicalMaterialCost = 0;
            double totalTechnicalMaterialFee = 0;
            double totalPipeMaterialCost = 0;
            MaterialItem meterItem = null;

            TransactionItems[] waterMeterItems = bde.expandItemsList(config.waterMeterItems);
            bool hasWaterItem=false;
            TransactionItems[] allSurveyItems=bde.expandItemsList(config.surveyItems);
            double pipelineQuantity = 0;
            bool ishalfinch = true;
            foreach (JobBOMItem item in bom.items)
            {
                BIZNET.iERP.TransactionItems titem = bde.bERP.GetTransactionItems(item.itemID);
                if (titem == null)
                    continue;

                BIZNET.iERP.MeasureUnit mu = bde.bERP.GetMeasureUnit(titem.MeasureUnitID);
                string mustr = mu == null ? "" : mu.Name;
                
                if (titem.GoodOrService == BIZNET.iERP.GoodOrService.Good)
                {
                    string desc;
                    if (string.IsNullOrEmpty(item.description))
                        desc = titem.Name;
                    else
                        desc = item.description;
                    bool isWaterMeter = bde.contains(waterMeterItems, titem.Code);
                    if (isWaterMeter)
                    {
                        if (hasWaterItem || item.quantity != 1)
                            throw new ServerUserMessage("Only one water meter item is permited.");
                        hasWaterItem = true;
                    }

                    MaterialItem mi = new MaterialItem();
                    mi.itemCode = item.itemID;
                    mi.itemName = desc;
                    mi.orderNo = materials.Count + 1;
                    mi.quantity = item.quantity;
                    bool isFixed;
                    mi.standardUnitCost = getUnitPrice(config, customer.subscriberType, item, titem,out isFixed);// titem.FixedUnitPrice;
                    
                    mi.customerUnitCost = item.referenceUnitPrice;
                    mi.inSourced = item.inSourced;
                    mi.unit = mustr;
                    mi.salesAccountID = titem.salesAccountID;
                    materials.Add(mi);
                    
                    double thisMaterialCost = 0;
                    bool isPipileine = bde.contains(bde.expandItemsList(config.pipelineItems), item.itemID);
                    if(isPipileine)
                    {
                        if (!item.itemID.Equals(config.smallGuage))
                            ishalfinch = false;
                        pipelineQuantity += item.quantity;
                    }
                    if (isWaterMeter || mi.inSourced)
                    {
                        if (isWaterMeter && !mi.inSourced)
                            throw new ServerUserMessage("Water meters can't be sourced from customers");
                        thisMaterialCost = mi.standardUnitCost * mi.quantity;
                        if (isWaterMeter)
                        {
                            meterItem = mi;
                            mi.freeInSourcedMaterial = true;
                        }
                        else
                        {
                            totalMaterialFee += thisMaterialCost;
                        }
                        totalTechnicalMaterialFee += thisMaterialCost;
                    }
                    else
                    {
                        thisMaterialCost = item.quantity * Math.Max(mi.standardUnitCost, Math.Max(mi.customerUnitCost, 0));
                    }
                    mi.materialCost = thisMaterialCost;
                    if (!isFixed)
                    {
                        totalTechnicalMaterialCost += thisMaterialCost;
                        if (isPipileine)
                            totalPipeMaterialCost += thisMaterialCost;
                    }
                }
                else
                {
                    string desc;
                    if (string.IsNullOrEmpty(item.description))
                        desc = titem.Name;
                    else
                        desc = item.description;

                    ServiceItem si = new ServiceItem();
                    si.itemCode = titem.Code;
                    si.itemName = desc;
                    si.orderNo = services.Count + 1;
                    si.quantity = item.quantity;
                    bool isFixed;
                    si.unitCost = getUnitPrice(config,customer.subscriberType, item, titem,out isFixed);
                    si.unit = mustr;
                    si.salesAccountID = titem.salesAccountID;
                    services.Add(si);
                    totalService += si.unitCost * si.quantity;
                }
            }

            int materialServiceChargeSalesAccount;

            GetMaterialServiceChargeAmount(totalTechnicalMaterialCost, customer.subscriberType, config, out materialServiceChargeSalesAccount);
            double grandTotal = 0;

            if (AccountBase.AmountGreater(totalMaterialFee, 0))
            {
                summary.Add(new SummaryItem("", "Material Cost", totalMaterialFee, false, -1, 0));
                grandTotal += totalMaterialFee;
            }

            if (AccountBase.AmountGreater(totalTechnicalMaterialFee, 0))
            {
                double transitCharge = config.transitFee.getValue(customer.subscriberType) * totalTechnicalMaterialFee/100;
                summary.Add(new SummaryItem("", "{0}% of Material Cost (Transit Charge)".format(config.transitFee.getValue(customer.subscriberType)), transitCharge, true, materialServiceChargeSalesAccount, totalTechnicalMaterialFee));
                grandTotal += transitCharge;
                
            }
            
            
            if (AccountBase.AmountGreater(totalTechnicalMaterialCost,0))
            {
                //BIZNET.iERP.TransactionItems t = bde.bERP.GetTransactionItems(
                //    config.technicalServiceMultiplier.getItemCode(
                //            !ishalfinch || Account.AmountGreater(pipelineQuantity,5)?SubscriberType.CommercialInstitution:SubscriberType.Private
                //    ));
                //if (t == null)
                //    throw new ServerUserMessage("Please configure technical service rate");

                //double technicalServiceCharge = totalTechnicalMaterialCost * config.technicalServiceMultiplier.getValue(customer.subscriberType) / 100.0d;
                //if (!AccountBase.AmountEqual(technicalServiceCharge, 0))
                //    summary.Add(new SummaryItem(t.Code, t.Name, technicalServiceCharge, true, t.salesAccountID, totalTechnicalMaterialCost));
                //grandTotal += technicalServiceCharge;
                {
                    //other items
                    BIZNET.iERP.TransactionItems t = bde.bERP.GetTransactionItems(
                        config.technicalServiceMultiplier.getItemCode(customer.subscriberType));
                    if (t == null)
                        throw new ServerUserMessage("Please configure technical service rate for fittings");

                    double technicalServiceCharge = (totalTechnicalMaterialCost - totalPipeMaterialCost) * config.technicalServiceMultiplier.getValue(customer.subscriberType) / 100.0d;
                    grandTotal += technicalServiceCharge;
                    if (!AccountBase.AmountEqual(technicalServiceCharge, 0))
                        summary.Add(new SummaryItem(t.Code, t.Name, technicalServiceCharge, true, t.salesAccountID, totalTechnicalMaterialCost - totalPipeMaterialCost));
                }

                if (AccountBase.AmountGreater(totalPipeMaterialCost, 0))
                {
                    //pipieline teir 1

                    BIZNET.iERP.TransactionItems pipeRateItem = bde.bERP.GetTransactionItems(
                        config.technicalServiceMultiplierTeir1.getItemCode(customer.subscriberType));
                    if (pipeRateItem == null)
                        throw new ServerUserMessage("Please configure technical service rate for pipe lines tier 1");
                    double baseValue = totalPipeMaterialCost * Math.Min(pipelineQuantity, 5) / pipelineQuantity;
                    double technicalServiceCharge = baseValue * config.technicalServiceMultiplierTeir1.getValue(customer.subscriberType) / 100.0d;
                    if (!AccountBase.AmountEqual(technicalServiceCharge, 0))
                        summary.Add(new SummaryItem(pipeRateItem.Code, pipeRateItem.Name, technicalServiceCharge, true, pipeRateItem.salesAccountID, baseValue));

                    grandTotal += technicalServiceCharge;
                }

                if (AccountBase.AmountGreater(pipelineQuantity, 5))
                {
                    //pipieline teir 2

                    BIZNET.iERP.TransactionItems pipeRateItem = bde.bERP.GetTransactionItems(
                        config.technicalServiceMultiplierTeir2.getItemCode(customer.subscriberType));
                    if (pipeRateItem == null)
                        throw new ServerUserMessage("Please configure technical service rate for pipe lines tier 2");
                    double q = pipelineQuantity - 5;
                    double baseValue = totalPipeMaterialCost * Math.Min(q,5) / pipelineQuantity;
                    double technicalServiceCharge = baseValue * config.technicalServiceMultiplierTeir2.getValue(customer.subscriberType) / 100.0d;
                    if (!AccountBase.AmountEqual(technicalServiceCharge, 0))
                        summary.Add(new SummaryItem(pipeRateItem.Code, pipeRateItem.Name, technicalServiceCharge, true, pipeRateItem.salesAccountID, baseValue));

                    grandTotal += technicalServiceCharge;
                }
                if (AccountBase.AmountGreater(pipelineQuantity, 10))
                {
                    //pipieline teir 3

                    BIZNET.iERP.TransactionItems pipeRateItem = bde.bERP.GetTransactionItems(
                        config.technicalServiceMultiplierTeir3.getItemCode(customer.subscriberType));
                    if (pipeRateItem == null)
                        throw new ServerUserMessage("Please configure technical service rate for pipe lines tier 3");
                    double baseValue = totalPipeMaterialCost * (pipelineQuantity-10) / pipelineQuantity;
                    double technicalServiceCharge = baseValue * config.technicalServiceMultiplierTeir3.getValue(customer.subscriberType) / 100.0d;
                    if (!AccountBase.AmountEqual(technicalServiceCharge, 0))
                        summary.Add(new SummaryItem(pipeRateItem.Code, pipeRateItem.Name, technicalServiceCharge, true, pipeRateItem.salesAccountID, baseValue));

                    grandTotal += technicalServiceCharge;
                }

            }
            if (!AccountBase.AmountEqual(totalService, 0))
                summary.Add(new SummaryItem("", "Service and Other Fees", totalService, false, -1, 0));
            grandTotal += totalService;

            //Water Meter
            if (meterItem != null && hasWaterItem)
            {
                bool chargeForMeter=false;
                if (job.applicationType == StandardJobTypes.NEW_LINE)
                    chargeForMeter = false;
                else if (job.applicationType == StandardJobTypes.CONNECTION_MAINTENANCE)
                {
                    ConntectionMaintenanceAsselaData mdata = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ConntectionMaintenanceAsselaData;
                    chargeForMeter = mdata.changeMeter;
                } 
                if(chargeForMeter)
                {
                    TransactionItems waterItem = berpBDE.GetTransactionItems(meterItem.itemCode);
                    if (!AccountBase.AmountEqual(meterItem.standardUnitCost * meterItem.quantity, 0))
                        summary.Add(new SummaryItem("", "Water Meter", meterItem.standardUnitCost * meterItem.quantity, true, waterItem.salesAccountID, 0));
                    grandTotal += meterItem.standardUnitCost * meterItem.quantity;
                }
                
            }

            //Deposit
            if (job.applicationType == StandardJobTypes.NEW_LINE && meterItem!=null)
            {
                for (int k = 0; k < config.meterDeposit.Length; k++)
                {
                    if (config.meterDeposit[k].privateItemCode.Equals(meterItem.itemCode))
                    {
                        double depoistAmount = customerType == SubscriberType.Private ? config.meterDeposit[k].privateValue : config.meterDeposit[k].institutionalValue;
                        if (!AccountBase.AmountEqual(depoistAmount, 0))
                            summary.Add(new SummaryItem("", "Water Meter Deposit", depoistAmount, true, customer.depositAccountID, 0));
                        grandTotal += customerType == SubscriberType.Private ? config.meterDeposit[k].privateValue : config.meterDeposit[k].institutionalValue;
                        break;
                    }
                }
                
            }
            if (AccountBase.AmountGreater(totalTechnicalMaterialCost, 0))
            {
                BIZNET.iERP.TransactionItems serviceChargeItem = bde.bERP.GetTransactionItems(config.serviceChargeMultiplier.getItemCode(customer.subscriberType));
                if (serviceChargeItem == null)
                    throw new ServerUserMessage("Please configure service charge rate");
                double serviceCharge = grandTotal * config.serviceChargeMultiplier.getValue(customer.subscriberType)/100;
                if (!AccountBase.AmountEqual(serviceCharge, 0))
                    summary.Add(new SummaryItem("", "Service Charge", serviceCharge, true, serviceChargeItem.salesAccountID,grandTotal));

                grandTotal += totalMaterialFee;
            }
            
        }

        private double GetMaterialServiceChargeAmount(double totalMaterialFee, SubscriberType customerType, AsselaEstimationConfiguration config, out int salesAccountID)
        {
            BIZNET.iERP.TransactionItems t = bde.bERP.GetTransactionItems(config.transitFee.getItemCode(customerType));
            if(t==null)                
                throw new ServerUserMessage("Please configure transit fee");
            double mServiceCharge = totalMaterialFee * config.transitFee.getValue(customerType) / 100.0d;
            salesAccountID = t.salesAccountID;
            return mServiceCharge;
        }

        

        
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            JobBillDocument bill = new JobBillDocument();

            List<SummaryItem> summary;
            List<MaterialItem> materials;
            List<ServiceItem> services;

            analyzeBillOfQuantity(customer, job, billOfMaterial, out summary, out materials, out services);
            bill.summaryItems = summary.ToArray();
            bill.materialItems = materials.ToArray();
            bill.serviceItems = services.ToArray();

            
            List<BillItem> items = new List<BillItem>();
            foreach (MaterialItem mi in materials)
            {
                if (!mi.inSourced || mi.freeInSourcedMaterial)
                    continue;
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.standardUnitCost;
                i.price = mi.quantity * mi.standardUnitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;

                items.Add(i);
            }
            foreach (ServiceItem mi in services)
            {
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.salesAccountID;
                i.description = mi.itemName;
                i.hasUnitPrice = true;
                i.unitPrice = mi.unitCost;
                i.price = mi.quantity * mi.unitCost;
                i.quantity = mi.quantity;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);

            }
            foreach (SummaryItem mi in summary)
            {
                if (!mi.add)
                    continue;
                BillItem i = new BillItem();
                i.itemTypeID = items.Count;
                i.incomeAccountID = mi.saleAccountID;
                i.description = mi.description;
                i.hasUnitPrice = false;
                i.price = mi.amount;
                i.accounting = BillItemAccounting.Cash;
                items.Add(i);
            }
            bill.job = job;
            bill.customer = customer;
            bill.bom = billOfMaterial;
            bill.DocumentDate = DateTime.Now;
            bill.jobItems = items.ToArray();
            bill.ShortDescription = billOfMaterial.note;
            bill.draft = false;
            return bill;
        }

        public override string generateInvoiceHTML(JobBillDocument document)
        {
            JobData job = document.job; ;
            Subscriber customer = document.customer;
            INTAPS.Evaluator.EData jobNo = new Evaluator.EData(Evaluator.DataType.Text, job.jobNo);
            INTAPS.Evaluator.EData appDate = new Evaluator.EData(Evaluator.DataType.Text, job.startDate.ToString("MMM dd,yyyy"));
            INTAPS.Evaluator.EData customerName = new Evaluator.EData(Evaluator.DataType.Text, customer.name);
            INTAPS.Evaluator.EData custCode = new Evaluator.EData(Evaluator.DataType.Text, customer.customerCode);
            Kebele keb = base.bde.subscriberBDE.GetKebele(document.customer.Kebele);
            INTAPS.Evaluator.EData kebele = new Evaluator.EData(Evaluator.DataType.Text, keb == null ? "[Unknown]" : keb.name);
            INTAPS.Evaluator.EData houseNo = new Evaluator.EData(Evaluator.DataType.Text, customer.address);
            INTAPS.Evaluator.EData phoneNo = new Evaluator.EData(Evaluator.DataType.Text, customer.phoneNo);
            INTAPS.Evaluator.EData customerType = new Evaluator.EData(Evaluator.DataType.Text, Subscriber.EnumToName(customer.subscriberType));
            INTAPS.Payroll.Employee prepareEmployee = bde.BdePayroll.GetEmployeeByLoginName(document.bom.preparedBy);
            INTAPS.Evaluator.EData approvedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData approvedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedBy = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData checkedByDate = new Evaluator.EData(Evaluator.DataType.Text, "");
            INTAPS.Evaluator.EData preparedBy = new Evaluator.EData(Evaluator.DataType.Text, prepareEmployee == null ? "" : prepareEmployee.employeeName);
            INTAPS.Evaluator.EData preparedByDate = new Evaluator.EData(Evaluator.DataType.Text, document.bom.analysisTime.ToString("MMM dd,yyyy"));


            INTAPS.Evaluator.ListData list;
            double totalMaterialValue = 0;
            double totalMaterialFeeValue=0;
            double totalServiceValue=0;
            double grandTotal = 0;
            int i;

            list = new Evaluator.ListData(document.materialItems.Length);
            i = 0;
            foreach (MaterialItem mi in document.materialItems)
            {
                double materialValue = mi.materialCost;
                double materialFee = (mi.inSourced ? 
                    mi.freeInSourcedMaterial?0:mi.standardUnitCost : 
                    0) * mi.quantity;
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(11);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, mi.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, mi.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, mi.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, mi.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, mi.standardUnitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (mi.standardUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[7] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost==-1 ? "" : mi.customerUnitCost.ToString("#,#00.00"));
                row.elements[8] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced || mi.customerUnitCost == -1 ? "" : (mi.customerUnitCost * mi.quantity).ToString("#,#00.00"));
                row.elements[9] = new Evaluator.EData(Evaluator.DataType.Text, mi.materialCost);
                row.elements[10] = new Evaluator.EData(Evaluator.DataType.Text, mi.inSourced ? materialFee.ToString("#,#00.00") : "");
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal+=materialFee;
                totalMaterialValue += materialValue;
                totalMaterialFeeValue += materialFee;
                i++;
            }
            INTAPS.Evaluator.EData materialList = new Evaluator.EData(Evaluator.DataType.ListData, list);

            list = new Evaluator.ListData(document.serviceItems.Length);
            i = 0;
            foreach (ServiceItem si in document.serviceItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(7);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.orderNo.ToString());
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, si.itemName);
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, si.itemCode);
                row.elements[3] = new Evaluator.EData(Evaluator.DataType.Text, si.quantity.ToString());
                row.elements[4] = new Evaluator.EData(Evaluator.DataType.Text, si.unit);
                row.elements[5] = new Evaluator.EData(Evaluator.DataType.Text, si.unitCost.ToString("#,#00.00"));
                row.elements[6] = new Evaluator.EData(Evaluator.DataType.Text, (si.unitCost * si.quantity).ToString("#,#00.00"));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                grandTotal+=si.unitCost*si.quantity;
                totalServiceValue += si.unitCost * si.quantity;
                i++;
            }
            INTAPS.Evaluator.EData labourList = new Evaluator.EData(Evaluator.DataType.ListData, list);


            i=0;
            list= new Evaluator.ListData(document.summaryItems.Length);
            foreach (SummaryItem si in document.summaryItems)
            {
                INTAPS.Evaluator.ListData row = new Evaluator.ListData(3);
                row.elements[0] = new Evaluator.EData(Evaluator.DataType.Text, si.description);
                row.elements[1] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.amount));
                row.elements[2] = new Evaluator.EData(Evaluator.DataType.Text, AccountBase.FormatAmount(si.basePrice));
                list[i] = new Evaluator.EData(Evaluator.DataType.ListData, row);
                if (si.add)
                    grandTotal += si.amount;
                i++;
            }
            INTAPS.Evaluator.EData feeSummaryData = new Evaluator.EData(Evaluator.DataType.ListData, list);



            INTAPS.Evaluator.EData totalMaterial = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalMaterialFee = new Evaluator.EData(Evaluator.DataType.Text, totalMaterialFeeValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalLabour = new Evaluator.EData(Evaluator.DataType.Text, totalServiceValue.ToString("#,#00.00"));
            INTAPS.Evaluator.EData totalSummary = new Evaluator.EData(Evaluator.DataType.Text, grandTotal.ToString("#,#00.00"));
            string str;
            string ret = bde.BdeAccounting.EvaluateEHTML(bde.SysPars.jobInvoiceReportID, new object[]{
                 "jobNo",jobNo
                ,"appDate",appDate
                ,"customerName",customerName
                ,"custCode",custCode
                ,"kebele",kebele
                ,"houseNo",houseNo
                ,"phoneNo",phoneNo
                ,"customerType",customerType
                ,"feeSummaryData",feeSummaryData
                ,"materialList",materialList
                ,"labourList",labourList
                ,"approvedBy",approvedBy
                ,"approvedByDate",approvedByDate
                ,"checkedBy",checkedBy
                ,"checkedByDate",checkedByDate
                ,"preparedBy",preparedBy
                ,"preparedByDate",preparedByDate
                ,"totalSummary",totalSummary
                ,"totalMaterial",totalMaterial
                ,"totalMaterialFee",totalMaterialFee
                ,"totalLabour",totalLabour
                },out str);
            return ret;
        }
        public override JobStatusType[] getAllStatusTypes()
        {
            return new JobStatusType[]{ 
                        StandardJobStatus.Application
                        ,StandardJobStatus.ApplicationApproval
                        ,StandardJobStatus.ApplicationPayment
                        ,StandardJobStatus.Survey
                        ,StandardJobStatus.Analysis
                        ,StandardJobStatus.WorkApproval
                        ,StandardJobStatus.WorkPayment
                        ,StandardJobStatus.Contract
                        ,StandardJobStatus.TechnicalWork
                        ,StandardJobStatus.Finalization
                        ,StandardJobStatus.Canceled
                        ,StandardJobStatus.Finished
            };
        }

    }
}
