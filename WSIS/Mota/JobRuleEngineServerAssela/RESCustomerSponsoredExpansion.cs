﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Mota;

namespace INTAPS.WSIS.Job.Mota.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK, "Customer Sponsored Work")]
    public class RESCustomerSponsoredExpansion : RESTechnicalServiceAssela<CustomerSponsoredNetworkInstallation>, IJobRuleServerHandler
    {
        public RESCustomerSponsoredExpansion(JobManagerBDE bde)
            : base(bde)
        {
        }

        public override int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                case StandardJobStatus.SURVEY:
                    return new int[] { StandardJobStatus.ANALYSIS, StandardJobStatus.CANCELED };
                case StandardJobStatus.ANALYSIS:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED };
                default:
                    return new int[0];
            }
        }

        protected override void onForwardToApplicationPaymet(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, AsselaEstimationConfiguration config)
        {
            
        }
        
        protected override void onForwardToFinalization(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, AsselaEstimationConfiguration config)
        {
            
        }
    }
    /*
public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            if (AID != -1 && job.applicationType == StandardJobTypes.NEW_LINE)
            {
                lock (bde.WriterHelper)
                {
                    bde.WriterHelper.BeginTransaction();
                    try
                    {

                        customer.depositAccountID = bde.subscriberBDE.getOrCreateCustomerAccountID(AID, customer, true);
                        JobBillDocument ret = base.generateInvoice(AID, customer, job, billOfMaterial);
                        bde.WriterHelper.CommitTransaction();
                        return ret;
                    }
                    catch
                    {
                        bde.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }
            }

            return base.generateInvoice(AID, customer, job, billOfMaterial);
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);

        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {

        }

       
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);

            JobBillOfMaterial[] boms = null;
            AsselaEstimationConfiguration config = bde.getConfiguration<AsselaEstimationConfiguration>(StandardJobTypes.NEW_LINE);
            if (config == null)
                throw new ServerUserMessage("Configure estimation parameters");

            switch (nextState)
            {
                case StandardJobStatus.FINALIZATION:
                    checkPayment(job.id);
                    CompletedJobDocument completed = new CompletedJobDocument();
                    completed.job = job;
                    completed.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, completed);
                    job.completionDocumentID = completed.AccountDocumentID;
                    bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                    bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    break;
                case StandardJobStatus.ANALYSIS:
                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length != 1)
                        throw new ServerUserMessage("Exactly one bill of material is required to send this job to analysis");
                    foreach (JobBOMItem item in boms[0].items)
                        if (item.referenceUnitPrice != -1)
                            throw new ServerUserMessage("It is not allowed to set reference prices at this stage");
                    List<JobBOMItem> items = new List<JobBOMItem>(boms[0].items);
                    //foreach (BIZNET.iERP.TransactionItems titem in bde.expandItemsList(config.getNewLineFixedItems()))
                    //{
                    //    JobBOMItem ji = new JobBOMItem();
                    //    ji.itemID = titem.Code;
                    //    ji.inSourced = true;
                    //    ji.description = titem.Name;
                    //    ji.quantity = 1;
                    //    items.Add(ji);
                    //}
                    boms[0].items = items.ToArray();
                    bde.SetBillofMateial(AID, boms[0]);
                    break;
                case StandardJobStatus.SURVEY:
                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length == 1)
                    {
                        List<JobBOMItem> newItems = new List<JobBOMItem>();

                        BIZNET.iERP.TransactionItems[] allowedItems = bde.expandItemsList(config.surveyItems);
                        foreach (JobBOMItem item in boms[0].items)
                        {
                            if (bde.contains(allowedItems, item.itemID))
                            {
                                item.referenceUnitPrice = -1;
                                newItems.Add(item);
                            }
                        }
                        boms[0].items = newItems.ToArray();
                        bde.SetBillofMateial(AID, boms[0]);
                    }
                    break;
                case StandardJobStatus.CONTRACT:
                    base.checkPayment(job.id, out boms);
                    break;
                case StandardJobStatus.TECHNICAL_WORK:
                    base.checkPayment(job.id);
                    if (job.completionDocumentID != -1)
                    {
                        bde.BdeAccounting.DeleteAccountDocument(AID, job.completionDocumentID, false);
                        job.completionDocumentID = -1;
                        bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                        bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    }
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                case StandardJobStatus.WORK_APPROVAL:
                    bde.verifyAccountingClearance(AID, job.customerID, date, job);
                    if (job.customerID < 1)
                    {
                        if (job.newCustomer == null)
                            throw new ServerUserMessage("New customer not set");
                        if (job.newCustomer.id == -1)
                        {
                            job.newCustomer.id = bde.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                            job.newCustomer = bde.subscriberBDE.GetSubscriber(job.newCustomer.id);
                            bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, job);
                        }
                    }
                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        if (bom.invoiceNo < 1)
                            bde.generateInvoice(AID, bom.id, worker);
                        foreach (JobBOMItem item in bom.items)
                            if (!item.inSourced && item.referenceUnitPrice == -1)
                                throw new ServerUserMessage("Customer reference price are not entered");
                    }
                    break;


            }
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
        }     
     */
}