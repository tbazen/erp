﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Mota
{
    [Serializable]
    public class ConntectionMaintenanceAsselaData : WorkFlowData
    {
        //public bool chargeForMeter;
       
        //public bool meterDiagnosis;
        //public bool supportLetter;
        public bool estimation;
        public bool changeMeter;
        public bool relocateMeter;
        public int connectionID;
        public MeterData updatedMeterData= null;
        public Subscription updatedSubscription = null;
        
        public bool upateConneciton = false;
        public ConntectionMaintenanceAsselaData()
        {
        }

        public bool hasApplicationFee { get { return true; } }
    }
}
