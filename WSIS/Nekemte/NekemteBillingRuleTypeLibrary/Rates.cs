﻿using System;
using System.Collections.Generic;

using System.Text;

namespace INTAPS.SubscriberManagment.Nekemte
{
  
    [Serializable]
    public class NekemteBillingRate
    {
        public int periodFrom=-1;
        public double fixedPenality = 30;
        public double proportionalPenality=10;

        public double calculate(double billAmount)
        {
            return fixedPenality + proportionalPenality * billAmount / 100;
        }
    }
}
