﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.WSIS.Job.Nekemte;

namespace INTAPS.WSIS.Job.Nekemte.REClient
{
    public partial class NekemteConnectionMaintenanceForm : NewApplicationForm
    {
        Subscription _connection;
        int _jobID;
        JobData _job;
        bool _connectionUpdated = false;
        public void initRelocateForm()
        {
            this.comboKebele.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
            Kebele item = new Kebele();
            item.id = -1;
            item.name = "Unknown";
            this.comboKebele.Items.Add(item);

            this.comboKebele.SelectedIndex = 0;

            this.comboDMA.Items.AddRange(SubscriberManagmentClient.getAllDMAs());
            DistrictMeteringZone dma = new DistrictMeteringZone();
            dma.id = -1;
            dma.name = "Unknown";
            this.comboDMA.Items.Add(dma);
            this.comboDMA.SelectedIndex = 0;

            this.comboPressureZone.Items.AddRange(SubscriberManagmentClient.getAllPressureZones());
            PressureZone zone = new PressureZone();
            zone.id = -1;
            zone.name = "Unknown";
            this.comboPressureZone.Items.Add(zone);
            this.comboPressureZone.SelectedIndex = 0;

        }
        public void populateRelocateTab(Subscription connection)
        {

            int i = 0;
            foreach (Kebele kebele in this.comboKebele.Items)
            {
                if (kebele.id == connection.Kebele)
                {
                    this.comboKebele.SelectedIndex = i;
                    break;
                }
                i++;
            }
            i = 0;
            foreach (DistrictMeteringZone dma in this.comboDMA.Items)
            {
                if (dma.id == connection.dma)
                {
                    this.comboDMA.SelectedIndex = i;
                    break;
                }
                i++;
            }
            i = 0;
            foreach (PressureZone zone in this.comboPressureZone.Items)
            {
                if (zone.id == connection.pressureZone)
                {
                    this.comboPressureZone.SelectedIndex = i;
                    break;
                }
                i++;
            }
            this.textLandCertifcate.Text = (connection.landCertificateNo == null) ? "" : connection.landCertificateNo;
            this.textAddress.Text = (connection.address == null) ? "" : connection.address;
            this.textParcelCode.Text = connection.parcelNo;

            this.textWMX.Text = connection.waterMeterX == 0 ? "" : connection.waterMeterX.ToString();
            this.textWMY.Text = connection.waterMeterY == 0 ? "" : connection.waterMeterY.ToString();

        }
        public bool validateRelocateData()
        {
            double WMx = 0, WMy = 0;
            if (!textWMX.Text.Equals("") || !textWMY.Text.Equals(""))
            {
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textWMX, "Please enter valid water meter x coordinate", out WMx))
                    return false;
                if (!INTAPS.UI.Helper.ValidateDoubleTextBox(textWMY, "Please enter valid water meter y coordinate", out WMy))
                    return false;
            }
            return true;
        }
        public Subscription getUpdatedConnection()
        {
            Subscription ret = new Subscription();
            ret.landCertificateNo = this.textLandCertifcate.Text.Trim();
            ret.Kebele = ((Kebele)this.comboKebele.SelectedItem).id;
            ret.dma = this.comboDMA.SelectedItem is DistrictMeteringZone ? ((DistrictMeteringZone)this.comboDMA.SelectedItem).id : -1;
            ret.pressureZone = this.comboPressureZone.SelectedItem is PressureZone ? ((PressureZone)this.comboPressureZone.SelectedItem).id : -1;

            ret.landCertificateNo = this.textLandCertifcate.Text;
            ret.address = this.textAddress.Text;

            ret.parcelNo = this.textParcelCode.Text;

            if (!double.TryParse(textWMX.Text, out ret.waterMeterX)) ret.waterMeterX = 0;
            if (!double.TryParse(textWMY.Text, out ret.waterMeterY)) ret.waterMeterY = 0;
            return ret;
        }
        public NekemteConnectionMaintenanceForm(int jobID, Subscription connection)
        {
            InitializeComponent();
            initRelocateForm();
            
            _jobID = jobID;
            if (_jobID == -1)
            {
                _connection = connection;
                this.populateRelocateTab(_connection);
                _job = null;
                setConnectionChange(false);
                setMeterChange(false);
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                NekemteConnectionMaintenanceData data = JobManagerClient.getWorkFlowData(job.id, StandardJobTypes.CONNECTION_MAINTENANCE, 0, true) as NekemteConnectionMaintenanceData;
                _connection = SubscriberManagmentClient.GetSubscription(data.connectionID, job.statusDate.Ticks);
                _job = job;
                setConnectionChange(data.relocateMeter);
                setMeterChange(data.changeMeter);
                if (data.changeMeter || data.relocateMeter)
                {
                    if (data.updatedSubscription == null)
                    {
                        this.populateRelocateTab(_connection);
                    }
                    else
                        this.populateRelocateTab(data.updatedSubscription);
                }
                checkRelocateMeter.Checked=data.relocateMeter;
                checkEstimation.Checked=data.estimation;
            }
            setConnection();
        }

        void setConnectionChange(bool relocate)
        {
            if (relocate && !tabControl.TabPages.Contains(pageConnection))
            {
                tabControl.TabPages.Add(pageConnection);
            }
            else if (!relocate && tabControl.TabPages.Contains(pageConnection))
            {
                tabControl.TabPages.Remove(pageConnection);
            }

        }
        void setMeterChange(bool change)
        {
            if (change && !tabControl.TabPages.Contains(pageMeter))
            {
                tabControl.TabPages.Add(pageMeter);
            }
            else if (!change && tabControl.TabPages.Contains(pageMeter))
            {
                tabControl.TabPages.Remove(pageMeter);
            }

        }
        void setConnection()
        {
            applicationPlaceHolder.setCustomer(_connection.subscriber, false);
            browserExisting.LoadTextPage("Connection", "<h2>Connection Detail</h1>" + BIZNET.iERP.bERPHtmlBuilder.ToString(JobRuleClientBase.createConnectionTable(_connection))
                );
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CONNECTION_MAINTENANCE;
                }
                else
                    app = _job;

                app.customerID = _connection.subscriber.id;
                applicationPlaceHolder.getApplicationInfo(out app.startDate, out app.description);
                NekemteConnectionMaintenanceData data = new NekemteConnectionMaintenanceData();
                data.connectionID = _connection.id;
                
                data.relocateMeter = checkRelocateMeter.Checked;
                data.estimation = checkEstimation.Checked;
                if (tabControl.TabPages.Contains(pageConnection))
                {
                    if (!this.validateRelocateData())
                        return;
                    data.updatedSubscription = this.getUpdatedConnection();
                }
                if (tabControl.TabPages.Contains(pageMeter))
                {
                    if (!meterEditor.validateControlData())
                        return;
                    data.updatedMeterData= meterEditor.getMeterData();
                }
                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, data);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }

                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void onServiceDetailChanged(object sender, EventArgs e)
        {
            if (checkRelocateMeter.Checked)
            {
                checkEstimation.Checked = true;
                checkEstimation.Enabled = false;
            }
            else
                checkEstimation.Enabled = true;
        }

        private void connection_SubscripiontUpdated(object sender, EventArgs e)
        {
            _connectionUpdated = true;
        }

    }
}
