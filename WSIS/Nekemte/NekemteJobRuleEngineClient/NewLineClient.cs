﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;

namespace INTAPS.WSIS.Job.Nekemte.REClient
{
    public class ContractGeneratorParamters
    {
        public string date;
        public string year;
        public string name;
    }
    [JobRuleClientHandlerAttribute(StandardJobTypes.NEW_LINE,"Configure Estimation Parameters",true)]
    public class NekemteNewLineClient : StandardNewLineClientBase
    {
        public override System.Windows.Forms.Form getConfigurationForm()
        {
            return new NekemteEstimationConfigurationForm();
        }
        public override bool processURL(System.Windows.Forms.Form parent, string path, System.Collections.Hashtable query)
        {
            switch (path)
            {
                case "genContract":
                    return true;
            }
            return base.processURL(parent, path, query);
        }
        
        public override string buildDataHTML(JobData job)
        {
            string html=base.buildDataHTML(job);
            if (job.status == StandardJobStatus.FINISHED)
                html += string.Format("<a href='genContract?jobID={0}'>Gnerate Contract</a",job.id);
            return html;
        }
    }
}
