﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Nekemte;

namespace INTAPS.WSIS.Job.Nekemte.REServer
{
    [JobRuleServerHandler(StandardJobTypes.NEW_LINE, "New Line")]
    public class RESNewLineApplicationNekemte : RESTechnicalServiceAssela<NewLineData>, IJobRuleServerHandler
    {
        public RESNewLineApplicationNekemte(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                case StandardJobStatus.SURVEY:
                    return new int[] { StandardJobStatus.ANALYSIS, StandardJobStatus.CANCELED };
                case StandardJobStatus.ANALYSIS:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.CONTRACT, StandardJobStatus.CANCELED };
                case StandardJobStatus.CONTRACT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINISHED:
                    return new int[] { };
                default:
                    return new int[0];
            }
        }
        protected override void onForwardToAnalysis(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, NekemteEstimationConfiguration config)
        {
            List<JobBillOfMaterial> boms = getNoneApplicationBom(bde, config, job.id);
            if (boms.Count != 1)
                throw new ServerUserMessage("Exactly two bill of material is required to send this job to analysis");
            JobBillOfMaterial workBOM = boms[0];

            if (job.status != StandardJobStatus.WORK_APPROVAL)
            {
                foreach (JobBOMItem item in workBOM.items)
                    if (!item.inSourced)
                    {
                        if (item.referenceUnitPrice != -1)
                            throw new ServerUserMessage("It is not allowed to set reference prices at this stage");
                        item.referenceUnitPrice = item.unitPrice;
                    }
            }

            List<JobBOMItem> items = new List<JobBOMItem>(workBOM.items);
            Subscriber customer = bde.getJobCustomer(job);
            foreach (NekemteEstimationConfiguration.ItemTypedValue fi in config.getNewLineFixedItems())
            {
                BIZNET.iERP.TransactionItems titiem = bde.bERP.GetTransactionItems(fi.getItemCode(customer.subscriberType));

                items.Add(new JobBOMItem()
                {
                    description = titiem.Name,
                    itemID = titiem.Code,
                    inSourced = true,
                    quantity = 1,
                    referenceUnitPrice = 0,
                    unitPrice = fi.getValue(customer.subscriberType),
                    calculated = true
                });
            }

            items.AddRange(new JobBOMItem[]{
                new JobBOMItem(){
                            description="Application Form",
                             itemID=config.formFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.formFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        new JobBOMItem(){
                            description="Estimation",
                             itemID=config.estimationFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.estimationFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        new JobBOMItem(){
                            description="Contract Card",
                             itemID=config.contractCardFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.contractCardFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        new JobBOMItem(){
                            description="File Folder Cover",
                             itemID=config.fileFolderFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.fileFolderFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                new JobBOMItem(){
                            description="Permission",
                             itemID=config.permissionFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.permissionFee.getValue(customer.subscriberType),
                             calculated=true
                        },


                    });

            workBOM.items = items.ToArray();
            _ignoreAID = AID;
            bde.SetBillofMateial(AID, workBOM, true);
            customer = bde.getJobCustomer(job);
        }
        public override void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            NewLineData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();

                    if (job.customerID < 1)
                    {
                        if (job.newCustomer == null)
                            throw new ServerUserMessage("New customer not set");
                        if (job.newCustomer.id == -1)
                        {
                            job.newCustomer.id = bde.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                            job.newCustomer = bde.subscriberBDE.GetSubscriber(job.newCustomer.id);
                            bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, job);
                        }
                    }
                    NekemteEstimationConfiguration config = bde.getConfiguration<NekemteEstimationConfiguration>(StandardJobTypes.NEW_LINE);
                    data.connectionData.subscriptionStatus = SubscriptionStatus.Active;
                    data.connectionData.contractNo = bde.GetNextContractNumber(AID, data.connectionData.Kebele);
                    data.connectionData.subscriberID = job.customerID < 1 ? job.newCustomer.id : job.customerID;
                    data.connectionData.ticksFrom = date.Ticks;
                    data.connectionData.timeBinding = RDBMS.DataTimeBinding.LowerBound;

                    data.connectionData.prePaid = false;
                    data.connectionData.id = bde.subscriberBDE.AddSubscription(AID, data.connectionData);
                    data.connectionData = bde.subscriberBDE.GetSubscription(data.connectionData.id, data.connectionData.ticksFrom);

                    bde.setWorkFlowData(AID, data);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        protected override void onForwardToApplicationPaymet(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, NekemteEstimationConfiguration config)
        {

            if (job.customerID < 1)
            {
                if (job.newCustomer == null)
                    throw new ServerUserMessage("New customer not set");
                if (job.newCustomer.id == -1)
                {
                    job.customerID = job.newCustomer.id = bde.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                    job.newCustomer = bde.subscriberBDE.GetSubscriber(job.newCustomer.id);
                    bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, job);
                }
            }
            Subscriber customer = bde.getJobCustomer(job);
            JobBillOfMaterial appPaymentBOM = new JobBillOfMaterial();
            appPaymentBOM.jobID = job.id;
            appPaymentBOM.note = "Application Payment";
            appPaymentBOM.analysisTime = DateTime.Now;
            appPaymentBOM.items = new JobBOMItem[]{
                new JobBOMItem(){
                            description="Application Form",
                             itemID=config.formFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.formFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        new JobBOMItem(){
                            description="Estimation",
                             itemID=config.estimationFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.estimationFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        new JobBOMItem(){
                            description="Contract Card",
                             itemID=config.contractCardFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.contractCardFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        new JobBOMItem(){
                            description="File Folder Cover",
                             itemID=config.fileFolderFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.fileFolderFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                new JobBOMItem(){
                            description="Permission",
                             itemID=config.permissionFee.getItemCode(customer.subscriberType),
                             inSourced=true,
                             quantity=1,
                             referenceUnitPrice=0,
                             unitPrice=config.permissionFee.getValue(customer.subscriberType),
                             calculated=true
                        },
                        
                        
                    };
            _ignoreAID = AID;
            appPaymentBOM.id = bde.SetBillofMateial(AID, appPaymentBOM, true);
            bde.generateInvoice(AID, appPaymentBOM.id, worker);
        }
        protected override void onForwardToFinalization(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, NekemteEstimationConfiguration config)
        {
            checkPayment(job.id);
            NewLineData data = checkAndGetData(job);
            if (data == null)
                throw new ServerUserMessage("Please enter connection data");
            if (string.IsNullOrEmpty(data.connectionData.serialNo) || string.IsNullOrEmpty(data.connectionData.modelNo))
                throw new ServerUserMessage("Meter data not complete");
        }
        public override void onBOMSet(int AID, JobData job, JobBillOfMaterial bom)
        {
            if (job.status == StandardJobStatus.SURVEY)
            {
                List<JobBOMItem> meters = getMeterItems(new JobBillOfMaterial[] { bom });
                if (meters.Count != 1 || meters[0].quantity != 1)
                    throw new ServerUserMessage("Exactly one meter item should be included in the bill of quantity");

                NewLineData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as NewLineData;
                if (data == null)
                {
                    data = new NewLineData();
                    data.connectionData = new Subscription();
                    data.jobID = job.id;
                    data.typeID = getTypeID();
                }
                data.connectionData.itemCode = meters[0].itemID;
                bde.setWorkFlowData(AID, data);
            }
            base.onBOMSet(AID, job, bom);
        }

    }

}
