﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;

namespace INTAPS.WSIS.Job.Nekemte.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_OWNERSHIP_TRANSFER, "Ownership Transfer", priority = 1)]
    public class RESOwnershipTransferNekemte : RESOwnershipTransfer, IJobRuleServerHandler
    {
        public RESOwnershipTransferNekemte(JobManagerBDE bde)
            : base(bde)
        {
        }
        protected override JobBOMItem getOwnerShipTransferItem(JobData job, Subscriber customer)
        {
            NekemteEstimationConfiguration config = bde.getConfiguration(StandardJobTypes.NEW_LINE, typeof(NekemteEstimationConfiguration)) as NekemteEstimationConfiguration;
            if (config == null)
                throw new ServerUserMessage("Estimation configuration is not set");
            JobBOMItem item = new JobBOMItem();
            
            string code = config.ownershipTransferFee.getItemCode(customer.subscriberType);
            BIZNET.iERP.TransactionItems titem=bde.bERP.GetTransactionItems(code);
            if (titem==null)
                throw new ServerConfigurationError("Please configure ownership transfer fee.");
            item.calculated = true;
            item.description = titem.Name;
            item.inSourced = true;
            item.itemID = code;
            item.quantity = 1;
            item.referenceUnitPrice = -1;
            item.tag = null;
            item.unitPrice = config.ownershipTransferFee.getValue(customer.subscriberType);
            return item;
        }
    }
}