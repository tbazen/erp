﻿namespace INTAPS.WSIS.Job.Adama.REClient
{
    partial class EstimationConfigurationAdama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.itemOther = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.itemComercial = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.itemLastMultiplier = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.itemMaterialHandling = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.itemPrivate = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelFixedMaitenance = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.itemMaintenanceFixed = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemNewLineFixed = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsSurvey = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsWaterMeter = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsPipeline = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.itemsHDP = new INTAPS.WSIS.Job.Client.ItemListPlaceHolder();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ratePrivate = new INTAPS.UI.ProgressiveRateEditor();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.rateComercial = new INTAPS.UI.ProgressiveRateEditor();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.rateOthers = new INTAPS.UI.ProgressiveRateEditor();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridDeposit = new System.Windows.Forms.DataGridView();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDeposit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.itemUnmeteredWaterFee = new BIZNET.iERP.Client.TransactionItemPlaceHolder();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratePrivate)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateComercial)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rateOthers)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pipeline Items:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(18, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "HDP Pipeline Items:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(18, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Water Meter Items:";
            // 
            // buttonOk
            // 
            this.buttonOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOk.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(406, 3);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(87, 30);
            this.buttonOk.TabIndex = 4;
            this.buttonOk.Text = "&Ok";
            this.buttonOk.UseVisualStyleBackColor = false;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(510, 3);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(82, 30);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = false;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(21, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(221, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Material Handling Cost Multiplier:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(605, 422);
            this.tabControl.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gray;
            this.tabPage1.Controls.Add(this.itemOther);
            this.tabPage1.Controls.Add(this.itemComercial);
            this.tabPage1.Controls.Add(this.itemLastMultiplier);
            this.tabPage1.Controls.Add(this.itemUnmeteredWaterFee);
            this.tabPage1.Controls.Add(this.itemMaterialHandling);
            this.tabPage1.Controls.Add(this.itemPrivate);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.labelFixedMaitenance);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.itemMaintenanceFixed);
            this.tabPage1.Controls.Add(this.itemNewLineFixed);
            this.tabPage1.Controls.Add(this.itemsSurvey);
            this.tabPage1.Controls.Add(this.itemsWaterMeter);
            this.tabPage1.Controls.Add(this.itemsPipeline);
            this.tabPage1.Controls.Add(this.itemsHDP);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(597, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // itemOther
            // 
            this.itemOther.anobject = null;
            this.itemOther.Location = new System.Drawing.Point(246, 331);
            this.itemOther.Name = "itemOther";
            this.itemOther.Size = new System.Drawing.Size(342, 20);
            this.itemOther.TabIndex = 7;
            this.itemOther.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // itemComercial
            // 
            this.itemComercial.anobject = null;
            this.itemComercial.Location = new System.Drawing.Point(246, 305);
            this.itemComercial.Name = "itemComercial";
            this.itemComercial.Size = new System.Drawing.Size(342, 20);
            this.itemComercial.TabIndex = 7;
            this.itemComercial.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // itemLastMultiplier
            // 
            this.itemLastMultiplier.anobject = null;
            this.itemLastMultiplier.Location = new System.Drawing.Point(246, 357);
            this.itemLastMultiplier.Name = "itemLastMultiplier";
            this.itemLastMultiplier.Size = new System.Drawing.Size(342, 20);
            this.itemLastMultiplier.TabIndex = 7;
            this.itemLastMultiplier.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // itemMaterialHandling
            // 
            this.itemMaterialHandling.anobject = null;
            this.itemMaterialHandling.Location = new System.Drawing.Point(246, 253);
            this.itemMaterialHandling.Name = "itemMaterialHandling";
            this.itemMaterialHandling.Size = new System.Drawing.Size(342, 20);
            this.itemMaterialHandling.TabIndex = 7;
            this.itemMaterialHandling.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // itemPrivate
            // 
            this.itemPrivate.anobject = null;
            this.itemPrivate.Location = new System.Drawing.Point(246, 279);
            this.itemPrivate.Name = "itemPrivate";
            this.itemPrivate.Size = new System.Drawing.Size(342, 20);
            this.itemPrivate.TabIndex = 7;
            this.itemPrivate.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(19, 331);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(183, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Other Service Charge Item:";
            this.label8.Click += new System.EventHandler(this.label4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(19, 305);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(215, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Comercial Service Charge Item:";
            this.label7.Click += new System.EventHandler(this.label4_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(19, 279);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(192, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Private Service Charge Item:";
            this.label6.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(19, 357);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Last Multiplier:";
            this.label5.Click += new System.EventHandler(this.label4_Click);
            // 
            // labelFixedMaitenance
            // 
            this.labelFixedMaitenance.AutoSize = true;
            this.labelFixedMaitenance.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.labelFixedMaitenance.ForeColor = System.Drawing.Color.White;
            this.labelFixedMaitenance.Location = new System.Drawing.Point(19, 175);
            this.labelFixedMaitenance.Name = "labelFixedMaitenance";
            this.labelFixedMaitenance.Size = new System.Drawing.Size(171, 17);
            this.labelFixedMaitenance.TabIndex = 0;
            this.labelFixedMaitenance.Text = "Maintenance Fixed Items:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(19, 141);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "New Line Fixed Items:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(19, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Survey Items:";
            // 
            // itemMaintenanceFixed
            // 
            this.itemMaintenanceFixed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemMaintenanceFixed.Location = new System.Drawing.Point(247, 160);
            this.itemMaintenanceFixed.Name = "itemMaintenanceFixed";
            this.itemMaintenanceFixed.Size = new System.Drawing.Size(342, 28);
            this.itemMaintenanceFixed.TabIndex = 1;
            this.itemMaintenanceFixed.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemNewLineFixed
            // 
            this.itemNewLineFixed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemNewLineFixed.Location = new System.Drawing.Point(247, 126);
            this.itemNewLineFixed.Name = "itemNewLineFixed";
            this.itemNewLineFixed.Size = new System.Drawing.Size(342, 28);
            this.itemNewLineFixed.TabIndex = 1;
            this.itemNewLineFixed.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsSurvey
            // 
            this.itemsSurvey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsSurvey.Location = new System.Drawing.Point(247, 92);
            this.itemsSurvey.Name = "itemsSurvey";
            this.itemsSurvey.Size = new System.Drawing.Size(342, 28);
            this.itemsSurvey.TabIndex = 1;
            this.itemsSurvey.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsWaterMeter
            // 
            this.itemsWaterMeter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsWaterMeter.Location = new System.Drawing.Point(246, 58);
            this.itemsWaterMeter.Name = "itemsWaterMeter";
            this.itemsWaterMeter.Size = new System.Drawing.Size(342, 28);
            this.itemsWaterMeter.TabIndex = 1;
            this.itemsWaterMeter.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsPipeline
            // 
            this.itemsPipeline.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsPipeline.Location = new System.Drawing.Point(246, 5);
            this.itemsPipeline.Name = "itemsPipeline";
            this.itemsPipeline.Size = new System.Drawing.Size(342, 28);
            this.itemsPipeline.TabIndex = 1;
            this.itemsPipeline.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // itemsHDP
            // 
            this.itemsHDP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.itemsHDP.Location = new System.Drawing.Point(246, 31);
            this.itemsHDP.Name = "itemsHDP";
            this.itemsHDP.Size = new System.Drawing.Size(342, 28);
            this.itemsHDP.TabIndex = 1;
            this.itemsHDP.Changed += new System.EventHandler(this.itemsPipeline_Changed);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ratePrivate);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(597, 383);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Private Service Charge";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ratePrivate
            // 
            this.ratePrivate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ratePrivate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ratePrivate.HorizontalEnterNavigationMode = true;
            this.ratePrivate.Location = new System.Drawing.Point(3, 3);
            this.ratePrivate.Name = "ratePrivate";
            this.ratePrivate.Size = new System.Drawing.Size(591, 377);
            this.ratePrivate.SuppessEnterKey = false;
            this.ratePrivate.TabIndex = 0;
            this.ratePrivate.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.progressiveRateCellEndEdit);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.rateComercial);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(597, 383);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Comercial Service Charge";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // rateComercial
            // 
            this.rateComercial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rateComercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateComercial.HorizontalEnterNavigationMode = true;
            this.rateComercial.Location = new System.Drawing.Point(3, 3);
            this.rateComercial.Name = "rateComercial";
            this.rateComercial.Size = new System.Drawing.Size(591, 377);
            this.rateComercial.SuppessEnterKey = false;
            this.rateComercial.TabIndex = 1;
            this.rateComercial.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.progressiveRateCellEndEdit);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.rateOthers);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(597, 383);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Others Service Charge";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // rateOthers
            // 
            this.rateOthers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rateOthers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rateOthers.HorizontalEnterNavigationMode = true;
            this.rateOthers.Location = new System.Drawing.Point(0, 0);
            this.rateOthers.Name = "rateOthers";
            this.rateOthers.Size = new System.Drawing.Size(597, 383);
            this.rateOthers.SuppessEnterKey = false;
            this.rateOthers.TabIndex = 1;
            this.rateOthers.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.progressiveRateCellEndEdit);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridDeposit);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(597, 383);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Deposit Rule";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridDeposit
            // 
            this.gridDeposit.AllowUserToAddRows = false;
            this.gridDeposit.AllowUserToDeleteRows = false;
            this.gridDeposit.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridDeposit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDeposit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colType,
            this.colDeposit});
            this.gridDeposit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDeposit.Location = new System.Drawing.Point(3, 3);
            this.gridDeposit.Name = "gridDeposit";
            this.gridDeposit.Size = new System.Drawing.Size(591, 377);
            this.gridDeposit.TabIndex = 0;
            // 
            // colType
            // 
            this.colType.HeaderText = "Meter Type";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 79;
            // 
            // colDeposit
            // 
            this.colDeposit.HeaderText = "Desposit Amount";
            this.colDeposit.Name = "colDeposit";
            this.colDeposit.Width = 103;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 422);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(605, 36);
            this.panel1.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(21, 229);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(185, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Unmetered Water Fee Item:";
            this.label11.Click += new System.EventHandler(this.label4_Click);
            // 
            // itemUnmeteredWaterFee
            // 
            this.itemUnmeteredWaterFee.anobject = null;
            this.itemUnmeteredWaterFee.Location = new System.Drawing.Point(246, 226);
            this.itemUnmeteredWaterFee.Name = "itemUnmeteredWaterFee";
            this.itemUnmeteredWaterFee.Size = new System.Drawing.Size(342, 20);
            this.itemUnmeteredWaterFee.TabIndex = 7;
            this.itemUnmeteredWaterFee.ObjectChanged += new System.EventHandler(this.itemLastMultiplier_ObjectChanged);
            // 
            // EstimationConfigurationAdama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 458);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Name = "EstimationConfigurationAdama";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estimation Configuration";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ratePrivate)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rateComercial)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rateOthers)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDeposit)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Client.ItemListPlaceHolder itemsPipeline;
        private Client.ItemListPlaceHolder itemsHDP;
        private Client.ItemListPlaceHolder itemsWaterMeter;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel1;
        private INTAPS.UI.ProgressiveRateEditor ratePrivate;
        private INTAPS.UI.ProgressiveRateEditor rateComercial;
        private INTAPS.UI.ProgressiveRateEditor rateOthers;
        private System.Windows.Forms.Label label5;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemOther;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemComercial;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemPrivate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelFixedMaitenance;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private Client.ItemListPlaceHolder itemMaintenanceFixed;
        private Client.ItemListPlaceHolder itemNewLineFixed;
        private Client.ItemListPlaceHolder itemsSurvey;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemLastMultiplier;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemMaterialHandling;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView gridDeposit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDeposit;
        private BIZNET.iERP.Client.TransactionItemPlaceHolder itemUnmeteredWaterFee;
        private System.Windows.Forms.Label label11;
    }
}