﻿using DocumentFormat.OpenXml.Packaging;
using Ap = DocumentFormat.OpenXml.ExtendedProperties;
using Vt = DocumentFormat.OpenXml.VariantTypes;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using Ds = DocumentFormat.OpenXml.CustomXmlDataProperties;
using M = DocumentFormat.OpenXml.Math;
using Ovml = DocumentFormat.OpenXml.Vml.Office;
using V = DocumentFormat.OpenXml.Vml;
using W15 = DocumentFormat.OpenXml.Office2013.Word;
using A = DocumentFormat.OpenXml.Drawing;
using Thm15 = DocumentFormat.OpenXml.Office2013.Theme;

namespace INTAPS.WSIS.Job.Bishoftu.REClient
{
    public class GeneratedClass
    {
        // Creates a WordprocessingDocument.
        public void CreatePackage(string filePath,ContractGeneratorParamters pars)
        {
            using (WordprocessingDocument package = WordprocessingDocument.Create(filePath, WordprocessingDocumentType.Document))
            {
                CreateParts(package,pars);
            }
        }

        // Adds child parts and generates content of the specified part.
        private void CreateParts(WordprocessingDocument document, ContractGeneratorParamters pars)
        {
            ExtendedFilePropertiesPart extendedFilePropertiesPart1 = document.AddNewPart<ExtendedFilePropertiesPart>("rId3");
            GenerateExtendedFilePropertiesPart1Content(extendedFilePropertiesPart1);

            MainDocumentPart mainDocumentPart1 = document.AddMainDocumentPart();
            GenerateMainDocumentPart1Content(mainDocumentPart1,pars);

            FontTablePart fontTablePart1 = mainDocumentPart1.AddNewPart<FontTablePart>("rId8");
            GenerateFontTablePart1Content(fontTablePart1);

            StyleDefinitionsPart styleDefinitionsPart1 = mainDocumentPart1.AddNewPart<StyleDefinitionsPart>("rId3");
            GenerateStyleDefinitionsPart1Content(styleDefinitionsPart1);

            EndnotesPart endnotesPart1 = mainDocumentPart1.AddNewPart<EndnotesPart>("rId7");
            GenerateEndnotesPart1Content(endnotesPart1);

            NumberingDefinitionsPart numberingDefinitionsPart1 = mainDocumentPart1.AddNewPart<NumberingDefinitionsPart>("rId2");
            GenerateNumberingDefinitionsPart1Content(numberingDefinitionsPart1);

            CustomXmlPart customXmlPart1 = mainDocumentPart1.AddNewPart<CustomXmlPart>("application/xml", "rId1");
            GenerateCustomXmlPart1Content(customXmlPart1);

            CustomXmlPropertiesPart customXmlPropertiesPart1 = customXmlPart1.AddNewPart<CustomXmlPropertiesPart>("rId1");
            GenerateCustomXmlPropertiesPart1Content(customXmlPropertiesPart1);

            FootnotesPart footnotesPart1 = mainDocumentPart1.AddNewPart<FootnotesPart>("rId6");
            GenerateFootnotesPart1Content(footnotesPart1);

            WebSettingsPart webSettingsPart1 = mainDocumentPart1.AddNewPart<WebSettingsPart>("rId5");
            GenerateWebSettingsPart1Content(webSettingsPart1);

            DocumentSettingsPart documentSettingsPart1 = mainDocumentPart1.AddNewPart<DocumentSettingsPart>("rId4");
            GenerateDocumentSettingsPart1Content(documentSettingsPart1);

            ThemePart themePart1 = mainDocumentPart1.AddNewPart<ThemePart>("rId9");
            GenerateThemePart1Content(themePart1);

            SetPackageProperties(document);
        }

        // Generates content of extendedFilePropertiesPart1.
        private void GenerateExtendedFilePropertiesPart1Content(ExtendedFilePropertiesPart extendedFilePropertiesPart1)
        {
            Ap.Properties properties1 = new Ap.Properties();
            properties1.AddNamespaceDeclaration("vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
            Ap.Template template1 = new Ap.Template();
            template1.Text = "Normal.dotm";
            Ap.TotalTime totalTime1 = new Ap.TotalTime();
            totalTime1.Text = "2";
            Ap.Pages pages1 = new Ap.Pages();
            pages1.Text = "7";
            Ap.Words words1 = new Ap.Words();
            words1.Text = "2361";
            Ap.Characters characters1 = new Ap.Characters();
            characters1.Text = "13463";
            Ap.Application application1 = new Ap.Application();
            application1.Text = "Microsoft Office Word";
            Ap.DocumentSecurity documentSecurity1 = new Ap.DocumentSecurity();
            documentSecurity1.Text = "0";
            Ap.Lines lines1 = new Ap.Lines();
            lines1.Text = "112";
            Ap.Paragraphs paragraphs1 = new Ap.Paragraphs();
            paragraphs1.Text = "31";
            Ap.ScaleCrop scaleCrop1 = new Ap.ScaleCrop();
            scaleCrop1.Text = "false";

            Ap.HeadingPairs headingPairs1 = new Ap.HeadingPairs();

            Vt.VTVector vTVector1 = new Vt.VTVector() { BaseType = Vt.VectorBaseValues.Variant, Size = (UInt32Value)2U };

            Vt.Variant variant1 = new Vt.Variant();
            Vt.VTLPSTR vTLPSTR1 = new Vt.VTLPSTR();
            vTLPSTR1.Text = "Title";

            variant1.Append(vTLPSTR1);

            Vt.Variant variant2 = new Vt.Variant();
            Vt.VTInt32 vTInt321 = new Vt.VTInt32();
            vTInt321.Text = "1";

            variant2.Append(vTInt321);

            vTVector1.Append(variant1);
            vTVector1.Append(variant2);

            headingPairs1.Append(vTVector1);

            Ap.TitlesOfParts titlesOfParts1 = new Ap.TitlesOfParts();

            Vt.VTVector vTVector2 = new Vt.VTVector() { BaseType = Vt.VectorBaseValues.Lpstr, Size = (UInt32Value)1U };
            Vt.VTLPSTR vTLPSTR2 = new Vt.VTLPSTR();
            vTLPSTR2.Text = "";

            vTVector2.Append(vTLPSTR2);

            titlesOfParts1.Append(vTVector2);
            Ap.Company company1 = new Ap.Company();
            company1.Text = "home";
            Ap.LinksUpToDate linksUpToDate1 = new Ap.LinksUpToDate();
            linksUpToDate1.Text = "false";
            Ap.CharactersWithSpaces charactersWithSpaces1 = new Ap.CharactersWithSpaces();
            charactersWithSpaces1.Text = "15793";
            Ap.SharedDocument sharedDocument1 = new Ap.SharedDocument();
            sharedDocument1.Text = "false";
            Ap.HyperlinksChanged hyperlinksChanged1 = new Ap.HyperlinksChanged();
            hyperlinksChanged1.Text = "false";
            Ap.ApplicationVersion applicationVersion1 = new Ap.ApplicationVersion();
            applicationVersion1.Text = "15.0000";

            properties1.Append(template1);
            properties1.Append(totalTime1);
            properties1.Append(pages1);
            properties1.Append(words1);
            properties1.Append(characters1);
            properties1.Append(application1);
            properties1.Append(documentSecurity1);
            properties1.Append(lines1);
            properties1.Append(paragraphs1);
            properties1.Append(scaleCrop1);
            properties1.Append(headingPairs1);
            properties1.Append(titlesOfParts1);
            properties1.Append(company1);
            properties1.Append(linksUpToDate1);
            properties1.Append(charactersWithSpaces1);
            properties1.Append(sharedDocument1);
            properties1.Append(hyperlinksChanged1);
            properties1.Append(applicationVersion1);

            extendedFilePropertiesPart1.Properties = properties1;
        }

        // Generates content of mainDocumentPart1.
        private void GenerateMainDocumentPart1Content(MainDocumentPart mainDocumentPart1, ContractGeneratorParamters pars)
        {
            Document document1 = new Document() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15 wp14" } };
            document1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
            document1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            document1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            document1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            document1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            document1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            document1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
            document1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            document1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            document1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            document1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            document1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");
            document1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
            document1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
            document1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            document1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

            Body body1 = new Body();

            Paragraph paragraph1 = new Paragraph() { RsidParagraphMarkRevision = "006B07D9", RsidParagraphAddition = "007322D6", RsidParagraphProperties = "00F93210", RsidRunAdditionDefault = "00F93210" };

            ParagraphProperties paragraphProperties1 = new ParagraphProperties();
            Justification justification1 = new Justification() { Val = JustificationValues.Center };

            ParagraphMarkRunProperties paragraphMarkRunProperties1 = new ParagraphMarkRunProperties();
            Bold bold1 = new Bold();

            paragraphMarkRunProperties1.Append(bold1);

            paragraphProperties1.Append(justification1);
            paragraphProperties1.Append(paragraphMarkRunProperties1);

            Run run1 = new Run() { RsidRunProperties = "006B07D9" };

            RunProperties runProperties1 = new RunProperties();
            Bold bold2 = new Bold();

            runProperties1.Append(bold2);
            Text text1 = new Text();
            text1.Text = "MOOTUMMAA NAANNOO OROMIYAATTII";

            run1.Append(runProperties1);
            run1.Append(text1);

            paragraph1.Append(paragraphProperties1);
            paragraph1.Append(run1);

            Paragraph paragraph2 = new Paragraph() { RsidParagraphMarkRevision = "006B07D9", RsidParagraphAddition = "00F93210", RsidParagraphProperties = "00F93210", RsidRunAdditionDefault = "00F93210" };

            ParagraphProperties paragraphProperties2 = new ParagraphProperties();
            Justification justification2 = new Justification() { Val = JustificationValues.Center };

            ParagraphMarkRunProperties paragraphMarkRunProperties2 = new ParagraphMarkRunProperties();
            Bold bold3 = new Bold();

            paragraphMarkRunProperties2.Append(bold3);

            paragraphProperties2.Append(justification2);
            paragraphProperties2.Append(paragraphMarkRunProperties2);

            Run run2 = new Run() { RsidRunProperties = "006B07D9" };

            RunProperties runProperties2 = new RunProperties();
            Bold bold4 = new Bold();

            runProperties2.Append(bold4);
            Text text2 = new Text();
            text2.Text = "BIIROO QABEENYA BISHAANI";

            run2.Append(runProperties2);
            run2.Append(text2);

            paragraph2.Append(paragraphProperties2);
            paragraph2.Append(run2);

            Paragraph paragraph3 = new Paragraph() { RsidParagraphMarkRevision = "006B07D9", RsidParagraphAddition = "00F93210", RsidParagraphProperties = "00F93210", RsidRunAdditionDefault = "00C85AB0" };

            ParagraphProperties paragraphProperties3 = new ParagraphProperties();
            Justification justification3 = new Justification() { Val = JustificationValues.Center };

            ParagraphMarkRunProperties paragraphMarkRunProperties3 = new ParagraphMarkRunProperties();
            RunFonts runFonts1 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold5 = new Bold();

            paragraphMarkRunProperties3.Append(runFonts1);
            paragraphMarkRunProperties3.Append(bold5);

            paragraphProperties3.Append(justification3);
            paragraphProperties3.Append(paragraphMarkRunProperties3);

            Run run3 = new Run();

            RunProperties runProperties3 = new RunProperties();
            RunFonts runFonts2 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold6 = new Bold();

            runProperties3.Append(runFonts2);
            runProperties3.Append(bold6);
            Text text3 = new Text();
            text3.Text = "WALLIIGALTEE DHIYEESSAA BS";

            run3.Append(runProperties3);
            run3.Append(text3);

            Run run4 = new Run() { RsidRunProperties = "006B07D9", RsidRunAddition = "00F93210" };

            RunProperties runProperties4 = new RunProperties();
            RunFonts runFonts3 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold7 = new Bold();

            runProperties4.Append(runFonts3);
            runProperties4.Append(bold7);
            Text text4 = new Text();
            text4.Text = "HAAN DHUGAATII";

            run4.Append(runProperties4);
            run4.Append(text4);

            paragraph3.Append(paragraphProperties3);
            paragraph3.Append(run3);
            paragraph3.Append(run4);

            Paragraph paragraph4 = new Paragraph() { RsidParagraphAddition = "00F93210", RsidRunAdditionDefault = "00F93210" };

            ParagraphProperties paragraphProperties4 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties4 = new ParagraphMarkRunProperties();
            RunFonts runFonts4 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties4.Append(runFonts4);

            SectionProperties sectionProperties1 = new SectionProperties() { RsidR = "00F93210", RsidSect = "00A846BA" };
            PageSize pageSize1 = new PageSize() { Width = (UInt32Value)12240U, Height = (UInt32Value)15840U };
            PageMargin pageMargin1 = new PageMargin() { Top = 450, Right = (UInt32Value)1440U, Bottom = 1440, Left = (UInt32Value)1440U, Header = (UInt32Value)720U, Footer = (UInt32Value)720U, Gutter = (UInt32Value)0U };
            Columns columns1 = new Columns() { Space = "720" };
            DocGrid docGrid1 = new DocGrid() { LinePitch = 360 };

            sectionProperties1.Append(pageSize1);
            sectionProperties1.Append(pageMargin1);
            sectionProperties1.Append(columns1);
            sectionProperties1.Append(docGrid1);

            paragraphProperties4.Append(paragraphMarkRunProperties4);
            paragraphProperties4.Append(sectionProperties1);

            paragraph4.Append(paragraphProperties4);

            Paragraph paragraph5 = new Paragraph() { RsidParagraphAddition = "00F93210", RsidRunAdditionDefault = "00F93210" };

            ParagraphProperties paragraphProperties5 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties5 = new ParagraphMarkRunProperties();
            RunFonts runFonts5 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties5.Append(runFonts5);

            paragraphProperties5.Append(paragraphMarkRunProperties5);

            Run run5 = new Run();

            RunProperties runProperties5 = new RunProperties();
            RunFonts runFonts6 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties5.Append(runFonts6);
            LastRenderedPageBreak lastRenderedPageBreak1 = new LastRenderedPageBreak();
            Text text5 = new Text();
            text5.Text = "Waliigalteen kun gu";

            run5.Append(runProperties5);
            run5.Append(lastRenderedPageBreak1);
            run5.Append(text5);

            Run run6 = new Run() { RsidRunAddition = "000764D1" };

            RunProperties runProperties6 = new RunProperties();
            RunFonts runFonts7 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties6.Append(runFonts7);
            Text text6 = new Text();
            text6.Text = "yyaa";

            run6.Append(runProperties6);
            run6.Append(text6);

            Run run7 = new Run() { RsidRunAddition = "00E1168B" };

            RunProperties runProperties7 = new RunProperties();
            RunFonts runFonts8 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties7.Append(runFonts8);
            Text text7 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text7.Text = " ";

            run7.Append(runProperties7);
            run7.Append(text7);

            Run run8 = new Run() { RsidRunProperties = "00E1168B", RsidRunAddition = "00E1168B" };

            RunProperties runProperties8 = new RunProperties();
            RunFonts runFonts9 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold8 = new Bold();
            Underline underline1 = new Underline() { Val = UnderlineValues.Single };

            runProperties8.Append(runFonts9);
            runProperties8.Append(bold8);
            runProperties8.Append(underline1);
            Text text8 = new Text();
            text8.Text = pars.date;

            run8.Append(runProperties8);
            run8.Append(text8);

            Run run9 = new Run() { RsidRunAddition = "00E1168B" };

            RunProperties runProperties9 = new RunProperties();
            RunFonts runFonts10 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties9.Append(runFonts10);
            Text text9 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text9.Text = " ";

            run9.Append(runProperties9);
            run9.Append(text9);

            Run run10 = new Run() { RsidRunAddition = "000764D1" };

            RunProperties runProperties10 = new RunProperties();
            RunFonts runFonts11 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties10.Append(runFonts11);
            Text text10 = new Text();
            text10.Text = "bara";

            run10.Append(runProperties10);
            run10.Append(text10);

            Run run11 = new Run() { RsidRunAddition = "00E1168B" };

            RunProperties runProperties11 = new RunProperties();
            RunFonts runFonts12 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties11.Append(runFonts12);
            Text text11 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text11.Text = " ";

            run11.Append(runProperties11);
            run11.Append(text11);

            Run run12 = new Run() { RsidRunProperties = "00E1168B", RsidRunAddition = "00E1168B" };

            RunProperties runProperties12 = new RunProperties();
            RunFonts runFonts13 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold9 = new Bold();
            Underline underline2 = new Underline() { Val = UnderlineValues.Single };

            runProperties12.Append(runFonts13);
            runProperties12.Append(bold9);
            runProperties12.Append(underline2);
            Text text12 = new Text();
            text12.Text = pars.year;

            run12.Append(runProperties12);
            run12.Append(text12);

            Run run13 = new Run() { RsidRunAddition = "00E1168B" };

            RunProperties runProperties13 = new RunProperties();
            RunFonts runFonts14 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties13.Append(runFonts14);
            Text text13 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text13.Text = " ";

            run13.Append(runProperties13);
            run13.Append(text13);

            Run run14 = new Run();

            RunProperties runProperties14 = new RunProperties();
            RunFonts runFonts15 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties14.Append(runFonts15);
            Text text14 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text14.Text = "Biiroo Qabeenya Bishaanii, kanaan booda Biiroo Jedhamee kan waamamu fi kanaan Booda maammila jedhamee kan wamammu Obbo/Adde/Durbee ";

            run14.Append(runProperties14);
            run14.Append(text14);
            BookmarkStart bookmarkStart1 = new BookmarkStart() { Name = "_GoBack", Id = "0" };

            Run run15 = new Run() { RsidRunProperties = "00E1168B", RsidRunAddition = "00E1168B" };

            RunProperties runProperties15 = new RunProperties();
            RunFonts runFonts16 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold10 = new Bold();
            Underline underline3 = new Underline() { Val = UnderlineValues.Single };

            runProperties15.Append(runFonts16);
            runProperties15.Append(bold10);
            runProperties15.Append(underline3);
            Text text15 = new Text();
            text15.Text = pars.name;

            run15.Append(runProperties15);
            run15.Append(text15);

            Run run16 = new Run() { RsidRunAddition = "00E1168B" };

            RunProperties runProperties16 = new RunProperties();
            RunFonts runFonts17 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties16.Append(runFonts17);
            Text text16 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text16.Text = " ";

            run16.Append(runProperties16);
            run16.Append(text16);
            BookmarkEnd bookmarkEnd1 = new BookmarkEnd() { Id = "0" };

            Run run17 = new Run() { RsidRunAddition = "000B7150" };

            RunProperties runProperties17 = new RunProperties();
            RunFonts runFonts18 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties17.Append(runFonts18);
            Text text17 = new Text();
            text17.Text = "Gidutti tajaajila Bishaan Dhugaatii kennuuf maammilchis itti fayyadamuu fi gatii bishaani kaffaluuf waligaltee godhameedha";

            run17.Append(runProperties17);
            run17.Append(text17);

            Run run18 = new Run() { RsidRunAddition = "00DE78CA" };

            RunProperties runProperties18 = new RunProperties();
            RunFonts runFonts19 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties18.Append(runFonts19);
            Text text18 = new Text();
            text18.Text = ".";

            run18.Append(runProperties18);
            run18.Append(text18);

            paragraph5.Append(paragraphProperties5);
            paragraph5.Append(run5);
            paragraph5.Append(run6);
            paragraph5.Append(run7);
            paragraph5.Append(run8);
            paragraph5.Append(run9);
            paragraph5.Append(run10);
            paragraph5.Append(run11);
            paragraph5.Append(run12);
            paragraph5.Append(run13);
            paragraph5.Append(run14);
            paragraph5.Append(bookmarkStart1);
            paragraph5.Append(run15);
            paragraph5.Append(run16);
            paragraph5.Append(bookmarkEnd1);
            paragraph5.Append(run17);
            paragraph5.Append(run18);

            Paragraph paragraph6 = new Paragraph() { RsidParagraphAddition = "00DE78CA", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00DE78CA" };

            ParagraphProperties paragraphProperties6 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId1 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties1 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference1 = new NumberingLevelReference() { Val = 0 };
            NumberingId numberingId1 = new NumberingId() { Val = 1 };

            numberingProperties1.Append(numberingLevelReference1);
            numberingProperties1.Append(numberingId1);

            ParagraphMarkRunProperties paragraphMarkRunProperties6 = new ParagraphMarkRunProperties();
            RunFonts runFonts20 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties6.Append(runFonts20);

            paragraphProperties6.Append(paragraphStyleId1);
            paragraphProperties6.Append(numberingProperties1);
            paragraphProperties6.Append(paragraphMarkRunProperties6);

            Run run19 = new Run();

            RunProperties runProperties19 = new RunProperties();
            RunFonts runFonts21 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties19.Append(runFonts21);
            Text text19 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text19.Text = "Hiikaa:- ";

            run19.Append(runProperties19);
            run19.Append(text19);

            paragraph6.Append(paragraphProperties6);
            paragraph6.Append(run19);

            Paragraph paragraph7 = new Paragraph() { RsidParagraphAddition = "00DE78CA", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00DE78CA" };

            ParagraphProperties paragraphProperties7 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId2 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties2 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference2 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId2 = new NumberingId() { Val = 1 };

            numberingProperties2.Append(numberingLevelReference2);
            numberingProperties2.Append(numberingId2);

            ParagraphMarkRunProperties paragraphMarkRunProperties7 = new ParagraphMarkRunProperties();
            RunFonts runFonts22 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties7.Append(runFonts22);

            paragraphProperties7.Append(paragraphStyleId2);
            paragraphProperties7.Append(numberingProperties2);
            paragraphProperties7.Append(paragraphMarkRunProperties7);

            Run run20 = new Run();

            RunProperties runProperties20 = new RunProperties();
            RunFonts runFonts23 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties20.Append(runFonts23);
            Text text20 = new Text();
            text20.Text = "“Waliigaltee” jechuun maamili bishaan dhugaatii argachuuf Biiroo wajiin kan armaan gaditi waligalee jechudha.";

            run20.Append(runProperties20);
            run20.Append(text20);

            paragraph7.Append(paragraphProperties7);
            paragraph7.Append(run20);

            Paragraph paragraph8 = new Paragraph() { RsidParagraphAddition = "00E85F43", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "006B07D9" };

            ParagraphProperties paragraphProperties8 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId3 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties3 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference3 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId3 = new NumberingId() { Val = 1 };

            numberingProperties3.Append(numberingLevelReference3);
            numberingProperties3.Append(numberingId3);

            ParagraphMarkRunProperties paragraphMarkRunProperties8 = new ParagraphMarkRunProperties();
            RunFonts runFonts24 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties8.Append(runFonts24);

            paragraphProperties8.Append(paragraphStyleId3);
            paragraphProperties8.Append(numberingProperties3);
            paragraphProperties8.Append(paragraphMarkRunProperties8);

            Run run21 = new Run();

            RunProperties runProperties21 = new RunProperties();
            RunFonts runFonts25 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties21.Append(runFonts25);
            Text text21 = new Text();
            text21.Text = "“Biiroo” jechuun Biiroo Qabeenya Bishaan Oromiyaa jechudha.";

            run21.Append(runProperties21);
            run21.Append(text21);

            paragraph8.Append(paragraphProperties8);
            paragraph8.Append(run21);

            Paragraph paragraph9 = new Paragraph() { RsidParagraphAddition = "00E85F43", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00E85F43" };

            ParagraphProperties paragraphProperties9 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId4 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties4 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference4 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId4 = new NumberingId() { Val = 1 };

            numberingProperties4.Append(numberingLevelReference4);
            numberingProperties4.Append(numberingId4);

            ParagraphMarkRunProperties paragraphMarkRunProperties9 = new ParagraphMarkRunProperties();
            RunFonts runFonts26 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties9.Append(runFonts26);

            paragraphProperties9.Append(paragraphStyleId4);
            paragraphProperties9.Append(numberingProperties4);
            paragraphProperties9.Append(paragraphMarkRunProperties9);

            Run run22 = new Run();

            RunProperties runProperties22 = new RunProperties();
            RunFonts runFonts27 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties22.Append(runFonts27);
            Text text22 = new Text();
            text22.Text = "“Maammila” jechuun tajaajila Bishaan dhugaatii argachuf Biiroo wajjin waliigaltee kan godhe nama dhunfaa, jaarmiyaa, dhaabbata daldala jechuudha.";

            run22.Append(runProperties22);
            run22.Append(text22);

            paragraph9.Append(paragraphProperties9);
            paragraph9.Append(run22);

            Paragraph paragraph10 = new Paragraph() { RsidParagraphAddition = "00E85F43", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00E85F43" };

            ParagraphProperties paragraphProperties10 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId5 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties5 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference5 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId5 = new NumberingId() { Val = 1 };

            numberingProperties5.Append(numberingLevelReference5);
            numberingProperties5.Append(numberingId5);

            ParagraphMarkRunProperties paragraphMarkRunProperties10 = new ParagraphMarkRunProperties();
            RunFonts runFonts28 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties10.Append(runFonts28);

            paragraphProperties10.Append(paragraphStyleId5);
            paragraphProperties10.Append(numberingProperties5);
            paragraphProperties10.Append(paragraphMarkRunProperties10);

            Run run23 = new Run();

            RunProperties runProperties23 = new RunProperties();
            RunFonts runFonts29 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties23.Append(runFonts29);
            Text text23 = new Text();
            text23.Text = "“Sarara Bishaanii” jechuun bishaan dhugaatii qulquluu raabsuf Biiroodhaan kan diririfame ykn akka dirirfamu kan hayyamame, ujjumo fi qabsiftu irraa jiru hunda kan walitiqabe jechuudha.";

            run23.Append(runProperties23);
            run23.Append(text23);

            paragraph10.Append(paragraphProperties10);
            paragraph10.Append(run23);

            Paragraph paragraph11 = new Paragraph() { RsidParagraphAddition = "00E85F43", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00E85F43" };

            ParagraphProperties paragraphProperties11 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId6 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties6 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference6 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId6 = new NumberingId() { Val = 1 };

            numberingProperties6.Append(numberingLevelReference6);
            numberingProperties6.Append(numberingId6);

            ParagraphMarkRunProperties paragraphMarkRunProperties11 = new ParagraphMarkRunProperties();
            RunFonts runFonts30 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties11.Append(runFonts30);

            paragraphProperties11.Append(paragraphStyleId6);
            paragraphProperties11.Append(numberingProperties6);
            paragraphProperties11.Append(paragraphMarkRunProperties11);

            Run run24 = new Run();

            RunProperties runProperties24 = new RunProperties();
            RunFonts runFonts31 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties24.Append(runFonts31);
            Text text24 = new Text();
            text24.Text = "“Lakkoftu” jechuun meesha baayi’ina bushaani Biiroon maamilaaf kennu kan lakka’udha.";

            run24.Append(runProperties24);
            run24.Append(text24);

            paragraph11.Append(paragraphProperties11);
            paragraph11.Append(run24);

            Paragraph paragraph12 = new Paragraph() { RsidParagraphAddition = "00E85F43", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00E85F43" };

            ParagraphProperties paragraphProperties12 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId7 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties7 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference7 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId7 = new NumberingId() { Val = 1 };

            numberingProperties7.Append(numberingLevelReference7);
            numberingProperties7.Append(numberingId7);

            ParagraphMarkRunProperties paragraphMarkRunProperties12 = new ParagraphMarkRunProperties();
            RunFonts runFonts32 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties12.Append(runFonts32);

            paragraphProperties12.Append(paragraphStyleId7);
            paragraphProperties12.Append(numberingProperties7);
            paragraphProperties12.Append(paragraphMarkRunProperties12);

            Run run25 = new Run();

            RunProperties runProperties25 = new RunProperties();
            RunFonts runFonts33 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties25.Append(runFonts33);
            Text text25 = new Text();
            text25.Text = "“Sarara guddaa” jechuun gara magaalaa fi gandoota adda addaa bishaan raabsuf sarara bishaanii Biiroodhaan dirirfame jechuudha.";

            run25.Append(runProperties25);
            run25.Append(text25);

            paragraph12.Append(paragraphProperties12);
            paragraph12.Append(run25);

            Paragraph paragraph13 = new Paragraph() { RsidParagraphAddition = "00E85F43", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "00E85F43" };

            ParagraphProperties paragraphProperties13 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId8 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties8 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference8 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId8 = new NumberingId() { Val = 1 };

            numberingProperties8.Append(numberingLevelReference8);
            numberingProperties8.Append(numberingId8);

            ParagraphMarkRunProperties paragraphMarkRunProperties13 = new ParagraphMarkRunProperties();
            RunFonts runFonts34 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties13.Append(runFonts34);

            paragraphProperties13.Append(paragraphStyleId8);
            paragraphProperties13.Append(numberingProperties8);
            paragraphProperties13.Append(paragraphMarkRunProperties13);

            Run run26 = new Run();

            RunProperties runProperties26 = new RunProperties();
            RunFonts runFonts35 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties26.Append(runFonts35);
            Text text26 = new Text();
            text26.Text = "“Sarara Lakkoftu dura jiru” jechun sarara guddaa Biiroo irra ykn sarari maamilaa iddo itti fufamee jalqabee hanga dhuma lakkooftu kan dirirfameedha";

            run26.Append(runProperties26);
            run26.Append(text26);

            Run run27 = new Run() { RsidRunAddition = "00D970BD" };

            RunProperties runProperties27 = new RunProperties();
            RunFonts runFonts36 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties27.Append(runFonts36);
            Text text27 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text27.Text = ". ";

            run27.Append(runProperties27);
            run27.Append(text27);

            paragraph13.Append(paragraphProperties13);
            paragraph13.Append(run26);
            paragraph13.Append(run27);

            Paragraph paragraph14 = new Paragraph() { RsidParagraphAddition = "002012F9", RsidParagraphProperties = "00DE78CA", RsidRunAdditionDefault = "002012F9" };

            ParagraphProperties paragraphProperties14 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId9 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties9 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference9 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId9 = new NumberingId() { Val = 1 };

            numberingProperties9.Append(numberingLevelReference9);
            numberingProperties9.Append(numberingId9);

            ParagraphMarkRunProperties paragraphMarkRunProperties14 = new ParagraphMarkRunProperties();
            RunFonts runFonts37 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties14.Append(runFonts37);

            paragraphProperties14.Append(paragraphStyleId9);
            paragraphProperties14.Append(numberingProperties9);
            paragraphProperties14.Append(paragraphMarkRunProperties14);

            Run run28 = new Run();

            RunProperties runProperties28 = new RunProperties();
            RunFonts runFonts38 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties28.Append(runFonts38);
            Text text28 = new Text();
            text28.Text = "Sarara Lakkoftu booda Jiru” jechuun sarara dhuma lakkooftu irra jalqabee maamilaaf, dirirfame jechuudha";

            run28.Append(runProperties28);
            run28.Append(text28);

            paragraph14.Append(paragraphProperties14);
            paragraph14.Append(run28);

            Paragraph paragraph15 = new Paragraph() { RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "008E6B7F" };

            ParagraphProperties paragraphProperties15 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties15 = new ParagraphMarkRunProperties();
            RunFonts runFonts39 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties15.Append(runFonts39);

            paragraphProperties15.Append(paragraphMarkRunProperties15);

            Run run29 = new Run();

            RunProperties runProperties29 = new RunProperties();
            RunFonts runFonts40 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties29.Append(runFonts40);
            Text text29 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text29.Text = "2.5. Biiroon barbaachisaa ta’e yoo arge sarara lakkooftuun dura jiru fi baasi maamila(Maamilotaatiin) dirirfame kana dura ";

            run29.Append(runProperties29);
            run29.Append(text29);

            Run run30 = new Run();

            RunProperties runProperties30 = new RunProperties();
            RunFonts runFonts41 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties30.Append(runFonts41);
            LastRenderedPageBreak lastRenderedPageBreak2 = new LastRenderedPageBreak();
            Text text30 = new Text();
            text30.Text = "jiru irraa maamila (Maamilota) harawaaf bishaan kennuf mirga ni qaba. Haata’u malee maamila (Maamilota) durse (dursanii) baasi isaan(isaanitin) sarara galfateef (galfatanif) gahe isaa (isaani) akka kaffaluu(kaffalan) ni godhama.";

            run30.Append(runProperties30);
            run30.Append(lastRenderedPageBreak2);
            run30.Append(text30);

            paragraph15.Append(paragraphProperties15);
            paragraph15.Append(run29);
            paragraph15.Append(run30);

            Paragraph paragraph16 = new Paragraph() { RsidParagraphAddition = "008E6B7F", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "008E6B7F" };

            ParagraphProperties paragraphProperties16 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties16 = new ParagraphMarkRunProperties();
            RunFonts runFonts42 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties16.Append(runFonts42);

            paragraphProperties16.Append(paragraphMarkRunProperties16);

            Run run31 = new Run();

            RunProperties runProperties31 = new RunProperties();
            RunFonts runFonts43 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties31.Append(runFonts43);
            Text text31 = new Text();
            text31.Text = "2.6. Maamil";

            run31.Append(runProperties31);
            run31.Append(text31);

            Run run32 = new Run() { RsidRunAddition = "0028632F" };

            RunProperties runProperties32 = new RunProperties();
            RunFonts runFonts44 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties32.Append(runFonts44);
            Text text32 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text32.Text = "a dabalata bishaanii argachuuf ";

            run32.Append(runProperties32);
            run32.Append(text32);

            Run run33 = new Run();

            RunProperties runProperties33 = new RunProperties();
            RunFonts runFonts45 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties33.Append(runFonts45);
            Text text33 = new Text();
            text33.Text = "iyyate yoo biiroon bishaan gahaa ta’ee jira jedhe amane maamilii baasii fi waanta barbaachisaa ta’e hunda eerga gutee bodaa biiroon dabalaa bishaanii ni keennaaf.";

            run33.Append(runProperties33);
            run33.Append(text33);

            paragraph16.Append(paragraphProperties16);
            paragraph16.Append(run31);
            paragraph16.Append(run32);
            paragraph16.Append(run33);

            Paragraph paragraph17 = new Paragraph() { RsidParagraphAddition = "008E6B7F", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "008E6B7F" };

            ParagraphProperties paragraphProperties17 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties17 = new ParagraphMarkRunProperties();
            RunFonts runFonts46 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties17.Append(runFonts46);

            paragraphProperties17.Append(paragraphMarkRunProperties17);

            Run run34 = new Run();

            RunProperties runProperties34 = new RunProperties();
            RunFonts runFonts47 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties34.Append(runFonts47);
            Text text34 = new Text();
            text34.Text = "2.7. Biiroon dhyeessa Bishaanii foyyeesuuf sarara guddaa irratti jijjirraa yeroo godhe sababa kanaan jijjirraan sarara maamilichaa yoo barbaachise maamilichi baasii isaa ni danda’a.";

            run34.Append(runProperties34);
            run34.Append(text34);

            paragraph17.Append(paragraphProperties17);
            paragraph17.Append(run34);

            Paragraph paragraph18 = new Paragraph() { RsidParagraphAddition = "008E6B7F", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "0028632F" };

            ParagraphProperties paragraphProperties18 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties18 = new ParagraphMarkRunProperties();
            RunFonts runFonts48 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties18.Append(runFonts48);

            paragraphProperties18.Append(paragraphMarkRunProperties18);

            Run run35 = new Run();

            RunProperties runProperties35 = new RunProperties();
            RunFonts runFonts49 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties35.Append(runFonts49);
            Text text35 = new Text();
            text35.Text = "2.8. Maamilli tokko sa";

            run35.Append(runProperties35);
            run35.Append(text35);

            Run run36 = new Run() { RsidRunAddition = "008E6B7F" };

            RunProperties runProperties36 = new RunProperties();
            RunFonts runFonts50 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties36.Append(runFonts50);
            Text text36 = new Text();
            text36.Text = "rara lakkooftu dura jiru ykn sarara guddaa irratti midhaa yoo geesise ykn bishaan itti yoo fayadame baasi fi kisaaraa biiroo irran ga";

            run36.Append(runProperties36);
            run36.Append(text36);

            Run run37 = new Run();

            RunProperties runProperties37 = new RunProperties();
            RunFonts runFonts51 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties37.Append(runFonts51);
            Text text37 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text37.Text = "heef mahaandisa ykn teeknishaani biirootin kan tilmaamame ni kaffala kaffaluf fedhii qabaachu baanan tajaajiili bishaan irraa muramee yakka raawateef seeraan ni gaafatama. ";

            run37.Append(runProperties37);
            run37.Append(text37);

            paragraph18.Append(paragraphProperties18);
            paragraph18.Append(run35);
            paragraph18.Append(run36);
            paragraph18.Append(run37);

            Paragraph paragraph19 = new Paragraph() { RsidParagraphMarkRevision = "000B426F", RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00FD42A3" };

            ParagraphProperties paragraphProperties19 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties19 = new ParagraphMarkRunProperties();
            RunFonts runFonts52 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold11 = new Bold();

            paragraphMarkRunProperties19.Append(runFonts52);
            paragraphMarkRunProperties19.Append(bold11);

            paragraphProperties19.Append(paragraphMarkRunProperties19);

            Run run38 = new Run();

            RunProperties runProperties38 = new RunProperties();
            RunFonts runFonts53 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties38.Append(runFonts53);
            Text text38 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text38.Text = "3. ";

            run38.Append(runProperties38);
            run38.Append(text38);

            Run run39 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties39 = new RunProperties();
            RunFonts runFonts54 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold12 = new Bold();

            runProperties39.Append(runFonts54);
            runProperties39.Append(bold12);
            Text text39 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text39.Text = "Ittigaafatammumaa lakkoftuu eeguu fi kununsuu. ";

            run39.Append(runProperties39);
            run39.Append(text39);

            paragraph19.Append(paragraphProperties19);
            paragraph19.Append(run38);
            paragraph19.Append(run39);

            Paragraph paragraph20 = new Paragraph() { RsidParagraphAddition = "00FD42A3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00FD42A3" };

            ParagraphProperties paragraphProperties20 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties20 = new ParagraphMarkRunProperties();
            RunFonts runFonts55 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties20.Append(runFonts55);

            paragraphProperties20.Append(paragraphMarkRunProperties20);

            Run run40 = new Run();

            RunProperties runProperties40 = new RunProperties();
            RunFonts runFonts56 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties40.Append(runFonts56);
            Text text40 = new Text();
            text40.Text = "3.1. Lakkooftuun bishaanii kamiyuu qabeenya Biirootti kanfuu lakkoof";

            run40.Append(runProperties40);
            run40.Append(text40);

            Run run41 = new Run() { RsidRunAddition = "00784B9C" };

            RunProperties runProperties41 = new RunProperties();
            RunFonts runFonts57 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties41.Append(runFonts57);
            Text text41 = new Text();
            text41.Text = "tuu hikuu, dhaabuu, kaasuu, geeda";

            run41.Append(runProperties41);
            run41.Append(text41);

            Run run42 = new Run();

            RunProperties runProperties42 = new RunProperties();
            RunFonts runFonts58 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties42.Append(runFonts58);
            Text text42 = new Text();
            text42.Text = "ruu, qulqulleessuu, sakata’u qorachuu, saamsuu fi kkf gochu kan danda’u Biiroo qofa.";

            run42.Append(runProperties42);
            run42.Append(text42);

            paragraph20.Append(paragraphProperties20);
            paragraph20.Append(run40);
            paragraph20.Append(run41);
            paragraph20.Append(run42);

            Paragraph paragraph21 = new Paragraph() { RsidParagraphAddition = "00FD42A3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00447E4A" };

            ParagraphProperties paragraphProperties21 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties21 = new ParagraphMarkRunProperties();
            RunFonts runFonts59 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties21.Append(runFonts59);

            paragraphProperties21.Append(paragraphMarkRunProperties21);

            Run run43 = new Run();

            RunProperties runProperties43 = new RunProperties();
            RunFonts runFonts60 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties43.Append(runFonts60);
            Text text43 = new Text();
            text43.Text = "3.2. Iddon lakkooftun Bishaani";

            run43.Append(runProperties43);
            run43.Append(text43);

            Run run44 = new Run() { RsidRunAddition = "00F842A6" };

            RunProperties runProperties44 = new RunProperties();
            RunFonts runFonts61 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties44.Append(runFonts61);
            Text text44 = new Text();
            text44.Text = "i dhaabatu maamila irratti rakk";

            run44.Append(runProperties44);
            run44.Append(text44);

            Run run45 = new Run();

            RunProperties runProperties45 = new RunProperties();
            RunFonts runFonts62 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties45.Append(runFonts62);
            Text text45 = new Text();
            text45.Text = "o akka hin uumne ta’ee biiroodhaan ni murtaa’a.";

            run45.Append(runProperties45);
            run45.Append(text45);

            paragraph21.Append(paragraphProperties21);
            paragraph21.Append(run43);
            paragraph21.Append(run44);
            paragraph21.Append(run45);

            Paragraph paragraph22 = new Paragraph() { RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "000B426F", RsidRunAdditionDefault = "00620EEB" };

            ParagraphProperties paragraphProperties22 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId10 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties10 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference10 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId10 = new NumberingId() { Val = 1 };

            numberingProperties10.Append(numberingLevelReference10);
            numberingProperties10.Append(numberingId10);

            ParagraphMarkRunProperties paragraphMarkRunProperties22 = new ParagraphMarkRunProperties();
            RunFonts runFonts63 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties22.Append(runFonts63);

            paragraphProperties22.Append(paragraphStyleId10);
            paragraphProperties22.Append(numberingProperties10);
            paragraphProperties22.Append(paragraphMarkRunProperties22);

            Run run46 = new Run();

            RunProperties runProperties46 = new RunProperties();
            RunFonts runFonts64 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties46.Append(runFonts64);
            Text text46 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text46.Text = " ";

            run46.Append(runProperties46);
            run46.Append(text46);

            Run run47 = new Run() { RsidRunAddition = "00081FDC" };

            RunProperties runProperties47 = new RunProperties();
            RunFonts runFonts65 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties47.Append(runFonts65);
            Text text47 = new Text();
            text47.Text = "“Foyyeessu sararaa Bishaanii” jechun sarara maammila tokko gara sararaa bala’aa ykn guddaatti jijiru jechuudha.";

            run47.Append(runProperties47);
            run47.Append(text47);

            paragraph22.Append(paragraphProperties22);
            paragraph22.Append(run46);
            paragraph22.Append(run47);

            Paragraph paragraph23 = new Paragraph() { RsidParagraphMarkRevision = "00777AA0", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties23 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId11 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties11 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference11 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId11 = new NumberingId() { Val = 1 };

            numberingProperties11.Append(numberingLevelReference11);
            numberingProperties11.Append(numberingId11);

            Tabs tabs1 = new Tabs();
            TabStop tabStop1 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs1.Append(tabStop1);
            Indentation indentation1 = new Indentation() { Start = "180", FirstLine = "0" };

            ParagraphMarkRunProperties paragraphMarkRunProperties23 = new ParagraphMarkRunProperties();
            RunFonts runFonts66 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties23.Append(runFonts66);

            paragraphProperties23.Append(paragraphStyleId11);
            paragraphProperties23.Append(numberingProperties11);
            paragraphProperties23.Append(tabs1);
            paragraphProperties23.Append(indentation1);
            paragraphProperties23.Append(paragraphMarkRunProperties23);

            Run run48 = new Run() { RsidRunProperties = "00777AA0" };

            RunProperties runProperties48 = new RunProperties();
            RunFonts runFonts67 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties48.Append(runFonts67);
            Text text48 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text48.Text = " “Jijirraa sarara Bishaanii";

            run48.Append(runProperties48);
            run48.Append(text48);

            Run run49 = new Run() { RsidRunAddition = "00A36FC5" };

            RunProperties runProperties49 = new RunProperties();
            RunFonts runFonts68 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties49.Append(runFonts68);
            Text text49 = new Text();
            text49.Text = "”";

            run49.Append(runProperties49);
            run49.Append(text49);

            Run run50 = new Run() { RsidRunProperties = "00777AA0" };

            RunProperties runProperties50 = new RunProperties();
            RunFonts runFonts69 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties50.Append(runFonts69);
            Text text50 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text50.Text = " jechun sarara maamilaa tokko iddoo jiru gara biraati";

            run50.Append(runProperties50);
            run50.Append(text50);

            Run run51 = new Run() { RsidRunAddition = "00777AA0" };

            RunProperties runProperties51 = new RunProperties();
            RunFonts runFonts70 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties51.Append(runFonts70);
            Text text51 = new Text();
            text51.Text = "ijiru jechuudha.";

            run51.Append(runProperties51);
            run51.Append(text51);

            paragraph23.Append(paragraphProperties23);
            paragraph23.Append(run48);
            paragraph23.Append(run49);
            paragraph23.Append(run50);
            paragraph23.Append(run51);

            Paragraph paragraph24 = new Paragraph() { RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties24 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId12 = new ParagraphStyleId() { Val = "ListParagraph" };

            Tabs tabs2 = new Tabs();
            TabStop tabStop2 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs2.Append(tabStop2);
            Indentation indentation2 = new Indentation() { Start = "180" };

            ParagraphMarkRunProperties paragraphMarkRunProperties24 = new ParagraphMarkRunProperties();
            RunFonts runFonts71 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties24.Append(runFonts71);

            paragraphProperties24.Append(paragraphStyleId12);
            paragraphProperties24.Append(tabs2);
            paragraphProperties24.Append(indentation2);
            paragraphProperties24.Append(paragraphMarkRunProperties24);

            Run run52 = new Run();

            RunProperties runProperties52 = new RunProperties();
            RunFonts runFonts72 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties52.Append(runFonts72);
            Text text52 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text52.Text = "1.11. “Geedara sarara Bishaan” jechun iddoo ";

            run52.Append(runProperties52);
            run52.Append(text52);

            paragraph24.Append(paragraphProperties24);
            paragraph24.Append(run52);

            Paragraph paragraph25 = new Paragraph() { RsidParagraphMarkRevision = "00A36FC5", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00A36FC5", RsidRunAdditionDefault = "00A36FC5" };

            ParagraphProperties paragraphProperties25 = new ParagraphProperties();

            Tabs tabs3 = new Tabs();
            TabStop tabStop3 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs3.Append(tabStop3);

            ParagraphMarkRunProperties paragraphMarkRunProperties25 = new ParagraphMarkRunProperties();
            RunFonts runFonts73 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties25.Append(runFonts73);

            paragraphProperties25.Append(tabs3);
            paragraphProperties25.Append(paragraphMarkRunProperties25);

            Run run53 = new Run() { RsidRunProperties = "00A36FC5" };

            RunProperties runProperties53 = new RunProperties();
            RunFonts runFonts74 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties53.Append(runFonts74);
            LastRenderedPageBreak lastRenderedPageBreak3 = new LastRenderedPageBreak();
            Text text53 = new Text();
            text53.Text = ".";

            run53.Append(runProperties53);
            run53.Append(lastRenderedPageBreak3);
            run53.Append(text53);

            Run run54 = new Run() { RsidRunProperties = "00A36FC5", RsidRunAddition = "00081FDC" };

            RunProperties runProperties54 = new RunProperties();
            RunFonts runFonts75 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties54.Append(runFonts75);
            Text text54 = new Text();
            text54.Text = "fi bali’ini sarara duraanii osso hin jijiiramin ujumoo harawatii jijiru jechuudha.";

            run54.Append(runProperties54);
            run54.Append(text54);

            paragraph25.Append(paragraphProperties25);
            paragraph25.Append(run53);
            paragraph25.Append(run54);

            Paragraph paragraph26 = new Paragraph() { RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties26 = new ParagraphProperties();

            Tabs tabs4 = new Tabs();
            TabStop tabStop4 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs4.Append(tabStop4);

            ParagraphMarkRunProperties paragraphMarkRunProperties26 = new ParagraphMarkRunProperties();
            RunFonts runFonts76 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties26.Append(runFonts76);

            paragraphProperties26.Append(tabs4);
            paragraphProperties26.Append(paragraphMarkRunProperties26);

            Run run55 = new Run();

            RunProperties runProperties55 = new RunProperties();
            RunFonts runFonts77 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties55.Append(runFonts77);
            Text text55 = new Text();
            text55.Text = "1.12.";

            run55.Append(runProperties55);
            run55.Append(text55);

            Run run56 = new Run() { RsidRunProperties = "009F0A63" };

            RunProperties runProperties56 = new RunProperties();
            RunFonts runFonts78 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties56.Append(runFonts78);
            Text text56 = new Text();
            text56.Text = "“Tajaajila Bishaan Dhugaatii fi";

            run56.Append(runProperties56);
            run56.Append(text56);

            Run run57 = new Run();

            RunProperties runProperties57 = new RunProperties();
            RunFonts runFonts79 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties57.Append(runFonts79);
            Text text57 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text57.Text = " ";

            run57.Append(runProperties57);
            run57.Append(text57);

            Run run58 = new Run() { RsidRunProperties = "009F0A63" };

            RunProperties runProperties58 = new RunProperties();
            RunFonts runFonts80 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties58.Append(runFonts80);
            Text text58 = new Text();
            text58.Text = "Dhangala’aa Magaalaa” jechun waajjira bakka bu’aa Biiroo ta’ee hojii bishaan magaalaa kan bulchudha.";

            run58.Append(runProperties58);
            run58.Append(text58);

            paragraph26.Append(paragraphProperties26);
            paragraph26.Append(run55);
            paragraph26.Append(run56);
            paragraph26.Append(run57);
            paragraph26.Append(run58);

            Paragraph paragraph27 = new Paragraph() { RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties27 = new ParagraphProperties();

            Tabs tabs5 = new Tabs();
            TabStop tabStop5 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs5.Append(tabStop5);

            ParagraphMarkRunProperties paragraphMarkRunProperties27 = new ParagraphMarkRunProperties();
            RunFonts runFonts81 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties27.Append(runFonts81);

            paragraphProperties27.Append(tabs5);
            paragraphProperties27.Append(paragraphMarkRunProperties27);

            Run run59 = new Run();

            RunProperties runProperties59 = new RunProperties();
            RunFonts runFonts82 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties59.Append(runFonts82);
            Text text59 = new Text();
            text59.Text = "1.13. “Taarif Bishaan Haarawa” Jechun gati bishaani M";

            run59.Append(runProperties59);
            run59.Append(text59);

            Run run60 = new Run() { RsidRunProperties = "000A060E" };

            RunProperties runProperties60 = new RunProperties();
            RunFonts runFonts83 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            VerticalTextAlignment verticalTextAlignment1 = new VerticalTextAlignment() { Val = VerticalPositionValues.Superscript };

            runProperties60.Append(runFonts83);
            runProperties60.Append(verticalTextAlignment1);
            Text text60 = new Text();
            text60.Text = "3";

            run60.Append(runProperties60);
            run60.Append(text60);

            Run run61 = new Run();

            RunProperties runProperties61 = new RunProperties();
            RunFonts runFonts84 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties61.Append(runFonts84);
            Text text61 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text61.Text = " tokko yeroodhaa yerooti";

            run61.Append(runProperties61);
            run61.Append(text61);

            Run run62 = new Run() { RsidRunAddition = "0020407A" };

            RunProperties runProperties62 = new RunProperties();
            RunFonts runFonts85 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties62.Append(runFonts85);
            Text text62 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text62.Text = " ";

            run62.Append(runProperties62);
            run62.Append(text62);

            Run run63 = new Run();

            RunProperties runProperties63 = new RunProperties();
            RunFonts runFonts86 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties63.Append(runFonts86);
            Text text63 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text63.Text = " gedarame hojii irra olu jechuudha.";

            run63.Append(runProperties63);
            run63.Append(text63);

            paragraph27.Append(paragraphProperties27);
            paragraph27.Append(run59);
            paragraph27.Append(run60);
            paragraph27.Append(run61);
            paragraph27.Append(run62);
            paragraph27.Append(run63);

            Paragraph paragraph28 = new Paragraph() { RsidParagraphMarkRevision = "000B426F", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties28 = new ParagraphProperties();

            Tabs tabs6 = new Tabs();
            TabStop tabStop6 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs6.Append(tabStop6);

            ParagraphMarkRunProperties paragraphMarkRunProperties28 = new ParagraphMarkRunProperties();
            RunFonts runFonts87 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold13 = new Bold();

            paragraphMarkRunProperties28.Append(runFonts87);
            paragraphMarkRunProperties28.Append(bold13);

            paragraphProperties28.Append(tabs6);
            paragraphProperties28.Append(paragraphMarkRunProperties28);

            Run run64 = new Run();

            RunProperties runProperties64 = new RunProperties();
            RunFonts runFonts88 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties64.Append(runFonts88);
            Text text64 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text64.Text = "2. ";

            run64.Append(runProperties64);
            run64.Append(text64);

            Run run65 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties65 = new RunProperties();
            RunFonts runFonts89 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold14 = new Bold();

            runProperties65.Append(runFonts89);
            runProperties65.Append(bold14);
            Text text65 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text65.Text = "Sararaa Lakkooftu dura jiru dirirsu fi kununsuu :- ";

            run65.Append(runProperties65);
            run65.Append(text65);

            paragraph28.Append(paragraphProperties28);
            paragraph28.Append(run64);
            paragraph28.Append(run65);

            Paragraph paragraph29 = new Paragraph() { RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties29 = new ParagraphProperties();

            Tabs tabs7 = new Tabs();
            TabStop tabStop7 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs7.Append(tabStop7);

            ParagraphMarkRunProperties paragraphMarkRunProperties29 = new ParagraphMarkRunProperties();
            RunFonts runFonts90 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties29.Append(runFonts90);

            paragraphProperties29.Append(tabs7);
            paragraphProperties29.Append(paragraphMarkRunProperties29);

            Run run66 = new Run();

            RunProperties runProperties66 = new RunProperties();
            RunFonts runFonts91 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties66.Append(runFonts91);
            Text text66 = new Text();
            text66.Text = "2.1. Naannoo Tajaajila Bishaan dhugaatitti bishaan raabsuu fi hiruuf kan dirirfame sarara gurguddaa fi sarara hanga dhuma lakkoftuu jiran fi meeshalee fi qabisistu irraa jiran irratti kan ajaju Biiroo qofa kanaafu, hojii sararaa gaggeessuu fi ujummoo dirirsu, foyeesu, jijiru., geedaru, kutuu, kaasuu, qoorachuu fi sakata’uu kan danda’an hojjatoota Biiroo qofa.";

            run66.Append(runProperties66);
            run66.Append(text66);

            paragraph29.Append(paragraphProperties29);
            paragraph29.Append(run66);

            Paragraph paragraph30 = new Paragraph() { RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties30 = new ParagraphProperties();

            Tabs tabs8 = new Tabs();
            TabStop tabStop8 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs8.Append(tabStop8);

            ParagraphMarkRunProperties paragraphMarkRunProperties30 = new ParagraphMarkRunProperties();
            RunFonts runFonts92 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties30.Append(runFonts92);

            paragraphProperties30.Append(tabs8);
            paragraphProperties30.Append(paragraphMarkRunProperties30);

            Run run67 = new Run();

            RunProperties runProperties67 = new RunProperties();
            RunFonts runFonts93 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties67.Append(runFonts93);
            Text text67 = new Text();
            text67.Text = "2.2. Bal’ina ujumoo bishaanii maamilootaaf kan dirirfamu fi karaa ujumoon kun itti dirifamu maamiloota wajjiin waligaluudhaan Biiroon ni murteessa.";

            run67.Append(runProperties67);
            run67.Append(text67);

            paragraph30.Append(paragraphProperties30);
            paragraph30.Append(run67);

            Paragraph paragraph31 = new Paragraph() { RsidParagraphAddition = "00584CDA", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties31 = new ParagraphProperties();

            Tabs tabs9 = new Tabs();
            TabStop tabStop9 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs9.Append(tabStop9);

            ParagraphMarkRunProperties paragraphMarkRunProperties31 = new ParagraphMarkRunProperties();
            RunFonts runFonts94 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize1 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties31.Append(runFonts94);
            paragraphMarkRunProperties31.Append(fontSize1);

            paragraphProperties31.Append(tabs9);
            paragraphProperties31.Append(paragraphMarkRunProperties31);

            Run run68 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties68 = new RunProperties();
            RunFonts runFonts95 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize2 = new FontSize() { Val = "20" };

            runProperties68.Append(runFonts95);
            runProperties68.Append(fontSize2);
            Text text68 = new Text();
            text68.Text = "2.3. Sarara Bishaanii sarara guddaa irraa ka’ee gara maamilchaati deemu dirirsuuf, suphuf, foyyeessuf, jijiruf, geedaruf kan barbaachisu kkf ujumoo fi qab";

            run68.Append(runProperties68);
            run68.Append(text68);

            Run run69 = new Run() { RsidRunAddition = "0020407A" };

            RunProperties runProperties69 = new RunProperties();
            RunFonts runFonts96 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize3 = new FontSize() { Val = "20" };

            runProperties69.Append(runFonts96);
            runProperties69.Append(fontSize3);
            Text text69 = new Text();
            text69.Text = "sisitu maamilichi baasii offisa";

            run69.Append(runProperties69);
            run69.Append(text69);

            Run run70 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties70 = new RunProperties();
            RunFonts runFonts97 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize4 = new FontSize() { Val = "20" };

            runProperties70.Append(runFonts97);
            runProperties70.Append(fontSize4);
            Text text70 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text70.Text = "antin ni dhiyeesa akksumas baasii hojii ";

            run70.Append(runProperties70);
            run70.Append(text70);

            Run run71 = new Run() { RsidRunAddition = "0020407A" };

            RunProperties runProperties71 = new RunProperties();
            RunFonts runFonts98 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize5 = new FontSize() { Val = "20" };

            runProperties71.Append(runFonts98);
            runProperties71.Append(fontSize5);
            Text text71 = new Text();
            text71.Text = "kanaaf barbaachisu hunda";

            run71.Append(runProperties71);
            run71.Append(text71);

            Run run72 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties72 = new RunProperties();
            RunFonts runFonts99 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize6 = new FontSize() { Val = "20" };

            runProperties72.Append(runFonts99);
            runProperties72.Append(fontSize6);
            Text text72 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text72.Text = ".4. ";

            run72.Append(runProperties72);
            run72.Append(text72);

            paragraph31.Append(paragraphProperties31);
            paragraph31.Append(run68);
            paragraph31.Append(run69);
            paragraph31.Append(run70);
            paragraph31.Append(run71);
            paragraph31.Append(run72);

            Paragraph paragraph32 = new Paragraph() { RsidParagraphAddition = "00620EEB", RsidParagraphProperties = "00620EEB", RsidRunAdditionDefault = "00620EEB" };

            ParagraphProperties paragraphProperties32 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties32 = new ParagraphMarkRunProperties();
            RunFonts runFonts100 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties32.Append(runFonts100);

            paragraphProperties32.Append(paragraphMarkRunProperties32);

            Run run73 = new Run();

            RunProperties runProperties73 = new RunProperties();
            RunFonts runFonts101 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties73.Append(runFonts101);
            Text text73 = new Text();
            text73.Text = "3.3. Lakkooftu Bishaani haala qajeelfaama biiroon kenuun sanduqa maamilli dhioyeessu keessatti dhaabachuu qaba.";

            run73.Append(runProperties73);
            run73.Append(text73);

            paragraph32.Append(paragraphProperties32);
            paragraph32.Append(run73);

            Paragraph paragraph33 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00865F0A" };

            ParagraphProperties paragraphProperties33 = new ParagraphProperties();

            Tabs tabs10 = new Tabs();
            TabStop tabStop10 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs10.Append(tabStop10);

            ParagraphMarkRunProperties paragraphMarkRunProperties33 = new ParagraphMarkRunProperties();
            RunFonts runFonts102 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize7 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties33.Append(runFonts102);
            paragraphMarkRunProperties33.Append(fontSize7);

            paragraphProperties33.Append(tabs10);
            paragraphProperties33.Append(paragraphMarkRunProperties33);

            Run run74 = new Run();

            RunProperties runProperties74 = new RunProperties();
            RunFonts runFonts103 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize8 = new FontSize() { Val = "20" };

            runProperties74.Append(runFonts103);
            runProperties74.Append(fontSize8);
            Text text74 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text74.Text = "3.4. Nageenya Lakkoofftu eeguun ittigaafatamummaa ";

            run74.Append(runProperties74);
            run74.Append(text74);

            Run run75 = new Run() { RsidRunAddition = "005B3F9C" };

            RunProperties runProperties75 = new RunProperties();
            RunFonts runFonts104 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize9 = new FontSize() { Val = "20" };

            runProperties75.Append(runFonts104);
            runProperties75.Append(fontSize9);
            Text text75 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text75.Text = "maamilichaattikanaafuu maamilichaan midhaa Lakkooftu irra gaheef maamilchi ";

            run75.Append(runProperties75);
            run75.Append(text75);

            Run run76 = new Run() { RsidRunProperties = "00CF635B", RsidRunAddition = "00081FDC" };

            RunProperties runProperties76 = new RunProperties();
            RunFonts runFonts105 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize10 = new FontSize() { Val = "20" };

            runProperties76.Append(runFonts105);
            runProperties76.Append(fontSize10);
            Text text76 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text76.Text = " baasii supha ykn jijirraa akka kafalu ta’ee yakka raawwateef seeran ni gaafatama.";

            run76.Append(runProperties76);
            run76.Append(text76);

            paragraph33.Append(paragraphProperties33);
            paragraph33.Append(run74);
            paragraph33.Append(run75);
            paragraph33.Append(run76);

            Paragraph paragraph34 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties34 = new ParagraphProperties();

            Tabs tabs11 = new Tabs();
            TabStop tabStop11 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs11.Append(tabStop11);

            ParagraphMarkRunProperties paragraphMarkRunProperties34 = new ParagraphMarkRunProperties();
            RunFonts runFonts106 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize11 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties34.Append(runFonts106);
            paragraphMarkRunProperties34.Append(fontSize11);

            paragraphProperties34.Append(tabs11);
            paragraphProperties34.Append(paragraphMarkRunProperties34);

            Run run77 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties77 = new RunProperties();
            RunFonts runFonts107 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize12 = new FontSize() { Val = "20" };

            runProperties77.Append(runFonts107);
            runProperties77.Append(fontSize12);
            Text text77 = new Text();
            text77.Text = "3.5. Sababa duluma ykn liqidaa’utiin midhaan lakkooftu irraa yoo gahe Biiroon baasii isaatin ni supha, ni jijiiras.";

            run77.Append(runProperties77);
            run77.Append(text77);

            paragraph34.Append(paragraphProperties34);
            paragraph34.Append(run77);

            Paragraph paragraph35 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties35 = new ParagraphProperties();

            Tabs tabs12 = new Tabs();
            TabStop tabStop12 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs12.Append(tabStop12);

            ParagraphMarkRunProperties paragraphMarkRunProperties35 = new ParagraphMarkRunProperties();
            RunFonts runFonts108 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize13 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties35.Append(runFonts108);
            paragraphMarkRunProperties35.Append(fontSize13);

            paragraphProperties35.Append(tabs12);
            paragraphProperties35.Append(paragraphMarkRunProperties35);

            Run run78 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties78 = new RunProperties();
            RunFonts runFonts109 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize14 = new FontSize() { Val = "20" };

            runProperties78.Append(runFonts109);
            runProperties78.Append(fontSize14);
            Text text78 = new Text();
            text78.Text = "3.6. Maamilichi lakkooftuu";

            run78.Append(runProperties78);
            run78.Append(text78);

            Run run79 = new Run() { RsidRunAddition = "001B1AEE" };

            RunProperties runProperties79 = new RunProperties();
            RunFonts runFonts110 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize15 = new FontSize() { Val = "20" };

            runProperties79.Append(runFonts110);
            runProperties79.Append(fontSize15);
            Text text79 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text79.Text = " ";

            run79.Append(runProperties79);
            run79.Append(text79);

            Run run80 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties80 = new RunProperties();
            RunFonts runFonts111 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize16 = new FontSize() { Val = "20" };

            runProperties80.Append(runFonts111);
            runProperties80.Append(fontSize16);
            Text text80 = new Text();
            text80.Text = "bishaanii akka hin";

            run80.Append(runProperties80);
            run80.Append(text80);

            Run run81 = new Run() { RsidRunAddition = "001B1AEE" };

            RunProperties runProperties81 = new RunProperties();
            RunFonts runFonts112 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize17 = new FontSize() { Val = "20" };

            runProperties81.Append(runFonts112);
            runProperties81.Append(fontSize17);
            Text text81 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text81.Text = " h";

            run81.Append(runProperties81);
            run81.Append(text81);

            Run run82 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties82 = new RunProperties();
            RunFonts runFonts113 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize18 = new FontSize() { Val = "20" };

            runProperties82.Append(runFonts113);
            runProperties82.Append(fontSize18);
            Text text82 = new Text();
            text82.Text = "ojjane ykn midhaan akka irra gahe ta’uu isaa yoo beeke ariitiin Biiroof beeksisu qaba.";

            run82.Append(runProperties82);
            run82.Append(text82);

            paragraph35.Append(paragraphProperties35);
            paragraph35.Append(run78);
            paragraph35.Append(run79);
            paragraph35.Append(run80);
            paragraph35.Append(run81);
            paragraph35.Append(run82);

            Paragraph paragraph36 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties36 = new ParagraphProperties();

            Tabs tabs13 = new Tabs();
            TabStop tabStop13 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs13.Append(tabStop13);

            ParagraphMarkRunProperties paragraphMarkRunProperties36 = new ParagraphMarkRunProperties();
            RunFonts runFonts114 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize19 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties36.Append(runFonts114);
            paragraphMarkRunProperties36.Append(fontSize19);

            paragraphProperties36.Append(tabs13);
            paragraphProperties36.Append(paragraphMarkRunProperties36);

            Run run83 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties83 = new RunProperties();
            RunFonts runFonts115 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize20 = new FontSize() { Val = "20" };

            runProperties83.Append(runFonts115);
            runProperties83.Append(fontSize20);
            LastRenderedPageBreak lastRenderedPageBreak4 = new LastRenderedPageBreak();
            Text text83 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text83.Text = "4. ";

            run83.Append(runProperties83);
            run83.Append(lastRenderedPageBreak4);
            run83.Append(text83);

            Run run84 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties84 = new RunProperties();
            RunFonts runFonts116 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold15 = new Bold();
            FontSize fontSize21 = new FontSize() { Val = "20" };

            runProperties84.Append(runFonts116);
            runProperties84.Append(bold15);
            runProperties84.Append(fontSize21);
            Text text84 = new Text();
            text84.Text = "Akkaataa Kaffalti itti Fayyadama Bishaanii";

            run84.Append(runProperties84);
            run84.Append(text84);

            Run run85 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties85 = new RunProperties();
            RunFonts runFonts117 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize22 = new FontSize() { Val = "20" };

            runProperties85.Append(runFonts117);
            runProperties85.Append(fontSize22);
            Text text85 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text85.Text = " ";

            run85.Append(runProperties85);
            run85.Append(text85);

            paragraph36.Append(paragraphProperties36);
            paragraph36.Append(run83);
            paragraph36.Append(run84);
            paragraph36.Append(run85);

            Paragraph paragraph37 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties37 = new ParagraphProperties();

            Tabs tabs14 = new Tabs();
            TabStop tabStop14 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs14.Append(tabStop14);

            ParagraphMarkRunProperties paragraphMarkRunProperties37 = new ParagraphMarkRunProperties();
            RunFonts runFonts118 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize23 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties37.Append(runFonts118);
            paragraphMarkRunProperties37.Append(fontSize23);

            paragraphProperties37.Append(tabs14);
            paragraphProperties37.Append(paragraphMarkRunProperties37);

            Run run86 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties86 = new RunProperties();
            RunFonts runFonts119 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize24 = new FontSize() { Val = "20" };

            runProperties86.Append(runFonts119);
            runProperties86.Append(fontSize24);
            Text text86 = new Text();
            text86.Text = "4.1. Maammilichii taarifa Biiroon Bishaan dhiyeesu fi murteessen haala lakkooftun mulisuun ittifayyadama bishaanii kafalu qaba.";

            run86.Append(runProperties86);
            run86.Append(text86);

            paragraph37.Append(paragraphProperties37);
            paragraph37.Append(run86);

            Paragraph paragraph38 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties38 = new ParagraphProperties();

            Tabs tabs15 = new Tabs();
            TabStop tabStop15 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs15.Append(tabStop15);

            ParagraphMarkRunProperties paragraphMarkRunProperties38 = new ParagraphMarkRunProperties();
            RunFonts runFonts120 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize25 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties38.Append(runFonts120);
            paragraphMarkRunProperties38.Append(fontSize25);

            paragraphProperties38.Append(tabs15);
            paragraphProperties38.Append(paragraphMarkRunProperties38);

            Run run87 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties87 = new RunProperties();
            RunFonts runFonts121 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize26 = new FontSize() { Val = "20" };

            runProperties87.Append(runFonts121);
            runProperties87.Append(fontSize26);
            Text text87 = new Text();
            text87.Text = "4.2";

            run87.Append(runProperties87);
            run87.Append(text87);

            Run run88 = new Run() { RsidRunAddition = "001B1AEE" };

            RunProperties runProperties88 = new RunProperties();
            RunFonts runFonts122 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize27 = new FontSize() { Val = "20" };

            runProperties88.Append(runFonts122);
            runProperties88.Append(fontSize27);
            Text text88 = new Text();
            text88.Text = ". Lakkooftuun sirritti kan hin h";

            run88.Append(runProperties88);
            run88.Append(text88);

            Run run89 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties89 = new RunProperties();
            RunFonts runFonts123 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize28 = new FontSize() { Val = "20" };

            runProperties89.Append(runFonts123);
            runProperties89.Append(fontSize28);
            Text text89 = new Text();
            text89.Text = "ojjane ykn hin dubifamni ta’u isaa duraa";

            run89.Append(runProperties89);
            run89.Append(text89);

            Run run90 = new Run() { RsidRunAddition = "001B1AEE" };

            RunProperties runProperties90 = new RunProperties();
            RunFonts runFonts124 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize29 = new FontSize() { Val = "20" };

            runProperties90.Append(runFonts124);
            runProperties90.Append(fontSize29);
            Text text90 = new Text();
            text90.Text = "n dursee yoo beekame malee dubis";

            run90.Append(runProperties90);
            run90.Append(text90);

            Run run91 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties91 = new RunProperties();
            RunFonts runFonts125 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize30 = new FontSize() { Val = "20" };

            runProperties91.Append(runFonts125);
            runProperties91.Append(fontSize30);
            Text text91 = new Text();
            text91.Text = "aan lakkooftuun galmeesse biiroo fi maamilichaan fudhatama ni qaba.";

            run91.Append(runProperties91);
            run91.Append(text91);

            paragraph38.Append(paragraphProperties38);
            paragraph38.Append(run87);
            paragraph38.Append(run88);
            paragraph38.Append(run89);
            paragraph38.Append(run90);
            paragraph38.Append(run91);

            Paragraph paragraph39 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties39 = new ParagraphProperties();

            Tabs tabs16 = new Tabs();
            TabStop tabStop16 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs16.Append(tabStop16);

            ParagraphMarkRunProperties paragraphMarkRunProperties39 = new ParagraphMarkRunProperties();
            RunFonts runFonts126 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize31 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties39.Append(runFonts126);
            paragraphMarkRunProperties39.Append(fontSize31);

            paragraphProperties39.Append(tabs16);
            paragraphProperties39.Append(paragraphMarkRunProperties39);

            Run run92 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties92 = new RunProperties();
            RunFonts runFonts127 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize32 = new FontSize() { Val = "20" };

            runProperties92.Append(runFonts127);
            runProperties92.Append(fontSize32);
            Text text92 = new Text();
            text92.Text = "4.3. Siruumaan lakkooftuu shakisisaa ta’ee yoo argame maamilichii haala seeraa fi qajeelfama Biirootiin lakkooftuu qorachisuu ni danda’a.";

            run92.Append(runProperties92);
            run92.Append(text92);

            paragraph39.Append(paragraphProperties39);
            paragraph39.Append(run92);

            Paragraph paragraph40 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties40 = new ParagraphProperties();

            Tabs tabs17 = new Tabs();
            TabStop tabStop17 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs17.Append(tabStop17);

            ParagraphMarkRunProperties paragraphMarkRunProperties40 = new ParagraphMarkRunProperties();
            RunFonts runFonts128 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize33 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties40.Append(runFonts128);
            paragraphMarkRunProperties40.Append(fontSize33);

            paragraphProperties40.Append(tabs17);
            paragraphProperties40.Append(paragraphMarkRunProperties40);

            Run run93 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties93 = new RunProperties();
            RunFonts runFonts129 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize34 = new FontSize() { Val = "20" };

            runProperties93.Append(runFonts129);
            runProperties93.Append(fontSize34);
            Text text93 = new Text();
            text93.Text = "4.4. Lakkooftuun rakkina akka qabu yoo beekamee baay’iini ittifayyadamuu bishaanii galmee ji’a 6 yeroo lakkooftuun fayyaa turee araagatiin gidugalessaa fudhatamee murta’a haata’u malee Biiroon maamilichi haala beekame ol itti fayyadamuu isaa kan argarsiisu raga yoo qabaate gatii murtaa’e maamilichii ni kaffalachiisa.";

            run93.Append(runProperties93);
            run93.Append(text93);

            paragraph40.Append(paragraphProperties40);
            paragraph40.Append(run93);

            Paragraph paragraph41 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties41 = new ParagraphProperties();

            Tabs tabs18 = new Tabs();
            TabStop tabStop18 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs18.Append(tabStop18);

            ParagraphMarkRunProperties paragraphMarkRunProperties41 = new ParagraphMarkRunProperties();
            RunFonts runFonts130 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize35 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties41.Append(runFonts130);
            paragraphMarkRunProperties41.Append(fontSize35);

            paragraphProperties41.Append(tabs18);
            paragraphProperties41.Append(paragraphMarkRunProperties41);

            Run run94 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties94 = new RunProperties();
            RunFonts runFonts131 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize36 = new FontSize() { Val = "20" };

            runProperties94.Append(runFonts131);
            runProperties94.Append(fontSize36);
            Text text94 = new Text();
            text94.Text = "4.5. Lakkooftuun qoratame caalmaan ykn hir’naan dogogora kan mulisu yoo ta’e herregni isaa haaluma kanaan caalmaan ykn hir’inaan ni sirraa.";

            run94.Append(runProperties94);
            run94.Append(text94);

            paragraph41.Append(paragraphProperties41);
            paragraph41.Append(run94);

            Paragraph paragraph42 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00081FDC", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties42 = new ParagraphProperties();

            Tabs tabs19 = new Tabs();
            TabStop tabStop19 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs19.Append(tabStop19);

            ParagraphMarkRunProperties paragraphMarkRunProperties42 = new ParagraphMarkRunProperties();
            RunFonts runFonts132 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize37 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties42.Append(runFonts132);
            paragraphMarkRunProperties42.Append(fontSize37);

            paragraphProperties42.Append(tabs19);
            paragraphProperties42.Append(paragraphMarkRunProperties42);

            Run run95 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties95 = new RunProperties();
            RunFonts runFonts133 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize38 = new FontSize() { Val = "20" };

            runProperties95.Append(runFonts133);
            runProperties95.Append(fontSize38);
            Text text95 = new Text();
            text95.Text = "kanaaf barbaachisu hunda maamilichi ni kafala(Haata’u malee, bifaa fi qulqullina meeshaa maamilichi dhiyeessu Biiroon ni to’aata)";

            run95.Append(runProperties95);
            run95.Append(text95);

            paragraph42.Append(paragraphProperties42);
            paragraph42.Append(run95);

            Paragraph paragraph43 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "003656B4", RsidParagraphProperties = "00081FDC", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties43 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties43 = new ParagraphMarkRunProperties();
            RunFonts runFonts134 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize39 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties43.Append(runFonts134);
            paragraphMarkRunProperties43.Append(fontSize39);

            paragraphProperties43.Append(paragraphMarkRunProperties43);

            Run run96 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties96 = new RunProperties();
            RunFonts runFonts135 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize40 = new FontSize() { Val = "20" };

            runProperties96.Append(runFonts135);
            runProperties96.Append(fontSize40);
            Text text96 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text96.Text = "2.4. Sarara lakkooftuun dura jiru dirirsuf, foyyeessuf, jijiruf, geedaruf, fi kkf hojechuuf ";

            run96.Append(runProperties96);
            run96.Append(text96);

            paragraph43.Append(paragraphProperties43);
            paragraph43.Append(run96);

            Paragraph paragraph44 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00081FDC" };

            ParagraphProperties paragraphProperties44 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties44 = new ParagraphMarkRunProperties();
            RunFonts runFonts136 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize41 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties44.Append(runFonts136);
            paragraphMarkRunProperties44.Append(fontSize41);

            paragraphProperties44.Append(paragraphMarkRunProperties44);

            Run run97 = new Run() { RsidRunProperties = "00CF635B" };

            RunProperties runProperties97 = new RunProperties();
            RunFonts runFonts137 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize42 = new FontSize() { Val = "20" };

            runProperties97.Append(runFonts137);
            runProperties97.Append(fontSize42);
            Text text97 = new Text();
            text97.Text = "meeshoole barbaachisaanii fi oggeessota muxxannoo qaban Biiroon ni dhiyeesa";

            run97.Append(runProperties97);
            run97.Append(text97);

            paragraph44.Append(paragraphProperties44);
            paragraph44.Append(run97);

            Paragraph paragraph45 = new Paragraph() { RsidParagraphMarkRevision = "00CF635B", RsidParagraphAddition = "00454820", RsidParagraphProperties = "00454820", RsidRunAdditionDefault = "00454820" };

            ParagraphProperties paragraphProperties45 = new ParagraphProperties();

            Tabs tabs20 = new Tabs();
            TabStop tabStop20 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs20.Append(tabStop20);

            ParagraphMarkRunProperties paragraphMarkRunProperties45 = new ParagraphMarkRunProperties();
            RunFonts runFonts138 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize43 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties45.Append(runFonts138);
            paragraphMarkRunProperties45.Append(fontSize43);

            paragraphProperties45.Append(tabs20);
            paragraphProperties45.Append(paragraphMarkRunProperties45);

            Run run98 = new Run();

            RunProperties runProperties98 = new RunProperties();
            RunFonts runFonts139 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize44 = new FontSize() { Val = "20" };

            runProperties98.Append(runFonts139);
            runProperties98.Append(fontSize44);
            Text text98 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text98.Text = "maammilichi ni kafala (Haata’u malee bifaa fi qalqullina meeshaa maamilichi dhiyeessu Biiroon ni to’aata) ";

            run98.Append(runProperties98);
            run98.Append(text98);

            paragraph45.Append(paragraphProperties45);
            paragraph45.Append(run98);

            Paragraph paragraph46 = new Paragraph() { RsidParagraphAddition = "00740E33", RsidParagraphProperties = "00F65E2E", RsidRunAdditionDefault = "00740E33" };

            ParagraphProperties paragraphProperties46 = new ParagraphProperties();

            Tabs tabs21 = new Tabs();
            TabStop tabStop21 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs21.Append(tabStop21);
            SpacingBetweenLines spacingBetweenLines1 = new SpacingBetweenLines() { Line = "240", LineRule = LineSpacingRuleValues.Auto };

            ParagraphMarkRunProperties paragraphMarkRunProperties46 = new ParagraphMarkRunProperties();
            RunFonts runFonts140 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties46.Append(runFonts140);

            paragraphProperties46.Append(tabs21);
            paragraphProperties46.Append(spacingBetweenLines1);
            paragraphProperties46.Append(paragraphMarkRunProperties46);

            Run run99 = new Run();

            RunProperties runProperties99 = new RunProperties();
            RunFonts runFonts141 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties99.Append(runFonts141);
            Text text99 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text99.Text = "4.6. Lakkooftuu qoratame sirri ta’ee yoo argame baasiin maamilichaan ni kafalama. Haata’u malee lakkooftuun sirri ta’ee argamu yoo baate ";

            run99.Append(runProperties99);
            run99.Append(text99);

            paragraph46.Append(paragraphProperties46);
            paragraph46.Append(run99);

            Paragraph paragraph47 = new Paragraph() { RsidParagraphAddition = "00740E33", RsidParagraphProperties = "00F65E2E", RsidRunAdditionDefault = "00740E33" };

            ParagraphProperties paragraphProperties47 = new ParagraphProperties();
            SpacingBetweenLines spacingBetweenLines2 = new SpacingBetweenLines() { Line = "240", LineRule = LineSpacingRuleValues.Auto };

            ParagraphMarkRunProperties paragraphMarkRunProperties47 = new ParagraphMarkRunProperties();
            RunFonts runFonts142 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties47.Append(runFonts142);

            paragraphProperties47.Append(spacingBetweenLines2);
            paragraphProperties47.Append(paragraphMarkRunProperties47);

            Run run100 = new Run();

            RunProperties runProperties100 = new RunProperties();
            RunFonts runFonts143 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties100.Append(runFonts143);
            Text text100 = new Text();
            text100.Text = "baasii qorannoo Biiroon ni k";

            run100.Append(runProperties100);
            run100.Append(text100);

            Run run101 = new Run() { RsidRunAddition = "00F65E2E" };

            RunProperties runProperties101 = new RunProperties();
            RunFonts runFonts144 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties101.Append(runFonts144);
            Text text101 = new Text();
            text101.Text = "afalama.";

            run101.Append(runProperties101);
            run101.Append(text101);

            paragraph47.Append(paragraphProperties47);
            paragraph47.Append(run100);
            paragraph47.Append(run101);

            Paragraph paragraph48 = new Paragraph() { RsidParagraphAddition = "00724030", RsidParagraphProperties = "00F65E2E", RsidRunAdditionDefault = "00724030" };

            ParagraphProperties paragraphProperties48 = new ParagraphProperties();
            SpacingBetweenLines spacingBetweenLines3 = new SpacingBetweenLines() { Line = "240", LineRule = LineSpacingRuleValues.Auto };

            ParagraphMarkRunProperties paragraphMarkRunProperties48 = new ParagraphMarkRunProperties();
            RunFonts runFonts145 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties48.Append(runFonts145);

            paragraphProperties48.Append(spacingBetweenLines3);
            paragraphProperties48.Append(paragraphMarkRunProperties48);

            Run run102 = new Run();

            RunProperties runProperties102 = new RunProperties();
            RunFonts runFonts146 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties102.Append(runFonts146);
            Text text102 = new Text();
            text102.Text = "4.7. Armaan gaditti kewwata 4 lakk 4.8 irratti offeegannoon caqasa";

            run102.Append(runProperties102);
            run102.Append(text102);

            Run run103 = new Run() { RsidRunAddition = "00F65E2E" };

            RunProperties runProperties103 = new RunProperties();
            RunFonts runFonts147 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties103.Append(runFonts147);
            Text text103 = new Text();
            text103.Text = "me maamilichaaf ykn miseensa wa";

            run103.Append(runProperties103);
            run103.Append(text103);

            Run run104 = new Run();

            RunProperties runProperties104 = new RunProperties();
            RunFonts runFonts148 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties104.Append(runFonts148);
            Text text104 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text104.Text = "rra manaa ta’e waggaa 15 ol ta’eef ykn hojjataa (hojjatuu) keenamu yoo danda’amuu baate beeksisni ";

            run104.Append(runProperties104);
            run104.Append(text104);

            Run run105 = new Run() { RsidRunAddition = "00F65E2E" };

            RunProperties runProperties105 = new RunProperties();
            RunFonts runFonts149 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties105.Append(runFonts149);
            Text text105 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text105.Text = " iffati akka mul’atu ta’ee m";

            run105.Append(runProperties105);
            run105.Append(text105);

            Run run106 = new Run();

            RunProperties runProperties106 = new RunProperties();
            RunFonts runFonts150 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties106.Append(runFonts150);
            Text text106 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text106.Text = "ana jireenyaa ykn waajjira ykn balbala dalaa maamilichaa fi iddoo kana fakkatu irratti ";

            run106.Append(runProperties106);
            run106.Append(text106);

            Run run107 = new Run();

            RunProperties runProperties107 = new RunProperties();
            RunFonts runFonts151 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties107.Append(runFonts151);
            LastRenderedPageBreak lastRenderedPageBreak5 = new LastRenderedPageBreak();
            Text text107 = new Text();
            text107.Text = "yoo maxanfame maamilichaaf akka gaheti fudhatama.";

            run107.Append(runProperties107);
            run107.Append(lastRenderedPageBreak5);
            run107.Append(text107);

            paragraph48.Append(paragraphProperties48);
            paragraph48.Append(run102);
            paragraph48.Append(run103);
            paragraph48.Append(run104);
            paragraph48.Append(run105);
            paragraph48.Append(run106);
            paragraph48.Append(run107);

            Paragraph paragraph49 = new Paragraph() { RsidParagraphAddition = "002A4B84", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00724030" };

            ParagraphProperties paragraphProperties49 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties49 = new ParagraphMarkRunProperties();
            RunFonts runFonts152 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties49.Append(runFonts152);

            paragraphProperties49.Append(paragraphMarkRunProperties49);

            Run run108 = new Run();

            RunProperties runProperties108 = new RunProperties();
            RunFonts runFonts153 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties108.Append(runFonts153);
            Text text108 = new Text();
            text108.Text = "4.8. Maamilichii ittifayyadama";

            run108.Append(runProperties108);
            run108.Append(text108);

            Run run109 = new Run() { RsidRunAddition = "00DD27E9" };

            RunProperties runProperties109 = new RunProperties();
            RunFonts runFonts154 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties109.Append(runFonts154);
            Text text109 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text109.Text = " ";

            run109.Append(runProperties109);
            run109.Append(text109);

            Run run110 = new Run();

            RunProperties runProperties110 = new RunProperties();
            RunFonts runFonts155 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties110.Append(runFonts155);
            Text text110 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text110.Text = " bishaanii haala biilii dhiyaatun kaffalun irraa jira. Kaffalu baanaan walduraa dubaan of eegannon guyyaa torbaa(7) sa’attii digdamii afurii(24) kenuudhaan deebii yoo dhabee Biiroon maamilichaa irraa tajaajila Bishaan ni dhaba.";

            run110.Append(runProperties110);
            run110.Append(text110);

            paragraph49.Append(paragraphProperties49);
            paragraph49.Append(run108);
            paragraph49.Append(run109);
            paragraph49.Append(run110);

            Paragraph paragraph50 = new Paragraph() { RsidParagraphAddition = "002A4B84", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002A4B84" };

            ParagraphProperties paragraphProperties50 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties50 = new ParagraphMarkRunProperties();
            RunFonts runFonts156 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties50.Append(runFonts156);

            paragraphProperties50.Append(paragraphMarkRunProperties50);

            Run run111 = new Run();

            RunProperties runProperties111 = new RunProperties();
            RunFonts runFonts157 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties111.Append(runFonts157);
            Text text111 = new Text();
            text111.Text = "4.9. Maammilichii itti fayyadama bishaanii yeroo yeeroodhaan kaffalu baate yoo irrati kuusame kuusaa kana haala dhala wajjin akka kafalu ni diriqama.";

            run111.Append(runProperties111);
            run111.Append(text111);

            paragraph50.Append(paragraphProperties50);
            paragraph50.Append(run111);

            Paragraph paragraph51 = new Paragraph() { RsidParagraphMarkRevision = "00907635", RsidParagraphAddition = "002A4B84", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002A4B84" };

            ParagraphProperties paragraphProperties51 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties51 = new ParagraphMarkRunProperties();
            RunFonts runFonts158 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold16 = new Bold();

            paragraphMarkRunProperties51.Append(runFonts158);
            paragraphMarkRunProperties51.Append(bold16);

            paragraphProperties51.Append(paragraphMarkRunProperties51);

            Run run112 = new Run() { RsidRunProperties = "00907635" };

            RunProperties runProperties112 = new RunProperties();
            RunFonts runFonts159 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold17 = new Bold();

            runProperties112.Append(runFonts159);
            runProperties112.Append(bold17);
            Text text112 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text112.Text = "5. Sarara ";

            run112.Append(runProperties112);
            run112.Append(text112);

            Run run113 = new Run() { RsidRunAddition = "00A223AC" };

            RunProperties runProperties113 = new RunProperties();
            RunFonts runFonts160 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold18 = new Bold();

            runProperties113.Append(runFonts160);
            runProperties113.Append(bold18);
            Text text113 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text113.Text = "lakkooftuun boda jiru dirissuu ";

            run113.Append(runProperties113);
            run113.Append(text113);

            Run run114 = new Run() { RsidRunProperties = "00907635" };

            RunProperties runProperties114 = new RunProperties();
            RunFonts runFonts161 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold19 = new Bold();

            runProperties114.Append(runFonts161);
            runProperties114.Append(bold19);
            Text text114 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text114.Text = "fi suphuu:- ";

            run114.Append(runProperties114);
            run114.Append(text114);

            paragraph51.Append(paragraphProperties51);
            paragraph51.Append(run112);
            paragraph51.Append(run113);
            paragraph51.Append(run114);

            Paragraph paragraph52 = new Paragraph() { RsidParagraphAddition = "002A4B84", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002A4B84" };

            ParagraphProperties paragraphProperties52 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties52 = new ParagraphMarkRunProperties();
            RunFonts runFonts162 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties52.Append(runFonts162);

            paragraphProperties52.Append(paragraphMarkRunProperties52);

            Run run115 = new Run();

            RunProperties runProperties115 = new RunProperties();
            RunFonts runFonts163 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties115.Append(runFonts163);
            Text text115 = new Text();
            text115.Text = "5.1. Sarara lakkooftuun booda jiru dirrisuu fi suphuun itti gaafatamummaa maamilichaati. Haata’uu malee barbaachisaa ta’ee yoo argame biiroo";

            run115.Append(runProperties115);
            run115.Append(text115);

            Run run116 = new Run() { RsidRunAddition = "00907635" };

            RunProperties runProperties116 = new RunProperties();
            RunFonts runFonts164 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties116.Append(runFonts164);
            Text text116 = new Text();
            text116.Text = "n sarara maamilichaa qorachuu f";

            run116.Append(runProperties116);
            run116.Append(text116);

            Run run117 = new Run();

            RunProperties runProperties117 = new RunProperties();
            RunFonts runFonts165 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties117.Append(runFonts165);
            Text text117 = new Text();
            text117.Text = "i sakata’uudhaaf taayitaa ni qaba.";

            run117.Append(runProperties117);
            run117.Append(text117);

            paragraph52.Append(paragraphProperties52);
            paragraph52.Append(run115);
            paragraph52.Append(run116);
            paragraph52.Append(run117);

            Paragraph paragraph53 = new Paragraph() { RsidParagraphAddition = "002A4B84", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002A4B84" };

            ParagraphProperties paragraphProperties53 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties53 = new ParagraphMarkRunProperties();
            RunFonts runFonts166 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties53.Append(runFonts166);

            paragraphProperties53.Append(paragraphMarkRunProperties53);

            Run run118 = new Run();

            RunProperties runProperties118 = new RunProperties();
            RunFonts runFonts167 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties118.Append(runFonts167);
            Text text118 = new Text();
            text118.Text = "5.2. Haalli sarara maamilichaa itti hojjatame bishaan bii";

            run118.Append(runProperties118);
            run118.Append(text118);

            Run run119 = new Run() { RsidRunAddition = "00C274BE" };

            RunProperties runProperties119 = new RunProperties();
            RunFonts runFonts168 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties119.Append(runFonts168);
            Text text119 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text119.Text = "roodhaan raabsamu ";

            run119.Append(runProperties119);
            run119.Append(text119);

            Run run120 = new Run();

            RunProperties runProperties120 = new RunProperties();
            RunFonts runFonts169 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties120.Append(runFonts169);
            Text text120 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text120.Text = "kan midhu ykn ureessu ta’e yoo argame Biiroon maamilicha irraa tajaajila bishaanii dhaabu danda’a ";

            run120.Append(runProperties120);
            run120.Append(text120);

            paragraph53.Append(paragraphProperties53);
            paragraph53.Append(run118);
            paragraph53.Append(run119);
            paragraph53.Append(run120);

            Paragraph paragraph54 = new Paragraph() { RsidParagraphAddition = "003C6204", RsidParagraphProperties = "003C6204", RsidRunAdditionDefault = "003C6204" };

            ParagraphProperties paragraphProperties54 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties54 = new ParagraphMarkRunProperties();
            RunFonts runFonts170 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties54.Append(runFonts170);

            paragraphProperties54.Append(paragraphMarkRunProperties54);

            Run run121 = new Run();

            RunProperties runProperties121 = new RunProperties();
            RunFonts runFonts171 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties121.Append(runFonts171);
            Text text121 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text121.Text = "5.3. Maamili kamiyyuu kuusaa bishaani mana jireenyaf yoo ta’e kan abbaan m3 tokko (1) mana daldalaa yoo ta’e abbaam3 lama (2-3) ol ";

            run121.Append(runProperties121);
            run121.Append(text121);

            paragraph54.Append(paragraphProperties54);
            paragraph54.Append(run121);

            Paragraph paragraph55 = new Paragraph() { RsidParagraphAddition = "003C6204", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00376E76" };

            ParagraphProperties paragraphProperties55 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties55 = new ParagraphMarkRunProperties();
            RunFonts runFonts172 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties55.Append(runFonts172);

            paragraphProperties55.Append(paragraphMarkRunProperties55);

            Run run122 = new Run();

            RunProperties runProperties122 = new RunProperties();
            RunFonts runFonts173 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties122.Append(runFonts173);
            Text text122 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text122.Text = "7. ";

            run122.Append(runProperties122);
            run122.Append(text122);

            Run run123 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties123 = new RunProperties();
            RunFonts runFonts174 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold20 = new Bold();

            runProperties123.Append(runFonts174);
            runProperties123.Append(bold20);
            Text text123 = new Text();
            text123.Text = "Haala waligalteen itti digamu";

            run123.Append(runProperties123);
            run123.Append(text123);

            Run run124 = new Run();

            RunProperties runProperties124 = new RunProperties();
            RunFonts runFonts175 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties124.Append(runFonts175);
            Text text124 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text124.Text = " ";

            run124.Append(runProperties124);
            run124.Append(text124);

            paragraph55.Append(paragraphProperties55);
            paragraph55.Append(run122);
            paragraph55.Append(run123);
            paragraph55.Append(run124);

            Paragraph paragraph56 = new Paragraph() { RsidParagraphAddition = "00376E76", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00376E76" };

            ParagraphProperties paragraphProperties56 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties56 = new ParagraphMarkRunProperties();
            RunFonts runFonts176 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties56.Append(runFonts176);

            paragraphProperties56.Append(paragraphMarkRunProperties56);

            Run run125 = new Run();

            RunProperties runProperties125 = new RunProperties();
            RunFonts runFonts177 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties125.Append(runFonts177);
            Text text125 = new Text();
            text125.Text = "7.1. Tajaajila bishaanii ilaalchise waligalteen kam iyyuu maamilichii akka digamu bareesuudhaan hanga beeksisuti hojii irra ni olia maamilichii barreeffamaan yoo beeksise ykn yoo gaaffate tajaajili bishaanii yoosudhaabata, waliigalteenis ni digamma.";

            run125.Append(runProperties125);
            run125.Append(text125);

            paragraph56.Append(paragraphProperties56);
            paragraph56.Append(run125);

            Paragraph paragraph57 = new Paragraph() { RsidParagraphAddition = "00376E76", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00376E76" };

            ParagraphProperties paragraphProperties57 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties57 = new ParagraphMarkRunProperties();
            RunFonts runFonts178 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties57.Append(runFonts178);

            paragraphProperties57.Append(paragraphMarkRunProperties57);

            Run run126 = new Run();

            RunProperties runProperties126 = new RunProperties();
            RunFonts runFonts179 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties126.Append(runFonts179);
            Text text126 = new Text();
            text126.Text = "7.2 Hfaala keewwata 4 Lakk. 4.8 irratti ibsameen tajaajili bishaanii erga dhaabatee guyyaa soddoma(30) keessatti maamilichii kaffalti irraa barbaadamu yoo kafalu baate walii galteen ni digamma.";

            run126.Append(runProperties126);
            run126.Append(text126);

            paragraph57.Append(paragraphProperties57);
            paragraph57.Append(run126);

            Paragraph paragraph58 = new Paragraph() { RsidParagraphAddition = "00376E76", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00376E76" };

            ParagraphProperties paragraphProperties58 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties58 = new ParagraphMarkRunProperties();
            RunFonts runFonts180 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties58.Append(runFonts180);

            paragraphProperties58.Append(paragraphMarkRunProperties58);

            Run run127 = new Run();

            RunProperties runProperties127 = new RunProperties();
            RunFonts runFonts181 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties127.Append(runFonts181);
            Text text127 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text127.Text = "7.3. ";

            run127.Append(runProperties127);
            run127.Append(text127);

            Run run128 = new Run() { RsidRunAddition = "008E4CA0" };

            RunProperties runProperties128 = new RunProperties();
            RunFonts runFonts182 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties128.Append(runFonts182);
            Text text128 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text128.Text = "Eerga waligalteen digame qabdiin maamilichaa ni deebi’aa herregni maamilichaa ";

            run128.Append(runProperties128);
            run128.Append(text128);

            Run run129 = new Run() { RsidRunAddition = "008E4CA0" };

            RunProperties runProperties129 = new RunProperties();
            RunFonts runFonts183 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties129.Append(runFonts183);
            LastRenderedPageBreak lastRenderedPageBreak6 = new LastRenderedPageBreak();
            Text text129 = new Text();
            text129.Text = "irraa barbaaddamu yoo jirate qabdii irra herregame ni hira’aa (kutama) haata’uu malee qabdiin kan maallaqa barbaadamu kafaluf kan hin dandeenye yoo ta’e mammilichi garaa garimaa kan irra barbaadamu akka kafalu ni gaafatama. Kafalti kanaaf fedhi yoo qabaachuu baate seeraan ni gaatama.";

            run129.Append(runProperties129);
            run129.Append(lastRenderedPageBreak6);
            run129.Append(text129);

            paragraph58.Append(paragraphProperties58);
            paragraph58.Append(run127);
            paragraph58.Append(run128);
            paragraph58.Append(run129);

            Paragraph paragraph59 = new Paragraph() { RsidParagraphAddition = "008E4CA0", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00C97465" };

            ParagraphProperties paragraphProperties59 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties59 = new ParagraphMarkRunProperties();
            RunFonts runFonts184 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties59.Append(runFonts184);

            paragraphProperties59.Append(paragraphMarkRunProperties59);

            Run run130 = new Run();

            RunProperties runProperties130 = new RunProperties();
            RunFonts runFonts185 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties130.Append(runFonts185);
            Text text130 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text130.Text = "8. ";

            run130.Append(runProperties130);
            run130.Append(text130);

            Run run131 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties131 = new RunProperties();
            RunFonts runFonts186 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold21 = new Bold();

            runProperties131.Append(runFonts186);
            runProperties131.Append(bold21);
            Text text131 = new Text();
            text131.Text = "Waliigaltee Foyyeessuu";

            run131.Append(runProperties131);
            run131.Append(text131);

            paragraph59.Append(paragraphProperties59);
            paragraph59.Append(run130);
            paragraph59.Append(run131);

            Paragraph paragraph60 = new Paragraph() { RsidParagraphMarkRevision = "000B426F", RsidParagraphAddition = "00C97465", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00C97465" };

            ParagraphProperties paragraphProperties60 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties60 = new ParagraphMarkRunProperties();
            RunFonts runFonts187 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties60.Append(runFonts187);

            paragraphProperties60.Append(paragraphMarkRunProperties60);

            Run run132 = new Run();

            RunProperties runProperties132 = new RunProperties();
            RunFonts runFonts188 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties132.Append(runFonts188);
            Text text132 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text132.Text = "8.1. Biiroon waliigaltee kana foyyeessuf ";

            run132.Append(runProperties132);
            run132.Append(text132);

            Run run133 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties133 = new RunProperties();
            RunFonts runFonts189 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties133.Append(runFonts189);
            Text text133 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text133.Text = "taa’tan ni qaba. ";

            run133.Append(runProperties133);
            run133.Append(text133);

            paragraph60.Append(paragraphProperties60);
            paragraph60.Append(run132);
            paragraph60.Append(run133);

            Paragraph paragraph61 = new Paragraph() { RsidParagraphAddition = "002C3824", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002C3824" };

            ParagraphProperties paragraphProperties61 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties61 = new ParagraphMarkRunProperties();
            RunFonts runFonts190 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold22 = new Bold();

            paragraphMarkRunProperties61.Append(runFonts190);
            paragraphMarkRunProperties61.Append(bold22);

            paragraphProperties61.Append(paragraphMarkRunProperties61);

            Run run134 = new Run() { RsidRunProperties = "002C3824" };

            RunProperties runProperties134 = new RunProperties();
            RunFonts runFonts191 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties134.Append(runFonts191);
            Text text134 = new Text();
            text134.Text = "8.2.";

            run134.Append(runProperties134);
            run134.Append(text134);

            Run run135 = new Run();

            RunProperties runProperties135 = new RunProperties();
            RunFonts runFonts192 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold23 = new Bold();

            runProperties135.Append(runFonts192);
            runProperties135.Append(bold23);
            Text text135 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text135.Text = " ";

            run135.Append(runProperties135);
            run135.Append(text135);

            Run run136 = new Run() { RsidRunProperties = "002C3824" };

            RunProperties runProperties136 = new RunProperties();
            RunFonts runFonts193 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties136.Append(runFonts193);
            Text text136 = new Text();
            text136.Text = "Biiroon yeroo-yeeroodhaan mirkanneesse kan baasu kan akka kaffalti tajaajilaa sirna fi danbii adda addaa kabajuf akka taarfa harawaatin kaffalu maamilichi walii-galtee jira";

            run136.Append(runProperties136);
            run136.Append(text136);

            Run run137 = new Run();

            RunProperties runProperties137 = new RunProperties();
            RunFonts runFonts194 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold24 = new Bold();

            runProperties137.Append(runFonts194);
            runProperties137.Append(bold24);
            Text text137 = new Text();
            text137.Text = ".";

            run137.Append(runProperties137);
            run137.Append(text137);

            paragraph61.Append(paragraphProperties61);
            paragraph61.Append(run134);
            paragraph61.Append(run135);
            paragraph61.Append(run136);
            paragraph61.Append(run137);

            Paragraph paragraph62 = new Paragraph() { RsidParagraphMarkRevision = "002C3824", RsidParagraphAddition = "002C3824", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002C3824" };

            ParagraphProperties paragraphProperties62 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties62 = new ParagraphMarkRunProperties();
            RunFonts runFonts195 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties62.Append(runFonts195);

            paragraphProperties62.Append(paragraphMarkRunProperties62);

            Run run138 = new Run() { RsidRunProperties = "002C3824" };

            RunProperties runProperties138 = new RunProperties();
            RunFonts runFonts196 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties138.Append(runFonts196);
            Text text138 = new Text();
            text138.Text = "8.3. Maamilichi foyyeessa waligaltee biiroodhaan dhi-yate fudhachu yoo baate of eeggannoo bira osoo hin keenamin biiroon waligaltee digu ni danda’a.";

            run138.Append(runProperties138);
            run138.Append(text138);

            paragraph62.Append(paragraphProperties62);
            paragraph62.Append(run138);

            Paragraph paragraph63 = new Paragraph() { RsidParagraphAddition = "002C3824", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002C3824" };

            ParagraphProperties paragraphProperties63 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties63 = new ParagraphMarkRunProperties();
            RunFonts runFonts197 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties63.Append(runFonts197);

            paragraphProperties63.Append(paragraphMarkRunProperties63);

            Run run139 = new Run() { RsidRunProperties = "002C3824" };

            RunProperties runProperties139 = new RunProperties();
            RunFonts runFonts198 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties139.Append(runFonts198);
            Text text139 = new Text();
            text139.Text = "8.4. Biiroon tarkaamfi keewwata 8 Lakk 8.2 jalati ibsame yoo fudhatu haala mijaawaa ta’een maamilichaaf ni ibsa.";

            run139.Append(runProperties139);
            run139.Append(text139);

            paragraph63.Append(paragraphProperties63);
            paragraph63.Append(run139);

            Paragraph paragraph64 = new Paragraph() { RsidParagraphAddition = "002C3824", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "002C3824" };

            ParagraphProperties paragraphProperties64 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties64 = new ParagraphMarkRunProperties();
            RunFonts runFonts199 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties64.Append(runFonts199);

            paragraphProperties64.Append(paragraphMarkRunProperties64);

            Run run140 = new Run();

            RunProperties runProperties140 = new RunProperties();
            RunFonts runFonts200 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties140.Append(runFonts200);
            Text text140 = new Text();
            text140.Text = "Waliigalteen kun guyyaa qama lamaanin maallatt’ee jalqabee kan mirkanaa’edha.";

            run140.Append(runProperties140);
            run140.Append(text140);

            paragraph64.Append(paragraphProperties64);
            paragraph64.Append(run140);

            Paragraph paragraph65 = new Paragraph() { RsidParagraphAddition = "003766EB", RsidParagraphProperties = "003766EB", RsidRunAdditionDefault = "003766EB" };

            ParagraphProperties paragraphProperties65 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties65 = new ParagraphMarkRunProperties();
            RunFonts runFonts201 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties65.Append(runFonts201);

            paragraphProperties65.Append(paragraphMarkRunProperties65);

            Run run141 = new Run();

            RunProperties runProperties141 = new RunProperties();
            RunFonts runFonts202 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties141.Append(runFonts202);
            Text text141 = new Text();
            text141.Text = "ijaarun hin hayyamamuf kuusiichis iddoo ka’ee fi ilaalchaaf tolu irratti ijaaramu qaba.";

            run141.Append(runProperties141);
            run141.Append(text141);

            paragraph65.Append(paragraphProperties65);
            paragraph65.Append(run141);

            Paragraph paragraph66 = new Paragraph() { RsidParagraphAddition = "003766EB", RsidParagraphProperties = "003766EB", RsidRunAdditionDefault = "003766EB" };

            ParagraphProperties paragraphProperties66 = new ParagraphProperties();

            ParagraphMarkRunProperties paragraphMarkRunProperties66 = new ParagraphMarkRunProperties();
            RunFonts runFonts203 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties66.Append(runFonts203);

            paragraphProperties66.Append(paragraphMarkRunProperties66);

            Run run142 = new Run();

            RunProperties runProperties142 = new RunProperties();
            RunFonts runFonts204 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties142.Append(runFonts204);
            Text text142 = new Text();
            text142.Text = "6";

            run142.Append(runProperties142);
            run142.Append(text142);

            Run run143 = new Run() { RsidRunProperties = "000B426F" };

            RunProperties runProperties143 = new RunProperties();
            RunFonts runFonts205 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold25 = new Bold();

            runProperties143.Append(runFonts205);
            runProperties143.Append(bold25);
            Text text143 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text143.Text = ". Haaromsu Tajaajila Bishaan Dhaabate (ciitee):- ";

            run143.Append(runProperties143);
            run143.Append(text143);

            Run run144 = new Run();

            RunProperties runProperties144 = new RunProperties();
            RunFonts runFonts206 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties144.Append(runFonts206);
            Text text144 = new Text();
            text144.Text = "Iyyannoon barbaachisaa ta’ee yoo dhiyaate taja";

            run144.Append(runProperties144);
            run144.Append(text144);

            Run run145 = new Run() { RsidRunAddition = "00C26F1A" };

            RunProperties runProperties145 = new RunProperties();
            RunFonts runFonts207 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties145.Append(runFonts207);
            Text text145 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text145.Text = "jila bishaan dhaabate (ciitee) ";

            run145.Append(runProperties145);
            run145.Append(text145);

            Run run146 = new Run();

            RunProperties runProperties146 = new RunProperties();
            RunFonts runFonts208 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties146.Append(runFonts208);
            Text text146 = new Text();
            text146.Text = "haaromsun ni danda’ama. Haata’u malee, tajaajili kan haaromsamu sababani tajaajili cite yoo siraa’ee fi akkasumas herreegni barbaachisu hundii gutamee erga kafalmee booda qofa. Dhimma, kaffaltti gaati bishaanitin tajaajila cite debissaanii dhaabudhaaf mamilichi adaba qarshii 30.00(Soddomma) jni kafala.";

            run146.Append(runProperties146);
            run146.Append(text146);

            paragraph66.Append(paragraphProperties66);
            paragraph66.Append(run142);
            paragraph66.Append(run143);
            paragraph66.Append(run144);
            paragraph66.Append(run145);
            paragraph66.Append(run146);

            Table table1 = new Table();

            TableProperties tableProperties1 = new TableProperties();
            TableStyle tableStyle1 = new TableStyle() { Val = "TableGrid" };
            TableWidth tableWidth1 = new TableWidth() { Width = "0", Type = TableWidthUnitValues.Auto };

            TableBorders tableBorders1 = new TableBorders();
            TopBorder topBorder1 = new TopBorder() { Val = BorderValues.None, Color = "auto", Size = (UInt32Value)0U, Space = (UInt32Value)0U };
            LeftBorder leftBorder1 = new LeftBorder() { Val = BorderValues.None, Color = "auto", Size = (UInt32Value)0U, Space = (UInt32Value)0U };
            BottomBorder bottomBorder1 = new BottomBorder() { Val = BorderValues.None, Color = "auto", Size = (UInt32Value)0U, Space = (UInt32Value)0U };
            RightBorder rightBorder1 = new RightBorder() { Val = BorderValues.None, Color = "auto", Size = (UInt32Value)0U, Space = (UInt32Value)0U };
            InsideHorizontalBorder insideHorizontalBorder1 = new InsideHorizontalBorder() { Val = BorderValues.None, Color = "auto", Size = (UInt32Value)0U, Space = (UInt32Value)0U };
            InsideVerticalBorder insideVerticalBorder1 = new InsideVerticalBorder() { Val = BorderValues.None, Color = "auto", Size = (UInt32Value)0U, Space = (UInt32Value)0U };

            tableBorders1.Append(topBorder1);
            tableBorders1.Append(leftBorder1);
            tableBorders1.Append(bottomBorder1);
            tableBorders1.Append(rightBorder1);
            tableBorders1.Append(insideHorizontalBorder1);
            tableBorders1.Append(insideVerticalBorder1);
            TableLook tableLook1 = new TableLook() { Val = "04A0" };

            tableProperties1.Append(tableStyle1);
            tableProperties1.Append(tableWidth1);
            tableProperties1.Append(tableBorders1);
            tableProperties1.Append(tableLook1);

            TableGrid tableGrid1 = new TableGrid();
            GridColumn gridColumn1 = new GridColumn() { Width = "2178" };
            GridColumn gridColumn2 = new GridColumn() { Width = "671" };
            GridColumn gridColumn3 = new GridColumn() { Width = "1687" };

            tableGrid1.Append(gridColumn1);
            tableGrid1.Append(gridColumn2);
            tableGrid1.Append(gridColumn3);

            TableRow tableRow1 = new TableRow() { RsidTableRowAddition = "00BB36BD", RsidTableRowProperties = "00BB36BD" };

            TableCell tableCell1 = new TableCell();

            TableCellProperties tableCellProperties1 = new TableCellProperties();
            TableCellWidth tableCellWidth1 = new TableCellWidth() { Width = "2178", Type = TableWidthUnitValues.Dxa };

            TableCellBorders tableCellBorders1 = new TableCellBorders();
            BottomBorder bottomBorder2 = new BottomBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };

            tableCellBorders1.Append(bottomBorder2);

            tableCellProperties1.Append(tableCellWidth1);
            tableCellProperties1.Append(tableCellBorders1);

            Paragraph paragraph67 = new Paragraph() { RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "005D6182", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties67 = new ParagraphProperties();

            Tabs tabs22 = new Tabs();
            TabStop tabStop22 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs22.Append(tabStop22);

            ParagraphMarkRunProperties paragraphMarkRunProperties67 = new ParagraphMarkRunProperties();
            RunFonts runFonts209 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize45 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties67.Append(runFonts209);
            paragraphMarkRunProperties67.Append(fontSize45);

            paragraphProperties67.Append(tabs22);
            paragraphProperties67.Append(paragraphMarkRunProperties67);

            paragraph67.Append(paragraphProperties67);

            tableCell1.Append(tableCellProperties1);
            tableCell1.Append(paragraph67);

            TableCell tableCell2 = new TableCell();

            TableCellProperties tableCellProperties2 = new TableCellProperties();
            TableCellWidth tableCellWidth2 = new TableCellWidth() { Width = "671", Type = TableWidthUnitValues.Dxa };

            tableCellProperties2.Append(tableCellWidth2);

            Paragraph paragraph68 = new Paragraph() { RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "005D6182", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties68 = new ParagraphProperties();

            Tabs tabs23 = new Tabs();
            TabStop tabStop23 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs23.Append(tabStop23);

            ParagraphMarkRunProperties paragraphMarkRunProperties68 = new ParagraphMarkRunProperties();
            RunFonts runFonts210 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize46 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties68.Append(runFonts210);
            paragraphMarkRunProperties68.Append(fontSize46);

            paragraphProperties68.Append(tabs23);
            paragraphProperties68.Append(paragraphMarkRunProperties68);

            paragraph68.Append(paragraphProperties68);

            tableCell2.Append(tableCellProperties2);
            tableCell2.Append(paragraph68);

            TableCell tableCell3 = new TableCell();

            TableCellProperties tableCellProperties3 = new TableCellProperties();
            TableCellWidth tableCellWidth3 = new TableCellWidth() { Width = "1687", Type = TableWidthUnitValues.Dxa };

            TableCellBorders tableCellBorders2 = new TableCellBorders();
            BottomBorder bottomBorder3 = new BottomBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };

            tableCellBorders2.Append(bottomBorder3);

            tableCellProperties3.Append(tableCellWidth3);
            tableCellProperties3.Append(tableCellBorders2);

            Paragraph paragraph69 = new Paragraph() { RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "005D6182", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties69 = new ParagraphProperties();

            Tabs tabs24 = new Tabs();
            TabStop tabStop24 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs24.Append(tabStop24);

            ParagraphMarkRunProperties paragraphMarkRunProperties69 = new ParagraphMarkRunProperties();
            RunFonts runFonts211 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize47 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties69.Append(runFonts211);
            paragraphMarkRunProperties69.Append(fontSize47);

            paragraphProperties69.Append(tabs24);
            paragraphProperties69.Append(paragraphMarkRunProperties69);

            paragraph69.Append(paragraphProperties69);

            tableCell3.Append(tableCellProperties3);
            tableCell3.Append(paragraph69);

            tableRow1.Append(tableCell1);
            tableRow1.Append(tableCell2);
            tableRow1.Append(tableCell3);

            TableRow tableRow2 = new TableRow() { RsidTableRowAddition = "00BB36BD", RsidTableRowProperties = "00BB36BD" };

            TableCell tableCell4 = new TableCell();

            TableCellProperties tableCellProperties4 = new TableCellProperties();
            TableCellWidth tableCellWidth4 = new TableCellWidth() { Width = "2178", Type = TableWidthUnitValues.Dxa };

            TableCellBorders tableCellBorders3 = new TableCellBorders();
            TopBorder topBorder2 = new TopBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };

            tableCellBorders3.Append(topBorder2);

            tableCellProperties4.Append(tableCellWidth4);
            tableCellProperties4.Append(tableCellBorders3);

            Paragraph paragraph70 = new Paragraph() { RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "00BB36BD", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties70 = new ParagraphProperties();

            Tabs tabs25 = new Tabs();
            TabStop tabStop25 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs25.Append(tabStop25);
            Justification justification4 = new Justification() { Val = JustificationValues.Center };

            ParagraphMarkRunProperties paragraphMarkRunProperties70 = new ParagraphMarkRunProperties();
            RunFonts runFonts212 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize48 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties70.Append(runFonts212);
            paragraphMarkRunProperties70.Append(fontSize48);

            paragraphProperties70.Append(tabs25);
            paragraphProperties70.Append(justification4);
            paragraphProperties70.Append(paragraphMarkRunProperties70);

            Run run147 = new Run() { RsidRunProperties = "008200EF" };

            RunProperties runProperties147 = new RunProperties();
            RunFonts runFonts213 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize49 = new FontSize() { Val = "20" };

            runProperties147.Append(runFonts213);
            runProperties147.Append(fontSize49);
            Text text147 = new Text();
            text147.Text = "Maamila";

            run147.Append(runProperties147);
            run147.Append(text147);

            paragraph70.Append(paragraphProperties70);
            paragraph70.Append(run147);

            tableCell4.Append(tableCellProperties4);
            tableCell4.Append(paragraph70);

            TableCell tableCell5 = new TableCell();

            TableCellProperties tableCellProperties5 = new TableCellProperties();
            TableCellWidth tableCellWidth5 = new TableCellWidth() { Width = "671", Type = TableWidthUnitValues.Dxa };

            tableCellProperties5.Append(tableCellWidth5);

            Paragraph paragraph71 = new Paragraph() { RsidParagraphMarkRevision = "008200EF", RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "00BB36BD", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties71 = new ParagraphProperties();

            Tabs tabs26 = new Tabs();
            TabStop tabStop26 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs26.Append(tabStop26);
            Justification justification5 = new Justification() { Val = JustificationValues.Center };

            ParagraphMarkRunProperties paragraphMarkRunProperties71 = new ParagraphMarkRunProperties();
            RunFonts runFonts214 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize50 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties71.Append(runFonts214);
            paragraphMarkRunProperties71.Append(fontSize50);

            paragraphProperties71.Append(tabs26);
            paragraphProperties71.Append(justification5);
            paragraphProperties71.Append(paragraphMarkRunProperties71);

            paragraph71.Append(paragraphProperties71);

            tableCell5.Append(tableCellProperties5);
            tableCell5.Append(paragraph71);

            TableCell tableCell6 = new TableCell();

            TableCellProperties tableCellProperties6 = new TableCellProperties();
            TableCellWidth tableCellWidth6 = new TableCellWidth() { Width = "1687", Type = TableWidthUnitValues.Dxa };

            TableCellBorders tableCellBorders4 = new TableCellBorders();
            TopBorder topBorder3 = new TopBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };

            tableCellBorders4.Append(topBorder3);

            tableCellProperties6.Append(tableCellWidth6);
            tableCellProperties6.Append(tableCellBorders4);

            Paragraph paragraph72 = new Paragraph() { RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "00BB36BD", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties72 = new ParagraphProperties();

            Tabs tabs27 = new Tabs();
            TabStop tabStop27 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs27.Append(tabStop27);
            Justification justification6 = new Justification() { Val = JustificationValues.Center };

            ParagraphMarkRunProperties paragraphMarkRunProperties72 = new ParagraphMarkRunProperties();
            RunFonts runFonts215 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize51 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties72.Append(runFonts215);
            paragraphMarkRunProperties72.Append(fontSize51);

            paragraphProperties72.Append(tabs27);
            paragraphProperties72.Append(justification6);
            paragraphProperties72.Append(paragraphMarkRunProperties72);

            Run run148 = new Run() { RsidRunProperties = "008200EF" };

            RunProperties runProperties148 = new RunProperties();
            RunFonts runFonts216 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize52 = new FontSize() { Val = "20" };

            runProperties148.Append(runFonts216);
            runProperties148.Append(fontSize52);
            Text text148 = new Text();
            text148.Text = "Waajjira Tajaajila Bish.Dhu. Fi Dha. Maggaalaa Bakka Hoggana Biiroo";

            run148.Append(runProperties148);
            run148.Append(text148);

            paragraph72.Append(paragraphProperties72);
            paragraph72.Append(run148);

            tableCell6.Append(tableCellProperties6);
            tableCell6.Append(paragraph72);

            tableRow2.Append(tableCell4);
            tableRow2.Append(tableCell5);
            tableRow2.Append(tableCell6);

            table1.Append(tableProperties1);
            table1.Append(tableGrid1);
            table1.Append(tableRow1);
            table1.Append(tableRow2);

            Paragraph paragraph73 = new Paragraph() { RsidParagraphAddition = "00BB36BD", RsidParagraphProperties = "005D6182", RsidRunAdditionDefault = "00BB36BD" };

            ParagraphProperties paragraphProperties73 = new ParagraphProperties();

            Tabs tabs28 = new Tabs();
            TabStop tabStop28 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs28.Append(tabStop28);

            ParagraphMarkRunProperties paragraphMarkRunProperties73 = new ParagraphMarkRunProperties();
            RunFonts runFonts217 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize53 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties73.Append(runFonts217);
            paragraphMarkRunProperties73.Append(fontSize53);

            paragraphProperties73.Append(tabs28);
            paragraphProperties73.Append(paragraphMarkRunProperties73);

            paragraph73.Append(paragraphProperties73);

            Paragraph paragraph74 = new Paragraph() { RsidParagraphAddition = "00E25FD2", RsidParagraphProperties = "00740E33", RsidRunAdditionDefault = "00F5457D" };

            ParagraphProperties paragraphProperties74 = new ParagraphProperties();

            Tabs tabs29 = new Tabs();
            TabStop tabStop29 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs29.Append(tabStop29);

            ParagraphMarkRunProperties paragraphMarkRunProperties74 = new ParagraphMarkRunProperties();
            RunFonts runFonts218 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize54 = new FontSize() { Val = "20" };

            paragraphMarkRunProperties74.Append(runFonts218);
            paragraphMarkRunProperties74.Append(fontSize54);

            paragraphProperties74.Append(tabs29);
            paragraphProperties74.Append(paragraphMarkRunProperties74);

            Run run149 = new Run();

            RunProperties runProperties149 = new RunProperties();
            RunFonts runFonts219 = new RunFonts() { ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            FontSize fontSize55 = new FontSize() { Val = "20" };

            runProperties149.Append(runFonts219);
            runProperties149.Append(fontSize55);
            LastRenderedPageBreak lastRenderedPageBreak7 = new LastRenderedPageBreak();
            Text text149 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text149.Text = "                    ";

            run149.Append(runProperties149);
            run149.Append(lastRenderedPageBreak7);
            run149.Append(text149);

            paragraph74.Append(paragraphProperties74);
            paragraph74.Append(run149);

            Paragraph paragraph75 = new Paragraph() { RsidParagraphAddition = "00DF151F", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00F62C39" };

            ParagraphProperties paragraphProperties75 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId13 = new ParagraphStyleId() { Val = "ListParagraph" };

            Tabs tabs30 = new Tabs();
            TabStop tabStop30 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs30.Append(tabStop30);
            Indentation indentation3 = new Indentation() { Start = "180" };

            ParagraphMarkRunProperties paragraphMarkRunProperties75 = new ParagraphMarkRunProperties();
            RunFonts runFonts220 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties75.Append(runFonts220);

            paragraphProperties75.Append(paragraphStyleId13);
            paragraphProperties75.Append(tabs30);
            paragraphProperties75.Append(indentation3);
            paragraphProperties75.Append(paragraphMarkRunProperties75);

            Run run150 = new Run();

            RunProperties runProperties150 = new RunProperties();
            RunFonts runFonts221 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties150.Append(runFonts221);
            Text text150 = new Text();
            text150.Text = "¾SÖØ ¨<H ›p`xƒ ¨M";

            run150.Append(runProperties150);
            run150.Append(text150);

            paragraph75.Append(paragraphProperties75);
            paragraph75.Append(run150);

            Paragraph paragraph76 = new Paragraph() { RsidParagraphAddition = "00F62C39", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00F62C39" };

            ParagraphProperties paragraphProperties76 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId14 = new ParagraphStyleId() { Val = "ListParagraph" };

            Tabs tabs31 = new Tabs();
            TabStop tabStop31 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs31.Append(tabStop31);
            Indentation indentation4 = new Indentation() { Start = "180" };

            ParagraphMarkRunProperties paragraphMarkRunProperties76 = new ParagraphMarkRunProperties();
            RunFonts runFonts222 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties76.Append(runFonts222);

            paragraphProperties76.Append(paragraphStyleId14);
            paragraphProperties76.Append(tabs31);
            paragraphProperties76.Append(indentation4);
            paragraphProperties76.Append(paragraphMarkRunProperties76);

            Run run151 = new Run();

            RunProperties runProperties151 = new RunProperties();
            RunFonts runFonts223 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties151.Append(runFonts223);
            Text text151 = new Text();
            text151.Text = "ÃI ¨<M ³_-------------------------------k” 20";

            run151.Append(runProperties151);
            run151.Append(text151);

            paragraph76.Append(paragraphProperties76);
            paragraph76.Append(run151);

            Paragraph paragraph77 = new Paragraph() { RsidParagraphAddition = "00F62C39", RsidParagraphProperties = "00F62C39", RsidRunAdditionDefault = "00F62C39" };

            ParagraphProperties paragraphProperties77 = new ParagraphProperties();

            Tabs tabs32 = new Tabs();
            TabStop tabStop32 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs32.Append(tabStop32);

            ParagraphMarkRunProperties paragraphMarkRunProperties77 = new ParagraphMarkRunProperties();
            RunFonts runFonts224 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties77.Append(runFonts224);

            paragraphProperties77.Append(tabs32);
            paragraphProperties77.Append(paragraphMarkRunProperties77);

            Run run152 = new Run();

            RunProperties runProperties152 = new RunProperties();
            RunFonts runFonts225 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties152.Append(runFonts225);
            Text text152 = new Text();
            text152.Text = "¯.U Ÿ²=I";

            run152.Append(runProperties152);
            run152.Append(text152);

            Run run153 = new Run();

            RunProperties runProperties153 = new RunProperties();
            RunFonts runFonts226 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties153.Append(runFonts226);
            Text text153 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text153.Text = " በታች በኦሮሚያ የውሃ ሃብት ቢሮ እና ደንበኛ እየተባለ በሚጠሩ በአቶ/ሮ/ወ/ት ------------------------------ መካከል የውሃ አገልግሎት በመስጠት እና ደንበኛውም በአገልግሎቱ ለመጠቀምና የአገልግሎቱንም ዋገ ለመከፈል የተደረገ ውል ነው፡፡";

            run153.Append(runProperties153);
            run153.Append(text153);

            paragraph77.Append(paragraphProperties77);
            paragraph77.Append(run152);
            paragraph77.Append(run153);

            Paragraph paragraph78 = new Paragraph() { RsidParagraphMarkRevision = "00A13E70", RsidParagraphAddition = "00F62C39", RsidParagraphProperties = "00A76263", RsidRunAdditionDefault = "00F62C39" };

            ParagraphProperties paragraphProperties78 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId15 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties12 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference12 = new NumberingLevelReference() { Val = 0 };
            NumberingId numberingId12 = new NumberingId() { Val = 4 };

            numberingProperties12.Append(numberingLevelReference12);
            numberingProperties12.Append(numberingId12);

            Tabs tabs33 = new Tabs();
            TabStop tabStop33 = new TabStop() { Val = TabStopValues.Left, Position = 270 };

            tabs33.Append(tabStop33);
            Indentation indentation5 = new Indentation() { Hanging = "720" };

            ParagraphMarkRunProperties paragraphMarkRunProperties78 = new ParagraphMarkRunProperties();
            RunFonts runFonts227 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold26 = new Bold();

            paragraphMarkRunProperties78.Append(runFonts227);
            paragraphMarkRunProperties78.Append(bold26);

            paragraphProperties78.Append(paragraphStyleId15);
            paragraphProperties78.Append(numberingProperties12);
            paragraphProperties78.Append(tabs33);
            paragraphProperties78.Append(indentation5);
            paragraphProperties78.Append(paragraphMarkRunProperties78);

            Run run154 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties154 = new RunProperties();
            RunFonts runFonts228 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold27 = new Bold();

            runProperties154.Append(runFonts228);
            runProperties154.Append(bold27);
            Text text154 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text154.Text = "ትርጋሜ ";

            run154.Append(runProperties154);
            run154.Append(text154);

            paragraph78.Append(paragraphProperties78);
            paragraph78.Append(run154);

            Paragraph paragraph79 = new Paragraph() { RsidParagraphAddition = "00F62C39", RsidParagraphProperties = "00A76263", RsidRunAdditionDefault = "00A76263" };

            ParagraphProperties paragraphProperties79 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId16 = new ParagraphStyleId() { Val = "ListParagraph" };

            Tabs tabs34 = new Tabs();
            TabStop tabStop34 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs34.Append(tabStop34);
            Indentation indentation6 = new Indentation() { Start = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties79 = new ParagraphMarkRunProperties();
            RunFonts runFonts229 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties79.Append(runFonts229);

            paragraphProperties79.Append(paragraphStyleId16);
            paragraphProperties79.Append(tabs34);
            paragraphProperties79.Append(indentation6);
            paragraphProperties79.Append(paragraphMarkRunProperties79);

            Run run155 = new Run();

            RunProperties runProperties155 = new RunProperties();
            RunFonts runFonts230 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties155.Append(runFonts230);
            Text text155 = new Text();
            text155.Text = "1.1.";

            run155.Append(runProperties155);
            run155.Append(text155);

            Run run156 = new Run() { RsidRunAddition = "00F62C39" };

            RunProperties runProperties156 = new RunProperties();
            RunFonts runFonts231 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties156.Append(runFonts231);
            Text text156 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text156.Text = "‹‹ውል‹‹ ማለት ደንበኛው የመጠጥ ውሃ አገልግሎት ለማግኘት ከቢሮ ጋር ከዚህ በታች ";

            run156.Append(runProperties156);
            run156.Append(text156);

            Run run157 = new Run();

            RunProperties runProperties157 = new RunProperties();
            RunFonts runFonts232 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties157.Append(runFonts232);
            Text text157 = new Text();
            text157.Text = "ያደረገው ስምምነት ነው፡፡";

            run157.Append(runProperties157);
            run157.Append(text157);

            paragraph79.Append(paragraphProperties79);
            paragraph79.Append(run155);
            paragraph79.Append(run156);
            paragraph79.Append(run157);

            Paragraph paragraph80 = new Paragraph() { RsidParagraphAddition = "00A36FC5", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "00E42C2F" };

            ParagraphProperties paragraphProperties80 = new ParagraphProperties();

            Tabs tabs35 = new Tabs();
            TabStop tabStop35 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs35.Append(tabStop35);

            ParagraphMarkRunProperties paragraphMarkRunProperties80 = new ParagraphMarkRunProperties();
            RunFonts runFonts233 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties80.Append(runFonts233);

            paragraphProperties80.Append(tabs35);
            paragraphProperties80.Append(paragraphMarkRunProperties80);

            Run run158 = new Run();

            RunProperties runProperties158 = new RunProperties();
            RunFonts runFonts234 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties158.Append(runFonts234);
            Text text158 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text158.Text = "1.2. ‹‹ቢሮ‹‹ ማለት የኦሮሚያ የውሃ ሃብት ቢሮ ማለት ";

            run158.Append(runProperties158);
            run158.Append(text158);

            Run run159 = new Run() { RsidRunAddition = "00A846BA" };

            RunProperties runProperties159 = new RunProperties();
            RunFonts runFonts235 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties159.Append(runFonts235);
            Text text159 = new Text();
            text159.Text = "ነው፡፡";

            run159.Append(runProperties159);
            run159.Append(text159);

            paragraph80.Append(paragraphProperties80);
            paragraph80.Append(run158);
            paragraph80.Append(run159);

            Paragraph paragraph81 = new Paragraph() { RsidParagraphAddition = "00E42C2F", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "00E42C2F" };

            ParagraphProperties paragraphProperties81 = new ParagraphProperties();

            Tabs tabs36 = new Tabs();
            TabStop tabStop36 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs36.Append(tabStop36);

            ParagraphMarkRunProperties paragraphMarkRunProperties81 = new ParagraphMarkRunProperties();
            RunFonts runFonts236 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties81.Append(runFonts236);

            paragraphProperties81.Append(tabs36);
            paragraphProperties81.Append(paragraphMarkRunProperties81);

            Run run160 = new Run();

            RunProperties runProperties160 = new RunProperties();
            RunFonts runFonts237 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties160.Append(runFonts237);
            Text text160 = new Text();
            text160.Text = "1.3. ‹‹ደንበኛ‹‹ ማለት የመጠጥ ውሃ አገልግሎት ለማግኘት ከቢሮ ጋር ውል ወይም ስምምነት ያደረገ ማንኛውም ሰው ድርጅት ወይም ተቋም ነው፡፡";

            run160.Append(runProperties160);
            run160.Append(text160);

            paragraph81.Append(paragraphProperties81);
            paragraph81.Append(run160);

            Paragraph paragraph82 = new Paragraph() { RsidParagraphAddition = "001519D2", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "001519D2" };

            ParagraphProperties paragraphProperties82 = new ParagraphProperties();

            Tabs tabs37 = new Tabs();
            TabStop tabStop37 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs37.Append(tabStop37);

            ParagraphMarkRunProperties paragraphMarkRunProperties82 = new ParagraphMarkRunProperties();
            RunFonts runFonts238 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties82.Append(runFonts238);

            paragraphProperties82.Append(tabs37);
            paragraphProperties82.Append(paragraphMarkRunProperties82);

            Run run161 = new Run();

            RunProperties runProperties161 = new RunProperties();
            RunFonts runFonts239 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties161.Append(runFonts239);
            Text text161 = new Text();
            text161.Text = "1.4. ‹‹የውሃ መስመር‹‹ ማለት ንጽህ የመጠጥ ውሃ ለማስተላለፍ በቢሮው የተዘረጋ ወይም እንዲዘረጋ በቢሮው የተፈቀደ ማናቸውም የውሃ ቧንቧና በላዩ የተገጠሙት መሳሪያዎችና መገጣጠሚያዎች ማለት ነው፡፡";

            run161.Append(runProperties161);
            run161.Append(text161);

            paragraph82.Append(paragraphProperties82);
            paragraph82.Append(run161);

            Paragraph paragraph83 = new Paragraph() { RsidParagraphAddition = "00B70886", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "00B70886" };

            ParagraphProperties paragraphProperties83 = new ParagraphProperties();

            Tabs tabs38 = new Tabs();
            TabStop tabStop38 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs38.Append(tabStop38);

            ParagraphMarkRunProperties paragraphMarkRunProperties83 = new ParagraphMarkRunProperties();
            RunFonts runFonts240 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties83.Append(runFonts240);

            paragraphProperties83.Append(tabs38);
            paragraphProperties83.Append(paragraphMarkRunProperties83);

            Run run162 = new Run();

            RunProperties runProperties162 = new RunProperties();
            RunFonts runFonts241 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties162.Append(runFonts241);
            Text text162 = new Text();
            text162.Text = "1.5. ‹‹ቆጣሪ‹‹ ማለት ቢሮው ለደንበኛው የሚሰጠውን የውሃ መጠን የሚለካ መሳሪያ ነው፡፡";

            run162.Append(runProperties162);
            run162.Append(text162);

            paragraph83.Append(paragraphProperties83);
            paragraph83.Append(run162);

            Paragraph paragraph84 = new Paragraph() { RsidParagraphAddition = "00B70886", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "00B70886" };

            ParagraphProperties paragraphProperties84 = new ParagraphProperties();

            Tabs tabs39 = new Tabs();
            TabStop tabStop39 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs39.Append(tabStop39);

            ParagraphMarkRunProperties paragraphMarkRunProperties84 = new ParagraphMarkRunProperties();
            RunFonts runFonts242 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties84.Append(runFonts242);

            paragraphProperties84.Append(tabs39);
            paragraphProperties84.Append(paragraphMarkRunProperties84);

            Run run163 = new Run();

            RunProperties runProperties163 = new RunProperties();
            RunFonts runFonts243 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties163.Append(runFonts243);
            Text text163 = new Text();
            text163.Text = "1.6.‹‹ዋና መስመር‹‹ ማለት ወደ ከተማውና ልዩ ልዩ ቀበሌዎች ወይም ቀጠናዎቸው ውሃ ለማሰራጨት በቢሮው የተዘረጋ የውሃ መስመር ማለት ነው፡፡";

            run163.Append(runProperties163);
            run163.Append(text163);

            paragraph84.Append(paragraphProperties84);
            paragraph84.Append(run163);

            Paragraph paragraph85 = new Paragraph() { RsidParagraphAddition = "00B70886", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "00B70886" };

            ParagraphProperties paragraphProperties85 = new ParagraphProperties();

            Tabs tabs40 = new Tabs();
            TabStop tabStop40 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs40.Append(tabStop40);

            ParagraphMarkRunProperties paragraphMarkRunProperties85 = new ParagraphMarkRunProperties();
            RunFonts runFonts244 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties85.Append(runFonts244);

            paragraphProperties85.Append(tabs40);
            paragraphProperties85.Append(paragraphMarkRunProperties85);

            Run run164 = new Run();

            RunProperties runProperties164 = new RunProperties();
            RunFonts runFonts245 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties164.Append(runFonts245);
            Text text164 = new Text();
            text164.Text = "1.7.‹‹ከቆጣሪ በፊት ያለ መስመሮች‹‹ ማለት ወይም ደንበኛው መስመር ከተቀጠለበት ማናቸውም መስመር ጀምሮ እስከ ቆጣሪው ጫፍ ቆጣሪውን ጨምሮ ድረስ ያለውን የውሃ መስመር ነው፡፡";

            run164.Append(runProperties164);
            run164.Append(text164);

            paragraph85.Append(paragraphProperties85);
            paragraph85.Append(run164);

            Paragraph paragraph86 = new Paragraph() { RsidParagraphAddition = "00B70886", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "0035622F" };

            ParagraphProperties paragraphProperties86 = new ParagraphProperties();

            Tabs tabs41 = new Tabs();
            TabStop tabStop41 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs41.Append(tabStop41);

            ParagraphMarkRunProperties paragraphMarkRunProperties86 = new ParagraphMarkRunProperties();
            RunFonts runFonts246 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties86.Append(runFonts246);

            paragraphProperties86.Append(tabs41);
            paragraphProperties86.Append(paragraphMarkRunProperties86);

            Run run165 = new Run();

            RunProperties runProperties165 = new RunProperties();
            RunFonts runFonts247 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties165.Append(runFonts247);
            Text text165 = new Text();
            text165.Text = "1.8. ‹‹ከቆጣሪው በኃላ ያለ መስመሮች‹‹ ማለት ከቆጣሪ ጀምሮ ለደንኛ የተዘረጋ ውሃ መስመር ማለት ነው፡፡";

            run165.Append(runProperties165);
            run165.Append(text165);

            paragraph86.Append(paragraphProperties86);
            paragraph86.Append(run165);

            Paragraph paragraph87 = new Paragraph() { RsidParagraphAddition = "0035622F", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "0035622F" };

            ParagraphProperties paragraphProperties87 = new ParagraphProperties();

            Tabs tabs42 = new Tabs();
            TabStop tabStop42 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs42.Append(tabStop42);

            ParagraphMarkRunProperties paragraphMarkRunProperties87 = new ParagraphMarkRunProperties();
            RunFonts runFonts248 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            paragraphMarkRunProperties87.Append(runFonts248);

            paragraphProperties87.Append(tabs42);
            paragraphProperties87.Append(paragraphMarkRunProperties87);

            Run run166 = new Run();

            RunProperties runProperties166 = new RunProperties();
            RunFonts runFonts249 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties166.Append(runFonts249);
            LastRenderedPageBreak lastRenderedPageBreak8 = new LastRenderedPageBreak();
            Text text166 = new Text();
            text166.Text = "1.9. ‹‹የውሃ መስመር‹‹ ማሻሻያ‹‹ ማለት የአንድ ደንበ";

            run166.Append(runProperties166);
            run166.Append(lastRenderedPageBreak8);
            run166.Append(text166);

            Run run167 = new Run();

            RunProperties runProperties167 = new RunProperties();
            RunFonts runFonts250 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties167.Append(runFonts250);
            Text text167 = new Text();
            text167.Text = "ኛ መስመር ስፋት በከፍተኛ መስመር ስፋት መለወጥ ማለት ነው፡፡";

            run167.Append(runProperties167);
            run167.Append(text167);

            paragraph87.Append(paragraphProperties87);
            paragraph87.Append(run166);
            paragraph87.Append(run167);

            Paragraph paragraph88 = new Paragraph() { RsidParagraphAddition = "0092470F", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "0092470F" };

            ParagraphProperties paragraphProperties88 = new ParagraphProperties();

            Tabs tabs43 = new Tabs();
            TabStop tabStop43 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs43.Append(tabStop43);

            ParagraphMarkRunProperties paragraphMarkRunProperties88 = new ParagraphMarkRunProperties();
            RunFonts runFonts251 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            paragraphMarkRunProperties88.Append(runFonts251);

            paragraphProperties88.Append(tabs43);
            paragraphProperties88.Append(paragraphMarkRunProperties88);

            Run run168 = new Run();

            RunProperties runProperties168 = new RunProperties();
            RunFonts runFonts252 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties168.Append(runFonts252);
            Text text168 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text168.Text = "1.10. ‹‹የውሃ መስመር ማዛወርም‹‹ ማለት የአንድ ደንበኛ መስመር ከነበረበት አቅጣጫ ወደ ሌላ አቅጣጫ ማዛውረ ";

            run168.Append(runProperties168);
            run168.Append(text168);

            Run run169 = new Run() { RsidRunAddition = "003A379A" };

            RunProperties runProperties169 = new RunProperties();
            RunFonts runFonts253 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties169.Append(runFonts253);
            Text text169 = new Text();
            text169.Text = "ማለት ነው፡፡";

            run169.Append(runProperties169);
            run169.Append(text169);

            paragraph88.Append(paragraphProperties88);
            paragraph88.Append(run168);
            paragraph88.Append(run169);

            Paragraph paragraph89 = new Paragraph() { RsidParagraphAddition = "003A379A", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "003A379A" };

            ParagraphProperties paragraphProperties89 = new ParagraphProperties();

            Tabs tabs44 = new Tabs();
            TabStop tabStop44 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs44.Append(tabStop44);

            ParagraphMarkRunProperties paragraphMarkRunProperties89 = new ParagraphMarkRunProperties();
            RunFonts runFonts254 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            paragraphMarkRunProperties89.Append(runFonts254);

            paragraphProperties89.Append(tabs44);
            paragraphProperties89.Append(paragraphMarkRunProperties89);

            Run run170 = new Run();

            RunProperties runProperties170 = new RunProperties();
            RunFonts runFonts255 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties170.Append(runFonts255);
            Text text170 = new Text();
            text170.Text = "1.11. ‹‹የውሃ መስመር መለወጫ‹‹ ማለት የነበሩ መስመር ስፋትና ቦታ ሳይለወጥ ባቺስ ጠመሳሳይ ስፋት ባለው ቧንቧ መተካት ማለት ነው፡፡";

            run170.Append(runProperties170);
            run170.Append(text170);

            paragraph89.Append(paragraphProperties89);
            paragraph89.Append(run170);

            Paragraph paragraph90 = new Paragraph() { RsidParagraphAddition = "003A379A", RsidParagraphProperties = "00E42C2F", RsidRunAdditionDefault = "003A379A" };

            ParagraphProperties paragraphProperties90 = new ParagraphProperties();

            Tabs tabs45 = new Tabs();
            TabStop tabStop45 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs45.Append(tabStop45);

            ParagraphMarkRunProperties paragraphMarkRunProperties90 = new ParagraphMarkRunProperties();
            RunFonts runFonts256 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            paragraphMarkRunProperties90.Append(runFonts256);

            paragraphProperties90.Append(tabs45);
            paragraphProperties90.Append(paragraphMarkRunProperties90);

            Run run171 = new Run();

            RunProperties runProperties171 = new RunProperties();
            RunFonts runFonts257 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties171.Append(runFonts257);
            Text text171 = new Text();
            text171.Text = "1.12. ‹‹የከተማው ውሃና ፍሳሽ አገልግሎት‹‹ ማለት ቢሮውን በመወከል የከተማውን የመጠጥ ውሃና ፍሳሽን የሚስተዳድር መ/ቤት ማለት ነው፡፡";

            run171.Append(runProperties171);
            run171.Append(text171);

            paragraph90.Append(paragraphProperties90);
            paragraph90.Append(run171);

            Paragraph paragraph91 = new Paragraph() { RsidParagraphMarkRevision = "00A846BA", RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00A846BA", RsidRunAdditionDefault = "003A379A" };

            ParagraphProperties paragraphProperties91 = new ParagraphProperties();

            Tabs tabs46 = new Tabs();
            TabStop tabStop46 = new TabStop() { Val = TabStopValues.Left, Position = 630 };

            tabs46.Append(tabStop46);

            ParagraphMarkRunProperties paragraphMarkRunProperties91 = new ParagraphMarkRunProperties();
            RunFonts runFonts258 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            paragraphMarkRunProperties91.Append(runFonts258);

            paragraphProperties91.Append(tabs46);
            paragraphProperties91.Append(paragraphMarkRunProperties91);

            Run run172 = new Run();

            RunProperties runProperties172 = new RunProperties();
            RunFonts runFonts259 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties172.Append(runFonts259);
            Text text172 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text172.Text = "1.13. ‹‹አዲሱ የውሃ ታሪፍ‹‹ ማለት በየጊዜው ተሻሽሎ የሚወጣ የአንድ ሜትር ኩብ የውሃ ዋጋ ተመን ማለት ";

            run172.Append(runProperties172);
            run172.Append(text172);

            Run run173 = new Run() { RsidRunAddition = "00A846BA" };

            RunProperties runProperties173 = new RunProperties();
            RunFonts runFonts260 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties173.Append(runFonts260);
            Text text173 = new Text();
            text173.Text = "ነው፡፡";

            run173.Append(runProperties173);
            run173.Append(text173);

            paragraph91.Append(paragraphProperties91);
            paragraph91.Append(run172);
            paragraph91.Append(run173);

            Paragraph paragraph92 = new Paragraph() { RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "00343795" };

            ParagraphProperties paragraphProperties92 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId17 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties13 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference13 = new NumberingLevelReference() { Val = 0 };
            NumberingId numberingId13 = new NumberingId() { Val = 4 };

            numberingProperties13.Append(numberingLevelReference13);
            numberingProperties13.Append(numberingId13);

            ParagraphMarkRunProperties paragraphMarkRunProperties92 = new ParagraphMarkRunProperties();
            RunFonts runFonts261 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties92.Append(runFonts261);

            paragraphProperties92.Append(paragraphStyleId17);
            paragraphProperties92.Append(numberingProperties13);
            paragraphProperties92.Append(paragraphMarkRunProperties92);

            Run run174 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties174 = new RunProperties();
            RunFonts runFonts262 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold28 = new Bold();

            runProperties174.Append(runFonts262);
            runProperties174.Append(bold28);
            Text text174 = new Text();
            text174.Text = "Ÿq×]¨< uòƒ ÁK¨<” SeS`";

            run174.Append(runProperties174);
            run174.Append(text174);

            Run run175 = new Run() { RsidRunAddition = "00DD1AB9" };

            RunProperties runProperties175 = new RunProperties();
            RunFonts runFonts263 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties175.Append(runFonts263);
            Text text175 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text175.Text = " ";

            run175.Append(runProperties175);
            run175.Append(text175);

            Run run176 = new Run() { RsidRunProperties = "00DD1AB9", RsidRunAddition = "00DD1AB9" };

            RunProperties runProperties176 = new RunProperties();
            RunFonts runFonts264 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold29 = new Bold();

            runProperties176.Append(runFonts264);
            runProperties176.Append(bold29);
            Text text176 = new Text();
            text176.Text = "eKS²`Òƒ“ eKS’ŸvŸw::";

            run176.Append(runProperties176);
            run176.Append(text176);

            paragraph92.Append(paragraphProperties92);
            paragraph92.Append(run174);
            paragraph92.Append(run175);
            paragraph92.Append(run176);

            Paragraph paragraph93 = new Paragraph() { RsidParagraphMarkRevision = "00510A19", RsidParagraphAddition = "00343795", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "00343795" };

            ParagraphProperties paragraphProperties93 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId18 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties14 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference14 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId14 = new NumberingId() { Val = 4 };

            numberingProperties14.Append(numberingLevelReference14);
            numberingProperties14.Append(numberingId14);
            Indentation indentation7 = new Indentation() { Start = "720", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties93 = new ParagraphMarkRunProperties();
            RunFonts runFonts265 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties93.Append(runFonts265);

            paragraphProperties93.Append(paragraphStyleId18);
            paragraphProperties93.Append(numberingProperties14);
            paragraphProperties93.Append(indentation7);
            paragraphProperties93.Append(paragraphMarkRunProperties93);

            Run run177 = new Run();

            RunProperties runProperties177 = new RunProperties();
            RunFonts runFonts266 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties177.Append(runFonts266);
            Text text177 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text177.Text = "u¨<H ›ÑMÓKA~ ¡MM ¨<eØ K¨<H Tc^Ý“ TŸóðÁ ¾}²[Ñ<ƒ” ª“ SeSa‹“ ”Ç=G<U ";

            run177.Append(runProperties177);
            run177.Append(text177);

            Run run178 = new Run();

            RunProperties runProperties178 = new RunProperties();
            RunFonts runFonts267 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties178.Append(runFonts267);
            Text text178 = new Text();
            text178.Text = "እ";

            run178.Append(runProperties178);
            run178.Append(text178);

            Run run179 = new Run() { RsidRunAddition = "00C252AE" };

            RunProperties runProperties179 = new RunProperties();
            RunFonts runFonts268 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties179.Append(runFonts268);
            Text text179 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text179.Text = "ስከ ቆጣሪ ጭምር ድረስ ያሉትን የውሃ መስመሮችና በላዩ የተተከሉትን መሳሪያዎች ጨምሮ ";

            run179.Append(runProperties179);
            run179.Append(text179);

            Run run180 = new Run() { RsidRunAddition = "00510A19" };

            RunProperties runProperties180 = new RunProperties();
            RunFonts runFonts269 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties180.Append(runFonts269);
            Text text180 = new Text();
            text180.Text = "ቢሮው ብቻ ያዝባዋቸል ስለሆነም እነዚህን የውሃ መስመሮችንና መሳሪያዎች የመዘርጋት የመግጠም የማሻሻል የመለወጥ የማዛወር የመቁረጥ የማንሳት የመመርመርና የመፈተሸ ስራዎችን ማከናውን የሚሉት የቢሮው ሠራተኞች ብቻ ናቸው፡፡";

            run180.Append(runProperties180);
            run180.Append(text180);

            paragraph93.Append(paragraphProperties93);
            paragraph93.Append(run177);
            paragraph93.Append(run178);
            paragraph93.Append(run179);
            paragraph93.Append(run180);

            Paragraph paragraph94 = new Paragraph() { RsidParagraphMarkRevision = "00510A19", RsidParagraphAddition = "00510A19", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "00510A19" };

            ParagraphProperties paragraphProperties94 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId19 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties15 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference15 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId15 = new NumberingId() { Val = 4 };

            numberingProperties15.Append(numberingLevelReference15);
            numberingProperties15.Append(numberingId15);
            Indentation indentation8 = new Indentation() { Start = "720", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties94 = new ParagraphMarkRunProperties();
            RunFonts runFonts270 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties94.Append(runFonts270);

            paragraphProperties94.Append(paragraphStyleId19);
            paragraphProperties94.Append(numberingProperties15);
            paragraphProperties94.Append(indentation8);
            paragraphProperties94.Append(paragraphMarkRunProperties94);

            Run run181 = new Run();

            RunProperties runProperties181 = new RunProperties();
            RunFonts runFonts271 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties181.Append(runFonts271);
            Text text181 = new Text();
            text181.Text = "ለእያንዳንዱ ደንበኛ የሚዘረጋውን የውሃ ቧንቧ ስፋትና ቧንቧው የሚዘረጋበትን አቅጣጫ ከደንበኛው ጋር በመስማማት ቢሮው ይወስናል፡፡";

            run181.Append(runProperties181);
            run181.Append(text181);

            paragraph94.Append(paragraphProperties94);
            paragraph94.Append(run181);

            Paragraph paragraph95 = new Paragraph() { RsidParagraphMarkRevision = "008A7DAC", RsidParagraphAddition = "00510A19", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "003306A1" };

            ParagraphProperties paragraphProperties95 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId20 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties16 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference16 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId16 = new NumberingId() { Val = 4 };

            numberingProperties16.Append(numberingLevelReference16);
            numberingProperties16.Append(numberingId16);
            Indentation indentation9 = new Indentation() { Start = "720", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties95 = new ParagraphMarkRunProperties();
            RunFonts runFonts272 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties95.Append(runFonts272);

            paragraphProperties95.Append(paragraphStyleId20);
            paragraphProperties95.Append(numberingProperties16);
            paragraphProperties95.Append(indentation9);
            paragraphProperties95.Append(paragraphMarkRunProperties95);

            Run run182 = new Run();

            RunProperties runProperties182 = new RunProperties();
            RunFonts runFonts273 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties182.Append(runFonts273);
            Text text182 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text182.Text = "ከቆጣሪያ በፊት ያለውን የውሃ መስመር ለመዘርጋት ለመጠገን ";

            run182.Append(runProperties182);
            run182.Append(text182);

            Run run183 = new Run() { RsidRunAddition = "008A7DAC" };

            RunProperties runProperties183 = new RunProperties();
            RunFonts runFonts274 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties183.Append(runFonts274);
            Text text183 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text183.Text = "ለማሻሻል ማዘውር ለመለወጥ ወዘተ የሚያስፈልገውን ቁሳቁስ ደንበኛው በራሱ ወጪ ያቀርባል፡፡ ደንበኛው የሚያቀርበውን ማናቸውም የዕቃ ዓይነትና ጥራት ቢሮ ይቆጣጠራል ";

            run183.Append(runProperties183);
            run183.Append(text183);

            paragraph95.Append(paragraphProperties95);
            paragraph95.Append(run182);
            paragraph95.Append(run183);

            Paragraph paragraph96 = new Paragraph() { RsidParagraphAddition = "008A7DAC", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "00FC4A92" };

            ParagraphProperties paragraphProperties96 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId21 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties17 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference17 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId17 = new NumberingId() { Val = 4 };

            numberingProperties17.Append(numberingLevelReference17);
            numberingProperties17.Append(numberingId17);
            Indentation indentation10 = new Indentation() { Start = "720", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties96 = new ParagraphMarkRunProperties();
            RunFonts runFonts275 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties96.Append(runFonts275);

            paragraphProperties96.Append(paragraphStyleId21);
            paragraphProperties96.Append(numberingProperties17);
            paragraphProperties96.Append(indentation10);
            paragraphProperties96.Append(paragraphMarkRunProperties96);

            Run run184 = new Run();

            RunProperties runProperties184 = new RunProperties();
            RunFonts runFonts276 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties184.Append(runFonts276);
            LastRenderedPageBreak lastRenderedPageBreak9 = new LastRenderedPageBreak();
            Text text184 = new Text();
            text184.Text = "Ÿq×] uòƒ ÁK¨<” ¾¨<H SeS` KS²`Òƒ KSÖÑ” KThhM KT³¨<` KSK¨Ø ...¨²} ¾T>ÁeðMÑ<ƒ” ¾SÑMÑÁ Sd]";

            run184.Append(runProperties184);
            run184.Append(lastRenderedPageBreak9);
            run184.Append(text184);

            Run run185 = new Run() { RsidRunAddition = "00AC61EA" };

            RunProperties runProperties185 = new RunProperties();
            RunFonts runFonts277 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties185.Append(runFonts277);
            Text text185 = new Text();
            text185.Text = "Á­‹“ ‹KA";

            run185.Append(runProperties185);
            run185.Append(text185);

            Run run186 = new Run() { RsidRunAddition = "004707F7" };

            RunProperties runProperties186 = new RunProperties();
            RunFonts runFonts278 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties186.Append(runFonts278);
            Text text186 = new Text();
            text186.Text = "ታ";

            run186.Append(runProperties186);
            run186.Append(text186);

            Run run187 = new Run();

            RunProperties runProperties187 = new RunProperties();
            RunFonts runFonts279 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties187.Append(runFonts279);
            Text text187 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text187.Text = " ÁL†¨<” vKS<Á";

            run187.Append(runProperties187);
            run187.Append(text187);

            Run run188 = new Run() { RsidRunAddition = "005F3766" };

            RunProperties runProperties188 = new RunProperties();
            RunFonts runFonts280 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties188.Append(runFonts280);
            Text text188 = new Text();
            text188.Text = "­";

            run188.Append(runProperties188);
            run188.Append(text188);

            Run run189 = new Run();

            RunProperties runProperties189 = new RunProperties();
            RunFonts runFonts281 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties189.Append(runFonts281);
            Text text189 = new Text();
            text189.Text = "‹ u=a¨< Ák`vM::";

            run189.Append(runProperties189);
            run189.Append(text189);

            paragraph96.Append(paragraphProperties96);
            paragraph96.Append(run184);
            paragraph96.Append(run185);
            paragraph96.Append(run186);
            paragraph96.Append(run187);
            paragraph96.Append(run188);
            paragraph96.Append(run189);

            Paragraph paragraph97 = new Paragraph() { RsidParagraphAddition = "001E253D", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "00C96C94" };

            ParagraphProperties paragraphProperties97 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId22 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties18 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference18 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId18 = new NumberingId() { Val = 4 };

            numberingProperties18.Append(numberingLevelReference18);
            numberingProperties18.Append(numberingId18);
            Indentation indentation11 = new Indentation() { Start = "720", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties97 = new ParagraphMarkRunProperties();
            RunFonts runFonts282 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties97.Append(runFonts282);

            paragraphProperties97.Append(paragraphStyleId22);
            paragraphProperties97.Append(numberingProperties18);
            paragraphProperties97.Append(indentation11);
            paragraphProperties97.Append(paragraphMarkRunProperties97);

            Run run190 = new Run();

            RunProperties runProperties190 = new RunProperties();
            RunFonts runFonts283 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties190.Append(runFonts283);
            Text text190 = new Text();
            text190.Text = "›eðLÑ> J• c=ÁÑ˜ u=a¨< Ÿq×]¨< uòƒ";

            run190.Append(runProperties190);
            run190.Append(text190);

            Run run191 = new Run() { RsidRunAddition = "001B06F3" };

            RunProperties runProperties191 = new RunProperties();
            RunFonts runFonts284 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties191.Append(runFonts284);
            Text text191 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text191.Text = " \"K ¾’v` SeS` Å”u™‹ ¾¨<H SeS` LÃ";

            run191.Append(runProperties191);
            run191.Append(text191);

            Run run192 = new Run();

            RunProperties runProperties192 = new RunProperties();
            RunFonts runFonts285 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties192.Append(runFonts285);
            Text text192 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text192.Text = " K›Ç=e Å”u—/Åu™‹ ¾¨<H SeS` KSkÖM S<K< Swƒ ›K¨<:: J•U Ÿ’v` SeS` Å”u—¨< ¾T>kØML†¨< Å”u™‹ ¾Ò^ SeS\\” ¨<Ü K’v\\ ¨ÃU SeS\\” upÉT>Á K²[Ò¨< Å”u— K²[Ñ<ƒ Å”u™‹ É`h¨<”/É`h†¨<” ";

            run192.Append(runProperties192);
            run192.Append(text192);

            Run run193 = new Run();

            RunProperties runProperties193 = new RunProperties();
            RunFonts runFonts286 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties193.Append(runFonts286);
            Text text193 = new Text();
            text193.Text = "እ";

            run193.Append(runProperties193);
            run193.Append(text193);

            Run run194 = new Run();

            RunProperties runProperties194 = new RunProperties();
            RunFonts runFonts287 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties194.Append(runFonts287);
            Text text194 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text194.Text = "”Ç=ŸõK< ÃÅ[ÒM:: ";

            run194.Append(runProperties194);
            run194.Append(text194);

            paragraph97.Append(paragraphProperties97);
            paragraph97.Append(run190);
            paragraph97.Append(run191);
            paragraph97.Append(run192);
            paragraph97.Append(run193);
            paragraph97.Append(run194);

            Paragraph paragraph98 = new Paragraph() { RsidParagraphMarkRevision = "00343795", RsidParagraphAddition = "0073487D", RsidParagraphProperties = "00343795", RsidRunAdditionDefault = "0073487D" };

            ParagraphProperties paragraphProperties98 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId23 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties19 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference19 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId19 = new NumberingId() { Val = 4 };

            numberingProperties19.Append(numberingLevelReference19);
            numberingProperties19.Append(numberingId19);
            Indentation indentation12 = new Indentation() { Start = "720", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties98 = new ParagraphMarkRunProperties();
            RunFonts runFonts288 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties98.Append(runFonts288);

            paragraphProperties98.Append(paragraphStyleId23);
            paragraphProperties98.Append(numberingProperties19);
            paragraphProperties98.Append(indentation12);
            paragraphProperties98.Append(paragraphMarkRunProperties98);

            Run run195 = new Run();

            RunProperties runProperties195 = new RunProperties();
            RunFonts runFonts289 = new RunFonts() { Ascii = "Ge\'ez-1", HighAnsi = "Ge\'ez-1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties195.Append(runFonts289);
            Text text195 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text195.Text = "Å”u—¨< uÚT] ¾¨<H Óòƒ KTÓ–ƒ c=ÁSK¡ƒ“ u=a¨<U ¾T>ÁÉK¨<” SÅu— ¾¨<H Óòƒ um J• c=ÁÑ˜ }ÚT]¨<” ¾¨< Óòƒ ";

            run195.Append(runProperties195);
            run195.Append(text195);

            paragraph98.Append(paragraphProperties98);
            paragraph98.Append(run195);

            Paragraph paragraph99 = new Paragraph() { RsidParagraphMarkRevision = "00AF310F", RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00C01404" };

            ParagraphProperties paragraphProperties99 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId24 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties99 = new ParagraphMarkRunProperties();
            RunFonts runFonts290 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties99.Append(runFonts290);

            paragraphProperties99.Append(paragraphStyleId24);
            paragraphProperties99.Append(paragraphMarkRunProperties99);

            Run run196 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties196 = new RunProperties();
            RunFonts runFonts291 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties196.Append(runFonts291);
            Text text196 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text196.Text = "4.5. ከቆጣሪው ተመርምሮ በይበልጥ ወይም በማነስ ስህተት የሚያሳይ ከሆነ ሂሳቡም በዚሁ መሠረት ";

            run196.Append(runProperties196);
            run196.Append(text196);

            Run run197 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "00AC4486" };

            RunProperties runProperties197 = new RunProperties();
            RunFonts runFonts292 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties197.Append(runFonts292);
            Text text197 = new Text();
            text197.Text = "በመጨመር ወይም በመቀነስ የስተካከላል፡፡";

            run197.Append(runProperties197);
            run197.Append(text197);

            paragraph99.Append(paragraphProperties99);
            paragraph99.Append(run196);
            paragraph99.Append(run197);

            Paragraph paragraph100 = new Paragraph() { RsidParagraphMarkRevision = "00AF310F", RsidParagraphAddition = "00AC4486", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00AC4486" };

            ParagraphProperties paragraphProperties100 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId25 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties100 = new ParagraphMarkRunProperties();
            RunFonts runFonts293 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties100.Append(runFonts293);

            paragraphProperties100.Append(paragraphStyleId25);
            paragraphProperties100.Append(paragraphMarkRunProperties100);

            Run run198 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties198 = new RunProperties();
            RunFonts runFonts294 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties198.Append(runFonts294);
            Text text198 = new Text();
            text198.Text = "4.6. ምርመራ የተደረገለት ቆጣሪ ትክክለኛ ሆኖ ቢገኝ ማናቸውም የማስመርመሪያ ወጪዎች ደንበኛው ይከፍላል፡፡ ነገር ግን ቆጣሪው ትክክለኛው ሆኖ በይገኝ የምርመራ ወጪዎችን በሙሉ ቢሮው የሸፍናል፡፡";

            run198.Append(runProperties198);
            run198.Append(text198);

            paragraph100.Append(paragraphProperties100);
            paragraph100.Append(run198);

            Paragraph paragraph101 = new Paragraph() { RsidParagraphMarkRevision = "00AF310F", RsidParagraphAddition = "00EB5BE0", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00AC4486" };

            ParagraphProperties paragraphProperties101 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId26 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties101 = new ParagraphMarkRunProperties();
            RunFonts runFonts295 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties101.Append(runFonts295);

            paragraphProperties101.Append(paragraphStyleId26);
            paragraphProperties101.Append(paragraphMarkRunProperties101);

            Run run199 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties199 = new RunProperties();
            RunFonts runFonts296 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties199.Append(runFonts296);
            Text text199 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text199.Text = "4.7. ከዚህ በታች በአንቀጽ 4 ተራ ቁጥር 48 የተጠቀሰው ማስጠንቀቂያ ለደንበኛው ከተሰጠ ወይም በዚህ ዓይነት ሁኔታ መስጠንቀቂያውን መዳረስ ";

            run199.Append(runProperties199);
            run199.Append(text199);

            Run run200 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "00E040C3" };

            RunProperties runProperties200 = new RunProperties();
            RunFonts runFonts297 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties200.Append(runFonts297);
            Text text200 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text200.Text = "የማይቻል የሆነ እንደሆነ ማስታወቂያውን በቀላሉና በግልጽ እንዲታይ ተደርጎ በመኖሪያ ቤቱ ወይም በመ/ቤቱ ግርግዳ ወይም አጥር ግቢ ";

            run200.Append(runProperties200);
            run200.Append(text200);

            Run run201 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "00E5769A" };

            RunProperties runProperties201 = new RunProperties();
            RunFonts runFonts298 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties201.Append(runFonts298);
            Text text201 = new Text();
            text201.Text = "በር ወይም ይህን በመሰለ ሌላ ቦታ ላይ ከተለጠፈ ለደንበኛው እን";

            run201.Append(runProperties201);
            run201.Append(text201);

            Run run202 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "00EB5BE0" };

            RunProperties runProperties202 = new RunProperties();
            RunFonts runFonts299 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties202.Append(runFonts299);
            Text text202 = new Text();
            text202.Text = "ደደረሰው ይቆጠራል፡፡";

            run202.Append(runProperties202);
            run202.Append(text202);

            paragraph101.Append(paragraphProperties101);
            paragraph101.Append(run199);
            paragraph101.Append(run200);
            paragraph101.Append(run201);
            paragraph101.Append(run202);

            Paragraph paragraph102 = new Paragraph() { RsidParagraphMarkRevision = "00AF310F", RsidParagraphAddition = "005A314B", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00EB5BE0" };

            ParagraphProperties paragraphProperties102 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId27 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties102 = new ParagraphMarkRunProperties();
            RunFonts runFonts300 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties102.Append(runFonts300);

            paragraphProperties102.Append(paragraphStyleId27);
            paragraphProperties102.Append(paragraphMarkRunProperties102);

            Run run203 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties203 = new RunProperties();
            RunFonts runFonts301 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties203.Append(runFonts301);
            Text text203 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text203.Text = "4.8. ";

            run203.Append(runProperties203);
            run203.Append(text203);

            Run run204 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "009C3F38" };

            RunProperties runProperties204 = new RunProperties();
            RunFonts runFonts302 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties204.Append(runFonts302);
            Text text204 = new Text();
            text204.Text = "ደንበኛው መጀመሪያ በሚቀርብለት የውሃ ፍጆታ ሂሳብ መክፈያ ቢል/ሰነድመሰረት ሂሳቡን መክፈል ይኖርበታል";

            run204.Append(runProperties204);
            run204.Append(text204);

            Run run205 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "004611A1" };

            RunProperties runProperties205 = new RunProperties();
            RunFonts runFonts303 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties205.Append(runFonts303);
            Text text205 = new Text();
            text205.Text = "፡፡ ባይ";

            run205.Append(runProperties205);
            run205.Append(text205);

            Run run206 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "005A314B" };

            RunProperties runProperties206 = new RunProperties();
            RunFonts runFonts304 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties206.Append(runFonts304);
            Text text206 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text206.Text = "ከፍል ግን በተከታታይ የሰዓት /7/ ቀንና የሃያ አራት /24/ ሰዓት ማስጠንቀቂያ በመስጠት ቢሮው ";

            run206.Append(runProperties206);
            run206.Append(text206);

            Run run207 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "005A314B" };

            RunProperties runProperties207 = new RunProperties();
            RunFonts runFonts305 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties207.Append(runFonts305);
            LastRenderedPageBreak lastRenderedPageBreak10 = new LastRenderedPageBreak();
            Text text207 = new Text();
            text207.Text = "ለደንበኛው የሚሰጠውን የውሃ አገልግሎት ያቋርጣል፡፡";

            run207.Append(runProperties207);
            run207.Append(lastRenderedPageBreak10);
            run207.Append(text207);

            paragraph102.Append(paragraphProperties102);
            paragraph102.Append(run203);
            paragraph102.Append(run204);
            paragraph102.Append(run205);
            paragraph102.Append(run206);
            paragraph102.Append(run207);

            Paragraph paragraph103 = new Paragraph() { RsidParagraphMarkRevision = "00AF310F", RsidParagraphAddition = "00AE3FBE", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "005A314B" };

            ParagraphProperties paragraphProperties103 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId28 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties103 = new ParagraphMarkRunProperties();
            RunFonts runFonts306 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties103.Append(runFonts306);

            paragraphProperties103.Append(paragraphStyleId28);
            paragraphProperties103.Append(paragraphMarkRunProperties103);

            Run run208 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties208 = new RunProperties();
            RunFonts runFonts307 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties208.Append(runFonts307);
            Text text208 = new Text();
            text208.Text = "4.9. ደንበኛው የሚፈለግበትን ፍጆታ ሂሳብ በየወቅቱ ሳይከፍል ቀርቶ ውዝፍ እዳ ሲኖርበት እዳውን ከነወለዱ እንዲከፍል ይገደዳል፡፡";

            run208.Append(runProperties208);
            run208.Append(text208);

            Run run209 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties209 = new RunProperties();
            RunFonts runFonts308 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties209.Append(runFonts308);
            Break break1 = new Break();

            run209.Append(runProperties209);
            run209.Append(break1);

            Run run210 = new Run() { RsidRunProperties = "00A13E70", RsidRunAddition = "00AE3FBE" };

            RunProperties runProperties210 = new RunProperties();
            RunFonts runFonts309 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold30 = new Bold();

            runProperties210.Append(runFonts309);
            runProperties210.Append(bold30);
            Text text209 = new Text();
            text209.Text = "5. ከቆጣሪው በኃላ መስመር";

            run210.Append(runProperties210);
            run210.Append(text209);

            Run run211 = new Run() { RsidRunProperties = "00AF310F", RsidRunAddition = "00AE3FBE" };

            RunProperties runProperties211 = new RunProperties();
            RunFonts runFonts310 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties211.Append(runFonts310);
            Text text210 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text210.Text = " ስለመዘርጋትና መጠገን";

            run211.Append(runProperties211);
            run211.Append(text210);

            paragraph103.Append(paragraphProperties103);
            paragraph103.Append(run208);
            paragraph103.Append(run209);
            paragraph103.Append(run210);
            paragraph103.Append(run211);

            Paragraph paragraph104 = new Paragraph() { RsidParagraphMarkRevision = "00AF310F", RsidParagraphAddition = "00AE3FBE", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00AE3FBE" };

            ParagraphProperties paragraphProperties104 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId29 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties104 = new ParagraphMarkRunProperties();
            RunFonts runFonts311 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties104.Append(runFonts311);

            paragraphProperties104.Append(paragraphStyleId29);
            paragraphProperties104.Append(paragraphMarkRunProperties104);

            Run run212 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties212 = new RunProperties();
            RunFonts runFonts312 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties212.Append(runFonts312);
            Text text211 = new Text();
            text211.Text = "5.1. ከቆጠሪው በኃላ መስመር መዘርጋትና መጠገን የደንበኛው ኃላፊነት ነው፡፡ ሆኖም አስፈላጊ ሆኖ ሲያገኘው ቢሮው የደንበኛውን የውስጥ መስመር የመመርመር ስለጣን አለው፡፡";

            run212.Append(runProperties212);
            run212.Append(text211);

            paragraph104.Append(paragraphProperties104);
            paragraph104.Append(run212);

            Paragraph paragraph105 = new Paragraph() { RsidParagraphMarkRevision = "00C01404", RsidParagraphAddition = "00AC4486", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00AE3FBE" };

            ParagraphProperties paragraphProperties105 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId30 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties105 = new ParagraphMarkRunProperties();
            RunFonts runFonts313 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties105.Append(runFonts313);

            paragraphProperties105.Append(paragraphStyleId30);
            paragraphProperties105.Append(paragraphMarkRunProperties105);

            Run run213 = new Run() { RsidRunProperties = "00AF310F" };

            RunProperties runProperties213 = new RunProperties();
            RunFonts runFonts314 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties213.Append(runFonts314);
            Text text212 = new Text();
            text212.Text = "5.2. የደንበኛው የውስጥ መስመር አሰራር የቢሮውን ውሃ የሚነክል ወይም በቢሮው የውሃ አገልግሎት ላይ ጉዳት የማያደርስ ሆኖ ካገኘው ቢሮው";

            run213.Append(runProperties213);
            run213.Append(text212);

            Run run214 = new Run();

            RunProperties runProperties214 = new RunProperties();
            RunFonts runFonts315 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties214.Append(runFonts315);
            Text text213 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text213.Text = " ";

            run214.Append(runProperties214);
            run214.Append(text213);

            paragraph105.Append(paragraphProperties105);
            paragraph105.Append(run213);
            paragraph105.Append(run214);

            Paragraph paragraph106 = new Paragraph() { RsidParagraphMarkRevision = "00E81041", RsidParagraphAddition = "00D970BD", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00E81041" };

            ParagraphProperties paragraphProperties106 = new ParagraphProperties();
            Indentation indentation13 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties106 = new ParagraphMarkRunProperties();
            RunFonts runFonts316 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties106.Append(runFonts316);

            paragraphProperties106.Append(indentation13);
            paragraphProperties106.Append(paragraphMarkRunProperties106);

            Run run215 = new Run() { RsidRunProperties = "00E81041" };

            RunProperties runProperties215 = new RunProperties();
            RunFonts runFonts317 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties215.Append(runFonts317);
            Text text214 = new Text();
            text214.Text = "ለማስገኘት የሚያስፈልገውን ሁሉ ማሟላት ና ወጪውን መቻል የደንበኛው ኃለፊነት ሆኖ ቢሮው ተጨማሪውን የውሃ ግፍት ይሰጣል፡፡";

            run215.Append(runProperties215);
            run215.Append(text214);

            paragraph106.Append(paragraphProperties106);
            paragraph106.Append(run215);

            Paragraph paragraph107 = new Paragraph() { RsidParagraphMarkRevision = "00CD3C3B", RsidParagraphAddition = "00E81041", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "00E81041" };

            ParagraphProperties paragraphProperties107 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId31 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties20 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference20 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId20 = new NumberingId() { Val = 4 };

            numberingProperties20.Append(numberingLevelReference20);
            numberingProperties20.Append(numberingId20);
            Indentation indentation14 = new Indentation() { Start = "270", Hanging = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties107 = new ParagraphMarkRunProperties();
            RunFonts runFonts318 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties107.Append(runFonts318);

            paragraphProperties107.Append(paragraphStyleId31);
            paragraphProperties107.Append(numberingProperties20);
            paragraphProperties107.Append(indentation14);
            paragraphProperties107.Append(paragraphMarkRunProperties107);

            Run run216 = new Run() { RsidRunProperties = "00CD3C3B" };

            RunProperties runProperties216 = new RunProperties();
            RunFonts runFonts319 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties216.Append(runFonts319);
            Text text215 = new Text();
            text215.Text = "ቢሮው የውሃ አቅርቦቱን ለማሻሻል በዋናው መስመር ላይ ለውጥ በሚያደርግበት ወቅት በዋና መስመሩ ለውጥ ምክንያት የደንበኛው ኃላፊነት ሆኖ ቢሮው ተጨማሪውን የውሃ ግፊት ይሰጣል፡፡";

            run216.Append(runProperties216);
            run216.Append(text215);

            paragraph107.Append(paragraphProperties107);
            paragraph107.Append(run216);

            Paragraph paragraph108 = new Paragraph() { RsidParagraphMarkRevision = "00CD3C3B", RsidParagraphAddition = "00CD3C3B", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "00E81041" };

            ParagraphProperties paragraphProperties108 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId32 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties21 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference21 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId21 = new NumberingId() { Val = 4 };

            numberingProperties21.Append(numberingLevelReference21);
            numberingProperties21.Append(numberingId21);
            Indentation indentation15 = new Indentation() { Start = "270", Hanging = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties108 = new ParagraphMarkRunProperties();
            RunFonts runFonts320 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties108.Append(runFonts320);

            paragraphProperties108.Append(paragraphStyleId32);
            paragraphProperties108.Append(numberingProperties21);
            paragraphProperties108.Append(indentation15);
            paragraphProperties108.Append(paragraphMarkRunProperties108);

            Run run217 = new Run() { RsidRunProperties = "00CD3C3B" };

            RunProperties runProperties217 = new RunProperties();
            RunFonts runFonts321 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties217.Append(runFonts321);
            Text text216 = new Text();
            text216.Text = "አንድ ደንበኛ ከቆጣሪ በፊት ባለ መስመር ላይ ወ";

            run217.Append(runProperties217);
            run217.Append(text216);

            Run run218 = new Run() { RsidRunProperties = "00CD3C3B", RsidRunAddition = "005A625D" };

            RunProperties runProperties218 = new RunProperties();
            RunFonts runFonts322 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties218.Append(runFonts322);
            Text text217 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text217.Text = "ይም በቢሮው ዋና መስመር ላይ ማናቸውንም ጉዳት ቢፈጽም ወይም በውሃ ቢጠቀም በቢሮው ላይ የደረሰውን ጠቅላላ ወጪ ኪሳራ ጭምር የቢሮው መሐንዲስ ወይም ቴክኒሽያን በገመተው መጠን ይከፍላል ለመከፈል ፈቃደኛ ባይሆን ግን የውሃ አገልግሎቱ ";

            run218.Append(runProperties218);
            run218.Append(text217);

            Run run219 = new Run() { RsidRunAddition = "00221352" };

            RunProperties runProperties219 = new RunProperties();
            RunFonts runFonts323 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties219.Append(runFonts323);
            Text text218 = new Text();
            text218.Text = "ተቋርጦ";

            run219.Append(runProperties219);
            run219.Append(text218);

            Run run220 = new Run() { RsidRunProperties = "00CD3C3B", RsidRunAddition = "005A625D" };

            RunProperties runProperties220 = new RunProperties();
            RunFonts runFonts324 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties220.Append(runFonts324);
            Text text219 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text219.Text = " ለፈጸመው ወንጀልና ላደረሰው ጉዳት በህግ ይጠየቃል";

            run220.Append(runProperties220);
            run220.Append(text219);

            Run run221 = new Run() { RsidRunProperties = "00CD3C3B", RsidRunAddition = "00CD3C3B" };

            RunProperties runProperties221 = new RunProperties();
            RunFonts runFonts325 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", EastAsia = "MingLiU", ComplexScript = "MingLiU" };

            runProperties221.Append(runFonts325);
            Text text220 = new Text();
            text220.Text = "፡፡";

            run221.Append(runProperties221);
            run221.Append(text220);

            paragraph108.Append(paragraphProperties108);
            paragraph108.Append(run217);
            paragraph108.Append(run218);
            paragraph108.Append(run219);
            paragraph108.Append(run220);
            paragraph108.Append(run221);

            Paragraph paragraph109 = new Paragraph() { RsidParagraphMarkRevision = "00A13E70", RsidParagraphAddition = "00E81041", RsidParagraphProperties = "00CD3C3B", RsidRunAdditionDefault = "00CD3C3B" };

            ParagraphProperties paragraphProperties109 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId33 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties22 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference22 = new NumberingLevelReference() { Val = 0 };
            NumberingId numberingId22 = new NumberingId() { Val = 4 };

            numberingProperties22.Append(numberingLevelReference22);
            numberingProperties22.Append(numberingId22);
            Indentation indentation16 = new Indentation() { Start = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties109 = new ParagraphMarkRunProperties();
            RunFonts runFonts326 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold31 = new Bold();

            paragraphMarkRunProperties109.Append(runFonts326);
            paragraphMarkRunProperties109.Append(bold31);

            paragraphProperties109.Append(paragraphStyleId33);
            paragraphProperties109.Append(numberingProperties22);
            paragraphProperties109.Append(indentation16);
            paragraphProperties109.Append(paragraphMarkRunProperties109);

            Run run222 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties222 = new RunProperties();
            RunFonts runFonts327 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold32 = new Bold();

            runProperties222.Append(runFonts327);
            runProperties222.Append(bold32);
            Text text221 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text221.Text = "ቆጣሪውን የመጠበቅና የመነከባከብ ኃላፊነት ";

            run222.Append(runProperties222);
            run222.Append(text221);

            paragraph109.Append(paragraphProperties109);
            paragraph109.Append(run222);

            Paragraph paragraph110 = new Paragraph() { RsidParagraphAddition = "00CD3C3B", RsidParagraphProperties = "00CD3C3B", RsidRunAdditionDefault = "00CD3C3B" };

            ParagraphProperties paragraphProperties110 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId34 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties23 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference23 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId23 = new NumberingId() { Val = 4 };

            numberingProperties23.Append(numberingLevelReference23);
            numberingProperties23.Append(numberingId23);

            Tabs tabs47 = new Tabs();
            TabStop tabStop47 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop48 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs47.Append(tabStop47);
            tabs47.Append(tabStop48);
            Indentation indentation17 = new Indentation() { Start = "180", Hanging = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties110 = new ParagraphMarkRunProperties();
            RunFonts runFonts328 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties110.Append(runFonts328);

            paragraphProperties110.Append(paragraphStyleId34);
            paragraphProperties110.Append(numberingProperties23);
            paragraphProperties110.Append(tabs47);
            paragraphProperties110.Append(indentation17);
            paragraphProperties110.Append(paragraphMarkRunProperties110);

            Run run223 = new Run();

            RunProperties runProperties223 = new RunProperties();
            RunFonts runFonts329 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties223.Append(runFonts329);
            Text text222 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text222.Text = "ማናቸውም ቆጣሪ የመ/ቤቱ ንብረት ነው፡፡ ስለሆነም ቆጣሪውን መፍታት፣መግጠም ፣ማንሳት ፣መለወጥ ፣ ማዛወር፣ ማጽዳት ፣ መፍታት፣መመርመር፣ማሽግና ";

            run223.Append(runProperties223);
            run223.Append(text222);

            Run run224 = new Run();

            RunProperties runProperties224 = new RunProperties();
            RunFonts runFonts330 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties224.Append(runFonts330);
            LastRenderedPageBreak lastRenderedPageBreak11 = new LastRenderedPageBreak();
            Text text223 = new Text();
            text223.Text = "መክፈት….ወዘተ የሚችለው ቢሮው ብቻ ነው፡፡";

            run224.Append(runProperties224);
            run224.Append(lastRenderedPageBreak11);
            run224.Append(text223);

            paragraph110.Append(paragraphProperties110);
            paragraph110.Append(run223);
            paragraph110.Append(run224);

            Paragraph paragraph111 = new Paragraph() { RsidParagraphAddition = "00741E1A", RsidParagraphProperties = "00CD3C3B", RsidRunAdditionDefault = "00AF310F" };

            ParagraphProperties paragraphProperties111 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId35 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties24 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference24 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId24 = new NumberingId() { Val = 4 };

            numberingProperties24.Append(numberingLevelReference24);
            numberingProperties24.Append(numberingId24);

            Tabs tabs48 = new Tabs();
            TabStop tabStop49 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop50 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs48.Append(tabStop49);
            tabs48.Append(tabStop50);
            Indentation indentation18 = new Indentation() { Start = "180", Hanging = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties111 = new ParagraphMarkRunProperties();
            RunFonts runFonts331 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties111.Append(runFonts331);

            paragraphProperties111.Append(paragraphStyleId35);
            paragraphProperties111.Append(numberingProperties24);
            paragraphProperties111.Append(tabs48);
            paragraphProperties111.Append(indentation18);
            paragraphProperties111.Append(paragraphMarkRunProperties111);

            Run run225 = new Run();

            RunProperties runProperties225 = new RunProperties();
            RunFonts runFonts332 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties225.Append(runFonts332);
            Text text224 = new Text();
            text224.Text = "ቆጣሪ የሚተከልበት ቦታ ደንበኛው ላይ ችግር እንደማያስከትል ሆኖ በቢሮው ይወሰናል፡፡";

            run225.Append(runProperties225);
            run225.Append(text224);

            paragraph111.Append(paragraphProperties111);
            paragraph111.Append(run225);

            Paragraph paragraph112 = new Paragraph() { RsidParagraphAddition = "00AF310F", RsidParagraphProperties = "00CD3C3B", RsidRunAdditionDefault = "00AF310F" };

            ParagraphProperties paragraphProperties112 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId36 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties25 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference25 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId25 = new NumberingId() { Val = 4 };

            numberingProperties25.Append(numberingLevelReference25);
            numberingProperties25.Append(numberingId25);

            Tabs tabs49 = new Tabs();
            TabStop tabStop51 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop52 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs49.Append(tabStop51);
            tabs49.Append(tabStop52);
            Indentation indentation19 = new Indentation() { Start = "180", Hanging = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties112 = new ParagraphMarkRunProperties();
            RunFonts runFonts333 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties112.Append(runFonts333);

            paragraphProperties112.Append(paragraphStyleId36);
            paragraphProperties112.Append(numberingProperties25);
            paragraphProperties112.Append(tabs49);
            paragraphProperties112.Append(indentation19);
            paragraphProperties112.Append(paragraphMarkRunProperties112);

            Run run226 = new Run();

            RunProperties runProperties226 = new RunProperties();
            RunFonts runFonts334 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties226.Append(runFonts334);
            Text text225 = new Text();
            text225.Text = "ማናቸውም ቆጣሪ ቢሮው በሚሰጠው መመሪያ መሰረት ደንበኛው በሚያቀርበው ቆጣሪ ሳጥን ውስጥ መትክል አለበት፡፡";

            run226.Append(runProperties226);
            run226.Append(text225);

            paragraph112.Append(paragraphProperties112);
            paragraph112.Append(run226);

            Paragraph paragraph113 = new Paragraph() { RsidParagraphAddition = "00AF310F", RsidParagraphProperties = "00CD3C3B", RsidRunAdditionDefault = "00AF310F" };

            ParagraphProperties paragraphProperties113 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId37 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties26 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference26 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId26 = new NumberingId() { Val = 4 };

            numberingProperties26.Append(numberingLevelReference26);
            numberingProperties26.Append(numberingId26);

            Tabs tabs50 = new Tabs();
            TabStop tabStop53 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop54 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs50.Append(tabStop53);
            tabs50.Append(tabStop54);
            Indentation indentation20 = new Indentation() { Start = "180", Hanging = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties113 = new ParagraphMarkRunProperties();
            RunFonts runFonts335 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties113.Append(runFonts335);

            paragraphProperties113.Append(paragraphStyleId37);
            paragraphProperties113.Append(numberingProperties26);
            paragraphProperties113.Append(tabs50);
            paragraphProperties113.Append(indentation20);
            paragraphProperties113.Append(paragraphMarkRunProperties113);

            Run run227 = new Run();

            RunProperties runProperties227 = new RunProperties();
            RunFonts runFonts336 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties227.Append(runFonts336);
            Text text226 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text226.Text = "ለቆጣሪ መጠንቀቅና የቆጣሪውን ደህንነት መጠበቅ የደንበኛው ኃላፍነት ነው፡፡ ስለሆነም በደንበኛው ";

            run227.Append(runProperties227);
            run227.Append(text226);

            Run run228 = new Run() { RsidRunAddition = "00B65470" };

            RunProperties runProperties228 = new RunProperties();
            RunFonts runFonts337 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties228.Append(runFonts337);
            Text text227 = new Text();
            text227.Text = "ግድየለ";

            run228.Append(runProperties228);
            run228.Append(text227);

            Run run229 = new Run();

            RunProperties runProperties229 = new RunProperties();
            RunFonts runFonts338 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties229.Append(runFonts338);
            Text text228 = new Text();
            text228.Text = "ሽነት በቆጣሪው ላይ ለሚደርሰው ብልሽት ሁሉ ደንበኛው የመስጠገኛውን ወይም የማስለወጫውን ወጪ አንዲከፍል ተደርጎ ለፍጸመው ወንጀል በህግ ይጠየቃል፡፡";

            run229.Append(runProperties229);
            run229.Append(text228);

            paragraph113.Append(paragraphProperties113);
            paragraph113.Append(run227);
            paragraph113.Append(run228);
            paragraph113.Append(run229);

            Paragraph paragraph114 = new Paragraph() { RsidParagraphMarkRevision = "00A846BA", RsidParagraphAddition = "00AF310F", RsidParagraphProperties = "00A846BA", RsidRunAdditionDefault = "00AF310F" };

            ParagraphProperties paragraphProperties114 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId38 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties27 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference27 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId27 = new NumberingId() { Val = 4 };

            numberingProperties27.Append(numberingLevelReference27);
            numberingProperties27.Append(numberingId27);

            Tabs tabs51 = new Tabs();
            TabStop tabStop55 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop56 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs51.Append(tabStop55);
            tabs51.Append(tabStop56);
            Indentation indentation21 = new Indentation() { Start = "180", Hanging = "270" };

            ParagraphMarkRunProperties paragraphMarkRunProperties114 = new ParagraphMarkRunProperties();
            RunFonts runFonts339 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties114.Append(runFonts339);

            paragraphProperties114.Append(paragraphStyleId38);
            paragraphProperties114.Append(numberingProperties27);
            paragraphProperties114.Append(tabs51);
            paragraphProperties114.Append(indentation21);
            paragraphProperties114.Append(paragraphMarkRunProperties114);

            Run run230 = new Run();

            RunProperties runProperties230 = new RunProperties();
            RunFonts runFonts340 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties230.Append(runFonts340);
            Text text229 = new Text();
            text229.Text = "በዝገት ወይም በእርጅና ምክንያት በቆጣሪው ላይ የሚደርሰውን ብልሽት ቢሮ በራሱ ውጪ የጠግናል፡፡";

            run230.Append(runProperties230);
            run230.Append(text229);

            paragraph114.Append(paragraphProperties114);
            paragraph114.Append(run230);

            Paragraph paragraph115 = new Paragraph() { RsidParagraphAddition = "00E81041", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "00E81041" };

            ParagraphProperties paragraphProperties115 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId39 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties115 = new ParagraphMarkRunProperties();
            RunFonts runFonts341 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties115.Append(runFonts341);

            paragraphProperties115.Append(paragraphStyleId39);
            paragraphProperties115.Append(paragraphMarkRunProperties115);

            Run run231 = new Run();

            RunProperties runProperties231 = new RunProperties();
            RunFonts runFonts342 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties231.Append(runFonts342);
            Text text230 = new Text();
            text230.Text = "የደንበኛውን የውሃ አገልግሎት ሊያቋርጥ ይችላል፡፡";

            run231.Append(runProperties231);
            run231.Append(text230);

            paragraph115.Append(paragraphProperties115);
            paragraph115.Append(run231);

            Paragraph paragraph116 = new Paragraph() { RsidParagraphAddition = "007F201B", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "007F201B" };

            ParagraphProperties paragraphProperties116 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId40 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties116 = new ParagraphMarkRunProperties();
            RunFonts runFonts343 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties116.Append(runFonts343);

            paragraphProperties116.Append(paragraphStyleId40);
            paragraphProperties116.Append(paragraphMarkRunProperties116);

            Run run232 = new Run();

            RunProperties runProperties232 = new RunProperties();
            RunFonts runFonts344 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties232.Append(runFonts344);
            Text text231 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text231.Text = "5.3. ማንኛውም ደንበኛ ለመኖሪያ ቤት ከአንድ ሜትር ኩብ ";

            run232.Append(runProperties232);
            run232.Append(text231);

            Run run233 = new Run() { RsidRunAddition = "0082671D" };

            RunProperties runProperties233 = new RunProperties();
            RunFonts runFonts345 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties233.Append(runFonts345);
            Text text232 = new Text();
            text232.Text = "ለንግድ";

            run233.Append(runProperties233);
            run233.Append(text232);

            Run run234 = new Run();

            RunProperties runProperties234 = new RunProperties();
            RunFonts runFonts346 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties234.Append(runFonts346);
            Text text233 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text233.Text = " ቤት ከሁለት ";

            run234.Append(runProperties234);
            run234.Append(text233);

            Run run235 = new Run() { RsidRunAddition = "0082671D" };

            RunProperties runProperties235 = new RunProperties();
            RunFonts runFonts347 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties235.Append(runFonts347);
            Text text234 = new Text();
            text234.Text = "እስከ ሶስት ሜትር ኩብ በላይ የሚይዝ ማጠራቀሚያ ማስራት አይፈቀድለትም፡፡ ማጠራቀሚያውም ከፍታ ባለው ቦታና ለእይታ የሚያመች ሆኖ መሰራደት አለበት፡፡";

            run235.Append(runProperties235);
            run235.Append(text234);

            paragraph116.Append(paragraphProperties116);
            paragraph116.Append(run232);
            paragraph116.Append(run233);
            paragraph116.Append(run234);
            paragraph116.Append(run235);

            Paragraph paragraph117 = new Paragraph() { RsidParagraphAddition = "0082671D", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "0082671D" };

            ParagraphProperties paragraphProperties117 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId41 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties117 = new ParagraphMarkRunProperties();
            RunFonts runFonts348 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties117.Append(runFonts348);

            paragraphProperties117.Append(paragraphStyleId41);
            paragraphProperties117.Append(paragraphMarkRunProperties117);

            Run run236 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties236 = new RunProperties();
            RunFonts runFonts349 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold33 = new Bold();

            runProperties236.Append(runFonts349);
            runProperties236.Append(bold33);
            Text text235 = new Text();
            text235.Text = "6. የተቋረጠ የውሃ አገልግሎት";

            run236.Append(runProperties236);
            run236.Append(text235);

            Run run237 = new Run();

            RunProperties runProperties237 = new RunProperties();
            RunFonts runFonts350 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties237.Append(runFonts350);
            Text text236 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text236.Text = " ስለማደስ፡፡";

            run237.Append(runProperties237);
            run237.Append(text236);

            paragraph117.Append(paragraphProperties117);
            paragraph117.Append(run236);
            paragraph117.Append(run237);

            Paragraph paragraph118 = new Paragraph() { RsidParagraphAddition = "0082671D", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "0082671D" };

            ParagraphProperties paragraphProperties118 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId42 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties118 = new ParagraphMarkRunProperties();
            RunFonts runFonts351 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties118.Append(runFonts351);

            paragraphProperties118.Append(paragraphStyleId42);
            paragraphProperties118.Append(paragraphMarkRunProperties118);

            Run run238 = new Run();

            RunProperties runProperties238 = new RunProperties();
            RunFonts runFonts352 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties238.Append(runFonts352);
            Text text237 = new Text();
            text237.Text = "ተገቢ ማመልከቻ የተቋረጠ የውሃ አገልግሎት ማደስ ይቻላል፡፡ ሆኖም አገልግሎቱ ሊታደስ የሚችለው አገልግሎቱ የተቋረጠባቸው ምክንያቶች ከተስተካከሉና እንዲሁም ማንኛውም ተፈላጊ ሂሳቦች ተሟልተው ከተከፈሉ ብቻ ነው፡፡ በወርሃዊ ክፍያ ምክንያት የተቋረጠውን ውሃ ለማስቀጠል ደንበኛው ሰላሳ/30/ ብር መቀጫ ይከፍላል፡፡";

            run238.Append(runProperties238);
            run238.Append(text237);

            paragraph118.Append(paragraphProperties118);
            paragraph118.Append(run238);

            Paragraph paragraph119 = new Paragraph() { RsidParagraphMarkRevision = "00A13E70", RsidParagraphAddition = "005B2A12", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "005B2A12" };

            ParagraphProperties paragraphProperties119 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId43 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties119 = new ParagraphMarkRunProperties();
            RunFonts runFonts353 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold34 = new Bold();

            paragraphMarkRunProperties119.Append(runFonts353);
            paragraphMarkRunProperties119.Append(bold34);

            paragraphProperties119.Append(paragraphStyleId43);
            paragraphProperties119.Append(paragraphMarkRunProperties119);

            Run run239 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties239 = new RunProperties();
            RunFonts runFonts354 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold35 = new Bold();

            runProperties239.Append(runFonts354);
            runProperties239.Append(bold35);
            Text text238 = new Text();
            text238.Text = "7. ውል የሚሰረዝ";

            run239.Append(runProperties239);
            run239.Append(text238);

            Run run240 = new Run() { RsidRunProperties = "00A13E70", RsidRunAddition = "00614396" };

            RunProperties runProperties240 = new RunProperties();
            RunFonts runFonts355 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold36 = new Bold();

            runProperties240.Append(runFonts355);
            runProperties240.Append(bold36);
            Text text239 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text239.Text = "በት ሁኔታ ";

            run240.Append(runProperties240);
            run240.Append(text239);

            paragraph119.Append(paragraphProperties119);
            paragraph119.Append(run239);
            paragraph119.Append(run240);

            Paragraph paragraph120 = new Paragraph() { RsidParagraphAddition = "00614396", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "00614396" };

            ParagraphProperties paragraphProperties120 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId44 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties120 = new ParagraphMarkRunProperties();
            RunFonts runFonts356 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties120.Append(runFonts356);

            paragraphProperties120.Append(paragraphStyleId44);
            paragraphProperties120.Append(paragraphMarkRunProperties120);

            Run run241 = new Run();

            RunProperties runProperties241 = new RunProperties();
            RunFonts runFonts357 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties241.Append(runFonts357);
            Text text240 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text240.Text = "7.1. የውሃ አገልግሎቱን በሚመለከት ማናቸውም ስምምነት ወይም ውል ደንበኛው ውሉን ለማቋረጥ /ለመሰረዝ/ በጽሁፍ ካላስታወቀና እስኪያስታውቅ ድረስ ውሉ በስራ ላይ ይውላል፡፡ ደንበኛው በጽሁፍ ሲያስታውቅ ወይም ";

            run241.Append(runProperties241);
            run241.Append(text240);

            Run run242 = new Run();

            RunProperties runProperties242 = new RunProperties();
            RunFonts runFonts358 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties242.Append(runFonts358);
            LastRenderedPageBreak lastRenderedPageBreak12 = new LastRenderedPageBreak();
            Text text241 = new Text();
            text241.Text = "ሲጠይቅ የውሃ አገልግሎቱ ተቋርጦ ውሉ ይሰረዛል፡፡";

            run242.Append(runProperties242);
            run242.Append(lastRenderedPageBreak12);
            run242.Append(text241);

            paragraph120.Append(paragraphProperties120);
            paragraph120.Append(run241);
            paragraph120.Append(run242);

            Paragraph paragraph121 = new Paragraph() { RsidParagraphAddition = "00614396", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "00614396" };

            ParagraphProperties paragraphProperties121 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId45 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties121 = new ParagraphMarkRunProperties();
            RunFonts runFonts359 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties121.Append(runFonts359);

            paragraphProperties121.Append(paragraphStyleId45);
            paragraphProperties121.Append(paragraphMarkRunProperties121);

            Run run243 = new Run();

            RunProperties runProperties243 = new RunProperties();
            RunFonts runFonts360 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties243.Append(runFonts360);
            Text text242 = new Text();
            text242.Text = "7.2. በአንቀጽ 4 ተራ ቁጥር 4.8 መሰረት የውሃ አገልግሎት ከተቋረጠ ጊዜ አንስቶ በሰላለሳ /30/ ቀኖች ውስጥ ደንበኛው ዕዳውን ካልከፈ";

            run243.Append(runProperties243);
            run243.Append(text242);

            Run run244 = new Run() { RsidRunAddition = "00D04D40" };

            RunProperties runProperties244 = new RunProperties();
            RunFonts runFonts361 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties244.Append(runFonts361);
            Text text243 = new Text();
            text243.Text = "ለ ወዲያው ይሰረዛል፡፡";

            run244.Append(runProperties244);
            run244.Append(text243);

            paragraph121.Append(paragraphProperties121);
            paragraph121.Append(run243);
            paragraph121.Append(run244);

            Paragraph paragraph122 = new Paragraph() { RsidParagraphAddition = "00D04D40", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties122 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId46 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties122 = new ParagraphMarkRunProperties();
            RunFonts runFonts362 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties122.Append(runFonts362);

            paragraphProperties122.Append(paragraphStyleId46);
            paragraphProperties122.Append(paragraphMarkRunProperties122);

            Run run245 = new Run();

            RunProperties runProperties245 = new RunProperties();
            RunFonts runFonts363 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties245.Append(runFonts363);
            Text text244 = new Text();
            text244.Text = "7.3. ውል ከተሰረዘ ደንበኛው ያስቀመጠው ገንዘብ ይመለስለታል ከደንበኛው የሚፈለግ ቀሪ ሂሳብ ካለ ከተቀማጭ ገንዘብ ላይ ይቀነሳል፡፡";

            run245.Append(runProperties245);
            run245.Append(text244);

            paragraph122.Append(paragraphProperties122);
            paragraph122.Append(run245);

            Paragraph paragraph123 = new Paragraph() { RsidParagraphMarkRevision = "00C01404", RsidParagraphAddition = "0017091A", RsidParagraphProperties = "00E81041", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties123 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId47 = new ParagraphStyleId() { Val = "ListParagraph" };

            ParagraphMarkRunProperties paragraphMarkRunProperties123 = new ParagraphMarkRunProperties();
            RunFonts runFonts364 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties123.Append(runFonts364);

            paragraphProperties123.Append(paragraphStyleId47);
            paragraphProperties123.Append(paragraphMarkRunProperties123);

            Run run246 = new Run();

            RunProperties runProperties246 = new RunProperties();
            RunFonts runFonts365 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties246.Append(runFonts365);
            Text text245 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text245.Text = "ሆኖም ተቀማጩ ገንዘብ ዕዳውን ሳይሸፍን ከሆነ ደንበኛው በልዩነት ";

            run246.Append(runProperties246);
            run246.Append(text245);

            paragraph123.Append(paragraphProperties123);
            paragraph123.Append(run246);

            Paragraph paragraph124 = new Paragraph() { RsidParagraphAddition = "00E81041", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "00E81041" };

            ParagraphProperties paragraphProperties124 = new ParagraphProperties();
            Indentation indentation22 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties124 = new ParagraphMarkRunProperties();
            RunFonts runFonts366 = new RunFonts() { Ascii = "Nyala", HighAnsi = "Nyala", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties124.Append(runFonts366);

            paragraphProperties124.Append(indentation22);
            paragraphProperties124.Append(paragraphMarkRunProperties124);

            paragraph124.Append(paragraphProperties124);

            Paragraph paragraph125 = new Paragraph() { RsidParagraphAddition = "0017091A", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties125 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId48 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties28 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference28 = new NumberingLevelReference() { Val = 1 };
            NumberingId numberingId28 = new NumberingId() { Val = 4 };

            numberingProperties28.Append(numberingLevelReference28);
            numberingProperties28.Append(numberingId28);

            Tabs tabs52 = new Tabs();
            TabStop tabStop57 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop58 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs52.Append(tabStop57);
            tabs52.Append(tabStop58);
            Indentation indentation23 = new Indentation() { Start = "360", Hanging = "450" };

            ParagraphMarkRunProperties paragraphMarkRunProperties125 = new ParagraphMarkRunProperties();
            RunFonts runFonts367 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties125.Append(runFonts367);

            paragraphProperties125.Append(paragraphStyleId48);
            paragraphProperties125.Append(numberingProperties28);
            paragraphProperties125.Append(tabs52);
            paragraphProperties125.Append(indentation23);
            paragraphProperties125.Append(paragraphMarkRunProperties125);

            Run run247 = new Run();

            RunProperties runProperties247 = new RunProperties();
            RunFonts runFonts368 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties247.Append(runFonts368);
            Text text246 = new Text();
            text246.Text = "ደንበኛው የውሃ ቆጣሪው አለመስራቱን ወይም ጉዳት እንደደረሰበት እንዳወቀ ወዲያውኑ ለቢሮው ማስታወቅ አለበት፡፡";

            run247.Append(runProperties247);
            run247.Append(text246);

            paragraph125.Append(paragraphProperties125);
            paragraph125.Append(run247);

            Paragraph paragraph126 = new Paragraph() { RsidParagraphMarkRevision = "00A13E70", RsidParagraphAddition = "0017091A", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties126 = new ParagraphProperties();
            ParagraphStyleId paragraphStyleId49 = new ParagraphStyleId() { Val = "ListParagraph" };

            NumberingProperties numberingProperties29 = new NumberingProperties();
            NumberingLevelReference numberingLevelReference29 = new NumberingLevelReference() { Val = 0 };
            NumberingId numberingId29 = new NumberingId() { Val = 4 };

            numberingProperties29.Append(numberingLevelReference29);
            numberingProperties29.Append(numberingId29);

            Tabs tabs53 = new Tabs();
            TabStop tabStop59 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop60 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs53.Append(tabStop59);
            tabs53.Append(tabStop60);

            ParagraphMarkRunProperties paragraphMarkRunProperties126 = new ParagraphMarkRunProperties();
            RunFonts runFonts369 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold37 = new Bold();

            paragraphMarkRunProperties126.Append(runFonts369);
            paragraphMarkRunProperties126.Append(bold37);

            paragraphProperties126.Append(paragraphStyleId49);
            paragraphProperties126.Append(numberingProperties29);
            paragraphProperties126.Append(tabs53);
            paragraphProperties126.Append(paragraphMarkRunProperties126);

            Run run248 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties248 = new RunProperties();
            RunFonts runFonts370 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold38 = new Bold();

            runProperties248.Append(runFonts370);
            runProperties248.Append(bold38);
            Text text247 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text247.Text = "ስለ ውሃፍጆታ ዋጋ አከፋፈል ";

            run248.Append(runProperties248);
            run248.Append(text247);

            paragraph126.Append(paragraphProperties126);
            paragraph126.Append(run248);

            Paragraph paragraph127 = new Paragraph() { RsidParagraphAddition = "0017091A", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties127 = new ParagraphProperties();

            Tabs tabs54 = new Tabs();
            TabStop tabStop61 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop62 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs54.Append(tabStop61);
            tabs54.Append(tabStop62);

            ParagraphMarkRunProperties paragraphMarkRunProperties127 = new ParagraphMarkRunProperties();
            RunFonts runFonts371 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties127.Append(runFonts371);

            paragraphProperties127.Append(tabs54);
            paragraphProperties127.Append(paragraphMarkRunProperties127);

            Run run249 = new Run();

            RunProperties runProperties249 = new RunProperties();
            RunFonts runFonts372 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties249.Append(runFonts372);
            Text text248 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text248.Text = "4.1.ቢሮው ";

            run249.Append(runProperties249);
            run249.Append(text248);

            Run run250 = new Run() { RsidRunAddition = "00EE55E2" };

            RunProperties runProperties250 = new RunProperties();
            RunFonts runFonts373 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties250.Append(runFonts373);
            Text text249 = new Text();
            text249.Text = "ለደንበኛው ለሚሰጠው የውሃ አቅርቦት በሚወስነው ታሪፍ እና ቆጣሪው በሚያመለከተው የውሃ ፍጆታ መሰረት ደንበኛው ሂሳቡን እንዲከፍል ይጠየቃል፡፡";

            run250.Append(runProperties250);
            run250.Append(text249);

            paragraph127.Append(paragraphProperties127);
            paragraph127.Append(run249);
            paragraph127.Append(run250);

            Paragraph paragraph128 = new Paragraph() { RsidParagraphAddition = "003B2111", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "00291323" };

            ParagraphProperties paragraphProperties128 = new ParagraphProperties();

            Tabs tabs55 = new Tabs();
            TabStop tabStop63 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop64 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs55.Append(tabStop63);
            tabs55.Append(tabStop64);

            ParagraphMarkRunProperties paragraphMarkRunProperties128 = new ParagraphMarkRunProperties();
            RunFonts runFonts374 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties128.Append(runFonts374);

            paragraphProperties128.Append(tabs55);
            paragraphProperties128.Append(paragraphMarkRunProperties128);

            Run run251 = new Run();

            RunProperties runProperties251 = new RunProperties();
            RunFonts runFonts375 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties251.Append(runFonts375);
            Text text250 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text250.Text = "4.2. ቆጣሪው በትክክል የማይመዘግብ ወይም የቆመ መሆኑ በቅድሚያ ካልተረጋገጠ በቀር ቆጣሪው ";

            run251.Append(runProperties251);
            run251.Append(text250);

            Run run252 = new Run() { RsidRunAddition = "003B2111" };

            RunProperties runProperties252 = new RunProperties();
            RunFonts runFonts376 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties252.Append(runFonts376);
            Text text251 = new Text();
            text251.Text = "የመዘገበው ንባብ";

            run252.Append(runProperties252);
            run252.Append(text251);

            Run run253 = new Run() { RsidRunAddition = "00141D42" };

            RunProperties runProperties253 = new RunProperties();
            RunFonts runFonts377 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties253.Append(runFonts377);
            Text text252 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text252.Text = " ";

            run253.Append(runProperties253);
            run253.Append(text252);

            Run run254 = new Run() { RsidRunAddition = "003B2111" };

            RunProperties runProperties254 = new RunProperties();
            RunFonts runFonts378 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties254.Append(runFonts378);
            Text text253 = new Text();
            text253.Text = "በቢሮውና በደንበኛው ተቀባይነት ይጠየቃል፡፡";

            run254.Append(runProperties254);
            run254.Append(text253);

            paragraph128.Append(paragraphProperties128);
            paragraph128.Append(run251);
            paragraph128.Append(run252);
            paragraph128.Append(run253);
            paragraph128.Append(run254);

            Paragraph paragraph129 = new Paragraph() { RsidParagraphAddition = "00291323", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "003B2111" };

            ParagraphProperties paragraphProperties129 = new ParagraphProperties();

            Tabs tabs56 = new Tabs();
            TabStop tabStop65 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop66 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs56.Append(tabStop65);
            tabs56.Append(tabStop66);

            ParagraphMarkRunProperties paragraphMarkRunProperties129 = new ParagraphMarkRunProperties();
            RunFonts runFonts379 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties129.Append(runFonts379);

            paragraphProperties129.Append(tabs56);
            paragraphProperties129.Append(paragraphMarkRunProperties129);

            Run run255 = new Run();

            RunProperties runProperties255 = new RunProperties();
            RunFonts runFonts380 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties255.Append(runFonts380);
            Text text254 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text254.Text = "4.3. የቆጣሪው ትክክለኛነት አጠራጣሪ ሆኖ ሲገኝ ደንበኛው በቢሮው ህግና ደንብ መሰረት ቆጣሪውን ማስመርመር ይችላል፡፡ እንዲሁም ቢሮው በራሱ አነሳሽነት ቆጣሪውን በማነኛውም ጊዜ አንስቶ መመርመር ይችላል፡፡ ";

            run255.Append(runProperties255);
            run255.Append(text254);

            paragraph129.Append(paragraphProperties129);
            paragraph129.Append(run255);

            Paragraph paragraph130 = new Paragraph() { RsidParagraphAddition = "00ED1485", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "00ED1485" };

            ParagraphProperties paragraphProperties130 = new ParagraphProperties();

            Tabs tabs57 = new Tabs();
            TabStop tabStop67 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop68 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs57.Append(tabStop67);
            tabs57.Append(tabStop68);

            ParagraphMarkRunProperties paragraphMarkRunProperties130 = new ParagraphMarkRunProperties();
            RunFonts runFonts381 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties130.Append(runFonts381);

            paragraphProperties130.Append(tabs57);
            paragraphProperties130.Append(paragraphMarkRunProperties130);

            Run run256 = new Run();

            RunProperties runProperties256 = new RunProperties();
            RunFonts runFonts382 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties256.Append(runFonts382);
            Text text255 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text255.Text = "4.4. የቆጣሪው ብልሽት አንዳለው ከታታቀ ግን የፍጆታው መጠን ቆጣሪው በደህና ሁኔታ ላይ በነበረበት ጊዜ የነበረውን የመጨረሻው የስድስት /6/ ውር ";

            run256.Append(runProperties256);
            run256.Append(text255);

            Run run257 = new Run() { RsidRunAddition = "00AF7B22" };

            RunProperties runProperties257 = new RunProperties();
            RunFonts runFonts383 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties257.Append(runFonts383);
            Text text256 = new Text();
            text256.Text = "አማካይ የውሃ ፍጆታ በመውሰድ ይ";

            run257.Append(runProperties257);
            run257.Append(text256);

            Run run258 = new Run() { RsidRunAddition = "00AE1135" };

            RunProperties runProperties258 = new RunProperties();
            RunFonts runFonts384 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties258.Append(runFonts384);
            Text text257 = new Text();
            text257.Text = "ወሰናል፡፡ ሆኖም ደንበኛው ከተለመደው መጠን በላይ በውሃው መጠቀሙን ቢሮው በማስረጃ ሲያረጋግጥ የሚወሰነውን ደንበኛው ይከፍላል፡፡";

            run258.Append(runProperties258);
            run258.Append(text257);

            paragraph130.Append(paragraphProperties130);
            paragraph130.Append(run256);
            paragraph130.Append(run257);
            paragraph130.Append(run258);

            Paragraph paragraph131 = new Paragraph() { RsidParagraphAddition = "00AE1135", RsidParagraphProperties = "0017091A", RsidRunAdditionDefault = "00AE1135" };

            ParagraphProperties paragraphProperties131 = new ParagraphProperties();

            Tabs tabs58 = new Tabs();
            TabStop tabStop69 = new TabStop() { Val = TabStopValues.Left, Position = 360 };
            TabStop tabStop70 = new TabStop() { Val = TabStopValues.Left, Position = 720 };

            tabs58.Append(tabStop69);
            tabs58.Append(tabStop70);

            ParagraphMarkRunProperties paragraphMarkRunProperties131 = new ParagraphMarkRunProperties();
            RunFonts runFonts385 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties131.Append(runFonts385);

            paragraphProperties131.Append(tabs58);
            paragraphProperties131.Append(paragraphMarkRunProperties131);

            paragraph131.Append(paragraphProperties131);

            Paragraph paragraph132 = new Paragraph() { RsidParagraphAddition = "00AF310F", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties132 = new ParagraphProperties();
            Indentation indentation24 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties132 = new ParagraphMarkRunProperties();
            RunFonts runFonts386 = new RunFonts() { Ascii = "Nyala", HighAnsi = "Nyala", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties132.Append(runFonts386);

            paragraphProperties132.Append(indentation24);
            paragraphProperties132.Append(paragraphMarkRunProperties132);

            Run run259 = new Run();

            RunProperties runProperties259 = new RunProperties();
            RunFonts runFonts387 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties259.Append(runFonts387);
            LastRenderedPageBreak lastRenderedPageBreak13 = new LastRenderedPageBreak();
            Text text258 = new Text();
            text258.Text = "የሚፈለግበትን ሂሳብ እንዲከፍል ይጠየቃል";

            run259.Append(runProperties259);
            run259.Append(lastRenderedPageBreak13);
            run259.Append(text258);

            paragraph132.Append(paragraphProperties132);
            paragraph132.Append(run259);

            Paragraph paragraph133 = new Paragraph() { RsidParagraphAddition = "00AF310F", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "0017091A" };

            ParagraphProperties paragraphProperties133 = new ParagraphProperties();
            Indentation indentation25 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties133 = new ParagraphMarkRunProperties();
            RunFonts runFonts388 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties133.Append(runFonts388);

            paragraphProperties133.Append(indentation25);
            paragraphProperties133.Append(paragraphMarkRunProperties133);

            Run run260 = new Run() { RsidRunProperties = "0017091A" };

            RunProperties runProperties260 = new RunProperties();
            RunFonts runFonts389 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties260.Append(runFonts389);
            Text text259 = new Text();
            text259.Text = "ፈቃደኛ ካልሆነ በህግ ይከሰሳል፡፡";

            run260.Append(runProperties260);
            run260.Append(text259);

            paragraph133.Append(paragraphProperties133);
            paragraph133.Append(run260);

            Paragraph paragraph134 = new Paragraph() { RsidParagraphAddition = "000267E3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "000267E3" };

            ParagraphProperties paragraphProperties134 = new ParagraphProperties();
            Indentation indentation26 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties134 = new ParagraphMarkRunProperties();
            RunFonts runFonts390 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties134.Append(runFonts390);

            paragraphProperties134.Append(indentation26);
            paragraphProperties134.Append(paragraphMarkRunProperties134);

            Run run261 = new Run();

            RunProperties runProperties261 = new RunProperties();
            RunFonts runFonts391 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties261.Append(runFonts391);
            Text text260 = new Text();
            text260.Text = "7.4. ውል ከተሰረዘ ቢሮው ከቆጣሪ በፊት ያለውን የውሃ መስመር ማንሳት ይችላል፡፡";

            run261.Append(runProperties261);
            run261.Append(text260);

            paragraph134.Append(paragraphProperties134);
            paragraph134.Append(run261);

            Paragraph paragraph135 = new Paragraph() { RsidParagraphMarkRevision = "00A13E70", RsidParagraphAddition = "000267E3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "000267E3" };

            ParagraphProperties paragraphProperties135 = new ParagraphProperties();
            Indentation indentation27 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties135 = new ParagraphMarkRunProperties();
            RunFonts runFonts392 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold39 = new Bold();

            paragraphMarkRunProperties135.Append(runFonts392);
            paragraphMarkRunProperties135.Append(bold39);

            paragraphProperties135.Append(indentation27);
            paragraphProperties135.Append(paragraphMarkRunProperties135);

            Run run262 = new Run() { RsidRunProperties = "00A13E70" };

            RunProperties runProperties262 = new RunProperties();
            RunFonts runFonts393 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };
            Bold bold40 = new Bold();

            runProperties262.Append(runFonts393);
            runProperties262.Append(bold40);
            Text text261 = new Text();
            text261.Text = "8. ውልን ስለማሻሻል";

            run262.Append(runProperties262);
            run262.Append(text261);

            paragraph135.Append(paragraphProperties135);
            paragraph135.Append(run262);

            Paragraph paragraph136 = new Paragraph() { RsidParagraphAddition = "000267E3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "000267E3" };

            ParagraphProperties paragraphProperties136 = new ParagraphProperties();
            Indentation indentation28 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties136 = new ParagraphMarkRunProperties();
            RunFonts runFonts394 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties136.Append(runFonts394);

            paragraphProperties136.Append(indentation28);
            paragraphProperties136.Append(paragraphMarkRunProperties136);

            Run run263 = new Run();

            RunProperties runProperties263 = new RunProperties();
            RunFonts runFonts395 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties263.Append(runFonts395);
            Text text262 = new Text();
            text262.Text = "8.1. ቢሮው ይህን ውል ለማሻሻል ያለው ስልጣን በዚህ ውል የተጠበቀ ነው፡፡";

            run263.Append(runProperties263);
            run263.Append(text262);

            paragraph136.Append(paragraphProperties136);
            paragraph136.Append(run263);

            Paragraph paragraph137 = new Paragraph() { RsidParagraphAddition = "000267E3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "000267E3" };

            ParagraphProperties paragraphProperties137 = new ParagraphProperties();
            Indentation indentation29 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties137 = new ParagraphMarkRunProperties();
            RunFonts runFonts396 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties137.Append(runFonts396);

            paragraphProperties137.Append(indentation29);
            paragraphProperties137.Append(paragraphMarkRunProperties137);

            Run run264 = new Run();

            RunProperties runProperties264 = new RunProperties();
            RunFonts runFonts397 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties264.Append(runFonts397);
            Text text263 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text263.Text = "8.2. ቢሮው በየጊዜው አጽድቆ የማያወጣቸውን የአገልለግሎት ክፍያዎችን የአገልግሎት ደንቦችንና ልዩ ልዩ ስርዓቶችን ለመጠበቅና በአዲሶቹ የክፍያ ተመን ለመከፈል ደንበኛው ተስማምቷል፡፡ ";

            run264.Append(runProperties264);
            run264.Append(text263);

            paragraph137.Append(paragraphProperties137);
            paragraph137.Append(run264);

            Paragraph paragraph138 = new Paragraph() { RsidParagraphAddition = "000267E3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "000267E3" };

            ParagraphProperties paragraphProperties138 = new ParagraphProperties();
            Indentation indentation30 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties138 = new ParagraphMarkRunProperties();
            RunFonts runFonts398 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties138.Append(runFonts398);

            paragraphProperties138.Append(indentation30);
            paragraphProperties138.Append(paragraphMarkRunProperties138);

            Run run265 = new Run();

            RunProperties runProperties265 = new RunProperties();
            RunFonts runFonts399 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties265.Append(runFonts399);
            Text text264 = new Text();
            text264.Text = "8.3. ደንበኛው በቢሮ የቀረበውን የውል ማሻሻያ ካልተቀበለ ሌላ ማስጠንቀቂያ ሳያስፈልግ ቢሮ ውሉን ሊሰርዝ ይችላል፡፡";

            run265.Append(runProperties265);
            run265.Append(text264);

            paragraph138.Append(paragraphProperties138);
            paragraph138.Append(run265);

            Paragraph paragraph139 = new Paragraph() { RsidParagraphAddition = "000267E3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "000267E3" };

            ParagraphProperties paragraphProperties139 = new ParagraphProperties();
            Indentation indentation31 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties139 = new ParagraphMarkRunProperties();
            RunFonts runFonts400 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties139.Append(runFonts400);

            paragraphProperties139.Append(indentation31);
            paragraphProperties139.Append(paragraphMarkRunProperties139);

            Run run266 = new Run();

            RunProperties runProperties266 = new RunProperties();
            RunFonts runFonts401 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties266.Append(runFonts401);
            Text text265 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text265.Text = "8.4. ቢሮው በአንቀጽ 8 ተራ ቁጥር 8.2 የተጠቀሰትን እርምጃዎች ";

            run266.Append(runProperties266);
            run266.Append(text265);

            Run run267 = new Run() { RsidRunAddition = "004966F3" };

            RunProperties runProperties267 = new RunProperties();
            RunFonts runFonts402 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties267.Append(runFonts402);
            Text text266 = new Text();
            text266.Text = "ሲወስድ በአመቺ ዘዴ ለደንበኛው ይገልጻል፡፡ ይህ ቡለቱም ወገኖች ከተፈረመበት ቀን ጀምሮ የፀና ይሆናል፡፡";

            run267.Append(runProperties267);
            run267.Append(text266);

            paragraph139.Append(paragraphProperties139);
            paragraph139.Append(run266);
            paragraph139.Append(run267);

            Paragraph paragraph140 = new Paragraph() { RsidParagraphAddition = "004966F3", RsidParagraphProperties = "00D970BD", RsidRunAdditionDefault = "004966F3" };

            ParagraphProperties paragraphProperties140 = new ParagraphProperties();
            Indentation indentation32 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties140 = new ParagraphMarkRunProperties();
            RunFonts runFonts403 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties140.Append(runFonts403);

            paragraphProperties140.Append(indentation32);
            paragraphProperties140.Append(paragraphMarkRunProperties140);

            Run run268 = new Run();

            RunProperties runProperties268 = new RunProperties();
            RunFonts runFonts404 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties268.Append(runFonts404);
            Text text267 = new Text();
            text267.Text = "----------------   ------------------------------";

            run268.Append(runProperties268);
            run268.Append(text267);

            paragraph140.Append(paragraphProperties140);
            paragraph140.Append(run268);

            Paragraph paragraph141 = new Paragraph() { RsidParagraphMarkRevision = "0017091A", RsidParagraphAddition = "004966F3", RsidParagraphProperties = "005D6182", RsidRunAdditionDefault = "004966F3" };

            ParagraphProperties paragraphProperties141 = new ParagraphProperties();
            Indentation indentation33 = new Indentation() { Start = "360" };

            ParagraphMarkRunProperties paragraphMarkRunProperties141 = new ParagraphMarkRunProperties();
            RunFonts runFonts405 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            paragraphMarkRunProperties141.Append(runFonts405);

            paragraphProperties141.Append(indentation33);
            paragraphProperties141.Append(paragraphMarkRunProperties141);

            Run run269 = new Run();

            RunProperties runProperties269 = new RunProperties();
            RunFonts runFonts406 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties269.Append(runFonts406);
            Text text268 = new Text();
            text268.Text = "ደንበኛው        የመጠጥ ውሃ እና ፍሳሽ";

            run269.Append(runProperties269);
            run269.Append(text268);

            Run run270 = new Run() { RsidRunAddition = "005D6182" };

            RunProperties runProperties270 = new RunProperties();
            RunFonts runFonts407 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties270.Append(runFonts407);
            Text text269 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text269.Text = "          ";

            run270.Append(runProperties270);
            run270.Append(text269);

            Run run271 = new Run();

            RunProperties runProperties271 = new RunProperties();
            RunFonts runFonts408 = new RunFonts() { Ascii = "Power Geez Unicode1", HighAnsi = "Power Geez Unicode1", ComplexScriptTheme = ThemeFontValues.MinorHighAnsi };

            runProperties271.Append(runFonts408);
            Text text270 = new Text() { Space = SpaceProcessingModeValues.Preserve };
            text270.Text = "አገልግሎት ";

            run271.Append(runProperties271);
            run271.Append(text270);

            paragraph141.Append(paragraphProperties141);
            paragraph141.Append(run269);
            paragraph141.Append(run270);
            paragraph141.Append(run271);

            SectionProperties sectionProperties2 = new SectionProperties() { RsidRPr = "0017091A", RsidR = "004966F3", RsidSect = "00A846BA" };
            SectionType sectionType1 = new SectionType() { Val = SectionMarkValues.Continuous };
            PageSize pageSize2 = new PageSize() { Width = (UInt32Value)12240U, Height = (UInt32Value)15840U };
            PageMargin pageMargin2 = new PageMargin() { Top = 1170, Right = (UInt32Value)1440U, Bottom = 1440, Left = (UInt32Value)1440U, Header = (UInt32Value)720U, Footer = (UInt32Value)720U, Gutter = (UInt32Value)0U };
            Columns columns2 = new Columns() { Space = "720", ColumnCount = 2 };
            DocGrid docGrid2 = new DocGrid() { LinePitch = 360 };

            sectionProperties2.Append(sectionType1);
            sectionProperties2.Append(pageSize2);
            sectionProperties2.Append(pageMargin2);
            sectionProperties2.Append(columns2);
            sectionProperties2.Append(docGrid2);

            body1.Append(paragraph1);
            body1.Append(paragraph2);
            body1.Append(paragraph3);
            body1.Append(paragraph4);
            body1.Append(paragraph5);
            body1.Append(paragraph6);
            body1.Append(paragraph7);
            body1.Append(paragraph8);
            body1.Append(paragraph9);
            body1.Append(paragraph10);
            body1.Append(paragraph11);
            body1.Append(paragraph12);
            body1.Append(paragraph13);
            body1.Append(paragraph14);
            body1.Append(paragraph15);
            body1.Append(paragraph16);
            body1.Append(paragraph17);
            body1.Append(paragraph18);
            body1.Append(paragraph19);
            body1.Append(paragraph20);
            body1.Append(paragraph21);
            body1.Append(paragraph22);
            body1.Append(paragraph23);
            body1.Append(paragraph24);
            body1.Append(paragraph25);
            body1.Append(paragraph26);
            body1.Append(paragraph27);
            body1.Append(paragraph28);
            body1.Append(paragraph29);
            body1.Append(paragraph30);
            body1.Append(paragraph31);
            body1.Append(paragraph32);
            body1.Append(paragraph33);
            body1.Append(paragraph34);
            body1.Append(paragraph35);
            body1.Append(paragraph36);
            body1.Append(paragraph37);
            body1.Append(paragraph38);
            body1.Append(paragraph39);
            body1.Append(paragraph40);
            body1.Append(paragraph41);
            body1.Append(paragraph42);
            body1.Append(paragraph43);
            body1.Append(paragraph44);
            body1.Append(paragraph45);
            body1.Append(paragraph46);
            body1.Append(paragraph47);
            body1.Append(paragraph48);
            body1.Append(paragraph49);
            body1.Append(paragraph50);
            body1.Append(paragraph51);
            body1.Append(paragraph52);
            body1.Append(paragraph53);
            body1.Append(paragraph54);
            body1.Append(paragraph55);
            body1.Append(paragraph56);
            body1.Append(paragraph57);
            body1.Append(paragraph58);
            body1.Append(paragraph59);
            body1.Append(paragraph60);
            body1.Append(paragraph61);
            body1.Append(paragraph62);
            body1.Append(paragraph63);
            body1.Append(paragraph64);
            body1.Append(paragraph65);
            body1.Append(paragraph66);
            body1.Append(table1);
            body1.Append(paragraph73);
            body1.Append(paragraph74);
            body1.Append(paragraph75);
            body1.Append(paragraph76);
            body1.Append(paragraph77);
            body1.Append(paragraph78);
            body1.Append(paragraph79);
            body1.Append(paragraph80);
            body1.Append(paragraph81);
            body1.Append(paragraph82);
            body1.Append(paragraph83);
            body1.Append(paragraph84);
            body1.Append(paragraph85);
            body1.Append(paragraph86);
            body1.Append(paragraph87);
            body1.Append(paragraph88);
            body1.Append(paragraph89);
            body1.Append(paragraph90);
            body1.Append(paragraph91);
            body1.Append(paragraph92);
            body1.Append(paragraph93);
            body1.Append(paragraph94);
            body1.Append(paragraph95);
            body1.Append(paragraph96);
            body1.Append(paragraph97);
            body1.Append(paragraph98);
            body1.Append(paragraph99);
            body1.Append(paragraph100);
            body1.Append(paragraph101);
            body1.Append(paragraph102);
            body1.Append(paragraph103);
            body1.Append(paragraph104);
            body1.Append(paragraph105);
            body1.Append(paragraph106);
            body1.Append(paragraph107);
            body1.Append(paragraph108);
            body1.Append(paragraph109);
            body1.Append(paragraph110);
            body1.Append(paragraph111);
            body1.Append(paragraph112);
            body1.Append(paragraph113);
            body1.Append(paragraph114);
            body1.Append(paragraph115);
            body1.Append(paragraph116);
            body1.Append(paragraph117);
            body1.Append(paragraph118);
            body1.Append(paragraph119);
            body1.Append(paragraph120);
            body1.Append(paragraph121);
            body1.Append(paragraph122);
            body1.Append(paragraph123);
            body1.Append(paragraph124);
            body1.Append(paragraph125);
            body1.Append(paragraph126);
            body1.Append(paragraph127);
            body1.Append(paragraph128);
            body1.Append(paragraph129);
            body1.Append(paragraph130);
            body1.Append(paragraph131);
            body1.Append(paragraph132);
            body1.Append(paragraph133);
            body1.Append(paragraph134);
            body1.Append(paragraph135);
            body1.Append(paragraph136);
            body1.Append(paragraph137);
            body1.Append(paragraph138);
            body1.Append(paragraph139);
            body1.Append(paragraph140);
            body1.Append(paragraph141);
            body1.Append(sectionProperties2);

            document1.Append(body1);

            mainDocumentPart1.Document = document1;
        }

        // Generates content of fontTablePart1.
        private void GenerateFontTablePart1Content(FontTablePart fontTablePart1)
        {
            Fonts fonts1 = new Fonts() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15" } };
            fonts1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            fonts1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            fonts1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            fonts1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            fonts1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");

            Font font1 = new Font() { Name = "Times New Roman" };
            Panose1Number panose1Number1 = new Panose1Number() { Val = "02020603050405020304" };
            FontCharSet fontCharSet1 = new FontCharSet() { Val = "00" };
            FontFamily fontFamily1 = new FontFamily() { Val = FontFamilyValues.Roman };
            Pitch pitch1 = new Pitch() { Val = FontPitchValues.Variable };
            FontSignature fontSignature1 = new FontSignature() { UnicodeSignature0 = "E0002AFF", UnicodeSignature1 = "C0007841", UnicodeSignature2 = "00000009", UnicodeSignature3 = "00000000", CodePageSignature0 = "000001FF", CodePageSignature1 = "00000000" };

            font1.Append(panose1Number1);
            font1.Append(fontCharSet1);
            font1.Append(fontFamily1);
            font1.Append(pitch1);
            font1.Append(fontSignature1);

            Font font2 = new Font() { Name = "Calibri" };
            Panose1Number panose1Number2 = new Panose1Number() { Val = "020F0502020204030204" };
            FontCharSet fontCharSet2 = new FontCharSet() { Val = "00" };
            FontFamily fontFamily2 = new FontFamily() { Val = FontFamilyValues.Swiss };
            Pitch pitch2 = new Pitch() { Val = FontPitchValues.Variable };
            FontSignature fontSignature2 = new FontSignature() { UnicodeSignature0 = "E00002FF", UnicodeSignature1 = "4000ACFF", UnicodeSignature2 = "00000001", UnicodeSignature3 = "00000000", CodePageSignature0 = "0000019F", CodePageSignature1 = "00000000" };

            font2.Append(panose1Number2);
            font2.Append(fontCharSet2);
            font2.Append(fontFamily2);
            font2.Append(pitch2);
            font2.Append(fontSignature2);

            Font font3 = new Font() { Name = "Ge\'ez-1" };
            AltName altName1 = new AltName() { Val = "Segoe UI" };
            FontCharSet fontCharSet3 = new FontCharSet() { Val = "00" };
            FontFamily fontFamily3 = new FontFamily() { Val = FontFamilyValues.Swiss };
            Pitch pitch3 = new Pitch() { Val = FontPitchValues.Variable };
            FontSignature fontSignature3 = new FontSignature() { UnicodeSignature0 = "00000001", UnicodeSignature1 = "00000000", UnicodeSignature2 = "00000000", UnicodeSignature3 = "00000000", CodePageSignature0 = "00000081", CodePageSignature1 = "00000000" };

            font3.Append(altName1);
            font3.Append(fontCharSet3);
            font3.Append(fontFamily3);
            font3.Append(pitch3);
            font3.Append(fontSignature3);

            Font font4 = new Font() { Name = "Power Geez Unicode1" };
            AltName altName2 = new AltName() { Val = "Courier New" };
            FontCharSet fontCharSet4 = new FontCharSet() { Val = "00" };
            FontFamily fontFamily4 = new FontFamily() { Val = FontFamilyValues.Auto };
            Pitch pitch4 = new Pitch() { Val = FontPitchValues.Variable };
            FontSignature fontSignature4 = new FontSignature() { UnicodeSignature0 = "00000003", UnicodeSignature1 = "00000000", UnicodeSignature2 = "00000000", UnicodeSignature3 = "00000000", CodePageSignature0 = "00000001", CodePageSignature1 = "00000000" };

            font4.Append(altName2);
            font4.Append(fontCharSet4);
            font4.Append(fontFamily4);
            font4.Append(pitch4);
            font4.Append(fontSignature4);

            Font font5 = new Font() { Name = "MingLiU" };
            AltName altName3 = new AltName() { Val = "細明體" };
            Panose1Number panose1Number3 = new Panose1Number() { Val = "02020509000000000000" };
            FontCharSet fontCharSet5 = new FontCharSet() { Val = "88" };
            FontFamily fontFamily5 = new FontFamily() { Val = FontFamilyValues.Modern };
            Pitch pitch5 = new Pitch() { Val = FontPitchValues.Fixed };
            FontSignature fontSignature5 = new FontSignature() { UnicodeSignature0 = "A00002FF", UnicodeSignature1 = "28CFFCFA", UnicodeSignature2 = "00000016", UnicodeSignature3 = "00000000", CodePageSignature0 = "00100001", CodePageSignature1 = "00000000" };

            font5.Append(altName3);
            font5.Append(panose1Number3);
            font5.Append(fontCharSet5);
            font5.Append(fontFamily5);
            font5.Append(pitch5);
            font5.Append(fontSignature5);

            Font font6 = new Font() { Name = "Nyala" };
            Panose1Number panose1Number4 = new Panose1Number() { Val = "02000504070300020003" };
            FontCharSet fontCharSet6 = new FontCharSet() { Val = "00" };
            FontFamily fontFamily6 = new FontFamily() { Val = FontFamilyValues.Auto };
            Pitch pitch6 = new Pitch() { Val = FontPitchValues.Variable };
            FontSignature fontSignature6 = new FontSignature() { UnicodeSignature0 = "A000006F", UnicodeSignature1 = "00000000", UnicodeSignature2 = "00000800", UnicodeSignature3 = "00000000", CodePageSignature0 = "00000093", CodePageSignature1 = "00000000" };

            font6.Append(panose1Number4);
            font6.Append(fontCharSet6);
            font6.Append(fontFamily6);
            font6.Append(pitch6);
            font6.Append(fontSignature6);

            Font font7 = new Font() { Name = "Calibri Light" };
            Panose1Number panose1Number5 = new Panose1Number() { Val = "020F0302020204030204" };
            FontCharSet fontCharSet7 = new FontCharSet() { Val = "00" };
            FontFamily fontFamily7 = new FontFamily() { Val = FontFamilyValues.Swiss };
            Pitch pitch7 = new Pitch() { Val = FontPitchValues.Variable };
            FontSignature fontSignature7 = new FontSignature() { UnicodeSignature0 = "A00002EF", UnicodeSignature1 = "4000207B", UnicodeSignature2 = "00000000", UnicodeSignature3 = "00000000", CodePageSignature0 = "0000019F", CodePageSignature1 = "00000000" };

            font7.Append(panose1Number5);
            font7.Append(fontCharSet7);
            font7.Append(fontFamily7);
            font7.Append(pitch7);
            font7.Append(fontSignature7);

            fonts1.Append(font1);
            fonts1.Append(font2);
            fonts1.Append(font3);
            fonts1.Append(font4);
            fonts1.Append(font5);
            fonts1.Append(font6);
            fonts1.Append(font7);

            fontTablePart1.Fonts = fonts1;
        }

        // Generates content of styleDefinitionsPart1.
        private void GenerateStyleDefinitionsPart1Content(StyleDefinitionsPart styleDefinitionsPart1)
        {
            Styles styles1 = new Styles() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15" } };
            styles1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            styles1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            styles1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            styles1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            styles1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");

            DocDefaults docDefaults1 = new DocDefaults();

            RunPropertiesDefault runPropertiesDefault1 = new RunPropertiesDefault();

            RunPropertiesBaseStyle runPropertiesBaseStyle1 = new RunPropertiesBaseStyle();
            RunFonts runFonts409 = new RunFonts() { AsciiTheme = ThemeFontValues.MinorHighAnsi, HighAnsiTheme = ThemeFontValues.MinorHighAnsi, EastAsiaTheme = ThemeFontValues.MinorHighAnsi, ComplexScriptTheme = ThemeFontValues.MinorBidi };
            FontSize fontSize56 = new FontSize() { Val = "22" };
            FontSizeComplexScript fontSizeComplexScript1 = new FontSizeComplexScript() { Val = "22" };
            Languages languages1 = new Languages() { Val = "en-US", EastAsia = "en-US", Bidi = "ar-SA" };

            runPropertiesBaseStyle1.Append(runFonts409);
            runPropertiesBaseStyle1.Append(fontSize56);
            runPropertiesBaseStyle1.Append(fontSizeComplexScript1);
            runPropertiesBaseStyle1.Append(languages1);

            runPropertiesDefault1.Append(runPropertiesBaseStyle1);

            ParagraphPropertiesDefault paragraphPropertiesDefault1 = new ParagraphPropertiesDefault();

            ParagraphPropertiesBaseStyle paragraphPropertiesBaseStyle1 = new ParagraphPropertiesBaseStyle();
            SpacingBetweenLines spacingBetweenLines4 = new SpacingBetweenLines() { After = "160", Line = "259", LineRule = LineSpacingRuleValues.Auto };

            paragraphPropertiesBaseStyle1.Append(spacingBetweenLines4);

            paragraphPropertiesDefault1.Append(paragraphPropertiesBaseStyle1);

            docDefaults1.Append(runPropertiesDefault1);
            docDefaults1.Append(paragraphPropertiesDefault1);

            LatentStyles latentStyles1 = new LatentStyles() { DefaultLockedState = false, DefaultUiPriority = 99, DefaultSemiHidden = false, DefaultUnhideWhenUsed = false, DefaultPrimaryStyle = false, Count = 371 };
            LatentStyleExceptionInfo latentStyleExceptionInfo1 = new LatentStyleExceptionInfo() { Name = "Normal", UiPriority = 0, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo2 = new LatentStyleExceptionInfo() { Name = "heading 1", UiPriority = 9, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo3 = new LatentStyleExceptionInfo() { Name = "heading 2", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo4 = new LatentStyleExceptionInfo() { Name = "heading 3", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo5 = new LatentStyleExceptionInfo() { Name = "heading 4", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo6 = new LatentStyleExceptionInfo() { Name = "heading 5", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo7 = new LatentStyleExceptionInfo() { Name = "heading 6", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo8 = new LatentStyleExceptionInfo() { Name = "heading 7", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo9 = new LatentStyleExceptionInfo() { Name = "heading 8", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo10 = new LatentStyleExceptionInfo() { Name = "heading 9", UiPriority = 9, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo11 = new LatentStyleExceptionInfo() { Name = "index 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo12 = new LatentStyleExceptionInfo() { Name = "index 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo13 = new LatentStyleExceptionInfo() { Name = "index 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo14 = new LatentStyleExceptionInfo() { Name = "index 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo15 = new LatentStyleExceptionInfo() { Name = "index 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo16 = new LatentStyleExceptionInfo() { Name = "index 6", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo17 = new LatentStyleExceptionInfo() { Name = "index 7", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo18 = new LatentStyleExceptionInfo() { Name = "index 8", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo19 = new LatentStyleExceptionInfo() { Name = "index 9", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo20 = new LatentStyleExceptionInfo() { Name = "toc 1", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo21 = new LatentStyleExceptionInfo() { Name = "toc 2", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo22 = new LatentStyleExceptionInfo() { Name = "toc 3", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo23 = new LatentStyleExceptionInfo() { Name = "toc 4", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo24 = new LatentStyleExceptionInfo() { Name = "toc 5", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo25 = new LatentStyleExceptionInfo() { Name = "toc 6", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo26 = new LatentStyleExceptionInfo() { Name = "toc 7", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo27 = new LatentStyleExceptionInfo() { Name = "toc 8", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo28 = new LatentStyleExceptionInfo() { Name = "toc 9", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo29 = new LatentStyleExceptionInfo() { Name = "Normal Indent", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo30 = new LatentStyleExceptionInfo() { Name = "footnote text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo31 = new LatentStyleExceptionInfo() { Name = "annotation text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo32 = new LatentStyleExceptionInfo() { Name = "header", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo33 = new LatentStyleExceptionInfo() { Name = "footer", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo34 = new LatentStyleExceptionInfo() { Name = "index heading", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo35 = new LatentStyleExceptionInfo() { Name = "caption", UiPriority = 35, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo36 = new LatentStyleExceptionInfo() { Name = "table of figures", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo37 = new LatentStyleExceptionInfo() { Name = "envelope address", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo38 = new LatentStyleExceptionInfo() { Name = "envelope return", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo39 = new LatentStyleExceptionInfo() { Name = "footnote reference", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo40 = new LatentStyleExceptionInfo() { Name = "annotation reference", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo41 = new LatentStyleExceptionInfo() { Name = "line number", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo42 = new LatentStyleExceptionInfo() { Name = "page number", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo43 = new LatentStyleExceptionInfo() { Name = "endnote reference", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo44 = new LatentStyleExceptionInfo() { Name = "endnote text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo45 = new LatentStyleExceptionInfo() { Name = "table of authorities", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo46 = new LatentStyleExceptionInfo() { Name = "macro", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo47 = new LatentStyleExceptionInfo() { Name = "toa heading", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo48 = new LatentStyleExceptionInfo() { Name = "List", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo49 = new LatentStyleExceptionInfo() { Name = "List Bullet", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo50 = new LatentStyleExceptionInfo() { Name = "List Number", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo51 = new LatentStyleExceptionInfo() { Name = "List 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo52 = new LatentStyleExceptionInfo() { Name = "List 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo53 = new LatentStyleExceptionInfo() { Name = "List 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo54 = new LatentStyleExceptionInfo() { Name = "List 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo55 = new LatentStyleExceptionInfo() { Name = "List Bullet 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo56 = new LatentStyleExceptionInfo() { Name = "List Bullet 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo57 = new LatentStyleExceptionInfo() { Name = "List Bullet 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo58 = new LatentStyleExceptionInfo() { Name = "List Bullet 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo59 = new LatentStyleExceptionInfo() { Name = "List Number 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo60 = new LatentStyleExceptionInfo() { Name = "List Number 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo61 = new LatentStyleExceptionInfo() { Name = "List Number 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo62 = new LatentStyleExceptionInfo() { Name = "List Number 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo63 = new LatentStyleExceptionInfo() { Name = "Title", UiPriority = 10, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo64 = new LatentStyleExceptionInfo() { Name = "Closing", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo65 = new LatentStyleExceptionInfo() { Name = "Signature", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo66 = new LatentStyleExceptionInfo() { Name = "Default Paragraph Font", UiPriority = 1, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo67 = new LatentStyleExceptionInfo() { Name = "Body Text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo68 = new LatentStyleExceptionInfo() { Name = "Body Text Indent", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo69 = new LatentStyleExceptionInfo() { Name = "List Continue", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo70 = new LatentStyleExceptionInfo() { Name = "List Continue 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo71 = new LatentStyleExceptionInfo() { Name = "List Continue 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo72 = new LatentStyleExceptionInfo() { Name = "List Continue 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo73 = new LatentStyleExceptionInfo() { Name = "List Continue 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo74 = new LatentStyleExceptionInfo() { Name = "Message Header", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo75 = new LatentStyleExceptionInfo() { Name = "Subtitle", UiPriority = 11, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo76 = new LatentStyleExceptionInfo() { Name = "Salutation", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo77 = new LatentStyleExceptionInfo() { Name = "Date", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo78 = new LatentStyleExceptionInfo() { Name = "Body Text First Indent", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo79 = new LatentStyleExceptionInfo() { Name = "Body Text First Indent 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo80 = new LatentStyleExceptionInfo() { Name = "Note Heading", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo81 = new LatentStyleExceptionInfo() { Name = "Body Text 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo82 = new LatentStyleExceptionInfo() { Name = "Body Text 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo83 = new LatentStyleExceptionInfo() { Name = "Body Text Indent 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo84 = new LatentStyleExceptionInfo() { Name = "Body Text Indent 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo85 = new LatentStyleExceptionInfo() { Name = "Block Text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo86 = new LatentStyleExceptionInfo() { Name = "Hyperlink", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo87 = new LatentStyleExceptionInfo() { Name = "FollowedHyperlink", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo88 = new LatentStyleExceptionInfo() { Name = "Strong", UiPriority = 22, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo89 = new LatentStyleExceptionInfo() { Name = "Emphasis", UiPriority = 20, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo90 = new LatentStyleExceptionInfo() { Name = "Document Map", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo91 = new LatentStyleExceptionInfo() { Name = "Plain Text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo92 = new LatentStyleExceptionInfo() { Name = "E-mail Signature", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo93 = new LatentStyleExceptionInfo() { Name = "HTML Top of Form", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo94 = new LatentStyleExceptionInfo() { Name = "HTML Bottom of Form", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo95 = new LatentStyleExceptionInfo() { Name = "Normal (Web)", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo96 = new LatentStyleExceptionInfo() { Name = "HTML Acronym", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo97 = new LatentStyleExceptionInfo() { Name = "HTML Address", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo98 = new LatentStyleExceptionInfo() { Name = "HTML Cite", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo99 = new LatentStyleExceptionInfo() { Name = "HTML Code", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo100 = new LatentStyleExceptionInfo() { Name = "HTML Definition", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo101 = new LatentStyleExceptionInfo() { Name = "HTML Keyboard", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo102 = new LatentStyleExceptionInfo() { Name = "HTML Preformatted", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo103 = new LatentStyleExceptionInfo() { Name = "HTML Sample", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo104 = new LatentStyleExceptionInfo() { Name = "HTML Typewriter", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo105 = new LatentStyleExceptionInfo() { Name = "HTML Variable", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo106 = new LatentStyleExceptionInfo() { Name = "Normal Table", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo107 = new LatentStyleExceptionInfo() { Name = "annotation subject", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo108 = new LatentStyleExceptionInfo() { Name = "No List", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo109 = new LatentStyleExceptionInfo() { Name = "Outline List 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo110 = new LatentStyleExceptionInfo() { Name = "Outline List 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo111 = new LatentStyleExceptionInfo() { Name = "Outline List 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo112 = new LatentStyleExceptionInfo() { Name = "Table Simple 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo113 = new LatentStyleExceptionInfo() { Name = "Table Simple 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo114 = new LatentStyleExceptionInfo() { Name = "Table Simple 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo115 = new LatentStyleExceptionInfo() { Name = "Table Classic 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo116 = new LatentStyleExceptionInfo() { Name = "Table Classic 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo117 = new LatentStyleExceptionInfo() { Name = "Table Classic 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo118 = new LatentStyleExceptionInfo() { Name = "Table Classic 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo119 = new LatentStyleExceptionInfo() { Name = "Table Colorful 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo120 = new LatentStyleExceptionInfo() { Name = "Table Colorful 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo121 = new LatentStyleExceptionInfo() { Name = "Table Colorful 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo122 = new LatentStyleExceptionInfo() { Name = "Table Columns 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo123 = new LatentStyleExceptionInfo() { Name = "Table Columns 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo124 = new LatentStyleExceptionInfo() { Name = "Table Columns 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo125 = new LatentStyleExceptionInfo() { Name = "Table Columns 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo126 = new LatentStyleExceptionInfo() { Name = "Table Columns 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo127 = new LatentStyleExceptionInfo() { Name = "Table Grid 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo128 = new LatentStyleExceptionInfo() { Name = "Table Grid 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo129 = new LatentStyleExceptionInfo() { Name = "Table Grid 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo130 = new LatentStyleExceptionInfo() { Name = "Table Grid 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo131 = new LatentStyleExceptionInfo() { Name = "Table Grid 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo132 = new LatentStyleExceptionInfo() { Name = "Table Grid 6", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo133 = new LatentStyleExceptionInfo() { Name = "Table Grid 7", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo134 = new LatentStyleExceptionInfo() { Name = "Table Grid 8", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo135 = new LatentStyleExceptionInfo() { Name = "Table List 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo136 = new LatentStyleExceptionInfo() { Name = "Table List 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo137 = new LatentStyleExceptionInfo() { Name = "Table List 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo138 = new LatentStyleExceptionInfo() { Name = "Table List 4", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo139 = new LatentStyleExceptionInfo() { Name = "Table List 5", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo140 = new LatentStyleExceptionInfo() { Name = "Table List 6", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo141 = new LatentStyleExceptionInfo() { Name = "Table List 7", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo142 = new LatentStyleExceptionInfo() { Name = "Table List 8", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo143 = new LatentStyleExceptionInfo() { Name = "Table 3D effects 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo144 = new LatentStyleExceptionInfo() { Name = "Table 3D effects 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo145 = new LatentStyleExceptionInfo() { Name = "Table 3D effects 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo146 = new LatentStyleExceptionInfo() { Name = "Table Contemporary", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo147 = new LatentStyleExceptionInfo() { Name = "Table Elegant", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo148 = new LatentStyleExceptionInfo() { Name = "Table Professional", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo149 = new LatentStyleExceptionInfo() { Name = "Table Subtle 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo150 = new LatentStyleExceptionInfo() { Name = "Table Subtle 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo151 = new LatentStyleExceptionInfo() { Name = "Table Web 1", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo152 = new LatentStyleExceptionInfo() { Name = "Table Web 2", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo153 = new LatentStyleExceptionInfo() { Name = "Table Web 3", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo154 = new LatentStyleExceptionInfo() { Name = "Balloon Text", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo155 = new LatentStyleExceptionInfo() { Name = "Table Grid", UiPriority = 39 };
            LatentStyleExceptionInfo latentStyleExceptionInfo156 = new LatentStyleExceptionInfo() { Name = "Table Theme", SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo157 = new LatentStyleExceptionInfo() { Name = "Placeholder Text", SemiHidden = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo158 = new LatentStyleExceptionInfo() { Name = "No Spacing", UiPriority = 1, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo159 = new LatentStyleExceptionInfo() { Name = "Light Shading", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo160 = new LatentStyleExceptionInfo() { Name = "Light List", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo161 = new LatentStyleExceptionInfo() { Name = "Light Grid", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo162 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo163 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo164 = new LatentStyleExceptionInfo() { Name = "Medium List 1", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo165 = new LatentStyleExceptionInfo() { Name = "Medium List 2", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo166 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo167 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo168 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo169 = new LatentStyleExceptionInfo() { Name = "Dark List", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo170 = new LatentStyleExceptionInfo() { Name = "Colorful Shading", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo171 = new LatentStyleExceptionInfo() { Name = "Colorful List", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo172 = new LatentStyleExceptionInfo() { Name = "Colorful Grid", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo173 = new LatentStyleExceptionInfo() { Name = "Light Shading Accent 1", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo174 = new LatentStyleExceptionInfo() { Name = "Light List Accent 1", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo175 = new LatentStyleExceptionInfo() { Name = "Light Grid Accent 1", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo176 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1 Accent 1", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo177 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2 Accent 1", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo178 = new LatentStyleExceptionInfo() { Name = "Medium List 1 Accent 1", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo179 = new LatentStyleExceptionInfo() { Name = "Revision", SemiHidden = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo180 = new LatentStyleExceptionInfo() { Name = "List Paragraph", UiPriority = 34, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo181 = new LatentStyleExceptionInfo() { Name = "Quote", UiPriority = 29, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo182 = new LatentStyleExceptionInfo() { Name = "Intense Quote", UiPriority = 30, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo183 = new LatentStyleExceptionInfo() { Name = "Medium List 2 Accent 1", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo184 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1 Accent 1", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo185 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2 Accent 1", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo186 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3 Accent 1", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo187 = new LatentStyleExceptionInfo() { Name = "Dark List Accent 1", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo188 = new LatentStyleExceptionInfo() { Name = "Colorful Shading Accent 1", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo189 = new LatentStyleExceptionInfo() { Name = "Colorful List Accent 1", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo190 = new LatentStyleExceptionInfo() { Name = "Colorful Grid Accent 1", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo191 = new LatentStyleExceptionInfo() { Name = "Light Shading Accent 2", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo192 = new LatentStyleExceptionInfo() { Name = "Light List Accent 2", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo193 = new LatentStyleExceptionInfo() { Name = "Light Grid Accent 2", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo194 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1 Accent 2", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo195 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2 Accent 2", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo196 = new LatentStyleExceptionInfo() { Name = "Medium List 1 Accent 2", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo197 = new LatentStyleExceptionInfo() { Name = "Medium List 2 Accent 2", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo198 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1 Accent 2", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo199 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2 Accent 2", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo200 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3 Accent 2", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo201 = new LatentStyleExceptionInfo() { Name = "Dark List Accent 2", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo202 = new LatentStyleExceptionInfo() { Name = "Colorful Shading Accent 2", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo203 = new LatentStyleExceptionInfo() { Name = "Colorful List Accent 2", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo204 = new LatentStyleExceptionInfo() { Name = "Colorful Grid Accent 2", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo205 = new LatentStyleExceptionInfo() { Name = "Light Shading Accent 3", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo206 = new LatentStyleExceptionInfo() { Name = "Light List Accent 3", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo207 = new LatentStyleExceptionInfo() { Name = "Light Grid Accent 3", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo208 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1 Accent 3", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo209 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2 Accent 3", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo210 = new LatentStyleExceptionInfo() { Name = "Medium List 1 Accent 3", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo211 = new LatentStyleExceptionInfo() { Name = "Medium List 2 Accent 3", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo212 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1 Accent 3", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo213 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2 Accent 3", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo214 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3 Accent 3", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo215 = new LatentStyleExceptionInfo() { Name = "Dark List Accent 3", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo216 = new LatentStyleExceptionInfo() { Name = "Colorful Shading Accent 3", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo217 = new LatentStyleExceptionInfo() { Name = "Colorful List Accent 3", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo218 = new LatentStyleExceptionInfo() { Name = "Colorful Grid Accent 3", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo219 = new LatentStyleExceptionInfo() { Name = "Light Shading Accent 4", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo220 = new LatentStyleExceptionInfo() { Name = "Light List Accent 4", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo221 = new LatentStyleExceptionInfo() { Name = "Light Grid Accent 4", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo222 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1 Accent 4", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo223 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2 Accent 4", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo224 = new LatentStyleExceptionInfo() { Name = "Medium List 1 Accent 4", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo225 = new LatentStyleExceptionInfo() { Name = "Medium List 2 Accent 4", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo226 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1 Accent 4", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo227 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2 Accent 4", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo228 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3 Accent 4", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo229 = new LatentStyleExceptionInfo() { Name = "Dark List Accent 4", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo230 = new LatentStyleExceptionInfo() { Name = "Colorful Shading Accent 4", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo231 = new LatentStyleExceptionInfo() { Name = "Colorful List Accent 4", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo232 = new LatentStyleExceptionInfo() { Name = "Colorful Grid Accent 4", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo233 = new LatentStyleExceptionInfo() { Name = "Light Shading Accent 5", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo234 = new LatentStyleExceptionInfo() { Name = "Light List Accent 5", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo235 = new LatentStyleExceptionInfo() { Name = "Light Grid Accent 5", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo236 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1 Accent 5", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo237 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2 Accent 5", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo238 = new LatentStyleExceptionInfo() { Name = "Medium List 1 Accent 5", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo239 = new LatentStyleExceptionInfo() { Name = "Medium List 2 Accent 5", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo240 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1 Accent 5", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo241 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2 Accent 5", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo242 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3 Accent 5", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo243 = new LatentStyleExceptionInfo() { Name = "Dark List Accent 5", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo244 = new LatentStyleExceptionInfo() { Name = "Colorful Shading Accent 5", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo245 = new LatentStyleExceptionInfo() { Name = "Colorful List Accent 5", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo246 = new LatentStyleExceptionInfo() { Name = "Colorful Grid Accent 5", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo247 = new LatentStyleExceptionInfo() { Name = "Light Shading Accent 6", UiPriority = 60 };
            LatentStyleExceptionInfo latentStyleExceptionInfo248 = new LatentStyleExceptionInfo() { Name = "Light List Accent 6", UiPriority = 61 };
            LatentStyleExceptionInfo latentStyleExceptionInfo249 = new LatentStyleExceptionInfo() { Name = "Light Grid Accent 6", UiPriority = 62 };
            LatentStyleExceptionInfo latentStyleExceptionInfo250 = new LatentStyleExceptionInfo() { Name = "Medium Shading 1 Accent 6", UiPriority = 63 };
            LatentStyleExceptionInfo latentStyleExceptionInfo251 = new LatentStyleExceptionInfo() { Name = "Medium Shading 2 Accent 6", UiPriority = 64 };
            LatentStyleExceptionInfo latentStyleExceptionInfo252 = new LatentStyleExceptionInfo() { Name = "Medium List 1 Accent 6", UiPriority = 65 };
            LatentStyleExceptionInfo latentStyleExceptionInfo253 = new LatentStyleExceptionInfo() { Name = "Medium List 2 Accent 6", UiPriority = 66 };
            LatentStyleExceptionInfo latentStyleExceptionInfo254 = new LatentStyleExceptionInfo() { Name = "Medium Grid 1 Accent 6", UiPriority = 67 };
            LatentStyleExceptionInfo latentStyleExceptionInfo255 = new LatentStyleExceptionInfo() { Name = "Medium Grid 2 Accent 6", UiPriority = 68 };
            LatentStyleExceptionInfo latentStyleExceptionInfo256 = new LatentStyleExceptionInfo() { Name = "Medium Grid 3 Accent 6", UiPriority = 69 };
            LatentStyleExceptionInfo latentStyleExceptionInfo257 = new LatentStyleExceptionInfo() { Name = "Dark List Accent 6", UiPriority = 70 };
            LatentStyleExceptionInfo latentStyleExceptionInfo258 = new LatentStyleExceptionInfo() { Name = "Colorful Shading Accent 6", UiPriority = 71 };
            LatentStyleExceptionInfo latentStyleExceptionInfo259 = new LatentStyleExceptionInfo() { Name = "Colorful List Accent 6", UiPriority = 72 };
            LatentStyleExceptionInfo latentStyleExceptionInfo260 = new LatentStyleExceptionInfo() { Name = "Colorful Grid Accent 6", UiPriority = 73 };
            LatentStyleExceptionInfo latentStyleExceptionInfo261 = new LatentStyleExceptionInfo() { Name = "Subtle Emphasis", UiPriority = 19, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo262 = new LatentStyleExceptionInfo() { Name = "Intense Emphasis", UiPriority = 21, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo263 = new LatentStyleExceptionInfo() { Name = "Subtle Reference", UiPriority = 31, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo264 = new LatentStyleExceptionInfo() { Name = "Intense Reference", UiPriority = 32, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo265 = new LatentStyleExceptionInfo() { Name = "Book Title", UiPriority = 33, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo266 = new LatentStyleExceptionInfo() { Name = "Bibliography", UiPriority = 37, SemiHidden = true, UnhideWhenUsed = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo267 = new LatentStyleExceptionInfo() { Name = "TOC Heading", UiPriority = 39, SemiHidden = true, UnhideWhenUsed = true, PrimaryStyle = true };
            LatentStyleExceptionInfo latentStyleExceptionInfo268 = new LatentStyleExceptionInfo() { Name = "Plain Table 1", UiPriority = 41 };
            LatentStyleExceptionInfo latentStyleExceptionInfo269 = new LatentStyleExceptionInfo() { Name = "Plain Table 2", UiPriority = 42 };
            LatentStyleExceptionInfo latentStyleExceptionInfo270 = new LatentStyleExceptionInfo() { Name = "Plain Table 3", UiPriority = 43 };
            LatentStyleExceptionInfo latentStyleExceptionInfo271 = new LatentStyleExceptionInfo() { Name = "Plain Table 4", UiPriority = 44 };
            LatentStyleExceptionInfo latentStyleExceptionInfo272 = new LatentStyleExceptionInfo() { Name = "Plain Table 5", UiPriority = 45 };
            LatentStyleExceptionInfo latentStyleExceptionInfo273 = new LatentStyleExceptionInfo() { Name = "Grid Table Light", UiPriority = 40 };
            LatentStyleExceptionInfo latentStyleExceptionInfo274 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo275 = new LatentStyleExceptionInfo() { Name = "Grid Table 2", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo276 = new LatentStyleExceptionInfo() { Name = "Grid Table 3", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo277 = new LatentStyleExceptionInfo() { Name = "Grid Table 4", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo278 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo279 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo280 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo281 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light Accent 1", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo282 = new LatentStyleExceptionInfo() { Name = "Grid Table 2 Accent 1", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo283 = new LatentStyleExceptionInfo() { Name = "Grid Table 3 Accent 1", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo284 = new LatentStyleExceptionInfo() { Name = "Grid Table 4 Accent 1", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo285 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark Accent 1", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo286 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful Accent 1", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo287 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful Accent 1", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo288 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light Accent 2", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo289 = new LatentStyleExceptionInfo() { Name = "Grid Table 2 Accent 2", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo290 = new LatentStyleExceptionInfo() { Name = "Grid Table 3 Accent 2", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo291 = new LatentStyleExceptionInfo() { Name = "Grid Table 4 Accent 2", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo292 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark Accent 2", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo293 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful Accent 2", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo294 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful Accent 2", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo295 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light Accent 3", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo296 = new LatentStyleExceptionInfo() { Name = "Grid Table 2 Accent 3", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo297 = new LatentStyleExceptionInfo() { Name = "Grid Table 3 Accent 3", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo298 = new LatentStyleExceptionInfo() { Name = "Grid Table 4 Accent 3", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo299 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark Accent 3", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo300 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful Accent 3", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo301 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful Accent 3", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo302 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light Accent 4", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo303 = new LatentStyleExceptionInfo() { Name = "Grid Table 2 Accent 4", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo304 = new LatentStyleExceptionInfo() { Name = "Grid Table 3 Accent 4", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo305 = new LatentStyleExceptionInfo() { Name = "Grid Table 4 Accent 4", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo306 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark Accent 4", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo307 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful Accent 4", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo308 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful Accent 4", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo309 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light Accent 5", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo310 = new LatentStyleExceptionInfo() { Name = "Grid Table 2 Accent 5", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo311 = new LatentStyleExceptionInfo() { Name = "Grid Table 3 Accent 5", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo312 = new LatentStyleExceptionInfo() { Name = "Grid Table 4 Accent 5", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo313 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark Accent 5", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo314 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful Accent 5", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo315 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful Accent 5", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo316 = new LatentStyleExceptionInfo() { Name = "Grid Table 1 Light Accent 6", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo317 = new LatentStyleExceptionInfo() { Name = "Grid Table 2 Accent 6", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo318 = new LatentStyleExceptionInfo() { Name = "Grid Table 3 Accent 6", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo319 = new LatentStyleExceptionInfo() { Name = "Grid Table 4 Accent 6", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo320 = new LatentStyleExceptionInfo() { Name = "Grid Table 5 Dark Accent 6", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo321 = new LatentStyleExceptionInfo() { Name = "Grid Table 6 Colorful Accent 6", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo322 = new LatentStyleExceptionInfo() { Name = "Grid Table 7 Colorful Accent 6", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo323 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo324 = new LatentStyleExceptionInfo() { Name = "List Table 2", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo325 = new LatentStyleExceptionInfo() { Name = "List Table 3", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo326 = new LatentStyleExceptionInfo() { Name = "List Table 4", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo327 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo328 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo329 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo330 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light Accent 1", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo331 = new LatentStyleExceptionInfo() { Name = "List Table 2 Accent 1", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo332 = new LatentStyleExceptionInfo() { Name = "List Table 3 Accent 1", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo333 = new LatentStyleExceptionInfo() { Name = "List Table 4 Accent 1", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo334 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark Accent 1", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo335 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful Accent 1", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo336 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful Accent 1", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo337 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light Accent 2", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo338 = new LatentStyleExceptionInfo() { Name = "List Table 2 Accent 2", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo339 = new LatentStyleExceptionInfo() { Name = "List Table 3 Accent 2", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo340 = new LatentStyleExceptionInfo() { Name = "List Table 4 Accent 2", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo341 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark Accent 2", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo342 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful Accent 2", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo343 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful Accent 2", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo344 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light Accent 3", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo345 = new LatentStyleExceptionInfo() { Name = "List Table 2 Accent 3", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo346 = new LatentStyleExceptionInfo() { Name = "List Table 3 Accent 3", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo347 = new LatentStyleExceptionInfo() { Name = "List Table 4 Accent 3", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo348 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark Accent 3", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo349 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful Accent 3", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo350 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful Accent 3", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo351 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light Accent 4", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo352 = new LatentStyleExceptionInfo() { Name = "List Table 2 Accent 4", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo353 = new LatentStyleExceptionInfo() { Name = "List Table 3 Accent 4", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo354 = new LatentStyleExceptionInfo() { Name = "List Table 4 Accent 4", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo355 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark Accent 4", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo356 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful Accent 4", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo357 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful Accent 4", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo358 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light Accent 5", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo359 = new LatentStyleExceptionInfo() { Name = "List Table 2 Accent 5", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo360 = new LatentStyleExceptionInfo() { Name = "List Table 3 Accent 5", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo361 = new LatentStyleExceptionInfo() { Name = "List Table 4 Accent 5", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo362 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark Accent 5", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo363 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful Accent 5", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo364 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful Accent 5", UiPriority = 52 };
            LatentStyleExceptionInfo latentStyleExceptionInfo365 = new LatentStyleExceptionInfo() { Name = "List Table 1 Light Accent 6", UiPriority = 46 };
            LatentStyleExceptionInfo latentStyleExceptionInfo366 = new LatentStyleExceptionInfo() { Name = "List Table 2 Accent 6", UiPriority = 47 };
            LatentStyleExceptionInfo latentStyleExceptionInfo367 = new LatentStyleExceptionInfo() { Name = "List Table 3 Accent 6", UiPriority = 48 };
            LatentStyleExceptionInfo latentStyleExceptionInfo368 = new LatentStyleExceptionInfo() { Name = "List Table 4 Accent 6", UiPriority = 49 };
            LatentStyleExceptionInfo latentStyleExceptionInfo369 = new LatentStyleExceptionInfo() { Name = "List Table 5 Dark Accent 6", UiPriority = 50 };
            LatentStyleExceptionInfo latentStyleExceptionInfo370 = new LatentStyleExceptionInfo() { Name = "List Table 6 Colorful Accent 6", UiPriority = 51 };
            LatentStyleExceptionInfo latentStyleExceptionInfo371 = new LatentStyleExceptionInfo() { Name = "List Table 7 Colorful Accent 6", UiPriority = 52 };

            latentStyles1.Append(latentStyleExceptionInfo1);
            latentStyles1.Append(latentStyleExceptionInfo2);
            latentStyles1.Append(latentStyleExceptionInfo3);
            latentStyles1.Append(latentStyleExceptionInfo4);
            latentStyles1.Append(latentStyleExceptionInfo5);
            latentStyles1.Append(latentStyleExceptionInfo6);
            latentStyles1.Append(latentStyleExceptionInfo7);
            latentStyles1.Append(latentStyleExceptionInfo8);
            latentStyles1.Append(latentStyleExceptionInfo9);
            latentStyles1.Append(latentStyleExceptionInfo10);
            latentStyles1.Append(latentStyleExceptionInfo11);
            latentStyles1.Append(latentStyleExceptionInfo12);
            latentStyles1.Append(latentStyleExceptionInfo13);
            latentStyles1.Append(latentStyleExceptionInfo14);
            latentStyles1.Append(latentStyleExceptionInfo15);
            latentStyles1.Append(latentStyleExceptionInfo16);
            latentStyles1.Append(latentStyleExceptionInfo17);
            latentStyles1.Append(latentStyleExceptionInfo18);
            latentStyles1.Append(latentStyleExceptionInfo19);
            latentStyles1.Append(latentStyleExceptionInfo20);
            latentStyles1.Append(latentStyleExceptionInfo21);
            latentStyles1.Append(latentStyleExceptionInfo22);
            latentStyles1.Append(latentStyleExceptionInfo23);
            latentStyles1.Append(latentStyleExceptionInfo24);
            latentStyles1.Append(latentStyleExceptionInfo25);
            latentStyles1.Append(latentStyleExceptionInfo26);
            latentStyles1.Append(latentStyleExceptionInfo27);
            latentStyles1.Append(latentStyleExceptionInfo28);
            latentStyles1.Append(latentStyleExceptionInfo29);
            latentStyles1.Append(latentStyleExceptionInfo30);
            latentStyles1.Append(latentStyleExceptionInfo31);
            latentStyles1.Append(latentStyleExceptionInfo32);
            latentStyles1.Append(latentStyleExceptionInfo33);
            latentStyles1.Append(latentStyleExceptionInfo34);
            latentStyles1.Append(latentStyleExceptionInfo35);
            latentStyles1.Append(latentStyleExceptionInfo36);
            latentStyles1.Append(latentStyleExceptionInfo37);
            latentStyles1.Append(latentStyleExceptionInfo38);
            latentStyles1.Append(latentStyleExceptionInfo39);
            latentStyles1.Append(latentStyleExceptionInfo40);
            latentStyles1.Append(latentStyleExceptionInfo41);
            latentStyles1.Append(latentStyleExceptionInfo42);
            latentStyles1.Append(latentStyleExceptionInfo43);
            latentStyles1.Append(latentStyleExceptionInfo44);
            latentStyles1.Append(latentStyleExceptionInfo45);
            latentStyles1.Append(latentStyleExceptionInfo46);
            latentStyles1.Append(latentStyleExceptionInfo47);
            latentStyles1.Append(latentStyleExceptionInfo48);
            latentStyles1.Append(latentStyleExceptionInfo49);
            latentStyles1.Append(latentStyleExceptionInfo50);
            latentStyles1.Append(latentStyleExceptionInfo51);
            latentStyles1.Append(latentStyleExceptionInfo52);
            latentStyles1.Append(latentStyleExceptionInfo53);
            latentStyles1.Append(latentStyleExceptionInfo54);
            latentStyles1.Append(latentStyleExceptionInfo55);
            latentStyles1.Append(latentStyleExceptionInfo56);
            latentStyles1.Append(latentStyleExceptionInfo57);
            latentStyles1.Append(latentStyleExceptionInfo58);
            latentStyles1.Append(latentStyleExceptionInfo59);
            latentStyles1.Append(latentStyleExceptionInfo60);
            latentStyles1.Append(latentStyleExceptionInfo61);
            latentStyles1.Append(latentStyleExceptionInfo62);
            latentStyles1.Append(latentStyleExceptionInfo63);
            latentStyles1.Append(latentStyleExceptionInfo64);
            latentStyles1.Append(latentStyleExceptionInfo65);
            latentStyles1.Append(latentStyleExceptionInfo66);
            latentStyles1.Append(latentStyleExceptionInfo67);
            latentStyles1.Append(latentStyleExceptionInfo68);
            latentStyles1.Append(latentStyleExceptionInfo69);
            latentStyles1.Append(latentStyleExceptionInfo70);
            latentStyles1.Append(latentStyleExceptionInfo71);
            latentStyles1.Append(latentStyleExceptionInfo72);
            latentStyles1.Append(latentStyleExceptionInfo73);
            latentStyles1.Append(latentStyleExceptionInfo74);
            latentStyles1.Append(latentStyleExceptionInfo75);
            latentStyles1.Append(latentStyleExceptionInfo76);
            latentStyles1.Append(latentStyleExceptionInfo77);
            latentStyles1.Append(latentStyleExceptionInfo78);
            latentStyles1.Append(latentStyleExceptionInfo79);
            latentStyles1.Append(latentStyleExceptionInfo80);
            latentStyles1.Append(latentStyleExceptionInfo81);
            latentStyles1.Append(latentStyleExceptionInfo82);
            latentStyles1.Append(latentStyleExceptionInfo83);
            latentStyles1.Append(latentStyleExceptionInfo84);
            latentStyles1.Append(latentStyleExceptionInfo85);
            latentStyles1.Append(latentStyleExceptionInfo86);
            latentStyles1.Append(latentStyleExceptionInfo87);
            latentStyles1.Append(latentStyleExceptionInfo88);
            latentStyles1.Append(latentStyleExceptionInfo89);
            latentStyles1.Append(latentStyleExceptionInfo90);
            latentStyles1.Append(latentStyleExceptionInfo91);
            latentStyles1.Append(latentStyleExceptionInfo92);
            latentStyles1.Append(latentStyleExceptionInfo93);
            latentStyles1.Append(latentStyleExceptionInfo94);
            latentStyles1.Append(latentStyleExceptionInfo95);
            latentStyles1.Append(latentStyleExceptionInfo96);
            latentStyles1.Append(latentStyleExceptionInfo97);
            latentStyles1.Append(latentStyleExceptionInfo98);
            latentStyles1.Append(latentStyleExceptionInfo99);
            latentStyles1.Append(latentStyleExceptionInfo100);
            latentStyles1.Append(latentStyleExceptionInfo101);
            latentStyles1.Append(latentStyleExceptionInfo102);
            latentStyles1.Append(latentStyleExceptionInfo103);
            latentStyles1.Append(latentStyleExceptionInfo104);
            latentStyles1.Append(latentStyleExceptionInfo105);
            latentStyles1.Append(latentStyleExceptionInfo106);
            latentStyles1.Append(latentStyleExceptionInfo107);
            latentStyles1.Append(latentStyleExceptionInfo108);
            latentStyles1.Append(latentStyleExceptionInfo109);
            latentStyles1.Append(latentStyleExceptionInfo110);
            latentStyles1.Append(latentStyleExceptionInfo111);
            latentStyles1.Append(latentStyleExceptionInfo112);
            latentStyles1.Append(latentStyleExceptionInfo113);
            latentStyles1.Append(latentStyleExceptionInfo114);
            latentStyles1.Append(latentStyleExceptionInfo115);
            latentStyles1.Append(latentStyleExceptionInfo116);
            latentStyles1.Append(latentStyleExceptionInfo117);
            latentStyles1.Append(latentStyleExceptionInfo118);
            latentStyles1.Append(latentStyleExceptionInfo119);
            latentStyles1.Append(latentStyleExceptionInfo120);
            latentStyles1.Append(latentStyleExceptionInfo121);
            latentStyles1.Append(latentStyleExceptionInfo122);
            latentStyles1.Append(latentStyleExceptionInfo123);
            latentStyles1.Append(latentStyleExceptionInfo124);
            latentStyles1.Append(latentStyleExceptionInfo125);
            latentStyles1.Append(latentStyleExceptionInfo126);
            latentStyles1.Append(latentStyleExceptionInfo127);
            latentStyles1.Append(latentStyleExceptionInfo128);
            latentStyles1.Append(latentStyleExceptionInfo129);
            latentStyles1.Append(latentStyleExceptionInfo130);
            latentStyles1.Append(latentStyleExceptionInfo131);
            latentStyles1.Append(latentStyleExceptionInfo132);
            latentStyles1.Append(latentStyleExceptionInfo133);
            latentStyles1.Append(latentStyleExceptionInfo134);
            latentStyles1.Append(latentStyleExceptionInfo135);
            latentStyles1.Append(latentStyleExceptionInfo136);
            latentStyles1.Append(latentStyleExceptionInfo137);
            latentStyles1.Append(latentStyleExceptionInfo138);
            latentStyles1.Append(latentStyleExceptionInfo139);
            latentStyles1.Append(latentStyleExceptionInfo140);
            latentStyles1.Append(latentStyleExceptionInfo141);
            latentStyles1.Append(latentStyleExceptionInfo142);
            latentStyles1.Append(latentStyleExceptionInfo143);
            latentStyles1.Append(latentStyleExceptionInfo144);
            latentStyles1.Append(latentStyleExceptionInfo145);
            latentStyles1.Append(latentStyleExceptionInfo146);
            latentStyles1.Append(latentStyleExceptionInfo147);
            latentStyles1.Append(latentStyleExceptionInfo148);
            latentStyles1.Append(latentStyleExceptionInfo149);
            latentStyles1.Append(latentStyleExceptionInfo150);
            latentStyles1.Append(latentStyleExceptionInfo151);
            latentStyles1.Append(latentStyleExceptionInfo152);
            latentStyles1.Append(latentStyleExceptionInfo153);
            latentStyles1.Append(latentStyleExceptionInfo154);
            latentStyles1.Append(latentStyleExceptionInfo155);
            latentStyles1.Append(latentStyleExceptionInfo156);
            latentStyles1.Append(latentStyleExceptionInfo157);
            latentStyles1.Append(latentStyleExceptionInfo158);
            latentStyles1.Append(latentStyleExceptionInfo159);
            latentStyles1.Append(latentStyleExceptionInfo160);
            latentStyles1.Append(latentStyleExceptionInfo161);
            latentStyles1.Append(latentStyleExceptionInfo162);
            latentStyles1.Append(latentStyleExceptionInfo163);
            latentStyles1.Append(latentStyleExceptionInfo164);
            latentStyles1.Append(latentStyleExceptionInfo165);
            latentStyles1.Append(latentStyleExceptionInfo166);
            latentStyles1.Append(latentStyleExceptionInfo167);
            latentStyles1.Append(latentStyleExceptionInfo168);
            latentStyles1.Append(latentStyleExceptionInfo169);
            latentStyles1.Append(latentStyleExceptionInfo170);
            latentStyles1.Append(latentStyleExceptionInfo171);
            latentStyles1.Append(latentStyleExceptionInfo172);
            latentStyles1.Append(latentStyleExceptionInfo173);
            latentStyles1.Append(latentStyleExceptionInfo174);
            latentStyles1.Append(latentStyleExceptionInfo175);
            latentStyles1.Append(latentStyleExceptionInfo176);
            latentStyles1.Append(latentStyleExceptionInfo177);
            latentStyles1.Append(latentStyleExceptionInfo178);
            latentStyles1.Append(latentStyleExceptionInfo179);
            latentStyles1.Append(latentStyleExceptionInfo180);
            latentStyles1.Append(latentStyleExceptionInfo181);
            latentStyles1.Append(latentStyleExceptionInfo182);
            latentStyles1.Append(latentStyleExceptionInfo183);
            latentStyles1.Append(latentStyleExceptionInfo184);
            latentStyles1.Append(latentStyleExceptionInfo185);
            latentStyles1.Append(latentStyleExceptionInfo186);
            latentStyles1.Append(latentStyleExceptionInfo187);
            latentStyles1.Append(latentStyleExceptionInfo188);
            latentStyles1.Append(latentStyleExceptionInfo189);
            latentStyles1.Append(latentStyleExceptionInfo190);
            latentStyles1.Append(latentStyleExceptionInfo191);
            latentStyles1.Append(latentStyleExceptionInfo192);
            latentStyles1.Append(latentStyleExceptionInfo193);
            latentStyles1.Append(latentStyleExceptionInfo194);
            latentStyles1.Append(latentStyleExceptionInfo195);
            latentStyles1.Append(latentStyleExceptionInfo196);
            latentStyles1.Append(latentStyleExceptionInfo197);
            latentStyles1.Append(latentStyleExceptionInfo198);
            latentStyles1.Append(latentStyleExceptionInfo199);
            latentStyles1.Append(latentStyleExceptionInfo200);
            latentStyles1.Append(latentStyleExceptionInfo201);
            latentStyles1.Append(latentStyleExceptionInfo202);
            latentStyles1.Append(latentStyleExceptionInfo203);
            latentStyles1.Append(latentStyleExceptionInfo204);
            latentStyles1.Append(latentStyleExceptionInfo205);
            latentStyles1.Append(latentStyleExceptionInfo206);
            latentStyles1.Append(latentStyleExceptionInfo207);
            latentStyles1.Append(latentStyleExceptionInfo208);
            latentStyles1.Append(latentStyleExceptionInfo209);
            latentStyles1.Append(latentStyleExceptionInfo210);
            latentStyles1.Append(latentStyleExceptionInfo211);
            latentStyles1.Append(latentStyleExceptionInfo212);
            latentStyles1.Append(latentStyleExceptionInfo213);
            latentStyles1.Append(latentStyleExceptionInfo214);
            latentStyles1.Append(latentStyleExceptionInfo215);
            latentStyles1.Append(latentStyleExceptionInfo216);
            latentStyles1.Append(latentStyleExceptionInfo217);
            latentStyles1.Append(latentStyleExceptionInfo218);
            latentStyles1.Append(latentStyleExceptionInfo219);
            latentStyles1.Append(latentStyleExceptionInfo220);
            latentStyles1.Append(latentStyleExceptionInfo221);
            latentStyles1.Append(latentStyleExceptionInfo222);
            latentStyles1.Append(latentStyleExceptionInfo223);
            latentStyles1.Append(latentStyleExceptionInfo224);
            latentStyles1.Append(latentStyleExceptionInfo225);
            latentStyles1.Append(latentStyleExceptionInfo226);
            latentStyles1.Append(latentStyleExceptionInfo227);
            latentStyles1.Append(latentStyleExceptionInfo228);
            latentStyles1.Append(latentStyleExceptionInfo229);
            latentStyles1.Append(latentStyleExceptionInfo230);
            latentStyles1.Append(latentStyleExceptionInfo231);
            latentStyles1.Append(latentStyleExceptionInfo232);
            latentStyles1.Append(latentStyleExceptionInfo233);
            latentStyles1.Append(latentStyleExceptionInfo234);
            latentStyles1.Append(latentStyleExceptionInfo235);
            latentStyles1.Append(latentStyleExceptionInfo236);
            latentStyles1.Append(latentStyleExceptionInfo237);
            latentStyles1.Append(latentStyleExceptionInfo238);
            latentStyles1.Append(latentStyleExceptionInfo239);
            latentStyles1.Append(latentStyleExceptionInfo240);
            latentStyles1.Append(latentStyleExceptionInfo241);
            latentStyles1.Append(latentStyleExceptionInfo242);
            latentStyles1.Append(latentStyleExceptionInfo243);
            latentStyles1.Append(latentStyleExceptionInfo244);
            latentStyles1.Append(latentStyleExceptionInfo245);
            latentStyles1.Append(latentStyleExceptionInfo246);
            latentStyles1.Append(latentStyleExceptionInfo247);
            latentStyles1.Append(latentStyleExceptionInfo248);
            latentStyles1.Append(latentStyleExceptionInfo249);
            latentStyles1.Append(latentStyleExceptionInfo250);
            latentStyles1.Append(latentStyleExceptionInfo251);
            latentStyles1.Append(latentStyleExceptionInfo252);
            latentStyles1.Append(latentStyleExceptionInfo253);
            latentStyles1.Append(latentStyleExceptionInfo254);
            latentStyles1.Append(latentStyleExceptionInfo255);
            latentStyles1.Append(latentStyleExceptionInfo256);
            latentStyles1.Append(latentStyleExceptionInfo257);
            latentStyles1.Append(latentStyleExceptionInfo258);
            latentStyles1.Append(latentStyleExceptionInfo259);
            latentStyles1.Append(latentStyleExceptionInfo260);
            latentStyles1.Append(latentStyleExceptionInfo261);
            latentStyles1.Append(latentStyleExceptionInfo262);
            latentStyles1.Append(latentStyleExceptionInfo263);
            latentStyles1.Append(latentStyleExceptionInfo264);
            latentStyles1.Append(latentStyleExceptionInfo265);
            latentStyles1.Append(latentStyleExceptionInfo266);
            latentStyles1.Append(latentStyleExceptionInfo267);
            latentStyles1.Append(latentStyleExceptionInfo268);
            latentStyles1.Append(latentStyleExceptionInfo269);
            latentStyles1.Append(latentStyleExceptionInfo270);
            latentStyles1.Append(latentStyleExceptionInfo271);
            latentStyles1.Append(latentStyleExceptionInfo272);
            latentStyles1.Append(latentStyleExceptionInfo273);
            latentStyles1.Append(latentStyleExceptionInfo274);
            latentStyles1.Append(latentStyleExceptionInfo275);
            latentStyles1.Append(latentStyleExceptionInfo276);
            latentStyles1.Append(latentStyleExceptionInfo277);
            latentStyles1.Append(latentStyleExceptionInfo278);
            latentStyles1.Append(latentStyleExceptionInfo279);
            latentStyles1.Append(latentStyleExceptionInfo280);
            latentStyles1.Append(latentStyleExceptionInfo281);
            latentStyles1.Append(latentStyleExceptionInfo282);
            latentStyles1.Append(latentStyleExceptionInfo283);
            latentStyles1.Append(latentStyleExceptionInfo284);
            latentStyles1.Append(latentStyleExceptionInfo285);
            latentStyles1.Append(latentStyleExceptionInfo286);
            latentStyles1.Append(latentStyleExceptionInfo287);
            latentStyles1.Append(latentStyleExceptionInfo288);
            latentStyles1.Append(latentStyleExceptionInfo289);
            latentStyles1.Append(latentStyleExceptionInfo290);
            latentStyles1.Append(latentStyleExceptionInfo291);
            latentStyles1.Append(latentStyleExceptionInfo292);
            latentStyles1.Append(latentStyleExceptionInfo293);
            latentStyles1.Append(latentStyleExceptionInfo294);
            latentStyles1.Append(latentStyleExceptionInfo295);
            latentStyles1.Append(latentStyleExceptionInfo296);
            latentStyles1.Append(latentStyleExceptionInfo297);
            latentStyles1.Append(latentStyleExceptionInfo298);
            latentStyles1.Append(latentStyleExceptionInfo299);
            latentStyles1.Append(latentStyleExceptionInfo300);
            latentStyles1.Append(latentStyleExceptionInfo301);
            latentStyles1.Append(latentStyleExceptionInfo302);
            latentStyles1.Append(latentStyleExceptionInfo303);
            latentStyles1.Append(latentStyleExceptionInfo304);
            latentStyles1.Append(latentStyleExceptionInfo305);
            latentStyles1.Append(latentStyleExceptionInfo306);
            latentStyles1.Append(latentStyleExceptionInfo307);
            latentStyles1.Append(latentStyleExceptionInfo308);
            latentStyles1.Append(latentStyleExceptionInfo309);
            latentStyles1.Append(latentStyleExceptionInfo310);
            latentStyles1.Append(latentStyleExceptionInfo311);
            latentStyles1.Append(latentStyleExceptionInfo312);
            latentStyles1.Append(latentStyleExceptionInfo313);
            latentStyles1.Append(latentStyleExceptionInfo314);
            latentStyles1.Append(latentStyleExceptionInfo315);
            latentStyles1.Append(latentStyleExceptionInfo316);
            latentStyles1.Append(latentStyleExceptionInfo317);
            latentStyles1.Append(latentStyleExceptionInfo318);
            latentStyles1.Append(latentStyleExceptionInfo319);
            latentStyles1.Append(latentStyleExceptionInfo320);
            latentStyles1.Append(latentStyleExceptionInfo321);
            latentStyles1.Append(latentStyleExceptionInfo322);
            latentStyles1.Append(latentStyleExceptionInfo323);
            latentStyles1.Append(latentStyleExceptionInfo324);
            latentStyles1.Append(latentStyleExceptionInfo325);
            latentStyles1.Append(latentStyleExceptionInfo326);
            latentStyles1.Append(latentStyleExceptionInfo327);
            latentStyles1.Append(latentStyleExceptionInfo328);
            latentStyles1.Append(latentStyleExceptionInfo329);
            latentStyles1.Append(latentStyleExceptionInfo330);
            latentStyles1.Append(latentStyleExceptionInfo331);
            latentStyles1.Append(latentStyleExceptionInfo332);
            latentStyles1.Append(latentStyleExceptionInfo333);
            latentStyles1.Append(latentStyleExceptionInfo334);
            latentStyles1.Append(latentStyleExceptionInfo335);
            latentStyles1.Append(latentStyleExceptionInfo336);
            latentStyles1.Append(latentStyleExceptionInfo337);
            latentStyles1.Append(latentStyleExceptionInfo338);
            latentStyles1.Append(latentStyleExceptionInfo339);
            latentStyles1.Append(latentStyleExceptionInfo340);
            latentStyles1.Append(latentStyleExceptionInfo341);
            latentStyles1.Append(latentStyleExceptionInfo342);
            latentStyles1.Append(latentStyleExceptionInfo343);
            latentStyles1.Append(latentStyleExceptionInfo344);
            latentStyles1.Append(latentStyleExceptionInfo345);
            latentStyles1.Append(latentStyleExceptionInfo346);
            latentStyles1.Append(latentStyleExceptionInfo347);
            latentStyles1.Append(latentStyleExceptionInfo348);
            latentStyles1.Append(latentStyleExceptionInfo349);
            latentStyles1.Append(latentStyleExceptionInfo350);
            latentStyles1.Append(latentStyleExceptionInfo351);
            latentStyles1.Append(latentStyleExceptionInfo352);
            latentStyles1.Append(latentStyleExceptionInfo353);
            latentStyles1.Append(latentStyleExceptionInfo354);
            latentStyles1.Append(latentStyleExceptionInfo355);
            latentStyles1.Append(latentStyleExceptionInfo356);
            latentStyles1.Append(latentStyleExceptionInfo357);
            latentStyles1.Append(latentStyleExceptionInfo358);
            latentStyles1.Append(latentStyleExceptionInfo359);
            latentStyles1.Append(latentStyleExceptionInfo360);
            latentStyles1.Append(latentStyleExceptionInfo361);
            latentStyles1.Append(latentStyleExceptionInfo362);
            latentStyles1.Append(latentStyleExceptionInfo363);
            latentStyles1.Append(latentStyleExceptionInfo364);
            latentStyles1.Append(latentStyleExceptionInfo365);
            latentStyles1.Append(latentStyleExceptionInfo366);
            latentStyles1.Append(latentStyleExceptionInfo367);
            latentStyles1.Append(latentStyleExceptionInfo368);
            latentStyles1.Append(latentStyleExceptionInfo369);
            latentStyles1.Append(latentStyleExceptionInfo370);
            latentStyles1.Append(latentStyleExceptionInfo371);

            Style style1 = new Style() { Type = StyleValues.Paragraph, StyleId = "Normal", Default = true };
            StyleName styleName1 = new StyleName() { Val = "Normal" };
            PrimaryStyle primaryStyle1 = new PrimaryStyle();

            style1.Append(styleName1);
            style1.Append(primaryStyle1);

            Style style2 = new Style() { Type = StyleValues.Character, StyleId = "DefaultParagraphFont", Default = true };
            StyleName styleName2 = new StyleName() { Val = "Default Paragraph Font" };
            UIPriority uIPriority1 = new UIPriority() { Val = 1 };
            SemiHidden semiHidden1 = new SemiHidden();
            UnhideWhenUsed unhideWhenUsed1 = new UnhideWhenUsed();

            style2.Append(styleName2);
            style2.Append(uIPriority1);
            style2.Append(semiHidden1);
            style2.Append(unhideWhenUsed1);

            Style style3 = new Style() { Type = StyleValues.Table, StyleId = "TableNormal", Default = true };
            StyleName styleName3 = new StyleName() { Val = "Normal Table" };
            UIPriority uIPriority2 = new UIPriority() { Val = 99 };
            SemiHidden semiHidden2 = new SemiHidden();
            UnhideWhenUsed unhideWhenUsed2 = new UnhideWhenUsed();

            StyleTableProperties styleTableProperties1 = new StyleTableProperties();
            TableIndentation tableIndentation1 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };

            TableCellMarginDefault tableCellMarginDefault1 = new TableCellMarginDefault();
            TopMargin topMargin1 = new TopMargin() { Width = "0", Type = TableWidthUnitValues.Dxa };
            TableCellLeftMargin tableCellLeftMargin1 = new TableCellLeftMargin() { Width = 108, Type = TableWidthValues.Dxa };
            BottomMargin bottomMargin1 = new BottomMargin() { Width = "0", Type = TableWidthUnitValues.Dxa };
            TableCellRightMargin tableCellRightMargin1 = new TableCellRightMargin() { Width = 108, Type = TableWidthValues.Dxa };

            tableCellMarginDefault1.Append(topMargin1);
            tableCellMarginDefault1.Append(tableCellLeftMargin1);
            tableCellMarginDefault1.Append(bottomMargin1);
            tableCellMarginDefault1.Append(tableCellRightMargin1);

            styleTableProperties1.Append(tableIndentation1);
            styleTableProperties1.Append(tableCellMarginDefault1);

            style3.Append(styleName3);
            style3.Append(uIPriority2);
            style3.Append(semiHidden2);
            style3.Append(unhideWhenUsed2);
            style3.Append(styleTableProperties1);

            Style style4 = new Style() { Type = StyleValues.Numbering, StyleId = "NoList", Default = true };
            StyleName styleName4 = new StyleName() { Val = "No List" };
            UIPriority uIPriority3 = new UIPriority() { Val = 99 };
            SemiHidden semiHidden3 = new SemiHidden();
            UnhideWhenUsed unhideWhenUsed3 = new UnhideWhenUsed();

            style4.Append(styleName4);
            style4.Append(uIPriority3);
            style4.Append(semiHidden3);
            style4.Append(unhideWhenUsed3);

            Style style5 = new Style() { Type = StyleValues.Paragraph, StyleId = "ListParagraph" };
            StyleName styleName5 = new StyleName() { Val = "List Paragraph" };
            BasedOn basedOn1 = new BasedOn() { Val = "Normal" };
            UIPriority uIPriority4 = new UIPriority() { Val = 34 };
            PrimaryStyle primaryStyle2 = new PrimaryStyle();
            Rsid rsid1 = new Rsid() { Val = "00DE78CA" };

            StyleParagraphProperties styleParagraphProperties1 = new StyleParagraphProperties();
            Indentation indentation34 = new Indentation() { Start = "720" };
            ContextualSpacing contextualSpacing1 = new ContextualSpacing();

            styleParagraphProperties1.Append(indentation34);
            styleParagraphProperties1.Append(contextualSpacing1);

            style5.Append(styleName5);
            style5.Append(basedOn1);
            style5.Append(uIPriority4);
            style5.Append(primaryStyle2);
            style5.Append(rsid1);
            style5.Append(styleParagraphProperties1);

            Style style6 = new Style() { Type = StyleValues.Paragraph, StyleId = "Header" };
            StyleName styleName6 = new StyleName() { Val = "header" };
            BasedOn basedOn2 = new BasedOn() { Val = "Normal" };
            LinkedStyle linkedStyle1 = new LinkedStyle() { Val = "HeaderChar" };
            UIPriority uIPriority5 = new UIPriority() { Val = 99 };
            UnhideWhenUsed unhideWhenUsed4 = new UnhideWhenUsed();
            Rsid rsid2 = new Rsid() { Val = "002F4BF7" };

            StyleParagraphProperties styleParagraphProperties2 = new StyleParagraphProperties();

            Tabs tabs59 = new Tabs();
            TabStop tabStop71 = new TabStop() { Val = TabStopValues.Center, Position = 4680 };
            TabStop tabStop72 = new TabStop() { Val = TabStopValues.Right, Position = 9360 };

            tabs59.Append(tabStop71);
            tabs59.Append(tabStop72);
            SpacingBetweenLines spacingBetweenLines5 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            styleParagraphProperties2.Append(tabs59);
            styleParagraphProperties2.Append(spacingBetweenLines5);

            style6.Append(styleName6);
            style6.Append(basedOn2);
            style6.Append(linkedStyle1);
            style6.Append(uIPriority5);
            style6.Append(unhideWhenUsed4);
            style6.Append(rsid2);
            style6.Append(styleParagraphProperties2);

            Style style7 = new Style() { Type = StyleValues.Character, StyleId = "HeaderChar", CustomStyle = true };
            StyleName styleName7 = new StyleName() { Val = "Header Char" };
            BasedOn basedOn3 = new BasedOn() { Val = "DefaultParagraphFont" };
            LinkedStyle linkedStyle2 = new LinkedStyle() { Val = "Header" };
            UIPriority uIPriority6 = new UIPriority() { Val = 99 };
            Rsid rsid3 = new Rsid() { Val = "002F4BF7" };

            style7.Append(styleName7);
            style7.Append(basedOn3);
            style7.Append(linkedStyle2);
            style7.Append(uIPriority6);
            style7.Append(rsid3);

            Style style8 = new Style() { Type = StyleValues.Paragraph, StyleId = "Footer" };
            StyleName styleName8 = new StyleName() { Val = "footer" };
            BasedOn basedOn4 = new BasedOn() { Val = "Normal" };
            LinkedStyle linkedStyle3 = new LinkedStyle() { Val = "FooterChar" };
            UIPriority uIPriority7 = new UIPriority() { Val = 99 };
            UnhideWhenUsed unhideWhenUsed5 = new UnhideWhenUsed();
            Rsid rsid4 = new Rsid() { Val = "002F4BF7" };

            StyleParagraphProperties styleParagraphProperties3 = new StyleParagraphProperties();

            Tabs tabs60 = new Tabs();
            TabStop tabStop73 = new TabStop() { Val = TabStopValues.Center, Position = 4680 };
            TabStop tabStop74 = new TabStop() { Val = TabStopValues.Right, Position = 9360 };

            tabs60.Append(tabStop73);
            tabs60.Append(tabStop74);
            SpacingBetweenLines spacingBetweenLines6 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            styleParagraphProperties3.Append(tabs60);
            styleParagraphProperties3.Append(spacingBetweenLines6);

            style8.Append(styleName8);
            style8.Append(basedOn4);
            style8.Append(linkedStyle3);
            style8.Append(uIPriority7);
            style8.Append(unhideWhenUsed5);
            style8.Append(rsid4);
            style8.Append(styleParagraphProperties3);

            Style style9 = new Style() { Type = StyleValues.Character, StyleId = "FooterChar", CustomStyle = true };
            StyleName styleName9 = new StyleName() { Val = "Footer Char" };
            BasedOn basedOn5 = new BasedOn() { Val = "DefaultParagraphFont" };
            LinkedStyle linkedStyle4 = new LinkedStyle() { Val = "Footer" };
            UIPriority uIPriority8 = new UIPriority() { Val = 99 };
            Rsid rsid5 = new Rsid() { Val = "002F4BF7" };

            style9.Append(styleName9);
            style9.Append(basedOn5);
            style9.Append(linkedStyle4);
            style9.Append(uIPriority8);
            style9.Append(rsid5);

            Style style10 = new Style() { Type = StyleValues.Table, StyleId = "TableGrid" };
            StyleName styleName10 = new StyleName() { Val = "Table Grid" };
            BasedOn basedOn6 = new BasedOn() { Val = "TableNormal" };
            UIPriority uIPriority9 = new UIPriority() { Val = 39 };
            Rsid rsid6 = new Rsid() { Val = "00BB36BD" };

            StyleParagraphProperties styleParagraphProperties4 = new StyleParagraphProperties();
            SpacingBetweenLines spacingBetweenLines7 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            styleParagraphProperties4.Append(spacingBetweenLines7);

            StyleTableProperties styleTableProperties2 = new StyleTableProperties();
            TableIndentation tableIndentation2 = new TableIndentation() { Width = 0, Type = TableWidthUnitValues.Dxa };

            TableBorders tableBorders2 = new TableBorders();
            TopBorder topBorder4 = new TopBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
            LeftBorder leftBorder2 = new LeftBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
            BottomBorder bottomBorder4 = new BottomBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
            RightBorder rightBorder2 = new RightBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
            InsideHorizontalBorder insideHorizontalBorder2 = new InsideHorizontalBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };
            InsideVerticalBorder insideVerticalBorder2 = new InsideVerticalBorder() { Val = BorderValues.Single, Color = "auto", Size = (UInt32Value)4U, Space = (UInt32Value)0U };

            tableBorders2.Append(topBorder4);
            tableBorders2.Append(leftBorder2);
            tableBorders2.Append(bottomBorder4);
            tableBorders2.Append(rightBorder2);
            tableBorders2.Append(insideHorizontalBorder2);
            tableBorders2.Append(insideVerticalBorder2);

            TableCellMarginDefault tableCellMarginDefault2 = new TableCellMarginDefault();
            TopMargin topMargin2 = new TopMargin() { Width = "0", Type = TableWidthUnitValues.Dxa };
            TableCellLeftMargin tableCellLeftMargin2 = new TableCellLeftMargin() { Width = 108, Type = TableWidthValues.Dxa };
            BottomMargin bottomMargin2 = new BottomMargin() { Width = "0", Type = TableWidthUnitValues.Dxa };
            TableCellRightMargin tableCellRightMargin2 = new TableCellRightMargin() { Width = 108, Type = TableWidthValues.Dxa };

            tableCellMarginDefault2.Append(topMargin2);
            tableCellMarginDefault2.Append(tableCellLeftMargin2);
            tableCellMarginDefault2.Append(bottomMargin2);
            tableCellMarginDefault2.Append(tableCellRightMargin2);

            styleTableProperties2.Append(tableIndentation2);
            styleTableProperties2.Append(tableBorders2);
            styleTableProperties2.Append(tableCellMarginDefault2);

            style10.Append(styleName10);
            style10.Append(basedOn6);
            style10.Append(uIPriority9);
            style10.Append(rsid6);
            style10.Append(styleParagraphProperties4);
            style10.Append(styleTableProperties2);

            styles1.Append(docDefaults1);
            styles1.Append(latentStyles1);
            styles1.Append(style1);
            styles1.Append(style2);
            styles1.Append(style3);
            styles1.Append(style4);
            styles1.Append(style5);
            styles1.Append(style6);
            styles1.Append(style7);
            styles1.Append(style8);
            styles1.Append(style9);
            styles1.Append(style10);

            styleDefinitionsPart1.Styles = styles1;
        }

        // Generates content of endnotesPart1.
        private void GenerateEndnotesPart1Content(EndnotesPart endnotesPart1)
        {
            Endnotes endnotes1 = new Endnotes() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15 wp14" } };
            endnotes1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
            endnotes1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            endnotes1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            endnotes1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            endnotes1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            endnotes1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            endnotes1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
            endnotes1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            endnotes1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            endnotes1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            endnotes1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            endnotes1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");
            endnotes1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
            endnotes1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
            endnotes1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            endnotes1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

            Endnote endnote1 = new Endnote() { Type = FootnoteEndnoteValues.Separator, Id = -1 };

            Paragraph paragraph142 = new Paragraph() { RsidParagraphAddition = "00931EEF", RsidParagraphProperties = "002F4BF7", RsidRunAdditionDefault = "00931EEF" };

            ParagraphProperties paragraphProperties142 = new ParagraphProperties();
            SpacingBetweenLines spacingBetweenLines8 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            paragraphProperties142.Append(spacingBetweenLines8);

            Run run272 = new Run();
            SeparatorMark separatorMark1 = new SeparatorMark();

            run272.Append(separatorMark1);

            paragraph142.Append(paragraphProperties142);
            paragraph142.Append(run272);

            endnote1.Append(paragraph142);

            Endnote endnote2 = new Endnote() { Type = FootnoteEndnoteValues.ContinuationSeparator, Id = 0 };

            Paragraph paragraph143 = new Paragraph() { RsidParagraphAddition = "00931EEF", RsidParagraphProperties = "002F4BF7", RsidRunAdditionDefault = "00931EEF" };

            ParagraphProperties paragraphProperties143 = new ParagraphProperties();
            SpacingBetweenLines spacingBetweenLines9 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            paragraphProperties143.Append(spacingBetweenLines9);

            Run run273 = new Run();
            ContinuationSeparatorMark continuationSeparatorMark1 = new ContinuationSeparatorMark();

            run273.Append(continuationSeparatorMark1);

            paragraph143.Append(paragraphProperties143);
            paragraph143.Append(run273);

            endnote2.Append(paragraph143);

            endnotes1.Append(endnote1);
            endnotes1.Append(endnote2);

            endnotesPart1.Endnotes = endnotes1;
        }

        // Generates content of numberingDefinitionsPart1.
        private void GenerateNumberingDefinitionsPart1Content(NumberingDefinitionsPart numberingDefinitionsPart1)
        {
            Numbering numbering1 = new Numbering() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15 wp14" } };
            numbering1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
            numbering1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            numbering1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            numbering1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            numbering1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            numbering1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            numbering1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
            numbering1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            numbering1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            numbering1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            numbering1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            numbering1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");
            numbering1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
            numbering1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
            numbering1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            numbering1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

            AbstractNum abstractNum1 = new AbstractNum() { AbstractNumberId = 0 };
            Nsid nsid1 = new Nsid() { Val = "1BDE5CC8" };
            MultiLevelType multiLevelType1 = new MultiLevelType() { Val = MultiLevelValues.Multilevel };
            TemplateCode templateCode1 = new TemplateCode() { Val = "739478CC" };

            Level level1 = new Level() { LevelIndex = 0 };
            StartNumberingValue startNumberingValue1 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat1 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            LevelText levelText1 = new LevelText() { Val = "%1." };
            LevelJustification levelJustification1 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties1 = new PreviousParagraphProperties();
            Indentation indentation35 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties1.Append(indentation35);

            NumberingSymbolRunProperties numberingSymbolRunProperties1 = new NumberingSymbolRunProperties();
            RunFonts runFonts410 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties1.Append(runFonts410);

            level1.Append(startNumberingValue1);
            level1.Append(numberingFormat1);
            level1.Append(levelText1);
            level1.Append(levelJustification1);
            level1.Append(previousParagraphProperties1);
            level1.Append(numberingSymbolRunProperties1);

            Level level2 = new Level() { LevelIndex = 1 };
            StartNumberingValue startNumberingValue2 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat2 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle1 = new IsLegalNumberingStyle();
            LevelText levelText2 = new LevelText() { Val = "%1.%2." };
            LevelJustification levelJustification2 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties2 = new PreviousParagraphProperties();
            Indentation indentation36 = new Indentation() { Start = "1260", Hanging = "720" };

            previousParagraphProperties2.Append(indentation36);

            NumberingSymbolRunProperties numberingSymbolRunProperties2 = new NumberingSymbolRunProperties();
            RunFonts runFonts411 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties2.Append(runFonts411);

            level2.Append(startNumberingValue2);
            level2.Append(numberingFormat2);
            level2.Append(isLegalNumberingStyle1);
            level2.Append(levelText2);
            level2.Append(levelJustification2);
            level2.Append(previousParagraphProperties2);
            level2.Append(numberingSymbolRunProperties2);

            Level level3 = new Level() { LevelIndex = 2 };
            StartNumberingValue startNumberingValue3 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat3 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle2 = new IsLegalNumberingStyle();
            LevelText levelText3 = new LevelText() { Val = "%1.%2.%3." };
            LevelJustification levelJustification3 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties3 = new PreviousParagraphProperties();
            Indentation indentation37 = new Indentation() { Start = "1800", Hanging = "720" };

            previousParagraphProperties3.Append(indentation37);

            NumberingSymbolRunProperties numberingSymbolRunProperties3 = new NumberingSymbolRunProperties();
            RunFonts runFonts412 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties3.Append(runFonts412);

            level3.Append(startNumberingValue3);
            level3.Append(numberingFormat3);
            level3.Append(isLegalNumberingStyle2);
            level3.Append(levelText3);
            level3.Append(levelJustification3);
            level3.Append(previousParagraphProperties3);
            level3.Append(numberingSymbolRunProperties3);

            Level level4 = new Level() { LevelIndex = 3 };
            StartNumberingValue startNumberingValue4 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat4 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle3 = new IsLegalNumberingStyle();
            LevelText levelText4 = new LevelText() { Val = "%1.%2.%3.%4." };
            LevelJustification levelJustification4 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties4 = new PreviousParagraphProperties();
            Indentation indentation38 = new Indentation() { Start = "2520", Hanging = "1080" };

            previousParagraphProperties4.Append(indentation38);

            NumberingSymbolRunProperties numberingSymbolRunProperties4 = new NumberingSymbolRunProperties();
            RunFonts runFonts413 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties4.Append(runFonts413);

            level4.Append(startNumberingValue4);
            level4.Append(numberingFormat4);
            level4.Append(isLegalNumberingStyle3);
            level4.Append(levelText4);
            level4.Append(levelJustification4);
            level4.Append(previousParagraphProperties4);
            level4.Append(numberingSymbolRunProperties4);

            Level level5 = new Level() { LevelIndex = 4 };
            StartNumberingValue startNumberingValue5 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat5 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle4 = new IsLegalNumberingStyle();
            LevelText levelText5 = new LevelText() { Val = "%1.%2.%3.%4.%5." };
            LevelJustification levelJustification5 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties5 = new PreviousParagraphProperties();
            Indentation indentation39 = new Indentation() { Start = "2880", Hanging = "1080" };

            previousParagraphProperties5.Append(indentation39);

            NumberingSymbolRunProperties numberingSymbolRunProperties5 = new NumberingSymbolRunProperties();
            RunFonts runFonts414 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties5.Append(runFonts414);

            level5.Append(startNumberingValue5);
            level5.Append(numberingFormat5);
            level5.Append(isLegalNumberingStyle4);
            level5.Append(levelText5);
            level5.Append(levelJustification5);
            level5.Append(previousParagraphProperties5);
            level5.Append(numberingSymbolRunProperties5);

            Level level6 = new Level() { LevelIndex = 5 };
            StartNumberingValue startNumberingValue6 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat6 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle5 = new IsLegalNumberingStyle();
            LevelText levelText6 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6." };
            LevelJustification levelJustification6 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties6 = new PreviousParagraphProperties();
            Indentation indentation40 = new Indentation() { Start = "3600", Hanging = "1440" };

            previousParagraphProperties6.Append(indentation40);

            NumberingSymbolRunProperties numberingSymbolRunProperties6 = new NumberingSymbolRunProperties();
            RunFonts runFonts415 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties6.Append(runFonts415);

            level6.Append(startNumberingValue6);
            level6.Append(numberingFormat6);
            level6.Append(isLegalNumberingStyle5);
            level6.Append(levelText6);
            level6.Append(levelJustification6);
            level6.Append(previousParagraphProperties6);
            level6.Append(numberingSymbolRunProperties6);

            Level level7 = new Level() { LevelIndex = 6 };
            StartNumberingValue startNumberingValue7 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat7 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle6 = new IsLegalNumberingStyle();
            LevelText levelText7 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7." };
            LevelJustification levelJustification7 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties7 = new PreviousParagraphProperties();
            Indentation indentation41 = new Indentation() { Start = "3960", Hanging = "1440" };

            previousParagraphProperties7.Append(indentation41);

            NumberingSymbolRunProperties numberingSymbolRunProperties7 = new NumberingSymbolRunProperties();
            RunFonts runFonts416 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties7.Append(runFonts416);

            level7.Append(startNumberingValue7);
            level7.Append(numberingFormat7);
            level7.Append(isLegalNumberingStyle6);
            level7.Append(levelText7);
            level7.Append(levelJustification7);
            level7.Append(previousParagraphProperties7);
            level7.Append(numberingSymbolRunProperties7);

            Level level8 = new Level() { LevelIndex = 7 };
            StartNumberingValue startNumberingValue8 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat8 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle7 = new IsLegalNumberingStyle();
            LevelText levelText8 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8." };
            LevelJustification levelJustification8 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties8 = new PreviousParagraphProperties();
            Indentation indentation42 = new Indentation() { Start = "4680", Hanging = "1800" };

            previousParagraphProperties8.Append(indentation42);

            NumberingSymbolRunProperties numberingSymbolRunProperties8 = new NumberingSymbolRunProperties();
            RunFonts runFonts417 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties8.Append(runFonts417);

            level8.Append(startNumberingValue8);
            level8.Append(numberingFormat8);
            level8.Append(isLegalNumberingStyle7);
            level8.Append(levelText8);
            level8.Append(levelJustification8);
            level8.Append(previousParagraphProperties8);
            level8.Append(numberingSymbolRunProperties8);

            Level level9 = new Level() { LevelIndex = 8 };
            StartNumberingValue startNumberingValue9 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat9 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle8 = new IsLegalNumberingStyle();
            LevelText levelText9 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8.%9." };
            LevelJustification levelJustification9 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties9 = new PreviousParagraphProperties();
            Indentation indentation43 = new Indentation() { Start = "5040", Hanging = "1800" };

            previousParagraphProperties9.Append(indentation43);

            NumberingSymbolRunProperties numberingSymbolRunProperties9 = new NumberingSymbolRunProperties();
            RunFonts runFonts418 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties9.Append(runFonts418);

            level9.Append(startNumberingValue9);
            level9.Append(numberingFormat9);
            level9.Append(isLegalNumberingStyle8);
            level9.Append(levelText9);
            level9.Append(levelJustification9);
            level9.Append(previousParagraphProperties9);
            level9.Append(numberingSymbolRunProperties9);

            abstractNum1.Append(nsid1);
            abstractNum1.Append(multiLevelType1);
            abstractNum1.Append(templateCode1);
            abstractNum1.Append(level1);
            abstractNum1.Append(level2);
            abstractNum1.Append(level3);
            abstractNum1.Append(level4);
            abstractNum1.Append(level5);
            abstractNum1.Append(level6);
            abstractNum1.Append(level7);
            abstractNum1.Append(level8);
            abstractNum1.Append(level9);

            AbstractNum abstractNum2 = new AbstractNum() { AbstractNumberId = 1 };
            Nsid nsid2 = new Nsid() { Val = "1E5E218E" };
            MultiLevelType multiLevelType2 = new MultiLevelType() { Val = MultiLevelValues.Multilevel };
            TemplateCode templateCode2 = new TemplateCode() { Val = "3CA25E52" };

            Level level10 = new Level() { LevelIndex = 0 };
            StartNumberingValue startNumberingValue10 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat10 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            LevelText levelText10 = new LevelText() { Val = "%1." };
            LevelJustification levelJustification10 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties10 = new PreviousParagraphProperties();
            Indentation indentation44 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties10.Append(indentation44);

            NumberingSymbolRunProperties numberingSymbolRunProperties10 = new NumberingSymbolRunProperties();
            RunFonts runFonts419 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties10.Append(runFonts419);

            level10.Append(startNumberingValue10);
            level10.Append(numberingFormat10);
            level10.Append(levelText10);
            level10.Append(levelJustification10);
            level10.Append(previousParagraphProperties10);
            level10.Append(numberingSymbolRunProperties10);

            Level level11 = new Level() { LevelIndex = 1 };
            StartNumberingValue startNumberingValue11 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat11 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle9 = new IsLegalNumberingStyle();
            LevelText levelText11 = new LevelText() { Val = "%1.%2." };
            LevelJustification levelJustification11 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties11 = new PreviousParagraphProperties();
            Indentation indentation45 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties11.Append(indentation45);

            NumberingSymbolRunProperties numberingSymbolRunProperties11 = new NumberingSymbolRunProperties();
            RunFonts runFonts420 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties11.Append(runFonts420);

            level11.Append(startNumberingValue11);
            level11.Append(numberingFormat11);
            level11.Append(isLegalNumberingStyle9);
            level11.Append(levelText11);
            level11.Append(levelJustification11);
            level11.Append(previousParagraphProperties11);
            level11.Append(numberingSymbolRunProperties11);

            Level level12 = new Level() { LevelIndex = 2 };
            StartNumberingValue startNumberingValue12 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat12 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle10 = new IsLegalNumberingStyle();
            LevelText levelText12 = new LevelText() { Val = "%1.%2.%3." };
            LevelJustification levelJustification12 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties12 = new PreviousParagraphProperties();
            Indentation indentation46 = new Indentation() { Start = "1080", Hanging = "720" };

            previousParagraphProperties12.Append(indentation46);

            NumberingSymbolRunProperties numberingSymbolRunProperties12 = new NumberingSymbolRunProperties();
            RunFonts runFonts421 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties12.Append(runFonts421);

            level12.Append(startNumberingValue12);
            level12.Append(numberingFormat12);
            level12.Append(isLegalNumberingStyle10);
            level12.Append(levelText12);
            level12.Append(levelJustification12);
            level12.Append(previousParagraphProperties12);
            level12.Append(numberingSymbolRunProperties12);

            Level level13 = new Level() { LevelIndex = 3 };
            StartNumberingValue startNumberingValue13 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat13 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle11 = new IsLegalNumberingStyle();
            LevelText levelText13 = new LevelText() { Val = "%1.%2.%3.%4." };
            LevelJustification levelJustification13 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties13 = new PreviousParagraphProperties();
            Indentation indentation47 = new Indentation() { Start = "1080", Hanging = "720" };

            previousParagraphProperties13.Append(indentation47);

            NumberingSymbolRunProperties numberingSymbolRunProperties13 = new NumberingSymbolRunProperties();
            RunFonts runFonts422 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties13.Append(runFonts422);

            level13.Append(startNumberingValue13);
            level13.Append(numberingFormat13);
            level13.Append(isLegalNumberingStyle11);
            level13.Append(levelText13);
            level13.Append(levelJustification13);
            level13.Append(previousParagraphProperties13);
            level13.Append(numberingSymbolRunProperties13);

            Level level14 = new Level() { LevelIndex = 4 };
            StartNumberingValue startNumberingValue14 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat14 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle12 = new IsLegalNumberingStyle();
            LevelText levelText14 = new LevelText() { Val = "%1.%2.%3.%4.%5." };
            LevelJustification levelJustification14 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties14 = new PreviousParagraphProperties();
            Indentation indentation48 = new Indentation() { Start = "1440", Hanging = "1080" };

            previousParagraphProperties14.Append(indentation48);

            NumberingSymbolRunProperties numberingSymbolRunProperties14 = new NumberingSymbolRunProperties();
            RunFonts runFonts423 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties14.Append(runFonts423);

            level14.Append(startNumberingValue14);
            level14.Append(numberingFormat14);
            level14.Append(isLegalNumberingStyle12);
            level14.Append(levelText14);
            level14.Append(levelJustification14);
            level14.Append(previousParagraphProperties14);
            level14.Append(numberingSymbolRunProperties14);

            Level level15 = new Level() { LevelIndex = 5 };
            StartNumberingValue startNumberingValue15 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat15 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle13 = new IsLegalNumberingStyle();
            LevelText levelText15 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6." };
            LevelJustification levelJustification15 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties15 = new PreviousParagraphProperties();
            Indentation indentation49 = new Indentation() { Start = "1440", Hanging = "1080" };

            previousParagraphProperties15.Append(indentation49);

            NumberingSymbolRunProperties numberingSymbolRunProperties15 = new NumberingSymbolRunProperties();
            RunFonts runFonts424 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties15.Append(runFonts424);

            level15.Append(startNumberingValue15);
            level15.Append(numberingFormat15);
            level15.Append(isLegalNumberingStyle13);
            level15.Append(levelText15);
            level15.Append(levelJustification15);
            level15.Append(previousParagraphProperties15);
            level15.Append(numberingSymbolRunProperties15);

            Level level16 = new Level() { LevelIndex = 6 };
            StartNumberingValue startNumberingValue16 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat16 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle14 = new IsLegalNumberingStyle();
            LevelText levelText16 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7." };
            LevelJustification levelJustification16 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties16 = new PreviousParagraphProperties();
            Indentation indentation50 = new Indentation() { Start = "1800", Hanging = "1440" };

            previousParagraphProperties16.Append(indentation50);

            NumberingSymbolRunProperties numberingSymbolRunProperties16 = new NumberingSymbolRunProperties();
            RunFonts runFonts425 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties16.Append(runFonts425);

            level16.Append(startNumberingValue16);
            level16.Append(numberingFormat16);
            level16.Append(isLegalNumberingStyle14);
            level16.Append(levelText16);
            level16.Append(levelJustification16);
            level16.Append(previousParagraphProperties16);
            level16.Append(numberingSymbolRunProperties16);

            Level level17 = new Level() { LevelIndex = 7 };
            StartNumberingValue startNumberingValue17 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat17 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle15 = new IsLegalNumberingStyle();
            LevelText levelText17 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8." };
            LevelJustification levelJustification17 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties17 = new PreviousParagraphProperties();
            Indentation indentation51 = new Indentation() { Start = "1800", Hanging = "1440" };

            previousParagraphProperties17.Append(indentation51);

            NumberingSymbolRunProperties numberingSymbolRunProperties17 = new NumberingSymbolRunProperties();
            RunFonts runFonts426 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties17.Append(runFonts426);

            level17.Append(startNumberingValue17);
            level17.Append(numberingFormat17);
            level17.Append(isLegalNumberingStyle15);
            level17.Append(levelText17);
            level17.Append(levelJustification17);
            level17.Append(previousParagraphProperties17);
            level17.Append(numberingSymbolRunProperties17);

            Level level18 = new Level() { LevelIndex = 8 };
            StartNumberingValue startNumberingValue18 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat18 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle16 = new IsLegalNumberingStyle();
            LevelText levelText18 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8.%9." };
            LevelJustification levelJustification18 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties18 = new PreviousParagraphProperties();
            Indentation indentation52 = new Indentation() { Start = "2160", Hanging = "1800" };

            previousParagraphProperties18.Append(indentation52);

            NumberingSymbolRunProperties numberingSymbolRunProperties18 = new NumberingSymbolRunProperties();
            RunFonts runFonts427 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties18.Append(runFonts427);

            level18.Append(startNumberingValue18);
            level18.Append(numberingFormat18);
            level18.Append(isLegalNumberingStyle16);
            level18.Append(levelText18);
            level18.Append(levelJustification18);
            level18.Append(previousParagraphProperties18);
            level18.Append(numberingSymbolRunProperties18);

            abstractNum2.Append(nsid2);
            abstractNum2.Append(multiLevelType2);
            abstractNum2.Append(templateCode2);
            abstractNum2.Append(level10);
            abstractNum2.Append(level11);
            abstractNum2.Append(level12);
            abstractNum2.Append(level13);
            abstractNum2.Append(level14);
            abstractNum2.Append(level15);
            abstractNum2.Append(level16);
            abstractNum2.Append(level17);
            abstractNum2.Append(level18);

            AbstractNum abstractNum3 = new AbstractNum() { AbstractNumberId = 2 };
            Nsid nsid3 = new Nsid() { Val = "2441194E" };
            MultiLevelType multiLevelType3 = new MultiLevelType() { Val = MultiLevelValues.Multilevel };
            TemplateCode templateCode3 = new TemplateCode() { Val = "739478CC" };

            Level level19 = new Level() { LevelIndex = 0 };
            StartNumberingValue startNumberingValue19 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat19 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            LevelText levelText19 = new LevelText() { Val = "%1." };
            LevelJustification levelJustification19 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties19 = new PreviousParagraphProperties();
            Indentation indentation53 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties19.Append(indentation53);

            NumberingSymbolRunProperties numberingSymbolRunProperties19 = new NumberingSymbolRunProperties();
            RunFonts runFonts428 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties19.Append(runFonts428);

            level19.Append(startNumberingValue19);
            level19.Append(numberingFormat19);
            level19.Append(levelText19);
            level19.Append(levelJustification19);
            level19.Append(previousParagraphProperties19);
            level19.Append(numberingSymbolRunProperties19);

            Level level20 = new Level() { LevelIndex = 1 };
            StartNumberingValue startNumberingValue20 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat20 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle17 = new IsLegalNumberingStyle();
            LevelText levelText20 = new LevelText() { Val = "%1.%2." };
            LevelJustification levelJustification20 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties20 = new PreviousParagraphProperties();
            Indentation indentation54 = new Indentation() { Start = "1260", Hanging = "720" };

            previousParagraphProperties20.Append(indentation54);

            NumberingSymbolRunProperties numberingSymbolRunProperties20 = new NumberingSymbolRunProperties();
            RunFonts runFonts429 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties20.Append(runFonts429);

            level20.Append(startNumberingValue20);
            level20.Append(numberingFormat20);
            level20.Append(isLegalNumberingStyle17);
            level20.Append(levelText20);
            level20.Append(levelJustification20);
            level20.Append(previousParagraphProperties20);
            level20.Append(numberingSymbolRunProperties20);

            Level level21 = new Level() { LevelIndex = 2 };
            StartNumberingValue startNumberingValue21 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat21 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle18 = new IsLegalNumberingStyle();
            LevelText levelText21 = new LevelText() { Val = "%1.%2.%3." };
            LevelJustification levelJustification21 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties21 = new PreviousParagraphProperties();
            Indentation indentation55 = new Indentation() { Start = "1800", Hanging = "720" };

            previousParagraphProperties21.Append(indentation55);

            NumberingSymbolRunProperties numberingSymbolRunProperties21 = new NumberingSymbolRunProperties();
            RunFonts runFonts430 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties21.Append(runFonts430);

            level21.Append(startNumberingValue21);
            level21.Append(numberingFormat21);
            level21.Append(isLegalNumberingStyle18);
            level21.Append(levelText21);
            level21.Append(levelJustification21);
            level21.Append(previousParagraphProperties21);
            level21.Append(numberingSymbolRunProperties21);

            Level level22 = new Level() { LevelIndex = 3 };
            StartNumberingValue startNumberingValue22 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat22 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle19 = new IsLegalNumberingStyle();
            LevelText levelText22 = new LevelText() { Val = "%1.%2.%3.%4." };
            LevelJustification levelJustification22 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties22 = new PreviousParagraphProperties();
            Indentation indentation56 = new Indentation() { Start = "2520", Hanging = "1080" };

            previousParagraphProperties22.Append(indentation56);

            NumberingSymbolRunProperties numberingSymbolRunProperties22 = new NumberingSymbolRunProperties();
            RunFonts runFonts431 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties22.Append(runFonts431);

            level22.Append(startNumberingValue22);
            level22.Append(numberingFormat22);
            level22.Append(isLegalNumberingStyle19);
            level22.Append(levelText22);
            level22.Append(levelJustification22);
            level22.Append(previousParagraphProperties22);
            level22.Append(numberingSymbolRunProperties22);

            Level level23 = new Level() { LevelIndex = 4 };
            StartNumberingValue startNumberingValue23 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat23 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle20 = new IsLegalNumberingStyle();
            LevelText levelText23 = new LevelText() { Val = "%1.%2.%3.%4.%5." };
            LevelJustification levelJustification23 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties23 = new PreviousParagraphProperties();
            Indentation indentation57 = new Indentation() { Start = "2880", Hanging = "1080" };

            previousParagraphProperties23.Append(indentation57);

            NumberingSymbolRunProperties numberingSymbolRunProperties23 = new NumberingSymbolRunProperties();
            RunFonts runFonts432 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties23.Append(runFonts432);

            level23.Append(startNumberingValue23);
            level23.Append(numberingFormat23);
            level23.Append(isLegalNumberingStyle20);
            level23.Append(levelText23);
            level23.Append(levelJustification23);
            level23.Append(previousParagraphProperties23);
            level23.Append(numberingSymbolRunProperties23);

            Level level24 = new Level() { LevelIndex = 5 };
            StartNumberingValue startNumberingValue24 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat24 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle21 = new IsLegalNumberingStyle();
            LevelText levelText24 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6." };
            LevelJustification levelJustification24 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties24 = new PreviousParagraphProperties();
            Indentation indentation58 = new Indentation() { Start = "3600", Hanging = "1440" };

            previousParagraphProperties24.Append(indentation58);

            NumberingSymbolRunProperties numberingSymbolRunProperties24 = new NumberingSymbolRunProperties();
            RunFonts runFonts433 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties24.Append(runFonts433);

            level24.Append(startNumberingValue24);
            level24.Append(numberingFormat24);
            level24.Append(isLegalNumberingStyle21);
            level24.Append(levelText24);
            level24.Append(levelJustification24);
            level24.Append(previousParagraphProperties24);
            level24.Append(numberingSymbolRunProperties24);

            Level level25 = new Level() { LevelIndex = 6 };
            StartNumberingValue startNumberingValue25 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat25 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle22 = new IsLegalNumberingStyle();
            LevelText levelText25 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7." };
            LevelJustification levelJustification25 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties25 = new PreviousParagraphProperties();
            Indentation indentation59 = new Indentation() { Start = "3960", Hanging = "1440" };

            previousParagraphProperties25.Append(indentation59);

            NumberingSymbolRunProperties numberingSymbolRunProperties25 = new NumberingSymbolRunProperties();
            RunFonts runFonts434 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties25.Append(runFonts434);

            level25.Append(startNumberingValue25);
            level25.Append(numberingFormat25);
            level25.Append(isLegalNumberingStyle22);
            level25.Append(levelText25);
            level25.Append(levelJustification25);
            level25.Append(previousParagraphProperties25);
            level25.Append(numberingSymbolRunProperties25);

            Level level26 = new Level() { LevelIndex = 7 };
            StartNumberingValue startNumberingValue26 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat26 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle23 = new IsLegalNumberingStyle();
            LevelText levelText26 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8." };
            LevelJustification levelJustification26 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties26 = new PreviousParagraphProperties();
            Indentation indentation60 = new Indentation() { Start = "4680", Hanging = "1800" };

            previousParagraphProperties26.Append(indentation60);

            NumberingSymbolRunProperties numberingSymbolRunProperties26 = new NumberingSymbolRunProperties();
            RunFonts runFonts435 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties26.Append(runFonts435);

            level26.Append(startNumberingValue26);
            level26.Append(numberingFormat26);
            level26.Append(isLegalNumberingStyle23);
            level26.Append(levelText26);
            level26.Append(levelJustification26);
            level26.Append(previousParagraphProperties26);
            level26.Append(numberingSymbolRunProperties26);

            Level level27 = new Level() { LevelIndex = 8 };
            StartNumberingValue startNumberingValue27 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat27 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle24 = new IsLegalNumberingStyle();
            LevelText levelText27 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8.%9." };
            LevelJustification levelJustification27 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties27 = new PreviousParagraphProperties();
            Indentation indentation61 = new Indentation() { Start = "5040", Hanging = "1800" };

            previousParagraphProperties27.Append(indentation61);

            NumberingSymbolRunProperties numberingSymbolRunProperties27 = new NumberingSymbolRunProperties();
            RunFonts runFonts436 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties27.Append(runFonts436);

            level27.Append(startNumberingValue27);
            level27.Append(numberingFormat27);
            level27.Append(isLegalNumberingStyle24);
            level27.Append(levelText27);
            level27.Append(levelJustification27);
            level27.Append(previousParagraphProperties27);
            level27.Append(numberingSymbolRunProperties27);

            abstractNum3.Append(nsid3);
            abstractNum3.Append(multiLevelType3);
            abstractNum3.Append(templateCode3);
            abstractNum3.Append(level19);
            abstractNum3.Append(level20);
            abstractNum3.Append(level21);
            abstractNum3.Append(level22);
            abstractNum3.Append(level23);
            abstractNum3.Append(level24);
            abstractNum3.Append(level25);
            abstractNum3.Append(level26);
            abstractNum3.Append(level27);

            AbstractNum abstractNum4 = new AbstractNum() { AbstractNumberId = 3 };
            Nsid nsid4 = new Nsid() { Val = "30710B25" };
            MultiLevelType multiLevelType4 = new MultiLevelType() { Val = MultiLevelValues.Multilevel };
            TemplateCode templateCode4 = new TemplateCode() { Val = "3CA25E52" };

            Level level28 = new Level() { LevelIndex = 0 };
            StartNumberingValue startNumberingValue28 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat28 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            LevelText levelText28 = new LevelText() { Val = "%1." };
            LevelJustification levelJustification28 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties28 = new PreviousParagraphProperties();
            Indentation indentation62 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties28.Append(indentation62);

            NumberingSymbolRunProperties numberingSymbolRunProperties28 = new NumberingSymbolRunProperties();
            RunFonts runFonts437 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties28.Append(runFonts437);

            level28.Append(startNumberingValue28);
            level28.Append(numberingFormat28);
            level28.Append(levelText28);
            level28.Append(levelJustification28);
            level28.Append(previousParagraphProperties28);
            level28.Append(numberingSymbolRunProperties28);

            Level level29 = new Level() { LevelIndex = 1 };
            StartNumberingValue startNumberingValue29 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat29 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle25 = new IsLegalNumberingStyle();
            LevelText levelText29 = new LevelText() { Val = "%1.%2." };
            LevelJustification levelJustification29 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties29 = new PreviousParagraphProperties();
            Indentation indentation63 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties29.Append(indentation63);

            NumberingSymbolRunProperties numberingSymbolRunProperties29 = new NumberingSymbolRunProperties();
            RunFonts runFonts438 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties29.Append(runFonts438);

            level29.Append(startNumberingValue29);
            level29.Append(numberingFormat29);
            level29.Append(isLegalNumberingStyle25);
            level29.Append(levelText29);
            level29.Append(levelJustification29);
            level29.Append(previousParagraphProperties29);
            level29.Append(numberingSymbolRunProperties29);

            Level level30 = new Level() { LevelIndex = 2 };
            StartNumberingValue startNumberingValue30 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat30 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle26 = new IsLegalNumberingStyle();
            LevelText levelText30 = new LevelText() { Val = "%1.%2.%3." };
            LevelJustification levelJustification30 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties30 = new PreviousParagraphProperties();
            Indentation indentation64 = new Indentation() { Start = "1080", Hanging = "720" };

            previousParagraphProperties30.Append(indentation64);

            NumberingSymbolRunProperties numberingSymbolRunProperties30 = new NumberingSymbolRunProperties();
            RunFonts runFonts439 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties30.Append(runFonts439);

            level30.Append(startNumberingValue30);
            level30.Append(numberingFormat30);
            level30.Append(isLegalNumberingStyle26);
            level30.Append(levelText30);
            level30.Append(levelJustification30);
            level30.Append(previousParagraphProperties30);
            level30.Append(numberingSymbolRunProperties30);

            Level level31 = new Level() { LevelIndex = 3 };
            StartNumberingValue startNumberingValue31 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat31 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle27 = new IsLegalNumberingStyle();
            LevelText levelText31 = new LevelText() { Val = "%1.%2.%3.%4." };
            LevelJustification levelJustification31 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties31 = new PreviousParagraphProperties();
            Indentation indentation65 = new Indentation() { Start = "1080", Hanging = "720" };

            previousParagraphProperties31.Append(indentation65);

            NumberingSymbolRunProperties numberingSymbolRunProperties31 = new NumberingSymbolRunProperties();
            RunFonts runFonts440 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties31.Append(runFonts440);

            level31.Append(startNumberingValue31);
            level31.Append(numberingFormat31);
            level31.Append(isLegalNumberingStyle27);
            level31.Append(levelText31);
            level31.Append(levelJustification31);
            level31.Append(previousParagraphProperties31);
            level31.Append(numberingSymbolRunProperties31);

            Level level32 = new Level() { LevelIndex = 4 };
            StartNumberingValue startNumberingValue32 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat32 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle28 = new IsLegalNumberingStyle();
            LevelText levelText32 = new LevelText() { Val = "%1.%2.%3.%4.%5." };
            LevelJustification levelJustification32 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties32 = new PreviousParagraphProperties();
            Indentation indentation66 = new Indentation() { Start = "1440", Hanging = "1080" };

            previousParagraphProperties32.Append(indentation66);

            NumberingSymbolRunProperties numberingSymbolRunProperties32 = new NumberingSymbolRunProperties();
            RunFonts runFonts441 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties32.Append(runFonts441);

            level32.Append(startNumberingValue32);
            level32.Append(numberingFormat32);
            level32.Append(isLegalNumberingStyle28);
            level32.Append(levelText32);
            level32.Append(levelJustification32);
            level32.Append(previousParagraphProperties32);
            level32.Append(numberingSymbolRunProperties32);

            Level level33 = new Level() { LevelIndex = 5 };
            StartNumberingValue startNumberingValue33 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat33 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle29 = new IsLegalNumberingStyle();
            LevelText levelText33 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6." };
            LevelJustification levelJustification33 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties33 = new PreviousParagraphProperties();
            Indentation indentation67 = new Indentation() { Start = "1440", Hanging = "1080" };

            previousParagraphProperties33.Append(indentation67);

            NumberingSymbolRunProperties numberingSymbolRunProperties33 = new NumberingSymbolRunProperties();
            RunFonts runFonts442 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties33.Append(runFonts442);

            level33.Append(startNumberingValue33);
            level33.Append(numberingFormat33);
            level33.Append(isLegalNumberingStyle29);
            level33.Append(levelText33);
            level33.Append(levelJustification33);
            level33.Append(previousParagraphProperties33);
            level33.Append(numberingSymbolRunProperties33);

            Level level34 = new Level() { LevelIndex = 6 };
            StartNumberingValue startNumberingValue34 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat34 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle30 = new IsLegalNumberingStyle();
            LevelText levelText34 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7." };
            LevelJustification levelJustification34 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties34 = new PreviousParagraphProperties();
            Indentation indentation68 = new Indentation() { Start = "1800", Hanging = "1440" };

            previousParagraphProperties34.Append(indentation68);

            NumberingSymbolRunProperties numberingSymbolRunProperties34 = new NumberingSymbolRunProperties();
            RunFonts runFonts443 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties34.Append(runFonts443);

            level34.Append(startNumberingValue34);
            level34.Append(numberingFormat34);
            level34.Append(isLegalNumberingStyle30);
            level34.Append(levelText34);
            level34.Append(levelJustification34);
            level34.Append(previousParagraphProperties34);
            level34.Append(numberingSymbolRunProperties34);

            Level level35 = new Level() { LevelIndex = 7 };
            StartNumberingValue startNumberingValue35 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat35 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle31 = new IsLegalNumberingStyle();
            LevelText levelText35 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8." };
            LevelJustification levelJustification35 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties35 = new PreviousParagraphProperties();
            Indentation indentation69 = new Indentation() { Start = "1800", Hanging = "1440" };

            previousParagraphProperties35.Append(indentation69);

            NumberingSymbolRunProperties numberingSymbolRunProperties35 = new NumberingSymbolRunProperties();
            RunFonts runFonts444 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties35.Append(runFonts444);

            level35.Append(startNumberingValue35);
            level35.Append(numberingFormat35);
            level35.Append(isLegalNumberingStyle31);
            level35.Append(levelText35);
            level35.Append(levelJustification35);
            level35.Append(previousParagraphProperties35);
            level35.Append(numberingSymbolRunProperties35);

            Level level36 = new Level() { LevelIndex = 8 };
            StartNumberingValue startNumberingValue36 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat36 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle32 = new IsLegalNumberingStyle();
            LevelText levelText36 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8.%9." };
            LevelJustification levelJustification36 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties36 = new PreviousParagraphProperties();
            Indentation indentation70 = new Indentation() { Start = "2160", Hanging = "1800" };

            previousParagraphProperties36.Append(indentation70);

            NumberingSymbolRunProperties numberingSymbolRunProperties36 = new NumberingSymbolRunProperties();
            RunFonts runFonts445 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties36.Append(runFonts445);

            level36.Append(startNumberingValue36);
            level36.Append(numberingFormat36);
            level36.Append(isLegalNumberingStyle32);
            level36.Append(levelText36);
            level36.Append(levelJustification36);
            level36.Append(previousParagraphProperties36);
            level36.Append(numberingSymbolRunProperties36);

            abstractNum4.Append(nsid4);
            abstractNum4.Append(multiLevelType4);
            abstractNum4.Append(templateCode4);
            abstractNum4.Append(level28);
            abstractNum4.Append(level29);
            abstractNum4.Append(level30);
            abstractNum4.Append(level31);
            abstractNum4.Append(level32);
            abstractNum4.Append(level33);
            abstractNum4.Append(level34);
            abstractNum4.Append(level35);
            abstractNum4.Append(level36);

            AbstractNum abstractNum5 = new AbstractNum() { AbstractNumberId = 4 };
            Nsid nsid5 = new Nsid() { Val = "771B122B" };
            MultiLevelType multiLevelType5 = new MultiLevelType() { Val = MultiLevelValues.Multilevel };
            TemplateCode templateCode5 = new TemplateCode() { Val = "3CA25E52" };

            Level level37 = new Level() { LevelIndex = 0 };
            StartNumberingValue startNumberingValue37 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat37 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            LevelText levelText37 = new LevelText() { Val = "%1." };
            LevelJustification levelJustification37 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties37 = new PreviousParagraphProperties();
            Indentation indentation71 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties37.Append(indentation71);

            NumberingSymbolRunProperties numberingSymbolRunProperties37 = new NumberingSymbolRunProperties();
            RunFonts runFonts446 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties37.Append(runFonts446);

            level37.Append(startNumberingValue37);
            level37.Append(numberingFormat37);
            level37.Append(levelText37);
            level37.Append(levelJustification37);
            level37.Append(previousParagraphProperties37);
            level37.Append(numberingSymbolRunProperties37);

            Level level38 = new Level() { LevelIndex = 1 };
            StartNumberingValue startNumberingValue38 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat38 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle33 = new IsLegalNumberingStyle();
            LevelText levelText38 = new LevelText() { Val = "%1.%2." };
            LevelJustification levelJustification38 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties38 = new PreviousParagraphProperties();
            Indentation indentation72 = new Indentation() { Start = "720", Hanging = "360" };

            previousParagraphProperties38.Append(indentation72);

            NumberingSymbolRunProperties numberingSymbolRunProperties38 = new NumberingSymbolRunProperties();
            RunFonts runFonts447 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties38.Append(runFonts447);

            level38.Append(startNumberingValue38);
            level38.Append(numberingFormat38);
            level38.Append(isLegalNumberingStyle33);
            level38.Append(levelText38);
            level38.Append(levelJustification38);
            level38.Append(previousParagraphProperties38);
            level38.Append(numberingSymbolRunProperties38);

            Level level39 = new Level() { LevelIndex = 2 };
            StartNumberingValue startNumberingValue39 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat39 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle34 = new IsLegalNumberingStyle();
            LevelText levelText39 = new LevelText() { Val = "%1.%2.%3." };
            LevelJustification levelJustification39 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties39 = new PreviousParagraphProperties();
            Indentation indentation73 = new Indentation() { Start = "1080", Hanging = "720" };

            previousParagraphProperties39.Append(indentation73);

            NumberingSymbolRunProperties numberingSymbolRunProperties39 = new NumberingSymbolRunProperties();
            RunFonts runFonts448 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties39.Append(runFonts448);

            level39.Append(startNumberingValue39);
            level39.Append(numberingFormat39);
            level39.Append(isLegalNumberingStyle34);
            level39.Append(levelText39);
            level39.Append(levelJustification39);
            level39.Append(previousParagraphProperties39);
            level39.Append(numberingSymbolRunProperties39);

            Level level40 = new Level() { LevelIndex = 3 };
            StartNumberingValue startNumberingValue40 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat40 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle35 = new IsLegalNumberingStyle();
            LevelText levelText40 = new LevelText() { Val = "%1.%2.%3.%4." };
            LevelJustification levelJustification40 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties40 = new PreviousParagraphProperties();
            Indentation indentation74 = new Indentation() { Start = "1080", Hanging = "720" };

            previousParagraphProperties40.Append(indentation74);

            NumberingSymbolRunProperties numberingSymbolRunProperties40 = new NumberingSymbolRunProperties();
            RunFonts runFonts449 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties40.Append(runFonts449);

            level40.Append(startNumberingValue40);
            level40.Append(numberingFormat40);
            level40.Append(isLegalNumberingStyle35);
            level40.Append(levelText40);
            level40.Append(levelJustification40);
            level40.Append(previousParagraphProperties40);
            level40.Append(numberingSymbolRunProperties40);

            Level level41 = new Level() { LevelIndex = 4 };
            StartNumberingValue startNumberingValue41 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat41 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle36 = new IsLegalNumberingStyle();
            LevelText levelText41 = new LevelText() { Val = "%1.%2.%3.%4.%5." };
            LevelJustification levelJustification41 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties41 = new PreviousParagraphProperties();
            Indentation indentation75 = new Indentation() { Start = "1440", Hanging = "1080" };

            previousParagraphProperties41.Append(indentation75);

            NumberingSymbolRunProperties numberingSymbolRunProperties41 = new NumberingSymbolRunProperties();
            RunFonts runFonts450 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties41.Append(runFonts450);

            level41.Append(startNumberingValue41);
            level41.Append(numberingFormat41);
            level41.Append(isLegalNumberingStyle36);
            level41.Append(levelText41);
            level41.Append(levelJustification41);
            level41.Append(previousParagraphProperties41);
            level41.Append(numberingSymbolRunProperties41);

            Level level42 = new Level() { LevelIndex = 5 };
            StartNumberingValue startNumberingValue42 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat42 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle37 = new IsLegalNumberingStyle();
            LevelText levelText42 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6." };
            LevelJustification levelJustification42 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties42 = new PreviousParagraphProperties();
            Indentation indentation76 = new Indentation() { Start = "1440", Hanging = "1080" };

            previousParagraphProperties42.Append(indentation76);

            NumberingSymbolRunProperties numberingSymbolRunProperties42 = new NumberingSymbolRunProperties();
            RunFonts runFonts451 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties42.Append(runFonts451);

            level42.Append(startNumberingValue42);
            level42.Append(numberingFormat42);
            level42.Append(isLegalNumberingStyle37);
            level42.Append(levelText42);
            level42.Append(levelJustification42);
            level42.Append(previousParagraphProperties42);
            level42.Append(numberingSymbolRunProperties42);

            Level level43 = new Level() { LevelIndex = 6 };
            StartNumberingValue startNumberingValue43 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat43 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle38 = new IsLegalNumberingStyle();
            LevelText levelText43 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7." };
            LevelJustification levelJustification43 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties43 = new PreviousParagraphProperties();
            Indentation indentation77 = new Indentation() { Start = "1800", Hanging = "1440" };

            previousParagraphProperties43.Append(indentation77);

            NumberingSymbolRunProperties numberingSymbolRunProperties43 = new NumberingSymbolRunProperties();
            RunFonts runFonts452 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties43.Append(runFonts452);

            level43.Append(startNumberingValue43);
            level43.Append(numberingFormat43);
            level43.Append(isLegalNumberingStyle38);
            level43.Append(levelText43);
            level43.Append(levelJustification43);
            level43.Append(previousParagraphProperties43);
            level43.Append(numberingSymbolRunProperties43);

            Level level44 = new Level() { LevelIndex = 7 };
            StartNumberingValue startNumberingValue44 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat44 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle39 = new IsLegalNumberingStyle();
            LevelText levelText44 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8." };
            LevelJustification levelJustification44 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties44 = new PreviousParagraphProperties();
            Indentation indentation78 = new Indentation() { Start = "1800", Hanging = "1440" };

            previousParagraphProperties44.Append(indentation78);

            NumberingSymbolRunProperties numberingSymbolRunProperties44 = new NumberingSymbolRunProperties();
            RunFonts runFonts453 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties44.Append(runFonts453);

            level44.Append(startNumberingValue44);
            level44.Append(numberingFormat44);
            level44.Append(isLegalNumberingStyle39);
            level44.Append(levelText44);
            level44.Append(levelJustification44);
            level44.Append(previousParagraphProperties44);
            level44.Append(numberingSymbolRunProperties44);

            Level level45 = new Level() { LevelIndex = 8 };
            StartNumberingValue startNumberingValue45 = new StartNumberingValue() { Val = 1 };
            NumberingFormat numberingFormat45 = new NumberingFormat() { Val = NumberFormatValues.Decimal };
            IsLegalNumberingStyle isLegalNumberingStyle40 = new IsLegalNumberingStyle();
            LevelText levelText45 = new LevelText() { Val = "%1.%2.%3.%4.%5.%6.%7.%8.%9." };
            LevelJustification levelJustification45 = new LevelJustification() { Val = LevelJustificationValues.Left };

            PreviousParagraphProperties previousParagraphProperties45 = new PreviousParagraphProperties();
            Indentation indentation79 = new Indentation() { Start = "2160", Hanging = "1800" };

            previousParagraphProperties45.Append(indentation79);

            NumberingSymbolRunProperties numberingSymbolRunProperties45 = new NumberingSymbolRunProperties();
            RunFonts runFonts454 = new RunFonts() { Hint = FontTypeHintValues.Default };

            numberingSymbolRunProperties45.Append(runFonts454);

            level45.Append(startNumberingValue45);
            level45.Append(numberingFormat45);
            level45.Append(isLegalNumberingStyle40);
            level45.Append(levelText45);
            level45.Append(levelJustification45);
            level45.Append(previousParagraphProperties45);
            level45.Append(numberingSymbolRunProperties45);

            abstractNum5.Append(nsid5);
            abstractNum5.Append(multiLevelType5);
            abstractNum5.Append(templateCode5);
            abstractNum5.Append(level37);
            abstractNum5.Append(level38);
            abstractNum5.Append(level39);
            abstractNum5.Append(level40);
            abstractNum5.Append(level41);
            abstractNum5.Append(level42);
            abstractNum5.Append(level43);
            abstractNum5.Append(level44);
            abstractNum5.Append(level45);

            NumberingInstance numberingInstance1 = new NumberingInstance() { NumberID = 1 };
            AbstractNumId abstractNumId1 = new AbstractNumId() { Val = 1 };

            numberingInstance1.Append(abstractNumId1);

            NumberingInstance numberingInstance2 = new NumberingInstance() { NumberID = 2 };
            AbstractNumId abstractNumId2 = new AbstractNumId() { Val = 4 };

            numberingInstance2.Append(abstractNumId2);

            NumberingInstance numberingInstance3 = new NumberingInstance() { NumberID = 3 };
            AbstractNumId abstractNumId3 = new AbstractNumId() { Val = 3 };

            numberingInstance3.Append(abstractNumId3);

            NumberingInstance numberingInstance4 = new NumberingInstance() { NumberID = 4 };
            AbstractNumId abstractNumId4 = new AbstractNumId() { Val = 0 };

            numberingInstance4.Append(abstractNumId4);

            NumberingInstance numberingInstance5 = new NumberingInstance() { NumberID = 5 };
            AbstractNumId abstractNumId5 = new AbstractNumId() { Val = 2 };

            numberingInstance5.Append(abstractNumId5);

            numbering1.Append(abstractNum1);
            numbering1.Append(abstractNum2);
            numbering1.Append(abstractNum3);
            numbering1.Append(abstractNum4);
            numbering1.Append(abstractNum5);
            numbering1.Append(numberingInstance1);
            numbering1.Append(numberingInstance2);
            numbering1.Append(numberingInstance3);
            numbering1.Append(numberingInstance4);
            numbering1.Append(numberingInstance5);

            numberingDefinitionsPart1.Numbering = numbering1;
        }

        // Generates content of customXmlPart1.
        private void GenerateCustomXmlPart1Content(CustomXmlPart customXmlPart1)
        {
            System.Xml.XmlTextWriter writer = new System.Xml.XmlTextWriter(customXmlPart1.GetStream(System.IO.FileMode.Create), System.Text.Encoding.UTF8);
            writer.WriteRaw("<b:Sources SelectedStyle=\"\\APASixthEditionOfficeOnline.xsl\" StyleName=\"APA\" Version=\"6\" xmlns:b=\"http://schemas.openxmlformats.org/officeDocument/2006/bibliography\" xmlns=\"http://schemas.openxmlformats.org/officeDocument/2006/bibliography\"></b:Sources>\r\n");
            writer.Flush();
            writer.Close();
        }

        // Generates content of customXmlPropertiesPart1.
        private void GenerateCustomXmlPropertiesPart1Content(CustomXmlPropertiesPart customXmlPropertiesPart1)
        {
            Ds.DataStoreItem dataStoreItem1 = new Ds.DataStoreItem() { ItemId = "{C4E24C94-28D7-421B-945A-278548D11D24}" };
            dataStoreItem1.AddNamespaceDeclaration("ds", "http://schemas.openxmlformats.org/officeDocument/2006/customXml");

            Ds.SchemaReferences schemaReferences1 = new Ds.SchemaReferences();
            Ds.SchemaReference schemaReference1 = new Ds.SchemaReference() { Uri = "http://schemas.openxmlformats.org/officeDocument/2006/bibliography" };

            schemaReferences1.Append(schemaReference1);

            dataStoreItem1.Append(schemaReferences1);

            customXmlPropertiesPart1.DataStoreItem = dataStoreItem1;
        }

        // Generates content of footnotesPart1.
        private void GenerateFootnotesPart1Content(FootnotesPart footnotesPart1)
        {
            Footnotes footnotes1 = new Footnotes() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15 wp14" } };
            footnotes1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
            footnotes1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            footnotes1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            footnotes1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            footnotes1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            footnotes1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            footnotes1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
            footnotes1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
            footnotes1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            footnotes1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            footnotes1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            footnotes1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");
            footnotes1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
            footnotes1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
            footnotes1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
            footnotes1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

            Footnote footnote1 = new Footnote() { Type = FootnoteEndnoteValues.Separator, Id = -1 };

            Paragraph paragraph144 = new Paragraph() { RsidParagraphAddition = "00931EEF", RsidParagraphProperties = "002F4BF7", RsidRunAdditionDefault = "00931EEF" };

            ParagraphProperties paragraphProperties144 = new ParagraphProperties();
            SpacingBetweenLines spacingBetweenLines10 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            paragraphProperties144.Append(spacingBetweenLines10);

            Run run274 = new Run();
            SeparatorMark separatorMark2 = new SeparatorMark();

            run274.Append(separatorMark2);

            paragraph144.Append(paragraphProperties144);
            paragraph144.Append(run274);

            footnote1.Append(paragraph144);

            Footnote footnote2 = new Footnote() { Type = FootnoteEndnoteValues.ContinuationSeparator, Id = 0 };

            Paragraph paragraph145 = new Paragraph() { RsidParagraphAddition = "00931EEF", RsidParagraphProperties = "002F4BF7", RsidRunAdditionDefault = "00931EEF" };

            ParagraphProperties paragraphProperties145 = new ParagraphProperties();
            SpacingBetweenLines spacingBetweenLines11 = new SpacingBetweenLines() { After = "0", Line = "240", LineRule = LineSpacingRuleValues.Auto };

            paragraphProperties145.Append(spacingBetweenLines11);

            Run run275 = new Run();
            ContinuationSeparatorMark continuationSeparatorMark2 = new ContinuationSeparatorMark();

            run275.Append(continuationSeparatorMark2);

            paragraph145.Append(paragraphProperties145);
            paragraph145.Append(run275);

            footnote2.Append(paragraph145);

            footnotes1.Append(footnote1);
            footnotes1.Append(footnote2);

            footnotesPart1.Footnotes = footnotes1;
        }

        // Generates content of webSettingsPart1.
        private void GenerateWebSettingsPart1Content(WebSettingsPart webSettingsPart1)
        {
            WebSettings webSettings1 = new WebSettings() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15" } };
            webSettings1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            webSettings1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            webSettings1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            webSettings1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            webSettings1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");
            OptimizeForBrowser optimizeForBrowser1 = new OptimizeForBrowser();
            AllowPNG allowPNG1 = new AllowPNG();

            webSettings1.Append(optimizeForBrowser1);
            webSettings1.Append(allowPNG1);

            webSettingsPart1.WebSettings = webSettings1;
        }

        // Generates content of documentSettingsPart1.
        private void GenerateDocumentSettingsPart1Content(DocumentSettingsPart documentSettingsPart1)
        {
            Settings settings1 = new Settings() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 w15" } };
            settings1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
            settings1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
            settings1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
            settings1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
            settings1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
            settings1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
            settings1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            settings1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
            settings1.AddNamespaceDeclaration("w15", "http://schemas.microsoft.com/office/word/2012/wordml");
            settings1.AddNamespaceDeclaration("sl", "http://schemas.openxmlformats.org/schemaLibrary/2006/main");
            Zoom zoom1 = new Zoom() { Percent = "100" };
            HideSpellingErrors hideSpellingErrors1 = new HideSpellingErrors();
            DefaultTabStop defaultTabStop1 = new DefaultTabStop() { Val = 720 };
            CharacterSpacingControl characterSpacingControl1 = new CharacterSpacingControl() { Val = CharacterSpacingValues.DoNotCompress };

            FootnoteDocumentWideProperties footnoteDocumentWideProperties1 = new FootnoteDocumentWideProperties();
            FootnoteSpecialReference footnoteSpecialReference1 = new FootnoteSpecialReference() { Id = -1 };
            FootnoteSpecialReference footnoteSpecialReference2 = new FootnoteSpecialReference() { Id = 0 };

            footnoteDocumentWideProperties1.Append(footnoteSpecialReference1);
            footnoteDocumentWideProperties1.Append(footnoteSpecialReference2);

            EndnoteDocumentWideProperties endnoteDocumentWideProperties1 = new EndnoteDocumentWideProperties();
            EndnoteSpecialReference endnoteSpecialReference1 = new EndnoteSpecialReference() { Id = -1 };
            EndnoteSpecialReference endnoteSpecialReference2 = new EndnoteSpecialReference() { Id = 0 };

            endnoteDocumentWideProperties1.Append(endnoteSpecialReference1);
            endnoteDocumentWideProperties1.Append(endnoteSpecialReference2);

            Compatibility compatibility1 = new Compatibility();
            CompatibilitySetting compatibilitySetting1 = new CompatibilitySetting() { Name = CompatSettingNameValues.CompatibilityMode, Uri = "http://schemas.microsoft.com/office/word", Val = "14" };
            CompatibilitySetting compatibilitySetting2 = new CompatibilitySetting() { Name = CompatSettingNameValues.OverrideTableStyleFontSizeAndJustification, Uri = "http://schemas.microsoft.com/office/word", Val = "1" };
            CompatibilitySetting compatibilitySetting3 = new CompatibilitySetting() { Name = CompatSettingNameValues.EnableOpenTypeFeatures, Uri = "http://schemas.microsoft.com/office/word", Val = "1" };
            CompatibilitySetting compatibilitySetting4 = new CompatibilitySetting() { Name = CompatSettingNameValues.DoNotFlipMirrorIndents, Uri = "http://schemas.microsoft.com/office/word", Val = "1" };

            compatibility1.Append(compatibilitySetting1);
            compatibility1.Append(compatibilitySetting2);
            compatibility1.Append(compatibilitySetting3);
            compatibility1.Append(compatibilitySetting4);

            Rsids rsids1 = new Rsids();
            RsidRoot rsidRoot1 = new RsidRoot() { Val = "00F93210" };
            Rsid rsid7 = new Rsid() { Val = "000267E3" };
            Rsid rsid8 = new Rsid() { Val = "000764D1" };
            Rsid rsid9 = new Rsid() { Val = "00081FDC" };
            Rsid rsid10 = new Rsid() { Val = "00096C19" };
            Rsid rsid11 = new Rsid() { Val = "000A060E" };
            Rsid rsid12 = new Rsid() { Val = "000B426F" };
            Rsid rsid13 = new Rsid() { Val = "000B7150" };
            Rsid rsid14 = new Rsid() { Val = "00141D42" };
            Rsid rsid15 = new Rsid() { Val = "00143880" };
            Rsid rsid16 = new Rsid() { Val = "001519D2" };
            Rsid rsid17 = new Rsid() { Val = "0017091A" };
            Rsid rsid18 = new Rsid() { Val = "001B06F3" };
            Rsid rsid19 = new Rsid() { Val = "001B1AEE" };
            Rsid rsid20 = new Rsid() { Val = "001E253D" };
            Rsid rsid21 = new Rsid() { Val = "002012F9" };
            Rsid rsid22 = new Rsid() { Val = "0020407A" };
            Rsid rsid23 = new Rsid() { Val = "00221352" };
            Rsid rsid24 = new Rsid() { Val = "00224ED1" };
            Rsid rsid25 = new Rsid() { Val = "0024603B" };
            Rsid rsid26 = new Rsid() { Val = "0027457D" };
            Rsid rsid27 = new Rsid() { Val = "0028495D" };
            Rsid rsid28 = new Rsid() { Val = "0028632F" };
            Rsid rsid29 = new Rsid() { Val = "00291323" };
            Rsid rsid30 = new Rsid() { Val = "002A4B84" };
            Rsid rsid31 = new Rsid() { Val = "002C3824" };
            Rsid rsid32 = new Rsid() { Val = "002E125E" };
            Rsid rsid33 = new Rsid() { Val = "002F4BF7" };
            Rsid rsid34 = new Rsid() { Val = "003052B6" };
            Rsid rsid35 = new Rsid() { Val = "003306A1" };
            Rsid rsid36 = new Rsid() { Val = "00343795" };
            Rsid rsid37 = new Rsid() { Val = "00344A29" };
            Rsid rsid38 = new Rsid() { Val = "0035622F" };
            Rsid rsid39 = new Rsid() { Val = "003656B4" };
            Rsid rsid40 = new Rsid() { Val = "00365943" };
            Rsid rsid41 = new Rsid() { Val = "003766EB" };
            Rsid rsid42 = new Rsid() { Val = "00376E76" };
            Rsid rsid43 = new Rsid() { Val = "0039307D" };
            Rsid rsid44 = new Rsid() { Val = "003A379A" };
            Rsid rsid45 = new Rsid() { Val = "003A601A" };
            Rsid rsid46 = new Rsid() { Val = "003B1F24" };
            Rsid rsid47 = new Rsid() { Val = "003B2111" };
            Rsid rsid48 = new Rsid() { Val = "003B3DDA" };
            Rsid rsid49 = new Rsid() { Val = "003C1AAF" };
            Rsid rsid50 = new Rsid() { Val = "003C6204" };
            Rsid rsid51 = new Rsid() { Val = "003F2BB0" };
            Rsid rsid52 = new Rsid() { Val = "003F2E6B" };
            Rsid rsid53 = new Rsid() { Val = "00440272" };
            Rsid rsid54 = new Rsid() { Val = "00447E4A" };
            Rsid rsid55 = new Rsid() { Val = "00454820" };
            Rsid rsid56 = new Rsid() { Val = "004611A1" };
            Rsid rsid57 = new Rsid() { Val = "004707F7" };
            Rsid rsid58 = new Rsid() { Val = "004956ED" };
            Rsid rsid59 = new Rsid() { Val = "004966F3" };
            Rsid rsid60 = new Rsid() { Val = "004A1599" };
            Rsid rsid61 = new Rsid() { Val = "004C5E4D" };
            Rsid rsid62 = new Rsid() { Val = "004F3C99" };
            Rsid rsid63 = new Rsid() { Val = "00510A19" };
            Rsid rsid64 = new Rsid() { Val = "00584CDA" };
            Rsid rsid65 = new Rsid() { Val = "005915FE" };
            Rsid rsid66 = new Rsid() { Val = "005A314B" };
            Rsid rsid67 = new Rsid() { Val = "005A625D" };
            Rsid rsid68 = new Rsid() { Val = "005B2A12" };
            Rsid rsid69 = new Rsid() { Val = "005B3F9C" };
            Rsid rsid70 = new Rsid() { Val = "005D6182" };
            Rsid rsid71 = new Rsid() { Val = "005F3766" };
            Rsid rsid72 = new Rsid() { Val = "005F7012" };
            Rsid rsid73 = new Rsid() { Val = "00614396" };
            Rsid rsid74 = new Rsid() { Val = "00620EEB" };
            Rsid rsid75 = new Rsid() { Val = "0063371E" };
            Rsid rsid76 = new Rsid() { Val = "00646B9F" };
            Rsid rsid77 = new Rsid() { Val = "00673D61" };
            Rsid rsid78 = new Rsid() { Val = "006A2F3C" };
            Rsid rsid79 = new Rsid() { Val = "006A709C" };
            Rsid rsid80 = new Rsid() { Val = "006B07D9" };
            Rsid rsid81 = new Rsid() { Val = "006C1E2F" };
            Rsid rsid82 = new Rsid() { Val = "006D1B42" };
            Rsid rsid83 = new Rsid() { Val = "007041A8" };
            Rsid rsid84 = new Rsid() { Val = "0071224D" };
            Rsid rsid85 = new Rsid() { Val = "00724030" };
            Rsid rsid86 = new Rsid() { Val = "007322D6" };
            Rsid rsid87 = new Rsid() { Val = "0073487D" };
            Rsid rsid88 = new Rsid() { Val = "00740E33" };
            Rsid rsid89 = new Rsid() { Val = "00741E1A" };
            Rsid rsid90 = new Rsid() { Val = "0076231E" };
            Rsid rsid91 = new Rsid() { Val = "00777AA0" };
            Rsid rsid92 = new Rsid() { Val = "00784B9C" };
            Rsid rsid93 = new Rsid() { Val = "00787983" };
            Rsid rsid94 = new Rsid() { Val = "007C7E33" };
            Rsid rsid95 = new Rsid() { Val = "007F201B" };
            Rsid rsid96 = new Rsid() { Val = "008118D6" };
            Rsid rsid97 = new Rsid() { Val = "008200EF" };
            Rsid rsid98 = new Rsid() { Val = "00821A3E" };
            Rsid rsid99 = new Rsid() { Val = "0082671D" };
            Rsid rsid100 = new Rsid() { Val = "00836B02" };
            Rsid rsid101 = new Rsid() { Val = "00865F0A" };
            Rsid rsid102 = new Rsid() { Val = "008A6CC9" };
            Rsid rsid103 = new Rsid() { Val = "008A7DAC" };
            Rsid rsid104 = new Rsid() { Val = "008D76D1" };
            Rsid rsid105 = new Rsid() { Val = "008E4CA0" };
            Rsid rsid106 = new Rsid() { Val = "008E6B7F" };
            Rsid rsid107 = new Rsid() { Val = "00907635" };
            Rsid rsid108 = new Rsid() { Val = "009076F9" };
            Rsid rsid109 = new Rsid() { Val = "009135B7" };
            Rsid rsid110 = new Rsid() { Val = "0092470F" };
            Rsid rsid111 = new Rsid() { Val = "00931EEF" };
            Rsid rsid112 = new Rsid() { Val = "0096055C" };
            Rsid rsid113 = new Rsid() { Val = "009B3F19" };
            Rsid rsid114 = new Rsid() { Val = "009B4977" };
            Rsid rsid115 = new Rsid() { Val = "009C3F38" };
            Rsid rsid116 = new Rsid() { Val = "009F0A63" };
            Rsid rsid117 = new Rsid() { Val = "00A13E70" };
            Rsid rsid118 = new Rsid() { Val = "00A223AC" };
            Rsid rsid119 = new Rsid() { Val = "00A36FC5" };
            Rsid rsid120 = new Rsid() { Val = "00A720F5" };
            Rsid rsid121 = new Rsid() { Val = "00A76263" };
            Rsid rsid122 = new Rsid() { Val = "00A846BA" };
            Rsid rsid123 = new Rsid() { Val = "00AB6C7C" };
            Rsid rsid124 = new Rsid() { Val = "00AB6E43" };
            Rsid rsid125 = new Rsid() { Val = "00AC4486" };
            Rsid rsid126 = new Rsid() { Val = "00AC61EA" };
            Rsid rsid127 = new Rsid() { Val = "00AE0B97" };
            Rsid rsid128 = new Rsid() { Val = "00AE1135" };
            Rsid rsid129 = new Rsid() { Val = "00AE3FBE" };
            Rsid rsid130 = new Rsid() { Val = "00AF310F" };
            Rsid rsid131 = new Rsid() { Val = "00AF7B22" };
            Rsid rsid132 = new Rsid() { Val = "00B4599C" };
            Rsid rsid133 = new Rsid() { Val = "00B53E61" };
            Rsid rsid134 = new Rsid() { Val = "00B6499D" };
            Rsid rsid135 = new Rsid() { Val = "00B65470" };
            Rsid rsid136 = new Rsid() { Val = "00B70886" };
            Rsid rsid137 = new Rsid() { Val = "00B82C1D" };
            Rsid rsid138 = new Rsid() { Val = "00BB36BD" };
            Rsid rsid139 = new Rsid() { Val = "00C01404" };
            Rsid rsid140 = new Rsid() { Val = "00C02174" };
            Rsid rsid141 = new Rsid() { Val = "00C1289B" };
            Rsid rsid142 = new Rsid() { Val = "00C252AE" };
            Rsid rsid143 = new Rsid() { Val = "00C26F1A" };
            Rsid rsid144 = new Rsid() { Val = "00C274BE" };
            Rsid rsid145 = new Rsid() { Val = "00C672A7" };
            Rsid rsid146 = new Rsid() { Val = "00C82AA6" };
            Rsid rsid147 = new Rsid() { Val = "00C85AB0" };
            Rsid rsid148 = new Rsid() { Val = "00C96C94" };
            Rsid rsid149 = new Rsid() { Val = "00C97465" };
            Rsid rsid150 = new Rsid() { Val = "00CB6727" };
            Rsid rsid151 = new Rsid() { Val = "00CD3C3B" };
            Rsid rsid152 = new Rsid() { Val = "00CF635B" };
            Rsid rsid153 = new Rsid() { Val = "00D04D40" };
            Rsid rsid154 = new Rsid() { Val = "00D10BF9" };
            Rsid rsid155 = new Rsid() { Val = "00D859FF" };
            Rsid rsid156 = new Rsid() { Val = "00D86913" };
            Rsid rsid157 = new Rsid() { Val = "00D970BD" };
            Rsid rsid158 = new Rsid() { Val = "00DD1AB9" };
            Rsid rsid159 = new Rsid() { Val = "00DD27E9" };
            Rsid rsid160 = new Rsid() { Val = "00DE0E62" };
            Rsid rsid161 = new Rsid() { Val = "00DE78CA" };
            Rsid rsid162 = new Rsid() { Val = "00DF0F19" };
            Rsid rsid163 = new Rsid() { Val = "00DF151F" };
            Rsid rsid164 = new Rsid() { Val = "00E040C3" };
            Rsid rsid165 = new Rsid() { Val = "00E1168B" };
            Rsid rsid166 = new Rsid() { Val = "00E207B2" };
            Rsid rsid167 = new Rsid() { Val = "00E25FD2" };
            Rsid rsid168 = new Rsid() { Val = "00E42C2F" };
            Rsid rsid169 = new Rsid() { Val = "00E5769A" };
            Rsid rsid170 = new Rsid() { Val = "00E60943" };
            Rsid rsid171 = new Rsid() { Val = "00E81041" };
            Rsid rsid172 = new Rsid() { Val = "00E85F43" };
            Rsid rsid173 = new Rsid() { Val = "00E863EC" };
            Rsid rsid174 = new Rsid() { Val = "00E928C6" };
            Rsid rsid175 = new Rsid() { Val = "00EB29A4" };
            Rsid rsid176 = new Rsid() { Val = "00EB5BE0" };
            Rsid rsid177 = new Rsid() { Val = "00ED1485" };
            Rsid rsid178 = new Rsid() { Val = "00EE55E2" };
            Rsid rsid179 = new Rsid() { Val = "00F03055" };
            Rsid rsid180 = new Rsid() { Val = "00F1332E" };
            Rsid rsid181 = new Rsid() { Val = "00F508B2" };
            Rsid rsid182 = new Rsid() { Val = "00F5457D" };
            Rsid rsid183 = new Rsid() { Val = "00F60449" };
            Rsid rsid184 = new Rsid() { Val = "00F6069A" };
            Rsid rsid185 = new Rsid() { Val = "00F62C39" };
            Rsid rsid186 = new Rsid() { Val = "00F65E2E" };
            Rsid rsid187 = new Rsid() { Val = "00F66B53" };
            Rsid rsid188 = new Rsid() { Val = "00F716BD" };
            Rsid rsid189 = new Rsid() { Val = "00F842A6" };
            Rsid rsid190 = new Rsid() { Val = "00F93210" };
            Rsid rsid191 = new Rsid() { Val = "00FC4A92" };
            Rsid rsid192 = new Rsid() { Val = "00FD42A3" };

            rsids1.Append(rsidRoot1);
            rsids1.Append(rsid7);
            rsids1.Append(rsid8);
            rsids1.Append(rsid9);
            rsids1.Append(rsid10);
            rsids1.Append(rsid11);
            rsids1.Append(rsid12);
            rsids1.Append(rsid13);
            rsids1.Append(rsid14);
            rsids1.Append(rsid15);
            rsids1.Append(rsid16);
            rsids1.Append(rsid17);
            rsids1.Append(rsid18);
            rsids1.Append(rsid19);
            rsids1.Append(rsid20);
            rsids1.Append(rsid21);
            rsids1.Append(rsid22);
            rsids1.Append(rsid23);
            rsids1.Append(rsid24);
            rsids1.Append(rsid25);
            rsids1.Append(rsid26);
            rsids1.Append(rsid27);
            rsids1.Append(rsid28);
            rsids1.Append(rsid29);
            rsids1.Append(rsid30);
            rsids1.Append(rsid31);
            rsids1.Append(rsid32);
            rsids1.Append(rsid33);
            rsids1.Append(rsid34);
            rsids1.Append(rsid35);
            rsids1.Append(rsid36);
            rsids1.Append(rsid37);
            rsids1.Append(rsid38);
            rsids1.Append(rsid39);
            rsids1.Append(rsid40);
            rsids1.Append(rsid41);
            rsids1.Append(rsid42);
            rsids1.Append(rsid43);
            rsids1.Append(rsid44);
            rsids1.Append(rsid45);
            rsids1.Append(rsid46);
            rsids1.Append(rsid47);
            rsids1.Append(rsid48);
            rsids1.Append(rsid49);
            rsids1.Append(rsid50);
            rsids1.Append(rsid51);
            rsids1.Append(rsid52);
            rsids1.Append(rsid53);
            rsids1.Append(rsid54);
            rsids1.Append(rsid55);
            rsids1.Append(rsid56);
            rsids1.Append(rsid57);
            rsids1.Append(rsid58);
            rsids1.Append(rsid59);
            rsids1.Append(rsid60);
            rsids1.Append(rsid61);
            rsids1.Append(rsid62);
            rsids1.Append(rsid63);
            rsids1.Append(rsid64);
            rsids1.Append(rsid65);
            rsids1.Append(rsid66);
            rsids1.Append(rsid67);
            rsids1.Append(rsid68);
            rsids1.Append(rsid69);
            rsids1.Append(rsid70);
            rsids1.Append(rsid71);
            rsids1.Append(rsid72);
            rsids1.Append(rsid73);
            rsids1.Append(rsid74);
            rsids1.Append(rsid75);
            rsids1.Append(rsid76);
            rsids1.Append(rsid77);
            rsids1.Append(rsid78);
            rsids1.Append(rsid79);
            rsids1.Append(rsid80);
            rsids1.Append(rsid81);
            rsids1.Append(rsid82);
            rsids1.Append(rsid83);
            rsids1.Append(rsid84);
            rsids1.Append(rsid85);
            rsids1.Append(rsid86);
            rsids1.Append(rsid87);
            rsids1.Append(rsid88);
            rsids1.Append(rsid89);
            rsids1.Append(rsid90);
            rsids1.Append(rsid91);
            rsids1.Append(rsid92);
            rsids1.Append(rsid93);
            rsids1.Append(rsid94);
            rsids1.Append(rsid95);
            rsids1.Append(rsid96);
            rsids1.Append(rsid97);
            rsids1.Append(rsid98);
            rsids1.Append(rsid99);
            rsids1.Append(rsid100);
            rsids1.Append(rsid101);
            rsids1.Append(rsid102);
            rsids1.Append(rsid103);
            rsids1.Append(rsid104);
            rsids1.Append(rsid105);
            rsids1.Append(rsid106);
            rsids1.Append(rsid107);
            rsids1.Append(rsid108);
            rsids1.Append(rsid109);
            rsids1.Append(rsid110);
            rsids1.Append(rsid111);
            rsids1.Append(rsid112);
            rsids1.Append(rsid113);
            rsids1.Append(rsid114);
            rsids1.Append(rsid115);
            rsids1.Append(rsid116);
            rsids1.Append(rsid117);
            rsids1.Append(rsid118);
            rsids1.Append(rsid119);
            rsids1.Append(rsid120);
            rsids1.Append(rsid121);
            rsids1.Append(rsid122);
            rsids1.Append(rsid123);
            rsids1.Append(rsid124);
            rsids1.Append(rsid125);
            rsids1.Append(rsid126);
            rsids1.Append(rsid127);
            rsids1.Append(rsid128);
            rsids1.Append(rsid129);
            rsids1.Append(rsid130);
            rsids1.Append(rsid131);
            rsids1.Append(rsid132);
            rsids1.Append(rsid133);
            rsids1.Append(rsid134);
            rsids1.Append(rsid135);
            rsids1.Append(rsid136);
            rsids1.Append(rsid137);
            rsids1.Append(rsid138);
            rsids1.Append(rsid139);
            rsids1.Append(rsid140);
            rsids1.Append(rsid141);
            rsids1.Append(rsid142);
            rsids1.Append(rsid143);
            rsids1.Append(rsid144);
            rsids1.Append(rsid145);
            rsids1.Append(rsid146);
            rsids1.Append(rsid147);
            rsids1.Append(rsid148);
            rsids1.Append(rsid149);
            rsids1.Append(rsid150);
            rsids1.Append(rsid151);
            rsids1.Append(rsid152);
            rsids1.Append(rsid153);
            rsids1.Append(rsid154);
            rsids1.Append(rsid155);
            rsids1.Append(rsid156);
            rsids1.Append(rsid157);
            rsids1.Append(rsid158);
            rsids1.Append(rsid159);
            rsids1.Append(rsid160);
            rsids1.Append(rsid161);
            rsids1.Append(rsid162);
            rsids1.Append(rsid163);
            rsids1.Append(rsid164);
            rsids1.Append(rsid165);
            rsids1.Append(rsid166);
            rsids1.Append(rsid167);
            rsids1.Append(rsid168);
            rsids1.Append(rsid169);
            rsids1.Append(rsid170);
            rsids1.Append(rsid171);
            rsids1.Append(rsid172);
            rsids1.Append(rsid173);
            rsids1.Append(rsid174);
            rsids1.Append(rsid175);
            rsids1.Append(rsid176);
            rsids1.Append(rsid177);
            rsids1.Append(rsid178);
            rsids1.Append(rsid179);
            rsids1.Append(rsid180);
            rsids1.Append(rsid181);
            rsids1.Append(rsid182);
            rsids1.Append(rsid183);
            rsids1.Append(rsid184);
            rsids1.Append(rsid185);
            rsids1.Append(rsid186);
            rsids1.Append(rsid187);
            rsids1.Append(rsid188);
            rsids1.Append(rsid189);
            rsids1.Append(rsid190);
            rsids1.Append(rsid191);
            rsids1.Append(rsid192);

            M.MathProperties mathProperties1 = new M.MathProperties();
            M.MathFont mathFont1 = new M.MathFont() { Val = "Cambria Math" };
            M.BreakBinary breakBinary1 = new M.BreakBinary() { Val = M.BreakBinaryOperatorValues.Before };
            M.BreakBinarySubtraction breakBinarySubtraction1 = new M.BreakBinarySubtraction() { Val = M.BreakBinarySubtractionValues.MinusMinus };
            M.SmallFraction smallFraction1 = new M.SmallFraction() { Val = M.BooleanValues.Zero };
            M.DisplayDefaults displayDefaults1 = new M.DisplayDefaults();
            M.LeftMargin leftMargin1 = new M.LeftMargin() { Val = (UInt32Value)0U };
            M.RightMargin rightMargin1 = new M.RightMargin() { Val = (UInt32Value)0U };
            M.DefaultJustification defaultJustification1 = new M.DefaultJustification() { Val = M.JustificationValues.CenterGroup };
            M.WrapIndent wrapIndent1 = new M.WrapIndent() { Val = (UInt32Value)1440U };
            M.IntegralLimitLocation integralLimitLocation1 = new M.IntegralLimitLocation() { Val = M.LimitLocationValues.SubscriptSuperscript };
            M.NaryLimitLocation naryLimitLocation1 = new M.NaryLimitLocation() { Val = M.LimitLocationValues.UnderOver };

            mathProperties1.Append(mathFont1);
            mathProperties1.Append(breakBinary1);
            mathProperties1.Append(breakBinarySubtraction1);
            mathProperties1.Append(smallFraction1);
            mathProperties1.Append(displayDefaults1);
            mathProperties1.Append(leftMargin1);
            mathProperties1.Append(rightMargin1);
            mathProperties1.Append(defaultJustification1);
            mathProperties1.Append(wrapIndent1);
            mathProperties1.Append(integralLimitLocation1);
            mathProperties1.Append(naryLimitLocation1);
            ThemeFontLanguages themeFontLanguages1 = new ThemeFontLanguages() { Val = "en-US" };
            ColorSchemeMapping colorSchemeMapping1 = new ColorSchemeMapping() { Background1 = ColorSchemeIndexValues.Light1, Text1 = ColorSchemeIndexValues.Dark1, Background2 = ColorSchemeIndexValues.Light2, Text2 = ColorSchemeIndexValues.Dark2, Accent1 = ColorSchemeIndexValues.Accent1, Accent2 = ColorSchemeIndexValues.Accent2, Accent3 = ColorSchemeIndexValues.Accent3, Accent4 = ColorSchemeIndexValues.Accent4, Accent5 = ColorSchemeIndexValues.Accent5, Accent6 = ColorSchemeIndexValues.Accent6, Hyperlink = ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = ColorSchemeIndexValues.FollowedHyperlink };

            ShapeDefaults shapeDefaults1 = new ShapeDefaults();
            Ovml.ShapeDefaults shapeDefaults2 = new Ovml.ShapeDefaults() { Extension = V.ExtensionHandlingBehaviorValues.Edit, MaxShapeId = 1026 };

            Ovml.ShapeLayout shapeLayout1 = new Ovml.ShapeLayout() { Extension = V.ExtensionHandlingBehaviorValues.Edit };
            Ovml.ShapeIdMap shapeIdMap1 = new Ovml.ShapeIdMap() { Extension = V.ExtensionHandlingBehaviorValues.Edit, Data = "1" };

            shapeLayout1.Append(shapeIdMap1);

            shapeDefaults1.Append(shapeDefaults2);
            shapeDefaults1.Append(shapeLayout1);
            DecimalSymbol decimalSymbol1 = new DecimalSymbol() { Val = "." };
            ListSeparator listSeparator1 = new ListSeparator() { Val = "," };
            W15.PersistentDocumentId persistentDocumentId1 = new W15.PersistentDocumentId() { Val = "{D334CD9F-6D9A-4E00-BAD7-E3E5FC64CDAE}" };

            settings1.Append(zoom1);
            settings1.Append(hideSpellingErrors1);
            settings1.Append(defaultTabStop1);
            settings1.Append(characterSpacingControl1);
            settings1.Append(footnoteDocumentWideProperties1);
            settings1.Append(endnoteDocumentWideProperties1);
            settings1.Append(compatibility1);
            settings1.Append(rsids1);
            settings1.Append(mathProperties1);
            settings1.Append(themeFontLanguages1);
            settings1.Append(colorSchemeMapping1);
            settings1.Append(shapeDefaults1);
            settings1.Append(decimalSymbol1);
            settings1.Append(listSeparator1);
            settings1.Append(persistentDocumentId1);

            documentSettingsPart1.Settings = settings1;
        }

        // Generates content of themePart1.
        private void GenerateThemePart1Content(ThemePart themePart1)
        {
            A.Theme theme1 = new A.Theme() { Name = "Office Theme" };
            theme1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            A.ThemeElements themeElements1 = new A.ThemeElements();

            A.ColorScheme colorScheme1 = new A.ColorScheme() { Name = "Office" };

            A.Dark1Color dark1Color1 = new A.Dark1Color();
            A.SystemColor systemColor1 = new A.SystemColor() { Val = A.SystemColorValues.WindowText, LastColor = "000000" };

            dark1Color1.Append(systemColor1);

            A.Light1Color light1Color1 = new A.Light1Color();
            A.SystemColor systemColor2 = new A.SystemColor() { Val = A.SystemColorValues.Window, LastColor = "FFFFFF" };

            light1Color1.Append(systemColor2);

            A.Dark2Color dark2Color1 = new A.Dark2Color();
            A.RgbColorModelHex rgbColorModelHex1 = new A.RgbColorModelHex() { Val = "44546A" };

            dark2Color1.Append(rgbColorModelHex1);

            A.Light2Color light2Color1 = new A.Light2Color();
            A.RgbColorModelHex rgbColorModelHex2 = new A.RgbColorModelHex() { Val = "E7E6E6" };

            light2Color1.Append(rgbColorModelHex2);

            A.Accent1Color accent1Color1 = new A.Accent1Color();
            A.RgbColorModelHex rgbColorModelHex3 = new A.RgbColorModelHex() { Val = "5B9BD5" };

            accent1Color1.Append(rgbColorModelHex3);

            A.Accent2Color accent2Color1 = new A.Accent2Color();
            A.RgbColorModelHex rgbColorModelHex4 = new A.RgbColorModelHex() { Val = "ED7D31" };

            accent2Color1.Append(rgbColorModelHex4);

            A.Accent3Color accent3Color1 = new A.Accent3Color();
            A.RgbColorModelHex rgbColorModelHex5 = new A.RgbColorModelHex() { Val = "A5A5A5" };

            accent3Color1.Append(rgbColorModelHex5);

            A.Accent4Color accent4Color1 = new A.Accent4Color();
            A.RgbColorModelHex rgbColorModelHex6 = new A.RgbColorModelHex() { Val = "FFC000" };

            accent4Color1.Append(rgbColorModelHex6);

            A.Accent5Color accent5Color1 = new A.Accent5Color();
            A.RgbColorModelHex rgbColorModelHex7 = new A.RgbColorModelHex() { Val = "4472C4" };

            accent5Color1.Append(rgbColorModelHex7);

            A.Accent6Color accent6Color1 = new A.Accent6Color();
            A.RgbColorModelHex rgbColorModelHex8 = new A.RgbColorModelHex() { Val = "70AD47" };

            accent6Color1.Append(rgbColorModelHex8);

            A.Hyperlink hyperlink1 = new A.Hyperlink();
            A.RgbColorModelHex rgbColorModelHex9 = new A.RgbColorModelHex() { Val = "0563C1" };

            hyperlink1.Append(rgbColorModelHex9);

            A.FollowedHyperlinkColor followedHyperlinkColor1 = new A.FollowedHyperlinkColor();
            A.RgbColorModelHex rgbColorModelHex10 = new A.RgbColorModelHex() { Val = "954F72" };

            followedHyperlinkColor1.Append(rgbColorModelHex10);

            colorScheme1.Append(dark1Color1);
            colorScheme1.Append(light1Color1);
            colorScheme1.Append(dark2Color1);
            colorScheme1.Append(light2Color1);
            colorScheme1.Append(accent1Color1);
            colorScheme1.Append(accent2Color1);
            colorScheme1.Append(accent3Color1);
            colorScheme1.Append(accent4Color1);
            colorScheme1.Append(accent5Color1);
            colorScheme1.Append(accent6Color1);
            colorScheme1.Append(hyperlink1);
            colorScheme1.Append(followedHyperlinkColor1);

            A.FontScheme fontScheme1 = new A.FontScheme() { Name = "Office" };

            A.MajorFont majorFont1 = new A.MajorFont();
            A.LatinFont latinFont1 = new A.LatinFont() { Typeface = "Calibri Light" };
            A.EastAsianFont eastAsianFont1 = new A.EastAsianFont() { Typeface = "" };
            A.ComplexScriptFont complexScriptFont1 = new A.ComplexScriptFont() { Typeface = "" };
            A.SupplementalFont supplementalFont1 = new A.SupplementalFont() { Script = "Jpan", Typeface = "ＭＳ ゴシック" };
            A.SupplementalFont supplementalFont2 = new A.SupplementalFont() { Script = "Hang", Typeface = "맑은 고딕" };
            A.SupplementalFont supplementalFont3 = new A.SupplementalFont() { Script = "Hans", Typeface = "宋体" };
            A.SupplementalFont supplementalFont4 = new A.SupplementalFont() { Script = "Hant", Typeface = "新細明體" };
            A.SupplementalFont supplementalFont5 = new A.SupplementalFont() { Script = "Arab", Typeface = "Times New Roman" };
            A.SupplementalFont supplementalFont6 = new A.SupplementalFont() { Script = "Hebr", Typeface = "Times New Roman" };
            A.SupplementalFont supplementalFont7 = new A.SupplementalFont() { Script = "Thai", Typeface = "Angsana New" };
            A.SupplementalFont supplementalFont8 = new A.SupplementalFont() { Script = "Ethi", Typeface = "Nyala" };
            A.SupplementalFont supplementalFont9 = new A.SupplementalFont() { Script = "Beng", Typeface = "Vrinda" };
            A.SupplementalFont supplementalFont10 = new A.SupplementalFont() { Script = "Gujr", Typeface = "Shruti" };
            A.SupplementalFont supplementalFont11 = new A.SupplementalFont() { Script = "Khmr", Typeface = "MoolBoran" };
            A.SupplementalFont supplementalFont12 = new A.SupplementalFont() { Script = "Knda", Typeface = "Tunga" };
            A.SupplementalFont supplementalFont13 = new A.SupplementalFont() { Script = "Guru", Typeface = "Raavi" };
            A.SupplementalFont supplementalFont14 = new A.SupplementalFont() { Script = "Cans", Typeface = "Euphemia" };
            A.SupplementalFont supplementalFont15 = new A.SupplementalFont() { Script = "Cher", Typeface = "Plantagenet Cherokee" };
            A.SupplementalFont supplementalFont16 = new A.SupplementalFont() { Script = "Yiii", Typeface = "Microsoft Yi Baiti" };
            A.SupplementalFont supplementalFont17 = new A.SupplementalFont() { Script = "Tibt", Typeface = "Microsoft Himalaya" };
            A.SupplementalFont supplementalFont18 = new A.SupplementalFont() { Script = "Thaa", Typeface = "MV Boli" };
            A.SupplementalFont supplementalFont19 = new A.SupplementalFont() { Script = "Deva", Typeface = "Mangal" };
            A.SupplementalFont supplementalFont20 = new A.SupplementalFont() { Script = "Telu", Typeface = "Gautami" };
            A.SupplementalFont supplementalFont21 = new A.SupplementalFont() { Script = "Taml", Typeface = "Latha" };
            A.SupplementalFont supplementalFont22 = new A.SupplementalFont() { Script = "Syrc", Typeface = "Estrangelo Edessa" };
            A.SupplementalFont supplementalFont23 = new A.SupplementalFont() { Script = "Orya", Typeface = "Kalinga" };
            A.SupplementalFont supplementalFont24 = new A.SupplementalFont() { Script = "Mlym", Typeface = "Kartika" };
            A.SupplementalFont supplementalFont25 = new A.SupplementalFont() { Script = "Laoo", Typeface = "DokChampa" };
            A.SupplementalFont supplementalFont26 = new A.SupplementalFont() { Script = "Sinh", Typeface = "Iskoola Pota" };
            A.SupplementalFont supplementalFont27 = new A.SupplementalFont() { Script = "Mong", Typeface = "Mongolian Baiti" };
            A.SupplementalFont supplementalFont28 = new A.SupplementalFont() { Script = "Viet", Typeface = "Times New Roman" };
            A.SupplementalFont supplementalFont29 = new A.SupplementalFont() { Script = "Uigh", Typeface = "Microsoft Uighur" };
            A.SupplementalFont supplementalFont30 = new A.SupplementalFont() { Script = "Geor", Typeface = "Sylfaen" };

            majorFont1.Append(latinFont1);
            majorFont1.Append(eastAsianFont1);
            majorFont1.Append(complexScriptFont1);
            majorFont1.Append(supplementalFont1);
            majorFont1.Append(supplementalFont2);
            majorFont1.Append(supplementalFont3);
            majorFont1.Append(supplementalFont4);
            majorFont1.Append(supplementalFont5);
            majorFont1.Append(supplementalFont6);
            majorFont1.Append(supplementalFont7);
            majorFont1.Append(supplementalFont8);
            majorFont1.Append(supplementalFont9);
            majorFont1.Append(supplementalFont10);
            majorFont1.Append(supplementalFont11);
            majorFont1.Append(supplementalFont12);
            majorFont1.Append(supplementalFont13);
            majorFont1.Append(supplementalFont14);
            majorFont1.Append(supplementalFont15);
            majorFont1.Append(supplementalFont16);
            majorFont1.Append(supplementalFont17);
            majorFont1.Append(supplementalFont18);
            majorFont1.Append(supplementalFont19);
            majorFont1.Append(supplementalFont20);
            majorFont1.Append(supplementalFont21);
            majorFont1.Append(supplementalFont22);
            majorFont1.Append(supplementalFont23);
            majorFont1.Append(supplementalFont24);
            majorFont1.Append(supplementalFont25);
            majorFont1.Append(supplementalFont26);
            majorFont1.Append(supplementalFont27);
            majorFont1.Append(supplementalFont28);
            majorFont1.Append(supplementalFont29);
            majorFont1.Append(supplementalFont30);

            A.MinorFont minorFont1 = new A.MinorFont();
            A.LatinFont latinFont2 = new A.LatinFont() { Typeface = "Calibri" };
            A.EastAsianFont eastAsianFont2 = new A.EastAsianFont() { Typeface = "" };
            A.ComplexScriptFont complexScriptFont2 = new A.ComplexScriptFont() { Typeface = "" };
            A.SupplementalFont supplementalFont31 = new A.SupplementalFont() { Script = "Jpan", Typeface = "ＭＳ 明朝" };
            A.SupplementalFont supplementalFont32 = new A.SupplementalFont() { Script = "Hang", Typeface = "맑은 고딕" };
            A.SupplementalFont supplementalFont33 = new A.SupplementalFont() { Script = "Hans", Typeface = "宋体" };
            A.SupplementalFont supplementalFont34 = new A.SupplementalFont() { Script = "Hant", Typeface = "新細明體" };
            A.SupplementalFont supplementalFont35 = new A.SupplementalFont() { Script = "Arab", Typeface = "Arial" };
            A.SupplementalFont supplementalFont36 = new A.SupplementalFont() { Script = "Hebr", Typeface = "Arial" };
            A.SupplementalFont supplementalFont37 = new A.SupplementalFont() { Script = "Thai", Typeface = "Cordia New" };
            A.SupplementalFont supplementalFont38 = new A.SupplementalFont() { Script = "Ethi", Typeface = "Nyala" };
            A.SupplementalFont supplementalFont39 = new A.SupplementalFont() { Script = "Beng", Typeface = "Vrinda" };
            A.SupplementalFont supplementalFont40 = new A.SupplementalFont() { Script = "Gujr", Typeface = "Shruti" };
            A.SupplementalFont supplementalFont41 = new A.SupplementalFont() { Script = "Khmr", Typeface = "DaunPenh" };
            A.SupplementalFont supplementalFont42 = new A.SupplementalFont() { Script = "Knda", Typeface = "Tunga" };
            A.SupplementalFont supplementalFont43 = new A.SupplementalFont() { Script = "Guru", Typeface = "Raavi" };
            A.SupplementalFont supplementalFont44 = new A.SupplementalFont() { Script = "Cans", Typeface = "Euphemia" };
            A.SupplementalFont supplementalFont45 = new A.SupplementalFont() { Script = "Cher", Typeface = "Plantagenet Cherokee" };
            A.SupplementalFont supplementalFont46 = new A.SupplementalFont() { Script = "Yiii", Typeface = "Microsoft Yi Baiti" };
            A.SupplementalFont supplementalFont47 = new A.SupplementalFont() { Script = "Tibt", Typeface = "Microsoft Himalaya" };
            A.SupplementalFont supplementalFont48 = new A.SupplementalFont() { Script = "Thaa", Typeface = "MV Boli" };
            A.SupplementalFont supplementalFont49 = new A.SupplementalFont() { Script = "Deva", Typeface = "Mangal" };
            A.SupplementalFont supplementalFont50 = new A.SupplementalFont() { Script = "Telu", Typeface = "Gautami" };
            A.SupplementalFont supplementalFont51 = new A.SupplementalFont() { Script = "Taml", Typeface = "Latha" };
            A.SupplementalFont supplementalFont52 = new A.SupplementalFont() { Script = "Syrc", Typeface = "Estrangelo Edessa" };
            A.SupplementalFont supplementalFont53 = new A.SupplementalFont() { Script = "Orya", Typeface = "Kalinga" };
            A.SupplementalFont supplementalFont54 = new A.SupplementalFont() { Script = "Mlym", Typeface = "Kartika" };
            A.SupplementalFont supplementalFont55 = new A.SupplementalFont() { Script = "Laoo", Typeface = "DokChampa" };
            A.SupplementalFont supplementalFont56 = new A.SupplementalFont() { Script = "Sinh", Typeface = "Iskoola Pota" };
            A.SupplementalFont supplementalFont57 = new A.SupplementalFont() { Script = "Mong", Typeface = "Mongolian Baiti" };
            A.SupplementalFont supplementalFont58 = new A.SupplementalFont() { Script = "Viet", Typeface = "Arial" };
            A.SupplementalFont supplementalFont59 = new A.SupplementalFont() { Script = "Uigh", Typeface = "Microsoft Uighur" };
            A.SupplementalFont supplementalFont60 = new A.SupplementalFont() { Script = "Geor", Typeface = "Sylfaen" };

            minorFont1.Append(latinFont2);
            minorFont1.Append(eastAsianFont2);
            minorFont1.Append(complexScriptFont2);
            minorFont1.Append(supplementalFont31);
            minorFont1.Append(supplementalFont32);
            minorFont1.Append(supplementalFont33);
            minorFont1.Append(supplementalFont34);
            minorFont1.Append(supplementalFont35);
            minorFont1.Append(supplementalFont36);
            minorFont1.Append(supplementalFont37);
            minorFont1.Append(supplementalFont38);
            minorFont1.Append(supplementalFont39);
            minorFont1.Append(supplementalFont40);
            minorFont1.Append(supplementalFont41);
            minorFont1.Append(supplementalFont42);
            minorFont1.Append(supplementalFont43);
            minorFont1.Append(supplementalFont44);
            minorFont1.Append(supplementalFont45);
            minorFont1.Append(supplementalFont46);
            minorFont1.Append(supplementalFont47);
            minorFont1.Append(supplementalFont48);
            minorFont1.Append(supplementalFont49);
            minorFont1.Append(supplementalFont50);
            minorFont1.Append(supplementalFont51);
            minorFont1.Append(supplementalFont52);
            minorFont1.Append(supplementalFont53);
            minorFont1.Append(supplementalFont54);
            minorFont1.Append(supplementalFont55);
            minorFont1.Append(supplementalFont56);
            minorFont1.Append(supplementalFont57);
            minorFont1.Append(supplementalFont58);
            minorFont1.Append(supplementalFont59);
            minorFont1.Append(supplementalFont60);

            fontScheme1.Append(majorFont1);
            fontScheme1.Append(minorFont1);

            A.FormatScheme formatScheme1 = new A.FormatScheme() { Name = "Office" };

            A.FillStyleList fillStyleList1 = new A.FillStyleList();

            A.SolidFill solidFill1 = new A.SolidFill();
            A.SchemeColor schemeColor1 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill1.Append(schemeColor1);

            A.GradientFill gradientFill1 = new A.GradientFill() { RotateWithShape = true };

            A.GradientStopList gradientStopList1 = new A.GradientStopList();

            A.GradientStop gradientStop1 = new A.GradientStop() { Position = 0 };

            A.SchemeColor schemeColor2 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation1 = new A.LuminanceModulation() { Val = 110000 };
            A.SaturationModulation saturationModulation1 = new A.SaturationModulation() { Val = 105000 };
            A.Tint tint1 = new A.Tint() { Val = 67000 };

            schemeColor2.Append(luminanceModulation1);
            schemeColor2.Append(saturationModulation1);
            schemeColor2.Append(tint1);

            gradientStop1.Append(schemeColor2);

            A.GradientStop gradientStop2 = new A.GradientStop() { Position = 50000 };

            A.SchemeColor schemeColor3 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation2 = new A.LuminanceModulation() { Val = 105000 };
            A.SaturationModulation saturationModulation2 = new A.SaturationModulation() { Val = 103000 };
            A.Tint tint2 = new A.Tint() { Val = 73000 };

            schemeColor3.Append(luminanceModulation2);
            schemeColor3.Append(saturationModulation2);
            schemeColor3.Append(tint2);

            gradientStop2.Append(schemeColor3);

            A.GradientStop gradientStop3 = new A.GradientStop() { Position = 100000 };

            A.SchemeColor schemeColor4 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation3 = new A.LuminanceModulation() { Val = 105000 };
            A.SaturationModulation saturationModulation3 = new A.SaturationModulation() { Val = 109000 };
            A.Tint tint3 = new A.Tint() { Val = 81000 };

            schemeColor4.Append(luminanceModulation3);
            schemeColor4.Append(saturationModulation3);
            schemeColor4.Append(tint3);

            gradientStop3.Append(schemeColor4);

            gradientStopList1.Append(gradientStop1);
            gradientStopList1.Append(gradientStop2);
            gradientStopList1.Append(gradientStop3);
            A.LinearGradientFill linearGradientFill1 = new A.LinearGradientFill() { Angle = 5400000, Scaled = false };

            gradientFill1.Append(gradientStopList1);
            gradientFill1.Append(linearGradientFill1);

            A.GradientFill gradientFill2 = new A.GradientFill() { RotateWithShape = true };

            A.GradientStopList gradientStopList2 = new A.GradientStopList();

            A.GradientStop gradientStop4 = new A.GradientStop() { Position = 0 };

            A.SchemeColor schemeColor5 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.SaturationModulation saturationModulation4 = new A.SaturationModulation() { Val = 103000 };
            A.LuminanceModulation luminanceModulation4 = new A.LuminanceModulation() { Val = 102000 };
            A.Tint tint4 = new A.Tint() { Val = 94000 };

            schemeColor5.Append(saturationModulation4);
            schemeColor5.Append(luminanceModulation4);
            schemeColor5.Append(tint4);

            gradientStop4.Append(schemeColor5);

            A.GradientStop gradientStop5 = new A.GradientStop() { Position = 50000 };

            A.SchemeColor schemeColor6 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.SaturationModulation saturationModulation5 = new A.SaturationModulation() { Val = 110000 };
            A.LuminanceModulation luminanceModulation5 = new A.LuminanceModulation() { Val = 100000 };
            A.Shade shade1 = new A.Shade() { Val = 100000 };

            schemeColor6.Append(saturationModulation5);
            schemeColor6.Append(luminanceModulation5);
            schemeColor6.Append(shade1);

            gradientStop5.Append(schemeColor6);

            A.GradientStop gradientStop6 = new A.GradientStop() { Position = 100000 };

            A.SchemeColor schemeColor7 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.LuminanceModulation luminanceModulation6 = new A.LuminanceModulation() { Val = 99000 };
            A.SaturationModulation saturationModulation6 = new A.SaturationModulation() { Val = 120000 };
            A.Shade shade2 = new A.Shade() { Val = 78000 };

            schemeColor7.Append(luminanceModulation6);
            schemeColor7.Append(saturationModulation6);
            schemeColor7.Append(shade2);

            gradientStop6.Append(schemeColor7);

            gradientStopList2.Append(gradientStop4);
            gradientStopList2.Append(gradientStop5);
            gradientStopList2.Append(gradientStop6);
            A.LinearGradientFill linearGradientFill2 = new A.LinearGradientFill() { Angle = 5400000, Scaled = false };

            gradientFill2.Append(gradientStopList2);
            gradientFill2.Append(linearGradientFill2);

            fillStyleList1.Append(solidFill1);
            fillStyleList1.Append(gradientFill1);
            fillStyleList1.Append(gradientFill2);

            A.LineStyleList lineStyleList1 = new A.LineStyleList();

            A.Outline outline1 = new A.Outline() { Width = 6350, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill2 = new A.SolidFill();
            A.SchemeColor schemeColor8 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill2.Append(schemeColor8);
            A.PresetDash presetDash1 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };
            A.Miter miter1 = new A.Miter() { Limit = 800000 };

            outline1.Append(solidFill2);
            outline1.Append(presetDash1);
            outline1.Append(miter1);

            A.Outline outline2 = new A.Outline() { Width = 12700, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill3 = new A.SolidFill();
            A.SchemeColor schemeColor9 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill3.Append(schemeColor9);
            A.PresetDash presetDash2 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };
            A.Miter miter2 = new A.Miter() { Limit = 800000 };

            outline2.Append(solidFill3);
            outline2.Append(presetDash2);
            outline2.Append(miter2);

            A.Outline outline3 = new A.Outline() { Width = 19050, CapType = A.LineCapValues.Flat, CompoundLineType = A.CompoundLineValues.Single, Alignment = A.PenAlignmentValues.Center };

            A.SolidFill solidFill4 = new A.SolidFill();
            A.SchemeColor schemeColor10 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill4.Append(schemeColor10);
            A.PresetDash presetDash3 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };
            A.Miter miter3 = new A.Miter() { Limit = 800000 };

            outline3.Append(solidFill4);
            outline3.Append(presetDash3);
            outline3.Append(miter3);

            lineStyleList1.Append(outline1);
            lineStyleList1.Append(outline2);
            lineStyleList1.Append(outline3);

            A.EffectStyleList effectStyleList1 = new A.EffectStyleList();

            A.EffectStyle effectStyle1 = new A.EffectStyle();
            A.EffectList effectList1 = new A.EffectList();

            effectStyle1.Append(effectList1);

            A.EffectStyle effectStyle2 = new A.EffectStyle();
            A.EffectList effectList2 = new A.EffectList();

            effectStyle2.Append(effectList2);

            A.EffectStyle effectStyle3 = new A.EffectStyle();

            A.EffectList effectList3 = new A.EffectList();

            A.OuterShadow outerShadow1 = new A.OuterShadow() { BlurRadius = 57150L, Distance = 19050L, Direction = 5400000, Alignment = A.RectangleAlignmentValues.Center, RotateWithShape = false };

            A.RgbColorModelHex rgbColorModelHex11 = new A.RgbColorModelHex() { Val = "000000" };
            A.Alpha alpha1 = new A.Alpha() { Val = 63000 };

            rgbColorModelHex11.Append(alpha1);

            outerShadow1.Append(rgbColorModelHex11);

            effectList3.Append(outerShadow1);

            effectStyle3.Append(effectList3);

            effectStyleList1.Append(effectStyle1);
            effectStyleList1.Append(effectStyle2);
            effectStyleList1.Append(effectStyle3);

            A.BackgroundFillStyleList backgroundFillStyleList1 = new A.BackgroundFillStyleList();

            A.SolidFill solidFill5 = new A.SolidFill();
            A.SchemeColor schemeColor11 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };

            solidFill5.Append(schemeColor11);

            A.SolidFill solidFill6 = new A.SolidFill();

            A.SchemeColor schemeColor12 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Tint tint5 = new A.Tint() { Val = 95000 };
            A.SaturationModulation saturationModulation7 = new A.SaturationModulation() { Val = 170000 };

            schemeColor12.Append(tint5);
            schemeColor12.Append(saturationModulation7);

            solidFill6.Append(schemeColor12);

            A.GradientFill gradientFill3 = new A.GradientFill() { RotateWithShape = true };

            A.GradientStopList gradientStopList3 = new A.GradientStopList();

            A.GradientStop gradientStop7 = new A.GradientStop() { Position = 0 };

            A.SchemeColor schemeColor13 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Tint tint6 = new A.Tint() { Val = 93000 };
            A.SaturationModulation saturationModulation8 = new A.SaturationModulation() { Val = 150000 };
            A.Shade shade3 = new A.Shade() { Val = 98000 };
            A.LuminanceModulation luminanceModulation7 = new A.LuminanceModulation() { Val = 102000 };

            schemeColor13.Append(tint6);
            schemeColor13.Append(saturationModulation8);
            schemeColor13.Append(shade3);
            schemeColor13.Append(luminanceModulation7);

            gradientStop7.Append(schemeColor13);

            A.GradientStop gradientStop8 = new A.GradientStop() { Position = 50000 };

            A.SchemeColor schemeColor14 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Tint tint7 = new A.Tint() { Val = 98000 };
            A.SaturationModulation saturationModulation9 = new A.SaturationModulation() { Val = 130000 };
            A.Shade shade4 = new A.Shade() { Val = 90000 };
            A.LuminanceModulation luminanceModulation8 = new A.LuminanceModulation() { Val = 103000 };

            schemeColor14.Append(tint7);
            schemeColor14.Append(saturationModulation9);
            schemeColor14.Append(shade4);
            schemeColor14.Append(luminanceModulation8);

            gradientStop8.Append(schemeColor14);

            A.GradientStop gradientStop9 = new A.GradientStop() { Position = 100000 };

            A.SchemeColor schemeColor15 = new A.SchemeColor() { Val = A.SchemeColorValues.PhColor };
            A.Shade shade5 = new A.Shade() { Val = 63000 };
            A.SaturationModulation saturationModulation10 = new A.SaturationModulation() { Val = 120000 };

            schemeColor15.Append(shade5);
            schemeColor15.Append(saturationModulation10);

            gradientStop9.Append(schemeColor15);

            gradientStopList3.Append(gradientStop7);
            gradientStopList3.Append(gradientStop8);
            gradientStopList3.Append(gradientStop9);
            A.LinearGradientFill linearGradientFill3 = new A.LinearGradientFill() { Angle = 5400000, Scaled = false };

            gradientFill3.Append(gradientStopList3);
            gradientFill3.Append(linearGradientFill3);

            backgroundFillStyleList1.Append(solidFill5);
            backgroundFillStyleList1.Append(solidFill6);
            backgroundFillStyleList1.Append(gradientFill3);

            formatScheme1.Append(fillStyleList1);
            formatScheme1.Append(lineStyleList1);
            formatScheme1.Append(effectStyleList1);
            formatScheme1.Append(backgroundFillStyleList1);

            themeElements1.Append(colorScheme1);
            themeElements1.Append(fontScheme1);
            themeElements1.Append(formatScheme1);
            A.ObjectDefaults objectDefaults1 = new A.ObjectDefaults();
            A.ExtraColorSchemeList extraColorSchemeList1 = new A.ExtraColorSchemeList();

            A.OfficeStyleSheetExtensionList officeStyleSheetExtensionList1 = new A.OfficeStyleSheetExtensionList();

            A.OfficeStyleSheetExtension officeStyleSheetExtension1 = new A.OfficeStyleSheetExtension() { Uri = "{05A4C25C-085E-4340-85A3-A5531E510DB2}" };

            Thm15.ThemeFamily themeFamily1 = new Thm15.ThemeFamily() { Name = "Office Theme", Id = "{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}", Vid = "{4A3C46E8-61CC-4603-A589-7422A47A8E4A}" };
            themeFamily1.AddNamespaceDeclaration("thm15", "http://schemas.microsoft.com/office/thememl/2012/main");

            officeStyleSheetExtension1.Append(themeFamily1);

            officeStyleSheetExtensionList1.Append(officeStyleSheetExtension1);

            theme1.Append(themeElements1);
            theme1.Append(objectDefaults1);
            theme1.Append(extraColorSchemeList1);
            theme1.Append(officeStyleSheetExtensionList1);

            themePart1.Theme = theme1;
        }

        private void SetPackageProperties(OpenXmlPackage document)
        {
            document.PackageProperties.Creator = "Birhane";
            document.PackageProperties.Revision = "3";
            document.PackageProperties.Created = System.Xml.XmlConvert.ToDateTime("2014-11-02T09:36:00Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            document.PackageProperties.Modified = System.Xml.XmlConvert.ToDateTime("2014-11-02T09:40:00Z", System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            document.PackageProperties.LastModifiedBy = "Teweldemedhin Aberra";
        }


    }
}
