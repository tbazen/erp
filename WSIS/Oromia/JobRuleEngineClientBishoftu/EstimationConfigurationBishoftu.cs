﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.UI;
using System.Collections;

namespace INTAPS.WSIS.Job.Bishoftu.REClient
{
    public partial class EstimationConfigurationBishoftu : Form
    {
        AdamaBishoftu.BishoftuEstimationConfiguration config;
        bool changed=false;
        int typeID;
        bool _ignoreEvents = false;
        public EstimationConfigurationBishoftu()
        {
            InitializeComponent();
            foreach (ProgressiveRateEditor pe in new ProgressiveRateEditor[] { ratePrivate, rateComercial, rateOthers })
                pe.initGrid();
            loadDepositGrid();
            buttonOk.Enabled = false;
            typeID = JobManagerClient.getJobTypeByClientHandlerType(typeof(NewLineClientBishoftu));
            config = JobManagerClient.getConfiguration(typeID,typeof(AdamaBishoftu.BishoftuEstimationConfiguration)) as AdamaBishoftu.BishoftuEstimationConfiguration;
            if (config == null)
            {
                config = new AdamaBishoftu.BishoftuEstimationConfiguration();
                onChanged();
            }
            _ignoreEvents = true;
            try
            {
                itemsPipeline.setItems(config.pipelineItems);
                itemsHDP.setItems(config.hdpPipelineItems);
                itemsWaterMeter.setItems(config.waterMeterItems);
                itemNewLineFixed.setItems(config.newLineFixedServiceItems);
                itemMaintenanceFixed.setItems(config.maintenanceFixedServiceItems);
                itemsSurvey.setItems(config.surveyItems);
                ratePrivate.setRate(config.privateServiceCharge);
                rateComercial.setRate(config.comercialServiceCharge);
                rateOthers.setRate(config.othersServiceCharge);
                itemMaterialHandling.SetByID(config.materialHandlingMultiplier);
                itemPrivate.SetByID(config.itemServiceChargePrivate);
                itemComercial.SetByID(config.itemServiceChargeComercial);
                itemOther.SetByID(config.itemServiceChargeOther);

                itemLastMultiplier.SetByID(config.lastMultiplier);
                setDepositData(config);
            }
            finally
            {
                _ignoreEvents = false;
            }
        }

        private void onChanged()
        {
            if (_ignoreEvents)
                return;
            buttonOk.Enabled = true;
            changed = true;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (!changed)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Nothing changed");
                return;
            }
            try
            {
                config.pipelineItems = itemsPipeline.getItems();
                config.hdpPipelineItems = itemsHDP.getItems();
                config.waterMeterItems = itemsWaterMeter.getItems();
                config.newLineFixedServiceItems = itemNewLineFixed.getItems();
                config.maintenanceFixedServiceItems= itemMaintenanceFixed.getItems();
                config.materialHandlingMultiplier = itemMaterialHandling.GetObjectID();
                config.surveyItems = itemsSurvey.getItems();
                config.itemServiceChargePrivate = itemPrivate.GetObjectID();
                config.itemServiceChargeComercial = itemComercial.GetObjectID();
                config.itemServiceChargeOther = itemOther.GetObjectID();
                config.lastMultiplier = itemLastMultiplier.GetObjectID();
                string msg;
                if (!ratePrivate.validateInput(out  config.privateServiceCharge, out msg))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(msg);
                    return;
                }
                if (!rateComercial.validateInput(out  config.comercialServiceCharge, out msg))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(msg);
                    return;
                }
                if (!rateOthers.validateInput(out  config.othersServiceCharge, out msg))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(msg);
                    return;
                }
                getDepositData(config);
                JobManagerClient.saveConfiguration(typeID, config);
                changed = false;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        private void setDepositData(AdamaBishoftu.BishoftuEstimationConfiguration rate)
        {
            foreach (DataGridViewRow row in (IEnumerable)this.gridDeposit.Rows)
            {
                string code = (string)row.Tag;
                for (int i = 0; i < rate.depositMeterType.Length; i++)
                {
                    if (code.Equals(rate.depositMeterType[i]))
                    {
                        row.Cells[1].Value = rate.deposit[i];
                        break;
                    }
                }
            }
        }
        private void getDepositData(AdamaBishoftu.BishoftuEstimationConfiguration rate)
        {
            List<double> listDeposit = new List<double>();
            List<string> listMeterType = new List<string>();
            foreach (DataGridViewRow row in (IEnumerable)this.gridDeposit.Rows)
            {
                if (row.Cells[1].Value is double)
                {
                    listDeposit.Add((double)row.Cells[1].Value);
                    listMeterType.Add((string)row.Tag);
                }
            }
            rate.deposit = listDeposit.ToArray();
            rate.depositMeterType = listMeterType.ToArray();
        }
        private void loadDepositGrid()
        {
            var conf= SubscriberManagmentClient.GetSystemParameter("meterMaterialCategory");
            int systemParameter = Convert.ToInt32(conf);
            this.gridDeposit.Columns[1].ValueType = typeof(double);
            if (systemParameter > 1)
            {
                try
                {
                    BIZNET.iERP.TransactionItems[] descriptionArray = BIZNET.iERP.Client.iERPTransactionClient.GetItemsInCategory(systemParameter);
                    if (descriptionArray.Length == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No meter types found.");
                    }
                    foreach (BIZNET.iERP.TransactionItems description in descriptionArray)
                    {
                        object[] values = new object[2];
                        values[0] = description.Name;
                        this.gridDeposit.Rows[this.gridDeposit.Rows.Add(values)].Tag = description.Code;
                    }
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error loading meter types.", exception);
                }
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (changed)
                e.Cancel = !INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Do you want to close without saving the changes?");
        }
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void itemsPipeline_Changed(object sender, EventArgs e)
        {
            onChanged();
        }

        private void progressiveRateCellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            onChanged();
        }

        private void textCostFactor_TextChanged(object sender, EventArgs e)
        {
            onChanged();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void itemLastMultiplier_ObjectChanged(object sender, EventArgs e)
        {
            onChanged();
        }
    }
}
