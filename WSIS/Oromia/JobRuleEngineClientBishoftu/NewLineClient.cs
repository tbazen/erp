﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.WSIS.Job.Client;

namespace INTAPS.WSIS.Job.Bishoftu.REClient
{
    public class ContractGeneratorParamters
    {
        public string date;
        public string year;
        public string name;
    }
    [JobRuleClientHandlerAttribute(StandardJobTypes.NEW_LINE,"Configure Estimation Parameters",true)]
    public class NewLineClientBishoftu : StandardNewLineClientBase
    {
        public override System.Windows.Forms.Form getConfigurationForm()
        {
            return new EstimationConfigurationBishoftu();
        }
        public override bool processURL(System.Windows.Forms.Form parent, string path, System.Collections.Hashtable query)
        {
            switch (path)
            {
                case "genContract":
                    parent.BeginInvoke(new INTAPS.UI.HTML.ProcessSingleParameter<int>(generateContract),int.Parse((string)query["jobID"]));
                    return true;
            }
            return base.processURL(parent, path, query);
        }
        public void generateContract(int jobID)
        {
            try
            {
                JobData jobData = JobManagerClient.GetJob(jobID);
                GeneratedClass g = new GeneratedClass();
                INTAPS.Ethiopic.EtGrDate etdate = INTAPS.Ethiopic.EtGrDate.ToEth(jobData.statusDate);
                string fn = INTAPS.UI.HTML.Helpers.GetTempName() + ".docx";
                g.CreatePackage(fn, new ContractGeneratorParamters { date = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(etdate.Month, 1) + " " + etdate.Day, name = jobData.newCustomer.name, year = etdate.Year.ToString() });
                System.Diagnostics.Process.Start(fn);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
            }
        }
        public override string buildDataHTML(JobData job)
        {
            string html=base.buildDataHTML(job);
            if (job.status == StandardJobStatus.FINISHED)
                html += string.Format("<a href='genContract?jobID={0}'>Gnerate Contract</a",job.id);
            return html;
        }
    }
}
