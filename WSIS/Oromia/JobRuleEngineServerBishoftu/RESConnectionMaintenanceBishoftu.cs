using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.AdamaBishoftu;
namespace INTAPS.WSIS.Job.Bishoftu.REServer
{
    [JobRuleServerHandler(StandardJobTypes.CONNECTION_MAINTENANCE, "Connection Maitenance")]
    public class RESConnectionMaintenanceBishoftu : RESBaseBishoftu, IJobRuleServerHandler
    {
        public RESConnectionMaintenanceBishoftu(JobManagerBDE bde)
            : base(bde)
        {
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);
            ConntectionMaintenanceData newLine = data as ConntectionMaintenanceData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ConntectionMaintenanceData newLine = data as ConntectionMaintenanceData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            ConntectionMaintenanceData wfData = data as ConntectionMaintenanceData;
            if (wfData == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfData.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                case StandardJobStatus.TECHNICAL_WORK:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify new line data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }

        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                case StandardJobStatus.SURVEY:
                    return new int[] { StandardJobStatus.ANALYSIS, StandardJobStatus.CANCELED };
                case StandardJobStatus.ANALYSIS:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED };
                default:
                    return new int[0];
            }
        }
        bool hasApplicationPaymentItems(int jobID)
        {
            JobBillOfMaterial[] boms = bde.GetJobBOM(jobID);
            if (boms.Length == 0)
                return false;
            JobBOMItem item = boms[0].findItem(bde.SysPars.servicePaymentItem);
            if (item == null)
                return false;
            return true;
        }
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            ConntectionMaintenanceData data;
            JobBillOfMaterial[] boms = null;
            BishoftuEstimationConfiguration config = bde.getConfiguration<BishoftuEstimationConfiguration>(StandardJobTypes.NEW_LINE);
            if (config == null)
                throw new ServerUserMessage("Configure estimation parameters");
            data = checkAndGetData(job);

            switch (nextState)
            {
                case StandardJobStatus.FINALIZATION:
                    checkPayment(job.id);
                    if (data == null)
                        throw new ServerUserMessage("Please enter connection data");
                    if (data.changeMeter)
                    {
                        if (data.meterData == null || string.IsNullOrEmpty(data.meterData.serialNo))
                            throw new ServerUserMessage("Meter data no complete");
                        if(data.meterData.opStatus!=MaterialOperationalStatus.Working)
                            throw new ServerUserMessage("Only working meter is allowed");
                    }
                    CompletedJobDocument completed = new CompletedJobDocument();
                    completed.job = job;
                    completed.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, completed);
                    job.completionDocumentID = completed.AccountDocumentID;
                    //bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                    //bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    break;
                case StandardJobStatus.ANALYSIS:
                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length != 1)
                        throw new ServerUserMessage("Exactly one bill of material is required to send this job to analysis");
                    foreach (JobBOMItem item in boms[0].items)
                        if (item.referenceUnitPrice != -1)
                            throw new ServerUserMessage("It is not allowed to set reference prices at this stage");
                    List<JobBOMItem> items = new List<JobBOMItem>(boms[0].items);
                    foreach (BIZNET.iERP.TransactionItems titem in bde.expandItemsList(config.maintenanceFixedServiceItems))
                    {
                        JobBOMItem ji = new JobBOMItem();
                        ji.itemID = titem.Code;
                        ji.inSourced = true;
                        ji.description = titem.Name;
                        ji.quantity = 1;
                        items.Add(ji);
                    }
                    boms[0].items = items.ToArray();
                    bde.SetBillofMateial(AID, boms[0]);
                    List<JobBOMItem> meters = getMeterItems(boms);

                    if (meters.Count > 0)
                    {
                        if (meters.Count != 1 || meters[0].quantity != 1)
                            throw new ServerUserMessage("Only one meter item should be included in the bill of quantity");
                        if (data.meterData == null)
                            data.meterData = new MeterData();
                        data.meterData.itemCode = meters[0].itemID;
                        data.changeMeter = true;
                        bde.setWorkFlowData(AID, data);
                    }
                    break;
                case StandardJobStatus.SURVEY:
                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length == 1)
                    {
                        List<JobBOMItem> newItems = new List<JobBOMItem>();

                        BIZNET.iERP.TransactionItems[] allowedItems = bde.expandItemsList(config.surveyItems);
                        foreach (JobBOMItem item in boms[0].items)
                        {
                            if (bde.contains(allowedItems, item.itemID))
                            {
                                item.referenceUnitPrice = -1;
                                newItems.Add(item);
                            }
                        }
                        boms[0].items = newItems.ToArray();
                        bde.SetBillofMateial(AID, boms[0]);
                    }
                    break;
                case StandardJobStatus.CONTRACT:
                    base.checkPayment(job.id, out boms);
                    break;
                case StandardJobStatus.TECHNICAL_WORK:
                    base.checkPayment(job.id);
                    if (job.completionDocumentID != -1)
                    {
                        bde.BdeAccounting.DeleteAccountDocument(AID, job.completionDocumentID, false);
                        job.completionDocumentID = -1;
                        //bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                        //bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    }
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                case StandardJobStatus.WORK_APPROVAL:
                    bde.verifyAccountingClearance(AID, job.customerID, date, job);
                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        if (bom.invoiceNo < 1)
                            bde.generateInvoice(AID, bom.id, worker);
                        foreach (JobBOMItem item in bom.items)
                            if (!item.inSourced && item.referenceUnitPrice == -1)
                                throw new ServerUserMessage("Customer reference price are not entered");
                    }
                    break;


            }
        }

        ConntectionMaintenanceData checkAndGetData(JobData job)
        {
            ConntectionMaintenanceData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as ConntectionMaintenanceData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            ConntectionMaintenanceData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    ReadingEntryClerk clerk = bde.subscriberBDE.BWFGetClerkByUserID(worker.userID);
                    if (data.changeMeter)
                    {
                        BillPeriod period = bde.subscriberBDE.getPeriodForDate(date);
                        if (period == null)
                            throw new ServerUserMessage("Bill period not defined for time " + date);
                        ReadingReset reset = new ReadingReset();
                        reset.connectionID = data.connectionID;
                        reset.periodID = period.id;
                        reset.reading = data.meterData.initialMeterReading;
                        reset.ticks = date.Ticks;
                        bde.subscriberBDE.setReadingReset(AID, clerk, reset);

                        bde.subscriberBDE.ChangeSubcriberMeter(AID, data.connectionID, date, data.meterData);
                    }
                    
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }


    }
}
