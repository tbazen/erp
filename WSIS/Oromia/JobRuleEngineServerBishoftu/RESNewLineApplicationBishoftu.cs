﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.AdamaBishoftu;

namespace INTAPS.WSIS.Job.Bishoftu.REServer
{
    [JobRuleServerHandler(StandardJobTypes.NEW_LINE, "New Line")]
    public class RESNewLineApplicationBishoftu : RESBaseBishoftu, IJobRuleServerHandler
    {
        public RESNewLineApplicationBishoftu(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            if (AID != -1 && job.applicationType==StandardJobTypes.NEW_LINE)
            {
                lock (bde.WriterHelper)
                {
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        
                        customer.depositAccountID = bde.subscriberBDE.getOrCreateCustomerAccountID(AID, customer, true);
                        JobBillDocument ret = base.generateInvoice(AID, customer, job, billOfMaterial);
                        bde.WriterHelper.CommitTransaction();
                        return ret;
                    }
                    catch
                    {
                        bde.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }
            }

            return base.generateInvoice(AID, customer, job, billOfMaterial);
        }
        public void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);

            NewLineData newLine = data as NewLineData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            NewLineData newLine = data as NewLineData;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            NewLineData wfData = data as NewLineData;
            if (wfData == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfData.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                case StandardJobStatus.CONTRACT:
                case StandardJobStatus.ANALYSIS:
                case StandardJobStatus.FINALIZATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify new line data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }

        public int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job)
        {
            List<int> list = new List<int>();
            JobStatusHistory[] history = bde.GetJobHistory(job.id);
            int prevStatus = history[history.Length - 1].oldStatus;
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                    return new int[] { StandardJobStatus.APPLICATION_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.APPLICATION_APPROVAL:
                    return new int[] { StandardJobStatus.SURVEY, StandardJobStatus.CANCELED };
                case StandardJobStatus.SURVEY:
                    return new int[] { StandardJobStatus.ANALYSIS, StandardJobStatus.CANCELED };
                case StandardJobStatus.ANALYSIS:
                    return new int[] { StandardJobStatus.WORK_APPROVAL, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_APPROVAL:
                    return new int[] { StandardJobStatus.WORK_PAYMENT, StandardJobStatus.CANCELED };
                case StandardJobStatus.WORK_PAYMENT:
                    return new int[] { StandardJobStatus.CONTRACT, StandardJobStatus.CANCELED };
                case StandardJobStatus.CONTRACT:
                    return new int[] { StandardJobStatus.TECHNICAL_WORK, StandardJobStatus.CANCELED };
                case StandardJobStatus.TECHNICAL_WORK:
                    return new int[] { StandardJobStatus.FINALIZATION, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINALIZATION:
                    return new int[] { StandardJobStatus.FINISHED, StandardJobStatus.CANCELED };
                case StandardJobStatus.FINISHED:
                    return new int[] { };
                default:
                    return new int[0];
            }
        }

        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION_APPROVAL:
                    if (nextState != StandardJobStatus.CANCELED)
                    {
                        if (job.newCustomer != null)
                            bde.registerNewCustomer(AID, job, date);
                    }
                    break;
            }
            NewLineData data;
            JobBillOfMaterial[] boms = null;
            BishoftuEstimationConfiguration config = bde.getConfiguration<BishoftuEstimationConfiguration>(StandardJobTypes.NEW_LINE);
            if (config == null)
                throw new ServerUserMessage("Configure estimation parameters");
            switch (nextState)
            {
                case StandardJobStatus.FINALIZATION:
                    checkPayment(job.id);
                    data = checkAndGetData(job);
                    if (data == null)
                        throw new ServerUserMessage("Please enter connection data");
                    if (string.IsNullOrEmpty(data.connectionData.serialNo) || string.IsNullOrEmpty(data.connectionData.modelNo))
                        throw new ServerUserMessage("Meter data not complete");
                    CompletedJobDocument completed = new CompletedJobDocument();
                    completed.job = job;
                    completed.AccountDocumentID = bde.BdeAccounting.PostGenericDocument(AID, completed);
                    job.completionDocumentID = completed.AccountDocumentID;
                    bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                    bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    break;
                case StandardJobStatus.ANALYSIS:

                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length != 1)
                        throw new ServerUserMessage("Exactly one bill of material is required to send this job to analysis");
                    foreach (JobBOMItem item in boms[0].items)
                        if (item.referenceUnitPrice != -1)
                            throw new ServerUserMessage("It is not allowed to set reference prices at this stage");
                    List<JobBOMItem> items = new List<JobBOMItem>(boms[0].items);
                    foreach (BIZNET.iERP.TransactionItems titem in bde.expandItemsList(config.newLineFixedServiceItems))
                    {
                        JobBOMItem ji = new JobBOMItem();
                        ji.itemID = titem.Code;
                        ji.inSourced = true;
                        ji.description = titem.Name;
                        ji.quantity = 1;
                        items.Add(ji);
                    }
                    boms[0].items = items.ToArray();
                    bde.SetBillofMateial(AID, boms[0]);
                    List<JobBOMItem> meters = getMeterItems(boms);
                    if (meters.Count != 1 || meters[0].quantity != 1)
                        throw new ServerUserMessage("Exactly one meter item should be include in the bill of quantity");
                    data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as NewLineData;
                    if (data == null)
                    {
                        data = new NewLineData();
                        data.connectionData = new Subscription();
                        data.jobID = job.id;
                        data.typeID = getTypeID();
                    }
                    data.connectionData.itemCode = meters[0].itemID;
                    bde.setWorkFlowData(AID, data);
                    break;
                case StandardJobStatus.SURVEY:
                    boms = bde.GetJobBOM(job.id);
                    if (boms.Length == 1)
                    {
                        List<JobBOMItem> newItems = new List<JobBOMItem>();

                        BIZNET.iERP.TransactionItems[] allowedItems = bde.expandItemsList(config.surveyItems);
                        foreach (JobBOMItem item in boms[0].items)
                        {
                            if (bde.contains(allowedItems, item.itemID))
                            {
                                item.referenceUnitPrice = -1;
                                newItems.Add(item);
                            }
                        }
                        boms[0].items = newItems.ToArray();
                        bde.SetBillofMateial(AID, boms[0]);
                    }
                    break;
                case StandardJobStatus.CONTRACT:
                    base.checkPayment(job.id, out boms);
                    break;
                case StandardJobStatus.TECHNICAL_WORK:

                    if (job.completionDocumentID != -1)
                    {
                        bde.BdeAccounting.DeleteAccountDocument(AID, job.completionDocumentID, false);
                        job.completionDocumentID = -1;
                        bde.WriterHelper.logDeletedData(AID, bde.DBName + ".dbo.JobData", "id=" + job.id);
                        bde.WriterHelper.Update(bde.DBName, "JobData", new string[] { "completionDocumentID", "__AID" }, new object[] { job.completionDocumentID, AID }, "id=" + job.id);
                    }
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                case StandardJobStatus.WORK_APPROVAL:
                    bde.verifyAccountingClearance(AID, job.customerID, date, job);

                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        if (bom.invoiceNo < 1)
                            bde.generateInvoice(AID, bom.id, worker);
                        foreach (JobBOMItem item in bom.items)
                            if (!item.inSourced && item.referenceUnitPrice == -1)
                                throw new ServerUserMessage("Customer reference price are not entered");
                    }
                    break;

            }
        }

        NewLineData checkAndGetData(JobData job)
        {
            NewLineData data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as NewLineData;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            NewLineData data = checkAndGetData(job);
            JobBillOfMaterial[] jobBOM;
            base.checkPayment(job.id, out jobBOM);
            lock (bde.WriterHelper)
            {
                try
                {
                    bde.WriterHelper.BeginTransaction();

                    if (job.customerID < 1)
                    {
                        if (job.newCustomer == null)
                            throw new ServerUserMessage("New customer not set");
                        if (job.newCustomer.id == -1)
                        {
                            job.customerID = job.newCustomer.id = bde.subscriberBDE.RegisterSubscriber(AID, job.newCustomer);
                            job.newCustomer = bde.subscriberBDE.GetSubscriber(job.newCustomer.id);
                            bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, job);
                        }
                    }
                    data.connectionData.subscriptionStatus = SubscriptionStatus.Active;
                    data.connectionData.contractNo = bde.GetNextContractNumber(AID, data.connectionData.Kebele);
                    data.connectionData.subscriberID = job.customerID < 1 ? job.newCustomer.id : job.customerID;
                    data.connectionData.ticksFrom = date.Ticks;
                    data.connectionData.timeBinding = RDBMS.DataTimeBinding.LowerBound;
                    data.connectionData.id = bde.subscriberBDE.AddSubscription(AID, data.connectionData);
                    data.connectionData = bde.subscriberBDE.GetSubscription(data.connectionData.id,data.connectionData.ticksFrom);
                    
                    bde.setWorkFlowData(AID, data);
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

    }
}
