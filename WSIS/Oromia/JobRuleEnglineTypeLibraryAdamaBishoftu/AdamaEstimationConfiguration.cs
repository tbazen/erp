﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.WSIS.Job.AdamaBishoftu
{
    [Serializable]
    public class AdamaEstimationConfiguration
    {
        public ItemsListItem[] pipelineItems = new ItemsListItem[0];
        public ItemsListItem[] hdpPipelineItems = new ItemsListItem[0];
        public ItemsListItem[] waterMeterItems = new ItemsListItem[0];
        public ItemsListItem[] newLineFixedServiceItems = new ItemsListItem[0];
        public ItemsListItem[] maintenanceFixedServiceItems = new ItemsListItem[0];
        public ItemsListItem[] surveyItems= new ItemsListItem[0];

        public string materialHandlingMultiplier =null;
        public INTAPS.UI.ProgressiveRate privateServiceCharge = new INTAPS.UI.ProgressiveRate(0);
        public INTAPS.UI.ProgressiveRate comercialServiceCharge = new INTAPS.UI.ProgressiveRate(0);
        public INTAPS.UI.ProgressiveRate othersServiceCharge = new INTAPS.UI.ProgressiveRate(0);

        public string itemUnmeteredWaterFee = null;
        public string itemServiceChargePrivate = null;
        public string itemServiceChargeComercial = null;
        public string itemServiceChargeOther = null;
        public string lastMultiplier=null;
        public double[] deposit = new double[0];
        public string[] depositMeterType = new string[0];
    }
}