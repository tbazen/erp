﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.SubscriberManagment.Client;
using INTAPS.SubscriberManagment;
using INTAPS.Accounting;
using System.Linq;

namespace MigrateOro
{

    static class Program
    {

        static string[] dbType = new string[] { "01", "02", "03", "04" };
        static SubscriberType[] subscType = new SubscriberType[] { SubscriberType.Private, SubscriberType.GovernmentInstitution, SubscriberType.CommercialInstitution, SubscriberType.Other };


        static string[] meterSizeDescID;// = new string[] { "067003030023", "067003030024", "067003030025", "067003030025", "067003030027", "067003030028" };
        static string[] dbMeterSize;// = new string[] { "1/2", "3/4", "1", @"1""", "11/2", "2" };


        public static BillPeriod getMonthAndYear(int year, int month)
        {
            return SubscriberManagmentClient.getPeriodForDate(INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, month, year)).GridDate);
        }
        public static SubscriberType getSubscType(string dbVal)
        {
            for (int i = 0; i < dbType.Length; i++)
                if (dbType[i].Equals(dbVal))
                    return subscType[i];
            return SubscriberType.Unknown;
        }
        static System.IO.StreamWriter resultFile;
        static void writeResult(string message)
        {
            Console.WriteLine(message);
            resultFile.WriteLine(message);
        }
        static BWFReadingBlock theReadingBlock = null;
        const int baseYear = 1980;
        const int baseMonth = 1;
        static DateTime basePeriodDate = new DateTime(1980, 1, 1);
        static BillPeriod basePeriod = null;
        static int migrationClerkID = -1;
        static int migrationReaderID = -1;
        const int BLOCK_SIZE = 100;
        static int newReadingBlockSize = 0;
        static int updatedReading = 0;
        static int addedReading = 0;
        static int updatedCust= 0;
        static int addedCust = 0;
        static int disconnected = 0;
        static int connected= 0;
        static bool prepareBlockMode;
        
        static BWFReadingBlock currentAssignBlock = null;
        static int nAssigned = 0;
        const int R_PER_BLK = 200;
        static int assignEmployeeID=-1;
        static int assignKebele = -1;
        static int custCodeColumn = -1;
        static void setReading(Subscription subscription, BillPeriod billPeriod, int prevReading, int currentReading)
        {
            int blockID;

            if (prepareBlockMode)
            {
                if (currentAssignBlock == null)
                {
                    int nExising = SubscriberManagmentClient.GetBlocks(billPeriod.id, -1).Length;
                    string blockName = (nExising + 1).ToString("0000");
                    foreach (BWFReadingBlock test in SubscriberManagmentClient.GetBlock(billPeriod.id, blockName))
                    {
                        int n;
                        foreach (BWFMeterReading r in SubscriberManagmentClient.GetReadings(test.id, 0, -1, out n))
                        {
                            SubscriberManagmentClient.BWFDeleteMeterReading(r.subscriptionID, r.periodID);
                        }
                        SubscriberManagmentClient.BWFDeleteReadingBlock(test.id);
                    }
                    currentAssignBlock = new BWFReadingBlock();
                    currentAssignBlock.periodID = billPeriod.id;
                    currentAssignBlock.blockName = blockName;
                    currentAssignBlock.readerID = assignEmployeeID;
                    currentAssignBlock.readingStart = DateTime.Now;
                    currentAssignBlock.readingEnd = DateTime.Now;
                    currentAssignBlock.id = SubscriberManagmentClient.BWFCreateReadingBlock(currentAssignBlock);
                    nAssigned = 0;
                }
                blockID = currentAssignBlock.id;
            }
            else
                blockID = theReadingBlock.id;


            BWFMeterReading exitingReading = null;
            BWFMeterReading reading = new BWFMeterReading();
            reading.readingBlockID = blockID;
            reading.subscriptionID = subscription.id;
            reading.entryDate = DateTime.Now;

            if (prepareBlockMode)
            {
                reading.readingType = MeterReadingType.Normal;
                reading.reading = 0;
                reading.consumption = 0;
                reading.bwfStatus = BWFStatus.Unread;
                reading.orderN = nAssigned + 1;
            }
            else
            {
                exitingReading = SubscriberManagmentClient.BWFGetMeterReadingByPeriodID(subscription.id, billPeriod.id);
                reading.readingType = MeterReadingType.MeterReset;
                reading.reading = currentReading;
                reading.consumption = currentReading - prevReading;
                reading.bwfStatus = BWFStatus.Read;
                if (exitingReading != null)
                    reading.orderN = exitingReading.orderN;
                else
                    reading.orderN = newReadingBlockSize + 1;
            }
            if (exitingReading == null)
            {
                SubscriberManagmentClient.BWFAddMeterReading(reading);
                addedReading++;
                newReadingBlockSize++;
                if (prepareBlockMode)
                {
                    nAssigned++;
                    if (nAssigned == R_PER_BLK)
                    {
                        currentAssignBlock = null;
                    }
                }
            }
            else
            {
                if (reading.reading != 0)
                {
                    if (!exitingReading.readingType.Equals(reading.readingType)
                        || !exitingReading.reading.Equals(reading.reading)
                        || !exitingReading.consumption.Equals(reading.consumption)
                        )
                    {
                        SubscriberManagmentClient.BWFUpdateMeterReading(reading);
                        updatedReading++;
                    }
                }
            }

        }
        static int compareRecords(string[] val1,string []val2)
        {
            return val1[custCodeColumn].Trim().CompareTo(val2[custCodeColumn].Trim());
        }
        public static void Main(string[] args)
        {
            string folder;
            string fileName;
            int year;
            int month;

            string empID = null;
            try
            {
                var meterSizeList = System.Configuration.ConfigurationManager.AppSettings["meter_size"];
                if(String.IsNullOrEmpty(meterSizeList))
                {
                    throw new Exception("Meter size list not configured");
                }
                var items= meterSizeList.Split(new char[] { '|'},StringSplitOptions.RemoveEmptyEntries);
                meterSizeDescID = new string[items.Length];
                dbMeterSize = new string[items.Length];
                for(var k=0;k<items.Length;k++)
                {
                    var item = items[k].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    if (item.Length != 2)
                        throw new Exception("Invalid meter_size entry: " + items[k]);
                    item = item.Select(x => x.Trim()).ToArray();
                    Console.Write($"{item[0]} > {item[1]}");
                    meterSizeDescID[k] = item[1];
                    dbMeterSize[k] = item[0];
                }
                folder = args[0];
                fileName = args[1];
                year = int.Parse(args[2]);
                month = int.Parse(args[3]);
                prepareBlockMode = args[4] == "B";
                int i = 5;
                if (prepareBlockMode)
                {
                    empID = args[5].Trim();
                    assignKebele = int.Parse(args[6]);
                    i = 7;

                }
                string userName = args[i];
                string password = args[i + 1];
                var url = System.Configuration.ConfigurationManager.AppSettings["Server"];
                INTAPS.ClientServer.Client.ApplicationClient.Connect(url, userName, password);
                SubscriberManagmentClient.Connect(url);
                INTAPS.Payroll.Client.PayrollClient.Connect(url);
                if (prepareBlockMode)
                {
                    INTAPS.Payroll.Employee re = INTAPS.Payroll.Client.PayrollClient.GetEmployeeByID(empID);
                    if (re == null)
                        throw new Exception("Invalid reading employee ID " + empID);
                    assignEmployeeID = re.id;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error initializing\n" + ex.Message+"\n"+ex.StackTrace);
                return;
            }
            System.IO.FileInfo fi = new System.IO.FileInfo(folder + "\\" + fileName); ;
            resultFile = System.IO.File.CreateText(folder + ".result\\" + fi.Name.Substring(0, fi.Name.Length - 4) + ".txt");
            try
            {
                BillPeriod prd = null;
                if (month != 0)
                    prd = getMonthAndYear(year, month);

                string fn = folder + "\\" + fileName;
                SocialExplorer.IO.FastDBF.DbfFile odbf = null;

                basePeriodDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, baseMonth, baseYear)).GridDate;

                try
                {
                    odbf = new SocialExplorer.IO.FastDBF.DbfFile();

                    odbf.Open(fn, System.IO.FileMode.Open);

                    SocialExplorer.IO.FastDBF.DbfRecord dbrec = new SocialExplorer.IO.FastDBF.DbfRecord(odbf.Header);

                    int nameColumn = -1;
                    int statusColumn = -1;
                    int kebeleColumn = -1;
                    int hnColumn = -1;
                    int conNoColumn = -1;
                    int meterNoColumn = -1;
                    int monthColumn = -1;
                    int meterSizeColumn = -1;
                    int typeColumn = -1;
                    int prevReadingColumn = -1;
                    int curReadingColumn = -1;
                    custCodeColumn = getColumnIndex(dbrec, "CUCODE");
                    if (custCodeColumn == -1)
                        throw new Exception("Couldn't find CUCODE Column in " + fn);

                    nameColumn = getColumnIndex(dbrec, "NAME");
                    if (nameColumn == -1)
                        throw new Exception("Couldn't find NAME Column in " + fn);

                    statusColumn = getColumnIndex(dbrec, "STAT");
                    kebeleColumn = getColumnIndex(dbrec, "KEB");
                    if (kebeleColumn == -1)
                    {
                        kebeleColumn = getColumnIndex(dbrec, "A");
                        if (kebeleColumn == -1)
                            throw new Exception("Couldn't find KEB Column in " + fn);
                    }
                    conNoColumn = getColumnIndex(dbrec, "CO_NO");

                    meterNoColumn = getColumnIndex(dbrec, "MNO");
                    monthColumn = getColumnIndex(dbrec, "MON");
                    meterSizeColumn = getColumnIndex(dbrec, "MSIZE");
                    typeColumn = getColumnIndex(dbrec, "TYPE");
                    hnColumn = getColumnIndex(dbrec, "H_NO");
                    prevReadingColumn = getColumnIndex(dbrec, "PREV");
                    curReadingColumn = getColumnIndex(dbrec, "LAST");



                    int c = (int)odbf.Header.RecordCount;
                    writeResult("Transfering records from " + fn);
                    Console.WriteLine();
                    int errorcount = 0;
                    int warningcount = 0;
                    int filteredOut = 0;

                    if (!prepareBlockMode && month != 0)
                    {
                        basePeriod = SubscriberManagmentClient.getPeriodForDate(basePeriodDate);
                        if (basePeriod == null)
                            throw new Exception("Configure base period for date " + basePeriodDate);
                        MeterReaderEmployee[] es = SubscriberManagmentClient.GetAllMeterReaderEmployees(basePeriod.id);
                        if (es.Length == 0)
                            throw new Exception("Configure a meter reader for period " + basePeriod.name);
                        migrationReaderID = es[0].employeeID;
                        MeterReaderEmployee[] me = SubscriberManagmentClient.GetAllMeterReaderEmployees(prd.id);
                        if (me.Length == 0)
                        {
                            MeterReaderEmployee thee = new MeterReaderEmployee();
                            thee.employeeID = migrationReaderID;
                            thee.periodID = prd.id;
                            SubscriberManagmentClient.CreateMeterReaderEmployee(thee);
                        }

                        BWFReadingBlock[] blocks = SubscriberManagmentClient.GetBlock(prd.id, "IMPORT");
                        if (blocks.Length == 0)
                        {
                            theReadingBlock = new BWFReadingBlock();
                            theReadingBlock.periodID = prd.id;
                            theReadingBlock.blockName = "IMPORT";
                            theReadingBlock.readerID = migrationReaderID;
                            theReadingBlock.readingStart = DateTime.Now;
                            theReadingBlock.readingEnd = DateTime.Now;
                            theReadingBlock.id = SubscriberManagmentClient.BWFCreateReadingBlock(theReadingBlock);
                            newReadingBlockSize = 0;
                        }
                        else if (blocks.Length == 1)
                        {
                            theReadingBlock = blocks[0];
                            BWFDescribedMeterReading[] readingArray = SubscriberManagmentClient.BWFGetDescribedReadings(prd.id, 0, 1, out newReadingBlockSize);
                        }
                        else
                            throw new Exception("Multiple blocks found for period " + prd.name);
                    }

                    List<string[]> records = new List<string[]>();
                    int i;
                    Console.WriteLine("Loading recods");
                    for (i = 0; i < odbf.Header.RecordCount; i++)
                    {
                        if (!odbf.Read(i, dbrec))
                            break;
                        string[] rec = new string[dbrec.ColumnCount];
                        for (int j = 0; j < rec.Length; j++)
                            rec[j] = dbrec[j];
                        records.Add(rec);
                    }
                    records.Sort(new Comparison<string[]>(compareRecords));
                    Console.WriteLine(records.Count + " records loaded into memory");
                    i = -1;
                    foreach (string[] orec in records)
                    {
                        i++;
                        Console.CursorTop--;
                        Console.WriteLine(c + "                               ");
                        try
                        {
                            bool warning = false;
                            string wariningText = "";
                            string custCodeValue = orec[custCodeColumn].Trim();
                            string nameValue = orec[nameColumn].Trim();
                            if (string.IsNullOrEmpty(nameValue))
                            {
                                wariningText += wariningText == "" ? "" : "\n";
                                wariningText += "Empty customer name";
                                warning = true;
                            }
                            string statusValue = statusColumn == -1 ? "" : orec[statusColumn].Trim();
                            string kebeleValue = orec[kebeleColumn].Trim();
                            string hnValue = hnColumn == -1 ? "" : orec[hnColumn].Trim();
                            string conNoValue = conNoColumn == -1 ? "" : orec[conNoColumn].Trim();
                            string meterNoValue = meterNoColumn == -1 ? "" : orec[meterNoColumn].Trim();
                            string monthValue = monthColumn == -1 ? "" : orec[monthColumn].Trim();
                            string meterSizeValue = meterSizeColumn == -1 ? "" : orec[meterSizeColumn].Trim();
                            string typeValue = typeColumn == -1 ? "" : orec[typeColumn].Trim();
                            string prevReadingValue = prevReadingColumn == -1 ? "" : orec[prevReadingColumn].Trim();
                            string curReadingValue = curReadingColumn == -1 ? "" : orec[curReadingColumn].Trim();
                            int kebele;
                            if (!int.TryParse(kebeleValue, out kebele))
                            {
                                throw new Exception("Invalid kebele:" + kebeleValue);
                            }
                            if (custCodeValue.Length < 3)
                                continue;
                            string contractNo = kebele.ToString("00") + "-" + custCodeValue;
                            bool DFlage = statusValue.Equals("D", StringComparison.CurrentCultureIgnoreCase);
                            Subscription existing = SubscriberManagmentClient.GetSubscription(contractNo, DateTime.Now.Ticks);
                            Subscription subscription;
                            bool isUpdated = false;
                            bool isAdded = false;

                            if (!prepareBlockMode)
                            {

                                Subscriber subscriber = new Subscriber();
                                subscriber.name = nameValue;
                                subscriber.amharicName = "";
                                subscriber.Kebele = kebele;
                                subscriber.address = "House no:" + hnValue;
                                subscriber.subscriberType = getSubscType(typeValue);
                                subscriber.customerCode = "C" + contractNo;
                                if (subscriber.subscriberType == SubscriberType.Unknown)
                                {
                                    wariningText += wariningText == "" ? "" : "\n";
                                    wariningText += "Invalid subscriber type " + typeValue;
                                    warning = true;
                                }
                                if (existing == null)
                                {
                                    subscriber.id = SubscriberManagmentClient.RegisterSubscriber(subscriber);
                                    isAdded = true;
                                }
                                else
                                {
                                    if (!DFlage)
                                    {
                                        subscriber.id = existing.subscriber.id;
                                        if (!subscriber.name.Equals(existing.subscriber.name)
                                            || !subscriber.Kebele.Equals(existing.subscriber.Kebele)
                                            || !subscriber.address.Equals(existing.subscriber.address)
                                            || !subscriber.subscriberType.Equals(existing.subscriber.subscriberType)
                                            )
                                        {
                                            SubscriberManagmentClient.UpdateSubscriber(subscriber, null);
                                            isUpdated = true;
                                        }
                                    }
                                }
                                subscription = new Subscription();
                                subscription.subscriberID = subscriber.id;
                                subscription.Kebele = subscriber.Kebele;
                                subscription.address = hnValue;
                                subscription.contractNo = contractNo;
                                subscription.subscriptionType = SubscriptionType.Tap;
                                subscription.parcelNo = "";
                                subscription.ticksFrom = basePeriodDate.Ticks;
                                MeterData meterData = new MeterData();
                                if (!string.IsNullOrEmpty(meterNoValue))
                                {
                                    Subscription test = SubscriberManagmentClient.getSubscriptionByMeterNo(meterNoValue);
                                    if (test == null || (existing != null && (existing.id == test.id)))
                                    {
                                        string did = getDesciptionID(meterSizeValue.Trim());
                                        if (did == null)
                                        {
                                            wariningText += wariningText == "" ? "" : "\n";
                                            wariningText += "Invalid meter size " + meterSizeValue;
                                            warning = true;
                                        }
                                        else
                                        {
                                            meterData = new MeterData();
                                            meterData.serialNo = meterNoValue;
                                            meterData.itemCode = did;

                                        }
                                    }
                                    else
                                    {
                                        wariningText += wariningText == "" ? "" : "\n";
                                        wariningText += "Meter no already used for contract no " + test.contractNo;
                                        warning = true;
                                    }
                                }
                                subscription.meterData = meterData;
                                subscription.subscriptionStatus = SubscriptionStatus.Active;
                                subscription.remark = wariningText;

                                if (existing == null)
                                {
                                    subscription.id = SubscriberManagmentClient.AddSubscription(subscription);
                                    isAdded = true;
                                }
                                else
                                {
                                    subscription.id = existing.id;
                                    subscription.subscriptionStatus = existing.subscriptionStatus;
                                    if (!DFlage)
                                    {
                                        if (
                                            !subscription.subscriberID.Equals(existing.subscriberID)
                                            || !subscription.Kebele.Equals(existing.Kebele)
                                            || !subscription.address.Equals(existing.address)
                                        || !subscription.contractNo.Equals(existing.contractNo)
                                        || !subscription.subscriptionType.Equals(existing.subscriptionType)
                                        || !subscription.ticksFrom.Equals(existing.ticksFrom)
                                        || string.Compare(subscription.serialNo, existing.serialNo) != 0
                                        || string.Compare(subscription.itemCode, existing.itemCode) != 0
                                        )
                                        {

                                            SubscriberManagmentClient.UpdateSubscription(subscription);
                                            isUpdated = true;
                                        }
                                    }
                                }
                                if (month == 0)
                                {
                                    if ((subscription.subscriptionStatus != SubscriptionStatus.Active) != DFlage)
                                    {
                                        SubscriberManagmentClient.ChangeSubcriptionStatus(subscription, DateTime.Parse("June 7,2014"), DFlage ? SubscriptionStatus.Diconnected : SubscriptionStatus.Active);
                                        if (DFlage)
                                            disconnected++;
                                        else
                                            connected++;
                                    }
                                }

                            }
                            else
                                subscription = existing;
                            if (warning)
                            {
                                warningcount++;
                            }

                            if (month != 0 && !DFlage)
                            {
                                if (prd == null)
                                    throw new Exception("Invalid month " + monthValue);
                                int prvR, curR;
                                if (!int.TryParse(prevReadingValue, out prvR))
                                    throw new Exception("Invalid previous reading " + prevReadingValue);
                                if (!int.TryParse(curReadingValue, out curR))
                                    throw new Exception("Invalid current reading " + curReadingValue);
                                if (curR == 0 && prvR > 0)
                                {
                                    continue;
                                }
                                if (subscription != null && (!Account.AmountEqual(prvR, 0) || !Account.AmountEqual(curR, 00)))
                                {
                                    if (prepareBlockMode)
                                    {
                                        if (subscription.Kebele == assignKebele)
                                            setReading(subscription, prd, prvR, curR);
                                    }
                                    else
                                        setReading(subscription, prd, prvR, curR);

                                }
                            }
                            if (isAdded)
                                addedCust++;
                            else if (isUpdated)
                                updatedCust++;
                        }
                        catch (Exception ex)
                        {
                            errorcount++;
                            writeResult("Error processing record " + i + "\n" + ex.Message);
                            Console.WriteLine();
                        }
                        finally
                        {
                            c--;
                        }
                    }
                    writeResult("Finished processing " + fn);
                    writeResult(odbf.Header.RecordCount + " records processed");
                    writeResult(errorcount + " errors");
                    writeResult(filteredOut + " records filterd out");
                    writeResult((odbf.Header.RecordCount - errorcount - filteredOut) + " records transfered");
                    writeResult(warningcount + " records transfered with warning");
                    writeResult(string.Format("{0} customers added {1} updated\n{2} reading added {3} updated\n{4} disconected {5} connected", addedCust, updatedCust, addedReading, updatedReading, disconnected, connected));
                    Console.Beep();
                }
                catch (Exception filex)
                {
                    writeResult("Error processing " + fn + " " + filex.Message);
                }
                finally
                {
                    odbf.Close();
                }

            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    Console.WriteLine(ex.Message);
                    ex = ex.InnerException;
                }
            }
            finally
            {
                resultFile.Close();
            }

        }

        private static string getDesciptionID(string meterSizeValue)
        {
            for (int i = 0; i < meterSizeDescID.Length; i++)
            {
                if (dbMeterSize[i].Equals(meterSizeValue))
                    return meterSizeDescID[i];
            }
            return null;

        }

        private static int getColumnIndex(SocialExplorer.IO.FastDBF.DbfRecord orec, string p)
        {
            for (int i = 0; i < orec.ColumnCount; i++)
                if (orec.Column(i).Name.ToUpper().Equals(p.ToUpper()))
                    return i;
            return -1;
        }
    }
}
