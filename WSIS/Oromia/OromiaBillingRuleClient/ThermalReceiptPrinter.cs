﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace INTAPS.SubscriberManagment.Oromia
{
    class ThermalReceiptPrinter
    {
        private string ip;
        private int portn;

        public ThermalReceiptPrinter(string ip, int portn)
        {
            this.ip = ip;
            this.portn = portn;
        }

        internal void printNow(CustomerPaymentReceipt receipt, Client.PrintReceiptParameters pars, bool reprint, int nCopy)
        {
            TcpClient client = new TcpClient(ip, portn);
            try
            {
                ThermalDotNet.ThermalPrinter tp = new ThermalDotNet.ThermalPrinter(client.GetStream());
                for (int nc = 0; nc < nCopy; nc++)
                {
                    tp.SetMargins(40, 550);
                    tp.SetAlignCenter();
                    tp.WriteLine_Bold(pars.mainTitle);
                    tp.WriteLine_Bold("Reciept");
                    if(reprint)
                        tp.WriteLine_Bold("!!!!!!!!Reprint!!!!!!!!");
                    if(nc>0)
                        tp.WriteLine_Bold("(Copy)");
                    tp.SetAlignRight();
                    tp.WriteLine("No. " + receipt.receiptNumber.reference);
                    tp.WriteLine("Date: " + INTAPS.Ethiopic.EtGrDate.ToEth(receipt.DocumentDate) + " " + receipt.DocumentDate.ToShortTimeString());
                    tp.SetAlignLeft();
                    tp.WriteLine("_____________________________________________");

                    tp.WriteLine_Bold("Item\tQuantity\tUnit Price\tPrice");
                    double total = 0;
                    double pageTotal = 0;
                    for (int i = 0; i < receipt.billItems.Count; i++)
                    {
                        BillItem row = receipt.billItems[i];
                        tp.SetAlignLeft();
                        tp.WriteLine("_____________________________________________");

                        tp.WriteLine(String.Format("{0}. {1}", i + 1, row.description));
                        tp.SetAlignRight();
                        if (row.quantity > 0)
                            tp.WriteLine(String.Format("\t{0}\t{1}\t{2}", row.quantity, row.unitPrice.ToString("#,#0.00"), row.price.ToString("#,#0.00")));
                        else
                            tp.WriteLine(String.Format("\t\t\t{0}", row.price.ToString("#,#0.00")));
                        total += row.price;
                        pageTotal += row.price;
                        if (i > 0 && i < receipt.billItems.Count - 1 && i % 4 == 3) //page total
                        {
                            tp.SetAlignLeft();
                            tp.WriteLine("____________________________________________");
                            tp.SetAlignRight();
                            tp.WriteLine("Page Total: " + pageTotal.ToString("#,#0.00"));
                            pageTotal = 0;
                        }
                    }
                    tp.SetAlignLeft();
                    tp.WriteLine("____________________________________________");
                    tp.SetAlignRight();
                    tp.WriteLine("Total: " + total.ToString("#,#0.00"));
                    tp.SetAlignLeft();
                    tp.WriteLine("");
                    tp.WriteLine("Casher: "+pars.casheirName);
                    tp.WriteLine("");
                    tp.WriteLine("Signature: _________________");
                    tp.WriteLine("____________________________________________");
                    tp.WriteLine("Computerized by INTAPS. Call 0911227220");
                    tp.CutPaper();
                }
            }
            finally
            {
                client.Close();
            }

        }
    }
}
