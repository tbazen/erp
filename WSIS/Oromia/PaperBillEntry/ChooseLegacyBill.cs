﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class ChooseLegacyBill : Form
    {
        public ChooseLegacyBill(LegacyBill [] lb)
        {
            InitializeComponent();
            setupGrid();
            foreach (LegacyBill l in lb)
            {
                addLegacyBillRow(SubscriberManagmentClient.GetSubscription(l.customerID, DateTime.Now.Ticks), l);
            }

        }
        private void addLegacyBillRow(Subscription subsc, LegacyBill lb)
        {
            BillPeriod bp = SubscriberManagmentClient.GetBillPeriod(lb.periodID);
            INTAPS.Ethiopic.EtGrDate d = INTAPS.Ethiopic.EtGrDate.ToEth(bp.fromDate);

            int index = dataGrid.Rows.Add("", subsc.Kebele.ToString("00"), d.Year, d.Month, subsc.contractNo.Substring(3), subsc.subscriber.name, lb.reading, lb.consumption, lb.reference
                , lb.getItemAmount(1), lb.getItemAmount(2), lb.getItemAmount(3), lb.getItemAmount(4), lb.billTotal
                , lb.remark
                );
            dataGrid.Rows[index].Tag = lb;
        }
        void setupGrid()
        {
            colKebele.ValueType = typeof(int);
            colYear.ValueType = typeof(int);
            colMonth.ValueType = typeof(int);
            colReading.ValueType = typeof(double);
            colUse.ValueType = typeof(double);
            colBill.ValueType = typeof(double);

            colRent.ValueType = typeof(double);
            colOther.ValueType = typeof(double); ;
            colServiceCharge.ValueType = typeof(double);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public LegacyBill selectedBill;
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (dataGrid.SelectedRows.Count == 0)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Select a bill");
                return;
            }
            selectedBill = dataGrid.SelectedRows[0].Tag as LegacyBill;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
