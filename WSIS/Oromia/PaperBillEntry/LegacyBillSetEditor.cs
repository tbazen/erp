﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace INTAPS.SubscriberManagment.Client
{
    
    public partial class LegacyBillSetEditor : Form
    {
        
        int _setID;
        void setupGrid()
        {
            colYear.ValueType = typeof(int);
            colMonth.ValueType = typeof(int);
            colReading.ValueType = typeof(double);
            colUse.ValueType = typeof(double);
            colBill.ValueType = typeof(double);

            colRent.ValueType = typeof(double);
            colOther.ValueType = typeof(double); ;
            colServiceCharge.ValueType = typeof(double);
        }
        public LegacyBillSetEditor(LegacyBillSet set)
        {
            InitializeComponent();
            comboKebeles.Items.AddRange(SubscriberManagmentClient.GetAllKebeles());
            comboKebeles.SelectedIndex = 0;
            string[] users = INTAPS.ClientServer.Client.SecurityClient.GetAllUsers();
            Array.Sort(users);
            comboUsers.Items.AddRange(users);

            setupGrid();
            if (set == null)
            {
                _setID = -1;
            }
            else
            {
                _setID = set.id;
                comboUsers.Text = set.encoderUserID;
                textDescription.Text = set.description;
                LegacyBill[] lbs = SubscriberManagmentClient.lbGetLegacyBills(set.id);
                Kebele kebele = null;
                foreach (LegacyBill lb in lbs)
                {
                    Subscription subsc = SubscriberManagmentClient.GetSubscription(lb.customerID, DateTime.Now.Ticks);
                    if (kebele == null)
                    {
                        kebele = SubscriberManagmentClient.GetKebele(subsc.Kebele);
                        foreach (Kebele k in comboKebeles.Items)
                        {
                            if (k.id == kebele.id)
                            {
                                comboKebeles.SelectedItem = k;
                                break;
                            }
                        }
                    }
                    else
                        if (kebele.id != subsc.Kebele)
                            throw new Exception("Multiple kebele not supported in a single overdue bill set");

                    addLegacyBillRow(subsc, lb);
                }
            }
        }
       
        private void addLegacyBillRow(Subscription subsc, LegacyBill lb)
        {
            BillPeriod bp = SubscriberManagmentClient.GetBillPeriod(lb.periodID);
            INTAPS.Ethiopic.EtGrDate d = INTAPS.Ethiopic.EtGrDate.ToEth(bp.fromDate);

            int index = dataGrid.Rows.Add("",d.Year, d.Month, subsc.contractNo.Substring(3), subsc.subscriber.name, lb.reading, lb.consumption, lb.reference
                , lb.getItemAmount(1), lb.getItemAmount(2), lb.getItemAmount(3), lb.getItemAmount(4), lb.billTotal
                , lb.remark
                );
            dataGrid.Rows[index].Tag = subsc.id;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (comboUsers.SelectedIndex == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select a user");
                return;
            }
            if (textDescription.Text.Trim() == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter description");
                return;
            }
            if (comboKebeles.SelectedIndex == -1)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please select kebele");
                return;
            }

            try
            {

                LegacyBillSet set = new LegacyBillSet();
                set.id = _setID;
                set.description = textDescription.Text;
                set.encoderUserID = comboUsers.Text;
                List<LegacyBill> bills = new List<LegacyBill>();
                Kebele k = (Kebele)comboKebeles.SelectedItem;
                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    if (row.IsNewRow)
                        continue;

                    string cc = row.Cells[colCustomerCode.Index].Value as string;
                    Subscription subsc;
                    if (string.IsNullOrEmpty(cc)
                        || (subsc = SubscriberManagmentClient.GetSubscription(k.id.ToString("00") + "-" + cc, DateTime.Now.Ticks)) == null
                        )
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid customer code at row " + (row.Index + 1));
                        return;
                    }
                    LegacyBill lb = new LegacyBill();
                    lb.customerID = subsc.id;
                    if (!(row.Cells[colReading.Index].Value is double)
                        || !(row.Cells[colUse.Index].Value is double)

                        )
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Incomplete reading data at row " + (row.Index + 1));
                        return;
                    }
                    lb.reading = (double)row.Cells[colReading.Index].Value;
                    lb.consumption = (double)row.Cells[colUse.Index].Value;

                    if (!(row.Cells[colYear.Index].Value is int)
                        || !(row.Cells[colMonth.Index].Value is int)

                        )
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Incomplete year month data" + (row.Index + 1));
                        return;
                    }
                    DateTime date = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, (int)row.Cells[colMonth.Index].Value, (int)row.Cells[colYear.Index].Value)).GridDate;

                    BillPeriod bp = SubscriberManagmentClient.getPeriodForDate(date);
                    if (bp == null)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid year month data" + (row.Index + 1));
                        return;

                    }
                    lb.periodID = bp.id;
                    int itemID = 0;
                    List<LegacyBillItem> items = new List<LegacyBillItem>();
                    foreach (DataGridViewColumn col in new DataGridViewColumn[] { colBill, colRent, colOther, colServiceCharge })
                    {
                        itemID++;
                        LegacyBillItem lbi = new LegacyBillItem();
                        if (!(row.Cells[col.Index].Value is double)
                            || Account.AmountEqual(lbi.amount = (double)row.Cells[col.Index].Value, 0)
                            )
                            continue;
                        if (lbi.amount < 0)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Negative amount at row " + (row.Index + 1));
                            return;
                        }
                        lbi.itemID = itemID;
                        items.Add(lbi);
                    }
                    if (items.Count == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("No amount enterd at row " + (row.Index + 1));
                        return;
                    }
                    lb.items = items.ToArray();
                    string bref = row.Cells[colBillNumber.Index].Value as string;
                    if (string.IsNullOrEmpty(bref))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Bill no not entered at row " + (row.Index + 1));
                        return;
                    }
                    lb.reference = bref;
                    string rmrk = row.Cells[colRemark.Index].Value as string;
                    lb.remark = rmrk == null ? "" : rmrk;
                    int rindex = 1;
                    foreach (LegacyBill ob in bills)
                    {
                        if (ob.periodID == lb.periodID && ob.customerID == lb.customerID)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Row " + (row.Index + 1)+" is the repeat of row "+rindex);
                            return;
                        }
                        rindex++;
                    }
                    bills.Add(lb);
                }
                _setID = SubscriberManagmentClient.lbSaveLegacySet(set, bills.ToArray());
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("Overdue bill set succesfully saved");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error saving overdue bill set", ex);
            }
        }

        private void dataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter valid amount");
            e.ThrowException = false;
        }

        private void dataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == colYear.Index)
            {
                loadReadingFromDB(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colMonth.Index)
            {
                loadReadingFromDB(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colCustomerCode.Index)
            {
                loadCustomerFromDB(dataGrid.CurrentRow);
                loadReadingFromDB(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colReading.Index)
            {
            }
            else if (e.ColumnIndex == colUse.Index)
            {
            }
            else if (e.ColumnIndex == colBillNumber.Index)
            {
            }
            else if (e.ColumnIndex == colBill.Index)
            {
                updateTotalColumn(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colRent.Index)
            {
                updateTotalColumn(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colOther.Index)
            {
                updateTotalColumn(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colServiceCharge.Index)
            {
                DataGridViewRow nextRow = dataGrid.Rows[dataGrid.CurrentCell.RowIndex + 1];
                updateTotalColumn(dataGrid.CurrentRow);
                if (nextRow.IsNewRow)
                {
                    nextRow.Cells[colYear.Index].Value = dataGrid.CurrentRow.Cells[colYear.Index].Value;
                    nextRow.Cells[colMonth.Index].Value = dataGrid.CurrentRow.Cells[colMonth.Index].Value;
                }
            }
        }

        private void updateTotalColumn(DataGridViewRow row)
        {
            double total = 0;
            foreach (DataGridViewColumn col in new DataGridViewColumn[] { colBill, colRent, colOther, colServiceCharge })
            {
                if (row.Cells[col.Index].Value is double)
                    total += (double)row.Cells[col.Index].Value;
            }
            row.Cells[colTotal.Index].Value = total;
        }

        private void loadCustomerFromDB(DataGridViewRow row)
        {
            if (!(comboKebeles.SelectedItem is Kebele))
                return;
            Kebele kebele = (Kebele)comboKebeles.SelectedItem;
            row.Tag = null;
            object custCode = row.Cells[colCustomerCode.Index].Value;
            if (!(custCode is string))
                return;

            Subscription subsc = SubscriberManagmentClient.GetSubscription(kebele.id.ToString("00") + "-" + (string)custCode, DateTime.Now.Ticks);
            if (subsc == null || subsc.subscriber == null)
                return;
            row.Cells[colNam.Index].Value = subsc.subscriber.name;
            row.Tag = subsc.id;
        }
        BillPeriod getPeriod(int year, int month)
        {
            DateTime dt = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, month, year)).GridDate;
            BillPeriod bp = SubscriberManagmentClient.getPeriodForDate(dt);
            return bp;
        }

        private void loadReadingFromDB(DataGridViewRow row)
        {
            object year = row.Cells[colYear.Index].Value;
            if (!(year is int))
                return;
            object month = row.Cells[colMonth.Index].Value;
            if (!(month is int))
                return;
            BillPeriod bp = getPeriod((int)year, (int)month);
            if (bp == null)
                return;
            object custID = row.Tag;
            if (!(custID is int))
                return;
            BWFMeterReading reading = SubscriberManagmentClient.BWFGetMeterReadingByPeriod((int)custID, bp.id);
            if (reading != null)
            {
                row.Cells[colReading.Index].Value = reading.reading;
                row.Cells[colUse.Index].Value = reading.consumption;
            }
            
        }

        private void dataGrid_EnterComit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == colYear.Index)
            {
                dataGrid.CurrentCell = dataGrid[colMonth.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colMonth.Index)
            {
                dataGrid.CurrentCell = dataGrid[colCustomerCode.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colCustomerCode.Index)
            {
                dataGrid.CurrentCell = dataGrid[colReading.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colReading.Index)
            {
                dataGrid.CurrentCell = dataGrid[colUse.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colUse.Index)
            {
                dataGrid.CurrentCell = dataGrid[colBillNumber.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colBillNumber.Index)
            {
                dataGrid.CurrentCell = dataGrid[colBill.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colBill.Index)
            {
                dataGrid.CurrentCell = dataGrid[colRent.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colRent.Index)
            {
                dataGrid.CurrentCell = dataGrid[colOther.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colOther.Index)
            {
                dataGrid.CurrentCell = dataGrid[colServiceCharge.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colServiceCharge.Index)
            {

                dataGrid.CurrentCell = dataGrid[colYear.Index, dataGrid.CurrentCell.RowIndex + 1];
            }
        }

        private void dataGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            comboKebeles.Enabled = dataGrid.Rows.Count < 2;
            for (int r = 0; r < e.RowCount; r++)
            {
                dataGrid[colRowN.Index, e.RowIndex + r].Value = (e.RowIndex + r + 1).ToString();
            }
        }

        private void dataGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            comboKebeles.Enabled = dataGrid.Rows.Count < 2;
        }
        static string getCellText(Cell cell, List<string> stringTable)
        {
            if (cell == null)
                return null;
            if (cell.DataType != null && cell.DataType.InnerText == "s")
                return stringTable[int.Parse(cell.CellValue.Text)];
            double d;
            if (double.TryParse(cell.CellValue.Text, out d))
                return Math.Round(d, 6).ToString();
            return cell.CellValue.Text;
        }
        private void buttonImport_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel Files|*.xlsx|Macro Enabled Excel Files|*.xlsm";
            ofd.Multiselect = false;
            if (ofd.ShowDialog(this) != DialogResult.OK)
                return;
            string fn = ofd.FileName;
            SpreadsheetDocument doc = null;
            try
            {
                doc = SpreadsheetDocument.Open(fn, true);
                if (doc == null)
                {
                    MessageBox.Show("Couldn't open " + fn);
                    return;
                }

                importResults = new List<string>();

                SharedStringTable ss = doc.WorkbookPart.SharedStringTablePart.SharedStringTable;
                List<string> stringTable = new List<string>();
                foreach (SharedStringItem item in ss)
                {
                    stringTable.Add(item.Text.Text);
                }
                int recordCount = 0;
                int skippedRecord = 0;
                int warningRecords=0;
                foreach (WorksheetPart ws in doc.WorkbookPart.WorksheetParts)
                {
                    foreach (OpenXmlElement data in ws.Worksheet)
                    {
                        if (data is SheetData)
                        {
                            int row = -1;
                            Console.WriteLine("Transfering " + (data.ChildElements.Count - 1) + " Rows");
                            Console.WriteLine();
                            foreach (Row el in data)
                            {
                                row++;
                                try
                                {
                                    //Console.CursorTop--;
                                    Console.WriteLine(row + "                       ");

                                    if (row > 0)
                                    {
                                        if (el.ChildElements.Count < 8)
                                        {
                                            writeResult("Record {0}: Data Format Error: row got only {1} items, 8 items expected", row, el.ChildElements.Count);
                                            skippedRecord++;
                                            continue;
                                        }
                                        Dictionary<int, Cell> cells = new Dictionary<int, Cell>();
                                        foreach (Cell c in el)
                                        {
                                            string r=c.CellReference.Value;
                                            for(int i=0;i<r.Length;i++)
                                                if(Char.IsNumber(r[i]))
                                                {
                                                    r=r.Substring(0,i);
                                                    break;
                                                }

                                            cells.Add((int)(r[0]-'A')+1, c);
                                        }
                                        int cellColIndex = 1;
                                        
                                        string strKebele = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string custCode = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string custName = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strMonth = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strYear = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strMeterReading = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strWaterUse = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string billNumber = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strBill = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strRent = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strOther = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        string strServiceCharge = getCellText(getCell(cells, cellColIndex++), stringTable);
                                        
                                        double meterReading;
                                        if (!double.TryParse(strMeterReading, out meterReading))
                                        {
                                            writeResult("Record {0}: Invalid meter reading: '{1}'", row, strMeterReading);
                                            skippedRecord++;
                                            continue;
                                        }

                                        double waterUse = -1;
                                        if (!string.IsNullOrEmpty(strWaterUse) && !double.TryParse(strWaterUse, out waterUse))
                                        {
                                            writeResult("Record {0}: Invalid water use: '{1}'", row, strWaterUse);
                                            skippedRecord++;
                                            continue;
                                        }

                                        double bill;
                                        if (!double.TryParse(strBill, out bill) || bill < 0)
                                        {
                                            writeResult("Record {0}: Invalid bill: '{1}'", row, strBill);
                                            skippedRecord++;
                                            continue;
                                        }

                                        double rent=0;
                                        if (!string.IsNullOrEmpty(strOther) && !double.TryParse(strRent, out rent) || rent < 0)
                                        {
                                            writeResult("Record {0}: Invalid rent: '{1}'", row, strRent);
                                            skippedRecord++;
                                            continue;
                                        }

                                        double other = 0;
                                        if (!string.IsNullOrEmpty(strOther) && (!double.TryParse(strOther, out other) || other < 0))
                                        {
                                            writeResult("Record {0}: Invalid other fee: '{1}'", row, strOther);
                                            skippedRecord++;
                                            continue;
                                        }

                                        double serviceCharge = 0;
                                        if (!string.IsNullOrEmpty(strServiceCharge) && (!double.TryParse(strServiceCharge, out serviceCharge) || serviceCharge < 0))
                                        {
                                            writeResult("Record {0}: Invalid service charege: '{1}'", row, strServiceCharge);
                                            skippedRecord++;
                                            continue;
                                        }



                                        int month;
                                        if (!int.TryParse(strMonth, out month) || month < 1 || month > 12)
                                        {
                                            writeResult("Record {0}: Invalid month: '{1}'", row, strMonth);
                                            skippedRecord++;
                                            continue;
                                        }
                                        int year;
                                        if (!int.TryParse(strYear, out year) || year < 1980 || year > 2006)
                                        {
                                            writeResult("Record {0}: Invalid year: '{1}'", row, strYear);
                                            skippedRecord++;
                                            continue;
                                        }
                                        int kebele;
                                        if (!int.TryParse(strKebele, out kebele))
                                        {
                                            writeResult("Record {0}: Invalid kebele: '{1}'", row, strKebele);
                                            skippedRecord++;
                                            continue;
                                        }
                                        if (recordCount == 0)
                                        {
                                            foreach (Kebele k in comboKebeles.Items)
                                            {
                                                if (k.id == kebele)
                                                {
                                                    comboKebeles.SelectedItem = k;
                                                    break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Kebele k = comboKebeles.SelectedItem as Kebele;
                                            if (k.id != kebele)
                                            {
                                                writeResult("Record {0}: Kebele different from first kebele: '{1}'", row, strKebele);
                                                skippedRecord++;
                                                continue;
                                            }
                                        }
                                        string kcustcode = kebele.ToString("00") + "-" + custCode;
                                        Subscription subsc = SubscriberManagmentClient.GetSubscription(kcustcode,DateTime.Now.Ticks);
                                        if (subsc == null)
                                        {
                                            writeResult("Record {0}: Invalid customer code: '{1}'", row, kcustcode);
                                            skippedRecord++;
                                            continue;
                                        }
                                        if (!subsc.subscriber.name.ToUpper().Trim().Equals(custName.ToUpper().Trim()))
                                        {
                                            //writeResult("Record {0}: Warning name mismatch. Database name:'{1}'  excel name:{2}", row, subsc.subscriber.name, custName);
                                        }
                                        BillPeriod bp = getPeriod(year, month);
                                        if (bp == null)
                                        {
                                            writeResult("Record {0}: Invalid month year {1} month {2}", row, year, month);
                                            skippedRecord++;
                                            continue;

                                        }
                                        string remark="";
                                        BWFMeterReading mr = SubscriberManagmentClient.BWFGetMeterReadingByPeriod(subsc.id, bp.id);
                                        if (mr == null)
                                        {
                                            remark = "Reading not found in database";
                                            warningRecords++;
                                        }
                                        else
                                        {
                                            if (Math.Abs(mr.reading - meterReading) > 0.5
                                                || Math.Abs(mr.consumption - waterUse) > 0.5
                                                )
                                            {
                                                remark = "Reading mismatch with database";
                                                warningRecords++;
                                            }
                                            if (waterUse == -1)
                                                waterUse = mr.consumption;
                                        }
                                        LegacyBill lb = new LegacyBill();
                                        lb.consumption = waterUse;
                                        lb.customerID = subsc.id;
                                        lb.remark = remark;
                                        List<LegacyBillItem> bis = new List<LegacyBillItem>();
                                        int itemID = 0;
                                        foreach (double a in new double[] { bill, rent, other, serviceCharge })
                                        {
                                            itemID++;
                                            if (Account.AmountEqual(a, 0))
                                                continue;
                                            LegacyBillItem bi = new LegacyBillItem();
                                            bi.itemID = itemID;
                                            bi.amount = a;
                                            bis.Add(bi);
                                        }
                                        lb.items = bis.ToArray();
                                        lb.periodID = bp.id;
                                        lb.reading = meterReading;
                                        lb.reference = billNumber;
                                        addLegacyBillRow(subsc, lb);
                                        recordCount++;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    writeResult("Record {0}: Generica error: '{1}'", row, ex.Message);
                                }
                            }
                        }
                    }
                }
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage(string.Format("{0} records transfered\n{1} records skipped\n{2} records transfered with warning",recordCount,skippedRecord,warningRecords));
                if (importResults.Count > 0)
                {
                    string rsult = null;
                    foreach (string r in importResults)
                    {
                        if (rsult == null)
                            rsult = r;
                        else
                            rsult += "\n" + r;
                    }
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError(rsult);
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error transfering records", ex);
            }
            finally
            {
                if (doc != null)
                    doc.Close();
            }
        }

        private Cell getCell(Dictionary<int, Cell> cells, int row)
        {
            if (cells.ContainsKey(row))
                return cells[row];
            return null;
        }
        List<string> importResults;
        private void writeResult(string format, params object[] vals)
        {
            importResults.Add(string.Format(format, vals));
        }
    }
}
