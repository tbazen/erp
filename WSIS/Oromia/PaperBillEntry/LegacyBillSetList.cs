﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class LegacyBillSetList : Form
    {
        string[] _users;
        public LegacyBillSetList()
        {
            InitializeComponent();
            reloadList();
            listView_SelectedIndexChanged(null, null);
            _users = INTAPS.ClientServer.Client.SecurityClient.GetAllUsers();
            Array.Sort(_users);
            comboUsers.Items.Add("All Encoders");
            comboUsers.Items.AddRange(_users);
        }
        void reloadList()
        {
            pageNavigator.RecordIndex = 0;
            loadPage();
        }
        private void loadPage()
        {
            listView.Items.Clear();
            int N;
            LegacyBillSet[] sets= SubscriberManagmentClient.lbGetSet(comboUsers.SelectedIndex<1?null:_users[comboUsers.SelectedIndex-1], pageNavigator.RecordIndex,pageNavigator.PageSize,out N);
            foreach (LegacyBillSet set in sets)
            {
                ListViewItem item = new ListViewItem(set.encoderUserID);
                item.SubItems.Add(set.description);
                item.SubItems.Add(set.verfied?"Verified":"Not Verified");
                item.Tag = set;
                listView.Items.Add(item);
            }
            pageNavigator.NRec=N;
            

        }

        private void listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool selected = listView.SelectedItems.Count > 0;
            LegacyBillSet set = selected ? listView.SelectedItems[0].Tag as LegacyBillSet : null;
            buttonDelete.Enabled = buttonEdit.Enabled = selected;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            if (sender == buttonNew)
            {
                LegacyBillVerificationSetEditor editor = new LegacyBillVerificationSetEditor(null);
                editor.Show(this);
            }
            else
            {
                LegacyBillSetEditor editor = new LegacyBillSetEditor(null);
                editor.Show(this);
            }
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            reloadList();   
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            loadPage();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;
            LegacyBillSet set = listView.SelectedItems[0].Tag as LegacyBillSet;
            if (set.verfied)
            {
                LegacyBillVerificationSetEditor edit = new LegacyBillVerificationSetEditor(set);
                edit.Show(this);
            }
            else
            {
                LegacyBillSetEditor edit = new LegacyBillSetEditor(set);
                edit.Show(this);
            }

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count == 0)
                return;
            LegacyBillSet set = listView.SelectedItems[0].Tag as LegacyBillSet;
            if (!INTAPS.UI.UIFormApplicationBase.CurrentAppliation.UserConfirms("Are you sure you want to delete the selected overdue bill set?"))
                return;
            try
            {
                SubscriberManagmentClient.lbDeleteSet(set.id);
                listView.Items.Remove(listView.SelectedItems[0]);
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error trying to deleted overdue bill set", ex);
            }

        }

        private void comboUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            reloadList();
        }
    }
}
