﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class LegacyBillVerificationSetEditor : Form
    {
        int _setID;
        List<DataGridViewRow> _modifiedSubscription = new List<DataGridViewRow>();
        void setupGrid()
        {
            colKebele.ValueType = typeof(string);
            colYear.ValueType = typeof(int);
            colMonth.ValueType = typeof(int);
            colReading.ValueType = typeof(double);
            colUse.ValueType = typeof(double);
            colBill.ValueType = typeof(double);

            colRent.ValueType = typeof(double);
            colOther.ValueType = typeof(double); ;
            colServiceCharge.ValueType = typeof(double);

            
        }
        public LegacyBillVerificationSetEditor(LegacyBillSet set)
        {
            InitializeComponent();
            buttonMoveDown.Enabled = buttonMoveUP.Enabled = false;
            setupGrid();
            if (set == null)
            {
                _setID = -1;
            }
            else
            {
                _setID = set.id;
                textDescription.Text = set.description;
                textTotal.Text=set.recordTotal>0?set.recordTotal.ToString("#,#0.00"):"";
                LegacyBill[] lbs = SubscriberManagmentClient.lbGetLegacyBills(set.id);
                foreach (LegacyBill lb in lbs)
                {
                    Subscription subsc = SubscriberManagmentClient.GetSubscription(lb.customerID, DateTime.Now.Ticks);
                    addLegacyBillRow(subsc, lb);
                }
            }
        }

        private void addLegacyBillRow(Subscription subsc, LegacyBill lb)
        {
            BillPeriod bp = SubscriberManagmentClient.GetBillPeriod(lb.periodID);
            INTAPS.Ethiopic.EtGrDate d = INTAPS.Ethiopic.EtGrDate.ToEth(bp.fromDate);

            int index = dataGrid.Rows.Add("","X",subsc.Kebele.ToString("00"), d.Year, d.Month, subsc.contractNo.Substring(3), subsc.subscriber.name, lb.reading, lb.consumption, lb.reference
                , lb.getItemAmount(1), lb.getItemAmount(2), lb.getItemAmount(3), lb.getItemAmount(4), lb.billTotal
                , lb.remark
                );
            dataGrid.Rows[index].Tag = subsc;
            dataGrid.Rows[index].Cells[colRowN.Index].Value = (index + 1).ToString();
        }

        private void setLegacyBillRow(Subscription subsc, DataGridViewRow row, LegacyBill lb)
        {
            BillPeriod bp = SubscriberManagmentClient.GetBillPeriod(lb.periodID);
            INTAPS.Ethiopic.EtGrDate d = INTAPS.Ethiopic.EtGrDate.ToEth(bp.fromDate);
            row.Cells[colRowN.Index].Value = (row.Index + 1).ToString();
            row.Cells[colKebele.Index].Value=subsc.Kebele.ToString("00");
            row.Cells[colYear.Index].Value=d.Year;
            row.Cells[colMonth.Index].Value=d.Month;
            row.Cells[colCustomerCode.Index].Value=subsc.contractNo.Substring(3);
            row.Cells[colNam.Index].Value=subsc.subscriber.name;
            row.Cells[colReading.Index].Value=lb.reading;
            row.Cells[colUse.Index].Value=lb.consumption;
            row.Cells[colBillNumber.Index].Value=lb.reference;
            row.Cells[colBill.Index].Value=lb.getItemAmount(1);
            row.Cells[colRent.Index].Value=lb.getItemAmount(2);
            row.Cells[colOther.Index].Value=lb.getItemAmount(3);
            row.Cells[colServiceCharge.Index].Value=lb.getItemAmount(4);
            row.Cells[colTotal.Index].Value=lb.billTotal;
            row.Cells[colRemark.Index].Value=lb.remark;
            row.Tag = subsc;
            verifyRow(row);
        }
        private void buttonSave_Click(object sender, EventArgs e)
        {
            
            if (textDescription.Text.Trim() == "")
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter description");
                return;
            }
            try
            {

                LegacyBillSet set = new LegacyBillSet();
                set.id = _setID;
                set.description = textDescription.Text;
                set.encoderUserID = INTAPS.ClientServer.Client.ApplicationClient.UserName;
                if (!string.IsNullOrEmpty(textTotal.Text))
                {
                    if (!double.TryParse(textTotal.Text, out set.recordTotal) || set.recordTotal<0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Enter valid total amount");
                        return;
                    }
                }
                List<VerificationEntry> bills = new List<VerificationEntry>();
                foreach (DataGridViewRow row in dataGrid.Rows)
                {
                    if (row.IsNewRow)
                        continue;

                    string cc = row.Cells[colCustomerCode.Index].Value as string;
                    if (string.IsNullOrEmpty(cc))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid customer code at row " + (row.Index + 1));
                        return;
                    }

                    Subscription subsc;
                    object ok=row.Cells[colKebele.Index].Value;
                    int kid;
                    Kebele k;
                    if (!int.TryParse(ok as string,out kid) || (k=SubscriberManagmentClient.GetKebele(kid))==null )
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Correct kebele net set at row " + (row.Index + 1));
                        return;
                    }

                    VerificationEntry v = new VerificationEntry();
                    string conCode=cc;
                    subsc = SubscriberManagmentClient.GetSubscription(conCode, DateTime.Now.Ticks);
                    if (subsc == null || _modifiedSubscription.Contains(row))
                    {
                        string name = row.Cells[colNam.Index].Value as string;
                        if (string.IsNullOrEmpty(name))
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Customer name not given for " + (row.Index + 1));
                            return;
                        }

                        subsc = new Subscription();
                        subsc.subscriber = new Subscriber();
                        subsc.subscriber.name = name;
                        subsc.subscriber.amharicName= "";
                        subsc.subscriber.subscriberType = SubscriberType.Private;
                        subsc.subscriber.Kebele = k.id;
                        subsc.contractNo = cc;
                        subsc.Kebele = k.id;
                        subsc.itemCode = "008028001";
                        subsc.subscriptionType = SubscriptionType.Tap;
                        subsc.subscriptionStatus = SubscriptionStatus.Active;
                        v.subsc = subsc;
                    }

                    LegacyBill lb = new LegacyBill();
                    lb.customerID = subsc.id;
                    if (!(row.Cells[colReading.Index].Value is double)
                        || !(row.Cells[colUse.Index].Value is double)

                        )
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Incomplete reading data at row " + (row.Index + 1));
                        return;
                    }
                    lb.reading = (double)row.Cells[colReading.Index].Value;
                    lb.consumption = (double)row.Cells[colUse.Index].Value;

                    if (!(row.Cells[colYear.Index].Value is int)
                        || !(row.Cells[colMonth.Index].Value is int)

                        )
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Incomplete year month data" + (row.Index + 1));
                        return;
                    }
                    DateTime date = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, (int)row.Cells[colMonth.Index].Value, (int)row.Cells[colYear.Index].Value)).GridDate;

                    BillPeriod bp = SubscriberManagmentClient.getPeriodForDate(date);
                    if (bp == null)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid year month data" + (row.Index + 1));
                        return;

                    }
                    lb.periodID = bp.id;
                    int itemID = 0;
                    List<LegacyBillItem> items = new List<LegacyBillItem>();
                    foreach (DataGridViewColumn col in new DataGridViewColumn[] { colBill, colRent, colOther, colServiceCharge })
                    {
                        itemID++;
                        LegacyBillItem lbi = new LegacyBillItem();
                        if (!(row.Cells[col.Index].Value is double)
                            || Account.AmountEqual(lbi.amount = (double)row.Cells[col.Index].Value, 0)
                            )
                            continue;
                        if (lbi.amount < 0)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Negative amount at row " + (row.Index + 1));
                            return;
                        }
                        lbi.itemID = itemID;
                        items.Add(lbi);
                    }
                    if (items.Count == 0)
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("No amount enterd at row " + (row.Index + 1));
                        return;
                    }
                    lb.items = items.ToArray();
                    string bref = row.Cells[colBillNumber.Index].Value as string;
                    if (string.IsNullOrEmpty(bref))
                    {
                        INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Bill no not entered at row " + (row.Index + 1));
                        return;
                    }
                    lb.reference = bref;
                    string rmrk = row.Cells[colRemark.Index].Value as string;
                    lb.remark = rmrk == null ? "" : rmrk;
                    int rindex = 1;
                    foreach (VerificationEntry ob in bills)
                    {
                        if (ob.bill.periodID == lb.periodID && ob.bill.customerID == lb.customerID)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Row " + (row.Index + 1)+" is the repeat of row "+rindex);
                            return;
                        }
                        rindex++;
                    }
                    v.bill = lb;
                    bills.Add(v);
                }
                _setID = SubscriberManagmentClient.lbSaveLegacyVerificationSet(set, bills.ToArray());
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("Overdue bill set succesfully saved");
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Error saving overdue bill set", ex);
            }
        }

        private void dataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Enter valid amount");
            e.ThrowException = false;
        }
        bool verifyRow(DataGridViewRow row)
        {

            object year = row.Cells[colYear.Index].Value;
            if (!(year is int))
                return true;
            object month = row.Cells[colMonth.Index].Value;
            if (!(month is int))
                return true;
            BillPeriod bp = getPeriod((int)year, (int)month);
            if (bp == null)
                return true;
            Subscription cust = row.Tag as Subscription;
            if (cust == null)
                return true;

            LegacyBillSet set= SubscriberManagmentClient.lbGetVerifiedSetByCustomerAndPeriod(cust.id,bp.id);
            if (set != null && set.id != _setID)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Allready saved in the set " + set.description);
                for(int i=0;i<row.Cells.Count;i++)
                    row.Cells[i].Value = null;
                row.Tag = null;
                return false;  
            }
            return true;
        }
        private void dataGrid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == colYear.Index)
            {
                loadReadingFromDB(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colMonth.Index)
            {
                loadReadingFromDB(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colCustomerCode.Index)
            {
                loadCustomerFromDB(dataGrid.CurrentRow);
                loadReadingFromDB(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colNam.Index)
            {
                dataGrid.CurrentRow.Tag = null;
                _modifiedSubscription.Add(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colReading.Index)
            {
            }
            else if (e.ColumnIndex == colUse.Index)
            {
            }
            else if (e.ColumnIndex == colBillNumber.Index)
            {
                if (dataGrid.CurrentRow.Tag == null)
                {
                    loadReadingFromDBByBillNo(dataGrid.CurrentRow);
                }
            }
            else if (e.ColumnIndex == colBill.Index)
            {
                updateTotalColumn(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colRent.Index)
            {
                updateTotalColumn(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colOther.Index)
            {
                updateTotalColumn(dataGrid.CurrentRow);
            }
            else if (e.ColumnIndex == colServiceCharge.Index)
            {
                DataGridViewRow nextRow = dataGrid.Rows[dataGrid.CurrentCell.RowIndex + 1];
                updateTotalColumn(dataGrid.CurrentRow);
                if (nextRow.IsNewRow)
                {
                    nextRow.Cells[colYear.Index].Value = dataGrid.CurrentRow.Cells[colYear.Index].Value;
                    nextRow.Cells[colMonth.Index].Value = dataGrid.CurrentRow.Cells[colMonth.Index].Value;
                }
            }
        }

        private void updateTotalColumn(DataGridViewRow row)
        {
            double total = 0;
            foreach (DataGridViewColumn col in new DataGridViewColumn[] { colBill, colRent, colOther, colServiceCharge })
            {
                if (row.Cells[col.Index].Value is double)
                    total += (double)row.Cells[col.Index].Value;
            }
            row.Cells[colTotal.Index].Value = total;
        }

        private void loadCustomerFromDB(DataGridViewRow row)
        {
            row.Tag = null;
            object custCode = row.Cells[colCustomerCode.Index].Value;
            if (!(custCode is string))
                return;
            object kebele = row.Cells[colKebele.Index].Value;
            if (!(kebele is string))
                return;
            int k;
            if (!int.TryParse((string)kebele, out k))
                return;

            Subscription subsc = SubscriberManagmentClient.GetSubscription((string)custCode, DateTime.Now.Ticks);
            if (subsc == null || subsc.subscriber == null)
                return;
            row.Cells[colNam.Index].Value = subsc.subscriber.name;
            row.Tag = subsc;
        }
        BillPeriod getPeriod(int year, int month)
        {
            DateTime dt = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(1, month, year)).GridDate;
            BillPeriod bp = SubscriberManagmentClient.getPeriodForDate(dt);
            return bp;
        }

        private void loadReadingFromDB(DataGridViewRow row)
        {
            object year = row.Cells[colYear.Index].Value;
            if (!(year is int))
                return;
            object month = row.Cells[colMonth.Index].Value;
            if (!(month is int))
                return;
            BillPeriod bp = getPeriod((int)year, (int)month);
            if (bp == null)
                return;
            Subscription cust = row.Tag as Subscription;
            if (cust==null)
                return;
            if (!verifyRow(row))
                return;
            LegacyBill[] lb = SubscriberManagmentClient.lbFindBySubscriberAndPeriod(cust.id, bp.id);
            if (lb.Length == 1)
            {
                setLegacyBillRow(cust, row, lb[0]);
                return;
            }
            if (lb.Length > 1)
            {
                ChooseLegacyBill cb = new ChooseLegacyBill(lb);
                if (cb.ShowDialog(this) == DialogResult.OK)
                {
                    setLegacyBillRow(cust, row, cb.selectedBill);
                }
                return;
            }
            BWFMeterReading reading = SubscriberManagmentClient.BWFGetMeterReadingByPeriod(cust.id, bp.id);
            if (reading != null)
            {
                row.Cells[colReading.Index].Value = reading.reading;
                row.Cells[colUse.Index].Value = reading.consumption;
            }
        }
        private void loadReadingFromDBByBillNo(DataGridViewRow row)
        {
            object billNo = row.Cells[colBillNumber.Index].Value;
            if (!(billNo is string))
                return;
            LegacyBill[] lb = SubscriberManagmentClient.lbFindByBillNo((string)billNo);
            if (lb.Length == 1)
            {
                setLegacyBillRow(SubscriberManagmentClient.GetSubscription(lb[0].customerID, DateTime.Now.Ticks), row, lb[0]);
                return;
            }
            if (lb.Length > 1)
            {
                ChooseLegacyBill cb = new ChooseLegacyBill(lb);
                if (cb.ShowDialog(this) == DialogResult.OK)
                {
                    setLegacyBillRow(SubscriberManagmentClient.GetSubscription(lb[0].customerID, DateTime.Now.Ticks), row, cb.selectedBill);
                }
                else
                {

                }
            }
        }
        private void dataGrid_EnterComit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == colYear.Index)
            {
                dataGrid.CurrentCell = dataGrid[colMonth.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colMonth.Index)
            {
                dataGrid.CurrentCell = dataGrid[colCustomerCode.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colCustomerCode.Index)
            {
                dataGrid.CurrentCell = dataGrid[colReading.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colReading.Index)
            {
                dataGrid.CurrentCell = dataGrid[colUse.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colUse.Index)
            {
                dataGrid.CurrentCell = dataGrid[colBillNumber.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colBillNumber.Index)
            {
                dataGrid.CurrentCell = dataGrid[colBill.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colBill.Index)
            {
                dataGrid.CurrentCell = dataGrid[colRent.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colRent.Index)
            {
                dataGrid.CurrentCell = dataGrid[colOther.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colOther.Index)
            {
                dataGrid.CurrentCell = dataGrid[colServiceCharge.Index, dataGrid.CurrentCell.RowIndex];
            }
            else if (e.ColumnIndex == colServiceCharge.Index)
            {

                dataGrid.CurrentCell = dataGrid[colYear.Index, dataGrid.CurrentCell.RowIndex + 1];
            }
        }

        private void dataGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            for (int r = 0; r < e.RowCount; r++)
            {
                dataGrid[colRowN.Index, e.RowIndex + r].Value = (e.RowIndex + r + 1).ToString();
            }
            updateTotal();
        }

        private void dataGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            for (int r = 0; r < e.RowCount; r++)
            {
                _modifiedSubscription.Remove(dataGrid.Rows[e.RowIndex + r]);
            }
            updateTotal();
        }


        private void dataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == colDelete.Index)
            {
                if (dataGrid.Rows[e.RowIndex].IsNewRow)
                    return;
                dataGrid.Rows.RemoveAt(e.RowIndex);
            }
        }

        private void dataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGrid.SelectedRows.Count != 1 || dataGrid.SelectedRows[0].IsNewRow)
            {
                buttonMoveUP.Enabled = buttonMoveDown.Enabled = false;
                return;
            }
            buttonMoveUP.Enabled = dataGrid.SelectedRows[0].Index>0;
            buttonMoveDown.Enabled = dataGrid.SelectedRows[0].Index < dataGrid.Rows.Count - 2;
        }

        private void buttonMoveUP_Click(object sender, EventArgs e)
        {
            if (dataGrid.SelectedRows.Count == 0)
                return;
            DataGridViewRow row = dataGrid.SelectedRows[0];
            if (row.IsNewRow)
                return;
            if (row.Index == 0)
                return;
            int index = row.Index;
            dataGrid.Rows.RemoveAt(row.Index);
            dataGrid.Rows.Insert(index- 1, row);
            row.Selected = true;
            row.Cells[colRowN.Index].Value = (row.Index + 1).ToString();
            dataGrid.Rows[index].Cells[colRowN.Index].Value = (index + 1).ToString();
        }

        private void buttonMoveDown_Click(object sender, EventArgs e)
        {
            if (dataGrid.SelectedRows.Count == 0)
                return;
            DataGridViewRow row = dataGrid.SelectedRows[0];
            if (row.IsNewRow)
                return;
            if (row.Index == dataGrid.Rows.Count-2)
                return;
            int index = row.Index;
            dataGrid.Rows.RemoveAt(row.Index);
            dataGrid.Rows.Insert(index+1, row);
            row.Selected = true;
            row.Cells[colRowN.Index].Value = (row.Index + 1).ToString();
            dataGrid.Rows[index].Cells[colRowN.Index].Value = (index + 1).ToString();
        }
        void updateTotal()
        {
            double total = 0;
            foreach (DataGridViewRow row in dataGrid.Rows)
            {
                double rowTotal = 0;
                foreach (DataGridViewColumn col in new DataGridViewColumn[] { colBill, colRent, colOther, colServiceCharge })
                {
                    if (row.Cells[col.Index].Value is double)
                        rowTotal += (double)row.Cells[col.Index].Value;
                }
                double rt = (row.Cells[colTotal.Index].Value is double)?(double)row.Cells[colTotal.Index].Value:0;
                if (!AccountBase.AmountEqual(rowTotal, rt))
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Row total not correct for row " + (row.Index + 1)+"\n Total is now not valid");
                }
                total += rowTotal;
            }
            labelTotal.Text = total.ToString("#,#0.00");
        }

        private void dataGrid_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            updateTotal();
        }
    }
}
