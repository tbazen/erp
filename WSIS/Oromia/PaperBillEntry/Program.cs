﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using INTAPS.ClientServer.Client;
using INTAPS.SubscriberManagment.Client;
using INTAPS.SubscriberManagment;
using INTAPS.UI;

namespace INTAPS.SubscriberManagment.Client
{
    static class Program
    {
        static System.IO.StreamWriter resultFile;
        static void writeResult(string format,params object [] data)
        {
            format = string.Format(format, data);
            Console.WriteLine(format);
            resultFile.WriteLine(format);
        }
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login login = new Login();
            login.TryLogin();
            if (login.logedin)
            {
                LegacyBillSetList main = new LegacyBillSetList();
                new UIFormApplicationBase(main);
                Application.Run(main);
            }
        }
        static void OldMain(string []args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Not enough arguments.\nUsage ImportFromExcel [Excel File] [Option]\n Option: B for outstanding bill\nG for GPS coordinate");
                return;
            }
            string fn = args[0];
            SpreadsheetDocument doc =null;
            resultFile = System.IO.File.CreateText("result.txt");
            try
            {
                doc = SpreadsheetDocument.Open(fn, true);
                if (doc == null)
                {
                    Console.WriteLine("Couldn't open " + fn);
                    return;
                }
                string option = args[1].ToUpper();
                switch (option)
                {
                    case "B": importOutstandingBill(doc);
                        break;
                    case "G":
                        importGPSCoordinage(doc);
                        break;
                    default:
                        Console.WriteLine("Invalid option " + option);
                        return;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("General error.\n" + ex.Message);
            }
            finally
            {
                if (doc != null)
                {
                    doc.Close();
                }
                resultFile.Close();
            }
            Console.WriteLine("Done. Press enter to finish");
            Console.ReadLine();
        }

        private static void importGPSCoordinage(SpreadsheetDocument doc)
        {
            
        }

        static string getCellText(Cell cell, List<string> stringTable)
        {
            if (cell.DataType!=null && cell.DataType.InnerText == "s")
                return stringTable[int.Parse(cell.CellValue.Text)];
            double d;
            if (double.TryParse(cell.CellValue.Text, out d))
                return Math.Round(d, 6).ToString();
            return cell.CellValue.Text;
        }
        static Dictionary<int, BillPeriod[]> bpCache = new Dictionary<int, BillPeriod[]>();
        static BillPeriod getBillPeriod(int year, int month)
        {
            BillPeriod[] months;
            if (bpCache.ContainsKey(year))
                months = bpCache[year];
            else
                bpCache.Add(year,months=SubscriberManagmentClient.GetBillPeriods(year,true));
            return months[month - 1];
        }
        private static void importOutstandingBill(SpreadsheetDocument doc)
        {
            try
            {
                string userName = "admin";
                string password = "wsis";

                ApplicationClient.Connect("tcp://localhost:3004", userName, password);
                SubscriberManagmentClient.Connect("tcp://localhost:3004");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error initializing\n" + ex.Message);
                return;
            }
            
        }
    }
}
