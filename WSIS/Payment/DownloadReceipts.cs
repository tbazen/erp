using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class DownloadReceipts : Form
    {
        public DownloadReceipts()
        {
            this.components = null;
            this.InitializeComponent();
            if (!string.IsNullOrEmpty(Properties.Settings.Default.ImportExportFolder))
                filePicker.Text = Properties.Settings.Default.ImportExportFolder;
        }



        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.ImportExportFolder = filePicker.Text;
            Properties.Settings.Default.Save();
            e.Cancel = !btnCancel.Enabled;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            FileStream serializationStream = null;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                string path = this.filePicker.Text;
                int from;

                from = Program.bde.MsgRepo.getFirstUndeliveredMessageNumber();
                if (from == -1)
                    from = 0;
                else
                    from = from - 1;
                int last = Program.bde.MsgRepo.getLastUndeliveredMessageNumber();
                int count = last - from;
                int nFile = count / SalePointBDE.NMSG_PER_FILE + (count % SalePointBDE.NMSG_PER_FILE == 0 ? 0 : 1);
                progressBar1.Maximum = nFile - 1;
                for (int i = 0; i < nFile; i++)
                {
                    int fileSize = count > SalePointBDE.NMSG_PER_FILE ? SalePointBDE.NMSG_PER_FILE : count;
                    MessageList list = Program.bde.MsgRepo.getUndeliveredMessageList(from + 1, from + fileSize);
                    if (list.data.Length == 0)
                        continue;
                    PaymentCenterMessageList pclist = new PaymentCenterMessageList(Program.bde.SysPars.centerAccountID, list);
                    serializationStream = File.Create(path + @"\" + (from + 1).ToString("0000000000") + "-" + (from + fileSize).ToString("0000000000") + ".posmessage");
                    formatter.Serialize(serializationStream, pclist);
                    serializationStream.Close();
                    progressBar1.Value = i;
                    from += SalePointBDE.NMSG_PER_FILE;
                    count -= SalePointBDE.NMSG_PER_FILE;
                    Application.DoEvents();
                }
                btnOk.Enabled = btnCancel.Enabled = filePicker.Enabled = true;
                base.Close();
            }
            catch (Exception exception)
            {
                btnOk.Enabled = btnCancel.Enabled = filePicker.Enabled = true;
                if (serializationStream != null)
                {
                    serializationStream.Close();
                }
                UIFormApplicationBase.CurrentAppliation.HandleException("Error downloading updates.", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}