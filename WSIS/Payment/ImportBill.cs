
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows.Forms;
using INTAPS.ClientServer;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ImportBill : Form
    {
 
        private IContainer components = null;
   
        public ImportBill()
        {
            this.InitializeComponent();
            try
            {
                if (!string.IsNullOrEmpty(Properties.Settings.Default.ImportExportFolder))
                    filePicker.Text = Properties.Settings.Default.ImportExportFolder;
            }
            catch
            {
                filePicker.Text = "";
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.ImportExportFolder = filePicker.Text;
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }
        

        private void btnOk_Click(object sender, EventArgs e)
        {
            Exception exception;
            try
            {
                base.Enabled = false;
                string[] files = Directory.GetFiles(this.filePicker.Text, "*.posupdate");
                Array.Sort<string>(files);
                this.lvErrors.Items.Clear();
                BinaryFormatter formatter = new BinaryFormatter();
                int num = 0;
                foreach (string str2 in files)
                {
                    ListViewItem item;
                    
                    FileStream serializationStream = null;
                    try
                    {
                        serializationStream = File.OpenRead(str2);
                        MessageList update = (MessageList)formatter.Deserialize(serializationStream);
                        serializationStream.Close();
                        Program.bde.Update(update);
                        if (this.chkDelete.Checked)
                        {
                            File.Delete(str2);
                        }
                        item = new ListViewItem(str2);
                        item.SubItems.Add("Done");
                        item.ForeColor = Color.Green;
                        this.lvErrors.Items.Insert(0,item);
                        Application.DoEvents();
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                        if (serializationStream != null)
                        {
                            serializationStream.Close();
                        }
                        item = new ListViewItem(str2);
                        item.ForeColor = Color.Red;
                        item.SubItems.Add(exception.Message);
                        this.lvErrors.Items.Insert(0,item);
                        item.Tag = ex;
                        num++;
                    }
                    Application.DoEvents();
                }
                if (num == 0)
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Update imported with no errors.");
                }
                else
                {
                    UIFormApplicationBase.CurrentAppliation.ShowUserMessage(string.Concat(new object[] { "Update imported with ", num, " error", (this.lvErrors.Items.Count == 1) ? "." : "s." }));
                }
            }
            catch (Exception exception2)
            {
                exception = exception2;
                UIFormApplicationBase.CurrentAppliation.HandleException("Error importing bills.", exception);
            }
            finally
            {
                base.Enabled = true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ImportBill_Load(object sender, EventArgs e)
        {
        }

        private void lvErrors_SelectedIndexChanged(object sender, EventArgs e)
        {
            textError.Text = "";
            if (lvErrors.SelectedItems.Count == 0)
                return;
            Exception ex = lvErrors.SelectedItems[0].Tag as Exception;
            if (ex == null)
                return;
            textError.Text = ex.Message + "\n" + ex.StackTrace;
        }

  
    }
}

