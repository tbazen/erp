using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class PrePaidCash : Form
    {
        private Subscription _subscription;
        private IContainer components = null;


        public PrePaidCash(Subscription subsc)
        {
            this.InitializeComponent();
            this._subscription = subsc;
            this.textCubicMeter.Text = "5";
            this.labelName.Text = subsc.subscriber.name;
            this.labelContractNo.Text = subsc.contractNo;
            this.UpdateAmount();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

            if (Program.bde.cardDriver == null)
            {
                bool posted = false;

                CustomerPaymentReceipt payment = null;
                PrintReceiptParameters printParam = null;
                try
                {

                    payment = Program.bde.PayPrepaid(this._subscription, double.Parse(this.textCubicMeter.Text));
                    posted = true;
                    printParam = new PrintReceiptParameters();
                    printParam.casheirName = Program.bde.SysPars.centerName;
                    printParam.mainTitle = Program.bde.SysPars.receiptTitle;
                    SubscriberManagmentClient.billingRuleClient.printReceipt(payment, new PrintReceiptParameters() { casheirName = Program.bde.SysPars.centerName, mainTitle = Program.bde.SysPars.receiptTitle }, false, 1);

                    //Write print log
                    PrintLogData.WritePrintLog(payment, printParam, 1, null, false);
                    base.DialogResult = DialogResult.OK;
                    base.Close();

                }
                catch (Exception exception)
                {
                    PrintLogData.WritePrintLog(payment, printParam, 1, exception, false);
                    if (posted)
                    {
                        UIFormApplicationBase.CurrentAppliation.HandleException("Payment is registerd but there was problem with priting the receipt", exception);
                        this.Close();
                    }
                    else
                        UIFormApplicationBase.CurrentAppliation.HandleException("Error registering prepayment", exception);
                }
            }
            else
            {
                bool posted = false;
                PrintReceiptParameters printParam = null;
                Program.bde.PayPrepaid(this._subscription, double.Parse(this.textCubicMeter.Text)
                    ,new UI.HTML.ProcessTowParameter<CustomerPaymentReceipt, Exception>(
                        (payment, ex)=>
                        {
                            if (ex == null)
                            {
                                posted = true;
                                printParam = new PrintReceiptParameters();
                                printParam.casheirName = Program.bde.SysPars.centerName;
                                printParam.mainTitle = Program.bde.SysPars.receiptTitle;
                                SubscriberManagmentClient.billingRuleClient.printReceipt(payment, new PrintReceiptParameters() { casheirName = Program.bde.SysPars.centerName, mainTitle = Program.bde.SysPars.receiptTitle }, false, 1);

                                //Write print log
                                PrintLogData.WritePrintLog(payment, printParam, 1, null, false);
                                base.DialogResult = DialogResult.OK;
                                base.Close();
                            }
                            else
                            {
                                PrintLogData.WritePrintLog(payment, printParam, 1, ex, false);
                                if (posted)
                                {
                                    UIFormApplicationBase.CurrentAppliation.HandleException("Payment is registerd but there was problem with priting the receipt", ex);
                                    this.Close();
                                }
                                else
                                    UIFormApplicationBase.CurrentAppliation.HandleException("Error registering prepayment", ex);
                            }
                        })
                    );
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void linkErrorDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Exception tag = this.linkErrorDetail.Tag as Exception;
            if (tag != null)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error calculating amounts", tag);
            }
        }

        private void textCash_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.UpdateAmount();
                this.btnPrint.Focus();
            }
        }

        private void textContractNo_TextChanged(object sender, EventArgs e)
        {
            this.btnPrint.Enabled = false;
        }

        private void UpdateAmount()
        {
            double num;
            if (!((double.TryParse(this.textCubicMeter.Text, out num) && (num >= 1.0)) && Account.AmountEqual(num, Math.Floor(num))))
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError("Please enter valid amount");
            }
            else
            {
                try
                {
                    PrePaymentBill receipt = SubscriberManagmentClient.PreviewPrepaidReceipt(DateTime.Now, this._subscription.id, num);
                    this.textAmount.Text = receipt.total.ToString("#,#.00");
                    this.textServiceAmount.Text = receipt.paymentAmount.ToString("#,#.00");
                    this.textRentAmount.Text = (receipt.total - receipt.paymentAmount).ToString("#,#.00");
                    this.btnPrint.Enabled = true;
                    this.linkErrorDetail.Visible = false;
                }
                catch (Exception exception)
                {
                    this.linkErrorDetail.Visible = true;
                    this.linkErrorDetail.Tag = exception;
                    UIFormApplicationBase.CurrentAppliation.HandleException("Error calculating amount", exception);
                    this.textAmount.Text = "Error";
                    this.textServiceAmount.Text = "Error";
                    this.textRentAmount.Text = "Error";
                }
            }
        }
    }
}

