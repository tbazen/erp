﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class PrePaidCash : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrePaidCash));
            this.textCubicMeter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textServiceAmount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textRentAmount = new System.Windows.Forms.TextBox();
            this.linkErrorDetail = new System.Windows.Forms.LinkLabel();
            this.labelName = new System.Windows.Forms.Label();
            this.labelContractNo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textCubicMeter
            // 
            this.textCubicMeter.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCubicMeter.Location = new System.Drawing.Point(166, 12);
            this.textCubicMeter.Name = "textCubicMeter";
            this.textCubicMeter.Size = new System.Drawing.Size(372, 29);
            this.textCubicMeter.TabIndex = 1;
            this.textCubicMeter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textCubicMeter.TextChanged += new System.EventHandler(this.textContractNo_TextChanged);
            this.textCubicMeter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textCash_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F);
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cubic Meter";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.btnPrint.Location = new System.Drawing.Point(271, 246);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(118, 37);
            this.btnPrint.TabIndex = 2;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 12F);
            this.btnCancel.Location = new System.Drawing.Point(415, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(118, 37);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F);
            this.label2.Location = new System.Drawing.Point(7, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 22);
            this.label2.TabIndex = 4;
            this.label2.Text = "Total Pament";
            // 
            // textAmount
            // 
            this.textAmount.BackColor = System.Drawing.Color.White;
            this.textAmount.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAmount.Location = new System.Drawing.Point(166, 56);
            this.textAmount.Name = "textAmount";
            this.textAmount.ReadOnly = true;
            this.textAmount.Size = new System.Drawing.Size(372, 29);
            this.textAmount.TabIndex = 5;
            this.textAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textAmount.TextChanged += new System.EventHandler(this.textContractNo_TextChanged);
            this.textAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textCash_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F);
            this.label3.Location = new System.Drawing.Point(175, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 22);
            this.label3.TabIndex = 6;
            this.label3.Text = "For Service";
            // 
            // textServiceAmount
            // 
            this.textServiceAmount.BackColor = System.Drawing.Color.White;
            this.textServiceAmount.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textServiceAmount.Location = new System.Drawing.Point(295, 106);
            this.textServiceAmount.Name = "textServiceAmount";
            this.textServiceAmount.ReadOnly = true;
            this.textServiceAmount.Size = new System.Drawing.Size(243, 29);
            this.textServiceAmount.TabIndex = 7;
            this.textServiceAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textServiceAmount.TextChanged += new System.EventHandler(this.textContractNo_TextChanged);
            this.textServiceAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textCash_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F);
            this.label4.Location = new System.Drawing.Point(175, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 22);
            this.label4.TabIndex = 8;
            this.label4.Text = "For Rent";
            // 
            // textRentAmount
            // 
            this.textRentAmount.BackColor = System.Drawing.Color.White;
            this.textRentAmount.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRentAmount.Location = new System.Drawing.Point(295, 141);
            this.textRentAmount.Name = "textRentAmount";
            this.textRentAmount.ReadOnly = true;
            this.textRentAmount.Size = new System.Drawing.Size(243, 29);
            this.textRentAmount.TabIndex = 9;
            this.textRentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textRentAmount.TextChanged += new System.EventHandler(this.textContractNo_TextChanged);
            this.textRentAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textCash_KeyDown);
            // 
            // linkErrorDetail
            // 
            this.linkErrorDetail.AutoSize = true;
            this.linkErrorDetail.Location = new System.Drawing.Point(474, 191);
            this.linkErrorDetail.Name = "linkErrorDetail";
            this.linkErrorDetail.Size = new System.Drawing.Size(59, 13);
            this.linkErrorDetail.TabIndex = 10;
            this.linkErrorDetail.TabStop = true;
            this.linkErrorDetail.Text = "Error Detail";
            this.linkErrorDetail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkErrorDetail_LinkClicked);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F);
            this.labelName.Location = new System.Drawing.Point(12, 205);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(63, 22);
            this.labelName.TabIndex = 8;
            this.labelName.Text = "Name";
            // 
            // labelContractNo
            // 
            this.labelContractNo.AutoSize = true;
            this.labelContractNo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F);
            this.labelContractNo.Location = new System.Drawing.Point(12, 244);
            this.labelContractNo.Name = "labelContractNo";
            this.labelContractNo.Size = new System.Drawing.Size(112, 22);
            this.labelContractNo.TabIndex = 8;
            this.labelContractNo.Text = "Contact No";
            // 
            // PrePaidCash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(549, 295);
            this.Controls.Add(this.linkErrorDetail);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.textRentAmount);
            this.Controls.Add(this.textServiceAmount);
            this.Controls.Add(this.labelContractNo);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textAmount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textCubicMeter);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PrePaidCash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pre Payment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.TextBox textCubicMeter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textServiceAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textRentAmount;
        private System.Windows.Forms.LinkLabel linkErrorDetail;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelContractNo;
    }
}