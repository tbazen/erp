    using INTAPS.UI;
    using System;
    using System.Configuration;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    internal static class Program
    {
        public static SalePointBDE bde;
        public static bool PosPrinterMode;
        public static PosPrinter thePostPrinter;

        [STAThread]
        private static void Main()
        {
            try
            {
                bool result;
                System.Threading.Mutex mutex = new System.Threading.Mutex(true, "INTAPS.WSIS.SalePoint", out result);

                if (!result)
                {
                    MessageBox.Show("Another instance is already running.");
                    return;
                }
                GC.KeepAlive(mutex);

                SalesMainForm form;
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ConnectionMode mode = (ConnectionMode)Enum.Parse(typeof(ConnectionMode), ConfigurationManager.AppSettings["ConnectionMode"]);
                if (!bool.TryParse(ConfigurationManager.AppSettings["PosPrinterMode"], out PosPrinterMode))
                {
                    PosPrinterMode = false;
                }
                if (PosPrinterMode)
                {
                    thePostPrinter = new PosPrinter();
                }
                if (mode == ConnectionMode.Connected)
                {
                    PaymentLogin login = new PaymentLogin();
                    login.TryLogin();
                    if (!login.logedin)
                    {
                        return;
                    }
                }
                bde = new SalePointBDE(System.Configuration.ConfigurationManager.AppSettings["DBName"], System.Configuration.ConfigurationManager.ConnectionStrings["main"].ConnectionString);
                SubscriberManagmentClient.loadBillingRuleEngineClient();
                bde.Connect(mode, true);
                new UIFormApplicationBase(form = new SalesMainForm());
                Application.Run(form);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error starting Sale point\n" + ex.Message + "\n" + ex.StackTrace);
            }
        }
        static void salesEmulator()
        {

        }
    }
}

