
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class ReceiptList : Form
    {
        private const int PAGE_SIZE = 100;

        public ReceiptList()
        {
            this.InitializeComponent();
            this.LoadPage();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.textCustomerCode.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.LoadPage();
        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
            e.Cancel = this.listView.SelectedItems.Count == 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void LoadPage()
        {
            try
            {
                int i;
                ListViewItem item;
                this.listView.Items.Clear();
                CustomerPaymentReceipt[] receiptArray = null;
                SubscriberSearchField field=textContractNo.Text.Trim()!=""?SubscriberSearchField.ConstactNo:SubscriberSearchField.CustomerCode;
                string code=textContractNo.Text.Trim()!=""?textContractNo.Text:textCustomerCode.Text;
                receiptArray = Program.bde.getReceipts(field,code);

                for (i = 0; i < receiptArray.Length; i++)
                {
                    if (receiptArray[i] == null)
                    {
                        item = new ListViewItem("Null receipt");
                    }
                    else
                    {
                        if (receiptArray[i].receiptNumber == null)
                            continue;
                        item = new ListViewItem(receiptArray[i].DocumentDate.ToString("MMM dd,yyyy  hh:mm tt"));
                        item.SubItems.Add(receiptArray[i].receiptNumber.reference);
                        item.SubItems.Add(receiptArray[i].customer.name);
                        item.SubItems.Add(receiptArray[i].customer.customerCode);
                        item.SubItems.Add(receiptArray[i].totalInstrument.ToString("0,0.00"));
                        item.SubItems.Add("");
                        item.Tag = receiptArray[i];
                    }
                    this.listView.Items.Add(item);
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error loading receipts", exception);
            }
        }

        private void miVoid_Click(object sender, EventArgs e)
        {
            SalesPointVoidReceipt voidForm;
            voidForm = new SalesPointVoidReceipt(this.listView.SelectedItems[0].Tag as CustomerPaymentReceipt);
            if (voidForm.ShowDialog(this) == DialogResult.OK)
            {
                this.LoadPage();
            }
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            this.LoadPage();
        }

        private void reprintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CustomerPaymentReceipt receipt = null;
            PrintReceiptParameters printParameters = new PrintReceiptParameters();
            try
            {
                receipt = this.listView.SelectedItems[0].Tag as CustomerPaymentReceipt;
                Program.bde.setBillItemField(receipt);
                int loggedCashierAccID = Program.bde.SysPars.centerAccountID;
                if (Program.bde.ConMode == ConnectionMode.Connected)
                {
                    if (loggedCashierAccID != receipt.assetAccountID)
                    {
                        string cashAccountName = AccountingClient.GetAccount<Account>(AccountingClient.GetCostCenterAccount(receipt.assetAccountID).accountID).Name;
                        MessageBox.Show("You cannot reprint this receipt since you're not the one who issued the receipt the first time. The receipt was first issued at " + cashAccountName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Reprint not allowed");
                    return;
                }
                printParameters.mainTitle = Program.bde.SysPars.receiptTitle;
                printParameters.casheirName = Program.bde.SysPars.centerName;
                SubscriberManagmentClient.billingRuleClient.printReceipt(receipt, printParameters, true, 1);
                PrintLogData.WritePrintLog(receipt, printParameters, 1, null,true);
            }
            catch (Exception ex)
            {
                PrintLogData.WritePrintLog(receipt, printParameters, 1, ex, true);
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(null, ex);
                string msg = "";
                while (ex != null)
                {
                    msg+="\n"+ex.Message + "\n" + ex.StackTrace;
                    ex = ex.InnerException;
                }
                MessageBox.Show(msg);
            }
        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnSearch_Click(null, null);
            }
        }
        bool _ignoreEvent = false;
        private void textQuery_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _ignoreEvent = true;
            textContractNo.Text = "";
            _ignoreEvent = false;
        }

        private void textContractNo_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _ignoreEvent = true;
            textCustomerCode.Text = "";
            _ignoreEvent = false;
        }

    }
}