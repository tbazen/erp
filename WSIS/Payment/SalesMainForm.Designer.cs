﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SalesMainForm : System.Windows.Forms.Form
    {
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesMainForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.miImport = new System.Windows.Forms.ToolStripMenuItem();
            this.miRefreshStatus = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miUpdateConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptsListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.controlBrowser = new System.Windows.Forms.WebBrowser();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSeach = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblTotalLabel = new System.Windows.Forms.Label();
            this.textContractNo = new System.Windows.Forms.TextBox();
            this.textCustomerCode = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.billsList = new INTAPS.UI.HTML.ControlBrowser();
            this.buttonReadCard = new System.Windows.Forms.Button();
            this.toolStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip.Size = new System.Drawing.Size(440, 30);
            this.toolStrip.TabIndex = 8;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExport,
            this.miImport,
            this.miRefreshStatus,
            this.toolStripSeparator1,
            this.miUpdateConfiguration,
            this.receiptsListToolStripMenuItem,
            this.toolStripMenuItem1});
            this.toolStripDropDownButton1.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(75, 27);
            this.toolStripDropDownButton1.Text = "More";
            this.toolStripDropDownButton1.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // miExport
            // 
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(344, 30);
            this.miExport.Text = "Export";
            this.miExport.Click += new System.EventHandler(this.btnSaveReceipts_Click);
            // 
            // miImport
            // 
            this.miImport.Name = "miImport";
            this.miImport.Size = new System.Drawing.Size(344, 30);
            this.miImport.Text = "Import";
            this.miImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // miRefreshStatus
            // 
            this.miRefreshStatus.Name = "miRefreshStatus";
            this.miRefreshStatus.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.miRefreshStatus.Size = new System.Drawing.Size(344, 30);
            this.miRefreshStatus.Text = "Refresh Status";
            this.miRefreshStatus.Click += new System.EventHandler(this.miRefreshStatus_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(341, 6);
            // 
            // miUpdateConfiguration
            // 
            this.miUpdateConfiguration.Name = "miUpdateConfiguration";
            this.miUpdateConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.miUpdateConfiguration.Size = new System.Drawing.Size(344, 30);
            this.miUpdateConfiguration.Text = "Update Configuration";
            this.miUpdateConfiguration.Click += new System.EventHandler(this.miUpdateConfiguration_Click);
            // 
            // receiptsListToolStripMenuItem
            // 
            this.receiptsListToolStripMenuItem.Name = "receiptsListToolStripMenuItem";
            this.receiptsListToolStripMenuItem.Size = new System.Drawing.Size(344, 30);
            this.receiptsListToolStripMenuItem.Text = "Receipts List";
            this.receiptsListToolStripMenuItem.Click += new System.EventHandler(this.receiptsListToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(344, 30);
            this.toolStripMenuItem1.Text = "Updater Status";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.controlBrowser);
            this.panel1.Controls.Add(this.toolStrip);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1028, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(442, 878);
            this.panel1.TabIndex = 11;
            // 
            // controlBrowser
            // 
            this.controlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser.Location = new System.Drawing.Point(0, 30);
            this.controlBrowser.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.controlBrowser.MinimumSize = new System.Drawing.Size(30, 31);
            this.controlBrowser.Name = "controlBrowser";
            this.controlBrowser.Size = new System.Drawing.Size(440, 846);
            this.controlBrowser.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel2.Controls.Add(this.buttonReadCard);
            this.panel2.Controls.Add(this.btnSeach);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblName);
            this.panel2.Controls.Add(this.lblTotalLabel);
            this.panel2.Controls.Add(this.textContractNo);
            this.panel2.Controls.Add(this.textCustomerCode);
            this.panel2.Controls.Add(this.lblTotal);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1028, 285);
            this.panel2.TabIndex = 14;
            // 
            // btnSeach
            // 
            this.btnSeach.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.btnSeach.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSeach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeach.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeach.ForeColor = System.Drawing.Color.White;
            this.btnSeach.Image = ((System.Drawing.Image)(resources.GetObject("btnSeach.Image")));
            this.btnSeach.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSeach.Location = new System.Drawing.Point(822, 145);
            this.btnSeach.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeach.Name = "btnSeach";
            this.btnSeach.Size = new System.Drawing.Size(177, 43);
            this.btnSeach.TabIndex = 7;
            this.btnSeach.Text = "&Search";
            this.btnSeach.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSeach.UseVisualStyleBackColor = false;
            this.btnSeach.Click += new System.EventHandler(this.btnSeach_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label2.Location = new System.Drawing.Point(18, 155);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Contract No";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.label1.Location = new System.Drawing.Point(18, 111);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Customer Code";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(18, 197);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(22, 23);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "#";
            // 
            // lblTotalLabel
            // 
            this.lblTotalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTotalLabel.AutoSize = true;
            this.lblTotalLabel.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.lblTotalLabel.Location = new System.Drawing.Point(18, 242);
            this.lblTotalLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalLabel.Name = "lblTotalLabel";
            this.lblTotalLabel.Size = new System.Drawing.Size(62, 23);
            this.lblTotalLabel.TabIndex = 4;
            this.lblTotalLabel.Text = "Total:";
            // 
            // textContractNo
            // 
            this.textContractNo.BackColor = System.Drawing.SystemColors.Window;
            this.textContractNo.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContractNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.textContractNo.Location = new System.Drawing.Point(262, 152);
            this.textContractNo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textContractNo.Name = "textContractNo";
            this.textContractNo.Size = new System.Drawing.Size(511, 30);
            this.textContractNo.TabIndex = 0;
            this.textContractNo.TextChanged += new System.EventHandler(this.textContractNo_TextChanged);
            this.textContractNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textContractNo_KeyDown);
            // 
            // textCustomerCode
            // 
            this.textCustomerCode.BackColor = System.Drawing.SystemColors.Window;
            this.textCustomerCode.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCustomerCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.textCustomerCode.Location = new System.Drawing.Point(262, 108);
            this.textCustomerCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textCustomerCode.Name = "textCustomerCode";
            this.textCustomerCode.Size = new System.Drawing.Size(511, 30);
            this.textCustomerCode.TabIndex = 0;
            this.textCustomerCode.TextChanged += new System.EventHandler(this.textCustomerCode_TextChanged);
            this.textCustomerCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textContractNo_KeyDown);
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTotal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.lblTotal.Location = new System.Drawing.Point(122, 229);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(268, 49);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "0.00";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // billsList
            // 
            this.billsList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.billsList.Location = new System.Drawing.Point(0, 285);
            this.billsList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.billsList.MinimumSize = new System.Drawing.Size(30, 31);
            this.billsList.Name = "billsList";
            this.billsList.Size = new System.Drawing.Size(1028, 593);
            this.billsList.StyleSheetFile = null;
            this.billsList.TabIndex = 15;
            // 
            // buttonReadCard
            // 
            this.buttonReadCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(123)))), ((int)(((byte)(173)))));
            this.buttonReadCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonReadCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReadCard.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReadCard.ForeColor = System.Drawing.Color.White;
            this.buttonReadCard.Image = ((System.Drawing.Image)(resources.GetObject("buttonReadCard.Image")));
            this.buttonReadCard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonReadCard.Location = new System.Drawing.Point(822, 188);
            this.buttonReadCard.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonReadCard.Name = "buttonReadCard";
            this.buttonReadCard.Size = new System.Drawing.Size(177, 43);
            this.buttonReadCard.TabIndex = 7;
            this.buttonReadCard.Text = "&Read Card";
            this.buttonReadCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonReadCard.UseVisualStyleBackColor = false;
            this.buttonReadCard.Click += new System.EventHandler(this.buttonReadCard_Click);
            // 
            // SalesMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.ClientSize = new System.Drawing.Size(1470, 878);
            this.Controls.Add(this.billsList);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SalesMainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Settlement";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SalesMainForm_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem miExport;
        private System.Windows.Forms.ToolStripMenuItem miImport;
        private System.Windows.Forms.ToolStripMenuItem miRefreshStatus;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.WebBrowser controlBrowser;
        private System.Windows.Forms.ToolStripMenuItem miUpdateConfiguration;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSeach;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblTotalLabel;
        private System.Windows.Forms.TextBox textCustomerCode;
        private System.Windows.Forms.Label lblTotal;
        private UI.HTML.ControlBrowser billsList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem receiptsListToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textContractNo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Button buttonReadCard;
    }
}