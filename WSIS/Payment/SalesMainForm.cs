using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer.Client;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SalesMainForm : Form
    {
        private const string CHECK_MARK = "X";
        private IContainer components = null;

        private CustomerBillDocument[] _bills = null;
        private bool m_searchState;
        private Subscriber _customer = null;
        protected const int MAX_ITEMS = 200;

        public SalesMainForm()
        {
            this.InitializeComponent();
            this.UpdateStatusText();
            FixWindowTitle();
            miExport.Enabled = miImport.Enabled = miUpdateConfiguration.Enabled = Program.bde.ConMode == ConnectionMode.DisconnectedUpdate;
            this.SetState(true);
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                SearchResultPopup popup = new SearchResultPopup(this.textCustomerCode.Text);
                if (popup.ShowDialog(this) == DialogResult.OK)
                {
                    this.setCustomerCode(popup.result.contractNo);
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't void the bills.", exception);
            }
        }



        int compareCustomerBillDocuments(CustomerBillDocument b1, CustomerBillDocument b2)
        {
            PeriodicBill pb1 = b1 as PeriodicBill;
            PeriodicBill pb2 = b2 as PeriodicBill;
            if (pb1 == null && pb2 == null)
                return b1.DocumentDate.CompareTo(b2.DocumentDate);
            if (pb1 == null && pb2 != null)
                return -1;
            if (pb2 == null && pb1 != null)
                return 1;
            WaterBillDocument cb1 = pb1 as WaterBillDocument;
            WaterBillDocument cb2 = pb2 as WaterBillDocument;
            if (cb1 == null && cb2 == null)
                return pb1.period.fromDate.CompareTo(pb2.period.fromDate);
            if (cb1 == null && cb2 != null)
                return -1;
            if (cb2 == null && cb1 != null)
                return 1;
            int comp = cb1.subscription.contractNo.CompareTo(cb2.subscription.contractNo);
            if(comp==0)
                return cb1.period.fromDate.CompareTo(cb2.period.fromDate);
            return comp;
        }
        private void PopulateBills(CustomerBillDocument[] cbills)
        {
            Array.Sort<CustomerBillDocument>(cbills,new Comparison<CustomerBillDocument>(compareCustomerBillDocuments));
            List<CustomerBillDocument> paiableBills = new List<CustomerBillDocument>();
            foreach (CustomerBillDocument bill in cbills)
            {
                if (bill == null)
                    continue;
                CustomerBillRecord rec = Program.bde.getCustomerBillRecord(bill.AccountDocumentID);
                if (rec.isPayedOrDiffered)
                    continue;
                paiableBills.Add(bill);
            }
            this._bills = paiableBills.ToArray();
            List<int> list = new List<int>();
            List<double> list2 = new List<double>();
            List<double> list3 = new List<double>();
            List<double> list4 = new List<double>();
            StringBuilder html = new StringBuilder( "<table>");
                double total = 0.0;
                html.Append("<tr><td>Item</td><td>Unit Price</td><td>Quantity</td><td>Price</td><td>From Deposit</td><td>Payment</td></tr>");
            foreach (CustomerBillDocument bill in paiableBills)
            {
                foreach(BillItem bi in Program.bde.getCustomerBillItems(bill.AccountDocumentID))
                {
                    SalePointBDE.hotFixBillMonth(bill, bi); 
                    html.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>"
                    ,bi.description
                    ,System.Web.HttpUtility.HtmlEncode(bi.hasUnitPrice?bi.unitPrice.ToString("#,#0.00"):"")
                    , System.Web.HttpUtility.HtmlEncode(bi.hasUnitPrice ? bi.quantity.ToString("0.00") : "")
                    , System.Web.HttpUtility.HtmlEncode(bi.price.ToString("0.00"))
                    , System.Web.HttpUtility.HtmlEncode(bi.settledFromDepositAmount.ToString("0.00"))
                    , System.Web.HttpUtility.HtmlEncode(bi.netPayment.ToString("0.00"))
                    ));
                total += bi.netPayment;
                }
            }
            if (total > 0.0)
            {
                this.lblTotal.Text = total.ToString("0,0.00");
            }
            else
            {
                this.lblTotal.Text = "";
            }
            if (this._customer != null)
            {
                this.lblName.Text = this._customer.name+" Customer Code: "+_customer.customerCode;
            }
            this.billsList.LoadTextPage("Bills", html.ToString());
        }

        public void setCustomerCode(string customerCode)
        {
            this.textCustomerCode.Text = customerCode;
            this.btnSeach_Click(null, null);
        }

        private void SetState(bool searchState)
        {
            if (searchState)
            {
                this.lblName.Visible = false;
                this.lblTotal.Visible = false;
                this.lblTotalLabel.Visible = false;
                this.billsList.Visible = false;
                this.btnSeach.Text = "Search";
                this.buttonReadCard.Enabled = Program.bde.cardDriver != null;
                this.billsList.LoadTextPage("Bills","");
            }
            else
            {
                this.lblName.Visible = true;
                this.lblTotal.Visible = true;
                this.lblTotalLabel.Visible = true;
                this.billsList.Visible = true;
                this.btnSeach.Text = "Pay";
                this.textCustomerCode.Focus();
                this.textCustomerCode.SelectAll();
            }
            this.m_searchState = searchState;
        }
        private void textContractNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnSeach_Click(this.btnSeach, null);
            }
        }

       

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            //if (Program.bde.ConMode == ConnectionMode.Connected)
            //{
            //    INTAPS.SubscriberManagment.Client.SubscriberManagmentClient.saveTranslation();
            //}
            base.OnFormClosing(e);
        }
        private void FixWindowTitle()
        {
            if (Program.bde.ConMode == ConnectionMode.Connected)
                this.Text = "Payment Center: " + SubscriberManagmentClient.GetCurrentPaymentCenter().centerName;
            else
                this.Text = "Payment Center: " + Program.bde.SysPars.centerName;
        }

        void statusChanged()
        {
            this.UpdateStatusText();
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            new ImportBill().ShowDialog(this);
            this.UpdateStatusText();
        }

        private void btnSaveReceipts_Click(object sender, EventArgs e)
        {
            new DownloadReceipts().ShowDialog(this);
        }

        private void btnVoid_Click(object sender, EventArgs e)
        {
            try
            {
                new ReceiptList().Show(this);
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't void the bills.", exception);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        

        private void miRefreshStatus_Click(object sender, EventArgs e)
        {
            this.UpdateStatusText();
        }

        
        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SalesReport().Show();
        }
        void invalidateStatusText()
        {
            this.controlBrowser.DocumentText = "Refresh status to update status.";
        }
        public void UpdateStatusText()
        {
            //string html = Program.bde.GetStatusHTML();
            //this.controlBrowser.DocumentText = html;
            this.controlBrowser.DocumentText = "Updating..";
            new System.Threading.Thread(new System.Threading.ThreadStart(
                delegate()
                {
                    string html = Program.bde.GetStatusHTML();
                    this.Invoke(new INTAPS.UI.HTML.ProcessParameterless(delegate()
                        {
                            this.controlBrowser.DocumentText = html;
                        }
                        ));
                })).Start();

        }

        private void miUpdateConfiguration_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog sf = new OpenFileDialog();
                sf.Filter = "WSIS Sales Point Configuration|*.spconfig";
                sf.Multiselect = false;
                if (sf.ShowDialog(this) == DialogResult.OK)
                {
                    SPSystemParameters pars = null;
                    System.IO.FileStream fs = System.IO.File.OpenRead(sf.FileName);
                    try
                    {
                        pars = new System.Xml.Serialization.XmlSerializer(typeof(SPSystemParameters)).Deserialize(fs) as SPSystemParameters;
                        Program.bde.updateParameter(pars);
                        FixWindowTitle();
                    }
                    finally
                    {
                        fs.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException(ex.Message, ex);
            }
        }
        private void btnSeach_Click(object sender, EventArgs e)
        {
            if (this.m_searchState)
            {
                string code;
                SubscriberSearchField field;
                if (!this.textCustomerCode.Text.Trim().Equals(""))
                {
                    code = textCustomerCode.Text;
                    field = SubscriberSearchField.CustomerCode;
                }
                else
                {
                    code = textContractNo.Text;
                    field = SubscriberSearchField.ConstactNo;
                }
                CustomerBillDocument[] spbills = null;
                try
                {
                    if (field == SubscriberSearchField.ConstactNo && Program.bde.ConMode==ConnectionMode.Connected)
                    {
                        Subscription subsc=Program.bde.GetSubscription(code, DateTime.Now);
                        if(subsc==null)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserInformation("Invalid contract no.");
                            return;
                        }
                        if (subsc.prePaid)
                        {
                            if (new PrePaidCash(subsc).ShowDialog(this) == DialogResult.OK)
                                invalidateStatusText();
                            return;
                        }
                    }
                    spbills = Program.bde.SearchBill(field, code, out _customer);
                }
                catch (Exception exception)
                {
                    UIFormApplicationBase.CurrentAppliation.HandleException("Search failed", exception);
                    return;
                }
                if ((spbills == null) || (spbills.Length == 0))
                {

                    if (_customer == null)
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Contract no not found.");
                    }
                    else
                    {
                        UIFormApplicationBase.CurrentAppliation.ShowUserMessage("No unpaid bill");
                    }

                    
                }
                else
                {

                    this.PopulateBills(spbills);
                    if (this._bills != null)
                    {
                        this.SetState(false);
                    }
                }
                if (field == SubscriberSearchField.CustomerCode)
                {
                    this.textCustomerCode.SelectAll();
                    this.textCustomerCode.Focus();
                }
                else
                {
                    this.textContractNo.SelectAll();
                    this.textContractNo.Focus();
                }
            }
            else
            {
                if(new Cash(Program.bde, _bills,1).ShowDialog(this)==DialogResult.OK)
                {
                    invalidateStatusText();
                }
                this.textCustomerCode.Focus();
                this.textCustomerCode.SelectAll();
                this.SetState(true);
            }
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void textCustomerCode_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            _ignoreEvent = true;
            try
            {
                textContractNo.Text = "";
                SetState(true);
            }
            finally
            {
                _ignoreEvent = false;
            }
        }

        private void receiptsListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReceiptList rl = new ReceiptList();
            rl.Show(this);
        }
        bool _ignoreEvent = false;
        private void textContractNo_TextChanged(object sender, EventArgs e)
        {
            if (_ignoreEvent) return;
            try
            {
                textCustomerCode.Text = "";
                SetState(true);
            }
            finally
            {
                _ignoreEvent = false;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Program.bde.runUpdateLoop = false;
            base.OnClosing(e);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UpdaterStatus s = new UpdaterStatus();
            s.Show(this);
        }

        private void SalesMainForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonReadCard_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            Program.bde.cardDriver.readConnectionID(PrepaidDriver.ConnectionIDType.ConnectionID,
                (conID) =>
                {
                    this.Invoke((MethodInvoker)(() => { 
                    this.Enabled = true;
                        try
                        {
                            int intConID;

                            if (!int.TryParse(conID, out intConID))
                            {
                                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Card contains invalid connection ID:" + conID);
                            }
                            Subscription subsc = Program.bde.GetSubscription(intConID, DateTime.Now);
                            if (subsc == null)
                            {
                                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Card contains invalid connection ID:" + conID);
                            }
                            this.textContractNo.Text = subsc.contractNo;
                            this.btnSeach_Click(null, null);
                        }
                        catch(Exception ex)
                        {
                            INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Failed to load connection data.", ex);
                        }
                    }));
                },
                (m,ex)=>
                {
                    this.Invoke((MethodInvoker)(() => {
                        this.Enabled = true;
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserError("Failed to read card");
                    }));
                }
                );
        }
    }
}

