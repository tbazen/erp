using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SalesPointVoidReceipt : Form
    {

        private CustomerPaymentReceipt _receipt;

        public SalesPointVoidReceipt(CustomerPaymentReceipt receipt)
        {
            this._receipt = null;
            this.components = null;
            this.InitializeComponent();
            this._receipt = receipt;
            this.comboReason.Items.AddRange(Enum.GetNames(typeof(VoidReasonType)));
            this.comboReason.SelectedIndex = 0;
        }



        private void btnCancel_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                VoidReasonType reason = (VoidReasonType)Enum.Parse(typeof(VoidReasonType), (string)this.comboReason.SelectedItem);
                Program.bde.voidReceipt(this._receipt);
                base.DialogResult = DialogResult.OK;
                base.Close();
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Error voiding receipt.", exception);
            }
        }




        private void textAddress_TextChanged(object sender, EventArgs e)
        {
        }
    }
}

