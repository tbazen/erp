﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class SalesReport : System.Windows.Forms.Form
    {
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SalesReport));
            this.bowserController1 = new INTAPS.UI.HTML.BowserController();
            this.controlBrowser1 = new INTAPS.UI.HTML.ControlBrowser();
            this.SuspendLayout();
            // 
            // bowserController1
            // 
            this.bowserController1.Location = new System.Drawing.Point(0, 0);
            this.bowserController1.Name = "bowserController1";
            this.bowserController1.ShowBackForward = true;
            this.bowserController1.ShowRefresh = true;
            this.bowserController1.Size = new System.Drawing.Size(566, 25);
            this.bowserController1.TabIndex = 1;
            this.bowserController1.Text = "bowserController1";
            // 
            // controlBrowser1
            // 
            this.controlBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlBrowser1.Location = new System.Drawing.Point(0, 25);
            this.controlBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.controlBrowser1.Name = "controlBrowser1";
            this.controlBrowser1.Size = new System.Drawing.Size(566, 368);
            this.controlBrowser1.StyleSheetFile = "StyleSheet.css";
            this.controlBrowser1.TabIndex = 2;
            // 
            // SalesReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 393);
            this.Controls.Add(this.controlBrowser1);
            this.Controls.Add(this.bowserController1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SalesReport";
            this.Text = "Sales Report";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private INTAPS.UI.HTML.BowserController bowserController1;
        private INTAPS.UI.HTML.ControlBrowser controlBrowser1;

    }
}