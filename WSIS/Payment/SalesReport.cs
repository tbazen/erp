
    using INTAPS.UI.HTML;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SalesReport : Form
    {
        
        private IContainer components = null;
        public SalesReport()
        {
            this.InitializeComponent();
            this.LoadData();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void LoadData()
        {
            this.bowserController1.SetBrowser(this.controlBrowser1);
            this.controlBrowser1.LoadTextPage("Reprot", Program.bde.GetSalesReport());
        }
    }
}

