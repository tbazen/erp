
    using INTAPS.SubscriberManagment;
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class SearchResultPopup : Form
    {
        private IContainer components;
  
        private string m_query;
        private const int PAGE_SIZE = 20;
      public Subscription result;
        public SearchResultPopup()
        {
            this.components = null;
            this.result = null;
            this.InitializeComponent();
        }

        public SearchResultPopup(string query) : this()
        {
            this.m_query = query.Trim();
            this.pageNavigator.PageSize = 20;
            this.pageNavigator.RecordIndex = 0;
            if (!string.IsNullOrEmpty(this.m_query))
            {
                this.LoadPage();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.textQuery.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.m_query = this.textQuery.Text;
            this.pageNavigator.RecordIndex = 0;
            this.LoadPage();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void listView_DoubleClick(object sender, EventArgs e)
        {
            if (this.listView.SelectedItems.Count != 0)
            {
                this.result = this.listView.SelectedItems[0].Tag as Subscription;
                base.DialogResult = DialogResult.OK;
            }
        }

        private void LoadPage()
        {
            try
            {
                int N;
                Subscriber[] customers = Program.bde.searchSustomers(this.m_query, this.pageNavigator.RecordIndex, 20, out N);
                this.pageNavigator.NRec = N;
                this.listView.Items.Clear();
                foreach (Subscriber customer in customers)
                {
                    ListViewItem item = new ListViewItem(customer.name);
                    item.SubItems.Add(customer.customerCode);
                    item.SubItems.Add(customer.Kebele.ToString("00"));
                    item.Tag = customer;
                    this.listView.Items.Add(item);
                }
            }
            catch (Exception exception)
            {
                UIFormApplicationBase.CurrentAppliation.HandleException("Couldn't load subscriptions.", exception);
            }
        }

        private void pageNavigator_PageChanged(object sender, EventArgs e)
        {
            this.LoadPage();
        }

        private void textQuery_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.btnSearch_Click(null, null);
            }
        }

        private void textQuery_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void textQuery_TextChanged(object sender, EventArgs e)
        {
        }
    }
}

