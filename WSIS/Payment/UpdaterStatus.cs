﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.SubscriberManagment.Client
{
    public partial class UpdaterStatus : Form
    {
        public UpdaterStatus()
        {
            InitializeComponent();
            if(!Program.bde.runUpdateLoop)
            {
                labelUpdates.Text = "Upadate loop is not running.";
            }
            else
                timer1.Start();
        }
        bool firstStatus = true;
        string loopSatus = null;
        bool loopError = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            bool er;
            string s = Program.bde.getUpdateLoopStatus(out er);
            if(s==null)
                s="";
            if(firstStatus || loopError!=er || !s.Equals(loopSatus))
            {
                loopSatus = s;
                loopError = er;
                firstStatus = false;
                labelUpdates.Text = s;
                if (loopError)
                    labelUpdates.ForeColor = Color.Red;
                else
                    labelUpdates.ForeColor = Color.Black;
            }
        }
    }
}
