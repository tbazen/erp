﻿namespace INTAPS.SubscriberManagment.Client
{
    public partial class VoidPassword : System.Windows.Forms.Form
    {
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoidPassword));
            this.textPassword = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textPassword
            // 
            this.textPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPassword.Location = new System.Drawing.Point(30, 24);
            this.textPassword.Name = "textPassword";
            this.textPassword.PasswordChar = 'X';
            this.textPassword.Size = new System.Drawing.Size(551, 31);
            this.textPassword.TabIndex = 1;
            this.textPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textPassword_KeyDown);
            // 
            // VoidPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(614, 83);
            this.ControlBox = false;
            this.Controls.Add(this.textPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VoidPassword";
            this.Text = "Void Password";
            this.Load += new System.EventHandler(this.VoidPassword_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.TextBox textPassword;
    }
}