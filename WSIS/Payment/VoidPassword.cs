
    using INTAPS.UI;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
namespace INTAPS.SubscriberManagment.Client
{

    public partial class VoidPassword : Form
    {
        private IContainer components = null;
        public VoidPassword()
        {
            this.InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }


        private void OnCancel()
        {
            base.Close();
        }

        private void OnOk()
        {
            if (this.textPassword.Text == Program.bde.SysPars.voidPassword)
            {
                base.DialogResult = DialogResult.OK;
            }
            else
            {
                UIFormApplicationBase.CurrentAppliation.ShowUserError("Invalid void password.");
            }
        }

        private void textPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                this.OnOk();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.OnCancel();
            }
        }

        private void VoidPassword_Load(object sender, EventArgs e)
        {

        }
    }
}

