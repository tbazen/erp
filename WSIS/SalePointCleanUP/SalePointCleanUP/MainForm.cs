﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment;
using INTAPS.RDBMS;

namespace SalePointCleanUP
{
    public partial class MainForm : XtraForm
    {
        private INTAPS.RDBMS.SQLHelper helper;
        public MainForm()
        {
            InitializeComponent();
            string connString = System.Configuration.ConfigurationManager.AppSettings["SalePointDB"];
            try
            {
                helper = new INTAPS.RDBMS.SQLHelper(connString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error connecting to SalePoint DB. Check connection string." + ex.Message);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            lblDescription.Text = "Finding old paid bills...";

            SetControlsState(false);
            progressBar.Properties.PercentView = true;
            progressBar.Properties.Step = 1;
            progressBar.Properties.Minimum = 0;

            string sqlCount = "Select count(*) from ObjectInstance where typeID=8";
            int count = (int)helper.ExecuteScalar(sqlCount);

            progressBar.Position = 0;
            progressBar.Properties.Maximum = count;

            Dictionary<int, CustomerBillRecord> deleteCustomerBillRecords = new Dictionary<int, CustomerBillRecord>();
            Dictionary<int, List<Tuple<int, string>>> deleteObjects = new Dictionary<int, List<Tuple<int, string>>>();
            IDataReader reader = helper.ExecuteReader("Select id, data,typeID from ObjectInstance where typeID=8");
            int i = 0;
            DateTime now = DateTime.Now;
            int delObjectCount = 0;
            while (reader.Read())
            {
                if (i % 100 == 0)
                {
                    progressBar.EditValue = i;
                    Application.DoEvents();
                }
                byte[] data = reader[1] as byte[];
                string id = reader[0] as string;
                int typeID = (int)reader[2];
                CustomerBillRecord ob = (CustomerBillRecord)SQLHelper.BinaryToObject(data);
                if (ob.paymentDocumentID > 0 && now.Subtract(ob.paymentDate).TotalDays > 40)
                {
                    deleteCustomerBillRecords.Add(ob.id, ob);
                    List<Tuple<int, string>> t = new List<Tuple<int, string>>();
                    t.Add(new Tuple<int, string>(typeID, id));
                    deleteObjects.Add(ob.id, t);
                    
                    delObjectCount++;
                }
                i++;
            }
            reader.Close();

            sqlCount = "Select count(*) from ObjectInstance where typeID<>8";
            count = (int)helper.ExecuteScalar(sqlCount);

            progressBar.Position = 0;
            progressBar.Properties.Maximum = count;

            i = 0;
            lblDescription.Text = deleteCustomerBillRecords.Count+ " old bills found. Finding related objects to delete...";
            try
            {
                //Select object instanance data row by row. Preferred this since loading the whole data on memory takes forever sometimes
                reader = helper.ExecuteReader("Select id, data,typeID from ObjectInstance where typeID<>8");
                while (reader.Read())
                {
                    progressBar.EditValue = i;
                    Application.DoEvents();

                    byte[] data = reader[1] as byte[];
                    string id = reader[0] as string;
                    int typeID = (int)reader[2];
                    object ob = SQLHelper.BinaryToObject(data);
                    if (data != null)
                    {
                        switch (typeID)
                        {
                            case 2:
                                BillItem bi = ob as BillItem;
                                if (deleteCustomerBillRecords.ContainsKey(bi.customerBillID))
                                {
                                    deleteObjects[bi.customerBillID].Add(new Tuple<int,string>(typeID, id));
                                    delObjectCount++;
                                }
                                break;
                            default:
                                CustomerBillDocument doc = ob as CustomerBillDocument;
                                if (doc != null && deleteCustomerBillRecords.ContainsKey(doc.AccountDocumentID))
                                {
                                    deleteObjects[doc.AccountDocumentID].Add(new Tuple<int, string>(typeID, id));
                                    delObjectCount++;
                                }
                                break;
                        }
                        CustomerBillRecord customerBill = ob as CustomerBillRecord;
                        if (customerBill != null)
                        {
                            if (customerBill.paymentDocumentID != -1) // is paid
                            {
                                //delete the paid bill
                                helper.DeleteSingleTableRecrod<ObjectInstance>(helper.ReadDB, customerBill.id);
                            }
                        }
                    }
                    i++;
                }
                reader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (deleteObjects.Count>0)
            {
                progressBar.Position = 0;
                progressBar.Properties.Maximum = count;
                lblDescription.Text = "Preparing "+delObjectCount+" objects for deletion";
                
                i = 0;
                foreach (KeyValuePair<int, List<Tuple<int, string>>> objects in deleteObjects)
                {
                    try
                    {
                        foreach (Tuple<int, string> kv in objects.Value)
                        {
                            if (i % 100 == 0)
                            {
                                progressBar.EditValue = i;
                                Application.DoEvents();
                            }
                            helper.Insert("SalePoint", "ObjectToDelete", new string[] { "id", "typeID" }, new object[] { kv.Item2, kv.Item1 });
                            i++;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error " + ex.Message);
                    }
                }
                if (MessageBox.Show("Are you sure you want to delete about " + delObjectCount + " item. Please make sure you have uploaded all sales to the server before you proceed. Are you sure you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    lblDescription.Text = "Transfering objects that will be retained to ObjectInstance_Temp";
                    Application.DoEvents();
                    string sql;
                    sql = @"CREATE TABLE [dbo].[ObjectInstance_temp](
	[id] [varchar](50) NOT NULL,
	[typeID] [int] NOT NULL,
	[data] [image] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

insert into ObjectInstance_temp
Select * from ObjectInstance
where not exists (Select id from ObjectToDelete where id=ObjectInstance.id and typeID=ObjectInstance.typeID)
";
                    helper.ExecuteNonQuery(sql);

                    sql = @"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ObjectInstance]') AND type in (N'U'))
DROP TABLE [dbo].[ObjectInstance]


CREATE TABLE [dbo].[ObjectInstance](
	[id] [varchar](50) NOT NULL,
	[typeID] [int] NOT NULL,
	[data] [image] NOT NULL,
 CONSTRAINT [PK_ObjectTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[typeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
insert into ObjectInstance Select * from ObjectInstance_temp
";
                    helper.ExecuteNonQuery(sql);
                    lblDescription.Text = "Transfering objects back to ObjectInsance";
                    Application.DoEvents();
                }
                SetControlsState(true);
                lblDescription.Text = "Ready";
            }
        }

       

        private void SetControlsState(bool enabled)
        {
            btnStart.Enabled = btnClose.Enabled = enabled;
        }
    }
}
