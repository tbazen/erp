﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SalePointCleanUP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            InitializeFormSkinning();
            Application.Run(new MainForm());
        }
        private static void InitializeFormSkinning()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
            DevExpress.UserSkins.BonusSkins.Register();
        }
    }
}
