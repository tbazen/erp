using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using INTAPS.WSIS.Job;



namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {

        [MaintenanceJobMethod("Accounting: mergeItems")]
        public static void itemsMerge()
        {
            itemMerge_buildMap();
            iERPTransactionBDE berp = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int c;
            Console.WriteLine("Mapping {0} items", c = itemMerge_CodeMap.Count);

            string sql = @"SELECT distinct AccountTransaction.BatchID
FROM         Accounting_2006.dbo.AccountTransaction INNER JOIN
                      Accounting_2006.dbo.CostCenterAccount AS csa ON AccountTransaction.AccountID = csa.id INNER JOIN
                      Main_2006.dbo.TransactionItems AS ti ON csa.accountID IN (ti.expenseAccountID, ti.prePaidExpenseAccountID, ti.salesAccountID, ti.unearnedRevenueAccountID, 
                      ti.finishedWorkAccountID, ti.inventoryAccountID, ti.finishedGoodAccountID, ti.pendingOrderAccountID) INNER JOIN
                      Main_2006.dbo.ItemCategory ON ti.categoryID = Main_2006.dbo.ItemCategory.ID
where ti.Code='{0}' and tranTicks>={1} and tranTicks<{2}";

            Console.WriteLine();
            int AID = ApplicationServer.SecurityBDE.WriteExecuteAudit("admin", 1, "Maintenance job - Accounting: mergeItems", -1);
            int f = 0;
            DateTime t1 = DateTime.Parse("1/1/1990");
            DateTime t2 = DateTime.Parse("7/8/2014");
            foreach (KeyValuePair<string, string> kv in itemMerge_CodeMap)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                         ", c--, f);
                //TransactionItems oldItems = berp.GetTransactionItems(kv.Key);
                int[] docIDs = accounting.WriterHelper.GetColumnArray<int>(sql.format(kv.Key, t1.Ticks, t2.Ticks));
                if (docIDs.Length == 0)
                    continue;
                int cc;
                Console.WriteLine((cc = docIDs.Length) + " transaction");
                Console.WriteLine();


                AccountDocument[] docs = new AccountDocument[docIDs.Length];
                for (int i = 0; i < docs.Length; i++)
                    docs[i] = accounting.GetAccountDocument(docIDs[i], true);
                Array.Sort(docs, (x, y) => x.tranTicks.CompareTo(y.tranTicks));

                foreach (AccountDocument doc in docs)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                         ", cc--, f);
                    if (doc is AdjustmentDocument)
                        continue;

                    accounting.WriterHelper.BeginTransaction();
                    try
                    {
                        TransactionDocumentItem[] items = null;
                        if (doc is ItemDeliveryDocument
                            || doc is FixedAssetDepreciationDocument
                            || doc is InventoryDocument
                            || doc is ReturnToStoreDocument
                            || doc is StoreIssueDocument
                            || doc is StoreFinishedGoodsDocument
                            || doc is StoreTransferDocument
                            || doc is InternalServiceDocument
                            || doc is Purchase2Document
                            || doc is PurchaseInvoiceDocument
                            || doc is Sell2Document
                            )
                        {
                            items = doc.GetType().GetField("items").GetValue(doc) as TransactionDocumentItem[];
                        }
                        if (items == null)
                            throw new Exception("Invalid document type {0}".format(doc.GetType()));
                        bool mapped = false;
                        foreach (TransactionDocumentItem item in items)
                        {
                            if (item.code.Equals(kv.Key, StringComparison.CurrentCultureIgnoreCase))
                            {
                                mapped = true;
                                item.code = kv.Value;
                                item.unitID = 1;
                            }
                        }
                        if (mapped)
                        {
                            items = TransactionDocumentItem.uniquefy(items);
                            doc.GetType().GetField("items").SetValue(doc, items);
                            TaxImposed[] prev = null;
                            if (doc is Purchase2Document)
                            {
                                prev = ((Purchase2Document)doc).taxImposed;
                            }
                            doc.AccountDocumentID = accounting.PostGenericDocument(AID, doc);
                            if (doc is Purchase2Document)
                            {
                                TaxImposed[] cur = accounting.GetAccountDocument<Purchase2Document>(doc.AccountDocumentID).taxImposed;
                                if (!TaxImposed.equal(prev, cur))
                                    throw new ServerUserMessage("Tax value changes after post. Document ref:{0}", doc.PaperRef);
                            }
                            f++;
                        }
                        //berp.DeleteTransactionItem(AID, kv.Value);
                        accounting.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        accounting.WriterHelper.RollBackTransaction();
                        Console.WriteLine("Error processing doc ref:{0} doc id: {1} item {2}\n{3}", doc.PaperRef, doc.AccountDocumentID, kv.Key, ex.Message);
                        Console.WriteLine();
                    }
                    finally
                    {
                        accounting.WriterHelper.CheckTransactionIntegrityWithoutReadDB();
                    }
                }
            }
        }

        [MaintenanceJobMethod("Accounting: list purchase without delivery")]
        public static void listPurchaseWithoutDelivery()
        {
            iERPTransactionBDE berp = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int c;
            DateTime fromDate;
            DateTime toDate;
            string input;
            System.Console.Write("Enter lower time bound filter (Enter for no time filter):");
            while (true)
            {
                input = System.Console.ReadLine();
                if (DateTime.TryParse(input, out fromDate))
                    break;
                else
                    Console.Write("Enter valid date: ");
            }


            System.Console.Write("Enter upper exclusive time bound filter (Enter for no time filter):");
            while (true)
            {
                input = System.Console.ReadLine();
                if (DateTime.TryParse(input, out toDate))
                    break;
                else
                    Console.Write("Enter valid date: ");
            }
            AccountDocument[] docs = accounting.GetAccountDocuments(new DocumentSearchPar()
            {
                type = accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id
            }, null, 0, -1, out c);

            Console.WriteLine("Processing {0} purchase documents", c);
            Console.WriteLine();
            List<string> messages = new List<string>();
            CachedObject<int, CostCenterAccount> accounts = new CachedObject<int, CostCenterAccount>(
                (x) => (accounting.GetCostCenterAccount(x)));
            int pendingOrderAccountID = 216830;
            double totalTransaction = 0;
            foreach (Purchase2Document p in docs)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}                          ", c--);
                foreach(AccountTransaction t in accounting.GetTransactionsOfDocument(p.AccountDocumentID))
                    if (accounts[t.AccountID].accountID == pendingOrderAccountID)
                    {
                        totalTransaction += t.Amount;
                    }
                Dictionary<string, double> items = new Dictionary<string, double>();
                foreach (TransactionDocumentItem item in p.items)
                {
                    TransactionItems titem = berp.GetTransactionItems(item.code);
                    if (!titem.IsInventoryItem && !titem.IsFixedAssetItem)
                        continue;
                    if (item.directExpense)
                        continue;
                    if (items.ContainsKey(item.code.ToUpper()))
                    {
                        items[item.code.ToUpper()] += item.quantity;
                    }
                    else
                    {
                        items.Add(item.code.ToUpper(), item.quantity);
                    }
                }
                bool invalidDelivery = false;
                foreach (int deliveryID in p.deliverieIDs)
                {
                    foreach (AccountTransaction t in accounting.GetTransactionsOfDocument(deliveryID))
                        if (accounts[t.AccountID].accountID == pendingOrderAccountID)
                        {
                            totalTransaction += t.Amount;
                        }
                    PurchasedItemDeliveryDocument delivery = accounting.GetAccountDocument<PurchasedItemDeliveryDocument>(deliveryID);
                    if (delivery == null)
                    {
                        invalidDelivery = true;
                        messages.Add("Purcahse ref: {0} id:{2} has deleted delivery for delivery doc ID:{1}".format(p.PaperRef, deliveryID, p.AccountDocumentID));
                        break;

                    }
                    foreach (TransactionDocumentItem item in delivery.items)
                    {
                        if (items.ContainsKey(item.code.ToUpper()))
                        {
                            items[item.code.ToUpper()] -= item.quantity;
                        }
                        else
                        {
                            invalidDelivery = true;
                            messages.Add("Purcahse ref: {0} id:{2} has invalid delivery for delivery {1}".format(p.PaperRef, delivery.PaperRef, p.AccountDocumentID));
                            break;
                        }
                    }
                    if (invalidDelivery)
                        break;
                }
                if (invalidDelivery)
                    continue;
                foreach (KeyValuePair<string, double> kv in items)
                {
                    //if (!AccountBase.AmountEqual(kv.Value, 0))
                    //{
                    //    messages.Add("Purcahse ref: {0} id:{1} doesn't match its delivery".format(p.PaperRef, p.AccountDocumentID));
                    //    break;
                    //}
                    if (AccountBase.AmountLess(kv.Value, 0))
                    {
                        messages.Add("Purcahse ref: {0} id:{1} has more delivery".format(p.PaperRef, p.AccountDocumentID));
                        break;
                    }
                }
            }
            foreach (string m in messages)
            {
                Console.WriteLine(m);
            }
            Console.WriteLine("Total: {0}", totalTransaction);
        }


        [MaintenanceJobMethod("Accounting: list orphaned delivery")]
        public static void listOrphanedPurchaseWithoutDelivery()
        {
            iERPTransactionBDE berp = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int c;
            DateTime fromDate;
            DateTime toDate;
            string input;
            System.Console.Write("Enter lower time bound filter (Enter for no time filter):");
            while (true)
            {
                input = System.Console.ReadLine();
                if (DateTime.TryParse(input, out fromDate))
                    break;
                else
                    Console.Write("Enter valid date: ");
            }


            System.Console.Write("Enter upper exclusive time bound filter (Enter for no time filter):");
            while (true)
            {
                input = System.Console.ReadLine();
                if (DateTime.TryParse(input, out toDate))
                    break;
                else
                    Console.Write("Enter valid date: ");
            }
            AccountDocument[] docs = accounting.GetAccountDocuments(new DocumentSearchPar()
            {
                type = accounting.GetDocumentTypeByType(typeof(PurchasedItemDeliveryDocument)).id
            }, null, 0, -1, out c);

            Console.WriteLine("Processing {0} purchase documents", c);
            Console.WriteLine();
            List<string> messages = new List<string>();
            foreach (PurchasedItemDeliveryDocument delivery in docs)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}                          ", c--);
                Dictionary<string, double> items = new Dictionary<string, double>();
                foreach (TransactionDocumentItem item in delivery.items)
                {
                    TransactionItems titem = berp.GetTransactionItems(item.code);
                    if (!titem.IsInventoryItem && !titem.IsFixedAssetItem)
                        continue;
                    if (item.directExpense)
                        continue;
                    if (items.ContainsKey(item.code.ToUpper()))
                    {
                        items[item.code.ToUpper()] += item.quantity;
                    }
                    else
                    {
                        items.Add(item.code.ToUpper(), item.quantity);
                    }
                }
                bool invalidPurchase = false;
                Purchase2Document purchase = accounting.GetAccountDocument<Purchase2Document>(delivery.purchaseDocumentID);
                    if (purchase == null)
                    {
                        invalidPurchase = true;
                        messages.Add("Delivery ref: {0} id:{2} has deleted purchase for delivery doc ID:{1}".format(delivery.PaperRef, delivery.purchaseDocumentID, delivery.AccountDocumentID));
                        break;
                    }
                    foreach (TransactionDocumentItem item in purchase.items)
                    {
                        if (items.ContainsKey(item.code.ToUpper()))
                        {
                            items[item.code.ToUpper()] -= item.quantity;
                        }
                        else
                        {
                            invalidPurchase = true;
                            messages.Add("Purcahse ref: {0} id:{2} has invalid delivery for delivery {1}".format(delivery.PaperRef, purchase.PaperRef, delivery.AccountDocumentID));
                            break;
                        }
                    }
                    
                if (invalidPurchase)
                    continue;
                foreach (KeyValuePair<string, double> kv in items)
                {
                    if (!AccountBase.AmountEqual(kv.Value, 0))
                    {
                        messages.Add("Purcahse ref: {0} id:{1} doesn't match its delivery".format(delivery.PaperRef, delivery.AccountDocumentID));
                        break;
                    }
                    //if (AccountBase.AmountLess(kv.Value, 0))
                    //{
                    //    messages.Add("Purcahse ref: {0} id:{1} has more delivery".format(p.PaperRef, p.AccountDocumentID));
                    //    break;
                    //}
                }
            }
            foreach (string m in messages)
            {
                Console.WriteLine(m);
            }
        }


        [MaintenanceJobMethod("Billing - Adama: list transactions that affect cash and receivable accounts")]
        public static void listBillDoubleCount()
        {
            iERPTransactionBDE berp = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int c;
            DateTime fromDate;
            DateTime toDate;
            string input;
            System.Console.Write("Enter lower time bound filter (Enter for no time filter):");
            while (true)
            {
                input = System.Console.ReadLine();
                if (DateTime.TryParse(input, out fromDate))
                    break;
                else
                    Console.Write("Enter valid date: ");
            }


            System.Console.Write("Enter upper exclusive time bound filter (Enter for no time filter):");
            while (true)
            {
                input = System.Console.ReadLine();
                if (DateTime.TryParse(input, out toDate))
                    break;
                else
                    Console.Write("Enter valid date: ");
            }
            string sql = @"SELECT batchID
  FROM [Accounting_2006].[dbo].[AccountTransaction] t
  inner join Accounting_2006.dbo.CostCenterAccount csa
  on t.AccountID=csa.id
  where csa.accountID in ({0}) and tranTicks>={1} and tranTicks<{2}";

            string accountIDs = null;
            foreach (BankAccountInfo b in berp.GetAllBankAccounts())
            {
                accountIDs = INTAPS.StringExtensions.AppendOperand(accountIDs, ",", accounting.GetCostCenterAccount(b.mainCsAccount).accountID.ToString());
            }

            foreach (CashAccount ca in berp.GetAllCashAccounts(true))
            {
                accountIDs = INTAPS.StringExtensions.AppendOperand(accountIDs, ",", accounting.GetCostCenterAccount(ca.csAccountID).accountID.ToString());
            }
            int[] docIDs = accounting.WriterHelper.GetColumnArray<int>(sql.format(accountIDs, fromDate.Ticks, toDate.Ticks));

            Console.WriteLine("Processing {0} documents", c = docIDs.Length);
            Console.WriteLine();
            HashSet<int> checkedDocs = new HashSet<int>();
            HashSet<int> receivableAccounts = new HashSet<int>();
            receivableAccounts.Add(216862);
            receivableAccounts.Add(217629);
            receivableAccounts.Add(218134);
            receivableAccounts.Add(218243);
            receivableAccounts.Add(218244);
            receivableAccounts.Add(218245);
            receivableAccounts.Add(276651);
            receivableAccounts.Add(276656);
            receivableAccounts.Add(276657);
            receivableAccounts.Add(276658);
            receivableAccounts.Add(276661);

            List<string> messages = new List<string>();
            CachedObject<int, CostCenterAccount> css = new CachedObject<int, CostCenterAccount>(
                (x) => accounting.GetCostCenterAccount(x));
            int f = 0;
            double totalIncome = 0;
            foreach (int p in docIDs)
            {

                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                          ", c--, f);
                if (checkedDocs.Contains(p))
                    continue;
                checkedDocs.Add(p);
                TransactionOfBatch[] batch = accounting.GetTransactionsOfDocument(p);

                bool found = false;
                foreach (TransactionOfBatch tb in batch)
                {
                    CostCenterAccount thisCsa = css[tb.AccountID];
                    if (receivableAccounts.Contains(thisCsa.accountID))
                    {
                        found = true;
                        totalIncome += tb.Amount;
                    }
                }

                if (found)
                {
                    Sell2Document sells = accounting.GetAccountDocument<Sell2Document>(p);
                    if (sells != null)
                    {

                        accounting.WriterHelper.BeginTransaction();
                        try
                        {
                            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job-Billing - Adama: list transactions that affect cash and receivable accounts", "", -1);
                            if (sells.items.Length != 1)
                                throw new Exception("{0} items found or quanity not 1".format(sells.items.Length));
                            CustomerCreditSettlement2Document s = new CustomerCreditSettlement2Document();
                            s.ShortDescription = sells.ShortDescription;
                            s.salesVoucher = sells.salesVoucher;
                            s.DocumentDate = sells.DocumentDate;
                            s.items = new TransactionDocumentItem[]
                            {
                                new TransactionDocumentItem()
                                {
                                    code="028035",
                                    quantity=sells.items[0].quantity,
                                    unitPrice=sells.items[0].unitPrice,
                                    price=sells.items[0].quantity*sells.items[0].unitPrice,
                                    costCenterID=sells.items[0].costCenterID,
                                }
                            };
                            s.paymentMethod = sells.paymentMethod;
                            s.checkNumber = sells.checkNumber;
                            s.transferReference = sells.transferReference;
                            s.cpoNumber = sells.cpoNumber;
                            s.assetAccountID = sells.assetAccountID;
                            s.serviceChargeAmount = sells.serviceChargeAmount;
                            s.serviceChargePayer = sells.serviceChargePayer;
                            s.relationCode = sells.relationCode;
                            s.PaperRef = sells.PaperRef;
                            s.amount = sells.amount;
                            s.taxImposed = new TaxImposed[0];
                            s.paidAmount = sells.paidAmount;
                            accounting.PostGenericDocument(AID, s);
                            accounting.DeleteGenericDocument(AID, p);
                            Console.WriteLine("Done ref:{0} \t\tdoc id: {1}\n".format(sells.PaperRef, p));
                            accounting.WriterHelper.CommitTransaction();

                        }
                        catch (Exception ex)
                        {
                            accounting.WriterHelper.RollBackTransaction();
                            messages.Add("Doc ref:{0} \t\tdoc id: {1} \n{2}".format(accounting.GetAccountDocument(p, false).PaperRef, p, ex.Message));
                        }
                        finally
                        {
                            accounting.WriterHelper.CheckTransactionIntegrityWithoutReadDB();
                        }
                    }
                    else
                        messages.Add("Doc ref:{0} \t\tdoc id: {1} ".format(accounting.GetAccountDocument(p, false).PaperRef, p));
                    f++;
                }
            }
            int n = 1;
            System.IO.StreamWriter fs = System.IO.File.CreateText("DocumentList.txt");
            foreach (string m in messages)
            {
                string line = "{0}: {1}".format(n++, m);
                Console.WriteLine(line);
                fs.WriteLine(line);
            }
            fs.Close();
            Console.WriteLine("Total income: {0}", totalIncome);
        }
        [MaintenanceJobMethod("Billing - Adama: list invlid void job entries")]
        public static void adama_listInvlidVoidJobEntries()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            JobManagerBDE jobbde = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;
            string sql = @"Select id from JobManager.dbo.JobData where applicationType=10 and status=9";
            int[] jobs = jobbde.WriterHelper.GetColumnArray<int>(sql);
            Console.WriteLine(jobs.Length + " void receipt jobs found");
            foreach (int jobID in jobs)
            {
                VoidReceiptData v = jobbde.getWorkFlowData(jobID, 10, 0, true) as VoidReceiptData;
                AccountDocument doc;
                if ((doc = bdesub.Accounting.GetAccountDocument(v.receitDocumentID, false)) != null)
                {
                    Console.WriteLine("DocID:{0}\tRef:{1}\t\tJob No:{2}", v.receitDocumentID, doc.PaperRef, jobbde.GetJob(jobID).jobNo);
                }
            }
            sql = @"Select ID from Accounting_2006.dbo.Document where DocumentTypeID=171
and ID not in (Select paymentDocumentID from Subscriber.dbo.CustomerBill)";
            int[] receipts = jobbde.WriterHelper.GetColumnArray<int>(sql);
            Console.WriteLine(receipts.Length + " invlid receipts found");
            int n = 1;
            foreach (int rid in receipts)
            {
                CustomerPaymentReceipt r = bdesub.Accounting.GetAccountDocument<CustomerPaymentReceipt>(rid);
                foreach (int billID in r.settledBills)
                {
                    CustomerBillRecord bill = bdesub.getCustomerBillRecord(billID);
                    Console.WriteLine("{4}:\tReceipt ID:{0}({1})\t\tBillID:{2}\t\tBill ReceiptID:{3}", r.assetAccountID, r.PaperRef,
                        billID, bill==null?"Invalid bill ID":bill.paymentDocumentID.ToString(),n);
                }
                n++;
            }
        }
        [MaintenanceJobMethod("Billing - Adama: fix missing sales transaction entries")]
        public static void adama_fixReceiptEntries()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int[] paymentDocIDs = bdesub.WriterHelper.GetColumnArray<int>(@"SELECT    c.paymentDocumentID,SUM(price-settledFromDepositAmount)
FROM         Subscriber.dbo.CustomerBill AS c INNER JOIN
                      Subscriber.dbo.CustomerBillItem ON c.id = CustomerBillItem.customerBillID
WHERE     
(c.paymentDocumentID <> - 1) 
and c.paymentDocumentID not in (Select BatchID from Accounting_2006.dbo.AccountTransaction)
group by c.paymentDocumentID,c.paymentDate
having SUM(price-settledFromDepositAmount)>0.00001");
            Console.WriteLine(paymentDocIDs.Length + " documents found");
            Console.WriteLine();
            int c = paymentDocIDs.Length;

            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job-Billing - Adama: fix missing sales transaction entries", "", -1);
            bdesub.WriterHelper.setReadDB(bdesub.Accounting.DBName);
            bdesub.WriterHelper.BeginTransaction();
            try
            {
                foreach (int p in paymentDocIDs)
                {
                    CustomerPaymentReceipt receipt = bdesub.Accounting.GetAccountDocument<CustomerPaymentReceipt>(p);
                    AccountTransaction[] tran = bdesub.Accounting.GetTransactionsOfDocument(receipt.AccountDocumentID);

                    double itemTotal = 0;
                    int nItems = 0;
                    foreach (int billID in receipt.settledBills)
                    {
                        BillItem[] items = bdesub.getCustomerBillItems(billID);
                        nItems += items.Length;
                        foreach (BillItem bi in items) itemTotal += bi.price - bi.settledFromDepositAmount;
                    }
                    Console.WriteLine("Receipt ID:{0}\tNTran:{1}\tTotal:{2}\tNBI:{3}", receipt.AccountDocumentID, tran.Length, itemTotal, nItems);
                    PaymentCenter pc = bdesub.GetPaymentCenterByCashAccount(receipt.assetAccountID);
                    
                    TransactionOfBatch[] newTran = new TransactionOfBatch[]
                    {
                        new TransactionOfBatch(receipt.assetAccountID,itemTotal,"Empty receipt transaction problem fix")
                        ,new TransactionOfBatch(bdesub.Accounting.GetCostCenterAccountID(bdesub.bERP.SysPars.mainCostCenterID,receipt.customer.accountID),-itemTotal,"Empty receipt transaction problem fix")
                        ,new TransactionOfBatch(pc.summaryAccountID,itemTotal,"Empty receipt transaction problem fix")
                        //,new TransactionOfBatch(bdesub.Accounting.GetCostCenterAccountID(bdesub.bERP.SysPars.mainCostCenterID,bdesub.SysPars.receivableAccountSummary),-itemTotal,"Empty receipt transaction problem fix")
                    };
                   bdesub.Accounting.RecordTransactionInternal(AID, receipt, receipt.DocumentDate, newTran, true);
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                throw ex;
            }
        }
        [MaintenanceJobMethod("Billing - Adama: move meter rent")]
        public static void fixMeterRentProblem()
        {
            iERPTransactionBDE berp = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int c;
            Console.WriteLine("Mapping {0} items", c = itemMerge_CodeMap.Count);

            string sql = @"SELECT id from Accounting_2006.dbo.Document
            where tranTicks>={0} and tranTicks<{1} and documentTypeID=146";

            Console.WriteLine();
            int AID = ApplicationServer.SecurityBDE.WriteExecuteAudit("admin", 1, "Maintenance job - Billing - Adama: move meter rent", -1);
            int f = 0;
            DateTime t1 = DateTime.Parse("1/1/1990");
            DateTime t2 = DateTime.Parse("7/8/2014");

            int[] docIDs = accounting.WriterHelper.GetColumnArray<int>(sql.format(t1.Ticks, t2.Ticks));
            int cc;
            Console.WriteLine((cc = docIDs.Length) + " transaction");
            Console.WriteLine();


            AccountDocument[] docs = new AccountDocument[docIDs.Length];
            for (int i = 0; i < docs.Length; i++)
                docs[i] = accounting.GetAccountDocument(docIDs[i], true);
            Array.Sort(docs, (x, y) => x.tranTicks.CompareTo(y.tranTicks));

            foreach (AccountDocument doc in docs)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}\t{2}                         ", cc--, f,doc.PaperRef);
                if (doc is AdjustmentDocument)
                    continue;
                Sell2Document sell = doc as Sell2Document;
                if (!sell.relationCode.Equals("20005"))
                    continue;
                accounting.WriterHelper.BeginTransaction();
                try
                {
                    if (sell.items.Length != 1)
                        throw new Exception("Invalid item lengt {0}".format(sell.items.Length));
                    sell.items[0].code = "134001";
                    doc.AccountDocumentID = accounting.PostGenericDocument(AID, doc);
                    f++;
                    
                    accounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    accounting.WriterHelper.RollBackTransaction();
                    Console.WriteLine("Error processing doc ref:{0} doc id: {1} item \n{2}", doc.PaperRef, doc.AccountDocumentID, ex.Message);
                    Console.WriteLine();
                }
                finally
                {
                    accounting.WriterHelper.CheckTransactionIntegrityWithoutReadDB();
                }
            }
        }
    }
}
