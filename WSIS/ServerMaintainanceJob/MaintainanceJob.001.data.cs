using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using INTAPS.WSIS.Job;



namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {
        static Dictionary<string, string> itemMerge_CodeMap = new Dictionary<string, string>();
        static void itemMerge_buildMap() 
        {
            //itemMerge_CodeMap.Add("022002002", "121004");
            //itemMerge_CodeMap.Add("022003003", "121001");
            //itemMerge_CodeMap.Add("022003012", "121002");
            //itemMerge_CodeMap.Add("022003018", "121003");
            //itemMerge_CodeMap.Add("028011", "028002");
            //itemMerge_CodeMap.Add("028012", "028001");
            //itemMerge_CodeMap.Add("028020", "028021");
            itemMerge_CodeMap.Add("028002","133001");
            itemMerge_CodeMap.Add("028001", "132001");
            itemMerge_CodeMap.Add("028021", "134001");
        }
    }
}
