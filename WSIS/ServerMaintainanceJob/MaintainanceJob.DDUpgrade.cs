using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using INTAPS.WSIS.Job;



namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {

        [MaintenanceJobMethod("Clear Data: Items, Trade Relations, Employees, Projects")]
        public static void clearAccountDependencies()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            PayrollBDE payroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

            int N;
            int AID;
            BIZNET.iERP.TradeRelation[] custs = bde.SearchCustomers(0, -1, new object[] { }, new string[] { }, out N);
            int c = custs.Length;
            Console.WriteLine(c + " customers found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TradeRelation cust in custs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteRelation(AID, cust.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;

            }
            custs = bde.SearchSuppliers(0, -1, new object[] { }, new string[] { }, out N);
            c = custs.Length;
            Console.WriteLine(c + " suppliers found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TradeRelation cust in custs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteRelation(AID, cust.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }


            BIZNET.iERP.TransactionItems[] items = bde.SearchTransactionItems(0, -1, new object[0], new string[0], out N);
            c = items.Length;
            Console.WriteLine(c + " items found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TransactionItems item in items)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteTransactionItem(AID, item.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }


            BankAccountInfo[] bas = bde.GetAllBankAccounts();
            c = bas.Length;
            Console.WriteLine(c + " bank accounts found");
            Console.WriteLine();
            foreach (BIZNET.iERP.BankAccountInfo b in bas)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteBankAccount(AID, b.mainCsAccount);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }

            CashAccount[] cas = bde.GetAllCashAccounts(false);
            c = cas.Length;
            Console.WriteLine(c + " cash accounts found");
            Console.WriteLine();
            foreach (BIZNET.iERP.CashAccount ca in cas)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteCashAccount(AID, ca.code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }
            Employee[] es = payroll.GetEmployees(new AppliesToEmployees(true));
            c = es.Length;
            Console.WriteLine(c + " employees found");
            Console.WriteLine();
            foreach (Employee e in es)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    payroll.DeleteEmployee(AID, e.id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                c--;
            }

            int[] parentAccounts = new int[]
                        {
                            payroll.SysPars.staffLongTermLoanAccountID
                            ,payroll.SysPars.staffSalaryAdvanceAccountID
                            ,payroll.SysPars.staffExpenseAdvanceAccountID
                            ,payroll.SysPars.UnclaimedSalaryAccount
                            ,payroll.SysPars.staffIncomeTaxAccountID
                            ,payroll.SysPars.staffPensionPayableAccountID
                            ,payroll.SysPars.staffCostSharingPayableAccountID
                            ,payroll.SysPars.LoanFromStaffAccountID
                            ,payroll.SysPars.OwnersEquityAccount
                            ,payroll.SysPars.staffPerDiemCostAccountID
                            ,payroll.SysPars.staffPerDiemAccountID
                            ,payroll.SysPars.PermanentPayrollCostAccount
                            ,payroll.SysPars.TemporaryPayrollCostAccount
                            ,payroll.SysPars.PermanentPayrollExpenseAccount
                            ,payroll.SysPars.TemporaryPayrollExpenseAccount
                            ,payroll.SysPars.ShareHoldersLoanAccountID
                        };
            Console.WriteLine();
            foreach (int parAc in parentAccounts)
            {
                if (parAc == -1)
                    continue;
                foreach (Account child in accounting.GetChildAcccount<Account>(parAc, 0, -1, out N))
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                       ");
                    try
                    {
                        AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                        accounting.DeleteAccount<Account>(AID, child.id, true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    c++;
                }
            }
            Project[] prjs = bde.GetAllProjects();
            c = prjs.Length;
            Console.WriteLine(c + " projects found");
            Console.WriteLine();
            foreach (BIZNET.iERP.Project p in prjs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    foreach (int e in p.projectData.employees)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveEmployeeFromProject(AID, e, p.code);

                        }
                        catch { }
                    }
                    foreach (string storeCode in p.projectData.stores)
                    {
                        StoreInfo s = bde.GetStoreInfo(storeCode);
                        if (s != null)
                        {
                            try
                            {
                                AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                                bde.RemoveStoreFromProject(AID, s.costCenterID, storeCode, p.code);
                            }
                            catch { }
                        }
                    }
                    foreach (int m in p.projectData.machinaryAndVehicles)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveMachinaryAndVehicleFromProject(AID, m, p.code);
                        }
                        catch { }
                    }
                    foreach (int div in p.projectData.projectDivisions)
                    {
                        try
                        {
                            AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                            bde.RemoveDivisionFromProject(AID, div, p.code);
                        }
                        catch { }
                    }
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteProject(AID, p.code);

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                c--;
            }

        }

        [MaintenanceJobMethod("Integrity Test: Document Xml Data Deserialization Test")]
        public static void integrityTestDocXML()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int[] docs = accounting.WriterHelper.GetColumnArray<int>(
                string.Format("Select ID from {0}.dbo.Document", accounting.DBName), 0);
            int c = docs.Length;
            Console.WriteLine("Processing " + c + " documents");
            Console.WriteLine();
            foreach (int docID in docs)
            {
                Console.CursorTop--;
                Console.WriteLine((c--) + "                  ");

                try
                {
                    DataTable dat = accounting.WriterHelper.GetDataTable(string.Format("Select DocumentData,DocumentTypeID from {0}.dbo.Document where ID={1}", accounting.DBName, docID));
                    string xml = dat.Rows[0][0] as string;
                    int typeID = (int)dat.Rows[0][1];
                    AccountDocument.DeserializeXML(xml, accounting.GetDocumentTypeByID(typeID).GetTypeObject());
                }
                catch (Exception ex)
                {
                    AccountDocument doc = accounting.GetAccountDocument(docID, false);
                    Console.WriteLine("Error deleting " + docID + ".\n" + ex.Message);
                    try
                    {
                        Console.WriteLine(string.Format("Ref: {0} ID: {1}", doc.PaperRef, doc.AccountDocumentID));
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Additional info couldn't be retrieved.\n" + ex2.Message);
                    }
                    Console.ReadLine();
                }

            }

        }
        const string legalChar = @"[\w\s\d\\\/\._\-\(\)\&]";
        static char[] replacebleList = new char[] { '[', ']', '{', '}' };
        static char[] replacementList = new char[] { '(', ')', '(', ')' };
        static Regex regEx = new Regex(legalChar, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        static List<string> illegalList = new List<string>();
        static string legalizeString(string str)
        {

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < str.Length; i++)
            {

                char theChar = str[i];
                if (theChar < 32)
                {
                    sb.Append(' ');
                    continue;
                }
                bool replaced = false;
                for (int j = 0; j < replacebleList.Length; j++)
                {

                    if (replacebleList[j] == theChar)
                    {
                        sb.Append(replacementList[j]);
                        replaced = true;
                        break;
                    }
                }
                if (replaced)
                    continue;

                string ch = str.Substring(i, 1);
                if (regEx.Match(ch).Success)
                {
                    sb.Append(ch);
                }
                else
                {
                    if (!illegalList.Contains(ch))
                        illegalList.Add(ch);
                    sb.Append("_");
                }
            }
            return sb.ToString();
        }
        [MaintenanceJobMethod("Bishoftu: Fix Illegal Characters in Subscriber database")]
        public static void bishoftuFixIllegalCharactersFromSubscriberDB()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            DataTable table = bdesub.WriterHelper.GetDataTable(string.Format("Select id,name,customerCode,kebele from {0}.dbo.Subscriber", bdesub.DBName));
            int c = table.Rows.Count;
            int nfixed = 0;
            Console.WriteLine(c + " subscribers found");
            Console.WriteLine();
            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Fix Illegal Characters", "", -1);
            try
            {
                bdesub.WriterHelper.BeginTransaction();
                foreach (DataRow row in table.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "\t" + nfixed + "                       ");

                    int id = (int)row[0];
                    string name = (string)row[1];
                    string customerCode = (string)row[2];
                    int kebele = (int)row[3];
                    int lkebele = (bdesub.GetKebele(kebele) == null) ? -1 : kebele;

                    string lName = legalizeString(name);
                    string lCustomerCode = legalizeString(customerCode);
                    if (name.Equals(lName) && customerCode.Equals(lCustomerCode) && lkebele == kebele)
                        continue;
                    bdesub.WriterHelper.Update(bdesub.DBName, "Subscriber", new string[] { "name", "customerCode", "kebele", "__AID" }, new object[] { lName, lCustomerCode, lkebele, AID }, "id=" + id);
                    bdesub.WriterHelper.logDeletedData(AID, bdesub.DBName + ".dbo.Subscriber", "id=" + id);
                    nfixed++;
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }

            table = bdesub.WriterHelper.GetDataTable(string.Format("Select id,ticksFrom,contractNo,kebele from {0}.dbo.Subscription", bdesub.DBName));
            c = table.Rows.Count;
            nfixed = 0;
            Console.WriteLine(c + " connections found");
            Console.WriteLine();
            try
            {
                bdesub.WriterHelper.BeginTransaction();
                foreach (DataRow row in table.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "\t" + nfixed + "                       ");

                    int id = (int)row[0];
                    long ticksFrom = (long)row[1];
                    string contractNo = (string)row[2];
                    int kebele = (int)row[3];
                    string lcontractNo = legalizeString(contractNo);
                    int lkebele = (bdesub.GetKebele(kebele) == null) ? -1 : kebele;
                    if (contractNo.Equals(lcontractNo))
                        continue;

                    string cr = "id=" + id + " and ticksFrom=" + ticksFrom;
                    bdesub.WriterHelper.Update(bdesub.DBName, "Subscription", new string[] { "contractNo", "kebele", "__AID" }, new object[] { lcontractNo, lkebele, AID }, cr);
                    bdesub.WriterHelper.logDeletedData(AID, bdesub.DBName + ".dbo.Subscription", cr);
                    nfixed++;
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }
        }
        [MaintenanceJobMethod("Bishoftu: Fix Illegal Characters in Subscriber database for 'House No/Address' Field")]
        public static void bishoftuFixIllegalCharactersHouseNoFieldFromSubscriberDB()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            DataTable table = bdesub.WriterHelper.GetDataTable(string.Format("Select id,address from {0}.dbo.Subscriber where address is not null", bdesub.DBName));
            int c = table.Rows.Count;
            int nfixed = 0;
            Console.WriteLine(c + " subscribers found");
            Console.WriteLine();
            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Fix Illegal Characters(House No/Address Field)", "", -1);
            try
            {
                bdesub.WriterHelper.BeginTransaction();
                foreach (DataRow row in table.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "\t" + nfixed + "                       ");

                    int id = (int)row[0];
                    string houseNo = (string)row[1];
                    //  string customerCode = (string)row[2];
                    //int kebele = (int)row[3];
                    //int lkebele = (bdesub.GetKebele(kebele) == null) ? -1 : kebele;

                    string lHouseNo = legalizeString(houseNo);
                    // string lCustomerCode = legalizeString(customerCode);
                    if (houseNo.Equals(lHouseNo))
                        continue;
                    bdesub.WriterHelper.Update(bdesub.DBName, "Subscriber", new string[] { "address", "__AID" }, new object[] { lHouseNo, AID }, "id=" + id);
                    bdesub.WriterHelper.logDeletedData(AID, bdesub.DBName + ".dbo.Subscriber", "id=" + id);
                    nfixed++;
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }

            #region Subscription data
            table = bdesub.WriterHelper.GetDataTable(string.Format("Select id,ticksFrom,address from {0}.dbo.Subscription where address is not null", bdesub.DBName));
            c = table.Rows.Count;
            nfixed = 0;
            Console.WriteLine(c + " connections found");
            Console.WriteLine();
            try
            {
                bdesub.WriterHelper.BeginTransaction();
                foreach (DataRow row in table.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "\t" + nfixed + "                       ");

                    int id = (int)row[0];
                    long ticksFrom = (long)row[1];
                    string address = (string)row[2];
                    string lAddress = legalizeString(address);
                    if (address.Equals(lAddress))
                        continue;

                    string cr = "id=" + id + " and ticksFrom=" + ticksFrom;
                    bdesub.WriterHelper.Update(bdesub.DBName, "Subscription", new string[] { "address", "__AID" }, new object[] { lAddress, AID }, cr);
                    bdesub.WriterHelper.logDeletedData(AID, bdesub.DBName + ".dbo.Subscription", cr);
                    nfixed++;
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }
            #endregion
        }

        [MaintenanceJobMethod("Bishoftu: Fix Illegal Characters in Document XML")]
        public static void bishoftuFixIllegalCharacters()
        {
            // create a compiled instance of the Regular Expression for the chars we would like to replace 
            // add all char points beween 0 and 31 excluding the allowed white spaces (9=TAB, 10=LF, 13=CR) 
            StringBuilder RegExp = new StringBuilder("&#(0");
            for (int i = 1; i <= 31; i++)
            {
                // ignore allowed white spaces 
                if (i == 9 || i == 10 || i == 13) continue;

                // add other control characters 
                RegExp.Append("|");
                RegExp.Append(i.ToString());

                // add hex representation as well 
                RegExp.Append("|x");
                RegExp.Append(i.ToString("x"));
            }
            RegExp.Append(");");
            string strRegExp = RegExp.ToString();

            // create regular expression assembly  
            Regex regEx = new Regex(strRegExp, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int[] docs = accounting.WriterHelper.GetColumnArray<int>(
                string.Format("Select ID from {0}.dbo.Document where DocumentTypeID in (4,5,12)", accounting.DBName), 0);
            int c = docs.Length;
            Console.WriteLine("Processing " + c + " documents");
            Console.WriteLine();

            foreach (int docID in docs)
            {
                Console.CursorTop--;
                Console.WriteLine((c--) + "                  ");
                try
                {
                    DataTable dat = accounting.WriterHelper.GetDataTable(string.Format("Select DocumentData,DocumentTypeID from {0}.dbo.Document where ID={1}", accounting.DBName, docID));
                    string xml = dat.Rows[0][0] as string;
                    int typeID = (int)dat.Rows[0][1];
                    string fixedXml = xml.Replace("&#x0;", "");
                    if (!fixedXml.Equals(xml))
                    {
                        Console.WriteLine("Fixed ID: {0}", docID);
                        accounting.WriterHelper.Update(accounting.DBName, "Document", new string[] { "DocumentData" }, new object[] { fixedXml }, "ID=" + docID);
                        AccountDocument.DeserializeXML(fixedXml, accounting.GetDocumentTypeByID(typeID).GetTypeObject());
                    }

                }
                catch (Exception ex)
                {
                    AccountDocument doc = accounting.GetAccountDocument(docID, false);
                    Console.WriteLine("Error fixing " + docID + ".\n" + ex.Message);
                    ex = ex.InnerException;
                    while (ex != null)
                    {
                        Console.WriteLine(ex.Message);
                        ex = ex.InnerException;
                    }
                    try
                    {
                        Console.WriteLine(string.Format("Ref: {0} ID: {1}", doc.PaperRef, doc.AccountDocumentID));
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Additional info couldn't be retrieved.\n" + ex2.Message);
                    }
                    Console.ReadLine();
                }
            }

            DataTable dtSubsc = accounting.WriterHelper.GetDataTable("Select ID,Name from Subscriber.dbo.Subscriber");
            c = dtSubsc.Rows.Count;
            Console.WriteLine("Processing " + c + " customers");
            Console.WriteLine();
            foreach (DataRow row in dtSubsc.Rows)
            {
                Console.CursorTop--;
                Console.WriteLine((c--) + "                  ");
                int custID = (int)row[0];
                string name = row[1] as string;
                try
                {
                    string fixedName = name.Replace((char)0, ' ');
                    if (!fixedName.Equals(name))
                    {
                        Console.WriteLine("Fix " + custID + ": " + fixedName + "  " + name);

                        accounting.WriterHelper.Update("Subscriber", "Subscriber", new string[] { "Name" }, new object[] { fixedName }, "ID=" + custID);
                        Console.WriteLine("Fixed");

                    }
                }
                catch (Exception ex)
                {
                    AccountDocument doc = accounting.GetAccountDocument(custID, false);
                    Console.WriteLine("Error updating " + custID + ".\n" + ex.Message);
                }

            }
        }

        [MaintenanceJobMethod("Clear Data: All Customers")]
        public static void deleteAllCustomers()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Deleting Customers");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscriber[] customers = bdesub.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);
                Console.WriteLine(customers.Length + " customers found");
                Console.WriteLine();
                int c = customers.Length;
                foreach (Subscriber customer in customers)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        bdesub.DeleteSubscriber(-1, customer.id);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Upgrade DD: set customer code")]
        public static void ddupgradeSetCustomerCodes()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Setting Customer Code");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscriber[] customers = bdesub.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);
                Console.WriteLine(customers.Length + " customers found");
                Console.WriteLine();
                int c = customers.Length;
                int ccode = 1;
                foreach (Subscriber customer in customers)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        customer.customerCode = ccode.ToString("000000");
                        bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, customer);
                        ccode++;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Upgrade DD: transfer outstanding bills")]
        public static void ddupgradeTransferOutstandingBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing bills");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscriber[] customers = bdesub.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);
                Console.WriteLine(customers.Length + " customers found");
                Console.WriteLine();
                int c = customers.Length;
                SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
                SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));
                int sysRefTypeID = bdesub.Accounting.getDocumentSerialType("JV").id;
                int adjustNumber = 1;
                foreach (Subscriber customer in customers)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {
                            customer.accountID = bdesub.getOrCreateCustomerAccountID(1, customer);
                            Subscription[] newSubscriptions = bdesub.GetSubscriptions(customer.id, DateTime.Now.Ticks);
                            List<WSIS1.BillRecord> allBills = new List<WSIS1.BillRecord>();
                            double totalBillDebit = 0;
                            double totalLedgerDebit = 0;
                            foreach (Subscription s in newSubscriptions)
                            {
                                WSIS1.Subscription oldSubsc = oldCustomerDB.GetSTRArrayByFilter<WSIS1.Subscription>("id=" + s.id)[0];
                                WSIS1.AccountBalance[] bal = oldAcountingDB.GetSTRArrayByFilter<WSIS1.AccountBalance>("itemID=0 and accountID=" + oldSubsc.accountID);
                                if (bal.Length > 0)
                                    totalLedgerDebit += bal[0].Balance;

                                foreach (WSIS1.WaterBill bill in (oldCustomerDB.GetSTRArrayByFilter<WSIS1.WaterBill>("accountDocumentID<>-1 and ( payDocumentID=-1 or  payDocumentID=accountDocumentID) and subscriptionID=" + s.id)))
                                {
                                    totalBillDebit += bill.Total;
                                    BillPeriod period = bdesub.GetBillPeriod(bill.periodID);
                                    CustomerBillRecord billRecord = new CustomerBillRecord();
                                    billRecord.customerID = customer.id;
                                    billRecord.connectionID = s.id;
                                    billRecord.billDate = bill.postDate;
                                    billRecord.billDocumentTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
                                    billRecord.draft = false;
                                    billRecord.paymentDiffered = bill.payDocumentID == bill.accountDocumentID;
                                    billRecord.paymentDocumentID = -1;
                                    billRecord.periodID = bill.periodID;
                                    billRecord.postDate = bill.postDate;
                                    BWFMeterReading[] readings = bdesub.WriterHelper.GetSTRArrayByFilter<BWFMeterReading>("periodID=" + bill.periodID + " and subscriptionID=" + s.id);

                                    List<BillItem> items = new List<BillItem>();
                                    if (!Accounting.AccountBase.AmountEqual(bill.consumptionFee, 0))
                                    {
                                        string readingDesc = "";
                                        if (readings.Length == 1)
                                        {
                                            switch (readings[0].readingType)
                                            {
                                                case MeterReadingType.MeterReset:
                                                    readingDesc = "Reading: " + Math.Round(readings[0].reading, 0) + " Use: " + Math.Round(readings[0].consumption, 0) + " (Meter reset)";
                                                    break;
                                                case MeterReadingType.Average:
                                                    readingDesc = "Reading: " + Math.Round(readings[0].reading, 0) + " Use: " + Math.Round(readings[0].consumption, 0) + " (Average)";
                                                    break;
                                                default:
                                                    readingDesc = Accounting.AccountBase.AmountLess(readings[0].consumption, 0) ? "(Negative)" :
                                                                    ("Reading: " + Math.Round(readings[0].reading, 0) + " Use: " + Math.Round(readings[0].consumption, 0));
                                                    break;
                                            }
                                        }
                                        BillItem bi = new BillItem();
                                        bi.itemTypeID = 0;
                                        bi.hasUnitPrice = false;
                                        bi.description =
                                            "Water Bill for Contract No: " + s.contractNo
                                            + "\nMonth: " + period.name
                                            + "\n" + readingDesc
                                            ;
                                        bi.price = bill.consumptionFee;
                                        bi.incomeAccountID = bdesub.SysPars.incomeAccount;
                                        items.Add(bi);
                                    }
                                    if (!Accounting.AccountBase.AmountEqual(bill.fixedFee, 0))
                                    {
                                        BillItem bi = new BillItem();
                                        bi.itemTypeID = 1;
                                        bi.hasUnitPrice = false;
                                        bi.description = "Rent " + period.name;
                                        bi.price = bill.fixedFee;
                                        bi.incomeAccountID = bdesub.SysPars.rentIncomeAccount;
                                        items.Add(bi);
                                    }

                                    if (items.Count > 0)
                                    {
                                        WaterBillDocument doc = new WaterBillDocument();
                                        doc.period = period;
                                        doc.customer = customer;
                                        doc.subscription = s;
                                        doc.reading = readings.Length == 0 ? null : readings[0];
                                        doc.waterBillItems = items.ToArray();
                                        doc.ShortDescription = "Water bill for connection " + doc.subscription.contractNo + " period:" + doc.period.name;
                                        doc.draft = false;
                                        bdesub.addBill(1, doc, billRecord, doc.itemsLessExemption.ToArray());
                                    }

                                }

                                foreach (WSIS1.LatePaymentBill bill in oldCustomerDB.GetSTRArrayByFilter<WSIS1.LatePaymentBill>("accountDocumentID<>-1 and (payDocumentID=-1 or  payDocumentID=accountDocumentID) and subscriptionID=" + s.id))
                                {
                                    totalBillDebit += bill.Total;

                                    BillPeriod period = bdesub.GetBillPeriod(bill.periodID);
                                    int billTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(PenalityBillDocument)).id;
                                    CustomerBillRecord billRecord = new CustomerBillRecord();
                                    billRecord.billDocumentTypeID = billTypeID;
                                    billRecord.connectionID = s.id;
                                    billRecord.customerID = customer.id;
                                    billRecord.periodID = period.id;
                                    billRecord.billDate = bill.postDate;
                                    billRecord.draft = false;
                                    billRecord.paymentDiffered = bill.payDocumentID == bill.accountDocumentID;
                                    billRecord.paymentDocumentID = -1;
                                    billRecord.postDate = bill.postDate;

                                    BillItem bi1 = new BillItem();
                                    bi1.itemTypeID = 1;
                                    bi1.hasUnitPrice = false;
                                    bi1.description = "Penality for " + period.name;
                                    bi1.price = bill.penality;
                                    bi1.incomeAccountID = bdesub.SysPars.billPenalityIncomeAccount;
                                    bi1.accounting = BillItemAccounting.Invoice;

                                    PenalityBillDocument doc = new PenalityBillDocument();
                                    doc.period = period;
                                    doc.customer = customer;
                                    doc.draft = false;
                                    doc.ShortDescription = "Penality Bill for customer " + customer.customerCode + " period:" + doc.period.name;
                                    doc.theItems = new BillItem[] { bi1 };

                                    bdesub.addBill(1, doc, billRecord, doc.itemsLessExemption.ToArray());
                                }

                                foreach (WSIS1.CreditBill bill in oldCustomerDB.GetSTRArrayByFilter<WSIS1.CreditBill>("accountDocumentID<>-1 and (payDocumentID=-1 or  payDocumentID=accountDocumentID) and subscriptionID=" + s.id))
                                {
                                    BillPeriod period = bdesub.GetBillPeriod(bill.periodID);
                                    int billTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id;

                                    CustomerBillRecord mainBill = new CustomerBillRecord();
                                    mainBill.billDocumentTypeID = billTypeID;
                                    mainBill.connectionID = -1;
                                    mainBill.customerID = customer.id;
                                    mainBill.periodID = period.id;
                                    mainBill.billDate = bill.postDate;
                                    mainBill.draft = false;
                                    mainBill.paymentDiffered = bill.payDocumentID == bill.accountDocumentID;
                                    mainBill.paymentDocumentID = -1;
                                    mainBill.postDate = bill.postDate;

                                    CreditBillDocument doc = new CreditBillDocument();
                                    doc.period = period;
                                    doc.customer = customer;
                                    doc.amount = bill.returnAmount;
                                    doc.ShortDescription = "Credit bill for customer " + customer.customerCode + " period:" + doc.period.name;
                                    bdesub.addBill(1, doc, mainBill, doc.itemsLessExemption.ToArray());
                                }
                            }
                            if (!AccountBase.AmountEqual(totalBillDebit, totalLedgerDebit))
                            {
                                double diff = totalLedgerDebit - totalBillDebit;
                                AdjustmentDocument adj = new AdjustmentDocument();
                                adj.voucher = new DocumentTypedReference(sysRefTypeID, "BA" + (adjustNumber++).ToString("00000000"), true);
                                adj.ShortDescription = "Ledger and bill record difference found during WSIS 2.0 upgrade";
                                adj.adjustmentAccounts = new AdjustmentAccount[]
                            {
                                new AdjustmentAccount(){accountID=customer.accountID,costCenterID=bdesub.bERP.SysPars.mainCostCenterID,debitAmount=diff>0?diff:0,creditAmount=diff<0?-diff:0,description="Bill ledger difference"}
                                ,new AdjustmentAccount(){accountID=bdesub.SysPars.incomeAccount,costCenterID=bdesub.bERP.SysPars.mainCostCenterID,debitAmount=diff<0?-diff:0,creditAmount=diff>0?diff:0,description="Bill ledger difference"}
                            };
                                bdesub.Accounting.PostGenericDocument(1, adj);
                            }
                            bdesub.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                        c--;
                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Upgrade DD: correct ledger difference")]
        public static void ddupgradeCorrectLedgerDifference()
        {

            Console.WriteLine("Processing customer ledgers");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            SQLHelper reader = bdesub.GetReaderHelper();
            DataTable data = reader.GetDataTable(@"Select b.customerID,s.customerCode code, sum(price) as amnt from OldSystemPayment p inner join CustomerBill b
on p.ID=b.paymentDocumentID inner join Subscriber s on b.customerID=s.ID
inner join CustomerBillItem i on i.customerBillID=b.ID
where b.BillDocumentTypeID=12 
group by b.customerID,s.customerCode");
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            Console.WriteLine(data.Rows.Count + " customers found");
            Console.WriteLine();
            int c = data.Rows.Count;
            SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
            SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));
            int sysRefTypeID = bdesub.Accounting.getDocumentSerialType("JV").id;
            //int adjustNumber = 2823;
            int found = 0;
            int noLedger = 0;
            DateTime custOffDate = DateTime.Parse("Sep 11,2014");
            bdesub.Accounting.UpdateLedger = false;
            foreach (DataRow row in data.Rows)
            {
                int customerID = (int)row[0];
                double debit = (double)row[2];
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}\t{2}                      ", c, found, noLedger);
                Subscriber customer = bdesub.GetSubscriber(customerID);
                lock (bdesub.WriterHelper)
                {
                    bdesub.WriterHelper.BeginTransaction();
                    try
                    {
                        customer.accountID = bdesub.getOrCreateCustomerAccountID(1, customer);
                        int csaID = bdesub.Accounting.GetOrCreateCostCenterAccount(1, bdesub.bERP.SysPars.mainCostCenterID, customer.accountID).id;
                        if (AccountBase.AmountGreater(debit, 0))
                        {
                            found++;
                            object docID = bdesub.WriterHelper.ExecuteScalar(string.Format(@"Select max(d.ID) from 
                                                {0}.dbo.Document 
                                                d inner join {0}.dbo.AccountTransaction t
                                                on d.ID=t.BatchID and t.accountID={1}
                                                where DocumentTypeID=123 and d.tranTicks<{2}
                                                ", bdesub.Accounting.DBName, csaID, custOffDate.Ticks));
                            if (docID is int)
                            {
                                AdjustmentDocument adj = bdesub.Accounting.GetAccountDocument((int)docID, true) as AdjustmentDocument;
                                bool accountSame = false;
                                foreach (AdjustmentAccount ac in adj.adjustmentAccounts)
                                {
                                    if (ac.accountID == customer.accountID)
                                    {
                                        debit += ac.debitAmount - ac.creditAmount;
                                        accountSame = true;
                                        break;
                                    }
                                }
                                if (!accountSame)
                                {
                                    Console.WriteLine("Account not same case!\nEnter to continue");
                                    Console.ReadLine();
                                }
                                adj.voucher = bdesub.Accounting.getDocumentSerials(adj.AccountDocumentID)[0].typedReference;
                                adj.ShortDescription = "Ledger and bill record difference found during WSIS 2.0 upgrade-fixed";
                                adj.adjustmentAccounts = new AdjustmentAccount[]
                                    {
                                        new AdjustmentAccount(){
                                            accountID=customer.accountID,
                                            costCenterID=bdesub.bERP.SysPars.mainCostCenterID,
                                            debitAmount=debit>0?debit:0,
                                            creditAmount=debit<0?-debit:0,
                                            description="Bill ledger difference - fixed"
                                        },
                                        new AdjustmentAccount(){
                                            accountID=bdesub.SysPars.incomeAccount,
                                            costCenterID=bdesub.bERP.SysPars.mainCostCenterID,
                                            debitAmount=debit<0?-debit:0,
                                            creditAmount=debit>0?debit:0,
                                            description="Bill ledger difference -fixed"
                                        }
                                    };
                                bdesub.Accounting.PostGenericDocument(1, adj);
                            }
                            else
                            {
                                noLedger++;
                                //bdesub.Accounting.PostGenericDocument(1, adj);
                            }
                        }
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
        }
        [MaintenanceJobMethod("Upgrade DD: fix repeat fix bug")]
        public static void ddupgradeFixRepeatFixBug()
        {

            Console.WriteLine("Processing bills that has become unpaid");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            SQLHelper reader = bdesub.GetReaderHelper();
            DataTable data = reader.GetDataTable(@"Select b2.id,b1.paymentDocumentID from Subscriber_bk.dbo.CustomerBill b1
inner join Subscriber.dbo.CustomerBill  b2
on b1.id=	b2.id and b1.paymentDocumentID<>-1 and b2.paymentDocumentID<1
and b2.__AID=1");
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            Console.WriteLine(data.Rows.Count + " bills found");
            Console.WriteLine();
            int c = data.Rows.Count;
            SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
            SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));
            int f = 0;
            bdesub.Accounting.UpdateLedger = false;
            Dictionary<int, CustomerPaymentReceipt> receipt = new Dictionary<int, CustomerPaymentReceipt>();
            List<int> billIDList = new List<int>();
            foreach (DataRow row in data.Rows)
            {
                int billID = (int)row[0];
                billIDList.Add(billID);
                int receiptID = (int)row[1];
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                      ", c, f);
                if (!receipt.ContainsKey(receiptID))
                {
                    string xml = bdesub.WriterHelper.ExecuteScalar("Select DocumentData from Accounting_2006_bk.dbo.Document where id=" + receiptID) as string;
                    if (xml == null)
                        receipt.Add(receiptID, null);
                    else
                        receipt.Add(receiptID, AccountDocument.DeserializeXML(xml, typeof(CustomerPaymentReceipt)) as CustomerPaymentReceipt);
                }
                CustomerPaymentReceipt thisReceipt = receipt[receiptID];
                if (thisReceipt == null)
                {
                    Console.WriteLine("Deleted receipt {0} not found", receiptID);
                    Console.WriteLine();
                    continue;
                }
                f++;
                c--;
            }

            bdesub.WriterHelper.BeginTransaction();
            try
            {
                int inList = 0, notInList = 0, notInListInBill = 0;
                c = receipt.Count;
                f = 0;
                int bcount = 0;
                foreach (KeyValuePair<int, CustomerPaymentReceipt> kv in receipt)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t r:{1}\tb:{2}                      ", c--, f,bcount);
                    if (kv.Value == null)
                        continue;
                    string userName = bdesub.WriterHelper.ExecuteScalar("Select UserName from Subscriber_bk.dbo.CustomerBill cb inner join WSISApp.dbo.Audit a on cb.__AID=a.ID where cb.paymentDocumentID=" + kv.Key) as string;
                    if (userName == null)
                    {
                        Console.WriteLine("User name null\n");
                        continue;
                    }
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit(userName, 1, "Server maintenance job-Upgrade DD: fix repeat fix bug", "", -1);
                    
                    foreach (int rbi in kv.Value.settledBills)
                    {
                        if (!billIDList.Contains(rbi))
                        {
                            notInList++;
                            if(bdesub.Accounting.GetAccountDocument(rbi,false)!=null)
                            {
                                Console.WriteLine("None null receipt in deleted bill.");
                                continue;
                            }

                            bdesub.WriterHelper.setReadDB("Subscriber_bk");
                            CustomerBillRecord[] rec = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("id=" + rbi);
                            BillItem[] billItems = bdesub.WriterHelper.GetSTRArrayByFilter<BillItem>("customerBillID=" + rbi);
                            bdesub.WriterHelper.restoreReadDB();

                            if(rec.Length==0)
                            {
                                Console.WriteLine("Bill record null\n");
                                continue;
                            }
                            string xml = bdesub.WriterHelper.ExecuteScalar("Select DocumentData from Accounting_2006_bk.dbo.Document where id=" + rbi) as string;
                            if (xml == null)
                            {
                                Console.WriteLine("Bill xml null\n");
                                continue;
                            }
                            CustomerBillDocument bill = AccountDocument.DeserializeXML(xml, bdesub.Accounting.GetDocumentTypeByID(rec[0].billDocumentTypeID).GetTypeObject()) as CustomerBillDocument;
                            if (bill == null)
                            {
                                Console.WriteLine("Bill null\n");
                                continue;
                            }
                            rec[0].paymentDocumentID = -1;
                            int oldBillID = bill.AccountDocumentID;
                            int newBillID=bdesub.addBill(AID, bill, rec[0], billItems);
                            if(oldBillID!=newBillID)
                            {
                                Console.WriteLine("Bill ID not same");
                                Console.WriteLine();
                                continue;
                            }
                            bcount++;
                            Console.CursorTop--;
                            Console.WriteLine("{0}\t r:{1}\tb:{2}                      ", c, f, bcount);

                        }
                        else
                            inList++;
                    }
                    bdesub.Accounting.PostGenericDocument(AID, kv.Value);
                    f++;
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t r:{1}\tb:{2}                      ", c, f, bcount);
                }
                Console.WriteLine("{0} in list {1} not inlist {2} not int list in bill", inList, notInList, notInListInBill);
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        static Subscription getSubscription(SubscriberManagmentBDE bdesub, Dictionary<int, Subscription> cache, int id)
        {
            if (cache.ContainsKey(id))
                return cache[id];
            Subscription s = bdesub.GetSubscription(id, DateTime.Now.Ticks);
            if (s == null)
                return null;
            cache.Add(id, s);
            return s;
        }
        [MaintenanceJobMethod("Upgrade DD: fix current year payment transfer bug")]
        public static void ddupgradeFixCurrentYearTransferBug()
        {
            Console.WriteLine("Processing water bill service payments");
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            DataTable dt = bdesub.WriterHelper.GetDataTable(string.Format("Select ID,DocumentData from {0}.dbo.OldSystemPayment", bdesub.DBName));
            int count=0;
            foreach (DataRow row in dt.Rows)
            {
                Console.CursorTop--;
                Console.WriteLine(count++ + "                      ");
                int ID = (int)row["ID"];
                string xml = (string)row["DocumentData"];
                CustomerPaymentReceipt r = CustomerPaymentReceipt.DeserializeXML(xml, typeof(CustomerPaymentReceipt)) as CustomerPaymentReceipt;
                foreach (int id in r.settledBills)
                {
                    bdesub.WriterHelper.Update(bdesub.DBName, "CustomerBill", new string[] { "paymentDocumentID" }, new object[] { ID }, "id=" + id);
                }
            }
        }
        [MaintenanceJobMethod("Upgrade DD: delete repeat bills")]
        public static void ddupgradeRepeaBills()
        {
            Console.WriteLine("Processing repeat bills");
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            DataTable dt = bdesub.WriterHelper.GetDataTable(string.Format("SELECT [billDocumentTypeID],[connectionID],[periodID] FROM [{0}].[dbo].[RepeatedBills]", bdesub.DBName));
            int count = dt.Rows.Count;
            Dictionary<int, CustomerPaymentReceiptSummaryDocument> receiptSummary = new Dictionary<int, CustomerPaymentReceiptSummaryDocument>();
            Dictionary<int, CustomerBillSummaryDocument> billSummary = new Dictionary<int, CustomerBillSummaryDocument>();
            Console.WriteLine("{0} repeated bills found", count);
            bdesub.Accounting.UpdateLedger = false;
            try
            {
                bdesub.WriterHelper.BeginTransaction();
                foreach (DataRow row in dt.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(count-- + "                      ");
                    int typeID = (int)row["billDocumentTypeID"];
                    int conID = (int)row["connectionID"];
                    int prdID = (int)row["periodID"];

                    int[] ids = bdesub.WriterHelper.GetColumnArray<int>(string.Format("SELECT id from {0}.dbo.CustomerBill where  [billDocumentTypeID]={1} and [connectionID]={2} and [periodID]={3}", bdesub.DBName, typeID, conID, prdID));
                    if (ids.Length == 2)
                    {
                        int minID = Math.Min(ids[0], ids[1]);
                        CustomerBillRecord bill = bdesub.getCustomerBillRecord(minID);

                        if (bill.paymentDocumentID != -1)
                        {
                            CustomerPaymentReceipt receipt = bdesub.Accounting.GetAccountDocument<CustomerPaymentReceipt>(bill.paymentDocumentID);
                            if (receipt.billBatchID != -1)
                            {
                                if (!receiptSummary.ContainsKey(receipt.billBatchID))
                                    receiptSummary.Add(receipt.billBatchID, bdesub.Accounting.GetAccountDocument<CustomerPaymentReceiptSummaryDocument>(receipt.billBatchID));
                                CustomerPaymentReceiptSummaryDocument sumDoc = receiptSummary[receipt.billBatchID];
                                sumDoc.receiptIDs = ArrayExtension.removeItem(sumDoc.receiptIDs, receipt.AccountDocumentID);
                                receipt.billBatchID = -1;
                                receipt.summerizeTransaction = false;
                                bdesub.Accounting.UpdateAccountDocumentData(1, receipt);
  //                              bdesub.Accounting.PostGenericDocument(1, sumDoc);
                            }
                            bdesub.WriterHelper.Delete(bdesub.Accounting.DBName, "DocumentSerial", "DocumentID=" + receipt.AccountDocumentID);
                            bdesub.Accounting.DeleteGenericDocument(1, receipt.AccountDocumentID);
                        }
                        CustomerBillDocument billDoc = bdesub.Accounting.GetAccountDocument<CustomerBillDocument>(bill.id);
                        if (billDoc.billBatchID != -1)
                        {
                            if (!billSummary.ContainsKey(billDoc.billBatchID))
                                billSummary.Add(billDoc.billBatchID, bdesub.Accounting.GetAccountDocument<CustomerBillSummaryDocument>(billDoc.billBatchID));
                            CustomerBillSummaryDocument sumDoc = billSummary[billDoc.billBatchID];
                            sumDoc.billIDs = ArrayExtension.removeItem(sumDoc.billIDs, billDoc.AccountDocumentID);
                            billDoc.billBatchID = -1;
                            billDoc.summerizeTransaction = false;
                            bdesub.Accounting.UpdateAccountDocumentData(1, billDoc);
//                            bdesub.Accounting.PostGenericDocument(1, sumDoc);
                        }
                        bdesub.Accounting.DeleteGenericDocument(1, bill.id);
                    }
                }
                Console.WriteLine("Updating {0} receipt summaries\n", receiptSummary.Count);
                count = receiptSummary.Count;
                foreach(KeyValuePair<int,CustomerPaymentReceiptSummaryDocument> kv in receiptSummary)
                {
                    Console.CursorTop--;
                    Console.WriteLine(count-- + "                  ");
                    bdesub.Accounting.PostGenericDocument(1, kv.Value);
                }
                Console.WriteLine("Updating {0} bill sammaries\n", billSummary.Count);
                count = billSummary.Count;
                foreach (KeyValuePair<int, CustomerBillSummaryDocument> kv in billSummary)
                {
                    Console.CursorTop--;
                    Console.WriteLine(count-- + "                  ");
                    bdesub.Accounting.PostGenericDocument(1, kv.Value);
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
        
        [MaintenanceJobMethod("Upgrade DD: transfer current year payments")]
        public static void ddupgradeTransferCurrentYearBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing customer service payments");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
                SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));

                DataTable datPayments = oldAcountingDB.GetDataTable("Select ID, PaperRef,DocumentDate,ShortDescription from Document where DocumentTypeID=17 and DocumentDate>='July 8,2014'");


                Console.WriteLine(datPayments.Rows.Count + " customer service payments found.");
                Console.WriteLine();
                int c = datPayments.Rows.Count;


                int[] oldCashAccounts = { 52, 54, 62896, 62944, 67109, 69466, 64843 };
                int[] newCashAccounts = { 391291, 381498, 381590, 298838, 298817, 298782, 448168 };


                Dictionary<int, int> accountMap = new Dictionary<int, int>();
                for (int i = 0; i < oldCashAccounts.Length; i++)
                    accountMap.Add(oldCashAccounts[i], newCashAccounts[i]);

                foreach (DataRow row in datPayments.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");

                    int ID = (int)row[0];
                    string PaperRef = (string)row[1];
                    DateTime DocumentDate = (DateTime)row[2];
                    string ShortDescription = (string)row[3];

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {
                            DataTable oldTran = oldAcountingDB.GetDataTable("Select * from AccountTransaction where BatchID=" + ID);
                            DataRow rowDoc = oldAcountingDB.GetDataTable("Select PaperRef,DocumentDate,ShortDescription from Document where ID=" + ID).Rows[0];
                            CustomerPaymentReceipt r = new CustomerPaymentReceipt();
                            r.receiptNumber = null;
                            r.PaperRef = PaperRef;
                            r.DocumentDate = DocumentDate;
                            r.ShortDescription = ShortDescription + " (Transfered from old system)";
                            r.offline = false;

                            TransactionOfBatch debit = null;
                            List<TransactionOfBatch> credit = new List<TransactionOfBatch>();
                            foreach (DataRow trow in oldTran.Rows)
                            {
                                AccountTransaction t = new AccountTransaction();
                                t.AccountID = (int)trow["AccountID"];
                                t.Amount = (double)trow["Amount"];
                                t.Note = (string)trow["Note"];

                                if (t.Amount > 0)
                                {
                                    if (debit != null)
                                        throw new ServerUserMessage("Debit transaction already found for {0}", r.PaperRef);
                                    debit = new TransactionOfBatch();
                                    debit.AccountID = accountMap[t.AccountID];
                                    debit.ItemID = 0;
                                    debit.Amount = t.Amount;
                                    debit.Note = t.Note + " (Transfered  from old system)";
                                }
                                else if (t.Amount < 0)
                                {
                                    string itemCode = oldCustomerDB.ExecuteScalar("SELECT [code] FROM [MaterialManagment].[dbo].[MaterialDescription] where salesAccountID=" + t.AccountID) as string;
                                    if (itemCode == null)
                                        throw new ServerUserMessage("ItemCode not found for {0}", r.PaperRef);
                                    itemCode = itemCode.Replace("-", "");
                                    TransactionItems titem = bdesub.bERP.GetTransactionItems(itemCode);
                                    if (titem == null)
                                        throw new ServerUserMessage("Item not found in the new system receipt: {0} item code: {1}", r.PaperRef, itemCode);
                                    if (titem.salesAccountID < 1)
                                        throw new ServerUserMessage("Item in the new system doesn't have valid sales account ID receipt: {0} item code: {1}", r.PaperRef, itemCode);
                                    TransactionOfBatch creditTran = new TransactionOfBatch();
                                    creditTran.AccountID = bdesub.Accounting.CreateCostCenterAccount(1, bdesub.bERP.SysPars.mainCostCenterID, titem.salesAccountID);
                                    creditTran.ItemID = 0;
                                    creditTran.Amount = t.Amount;
                                    creditTran.Note = t.Note + " (Transfered  from old system)";
                                    credit.Add(creditTran);
                                }
                            }
                            //JobBillDocument doc= new JobBillDocument();
                            //doc.DocumentDate = r.DocumentDate;
                            //doc.PaperRef = r.PaperRef;
                            //doc.ShortDescription = "Job bill for transfered customer receipt receipt. Description: "+ShortDescription;
                            //doc.AccountDocumentID = bdesub.Accounting.RecordTransaction(1, r, new TransactionOfBatch[] { });

                            if (debit == null)
                                throw new ServerUserMessage("Debit transaction not found for {0}", r.PaperRef);
                            if (credit == null)
                                throw new ServerUserMessage("Credit transaction not found for {0}", r.PaperRef);
                            r.assetAccountID = debit.AccountID;
                            r.paymentInstruments = new PaymentInstrumentItem[] { new PaymentInstrumentItem(bdesub.bERP.SysPars.cashInstrumentCode, debit.Amount) };
                            r.customer = null;
                            r.settledBills = new int[] { };
                            credit.Insert(0, debit);
                            r.AccountDocumentID = bdesub.Accounting.RecordTransaction(1, r, credit.ToArray());

                            //bdesub.Accounting.UpdateAccountDocumentData(1, doc);
                            bdesub.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                        c--;
                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Upgrade DD: transfer current year customer service payments")]
        public static void ddupgradeTransferCustomerServicePayments()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing customer service payments");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
                SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));

                DataTable datPayments = oldAcountingDB.GetDataTable("Select ID, PaperRef,DocumentDate,ShortDescription from Document where DocumentTypeID=17 and DocumentDate>='July 8,2014'");


                Console.WriteLine(datPayments.Rows.Count + " customer service payments found.");
                Console.WriteLine();
                int c = datPayments.Rows.Count;


                int[] oldCashAccounts = { 52, 54, 62896, 62944, 67109, 69466, 64843 };
                int[] newCashAccounts = { 391291, 381498, 381590, 298838, 298817, 298782, 448168 };


                Dictionary<int, int> accountMap = new Dictionary<int, int>();
                for (int i = 0; i < oldCashAccounts.Length; i++)
                    accountMap.Add(oldCashAccounts[i], newCashAccounts[i]);

                foreach (DataRow row in datPayments.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");

                    int ID = (int)row[0];
                    string PaperRef = (string)row[1];
                    DateTime DocumentDate = (DateTime)row[2];
                    string ShortDescription = (string)row[3];

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {
                            DataTable oldTran = oldAcountingDB.GetDataTable("Select * from AccountTransaction where BatchID=" + ID);
                            DataRow rowDoc = oldAcountingDB.GetDataTable("Select PaperRef,DocumentDate,ShortDescription from Document where ID=" + ID).Rows[0];
                            CustomerPaymentReceipt r = new CustomerPaymentReceipt();
                            r.receiptNumber = null;
                            r.PaperRef = PaperRef;
                            r.DocumentDate = DocumentDate;
                            r.ShortDescription = ShortDescription + " (Transfered from old system)";
                            r.offline = false;

                            TransactionOfBatch debit = null;
                            List<TransactionOfBatch> credit = new List<TransactionOfBatch>();
                            foreach (DataRow trow in oldTran.Rows)
                            {
                                AccountTransaction t = new AccountTransaction();
                                t.AccountID = (int)trow["AccountID"];
                                t.Amount = (double)trow["Amount"];
                                t.Note = (string)trow["Note"];

                                if (t.Amount > 0)
                                {
                                    if (debit != null)
                                        throw new ServerUserMessage("Debit transaction already found for {0}", r.PaperRef);
                                    debit = new TransactionOfBatch();
                                    debit.AccountID = accountMap[t.AccountID];
                                    debit.ItemID = 0;
                                    debit.Amount = t.Amount;
                                    debit.Note = t.Note + " (Transfered  from old system)";
                                }
                                else if (t.Amount < 0)
                                {
                                    string itemCode = oldCustomerDB.ExecuteScalar("SELECT [code] FROM [MaterialManagment].[dbo].[MaterialDescription] where salesAccountID=" + t.AccountID) as string;
                                    if (itemCode == null)
                                        throw new ServerUserMessage("ItemCode not found for {0}", r.PaperRef);
                                    itemCode = itemCode.Replace("-", "");
                                    TransactionItems titem = bdesub.bERP.GetTransactionItems(itemCode);
                                    if (titem == null)
                                        throw new ServerUserMessage("Item not found in the new system receipt: {0} item code: {1}", r.PaperRef, itemCode);
                                    if (titem.salesAccountID < 1)
                                        throw new ServerUserMessage("Item in the new system doesn't have valid sales account ID receipt: {0} item code: {1}", r.PaperRef, itemCode);
                                    TransactionOfBatch creditTran = new TransactionOfBatch();
                                    creditTran.AccountID = bdesub.Accounting.CreateCostCenterAccount(1, bdesub.bERP.SysPars.mainCostCenterID, titem.salesAccountID);
                                    creditTran.ItemID = 0;
                                    creditTran.Amount = t.Amount;
                                    creditTran.Note = t.Note + " (Transfered  from old system)";
                                    credit.Add(creditTran);
                                }
                            }
                            //JobBillDocument doc= new JobBillDocument();
                            //doc.DocumentDate = r.DocumentDate;
                            //doc.PaperRef = r.PaperRef;
                            //doc.ShortDescription = "Job bill for transfered customer receipt receipt. Description: "+ShortDescription;
                            //doc.AccountDocumentID = bdesub.Accounting.RecordTransaction(1, r, new TransactionOfBatch[] { });

                            if (debit == null)
                                throw new ServerUserMessage("Debit transaction not found for {0}", r.PaperRef);
                            if (credit == null)
                                throw new ServerUserMessage("Credit transaction not found for {0}", r.PaperRef);
                            r.assetAccountID = debit.AccountID;
                            r.paymentInstruments = new PaymentInstrumentItem[] { new PaymentInstrumentItem(bdesub.bERP.SysPars.cashInstrumentCode, debit.Amount) };
                            r.customer = null;
                            r.settledBills = new int[] { };
                            credit.Insert(0, debit);
                            r.AccountDocumentID = bdesub.Accounting.RecordTransaction(1, r, credit.ToArray());

                            //bdesub.Accounting.UpdateAccountDocumentData(1, doc);
                            bdesub.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                        c--;
                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        private static void ddupgradePostPayment(SubscriberManagmentBDE bdesub, SQLHelper oldAcountingDB, Dictionary<int, int> accountMap, Dictionary<int, CustomerPaymentReceipt> receipts, Subscriber customer, int customerCsa, WSIS1.BillRecord bill, CustomerBillRecord billRecord, PeriodicBill doc)
        {
            if (receipts.ContainsKey(bill.payDocumentID))
            {
                CustomerPaymentReceipt r = receipts[bill.payDocumentID];
                ArrayExtension.appendToArray(r.settledBills, doc.AccountDocumentID);
                billRecord.paymentDocumentID = receipts[bill.payDocumentID].AccountDocumentID;
                bdesub.Accounting.UpdateAccountDocumentData(1, r);
                bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, billRecord);
            }
            else
            {
                DataTable oldTran = oldAcountingDB.GetDataTable("Select * from AccountTransaction where BatchID=" + bill.payDocumentID);
                DataRow rowDoc = oldAcountingDB.GetDataTable("Select PaperRef,DocumentDate,ShortDescription from Document where ID=" + bill.payDocumentID).Rows[0];
                CustomerPaymentReceipt r = new CustomerPaymentReceipt();
                r.receiptNumber = null;
                r.PaperRef = rowDoc[0] as string;
                r.DocumentDate = (DateTime)rowDoc[1];
                r.ShortDescription = rowDoc[2] as String + " (Tranfred from old system)";
                r.offline = false;

                TransactionOfBatch debit = null;
                TransactionOfBatch credit = null;
                foreach (DataRow trow in oldTran.Rows)
                {
                    AccountTransaction t = new AccountTransaction();
                    t.AccountID = (int)trow["AccountID"];
                    t.Amount = (double)trow["Amount"];
                    t.Note = (string)trow["Note"];
                    if (t.Amount > 0)
                    {
                        if (debit != null)
                            throw new ServerUserMessage("Debit transaction already found for {0}", r.PaperRef);
                        debit = new TransactionOfBatch();
                        debit.AccountID = accountMap[t.AccountID];
                        debit.ItemID = 0;
                        debit.Amount = t.Amount;
                        debit.Note = t.Note + " (Tranfred from old system)";
                    }
                    else if (t.Amount < 0)
                    {
                        if (credit == null)
                        {
                            credit = new TransactionOfBatch();
                            credit.AccountID = customerCsa;
                            credit.ItemID = 0;
                            credit.Amount = t.Amount;
                            credit.Note = t.Note + " (Tranfred from old system)";
                        }
                        else
                            credit.Amount += t.Amount;
                    }
                }

                if (debit == null)
                    throw new ServerUserMessage("Debit transaction not found for {0}", r.PaperRef);
                if (credit == null)
                    throw new ServerUserMessage("Credit transaction not found for {0}", r.PaperRef);
                r.assetAccountID = debit.AccountID;
                r.paymentInstruments = new PaymentInstrumentItem[] { new PaymentInstrumentItem(bdesub.bERP.SysPars.cashInstrumentCode, debit.Amount) };
                r.customer = customer;
                r.settledBills = new int[] { doc.AccountDocumentID };
                r.AccountDocumentID = bdesub.Accounting.RecordTransaction(1, r, new TransactionOfBatch[] { debit, credit });
                receipts.Add(bill.payDocumentID, r);
            }
        }
        [MaintenanceJobMethod("Upgrade DD: Align penalities with connections")]
        public static void ddupgradeAlignPenalityBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing bills");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscriber[] customers = bdesub.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);
                Console.WriteLine(customers.Length + " customers found");
                Console.WriteLine();
                int c = customers.Length;
                SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
                SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));

                foreach (Subscriber customer in customers)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        double begBalance = 0;
                        int csaid = 1;
                        try
                        {
                            customer.accountID = bdesub.getOrCreateCustomerAccountID(1, customer);
                            csaid = bdesub.Accounting.GetOrCreateCostCenterAccount(1, bdesub.bERP.SysPars.mainCostCenterID, customer.accountID).id;
                            begBalance = bdesub.Accounting.GetEndBalance(csaid, 0).GetBalance(false);
                            Subscription[] newSubscriptions = bdesub.GetSubscriptions(customer.id, DateTime.Now.Ticks);
                            List<WSIS1.BillRecord> allBills = new List<WSIS1.BillRecord>();
                            int penalitybillTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(PenalityBillDocument)).id;
                            int creditbillTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id;
                            CustomerBillRecord[] billRec = bdesub.getBills(-1, customer.id, -1, -1);
                            bool paid = false;
                            foreach (CustomerBillRecord br in billRec)
                            {
                                if (br.paymentDocumentID != -1)
                                {
                                    paid = true;
                                }
                                else
                                {
                                    if (paid)
                                        throw new Exception("Paid and unpaid bills mixed for customer " + customer.customerCode);
                                }
                            }
                            if (!paid)
                            {
                                bool hasPenalityBill = false;
                                foreach (CustomerBillRecord br in billRec)
                                {
                                    if (!br.isPayedOrDiffered)
                                    {
                                        if (br.billDocumentTypeID == penalitybillTypeID || br.billDocumentTypeID == creditbillTypeID)
                                        {
                                            hasPenalityBill = true;
                                            bdesub.deleteCustomerBill(1, br.id, null);
                                        }
                                    }
                                }
                                if (hasPenalityBill)
                                {
                                    bdesub.generatCustomerPeriodicBills(1, DateTime.Now, customer.id, 2311);
                                    foreach (Subscription s in newSubscriptions)
                                    {

                                        WSIS1.Subscription oldSubsc = oldCustomerDB.GetSTRArrayByFilter<WSIS1.Subscription>("id=" + s.id)[0];

                                        foreach (WSIS1.LatePaymentBill bill in oldCustomerDB.GetSTRArrayByFilter<WSIS1.LatePaymentBill>("accountDocumentID<>-1 and (payDocumentID=-1 or  payDocumentID=accountDocumentID) and subscriptionID=" + s.id))
                                        {

                                            BillPeriod period = bdesub.GetBillPeriod(bill.periodID);

                                            CustomerBillRecord billRecord = new CustomerBillRecord();
                                            billRecord.billDocumentTypeID = penalitybillTypeID;
                                            billRecord.connectionID = s.id;
                                            billRecord.customerID = customer.id;
                                            billRecord.periodID = period.id;
                                            billRecord.billDate = bill.postDate;
                                            billRecord.draft = false;
                                            billRecord.paymentDiffered = bill.payDocumentID == bill.accountDocumentID;
                                            billRecord.paymentDocumentID = -1;
                                            billRecord.postDate = bill.postDate;

                                            BillItem bi1 = new BillItem();
                                            bi1.itemTypeID = 1;
                                            bi1.hasUnitPrice = false;
                                            bi1.description = "Penality for " + period.name;
                                            bi1.price = bill.penality;
                                            bi1.incomeAccountID = bdesub.SysPars.billPenalityIncomeAccount;
                                            bi1.accounting = BillItemAccounting.Invoice;

                                            PenalityBillDocument doc = new PenalityBillDocument();
                                            doc.period = period;
                                            doc.customer = customer;
                                            doc.draft = false;
                                            doc.ShortDescription = "Penality Bill for customer " + customer.customerCode + " period:" + doc.period.name;
                                            doc.theItems = new BillItem[] { bi1 };

                                            bdesub.addBill(1, doc, billRecord, doc.itemsLessExemption.ToArray());
                                        }

                                        foreach (WSIS1.CreditBill bill in oldCustomerDB.GetSTRArrayByFilter<WSIS1.CreditBill>("accountDocumentID<>-1 and (payDocumentID=-1 or  payDocumentID=accountDocumentID) and subscriptionID=" + s.id))
                                        {
                                            BillPeriod period = bdesub.GetBillPeriod(bill.periodID);


                                            CustomerBillRecord mainBill = new CustomerBillRecord();
                                            mainBill.billDocumentTypeID = creditbillTypeID;
                                            mainBill.connectionID = -1;
                                            mainBill.customerID = customer.id;
                                            mainBill.periodID = period.id;
                                            mainBill.billDate = bill.postDate;
                                            mainBill.draft = false;
                                            mainBill.paymentDiffered = bill.payDocumentID == bill.accountDocumentID;
                                            mainBill.paymentDocumentID = -1;
                                            mainBill.postDate = bill.postDate;

                                            CreditBillDocument doc = new CreditBillDocument();
                                            doc.period = period;
                                            doc.customer = customer;
                                            doc.amount = bill.returnAmount;
                                            doc.ShortDescription = "Credit bill for customer " + customer.customerCode + " period:" + doc.period.name;
                                            bdesub.addBill(1, doc, mainBill, doc.itemsLessExemption.ToArray());
                                        }
                                    }
                                }
                            }
                            double endBalance = bdesub.Accounting.GetEndBalance(csaid, 0).GetBalance(false);
                            if (!AccountBase.AmountEqual(begBalance, endBalance))
                                throw new ServerUserMessage("Beging and ending balance mismatch");
                            bdesub.WriterHelper.CommitTransaction();
                            if (bdesub.WriterHelper.InTransaction)
                                throw new ServerUserMessage("Still in transaction");
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error on customer :" + customer.customerCode);
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }

                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Upgrade DD: delete upgrade adjustments")]
        public static void deleteAdjustments()
        {
            Console.WriteLine("Deleteing upgrade adjustments");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.Accounting.UpdateLedger = false;
            int[] docID = bdesub.WriterHelper.GetColumnArray<int>("Select ID from Accounting_2006.dbo.Document where ShortDescription = 'Ledger and bill record difference found during WSIS 2.0 upgrade'");
            int c = docID.Length;
            foreach (int doc in docID)
            {
                Console.CursorTop--;
                Console.WriteLine(c-- + "                      ");
                try
                {
                    bdesub.Accounting.DeleteAccountDocument(1, doc, false);
                }
                catch (Exception ex)
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }

            }
        }
        [MaintenanceJobMethod("Upgrade DD: Regenerate unpaid bills")]
        public static void ddupgradeRegenerateBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing bills");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscriber[] customers = bdesub.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);
                Console.WriteLine(customers.Length + " customers found");
                Console.WriteLine();
                int c = customers.Length;
                SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
                SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));

                foreach (Subscriber customer in customers)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        double begBalance = 0;
                        double endBalance = 0;
                        try
                        {
                            customer.accountID = bdesub.getOrCreateCustomerAccountID(1, customer);
                            CustomerBillRecord[] billRec = bdesub.getBills(-1, customer.id, -1, -1);
                            bool paid = false;
                            foreach (CustomerBillRecord br in billRec)
                            {
                                if (br.paymentDocumentID != -1)
                                {
                                    paid = true;
                                }
                                else
                                {
                                    if (paid)
                                        throw new Exception("Paid and unpaid bills mixed for customer " + customer.customerCode);
                                }
                            }
                            if (!paid)
                            {
                                bool hasBill = false;
                                foreach (CustomerBillRecord br in billRec)
                                {
                                    if (br.periodID == 2311)
                                    {
                                        if (!br.isPayedOrDiffered)
                                        {

                                            hasBill = true;
                                            if (br.billDocumentTypeID != 4 && br.billDocumentTypeID != 12)
                                                begBalance += (bdesub.Accounting.GetAccountDocument(br.id, true) as CustomerBillDocument).total;
                                            bdesub.deleteCustomerBill(1, br.id);

                                        }
                                    }
                                }
                                if (hasBill)
                                {
                                    bdesub.generatCustomerPeriodicBills(1, DateTime.Now, customer.id, 2311);
                                    foreach (CustomerBillDocument doc in bdesub.getBillDocuments(-1, customer.id, -1, 2311, false))
                                        if (!(doc is PenalityBillDocument) && !(doc is CreditBillDocument))
                                            endBalance += doc.total;

                                }
                            }


                            if (!AccountBase.AmountEqual(begBalance, endBalance))
                                throw new ServerUserMessage("Beging and ending balance mismatch");
                            bdesub.WriterHelper.CommitTransaction();
                            if (bdesub.WriterHelper.InTransaction)
                                throw new ServerUserMessage("Still in transaction");
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error on customer :" + customer.customerCode);
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }

                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Upgrade DD: Update bill description for outstanding bills")]
        public static void ddupgradeUpdateBillDescription()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing bills");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscriber[] customers = bdesub.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);//"id=105978");
                Console.WriteLine(customers.Length + " customers found");
                Console.WriteLine();
                int c = customers.Length;

                foreach (Subscriber customer in customers)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {
                            customer.accountID = bdesub.getOrCreateCustomerAccountID(1, customer);
                            CustomerBillDocument[] billRec = bdesub.getBillDocuments(5, customer.id, -1, -1, true);
                            foreach (WaterBillDocument b in billRec)
                            {
                                if (b.period.id != 2311)
                                {
                                    foreach (BillItem bi in b.items)
                                    {
                                        if (bi.itemTypeID == 0)
                                        {
                                            string readingDesc = "";
                                            BWFMeterReading reading = b.reading;
                                            switch (reading.readingType)
                                            {
                                                case MeterReadingType.MeterReset:
                                                    readingDesc = "Current Reading: " + Math.Round(reading.reading, 0) + " Use: " + Math.Round(reading.consumption, 0) + " (Meter reset)";
                                                    break;
                                                case MeterReadingType.Average:
                                                    readingDesc = "Current Reading: " + Math.Round(reading.reading, 0) + " Use: " + Math.Round(reading.consumption, 0) + " (Average)";
                                                    break;
                                                default:
                                                    readingDesc = Accounting.AccountBase.AmountLess(reading.consumption, 0) ? "(Negative)" :
                                                                    ("Previous Reading: " + Math.Round(reading.reading - reading.consumption, 0) +
                                                                    "\nCurrent Reading: " + Math.Round(reading.reading, 0) + " Use: " + Math.Round(reading.consumption, 0));
                                                    break;
                                            }
                                            bi.description =
                                                        "Water Bill for Contract No: " + b.subscription.contractNo
                                                        + "\nMonth: " + b.period.name
                                                        + "\n" + readingDesc;
                                            bi.customerBillID = b.AccountDocumentID;
                                            bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, bi);
                                            bdesub.Accounting.UpdateAccountDocumentData(1, b);
                                        }
                                    }
                                }
                            }
                            bdesub.WriterHelper.CommitTransaction();
                            if (bdesub.WriterHelper.InTransaction)
                                throw new ServerUserMessage("Still in transaction");
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error on customer :" + customer.customerCode);
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }

                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Upgrade DD: fix draft flag of credit bills")]
        public static void ddupgradeFixDraftFlag()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing bills");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                int[] customerBill = bdesub.WriterHelper.GetColumnArray<int>(string.Format("Select id from {0}.dbo.CustomerBill where paymentDocumentID=-1", bdesub.DBName), 0);
                Console.WriteLine(customerBill.Length + " bills found");
                Console.WriteLine();
                int c = customerBill.Length;
                int nfixed = 0;
                int nError = 0;
                foreach (int billID in customerBill)
                {
                    CustomerBillDocument bill = bdesub.Accounting.GetAccountDocument(billID, true) as CustomerBillDocument;
                    Console.CursorTop--;
                    Console.WriteLine(string.Format("{0}\tFixed:{1}\tNot Fixed:{2}                       ", c--, nfixed, nError));
                    if (!bill.draft)
                        continue;
                    if (!(bill is CreditBillDocument)
                        && !(bill is PrePaymentBill)
                        )
                    {
                        nError++;
                        continue;
                    }
                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {

                            bill.draft = false;
                            bdesub.Accounting.UpdateAccountDocumentData(1, bill);
                            bdesub.WriterHelper.CommitTransaction();
                            nfixed++;
                            if (bdesub.WriterHelper.InTransaction)
                                throw new ServerUserMessage("Still in transaction");
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error updating bill ref:" + bill.invoiceNumber.reference);
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }

                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Billing: fix billing rule setting xmls serialization issue")]
        public static void ddupgradeFixSetting()
        {
            Console.WriteLine("Processing bills");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            DataTable dt = bdesub.WriterHelper.GetDataTable(string.Format("Select [settingType],[settingValue] from {0}.dbo.[BillingRuleSetting]", bdesub.DBName));

            int c = dt.Rows.Count;
            Regex regex = new Regex("<.*?>");
            foreach (DataRow row in dt.Rows)
            {
                string type = (string)row[0];
                string xml = (string)row[1];
                Match m = regex.Match(xml);
                if (m.Length == 0)
                    throw new ServerUserMessage("first tag not found");
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(xml.Substring(m.Index + m.Length));
                StringWriter sw = new StringWriter();
                doc.Save(sw);
                xml = sw.ToString();
                bdesub.WriterHelper.Update(bdesub.DBName, "BillingRuleSetting", new string[] { "settingValue" }, new object[] { xml }
                    , "settingType='" + type + "'");
            }

        }
        [MaintenanceJobMethod("Upgrade DD: Transfer registeration date")]
        public static void ddupgradeTransferFromDate()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Transfer registration date");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscription[] subscs = bdesub.WriterHelper.GetSTRArrayByFilter<Subscription>(null);
                Console.WriteLine(subscs.Length + " customers found");
                Console.WriteLine();
                int c = subscs.Length;
                int prevSubsc = -1;
                Array.Reverse(subscs);
                foreach (Subscription subsc in subscs)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        if (prevSubsc != subsc.id)
                        {
                            object regDate = bdesub.WriterHelper.ExecuteScalar("SELECT [changeDate] FROM [Subscriber_dd].[dbo].[SubscriptionHistoryItem] where changeType=4 and subscriptionID=" + subsc.id);
                            long ticksFrom;
                            if (regDate is DateTime)
                            {
                                ticksFrom = ((DateTime)regDate).Ticks;
                            }
                            else
                            {
                                ticksFrom = new DateTime(2014, 8, 7).Ticks;
                            }
                            if (subsc.ticksTo != -1 && ticksFrom >= subsc.ticksTo)
                                throw new ServerUserMessage("From greater than to");
                            bdesub.WriterHelper.Update(bdesub.DBName, "Subscription", new string[] { "ticksFrom" }, new object[] { ticksFrom }, "id=" + subsc.id + " and ticksFrom=" + subsc.ticksFrom);
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Upgrade DD: Transfer prepayments")]
        public static void ddupgradeTransferPrepayments()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Processing bills");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);


                SQLHelper oldCustomerDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["SubscriberRO"].ConnectionString.Replace("Subscriber", "Subscriber_dd"));
                SQLHelper oldAcountingDB = new SQLHelper(System.Configuration.ConfigurationManager.ConnectionStrings["AccountingRO"].ConnectionString.Replace("Accounting_2006", "Accounting"));
                int sysRefTypeID = bdesub.Accounting.getDocumentSerialType("JV").id;

                WSIS1.PrePaymentData[] prep = oldCustomerDB.GetSTRArrayByFilter<WSIS1.PrePaymentData>(null);
                Console.WriteLine(prep.Length + " prepayments found");
                Console.WriteLine();
                int c = prep.Length;
                foreach (WSIS1.PrePaymentData pp in prep)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");

                    lock (bdesub.WriterHelper)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {
                            PrePaymentBill bill = new PrePaymentBill();
                            bill.connection = bdesub.GetSubscription(pp.subscriptionID, DateTime.Now.Ticks);
                            bill.customer = bdesub.GetSubscriber(bill.connection.subscriberID);
                            bill.rents = new double[pp.rents.Length];
                            bill.rentPeriods = new BillPeriod[pp.rentPeriods.Length];
                            bill.ShortDescription = "Prepayments transfered from WSIS 2.0";
                            for (int i = 0; i < bill.rentPeriods.Length; i++)
                                bill.rentPeriods[i] = bdesub.GetBillPeriod(pp.rentPeriods[i]);
                            bdesub.addBill(1, bill, bill.itemsLessExemption.ToArray(), bill.connection.id);
                            bdesub.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                        c--;
                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Upgrade DD: resovle multiple credit schemes")]
        public static void deleteMultipleCreditSchemes()
        {
            Console.WriteLine("Processing credit schemes");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            int[] cids = bdesub.WriterHelper.GetColumnArray<int>(@"SELECT [customerID]  FROM [Subscriber].[dbo].[CreditScheme]
group  by customerID having count(*) >1");

            Console.WriteLine(cids.Length + " customers with multiple credit schemes found");
            Console.ReadLine();
            Console.WriteLine();
            int c = cids.Length;
            foreach (int cid in cids)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");

                lock (bdesub.WriterHelper)
                {
                    bdesub.WriterHelper.BeginTransaction();
                    try
                    {
                        int AID=ApplicationServer.SecurityBDE.WriteAddAudit("admin",1,"Server maintenance job-Upgrade DD: resovle multiple credit schemes","",-1);
                        CreditScheme[] cs = bdesub.getCustomerCreditSchemes(cid);
                        if (cs.Length < 2)
                            throw new Exception("BUG: not repeated. Count " + cs.Length);
                        CreditScheme max = null;
                        foreach (CreditScheme s in cs)
                        {
                            if (max == null || s.paymentAmount > max.paymentAmount)
                                max = s;
                        }
                        foreach (CreditScheme s in cs)
                        {
                            if (max != s)
                            {
                                CustomerBillDocument[] creditBills = bdesub.getBillDocuments(bdesub.Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id, s.customerID, -1, -1, false);
                                foreach (CreditBillDocument cb in creditBills)
                                {
                                    if (cb.creditSchemeID == s.id)
                                    {
                                        cb.creditSchemeID = max.id;
                                        bdesub.Accounting.UpdateAccountDocumentData(AID, cb);
                                    }
                                }
                                bdesub.deleteCreditScheme(AID, s.id,false);
                            }

                        }
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
        }

        [MaintenanceJobMethod("Upgrade DD: fix -1 credit scheme")]
        public static void ddupgradeFixNoCreditScheme()
        {
            Console.WriteLine("Processing credit schemes");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            int[] cids = bdesub.WriterHelper.GetColumnArray<int>(@"SELECT distinct [customerID]
                FROM [Subscriber].[dbo].[CreditScheme]");

            Console.WriteLine(cids.Length + " customers with credits found");
            Console.ReadLine();
            Console.WriteLine();
            int c = cids.Length;
            int f = 0;
            int creditTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id;
            foreach (int cid in cids)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                      ",c,f);

                lock (bdesub.WriterHelper)
                {
                    bdesub.WriterHelper.BeginTransaction();
                    try
                    {
                        int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job-Upgrade DD: resovle multiple credit schemes", "", -1);
                        CreditScheme[] cs = bdesub.getCustomerCreditSchemes(cid);
                        if (cs.Length > 1)
                        {
                            Console.WriteLine("Multiple credit schemes found for customer ID:" + cid);
                            Console.ReadLine();
                            continue;
                        }
                        CustomerBillDocument[] creditBills = bdesub.getBillDocuments(creditTypeID, cid, -1, -1, false);
                        foreach (CreditBillDocument doc in creditBills)
                        {
                            if (doc.creditSchemeID != cs[0].id)
                            {
                                f++;
                                doc.creditSchemeID = cs[0].id;
                                bdesub.Accounting.UpdateAccountDocumentData(AID, doc);
                            }
                        }

                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
        }
        [MaintenanceJobMethod("Upgrade DD: resolve credit scheme ledger conflict")]
        public static void ddupgradeResovleCreditSchemeLedgerConflict()
        {
            Console.WriteLine("Processing credit schemes");

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            int[] cids = bdesub.WriterHelper.GetColumnArray<int>(@"SELECT distinct [customerID]
                FROM [Subscriber].[dbo].[CreditScheme]");

            Console.WriteLine(cids.Length + " customers with credits found");
            Console.ReadLine();
            Console.WriteLine();
            int c = cids.Length;
            int f = 0;
            int creditTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id;
            foreach (int cid in cids)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                      ", c, f);
                Subscriber customer=bdesub.GetSubscriber(cid);
                int []postPaidBillTypes=new int[]
                {
                    bdesub.Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id,
                    bdesub.Accounting.GetDocumentTypeByType(typeof(PenalityBillDocument)).id,
                };
                lock (bdesub.WriterHelper)
                {
                    bdesub.WriterHelper.BeginTransaction();
                    try
                    {
                        int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job-Upgrade DD: resolve credit scheme ledger conflict", "", -1);
                        CreditScheme[] cs = bdesub.getCustomerCreditSchemes(cid);
                        if (cs.Length > 1)
                        {
                            Console.WriteLine("Multiple credit schemes found for customer ID:" + cid);
                            Console.ReadLine();
                            continue;
                        }
                        int csaid = bdesub.Accounting.GetOrCreateCostCenterAccount(AID, bdesub.bERP.SysPars.mainCostCenterID, customer.accountID).id;
                        double debit = bdesub.Accounting.GetEndNetBalance(csaid, 0);
                        foreach (int t in postPaidBillTypes)
                        {
                            foreach (CustomerBillDocument bill in bdesub.getBillDocuments(t, cid, -1, -1, true))
                            {
                                debit -= bill.total;
                            }
                        }
                        double totalCreditBills=0;
                        CustomerBillDocument[] creditBills = bdesub.getBillDocuments(creditTypeID, cid, -1, -1, false);
                        List<int> deletedCreditBills = new List<int>();
                        foreach (CreditBillDocument doc in creditBills)
                        {
                            if (doc.creditSchemeID != cs[0].id)
                                throw new ServerUserMessage("Credit bill and credit scheme doesn't match");
                            CustomerBillRecord creditBillRec = bdesub.getCustomerBillRecord(doc.AccountDocumentID);
                            if (creditBillRec.paymentDiffered)
                                continue;
                            else if (creditBillRec.paymentDocumentID < 1)
                            {
                                bdesub.deleteCustomerBill(AID, doc.AccountDocumentID);
                                deletedCreditBills.Add(doc.period.id);
                            }
                            else
                                totalCreditBills += doc.total;
                        }
                        
                        double remaining = cs[0].creditedAmount - totalCreditBills;
                        if(Account.AmountLess(debit,remaining) && AccountBase.AmountGreater(remaining,0)) //if the credit scheme shows remaining credit bills while the ledger shows less debit
                        {
                            double removeCredit;
                            if (!AccountBase.AmountGreater(debit, 0))
                            {
                                removeCredit= cs[0].creditedAmount-totalCreditBills;
                            }
                            else
                            {
                                removeCredit= remaining - debit;
                            }
                            cs[0].creditedAmount -= removeCredit;
                            bdesub.WriterHelper.logDeletedData(AID, bdesub.DBName + ".dbo.CreditScheme", "id=" + cs[0].id);
                            bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, cs[0], new string[] { "__AID" }, new object[] { AID });
                            f++;
                        }
                        foreach(int prdID in deletedCreditBills)
                        {
                            CustomerBillDocument[]  docs;
                            CustomerBillRecord [] recs;
                            BillItem[][] items;
                            string msg;
                            if (bdesub.generateMonthlyBill(DateTime.Now, bdesub.GetBillPeriod(prdID), customer, out docs, out recs, out items, out msg))
                            {
                                for (int j = 0; j < docs.Length; j++)
                                    if (docs[j] is CreditBillDocument)
                                        bdesub.addBill(AID, docs[j], recs[j], items[j]);
                            }
                            else
                            {
                                Console.WriteLine(msg);
                                Console.ReadLine();
                            }
                        }
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
        }
        [MaintenanceJobMethod("Job Database: delete bills from canceled jobs")]
        public static void jobDeleteBillsfromCanaceledJobs()
        {
            JobManagerBDE job = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;
            string sql = @"Select invoiceNo from {0}.dbo.JobBillOfMaterial m inner join {0}.dbo.JobData j
on m.jobID=j.id
where invoiceNo<>-1 and j.status=8";
            int[] invoices = job.WriterHelper.GetColumnArray<int>(string.Format(sql, job.DBName));

            Console.WriteLine(invoices.Length + " bills in canceled jobs found");
            Console.WriteLine("Press enter to continue or close the window to terminate");
            Console.ReadLine();
            Console.WriteLine();
            int c = invoices.Length;
            foreach (int invoiceID in invoices)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");

                lock (job.WriterHelper)
                {
                    job.WriterHelper.BeginTransaction();
                    try
                    {
                        if (job.subscriberBDE.isBillPaid(invoiceID))
                        {
                            string query = @"Select j.jobNo from {0}.dbo.JobBillOfMaterial m inner join {0}.dbo.JobData j
                                                on m.jobID=j.id
                                                where invoiceNo=" + invoiceID + " and j.status=8";
                            string jobno = (string)job.WriterHelper.ExecuteScalar(string.Format(query, job.DBName));

                            throw new ServerUserMessage("Job No:" + jobno + ", Invoice ID:" + invoiceID + " is paid. Void the receipt and run the job again. This will affect casher accounts.");
                        }
                        if (job.BdeAccounting.GetAccountDocument(invoiceID, false) != null) //If document exists
                            job.BdeAccounting.DeleteGenericDocument(1, invoiceID);

                        job.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        job.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.WriteLine();
                    }
                    c--;
                }
            }
        }
        [MaintenanceJobMethod("Harar : Clear employees")]
        public static void hararClearEmployees()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            PayrollBDE payroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

            int maxID;
            Console.Write("Enter maximum ID:");
            string idstr = Console.ReadLine();
            int[] empIDs = accounting.WriterHelper.GetColumnArray<int>("Select id from Payroll_2006.dbo.Employee where __AID=1");
            if (!int.TryParse(idstr, out maxID))
            {
                Console.WriteLine("Invalid id string " + idstr);
                return;
            }
            int N;
            int AID;
            int c;

            c = empIDs.Length;
            Console.WriteLine(c + " employees found");
            Console.WriteLine();
            foreach (int eid in empIDs)
            {
                c--;
                Console.CursorTop--;
                Console.WriteLine(c + "                       ");
                try
                {
                    AID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    payroll.DeleteEmployee(AID, eid);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        [MaintenanceJobMethod("Fix customer bill and solidwaste disposal accounting problem")]
        public static void jobFixCustomerBillACcounting()
        {

            Console.WriteLine("Fixing customer service bills");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("billDocumentTypeID in (172,176) and paymentDocumentID=-1");
            Console.WriteLine(bills.Length + " bills found");
            Console.WriteLine();
            int c = bills.Length;
            List<int> paid = new List<int>();

            foreach (CustomerBillRecord bill in bills)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                bdesub.WriterHelper.BeginTransaction();
                try
                {

                    BillItem[] items = bdesub.getCustomerBillItems(bill.id);
                    foreach (BillItem i in items)
                    {
                        i.accounting = BillItemAccounting.Cash;
                        i.settledFromDepositAmount = 0;
                        bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, i);
                    }
                    CustomerBillDocument cbill = accounting.GetAccountDocument(bill.id, true) as CustomerBillDocument;
                    JobBillDocument jobBill = cbill as JobBillDocument;
                    if (jobBill != null)
                    {
                        jobBill.jobItems = items;
                        accounting.UpdateAccountDocumentData(1, jobBill);
                    }
                    else
                    {
                        //todo habtamu
                        //SolidWasteDisposalBillDocument sbill = cbill as SolidWasteDisposalBillDocument;
                        //sbill.billItems = items;
                        //accounting.UpdateAccountDocumentData(1, sbill);
                    }
                    bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }

        }
        [MaintenanceJobMethod("Update 'VoidReceipt' Job data CustomerIDs")]
        public static void UpdateVoidReceiptCustomerIDs()
        {

            Console.WriteLine("Updating Job Data");

            JobManagerBDE bdeJob = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;
            bdeJob.WriterHelper.setReadDB(bdeJob.DBName);
            JobData[] jobData = bdeJob.WriterHelper.GetSTRArrayByFilter<JobData>("applicationType=10");
            Console.WriteLine(jobData.Length + " Void Receipt Jobs found");
            Console.WriteLine();
            int c = jobData.Length;

            foreach (JobData data in jobData)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                bdeJob.WriterHelper.BeginTransaction();
                try
                {

                    VoidReceiptData voidData = (VoidReceiptData)bdeJob.getWorkFlowData(data.id, 10, 0, true);
                    string query = @"SELECT customerID
  FROM [Subscriber].[dbo].[CustomerBill_Deleted]
  where paymentDocumentID=" + voidData.receitDocumentID;
                    object obj = bdeJob.WriterHelper.ExecuteScalar(query);
                    if (obj != null)
                    {
                        int customerID = (int)obj;
                        bdeJob.WriterHelper.ExecuteScalar(String.Format("Update JobManager.dbo.JobData SET customerID={0} where id={1}", customerID, data.id));
                    }
                    bdeJob.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdeJob.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }

        }
        [MaintenanceJobMethod("Fix paid customer bill accounting problem")]
        public static void jobFixCustomerBillAccountingPaid()
        {

            Console.WriteLine("Fixing customer service bills");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("billDocumentTypeID in (172) and paymentDocumentID<>-1");
            Console.WriteLine(bills.Length + " bills found");
            Console.WriteLine();
            int c = bills.Length;
            List<int> paid = new List<int>();

            foreach (CustomerBillRecord bill in bills)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                bdesub.WriterHelper.BeginTransaction();
                try
                {
                    int oldAID = (int)bde.WriterHelper.ExecuteScalar("Select __AID from Accounting_2006.dbo.Document where ID=" + bill.paymentDocumentID);
                    AuditInfo ai = ApplicationServer.SecurityBDE.getAuditInfo(oldAID);
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit(ai.userName, ApplicationServer.SecurityBDE.GetUIDForName(ai.userName), "Fix Customer Service Bill", "", -1);
                    BillItem[] items = bdesub.getCustomerBillItems(bill.id);
                    foreach (BillItem i in items)
                    {
                        i.accounting = BillItemAccounting.Cash;
                        i.settledFromDepositAmount = 0;
                        bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, i);
                    }
                    CustomerBillDocument cbill = accounting.GetAccountDocument(bill.id, true) as CustomerBillDocument;
                    JobBillDocument jobBill = cbill as JobBillDocument;
                    jobBill.jobItems = items;
                    accounting.UpdateAccountDocumentData(AID, jobBill);
                    CustomerPaymentReceipt r = accounting.GetAccountDocument(bill.paymentDocumentID, true) as CustomerPaymentReceipt;
                    accounting.PostGenericDocument(AID, r);

                    bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }
        }
        //static Account moveDetailChildAccounts(AccountingBDE accounting, int AID, Account rootAccount, int accountID)
        //{
        //    Account oldAccount = accounting.GetAccount<Account>(accountID);
        //    Account newAccount = accounting.GetAccount<Account>(createOrGetDetailAccount(AID, rootAccount, accounting, oldAccount));
        //    int N;
        //    foreach (Account ac in accounting.GetChildAcccount<Account>(oldAccount.id, 0, -1, out N))
        //    {
        //        Console.CursorTop--;
        //        Console.WriteLine(ac.Code + "                                    ");
        //        if (ac.PID != newAccount.id)
        //        {
        //            if (ac.Code.IndexOf(rootAccount.Code) != 0)
        //            {
        //                if (ac.Code.IndexOf(oldAccount.Code + "-") == 0)
        //                {
        //                    accounting.changeParent<Account>(AID, ac.id, newAccount.id, false);
        //                    string newCode = ac.Code.Replace(oldAccount.Code + "-", rootAccount.Code + "-" + oldAccount.Code + "-"); ;
        //                    if (!ac.Code.Equals(newCode))
        //                    {
        //                        ac.Code = newCode;
        //                        ac.PID = newAccount.id;
        //                        accounting.UpdateAccount(AID, ac, false);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return newAccount;
        //}
        //static Account moveToAccountToDetail(AccountingBDE accounting, int AID, Account rootAccount, int accountID, bool createSumAc)
        //{
        //    Account ac = accounting.GetAccount<Account>(accountID);
        //    if (ac.isControl)
        //        throw new ServerUserMessage("It is not allowed to move parent account");
        //    if (ac.Code.IndexOf(rootAccount.Code) != 0)
        //    {
        //        string detailAccountCode = rootAccount.Code + "-" + ac.Code;
        //        Account summaryAccount = (Account)ac.Clone();
        //        int detailParentID = createOrGetDetailAccount(AID, rootAccount, accounting, accounting.GetAccount<Account>(ac.PID));
        //        accounting.changeParent<Account>(AID, ac.id, detailParentID, false);
        //        if (!ac.Code.Equals(detailAccountCode))
        //        {
        //            ac.Code = detailAccountCode;
        //            ac.PID = detailParentID;
        //            accounting.UpdateAccount(AID, ac, false);
        //        }
        //        if (createSumAc)
        //        {
        //            summaryAccount.id = -1;
        //            summaryAccount.id = accounting.CreateAccount(AID, summaryAccount, false, false);
        //            return summaryAccount;
        //        }
        //    }
        //    else
        //    {
        //        if (createSumAc)
        //        {
        //            Account summaryAccount = accounting.GetAccount<Account>(ac.Code.Substring(rootAccount.Code.Length + 1));
        //            if (summaryAccount == null)
        //                throw new ServerUserMessage("Summary account not found for detail account {0} " + ac.Code);
        //            return summaryAccount;
        //        }
        //    }
        //    return null;
        //}

        [MaintenanceJobMethod("Billing: remove cashhandover transactions from billing detail transaction")]
        public static void moveCashHandoverTransactions()
        {
            Console.WriteLine("Removing cashhandover transactions from billing detail transactions");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int n;
            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(CashHandoverDocument)).id, false, DateTime.Now, DateTime.Now, 0, -1, out n);
            Console.WriteLine("Pocessing {0} documents", docs.Length);
            Console.WriteLine();
            int c = docs.Length;
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Move cash handover documents", "", -1);
                foreach (CashHandoverDocument d in docs)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                        ");
                    AccountTransaction[] trans = accounting.GetTransactionsOfDocument(d.AccountDocumentID);
                    accounting.WriterHelper.BeginTransaction();
                    try
                    {
                        bool update = false;
                        foreach (AccountTransaction t in trans)
                        {
                            CostCenterAccount csa = accounting.GetCostCenterAccount(t.AccountID);
                            Account ac = accounting.GetAccount<Account>(csa.accountID);
                            if (ac.Code.Substring(0, 4).Equals("CUST"))
                            {
                                Account sumac = accounting.GetAccount<Account>(ac.Code.Substring(5));
                                if (sumac == null)
                                    throw new ServerUserMessage("Summary account not found for {0}", ac.NameCode);
                                update = true;
                                int newAccountID = accounting.GetOrCreateCostCenterAccount(AID, csa.costCenterID, sumac.id).id;
                                if (t.AccountID == d.data.sourceCashAccountID)
                                    d.data.sourceCashAccountID = newAccountID;
                                t.AccountID = newAccountID;
                            }
                        }
                        if (update)
                        {
                            accounting.UpdateTransaction(AID, accounting.GetAccountDocument(d.AccountDocumentID, true), TransactionOfBatch.fromAccountTransaction(trans), false
                            , DocumentTypedReference.cloneArray(false, accounting.getDocumentSerials(d.AccountDocumentID)));
                        }
                        accounting.WriterHelper.CommitTransaction();
                    }
                    catch
                    {
                        accounting.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [MaintenanceJobMethod("Billing: remove direct entry transactions from billing detail transaction")]
        public static void moveDirecEntryTransactions()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int n;
            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id, false, DateTime.Now, DateTime.Now, 0, -1, out n);
            Console.WriteLine("Pocessing {0} documents", docs.Length);
            Console.WriteLine();
            int c = docs.Length;
            int f = 0;
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Move direct entry documents", "", -1);
                foreach (AdjustmentDocument d in docs)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}            ", c--, f);
                    AccountTransaction[] trans = accounting.GetTransactionsOfDocument(d.AccountDocumentID);
                    accounting.WriterHelper.BeginTransaction();
                    try
                    {
                        bool update = false;
                        foreach (AccountTransaction t in trans)
                        {
                            if (bde.GetCashAccount(t.AccountID) == null)
                                continue;
                            CostCenterAccount csa = accounting.GetCostCenterAccount(t.AccountID);
                            Account ac = accounting.GetAccount<Account>(csa.accountID);
                            if (ac.Code.Substring(0, 4).Equals("CUST"))
                            {
                                Account sumac = accounting.GetAccount<Account>(ac.Code.Substring(5));
                                if (sumac == null)
                                    throw new ServerUserMessage("Summary account not found for {0}", ac.NameCode);
                                update = true;
                                t.AccountID = accounting.GetOrCreateCostCenterAccount(AID, csa.costCenterID, sumac.id).id;
                                foreach (AdjustmentAccount adac in d.adjustmentAccounts)
                                {
                                    if (adac.costCenterID == csa.costCenterID && adac.accountID == csa.accountID)
                                    {
                                        adac.accountID = sumac.id;
                                        break;
                                    }
                                }
                            }
                        }
                        if (update)
                        {
                            f++;
                            accounting.UpdateTransaction(AID, accounting.GetAccountDocument(d.AccountDocumentID, true), TransactionOfBatch.fromAccountTransaction(trans), false
                                , DocumentTypedReference.cloneArray(false, accounting.getDocumentSerials(d.AccountDocumentID)));
                        }
                        accounting.WriterHelper.CommitTransaction();
                    }
                    catch
                    {
                        accounting.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [MaintenanceJobMethod("Billing: remove casher to casher from billing detail transaction")]
        public static void moveC2CTransactions()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int n;
            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(BIZNET.iERP.CashierToCashierDocument)).id, false, DateTime.Now, DateTime.Now, 0, -1, out n);
            Console.WriteLine("Pocessing {0} documents", docs.Length);
            Console.WriteLine();
            int c = docs.Length;
            int f = 0;
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Move cash to cash documents", "", -1);
                foreach (CashierToCashierDocument d in docs)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                  ", c--, f);
                    AccountTransaction[] trans = accounting.GetTransactionsOfDocument(d.AccountDocumentID);
                    accounting.WriterHelper.BeginTransaction();
                    try
                    {
                        bool update = false;
                        PaymentCenter sourcePc = subsc.GetPaymentCenterByCashAccount(d.fromCashierAccountID);
                        if (sourcePc != null)
                        {
                            if (d.fromCashierAccountID == sourcePc.casherAccountID)
                            {
                                update = true;
                                d.fromCashierAccountID = sourcePc.summaryAccountID;
                            }
                        }
                        PaymentCenter destPC = subsc.GetPaymentCenterByCashAccount(d.toCashierAccountID);
                        if (destPC != null)
                        {
                            if (d.toCashierAccountID == destPC.casherAccountID)
                            {
                                update = true;
                                d.toCashierAccountID = destPC.summaryAccountID;
                            }
                        }
                        if (update)
                        {
                            DocumentTypedReference[] refs = DocumentTypedReference.createArray(accounting.getDocumentSerials(d.AccountDocumentID));
                            accounting.PostGenericDocument(AID, d);
                            accounting.addDocumentSerials(AID, d, refs);
                            f++;
                        }
                        accounting.WriterHelper.CommitTransaction();
                    }
                    catch
                    {
                        accounting.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [MaintenanceJobMethod("Billing: remove bank desposit from billing detail transaction")]
        public static void moveBankDespositTransactions()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int n;
            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(BIZNET.iERP.BankDepositDocument)).id, false, DateTime.Now, DateTime.Now, 0, -1, out n);
            Console.WriteLine("Pocessing {0} documents", docs.Length);
            Console.WriteLine();
            int c = docs.Length;
            int f = 0;
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Move cash to cash documents", "", -1);
                foreach (BankDepositDocument d in docs)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                  ", c--, f);
                    AccountTransaction[] trans = accounting.GetTransactionsOfDocument(d.AccountDocumentID);
                    accounting.WriterHelper.BeginTransaction();
                    try
                    {
                        bool update = false;
                        PaymentCenter sourcePc = subsc.GetPaymentCenterByCashAccount(d.casherAccountID);
                        if (sourcePc != null)
                        {
                            if (d.casherAccountID == sourcePc.casherAccountID)
                            {
                                update = true;
                                d.casherAccountID = sourcePc.summaryAccountID;
                            }
                        }
                        if (update)
                        {
                            DocumentTypedReference[] refs = DocumentTypedReference.createArray(accounting.getDocumentSerials(d.AccountDocumentID));
                            accounting.PostGenericDocument(AID, d);
                            accounting.addDocumentSerials(AID, d, refs);
                            f++;
                        }
                        accounting.WriterHelper.CommitTransaction();
                    }
                    catch
                    {
                        accounting.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        [MaintenanceJobMethod("Billing: summerize billing transactions")]
        public static void summerizeBillingTransactions()
        {
            Console.WriteLine("Summerizing billing stransactions");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            string sql = @"SELECT 
datepart(year,billDate),
datepart(month,billDate),
datepart(day,billDate),
periodID,
count(*)
FROM Subscriber.[dbo].CustomerBill
{0}
group by 
datepart(year,billDate),
datepart(month,billDate),
datepart(day,billDate),
periodID
order by 
datepart(year,billDate),
datepart(month,billDate),
datepart(day,billDate),
periodID
";
            /*sql = string.Format(sql, @" where datepart(year,billDate)=2014 and 
datepart(month,billDate)=9 and
datepart(day,billDate)=24 and
periodID=-1");*/
            //debug sql
            sql = string.Format(sql, "");
            DataTable table = accounting.WriterHelper.GetDataTable(sql);
            Console.WriteLine(table.Rows.Count + " days");
            int c = table.Rows.Count;
            Console.WriteLine();
            Console.WriteLine();
            int f = 0;
            foreach (DataRow row in table.Rows)
            {
                Console.CursorTop -= 2;
                //Console.WriteLine((c--) + "                   ");


                int year = (int)row[0];
                int month = (int)row[1];
                int day = (int)row[2];
                int periodID = (int)row[3];
                int count = (int)row[4];
                Console.WriteLine("Year: {0} Month:{1}: Day:{2} PeriodID:{3}", year, month, day, periodID);
                int subC = count;

                sql = @"SELECT id FROM Subscriber.[dbo].CustomerBill
where 
datepart(year,billDate)={0} and 
datepart(month,billDate)={1} and
datepart(day,billDate)={2} and
periodID={3}
";
                sql = string.Format(sql, year, month, day, periodID);
                int[] docIDs = accounting.WriterHelper.GetColumnArray<int>(sql);
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Transfer accounts items to CUST", "", -1);
                Console.WriteLine("Processing {0} bills", count);
                Console.WriteLine();
                if (count < 50)
                {
                    foreach (int docID in docIDs)
                    {
                        if (subC % 10 == 0)
                        {
                            Console.CursorTop--;
                            Console.WriteLine("{0}\t{1}                   ", subC, f);
                        }
                        subC--;
                        CustomerBillDocument bill = accounting.GetAccountDocument(docID, true) as CustomerBillDocument;
                        if (bill.summerizeTransaction || bill.billBatchID > 0)
                            continue;
                        f++;
                        accounting.WriterHelper.BeginTransaction();
                        accounting.WriterHelper.setReadDB(accounting.DBName);
                        try
                        {
                            bill.summerizeTransaction = true;
                            bill.billBatchID = -1;
                            TransactionOfBatch[] trans = accounting.GetTransactionsOfDocument(docID);
                            TransactionSummerizer sum = new TransactionSummerizer(accounting, bde.SysPars.summaryRoots);
                            sum.addTransaction(AID, trans);
                            accounting.RecordTransactionInternal(AID,bill, bill.DocumentDate, sum.getSummary(), true);
                            accounting.UpdateAccountDocumentData(AID, bill);
                            accounting.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            accounting.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                        finally
                        {
                            accounting.WriterHelper.restoreReadDB();
                        }
                    }
                }
                else
                {
                    int batchCount = 0;
                    for (int i = 0; i < docIDs.Length; )
                    {

                        List<int> batchIDs = new List<int>();
                        TransactionSummerizer sum = new TransactionSummerizer(accounting, bde.SysPars.summaryRoots);
                        accounting.WriterHelper.BeginTransaction();
                        try
                        {
                            //BillPeriod period = null;
                            int batchPeriod = -2;
                            DateTime time = DateTime.MinValue;
                            for (; i < docIDs.Length && batchIDs.Count < subsc.SysPars.billBatchSize; i++)
                            {
                                if (subC % 10 == 0)
                                {
                                    Console.CursorTop--;
                                    Console.WriteLine("{0}\t{1}                   ", subC, f);
                                }
                                subC--;
                                CustomerBillDocument record = accounting.GetAccountDocument(docIDs[i], true) as CustomerBillDocument;
                                if (record.billBatchID < 1 && !record.summerizeTransaction)
                                {
                                    int thisPerioID;
                                    if (record is PeriodicBill)
                                    {
                                        thisPerioID = ((PeriodicBill)record).period.id;
                                    }
                                    else
                                        thisPerioID = -1;
                                    if (batchPeriod == -2)
                                        batchPeriod = thisPerioID;
                                    else if (batchPeriod != thisPerioID)
                                    {
                                        throw new ServerUserMessage("None uniform period");
                                    }
                                    batchIDs.Add(docIDs[i]);
                                    if (time < record.DocumentDate)
                                        time = record.DocumentDate;
                                    sum.addTransaction(AID, accounting.GetTransactionsOfDocument(docIDs[i]));
                                    f++;
                                }
                            }
                            if (batchIDs.Count > 0)
                            {
                                string periodName;
                                if (batchPeriod > 0)
                                    periodName = subsc.GetBillPeriod(batchPeriod).name;
                                else
                                    periodName = "None Periodic";
                                CustomerBillSummaryDocument summaryBill = new CustomerBillSummaryDocument();
                                summaryBill.ShortDescription = string.Format("Bill Batch summary {0} - {1}", periodName, batchCount + 1);
                                summaryBill.billIDs = batchIDs.ToArray();
                                summaryBill.transactions = sum.getSummary();
                                summaryBill.DocumentDate = time;
                                accounting.PostGenericDocument(AID, summaryBill);
                            }
                            batchCount++;
                            accounting.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            accounting.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                    }
                }
            }
        }

        [MaintenanceJobMethod("Billing: summerize payment receipts")]
        public static void summerizePaymentReceiptTransactions()
        {
            Console.WriteLine("Summerizing payment receipt stransactions");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            string sql = @"SELECT 
datepart(year,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
datepart(month,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
datepart(day,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
t.accountID,
count(*)
FROM Accounting_2006.[dbo].Document d
inner join Accounting_2006.[dbo].AccountTransaction t
on d.ID=t.BatchID
where documentTypeID=171 and Amount>0
{0}
group by 
datepart(year,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
datepart(month,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
datepart(day,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
t.accountID
order by 
datepart(year,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
datepart(month,Accounting_2006.dbo.ticksToDate(d.tranTicks)),
datepart(day,Accounting_2006.dbo.ticksToDate(d.tranTicks))
";
            string cr = getDocumentFilterInput("d",accounting);

            //            sql = string.Format(sql, @"and datepart(year,Accounting_2006.dbo.ticksToDate(d.tranTicks))=2014
            //and datepart(month,Accounting_2006.dbo.ticksToDate(d.tranTicks))=11
            //and datepart(day,Accounting_2006.dbo.ticksToDate(d.tranTicks))=3");
            if(string.IsNullOrEmpty(cr))
                sql = string.Format(sql, "");
            else
                sql = string.Format(sql, "  and  "+cr);
            DataTable table = accounting.WriterHelper.GetDataTable(sql);
            Console.WriteLine(table.Rows.Count + " days");
            int c = table.Rows.Count;
            int f = 0;
            Console.WriteLine();
            Console.WriteLine();
            foreach (DataRow row in table.Rows)
            {
                Console.CursorTop -= 2;
                //Console.WriteLine((c--) + "                   ");


                int year = (int)row[0];
                int month = (int)row[1];
                int day = (int)row[2];
                int accountID = (int)row[3];
                int count = (int)row[4];
                CashAccount cashAccount = bde.GetCashAccount(accountID);

                Console.WriteLine("Year: {0} Month: {1}: Day: {2} cash account: {3}", year, month, day, cashAccount.nameCode);
                int subC = count;

                sql = @"SELECT d.id from Accounting_2006.[dbo].Document d
inner join Accounting_2006.[dbo].AccountTransaction t
on d.ID=t.BatchID
where documentTypeID=171 and Amount>0 and
datepart(year,Accounting_2006.dbo.ticksToDate(d.tranTicks))={0} and 
datepart(month,Accounting_2006.dbo.ticksToDate(d.tranTicks))={1} and
datepart(day,Accounting_2006.dbo.ticksToDate(d.tranTicks))={2} and
t.accountID={3}
";
                sql = string.Format(sql, year, month, day, accountID);
                int[] docIDs = accounting.WriterHelper.GetColumnArray<int>(sql);

                Dictionary<int, int> test = new Dictionary<int, int>(); //unique test
                foreach (int d in docIDs) test.Add(d, d);

                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Transfer accounts items to CUST", "", -1);
                Console.WriteLine("Processing {0} bills", count);
                Console.WriteLine();

                int batchCount = 0;
                List<int> batchIDs = new List<int>();
                TransactionSummerizer sum = new TransactionSummerizer(accounting, bde.SysPars.summaryRoots);
                accounting.WriterHelper.BeginTransaction();
                try
                {
                    //BillPeriod period = null;
                    DateTime time = DateTime.MinValue;

                    for (int i = 0; i < docIDs.Length; i++)
                    {
                        Console.CursorTop--;
                        Console.WriteLine("{0}\t{1}        ", subC--, f);
                        CustomerPaymentReceipt receipt = accounting.GetAccountDocument(docIDs[i], true) as CustomerPaymentReceipt;
                        if (receipt.billBatchID < 1 && !receipt.summerizeTransaction)
                        {
                            batchIDs.Add(docIDs[i]);
                            if (time < receipt.DocumentDate)
                                time = receipt.DocumentDate;
                            sum.addTransaction(AID, accounting.GetTransactionsOfDocument(docIDs[i]));
                            f++;
                        }
                    }
                    if (batchIDs.Count > 0)
                    {
                        CustomerPaymentReceiptSummaryDocument summaryReceipt = new CustomerPaymentReceiptSummaryDocument();
                        summaryReceipt.ShortDescription = string.Format("Receipt summary {0}-{1}-{2}-{3}", day, month, year, cashAccount.nameCode);
                        summaryReceipt.receiptIDs = batchIDs.ToArray();
                        summaryReceipt.transactions = sum.getSummary();
                        summaryReceipt.DocumentDate = time;
                        summaryReceipt.assetAccountID = cashAccount.csAccountID;
                        accounting.PostGenericDocument(AID, summaryReceipt);
                    }
                    batchCount++;
                    accounting.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    accounting.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }

        }

        [MaintenanceJobMethod("Billing: summerize direct entry on CUST")]
        public static void summerizeDirectEtnryTransactions()
        {

            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Summerizing direct entry on CUST");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                accounting.UpdateLedger = false;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;

                INTAPS.CachedObject<int, string> accountCodes = new INTAPS.CachedObject<int, string>
            (
            new RetrieveObjectDelegate<int, string>(delegate(int id)
            {
                CostCenterAccount csa = accounting.GetCostCenterAccount(id);
                return accounting.GetAccount<Account>(csa.accountID).Code;
            })
            );

                accounting.WriterHelper.setReadDB(accounting.DBName);
                int custNetACcount = accounting.GetAccountID<Account>("CUST-7000");
                if(custNetACcount==-1)
                {
                    Console.WriteLine("Create account CUST-7000  customer transaction balance.");
                    return;
                }
                try
                {

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Billing: summerize direct entry on CUST", "", -1);
                    ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
                    int c;
                    Console.WriteLine("Loading documents..");
                    List<AccountDocument> docs = new List<AccountDocument>();
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));

                    Console.WriteLine(docs.Count + " docs documents found");
                    Console.WriteLine();
                    c = docs.Count;

                    for (int i = docs.Count - 1; i >= 0; i--)
                    {
                        Console.CursorTop--;
                        Console.WriteLine((c--) + "                      ");
                        AccountDocument theDocument = docs[i];
                        DateTime closedUpto = bde.getTransactionsClosedUpto();
                        if (theDocument.DocumentDate <= closedUpto)
                        {
                            continue;
                        }
                        AccountTransaction[] tran = accounting.GetTransactionsOfDocument(theDocument.AccountDocumentID);
                        INTAPS.Accounting.BDE.TransactionSummerizer sum = new TransactionSummerizer(accounting, new string[] { "CUST" });

                        List<string> codes = new List<string>();
                        foreach (AccountTransaction t in tran)
                        {
                            codes.Add(accountCodes[t.AccountID]);
                        }
                        bool alreadySummerized = true;
                        foreach (string ac in codes)
                        {
                            if (!ac.Equals("CUST-7000") && ac.IndexOf("CUST") == 0)
                            {
                                string sumAc = ac.Substring("CUST-".Length);
                                bool found = false;
                                foreach (string acc in codes)
                                {
                                    if (sumAc.IndexOf(acc) == 0)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    alreadySummerized = false;
                                    break;
                                }
                            }
                        }
                        if (alreadySummerized)
                            continue;

                        foreach (AccountTransaction t in tran)
                            if (accountCodes[t.AccountID].IndexOf("CUST") == 0)
                                sum.addTransaction(AID, t);
                        TransactionOfBatch[] sumTran = sum.getSummary();
                        if (sumTran.Length == 0)
                            continue;
                        double vnet = 0;
                        double qnet = 0;
                        foreach (TransactionOfBatch t in sumTran)
                            if (t.ItemID == 0)
                                vnet += t.Amount;
                            else
                                qnet += t.Amount;
                        List<TransactionOfBatch> tranList = new List<TransactionOfBatch>(sumTran);
                        CostCenterAccount csa = accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, custNetACcount);
                        if (!AccountBase.AmountEqual(vnet, 0))
                            tranList.Add(new TransactionOfBatch(csa.id, -vnet, "Summerization in batch by job"));
                        if (!AccountBase.AmountEqual(qnet, 0))
                            tranList.Add(new TransactionOfBatch(csa.id, TransactionItem.MATERIAL_QUANTITY, -qnet, "Summerization in batch by job"));


                        List<DocumentTypedReference> refs = new List<DocumentTypedReference>();
                        foreach (DocumentSerial s in accounting.getDocumentSerials(theDocument.AccountDocumentID))
                            refs.Add(s.typedReference);
                        AdjustmentDocument a = theDocument as AdjustmentDocument;
                        List<AdjustmentAccount> adj = new List<AdjustmentAccount>(a.adjustmentAccounts);
                        foreach (TransactionOfBatch b in tranList)
                        {
                            CostCenterAccount costCenterAccount = accounting.GetCostCenterAccount(b.AccountID);
                            adj.Add(new AdjustmentAccount()
                            {
                                accountID = costCenterAccount.accountID,
                                costCenterID = costCenterAccount.costCenterID,
                                debitAmount = b.Amount > 0 ? b.Amount : 0,
                                creditAmount = b.Amount < 0 ? -b.Amount : 0
                            }
                                );
                        }
                        a.adjustmentAccounts = adj.ToArray();
                        if (refs.Count > 0)
                            a.voucher = refs[0];

                        
                        bde.WriterHelper.BeginTransaction();
                        try
                        {
                            accounting.PostGenericDocument(AID, a);
                            if (refs.Count > 1)
                            {
                                for (int j = 1; j < refs.Count; j++)
                                    accounting.addDocumentSerials(AID, a, refs[j]);
                            }

                            bde.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error processing document ID:{0} ref:{1}\n{2}\n{3}", theDocument.AccountDocumentID, theDocument.PaperRef, ex.Message, ex.StackTrace);
                            Console.WriteLine();
                        }
                        finally
                        {
                            bde.WriterHelper.CheckTransactionIntegrityWithoutReadDB();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }


        [MaintenanceJobMethod("Job Database: cancel all active jobs")]
        public static void jobCancelActiveJobs()
        {
            JobManagerBDE job = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;
            string sql = @"Select id from {0}.dbo.JobData where status not in (9,8)";
            int[] jobIDs = job.WriterHelper.GetColumnArray<int>(string.Format(sql, job.DBName));

            Console.WriteLine(jobIDs.Length + " active jobs found");
            Console.WriteLine("Press enter to continue or close the window to terminate");
            Console.ReadLine();
            Console.WriteLine();
            int c = jobIDs.Length;
            JobWorker w = job.GetWorker("admin");
            foreach (int invoiceID in jobIDs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");

                lock (job.WriterHelper)
                {
                    job.WriterHelper.BeginTransaction();
                    try
                    {
                        job.ChangeJobStatus(-1,invoiceID,DateTime.Now,StandardJobStatus.CANCELED,"Canceled by job",w);

                        job.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        job.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.WriteLine();
                    }
                    c--;
                }
            }
        }

        [MaintenanceJobMethod("Bug investigation: investigate Adama Billing rounding Error")]
        public static void miscAdamaBillingRoundingError()
        {
            try
            {
                Console.WriteLine("Loading excption data");
                //unescapped preparation code commented
                string fn = @"C:\Projects\OromiaWSIS\Support\Adama\roudingerror\billroundingerror.xml";
                string outFn = @"C:\Projects\OromiaWSIS\Support\Adama\roudingerror\unescapped.xml";
                //System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                //doc.Load(fn);
                //System.IO.File.WriteAllText(outFn, doc.FirstChild.FirstChild.InnerText);
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.Load(outFn);
                
                double totalBill = 0;
                List<Tuple<string, double>> bills= new List<Tuple<string, double>>();
                foreach (System.Xml.XmlElement el in doc.FirstChild.ChildNodes)
                {
                    string id = ((System.Xml.XmlElement)el.GetElementsByTagName("customer")[0]).GetElementsByTagName("customerCode")[0].InnerText;
                    double total = 0;
                    foreach (System.Xml.XmlElement billItem in el.GetElementsByTagName("BillItem"))
                    {
                        total += double.Parse(billItem.GetElementsByTagName("price")[0].InnerText);
                        total += double.Parse(billItem.GetElementsByTagName("settledFromDepositAmount")[0].InnerText);
                    }
                    bills.Add(new Tuple<string, double>(id, total));
                    totalBill += total;
                }
                bills.Sort((x, y) => x.Item2.CompareTo(y.Item2));
                foreach (Tuple<string, double> v in bills)
                {
                    Console.WriteLine("id: {0}\t bill:{1}", v.Item1, v.Item2);
                }
                Console.WriteLine("total : {0}", totalBill);

                
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

    }
}
