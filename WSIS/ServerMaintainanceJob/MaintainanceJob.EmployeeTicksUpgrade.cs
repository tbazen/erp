using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using INTAPS.RDBMS;
using System.Xml.Serialization;
namespace INTAPS.ClientServer.RemottingServer
{
    public static partial class MaintainanceJob
    {
        [MaintenanceJobMethod("Accounting: generate Tax Declaration Period")]
        public static void CreatePeriods()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            int year;
            Console.Write("Please enter year: ");
            if (int.TryParse(Console.ReadLine(), out year))
                bde.GenerateTaxDeclarationPeriod(year);
        }
        [MaintenanceJobMethod("Accounting: generate accounting period")]
        public static void CreateAccountingPeriods()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            int year;
            Console.Write("Please enter year: ");
            if (int.TryParse(Console.ReadLine(), out year))
                bde.GenerateYearAccountingPeriod(year);
       }
        [MaintenanceJobMethod("Accounting: generate accounting years")]
        public static void CreateAccountingYears()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            int year1;
            Console.Write("Please enter year1: ");
            while (!int.TryParse(Console.ReadLine(), out year1))
            {
                Console.Write("Please enter valid year");
            }
            int year2;
            Console.Write("Please enter year2({0}): ", year1);
            do
            {
                string ln = Console.ReadLine();
                if (ln.Equals(""))
                {
                    year2 = year1;
                    break;
                }
                if (int.TryParse(ln, out year2))
                    break;
                Console.Write("Please enter valid year");
            }
            while (true);
            for (int year = year1; year <= year2; year++)
            {
                bde.GenerateYearAccountingPeriod(year);
            }

        }
        static AccountDocument getCashedAccountDocument(AccountingBDE bde, Dictionary<int, AccountDocument> cache, int docID, bool fullData)
        {
            if (cache.ContainsKey(docID))
                return cache[docID];
            AccountDocument doc = bde.GetAccountDocument(docID, fullData);
            cache.Add(docID, doc);
            return doc;
        }
        static bool employeesEqual(Employee e1, Employee e2)
        {
            if (!e1.employeeName.Equals(e2.employeeName))
                return false;
            if (!e1.orgUnitID.Equals(e2.orgUnitID))
                return false;


            if (!e1.grossSalary.Equals(e2.grossSalary))
                return false;
            if (!e1.status.Equals(e2.status))
                return false;
            if (!e1.enrollmentDate.Equals(e2.enrollmentDate))
                return false;


            if (!e1.dismissDate.Equals(e2.dismissDate))
                return false;
            if (!e1.employmentType.Equals(e2.employmentType))
                return false;
            if (!e1.shareHolder.Equals(e2.shareHolder))
                return false;

            if (!e1.birthDate.Equals(e2.birthDate))
                return false;
            if (!e1.sex.Equals(e2.sex))
                return false;
            if (!e1.telephone.Equals(e2.telephone))
                return false;

            if (!e1.address.Equals(e2.address))
                return false;
            if (!e1.TIN.Equals(e2.TIN))
                return false;
            if (!e1.transportAllowance.Equals(e2.transportAllowance))
                return false;

            if (!e1.medicalBenefit.Equals(e2.medicalBenefit))
                return false;
            if (!e1.otherBenefits.Equals(e2.otherBenefits))
                return false;
            if (!e1.totalCostSharingDebt.Equals(e2.totalCostSharingDebt))
                return false;


            if (!e1.costSharingPayableAmount.Equals(e2.costSharingPayableAmount))
                return false;
            if (!e1.costSharingStatus.Equals(e2.costSharingStatus))
                return false;
            if (!e1.costCenterID.Equals(e2.costCenterID))
                return false;

            if (!e1.salaryKind.Equals(e2.salaryKind))
                return false;
            if (!e1.TaxCenter.Equals(e2.TaxCenter))
                return false;
            if (!e1.accountAsCost.Equals(e2.accountAsCost))
                return false;

            if (!e1.loginName.Equals(e2.loginName))
                return false;
            if (!e1.title.Equals(e2.title))
                return false;
            return true;
        }
        static PayrollPeriod getCachedPayrollPeriod(PayrollBDE bde, Dictionary<int, PayrollPeriod> cache, int periodID)
        {
            if (cache.ContainsKey(periodID))
                return cache[periodID];
            PayrollPeriod period = bde.GetPayPeriod(periodID);
            cache.Add(periodID, period);
            return period;
        }
        [MaintenanceJobMethod("Payroll: employee ticks upgrade")]
        public static void payrollEmployeeTicksUpgrade()
        {
            Console.WriteLine("Ticks upgrade for employee");
            PayrollBDE payroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
            payroll.WriterHelper.setReadDB(payroll.DBName);
            Employee[] employee = payroll.WriterHelper.GetSTRArrayByFilter<Employee>(null);

            int c = employee.Length;
            int fixCount = 0;
            int errorCount = 0;
            Console.WriteLine("{0} employees found ", c);
            Console.WriteLine();
            Dictionary<int, AccountDocument> payrollDocs = new Dictionary<int, AccountDocument>();
            Dictionary<int, PayrollPeriod> periods = new Dictionary<int, PayrollPeriod>();

            foreach (Employee ee in employee)
            {

                Console.CursorTop--;
                Console.WriteLine("{0}\tDone: {1}\tError: {2}     ", c--, fixCount, errorCount);
                if (ee.ticksFrom != -1)
                    continue;
                payroll.WriterHelper.BeginTransaction();
                try
                {
                    INTAPS.Payroll.PayrollDocument[] payrolls = payroll.WriterHelper.GetSTRArrayByFilter<PayrollDocument>("employeeID=" + ee.id);
                    SortedList<long, Employee> versions = new SortedList<long, Employee>();
                    versions.Add(DateTime.Now.Ticks, ee);
                    foreach (PayrollDocument pdoc in payrolls)
                    {
                        AccountDocument doc = getCashedAccountDocument(payroll.Accounting, payrollDocs, pdoc.accountDocumentID, true);
                        PayrollPeriod period = getCachedPayrollPeriod(payroll, periods, pdoc.periodID);
                        if (doc == null)
                        {
                            errorCount++;
                            Console.WriteLine("Invalid payroll document ID {0}", pdoc.accountDocumentID);
                            Console.WriteLine();
                            continue;
                        }
                        if (period == null)
                            throw new ServerUserMessage("Invalid payroll period ID {0}", pdoc.periodID);
                        Employee emp = null;
                        if (doc is PayrollDocument)
                        {
                            emp = ((PayrollDocument)doc).employee;
                        }
                        else if (doc is PayrollSetDocument)
                        {

                            foreach (PayrollDocument theDoc in ((PayrollSetDocument)doc).payrolls)
                            {
                                if (theDoc.employee.id == ee.id)
                                {
                                    emp = theDoc.employee;
                                    break;
                                }
                            }
                        }
                        if (emp == null)
                            throw new ServerUserMessage("Employee data not found in the payroll documents");
                        versions.Add(period.fromDate.Ticks, emp);
                    }
                    SortedList<long, Employee> uniqueVersions = new SortedList<long, Employee>();
                    Nullable<KeyValuePair<long, Employee>> prev = null;
                    foreach (KeyValuePair<long, Employee> kv in versions)
                    {
                        if (prev == null || !employeesEqual(prev.Value.Value, kv.Value))
                        {
                            uniqueVersions.Add(kv.Key, kv.Value);
                            prev = kv;
                        }
                    }
                    if (uniqueVersions.Count == 1)
                    {
                        payroll.WriterHelper.Update(payroll.DBName, "Employee", new string[] { "ticksFrom" }, new object[] { ee.enrollmentDate.Ticks }, "ticksFrom=-1 and id=" + ee.id);
                    }
                    else
                    {
                        long leastTime = -1;
                        foreach (KeyValuePair<long, Employee> kv in uniqueVersions)
                        {
                            if (leastTime == -1 || leastTime > kv.Value.enrollmentDate.Ticks)
                                leastTime = kv.Value.enrollmentDate.Ticks;
                            if (leastTime == -1 || leastTime > kv.Key)
                                leastTime = kv.Key;
                        }
                        long fromTicks = leastTime;
                        int vCount = 0;
                        List<Employee> listVersion = new List<Employee>();
                        foreach (KeyValuePair<long, Employee> kv in uniqueVersions)
                        {
                            if (vCount == 0)
                                kv.Value.ticksFrom = leastTime;
                            else
                                kv.Value.ticksFrom = kv.Key;
                            vCount++;
                            listVersion.Add(kv.Value);
                        }
                        vCount = 0;
                        foreach (Employee v in listVersion)
                        {
                            if (vCount == uniqueVersions.Count - 1)
                            {
                                payroll.WriterHelper.Update(payroll.DBName, "Employee", new string[] { "ticksFrom" }, new object[] { v.ticksFrom }, "ticksFrom=-1 and id=" + ee.id);
                            }
                            else
                            {
                                v.BankAccountNo = "";
                                v.ticksTo = listVersion[vCount + 1].ticksFrom;
                                payroll.WriterHelper.InsertSingleTableRecord(payroll.DBName, v);
                            }
                            vCount++;
                        }
                        fixCount++;
                    }

                    payroll.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    payroll.WriterHelper.RollBackTransaction();
                    errorCount++;
                    Console.WriteLine("Error processing {0} : {1} \n{2}", ee.employeeID, ee.id, ex.Message);
                    Console.WriteLine();
                }
            }
        }
        //public static int createOrGetDetailAccount(int AID, Account rootAccount, AccountingBDE accounting, Account account)
        //{
        //    if (account.Code.IndexOf(rootAccount.Code) == 0)
        //        return account.id;
        //    string code = rootAccount.Code + "-" + account.Code;
        //    Account newAccount = accounting.GetAccount<Account>(code);
        //    if (newAccount != null)
        //        return newAccount.id;
        //    newAccount = (Account)account.Clone();
        //    newAccount.Code = code;
        //    if (account.PID == -1)
        //        newAccount.PID = rootAccount.Obj;
        //    else
        //        newAccount.PID = createOrGetDetailAccount(AID, rootAccount, accounting, accounting.GetAccount<Account>(account.PID));
        //    newAccount.id = -1;
        //    CostCenterAccount[] csa = accounting.GetCostCenterAccountsOf<Account>(account.id);
        //    int ret = accounting.CreateAccount<Account>(AID, newAccount, true, false);
        //    foreach (CostCenterAccount cs in csa)
        //    {
        //        accounting.CreateCostCenterAccount(AID, cs.costCenterID, ret, false);
        //    }
        //    return ret;
        //}
        //[MaintenanceJobMethod("Accounting: move transaction items accounts to ITEM")]
        //public static void moveDetailItemAccountsToITEM()
        //{
        //    Console.WriteLine("Transfering transaction items to ITEM");
        //    AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
        //    iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;

        //    bde.WriterHelper.setReadDB(bde.DBName);

        //    try
        //    {
        //        int AID = ApplicationServer.SecurityBDE.WriteAddAudit("Admin", 1, "Transfer transaction items to ITEM", "", -1);
        //        Account acITEM = accounting.GetAccount<Account>("ITEM");
        //        if (acITEM == null)
        //            throw new ServerUserMessage("Account ITEM not setup");
        //        if (acITEM.PID != -1)
        //            throw new ServerUserMessage("Account ITEM must be root account");
        //        Account contrAc = accounting.GetAccount<Account>(bde.SysPars.summerizationContraAccountID);
        //        if (contrAc == null)
        //        {
        //            Console.WriteLine("ITEM contra account not found. Creating");
        //            contrAc = new Account();
        //            contrAc.Code = "ITEM-7000";
        //            contrAc.Name = contrAc.Description = "Item Transaction Balance";
        //            contrAc.CreditAccount = false;
        //            contrAc.PID = acITEM.id;
        //            contrAc.id = accounting.CreateAccount(AID, contrAc);
        //            bde.SetSystemParameters(AID, new string[] { "summerizationContraAccountID", "summerizeItemTransactions" }, new object[] { contrAc.id, true });
        //        }
        //        ItemCategory[] cats = bde.WriterHelper.GetSTRArrayByFilter<ItemCategory>(null);
        //        Array.Reverse(cats);
        //        int c = cats.Length;
        //        Console.WriteLine(c + " category items found. Press enter to continue.");
        //        Console.ReadLine();
        //        List<System.Reflection.FieldInfo> fields = new List<FieldInfo>();
        //        foreach (System.Reflection.FieldInfo f in typeof(ItemCategory).GetFields())
        //        {
        //            object[] atr = f.GetCustomAttributes(typeof(AIParentAccountFieldAttribute), false);
        //            if (atr.Length == 0)
        //                continue;
        //            fields.Add(f);
        //        }
        //        foreach (ItemCategory cat in cats)
        //        {
        //            Console.CursorTop--;
        //            Console.WriteLine((c--) + "                                   ");

        //            //if (INTAPS.ArrayExtension.contains(new string[] { "060","060033" }, cat.Code))
        //            //    continue;
        //            bde.WriterHelper.BeginTransaction();
        //            try
        //            {
        //                bool update = false;
        //                foreach (System.Reflection.FieldInfo f in fields)
        //                {
        //                    object val = f.GetValue(cat);
        //                    int oldCategoryAccountID = (int)f.GetValue(cat);
        //                    if (oldCategoryAccountID != -1)
        //                    {
        //                        Account catAc = accounting.GetAccount<Account>(oldCategoryAccountID);
        //                        if (catAc == null)
        //                            f.SetValue(cat, -1);
        //                        else
        //                        {
        //                            int newCategoryAccountID = createOrGetDetailAccount(AID, acITEM, accounting, catAc);
        //                            if (newCategoryAccountID != oldCategoryAccountID)
        //                            {
        //                                f.SetValue(cat, newCategoryAccountID);
        //                                int N;
        //                                Console.WriteLine();
        //                                foreach (Account ac in accounting.GetChildAcccount<Account>(catAc.id, 0, -1, out N))
        //                                {
        //                                    Console.CursorTop--;
        //                                    Console.WriteLine(ac.Code + "                                    ");
        //                                    if (ac.PID != newCategoryAccountID)
        //                                    {
        //                                        if (ac.Code.IndexOf("ITEM") != 0)
        //                                        {
        //                                            if (ac.Code.IndexOf(catAc.Code + "-") == 0)
        //                                            {
        //                                                accounting.changeParent<Account>(AID, ac.id, newCategoryAccountID, false);
        //                                                string newCode = ac.Code.Replace(catAc.Code + "-", "ITEM-" + catAc.Code + "-"); ;
        //                                                if (!ac.Code.Equals(newCode))
        //                                                {
        //                                                    ac.Code = newCode;
        //                                                    ac.PID = newCategoryAccountID;
        //                                                    accounting.UpdateAccount(AID, ac, false);
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                update = true;
        //                            }
        //                            Console.CursorTop--;
        //                            Console.WriteLine("                     ");
        //                            Console.CursorTop--;
        //                        }
        //                    }
        //                }
        //                if (update)
        //                {
        //                    bde.WriterHelper.UpdateSingleTableRecord(bde.DBName, cat);
        //                }
        //                bde.WriterHelper.CommitTransaction();
        //            }
        //            catch (Exception exp)
        //            {
        //                bde.WriterHelper.RollBackTransaction();
        //                Console.WriteLine(cat.Code);
        //                Console.WriteLine(exp.Message);
        //                Console.WriteLine();
        //            }
        //        }
        //        Console.WriteLine("Rebuilding CostCenterLeafAccountTable");
        //        accounting.rebuildCostCenterLeafAccountTable(AID);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Failed\n" + ex.Message);
        //    }
        //}
        [MaintenanceJobMethod("Fix Account Transaction Sequence for all accounts")]
        public static void FixTransactionSquence()
        {
            bool fix = true;
            Console.WriteLine(fix ? "Check and fixing tarnsaction sequence errors" : "Checking transaction sequence errors");
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;

            DataTable dt = accounting.WriterHelper.GetDataTable("Select distinct accountID,itemID from " + accounting.DBName + ".dbo.AccountTransaction");
            int c = dt.Rows.Count;
            Console.WriteLine();
            foreach (DataRow row in dt.Rows)
            {
                int accountID = (int)row[0];
                int itemID = (int)row[1];
                CostCenterAccountWithDescription leafAccount = accounting.GetCostCenterAccountWithDescription(accountID);

               //Console.WriteLine(leafAccount.CodeName + " itemID " + itemID + "                    ");
                Console.CursorTop--;
                Console.WriteLine(c-- + "                                             ");
                if (fix)
                {
                    FixTransactionSquence(accounting, accountID, itemID);
                }
                else
                {
                    INTAPS.RDBMS.SQLHelper readerHelper = accounting.GetReaderHelper();
                    try
                    {
                        string str;
                        if (accounting.VerifyTransactionConsistency(readerHelper, leafAccount.id, 0, out str) != -1)
                        {
                            Console.WriteLine("Error " + leafAccount.CodeName + "\nError:" + str);
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Error verifying " + leafAccount.CodeName + "\nError:" + exception.Message);
                    }
                    finally
                    {
                        accounting.ReleaseHelper(readerHelper);
                    }
                }
            }
        }


        [MaintenanceJobMethod("Transfer transactions to AT schema")]
        public static void transferTransactionsToATSchema()
        {
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;
            int count;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            //Console.Write("Do you want to create transaction table (Y/N)?");
            //if (Console.ReadLine().Equals("Y"))
            //{
            //    Console.WriteLine("Loading accounts.");
            //    CostCenterAccount[] csas = accounting.WriterHelper.GetSTRArrayByFilter<CostCenterAccount>(null);
            //    Console.WriteLine("{0} accounts found.", count = csas.Length);
            //    Console.WriteLine();

            //    foreach (CostCenterAccount csa in csas)
            //    {
            //        Console.CursorTop--;
            //        Console.WriteLine("{0}           ", count--);
            //        if (!accounting.tranTableExists(csa.id, TransactionItem.DEFAULT_CURRENCY))
            //        {
            //            accounting.createTranTable(-1, csa.id, TransactionItem.DEFAULT_CURRENCY);
            //        }
            //        if (!accounting.tranTableExists(csa.id, TransactionItem.MATERIAL_QUANTITY))
            //        {
            //            accounting.createTranTable(-1, csa.id, TransactionItem.MATERIAL_QUANTITY);
            //        }
            //    }
            //}
            
            count = (int)accounting.WriterHelper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.AccountTransaction", accounting.DBName));
            Console.WriteLine("{0} transactions found.",count);
            Console.WriteLine("Loading transactions");
            SQLHelper helper = accounting.GetReaderHelper();
            IDataReader reader = helper.ExecuteReader(string.Format("Select * from {0}.dbo.AccountTransaction", accounting.DBName));
            Console.WriteLine("Transactions loaded");
            Console.WriteLine();
            HashSet<ulong> isCleared = new HashSet<ulong>();
            int nTCreate = 0;
            while (reader.Read())
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\tCreated Table{1}           ", count--, nTCreate);

                AccountTransaction tran = new AccountTransaction();
                tran.ID = (int)reader["ID"];
                tran.documentID = (int)reader["BatchID"];
                tran.AccountID = (int)reader["accountID"];
                tran.ItemID = (int)reader["itemID"];
                tran.OrderN = (int)reader["OrderN"];
                tran.tranTicks = (long)reader["tranTicks"];
                tran.Amount = (double)reader["Amount"];
                tran.TotalCrBefore = (double)reader["TotalCrBefore"];
                tran.TotalDbBefore = (double)reader["TotalDbBefore"];
                tran.Note = reader["Note"] as string;
                if(!isCleared.Contains(tran.ItemAccountID))
                {
                    if (!accounting.tranTableExists(tran.AccountID, tran.ItemID))
                    {
                        accounting.createTranTable(-1, tran.AccountID, tran.ItemID);
                        nTCreate++;
                    }
                    accounting.WriterHelper.ExecuteNonQuery("delete from AccountingTransaction.dbo.tran_{0}_{1}".format( tran.AccountID, tran.ItemID));
                    isCleared.Add(tran.ItemAccountID);
                }
                accounting.WriterHelper.Insert("AccountingTransaction", "tran_{0}_{1}".format(tran.AccountID, tran.ItemID),
                    new string[]
                    {
                        "id"
                        ,"documentID"
                      ,"orderN"
                      ,"tranTicks"
                      ,"amount"
                      ,"totalDbBefore"
                      ,"totalCrBefore"
                      ,"note"
                    }
                    ,
                    new object[]
                    {
                        tran.ID,
                        tran.documentID,
                        tran.OrderN,
                        tran.tranTicks,
                        tran.Amount,
                        tran.TotalDbBefore,
                        tran.TotalCrBefore,
                        tran.Note
                    }
                    );
            }       
            
        }
        [MaintenanceJobMethod("Accounting-inventory: Re-post issue documents")]
        public static void accountingInventoryRepostStoreIssue()
        {
            int N;
            Console.WriteLine("Repositng issue documents");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            try
            {
                CallTimer timerTotal = new CallTimer();
                CallTimer postTransaction = new CallTimer();
                timerTotal.start();
                accounting.recordTransactionInternalTimer = new CallTimer();
                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-inventory: Re-post issue documents", "", -1);

                int c;

                AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(StoreIssueDocument)).id,
                                false, DateTime.Now, DateTime.Now, 0, -1, out N);
                Console.WriteLine(docs.Length + " store issues found");
                Console.WriteLine();
                c = docs.Length;
                
                for (int i = docs.Length - 1; i >= 0; i--)
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "                      ");
                    if (Console.KeyAvailable)
                    {
                        if (Console.ReadKey().Key == ConsoleKey.Q)
                        {
                            break;
                        }
                    }
                    postTransaction.start();
                    StoreIssueDocument storeIssue = accounting.GetAccountDocument(docs[i].AccountDocumentID, true) as StoreIssueDocument;
                    DateTime closedUpto = bde.getTransactionsClosedUpto();
                    if (storeIssue.DocumentDate <= closedUpto)
                    {
                        continue;
                    }
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        
                        accounting.PostGenericDocument(AID, storeIssue);
                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine("Error processing storeIssue ID:{0} ref:{1}\n{2}", storeIssue.AccountDocumentID, storeIssue.PaperRef, ex.Message);
                        Console.WriteLine();
                    }
                    postTransaction.stop();
                }
                timerTotal.stop();
                Console.WriteLine("Total:\t\t{0}", timerTotal);
                Console.WriteLine("Post transaction:\t\t{0}", postTransaction);
                Console.WriteLine("Record transaction:\t\t{0}", accounting.recordTransactionInternalTimer);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
            
        }
        //[MaintenanceJobMethod("Accounting-summerization: Re-post purchase,sales, etc")]
        //public static void accountingRepostItemSummaryAccounts()
        //{
        //    MessageRepository.dontDoDiff = true;
        //    try
        //    {
        //        int N;
        //        Console.WriteLine("Repositng issue documents");
        //        AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
        //        accounting.UpdateLedger = false;
        //        iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
        //        accounting.WriterHelper.setReadDB(accounting.DBName);

        //        try
        //        {

        //            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Accounting-summerization: Re-post purchase,sales, etc", "", -1);
        //            ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
        //            int c;
        //            Console.WriteLine("Loading documents..");
        //            List<AccountDocument> docs = new List<AccountDocument>();
        //            AccountDocument[] purchases = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
        //                            false, DateTime.Now, DateTime.Now, 0, -1, out N);
        //            foreach (Purchase2Document purchase in purchases)
        //            {
        //                int i = 0;
        //                purchase.prepayments = new PurchasePrePaymentDocument[purchase.prepaymentsIDs.Length];
        //                for (i = 0; i < purchase.prepayments.Length; i++)
        //                    purchase.prepayments[i] = accounting.GetAccountDocument<PurchasePrePaymentDocument>(purchase.prepaymentsIDs[i]);
        //                purchase.deliveries = new PurchasedItemDeliveryDocument[purchase.deliverieIDs.Length];
        //                for (i = 0; i < purchase.deliveries.Length; i++)
        //                    purchase.deliveries[i] = accounting.GetAccountDocument<PurchasedItemDeliveryDocument>(purchase.deliverieIDs[i]);
        //                purchase.paymentVoucher = accounting.GetAccountDocument<Purchase2Document>(purchase.AccountDocumentID).paymentVoucher;
        //            }
        //            docs.AddRange(purchases);

        //            Console.WriteLine("Purchase documents loaded");
        //            //AccountDocument[] sells = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
        //            //                false, DateTime.Now, DateTime.Now, 0, -1, out N);
        //            //foreach(Sell2Document s in sells)
        //            //{
        //            //    int i = 0;
        //            //    s.prepayments = new SellPrePaymentDocument[s.prepaymentsIDs.Length];
        //            //    for (i = 0; i < s.prepayments.Length; i++)
        //            //        s.prepayments[i] = accounting.GetAccountDocument<SellPrePaymentDocument>(s.prepaymentsIDs[i]);
        //            //    s.deliveries = new SoldItemDeliveryDocument[s.deliverieIDs.Length];
        //            //    for (i = 0; i < s.deliveries.Length; i++)
        //            //        s.deliveries[i] = accounting.GetAccountDocument<SoldItemDeliveryDocument>(s.deliverieIDs[i]);
        //            //    s.salesVoucher = accounting.GetAccountDocument<Sell2Document>(s.AccountDocumentID).salesVoucher;
        //            //}
        //            //docs.AddRange(sells);
        //            //Console.WriteLine("Sells documents loaded");

        //            //docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(PurchasePrePaymentDocument)).id,
        //            //    false, DateTime.Now, DateTime.Now, 0, -1, out N));
        //            //Console.WriteLine("Prepayment documents loaded");

        //            //docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(PurchasedItemDeliveryDocument)).id,
        //            //    false, DateTime.Now, DateTime.Now, 0, -1, out N));
        //            //Console.WriteLine("Purchase item delivery documents loaded");

        //            //docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(SoldItemDeliveryDocument)).id,
        //            //    false, DateTime.Now, DateTime.Now, 0, -1, out N));
        //            //Console.WriteLine("Sold item delivery documents loaded");

        //            Console.WriteLine(docs.Count + " docs documents found");
        //            Console.WriteLine();
        //            c = docs.Count;

        //            for (int i = docs.Count - 1; i >= 0; i--)
        //            {
        //                Console.CursorTop--;
        //                Console.WriteLine((c--) + "                      ");
        //                AccountDocument purchase = docs[i];
        //                DateTime closedUpto = bde.getTransactionsClosedUpto();
        //                if (purchase.DocumentDate <= closedUpto)
        //                {
        //                    continue;
        //                }
        //                bde.WriterHelper.BeginTransaction();
        //                try
        //                {
        //                    accounting.PostGenericDocument(AID, purchase);
        //                    bde.WriterHelper.CommitTransaction();
        //                }
        //                catch (Exception ex)
        //                {
        //                    string log = Guid.NewGuid().ToString();

        //                    System.IO.File.WriteAllText(System.Windows.Forms.Application.StartupPath + "\\log\\" + log, string.Format("Error processing document ID:{0} ref:{1}\n{2}\n{3}", purchase.AccountDocumentID, purchase.PaperRef, ex.Message, ex.StackTrace));
        //                    bde.WriterHelper.RollBackTransaction();
        //                    Console.WriteLine("Error processing document ID:{0} ref:{1}\n{2}\n{3}", purchase.AccountDocumentID, purchase.PaperRef, ex.Message, ex.StackTrace);
        //                    Console.WriteLine();
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
        //            Console.ReadLine();
        //        }
        //    }
        //    finally
        //    {
        //        MessageRepository.dontDoDiff = false;
        //    }
        //}
        //[MaintenanceJobMethod("Accounting-summerization: Move direct entry transactions to summary")]
        public static void accountingSummerizeDirectEntry()
        {
            try
            {
                int N;
                Console.WriteLine("Repositng store transfer documents");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                accounting.UpdateLedger = false;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                accounting.WriterHelper.setReadDB(accounting.DBName);
                try
                {

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-summerization: Move direct entry transactions to summary", "", -1);

                    int c;

                    AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N);
                    Console.WriteLine(docs.Length + " adjustments found");
                    Console.WriteLine();
                    c = docs.Length;
                    int f = 0;

                    INTAPS.CachedObject<int, Account> accounts = new CachedObject<int, Account>(new RetrieveObjectDelegate<int, Account>(delegate(int a) { return accounting.GetAccount<Account>(a); }));
                    for (int i = docs.Length - 1; i >= 0; i--)
                    {
                        Console.CursorTop--;
                        Console.WriteLine("{0}\t{1}                      ", (c--), f);
                        AdjustmentDocument adj = accounting.GetAccountDocument(docs[i].AccountDocumentID, true) as AdjustmentDocument;
                        DateTime closedUpto = bde.getTransactionsClosedUpto();
                        if (adj.DocumentDate <= closedUpto)
                        {
                            continue;
                        }
                        bool hasITEM = false;
                        foreach (AdjustmentAccount ac in adj.adjustmentAccounts)
                        {
                            Account account = accounts[ac.accountID];
                            if (account.Code.IndexOf("ITEM-") == 0)
                            {
                                hasITEM = true;

                                Account parentAccount = accounting.GetAccount<Account>(account.PID);
                                if (parentAccount == null)
                                    throw new ServerUserMessage("Parent account not found to " + account.Code);

                                Account sumAccount;
                                if (parentAccount.Code.IndexOf("ITEM-") == 0)
                                {
                                    sumAccount = accounting.GetAccount<Account>(parentAccount.Code.Substring("ITEM-".Length));
                                }
                                else
                                    sumAccount = null;
                                if (sumAccount == null)
                                    throw new ServerUserMessage("Summary account not found to " + account.Code);
                                ac.accountID = sumAccount.id;
                                ac.accountCode = null;
                            }

                        }
                        if (!hasITEM)
                            continue;
                        f++;
                        bde.WriterHelper.BeginTransaction();
                        try
                        {
                            accounting.PostGenericDocument(AID, adj);
                            bde.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error processing adjustment ID:{0} ref:{1}\n{2}", adj.AccountDocumentID, adj.PaperRef, ex.Message);
                            Console.WriteLine();
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Accounting-inventory: Re-post store transfer documents")]
        public static void accountingInventoryRepostStoreTransfer()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Repositng store transfer documents");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                accounting.WriterHelper.setReadDB(accounting.DBName);
                try
                {

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-inventory: Re-post store transfer documents", "", -1);

                    int c;

                    AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(StoreTransferDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N);
                    Console.WriteLine(docs.Length + " store transfers found");
                    Console.WriteLine();
                    c = docs.Length;

                    for (int i = docs.Length - 1; i >= 0; i--)
                    {
                        Console.CursorTop--;
                        Console.WriteLine((c--) + "                      ");
                        StoreTransferDocument storeTransfer = accounting.GetAccountDocument(docs[i].AccountDocumentID, true) as StoreTransferDocument;
                        DateTime closedUpto = bde.getTransactionsClosedUpto();
                        if (storeTransfer.DocumentDate <= closedUpto)
                        {
                            continue;
                        }
                        bde.WriterHelper.BeginTransaction();
                        try
                        {
                            accounting.PostGenericDocument(AID, storeTransfer);
                            bde.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error processing store transfer ID:{0} ref:{1}\n{2}", storeTransfer.AccountDocumentID, storeTransfer.PaperRef, ex.Message);
                            Console.WriteLine();
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Accounting-summerization: summerize trial balance for closing date")]
        public static void accountingSummerizeTrialBalanceForClosingDate()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Repositng issue documents");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                //accounting.UpdateLedger = false;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                accounting.WriterHelper.setReadDB(accounting.DBName);
                DateTime balanceDate = new DateTime(2014, 7, 8);
                DateTime postDate = new DateTime(2014, 7, 7, 23, 57, 00);
                try
                {

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Accounting-summerization: summerize trial balance for closing date", "", -1);
                    List<Account> itemAccounts = new List<Account>();
                    Console.WriteLine("Loading balances..");
                    itemAccounts.AddRange(accounting.GetLeafAccounts<Account>(accounting.GetAccountID<Account>("ITEM-1000")));
                    itemAccounts.AddRange(accounting.GetLeafAccounts<Account>(accounting.GetAccountID<Account>("ITEM-2000")));
                    itemAccounts.AddRange(accounting.GetLeafAccounts<Account>(accounting.GetAccountID<Account>("ITEM-4000")));
                    itemAccounts.AddRange(accounting.GetLeafAccounts<Account>(accounting.GetAccountID<Account>("ITEM-5000")));
                    itemAccounts.AddRange(accounting.GetLeafAccounts<Account>(accounting.GetAccountID<Account>("ITEM-6000")));
                    int c;

                    Dictionary<int, AccountBalance> balances = new Dictionary<int, AccountBalance>();
                    c = itemAccounts.Count;
                    Console.WriteLine();
                    TransactionSummerizer t = new TransactionSummerizer(accounting, new string[] { "ITEM" });
                    CostCenter[] leaveCostCenters = accounting.GetLeafAccounts<CostCenter>(1);
                    foreach (Account ac in itemAccounts)
                    {
                        Console.CursorTop--;
                        Console.WriteLine("{0}\t{1}\t{2}          ", c--, balances.Count, t.summary.Count);
                        AccountBalance bal = accounting.GetBalanceAsOf(accounting.GetCostCenterAccountID(1, ac.id), 0, balanceDate);
                        if (AccountBase.AmountEqual(bal.DebitBalance, 0))
                            continue;
                        foreach (CostCenter cs in leaveCostCenters)
                        {
                            int csaID = accounting.GetCostCenterAccountID(cs.id, ac.id);
                            if (csaID != -1)
                            {


                                AccountBalance csBal = accounting.GetBalanceAsOf(csaID, 0, balanceDate);
                                if (AccountBase.AmountEqual(csBal.DebitBalance, 0))
                                    continue;
                                t.addTransaction(AID, new TransactionOfBatch(
                                    csaID, csBal.DebitBalance, "Begining balance summerization"
                                    ));
                            }
                        }
                    }
                    if (t.summary.Count == 0)
                    {
                        Console.WriteLine("Nothing to summerize");
                        return;
                    }
                    Console.WriteLine("Prepareing to post {0} summerizations", t.summary.Count);
                    AdjustmentDocument adj = new AdjustmentDocument();
                    adj.voucher = new DocumentTypedReference(accounting.getDocumentSerialType("JV").id, "2007-SUM", true);
                    adj.DocumentDate = postDate;
                    adj.ShortDescription = "Summerization of begining blaance";
                    adj.adjustmentAccounts = new AdjustmentAccount[1 + t.summary.Count];
                    TransactionOfBatch[] sumTran = t.getSummary();
                    double netBalance = 0;
                    for (int i = 0; i < sumTran.Length; i++)
                    {
                        TransactionOfBatch tran = sumTran[i];
                        CostCenterAccount csa = accounting.GetCostCenterAccount(tran.AccountID);
                        adj.adjustmentAccounts[i] = new AdjustmentAccount()
                        {
                            accountCode = accounting.GetAccount<Account>(csa.accountID).Code,
                            accountID = csa.accountID,
                            costCenterID = csa.costCenterID,
                            debitAmount = tran.Amount > 0 ? tran.Amount : 0,
                            creditAmount = tran.Amount < 0 ? -tran.Amount : 0,
                            description = "Summerization of begining blaance",
                        };
                        netBalance += tran.Amount;
                    }

                    CostCenterAccount itemBalAccount = accounting.GetOrCreateCostCenterAccount(
                        AID,
                        bde.SysPars.mainCostCenterID,
                        accounting.GetAccount<Account>("ITEM-7000").id
                        );
                    double itemBalAmount = accounting.GetBalanceAsOf(itemBalAccount.id, 0, balanceDate).DebitBalance;
                    Console.WriteLine("ITEM 7000 balance {0}", itemBalAmount);
                    Console.ReadLine();
                    //netBalance += itemBalAmount;
                    netBalance = -netBalance;
                    adj.adjustmentAccounts[sumTran.Length] = new AdjustmentAccount()
                    {
                        accountCode = accounting.GetAccount<Account>(itemBalAccount.accountID).Code,
                        accountID = itemBalAccount.accountID,
                        costCenterID = itemBalAccount.costCenterID,
                        debitAmount = netBalance > 0 ? netBalance : 0,
                        creditAmount = netBalance < 0 ? -netBalance : 0,
                        description = "Summerization of begining blaance",
                    };
                    Console.WriteLine("Posting summerization transaction");
                    accounting.WriterHelper.BeginTransaction();
                    try
                    {
                        accounting.PostGenericDocument(AID, adj);
                        accounting.WriterHelper.CommitTransaction();
                    }
                    catch
                    {
                        accounting.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        
        [MaintenanceJobMethod("Accounting-summerization: summerize without reposting")]
        public static void accountingSummerizeItemTransactionsWithoutReposting()
        {


            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Summerizing item transactions without complete repost");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                accounting.UpdateLedger = true;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;

                INTAPS.CachedObject<int, string> accountCodes = new INTAPS.CachedObject<int, string>
            (
            new RetrieveObjectDelegate<int, string>(delegate(int id)
            {
                CostCenterAccount csa = accounting.GetCostCenterAccount(id);
                return accounting.GetAccount<Account>(csa.accountID).Code;
            })
            );

                accounting.WriterHelper.setReadDB(accounting.DBName);

                try
                {

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Accounting-summerization: Re-post purchase,sales, etc", "", -1);
                    ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
                    int c;
                    Console.WriteLine("Loading documents..");
                    List<AccountDocument> docs = new List<AccountDocument>();
                    //docs.Add(accounting.GetAccountDocument(519683, true));

                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(PurchasePrePaymentDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(PurchasedItemDeliveryDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(SellPrePaymentDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(SoldItemDeliveryDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));

                    Console.WriteLine(docs.Count + " docs documents found");
                    Console.WriteLine();
                    c = docs.Count;

                    for (int i = docs.Count - 1; i >= 0; i--)
                    {
                        Console.CursorTop--;
                        Console.WriteLine((c--) + "                      ");
                        AccountDocument theDocument = docs[i];
                        DateTime closedUpto = bde.getTransactionsClosedUpto();
                        if (theDocument.DocumentDate <= closedUpto)
                        {
                            continue;
                        }
                        bde.WriterHelper.BeginTransaction();
                        try
                        {
                            AccountTransaction[] tran = accounting.GetTransactionsOfDocument(theDocument.AccountDocumentID);
                            INTAPS.Accounting.BDE.TransactionSummerizer sum = new TransactionSummerizer(accounting, new string[] { "ITEM" });
                            bool alreadySummerized = false;
                            foreach (AccountTransaction t in tran)
                                if (accountCodes[t.AccountID].Equals("ITEM-7000"))
                                {
                                    alreadySummerized = true;
                                    break;
                                }
                            if (!alreadySummerized)
                            {
                                foreach (AccountTransaction t in tran)
                                    if (accountCodes[t.AccountID].IndexOf("ITEM") == 0)
                                        sum.addTransaction(AID, t);
                                TransactionOfBatch[] sumTran = sum.getSummary();
                                if (sumTran.Length > 0)
                                {
                                    double vnet = 0;
                                    double qnet = 0;
                                    foreach (TransactionOfBatch t in sumTran)
                                        if (t.ItemID == 0)
                                            vnet += t.Amount;
                                        else
                                            qnet += t.Amount;
                                    List<TransactionOfBatch> tranList = new List<TransactionOfBatch>(TransactionOfBatch.fromAccountTransaction(tran));
                                    tranList.AddRange(sumTran);
                                    CostCenterAccount csa = accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.summerizationContraAccountID);
                                    if (!AccountBase.AmountEqual(vnet, 0))
                                        tranList.Add(new TransactionOfBatch(csa.id, -vnet, "Summerization in batch by job"));
                                    if (!AccountBase.AmountEqual(qnet, 0))
                                        tranList.Add(new TransactionOfBatch(csa.id, TransactionItem.MATERIAL_QUANTITY, -qnet, "Summerization in batch by job"));


                                    List<DocumentTypedReference> refs = new List<DocumentTypedReference>();
                                    foreach (DocumentSerial s in accounting.getDocumentSerials(theDocument.AccountDocumentID))
                                        refs.Add(s.typedReference);
                                    accounting.UpdateTransaction(AID, theDocument, tranList.ToArray(), false, refs.ToArray());
                                }
                            }
                            bde.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error processing document ID:{0} ref:{1}\n{2}\n{3}", theDocument.AccountDocumentID, theDocument.PaperRef, ex.Message, ex.StackTrace);
                            Console.WriteLine();
                        }
                        //finally
                        //{
                        //    bde.WriterHelper.CheckTransactionIntegrity();
                        //}
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Accounting-Taxation: Add Input VAT to Items Cost for purchase documents")]
        public static void accountingAddInputVATToItemCost()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Adding Input VAT to Items cost");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                accounting.UpdateLedger = false;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                accounting.WriterHelper.setReadDB(accounting.DBName);
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-Taxation: Add Input VAT to Item Cost for purchase documents", "", -1);

                    int c;
                    AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                                   false, DateTime.Now, DateTime.Now, 0, -1, out N);
                    // AccountDocument[] docs = new AccountDocument[] { accounting.GetAccountDocument(991, true) };
                    Console.WriteLine(docs.Length + " purchases found");
                    Console.WriteLine();
                    c = docs.Length;
                    for (int i = docs.Length - 1; i >= 0; i--)
                    {
                        Console.CursorTop--;
                        Console.WriteLine((c--) + "                      ");
                        Purchase2Document purchase = accounting.GetAccountDocument(docs[i].AccountDocumentID, true) as Purchase2Document;
                        int j = 0;
                        purchase.prepayments = new PurchasePrePaymentDocument[purchase.prepaymentsIDs.Length];
                        for (j = 0; j < purchase.prepayments.Length; j++)
                            purchase.prepayments[j] = accounting.GetAccountDocument<PurchasePrePaymentDocument>(purchase.prepaymentsIDs[j]);
                        purchase.deliveries = new PurchasedItemDeliveryDocument[purchase.deliverieIDs.Length];
                        for (j = 0; j < purchase.deliveries.Length; j++)
                            purchase.deliveries[j] = accounting.GetAccountDocument<PurchasedItemDeliveryDocument>(purchase.deliverieIDs[j]);
                        purchase.paymentVoucher = accounting.GetAccountDocument<Purchase2Document>(purchase.AccountDocumentID).paymentVoucher;

                        bool isVATTaxType = false;
                        foreach (TaxImposed tax in purchase.taxImposed)
                        {
                            if (tax.TaxType == TaxType.VAT)
                            {
                                isVATTaxType = true;
                                break;
                            }
                        }
                        if (isVATTaxType)
                        {
                            //purchase.paymentVoucher = accounting.getDocumentSerials(purchase.AccountDocumentID)[0].typedReference;
                            DateTime closedUpto = bde.getTransactionsClosedUpto();
                            if (purchase.DocumentDate <= closedUpto)
                            {
                                continue;
                            }

                            try
                            {
                                bde.WriterHelper.BeginTransaction();
                                accounting.PostGenericDocument(AID, purchase);
                                // accounting.UpdateTransaction(AID, purchase, TransactionOfBatch.fromAccountTransaction(transactions.ToArray()), false, refs.ToArray());
                                bde.WriterHelper.CommitTransaction();
                            }
                            catch (Exception ex)
                            {
                                bde.WriterHelper.RollBackTransaction();
                                Console.WriteLine("Error processing purchase:{0} ref:{1}\n{2}", purchase.AccountDocumentID, purchase.PaperRef, ex.Message);
                                Console.WriteLine();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Accounting-Inventory: Repost issue documents of an inventory item")]
        public static void RepostAllIssueDocumentsOfAnItem()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Repositng store issue documents");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                bde.WriterHelper.setReadDB(bde.DBName);
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-inventory: Re-post store transfer documents", "", -1);
                    int n;
                    AccountBalance bal;
                    
                    string itemCode = "010001";
                    TransactionItems item = null;
                    while (true)
                    {
                        Console.Write("Enter item code:");
                        itemCode = Console.ReadLine();
                        item = bde.GetTransactionItems(itemCode);
                        if (item != null && item.IsInventoryItem && (item.inventoryAccountID != -1 && item.expenseAccountID != -1))
                        {
                            Console.WriteLine(item.NameCode);
                            break;
                        }

                    }
                    accounting.WriterHelper.setReadDB(accounting.DBName);
                    {
                        //Find issued documents of the expense account
                        CostCenterAccount csAccount = accounting.GetCostCenterAccount(bde.SysPars.mainCostCenterID, item.expenseAccountID);
                        AccountTransaction[] tran = accounting.GetTransactions(csAccount.id, 0, false, DateTime.Now, DateTime.Now, new int[] { accounting.GetDocumentTypeByType(typeof(StoreIssueDocument)).id }, 0, -1, out n, out bal);
                        Console.WriteLine(tran.Length + " store issues found");
                        Console.WriteLine();
                        int c = tran.Length;
                        foreach (AccountTransaction t in tran)
                        {
                            //repost issue document
                            Console.CursorTop--;
                            Console.WriteLine((c--) + "                      ");
                            AccountDocument issueDoc = accounting.GetAccountDocument(t.documentID, true);

                            accounting.WriterHelper.BeginTransaction();
                            try
                            {
                                accounting.PostGenericDocument(AID, issueDoc);
                                accounting.WriterHelper.CommitTransaction();
                            }
                            catch (Exception ex)
                            {
                                accounting.WriterHelper.RollBackTransaction();
                                Console.WriteLine("Error processing store issue ID:{0} ref:{1}\n{2}", t.documentID, issueDoc.PaperRef, ex.Message);
                                Console.WriteLine();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Accounting-summerization:summarize delivery documents only for Summary accounts")]
        public static void accountingSummarizeDeliveryDocumentsOnly()
        {


            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Summerizing item transactions without complete repost");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                accounting.UpdateLedger = false;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;

                INTAPS.CachedObject<int, string> accountCodes = new INTAPS.CachedObject<int, string>
            (
            new RetrieveObjectDelegate<int, string>(delegate(int id)
            {
                CostCenterAccount csa = accounting.GetCostCenterAccount(id);
                return accounting.GetAccount<Account>(csa.accountID).Code;
            })
            );

                accounting.WriterHelper.setReadDB(accounting.DBName);

                try
                {

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Accounting-summerization: Re-post purchase,sales, etc", "", -1);
                    ObjectFilters.GetFilterInstance<DeclaredDocumentFilter>().allowAction(AID);
                    int c;
                    Console.WriteLine("Loading documents..");
                    List<AccountDocument> docs = new List<AccountDocument>();
                    //docs.Add(accounting.GetAccountDocument(519683, true));

                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(PurchasedItemDeliveryDocument)).id,
                                    false, DateTime.Now, DateTime.Now, 0, -1, out N));

                    Console.WriteLine(docs.Count + " docs documents found");
                    Console.WriteLine();
                    c = docs.Count;

                    for (int i = docs.Count - 1; i >= 0; i--)
                    {
                        Console.CursorTop--;
                        Console.WriteLine((c--) + "                      ");
                        AccountDocument theDocument = docs[i];
                        DateTime closedUpto = bde.getTransactionsClosedUpto();
                        if (theDocument.DocumentDate <= closedUpto)
                        {
                            continue;
                        }
                        bde.WriterHelper.BeginTransaction();
                        try
                        {
                            AccountTransaction[] tran = accounting.GetTransactionsOfDocument(theDocument.AccountDocumentID);
                            List<AccountTransaction> tranSummary = new List<AccountTransaction>();
                            foreach (AccountTransaction tt in tran)
                            {
                                if (accountCodes[tt.AccountID].Contains("ITEM"))
                                    tranSummary.Add(tt);
                            }
                            tran = tranSummary.ToArray();
                            INTAPS.Accounting.BDE.TransactionSummerizer sum = new TransactionSummerizer(accounting, new string[] { "ITEM" });

                            foreach (AccountTransaction t in tran)
                                if (accountCodes[t.AccountID].IndexOf("ITEM") == 0)
                                    sum.addTransaction(AID, t);
                            TransactionOfBatch[] sumTran = sum.getSummary();
                            if (sumTran.Length > 0)
                            {
                                double vnet = 0;
                                double qnet = 0;
                                foreach (TransactionOfBatch t in sumTran)
                                    if (t.ItemID == 0)
                                        vnet += t.Amount;
                                    else
                                        qnet += t.Amount;
                                List<TransactionOfBatch> tranList = new List<TransactionOfBatch>(TransactionOfBatch.fromAccountTransaction(tran));
                                tranList.AddRange(sumTran);
                                CostCenterAccount csa = accounting.GetOrCreateCostCenterAccount(AID, bde.SysPars.mainCostCenterID, bde.SysPars.summerizationContraAccountID);
                                if (!AccountBase.AmountEqual(vnet, 0))
                                    tranList.Add(new TransactionOfBatch(csa.id, -vnet, "Summerization in batch by job"));
                                if (!AccountBase.AmountEqual(qnet, 0))
                                    tranList.Add(new TransactionOfBatch(csa.id, TransactionItem.MATERIAL_QUANTITY, -qnet, "Summerization in batch by job"));


                                List<DocumentTypedReference> refs = new List<DocumentTypedReference>();
                                foreach (DocumentSerial s in accounting.getDocumentSerials(theDocument.AccountDocumentID))
                                    refs.Add(s.typedReference);
                                accounting.UpdateTransaction(AID, theDocument, tranList.ToArray(), false, refs.ToArray());
                            }

                            bde.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Error processing document ID:{0} ref:{1}\n{2}\n{3}", theDocument.AccountDocumentID, theDocument.PaperRef, ex.Message, ex.StackTrace);
                            Console.WriteLine();
                        }
                        //finally
                        //{
                        //    bde.WriterHelper.CheckTransactionIntegrity();
                        //}
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Accounting-summerization(Impact RS): Summarize direct entry accounts from log")]
        public static void accountingSummarizeDirectEntryFromLog()
        {
            int N;
            Console.WriteLine("Summarizing direct entriy documents");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            //accounting.UpdateLedger = false;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            DataTable oldVersion = accounting.WriterHelper.GetDataTable(@"Select ID,DocumentData from Accounting_2006.dbo.Document_Deleted d
where 
exists (Select * from Accounting_2006.dbo.MovedDE
where MovedDE.__AID=d.__DAID and MovedDE.ID=d.ID)
");
            int f = 0;
            INTAPS.CachedObject<int, Account> accounts = new CachedObject<int, Account>(new RetrieveObjectDelegate<int, Account>(delegate(int a) { return accounting.GetAccount<Account>(a); }));

            try
            {

                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-summerization: Summarize direct entry documents by ITEM", "", -1);
                CostCenterAccount itemBalAccount = accounting.GetOrCreateCostCenterAccount(
                                AID,
                                bde.SysPars.mainCostCenterID,
                                accounting.GetAccount<Account>("ITEM-7000").id
                                );

                int c = oldVersion.Rows.Count;
                foreach (DataRow row in oldVersion.Rows)
                {
                    Console.WriteLine("{0}\tDone: {1}     ", c--, f);

                    int documentID = (int)row[0];
                    AdjustmentDocument adjDoc = AccountDocument.DeserializeXML((string)row[1], typeof(AdjustmentDocument)) as AdjustmentDocument;
                    bool hasITEM = false;
                    List<AdjustmentAccount> sumAdjustments = new List<AdjustmentAccount>();
                    double netSumTransation = 0;
                    foreach (AdjustmentAccount ac in adjDoc.adjustmentAccounts)
                    {
                        Account account = accounts[ac.accountID];
                        if (account.Code.IndexOf("ITEM-") == 0)
                        {
                            hasITEM = true;
                            Account parentAccount = accounting.GetAccount<Account>(account.PID);
                            if (parentAccount == null)
                                throw new ServerUserMessage("Parent account not found to " + account.Code);

                            Account sumAccount;
                            if (parentAccount.Code.IndexOf("ITEM-") == 0)
                            {
                                sumAccount = accounting.GetAccount<Account>(parentAccount.Code.Substring("ITEM-".Length));
                            }
                            else
                                sumAccount = null;
                            if (sumAccount == null)
                                throw new ServerUserMessage("Summary account not found to " + account.Code);
                            sumAdjustments.Add(new AdjustmentAccount()
                            {
                                accountID = sumAccount.id,
                                costCenterID = ac.costCenterID,
                                debitAmount = ac.debitAmount,
                                creditAmount = ac.creditAmount,
                            }
                            );
                            netSumTransation += ac.debitAmount - ac.creditAmount;
                        }
                    }
                    if (!AccountBase.AmountEqual(netSumTransation, 0))
                    {
                        sumAdjustments.Add(new AdjustmentAccount()
                        {
                            accountID = itemBalAccount.accountID,
                            costCenterID = itemBalAccount.costCenterID,
                            debitAmount = netSumTransation < 0 ? -netSumTransation : 0,
                            creditAmount = netSumTransation > 0 ? netSumTransation : 0
                        }
                           );
                    }
                    if (!hasITEM)
                        continue;
                    f++;
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        adjDoc.AccountDocumentID = documentID;
                        adjDoc.adjustmentAccounts = ArrayExtension.mergeArray(adjDoc.adjustmentAccounts, sumAdjustments.ToArray());
                        DocumentSerial[] s = accounting.getDocumentSerials(documentID);
                        if (s.Length > 0)
                            adjDoc.voucher = s[0].typedReference;
                        accounting.PostGenericDocument(AID, adjDoc);

                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine("Error processing adjustment ID:{0} ref:{1}\n{2}", adjDoc.AccountDocumentID, adjDoc.PaperRef, ex.Message);
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }

        [MaintenanceJobMethod("Accounting-summerization: Summarize direct entry ITEM accounts")]
        public static void accountingSummarizeDirectEntryITEM()
        {
            int N;
            Console.WriteLine("Summarizing direct entriy documents");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            //accounting.UpdateLedger = false;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            //AccountDocument[] docs = new AccountDocument[] { accounting.GetAccountDocument(688437, true) };
            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id,
                            false, DateTime.Now, DateTime.Now, 0, -1, out N);
            Console.WriteLine(docs.Length + " adjustments found");
            Console.WriteLine();
            int f = 0;
            INTAPS.CachedObject<int, Account> accounts = new CachedObject<int, Account>(new RetrieveObjectDelegate<int, Account>(delegate(int a) { return accounting.GetAccount<Account>(a); }));

            try
            {

                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-summerization: Summarize direct entry documents by ITEM", "", -1);
                CostCenterAccount itemBalAccount = accounting.GetOrCreateCostCenterAccount(
                                AID,
                                bde.SysPars.mainCostCenterID,
                                accounting.GetAccount<Account>("ITEM-7000").id
                                );

                int c = docs.Length;
                for (int i = docs.Length - 1; i >= 0; i--)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\tDone: {1}     ", c--, f);

                    AdjustmentDocument adjDoc = accounting.GetAccountDocument(docs[i].AccountDocumentID, true) as AdjustmentDocument;
                    DateTime closedUpto = bde.getTransactionsClosedUpto();
                    if (adjDoc.DocumentDate <= closedUpto)
                    {
                        continue;
                    }
                    bool hasITEM = false;
                    List<AdjustmentAccount> sumAdjustments = new List<AdjustmentAccount>();
                    double netSumTransation = 0;
                    foreach (AdjustmentAccount ac in adjDoc.adjustmentAccounts)
                    {
                        Account account = accounts[ac.accountID];
                        if (account.Code.IndexOf("ITEM-") == 0 && ac.accountID != itemBalAccount.accountID)
                        {
                            hasITEM = true;
                            Account parentAccount = accounting.GetAccount<Account>(account.PID);
                            if (parentAccount == null)
                                throw new ServerUserMessage("Parent account not found to " + account.Code);

                            Account sumAccount;
                            if (parentAccount.Code.IndexOf("ITEM-") == 0)
                            {
                                sumAccount = accounting.GetAccount<Account>(parentAccount.Code.Substring("ITEM-".Length));
                            }
                            else
                                sumAccount = null;
                            if (sumAccount == null)
                                throw new ServerUserMessage("Summary account not found to " + account.Code);
                            bool alreadySummarized=false;
                            foreach (AdjustmentAccount adjacc in adjDoc.adjustmentAccounts)
                            {
                                if(adjacc.accountID==sumAccount.id)
                                {
                                    alreadySummarized = true;
                                    break;
                                }
                            }
                            if (!alreadySummarized)
                            {
                                sumAdjustments.Add(new AdjustmentAccount()
                                {
                                    accountID = sumAccount.id,
                                    costCenterID = ac.costCenterID,
                                    debitAmount = ac.debitAmount,
                                    creditAmount = ac.creditAmount,
                                }
                                );

                                netSumTransation += ac.debitAmount - ac.creditAmount;
                            }
                            else
                            {
                                netSumTransation = 0;
                                break;
                            }
                        }
                    }
                    if (!AccountBase.AmountEqual(netSumTransation, 0))
                    {
                        sumAdjustments.Add(new AdjustmentAccount()
                        {
                            accountID = itemBalAccount.accountID,
                            costCenterID = itemBalAccount.costCenterID,
                            debitAmount = netSumTransation < 0 ? -netSumTransation : 0,
                            creditAmount = netSumTransation > 0 ? netSumTransation : 0
                        }
                           );
                    }
                    if (!hasITEM)
                    {
                        f++;
                        continue;
                    }
                    f++;
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        adjDoc.AccountDocumentID = docs[i].AccountDocumentID;
                        adjDoc.adjustmentAccounts = ArrayExtension.mergeArray(adjDoc.adjustmentAccounts, sumAdjustments.ToArray());
                        DocumentSerial[] s = accounting.getDocumentSerials(docs[i].AccountDocumentID);
                        if (s.Length > 0)
                            adjDoc.voucher = s[0].typedReference;
                        accounting.PostGenericDocument(AID, adjDoc);

                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine("Error processing adjustment ID:{0} ref:{1}\n{2}", adjDoc.AccountDocumentID, adjDoc.PaperRef, ex.Message);
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }

        [MaintenanceJobMethod("Accounting-Inventory: zero out all inventory items")]
        public static void zeroOutAllInventoryItems()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                DateTime balanceTime;
                DateTime inventoryTime;
                string reference;
                do
                {
                    Console.Write("Enter balance time:");
                    if (DateTime.TryParse(Console.ReadLine(), out balanceTime))
                        break;
                }
                while (true);

                do
                {
                    Console.Write("Enter inventory time:");
                    if (DateTime.TryParse(Console.ReadLine(), out inventoryTime))
                        break;
                }
                while (true);
                Console.Write("Enter jv reference:");
                reference = Console.ReadLine();

                

                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                
                StoreInfo[] stores=bde.GetAllStores();
                int si=1;
                foreach(StoreInfo s in stores)
                {
                    Console.WriteLine("{0}: {1}",si,stores[si-1].nameCode);
                    si++;
                }
                StoreInfo store=null;
                while(true)
                {
                    Console.Write("Select store: ");
                    int n;
                    if(int.TryParse(Console.ReadLine(),out n))
                    {
                        if(n>0 && n<=stores.Length)
                        {
                            store=stores[n-1];
                            break;
                        }
                    }
                }

                bde.WriterHelper.setReadDB(bde.DBName);
                Console.WriteLine("Loading all transaction items..");
                TransactionItems[] items = bde.WriterHelper.GetSTRArrayByFilter<TransactionItems>("IsInventoryItem=1");
                Console.WriteLine("Checking balances..");
                List<TransactionDocumentItem> itemsWithBalances = new List<TransactionDocumentItem>();
                Console.WriteLine();
                int c = items.Length;
                foreach (TransactionItems item in items)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                  ", c--, itemsWithBalances.Count);
                    if (item.inventoryAccountID == -1)
                        continue;
                    double quantity = accounting.GetNetBalanceAsOf(accounting.GetCostCenterAccountID(store.costCenterID, item.inventoryAccountID), TransactionItem.DEFAULT_CURRENCY, balanceTime);
                    double amount = accounting.GetNetBalanceAsOf(accounting.GetCostCenterAccountID(store.costCenterID, item.inventoryAccountID), TransactionItem.MATERIAL_QUANTITY, balanceTime);
                    if (item.Code.Equals("001452"))
                    {
                        Console.WriteLine("q:{0} p:{1}", quantity, amount);
                        Console.ReadLine();
                    }
                    if (AccountBase.AmountEqual(quantity, 0) && AccountBase.AmountEqual(amount, 0))
                        continue;
                    itemsWithBalances.Add(new TransactionDocumentItem(store.costCenterID, item, 0, 0));
                }


                Console.WriteLine("Balance time: {0}", balanceTime);
                Console.WriteLine("Inventory time: {0}", inventoryTime);
                Console.WriteLine("JV: {0}", reference);
                Console.WriteLine("Store: {0}", store.nameCode);
                Console.WriteLine("Number of items: {0}", itemsWithBalances.Count);
                Console.Write("Enter to continue");
                Console.ReadLine();

                bde.WriterHelper.setReadDB(bde.DBName);
                bde.WriterHelper.BeginTransaction();
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Accounting-Inventory: zero out all inventory items", "", -1);
                    Console.WriteLine("Posting inventory document...");
                    InventoryDocument inventory = new InventoryDocument();
                    inventory.AccountDocumentID = -1;
                    inventory.DocumentDate = inventoryTime;
                    inventory.voucher = new DocumentTypedReference(accounting.getDocumentSerialType("JV").id, reference, true);
                    inventory.PaperRef = inventory.voucher.reference;
                    inventory.storeID = store.costCenterID;
                    inventory.items = itemsWithBalances.ToArray();
                    inventory.ShortDescription = "Automatically zeroed out inventory items";
                    accounting.PostGenericDocument(AID, inventory);
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }


      

        [MaintenanceJobMethod("Accounting-Inventory: Issue all inventory delivered items of budger year 2007E.C (Diredawa-Special)")]
        public static void IssueAllDeliveryItems()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Issuing all inventory items");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                bde.WriterHelper.setReadDB(bde.DBName);
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-Inventory: Issue all inventory items", "", -1);
                    const int hqStoreID = 141;
                    accounting.WriterHelper.setReadDB(accounting.DBName);

                    Console.WriteLine("Loading documents..");
                    List<AccountDocument> docs = new List<AccountDocument>();
                    docs.AddRange(accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(PurchasedItemDeliveryDocument)).id,
                                    true, new DateTime(2013, 7, 8), new DateTime(2015, 7, 8), 0, -1, out N));

                    Console.WriteLine(docs.Count + " delivery documents found");
                    Console.WriteLine("Preparing deliveries to issue...");
                    Console.WriteLine();
                    int c = docs.Count;
                    List<TransactionDocumentItem> items = new List<TransactionDocumentItem>();
                    bde.WriterHelper.BeginTransaction();

                    foreach (AccountDocument delivDoc in docs)
                    {
                        Console.CursorTop--;
                        Console.WriteLine((c--) + "                      ");
                        PurchasedItemDeliveryDocument delivery = (PurchasedItemDeliveryDocument)delivDoc;
                        //var docSerials = accounting.getDocumentSerials(delivery.AccountDocumentID);
                        //DocumentSerialType grv = accounting.getDocumentSerialType("SRV");
                        if (!string.IsNullOrEmpty(delivery.PaperRef))
                        {

                            if (delivery.PaperRef.Equals("00501") || delivery.PaperRef.Equals("00522") || delivery.PaperRef.Equals("04904")
                                || delivery.PaperRef.Equals("04911") || delivery.PaperRef.Equals("00538") || delivery.PaperRef.Equals("SIV"))
                                continue;
                        }

                        foreach (TransactionDocumentItem tranItem in delivery.items)
                        {
                            items.Add(tranItem);
                        }
                    }
                    Console.WriteLine("Posting issue document...");

                    var query = items.GroupBy(a => new { a.code })
                                    .Select(a => new { Quantity = a.Sum(b => b.quantity), Key = a.Key })
                                    .ToList();


                    List<TransactionDocumentItem> uniquifiedList = new List<TransactionDocumentItem>();
                    foreach (var t in query)
                    {
                        TransactionItems tranItem = bde.GetTransactionItems(t.Key.code);
                        TransactionDocumentItem docItem = new TransactionDocumentItem(bde.SysPars.mainCostCenterID, tranItem, t.Quantity, 0d);
                        docItem.directExpense = true;
                        if (tranItem.expenseAccountID == -1)
                            throw new Exception("Expense account not found for item code:" + tranItem.Code);
                        if (tranItem.expenseSummaryAccountID == -1)
                            throw new Exception("Summary expense account not found for item code:" + tranItem.Code);

                        uniquifiedList.Add(docItem);
                    }
                    StoreIssueDocument issueDoc = new StoreIssueDocument();
                    issueDoc.AccountDocumentID = -1;
                    issueDoc.DocumentDate = new DateTime(2015, 7, 6);
                    issueDoc.voucher = new DocumentTypedReference(accounting.getDocumentSerialType("SIV").id, "2007", true);
                    issueDoc.PaperRef = issueDoc.voucher.reference;
                    issueDoc.storeID = hqStoreID;
                    issueDoc.items = uniquifiedList.ToArray();
                    issueDoc.ShortDescription = "Automatically Issued from system for 2007E.C";
                    accounting.PostGenericDocument(AID, issueDoc);
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }


        [MaintenanceJobMethod("Accounting-summerization: Summarize direct entry ITEM accounts by prompt")]
        public static void accountingSummaryDirectEntryITEMByPrompt()
        {
            int N;
            Console.WriteLine("Summarizing direct entriy documents");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            //accounting.UpdateLedger = false;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            bool keyEntered = false;
            int docID;
            AccountDocument accountDoc = null;
            while (!keyEntered)
            {
                Console.WriteLine("Enter document ID");
                string read = Console.ReadLine();
                if(int.TryParse(read,out docID))
                {
                    accountDoc = accounting.GetAccountDocument(docID, true);
                    keyEntered = true;
                }
                else
                {
                    Console.WriteLine("Invalid document id");
                    keyEntered = false;
                }
            }
            //AccountDocument[] docs = new AccountDocument[] { accounting.GetAccountDocument(688437, true) };
            AccountDocument[] docs = new AccountDocument[] { accountDoc };
            Console.WriteLine(docs.Length + " adjustments found");
            Console.WriteLine();
            int f = 0;
            INTAPS.CachedObject<int, Account> accounts = new CachedObject<int, Account>(new RetrieveObjectDelegate<int, Account>(delegate(int a) { return accounting.GetAccount<Account>(a); }));

            try
            {

                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-summerization: Summarize direct entry documents by ITEM", "", -1);
                CostCenterAccount itemBalAccount = accounting.GetOrCreateCostCenterAccount(
                                AID,
                                bde.SysPars.mainCostCenterID,
                                accounting.GetAccount<Account>("ITEM-7000").id
                                );

                int c = docs.Length;
                for (int i = docs.Length - 1; i >= 0; i--)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\tDone: {1}     ", c--, f);

                    AdjustmentDocument adjDoc = accounting.GetAccountDocument(docs[i].AccountDocumentID, true) as AdjustmentDocument;
                    DateTime closedUpto = bde.getTransactionsClosedUpto();
                    if (adjDoc.DocumentDate <= closedUpto)
                    {
                        continue;
                    }
                    bool hasITEM = false;
                    List<AdjustmentAccount> sumAdjustments = new List<AdjustmentAccount>();
                    double netSumTransation = 0;
                    foreach (AdjustmentAccount ac in adjDoc.adjustmentAccounts)
                    {
                        Account account = accounts[ac.accountID];
                        if (account.Code.IndexOf("ITEM-") == 0 && ac.accountID != itemBalAccount.accountID)
                        {
                            hasITEM = true;
                            Account parentAccount = accounting.GetAccount<Account>(account.PID);
                            if (parentAccount == null)
                                throw new ServerUserMessage("Parent account not found to " + account.Code);

                            Account sumAccount;
                            if (parentAccount.Code.IndexOf("ITEM-") == 0)
                            {
                                sumAccount = accounting.GetAccount<Account>(parentAccount.Code.Substring("ITEM-".Length));
                            }
                            else
                                sumAccount = null;
                            if (sumAccount == null)
                                throw new ServerUserMessage("Summary account not found to " + account.Code);
                            bool alreadySummarized = false;
                            foreach (AdjustmentAccount adjacc in adjDoc.adjustmentAccounts)
                            {
                                if (adjacc.accountID == sumAccount.id)
                                {
                                    alreadySummarized = true;
                                    break;
                                }
                            }
                            if (!alreadySummarized)
                            {
                                sumAdjustments.Add(new AdjustmentAccount()
                                {
                                    accountID = sumAccount.id,
                                    costCenterID = ac.costCenterID,
                                    debitAmount = ac.debitAmount,
                                    creditAmount = ac.creditAmount,
                                }
                                );

                                netSumTransation += ac.debitAmount - ac.creditAmount;
                            }
                            else
                            {
                                netSumTransation = 0;
                                break;
                            }
                        }
                    }
                    if (!AccountBase.AmountEqual(netSumTransation, 0))
                    {
                        sumAdjustments.Add(new AdjustmentAccount()
                        {
                            accountID = itemBalAccount.accountID,
                            costCenterID = itemBalAccount.costCenterID,
                            debitAmount = netSumTransation < 0 ? -netSumTransation : 0,
                            creditAmount = netSumTransation > 0 ? netSumTransation : 0
                        }
                           );
                    }
                    if (!hasITEM)
                    {
                        f++;
                        continue;
                    }
                    f++;
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        adjDoc.AccountDocumentID = docs[i].AccountDocumentID;
                        adjDoc.adjustmentAccounts = ArrayExtension.mergeArray(adjDoc.adjustmentAccounts, sumAdjustments.ToArray());
                        DocumentSerial[] s = accounting.getDocumentSerials(docs[i].AccountDocumentID);
                        if (s.Length > 0)
                            adjDoc.voucher = s[0].typedReference;
                        accounting.PostGenericDocument(AID, adjDoc);

                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine("Error processing adjustment ID:{0} ref:{1}\n{2}", adjDoc.AccountDocumentID, adjDoc.PaperRef, ex.Message);
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
        [MaintenanceJobMethod("Accounting-budget: mark none cash transactions as unbudgeted")]
        public static void budgetingMarkNoCashTransactions()
        {
            int N;
            Console.WriteLine("Marking no cash transaction as not budgeted");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            accounting.WriterHelper.setReadDB(accounting.DBName);
            string sql = @"SELECT    [Document].ID
                        FROM         {1}.dbo.[Document] LEFT OUTER JOIN
                      {0}.dbo.BudgetedTransaction ON [Document].ID = BudgetedTransaction.documentID
                      where BudgetedTransaction.budgetDocumentID is null
                     order by tranTicks desc";
            int[] docID = accounting.WriterHelper.GetColumnArray<int>(sql.format(bde.DBName, accounting.DBName), 0, 0, -1, out N);
            Console.WriteLine(docID.Length+ " unmarked documents found");
            Console.WriteLine();
            int f = 0;
            try
            {

                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job - Accounting-budget: mark none cash transactions as unbudgeted", "", -1);
                int c = docID.Length;
                List<int> cashAccounts = new List<int>();
                cashAccounts.AddRange(bde.WriterHelper.GetColumnArray<int>("SELECT [mainCsAccount] FROM {0}.[dbo].[BankAccountInfo]".format(bde.DBName)));
                cashAccounts.AddRange(bde.WriterHelper.GetColumnArray<int>("SELECT [csAccountID] FROM {0}.[dbo].[CashAccount]".format(bde.DBName)));
                HashSet<int> ignoreDocumentTypeList = new HashSet<int>();
                foreach (DocumentType t in accounting.GetAllDocumentTypes())
                {
                    Type docType = t.GetTypeObject();
                    if (docType == null || docType.GetCustomAttributes(typeof(NoBudgetAttribute), false).Length > 0)
                        ignoreDocumentTypeList.Add(t.id);
                }

                for (int i = docID.Length - 1; i >= 0; i--)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\tDone: {1}     ", c--, f);
                    AccountDocument document = accounting.GetAccountDocument(docID[i], false);
                    double net = 0;
                    int documentTypeID = document.DocumentTypeID;
                    if (!ignoreDocumentTypeList.Contains(documentTypeID))
                    {
                        foreach (AccountTransaction b in accounting.GetTransactionsOfDocument(document.AccountDocumentID))
                        {
                            if (cashAccounts.Contains(b.AccountID))
                                net += b.Amount;
                        }
                    }
                    if (AccountBase.AmountEqual(net, 0))
                    {
                        f++;
                        bde.setBudgetDocument(AID, document.AccountDocumentID, -1);
                    }

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
        static bool isValidCashFlow(AccountingBDE accounting,iERPTransactionBDE erp, SQLHelper reader,int documentID,HashSet<int> cashAccounts)
        {
            AccountTransaction[] trans = accounting.GetTransactionOfBatchInternal(reader, documentID);
            Dictionary<int, AccountBalance> changes = new Dictionary<int, AccountBalance>();
            double totalCash = 0;
            foreach(AccountTransaction t in trans)
            {
                if (t.ItemID != 0)
                    continue;
                if (cashAccounts.Contains(t.AccountID))
                    totalCash += t.Amount;
                foreach(int ac in reader.GetColumnArray<int>("Select parentCostCenterAccountID from {0}.dbo.CostCenterLeafAccount where childCostCenterAccountID={1}".format(accounting.DBName,t.AccountID)))
                {
                    AccountBalance bal;
                    if(!changes.TryGetValue(ac,out bal))
                        changes.Add(ac,new AccountBalance(-1,-1,t.Amount));
                    else
                        bal.AddBalance(new AccountBalance(-1,-1,t.Amount));
                }
            }
            if(erp.SysPars.reportCashFlow1==null)
                throw new ServerUserMessage("Design cash flow statement.");
            SummaryValueSection sec= erp.evaluateSummaryTable(erp.SysPars.reportCashFlow1,
                1, DateTime.Now, DateTime.Now,true
                , new iERPTransactionBDE.EvaluteSummeryItemDelegate(delegate(int costCenterID, int accountID, int itemID, DateTime time1, DateTime time2, bool flow)
                    {
                        if (itemID != 0)
                            return new AccountBalance();
                        int csAccountID=accounting.GetCostCenterAccountID(costCenterID,accountID);
                        AccountBalance bal;
                        if(csAccountID==-1 || !changes.TryGetValue(csAccountID,out bal))
                            return new AccountBalance();
                        return bal;
                    }
                    )
                );

            SummaryValueItem item = sec.getValueByTag("cashDiff");
            if(item==null)
                throw new ServerUserMessage("cashDIff tag not found in cashflow design");
            item.accountBalance.TotalDebit -= totalCash;
            return AccountBase.AmountEqual( item.accountBalance.DebitBalance,0);
        }

        [MaintenanceJobMethod("Accounting-cash flow: filter invalid cash flow transaction")]
        public static void accountingSummerizationFilterInvalidCashFlowDocuments()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE erp = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            
            string cr = getDocumentFilterInput("Document",accounting);
            try
            {
                int N;
                Console.WriteLine("Loading transactions IDs");
                accounting.WriterHelper.setReadDB(accounting.DBName);

                int c;
                if (!string.IsNullOrEmpty(cr))
                    cr = "where " + cr;

                int[] docs = accounting.WriterHelper.GetColumnArray<int>("Select id from Accounting_2006.dbo.Document " + cr);
                Console.WriteLine(docs.Length + " transactions found");
                Console.WriteLine();
                c = docs.Length;
                List<int> errorDocs = new List<int>();
                int cand = 0;
                HashSet<int> cashAccounts = new HashSet<int>();
                foreach(int aid in erp.WriterHelper.GetColumnArray<int>("SELECT [mainCsAccount] FROM {0}.[dbo].[BankAccountInfo]".format(erp.DBName)))
                    cashAccounts.Add(aid);
                foreach (int aid in erp.WriterHelper.GetColumnArray<int>("SELECT [csAccountID] FROM {0}.[dbo].[CashAccount]".format(erp.DBName)))
                    cashAccounts.Add(aid);

                INTAPS.RDBMS.SQLHelper reader = accounting.GetReaderHelper();
                for (int i = docs.Length - 1; i >= 0; i--)
                {
                    if (i % 100 == 0)
                    {
                        if (Console.KeyAvailable)
                        {
                            Console.ReadKey();
                            Console.WriteLine("Press Y to stop searching and display result");
                            if (Console.ReadLine() == "Y")
                            {
                                break;
                            }

                        }
                    }
                    Console.CursorTop--;
                    Console.WriteLine("{0}\tCand: {1}\tError: {2}                 ", c--, cand, errorDocs.Count);
                    bool isCand = false;
                    if (!isValidCashFlow(accounting,erp, reader, docs[i],cashAccounts))
                        errorDocs.Add(docs[i]);
                    if (isCand)
                        cand++;

                }

                if (errorDocs.Count > 0)
                {
                    Console.WriteLine("Unblanced documents.");
                    for (int i = 0; i < errorDocs.Count; i++)
                    {
                        AccountDocument doc = accounting.GetAccountDocument(errorDocs[i], false);
                        Console.WriteLine("{0}. \tRef:{1}\tID:{2}\tType:{3}", i + 1, doc.PaperRef, doc.AccountDocumentID, accounting.GetDocumentTypeByID(doc.DocumentTypeID).name);
                        if (i % 20 == 19)
                        {
                            Console.WriteLine("Press enter to list more.");
                            Console.ReadLine();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
        private static void clearTransactionItems(iERPTransactionBDE bde)
        {
            int itemN;
            BIZNET.iERP.TransactionItems[] items = bde.SearchTransactionItems(0, -1, new object[0], new string[0], out itemN);
            int itemC = items.Length;
            Console.WriteLine(itemC + " items found");
            Console.WriteLine();
            foreach (BIZNET.iERP.TransactionItems item in items)
            {
                Console.CursorTop--;
                Console.WriteLine(itemC + "                       ");
                try
                {
                    int itemAID = ApplicationServer.SecurityBDE.WriteAudit("System", -1, "Clear Database", "", "", -1);
                    bde.DeleteTransactionItem(itemAID, item.Code);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                itemC--;
            }
        }
        [MaintenanceJobMethod("Accounting - data clearing: clear items")]
        public static void clearTransactionItems()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            clearTransactionItems(bde);
        }
        [Flags]    
        enum DocumentIDFilterFields
        {
            DocumentID=1,
            TimeLowerBound=2,
            TimeUpperBound=4,
            TypeInclusive=8,
            TypeExclusive=16,
            All=DocumentID|TimeLowerBound|TimeUpperBound|TypeInclusive|TypeExclusive
        }
        private static string getDocumentFilterInput(string docTableAlias, AccountingBDE accounting)
        {
            return getDocumentFilterInput(docTableAlias, accounting, DocumentIDFilterFields.All);
        }
        private static string getDocumentFilterInput(string docTableAlias, AccountingBDE accounting,DocumentIDFilterFields fields)
        {
            string cr = null;
            String input;

            if ((fields & DocumentIDFilterFields.DocumentID) == DocumentIDFilterFields.DocumentID)
            {
                System.Console.Write("Enter specific document ID(Enter for general):");
                while (!(input = System.Console.ReadLine()).Equals(""))
                {
                    int docID;

                    if (int.TryParse(input, out docID))
                    {
                        cr = docTableAlias + ".ID=" + docID;
                        break;
                    }
                    else
                        Console.Write("Enter valid id or press enter: ");
                }
            }
            if (cr == null)
            {
                if ((fields & DocumentIDFilterFields.TimeLowerBound) == DocumentIDFilterFields.TimeLowerBound)
                {
                    System.Console.Write("Enter lower time bound filter (Enter for no time filter):");
                    while (!(input = System.Console.ReadLine()).Equals(""))
                    {
                        DateTime fromDate;
                        if (DateTime.TryParse(input, out fromDate))
                        {
                            cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", docTableAlias + ".tranTicks>=" + fromDate.Ticks);
                            break;
                        }
                        else
                            Console.Write("Enter valid date or press enter: ");
                    }
                }
                if ((fields & DocumentIDFilterFields.TimeUpperBound) == DocumentIDFilterFields.TimeUpperBound)
                {
                    System.Console.Write("Enter upper exclusive time bound filter (Enter for no time filter):");
                    while (!(input = System.Console.ReadLine()).Equals(""))
                    {
                        DateTime toDate;
                        if (DateTime.TryParse(input, out toDate))
                        {
                            cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", docTableAlias + ".tranTicks<" + toDate.Ticks);
                            break;
                        }
                        else
                            Console.Write("Enter valid date or press enter: ");
                    }
                }
                if ((fields & DocumentIDFilterFields.TypeInclusive) == DocumentIDFilterFields.TypeInclusive)
                {
                    List<int> documentTypes = new List<int>();
                    System.Console.Write("Enter document typeID filter (Enter for no document type filter):");
                    while (!(input = System.Console.ReadLine()).Equals(""))
                    {
                        int documentTypeID;
                        DocumentType docType;
                        if (int.TryParse(input, out documentTypeID) && (docType = accounting.GetDocumentTypeByID(documentTypeID)) != null)
                        {
                            Console.WriteLine("Document type {0} added to filter", docType.name);
                            System.Console.Write("Enter another document typeID filter (Enter to finish):");
                            documentTypes.Add(documentTypeID);
                        }
                        else
                            Console.Write("Enter valid document ID or press enter: ");
                    }
                    if (documentTypes.Count > 0)
                    {
                        string docTypeFilter = null;
                        foreach (int docTypeID in documentTypes)
                            docTypeFilter = INTAPS.StringExtensions.AppendOperand(docTypeFilter, ",", docTypeID.ToString());
                        cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", string.Format("{1}.DocumentTypeID in ({0})", docTypeFilter, docTableAlias));
                    }
                }
                if ((fields & DocumentIDFilterFields.TypeExclusive) == DocumentIDFilterFields.TypeExclusive)
                {
                    List<int> exculdeType = new List<int>();
                    System.Console.Write("Enter document typeID exclusion filter (Enter for no exclusion):");
                    while (!(input = System.Console.ReadLine()).Equals(""))
                    {
                        int documentTypeID;
                        DocumentType docType;
                        if (int.TryParse(input, out documentTypeID) && (docType = accounting.GetDocumentTypeByID(documentTypeID)) != null)
                        {
                            Console.WriteLine("Document type {0} added to exclusion filter", docType.name);
                            System.Console.Write("Enter another document typeID exclusion filter (Enter to finish):");
                            exculdeType.Add(documentTypeID);
                        }
                        else
                            Console.Write("Enter valid document ID or press enter: ");
                    }



                    if (exculdeType.Count > 0)
                    {
                        string excl = null;
                        foreach (int docTypeID in exculdeType)
                            excl = INTAPS.StringExtensions.AppendOperand(excl, ",", docTypeID.ToString());
                        cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", string.Format("{1}.DocumentTypeID not in ({0})", excl, docTableAlias));
                    }
                }
            }
            return cr;
        }

        [MaintenanceJobMethod("Accounting - summerization: compare ledgers")]
        public static void summerizationCompareLedgers()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            Account a1=null, a2=null;
            while(a1==null)
            {
                Console.Write("Enter account1: ");
                a1 = accounting.GetAccount<Account>(Console.ReadLine());
            }
            while (a2 == null)
            {
                Console.Write("Enter account2: ");
                a2 = accounting.GetAccount<Account>(Console.ReadLine());
            }
            DateTime t1;
            DateTime t2;
            
            while(true)
            {
                Console.Write("Enter time1: ");
                if(DateTime.TryParse(Console.ReadLine(),out t1))
                    break;
            }
            while (true)
            {
                Console.Write("Enter time2: ");
                if (DateTime.TryParse(Console.ReadLine(), out t2))
                    break;
            }
            int N;
            AccountTransaction[] l1 = accounting.GetTransactions(new int[]{accounting.GetCostCenterAccountID(1, a1.id)}, 0, t1, t2, 0, -1, out N);
            AccountTransaction[] l2 = accounting.GetTransactions(new int[] { accounting.GetCostCenterAccountID(1, a2.id) }, 0, t1, t2, 0, -1, out N);
            double total1 = 0;
            double total2 = 0;
            Dictionary<int, double> docs1 = new Dictionary<int, double>();
            foreach (AccountTransaction t in l1)
            {
                total1 += t.Amount;
                if (docs1.ContainsKey(t.documentID))
                    docs1[t.documentID] += t.Amount;
                else
                    docs1.Add(t.documentID, t.Amount);
            }
            Dictionary<int, double> docs2 = new Dictionary<int, double>();

            foreach (AccountTransaction t in l2)
            {
                total2+= t.Amount;
                if (docs2.ContainsKey(t.documentID))
                    docs2[t.documentID] += t.Amount;
                else
                    docs2.Add(t.documentID, t.Amount);
            }
            foreach(KeyValuePair<int,double> kv in docs1)
            {
                if (docs2.ContainsKey(kv.Key))
                {
                    if (!AccountBase.AmountEqual(kv.Value, docs2[kv.Key]))
                        Console.WriteLine("docID1:{0} amount1:{1} amount1:{2}".format(kv.Key, kv.Value, docs2[kv.Key]));
                }
                else
                    Console.WriteLine("docID1:{0} amount1:{1} amount2:{2}".format(kv.Key, kv.Value, "-"));
            }
            foreach (KeyValuePair<int, double> kv in docs2)
            {
                if (docs1.ContainsKey(kv.Key))
                {
                    if (!AccountBase.AmountEqual(kv.Value, docs1[kv.Key]))
                        Console.WriteLine("docID2:{0} amount1:{1} amount1:{2}".format(kv.Key, docs1[kv.Key], kv.Value));
                }
                else
                    Console.WriteLine("docID2:{0} amount1:{1} amount1:{2}".format(kv.Key, "-", kv.Value));
            }
            Console.WriteLine("{0} - {1}", a1.NameCode, total1);
            Console.WriteLine("{0} - {1}", a2.NameCode, total2);
        }

        [MaintenanceJobMethod("Accounting : set relation data for trade transactions")]
        public static void fixTradeRelationTransactionRelationData()
        {
            try
            {
                Console.WriteLine("set relation data for trade transactions");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                accounting.WriterHelper.setReadDB(bde.DBName);
                int N;
                AccountDocument[] docs = accounting.GetAccountDocuments(new DocumentSearchPar()
                {
                    date = false,
                    includeType = new int[]
                    {
                    accounting.GetDocumentTypeByType(typeof(Purchase2Document)).id,
                    accounting.GetDocumentTypeByType(typeof(SupplierAdvancePayment2Document)).id,
                    accounting.GetDocumentTypeByType(typeof(SupplierAdvanceReturn2Document)).id,
                    accounting.GetDocumentTypeByType(typeof(SupplierRetentionSettlementDocument)).id,
                    accounting.GetDocumentTypeByType(typeof(SupplierCreditSettlement2Document)).id,

                    accounting.GetDocumentTypeByType(typeof(Sell2Document)).id,
                    accounting.GetDocumentTypeByType(typeof(CustomerAdvancePayment2Document)).id,
                    accounting.GetDocumentTypeByType(typeof(CustomerAdvanceReturn2Document)).id,
                    accounting.GetDocumentTypeByType(typeof(CustomerRetentionSettlementDocument)).id,
                    accounting.GetDocumentTypeByType(typeof(CustomerCreditSettlement2Document)).id,
                    }
                }, null, 0, -1, out N);
                Console.WriteLine("{0} documents found", N);
                Console.WriteLine("Press enter to start fixing");
                Console.ReadLine();
                foreach (TradeTransaction doc in docs)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}           ", N--);
                    int docAID = (int)accounting.WriterHelper.ExecuteScalar(
                                string.Format("Select __AID from {0}.dbo.Document where id={1}", accounting.DBName,
                                    doc.AccountDocumentID));
                    int relationAID = (int)accounting.WriterHelper.ExecuteScalar(
                                string.Format("Select __AID from {0}.dbo.TradeRelation where code='{1}'", bde.DBName,
                                    doc.relationCode));
                    doc.relationData = null;
                    if (relationAID > docAID)
                    {
                        int vAID = (int)accounting.WriterHelper.ExecuteScalar(
                                string.Format("Select max(__AID) from {0}.dbo.TradeRelation_Deleted where code='{1}' and __AID<{2}", bde.DBName,
                                    doc.relationCode, docAID));
                        TradeRelation[] rel = accounting.WriterHelper.GetSTRArrayBySQL<TradeRelation>(
                           string.Format("Select * from {0}.dbo.TradeRelation_Deleted where code='{1}' and __AID={2}", bde.DBName, doc.relationCode, vAID)
                            );
                        if (rel.Length > 0)
                            doc.relationData = rel[0];
                    }
                    if (doc.relationData == null)
                        doc.relationData = bde.GetRelation(doc.relationCode);

                    doc.relationData.Logo = null;
                    accounting.UpdateAccountDocumentData(docAID, doc);
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
    
    }

}

