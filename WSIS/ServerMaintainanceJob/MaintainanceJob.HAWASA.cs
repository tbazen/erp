using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using INTAPS.WSIS.Job;



namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {

       
        [MaintenanceJobMethod("Hawasa WSIS: Transfer Hawasa WSS Customers")]
        public static void transferHawasaSubscribersToWSIS()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Transfering customers");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string custSql = "SELECT * FROM [WSS].[dbo].[co_Customers]";
                DataTable custTable = bdesub.WriterHelper.GetDataTable(custSql);
                int custLength = custTable.Rows.Count;

                Console.WriteLine(custLength + " customers found");
                Console.WriteLine();
                int c = custLength;

                int i=0;
                foreach (DataRow row in custTable.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");
                    bdesub.WriterHelper.BeginTransaction();

                    try
                    {
                        Subscriber subscriber = new Subscriber();
                        subscriber.id = i + 1;
                        subscriber.name = row["CustName"] as string;
                        subscriber.subscriberType = GetSubscriberType((int)row["CusType"], bdesub.WriterHelper);
                        subscriber.Kebele = GetKebele(row["Ketena"] as string, bdesub.WriterHelper);
                        subscriber.address = row["HNo"] as string;
                        subscriber.amharicName = "N/A";
                        subscriber.customerCode = ((int)row["CusIndex"]).ToString();
                        subscriber.phoneNo = row["Tele"] as string;
                        subscriber.email = "N/A";
                        subscriber.nameOfPlace = GetKebeleName(subscriber.Kebele, bdesub.WriterHelper);
                        subscriber.status = CustomerStatus.Active;
                        subscriber.statusDate = (DateTime)row["RegDate"];
                        subscriber.accountID = -1;
                        subscriber.depositAccountID = -1;
                        subscriber.subTypeID = -1;
                        bdesub.WriterHelper.InsertSingleTableRecord<Subscriber>(bdesub.DBName, subscriber);

                        Subscription subscription = new Subscription();
                        subscription.id = i + 1;
                        subscription.ticksFrom = 635641776000000000;
                        subscription.ticksTo = -1;
                        subscription.timeBinding = DataTimeBinding.LowerBound;
                        subscription.subscriberID = subscriber.id;
                        subscription.contractNo = subscriber.customerCode;
                        subscription.subscriptionType = SubscriptionType.Tap;
                        subscription.subscriptionStatus = GetSubscriptionStatus(row["Status"] as string, bdesub.WriterHelper);
                        subscription.Kebele = subscriber.Kebele;
                        subscription.address = subscriber.address;
                        subscription.initialMeterReading = 0;
                        subscription.prePaid = false;
                        subscription.parcelNo = "N/A";
                        subscription.connectionType = ConnectionType.Unknown;
                        subscription.supplyCondition = SupplyCondition.Unknown;
                        subscription.remark = "Job Transfer from HAWASA WSSA System";
                        subscription.dataStatus = DataStatus.Unknown;
                        subscription.meterPosition = 0;
                        subscription.waterMeterX = 0;
                        subscription.waterMeterY = 0;
                        subscription.houseConnectionX = 0;
                        subscription.houseConnectionY = 0;
                        subscription.serialNo = row["MeterNo"] as string;
                        subscription.modelNo = subscription.serialNo;
                        subscription.opStatus = MaterialOperationalStatus.Working;
                        subscription.householdSize = 0;
                        bdesub.WriterHelper.InsertSingleTableRecord<Subscription>(bdesub.DBName, subscription);
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    i++;
                }
                //custTable.Close();

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Hawasa WSIS: Import previous reading (Ginbot 2007) from WSS ")]
        public static void importPreviousReadingFromWSS()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Importing previous reading...");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string custSql = @"Select Subscription.id,ActualReading,Consumtion,ReDate from Subscriber.dbo.Subscription inner join WSS.dbo.bill_Consumption as con
on Subscription.contractNo=con.CusIndex where con.ForMonth=237 order by Subscription.id asc";

                DataTable custTable = bdesub.WriterHelper.GetDataTable(custSql);
                int custLength = custTable.Rows.Count;

                Console.WriteLine(custLength + " subscriptions found");
                Console.WriteLine();
                int c = custLength;

                int i = 0;
                foreach (DataRow row in custTable.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");
                    bdesub.WriterHelper.BeginTransaction();

                    try
                    {
                        BWFMeterReading reading = new BWFMeterReading();
                        reading.readingBlockID = 78183;
                        reading.subscriptionID = (int)row["id"];
                        reading.bwfStatus = BWFStatus.Read;
                        reading.readingType = MeterReadingType.Normal;
                        reading.averageMonths = 0;
                        reading.reading = double.Parse(row["ActualReading"].ToString());
                        reading.consumption = double.Parse(row["Consumtion"].ToString());
                        reading.entryDate = (DateTime)row["ReDate"];
                        reading.orderN = i + 1;
                        reading.billDocumentID = -1;
                        reading.periodID = 2320;
                        reading.readingProblem = MeterReadingProblem.NoProblem;
                        reading.extraInfo = ReadingExtraInfo.None;
                        reading.readingRemark = "Reading imported from Job";
                        reading.readingTime = reading.entryDate;
                        reading.readingX = 0;
                        reading.readingY = 0;
                        reading.method = ReadingMethod.Unknown;
                        bdesub.WriterHelper.InsertSingleTableRecord<BWFMeterReading>(bdesub.DBName, reading);
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    i++;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Hawasa WSIS: Prepare/import Sene Reading for kebele ")]
        public static void importSubscriptionsUnderKebele()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Importing subscriptions as unread for kebele...");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string custSql = "Select id from Subscriber.dbo.Subscription where kebele=29 order by id asc";

                DataTable custTable = bdesub.WriterHelper.GetDataTable(custSql);
                int custLength = custTable.Rows.Count;

                Console.WriteLine(custLength + " subscriptions found");
                Console.WriteLine();
                int c = custLength;

                int i = 0;
                foreach (DataRow row in custTable.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");
                    bdesub.WriterHelper.BeginTransaction();

                    try
                    {
                        BWFMeterReading reading = new BWFMeterReading();
                        reading.readingBlockID = 78193; //block of a reader for which to import the subscriptions
                        reading.subscriptionID = (int)row["id"];
                        reading.bwfStatus = BWFStatus.Unread;
                        reading.readingType = MeterReadingType.Normal;
                        reading.averageMonths = 0;
                        reading.reading = 0d;
                        reading.consumption = 0d;
                        reading.entryDate = DateTime.Now.Date;
                        reading.orderN = i + 1;
                        reading.billDocumentID = -1;
                        reading.periodID = 2321;
                        reading.readingProblem = MeterReadingProblem.NoProblem;
                        reading.extraInfo = ReadingExtraInfo.None;
                        reading.readingRemark = "Sene unread Reading imported from Job";
                        reading.readingTime = DateTime.Now.Date;
                        reading.readingX = 0;
                        reading.readingY = 0;
                        reading.method = ReadingMethod.Unknown;
                        bdesub.WriterHelper.InsertSingleTableRecord<BWFMeterReading>(bdesub.DBName, reading);
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    i++;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Hawasa WSIS: Update duplicate meter numbers ")]
        public static void UpdateDuplicateMeterNumbers()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Updating duplicate meter numbers...");

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string custSql = @"SELECT count(*)as count,serialNo
  FROM [Subscriber].[dbo].[Subscription]
  group by serialNo having count(*)>1";

                DataTable custTable = bdesub.WriterHelper.GetDataTable(custSql);
                int custLength = custTable.Rows.Count;

                Console.WriteLine(custLength + " duplicates found");
                Console.WriteLine();
                int c = custLength;

                foreach (DataRow row in custTable.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                      ");

                    try
                    {
                        string serialNo = (string)row["serialNo"];
                        string sql = "select contractNo from Subscriber.dbo.Subscription where serialNo='" + serialNo + "'";
                        DataTable conTable = bdesub.WriterHelper.GetDataTable(sql);
                        foreach (DataRow conRow in conTable.Rows)
                        {
                            bdesub.WriterHelper.BeginTransaction();
                            string contractNo = (string)conRow[0];
                            string updateSql = "Update Subscriber.dbo.Subscription set serialNo='" + serialNo + "-" + contractNo + "' where contractNo=" + contractNo;
                            bdesub.WriterHelper.ExecuteScalar(updateSql);
                            bdesub.WriterHelper.CommitTransaction();
                        }
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Hawasa WSS: Update bill_consumption Read Date Format to dd/MM/yyyy for Meskerem 2008")]
        public static void UpdateBillConsumptionReadDateFormat()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Updating reading date format...");

                string connWSS = System.Configuration.ConfigurationManager.AppSettings["WSSDB"];
                SQLHelper helper = new SQLHelper(connWSS);


                string billSql = "Select ConID, ReDate FROM [WSS].[dbo].[bill_Consumption] where ForMonth=241";

                DataTable table = helper.GetDataTable(billSql);
               
                Console.WriteLine(table.Rows.Count + " readings found");
                Console.WriteLine();
                int c = table.Rows.Count;
                helper.BeginTransaction();
                try
                {
                    foreach (DataRow row in table.Rows)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c-- + "                      ");
                        int conID=(int)row[0];
                        string ethDate = INTAPS.Ethiopic.EtGrDate.ToEth(((DateTime)row[1])).GridDate.ToString("dd/MM/yyyy");
                   
                       // DateTime ReDateA = new INTAPS.Ethiopic.EtGrDate(int.Parse(am[1]), int.Parse(am[0]), int.Parse(am[2])).GridDate;
                        string update = "Update WSS.dbo.bill_Consumption set ReDateA='" + ethDate + "' where ConID=" + conID;
                        helper.ExecuteScalar(update);
                    }
                    helper.CommitTransaction();
                }
                catch(Exception ex)
                {
                    helper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Hawasa WSS: Update meter rent based on meter size for Meskerem 2008")]
        public static void UpdateMeterNumber()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Updating meter number...");

                string connWSS = System.Configuration.ConfigurationManager.AppSettings["WSSDB"];
                SQLHelper helper = new SQLHelper(connWSS);


                string billSql = "Select CusIndex, Amount FROM [WSS].[dbo].[bill_Consumption] where ForMonth=241";

                DataTable table = helper.GetDataTable(billSql);

                Console.WriteLine(table.Rows.Count + " readings found");
                Console.WriteLine();
                int c = table.Rows.Count;
                helper.BeginTransaction();
                try
                {
                    foreach (DataRow row in table.Rows)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c-- + "                      ");
                        int cusIndex = (int)row[0];
                        double amount = (double)row[1];

                        string meterSizeSql = "select MeterSize from WSS.dbo.co_Customers where CusIndex=" + cusIndex;
                        string meterRentSql = "select Amount from WSS.dbo.bill_StgMeterSize where Pk=" + (int)helper.ExecuteScalar(meterSizeSql);
                        double meterRent = (double)helper.ExecuteScalar(meterRentSql);
                        double total = amount + meterRent;

                        string update = "Update WSS.dbo.bill_Consumption set MeterRent=" + meterRent + ",Total=" + total + " where CusIndex=" + cusIndex + " and ForMonth=241";
                        helper.ExecuteScalar(update);
                    }
                    helper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    helper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        private static SubscriptionStatus GetSubscriptionStatus(string status, SQLHelper sQLHelper)
        {
            string sql = "Select WSISSubscriptionStatus from Subscriber.dbo.CustStatusToSubscriptionStatusMap where WSSCustStatus='" + status + "'";
            int s = (int)sQLHelper.ExecuteScalar(sql);
            return (SubscriptionStatus)s;
        }

        private static string GetKebeleName(int kebele, SQLHelper sQLHelper)
        {
            string sql = "Select name from Subscriber.dbo.Kebele where id=" + kebele;
            string name = (string)sQLHelper.ExecuteScalar(sql);
            return name;
        }

        private static int GetKebele(string kebele, SQLHelper sQLHelper)
        {
            string sql = "Select id from Subscriber.dbo.Kebele where name='" + kebele + "'";
            int id = (int)sQLHelper.ExecuteScalar(sql);
            return id;
        }

        private static SubscriberType GetSubscriberType(int custType, SQLHelper sQLHelper)
        {
            string sql = "select WSISSubscriberType From Subscriber.dbo.CustTypeToSubscribeTypeMap where WSSACustType=" + custType;
            int subscriberType = (int)sQLHelper.ExecuteScalar(sql);
            return (SubscriberType)subscriberType;
        }

       
    }

}
