using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;


namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {
        [Flags]
        public enum LBTransferProblem
        {
            None = 0,
            ReadingMismatc = 1,
            CustomerDataMismatch = 2,
            NoLegacyBillReading = 4,
            NoLegacyCustomer = 8,
            Exception = 16,
            CustomerNotInDatabase=32,
        }

        [SingleTableObject]
        class LBTransferReport
        {
            [DataField]
            public bool skipped = false;
            [DataField]
            public int legacyBillID;
            [DataField]
            public LBTransferProblem problemCode=LBTransferProblem.None;
            [DataField]
            public string data1="";
            [DataField]
            public string data2 = "";
            [DataField]
            public string data3 = "";
        }
        //[MaintenanceJobMethod("TOUT", "Transfer oustandingbills")]
        //public static void fixMonthOnMessages()
        //{
        //}
        [MaintenanceJobMethod("Transfer oustandingbills")]
        public static void transferOutandingBills()
        {
            try
            {
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                ReadingEntryClerk clerk = bdesub.BWFGetClerkByUserID("admin");
                if (clerk == null)
                    throw new Exception("Setup system reading clerk");

                INTAPS.RDBMS.SQLHelper legacyReader = new RDBMS.SQLHelper("Data Source=DC01\\SQLEXPRESS;Integrated Security=True;User ID=sa;Password=cisgis;Initial Catalog=Subscriber_os");
                LegacyBill[] bills;
                Console.WriteLine();
                Console.Write("Bill ID:");
                string ln=Console.ReadLine();
                int billID;
                if(!string.IsNullOrEmpty(ln) && int.TryParse(ln,out billID))
                    bills = legacyReader.GetSTRArrayByFilter<LegacyBill>("id="+billID);
                else
                    bills= legacyReader.GetSTRArrayByFilter<LegacyBill>("setID in (Select id from LegacyBillSet where verfied=1)");
                Console.WriteLine("Transfering " + bills.Length + " outstanding bills");
                Console.WriteLine();
                int c = bills.Length;
                Console.WriteLine();
                
                foreach (LegacyBill b in bills)
                {
                    
                    LBTransferReport r = new LBTransferReport();
                    bool hasProblem=false;
                    r.legacyBillID = b.id;
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "           ");
                    try
                    {
                        Subscription[] lbSubsc = legacyReader.GetSTRArrayByFilter<Subscription>("id=" + b.customerID);
                        if (lbSubsc.Length == 0)
                        {
                            Console.WriteLine("No legacy connection");
                            r.skipped = true;
                            r.problemCode = LBTransferProblem.NoLegacyCustomer;
                            r.data1 = "No legacy connection";
                            goto l_addProblem;
                        }
                        Subscriber[] lbCust = legacyReader.GetSTRArrayByFilter<Subscriber>("id=" + lbSubsc[0].subscriberID);
                        if (lbCust.Length == 0)
                        {
                            Console.WriteLine("No legacy customer");
                            r.skipped = true;
                            r.problemCode = LBTransferProblem.NoLegacyCustomer;
                            r.data1 = "No legacy customer";
                            goto l_addProblem;
                        }
                        Subscription subsc = bdesub.GetSubscription(lbSubsc[0].contractNo, DateTime.Now.Ticks);
                        if (subsc == null)
                        {
                            int AID=ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Transfer Legacy Bills", "", "", -1);
                            lbCust[0].id = -1;
                            lbCust[0].id = bdesub.RegisterSubscriber(AID, lbCust[0]);
                            lbSubsc[0].subscriberID = lbSubsc[0].id;
                            lbSubsc[0].id = -1;
                            lbSubsc[0].ticksFrom = new DateTime(1970, 1, 1).Ticks;
                            
                            lbSubsc[0].id = bdesub.AddSubscription(AID, lbSubsc[0]);
                            r.problemCode = LBTransferProblem.CustomerNotInDatabase;
                            r.data1 = "Customer not in main database";
                            Console.WriteLine("Customer not in main database");
                        }

                        LegacyBillItem[] items = legacyReader.GetSTRArrayByFilter<LegacyBillItem>("legacyBillID=" + b.id);
                        /*BWFMeterReading reading = bdesub.BWFGetMeterReadingByPeriod(b.customerID, b.periodID);
                        BWFMeterReading[] lbreading = legacyReader.GetSTRArrayByFilter<BWFMeterReading>("periodID=" + b.periodID + " and subscriptionID=" + b.customerID);
                        if (lbreading.Length == 0)
                        {
                            Console.WriteLine("No legacy bill reading");
                            hasProblem = true;
                            r.problemCode |= LBTransferProblem.NoLegacyBillReading;
                            r.data1 += "No Legacy Bill Reading";
                        }
                        else
                        {
                            if (reading == null)
                            {
                                //int blockID = -1;
                                //lbreading[0].subscriptionID = lbSubsc[0].id;
                                //BWFReadingBlock[] block = bdesub.GetBlock(lbreading[0].periodID, SubscriberManagmentBDE.OB_BLOCK);
                                //if (block.Length == 0)
                                //{
                                //    BWFReadingBlock blk = new BWFReadingBlock();
                                //    blk.blockName = SubscriberManagmentBDE.OB_BLOCK;
                                //    blk.periodID = lbreading[0].periodID;
                                //    MeterReaderEmployee emp = bdesub.GetMeterReadingEmployee(clerk.employeeID);
                                //    if (emp == null)
                                //    {
                                //        emp = new MeterReaderEmployee();
                                //        emp.employeeID = clerk.employeeID;
                                //        emp.periodID = blk.periodID;
                                //        bdesub.CreateMeterReaderEmployee(emp);
                                //    }
                                //    blk.readerID = emp.employeeID;
                                //    blk.readingStart = blk.readingEnd = DateTime.Now;
                                //    blockID = bdesub.BWFCreateReadingBlock(-1, clerk, blk);
                                //}
                                //else if (block.Length == 1)
                                //{
                                //    blockID = block[0].id;
                                //}
                                //else
                                //    throw new ServerUserMessage("Multiple " + SubscriberManagmentBDE.OB_BLOCK + " blocks founds");
                                //lbreading[0].readingBlockID = blockID;
                                //BillPeriod bp = bdesub.GetBillPeriod(lbreading[0].periodID);
                                //if (bdesub.GetSubscriptionStatusAtDate(lbreading[0].subscriptionID, bp.toDate) != SubscriptionStatus.Active)
                                //{
                                //    bdesub.ChangeSubcriptionStatus(-1, lbreading[0].subscriptionID, bp.toDate, SubscriptionStatus.Active, true);
                                //}
                                //bdesub.BWFAddMeterReading(-1, clerk, lbreading[0], true);
                            }
                            else
                            {
                                if (!reading.reading.Equals(lbreading[0].reading)
                                    || !reading.consumption.Equals(lbreading[0].consumption))
                                {
                                    hasProblem = true;
                                    r.problemCode = LBTransferProblem.ReadingMismatc;
                                    r.data1 = "Reading mismatch";
                                }
                            }
                        }*/
                        b.customerID = lbSubsc[0].id;
                        bdesub.WriterHelper.InsertSingleTableRecord(bdesub.DBName, b);
                        foreach (LegacyBillItem it in items)
                            bdesub.WriterHelper.InsertSingleTableRecord(bdesub.DBName, it);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        hasProblem = true;
                        r.skipped = true;
                        r.data1 = ex.Message;
                        r.problemCode = LBTransferProblem.Exception;
                        goto l_addProblem;
                    }
                    if(!hasProblem)
                    continue;
                l_addProblem:
                    legacyReader.InsertSingleTableRecord(null, r);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error\b" + ex.Message+"\n"+ex.StackTrace);
                Console.WriteLine();
            }
        }
        [MaintenanceJobMethod("Billing: Generate outstanding bills")]
        public static void generateOutstandingBills()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            try
            {
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Console.WriteLine("Retreiving bills list");
                int[] billIDs = bdesub.WriterHelper.GetColumnArray<int>(@"SELECT     LegacyBill.id
FROM         Subscriber.dbo.LegacyBill INNER JOIN
                      Subscriber.dbo.LegacyBillSet ON LegacyBill.setID = LegacyBillSet.id
WHERE     (LegacyBillSet.verfied = 1) and not exists(Select * from 
  Subscriber.dbo.CustomerBill where periodID=[LegacyBill].periodID and connectionID=[LegacyBill].customerID )", 0);
                int c = billIDs.Length;
                Console.WriteLine();
                Dictionary<int, LegacyBillItemDefination> defs = new Dictionary<int, LegacyBillItemDefination>();
                foreach (LegacyBillItemDefination def in bdesub.WriterHelper.GetSTRArrayByFilter<LegacyBillItemDefination>(null))
                    defs.Add(def.id, def);
                foreach (int b in billIDs)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                                  ");
                    c--;
                    try
                    {
                        LegacyBill bill = bdesub.lbGetLegacyBill(b);
                        Subscription subsc = bdesub.GetSubscription(bill.customerID, DateTime.Now.Ticks);
                        BWFMeterReading read = new BWFMeterReading();
                        read.periodID = bill.periodID;
                        read.readingBlockID = -1;
                        read.subscriptionID = bill.customerID;
                        read.reading = bill.reading;
                        read.consumption = bill.consumption;
                        /*if (read == null)
                        {
                            Console.WriteLine("Reading not found:"+subsc.contractNo+" "+bill.periodID);
                            Console.WriteLine();
                            continue;
                        }*/
                        int typeID = bdesub.Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
                        foreach (CustomerBillRecord existing in bdesub.getBills(typeID, subsc.subscriberID, subsc.id, bill.periodID))
                        {
                            bdesub.Accounting.DeleteGenericDocument(-1, existing.id);
                        }
                        CustomerBillRecord rec = new CustomerBillRecord();
                        rec.billDate = DateTime.Now;
                        rec.billDocumentTypeID = typeID;
                        rec.customerID = subsc.subscriberID;
                        rec.connectionID = subsc.id;
                        rec.periodID = bill.periodID;
                        rec.postDate = DateTime.Now;
                        rec.draft = false;
                        WaterBillDocument billDoc = new WaterBillDocument();
                        billDoc.period = bdesub.GetBillPeriod(rec.periodID);
                        billDoc.subscription = subsc;
                        billDoc.reading = read;

                        string desc = "Bilii hin Kaffalamin Lakk. Waliigaltee:" + subsc.contractNo + " Ji'a: " + billDoc.period.name
                            + "\nDubisaa " + read.reading + " Fayyadama " + read.consumption;
                        List<BillItem> items = new List<BillItem>();
                        foreach (LegacyBillItem item in bill.items)
                        {
                            if (!AccountBase.AmountEqual(item.amount, 0))
                            {
                                BillItem billItem = new BillItem();
                                billItem.description = defs[item.itemID].description + " " + billDoc.period.name;
                                billItem.accounting = BillItemAccounting.Invoice;
                                billItem.hasUnitPrice = false;
                                billItem.price = item.amount;
                                switch (item.itemID)
                                {
                                    case 1:
                                        billItem.itemTypeID = 0;
                                        billItem.incomeAccountID = bdesub.SysPars.incomeAccount;
                                        break;
                                    case 2:
                                        billItem.itemTypeID = 1;
                                        billItem.incomeAccountID = bdesub.SysPars.rentIncomeAccount;
                                        break;
                                    case 4:
                                        billItem.itemTypeID = 2;
                                        billItem.incomeAccountID = bdesub.SysPars.additionalFeeIncomeAcount;
                                        break;
                                    case 3:
                                        billItem.itemTypeID = 3;
                                        billItem.incomeAccountID = bdesub.SysPars.otherBillIncomeAccount;
                                        break;
                                }
                                items.Add(billItem);
                            }
                        }


                        billDoc.waterBillItems = items.ToArray();
                        billDoc.ShortDescription = "Outstanding Bill";
                        billDoc.customer = subsc.subscriber;
                        billDoc.draft = false;

                        bdesub.addBill(-1, billDoc, rec, items.ToArray());


                    }
                    catch (Exception bex)
                    {
                        Console.WriteLine(bex.Message);
                        Console.WriteLine();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
