using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {
        [MaintenanceJobMethod("Offset Offline Data File")]
        public static void offsetOfflineData()
        {
            int prevMsgNo = -1;
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                Console.WriteLine("Enter last message number");
                string strOffset = Console.ReadLine();
                if (strOffset.Equals("Q"))
                    return;
                if (!int.TryParse(strOffset, out prevMsgNo) || prevMsgNo < 0)
                {
                    Console.WriteLine("Invalid offset number");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                foreach (OfflineMessage msg in update.list.data)
                {
                    msg.id = prevMsgNo + 1;
                    msg.previousMessageID = prevMsgNo;
                    prevMsgNo++;
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi= new System.IO.FileInfo(file);
                string newFile= fi.Directory.FullName+"\\"+fi.Name+".ofst.posmessage";
                System.IO.FileStream ostream=System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Offline: replace cash account")]
        public static void offlineReplaceCashAccout()
        {
            string file;
                int intCashAcount;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                Console.WriteLine("Enter new cash account");
                string cashAccount = Console.ReadLine();
                if (cashAccount.Equals("Q"))
                    return;
                if (!int.TryParse(cashAccount, out intCashAcount) || intCashAcount < 0)
                {
                    Console.WriteLine("Invalid cash account");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                        rmsg.payment.assetAccountID = intCashAcount;
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".edit.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        [MaintenanceJobMethod("Edit date of offline payment")]
        public static void editOfflineDateDate()
        {
            int msgNo = -1;
            DateTime date;
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                Console.WriteLine("Enter message number");
                string strMsgN = Console.ReadLine();
                if (strMsgN.Equals("Q"))
                    return;
                if (!int.TryParse(strMsgN, out msgNo) || msgNo < 0)
                {
                    Console.WriteLine("Invalid offset number");
                    continue;
                }
                Console.WriteLine("Enter new date");
                string strDate = Console.ReadLine();
                if (strDate.Equals("Q"))
                    return;
                if (!DateTime.TryParse(strDate, out date) )
                {
                    Console.WriteLine("Invalid date");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                bool found = false;
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        if (msg.id == msgNo)
                        {
                            INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                            msg.sentDate = date;
                            rmsg.payment.DocumentDate = date;
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    Console.WriteLine("MsgNo not found");
                    return;
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".edit.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Offline: Offset Receipt Numbers")]
        public static void offsetOfflineDataReceiptNumber()
        {
            int prevReceiptNo = -1;
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                Console.WriteLine("Enter last receipt number");
                string strReceipt = Console.ReadLine();
                if (strReceipt.Equals("Q"))
                    return;
                if (!int.TryParse(strReceipt, out prevReceiptNo) || prevReceiptNo < 0)
                {
                    Console.WriteLine("Invalid receiptNumber number");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                        rmsg.payment.receiptNumber.reference = (prevReceiptNo + 1).ToString("000000000");
                        prevReceiptNo++;
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        [MaintenanceJobMethod("Offline: Rebuild Offline Data File")]
        public static void rebuildOfflineDataFile()
        {
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList data = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                
                data = new PaymentCenterMessageList(data.paymentCenterID, new MessageList(data.list.data));

                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name.Substring(0,fi.Name.Length-fi.Extension.Length) + ".rebuilt.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, data);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Offset Xayituu Offline Data (Bishoftu)")]
        public static void offsetOfflineSales()
        {
            int prevMsgNo = -1;
            int prevReceiptNo = -1;
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                //Console.WriteLine("Enter last message number");
                //string strOffset = Console.ReadLine();
                //if (strOffset.Equals("Q"))
                //    return;
                //if (!int.TryParse(strOffset, out prevMsgNo) || prevMsgNo < 0)
                //{
                //    Console.WriteLine("Invalid offset number");
                //    continue;
                //}
                //Console.WriteLine("Enter last receipt number");
                //string strReceipt = Console.ReadLine();
                //if (strReceipt.Equals("Q"))
                //    return;
                //if (!int.TryParse(strReceipt, out prevReceiptNo) || prevReceiptNo < 0)
                //{
                //    Console.WriteLine("Invalid receiptNumber number");
                //    continue;
                //}
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            List<OfflineMessage> offlineMessages = new List<OfflineMessage>();
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.id < 244)
                    {
                        offlineMessages.Add(msg);
                    }
                    //msg.id = prevMsgNo + 1;
                    //msg.previousMessageID = prevMsgNo;
                    //prevMsgNo++;
                    //if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    //{
                    //    INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                    //    rmsg.payment.receiptNumber.reference = (prevReceiptNo + 1).ToString("000000000");
                    //    prevReceiptNo++;
                    //}
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(offlineMessages.ToArray()));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        [MaintenanceJobMethod("Filter Mixed Data from Offline Table")]
        public static void filterOfflineData()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            string userName = null;
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                Console.WriteLine("Enter User Name");
                userName= Console.ReadLine();
                if (userName.Equals("Q"))
                    return;
               
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                List<OfflineMessage> filtered = new List<OfflineMessage>();
                foreach (OfflineMessage msg in update.list.data)
                {

                    INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                    PaymentCenter pc = bdesub.GetPaymentCenterByCashAccount(rmsg.paymentCenterID);
                    if (pc.userName.Equals(userName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if(filtered.Count==0)
                            msg.previousMessageID=-1;
                        filtered.Add(msg);
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(filtered.ToArray()));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".trunc.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Fix Invalid Offline Data dates for Bizu")]
        public static void fixBizu()
        {
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                DateTime refDate = DateTime.Parse("7/15/2014 9:20 AM");
                DateTime date1 = DateTime.Parse("1/4/1980 1:57 PM");
                DateTime date2 = DateTime.Parse("1/4/1990 1:57 PM");
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.sentDate < date2)
                    {
                        msg.sentDate = refDate.Add(msg.sentDate.Subtract(date1));
                        ((INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData).payment.DocumentDate
                            = refDate.Add(((INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData).payment.DocumentDate.Subtract(date1));
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));

                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        [MaintenanceJobMethod("Fix nunu problem")]
        public static void fixNunu()
        {
            string file=@"C:\Projects\OromiaWSIS\Support\Bishoftu\12-export\0000000003-0000000120.posmessage";

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                int prevMsgNo = 124;
                int prevReceiptNo = 700122;
                List<OfflineMessage> list = new List<OfflineMessage>();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.id < 57)
                        continue;
                    msg.id = prevMsgNo + 1;
                    msg.previousMessageID = prevMsgNo;
                    prevMsgNo++;
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                        rmsg.payment.receiptNumber.reference = (prevReceiptNo + 1).ToString("000000000");
                        prevReceiptNo++;
                        list.Add(msg);
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(list.ToArray()));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Fix rounding problem")]
        public static void fixUnEqualBills()
        {
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
                        SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                        double total=0;
                        foreach(int billID in rmsg.payment.settledBills)
                        {
                            CustomerBillDocument doc=bdesub.Accounting.GetAccountDocument(billID,true) as CustomerBillDocument;
                            total+=doc.total;
                        }
                        if (Math.Abs(rmsg.payment.paymentInstruments[0].amount - total) < 1)
                            rmsg.payment.paymentInstruments[0].amount = total;
                        else
                            throw new Exception(string.Format("Too big difference {0} {1}", rmsg.payment.paymentInstruments[0].amount, total));
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Ticks: Fix corrupt update offline file")]
        public static void fixTicksCorruptUpdate()
        {
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        INTAPS.SubscriberManagment.PaymentReceivedMessage rmsg = (INTAPS.SubscriberManagment.PaymentReceivedMessage)msg.messageData;
                        rmsg.payment.DocumentDate = msg.sentDate;
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        [MaintenanceJobMethod("Offline: Reset updatelog chain")]
        public static void resetUpdatLog()
        {
            int prevReceiptNo = -1;
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                break;
            }
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                update.list.data[0].previousMessageID = -1;
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Offline: accept data update requests")]
        public static void acceptDataUpdateRequest()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            
            
            int periodID;
            BillPeriod billPeriod = null;
            do
            {
                Console.Write("Enter period name or id: ");
                String prd = Console.ReadLine();
                
                if (int.TryParse(prd,out periodID))
                {
                    billPeriod = bdesub.GetBillPeriod(periodID);
                    if(billPeriod!=null)
                    {
                        Console.WriteLine("Period selected: " + billPeriod.name);
                        break;
                    }
                }
                else
                {
                    int[] pids = bdesub.WriterHelper.GetColumnArray<int>("Select id from Subscriber.dbo.BillPeriod where name='{0}'".format(prd));
                    if(pids.Length==1)
                    {
                        periodID = pids[0];
                        billPeriod = bdesub.GetBillPeriod(periodID);
                        if (billPeriod != null)
                        {
                            Console.WriteLine("Period selected: " + billPeriod.name);
                            break;
                        }
                    }

                }
                Console.WriteLine("Invalid period name or id");
            } while (true);

            int count = (int)bdesub.WriterHelper.ExecuteScalar("Select count(*) from Subscriber.dbo.DataUpdateRequest where status=0 and  periodID={0}".format(periodID));
            Console.Write("{0} update requests found. Continue (Y/N)? ",count);
            if (!Console.ReadLine().Equals("Y"))
            {
                Console.WriteLine("Okay, we understand you are having a cold feet");
                return;
            }
            Console.WriteLine("Loading data..");
            DataTable dt=bdesub.WriterHelper.GetDataTable("Select connectionID,updateData from Subscriber.dbo.DataUpdateRequest where status=0 and periodID={0}".format(periodID));
            bdesub.WriterHelper.BeginTransaction();
            int c = dt.Rows.Count;
            Console.WriteLine();
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Offline: accept data update requests", "", "", -1);
                foreach(DataRow row in dt.Rows)
                {
                    int conid = (int)row[0];
                    String json = (String)row[1];
                    DataUpdate update = Newtonsoft.Json.JsonConvert.DeserializeObject<DataUpdate>(json);
                    Subscription subsc=bdesub.GetSubscription(update.connectionID,DateTime.Now.Ticks);
                    bool conMod = false;
                    bool custMod = false;
                    bool consStatusMode = false;
                    int nameChange = 0;
                    int phoneNoChange = 0;
                    int meterNoChnage = 0;
                    int coordinateChange = 0;
                    int otherChange = 0;
                    SubscriptionStatus conStatus = SubscriptionStatus.Unknown;
                    if (update.updatedFields == null)
                    {
                        Console.WriteLine("Data for Connection: " + subsc.contractNo + " is null.");
                        Console.WriteLine();
                        continue;
                    }
                    foreach (DataUpdate.DataUpdateField fld in update.updatedFields)
                    {
                        switch(fld.fieldName)
                        {
                            case "customerName":
                                subsc.subscriber.name = fld.value.ToString();
                                custMod = true;
                                nameChange++;
                                break;
                            case "phoneNo":
                                subsc.subscriber.phoneNo = fld.value.ToString();
                                custMod = true;
                                phoneNoChange++;
                                break;
                            case "meterNo":
                                subsc.serialNo= fld.value.ToString();
                                conMod = true;
                                meterNoChnage++;
                                break;
                            case "meterTypeID":
                                subsc.itemCode= fld.value.ToString();
                                conMod = true;
                                otherChange++;
                                break;
                            case "customerType":
                                subsc.subscriber.subscriberType = (SubscriberType)int.Parse(fld.value.ToString());
                                custMod = true;
                                otherChange++;
                                break;
                            case "connectionType":
                                subsc.connectionType = (ConnectionType)int.Parse(fld.value.ToString());
                                conMod = true;
                                otherChange++;
                                break;
                            case "connectionStatus":
                                conStatus= (SubscriptionStatus)int.Parse(fld.value.ToString());
                                consStatusMode = true;
                                break;
                            case "coordinate":
                                DataUpdate.GEOLocation loc = Newtonsoft.Json.JsonConvert.DeserializeObject<DataUpdate.GEOLocation>(fld.value.ToString());
                                subsc.waterMeterX = loc.lat;
                                subsc.waterMeterY = loc.lng;
                                conMod = true;
                                coordinateChange++;
                                break;
                        }
                        
                    }
                    if(subsc.subscriptionStatus==SubscriptionStatus.Discontinued)
                    {
                        Console.WriteLine("Connection: " + subsc.contractNo + " is discontinued. Data update is not expected. Please check.");
                        Console.WriteLine();
                        continue;
                    }
                    if (custMod)
                        bdesub.UpdateSubscriber(AID, subsc.subscriber, null);
                    bdesub.WriterHelper.ExecuteNonQuery("Update Subscriber.dbo.DataUpdateRequest set status=1  where status=0 and periodID={0} and connectionid={1}".format(periodID, subsc.id));
                    subsc.ticksFrom=billPeriod.toDate.AddDays(-1).Ticks;
                    if(conMod)
                    {
                        bdesub.changeSubscription(AID, subsc);
                    }
                    if(consStatusMode && conStatus!=subsc.subscriptionStatus)
                    {
                        if (conStatus == SubscriptionStatus.Active)
                            bdesub.ChangeSubcriptionStatus(AID, subsc.id, new DateTime(subsc.ticksFrom),conStatus,true);
                    }
                    Console.CursorTop--;
                    c--;
                    Console.WriteLine("{0}                                ", c);
                }
                bdesub.WriterHelper.CommitTransaction();
                Console.WriteLine("Done.");
            }
            catch(Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine("Sorry it didn't work.");
                while (ex != null)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                    ex = ex.InnerException;
                }
            }
        }

        [MaintenanceJobMethod("Offline: Fix Offline Wrong Date for Data File (Diredawa)")]
        public static void fixOfflineWrongDate()
        {
            string file;
            while (true)
            {
                Console.WriteLine("Enter file");
                file = Console.ReadLine();
                if (file.Equals("Q"))
                    return;
                if (!System.IO.File.Exists(file))
                {
                    Console.WriteLine("File doesn't exist");
                    continue;
                }
                break;
            }
            DateTime date1;
            while (true)
            {
                Console.WriteLine("Enter first receipt date and time: ");
                string str= Console.ReadLine();
                if (!DateTime.TryParse(str,out date1))
                {
                    Console.WriteLine("Please enter valid date: ");
                    continue;
                }
                break;
            }

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream serializationStream = null;
            try
            {
                serializationStream = File.OpenRead(file);
                PaymentCenterMessageList update = (PaymentCenterMessageList)formatter.Deserialize(serializationStream);
                serializationStream.Close();
                bool first = true;
                DateTime firstDate=date1;
                foreach (OfflineMessage msg in update.list.data)
                {
                    if (msg.messageData is INTAPS.SubscriberManagment.PaymentReceivedMessage)
                    {
                        DateTime newPaymentDate;
                        if (first)
                        {
                            newPaymentDate = date1;
                            first = false;
                            firstDate = ((PaymentReceivedMessage)msg.messageData).payment.DocumentDate;
                        }
                        else
                        {
                            newPaymentDate = date1.Add(((PaymentReceivedMessage)msg.messageData).payment.DocumentDate.Subtract(firstDate));
                        }
                        ((PaymentReceivedMessage)msg.messageData).payment.DocumentDate = newPaymentDate;
                    }
                }
                update = new PaymentCenterMessageList(update.paymentCenterID, new MessageList(update.list.data));
                FileInfo fi = new System.IO.FileInfo(file);
                string newFile = fi.Directory.FullName + "\\" + fi.Name + ".ofst.posmessage";
                System.IO.FileStream ostream = System.IO.File.Create(newFile);
                formatter.Serialize(ostream, update);
                ostream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


    }
}
