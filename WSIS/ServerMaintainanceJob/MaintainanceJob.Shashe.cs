using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using INTAPS.WSIS.Job;
using System.Linq;


namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {


        [MaintenanceJobMethod("Sashe Disaster: Restore lost receipts")]
        public static void shasheDisasterRecovery1()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.Write("Restore lost receipts. Press enter to continue");
                Console.ReadLine();

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string lostReceiptsSQL = @" Select  distinct paymentDocumentID,UsedSerial.val,CustomerBill.PaymentDate,UsedSerial.__AID from Subscriber.dbo.CustomerBill 
 inner join Accounting_2006.dbo.UsedSerial on CustomerBill.__AID=UsedSerial.__AID
 where paymentDocumentID not in (Select id from Accounting_2006.dbo.Document) and paymentDocumentID<>-1";
                DataTable lostReceiptTable = bdesub.WriterHelper.GetDataTable(lostReceiptsSQL);
                int custLength = lostReceiptTable.Rows.Count;

                Console.WriteLine(custLength + " receipts found");
                Console.WriteLine();
                int c = custLength;

                int i = 0;
                bdesub.WriterHelper.BeginTransaction();

                try
                {
                    foreach (DataRow row in lostReceiptTable.Rows)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c-- + "                      ");
                        var paymentDocumentID = (int)row[0];
                        var serial = (int)row[1];
                        var payTime = (DateTime)row[2];
                        var aid = (int)row[3];

                        var bills = bdesub.WriterHelper.GetColumnArray<int>($"Select id from Subscriber.dbo.CustomerBill where paymentDocumentID={paymentDocumentID}");
                        if (bills.Length == 0)
                            throw new Exception($"No bill found for payment id:{paymentDocumentID}");
                        var billRec = bdesub.getCustomerBillRecord(bills[0]);
                        for (var j = 1; j < bills.Length; j++)
                            if (bdesub.getCustomerBillRecord(bills[0]).customerID != billRec.customerID)
                                throw new Exception($"Customer mistmatch. Payment id:{paymentDocumentID}");
                        var total = 0.0d;
                        foreach (var b in bills)
                            total += (double)bdesub.WriterHelper.ExecuteScalar($"Select sum(price) from Subscriber.dbo.CustomerBillItem where customerBillID={b}");

                        var customer = bdesub.GetSubscriber(billRec.customerID);
                        if (customer == null)
                            throw new Exception($"Invalid customer. Payment id:{paymentDocumentID}");

                        var audit = ApplicationServer.SecurityBDE.getAuditInfo(aid);
                        if (audit == null)
                            throw new Exception($"Audit info not found:{paymentDocumentID}");

                        var paymentCenter = bdesub.GetPaymentCenter(audit.userName);
                        if (paymentCenter == null)
                            throw new Exception($"paymentCenter not found:{paymentDocumentID}");
                        if (paymentCenter.serialBatches.Length != 1)
                            throw new Exception($"Invalid number of serial batches");
                        var receipt = new CustomerPaymentReceipt();
                        receipt.AccountDocumentID = paymentDocumentID;
                        receipt.ShortDescription = "Restore receipt";
                        receipt.DocumentDate = payTime;

                        receipt.customer = customer;
                        receipt.settledBills = bills;
                        receipt.billBatchID = -1;
                        receipt.offline = false;
                        receipt.receiptNumber = new DocumentTypedReference(28, serial.ToString("000000000"), true);
                        receipt.PaperRef = receipt.receiptNumber.reference;
                        receipt.paymentInstruments = new PaymentInstrumentItem[] {
                            new PaymentInstrumentItem(){ instrumentTypeItemCode=bdesub.bERP.SysPars.cashInstrumentCode,amount=total}
                        };
                        receipt.assetAccountID = paymentCenter.casherAccountID;

                        int AID = ApplicationServer.SecurityBDE.WriteAudit(audit.userName, 1, "Sashe Disaster: Restore lost receipts", "", "", -1);

                        foreach (var b in bills)
                            bdesub.WriterHelper.ExecuteNonQuery($"Update Subscriber.dbo.CustomerBill set paymentDocumentID=-1 where id={b}");
                        bdesub.WriterHelper.ExecuteNonQuery($"delete from Accounting_2006.dbo.UsedSerial where batchID={paymentCenter.serialBatches[0].batchID} and val={serial}");
                        bdesub.Accounting.PostGenericDocument(AID, receipt);

                        i++;
                    }
                    bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                //custTable.Close();

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }


        [MaintenanceJobMethod("Sashe Disaster: Restore corrupt receipts")]
        public static void shasheDisasterRecovery2()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.Write("Sashe Disaster: Restore corrupt receipts. Press enter to continue");
                Console.ReadLine();

                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string lostReceiptsSQL = @"Select id from Accounting_2006.dbo.Document where DocumentTypeID=171
and ID not in (Select BatchID from Accounting_2006.dbo.AccountTransaction)";
                var corruptReceipts = bdesub.WriterHelper.GetColumnArray<int>(lostReceiptsSQL);
                int custLength = corruptReceipts.Length;

                Console.WriteLine(custLength + " receipts found");
                Console.WriteLine();
                int c = custLength;

                int i = 0;
                bdesub.WriterHelper.BeginTransaction();

                try
                {
                    foreach (int doc in corruptReceipts)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c-- + "                      ");
                        var receipt = bdesub.Accounting.GetAccountDocument<CustomerPaymentReceipt>(doc);
                        if (receipt == null)
                            throw new Exception($"Receipt couldn't be loaded. receipt id:{doc}");



                        var paymentCenter = bdesub.GetPaymentCenterByCashAccount(receipt.assetAccountID);
                        if (paymentCenter == null)
                            throw new Exception($"paymentCenter not found:{receipt.assetAccountID}");


                        int AID = ApplicationServer.SecurityBDE.WriteAudit(paymentCenter.userName, 1, "Sashe Disaster: Restore corrupt receipts. Press enter to continue", "", "", -1);
                        receipt.billItems = null;
                        foreach (var b in receipt.settledBills)
                            bdesub.WriterHelper.ExecuteNonQuery($"Update Subscriber.dbo.CustomerBill set paymentDocumentID=-1 where id={b}");
                        bdesub.WriterHelper.ExecuteNonQuery($"delete from Accounting_2006.dbo.UsedSerial where batchID={paymentCenter.serialBatches[0].batchID} and val={receipt.receiptNumber.serial}");
                        bdesub.Accounting.PostGenericDocument(AID, receipt);

                        i++;
                    }
                    bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                //custTable.Close();

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        static string loadTextData(string fn)
        {
            if (!System.IO.File.Exists(fn))
                throw new ServerUserMessage($"Data file {fn} doesnt' exist");
            return System.IO.File.ReadAllText(fn);
        }

        [MaintenanceJobMethod("Billing: generate new customer codes")]
        public static void billingGenerateNewCustomerCodes()
        {
            var fn = System.IO.Path.Combine("MJData", "GenerateNewCustomerCodes.txt");
            Console.WriteLine($"Loading data from {fn}");
            String[] customerCodes = loadTextData(fn).Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            customerCodes = customerCodes.Select(x => x.Trim()).ToArray();
            Console.WriteLine(customerCodes.Length + " customer codes loaded");
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            var bdeJob = ApplicationServer.GetBDE("JobManager") as JobManagerBDE; ;

            Console.WriteLine("Verifying custome codes");
            var valid = true;
            var set = new Dictionary<String,Subscriber>();
            for (int i=0;i<customerCodes.Length;i++)
            {
                Subscriber subsc;
                if((subsc=bdesub.GetSubscriber(customerCodes[i]))==null)
                {
                    Console.WriteLine($"Customer code {customerCodes[i]} at line {i + 1} is not valid");
                    valid = false;
                }
                if(set.ContainsKey(customerCodes[i]))
                {
                    Console.WriteLine($"Customer code {customerCodes[i]} at line {i + 1} is duplicate");
                    valid = false;
                }
                set.Add(customerCodes[i], subsc);
            }
            if (!valid)
                return;

            Console.Write($"Ready to process {customerCodes.Length} customers. Press enter to continue.");
            Console.ReadLine();

            bdesub.WriterHelper.setReadDB(bdesub.DBName);

            Console.WriteLine();
            int c = 1;
            Console.WriteLine();
            bdesub.WriterHelper.BeginTransaction();
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Billing: generate new customer codes", "", "", -1);

                foreach (var customerCode in customerCodes)
                {
                    
                    Console.WriteLine($"Processing {c++}/{customerCodes.Length}");

                    var customer = set[customerCode];
                    var newCode = bdesub.useNextCustomerCode(AID, customer);
                    if (bdesub.GetSubscriber(newCode) != null)
                        throw new ServerUserMessage($"New customer code {newCode} is already used");
                    if (!newCode.Equals(customerCode))
                    {
                        Console.Write($"Changing customer code {customerCode} to {newCode} ...");
                        //fix subscriber record
                        customer.amharicName = "Old Code: " + customer.customerCode;
                        customer.customerCode = newCode;
                        
                        bdesub.WriterHelper.logDeletedData(AID, $"{bdesub.DBName}.dbo.Subscriber", "id=" + customer.id);
                        bdesub.WriterHelper.Update(bdesub.DBName, "Subscriber", new string[] { "customerCode", "__AID" }, new object[] { newCode, AID }, "id=" + customer.id);
                        //fix related accounts
                        foreach (int ac in new int[] { customer.accountID, customer.depositAccountID })
                        {
                            if (ac > 0)
                            {
                                var account = bdesub.Accounting.GetAccount<Account>(customer.accountID);
                                if(account==null)
                                {
                                    throw new ServerUserMessage($"Account of customer {customerCode} id: {customer.accountID} doesn't exist in database");
                                }
                                account.Code = account.Code.Replace(customerCode, newCode);
                                bdesub.Accounting.UpdateAccount(AID, account);
                            }
                        }
                        Console.WriteLine("OK");
                    }
                    var connections = bdesub.GetSubscriptions(customer.id, DateTime.Now.Ticks);
                    if (connections.Length > 1)
                        throw new ServerUserMessage($"The maintenance job doesn't support multiple connections per customer. Custome code:{customerCode}");
                    if (connections.Length == 0)
                        continue;
                    var connection = connections[0];
                    var newContractNo = bdeJob.GetNextContractNumber(AID, connection);
                    if (bdesub.GetSubscription(newContractNo,DateTime.Now.Ticks) != null)
                        throw new ServerUserMessage($"New contract no {newContractNo} is already used");
                    if (!connection.contractNo.Equals(newContractNo))
                    {
                        Console.Write($"Changing contract no {connection.contractNo} to {newContractNo} ...");
                        connection.remark = "Old Contract No: " + connection.contractNo;
                        connection.contractNo = newContractNo;
                        bdesub.changeSubscription(AID, connection);
                        Console.WriteLine("OK");
                    }
                }
               
                bdesub.WriterHelper.CommitTransaction();
            }
            catch
            {
                bdesub.WriterHelper.RollBackTransaction();
                throw;
            }
        }
    }

}
