﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.WSIS.Job;
using INTAPS.SubscriberManagment;
namespace INTAPS.ClientServer.RemottingServer
{
    public static partial class MaintainanceJob
    {
        [MaintenanceJobMethod("Billing: list all document types in detailed customer receivable accounts")]
        public static void listDocumentTypesForCustomerDetail()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int n;
            Dictionary<int, List<AccountDocument>> docTypes = new Dictionary<int, List<AccountDocument>>();
            subsc.WriterHelper.setReadDB(subsc.DBName);
            Subscriber[] subscribers = subsc.WriterHelper.GetSTRArrayByFilter<Subscriber>(null);
            int c = subscribers.Length;
            Console.WriteLine("Processing {0} subscribers", c);
            Console.WriteLine();
            int cashHandoverDocumentTypeId = accounting.GetDocumentTypeByType(typeof(CashHandoverDocument)).id;
            int paymentDocumentTypeID = accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id;
            foreach (Subscriber subscriber in subscribers)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\t{1}                              ", c--, docTypes.Count);
                AccountBalance bal;
                if (subscriber.accountID < 1)
                    continue;
                AccountTransaction[] tran = accounting.GetTransactions(subscriber.accountID, 0, false, DateTime.Now, DateTime.Now, 0, -1, out n, out bal);
                foreach (AccountTransaction t in tran)
                {
                    AccountDocument doc = accounting.GetAccountDocument(t.documentID, false);
                    int docTypeID = doc.DocumentTypeID;
                    if (!docTypes.ContainsKey(docTypeID))
                        docTypes.Add(docTypeID, new List<AccountDocument>(new AccountDocument[] { doc }));
                    else
                        docTypes[docTypeID].Add(doc);
                }
                if (c % 500 == 0)
                {
                    foreach (KeyValuePair<int, List<AccountDocument>> dt in docTypes)
                    {
                        Console.WriteLine("{0}: {1}....{2}", dt, accounting.GetDocumentTypeByID(dt.Key).name, dt.Value.Count);
                    }
                    if (docTypes.Count > 0)
                        Console.WriteLine();
                }
            }
            displayDocumentResultListing(accounting, docTypes);
        }
        [MaintenanceJobMethod("Billing: list all document types in detailed payment accounts")]
        public static void listDocumentTypesForCashDetail()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            int n;
            Dictionary<int, List<AccountDocument>> docTypes = new Dictionary<int, List<AccountDocument>>();
            PaymentCenter[] pcs = subsc.GetAllPaymentCenters();
            int c = pcs.Length;
            Console.WriteLine("Processing {0} payment centers", c);
            Console.WriteLine();
            int cashHandoverDocumentTypeId = accounting.GetDocumentTypeByType(typeof(CashHandoverDocument)).id;
            int paymentDocumentTypeID = accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id;
            foreach (PaymentCenter pc in pcs)
            {
                Console.CursorTop--;
                Console.WriteLine(c-- + "                              ");
                AccountBalance bal;
                AccountTransaction[] tran = accounting.GetTransactions(pc.casherAccountID, 0, false, DateTime.Now, DateTime.Now, 0, -1, out n, out bal);
                foreach (AccountTransaction t in tran)
                {
                    AccountDocument doc = accounting.GetAccountDocument(t.documentID, false);
                    int docTypeID = doc.DocumentTypeID;
                    if (!docTypes.ContainsKey(docTypeID))
                        docTypes.Add(docTypeID, new List<AccountDocument>(new AccountDocument[] { doc }));
                    else
                        docTypes[docTypeID].Add(doc);
                }
            }
            displayDocumentResultListing(accounting, docTypes);
        }
        const int DISPLAY_PAGE_SIZE = 100;
        private static void displayDocumentResultListing(AccountingBDE accounting, Dictionary<int, List<AccountDocument>> docTypes)
        {
            do
            {
                foreach (KeyValuePair<int, List<AccountDocument>> dt in docTypes)
                {
                    Console.WriteLine("Type {0} ({1}):\t{2}", dt.Key, accounting.GetDocumentTypeByID(dt.Key).name, dt.Value.Count);
                }
                Console.Write("Enter document type ID to list documents or press enter to finish: ");
                string input = Console.ReadLine();
                if (input.Equals(""))
                    break;
                int docTypeID;
                if (int.TryParse(input, out docTypeID) && (docTypeID == -1 || docTypes.ContainsKey(docTypeID)))
                {
                    int count = 0;
                    Console.WriteLine("======Documents list begin=====");
                    if (docTypeID == -1)
                    {
                        foreach (KeyValuePair<int, List<AccountDocument>> dt in docTypes)
                        {
                            bool quit = false;
                            foreach (AccountDocument doc in dt.Value)
                            {
                                Console.WriteLine("{0}. \t\tRef:{1}\t\ttID:{2}\t\tType:{3}", count + 1, doc.PaperRef, doc.AccountDocumentID, accounting.GetDocumentTypeByID(doc.DocumentTypeID).name);
                                count++;
                                if (count % DISPLAY_PAGE_SIZE == 0)
                                {
                                    Console.WriteLine("Press enter to list more (or Q end listing).");

                                    if (Console.ReadLine() == "Q")
                                    {
                                        quit = true;
                                        break;
                                    }
                                }
                            }
                            if (quit)
                                break;
                        }
                    }
                    else
                        foreach (AccountDocument doc in docTypes[docTypeID])
                        {
                            Console.WriteLine("{0}. \t\tRef:{1}\t\ttID:{2}\t\tType:{3}", count + 1, doc.PaperRef, doc.AccountDocumentID, accounting.GetDocumentTypeByID(doc.DocumentTypeID).name);
                            count++;
                            if (count % DISPLAY_PAGE_SIZE == 0)
                            {
                                Console.WriteLine("Press enter to list more (or Q end listing).");
                                if (Console.ReadLine() == "Q")
                                {
                                    break;
                                }
                            }
                        }
                    Console.WriteLine("======Documents list end=====");
                }
                else
                {
                    Console.WriteLine("Please enter valid document type ID");
                }
            }
            while (true);
        }


        [MaintenanceJobMethod("Billing: fix double receipt summerization bug")]
        public static void billingFixDoubleReceiptSummerizationBug()
        {
            Console.WriteLine("Double receipt summerization bug fix tool.");
            Console.Write("Fix too? (Y=Fix,N=Only list):");
            bool fixToo = "Y".Equals(Console.ReadLine());
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            string cr = getDocumentFilterInput("Document", accounting, DocumentIDFilterFields.TimeLowerBound | DocumentIDFilterFields.TimeUpperBound);
            string sql = "Select id from {0}.dbo.Document where DocumentTypeID in ({1},{2}) {3}";
            sql = sql.format(
                accounting.DBName,
                accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id,
                accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceiptSummaryDocument)).id,
                string.IsNullOrEmpty(cr) ? "" : " AND ({0})".format(cr));
            int[] docIDS = bde.WriterHelper.GetColumnArray<int>(sql);
            Console.WriteLine("Processing {0} documents", docIDS.Length);
            Console.WriteLine();
            Dictionary<int, List<int>> summerizedReceipts = new Dictionary<int, List<int>>();
            Dictionary<int, CustomerPaymentReceipt> receipts = new Dictionary<int, CustomerPaymentReceipt>();
            Dictionary<int, CustomerPaymentReceiptSummaryDocument> summaries = new Dictionary<int, CustomerPaymentReceiptSummaryDocument>();
            int c = docIDS.Length;
            int nR = 0, nS = 0;
            foreach (int d in docIDS)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}                           ", c--);

                AccountDocument doc = accounting.GetAccountDocument(d, true);
                if (doc is CustomerPaymentReceipt)
                {
                    receipts.Add(doc.AccountDocumentID, (CustomerPaymentReceipt)doc);
                    nR++;
                }
                else
                {
                    CustomerPaymentReceiptSummaryDocument sum = (CustomerPaymentReceiptSummaryDocument)doc;
                    summaries.Add(doc.AccountDocumentID, sum);
                    nS++;
                    foreach (int r in sum.receiptIDs)
                    {
                        if (summerizedReceipts.ContainsKey(r))
                            summerizedReceipts[r].Add(sum.AccountDocumentID);
                        else
                            summerizedReceipts.Add(r, new List<int>(new int[] { sum.AccountDocumentID }));
                    }
                }
            }
            int nullBatchID = 0;
            int nFixable = 0;
            int invalidBatchID = 0;
            foreach (KeyValuePair<int, CustomerPaymentReceipt> kv in receipts)
            {
                if (kv.Value.billBatchID == -1)
                {
                    Console.WriteLine("-1 billBatchID: Document ID:{0}", kv.Key);
                    if (summerizedReceipts.ContainsKey(kv.Key))
                        nFixable++;
                    nullBatchID++;
                }
                else if (!summaries.ContainsKey(kv.Value.billBatchID))
                {
                    Console.WriteLine("Nonexistent billBatchID: Document ID:{0}", kv.Key);
                    invalidBatchID++;
                }
            }
            Console.WriteLine("Begin summary list");
            int nSummerized = 0;
            CustomerPaymentReceiptSummaryDocument[] sumArray = new CustomerPaymentReceiptSummaryDocument[summaries.Count];
            summaries.Values.CopyTo(sumArray, 0);
            Array.Sort(sumArray, (x, y) => x.DocumentDate.CompareTo(y.DocumentDate));
            foreach (CustomerPaymentReceiptSummaryDocument doc in sumArray)
            {
                Console.WriteLine("Summary Document ID:{0}  count:{1}", doc.DocumentDate, doc.receiptIDs.Length);
                nSummerized += doc.receiptIDs.Length;
                if (doc.receiptIDs.Length == 0)
                {
                    if (fixToo)
                    {
                        try
                        {
                            accounting.WriterHelper.BeginTransaction();
                            accounting.DeleteAccountDocument(-1, doc.AccountDocumentID, false);
                            accounting.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            accounting.WriterHelper.RollBackTransaction();
                            Console.WriteLine("Delete failed for {0}\n{1}", doc.DocumentDate, ex.Message);

                        }
                    }
                }
            }
            Console.WriteLine("End summary list");

            Console.WriteLine("{0} Receipts {1} Summary {2} summerized", nR, nS, nSummerized);
            Console.WriteLine("{0} null batch ID {1} invalid batchID {2} fixable null", nullBatchID, invalidBatchID, nFixable);
            Console.WriteLine("Begin list");
            foreach (KeyValuePair<int, List<int>> kv in summerizedReceipts)
            {
                if (kv.Value.Count > 1)
                {
                    Console.WriteLine("Document ID:{0}  count:{1}", kv.Key, kv.Value.Count);
                }
            }
            Console.WriteLine("End list");
            Console.ReadLine();
        }


        [MaintenanceJobMethod("Billing: fix double receipt summerization bug second type")]
        public static void billingFixDoubleReceiptSummerizationBug2()
        {
            Console.WriteLine("Double receipt summerization bug fix tool.");
            Console.Write("Fix too? (Y=Fix,N=Only list):");
            bool fixToo = "Y".Equals(Console.ReadLine());
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            string cr = getDocumentFilterInput("Document", accounting, DocumentIDFilterFields.TimeLowerBound | DocumentIDFilterFields.TimeUpperBound);
            string sql = "Select id from {0}.dbo.Document where DocumentTypeID in ({1}) {2}";
            sql = sql.format(
                accounting.DBName,
                accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceiptSummaryDocument)).id,
                string.IsNullOrEmpty(cr) ? "" : " AND ({0})".format(cr));
            int[] docIDS = bde.WriterHelper.GetColumnArray<int>(sql);
            int c = docIDS.Length;
            Console.WriteLine("Processing {0} summerization documents", docIDS.Length);
            foreach(int docID in docIDS)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}                           ", c--);

                CustomerPaymentReceiptSummaryDocument sum = (CustomerPaymentReceiptSummaryDocument)accounting.GetAccountDocument(docID, true);
                
                Dictionary<int, int> counts = new Dictionary<int, int>();
                
                foreach(int r in sum.receiptIDs)
                {
                    if (counts.ContainsKey(r))
                        counts[r] = counts[r] + 1;
                    else
                        counts.Add(r, 1);
                }
                bool isDoubleBug = !counts.Values.Where(x => x != 2).Any() && sum.receiptIDs.Length > 0;
                if (isDoubleBug)
                {
                    Console.WriteLine($"Document ID:{docID} ref:{sum.PaperRef} is a double summerization bug case type 2");
                    if (fixToo)
                    {
                        DocumentSerial[] refs = bde.Accounting.getDocumentSerials(docID);
                        try
                        {
                            bde.WriterHelper.BeginTransaction();
                            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job-Billing: fix double receipt summerization bug second type", "", -1);
                            List<int> ids = new List<int>();
                            foreach (int r in sum.receiptIDs)
                                if (!ids.Contains(r))
                                    ids.Add(r);
                            sum.receiptIDs = ids.ToArray();
                            TransactionSummerizer summerizer = new TransactionSummerizer(bde.Accounting, subsc.bERP.SysPars.summaryRoots);
                            foreach(int r in sum.receiptIDs)
                            {
                                AccountTransaction[] trans = bde.Accounting.GetTransactionsOfDocument(r);
                                foreach (AccountTransaction tt in trans)
                                    if (tt.ItemID == 0)
                                        summerizer.addTransaction(AID, new TransactionOfBatch(tt));
                            }
                            sum.transactions = summerizer.getSummary();
                            bde.Accounting.PostGenericDocument(AID, sum);
                            bde.Accounting.addDocumentSerials(AID, sum, DocumentTypedReference.createArray(refs));
                            bde.WriterHelper.CommitTransaction();
                            Console.Write(".....Fixed!");
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine($"Failed to fix document ID:{docID} ref:{sum.PaperRef}.\n{ex.Message}\n{ex.StackTrace}");
                            Console.ReadLine();
                        }
                    }
                    Console.WriteLine();
                }

            }

        }
    }
}
