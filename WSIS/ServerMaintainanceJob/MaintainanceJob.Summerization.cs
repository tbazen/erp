﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using INTAPS.RDBMS;
using System.Xml.Serialization;
namespace INTAPS.ClientServer.RemottingServer
{
    public static partial class MaintainanceJob
    {
        [MaintenanceJobMethod("Accounting-summerization: report direct entry transactions")]
        public static void accountingSummerizationReportDirectEntery()
        {

            int N;
            Console.WriteLine("Reposting direct entry documents");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            INTAPS.CachedObject<int, string> accountCodes = new INTAPS.CachedObject<int, string>
            (
            new RetrieveObjectDelegate<int, string>(delegate(int id)
            {
                CostCenterAccount csa = accounting.GetCostCenterAccount(id);
                return accounting.GetAccount<Account>(csa.accountID).Code;
            })
            );

            accounting.WriterHelper.setReadDB(accounting.DBName);

            try
            {

                int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Accounting-summerization: report direct entry transactions", "", -1);

                int c;

                int[] docs = accounting.WriterHelper.GetColumnArray<int>("Select id from Accounting_2006.dbo.Document where documentTypeID="+accounting.GetDocumentTypeByType(typeof(AdjustmentDocument)).id);
                Console.WriteLine(docs.Length + " direct entires found");
                Console.WriteLine();
                c = docs.Length;
                int f = 0;
                INTAPS.RDBMS.SQLHelper reader = accounting.GetReaderHelper();
                for (int i = docs.Length - 1; i >= 0; i--)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                      ", (c--), f);
                    bool isCand;
                    if (isSummerized(accounting,reader, accountCodes, docs[i], out isCand))
                        continue;
                    AdjustmentDocument adj = accounting.GetAccountDocument(docs[i], true) as AdjustmentDocument;
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        accounting.PostGenericDocument(AID, adj);
                        f++;
                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine("Error processing adjustment ID:{0} ref:{1}\n{2}", adj.AccountDocumentID, adj.PaperRef, ex.Message);
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }
        static bool isSummerized(AccountingBDE accounting,SQLHelper reader,  INTAPS.CachedObject<int, string> accountCodes,int documentID, out bool isCand)
        {
            AccountTransaction[] trans = accounting.GetTransactionOfBatchInternal(reader,documentID);
            double balance = 0;
            isCand = false;
            foreach (AccountTransaction t in trans)
            {
                if (t.ItemID != 0)
                    continue;
                if (accountCodes[t.AccountID].IndexOf("ITEM") == 0 || accountCodes[t.AccountID].IndexOf("CUST") == 0)
                {
                    balance += t.Amount;
                    isCand = true;
                }
            }
            return AccountBase.AmountEqual(balance, 0);
        }
        [MaintenanceJobMethod("Accounting-summerization: filter unsummerized documents")]
        public static void accountingSummerizationFilterUnsummerizedDocuments()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            INTAPS.CachedObject<int, Account> accountCache = new CachedObject<int, Account>
            (
            new RetrieveObjectDelegate<int, Account>(delegate(int id)
            {
                return accounting.GetAccount<Account>(id);
            })
            );
            INTAPS.CachedObject<int, string> accountCodes = new INTAPS.CachedObject<int, string>
            (
            new RetrieveObjectDelegate<int, string>(delegate(int id)
            {
                CostCenterAccount csa = accounting.GetCostCenterAccount(id);
                return accountCache[csa.accountID].Code;
            })
            );
            string cr = null;
            
            String input;

            System.Console.Write("Enter lower time bound filter (Enter for no time filter):");
            while (!(input = System.Console.ReadLine()).Equals(""))
            {
                DateTime fromDate;
                if (DateTime.TryParse(input, out fromDate))
                {
                    cr=INTAPS.StringExtensions.AppendOperand(cr," AND ","tranTicks>="+fromDate.Ticks);
                    break;
                }
                else
                    Console.Write("Enter valid date or press enter: ");

            }
            List<int> documentTypes=new List<int>();
            System.Console.Write("Enter document typeID filter (Enter for no document type filter):");
            while (!(input = System.Console.ReadLine()).Equals(""))
            {
                int documentTypeID;
                DocumentType docType;
                if (int.TryParse(input, out documentTypeID) && (docType= accounting.GetDocumentTypeByID(documentTypeID))!=null)
                {
                    Console.WriteLine("Document type {0} added to filter",docType.name);
                    System.Console.Write("Enter another document typeID filter (Enter to finish):");
                    documentTypes.Add(documentTypeID);
                }
                else
                    Console.Write("Enter valid document ID or press enter: ");
            }

            List<int> exculdeType = new List<int>();
            System.Console.Write("Enter document typeID exclusion filter (Enter for no exclusion):");
            while (!(input = System.Console.ReadLine()).Equals(""))
            {
                int documentTypeID;
                DocumentType docType;
                if (int.TryParse(input, out documentTypeID) && (docType = accounting.GetDocumentTypeByID(documentTypeID)) != null)
                {
                    Console.WriteLine("Document type {0} added to exclusion filter", docType.name);
                    System.Console.Write("Enter another document typeID exclusion filter (Enter to finish):");
                    exculdeType.Add(documentTypeID);
                }
                else
                    Console.Write("Enter valid document ID or press enter: ");
            }
            if(documentTypes.Count>0)
            {
                string docTypeFilter=null;
                foreach(int docTypeID in documentTypes)
                    docTypeFilter=INTAPS.StringExtensions.AppendOperand(docTypeFilter,",",docTypeID.ToString());
                cr=INTAPS.StringExtensions.AppendOperand(cr," AND ",string.Format("DocumentTypeID in ({0})",docTypeFilter));
            }


            if (exculdeType.Count > 0)
            {
                string excl = null;
                foreach (int docTypeID in exculdeType)
                    excl = INTAPS.StringExtensions.AppendOperand(excl, ",", docTypeID.ToString());
                cr = INTAPS.StringExtensions.AppendOperand(cr, " AND ", string.Format("DocumentTypeID not in ({0})", excl));
            }
            try
            {
                int N;
                Console.WriteLine("Loading transactions IDs");
                accounting.WriterHelper.setReadDB(accounting.DBName);

                int c;
                if(!string.IsNullOrEmpty(cr))
                    cr="where "+cr;
                
                int[] docs = accounting.WriterHelper.GetColumnArray<int>("Select id from Accounting_2006.dbo.Document "+cr);
                Console.WriteLine(docs.Length + " transactions found");
                Console.WriteLine();
                c = docs.Length;
                List<int> errorDocs = new List<int>();
                int cand = 0;
                INTAPS.RDBMS.SQLHelper reader = accounting.GetReaderHelper();
                for (int i = docs.Length - 1; i >= 0; i--)
                {
                    if (i % 100 == 0)
                    {
                        if (Console.KeyAvailable)
                        {
                            Console.ReadKey();
                            Console.WriteLine("Press Y to stop searching and display result");
                            if (Console.ReadLine() == "Y")
                            {
                                break;
                            }

                        }
                    }
                    Console.CursorTop--;
                    Console.WriteLine("{0}\tCand: {1}\tError: {2}                 ", c--, cand, errorDocs.Count);
                    bool isCand = false;
                    if(!isSummerized(accounting, reader,accountCodes, docs[i],out isCand))
                        errorDocs.Add(docs[i]);
                    if (isCand)
                        cand++;
                    //AccountTransaction[] trans = accounting.GetTransactionsOfDocument(docs[i]);
                    //double balance = 0;
                    //bool isCand = false;
                    //foreach (AccountTransaction t in trans)
                    //{
                    //    if (t.ItemID != 0)
                    //        continue;
                    //    if (accountCodes[t.AccountID].IndexOf("ITEM") == 0 || accountCodes[t.AccountID].IndexOf("CUST") == 0)
                    //    {
                    //        balance += t.Amount;
                    //        isCand = true;
                    //    }
                    //}
                    //if (isCand)
                    //    cand++;
                    //if (!AccountBase.AmountEqual(balance, 0))
                    //{
                    //    errorDocs.Add(docs[i]);
                    //}

                }

                if (errorDocs.Count > 0)
                {
                    Console.WriteLine("Unblanced documents.");
                    for (int i = 0; i < errorDocs.Count; i++)
                    {
                        AccountDocument doc = accounting.GetAccountDocument(errorDocs[i], false);
                        string refs = "";
                        foreach (DocumentSerial s in accounting.getDocumentSerials(doc.AccountDocumentID))
                        {
                            refs += accounting.getDocumentSerialType(s.typeID).prefix + ":" + s.reference + " ";
                        }
                        Console.WriteLine("{0}. \tRef1:{4}\tRef2:{1}\tID:{2}\tType:{3}\tDate:{5}", i + 1, doc.PaperRef, doc.AccountDocumentID, accounting.GetDocumentTypeByID(doc.DocumentTypeID).name,refs,doc.DocumentDate.ToString());
                        if (i % 20 == 19)
                        {
                            Console.WriteLine("Press enter to list more.");
                            Console.ReadLine();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }

        }

        [MaintenanceJobMethod("Accounting - Re-post Deleted Inventory Document By ID and __DAID", "spatni")]
        public static void RepostDeletedDocument()
        {
            Console.Write("Enter Document ID (From Document_Deleted Table]:");
            int documentId;
            while (!int.TryParse(Console.ReadLine(), out documentId) || documentId <= 0)
            {
                Console.Write("Invalid Document ID. Enter again:");
            }
            Console.Write("Enter __DAID (From Document_Deleted Table]:");
            int daid;
            while (!int.TryParse(Console.ReadLine(), out daid) || daid <= 0)
            {
                Console.Write("Invalid __DAID. Enter again:");
            }
            try
            {
                iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                bdeBERP.WriterHelper.BeginTransaction();
                try
                {
                    //decerialize from document_deleted DocumentData field
                    int AID = ApplicationServer.SecurityBDE.WriteExecuteAudit("admin", 1, "Restore deleted document", -1);
                    InventoryDocument doc = (InventoryDocument)bdeBERP.Accounting.GetDeletedAccountDocument(documentId,daid);
                    doc.voucher = bdeBERP.Accounting.GetDeletedDocumentSerial(documentId, daid);
                    bdeBERP.Accounting.PostGenericDocument(AID, doc);
                    bdeBERP.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeBERP.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed\n" + ex.Message);
            }
        }

        [MaintenanceJobMethod("Accounting - Re-post Deleted General Document By ID and __DAID", "spatni")]
        public static void RepostDeletedDocumentGeneral()
        {
            Console.WriteLine("Restore deleted document. Note that this will not restore refernces");
            Console.Write("Enter Document ID (From Document_Deleted Table]:");
            int documentId;
            while (!int.TryParse(Console.ReadLine(), out documentId) || documentId <= 0)
            {
                Console.Write("Invalid Document ID. Enter again:");
            }
            Console.Write("Enter __DAID (From Document_Deleted Table]:");
            int daid;
            while (!int.TryParse(Console.ReadLine(), out daid) || daid <= 0)
            {
                Console.Write("Invalid __DAID. Enter again:");
            }
            try
            {
                iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                bdeBERP.WriterHelper.BeginTransaction();
                try
                {
                    //decerialize from document_deleted DocumentData field
                    int AID = ApplicationServer.SecurityBDE.WriteExecuteAudit("admin", 1, "Restore deleted document", -1);
                    AccountDocument doc = bdeBERP.Accounting.GetDeletedAccountDocument(documentId, daid);
                    //doc.voucher = bdeBERP.Accounting.GetDeletedDocumentSerial(documentId, daid);
                    bdeBERP.Accounting.PostGenericDocument(AID, doc);
                    bdeBERP.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeBERP.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed\n" + ex.Message);
            }
        }


        internal class SellDocumentModel
        {
            public int DocumentID { get; set; }
            public int DAID { get; set; }
        }

        [MaintenanceJobMethod("Accounting - Re-post Deleted Sells of Goods/Services [For GAST Only]", "spatni")]
        public static void RepostDeletedSellsDocument()
        {
            List<SellDocumentModel> documents = new List<SellDocumentModel>();
            documents.Add(new SellDocumentModel() { DocumentID = 1434, DAID = 20418 });//1434 20418
            documents.Add(new SellDocumentModel() { DocumentID = 1435, DAID = 20416 });//1435 20416
            documents.Add(new SellDocumentModel() { DocumentID = 1436, DAID = 20414 });//1436 20414
            documents.Add(new SellDocumentModel() { DocumentID = 1447, DAID = 20412 });//1447 20412
            documents.Add(new SellDocumentModel() { DocumentID = 1448, DAID = 20410 });//1448 20410
            documents.Add(new SellDocumentModel() { DocumentID = 1449, DAID = 20240 });//1449 20240
            documents.Add(new SellDocumentModel() { DocumentID = 1450, DAID = 20238 });//1450 20238
            documents.Add(new SellDocumentModel() { DocumentID = 1451, DAID = 20236 });//1451 20236
            documents.Add(new SellDocumentModel() { DocumentID = 1452, DAID = 20234 });//1452 20234
            documents.Add(new SellDocumentModel() { DocumentID = 1453, DAID = 20232 });//1453 20232
            documents.Add(new SellDocumentModel() { DocumentID = 1454, DAID = 20230 });//1454 20230
            documents.Add(new SellDocumentModel() { DocumentID = 1455, DAID = 20228 });//1455 20228
            documents.Add(new SellDocumentModel() { DocumentID = 1456, DAID = 20226 });//1456 20226
            documents.Add(new SellDocumentModel() { DocumentID = 1457, DAID = 20220 });//1457 20220
            documents.Add(new SellDocumentModel() { DocumentID = 1458, DAID = 20224 });//1458 20224
            documents.Add(new SellDocumentModel() { DocumentID = 1459, DAID = 20222 });//1459 20222
            documents.Add(new SellDocumentModel() { DocumentID = 1460, DAID = 20218 });//1460 20218
            documents.Add(new SellDocumentModel() { DocumentID = 1461, DAID = 20216 });//1461 20216
            documents.Add(new SellDocumentModel() { DocumentID = 1462, DAID = 20214 });//1462 20214
            documents.Add(new SellDocumentModel() { DocumentID = 1463, DAID = 20212 });//1463 20212
            documents.Add(new SellDocumentModel() { DocumentID = 1464, DAID = 20210 });//1464 20210
            documents.Add(new SellDocumentModel() { DocumentID = 1465, DAID = 20208 });//1465 20208
            documents.Add(new SellDocumentModel() { DocumentID = 1466, DAID = 20206 });//1466 20206
            documents.Add(new SellDocumentModel() { DocumentID = 1467, DAID = 20204 });//1467 20204
            documents.Add(new SellDocumentModel() { DocumentID = 1468, DAID = 20202 });//1468 20202
            documents.Add(new SellDocumentModel() { DocumentID = 1469, DAID = 20200 });//1469 20200
            documents.Add(new SellDocumentModel() { DocumentID = 1470, DAID = 20198 });//1470 20198
            documents.Add(new SellDocumentModel() { DocumentID = 1471, DAID = 20192 });//1471 20192
            documents.Add(new SellDocumentModel() { DocumentID = 1472, DAID = 20196 });//1472 20196
            documents.Add(new SellDocumentModel() { DocumentID = 1473, DAID = 20194 });//1473 20194
            documents.Add(new SellDocumentModel() { DocumentID = 1474, DAID = 20190 });//1474 20190
            documents.Add(new SellDocumentModel() { DocumentID = 1474, DAID = 20857 });//1474 20857
            documents.Add(new SellDocumentModel() { DocumentID = 1475, DAID = 20858 });//1475 20858
            documents.Add(new SellDocumentModel() { DocumentID = 1475, DAID = 20188 });//1475 20188
            documents.Add(new SellDocumentModel() { DocumentID = 1476, DAID = 20186 });//1476 20186
            documents.Add(new SellDocumentModel() { DocumentID = 1476, DAID = 20859 });//1476 20859
            documents.Add(new SellDocumentModel() { DocumentID = 1477, DAID = 20860 });//1477 20860
            documents.Add(new SellDocumentModel() { DocumentID = 1477, DAID = 20184 });//1477 20184
            documents.Add(new SellDocumentModel() { DocumentID = 1478, DAID = 20182 });//1478 20182
            documents.Add(new SellDocumentModel() { DocumentID = 1478, DAID = 20861 });//1478 20861

            foreach (var document in documents)
            {
                try
                {
                    iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                    bdeBERP.WriterHelper.BeginTransaction();
                    try
                    {
                        Console.WriteLine("Document ID={0}", document.DocumentID);
                        //decerialize from document_deleted DocumentData field
                        int AID = ApplicationServer.SecurityBDE.WriteExecuteAudit("admin", 1, "Restore deleted document", -1);
                        Sell2Document doc = (Sell2Document)bdeBERP.Accounting.GetDeletedAccountDocument(document.DocumentID, document.DAID);
                        doc.salesVoucher = bdeBERP.Accounting.GetDeletedDocumentSerial(document.DocumentID, document.DAID);
                        bdeBERP.Accounting.PostGenericDocument(AID, doc);
                        bdeBERP.WriterHelper.CommitTransaction();
                        Console.WriteLine("{0} reposted successfully", document.DocumentID);
                    }catch
                    {
                        bdeBERP.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed\n" + ex.Message);
                }
            }
            
        }
    }
}
