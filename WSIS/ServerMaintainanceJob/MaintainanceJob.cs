using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;

using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Threading;
using INTAPS.RDBMS;
using BIZNET.iERP;
using INTAPS.WSIS.Job;


namespace INTAPS.ClientServer.RemottingServer
{
    class MaintenanceJobMethod : Attribute
    {
        public string name;
        public string password = null;
        public MaintenanceJobMethod(string n)
        {
            this.name = n;
        }
        public MaintenanceJobMethod(string n, string pw)
        {
            this.name = n;
            this.password = pw;
        }
    }
    public static partial class MaintainanceJob
    {
        public static void DoJob(string args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            try
            {
                Console.WriteLine("Which job do want to run?");
                SortedList<string, MethodInfo> methods = new SortedList<string, MethodInfo>();
                Dictionary<string, MaintenanceJobMethod> methodData = new Dictionary<string, MaintenanceJobMethod>();
                foreach (MethodInfo m in typeof(MaintainanceJob).GetMethods())
                {
                    object[] atr = m.GetCustomAttributes(typeof(MaintenanceJobMethod), false);
                    if (atr == null || atr.Length == 0)
                        continue;
                    MaintenanceJobMethod jb = atr[0] as MaintenanceJobMethod;
                    methodData.Add(jb.name, jb);
                    methods.Add(jb.name, m);
                }
                int count = 1;

                foreach (KeyValuePair<string, MethodInfo> kv in methods)
                {
                    Console.WriteLine("{0}:\t{1}", count++, kv.Key);
                }
                Console.WriteLine("{0}:\t{1}", "Q", "Quit");
                do
                {
                    Console.Write("?: ");
                    string read = Console.ReadLine();
                    if (read.Equals("Q", StringComparison.CurrentCultureIgnoreCase))
                        return;
                    if (int.TryParse(read, out count) && count <= methods.Count && count > 0)
                    {
                        MaintenanceJobMethod data = methodData[methods.Keys[count - 1]];
                        if (!string.IsNullOrEmpty(data.password))
                        {
                            do
                            {

                                Console.Write("Enter password:");

                                String typedPass = "";
                                while (true)
                                {
                                    ConsoleKeyInfo key = Console.ReadKey(true);
                                    if (key.Key == ConsoleKey.Enter)
                                        break;
                                    typedPass += key.KeyChar;
                                }
                                if (!typedPass.Equals(data.password))
                                    Console.WriteLine("Invalid password");
                                else
                                    break;

                            } while (true);
                            Console.WriteLine();
                        }
                        methods.Values[count - 1].Invoke(null, new object[0]);
                        return;

                    }
                }
                while (true);
            }
            catch (Exception bex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Job error");
                while (bex != null)
                {
                    Console.WriteLine(bex.Message);
                    Console.WriteLine(bex.StackTrace);
                    bex = bex.InnerException;
                }
                Console.WriteLine();

            }
            finally
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Job Finished, press enter to terminate application");
                Console.ReadLine();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }


        [MaintenanceJobMethod("Convert Date To Ticks")]
        public static void convertToTock()
        {
            Console.WriteLine("Enter time");
        }

        [MaintenanceJobMethod("Repost empty bills")]
        public static void repostEmptyBills()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            try
            {
                CustomerBillRecord[] recs = bdesub.getBills(-1, -1, -1, -1);
                int c = recs.Length;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Console.WriteLine("Processing " + c + " records");
                Console.WriteLine();
                int fixCount = 0;
                foreach (CustomerBillRecord rec in recs)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                  \tFixcount " + fixCount + "                 ");
                    BillItem[] items = bdesub.getCustomerBillItems(rec.id);
                    if (items.Length > 0)
                    {
                        TransactionOfBatch[] trans = bdesub.Accounting.GetTransactionsOfDocument(rec.id);
                        if (trans.Length == 0 && rec.billDocumentTypeID == 5)
                        {
                            CustomerBillDocument docs = bdesub.Accounting.GetAccountDocument(rec.id, true) as CustomerBillDocument;
                            docs.draft = false;
                            bdesub.WriterHelper.BeginTransaction();
                            try
                            {
                                bdesub.Accounting.PostGenericDocument(-1, docs);
                                bdesub.WriterHelper.Update(bdesub.DBName, "CustomerBill", new string[] { "draft" }, new object[] { 0 }, "id=" + rec.id);
                                fixCount++;
                                bdesub.WriterHelper.CommitTransaction();
                            }
                            catch (Exception ex)
                            {
                                bdesub.WriterHelper.RollBackTransaction();
                                throw ex;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
        [MaintenanceJobMethod("Fix Period Names")]
        public static void fixBillPeriodNames()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            try
            {
                //foreach (System.Globalization.CultureInfo ci in System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures))
                //{
                //Console.WriteLine(ci.ThreeLetterISOLanguageName);
                //}
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                BillPeriod[] periods = bdesub.WriterHelper.GetSTRArrayByFilter<BillPeriod>(null);
                foreach (BillPeriod bp in periods)
                {
                    INTAPS.Ethiopic.EtGrDate d = INTAPS.Ethiopic.EtGrDate.ToEth(bp.fromDate);
                    bp.name = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(d.Month, 0) + " " + d.Year;
                    bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, bp);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }

        [MaintenanceJobMethod("Allocate kebeles")]
        public static void allocateKebeles()
        {
            //string[] emps = new string[] { "SYSK01", "SYSK02", "SYSK03", "SYSK04", "A067", "A057", "A055",
            //    "SYSK09","SYSK10", "SYSK11", "A054", "A048", "F033", "F031", "A061" };
            //int[][] ks = new int[][] { 
            //    new int[] { 1 }, new int[] { 2 }, new int[] { 3 }, new int[] { 4 }, new int[] { 5 }
            //    , new int[] { 6 }, new int[] { 7 },
            //    new int[] { 9 },new int[] { 10 }, new int[] { 11 }, new int[] { 12 }, new int[] { 13 }, new int[] { 14 }, new int[] { 16 }, new int[] { 21 } };
            string[] emps = new string[] { "A055", "A067", "A054", "A057" };
            int[][] ks = new int[][] {new int[]{17,19,20}, new int[] { 05,18 }, new int[] {12, 15 }, new int[] {06, 08 }
                };
            allocateKebeles(emps, ks);
        }
        public static void allocateKebeles(string[] empIDs, int[][] kebeles)
        {

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            try
            {
                BillPeriod bp = bdesub.getPeriodForDate(DateTime.Parse("July 18,2014"));
                if (bp == null)
                    throw new Exception("Configure Hamle 2006 bill period");
                ReadingEntryClerk clerk = bdesub.BWFGetClerkByUserID("admin");
                if (clerk == null)
                    throw new Exception("Setup system reading clerk");
                int i = 0;
                int blockN = (int)bdesub.WriterHelper.ExecuteScalar("Select max(cast(blockName as int)) from Subscriber.dbo.BWFReadingBlock where periodID=" + bp.id) + 1;
                foreach (string strID in empIDs)
                {
                    Employee emp = bdesub.bdePayroll.GetEmployeeByID(strID);
                    if (emp == null)
                        throw new Exception("Invalid employee ID:" + strID);
                    Console.WriteLine("Processing " + emp.employeeName);
                    Console.WriteLine("Deleting existing blocks");
                    BWFReadingBlock[] blocks = bdesub.GetBlocks(bp.id, emp.id);
                    foreach (BWFReadingBlock block in blocks)
                    {
                        int N;
                        foreach (BWFMeterReading r in bdesub.GetReadings(block.id, 0, -1, out N))
                        {
                            bdesub.BWFDeleteMeterReading(-1, clerk, r.subscriptionID, r.periodID);
                        }
                        bdesub.BWFDeleteReadingBlock(-1, clerk, block.id);
                    }

                    Console.WriteLine("Creating blocks");


                    foreach (int k in kebeles[i])
                    {
                        Console.WriteLine("Kebele " + k);
                        Subscription[] subsc = bdesub.WriterHelper.GetSTRArrayByFilter<Subscription>("Kebele=" + k);
                        int nblocks = subsc.Length / 200;
                        nblocks = nblocks + (subsc.Length % 200 == 0 ? 0 : 1);
                        Console.WriteLine();
                        int c = nblocks;
                        for (int b = 0; b < nblocks; b++)
                        {
                            Console.CursorTop--;
                            Console.WriteLine((c--) + "                               ");
                            BWFReadingBlock blk = new BWFReadingBlock();
                            blk.blockName = blockN.ToString("0000");
                            blockN++;
                            blk.entryClerkID = clerk.employeeID;
                            blk.periodID = bp.id;
                            blk.readerID = emp.id;
                            blk.readingStart = DateTime.Now;
                            blk.readingEnd = DateTime.Now;
                            blk.id = bdesub.BWFCreateReadingBlock(-1, clerk, blk);
                            int blockSize = b == nblocks - 1 ? subsc.Length - (nblocks - 1) * 200 : 200;
                            for (int z = 0; z < blockSize; z++)
                            {
                                BWFMeterReading mr = new BWFMeterReading();
                                mr.readingBlockID = blk.id;
                                mr.subscriptionID = subsc[b * 200 + z].id;
                                mr.bwfStatus = BWFStatus.Unread;
                                bdesub.BWFAddMeterReading(-1, clerk, mr, false);
                            }
                        }
                    }
                    i++;
                }

            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    Console.WriteLine(ex.Message);
                    ex = ex.InnerException;
                }
            }
            finally
            {
                bdesub.WriterHelper.restoreReadDB();
            }

        }
        static int joinEverythingToMainCostCenter(AccountingBDE accounting, int PID, int c, int mcsa)
        {
            int N;
            foreach (Account ac in accounting.GetChildAcccount<Account>(PID, 0, -1, out N))
            {
                //c=joinEverythingToMainCostCenter(accounting, ac.id, c,mcsa);
                Console.CursorTop--;
                Console.WriteLine(c + "                                  ");
                try
                {
                    accounting.CreateCostCenterAccount(-1, mcsa, ac.id);
                    c--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error creating account for account id" + ac + "\n" + ex.Message);
                }
            }
            return c;
        }
        [MaintenanceJobMethod("Generate Bills For Skipped customers")]
        public static void generateBillsFoSkipped()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Generating bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                Subscription[] ss = bdesub.WriterHelper.GetSTRArrayByFilter<Subscription>(string.Format("subscriptionStatus=2 and timeBinding=1 and id not in (Select connectionID from {0}.dbo.CustomerBill where periodID=2312)", bdesub.DBName));
                Console.WriteLine(ss.Length + " bills found.Press enter to continue");
                Console.ReadLine();
                Console.WriteLine();
                int c = ss.Length;
                List<int> paid = new List<int>();
                BillPeriod prd = bdesub.GetBillPeriod(2309);
                foreach (Subscription s in ss)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        CustomerBillRecord[] rec;
                        CustomerBillDocument[] doc;
                        BillItem[][] item;
                        string msg;
                        //Cus

                        bool ret = bdesub.generateMonthlyBill(DateTime.Now, prd, bdesub.GetSubscriber(s.subscriberID), out doc, out rec, out item, out msg);
                        if (!ret)
                            throw new Exception(msg);
                        for (int i = 0; i < rec.Length; i++)
                        {
                            int id = bdesub.addBill(-1, doc[i], rec[i], item[i]);
                            bdesub.postBill(-1, DateTime.Now, id);
                        }
                        bdesub.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.WriteLine();
                    }
                    c--;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Delete Unpaid Bills with Deposits")]
        public static void deleteUnpaidBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Deleting unpaid bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("periodID=2311 and paymentDocumentID=-1 and paymentDiffered=0 and billDocumentTypeID in (4,5) and id in (Select customerBillID from Subscriber.dbo.CustomerBillItem where settledFromDepositAmount>0)");
                Console.WriteLine(bills.Length + " unpaid bills found");
                Console.WriteLine();
                int c = 0;


                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        bdesub.deleteCustomerBill(-1, bill.id);
                        c++;

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Delete Unpaid Penality Bills")]
        public static void deletePenalityBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Deleting unpaid penality bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                int periodID;
                do
                {
                    Console.Write("Enter period ID:");
                    if (!int.TryParse(Console.ReadLine(), out periodID))
                    {
                        Console.WriteLine("Invalid period ID");
                        continue;
                    }
                    var p = bdesub.GetBillPeriod(periodID);
                    if (p == null)
                    {
                        Console.WriteLine("Invalid period ID");
                        continue;
                    }
                    Console.WriteLine(p.name);
                    break;
                } while (true);

                CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>($"periodID={periodID} and paymentDocumentID=-1 and paymentDiffered=0 and billDocumentTypeID=4");
                Console.WriteLine(bills.Length + " unpaid penality found");
                Console.WriteLine("Close the window if you don't want to continue");
                Console.ReadLine();
                Console.WriteLine();
                int c = 0;


                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    bdesub.WriterHelper.BeginTransaction();
                    try
                    {
                        bdesub.deleteCustomerBill(-1, bill.id);
                        c++;
                        bdesub.WriterHelper.CommitTransaction();

                    }
                    catch (Exception ex)
                    {
                        bdesub.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Delete Repeat Bills")]
        public static void delteRepeateills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                string sql = @"Select customerID,billDocumentTypeID, count(*) from {0}.dbo.CustomerBill where periodID=2309
group by customerID,billDocumentTypeID having count(*)>1";

                Console.WriteLine("Deleting bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                int[] custIDs = bde.WriterHelper.GetColumnArray<int>(string.Format(sql, bdesub.DBName));
                Console.WriteLine(custIDs.Length + " bills found");
                Console.WriteLine();
                int c = custIDs.Length;
                List<int> paid = new List<int>();

                foreach (int custID in custIDs)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        CustomerBillDocument[] docs = bdesub.getBillDocuments(-1, custID, -1, 2309, false);
                        Console.WriteLine(docs.Length + " Bills found");
                        Console.WriteLine();
                        if (docs.Length != 2)
                            continue;
                        CustomerBillRecord rec = bdesub.getCustomerBillRecord(docs[0].AccountDocumentID);
                        if (rec.paymentDocumentID != -1)
                        {
                            Console.WriteLine("Skipping paid bill. Customer ID:" + rec.customerID);
                            Console.WriteLine();
                            continue;
                        }
                        bdesub.deleteCustomerBill(-1, rec.id);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Clear all bills(Including Paid ones)")]
        public static void deleteAllBills()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Deleting bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);

                CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>(null);

                accounting.WriterHelper.setReadDB(accounting.DBName);

                Console.WriteLine(bills.Length + " bills found");
                Console.WriteLine();
                int c = bills.Length;
                List<int> paid = new List<int>();


                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {

                        if (bill.paymentDocumentID != -1) //paid bill
                        {
                            DocumentSerial[] documentSerials = accounting.WriterHelper.GetSTRArrayByFilter<DocumentSerial>("documentID=" + bill.paymentDocumentID);
                            foreach (DocumentSerial s in documentSerials)
                            {
                                if (s.primaryDocument)
                                {
                                    DeleteDocumentsByTypedReference(accounting, new DocumentTypedReference(s.typeID, s.reference, true));
                                }
                                else
                                {
                                    accounting.DeleteGenericDocument(-1, bill.paymentDocumentID);
                                    string deleteSql = "Delete from {0}.dbo.DocumentSerial where documentID=" + bill.paymentDocumentID;
                                    bde.WriterHelper.ExecuteNonQuery(string.Format(deleteSql, bde.DBName));
                                }
                            }
                        }
                        // bdesub.deleteCustomerBill(-1, bill.id);
                        if (accounting.GetAccountDocument(bill.id, false) != null)
                        {
                            accounting.DeleteGenericDocument(-1, bill.id);
                            bdesub.deleteCustomerBillRecord(-1, bill.id);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
                // accounting.WriterHelper.CommitTransaction();
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Billing: Delete monthly bill by bill period and bill type")]
        public static void deleteBillsByBillTypeAndPeriod()
        {
            try
            {

                Console.WriteLine("Deleting bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                int billTypeID = -1;
                while (true)
                {
                    Console.Write("Enter bill type: ");
                    if (int.TryParse(Console.ReadLine(), out billTypeID))
                    {
                        DocumentType t;
                        if ((t = accounting.GetDocumentTypeByID(billTypeID)) != null)
                        {
                            Console.WriteLine("Bill Type:" + t);
                            break;
                        }
                    }
                }
                int periodID = -1;
                while (true)
                {
                    Console.Write("Enter periodID: ");
                    if (int.TryParse(Console.ReadLine(), out periodID))
                    {
                        BillPeriod p;
                        if ((p = bdesub.GetBillPeriod(periodID)) != null)
                        {
                            Console.WriteLine("Period:" + p);
                            break;
                        }
                    }
                }
                int kebeleID=-1;
                while (true)
                {
                    Console.Write("Enter kebele ID (-1):");
                    string line = Console.ReadLine();
                    if (string.IsNullOrEmpty(line))
                    {
                        break;
                    }
                    if (int.TryParse(line, out kebeleID))
                    {
                        Kebele keb;
                        if ((keb = bdesub.GetKebele(kebeleID)) != null)
                        {
                            Console.WriteLine("Kebele:" + keb.name);
                            break;
                        }
                    }

                }

                Console.Write("Do you want to continue (y/n)?: ");
                if (!Console.ReadLine().ToLower().Equals("y"))
                    return;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);

                string cr;
                if (kebeleID == -1)
                    cr = "periodID={0} and billDocumentTypeID={1}".format(periodID, billTypeID);
                else
                    cr = @"periodID={0} and billDocumentTypeID={1} and connectionID in (SELECT [id]
 
  FROM [Subscriber].[dbo].[Subscription]
where kebele={2} and ticksTo=-1)".format(periodID, billTypeID,kebeleID);
                CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>(cr);

                accounting.WriterHelper.setReadDB(accounting.DBName);

                Console.WriteLine(bills.Length + " bills found. Press enter to continue.");
                Console.ReadLine();
                int c = bills.Length;
                List<int> paid = new List<int>();

                int del = 0;
                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                      ",c,del);
                    bde.WriterHelper.BeginTransaction();
                    try
                    {
                        int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Maintenance Job: Delete monthly bill by bill period and bill type", "", "", -1);
                        if (bill.paymentDocumentID == -1 && !bill.paymentDiffered)
                        {
                            bdesub.deleteCustomerBill(AID, bill.id);
                            del++;
                        }
                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    finally
                    {
                        bde.WriterHelper.CheckTransactionIntegrityWithoutReadDB();
                    }
                    c--;
                }
            }
            catch(Exception ex)
            {
                while(ex!=null)
                {
                    Console.WriteLine("{0}\n{1}",ex.Message,ex.StackTrace);
                    ex=ex.InnerException;
                }
            }
           
        }
        [MaintenanceJobMethod("Delete Customer Payment Receipt")]
        public static void deleteReceipt()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Deleting bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                accounting.WriterHelper.setReadDB(accounting.DBName);
                string sql = "documentID IN (Select s.documentID from {0}.dbo.Document d inner join {0}.dbo.DocumentSerial s on d.id=s.documentID where d.DocumentTypeID=171 and reference='000029462')";

                DocumentSerial[] documentSerials = accounting.WriterHelper.GetSTRArrayByFilter<DocumentSerial>(string.Format(sql, accounting.DBName));

                Console.WriteLine(documentSerials.Length + " receipt document found");
                Console.WriteLine();
                int c = documentSerials.Length;

                foreach (DocumentSerial s in documentSerials)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {

                        if (s.primaryDocument)
                        {
                            DeleteDocumentsByTypedReference(accounting, new DocumentTypedReference(s.typeID, s.reference, true));
                        }
                        else
                        {
                            accounting.DeleteGenericDocument(-1, s.documentID);
                            string deleteSql = "Delete from {0}.dbo.DocumentSerial where documentID=" + s.documentID;
                            bde.WriterHelper.ExecuteNonQuery(string.Format(deleteSql, bde.DBName));
                        }

                        //unpay customer bill
                        string updatebill = "UPDATE [Subscriber].[dbo].[CustomerBill] set paymentDocumentID=-1 where paymentDocumentID=" + s.documentID;
                        bde.WriterHelper.ExecuteNonQuery(string.Format(updatebill, bde.DBName));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
                // accounting.WriterHelper.CommitTransaction();
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Delete account documents not shown on Bills")]
        public static void DeleteDocumentsNotShownOnBills()
        {
            MessageRepository.dontDoDiff = true;

            Console.WriteLine("Deleting account documents");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            try
            {
                accounting.WriterHelper.BeginTransaction();
                accounting.WriterHelper.setReadDB(accounting.DBName);

                AccountDocument[] acc = accounting.WriterHelper.GetSTRArrayByFilter<AccountDocument>(string.Format("DocumentTypeID=5 and ID not in (Select id from Subscriber.dbo.CustomerBill)", accounting.DBName));

                Console.WriteLine(acc.Length + " bills found");
                Console.WriteLine();
                int c = acc.Length;
                List<int> paid = new List<int>();
                foreach (AccountDocument ac in acc)
                {
                    //Generate Bill
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    accounting.DeleteAccountDocument(-1, ac.AccountDocumentID, false);

                    c--;
                }
                accounting.WriterHelper.CommitTransaction();

            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
                Console.WriteLine();
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Adama - waxa 2010 half average calculation","wsisspatni")]
        public static void admafaFixWaxa2010()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Fixing waxa 2010");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                INTAPS.RDBMS.SQLHelper reader = bdesub.GetReaderHelper();
                int[] billIDS;
                try
                {
                    billIDS = reader.GetColumnArray<int>(@"Select id from CustomerBill where connectionID in 
                    (Select subscriptionID from BWFMeterReading where periodID=167 and
                    readingRemark='Half six month average-correction 1')
                    and periodID=167 and billDocumentTypeID=5 and paymentDocumentID=-1 and paymentDiffered=0");
                }
                finally
                {
                    bdesub.ReleaseHelper(reader);
                }
                Console.WriteLine(billIDS.Length+ " bills found");
                Console.WriteLine();
                int c = billIDS.Length;
                bdesub.WriterHelper.BeginTransaction();
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Adama - waxa 2010 half average calculation", "", -1);
                    IDocumentServerHandler handler = accounting.GetDocumentHandler(5);
                    foreach (int billID in billIDS)
                    {
                        Console.CursorTop--;
                        Console.WriteLine("{0} left to process               ", c--);
                        
                        WaterBillDocument bill = accounting.GetAccountDocument<WaterBillDocument>(billID);
                        Console.WriteLine("Before sid:{0} and periodID:{1}", bill.reading.subscriptionID, bill.reading.periodID);
                        Console.WriteLine(handler.GetHTML(bill));
                        bill.reading = bdesub.BWFGetMeterReadingByPeriod(bill.reading.subscriptionID, bill.reading.periodID);
                        foreach(BillItem item in bill.waterBillItems)
                        {
                            if(item.itemTypeID==0)
                            {
                                String readingDesc = "Dubisaa: " + Math.Round(bill.reading.reading, 0) + " Fayyadama: " + Math.Round(bill.reading.consumption, 0) + " (Average)";
                                item.description =
                                "Lakk. Waliigaltee: " + bill.subscription.contractNo
                                + "\nJi'a: " + bill.period.name
                                + "\n" + readingDesc
                                ;
                            }
                        }
                        Console.WriteLine("After");
                        Console.WriteLine(handler.GetHTML(bill));
                        //Console.ReadLine();
                        accounting.UpdateAccountDocumentData(AID, bill);
                    }
                    bdesub.WriterHelper.CommitTransaction();
                    
                }
                catch
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    throw;
                }

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Adama - waxa 2010 spill over problem", "wsisspatni")]
        public static void admafaFixWaxa2010_2()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Fixing waxa 2010");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                INTAPS.RDBMS.SQLHelper reader = bdesub.GetReaderHelper();
                int[] conIDs;
                try
                {
                    conIDs= reader.GetColumnArray<int>(@"Select id from Subscription where ticksTo=-1 and id in(Select subscriptionID from adol2010) and id<>196308");
                }
                finally
                {
                    bdesub.ReleaseHelper(reader);
                }
                Console.WriteLine("Checking "+ conIDs.Length + " connections");
                Console.WriteLine();
                int c = conIDs.Length;
                int nProblem= 0;
                bdesub.WriterHelper.BeginTransaction();
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "MaintenanceJob: Adama - waxa 2010 spill over problem", "", -1);
                    IDocumentServerHandler handler = accounting.GetDocumentHandler(5);
                    int[] periods = new int[] { 166, 167, 168, 169 };
                    foreach (int conID in conIDs)
                    {
                        Console.CursorTop--;
                        Console.WriteLine("{0} left to process {1} problems              ", c--,nProblem);

                        List<BWFMeterReading> readings = new List<BWFMeterReading>();
                        BWFMeterReading prevReading = null;
                        foreach (int p in periods)
                        {
                            BWFMeterReading thisReading = bdesub.BWFGetMeterReadingByPeriod(conID, p);
                            if (thisReading == null || thisReading.bwfStatus!=BWFStatus.Read)
                                thisReading = new BWFMeterReading() { reading = prevReading == null ? 0 : prevReading.reading, consumption = 0, readingType = MeterReadingType.Normal };
                            readings.Add(thisReading);
                            prevReading = thisReading;
                        }
                        prevReading = readings[0];
                        bool hasError = false;
                        for (int i = 1; i < readings.Count; i++)
                        {
                            BWFMeterReading thisReading = readings[i];
                            switch (thisReading.readingType)
                            {
                                case MeterReadingType.Normal:
                                case MeterReadingType.Average:
                                    if(!AccountBase.AmountEqual(thisReading.reading-prevReading.reading,thisReading.consumption) && (thisReading.consumption>0))
                                    {
                                        Console.WriteLine(" subscription {0} period {1}- {2} {3}=>{4}", conID, periods[i],prevReading.reading,thisReading.reading,thisReading.consumption);
                                        thisReading.reading = prevReading.reading + thisReading.consumption;
                                        bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, thisReading, new string[] { "__AID" }, new object[] { AID });
                                        hasError = true;
                                    }
                                    break;
                                case MeterReadingType.MeterReset:
                                    break;
                                default:
                                    break;
                            }
                            prevReading = thisReading;
                        }
                        if (hasError)
                        {
                            Console.WriteLine();
                            nProblem++;
                        }


                        
                    }
                    bdesub.WriterHelper.CommitTransaction();

                }
                catch
                {
                    bdesub.WriterHelper.RollBackTransaction();
                    throw;
                }

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Generate Unsold Bills For customers of period Hamle 2006")]
        public static void generateBillsForUnSold()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Generating bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                accounting.WriterHelper.setReadDB(accounting.DBName);
                int typeID = bdesub.Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
                int creditBillTypeID = bdesub.Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id;
                BillPeriod prd = bdesub.GetBillPeriod(2310);
                BillPeriod prdNext = bdesub.GetBillPeriod(2311);
                Account[] acc = accounting.WriterHelper.GetSTRArrayByFilter<Account>(string.Format("code like '13200-%' and CreditAccount=1", accounting.DBName));
                bdesub.WriterHelper.setReadDB(bdesub.DBName);

                Console.WriteLine(acc.Length + " subscribers found");
                Console.WriteLine();
                int c = acc.Length;
                List<int> paid = new List<int>();
                foreach (Account ac in acc)
                {
                    string customerCode = (string)bdesub.WriterHelper.ExecuteScalar(string.Format("Select customerCode from {0}.dbo.Subscriber where accountID=" + ac.id, bdesub.DBName));

                    Subscriber subs = bdesub.GetSubscriber(customerCode);
                    //Get subscriber bill record for bill period hamle 2006
                    CustomerBillRecord[] billrec = bdesub.getBills(typeID, subs.id, -1, prd.id);
                    foreach (CustomerBillRecord rec in billrec)
                    {
                        try
                        {
                            bdesub.WriterHelper.BeginTransaction();
                            if (rec.paymentDocumentID == -1) // not sold
                            {
                                //Generate Bill
                                Console.CursorTop--;
                                Console.WriteLine(c + "                      ");

                                CustomerBillRecord[] newRecords;
                                CustomerBillDocument[] doc;
                                BillItem[][] item;
                                string msg;

                                Account newAccount = ac;
                                newAccount.CreditAccount = false;
                                accounting.UpdateAccount<Account>(-1, newAccount); //Update Account
                                bdesub.deleteCustomerBill(-1, rec.id); //delete old customer bill data
                                bool ret = bdesub.generateMonthlyBill(DateTime.Now, prd, subs, out doc, out newRecords, out item, out msg); //Generate
                                if (!ret)
                                    throw new Exception(msg);
                                for (int i = 0; i < newRecords.Length; i++)
                                {
                                    doc[i].draft = false;
                                    newRecords[i].draft = false;
                                    int id = bdesub.addBill(-1, doc[i], newRecords[i], item[i]);
                                    //bdesub.postBill(-1, DateTime.Now, id);
                                }

                                c--;

                            }
                            else //Paid bill
                            {
                                Console.CursorTop--;
                                Console.WriteLine(c + "                      ");

                                CustomerBillRecord[] newRecords;
                                CustomerBillDocument[] doc;
                                BillItem[][] item;
                                string msg;
                                Account newAccount = ac;
                                newAccount.CreditAccount = false;
                                accounting.UpdateAccount<Account>(-1, newAccount); //Update Account

                                int csaID = accounting.GetCostCenterAccountID(bde.SysPars.mainCostCenterID, ac.id);
                                double bal = accounting.GetNetBalanceAsOf(csaID, 0, DateTime.Now);
                                if (Account.AmountGreater(bal, 0))
                                {
                                    CreditScheme scheme = new CreditScheme();
                                    scheme.customerID = subs.id;
                                    scheme.paymentAmount = bal;
                                    scheme.creditedAmount = bal;
                                    scheme.startPeriodID = prdNext.id;
                                    scheme.issueDate = DateTime.Now;
                                    bdesub.differePayment(-1, new int[] { }, scheme); //register credit scheme

                                    //Generate credit bills
                                    bool ret = bdesub.generateMonthlyBill(DateTime.Now, prdNext, subs, out doc, out newRecords, out item, out msg); //Generate
                                    if (!ret)
                                        throw new Exception(msg);
                                    for (int i = 0; i < newRecords.Length; i++)
                                    {
                                        if (doc[i].GetType() == typeof(CreditBillDocument))
                                        {
                                            doc[i].draft = false;
                                            newRecords[i].draft = false;
                                            doc[i].ShortDescription = "Outstanding Bill Carried forward from " + prd.name;
                                            int id = bdesub.addBill(-1, doc[i], newRecords[i], item[i]);
                                        }
                                    }

                                }
                                c--;
                            }
                            bdesub.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bdesub.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.WriteLine();
                        }

                    }
                }

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Fix Account Transaction-Special 269732")]
        public static void FixTransactionSquenceSpecial()
        {
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;
            FixTransactionSquence(accounting, 269732, 0);

        }
        [MaintenanceJobMethod("Fix Account Transaction-By Prompt")]
        public static void FixTransactionSquencePromot()
        {
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;
            int accountID;
            string strID;
            do
            {
                Console.WriteLine("Please enter accountID");
                strID = Console.ReadLine();

            }
            while (!int.TryParse(strID, out accountID));
            FixTransactionSquence(accounting, accountID, 0);
        }

        [MaintenanceJobMethod("Accounting: Rebuild Leaves Tables")]
        public static void accountingRebuildLeaves()
        {
            Console.WriteLine("Rebuild leaves table?");
            if (!Console.ReadLine().Equals("Y"))
                return;
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;
            accounting.WriterHelper.BeginTransaction();
            try
            {
                accounting.rebuildCostCenterLeafAccountTable(-1);
                accounting.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }
        }
        [MaintenanceJobMethod("Accounting: Fix Account Transaction Sequence for all accounts")]
        public static void fixTransactionSquence()
        {
            bool fix = true;
            Console.WriteLine(fix ? "Check and fixing tarnsaction sequence errors" : "Checking transaction sequence errors");
            iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            AccountingBDE accounting = bdeBERP.Accounting;

            DataTable dt = accounting.WriterHelper.GetDataTable("Select distinct accountID,itemID from " + accounting.DBName + ".dbo.AccountTransaction");
            int c = dt.Rows.Count;
            Console.WriteLine();
            foreach (DataRow row in dt.Rows)
            {
                int accountID = (int)row[0];
                int itemID = (int)row[1];
                CostCenterAccountWithDescription leafAccount = accounting.GetCostCenterAccountWithDescription(accountID);

                //Console.WriteLine(leafAccount.CodeName + " itemID " + itemID + "                    ");
                Console.CursorTop--;
                Console.WriteLine(c-- + "                             ");
                if (fix)
                {
                    FixTransactionSquence(accounting, accountID, itemID);
                }
                else
                {
                    INTAPS.RDBMS.SQLHelper readerHelper = accounting.GetReaderHelper();
                    try
                    {
                        string str;
                        if (accounting.VerifyTransactionConsistency(readerHelper, leafAccount.id, 0, out str) != -1)
                        {
                            Console.WriteLine("Error " + leafAccount.CodeName + "\nError:" + str);
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Error verifying " + leafAccount.CodeName + "\nError:" + exception.Message);
                    }
                    finally
                    {
                        accounting.ReleaseHelper(readerHelper);
                    }
                }
            }
        }

        [MaintenanceJobMethod("Fix Employee Accounts (Dire Dawa)")]
        public static void FixEmployeeAccounts()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Fixing Employee Accounts");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
                bdePayroll.WriterHelper.setReadDB(bdePayroll.DBName);
                Employee[] employees = bdePayroll.WriterHelper.GetSTRArrayByFilter<Employee>(null);
                bde.WriterHelper.setReadDB(bde.DBName);
                Console.WriteLine(employees.Length + " employees found");
                Console.WriteLine();
                int c = employees.Length;
                int hardShipAllowanceCostID = accounting.GetAccount<Account>("5005").id;
                int overTimeCostID = accounting.GetAccount<Account>("5004").id;
                int housingAllowanceCost = accounting.GetAccount<Account>("5006").id;
                int milkAllowanceCostID = accounting.GetAccount<Account>("5007").id;
                int transportAllowanceCostID = accounting.GetAccount<Account>("5008").id;
                int topupAllowanceCostID = accounting.GetAccount<Account>("5010").id;
                int indemnityAllwoanceCostID = accounting.GetAccount<Account>("5011").id;

                int hardShipAllowanceExpenseID = accounting.GetAccount<Account>("6005").id;
                int overTimeExpenseID = accounting.GetAccount<Account>("6004").id;
                int housingAllowanceExpenseID = accounting.GetAccount<Account>("6006").id;
                int milkAllowanceExpenseID = accounting.GetAccount<Account>("6007").id;
                int transportAllowanceExpenseID = accounting.GetAccount<Account>("6008").id;
                int topupAllowanceExpenseID = accounting.GetAccount<Account>("6010").id;
                int indemnityAllowanceExpenseID = accounting.GetAccount<Account>("6011").id;

                int courtOrderID = accounting.GetAccount<Account>("2005").id;
                int edirID = accounting.GetAccount<Account>("2006").id;
                int melesID = accounting.GetAccount<Account>("2007").id;
                int microID = accounting.GetAccount<Account>("2008").id;
                int damID = accounting.GetAccount<Account>("2009").id;

                int[] otherAccountsCost = new int[]{hardShipAllowanceCostID,overTimeCostID,housingAllowanceCost,topupAllowanceCostID,indemnityAllwoanceCostID,milkAllowanceCostID,transportAllowanceCostID
                    ,courtOrderID,edirID,melesID,microID,damID};
                int[] otherAccountsExpense = new int[]{hardShipAllowanceExpenseID,overTimeExpenseID,housingAllowanceExpenseID,topupAllowanceExpenseID,milkAllowanceExpenseID, indemnityAllowanceExpenseID,transportAllowanceExpenseID
                    ,courtOrderID,edirID,melesID,microID,damID};
                try
                {

                    bde.WriterHelper.BeginTransaction();
                    foreach (Employee emp in employees)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c + "                      ");
                        if (emp.status == EmployeeStatus.Enrolled)
                        {
                            bde.RegisterEmployee(-1, emp, null, false, emp.accountAsCost ? otherAccountsCost : otherAccountsExpense, -1, null, null, null, null);
                        }
                        c--;
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
    //    [MaintenanceJobMethod("Convert long term loans to support Interest (Dire Dawa)")]
    //    public static void UpdateLongTermLoanWithInterest()
    //    {
    //        MessageRepository.dontDoDiff = true;
    //        try
    //        {
    //            int N;
    //            Console.WriteLine("Updating long term loans");
    //            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
    //            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
    //            PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
    //            //bde.WriterHelper.setReadDB(bde.DBName);
    //            //bde.GetAccountingPeriod(
    //            accounting.WriterHelper.setReadDB(accounting.DBName);

    //            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(LongTermLoanDocument)).id,
    //false, DateTime.Now, DateTime.Now, 0, -1, out N);


    //            Console.WriteLine(docs.Length + " long term loans found");
    //            Console.WriteLine();
    //            int c = docs.Length;
    //            try
    //            {
    //                bde.WriterHelper.BeginTransaction();
    //                foreach (AccountDocument doc in docs)
    //                {
    //                    Console.CursorTop--;
    //                    Console.WriteLine(c + "                      ");

    //                    LongTermLoanDocument longTerm = (LongTermLoanDocument)doc;
    //                    LongTermLoanWithServiceChargeDocument loan = new LongTermLoanWithServiceChargeDocument();
    //                    loan.AccountDocumentID = longTerm.AccountDocumentID;
    //                    loan.employeeID = longTerm.employeeID;
    //                    loan.DocumentDate = longTerm.DocumentDate;
    //                    loan.PaperRef = longTerm.PaperRef;
    //                    DocumentTypedReference[] reference = accounting.getAllDocumentReferences(doc.AccountDocumentID);
    //                    loan.voucher = reference[0];
    //                    loan.paymentMethod = longTerm.paymentMethod;
    //                    loan.assetAccountID = longTerm.assetAccountID;
    //                    loan.amount = longTerm.amount;
    //                    loan.periodID = longTerm.periodID;
    //                    loan.monthlyReturn = longTerm.monthlyReturn;
    //                    ((StaffLoandDocumentBase)loan).returnExceptions = ((StaffLoandDocumentBase)longTerm).returnExceptions;
    //                    loan.ShortDescription = longTerm.ShortDescription;
    //                    SetReferenceNumber(true, longTerm, loan);

    //                    accounting.PostGenericDocument(-1, loan);
    //                    c--;
    //                }
    //                bde.WriterHelper.CommitTransaction();
    //            }
    //            catch (Exception ex)
    //            {
    //                bde.WriterHelper.RollBackTransaction();
    //                Console.WriteLine(ex.Message);
    //                Console.ReadLine();
    //            }
    //            c--;
    //        }
    //        finally
    //        {
    //            MessageRepository.dontDoDiff = false;
    //        }
    //    }
    //    [MaintenanceJobMethod("Update Long Term Loan With no References (Dire Dawa)")]
    //    public static void UpdateLongTermLoanWithInterestReferences()
    //    {
    //        MessageRepository.dontDoDiff = true;
    //        try
    //        {
    //            int N;
    //            Console.WriteLine("Updating long term loans with no references");
    //            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
    //            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
    //            PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
    //            //bde.WriterHelper.setReadDB(bde.DBName);
    //            //bde.GetAccountingPeriod(
    //            accounting.WriterHelper.setReadDB(accounting.DBName);

    //            AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(LongTermLoanWithServiceChargeDocument)).id,
    //false, DateTime.Now, DateTime.Now, 0, -1, out N);


    //            Console.WriteLine(docs.Length + " long term loans found");
    //            Console.WriteLine();
    //            int c = docs.Length;
    //            try
    //            {
    //                bde.WriterHelper.BeginTransaction();
    //                foreach (AccountDocument doc in docs)
    //                {
    //                    Console.CursorTop--;
    //                    Console.WriteLine(c + "                      ");

    //                    LongTermLoanWithServiceChargeDocument longTerm = (LongTermLoanWithServiceChargeDocument)doc;

    //                    DocumentTypedReference[] reference = accounting.getAllDocumentReferences(doc.AccountDocumentID);
    //                    DocumentTypedReference refer = null;
    //                    int paperRef;
    //                    if (reference.Length == 0)
    //                    {
    //                        if (int.TryParse(longTerm.PaperRef, out paperRef))
    //                        {
    //                            if (paperRef <= 10)
    //                                refer = new DocumentTypedReference(24, longTerm.PaperRef, true); //JV
    //                            else
    //                                refer = new DocumentTypedReference(21, longTerm.PaperRef, true); //CHPV
    //                        }
    //                        else
    //                        {
    //                            Console.WriteLine("Invalid reference " + longTerm.PaperRef);
    //                            Console.ReadKey();
    //                        }
    //                    }
    //                    else
    //                        continue;

    //                    longTerm.voucher = refer;
    //                    accounting.PostGenericDocument(-1, longTerm);
    //                    c--;
    //                }
    //                bde.WriterHelper.CommitTransaction();
    //            }
    //            catch (Exception ex)
    //            {
    //                bde.WriterHelper.RollBackTransaction();
    //                Console.WriteLine(ex.Message);
    //                Console.ReadLine();
    //            }
    //            c--;
    //        }
    //        finally
    //        {
    //            MessageRepository.dontDoDiff = false;
    //        }
    //    }

        [MaintenanceJobMethod("Update Short Term Loan With no References (Dire Dawa)")]
        public static void UpdateShortTermLoanReferences()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                int N;
                Console.WriteLine("Updating short term loans with no references");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
                //bde.WriterHelper.setReadDB(bde.DBName);
                //bde.GetAccountingPeriod(
                accounting.WriterHelper.setReadDB(accounting.DBName);

                AccountDocument[] docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(ShortTermLoanDocument)).id,
    false, DateTime.Now, DateTime.Now, 0, -1, out N);


                Console.WriteLine(docs.Length + " short term loans found");
                Console.WriteLine();
                int c = docs.Length;
                try
                {
                    bde.WriterHelper.BeginTransaction();
                    foreach (AccountDocument doc in docs)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c + "                      ");

                        ShortTermLoanDocument longTerm = (ShortTermLoanDocument)doc;

                        DocumentTypedReference[] reference = accounting.getAllDocumentReferences(doc.AccountDocumentID);
                        DocumentTypedReference refer = null;
                        int paperRef;
                        if (reference.Length == 0)
                        {
                            if (int.TryParse(longTerm.PaperRef, out paperRef))
                            {
                                if (paperRef <= 10)
                                    refer = new DocumentTypedReference(24, longTerm.PaperRef, true); //JV
                                else
                                    refer = new DocumentTypedReference(21, longTerm.PaperRef, true); //CHPV
                            }
                            else
                            {
                                Console.WriteLine("Invalid reference " + longTerm.PaperRef);
                                Console.ReadKey();
                            }
                        }
                        else
                            continue;

                        longTerm.voucher = refer;
                        accounting.PostGenericDocument(-1, longTerm);
                        c--;
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }



        public static void joinEverythingToMainCostCenter()
        {

            Console.WriteLine("Creating cost center accounts");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            lock (accounting.WriterHelper)
            {
                int c = (int)accounting.WriterHelper.ExecuteScalar("Select count(*) from " + accounting.DBName + ".dbo.Account");
                Console.WriteLine(c + " accounts found");
                Console.WriteLine();
                joinEverythingToMainCostCenter(accounting, -1, c, 1);
            }
        }
        public static void fixSuppliersAndCustomers()
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            int N;
            TradeRelation[] tr = bde.searchTradeRelation(null, null, 0, -1, null, null, out N);
            Console.CursorTop--;
            Console.WriteLine(tr.Length + " relations found");
            int c = tr.Length;
            Console.WriteLine();
            foreach (TradeRelation r in tr)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                                               ");
                try
                {
                    bde.RegisterRelation(-1, r);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error processing " + r.Code);
                    Console.WriteLine(ex.Message);

                }
            }
        }
        private static void clearTransaction()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int[] docs = accounting.WriterHelper.GetColumnArray<int>(
                string.Format("Select ID from {0}.dbo.Document", accounting.DBName), 0);
            int c = 1;
            foreach (int docID in docs)
            {
                Console.WriteLine((c++) + "/" + docs.Length);

                try
                {
                    accounting.DeleteAccountDocument(-1, docID, false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error deleting " + docID + ".\n" + ex.Message);
                }
            }
        }

        private static void updateSalesAndPurchasePaidAmountValue()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

            foreach (int docID in accounting.WriterHelper.GetColumnArray<int>(string.Format("Select id from {0}.dbo.Document where documentTypeID in ({1})", accounting.DBName, "142,146"), 0))
            {
                AccountDocument doc = accounting.GetAccountDocument(docID, true);
                AccountTransaction[] tran = accounting.GetTransactionsOfDocument(docID);

                if (doc is Sell2Document)
                {
                    Sell2Document sell = doc as Sell2Document;
                    if (!Account.AmountEqual(sell.paidAmount, 0))
                        continue;
                    bool done = false;
                    foreach (AccountTransaction t in tran)
                    {
                        if (t.AccountID == sell.assetAccountID)
                        {
                            done = true;
                            sell.paidAmount = t.Amount;
                            sell.doWithheldVAT = true;
                            accounting.UpdateAccountDocumentData(-1, sell);
                            continue;
                        }
                    }
                    if (!done)
                        Console.WriteLine("Sells :" + sell.PaperRef + " has no payment");
                }
                else
                {
                    Purchase2Document purchase = doc as Purchase2Document;
                    if (!Account.AmountEqual(purchase.paidAmount, 0))
                        continue;

                    bool done = false;
                    foreach (AccountTransaction t in tran)
                    {
                        if (t.AccountID == purchase.assetAccountID)
                        {
                            done = true;
                            purchase.paidAmount = -t.Amount;
                            accounting.UpdateAccountDocumentData(-1, purchase);
                            continue;
                        }
                    }
                    if (!done)
                        Console.WriteLine("Purchase :" + purchase.PaperRef + " has no payment");
                }
            }
        }

        private static void fixCostCenterAccountDataError()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            string sql;

            Console.WriteLine("Synching child count fields for CostCenterAccount");
            sql = @"Update {0}.dbo.CostCenterAccount set childAccountCount=(Select childCount from {0}.dbo.Account where id=CostCenterAccount.accountID)
                        ,childCostCenterCount=(Select childCount from {0}.dbo.CostCenter where id=CostCenterAccount.costCenterID)";
            accounting.WriterHelper.ExecuteNonQuery(string.Format(sql, accounting.DBName));

            accounting.WriterHelper.setReadDB(accounting.DBName);
            accounting.WriterHelper.BeginTransaction();
            try
            {
                sql = "Delete from {0}.dbo.CostCenterLeafAccount";
                accounting.WriterHelper.ExecuteNonQuery(string.Format(sql, accounting.DBName));
                CostCenterAccount[] allAccounts = accounting.WriterHelper.GetSTRArrayByFilter<CostCenterAccount>("childCostCenterCount=0 and childAccountCount=0");
                foreach (CostCenterAccount a in allAccounts)
                {
                    int[] ac = accounting.ExpandParents<Account>(accounting.WriterHelper, a.accountID);
                    int[] cs = accounting.ExpandParents<CostCenter>(accounting.WriterHelper, a.costCenterID);
                    if (ac.Length == 1 && cs.Length == 1)
                        throw new Exception();
                    foreach (int acID in ac)
                    {
                        foreach (int csID in cs)
                        {
                            if (acID == a.accountID && csID == a.costCenterID)
                                continue;
                            accounting.WriterHelper.Insert(accounting.DBName, "CostCenterLeafAccount"
                                , new string[] { "childCostCenterAccountID", "parentCostCenterAccountID", "accountID", "costCenterID" }
                                , new object[] { a.id, accounting.GetCostCenterAccount(csID, acID).id, a.accountID, a.costCenterID });
                        }
                    }
                }
                accounting.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                accounting.WriterHelper.RollBackTransaction();
                Console.WriteLine("Failed to rebuild cost center leaf account table." + ex.Message);
            }
        }
        private static void DeleteDocumentsByTypedReference(AccountingBDE bde, DocumentTypedReference tref)
        {

            string sql = "Select s.documentID from {0}.dbo.Document d inner join {0}.dbo.DocumentSerial s on d.id=s.documentID where typeID={1} and reference='{2}'";
            DataTable table = bde.WriterHelper.GetDataTable(string.Format(sql, bde.DBName, tref.typeID, tref.reference));
            foreach (DataRow row in table.Rows)
            {
                int documentID = (int)row[0];
                bde.DeleteGenericDocument(-1, documentID);
                string deleteSql = "Delete from {0}.dbo.DocumentSerial where documentID=" + documentID;
                bde.WriterHelper.ExecuteNonQuery(string.Format(deleteSql, bde.DBName));
            }
            //  if (bde.GetAccountDocument(paymentDocumentID, false) != null)
            //    bde.DeleteGenericDocument(-1, paymentDocumentID);
        }
        private static void FixTransactionSquence(AccountingBDE finance, int accountID, int itemID)
        {
            INTAPS.RDBMS.SQLHelper helper;
            Monitor.Enter(helper = finance.WriterHelper);
            try
            {
                string str2;
                finance.WriterHelper.BeginTransaction();
                string sql = string.Format("Select TransactionType,TotalDbBefore,TotalCrBefore,Amount,ID,tranTicks from {2}.dbo.AccountTransaction where AccountID={0} and ItemID={1} order by tranTicks", accountID, itemID, finance.DBName);
                DataTable dataTable = finance.WriterHelper.GetDataTable(sql);
                double newDebit = 0.0;
                double newCredit = 0.0;
                int orderN = 1;
                long now = DateTime.Now.Ticks;
                foreach (DataRow row in dataTable.Rows)
                {
                    double amount = (double)row[3];
                    int tranID = (int)row[4];
                    /*if (AccountBase.AmountEqual(amount, 0))
                    {
                        finance.WriterHelper.ExecuteNonQuery(string.Format("Delete from {0}.dbo.AccountTransaction where ID={1}", finance.DBName, tranID));
                        continue;
                    }*/
                    TransactionOfBatch tran = new TransactionOfBatch(accountID, amount, "");
                    tran.ItemID = itemID;
                    tran.ID = tranID;
                    tran.TransactionType = (INTAPS.Accounting.TransactionType)row[0];
                    tran.TotalCrBefore = newCredit;
                    tran.TotalDbBefore = newDebit;
                    finance.WriterHelper.ExecuteNonQuery(string.Format("Update {0}.dbo.AccountTransaction set TotalDbBefore={1},TotalCrBefore={2},OrderN={3} where ID={4}", finance.DBName, tran.TotalDbBefore, tran.TotalCrBefore, orderN, tran.ID));
                    newCredit = tran.NewCredit;
                    newDebit = tran.NewDebit;
                    now = (long)row[5];
                    orderN++;
                }
                if (orderN == 1)
                {
                    finance.WriterHelper.ExecuteNonQuery(string.Concat(new object[] { "DELETE FROM [Accounting_2006].[dbo].[AccountBalance] WHERE accountID=", accountID, " and ItemID=", itemID }));
                }
                else
                {
                    AccountBalance accountBalance = finance.GetEndBalance(accountID, itemID);
                    if (accountBalance.LastOrderN == 0)
                    {
                        accountBalance.LastOrderN = orderN - 1;
                        accountBalance.tranTicks = now;
                        accountBalance.TotalDebit = newDebit;
                        accountBalance.TotalCredit = newCredit;
                        finance.WriterHelper.InsertSingleTableRecord<AccountBalance>(finance.DBName, accountBalance);
                    }
                    else
                    {
                        accountBalance.LastOrderN = orderN - 1;
                        accountBalance.tranTicks = now;
                        accountBalance.TotalDebit = newDebit;
                        accountBalance.TotalCredit = newCredit;
                        finance.WriterHelper.UpdateSingleTableRecord<AccountBalance>(finance.DBName, accountBalance);
                        //finance.WriterHelper.ExecuteNonQuery(string.Concat(new object[] { "UPDATE [Accounting_2006].[dbo].[AccountBalance] SET [LastTransactionDate] ='", now.ToLongTimeString(), "',[LastOrderN] = ", orderN - 1, ",[TotalCredit] = ", newCredit, "\r\n                                    ,[TotalDebit] = ", newDebit, " WHERE accountID=", accountID, " and ItemID=", itemID }));
                    }
                }
                if (finance.VerifyTransactionConsistency(finance.WriterHelper, accountID, itemID, out str2) != -1)
                {
                    throw new Exception(str2);
                }
                finance.WriterHelper.CommitTransaction();
            }
            catch (Exception exception)
            {
                finance.WriterHelper.RollBackTransaction();
                Console.WriteLine(string.Concat(new object[] { "Failed to fix ", accountID, "\n", exception.Message }));
            }
            finally
            {
                Monitor.Exit(helper);
            }
        }

        public static void CreatePeriods(int fiscalYear)
        {
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            bde.GenerateYearAccountingPeriod(fiscalYear);
        }

        static System.IO.StreamWriter logStream = null;

        static void OpenLogStream()
        {
            if (logStream == null)
                logStream = System.IO.File.CreateText("ServerMaintenanceJob.txt");

        }

        static void writeLog(string line)
        {
            Console.WriteLine(line);
            if (logStream != null)
                OpenLogStream();
            logStream.WriteLine(line);
        }

        static void closeLogStream()
        {
            if (logStream != null)
                logStream.Close();
        }

        static void repostIntegrityTest()
        {
            OpenLogStream();
            try
            {
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                bde.WriterHelper.ExecuteNonQuery(string.Format("delete from {0}.dbo.Document_Deleted", bde.Accounting.DBName));
                int[] docIDs = bde.WriterHelper.GetColumnArray<int>(string.Format("Select ID from {0}.dbo.Document where DocumentTypeID not in (1,107,106,152,120)order by DocumentDate", bde.Accounting.DBName), 0);
                Console.WriteLine(docIDs.Length + " found");
                Console.WriteLine();
                for (int i = 0; i < docIDs.Length; i++)
                {
                    Console.CursorTop--;
                    Console.WriteLine((docIDs.Length - i) + "                                ");
                    AccountDocument doc;
                    AccountTransaction[] oldTransaction, newTransaction;
                    try
                    {
                        doc = bde.Accounting.GetAccountDocument(docIDs[i], true);
                        oldTransaction = bde.Accounting.GetTransactionsOfDocument(docIDs[i]);
                        try
                        {
                            int oldDocID = doc.AccountDocumentID;
                            int newDocID = bde.Accounting.PostGenericDocument(-1, doc);
                            newTransaction = bde.Accounting.GetTransactionsOfDocument(newDocID);
                            if (oldDocID != newDocID)
                            {
                                writeLog(string.Format("Old new document ID mismatch new docid:{0} documenType:{1}", newDocID, bde.Accounting.GetDocumentTypeByID(doc.DocumentTypeID).name));
                                Console.WriteLine();
                            }
                            foreach (AccountTransaction oldt in oldTransaction)
                            {

                                double amount = 0;
                                foreach (AccountTransaction oldtt in oldTransaction)
                                {
                                    if (oldt.AccountID == oldtt.AccountID)
                                    {
                                        amount += oldtt.Amount;
                                    }
                                }
                                foreach (AccountTransaction newT in newTransaction)
                                {
                                    if (oldt.AccountID == newT.AccountID)
                                    {
                                        amount -= newT.Amount;
                                    }
                                }
                                if (!Account.AmountEqual(amount, 0))
                                {
                                    writeLog(string.Format("Old new transaction set mismatch new docid:{0} documenType:{1}", newDocID, bde.Accounting.GetDocumentTypeByID(doc.DocumentTypeID).name));
                                    Console.WriteLine();
                                    break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            writeLog("Error reposting document ID:" + docIDs[i]);
                            writeLog(ex.Message);
                            Console.WriteLine();
                        }
                    }
                    catch (Exception ex)
                    {
                        writeLog("Error loading document ID:" + docIDs[i]);
                        writeLog(ex.Message);
                        Console.WriteLine();
                    }


                }
            }
            finally
            {
                closeLogStream();
            }
        }

        //private static void CreateCostCenterLeafAccounts()
        //{
        //    AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;

        //    string sql = "Select id from {0}.dbo.CostCenterAccount where [childAccountCount]=0 and [childCostCenterCount]=0";
        //    int[] csids = accounting.WriterHelper.GetColumnArray<int>(string.Format(sql,accounting.DBName),0);
        //    try
        //    {
        //        int c = csids.Length;
        //        Console.WriteLine();
        //        foreach (int id in csids)
        //        {
        //            Console.CursorTop--;
        //            Console.WriteLine(id + "                    ");
        //            accounting.rebuildLeaveEntries(id);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        Console.ReadLine();
        //    }
        //}


        [MaintenanceJobMethod("Rebuild Diffs")]
        public static void rebuildAllDiffs()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            Console.WriteLine("Clearing messages");
            bdesub.WriterHelper.Delete(ApplicationServer.SecurityBDE.DBName, "OfflineMessage", "");
            bdesub.WriterHelper.Delete(bdesub.DBName, "UpdateLog", "");
            Console.WriteLine("Loading customers list");
            Console.WriteLine();
            Subscriber[] subs = bde.WriterHelper.GetSTRArrayByFilter<Subscriber>("");
            int c = subs.Length;
            int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Rebuild diffs", "", "", -1);
            foreach (Subscriber s in subs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                try
                {
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(s.id, s));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }
            rebuildConnectionDiffs();
            rebuildLookupDiffs();
            rebuildBillDiffs();
        }

        [MaintenanceJobMethod("Rebuild Connection Diffs")]
        public static void rebuildConnectionDiffs()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            Console.WriteLine("Loading connections list");
            Console.WriteLine();
            Subscription[] subs = bde.WriterHelper.GetSTRArrayByFilter<Subscription>("");
            int c = subs.Length;
            int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Rebuild diffs", "", "", -1);
            foreach (Subscription s in subs)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                try
                {
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(s.id, s.ticksFrom, s));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }
        }
        [MaintenanceJobMethod("Rebuild Look Table Diffs. It can corrupt database if run twice")]
        public static void rebuildLookupDiffs()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Rebuild diffs", "", "", -1);
            Console.WriteLine("Transfering bank list");
            Console.WriteLine();
            foreach (BankInfo bankInfo in bde.getAllBanks())
            {
                try
                {
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(bankInfo.id, bankInfo));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            Console.WriteLine("Transfering bank branch list");
            foreach (BankInfo bankInfo in bde.getAllBanks())
            {
                foreach (BankBranchInfo branchInfo in bde.getAllBranchsOfBank(bankInfo.id))
                {
                    try
                    {
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(branchInfo.id, branchInfo));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                }
            }
            Console.WriteLine("Transfering payment instrument types");
            foreach (PaymentInstrumentType inst in bde.getAllPaymentInstrumentTypes())
            {
                try
                {
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(inst.itemCode, inst));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }

        }
        [MaintenanceJobMethod("Rebuild bill diffs")]
        public static void rebuildBillDiffs()
        {
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            Console.WriteLine("Loading bills list");
            Console.WriteLine();
            CustomerBillRecord[] records = bde.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("paymentDocumentID=-1 and paymentDiffered=0");
            int c = records.Length;
            int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Rebuild diffs", "", "", -1);
            foreach (CustomerBillRecord record in records)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                bde.WriterHelper.BeginTransaction();
                try
                {
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(record.id, record));
                    CustomerBillDocument doc = accounting.GetAccountDocument(record.id, true) as CustomerBillDocument;
                    if (doc == null)
                        throw new Exception("Bill record without corresponding account document found");
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(doc.AccountDocumentID, doc));
                    foreach (BillItem item in bdesub.getCustomerBillItems(record.id))
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(item.customerBillID, item.itemTypeID, item));
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }
        }
        [MaintenanceJobMethod("Randomly Generate Coordinates")]
        public static void randomGIS()
        {

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            double lat2 = 8.769935, lng1 = 38.945284;
            double lat1 = 8.733311, lng2 = 39.005643;
            List<double[]> rnd1 = new List<double[]>();
            Random r = new Random();
            Subscription[] s = bdesub.WriterHelper.GetSTRArrayByFilter<Subscription>(null);
            for (int i = 0; i < 10000 && i < s.Length; i++)
            {
                Console.CursorTop--;
                Console.WriteLine((s.Length - i) + "                     ");
                double rx = (double)r.Next(10000) / (double)1000;
                double ry = (double)r.Next(10000) / (double)1000;
                rnd1.Add(new double[] { lat1 + (lat2 - lat1) * rx, lng1 + (lng2 - lng1) * ry });
                s[i].waterMeterX = rnd1[i][0];
                s[i].waterMeterY = rnd1[i][1];
                bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, s[i]);
            }

        }

        [MaintenanceJobMethod("Generate ticks for subscription tables")]
        public static void setSubscriptionTicks()
        {

            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            DataTable dt = bdesub.WriterHelper.GetDataTable("Select id,dateFrom,dateTo,timeBinding from " + bdesub.DBName + ".dbo.Subscription");
            int c = dt.Rows.Count;
            Console.WriteLine();
            foreach (DataRow row in dt.Rows)
            {
                Console.CursorTop--;
                Console.WriteLine((c--) + "                                   ");
                int subscriptionID = (int)row[0];
                DateTime from = (DateTime)row[1];
                DateTime to = (DateTime)row[2];
                int timeBinding = (int)row[3];
                bdesub.WriterHelper.Update(bdesub.DBName
                    , "Subscription"
                    , new string[] { "ticksFrom", "ticksTo", "timeBinding" }
                    , new object[] { from.Ticks, timeBinding == 1 ? -1 : to.Ticks, timeBinding }, string.Format("id={0} and dateFrom='{1}'", subscriptionID, from));

            }
        }
        [MaintenanceJobMethod("Randomly Separate Overlapping Coordinates")]
        public static void unpackGIS()
        {


            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            List<double[]> rnd1 = new List<double[]>();
            Random r = new Random();
            Subscription[] s = bdesub.WriterHelper.GetSTRArrayByFilter<Subscription>(null);
            List<int> updatedList = new List<int>();
            for (int i = 0; i < s.Length - 1; i++)
            {
                if (!s[i].hasCoordinate)
                    continue;
                Console.CursorTop--;
                Console.WriteLine((s.Length - i) + "                     ");
                for (int j = i + 1; j < s.Length; j++)
                {
                    if (!s[j].hasCoordinate)
                        continue;
                    if (updatedList.Contains(s[j].id))
                        continue;
                    if (s[i].waterMeterX == s[j].waterMeterX && s[i].waterMeterY == s[j].waterMeterY)
                    {
                        double rx = (double)r.Next(1000) / (double)1000;
                        double ry = (double)r.Next(1000) / (double)1000;
                        s[j].waterMeterX = s[i].waterMeterY + (rx - 0.5) * 1e-3;
                        s[j].waterMeterY = s[i].waterMeterY + (ry - 0.5) * 1e-3;
                        bdesub.WriterHelper.UpdateSingleTableRecord(bdesub.DBName, s[j]);
                        updatedList.Add(s[j].id);
                    }
                }
            }

        }



        [MaintenanceJobMethod("Delete wrong period outstanding bills")]
        public static void deleteWrongOutstanding()
        {
            MessageRepository.dontDoDiff = true;

            try
            {
                Console.WriteLine("Deleting wrong period bills");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>(string.Format(@"periodID<>-1 and
                                (Select fromDate from {0}.dbo.BillPeriod where id=CustomerBill.periodID)>'June 1,2014'
                                and exists(Select * from {0}.dbo.LegacyBill where periodID=CustomerBill.periodID and customerID=CustomerBill.connectionID)", bdesub.DBName));
                Console.WriteLine(bills.Length + " bills found");
                Console.WriteLine();
                int c = bills.Length;
                List<int> paid = new List<int>();

                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    try
                    {
                        if (bill.paymentDocumentID != -1)
                        {
                            accounting.DeleteGenericDocument(-1, bill.paymentDocumentID);
                        }
                        bdesub.deleteCustomerBill(-1, bill.id);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    c--;
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        //[MaintenanceJobMethod("Payroll: fix orphaned loan return data")]
        //public static void payrollFixOrphanedLoanReturnData()
        //{
        //    MessageRepository.dontDoDiff = true;
        //    try
        //    {
        //        int N;
        //        Console.WriteLine("Cleaning orphaned loan payroll data");
        //        AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
        //        iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
        //        PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
        //        accounting.WriterHelper.setReadDB(accounting.DBName);


        //        bde.WriterHelper.BeginTransaction();
        //        try
        //        {
        //            int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Server maintenance job-Payroll: fix orphaned loan return data", "", -1);
        //            int c;
        //            Employee[] allEmployes = bdePayroll.GetEmployees(new AppliesToEmployees(true));
        //            c = allEmployes.Length;
        //            Console.WriteLine("Clearing " + c + " employees return data");
        //            foreach (Employee emp in allEmployes)
        //            {
        //                Console.CursorTop--;
        //                Console.WriteLine((c--) + "                      ");
        //                if (emp.status != EmployeeStatus.Enrolled)
        //                    continue;
        //                PayrollComponentData[] allData = bdePayroll.GetPCData(4, -1, AppliesToObjectType.Employee, emp.id, -1, false);
        //                foreach (PayrollComponentData data in allData)
        //                {
        //                    bdePayroll.DeleteData(1, data.ID);
        //                }
        //            }
        //            DocumentType dt = accounting.GetDocumentTypeByTypeNoException(typeof(LongTermLoanWithServiceChargeDocument));
        //            if (dt == null)
        //                dt = accounting.GetDocumentTypeByTypeNoException(typeof(LongTermLoanDocument));
        //            AccountDocument[] docs = accounting.GetAccountDocuments(null, dt.id,
        //                            false, DateTime.Now, DateTime.Now, 0, -1, out N);
        //            Console.WriteLine(docs.Length + " long term loans found");
        //            Console.WriteLine();
        //            c = docs.Length;

        //            foreach (StaffLoandDocumentBase doc in docs)
        //            {
        //                Console.CursorTop--;
        //                Console.WriteLine((c--) + "                      ");
        //                if (bdePayroll.GetEmployee(doc.employeeID).status != EmployeeStatus.Enrolled)
        //                    continue;
        //                accounting.PostGenericDocument(AID, accounting.GetAccountDocument(doc.AccountDocumentID, true));

        //            }
        //            Console.WriteLine(docs.Length + " short term loans found");
        //            Console.WriteLine();

        //            docs = accounting.GetAccountDocuments(null, accounting.GetDocumentTypeByType(typeof(ShortTermLoanDocument)).id,
        //                false, DateTime.Now, DateTime.Now, 0, -1, out N);
        //            c = docs.Length;
        //            foreach (ShortTermLoanDocument doc in docs)
        //            {
        //                Console.CursorTop--;
        //                Console.WriteLine((c--) + "                      ");
        //                if (bdePayroll.GetEmployee(doc.employeeID).status != EmployeeStatus.Enrolled)
        //                    continue;

        //                accounting.PostGenericDocument(-1, accounting.GetAccountDocument(doc.AccountDocumentID, true));

        //            }
        //            bde.WriterHelper.CommitTransaction();
        //        }
        //        catch (Exception ex)
        //        {
        //            bde.WriterHelper.RollBackTransaction();
        //            Console.WriteLine(ex.Message);
        //            Console.ReadLine();
        //        }
        //    }
        //    finally
        //    {
        //        MessageRepository.dontDoDiff = false;
        //    }
        //}

        [MaintenanceJobMethod("Woliso: fix data migration mistake")]
        public static void fixWolisoMigrateMistake()
        {
            Console.WriteLine("Fix data migration mistake");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);

            string sql = @"SELECT subscriptionID
                        FROM Subscriber.[dbo].[BWFMeterReading] r
                    where periodID=156
                and ((Select reading from Subscriber.[dbo].BWFMeterReading where periodID=155 and r.subscriptionID=subscriptionID)+consumption<>reading)";
            
            int[] subscids= bdesub.WriterHelper.GetColumnArray<int>(sql);
            Console.WriteLine(subscids.Length + " errors found.Press enter to continue");
            Console.ReadLine();
            Console.WriteLine();
            int c = subscids.Length;
            List<Subscription> paid = new List<Subscription>();
            ReadingEntryClerk clerk=bdesub.BWFGetClerkByUserID("admin");
            int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Maintenance job - Billing: fix data migration mistake", "", "", -1);
            bdesub.WriterHelper.BeginTransaction();
            try
            {
            foreach (int subscid in subscids)
            {
                
                    Subscription subsc = bdesub.GetSubscription(subscid, DateTime.Now.Ticks);
                    if (subsc == null)
                        throw new Exception("Invalid subscription id:" + subscid);
                    Console.WriteLine("Processing {0} remaining {1}", subsc.contractNo, --c);
                    CustomerBillRecord [] bill = bdesub.getBills(5, subsc.subscriber.id, subsc.id, 156);

                    if (bill.Length == 1)
                    {
                        
                        if(bill[0].paymentDocumentID!=-1)
                        {
                            Console.WriteLine("Bill paid, skipping");
                            paid.Add(subsc);
                                continue;
                        }
                            Console.WriteLine("Deleting bill");
                        bdesub.deleteCustomerBill(AID, bill[0].id);
                    }
                    else if (bill.Length > 1)
                    {
                        throw new Exception("Multiple bills found");
                    }
                    else
                    {
                        Console.Write("Bill not found, is it ok? Close window if not ok.");
                        Console.ReadLine();
                    }
                    Console.WriteLine("Updating reading");
                    BWFMeterReading reading=bdesub.BWFGetMeterReadingByPeriod(subsc.id, 156);
                    bdesub.BWFUpdateMeterReading(AID, clerk, reading);
                    Console.WriteLine("Generating bill");    
                    bdesub.generateBills(AID, DateTime.Now, 156, new int[] { subsc.subscriber.id });
                }
                bdesub.WriterHelper.CommitTransaction();
                Console.WriteLine("Paid bills");
                foreach(Subscription s in paid)
                {
                    Console.WriteLine(s.contractNo);
                }
                Console.ReadLine();
            }
            catch(Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                throw;
            }

        }


        [MaintenanceJobMethod("Delete Water Bills With No Reading")]
        public static void deleteBillsWithNoReading()
        {
            Console.WriteLine("Deleting bills");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>(
                string.Format("periodID={0} and connectionID in (Select subscriptionID from {1}.dbo.BWFMeterReading where periodID={0} and bwfStatus=0)", 2312, bdesub.DBName)
                );
            Console.WriteLine(bills.Length + " bills found.Press enter to continue");
            Console.ReadLine();
            Console.WriteLine();
            int c = bills.Length;
            List<int> paid = new List<int>();

            foreach (CustomerBillRecord bill in bills)
            {
                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                try
                {
                    if (bill.paymentDocumentID != -1)
                    {
                        accounting.DeleteGenericDocument(-1, bill.paymentDocumentID);
                    }
                    bdesub.deleteCustomerBill(-1, bill.id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
                c--;
            }

        }

        [MaintenanceJobMethod("Transfer Permanent Benefit Data To Tikimt By Prompt (Adama)")]
        public static void TransferPermanentBenefitDataByEmployeeID()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
                bde.WriterHelper.setReadDB(bde.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();

                    Console.WriteLine("Enter Employee ID");
                    string employeeID = Console.ReadLine();
                    int empID = bdePayroll.GetEmployeeByID(employeeID).id;
                    int periodID = 344;
                    int newPeriodID = 347;
                    List<SingleValueData> otherData = new List<SingleValueData>();
                    List<string> names = new List<string>();
                    List<int> singleValueFormulaID = new List<int>();
                    int pcdID = (int)bde.GetSystemParameters(new string[] { "permanentBenefitComponentID" })[0];
                    bdePayroll.WriterHelper.setReadDB(bdePayroll.DBName);
                    int c;
                    if (pcdID != 0)
                    {
                        PayrollComponentFormula[] permanentBenefitFormula = bdePayroll.GetPCFormulae(pcdID);

                        int i = 0;
                        foreach (PayrollComponentFormula formula in permanentBenefitFormula)
                        {
                            PayrollComponentData[] ret = bdePayroll.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, empID, periodID, false);
                            if (ret.Length > 0)
                            {
                                object singleValueData = bdePayroll.GetPCAdditionalData(ret[0].ID);
                                if (singleValueData != null)
                                {
                                    SingleValueData data = (SingleValueData)singleValueData;
                                    otherData.Add(data);
                                    names.Add(formula.Name);
                                    singleValueFormulaID.Add(formula.ID);
                                    //PayPeriodRange range = ret[0].periodRange;
                                }
                            }

                            i++;
                        }
                        Console.WriteLine(singleValueFormulaID.Count + "Permanent Benefit Forumula found");
                        Console.WriteLine();
                        c = singleValueFormulaID.Count;
                        for (int j = 0; j < singleValueFormulaID.Count; j++)
                        {
                            Console.CursorTop--;
                            Console.WriteLine(c + "                      ");

                            PayrollComponentFormula f = bdePayroll.GetPCFormula(singleValueFormulaID[j]);
                            PayrollComponentData singleValueData = new PayrollComponentData();
                            singleValueData.at = new INTAPS.Payroll.AppliesToEmployees(empID);
                            singleValueData.FormulaID = singleValueFormulaID[j];
                            singleValueData.DataDate = DateTime.Now;
                            singleValueData.PCDID = f.PCDID;

                            PayrollPeriod thisPeriod = bdePayroll.GetPayPeriod(newPeriodID);
                            PayrollComponentData[] data = bdePayroll.GetPCData(f.PCDID, f.ID, AppliesToObjectType.Employee, empID, newPeriodID, false);
                            if (data.Length > 1)
                                throw new ServerUserMessage("Database error multiple permanent benefit found for period " + thisPeriod.name + ". Fix the problem using the payroll client and try again");
                            if (data.Length == 1)
                            {
                                PayrollPeriod prd = bdePayroll.GetPayPeriod(data[0].periodRange.PeriodFrom);
                                if (prd.fromDate < thisPeriod.fromDate)
                                {
                                    PayrollPeriod prev = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPreviousPeriod<PayrollPeriod>(bdePayroll.WriterHelper, "PayPeriod", thisPeriod);
                                    if (prev == null)
                                        throw new ServerUserMessage("Previos payroll pariod not generated for " + thisPeriod.name);
                                    singleValueData.periodRange = new PayPeriodRange(newPeriodID, data[0].periodRange.PeriodTo);
                                    data[0].periodRange.PeriodTo = prev.id;
                                    bdePayroll.UpdatePCData(1, data[0]);
                                }
                                else if (prd.fromDate.Equals(thisPeriod.fromDate))
                                {
                                    bdePayroll.DeleteData(1, data[0].ID);
                                    singleValueData.periodRange = new PayPeriodRange(newPeriodID, data[0].periodRange.PeriodTo);
                                }
                                else
                                {
                                    throw new ServerUserMessage("BUG: unexpected case. GetPCData returned data that applies to future date");
                                }
                            }
                            else
                                singleValueData.periodRange = new PayPeriodRange(newPeriodID, -1);

                            bdePayroll.CreatePCData(1, singleValueData, otherData[j]);
                            c--;
                        }


                        bde.WriterHelper.CommitTransaction();
                    }
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.StackTrace);
                    Console.ReadLine();
                }

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }

        [MaintenanceJobMethod("Transfer All Employee Permanent Benefit Data To Tikimt(Adama)")]
        public static void TransferAllEmployeesPermanentBenefitData()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                PayrollBDE bdePayroll = ApplicationServer.GetBDE("Payroll") as PayrollBDE;
                bde.WriterHelper.setReadDB(bde.DBName);
                try
                {
                    bde.WriterHelper.BeginTransaction();


                    int periodID = 205; //Previous period
                    int newPeriodID = 207; //Destination period
                    List<SingleValueData> otherData = new List<SingleValueData>();
                    List<string> names = new List<string>();
                    List<int> singleValueFormulaID = new List<int>();
                    int pcdID = (int)bde.GetSystemParameters(new string[] { "permanentBenefitComponentID" })[0];
                    bdePayroll.WriterHelper.setReadDB(bdePayroll.DBName);
                    int c = 0;
                    if (pcdID != 0)
                    {

                        Employee[] employees = bdePayroll.GetEmployees(new AppliesToEmployees(true));
                        Console.WriteLine(employees.Length + "employees found");
                        Console.WriteLine();
                        c = employees.Length;
                        foreach (Employee employee in employees)
                        {
                            otherData.Clear();
                            names.Clear();
                            singleValueFormulaID.Clear();
                            Console.CursorTop--;
                            Console.WriteLine(c + "                      ");

                            if (employee.status == EmployeeStatus.Enrolled)
                            {
                                PayrollComponentFormula[] permanentBenefitFormula = bdePayroll.GetPCFormulae(pcdID);

                                int i = 0;
                                foreach (PayrollComponentFormula formula in permanentBenefitFormula)
                                {
                                    PayrollComponentData[] ret = bdePayroll.GetPCData(pcdID, permanentBenefitFormula[i].ID, AppliesToObjectType.Employee, employee.id, periodID, false);
                                    if (ret.Length > 0)
                                    {
                                        object singleValueData = bdePayroll.GetPCAdditionalData(ret[0].ID);
                                        if (singleValueData != null)
                                        {
                                            SingleValueData data = (SingleValueData)singleValueData;
                                            otherData.Add(data);
                                            names.Add(formula.Name);
                                            singleValueFormulaID.Add(formula.ID);
                                            //PayPeriodRange range = ret[0].periodRange;
                                        }
                                    }

                                    i++;
                                }

                                for (int j = 0; j < singleValueFormulaID.Count; j++)
                                {

                                    PayrollComponentFormula f = bdePayroll.GetPCFormula(singleValueFormulaID[j]);
                                    PayrollComponentData singleValueData = new PayrollComponentData();
                                    singleValueData.at = new INTAPS.Payroll.AppliesToEmployees(employee.id);
                                    singleValueData.FormulaID = singleValueFormulaID[j];
                                    singleValueData.DataDate = DateTime.Now;
                                    singleValueData.PCDID = f.PCDID;

                                    PayrollPeriod thisPeriod = bdePayroll.GetPayPeriod(newPeriodID);
                                    PayrollComponentData[] data = bdePayroll.GetPCData(f.PCDID, f.ID, AppliesToObjectType.Employee, employee.id, newPeriodID, false);
                                    if (data.Length > 1)
                                        throw new ServerUserMessage("Database error multiple permanent benefit found for period " + thisPeriod.name + ". Fix the problem using the payroll client and try again");
                                    if (data.Length == 1)
                                    {
                                        PayrollPeriod prd = bdePayroll.GetPayPeriod(data[0].periodRange.PeriodFrom);
                                        if (prd.fromDate < thisPeriod.fromDate)
                                        {
                                            PayrollPeriod prev = INTAPS.Accounting.BDE.AccountingPeriodHelper.GetPreviousPeriod<PayrollPeriod>(bdePayroll.WriterHelper, "PayPeriod", thisPeriod);
                                            if (prev == null)
                                                throw new ServerUserMessage("Previos payroll pariod not generated for " + thisPeriod.name);
                                            singleValueData.periodRange = new PayPeriodRange(newPeriodID, data[0].periodRange.PeriodTo);
                                            data[0].periodRange.PeriodTo = prev.id;
                                            bdePayroll.UpdatePCData(1, data[0]);
                                        }
                                        else if (prd.fromDate.Equals(thisPeriod.fromDate))
                                        {
                                            bdePayroll.DeleteData(1, data[0].ID);
                                            singleValueData.periodRange = new PayPeriodRange(newPeriodID, data[0].periodRange.PeriodTo);
                                        }
                                        else
                                        {
                                            throw new ServerUserMessage("BUG: unexpected case. GetPCData returned data that applies to future date");
                                        }
                                    }
                                    else
                                        singleValueData.periodRange = new PayPeriodRange(newPeriodID, -1);

                                    bdePayroll.CreatePCData(1, singleValueData, otherData[j]);

                                }
                                c--;
                            }
                        }
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.StackTrace);
                    Console.ReadLine();
                }

            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Clear customer service transactions")]
        public static void deleteCustomerServiceTransactions()
        {
            Console.WriteLine("Deleting bills");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            JobManagerBDE bdeJob = ApplicationServer.GetBDE("JobManager") as JobManagerBDE;

            try
            {
                bdeJob.WriterHelper.setReadDB(bdeJob.DBName);
                JobData[] data = bdeJob.WriterHelper.GetSTRArrayByFilter<JobData>(null);
                bdeJob.WriterHelper.restoreReadDB();
                int c = data.Length;
                Console.WriteLine(c + " jobs found");
                Console.WriteLine();
                int completion = 0;
                int bill = 0;
                int pyament = 0;
                foreach (JobData job in data)
                {
                    Console.CursorTop--;
                    Console.WriteLine(string.Format("Remaining:{0} \tBills:{1} \tPayments:{2} \tCompletion:{3}      ", c--, bill, pyament, completion));
                    JobBillOfMaterial[] bom = bdeJob.GetJobBOM(job.id);
                    if (job.completionDocumentID != -1 && accounting.DocumentExists(job.completionDocumentID))
                    {
                        completion++;
                        CompletedJobHandler handler = accounting.GetDocumentHandler(accounting.GetDocumentTypeByType(typeof(CompletedJobDocument)).id) as CompletedJobHandler;
                        handler.delteJobCompletionDocument(-1, job.completionDocumentID);
                    }
                    foreach (JobBillOfMaterial b in bom)
                    {
                        if (b.invoiceNo != -1 && accounting.DocumentExists(b.invoiceNo))
                        {
                            CustomerBillRecord rec = bdesub.getCustomerBillRecord(b.invoiceNo);
                            foreach (DocumentSerial r in accounting.getDocumentSerials(b.invoiceNo))
                                accounting.removeDocumentSerial(-1, b.invoiceNo, r.typedReference);
                            if (accounting.DocumentExists(b.invoiceNo))
                                accounting.DeleteGenericDocument(-1, b.invoiceNo);
                            bill++;

                            if (rec != null && rec.paymentDocumentID != -1 && accounting.DocumentExists(rec.paymentDocumentID))
                            {
                                pyament++;

                                foreach (DocumentSerial r in accounting.getDocumentSerials(rec.paymentDocumentID))
                                    accounting.removeDocumentSerial(-1, rec.paymentDocumentID, r.typedReference);
                                if (accounting.DocumentExists(rec.paymentDocumentID))
                                    accounting.DeleteGenericDocument(-1, rec.paymentDocumentID);
                            }
                        }
                    }
                    bdeJob.WriterHelper.CheckTransactionIntegrity();
                }
                Console.WriteLine("Deleting remaining transactions");
                Account ac = accounting.GetAccount<Account>("4200");
                int ac4200 = accounting.GetCostCenterAccount(1, ac.id).id;
                int N;
                AccountBalance bal;
                List<int> deletedDocument = new List<int>();
                Console.WriteLine();
                AccountTransaction[] trans = accounting.GetLedger(ac4200, 0, new int[] { 0 }, new DateTime(1800, 1, 1), new DateTime(2100, 1, 1), 0, -1, out N, out bal, out bal);
                c = trans.Length;
                foreach (AccountTransaction tran in trans)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c-- + "                       ");
                    if (deletedDocument.Contains(tran.documentID))
                        continue;
                    foreach (DocumentSerial r in accounting.getDocumentSerials(tran.documentID))
                        accounting.removeDocumentSerial(-1, tran.documentID, r.typedReference);
                    accounting.DeleteAccountDocument(-1, tran.documentID, false);
                    deletedDocument.Add(tran.documentID);
                }

            }
            catch (Exception ex)
            {
                ApplicationServer.EventLoger.LogException("", ex);
            }
            finally
            {
                bdeJob.WriterHelper.CheckTransactionIntegrity();
            }
        }
        //[MaintenanceJobMethod("WF: assing all roles to admin")]
        //public static void wfAssingAllRolesToAdmin()
        //{
        //    INTAPS.WF.Server.WFBDE wfbde = ApplicationServer.GetBDE("WF") as INTAPS.WF.Server.WFBDE;
        //    long ticks = DateTime.Now.Ticks;
        //    Payroll.Employee emp = wfbde.getEmployeeProvide().getEmployee(ticks, "admin");
        //    if (emp == null)
        //    {
        //        Console.WriteLine("No employee set with admin user name");
        //        return;
        //    }
        //    wfbde.WriterHelper.BeginTransaction();
        //    try
        //    {
        //        foreach (INTAPS.WF.WorkType wt in wfbde.getAllWorkTypes())
        //        {
        //            INTAPS.WF.Server.IWorkFlowRuleServer rule = wfbde.getRuleServer(wt.id, ticks);
        //            if (rule == null)
        //                continue;
        //            foreach (INTAPS.WF.WFRole role in rule.possibleRoles)
        //            {
        //                if (!wfbde.employeeHasRole(wfbde.WriterHelper, ticks, "admin", wt.id, role.id))
        //                    wfbde.assignRole(-1, -1, emp.id, ticks, wt.id, role.id);
        //            }

        //        }
        //        wfbde.WriterHelper.CommitTransaction();
        //    }
        //    catch (Exception ex)
        //    {
        //        wfbde.WriterHelper.RollBackTransaction();
        //        Console.WriteLine(ex.Message);
        //    }
        //}

        [MaintenanceJobMethod("Import Outstanding Bills (Harar WSIS)")]
        public static void importOutstandingBills()
        {

            Console.WriteLine("Importing outstanding bills");
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            DataTable hararTable = bdesub.WriterHelper.GetDataTable("select * from HararSourceDB.dbo.Outstanding");
            int i = 0;
            //bdesub.WriterHelper.BeginTransaction();
            Console.WriteLine(hararTable.Rows.Count + " outstanding bills found.Press enter to continue");
            Console.ReadLine();
            Console.WriteLine();
            int c = hararTable.Rows.Count;
            foreach (DataRow row in hararTable.Rows)
            {

                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                try
                {
                    // bdesub.WriterHelper.BeginTransaction();
                    string reference = row[0] is DBNull ? "NoRef" : (string)row[0];
                    if (row[1] is DBNull)
                        continue;
                    // Subscription subscription = bdesub.GetSubscriptionNoException((string)row[1]);
                    Subscription subscription = bdesub.GetSubscription(row[1].ToString(), DateTime.Now.Ticks);

                    if (subscription == null)
                        continue;
                    int subscriptionID = subscription.id;
                    //else
                    //   subscriberID = subscription == null ? -1 : subscription.subscriberID;
                    int year = int.Parse((string)row[6]);
                    if (row[7] is DBNull && row[8] is DBNull)
                        continue;
                    string month = row[7] is DBNull || string.IsNullOrEmpty(row[7] as string) ? (string)row[8] : (string)row[7];
                    string period = month + " " + year;
                    int periodID = _getBillPeriodByName(period, bdesub.WriterHelper);
                    object reading = row[10];
                    object consumption = row[12];
                    double meterRent = row[13] is DBNull ? 0 : double.Parse(row[13].ToString());
                    double billamount = row[14] is DBNull ? 0 : double.Parse(row[14].ToString());
                    double totalAmount = row[15] is DBNull ? 0 : double.Parse(row[15].ToString());

                    int orderN = i + 1;

                    LegacyBill outStandingBill = new LegacyBill();
                    outStandingBill.id = INTAPS.ClientServer.AutoIncrement.GetKey("LegacyBillID");
                    outStandingBill.reference = reference;
                    outStandingBill.customerID = subscriptionID;
                    outStandingBill.periodID = periodID;
                    outStandingBill.setID = 1;
                    outStandingBill.reading = reading is DBNull ? 0 : double.Parse(reading.ToString());
                    outStandingBill.consumption = consumption is DBNull ? 0 : double.Parse(consumption.ToString());
                    outStandingBill.setOrderN = orderN;
                    outStandingBill.remark = "Import outstanding bill from Harar DB";
                    int legacyBillID = bdesub.WriterHelper.InsertSingleTableRecord<LegacyBill>(bdesub.DBName, outStandingBill);
                    //Insert legacy bill item
                    if (meterRent != 0)
                    {
                        LegacyBillItem item = new LegacyBillItem();
                        item.legacyBillID = outStandingBill.id;
                        item.itemID = 2;
                        item.amount = meterRent;
                        bdesub.WriterHelper.InsertSingleTableRecord<LegacyBillItem>(bdesub.DBName, item);
                    }
                    if (billamount != 0)
                    {
                        LegacyBillItem item = new LegacyBillItem();
                        item.legacyBillID = outStandingBill.id;
                        item.itemID = 1;
                        item.amount = billamount;
                        bdesub.WriterHelper.InsertSingleTableRecord<LegacyBillItem>(bdesub.DBName, item);
                    }
                    if (meterRent == 0 && billamount == 0)
                    {
                        LegacyBillItem item = new LegacyBillItem();
                        item.legacyBillID = outStandingBill.id;
                        item.itemID = 1;
                        item.amount = totalAmount;
                        bdesub.WriterHelper.InsertSingleTableRecord<LegacyBillItem>(bdesub.DBName, item);
                    }

                    i++;
                    // bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    // bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                c--;
            }

        }
        [MaintenanceJobMethod("Billing: generate sms bill notification")]
        public static void generateSMSBillNotification()
        {
            string format = System.Configuration.ConfigurationManager.AppSettings["smsFormat"];
            Console.WriteLine("Generating sms bill notififcation");
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            bdesub.WriterHelper.BeginTransaction();
            try
            {
                Console.WriteLine("Clearing existing data.");
                bdesub.WriterHelper.ExecuteNonQuery(string.Format("Delete from {0}.dbo.SMSOut", bdesub.DBName));
                Console.WriteLine("Data cleared.");
                string sql = @"SELECT     Subscriber.phoneNo,Subscription.ContractNo, Subscriber.name, 

isNull(SUM(case when periodID={1} then CustomerBillItem.price end),0) AS Cur,
IsNull(SUM(case when periodID<>{1} then CustomerBillItem.price end),0) AS Overd
FROM         {0}.dbo.Subscriber INNER JOIN
                      {0}.dbo.Subscription ON Subscriber.id = Subscription.subscriberID INNER JOIN
                      {0}.dbo.CustomerBill INNER JOIN
                      {0}.dbo.CustomerBillItem ON CustomerBill.id = CustomerBillItem.customerBillID ON Subscription.id = CustomerBill.connectionID
where Subscriber.phoneNo<>'' and paymentDocumentID=-1 and paymentDiffered=0 and timeBinding=1
GROUP BY CustomerBill.connectionID, Subscriber.name, Subscription.id,Subscriber.phoneNo,Subscription.ContractNo
having isNull(SUM(case when periodID={1} then CustomerBillItem.price end),0)>0";
                sql = string.Format(sql, bdesub.DBName, bdesub.SysPars.currentPeriod);
                DataTable table = bdesub.WriterHelper.GetDataTable(sql);
                int c = table.Rows.Count;
                BillPeriod period = bdesub.GetBillPeriod(bdesub.SysPars.currentPeriod);
                BillPeriod nextPeriod = AccountingPeriodHelper.GetNextPeriod<BillPeriod>(bdesub.WriterHelper, "BillPeriod", period);
                Console.WriteLine("{0} sms will be generated", c);
                Console.WriteLine();
                foreach (DataRow row in table.Rows)
                {
                    Console.CursorTop--;
                    Console.WriteLine((c--) + "                ");
                    string phoneNo = row[0] as string;
                    string contNo = row[1] as string;
                    string name = row[2] as string;
                    double current = (double)row[3];
                    double overd = (double)row[4];
                    string sms = format.Replace("\\n", "\n");
                    sms = sms.Replace("$name", name);
                    sms = sms.Replace("$bill", (current + overd).ToString("#,#0.00"));
                    sms = sms.Replace("$cont", contNo);
                    sms = sms.Replace("$period", period.name);
                    string before = INTAPS.Ethiopic.EtGrDate.GetEtMonthNameEng(INTAPS.Ethiopic.EtGrDate.ToEth(nextPeriod.fromDate).Month);
                    sms = sms.Replace("$before", before);
                    bdesub.WriterHelper.Insert(bdesub.DBName, "SMSOut", new string[] { "phoneNo", "message" }
                        , new object[] { phoneNo, sms });
                }
                bdesub.WriterHelper.CommitTransaction();
            }
            catch (Exception ex)
            {
                bdesub.WriterHelper.RollBackTransaction();
                Console.WriteLine(ex.Message);
            }

        }
        [MaintenanceJobMethod("Report: export all reports)")]
        public static void reportExportAll()
        {
            Console.WriteLine("Exporting");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(ReportDefination));
            Console.WriteLine();
            reportExportAll(accounting, ".\\reports", s, -1, 0);
        }
        static int reportExportAll(AccountingBDE accounting, string folder, System.Xml.Serialization.XmlSerializer s, int catID, int f)
        {
            if (!System.IO.Directory.Exists(folder))
                System.IO.Directory.CreateDirectory(folder);
            foreach (ReportDefination def in accounting.GetReportByCategory(catID))
            {
                string filename = folder + "\\" + def.name + ".reportdef";
                using (System.IO.FileStream fs = System.IO.File.Create(filename))
                {
                    Console.CursorTop--;
                    Console.WriteLine(f++ + "                           ");
                    s.Serialize(fs, def);
                }
            }
            foreach (ReportCategory cat in accounting.GetChildReportCategories(catID))
            {
                string sbFolder = folder + "\\" + cat.name;
                f = reportExportAll(accounting, sbFolder, s, cat.id, f);
            }
            return f;
        }
        [MaintenanceJobMethod("Resolve repeated outstanding bills (Harar WSIS)")]
        public static void fixRepeatedOutstandingBills()
        {

            Console.WriteLine("Fixing repeated outstanding bills");
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            DataTable hararTable = bdesub.WriterHelper.GetDataTable("select * from HararSourceDB.dbo.repeatedOutstandingBills");

            Console.WriteLine(hararTable.Rows.Count + " repeated bills found.Press enter to continue");
            Console.ReadLine();
            Console.WriteLine();
            int c = hararTable.Rows.Count;
            foreach (DataRow row in hararTable.Rows)
            {

                Console.CursorTop--;
                Console.WriteLine(c + "                      ");
                try
                {
                    //bdesub.WriterHelper.BeginTransaction();
                    string monthFilter = "";
                    if (row[2] is DBNull && row[3] is string)
                        monthFilter = String.Format("MonthFrom is null AND MonthTo='{0}'", (string)row[3]);
                    else if (row[3] is DBNull && row[2] is string)
                        monthFilter = String.Format("MonthFrom='{0}' AND MonthTo is null", (string)row[2]);
                    else if (row[2] is DBNull && row[3] is DBNull)
                        continue;
                    //else if(row[2] is string && row[3] is string)

                    else
                        monthFilter = String.Format("MonthFrom='{0}' AND MonthTo='{1}'", ((string)row[2]).Trim(), ((string)row[3])).Trim();

                    string sql = String.Format(@"Delete from HararSourceDB.dbo.Outstanding where contratNo='{0}' AND Year='{1}' AND " + monthFilter, (string)row[0], (string)row[1]);
                    bdesub.WriterHelper.ExecuteScalar(sql);
                    //  bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    //bdesub.WriterHelper.RollBackTransaction();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                }
                c--;
            }

        }
        private static int _getBillPeriodByName(string period, SQLHelper sQLHelper)
        {
            string sql = String.Format("select id from Subscriber.dbo.BillPeriod where name='{0}'", period);
            int periodID = (int)sQLHelper.ExecuteScalar(sql);
            return periodID;
        }
        [MaintenanceJobMethod("Sales Point: Index sales point DB")]
        public static void indexSalesPointDB()
        {
            string dbname = "SalePoint";
            INTAPS.RDBMS.SQLHelper readerHelper = new SQLHelper("Data Source=localhost;Integrated Security=True;User ID=sa;Password=cisgis;Initial Catalog=SalePoint");
            INTAPS.RDBMS.SQLHelper writerHelper = new SQLHelper("Data Source=localhost;Integrated Security=True;User ID=sa;Password=cisgis;Initial Catalog=SalePoint");
            Dictionary<int, Type> typeByID = new Dictionary<int, Type>();
            {
                string alist = "SubscriberManagmentTypeLibrary;JobManagerTypeLibrary;iERPTransactionTypeLibrary;DDBillRuleTypeLibrary";
                string[] aname = alist.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string aa in aname)
                {
                    Assembly a = System.Reflection.Assembly.Load(aa);
                    foreach (Type t in a.GetTypes())
                    {
                        int id = ObjectTable.getTypeID(t, false);
                        if (id == -1)
                            continue;
                        typeByID.Add(id, t);
                    }
                }
            }
            ObjectTable.bindConnection(writerHelper);
            ObjectTable.loadTypes(System.Reflection.Assembly.Load("SubscriberManagmentTypeLibrary"));

            SqlDataReader reader = null;
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            try
            {
                int count = (int)readerHelper.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.ObjectInstance", dbname));
                Console.WriteLine("Indexing {0} objects", count);

                reader = readerHelper.ExecuteReader(string.Format("Select id,typeID,data from {0}.dbo.ObjectInstance", dbname));
                int c = count;
                int f = 0;
                Console.WriteLine();
                while (reader.Read())
                {
                    if (c % 100 == 0)
                    {
                        Console.CursorTop--;
                        Console.WriteLine("{0}\tIndexed: {1}      ", c, f);
                    }
                    c--;
                    string id = reader[0] as string;
                    int typeID = (int)reader[1];
                    byte[] data = reader[2] as byte[];
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(data, false);
                    object o = bf.Deserialize(new System.IO.MemoryStream(data, false));
                    ms.Dispose();
                    data = null;
                    ITypeIndexer indexer = ObjectTable.getIndexer(typeByID[typeID]);
                    if (indexer != null)
                    {
                        writerHelper.BeginTransaction();
                        try
                        {
                            indexer.createIndex(writerHelper, o);
                            f++;
                            writerHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            writerHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.WriteLine();
                        }
                    }
                }
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }
        [MaintenanceJobMethod("Accounting: Close Year")]
        public static void closeYear()
        {
            Console.Write("Enter year:");
            int year;
            while (!int.TryParse(Console.ReadLine(), out year) || year < 2000 || year > 2010)
            {
                Console.Write("Invalid year. Enter again:");
            }

            try
            {
                iERPTransactionBDE bdeBERP = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                Account capitalAccount;
                Console.WriteLine("Capital accounts");
                foreach (Account ac in bdeBERP.Accounting.GetLeafAccounts<Account>(bdeBERP.Accounting.GetAccountID<Account>("30000")))
                {
                    Console.WriteLine(ac.Code + "\t" + ac.Name);
                }
                Console.Write("Enter capital account code:");
                while ((capitalAccount = bdeBERP.Accounting.GetAccount<Account>(Console.ReadLine())) == null || !capitalAccount.Code.StartsWith("3"))
                {
                    Console.Write("Invalid capital account. Enter again:");
                }
                AdjustmentDocument adj = new AdjustmentDocument();
                adj.DocumentDate = INTAPS.Ethiopic.EtGrDate.ToGrig(new INTAPS.Ethiopic.EtGrDate(30, 10, year)).GridDate;
                adj.DocumentDate = new DateTime(adj.DocumentDate.Year, adj.DocumentDate.Month, adj.DocumentDate.Day, 23, 58, 59);
                adj.voucher = new DocumentTypedReference(bdeBERP.Accounting.getDocumentSerialType("JV").id, "JV-Closing-" + year, true);
                List<AdjustmentAccount> incomeAccounts = new List<AdjustmentAccount>();
                double netIncome = 0;
                Console.WriteLine("Collecting balances..");
                List<Account> inactiveAccounts = new List<Account>();
                double totalCr = 0, totalDb = 0;
                foreach (string parent in new string[] { "41000", "50000" })
                {
                    Account parentAccount = bdeBERP.Accounting.GetAccount<Account>(parent);
                    foreach (Account la in bdeBERP.Accounting.GetLeafAccounts<Account>(parentAccount.id))
                    {
                        bool hasTransaction = false;
                        foreach (CostCenterAccount csa in bdeBERP.Accounting.GetCostCenterAccountsOf<Account>(la.id))
                        {
                            if (csa.isControlAccount)
                                continue;
                            double balance = bdeBERP.Accounting.GetNetBalanceAsOf(csa.id, 0, adj.DocumentDate);
                            if (AccountBase.AmountEqual(balance, 0))
                                continue;
                            AdjustmentAccount adjac = new AdjustmentAccount();
                            adjac.accountCode = la.Code;
                            adjac.accountID = csa.accountID;
                            adjac.costCenterID = csa.costCenterID;

                            if (csa.creditAccount)
                            {
                                if (balance > 0)
                                {
                                    adjac.creditAmount = 0;
                                    adjac.debitAmount = balance;
                                    totalDb += balance;
                                }
                                else
                                {
                                    adjac.creditAmount = -balance;
                                    adjac.debitAmount = 0;
                                    totalDb += balance;
                                }
                                netIncome += balance;
                            }
                            else
                            {
                                if (balance > 0)
                                {
                                    adjac.creditAmount = balance;
                                    adjac.debitAmount = 0;
                                    totalDb -= balance;
                                }
                                else
                                {
                                    adjac.creditAmount = 0;
                                    adjac.debitAmount = -balance;
                                    totalDb -= balance;
                                }
                                netIncome -= balance;
                            }
                            adjac.description = "Closing transaction";
                            incomeAccounts.Add(adjac);
                            hasTransaction = true;
                        }
                        if (hasTransaction && la.Status != AccountStatus.Activated)
                        {
                            inactiveAccounts.Add(la);
                        }
                    }
                }

                if (!AccountBase.AmountEqual(netIncome, 0))
                {
                    AdjustmentAccount adjac = new AdjustmentAccount();
                    adjac.accountCode = capitalAccount.Code;
                    adjac.accountID = capitalAccount.id;
                    adjac.costCenterID = bdeBERP.SysPars.mainCostCenterID;
                    if (netIncome > 0)
                    {
                        adjac.creditAmount = netIncome;
                        adjac.debitAmount = 0;
                        totalDb -= netIncome;
                    }
                    else
                    {
                        adjac.creditAmount = 0;
                        adjac.debitAmount = -netIncome;
                        totalDb += -netIncome;
                    }
                    adjac.description = "Closing transaction";
                    incomeAccounts.Add(adjac);
                }
                Console.WriteLine("{0} accounts found. Net Income:{1} TotalDb:{2} TotalCr:{3}", incomeAccounts.Count - 1, netIncome.ToString("#,#0.00"), totalDb, totalCr);

                adj.adjustmentAccounts = incomeAccounts.ToArray();
                adj.ShortDescription = "Closing transaction for year " + year;
                Console.WriteLine("Enter to post or closing window to cancel.");
                Console.ReadLine();
                Console.WriteLine("Posting transaction. Will take time.");
                bdeBERP.WriterHelper.BeginTransaction();
                try
                {
                    foreach (Account a in inactiveAccounts)
                        bdeBERP.Accounting.ActivateAcount<Account>(1, a.id, DateTime.Now);
                    bdeBERP.Accounting.PostGenericDocument(1, adj);
                    foreach (Account a in inactiveAccounts)
                        bdeBERP.Accounting.DeactivateAccount<Account>(1, a.id, DateTime.Now);
                    bdeBERP.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeBERP.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed\n" + ex.Message);
            }
        }
        [MaintenanceJobMethod("Billing: Delete unpaid bills without rent")]
        public static void deleteUnpaidBillsWithNoRent()
        {
            MessageRepository.dontDoDiff = true;
            try
            {
                Console.WriteLine("Deleting unpaid bills without rent");
                AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
                iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
                SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                bdesub.WriterHelper.setReadDB(bdesub.DBName);
                string cr = @"paymentDocumentID=-1 and id in
(Select id from Subscriber.dbo.CustomerBill b inner join Subscriber.dbo.CustomerBillItem i on b.id=i.customerBillID
where b.periodID=2360 and i.itemTypeID=0 and b.billDocumentTypeID=5
and id not in (
Select id from Subscriber.dbo.CustomerBill b inner join Subscriber.dbo.CustomerBillItem i on b.id=i.customerBillID
where b.periodID=2360 and i.itemTypeID=1 and b.billDocumentTypeID=5))
";
                CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>(cr);
                Console.WriteLine(bills.Length + " bills found");
                Console.ReadLine();
                int c = bills.Length;
                bdesub.WriterHelper.BeginTransaction();
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Maintenance job - Billing: Delete unpaid bills without rent", "", "", -1);
                    foreach (CustomerBillRecord bill in bills)
                    {
                        Console.CursorTop--;
                        Console.WriteLine(c + "                      ");
                        bdesub.deleteCustomerBill(-1, bill.id);
                        c--;
                    }
                    bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                }
            }
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }


        static void mergeCustomers(SubscriberManagmentBDE subsc, ReadingEntryClerk clerk, int AID, Subscriber c1, Subscriber c2)
        {
            if (!c1.name.Trim().Equals(c2.name.Trim()))
                throw new ServerUserMessage("{0} and {1} don't have the same name", c1.customerCode, c2.customerCode);
            Subscription[] ss = subsc.GetSubscriptions(c1.id, DateTime.Now.Ticks);
            if (ss.Length == 0)
                throw new ServerUserMessage("Subscriptions not found for customers {0}.", c1.id);
            if (ss.Length > 1)
                throw new ServerUserMessage("Multiple subscriptions found for customers {0}.", c1.id);
            Subscription s1 = ss[0];

            ss = subsc.GetSubscriptions(c2.id, DateTime.Now.Ticks);
            if (ss.Length == 0)
                throw new ServerUserMessage("Subscriptions not found for customers {0}.", c2.id);
            if (ss.Length > 1)
                throw new ServerUserMessage("Multiple subscriptions found for customers {0}.", c2.id);
            Subscription s2 = ss[0];
            BWFMeterReading[] r1 = subsc.BWFGetMeterReadings(s1.id);
            BWFMeterReading[] r2 = subsc.BWFGetMeterReadings(s2.id);
            Dictionary<int, BWFMeterReading> ps = new Dictionary<int, BWFMeterReading>();
            foreach (BWFMeterReading r in r2)
                ps.Add(r.periodID, r);
            foreach (BWFMeterReading r in r1)
                if (ps.ContainsKey(r.periodID))
                {
                    BWFMeterReading reading2 = ps[r.periodID];
                    if (r.reading != reading2.reading || r.consumption != reading2.consumption)
                    {
                        throw new ServerUserMessage("Readings for subscription {0} and {1} overlap at period {2}", s1.id, s2.id, r.periodID);
                    }
                    ps.Remove(r.periodID);
                }

            foreach (BWFMeterReading r in r2)
            {
                if (ps.ContainsKey(r.periodID))
                {
                    r.subscriptionID = s1.id;
                    subsc.BWFAddMeterReading(AID, clerk, r, true);
                }
                else
                {
                    r.bwfStatus = 0;
                    //subsc.BWFUpdateMeterReading(AID, clerk, r);
                    //subsc.BWFDeleteMeterReading(AID, clerk, r.subscriptionID, r.periodID);
                }
            }
            //subsc.DeleteSubscription(AID, s2.id);
            //subsc.DeleteSubscriber(AID, c2.id);
        }
        [MaintenanceJobMethod("Billing(Dukem): merge repeated customers")]
        public static void billingDukemMergeCustomers()
        {
            Console.WriteLine("Merging repeated customers");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;


            ReadingEntryClerk clerk = subsc.BWFGetClerkByUserID("admin");
            if (clerk == null)
                throw new ServerUserMessage("Please setup reading clerk for admin");
            Console.WriteLine("Analyzing customer table...");
            DataTable data = accounting.WriterHelper.GetDataTable("Select CUCODE,KOD,KEB from Subscriber.dbo.Customer where KOD is not NULL and CUCODE is not null and KEB is not null");
            Dictionary<string, List<string>> newToOld = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> oldToNew = new Dictionary<string, List<string>>();
            HashSet<string> multiMapNew = new HashSet<string>();
            HashSet<string> multiMapOld = new HashSet<string>();
            foreach (DataRow row in data.Rows)
            {
                string keb;
                string kstr = (string)row[2];
                int knum;
                if (int.TryParse(kstr, out knum))
                    keb = knum.ToString("00");
                else
                    keb = "00";

                string newCode = "C" + keb + "-" + ((string)row[0]).Trim();
                string oldCode = "C" + keb + "-" + ((string)row[1]).Trim();
                if (newToOld.ContainsKey(newCode))
                {
                    List<string> oldCodes = newToOld[newCode];
                    if (!oldCodes.Contains(oldCode))
                    {
                        oldCodes.Add(oldCode);
                        if (oldCodes.Count == 2)
                        {
                            multiMapNew.Add(newCode);
                        }
                    }
                }
                else
                    newToOld.Add(newCode, new List<string>(new string[] { oldCode }));

                if (oldToNew.ContainsKey(oldCode))
                {
                    List<string> newCodes = oldToNew[oldCode];
                    if (!newCodes.Contains(newCode))
                    {
                        newCodes.Add(newCode);
                        if (newCodes.Count == 2)
                        {
                            multiMapOld.Add(oldCode);
                        }

                    }
                }
                else
                    oldToNew.Add(oldCode, new List<string>(new string[] { newCode }));
            }
            Console.WriteLine("{0} records processed", data.Rows.Count);
            Console.WriteLine("{0} new to old maps found", newToOld.Count);
            Console.WriteLine("{0} new to old map conflicts found", multiMapNew.Count);
            Console.WriteLine("{0} old to new map conflicts found", multiMapOld.Count);
            Console.ReadLine();

            int count = newToOld.Count;
            int success = 0, error = 0;
            foreach (KeyValuePair<string, List<string>> kv in newToOld)
            {
                Console.CursorTop--;
                Console.WriteLine("{0}\tOK:{1}\tError:{2}", count--, success, error);
                bde.WriterHelper.BeginTransaction();
                try
                {
                    if (kv.Value.Count > 1)
                    {
                        throw new ServerUserMessage("{0} mapped to more than one KODs", kv.Key);
                    }
                    string oldCode = kv.Value[0];
                    if (multiMapOld.Contains(oldCode))
                        throw new ServerUserMessage("{0} mapped to more than one CUCODES", kv.Key);

                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Billing(Dukem): merge repeated customers", "", -1);
                    Subscriber c1 = subsc.GetSubscriber(kv.Key);
                    Subscriber c2 = subsc.GetSubscriber(oldCode);
                    if (c1 == null || c2 == null)
                        throw new ServerUserMessage("One or both of customers not found. CUCODE:{0},KOD:{1}", kv.Key, oldCode);
                    mergeCustomers(subsc, clerk, AID, c1, c2);
                    success++;
                    bde.WriterHelper.RollBackTransaction();
                }
                catch (Exception ex)
                {
                    error++;
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine("Error processing {0}\n{1}\n", kv.Key, ex.Message);
                }

            }

        }

        [MaintenanceJobMethod("Billing(Dukem): merge repeated customers by name")]
        public static void billingDukemMergeCustomersByName()
        {
            Console.WriteLine("Merging repeated customers");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;


            ReadingEntryClerk clerk = subsc.BWFGetClerkByUserID("admin");
            if (clerk == null)
                throw new ServerUserMessage("Please setup reading clerk for admin");
            Console.WriteLine("Analyzing customer table...");
            DataTable data = accounting.WriterHelper.GetDataTable(@"Select id,name from Subscriber.dbo.Subscriber where name in (
Select name from Subscriber.dbo.Subscriber 
group by name having count(*)=2)
order by name,ID desc");
            Subscriber prev = null;
            int count = data.Rows.Count;
            int success = 0, error = 0;
            foreach (DataRow row in data.Rows)
            {

                Console.CursorTop--;
                Console.WriteLine("{0}\tOK:{1}\tError:{2}", count--, success, error);

                bde.WriterHelper.BeginTransaction();
                try
                {
                    int AID = ApplicationServer.SecurityBDE.WriteAddAudit("admin", 1, "Billing(Dukem): merge repeated customers", "", -1);
                    Subscriber s = subsc.GetSubscriber((int)row[0]);
                    if (prev != null && prev.name.Equals(s.name))
                        mergeCustomers(subsc, clerk, AID, prev, s);
                    prev = s;

                    success++;
                    bde.WriterHelper.RollBackTransaction();
                }
                catch (Exception ex)
                {
                    error++;
                    bde.WriterHelper.RollBackTransaction();
                    Console.WriteLine("Error processing {0}\n{1}\n", row[0], ex.Message);
                }

            }

        }
        [MaintenanceJobMethod("Dukem: fix incorret overdue bill that was paid by accounting as deposit")]
        public static void dukemFixIncorrectOverdueBills()
        {
            MessageRepository.dontDoDiff = true;


            Console.WriteLine("Fixing incorret overdue bill that was paid by accounting as deposit");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            bdesub.WriterHelper.setReadDB(bdesub.DBName);

            CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("periodID not in (144,145)");
            accounting.WriterHelper.setReadDB(accounting.DBName);
            
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Maintenance Job: Dukem: fix incorret overdue bill that was paid by accounting as deposit", "", "", -1);
                Console.WriteLine(bills.Length + " bills found\nEnter to start fixing");
                Console.ReadLine();
                int c = bills.Length;
                List<int> paid = new List<int>();
                int docType = accounting.GetDocumentTypeByType(typeof(UnclassifiedPayment)).id;
                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine(c + "                      ");
                    if (bill.paymentDocumentID == -1) //unpaid bills not expected
                        throw new ServerUserMessage("Unpaid bill found. ID:{0}".format(bill.paymentDocumentID));
                    bdesub.WriterHelper.BeginTransaction();
                    try
                    {
                        CustomerBillDocument doc = accounting.GetAccountDocument(bill.id, true) as CustomerBillDocument;
                        bdesub.WriterHelper.Update(bdesub.DBName, "CustomerBill", new string[] { "paymentDocumentID" }, new object[] { -1 }, "id=" + bill.id);
                        bdesub.deleteCustomerBill(AID, bill.id);
                        BillItem i = new BillItem();
                        i.itemTypeID = 0;
                        i.incomeAccountID = -1;
                        i.description = "Settled incorrect overdue bills";
                        i.hasUnitPrice = false;
                        i.price = doc.total;
                        i.accounting = BillItemAccounting.Advance;
                        UnclassifiedPayment b = new UnclassifiedPayment();
                        b.AccountDocumentID = doc.AccountDocumentID;
                        b.DocumentDate = doc.DocumentDate;
                        b.customer = bdesub.GetSubscriber(bill.customerID);
                        b.ShortDescription = "Settled incorrect overdue bills ({0})".format(doc.ShortDescription);
                        b.billItems = new BillItem[] { i };
                        b.draft = false;

                        accounting.PostGenericDocument(AID, b);
                        bill.billDocumentTypeID = docType;
                        bill.connectionID = -1;
                        bill.periodID = -1;
                        bdesub.WriterHelper.InsertSingleTableRecord(bdesub.DBName, bill, new string[] { "__AID" }, new object[] { AID });
                        foreach (BillItem bi in b.items)
                        {
                            bi.customerBillID = bill.id;
                            bdesub.WriterHelper.InsertSingleTableRecord(bdesub.DBName, bi, new string[] { "__AID" }, new object[] { AID });
                        }
                        c--;
                        bde.WriterHelper.CommitTransaction();
                    }
                    catch (Exception ex)
                    {
                        bde.WriterHelper.RollBackTransaction();
                        Console.WriteLine(ex.Message);
                        Console.ReadLine();
                    }
                    finally
                    {
                        bdesub.WriterHelper.CheckTransactionIntegrity();
                    }
                }
                
            }
            
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
        [MaintenanceJobMethod("Dukem: delete incorret overdue bill")]
        public static void dukemDeleteIncorrectOverdueBills()
        {
            MessageRepository.dontDoDiff = true;


            Console.WriteLine("Fixing incorret overdue bill that was paid by accounting as deposit");
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            iERPTransactionBDE bde = ApplicationServer.GetBDE("iERP") as iERPTransactionBDE;
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;

            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            CustomerBillRecord[] bills = bdesub.WriterHelper.GetSTRArrayByFilter<CustomerBillRecord>("periodID not in (144,145)");
            bdesub.WriterHelper.restoreReadDB();            
            try
            {
                int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Maintenance Job: Dukem: delete incorret overdue bill", "", "", -1);
                Console.WriteLine(bills.Length + " bills found\nEnter to start deletion");
                Console.ReadLine();
                int c = bills.Length;
                List<int> paid = new List<int>();
                int docType = accounting.GetDocumentTypeByType(typeof(UnclassifiedPayment)).id;
                int delCount = 0;
                foreach (CustomerBillRecord bill in bills)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\t{1}                      ",c,delCount);
                    if (bill.paymentDocumentID == -1)
                    {
                        bdesub.WriterHelper.BeginTransaction();
                        try
                        {
                            bdesub.deleteCustomerBill(AID, bill.id);
                            delCount++;
                            bde.WriterHelper.CommitTransaction();
                        }
                        catch (Exception ex)
                        {
                            bde.WriterHelper.RollBackTransaction();
                            Console.WriteLine(ex.Message);
                            Console.ReadLine();
                        }
                        finally
                        {
                            bdesub.WriterHelper.CheckTransactionIntegrity();
                        }
                    }
                    c--;
                }
                
            }
            
            finally
            {
                MessageRepository.dontDoDiff = false;
            }
        }
    }
}
