using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;
using INTAPS.Payroll.BDE;
using INTAPS.Payroll;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using BIZNET.iERP.Server;
using BIZNET.iERP;
using INTAPS.SubscriberManagment.BDE;
using INTAPS.SubscriberManagment;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using INTAPS.RDBMS;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using INTAPS.WSIS.Job;



namespace INTAPS.ClientServer.RemottingServer
{

    public static partial class MaintainanceJob
    {

        [MaintenanceJobMethod("Accounting-hledger: build hledger")]
        public static void hledgerBuildHledger()
        {
            int N_DAYS1 = 50;
            Console.WriteLine("HLedger level 1 with NDays:{0}", N_DAYS1);
            AccountingBDE accounting = ApplicationServer.GetBDE("Accounting") as AccountingBDE;
            int currentAccountID = -1;
            int currentItemID = -1;
            double totalDebit0 = 0;
            double totalCredit0 = 0;
            double totalDebit1 = 0;
            double totalCredit1 = 0;
            long time = -1;

            DataTable insert0Table = new DataTable();

            insert0Table.Columns.Add("ID");
            insert0Table.Columns.Add("BatchID");
            insert0Table.Columns.Add("AccountID");
            insert0Table.Columns.Add("ItemID");
            insert0Table.Columns.Add("TransactionType");
            insert0Table.Columns.Add("OrderN");
            insert0Table.Columns.Add("tranTicks");
            insert0Table.Columns.Add("Amount");
            insert0Table.Columns.Add("TotalCrBefore");
            insert0Table.Columns.Add("TotalDbBefore");
            insert0Table.Columns.Add("Note");

            DataTable insert1Table = new DataTable();
            insert1Table.Columns.Add("AccountID");
            insert1Table.Columns.Add("ItemID");
            insert1Table.Columns.Add("tranTicks");
            insert1Table.Columns.Add("TotalCrBefore");
            insert1Table.Columns.Add("TotalDbBefore");

            HashSet<long> times = new HashSet<long>();

            SqlBulkCopy bk0 = new SqlBulkCopy(accounting.WriterHelper.getOpenConnection());
            bk0.DestinationTableName = "Accounting_2006.dbo.AccountTransaction0";
            SqlBulkCopy bk1 = new SqlBulkCopy(accounting.WriterHelper.getOpenConnection());
            bk1.DestinationTableName = "Accounting_2006.dbo.AccountTransaction1";

            int count = (int)accounting.WriterHelper.ExecuteScalar("Select count(*) from Accounting_2006.dbo.AccountTransaction");
            Console.Write("Ready to process {0} transaction. You can close this window if you don't want to continue", count);
            Console.ReadLine();

            Console.Write("Clearing existing data..");
            accounting.WriterHelper.ExecuteNonQuery("Delete from Accounting_2006.dbo.TimeSeries1");
            accounting.WriterHelper.ExecuteNonQuery("Delete from Accounting_2006.dbo.AccountTransaction0");
            accounting.WriterHelper.ExecuteNonQuery("Delete from Accounting_2006.dbo.AccountTransaction1");
            Console.WriteLine("done.");

            Console.Write("Loading transactions records to memory..");
            DataTable data= accounting.GetReaderHelper().GetDataTable("Select * from Accounting_2006.dbo.AccountTransaction order by accountID,itemID,orderN");
            Console.WriteLine("done.");

            Console.WriteLine();
            int days = 0;
            int ledger = 0;
            foreach (DataRow reader in data.Rows)
            {
                count--;
                if (count % 1000 == 0)
                {
                    Console.CursorTop--;
                    Console.WriteLine("{0}\tdays:{1}\tledger:{2}", count, days, ledger);
                }
                int ID = (int)reader["ID"];
                int BatchID = (int)reader["BatchID"];
                int AccountID = (int)reader["AccountID"];
                int ItemID = (int)reader["ItemID"];
                int TransactionType = (int)reader["TransactionType"];
                int OrderN = (int)reader["OrderN"];
                long tranTicks = (long)reader["tranTicks"];
                double Amount = (double)reader["Amount"];
                double TotalCrBefore = (double)reader["TotalCrBefore"];
                double TotalDbBefore = (double)reader["TotalDbBefore"];
                String Note = reader["Note"] as String;

                if (currentAccountID != AccountID || currentItemID != ItemID)
                {
                    
                    if (currentAccountID != -1)
                    {
                        totalDebit0 = 0;
                        totalCredit0 = 0;
                    }
                    currentAccountID = AccountID;
                    currentItemID = ItemID;
                    time = new DateTime(tranTicks).Date.AddDays(N_DAYS1).Ticks;
                    ledger++;
                }

                if (tranTicks >= time)
                {
                    DataRow row1=insert1Table.Rows.Add();
                    row1["AccountID"]= currentAccountID;
                    row1["ItemID"]= currentItemID;
                    row1["tranTicks"]= time;
                    row1["TotalDbBefore"]= totalDebit1;
                    row1["TotalCrBefore"]= totalCredit1;
                    if(insert1Table.Rows.Count>=10000)
                    {
                        bk1.WriteToServer(insert1Table);
                        insert1Table.Rows.Clear();
                        insert1Table.AcceptChanges();
                    }

                    days ++;
                    totalDebit0 = 0;
                    totalCredit0 = 0;
                    if (!times.Contains(time)) times.Add(time);
                    time = new DateTime(tranTicks).Date.AddDays(N_DAYS1).Ticks;
                }

                DataRow row=insert0Table.Rows.Add();
                row["ID"]= ID;
                row["BatchID"] = BatchID;
                row["AccountID"]= AccountID;
                row["ItemID"]= ItemID;
                row["TransactionType"]= TransactionType;
                row["OrderN"]= OrderN;
                row["tranTicks"]= tranTicks;
                row["Amount"]= Amount;
                row["TotalDbBefore"]= totalDebit0;
                row["TotalCrBefore"]= totalCredit0;
                row["Note"]= Note == null ? (object)DBNull.Value : Note;
                if (count % 10000 == 0)
                {
                    bk0.WriteToServer(insert0Table);
                    insert0Table.Rows.Clear();
                    insert0Table.AcceptChanges();
                }

                if (Amount > 0)
                {
                    totalDebit0 += Amount;
                    totalDebit1 += Amount;
                }
                else
                {
                    totalCredit0 -= Amount;
                    totalCredit1 -= Amount;
                }
            }
            Console.CursorTop--;
            Console.WriteLine("{0}\tdays:{1}\tledger:{2}", count, days, ledger);
            //write any remaining data
            if (insert0Table.Rows.Count>0)
                bk0.WriteToServer(insert0Table);
            if(insert1Table.Rows.Count>0)
                bk1.WriteToServer(insert1Table);
            Console.WriteLine("Dumping to TimeSeries1 table");
            foreach (long t in times)
                accounting.WriterHelper.Insert(accounting.DBName, "TimeSeries1", new string[] { "ticks" }, new object[] { t });
        }
    }
}