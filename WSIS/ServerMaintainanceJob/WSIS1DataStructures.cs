﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.RDBMS;
using System.Xml.Serialization;

namespace WSIS1
{
    [Serializable, SingleTableObject(orderBy = "paymentDate desc")]
    public class PrePaymentData
    {
        [IDField]
        public int subscriptionID;
        [IDField]
        public int paymentDocumentID;
        [DataField]
        public double cubicMeters;
        [DataField]
        public double paymentAmount;
        [DataField]
        public DateTime paymentDate;

        [XMLField]
        public int[] rentPeriods;
        [XMLField]
        public double[] rents;

    }
    [Serializable, SingleTableObject]
    public class Subscriber
    {
        [IDField]
        public int id;
        [DataField(false)]
        public string name;
        [DataField(false)]
        public INTAPS.SubscriberManagment.SubscriberType subscriberType;
        [DataField(false)]
        public int Kebele;
        [DataField]
        public string address;
        [DataField]
        public string amharicName;

        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }
    }
    [Serializable, XmlInclude(typeof(Subscriber)), SingleTableObject]
    public class Subscription
    {
        [IDField]
        public int id = -1;
        [DataField(false)]
        public int subscriberID = -1;
        [DataField]
        public string contractNo = null;
        [DataField(false)]
        public INTAPS.SubscriberManagment.SubscriptionType subscriptionType = INTAPS.SubscriberManagment.SubscriptionType.Unknown;
        [DataField(false)]
        public INTAPS.SubscriberManagment.SubscriptionStatus subscriptionStatus = INTAPS.SubscriberManagment.SubscriptionStatus.Unknown;
        [DataField(false)]
        public DateTime subscriptionStatusDate = DateTime.Now;
        [DataField(false)]
        public int meter = -1;
        [DataField(false)]
        public int Kebele = -1;
        [DataField]
        public string address = null;
        [DataField]
        public string landCertificateNo = null;
        [DataField]
        public int accountID = -1;
        public Subscriber subscriber;
        [DataField]
        public double initialMeterReading = 0;
        [DataField]
        public bool prePaid = false;
        [DataField]
        public string parcelNo = "";
        [DataField]
        public string phoneNo = "";
        [DataField]
        public string email = "";
        [DataField]
        public INTAPS.SubscriberManagment.ConnectionType connectionType = INTAPS.SubscriberManagment.ConnectionType.Unknown;
        [DataField]
        public INTAPS.SubscriberManagment.SupplyCondition supplyCondition = INTAPS.SubscriberManagment.SupplyCondition.Unknown;
        [DataField]
        public INTAPS.SubscriberManagment.DataStatus dataStatus = INTAPS.SubscriberManagment.DataStatus.Unknown;
        [DataField]
        public INTAPS.SubscriberManagment.MeterPositioning meterPosition = INTAPS.SubscriberManagment.MeterPositioning.Unknown;

        [DataField]
        public double waterMeterX = 0;
        [DataField]
        public double waterMeterY = 0;
        [DataField]
        public double houseConnectionX = 0;
        [DataField]
        public double houseConnectionY = 0;

        [DataField]
        public string remark = "";


        public int ID
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }
    }
    
    [Serializable]
    public class BillRecord
    {
        [IDField]
        public int subscriptionID;
        [IDField]
        public int periodID;
        [DataField]
        public int accountDocumentID = -1;
        [DataField(false)]
        public string billDocumentID;
        [DataField(false)]
        public int payCenterID;
        [DataField(false)]
        public DateTime payDocumentDate;
        [DataField(false)]
        public int payDocumentID = -1;
        [DataField]
        public DateTime postDate;

        public BillRecord()
        {
            this.payDocumentID = -1;
            this.postDate = DateTime.Now;
            this.payDocumentDate = DateTime.Now;
        }

        public virtual double Total
        {
            get
            {
                return 0.0;
            }
        }
    }

    [Serializable, SingleTableObject, STOInclude(typeof(BillRecord))]
    public class CreditBill : BillRecord
    {
        [DataField]
        public double returnAmount;

        public override double Total
        {
            get
            {
                return this.returnAmount;
            }
        }
    }
    [Serializable, SingleTableObject, STOInclude(typeof(BillRecord))]
    public class WaterBill : BillRecord
    {
        [DataField(false)]
        public double consumptionFee;
        [DataField(false)]
        public double fixedFee;

        public bool Payed
        {
            get
            {
                return (base.payDocumentID != -1);
            }
        }

        public override double Total
        {
            get
            {
                return (this.consumptionFee + this.fixedFee);
            }
        }
    }

    [Serializable, SingleTableObject, STOInclude(typeof(BillRecord))]
    public class LatePaymentBill : BillRecord
    {
        [DataField(false)]
        public double penality;

        public override double Total
        {
            get
            {
                return this.penality;
            }
        }
    }
    [Serializable, SingleTableObject]
    public class PaymentCenter
    {
        [IDField]
        public int id;
        [DataField(false)]
        public int casheir;
        [DataField(false)]
        public int casherAccountID;
        [DataField(false)]
        public string centerName;
        [DataField]
        public int summaryAccountID;
        [DataField(false)]
        public string userName;

        //public PaymentCenterSerialBatch[] serialBatches = new PaymentCenterSerialBatch[0];

        //public int GetBatchID(int doctTypeID)
        //{
        //    foreach (PaymentCenterSerialBatch batch in this.serialBatches)
        //    {
        //        if (batch.documentTypeID == doctTypeID)
        //        {
        //            return batch.batchID;
        //        }
        //    }
        //    return -1;
        //}

        public override string ToString()
        {
            return this.centerName;
        }
    }

    [Serializable, SingleTableObject]
    public class AccountBalance 
    {
        [IDField]
        public int AccountID = -1;
        [IDField]
        public int ItemID = -1;
        [DataField]
        public int LastOrderN;
        [DataField]
        public DateTime LastTransactionDate;
        [DataField]
        public double TotalCredit = 0.0;
        [DataField]
        public double TotalDebit = 0.0;

        public AccountBalance()
        {
        }
        public AccountBalance(int resAccountID, int resItemID)
        {
            // TODO: Complete member initialization
            this.AccountID = resAccountID;
            this.ItemID = resItemID;
        }

        public void AddBalance(AccountBalance bal)
        {
            this.TotalDebit += bal.TotalDebit;
            this.TotalCredit += bal.TotalCredit;
        }

        public static ulong Combine(int AccountID, int ItemID)
        {
            ulong num = (ulong)ItemID;
            return ((((ulong)AccountID) << 0x20) | num);
        }

        public static void Split(ulong ItemAccountID, out int AccountID, out int ItemID)
        {
            AccountID = (int)(ItemAccountID >> 0x20);
            ItemID = (int)(ItemAccountID & 0xffffffffL);
        }

        public double Balance
        {
            get
            {
                return (this.TotalDebit - this.TotalCredit);
            }
        }

        public ulong ItemAccountID
        {
            get
            {
                ulong itemID = (ulong)this.ItemID;
                return ((((ulong)this.AccountID) << 0x20) | itemID);
            }
        }
    }
    [Serializable, SingleTableObject(orderBy = "readingBlockID,orderN")]
    public class BWFMeterReading
    {
        [IDField]
        public int readingBlockID = -1;
        [IDField]
        public int subscriptionID = -1;

        [DataField]
        public int averageMonths = 0;
        [DataField(false)]
        public int billDocumentID = -1;
        [DataField]
        public INTAPS.SubscriberManagment.BWFStatus bwfStatus = INTAPS.SubscriberManagment.BWFStatus.Unread;
        [DataField(false)]
        public double consumption = 0.0;
        [DataField(false)]
        public DateTime entryDate = DateTime.Now;
        [DataField(false)]
        public int orderN = -1;
        [DataField]
        public int periodID;
        [DataField(false)]
        public double reading = 0.0;

        [DataField(false)]
        public INTAPS.SubscriberManagment.MeterReadingType readingType = INTAPS.SubscriberManagment.MeterReadingType.Normal;


        public string getRangeString()
        {
            switch (this.readingType)
            {
                case INTAPS.SubscriberManagment.MeterReadingType.Normal:
                case INTAPS.SubscriberManagment.MeterReadingType.Average:
                    return string.Concat(new object[] { this.consumption, " meter cube (", (this.reading - this.consumption).ToString("0"), " to ", this.reading.ToString("0"), " )" });
            }
            return (this.consumption + " meter cube ");
        }
    }
}
