﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using INTAPS.Accounting.Client;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Client;
using INTAPS.SubscriberManagment.Client;

namespace INTAPS.WSIS.Job.Shashemene.REClient
{
    public partial class CustomerSponsoredExpansionForm: NewApplicationForm
    {
        int _jobID;
        JobData _job;
        ApplicationPlaceHolder applicationPlaceHolder;
        Subscriber _customer = null;
        public CustomerSponsoredExpansionForm(int jobID, Subscriber customer)
        {
            InitializeComponent();
            applicationPlaceHolder = new ApplicationPlaceHolder();
            this.Controls.Add(applicationPlaceHolder);
            applicationPlaceHolder.Dock = DockStyle.Fill;

            _customer = customer;
            
            _jobID = jobID;
            if (_jobID == -1)
            {
                _job = null;
                if (customer == null)
                    applicationPlaceHolder.setCustomer(null, true);
                else
                {
                    applicationPlaceHolder.setCustomer(customer, false);
                    applicationPlaceHolder.Enabled = false;
                }
            }
            else
            {
                JobData job = JobManagerClient.GetJob(_jobID);
                _job = job;
                if(job.customerID==-1)
                    applicationPlaceHolder.setCustomer(job.newCustomer, true);
                else
                    applicationPlaceHolder.setCustomer(SubscriberManagmentClient.GetSubscriber(job.customerID), false);
            }      
        }
        private void buttonOk_Click(object sender, EventArgs e)
        {
            try
            {
                JobData app;
                bool newJob;
                newJob = (_job == null);
                if (newJob)
                {
                    app = new JobData();
                    app.applicationType = StandardJobTypes.CUSTOMER_SPONSORED_NETWORK_WORK;
                }
                else
                    app = _job;
                if (applicationPlaceHolder.isNewCustomerMode())
                {
                    if (!applicationPlaceHolder.validateCustomerData())
                        return;
                    app.newCustomer = applicationPlaceHolder.getCustomer();
                }
                else
                    app.customerID = _customer.id;



                if (newJob)
                {
                    app.id = JobManagerClient.AddJob(app, null);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully created");
                }
                else
                {
                    JobManagerClient.UpdateJob(app, null);
                    INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserMessage("Job successfully updated");
                }
                _job = app;
                createJobID = app.id;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.HandleException("Could not save application data", ex);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    
}
