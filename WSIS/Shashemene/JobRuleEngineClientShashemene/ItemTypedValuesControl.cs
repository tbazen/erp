﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace INTAPS.WSIS.Job.Shashemene.REClient
{
    public partial class ItemTypedValuesControl : UserControl
    {
        
        public ItemTypedValuesControl()
        {
            InitializeComponent();
        }
        public event EventHandler changed = null;
        void OnChanged()
        {
            if (changed != null)
            changed(this, null);
        }
        public ShashemeneEstimationConfiguration.ItemTypedValue values
        {
            get
            {
                ShashemeneEstimationConfiguration.ItemTypedValue ret = new ShashemeneEstimationConfiguration.ItemTypedValue();
                ret.privateItemCode = itemPrivate.GetObjectID();
                ret.institutionalItemCode = itemInstitutional.GetObjectID();
                ret.otherItemCode = itemOther.GetObjectID();

                if (!double.TryParse(textPrivateValue.Text, out ret.privateValue))
                    ret.privateValue = 0;

                if (!double.TryParse(textInstitutionalValue.Text, out ret.institutionalValue))
                    ret.institutionalValue = 0;

                if (!double.TryParse(textOtherValue.Text, out ret.otherValue))
                    ret.otherValue = 0;
                return ret;
            }
            set
            {
                if (value == null)
                {
                    itemPrivate.SetByID(null);
                    itemInstitutional.SetByID(null);
                    itemOther.SetByID(null);
                    textPrivateValue.Text = "";
                    textInstitutionalValue.Text = "";
                    textOtherValue.Text = "";
                    return;
                }

                itemPrivate.SetByID(value.privateItemCode);
                itemInstitutional.SetByID(value.institutionalItemCode);
                itemOther.SetByID(value.otherItemCode);

                if (value.privateValue == 0)
                    textPrivateValue.Text = "";
                else
                    textPrivateValue.Text = value.privateValue.ToString("#,#0.00");
                if (value.institutionalValue == 0)
                    textInstitutionalValue.Text = "";
                else
                    textInstitutionalValue.Text = value.institutionalValue.ToString("#,#0.00");
                if (value.otherValue == 0)
                    textOtherValue.Text = "";
                else
                    textOtherValue.Text = value.otherValue.ToString("#,#0.00");
            }
        }

        private void itemPrivate_ObjectChanged(object sender, EventArgs e)
        {
            OnChanged();
        }

        private void textValueChanged(object sender, EventArgs e)
        {
            OnChanged();
        }
    }
}
