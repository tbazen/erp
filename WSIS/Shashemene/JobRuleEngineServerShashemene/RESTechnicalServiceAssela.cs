using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using INTAPS.SubscriberManagment;
using INTAPS.WSIS.Job.Shashemene;
namespace INTAPS.WSIS.Job.Shashemene.REServer
{
    public abstract class RESTechnicalServiceAssela<DataType> : RESBaseAssela where DataType:WorkFlowData, new()
    {
        public RESTechnicalServiceAssela(JobManagerBDE bde)
            : base(bde)
        {
        }
        public override JobBillDocument generateInvoice(int AID, Subscriber customer, JobData job, JobBillOfMaterial billOfMaterial)
        {
            if (AID != -1 && job.applicationType == StandardJobTypes.NEW_LINE)
            {
                lock (bde.WriterHelper)
                {
                    bde.WriterHelper.BeginTransaction();
                    try
                    {

                        customer.depositAccountID = bde.subscriberBDE.getOrCreateCustomerAccountID(AID, customer, true);
                        JobBillDocument ret = base.generateInvoice(AID, customer, job, billOfMaterial);
                        bde.WriterHelper.CommitTransaction();
                        return ret;
                    }
                    catch
                    {
                        bde.WriterHelper.RollBackTransaction();
                        throw;
                    }
                }
            }

            return base.generateInvoice(AID, customer, job, billOfMaterial);
        }
        public virtual void createApplication(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data, string note)
        {
            bde.verifyAccountingClearance(AID, job.customerID, date, job);
            DataType newLine = data as DataType;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public virtual void updateApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DataType newLine = data as DataType;
            if (newLine != null)
                addApplicationData(AID, date, job, worker, data);
        }
        public virtual void addApplicationData(int AID, DateTime date, JobData job, JobWorker worker, WorkFlowData data)
        {
            DataType wfData = data as DataType;
            if (wfData == null)
                throw new ServerUserMessage("Empty data not allowed");
            wfData.typeID = getTypeID();
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION:
                case StandardJobStatus.SURVEY:
                case StandardJobStatus.ANALYSIS:
                case StandardJobStatus.FINALIZATION:
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to modify new line data at this stage");
            }
            data.setID(job.id, base.getTypeID());
            bde.setWorkFlowData(AID, data);
        }
        public abstract int[] getPossibleNextState(DateTime date, JobWorker worker, JobData job);
        internal static List<JobBillOfMaterial> getNoneApplicationBom(JobManagerBDE bde, ShashemeneEstimationConfiguration config, int jobID)
        {
            List<JobBillOfMaterial> boms = new List<JobBillOfMaterial>(bde.GetJobBOM(jobID));
            foreach (JobBillOfMaterial b in boms)
            {
                if (isApplicationBOM(config, b))
                {
                    boms.Remove(b);
                    break;
                }
            }
            return boms;
        }

        private static bool isApplicationBOM(ShashemeneEstimationConfiguration config, JobBillOfMaterial b)
        {
            foreach (ShashemeneEstimationConfiguration.ItemTypedValue val in new ShashemeneEstimationConfiguration.ItemTypedValue[]
                {
                    config.estimationFee,
                    config.contractCardFee,
                    config.fileFolderFee,
                    config.formFee,
                    config.permissionFee,
                }
            )
            {
                foreach (JobBOMItem item in b.items)
                {
                    if (item.itemID.Equals(val.institutionalItemCode)
                        || item.itemID.Equals(val.otherItemCode)
                        || item.itemID.Equals(val.privateItemCode)
                        )
                        return true;
                }
            }
            return false;
        }

        protected int _ignoreAID = -1;
        protected abstract void onForwardToApplicationPaymet(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, ShashemeneEstimationConfiguration config);
        protected virtual void onForwardToAnalysis(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, ShashemeneEstimationConfiguration config)
        {
            
        }
        public override void changeState(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note)
        {
            base.changeState(AID, date, worker, job, nextState, note);
            switch (job.status)
            {
                case StandardJobStatus.APPLICATION_APPROVAL:
                    if (nextState != StandardJobStatus.CANCELED)
                    {
                        if (job.newCustomer != null)
                            bde.registerNewCustomer(AID, job, date);
                    }
                    break;
                case StandardJobStatus.APPLICATION_PAYMENT:
                    break;
            }
            ShashemeneEstimationConfiguration config = bde.getConfiguration<ShashemeneEstimationConfiguration>(StandardJobTypes.NEW_LINE);
            if (config == null)
                throw new ServerUserMessage("Configure estimation parameters");
            switch (nextState)
            {
                case StandardJobStatus.APPLICATION_PAYMENT:
                    foreach (JobBillOfMaterial b in bde.GetJobBOM(job.id))
                            bde.DeleteBOM(AID, b.id, true);
                    onForwardToApplicationPaymet(AID, date, worker, job, nextState, note,config);
                    break;
                case StandardJobStatus.SURVEY:
                    JobBillOfMaterial[] thisCheckBom;
                    if (job.status == StandardJobStatus.APPLICATION_PAYMENT)
                        base.checkPayment(job.id, out thisCheckBom);
                    List<JobBillOfMaterial> thisBoms = resetWorkBOM(job, config);
                    if (thisBoms.Count > 0)
                    {
                        _ignoreAID = AID;
                        if (thisBoms[0].invoiceNo > 0)
                            bde.subscriberBDE.deleteCustomerBill(AID, thisBoms[0].invoiceNo);
                        thisBoms[0].invoiceNo = -1;
                        bde.SetBillofMateial(AID, thisBoms[0], true);
                    }
                    
                    break;
                case StandardJobStatus.ANALYSIS:
                    onForwardToAnalysis(AID, date, worker, job, nextState, note,config);
                    break;
                case StandardJobStatus.CONTRACT:
                    JobBillOfMaterial[] checkBom;
                    base.checkPayment(job.id, out checkBom);
                    break;
                case StandardJobStatus.WORK_PAYMENT:
                case StandardJobStatus.WORK_APPROVAL:
                    bde.verifyAccountingClearance(AID, job.customerID, date, job);
                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        if (bom.invoiceNo < 1)
                            bde.generateInvoice(AID, bom.id, worker);
                        foreach (JobBOMItem item in bom.items)
                            if (!item.inSourced && item.referenceUnitPrice == -1)
                                throw new ServerUserMessage("Customer reference price are not entered");
                    }
                    break;
                case StandardJobStatus.TECHNICAL_WORK:
                    checkPayment(job.id);
                    break;
                case StandardJobStatus.FINALIZATION:
                    onForwardToFinalization(AID, date, worker, job, nextState, note, config);
                    break;
                case StandardJobStatus.STORE_ISSUE:
                    bool hasMaterial = false;
                    foreach (JobBillOfMaterial bom in bde.GetJobBOM(job.id))
                    {
                        if (bom.storeIssueID != -1)
                            continue;
                        foreach (JobBOMItem item in bom.items)
                            if (item.inSourced && item.quantity > 0)
                            {
                                hasMaterial = true;
                                break;
                            }
                        if (hasMaterial)
                            break;
                    }
                    if (hasMaterial)
                        throw new ServerUserMessage("No material to issue");
                    break;
            }
        }

        protected abstract void onForwardToFinalization(int AID, DateTime date, JobWorker worker, JobData job, int nextState, string note, ShashemeneEstimationConfiguration config);

        private List<JobBillOfMaterial> resetWorkBOM(JobData job, ShashemeneEstimationConfiguration config)
        {
            List<JobBillOfMaterial> thisBoms = getNoneApplicationBom(bde, config, job.id);
            if (thisBoms.Count == 1)
            {
                List<JobBOMItem> newItems = new List<JobBOMItem>();
                BIZNET.iERP.TransactionItems[] allowedItems = bde.expandItemsList(config.surveyItems);
                foreach (JobBOMItem item in thisBoms[0].items)
                {
                    if (bde.contains(allowedItems, item.itemID) && !item.calculated)
                    {
                        item.referenceUnitPrice = -1;
                        newItems.Add(item);
                    }
                }
                thisBoms[0].items = newItems.ToArray();
            }
            return thisBoms;
        }

        protected DataType checkAndGetData(JobData job)
        {
            DataType data = bde.getWorkFlowData(job.id, getTypeID(), 0, true) as DataType;
            if (data == null)
                throw new ServerUserMessage("Data is not completed");
            return data;
        }
        public virtual void finishJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            
        }
        protected override void cancelJob(int AID, DateTime date, JobWorker worker, JobData job, string note)
        {
            JobBillOfMaterial[] bom = bde.GetJobBOM(job.id);
            lock (bde.WriterHelper)
            {
                bde.WriterHelper.BeginTransaction();
                try
                {
                    ShashemeneEstimationConfiguration config = bde.getConfiguration<ShashemeneEstimationConfiguration>(StandardJobTypes.NEW_LINE);

                    foreach (JobBillOfMaterial b in bom)
                    {
                        if (isApplicationBOM(config, b))
                            continue;

                        if (b.invoiceNo != -1)
                        {
                            if (!bde.subscriberBDE.isBillPaid(b.invoiceNo))
                            {
                                bde.subscriberBDE.deleteCustomerBill(AID, b.invoiceNo);
                            }
                        }
                    }
                    bde.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bde.WriterHelper.RollBackTransaction();
                    throw;
                }

            }
        }
        public override void onBOMSet(int AID, JobData job, JobBillOfMaterial bom)
        {
            if (AID == _ignoreAID)
                return;
            switch (job.status)
            {
                case StandardJobStatus.SURVEY:
                    INTAPS.WSIS.Job.Shashemene.ShashemeneEstimationConfiguration config = bde.getConfiguration(StandardJobTypes.NEW_LINE, typeof(ShashemeneEstimationConfiguration)) as ShashemeneEstimationConfiguration;
                    if (config == null)
                        throw new ServerUserMessage("Estimation configuration is not set");
                    Subscriber customer = bde.getJobCustomer(job);
                    foreach (JobBOMItem item in bom.items)
                    {
                        double price;
                        if (config.isFixedItemCode(item.itemID, customer.subscriberType, 1, out price))
                        {
                            throw new ServerUserMessage("Item {0} can't be entered manually", item.itemID);
                        }
                        item.referenceUnitPrice = -1;
                    }
                    break;
                case StandardJobStatus.ANALYSIS:
                    if (bom.id == -1)
                        throw new ServerUserMessage("It is not allowed to add new bill of quantities at this stage");
                    JobBillOfMaterial oldBom = bde.GetBillOfMaterial(bom.id);
                    foreach (JobBOMItem oldItem in oldBom.items)
                    {
                        if (!oldItem.inSourced)
                        {
                            bool found = false;
                            foreach (JobBOMItem newItem in bom.items)
                            {
                                if (newItem.referenceUnitPrice >= 0 && newItem.itemID.Equals(oldItem.itemID, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    found = true;
                                    oldItem.referenceUnitPrice = newItem.referenceUnitPrice;
                                }
                            }
                            if (!found)
                                throw new ServerUserMessage("Customer price not set to {0}", oldItem.itemID);
                        }
                    }
                    bom.invoiceDate = oldBom.invoiceDate;
                    bom.invoiceNo = oldBom.invoiceNo;
                    bom.items = oldBom.items;
                    bom.jobID = oldBom.jobID;
                    bom.storeIssueID = oldBom.storeIssueID;
                    break;
                default:
                    throw new ServerUserMessage("It is not allowed to edit bill of quantities at this stage");
            }
        }
    }
}
