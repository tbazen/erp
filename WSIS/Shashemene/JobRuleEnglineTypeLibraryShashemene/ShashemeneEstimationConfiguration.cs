﻿using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.WSIS.Job.Shashemene
{
    [Serializable]
    public class ShashemeneEstimationConfiguration
    {
        [Serializable]
        public class ItemTypedValue
        {
            public string privateItemCode = null;
            public string institutionalItemCode = null;
            public string otherItemCode = null;
            public double privateValue = 0;
            public double institutionalValue = 0;
            public double otherValue;
            public string getItemCode(SubscriberManagment.SubscriberType customerType)
            {
                switch (customerType)
                {
                    case INTAPS.SubscriberManagment.SubscriberType.Private:
                        return this.privateItemCode;
                    case INTAPS.SubscriberManagment.SubscriberType.CommercialInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Community:
                    case INTAPS.SubscriberManagment.SubscriberType.GovernmentInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Industry:
                    case INTAPS.SubscriberManagment.SubscriberType.NGO:
                    case INTAPS.SubscriberManagment.SubscriberType.Other:
                    case INTAPS.SubscriberManagment.SubscriberType.RelegiousInstitution:
                        return this.institutionalItemCode;
                    case INTAPS.SubscriberManagment.SubscriberType.Unknown:
                        return this.institutionalItemCode;
                    default:
                        return this.institutionalItemCode;
                }
            }
            public double getValue(SubscriberManagment.SubscriberType customerType)
            {
                switch (customerType)
                {
                    case INTAPS.SubscriberManagment.SubscriberType.Private:
                        return this.privateValue;
                    case INTAPS.SubscriberManagment.SubscriberType.CommercialInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Community:
                    case INTAPS.SubscriberManagment.SubscriberType.GovernmentInstitution:
                    case INTAPS.SubscriberManagment.SubscriberType.Industry:
                    case INTAPS.SubscriberManagment.SubscriberType.NGO:
                    case INTAPS.SubscriberManagment.SubscriberType.Other:
                    case INTAPS.SubscriberManagment.SubscriberType.RelegiousInstitution:
                        return this.institutionalValue;
                    case INTAPS.SubscriberManagment.SubscriberType.Unknown:
                        return this.institutionalValue;
                    default:
                        return this.institutionalValue;
                }
            }
        }
        public ItemsListItem[] pipelineItems = new ItemsListItem[0];
        public ItemsListItem[] hdpPipelineItems = new ItemsListItem[0];
        public ItemsListItem[] waterMeterItems = new ItemsListItem[0];
        public ItemsListItem[] surveyItems= new ItemsListItem[0];

        public ItemTypedValue[] meterDeposit = new ItemTypedValue[0];
        public ItemTypedValue[] meterServiceValues = new ItemTypedValue[0];

        public ItemTypedValue materialProfitMargin = new ItemTypedValue();
        public ItemTypedValue technicalServiceMultiplier = new ItemTypedValue();
        public ItemTypedValue technicalServiceMultiplierTeir1 = new ItemTypedValue();
        public ItemTypedValue technicalServiceMultiplierTeir2 = new ItemTypedValue();
        public ItemTypedValue technicalServiceMultiplierTeir3 = new ItemTypedValue();
        public ItemTypedValue serviceChargeMultiplier= new ItemTypedValue();

        public ItemTypedValue formFee = new ItemTypedValue();
        public ItemTypedValue estimationFee= new ItemTypedValue();
        public ItemTypedValue transitFee = new ItemTypedValue();
        public ItemTypedValue fileFolderFee= new ItemTypedValue();
        public ItemTypedValue contractCardFee = new ItemTypedValue();
        public ItemTypedValue permissionFee = new ItemTypedValue();

        public ItemTypedValue ownershipTransferFee = new ItemTypedValue();
        public string smallGuage="101023001";

        public bool isFixedItemCode(string code, SubscriberManagment.SubscriberType type, int incedence, out double value)
        {
            foreach (System.Reflection.FieldInfo fi in typeof(ShashemeneEstimationConfiguration).GetFields())
            {
                object val = fi.GetValue(this);

                if (val is ItemTypedValue)
                {
                    ItemTypedValue setting = (ItemTypedValue)val;
                    foreach (string thisCode in new string[] { setting.institutionalItemCode, setting.otherItemCode, setting.privateItemCode })
                        if (code.Equals(thisCode, StringComparison.CurrentCultureIgnoreCase))
                        {
                            value = setting.getValue(type);
                            return true;
                        }
                }
            }
            value = 0;
            return false;
        }

        public ShashemeneEstimationConfiguration.ItemTypedValue[] getNewLineFixedItems()
        {
            return new ShashemeneEstimationConfiguration.ItemTypedValue[]
                {
                        this.permissionFee,
                        this.contractCardFee,
                    
                };

        }
    }
}