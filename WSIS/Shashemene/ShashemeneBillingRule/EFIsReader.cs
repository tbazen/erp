﻿using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll.BDE;
using INTAPS.SubscriberManagment.BDE;

namespace INTAPS.SubscriberManagment.Shashemene.Server
{
    internal class EFIsReader : EFBase
    {
        SubscriberManagmentBDE _subsc;
        SubscriberManagmentBDE subscBDE
        {
            get
            {
                if(_subsc==null)
                    _subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _subsc;
            }
        }
        public EFIsReader(PayrollBDE bde) : base(bde)
        {
            
        }

        public override string Symbol => "IsReader";

        public override int ParCount => 2;

        public override EData Evaluate(EData[] Pars)
        {
            int periodID, employeeID;
            if (Pars[0].Value is int)
                employeeID = (int)Pars[0].Value;
            else
                return new FSError("IsReader - first parameter should be integer").ToEData();

            if (Pars[1].Value is int)
                periodID = (int)Pars[1].Value;
            else
                return new FSError("IsReader - second parameter should be integer").ToEData();
            
            int billPeriodID= subscBDE.getPeriodForDate(base.m_payroll.GetPayPeriod(periodID).fromDate).id;
            return new EData(subscBDE.GetMeterReaderEmployees(billPeriodID, employeeID) != null);
        }
    }

    internal class EFReadMeters : EFBase
    {
        SubscriberManagmentBDE _subsc;
        SubscriberManagmentBDE subscBDE
        {
            get
            {
                if (_subsc == null)
                    _subsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _subsc;
            }
        }
        public EFReadMeters(PayrollBDE bde) : base(bde)
        {
            
        }

        public override string Symbol => "MeterReadCount";

        public override int ParCount => 2;

        public override EData Evaluate(EData[] Pars)
        {
            int periodID, employeeID;
            if (Pars[0].Value is int)
                employeeID = (int)Pars[0].Value;
            else
                return new FSError("IsReader - first parameter should be integer").ToEData();

            if (Pars[1].Value is int)
                periodID = (int)Pars[1].Value;
            else
                return new FSError("IsReader - second parameter should be integer").ToEData();

            int billPeriodID = subscBDE.getPeriodForDate(base.m_payroll.GetPayPeriod(periodID).fromDate).id;
            if(subscBDE.GetMeterReaderEmployees(billPeriodID, employeeID)==null)
            {
                return new EData(0);
            }
            else
            {
                INTAPS.RDBMS.SQLHelper reader= subscBDE.GetReaderHelper();
                try
                {
                    string sql = string.Format("Select count(*) from {0}.dbo.BWFMeterReading where method=2 and bwfStatus=1 and periodID={1} and readingBlockID in (Select id from {0}.dbo.BWFReadingBlock where periodID={1} and readerID={2})"
                        , subscBDE.BDEName, billPeriodID, employeeID);
                    return new EData(
                        (double)(int)reader.ExecuteScalar(sql)
                    );
                }
                finally
                {
                    subscBDE.ReleaseHelper(reader);
                }
            }
        }
    }
}