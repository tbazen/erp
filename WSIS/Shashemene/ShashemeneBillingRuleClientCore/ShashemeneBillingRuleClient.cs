﻿using INTAPS.SubscriberManagment;
using INTAPS.SubscriberManagment.Client;
using System;
using System.Resources;

namespace ShashemeneBillingRuleClientCore
{
    public class ShashemeneBillingRuleClient : IBillingRuleClient
    {
        public ResourceManager getStringTable(int languageID) => StringTable_Or.ResourceManager;

        public void showSettingEditor(Type type, object setting)
        {
            throw new NotImplementedException();
        }

        public void printReceipt(CustomerPaymentReceipt receipt, PrintReceiptParameters pars, bool reprint, int nCopies)
        {
            if ("true".Equals(System.Configuration.ConfigurationManager.AppSettings["thermal"], StringComparison.CurrentCultureIgnoreCase))
            {
                INTAPS.UI.UIFormApplicationBase.CurrentAppliation.ShowUserWarning("Thermal printing");
                String ip = System.Configuration.ConfigurationManager.AppSettings["thermal_ip"];
                String port = System.Configuration.ConfigurationManager.AppSettings["thermal_port"];
                int portn;
                if (string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(port) || !int.TryParse(port, out portn))
                    throw new INTAPS.ClientServer.ServerUserMessage("Please confgure thermal printer ip address and port.");
                new ThermalReceiptPrinter(ip, portn).printNow(receipt, pars, reprint, nCopies);
            }
            else
                new ReceiptPrinter(receipt, pars).PrintNow(reprint, nCopies);
        }


    }
}
