﻿using System;
using System.Collections.Generic;

using System.Text;

namespace INTAPS.SubscriberManagment.Shashemene
{
  
    [Serializable]
    public class ShashemeneBillingRate
    {
        public int periodFrom=-1;
        public double fixedPenality = 30;
        public double proportionalPenality=0;

        public double calculate(double billAmount)
        {
            return fixedPenality + proportionalPenality * billAmount / 100;
        }
    }
}
