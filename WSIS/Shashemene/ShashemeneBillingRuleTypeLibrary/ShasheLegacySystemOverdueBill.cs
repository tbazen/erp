﻿using INTAPS.RDBMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace INTAPS.SubscriberManagment.Shashemene
{
    [TypeID(50001)]
    [Serializable]
    public class ShasheLegacySystemOverdueBill : INTAPS.SubscriberManagment.PeriodicBill
    {
        
        [INTAPS.RDBMS.SingleTableObject("billGenerate")]
        [Serializable]
        public class ReadingRecord
        {
            [DataField]
            public double penality;
            [DataField]
            public int prevNoMth;
            [DataField]
            public double prevMRate;
            [DataField]
            public double prevTariff;
            [DataField]
            public double prevPenality;
            [DataField]
            public double prevServiceC;
            [DataField]
            public double prevMiscCost;
            [DataField]
            public double prevSewerageD;
            [DataField]
            public String custID;
            [DataField]
            public int readingPrev;
            [DataField]
            public int readingCurrent;
            [DataField]
            public int readingCons;
            [DataField]
            public double currTariff;
            [DataField]
            public double serviceCharge;
            [DataField]
            public double meterRent;
        }
        public ReadingRecord record=null;
        public BillItem[] billItems=null;
        public override BillItem[] items   
        {
            get
            {
                return billItems;
            }
        }
        
    }
}
