﻿using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.SubscriberManagment.BDE;
using System;
using System.Collections.Generic;

namespace INTAPS.SubscriberManagment.Shashemene.Server
{
    public class ShashemeneMigrate
    {
        static DateTime yesterDay = DateTime.Now.AddDays(-1).Date;
        public static ShasheLegacySystemOverdueBill buildBill(SubscriberManagmentBDE bdesub, int docTypeID, BillPeriod period, ShasheLegacySystemOverdueBill.ReadingRecord record, out CustomerBillRecord billRec)
        {
            var connectionID = (int)(long.Parse(record.custID) - 90000000000);
                                                                 
            var connection = bdesub.GetSubscription(connectionID, yesterDay.Ticks);
            if (connection == null)
                throw new Exception($"Connection  not found in WSIS dastabase custID:{record.custID}");
            if (record.prevTariff + 
                record.prevMRate + 
                record.prevPenality + 
                record.prevServiceC+record.penality +
                record.meterRent+
                record.currTariff+
                record.serviceCharge                
                > 0)
            {
                SingeRateSetting[] rateSettings = bdesub.GetRates(connection, period);
                if (rateSettings.Length == 0)
                {
                    throw new Exception("No rate that applies to the subscriber found.");
                }
                int i;
                if (rateSettings.Length > 1)
                {
                    var msg = "More than one rates found: " + rateSettings[0].name;
                    for (i = 1; i < rateSettings.Length; i++)
                    {
                        msg = msg + "," + rateSettings[i].name;
                    }
                    throw new Exception(msg);
                }
                var rate = rateSettings[0];
                var billItems = new List<BillItem>();
                if (AccountBase.AmountGreater(record.prevTariff+record.currTariff, 0))
                {
                    billItems.Add(new BillItem()
                    {
                        accounting = BillItemAccounting.Invoice,
                        customerBillID = -1,
                        description = $"Kufaamaa Ittii Fayadamaa",
                        incomeAccountID = rate.mainAccountID == -1 ? bdesub.SysPars.incomeAccount : rate.mainAccountID,
                        itemTypeID = 1,
                        price = Math.Round(record.prevTariff + record.currTariff,2),
                        hasUnitPrice = false,
                        quantity = -1,
                        settledFromDepositAmount = 0,
                    });
                }
                if (AccountBase.AmountGreater(record.prevMRate+record.meterRent, 0))
                {
                    billItems.Add(new BillItem()
                    {
                        accounting = BillItemAccounting.Invoice,
                        customerBillID = -1,
                        description = $"Kufaamaa Kiraa Lakkoftuu",
                        incomeAccountID = rate.rentAccountID == -1 ? bdesub.SysPars.rentIncomeAccount : rate.rentAccountID,
                        itemTypeID = 2,
                        price = Math.Round(record.prevMRate + record.meterRent, 2),
                        hasUnitPrice = false,
                        quantity = -1,
                        settledFromDepositAmount = 0,
                    });
                }
                if (AccountBase.AmountGreater(record.prevServiceC+record.serviceCharge, 0))
                {
                    billItems.Add(new BillItem()
                    {
                        accounting = BillItemAccounting.Invoice,
                        customerBillID = -1,
                        description = $"Kafaltii Kufaamaa Tajajjilaa",
                        incomeAccountID = rate.additionalAccountID == -1 ? bdesub.SysPars.additionalFeeIncomeAcount : rate.additionalAccountID,
                        itemTypeID = 3,
                        price = Math.Round(record.prevServiceC + record.serviceCharge, 2),
                        hasUnitPrice = false,
                        quantity = -1,
                        settledFromDepositAmount = 0,
                    });
                }
                if (AccountBase.AmountGreater(record.prevPenality + record.penality, 0))
                {
                    billItems.Add(new BillItem()
                    {
                        accounting = BillItemAccounting.Invoice,
                        customerBillID = -1,
                        description = $"Adaabii",
                        incomeAccountID = rate.penalityAccountID == -1 ? bdesub.SysPars.billPenalityIncomeAccount : rate.penalityAccountID,
                        itemTypeID = 4,
                        price = Math.Round(record.prevPenality + record.penality,2),
                        hasUnitPrice = false,
                        quantity = -1,
                        settledFromDepositAmount = 0,
                    });
                }

                var bill = new ShasheLegacySystemOverdueBill();
                bill.DocumentDate = yesterDay;
                bill.ShortDescription = "Overdue bill transfered from old system";
                bill.customer = connection.subscriber;
                bill.period = period;

                bill.record = record;
                bill.billItems = billItems.ToArray();
                billRec = new CustomerBillRecord()
                {
                    billDate = bill.DocumentDate,
                    billDocumentTypeID = docTypeID,
                    connectionID = connection.id,
                    customerID = connection.subscriber.id,
                    draft = false,
                    paymentDiffered = false,
                    paymentDocumentID = -1,
                    periodID = period.id,
                    postDate = bill.DocumentDate,
                };

                return bill;
            }
            billRec = null;
            return null;
        }
        public static void ProcessReadingsAndOverdue()
        {
            SubscriberManagmentBDE bdesub = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
            bdesub.WriterHelper.setReadDB(bdesub.DBName);
            var period = bdesub.getPeriodForDate(DateTime.Parse("2018-12-11"));
            if (period == null)
                throw new Exception($"The billing period that covers the date 2018-12-11 not configured in WSIS");
            Console.WriteLine($"Generating bills for period {period.name}");
            var overdueBillDocType = bdesub.Accounting.GetDocumentTypeByTypeNoException(typeof(ShasheLegacySystemOverdueBill));
            if (overdueBillDocType == null)
                throw new Exception($"ShasheLegacySystemOverdueBill document type is not registered in WSIS");
            Console.WriteLine("Records for fiscalyear 11 and month 6 from old system");

            var records = bdesub.WriterHelper.GetSTRArrayBySQL<ShasheLegacySystemOverdueBill.ReadingRecord>(
                @"Select * from DB_CUSTOMER.dbo.billGenerate where fiscalYear=2011 and monthIndex=6
and billStatus<>'SOLD' and custID not in (Select custID from DB_CUSTOMER.dbo.billGenerate where fiscalYear=2011 and monthIndex=6
group by custID having COUNT(*)>1)");

            Console.WriteLine($"{records.Length} records loaded");
            int c = 0;
            Console.WriteLine();

            int AID = ApplicationServer.SecurityBDE.WriteAudit("admin", 1, "Migration of overdue bills from old system", "", "", -1);
            int batchCount = 0;
            List<int> batchItems = new List<int>();
            List<CustomerBillDocument> batchBills = new List<CustomerBillDocument>();
            TransactionSummerizer sum = null;
            sum = new TransactionSummerizer(bdesub.Accounting, bdesub.bERP.SysPars.summaryRoots);
            int nbills = 0;
            int nErrors = 0;
            foreach (var record in records)
            {
                c++;
                Console.CursorTop--;
                Console.WriteLine($"Processing Record {c} of {records.Length}. Batches Posted:{batchCount} Bills Found:{nbills} Errors: {nErrors}");
                bdesub.WriterHelper.BeginTransaction();
                try
                {
                    CustomerBillRecord billRec;
                    var bill = buildBill(bdesub, overdueBillDocType.id, period, record, out billRec);
                    if (bill != null)
                    {
                        nbills++;
                        bill.draft = false;
                        billRec.draft = false;
                        bill.summerizeTransaction = false;
                        bill.billBatchID = -1;
                        int id = bdesub.addBill(AID, bill, billRec, bill.billItems);
                        sum.addTransaction(AID, bdesub.Accounting.GetTransactionsOfDocument(id));
                        batchItems.Add(id);
                        batchBills.Add(bill);
                        if (batchBills.Count == bdesub.SysPars.billBatchSize)
                        {
                            CustomerBillSummaryDocument summaryBill = new CustomerBillSummaryDocument();
                            summaryBill.ShortDescription = string.Format("Bill Batch summary {0} - batch: {1}", period.name, batchCount + 1);
                            summaryBill.billIDs = batchItems.ToArray();
                            summaryBill.transactions = sum.getSummary();
                            summaryBill.DocumentDate = yesterDay;    
                            try
                            {
                                bdesub.Accounting.PostGenericDocument(AID, summaryBill);
                            }
                            catch (Exception sumEx)
                            {
                                throw new Exception("Error trying to post bill summary", sumEx);
                            }
                            batchCount++;
                            batchBills.Clear();
                            batchItems.Clear();
                            sum = new TransactionSummerizer(bdesub.Accounting, bdesub.bERP.SysPars.summaryRoots);
                        }
                    }
                    bdesub.WriterHelper.CommitTransaction();
                }
                catch (Exception ex)
                {
                    nErrors++;
                    bdesub.WriterHelper.RollBackTransaction();
                    printException(ex, $"Error processing record for custID:{record.custID}");
                    Console.WriteLine();
                }
            }
            if (batchBills.Count>0)
            {
                CustomerBillSummaryDocument summaryBill = new CustomerBillSummaryDocument();
                summaryBill.ShortDescription = string.Format("Bill Batch summary {0} - batch: {1}", period.name, batchCount + 1);
                summaryBill.billIDs = batchItems.ToArray();
                summaryBill.transactions = sum.getSummary();
                summaryBill.DocumentDate = yesterDay;
                try
                {
                    bdesub.Accounting.PostGenericDocument(AID, summaryBill);
                }
                catch (Exception sumEx)
                {
                    throw new Exception("Error trying to post bill summary", sumEx);
                }
                batchCount++;
            }
        }
        public static void DoJob(string args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.Yellow;
            try
            {
                Console.WriteLine("=======Shasheme Data Migration=======");
                Console.WriteLine("Press enter to start");
                Console.ReadLine();
                ProcessReadingsAndOverdue();
                Console.WriteLine("Data migration complete");
                Console.ReadLine();
            }
            catch (Exception bex)
            {
                printException(bex, "Data migration error");
                Console.ReadLine();
            }
        }

        private static void printException(Exception bex, String msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(msg);
            while (bex != null)
            {
                Console.WriteLine(bex.Message);
                Console.WriteLine(bex.StackTrace);
                bex = bex.InnerException;
            }
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.Yellow;
        }
    }
}
