using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Accounting;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{
    public class BillSettlementHandler : INTAPS.Accounting.IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if (_bdeSubsc == null)
                    _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public BillSettlementHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }
        public bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, UserSessionData userSession)
        {
            return true;
        }
        void deleteExisting(int AID, int docID, bool forUpdate)
        {
            CustomerPaymentReceipt settlment = _accounting.GetAccountDocument(docID, true) as CustomerPaymentReceipt;
            if (settlment == null)
                return;
            if (settlment.billBatchID != -1)
                throw new ServerUserMessage("This receipt can't be deleted because it is summerized");
            lock (bdeSubsc.WriterHelper)
            {
                bdeSubsc.WriterHelper.BeginTransaction();
                try
                {
                    CustomerBillRecord[] record = bdeSubsc.getBillRecordsByPayment(docID);
                    foreach (CustomerBillRecord r in record)
                    {
                        r.paymentDocumentID = -1;
                        bdeSubsc.WriterHelper.logDeletedData(AID, bdeSubsc.DBName + ".dbo.CustomerBill", "id=" + r.id);
                        bdeSubsc.WriterHelper.UpdateSingleTableRecord(bdeSubsc.DBName, r, new string[] { "__AID" }, new object[] { AID }, "id=" + r.id);
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(r.id, r));
                    }
                    foreach (int d in settlment.bankDepsitDocuments)
                        _accounting.DeleteAccountDocument(AID, d, forUpdate);
                    _accounting.DeleteAccountDocument(AID, docID, forUpdate);
                    //_accounting.undoVoidSerialNo
                    bdeSubsc.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeSubsc.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public void DeleteDocument(int AID, int docID)
        {
            deleteExisting(AID, docID, false);
        }

        public string GetHTML(Accounting.AccountDocument _doc)
        {
            CustomerPaymentReceipt settlement = (CustomerPaymentReceipt)_doc;
            StringBuilder table = new StringBuilder("<table>");
            table.Append("<tr><td>Item</td><td>Unit Price</td><td>Quanity</td><td>Price</td></tr>");
            foreach (int bid in settlement.settledBills)
            {
                foreach (BillItem item in bdeSubsc.getCustomerBillItems(bid))
                {
                    table.Append(string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>"
                        , System.Web.HttpUtility.HtmlEncode(item.description)
                        , item.hasUnitPrice ? System.Web.HttpUtility.HtmlEncode(item.unitPrice.ToString("#,#0.00")) : ""
                        , item.hasUnitPrice ? System.Web.HttpUtility.HtmlEncode(item.quantity.ToString("0.00") + " " + item.unit) : ""
                        , System.Web.HttpUtility.HtmlEncode(item.price.ToString("#,#0.00"))
                        ));
                }
            }
            table.Append("</table>");
            string html = string.Format("Customer Name: {0}<br/>Receipt No: {1}<br/>Items<br/>{2}", settlement.customer == null ? "" : settlement.customer.name, settlement.receiptNumber == null ? "" : settlement.receiptNumber.reference, table.ToString());
            return html;
        }

        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {

                AuditInfo ai = ApplicationServer.SecurityBDE.getAuditInfo(AID);

                PaymentCenter center = bdeSubsc.GetPaymentCenter(ai.userName);
                if (center == null)
                    throw new ServerUserMessage("Please login with the account of the payment center");

                CustomerPaymentReceipt settlment = (CustomerPaymentReceipt)_doc;
                Console.Write($"Posting settlment {(settlment.receiptNumber == null ? "<null>":settlment.receiptNumber.reference)}");
                if (settlment.AccountDocumentID != -1)
                    deleteExisting(AID, settlment.AccountDocumentID, true);

                if (settlment.assetAccountID != center.casherAccountID)
                    throw new ServerUserMessage("You must login with the account that made the sale");

                if (settlment.getCollectionMethod() != center.collectionMethod)
                    throw new ServerUserMessage("{0} is an online payment center is doesn't have permission for {1} payment collection", center.centerName, PaymentCenter.getCollectionMethodText(settlment.getCollectionMethod()));

                settlment.assetAccountID = center.casherAccountID;
                if (settlment.receiptNumber == null)
                {
                    UsedSerial sr = new UsedSerial();
                    sr.batchID = center.GetBatchID(_accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id);
                    sr.val = _accounting.GetNextSerial(sr.batchID);
                    sr.date = settlment.DocumentDate;
                    sr.isVoid = false;
                    sr.voidReason = VoidReasonType.Other;
                    sr.note = "Customer Payment";
                    _accounting.UseSerial(AID, sr);
                    DocumentSerialType st = _accounting.getDocumentSerialType("BSN");
                    if (st == null)
                        throw new ServerUserMessage("Configure BSN document reference type");
                    settlment.receiptNumber = new DocumentTypedReference(st.id, sr.val.ToString("000000000"), true);
                }
                else if (settlment.receiptNumber.typeID != _accounting.getDocumentSerialType("CRV").id)
                {
                    UsedSerial sr = new UsedSerial();
                    sr.batchID = center.GetBatchID(_accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id);
                    sr.val = int.Parse(settlment.receiptNumber.reference);
                    sr.date = settlment.DocumentDate;
                    sr.isVoid = false;
                    sr.voidReason = VoidReasonType.Other;
                    sr.note = "Customer Payment";
                    _accounting.UseSerial(AID, sr);
                }

                settlment.PaperRef = settlment.receiptNumber.reference;
                settlment.customer.accountID = bdeSubsc.getOrCreateCustomerAccountID(AID, settlment.customer);

                int csaIDCustomer = _accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, settlment.customer.accountID).id;
                double billPaymentTotal = 0;
                List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                List<CustomerBillRecord> settledBills = new List<CustomerBillRecord>();
                List<DocumentTypedReference> refs = new List<DocumentTypedReference>();
                List<CustomerBillDocument> billDocuments = new List<CustomerBillDocument>();
                refs.Add(settlment.receiptNumber);
                double totalCashSalesItem = 0;
                foreach (int billID in settlment.settledBills)
                {
                    CustomerBillDocument bill = _accounting.GetAccountDocument(billID, true) as CustomerBillDocument;
                    if (bill.customer.id != settlment.customer.id)
                        throw new ServerUserMessage("Only bills of a single customer can be settled with one receipt");

                    DocumentSerialType stBill = _accounting.getDocumentSerialType("BILL");
                    if (stBill == null)
                        throw new ServerUserMessage("Configure BILL document reference type");


                    CustomerBillRecord rec = _bdeSubsc.getCustomerBillRecord(bill.AccountDocumentID);
                    if (rec.paymentDocumentID != -1)
                        throw new ServerUserMessage("Bill already paid. ID:" + bill.AccountDocumentID);
                    foreach (BillItem it in _bdeSubsc.getCustomerBillItems(bill.AccountDocumentID))
                    {
                        if (!AccountBase.AmountEqual(it.netPayment, 0))
                        {
                            switch (it.accounting)
                            {
                                case BillItemAccounting.Cash:
                                    trans.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, it.incomeAccountID).id, -it.netPayment, it.description));
                                    totalCashSalesItem += it.netPayment;
                                    break;
                                default:
                                    break;
                            }
                            billPaymentTotal += it.netPayment;
                        }
                    }
                    settledBills.Add(rec);
                    billDocuments.Add(bill);
                }


                double totalInstruments = 0;
                double totalCash = 0;
                List<CustomerBankDepositDocument> deposits = new List<CustomerBankDepositDocument>();
                if (!AccountBase.AmountEqual(billPaymentTotal, 0))
                {
                    foreach (BIZNET.iERP.PaymentInstrumentItem item in settlment.paymentInstruments)
                    {
                        totalInstruments += item.amount;
                        if (item.despositToBankAccountID == -1)
                            totalCash += item.amount;
                        else
                        {
                            CustomerBankDepositDocument deposit = new CustomerBankDepositDocument();
                            deposit.voucher = settlment.receiptNumber;
                            deposit.amount = item.amount;
                            deposit.bankAccountID = item.despositToBankAccountID;
                            deposit.customer = settlment.customer;
                            deposit.ShortDescription = "Bank deposit by customer for settlement of bills";
                            deposits.Add(deposit);
                        }

                        TransactionItems titem = _bdeSubsc.bERP.GetTransactionItems(item.instrumentTypeItemCode);
                        if (titem == null)
                            throw new ServerUserMessage("Transaction item code is not valid for payment instrument");
                        if (!titem.IsInventoryItem)
                            throw new ServerUserMessage("Transaction item set for payment instrument " + item.instrumentTypeItemCode + " is not inventory item");
                        if (item.instrumentTypeItemCode.Equals(_bdeSubsc.bERP.SysPars.cashInstrumentCode))
                            continue;

                        trans.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, center.storeCostCenterID, titem.inventoryAccountID).id, TransactionItem.MATERIAL_QUANTITY, 1, ""));
                        trans.Add(new TransactionOfBatch(
                                _accounting.GetOrCreateCostCenterAccount(AID, _bdeSubsc.bERP.SysPars.mainCostCenterID, _bdeSubsc.bERP.SysPars.materialInFlowAccountID).id
                                , TransactionItem.MATERIAL_QUANTITY, -1, ""));
                    }
                    trans.Add(new TransactionOfBatch(csaIDCustomer, -totalCash + totalCashSalesItem));
                    trans.Add(new TransactionOfBatch(settlment.assetAccountID, totalCash));
                }
                if (!AccountBase.AmountEqual(billPaymentTotal, totalInstruments))
                {

                    throw new ServerUserMessage("Payment and bill amount are not equal for receipt no:" + settlment.receiptNumber.reference + ". This indicates a problem in the sales point client. Payment amount " + totalInstruments + " bill amount" + billPaymentTotal);
                }
                settlment.bankDepsitDocuments = new int[deposits.Count];
                for (int k = 0; k < settlment.bankDepsitDocuments.Length; k++)
                {
                    deposits[k].AccountDocumentID = _accounting.PostGenericDocument(AID, deposits[k]);
                    _accounting.addDocumentSerials(AID, deposits[k], refs.ToArray());
                    settlment.bankDepsitDocuments[k] = deposits[k].AccountDocumentID;
                }
                settlment.AccountDocumentID = _accounting.RecordTransaction(AID, settlment, trans.ToArray(), refs.ToArray());

                foreach (CustomerBillDocument b in billDocuments)
                {
                    _accounting.addDocumentSerials(AID, b, new DocumentTypedReference(settlment.receiptNumber.typeID, settlment.receiptNumber.reference, false));
                }

                foreach (CustomerBillRecord bill in settledBills)
                {
                    bdeSubsc.WriterHelper.logDeletedData(AID, bdeSubsc.DBName + ".dbo.CustomerBill", "id=" + bill.id);
                    bill.paymentDocumentID = settlment.AccountDocumentID;
                    bill.paymentDate = settlment.DocumentDate;

                    bdeSubsc.WriterHelper.UpdateSingleTableRecord(bdeSubsc.DBName, bill, new string[] { "__AID" }, new object[] { AID }, "id=" + bill.id);
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(bill.id, bill));

                    CustomerBillDocumentHandler h = _accounting.GetDocumentHandler(bill.billDocumentTypeID) as CustomerBillDocumentHandler;
                    if (h == null)
                        continue;
                    h.onSettled(AID, settlment);
                }
                ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(settlment.AccountDocumentID, settlment));
                try
                {
                    if (settlment.offline && settlment.mobile)
                    {
                        bdeSubsc.sendSMS(AID, false,new CustomerSMSSendParameter(){
                                sendType = CustomerSMSSendType.General,
                                passCode = bdeSubsc.SysPars.sms_passs_code,
                                manualSMS = new CustomerSMS[]{
                                    new CustomerSMS(){
                                        message="Dear {0} A payment of amount {1} for your water bill is recorded on the server.".format(settlment.customer.name,(totalInstruments + bdeSubsc.SysPars.commissionAmount-bdeSubsc.SysPars.commissionAmount_Utility_Share).ToString("#,#0.00")),
                                        phoneNo=settlment.customer.phoneNo,
                                        }
                                    },
                                });
                    }
                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException("Customer {0} couldn't be notifed about his bill settlment.".format(settlment.customer.customerCode), ex);
                }
                return settlment.AccountDocumentID;
            }
        }

        public void ReverseDocument(int AID, int docID)
        {
            throw new NotImplementedException();
        }
    }
}
