﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting;
using INTAPS.ClientServer;
using INTAPS.Accounting.BDE;

namespace INTAPS.SubscriberManagment.BDE
{
    public class CustomerBankDepositHandler:IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if (_bdeSubsc == null)
                    _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public CustomerBankDepositHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }
        public bool CheckPostPermission(int docID, AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return true;
        }

        public int Post(int AID, AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                try
                {
                    _accounting.WriterHelper.BeginTransaction();
                    CustomerBankDepositDocument deposit = (CustomerBankDepositDocument)_doc;
                    deposit.PaperRef = deposit.voucher.reference;
                    deposit.customer.accountID = bdeSubsc.getOrCreateCustomerAccountID(AID, deposit.customer);
                    int csaIDCustomer = _accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, deposit.customer.accountID).id;

                    List<TransactionOfBatch> trans = new List<TransactionOfBatch>();
                    trans.AddRange(new TransactionOfBatch[]{
                        new TransactionOfBatch(csaIDCustomer,-deposit.amount,"Customer Bank Desposit")
                        ,new TransactionOfBatch(deposit.bankAccountID,deposit.amount,"Customer Bank Desposit")
                        ,new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID,bdeSubsc.bERP.SysPars.mainCostCenterID, _accounting.GetAccountID<Account>("CUST-7000")).id,deposit.amount, "Customer Cash Deposit")
                    });
                    TransactionSummerizer sum = new TransactionSummerizer(_accounting, bdeSubsc.bERP.SysPars.summaryRoots);
                    sum.addTransaction(AID, new TransactionOfBatch(csaIDCustomer, -deposit.amount, "Customer Bank Desposit"));
                    trans.AddRange(sum.getSummary());
                    deposit.AccountDocumentID = _accounting.RecordTransaction(AID, deposit, trans.ToArray()
                        );
                    _accounting.WriterHelper.CommitTransaction();
                    return deposit.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public string GetHTML(AccountDocument _doc)
        {
            return "Customer Bank Desposit";
        }

        public void DeleteDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Customer Bankd Desposit Can't be Deleted Alone");
        }

        public void ReverseDocument(int AID, int docID)
        {
            throw new NotImplementedException();
        }
    }
}
