﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.BDE
{
    public class CustomerBillDocumentHandler:INTAPS.Accounting.IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if(_bdeSubsc==null)
                _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public CustomerBillDocumentHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }

        public virtual bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return false;
        }

        void deleteExisting(int AID, int docID, bool forUpdate)
        {
            CustomerBillRecord rec = bdeSubsc.getCustomerBillRecord(docID);
            CustomerBillDocument billDoc = _accounting.GetAccountDocument(docID, true) as CustomerBillDocument;
            if (billDoc == null)
                return;
            lock (bdeSubsc.WriterHelper)
            {

                bdeSubsc.WriterHelper.BeginTransaction();
                try
                {
                    if (billDoc.billBatchID == -1)
                    {
                        _accounting.DeleteAccountDocument(AID, docID, forUpdate);
                        if (!forUpdate)
                        {
                            if (billDoc.invoiceNumber != null)
                            {
                                this._accounting.voidSerialNo(AID, DateTime.Now,
                                billDoc.invoiceNumber, "Delete bill");
                            }
                            bdeSubsc.deleteCustomerBillRecord(AID, docID);
                            ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(_accounting.GetDocumentTypeByID(rec.billDocumentTypeID).GetTypeObject(), docID));
                        }
                    }
                    else
                        throw new ServerUserMessage("It is not possible to delete a bill that is part of a batch. You bill reverse mechanism");

                    bdeSubsc.WriterHelper.CommitTransaction();
                }
                catch
                {
                    bdeSubsc.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }

        public virtual void DeleteDocument(int AID, int docID)
        {
            deleteExisting(AID, docID, false);
        }
        public string GetHTML(Accounting.AccountDocument _doc)
        {
            CustomerBillDocument cb = (CustomerBillDocument)_doc;
            return cb.BuildHTML();
        }
        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    CustomerBillDocument cb = (CustomerBillDocument)_doc;
                    if (cb.invoiceNumber == null)
                    {
                        DocumentSerialType st= _accounting.getDocumentSerialType("BILL");
                        if (st == null)
                            throw new ServerUserMessage("Configure BILL document reference type");
                        cb.invoiceNumber=_accounting.getNextTypedReference(st.id, _doc.DocumentDate);
                    }
                    cb.PaperRef = cb.invoiceNumber.reference;
                    if (cb.AccountDocumentID != -1)
                        deleteExisting(AID, cb.AccountDocumentID,true);
                    if (cb.draft)
                        cb.AccountDocumentID = _accounting.RecordTransaction(AID, cb, new TransactionOfBatch[0], cb.invoiceNumber);
                    else
                    {
                        cb.customer.accountID = bdeSubsc.getOrCreateCustomerAccountID(AID, cb.customer);
                        int custCSID = _accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, cb.customer.accountID).id;

                        List<TransactionOfBatch> tran = new List<TransactionOfBatch>();
                        double total = 0;
                        foreach (BillItem bi in cb.itemsLessExemption)// _bdeSubsc.getCustomerBillItems(cb.AccountDocumentID))
                        {
                            if (!AccountBase.AmountEqual(bi.price, 0) && bi.accounting== BillItemAccounting.Invoice)
                            {
                                tran.Add(new TransactionOfBatch(_accounting.GetOrCreateCostCenterAccount(AID, bdeSubsc.bERP.SysPars.mainCostCenterID, bi.incomeAccountID).id, -bi.price, bi.description));
                                total += bi.price;
                            }                            
                        }
                        if (!AccountBase.AmountEqual(total, 0))
                        {
                            tran.Add(new TransactionOfBatch(custCSID, total, cb.ShortDescription));
                        }

                        if (cb.summerizeTransaction)
                        {
                            cb.billBatchID = -1;
                            TransactionSummerizer sum = new TransactionSummerizer(this._accounting, _bdeSubsc.bERP.SysPars.summaryRoots);
                            sum.addTransaction(AID,tran.ToArray());
                            tran.AddRange(sum.summary.Values);
                        }

                        cb.AccountDocumentID = _accounting.RecordTransaction(AID, cb, tran.ToArray(), cb.invoiceNumber);
                    }
                    _accounting.WriterHelper.CommitTransaction();
                    return cb.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Reverse transaction not supported");
        }

        public virtual void onSettled(int AID,CustomerPaymentReceipt settlment)
        {
        }
    }

    
}
