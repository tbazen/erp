﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.BDE
{
    public class CustomerBillSummaryHandler:INTAPS.Accounting.IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if(_bdeSubsc==null)
                _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public CustomerBillSummaryHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
            

        }

        public virtual bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return false;
        }
        public virtual void DeleteDocument(int AID, int docID)
        {
            deleteExisting(AID, docID,false);
        }

        private void deleteExisting(int AID, int docID,bool forUpdate)
        {
            CustomerBillSummaryDocument sum = _accounting.GetAccountDocument(docID, true) as CustomerBillSummaryDocument;
            foreach (int billID in sum.billIDs)
            {
                CustomerBillDocument bill = _accounting.GetAccountDocument(billID, true) as CustomerBillDocument;
                if (bill != null)
                {
                    bill.billBatchID = -1;
                    _accounting.UpdateAccountDocumentData(AID, bill);
                }
            }
            _accounting.DeleteAccountDocument(AID, docID, forUpdate);
        }
        public string GetHTML(Accounting.AccountDocument _doc)
        {
            CustomerBillSummaryDocument cb = (CustomerBillSummaryDocument)_doc;
            return cb.billIDs.Length + " Bills";
        }
        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    CustomerBillSummaryDocument cb = (CustomerBillSummaryDocument)_doc;

                    if (cb.AccountDocumentID != -1)
                        deleteExisting(AID, cb.AccountDocumentID, true);
                    cb.AccountDocumentID = _accounting.RecordTransaction(AID, cb, cb.transactions);
                    int c = cb.billIDs.Length;
                    Console.WriteLine("Setting billBatchID fields");
                    foreach (int billID in cb.billIDs)
                    {
                        //Console.CursorTop--;
                        Console.WriteLine((c--) + "                    ");
                        CustomerBillDocument bill = _accounting.GetAccountDocument(billID, true) as CustomerBillDocument;
                        bill.summerizeTransaction = false;
                        bill.billBatchID = cb.AccountDocumentID;
                        _accounting.UpdateAccountDocumentData(AID, bill);
                    }
                    //Console.CursorTop--;
                    Console.WriteLine("                                  ");
                    //Console.CursorTop--;
                    _accounting.WriterHelper.CommitTransaction();
                    return cb.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        
        public void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Reverse transaction not supported");
        }
        

    }    
}
