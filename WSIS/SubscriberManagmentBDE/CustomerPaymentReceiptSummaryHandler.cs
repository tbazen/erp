﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Accounting;

namespace INTAPS.SubscriberManagment.BDE
{
    public class CustomerPaymentReceiptSummaryHandler:INTAPS.Accounting.IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if(_bdeSubsc==null)
                _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public CustomerPaymentReceiptSummaryHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }

        public virtual bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return "true".Equals(System.Configuration.ConfigurationManager.AppSettings["supperFinanceUserMode"])
                && userSession.Permissions.UserName.Equals("admin",StringComparison.CurrentCultureIgnoreCase);
        }
        public virtual void DeleteDocument(int AID, int docID)
        {
            deleteExisting(AID, docID,false);
        }

        private void deleteExisting(int AID, int docID,bool forUpdate)
        {
            CustomerPaymentReceiptSummaryDocument sum = _accounting.GetAccountDocument(docID, true) as CustomerPaymentReceiptSummaryDocument;
            foreach (int billID in sum.receiptIDs)
            {
                CustomerPaymentReceipt receipt = _accounting.GetAccountDocument(billID, true) as CustomerPaymentReceipt;
                if (receipt != null)
                {
                    receipt.billBatchID = -1;
                    _accounting.UpdateAccountDocumentData(AID, receipt);
                }
            }
            _accounting.DeleteAccountDocument(AID, docID, forUpdate);
        }
        public string GetHTML(Accounting.AccountDocument _doc)
        {
            CustomerPaymentReceiptSummaryDocument cb = (CustomerPaymentReceiptSummaryDocument)_doc;
            return cb.receiptIDs.Length + " Receipts";
        }
        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    CustomerPaymentReceiptSummaryDocument cb = (CustomerPaymentReceiptSummaryDocument)_doc;
                    CustomerPaymentReceipt[] receipts = new CustomerPaymentReceipt[cb.receiptIDs.Length];
                    int assetAccountID = -1;
                    for (int i = 0; i < receipts.Length; i++)
                    {
                        CustomerPaymentReceipt receipt = _accounting.GetAccountDocument(cb.receiptIDs[i], true) as CustomerPaymentReceipt;
                        if (assetAccountID == -1)
                            assetAccountID = receipt.assetAccountID;
                        else
                            if (assetAccountID != receipt.assetAccountID)
                                throw new ServerUserMessage("All receipts in summary should be from the same casher");
                        receipts[i] = receipt;

                    }
                    if (cb.AccountDocumentID != -1)
                        deleteExisting(AID, cb.AccountDocumentID, true);
                    cb.assetAccountID = assetAccountID;
                    cb.AccountDocumentID = _accounting.RecordTransaction(AID, cb, cb.transactions);
                    int c = cb.receiptIDs.Length;
                    Console.WriteLine("Setting batchID fie  lds");
                    
                    foreach (CustomerPaymentReceipt receipt in receipts)
                    {
                        //Console.CursorTop--;
                        Console.WriteLine((c--) + "                    ");
                        receipt.summerizeTransaction = false;
                        receipt.billBatchID = cb.AccountDocumentID;
                        _accounting.UpdateAccountDocumentData(AID, receipt);
                    }
                    
                    //Console.CursorTop--;
                    Console.WriteLine("                                  ");
                    //Console.CursorTop--;
                    _accounting.WriterHelper.CommitTransaction();
                    return cb.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        
        public void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Reverse transaction not supported");
        }
        

    }    
}
