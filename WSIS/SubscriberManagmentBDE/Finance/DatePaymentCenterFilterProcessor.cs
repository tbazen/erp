
using INTAPS.Accounting.BDE;
using INTAPS.Evaluator;
using System;
namespace INTAPS.SubscriberManagment.BDE
{

    public class DatePaymentCenterFilterProcessor : DateFilterProcessor
    {
        public override object[] GetDefaultPars()
        {
            return new object[] { DateTime.Now, DateTime.Now, -1 };
        }

        public override void PrepareEvaluator(EHTML data, params object[] parameters)
        {
            base.PrepareEvaluator(data, parameters);
            int v = Convert.ToInt32(parameters[2]);
            data.AddVariable("pcID", new EData(DataType.Int, v));
        }
    }
}

