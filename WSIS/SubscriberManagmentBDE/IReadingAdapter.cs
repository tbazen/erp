﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INTAPS.SubscriberManagment.BDE
{
    public interface IReadingAdapter
    {
        void initialize();
        void prepareReadingCycleData();
        void generateReadingPeriod();
        void transferCustomerData(int AID, int wsisPeriodID);
        void UploadReading(int wsisPeriodID);
        string getOverdue(int periodID,DateTime date,int customerID, int connectionID);
        string[] getCustomFields();
        string[] getcustomFieldValues(int periodID,int customerID,int connectionID);
    }
}
