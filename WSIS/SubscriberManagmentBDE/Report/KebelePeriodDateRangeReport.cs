
    using INTAPS.Accounting.BDE;
    using INTAPS.Evaluator;
    using System;
namespace INTAPS.SubscriberManagment.BDE
{

    public class KebelePeriodDateRangeReport : ReportProcessorBase
    {
        public override object[] GetDefaultPars()
        {
            return new object[] { -1, -1, -1, -1, DateTime.Now, DateTime.Now };
        }

        public override void PrepareEvaluator(EHTML data, params object[] paramter)
        {
            int v = Convert.ToInt32(paramter[0]);
            int num2 = Convert.ToInt32(paramter[1]);
            int num3 = Convert.ToInt32(paramter[2]);
            int num4 = Convert.ToInt32(paramter[3]);
            DateTime time = (DateTime) paramter[4];
            DateTime time2 = (DateTime) paramter[5];
            data.AddVariable("pcID", new EData(DataType.Int, v));
            data.AddVariable("kebele", new EData(DataType.Int, num2));
            data.AddVariable("prd1", new EData(DataType.Int, num3));
            data.AddVariable("prd2", new EData(DataType.Int, num4));
            data.AddVariable("date1", new EData(DataType.DateTime, time));
            data.AddVariable("date2", new EData(DataType.DateTime, time2));
            base.PrepareEvaluator(data, paramter);
        }
    }
}

