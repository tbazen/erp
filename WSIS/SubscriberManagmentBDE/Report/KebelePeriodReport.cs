
    using INTAPS.Accounting.BDE;
    using INTAPS.Evaluator;
    using System;
namespace INTAPS.SubscriberManagment.BDE
{

    public class KebelePeriodReport : ReportProcessorBase
    {
        public override object[] GetDefaultPars()
        {
            return new object[] { -1, -1, -1, -1 };
        }

        public override void PrepareEvaluator(EHTML data, params object[] paramter)
        {
            int pc = Convert.ToInt32(paramter[0]);
            int kebele = Convert.ToInt32(paramter[1]);
            int prd1 = Convert.ToInt32(paramter[2]);
            int prd2 = Convert.ToInt32(paramter[3]);
            data.AddVariable("pcID", new EData(DataType.Int, pc));
            data.AddVariable("kebele", new EData(DataType.Int, kebele));
            data.AddVariable("prd1", new EData(DataType.Int, prd1));
            data.AddVariable("prd2", new EData(DataType.Int, prd2));
            base.PrepareEvaluator(data, paramter);
        }
    }
}

