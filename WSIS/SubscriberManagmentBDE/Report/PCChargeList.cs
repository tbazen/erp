using INTAPS.Evaluator;
using System;
using System.Configuration;
using System.Data;
using INTAPS.Accounting.BDE;
using System.Collections.Generic;
using lcpi.data.oledb;

namespace INTAPS.SubscriberManagment.BDE
{

    public class PCChargeList : IFunction
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCChargeList(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            EData data3;
            FSError error;
            string str = Pars[0].Value.ToString();
            if (!((Pars[1].Value is DateTime) && (Pars[2].Value is DateTime)))
            {
                error = new FSError("Parameter 2 and 3 should be date/time values");
                return error.ToEData();
            }
            DateTime time = (DateTime)Pars[1].Value;
            DateTime time2 = (DateTime)Pars[2].Value;
            string str2 = ConfigurationManager.AppSettings["PPDB"];
            if (string.IsNullOrEmpty(str2))
            {
                error = new FSError("Perpaid access database connection string not set");
                return error.ToEData();
            }
            OleDbConnection connection = new OleDbConnection(str2);
            try
            {
                string str3;
                connection.Open();
                if (string.IsNullOrEmpty(str))
                {
                    str3 = string.Format("SELECT PaymentWaterList.UserID, Users.UserName, Users.UserMemo, PaymentWaterList.RemainWater, PaymentWaterList.ChargeDate\r\nFROM Users INNER JOIN PaymentWaterList ON Users.UserID = PaymentWaterList.UserID\r\nWHERE (((PaymentWaterList.ChargeDate)>=#{0}# and PaymentWaterList.ChargeDate<#{1}#));", time, time2);
                }
                else
                {
                    str3 = string.Format("SELECT PaymentWaterList.UserID, Users.UserName, Users.UserMemo, PaymentWaterList.RemainWater, PaymentWaterList.ChargeDate\r\nFROM Users INNER JOIN PaymentWaterList ON Users.UserID = PaymentWaterList.UserID\r\nWHERE (((PaymentWaterList.ChargeDate)>=#{0}# and PaymentWaterList.ChargeDate<#{1}#)) and UserMemo=\"{2}\";", time, time2, str);
                }
                OleDbCommand selectCommand = new OleDbCommand(str3, connection);
                DataTable dataTable = new DataTable();
                new OleDbDataAdapter(selectCommand).Fill(dataTable);
                ListData v = new ListData(dataTable.Rows.Count);
                int num = 0;
                foreach (DataRow row in dataTable.Rows)
                {
                    string str4;
                    ListData data2 = new ListData(6);
                    data2[0] = new EData(DataType.Text, row[0].ToString());
                    data2[1] = new EData(DataType.Text, row[1].ToString());
                    data2[2] = new EData(DataType.Text, str4 = row[2].ToString());
                    data2[3] = new EData(DataType.Float, Convert.ToDouble(row[3]));
                    data2[4] = new EData(DataType.DateTime, Convert.ToDateTime(row[4]));
                    Subscription subscriptionNoException = this.m_subsc.GetSubscriptionNoException(str4);
                    if (subscriptionNoException == null)
                    {
                        data2[5] = new EData(DataType.Text, "[Subscription not in main database]");
                    }
                    else
                    {
                        data2[5] = new EData(DataType.Text, subscriptionNoException.subscriber.name);
                    }
                    v[num] = new EData(DataType.ListData, data2);
                    num++;
                }
                data3 = new EData(DataType.ListData, v);
            }
            catch (Exception exception)
            {
                data3 = new FSError("Error retreiving prepaid data " + exception.Message).ToEData();
            }
            finally
            {
                if ((connection != null) && (connection.State != ConnectionState.Closed))
                {
                    connection.Close();
                }
            }
            return data3;
        }

        public string Name
        {
            get
            {
                return "Prepaid Charge List";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "PPChargeList";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
    public class PCReaderWorkData : IFunction, System.Collections.Generic.IComparer<BWFMeterReading>
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCReaderWorkData(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }
        class ReadingWork
        {
            public double work = 0;
            public double workBreak = 0;
            public double count = 0;
            public double countManual = 0;
        }

        public EData Evaluate(EData[] Pars)
        {
            int readerID = (int)Pars[0].Value;
            int periodID = (int)Pars[1].Value;
            BWFMeterReading[] reading;
            INTAPS.RDBMS.SQLHelper dspReader = m_subsc.GetReaderHelper();
            try
            {

                reading = dspReader.GetSTRArrayBySQL<BWFMeterReading>(string.Format("Select * from {0}.dbo.BWFMeterReading where bwfStatus=1 and ((extraInfo%4)>=2) and method in (2,3) and periodID={1} and readingBlockID in (Select id from {0}.dbo.BWFReadingBlock where readerID={2}) order by readingTime", m_subsc.DBName, periodID, readerID));

            }
            finally
            {
                m_subsc.ReleaseHelper(dspReader);
            }
            Array.Sort(reading, this);
            SortedList<DateTime, ReadingWork> data = new SortedList<DateTime, ReadingWork>();
            DateTime prev = DateTime.MinValue;
            ReadingWork thisWork = null;
            List<double> hs = null;
            foreach (BWFMeterReading r in reading)
            {
                if (prev.Date != r.readingTime.Date)
                {
                    thisWork = new ReadingWork();
                    thisWork.count = 1;
                    data.Add(r.readingTime.Date, thisWork);
                    hs = new List<double>();
                    hs.Add(0);
                }
                else
                {
                    thisWork.count++;
                    double hours = r.readingTime.Subtract(prev).TotalHours;
                    if (hours > 0.5)
                    {
                        thisWork.workBreak += hours;
                        hs.Add(0);
                    }
                    else
                    {
                        thisWork.work += hours;
                        hs.Add(hours);
                    }
                }
                prev = r.readingTime;

            }
            ListData listData = new ListData(data.Count);
            int i = 0;
            foreach (KeyValuePair<DateTime, ReadingWork> kv in data)
            {
                ListData row = new ListData(4);
                row[0] = new EData(DataType.Text, Accounting.AccountBase.FormatDate(kv.Key));
                row[1] = new EData(DataType.Float, kv.Value.count);
                row[2] = new EData(DataType.Float, kv.Value.work);
                row[3] = new EData(DataType.Float, kv.Value.workBreak);
                listData[i++] = new EData(DataType.ListData, row);
            }
            return new EData(DataType.ListData, listData);
        }

        public string Name
        {
            get
            {
                return "Readers Work Data";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "RdrWrkDat";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }

        public int Compare(BWFMeterReading x, BWFMeterReading y)
        {
            return x.readingTime.CompareTo(y.readingTime);
        }
    }
}

