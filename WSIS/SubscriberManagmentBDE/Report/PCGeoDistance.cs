using INTAPS.Evaluator;
using INTAPS.SubscriberManagment;
using System;
using INTAPS.Accounting.BDE;
namespace INTAPS.SubscriberManagment.BDE
{
    public class PCGeoDistance : IFunction
    {
        public PCGeoDistance()
        {
        }

        public EData Evaluate(EData[] Pars)
        {
            double lat1 = Convert.ToDouble(Pars[0].Value);
            double lng1 = Convert.ToDouble(Pars[1].Value);
            double lat2 = Convert.ToDouble(Pars[2].Value);
            double lng2 = Convert.ToDouble(Pars[3].Value);
            return new EData(DataType.Float, Subscription.sphericalDistance(lat1, lng1, lat2, lng2));
        }

        public string Name
        {
            get
            {
                return "GeoDistance";
            }
        }

        public int ParCount
        {
            get
            {
                return 4;
            }
        }

        public string Symbol
        {
            get
            {
                return "GeoDist";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}
