﻿using INTAPS.Evaluator;
using INTAPS.RDBMS;
using System;
using System.Collections.Generic;

namespace INTAPS.SubscriberManagment.BDE
{
    public class PCGetCollectionSummary : IVarParamCountFunction
    {
        int n_pars = 5;
        private SubscriberManagmentBDE m_subsc;
        public PCGetCollectionSummary(SubscriberManagmentBDE subsc)
        {
            this.m_subsc = subsc;
        }
        public string Name => "Billing Collection Summary";

        public int ParCount => n_pars;

        public string Symbol => "WSISCollection";

        public FunctionType Type => FunctionType.PreFix;


        public IVarParamCountFunction Clone()
        {
            return new PCGetCollectionSummary(m_subsc) { n_pars = n_pars };
        }
        List<int> toIntList(EData par)
        {
            List<int> ret = new List<int>();
            if (par.Type != DataType.ListData)
                return ret;
            foreach(EData data in ((ListData)par.Value).elements)
            {
                ret.Add(Convert.ToInt32(data.Value));
            }
            return ret;
        }
        
        public EData Evaluate(EData[] Pars)
        {
            //parameters
            DateTime dateFrom = Convert.ToDateTime(Pars[0].Value);  //date lower bound
            DateTime dateTo = Convert.ToDateTime(Pars[1].Value);    //date upper bound (exclusive_
            List<int> documentType = toIntList(Pars[2]);//list of document types
            List<int> itemID= toIntList(Pars[3]);   //list of item ides
            List<int> accountID= toIntList(Pars[4]);    //list of acount ids from incomeAccountID of bill items
            List<int> customerType = Pars.Length > 5 ? toIntList(Pars[5]) : new List<int>();   //list of customer types
            List<int> parentAccountID = Pars.Length > 6 ? toIntList(Pars[6]) : new List<int>(); //list of parent account id
            String cr=null;
            String list = null;

            foreach (int t in documentType)
                list = INTAPS.StringExtensions.AppendOperand(list, ",", t.ToString());
            if (list != null)
                cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "billDocumentTypeID in (" + list + ")");
            list = null;
            foreach(int t in itemID)
                list = INTAPS.StringExtensions.AppendOperand(list, ",", t.ToString());
            if (list != null)
                cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "itemTypeID in (" + list + ")");

            list = null;
            foreach (int t in accountID)
                list = INTAPS.StringExtensions.AppendOperand(list, ",", t.ToString());
            if (list != null)
                cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "incomeAccountID in (" + list + ")");
            list = null;
            foreach (int t in customerType)
                list = INTAPS.StringExtensions.AppendOperand(list, ",", t.ToString());
            if (list != null)
            {
                cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "customerID in (Select id from {0}.dbo.Subscriber where subscriberType in ({1}))".format(m_subsc.DBName,list));
            }

            list = null;
            foreach (int t in parentAccountID)
                list = INTAPS.StringExtensions.AppendOperand(list, ",", t.ToString());
            if (list != null)
            {
                cr = INTAPS.StringExtensions.AppendOperand(cr, " and ", "(Select pid from {0}.dbo.Account where id=i.incomeAccountID) in ({1})".format(m_subsc.Accounting.DBName, list));
            }

            String sql = @"Select sum(price-settledFromDepositAmount) from {0}.dbo.CustomerBillItem i inner join {0}.dbo.CustomerBill b on i.customerBillID=b.id 
            where paymentDocumentID<>-1 and paymentDate>='{2}' and paymentDate<='{3}' and {1}".format(m_subsc.DBName,cr,dateFrom,dateTo);
            SQLHelper reader = m_subsc.GetReaderHelper();
            try
            {
                double amount;
                object _amount = reader.ExecuteScalar(sql);
                if (_amount is double)
                    amount = (double)_amount;
                else
                    amount = 0;
                return new EData(amount);
            }
            finally
            {
                m_subsc.ReleaseHelper(reader);
            }       
        }

        public bool SetParCount(int n)
        {
            if (ParCount < 5 || ParCount > 7)
                return false;
            this.n_pars = n;
            return true;
        }
       
    }
}

