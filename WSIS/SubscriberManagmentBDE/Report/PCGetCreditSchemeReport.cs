
using INTAPS.Accounting;
using INTAPS.Evaluator;
using INTAPS.SubscriberManagment;
using System;
using System.Collections.Generic;
using INTAPS.Accounting.BDE;
using BIZNET.iERP;
using INTAPS.RDBMS;
using System.Data;
using System.Linq;

namespace INTAPS.SubscriberManagment.BDE
{
    public class PCBillLedgerEntry : IFunction
    {
        SubscriberManagmentBDE subsc;
        public PCBillLedgerEntry(SubscriberManagmentBDE subsc)
        {
            this.subsc = subsc;
        }
        public string Name => "Bill Ledger Entry";

        //account IDs
        //debit side doc filters
        //credit side doc filters
        //date 1
        //date 2
        public int ParCount => 5;

        public string Symbol=> "BillLedgerEntry";

        public FunctionType Type => FunctionType.PreFix;

        public EData Evaluate(EData[] Pars)
        {
            var accountIDs = ((ListData)Pars[0].Value).elements.Select(x => Convert.ToInt32(x.Value))
                .Select(x=>subsc.Accounting.GetCostCenterAccountID(CostCenter.ROOT_COST_CENTER,x)).ToArray();
            var debitSideFilters = ((ListData)Pars[1].Value).elements.Select(x =>
            ((ListData)x.Value).elements.Select(y => Convert.ToInt32(y.Value)).ToList()
            ).ToArray();
            var creditSideFilters = ((ListData)Pars[2].Value).elements.Select(x =>
                ((ListData)x.Value).elements.Select(y => Convert.ToInt32(y.Value)).ToList()
                ).ToArray();
            var date1 = (DateTime)Pars[3].Value;
            var date2 = (DateTime)Pars[4].Value;
            int N;
            var tran = subsc.Accounting.GetLedger(0, 0, accountIDs, new int[] { 0 }, date1, date2, 0, -1, out N);
            var dbSummary = new double[debitSideFilters.Length+1];
            var crSummary = new double[creditSideFilters.Length+1];
            for (var i = 0; i < crSummary.Length; i++)
                dbSummary[i] = 0;
            for (var i = 0; i < crSummary.Length; i++)
                crSummary[i] = 0;

            foreach (var t in tran)
            {
                if(AccountBase.AmountGreater(t.Amount,0))
                {
                    bool found = false;
                    for(var i=0;i<debitSideFilters.Length;i++)
                    {
                        if(debitSideFilters[i].Contains(t.documentID))
                        {
                            dbSummary[i] += t.Amount;
                            found = true;
                        }
                    }
                    if (!found)
                        dbSummary[dbSummary.Length - 1] += t.Amount;
                }
                else if (AccountBase.AmountLess(t.Amount, 0))
                {
                    bool found = false;
                    for (var i = 0; i < creditSideFilters.Length; i++)
                    {
                        if (creditSideFilters[i].Contains(t.documentID))
                        {
                            crSummary[i] -= t.Amount;
                            found = true;
                        }
                    }
                    if (!found)
                        crSummary[crSummary.Length - 1] -= t.Amount;
                }
            }
            var DBSide = new EData(dbSummary);
            var CRSide = new EData(crSummary);
            return new EData(DataType.ListData, new ListData(new EData[] { DBSide, CRSide }));

        }
    }
    public class PCGetCreditSchemeReport : IFunction
    {
        [Serializable]
        class CreditSchemeRow
        {
            public int schemeID { get; set; }

            public string issueData { get; set; }

            public int customerID { get; set; }

            public string customerCode { get; set; }

            public string name { get; set; }

            public double creditedAmount { get; set; }

            public double paymentAmount { get; set; }

            public string startPeriod { get; set; }

            public string active { get; set; }

            public double differedAmount { get; set; }

            public string userName { get; set; }
        }
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCGetCreditSchemeReport(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            //Columns: scheme ID, scheme date, customer id, customer code, customer name,differed amount,monthly payment, start month,status, differed bills amount

            INTAPS.RDBMS.SQLHelper reader = m_subsc.GetReaderHelper();
            this.m_finance.PushProgress("Retreving data");
            try
            {
                string sql = @"SELECT     CreditScheme.id AS schemeID, CreditScheme.issueDate, CreditScheme.customerID, Subscriber.customerCode, Subscriber.name, CreditScheme.creditedAmount, 
                      CreditScheme.paymentAmount, CreditScheme.startPeriodID, CreditScheme.active, CreditScheme.__AID
FROM         {0}.dbo.CreditScheme INNER JOIN
                      {0}.dbo.Subscriber ON CreditScheme.customerID = Subscriber.id order by customerCode".format(m_subsc.DBName);
                string differedBillSql = @"SELECT     isnull(SUM(price-settledFromDepositAmount),0.0)
                            FROM         {0}.dbo.DifferedBill INNER JOIN
                      {0}.dbo.CustomerBillItem ON DifferedBill.billRecordID = CustomerBillItem.customerBillID
                    WHERE     (DifferedBill.creditSchemeID = {1})";
                DataTable data = reader.GetDataTable(sql);
                ListData list = new ListData(data.Rows.Count);
                int i = 0;
                foreach (DataRow row in data.Rows)
                {
                    int schemeID = (int)row["schemeID"];
                    double differedAmount = (double)reader.ExecuteScalar(differedBillSql.format(m_subsc.DBName, schemeID));
                    int startPeriodID = (int)row["startPeriodID"];
                    INTAPS.ClientServer.AuditInfo audit=INTAPS.ClientServer.ApplicationServer.SecurityBDE.getAuditInfo((int)row["__AID"]);
                    string userName;
                    if (audit == null)
                        userName = "";
                    else
                    {
                        if (audit.userName.Equals("admin",StringComparison.CurrentCultureIgnoreCase))
                            userName = "";
                        else
                            userName = audit.userName;
                    }
                    list[i++] = new EData(new CreditSchemeRow
                        {
                            schemeID = schemeID,
                            issueData = AccountBase.FormatTime((DateTime)row["issueDate"]),
                            customerID = (int)row["customerID"],
                            customerCode = (string)row["customerCode"],
                            name = (string)row["name"],
                            creditedAmount = (double)row["creditedAmount"],
                            paymentAmount = (double)row["paymentAmount"],
                            startPeriod = m_subsc.GetBillPeriod(startPeriodID).name,
                            active = (bool)row["active"] ? "" : "Closed",
                            differedAmount = differedAmount,
                            userName = userName,
                        });
                    m_finance.SetProgress((double)i / (double)data.Rows.Count);
                }

                return new EData(DataType.ListData, list);
            }
            finally
            {
                m_finance.PopProgress();
            }
        }

        public string Name
        {
            get
            {
                return "Credit Scheme Report";
            }
        }

        public int ParCount
        {
            get
            {
                return 0;
            }
        }

        public string Symbol
        {
            get
            {
                return "SUBSC_CreditScheme";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

