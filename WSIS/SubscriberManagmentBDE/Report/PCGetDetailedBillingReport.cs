
using INTAPS.Accounting;
using INTAPS.Evaluator;
using INTAPS.SubscriberManagment;
using System;
using System.Collections.Generic;
using INTAPS.Accounting.BDE;
using BIZNET.iERP;
using INTAPS.RDBMS;
namespace INTAPS.SubscriberManagment.BDE
{
    [SingleTableObject,Serializable]
    public class REPCollection
    {
        [IDField]
        public long ticksFrom;
        [IDField]
        public long ticksTo;
        [IDField]
        public int periodID;
        [IDField]
        public int kebele;
        [IDField]
        public int customerTypeID;
        [IDField]
        public int billTypeID;
        [DataField]
        public double totalAmount;
        [DataField]
        public double amount1;
        [DataField]
        public double amount2;
        [DataField]
        public double amount3;
        [DataField]
        public double amount4;
        [DataField]
        public double volume;

        public string key
        {
            get
            {
                return ticksFrom+"_"+ticksTo+"_"+ periodID + "_" + kebele + "_" + customerTypeID + "_" + billTypeID;
            }
        }

    }

    [SingleTableObject,Serializable]
    public class REPBilling
    {
        [IDField]
        public int periodID;
        [IDField]
        public int kebele;
        [IDField]
        public int customerTypeID;
        [IDField]
        public int billTypeID;
        [DataField]
        public double totalAmount;
        [DataField]
        public double amount1;
        [DataField]
        public double amount2;
        [DataField]
        public double amount3;
        [DataField]
        public double amount4;
        [DataField]
        public double volume;
        [DataField]
        public int nPaid;
        [DataField]
        public int nDiffered;
        public string key
        {
            get
            {
                return periodID + "_" + kebele + "_" + customerTypeID + "_" + billTypeID;
            }
        }

        internal void add(REPBilling rep)
        {
            totalAmount += rep.totalAmount;
            amount1 += rep.amount1;
            amount2 += rep.amount2;
            amount3 += rep.amount3;
            amount4 += rep.amount4;
        }
    }

    public class PCGetDetailedBillingReport : IFunction
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCGetDetailedBillingReport(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            int periodID = (int)Convert.ChangeType(Pars[0].Value, typeof(int));
            DateTime from = (DateTime)Pars[1].Value;
            DateTime to = (DateTime)Pars[2].Value;
            CustomerBillRecord[] bills;

            Dictionary<int, List<BillItem>> billItems = new Dictionary<int, List<BillItem>>();
            Dictionary<int, Subscriber> subscribers = new Dictionary<int, Subscriber>();
            INTAPS.RDBMS.SQLHelper reader = m_subsc.GetReaderHelper();
            this.m_finance.PushProgress("Retreving data");
            try
            {
                string crBills = string.Format("periodID={0} or (periodID=-1 and billDate>='{1}' and billDate<'{2}')", periodID, from, to);
                bills = reader.GetSTRArrayByFilter<CustomerBillRecord>(crBills);
                foreach (CustomerBillRecord b in bills)
                    billItems.Add(b.id, new List<BillItem>());
                BillItem[] dbItems = reader.GetSTRArrayByFilter<BillItem>(string.Format("customerBillID in (Select id from {0}.dbo.CustomerBill where {1})", m_subsc.DBName, crBills));
                foreach (BillItem i in dbItems)
                {
                    billItems[i.customerBillID].Add(i);
                }
                //BWFMeterReading[] readings = reader.GetSTRArrayByFilter<BWFMeterReading>(string.Format("subscriptionID in (Select connectionID from {0}.dbo.CustomerBill where {1}) and periodID={2}",m_subsc.DBName,crBills, periodID));
                Subscriber[] subscs = reader.GetSTRArrayByFilter<Subscriber>(string.Format("id in (Select customerID from {0}.dbo.CustomerBill where {1})", m_subsc.DBName, crBills));
                foreach (Subscriber s in subscs)
                    subscribers.Add(s.id, s);
                string[] docXml = reader.GetColumnArray<string>(string.Format("Select documentData from {0}.dbo.Document where id in (Select id from {1}.dbo.CustomerBill where {2})", m_finance.DBName,m_subsc.DBName, crBills));
                SortedDictionary<string, REPBilling> billSummary = new SortedDictionary<string, REPBilling>();
                Dictionary<int, Subscriber> customers = new Dictionary<int, Subscriber>();

                int waterBillTypeID = m_finance.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
                int c = bills.Length;
                Console.WriteLine("{0} bills", c);
                Console.WriteLine();
                this.m_finance.PushProgress("Processing bills");
                try
                {
                    foreach (CustomerBillRecord b in bills)
                    {
                        
                        this.m_finance.SetProgress((double)(bills.Length - c) / (double)bills.Length);
                        //Console.CursorTop--;
                        Console.WriteLine(c-- + "                 ");
                        List<BillItem> items = billItems[b.id];

                        REPBilling rep = new REPBilling();

                        rep.periodID = periodID;
                        Subscriber subsc = subscribers[b.customerID];
                        rep.kebele = subsc.Kebele;
                        rep.customerTypeID = (int)subsc.subscriberType;
                        rep.billTypeID = b.billDocumentTypeID;

                        rep.totalAmount = 0;
                        if (b.billDocumentTypeID == waterBillTypeID)
                        {
                            WaterBillDocument doc = m_finance.GetAccountDocument(b.id, true) as WaterBillDocument;
                            rep.volume = doc.reading.consumption;
                        }
                        foreach (BillItem bi in items)
                        {
                            rep.totalAmount += bi.price;
                            if (b.billDocumentTypeID == waterBillTypeID)
                            {
                                
                                
                                switch (bi.itemTypeID)
                                {
                                    case 0:
                                        rep.amount1 = bi.price;
                                        break;
                                    case 1:
                                        rep.amount2 = bi.price;
                                        break;
                                    case 2:
                                        rep.amount3 = bi.price;
                                        break;
                                    case 3:
                                        rep.amount4 = bi.price;
                                        break;
                                }
                            }
                        }
                        if (billSummary.ContainsKey(rep.key))
                            billSummary[rep.key].add(rep);
                        else
                            billSummary.Add(rep.key, rep);
                    }
                    ListData ld = new ListData(billSummary.Count);
                    int i = 0;
                    foreach (REPBilling b in billSummary.Values)
                    {
                        ld[i++] = new EData(DataType.Object, b);
                    }
                    return new EData(DataType.ListData, ld);
                }
                finally
                {
                    m_subsc.ReleaseHelper(reader);
                    this.m_finance.PopProgress();
                }
            }
            finally
            {
                m_finance.PopProgress();
            }
        }

        public string Name
        {
            get
            {
                return "Detailed Billing Report";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "SUBSC_BillRep";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

