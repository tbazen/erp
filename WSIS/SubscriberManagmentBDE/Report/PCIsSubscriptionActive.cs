
    using INTAPS.Evaluator;
    using INTAPS.SubscriberManagment;
    using System;
using System.Collections.Generic;
using System.Data;
namespace INTAPS.SubscriberManagment.BDE
{
    public class PCGetReaderAllocation : IFunction
    {
         private SubscriberManagmentBDE _bde;

         public PCGetReaderAllocation(SubscriberManagmentBDE subsc)
        {
            this._bde = subsc;
        }
        public EData Evaluate(EData[] Pars)
        {
            try
            {
                ListData readerIDs = (ListData)Pars[0].Value;
                int periodID= Convert.ToInt32(Pars[1].Value);
                
                string sql = @"SELECT     subscriptionID,readerID
FROM         {0}.dbo.BWFMeterReading INNER JOIN
                      {0}.dbo.BWFReadingBlock ON BWFMeterReading.readingBlockID = BWFReadingBlock.id
where BWFReadingBlock.periodID={1}";
                
                sql = string.Format(sql, _bde.DBName, periodID);
                INTAPS.RDBMS.SQLHelper reader= _bde.GetReaderHelper();
                try
                {
                    BillPeriod prd=_bde.GetBillPeriodInternal(reader,periodID);
                    if (prd == null)
                        throw new Exception("Invalid period ID:" + periodID);
                    Dictionary<int, int[]> data = new Dictionary<int, int[]>();
                    
                    DataTable dt=reader.GetDataTable(sql);
                    int rowIndex = 0;
                    _bde.Accounting.PushProgress("Parsing reading sheet");
                    foreach(DataRow r in dt.Rows)
                    {
                        bool inactive= false;
                        bool prepaid= false;
                        
                        int readerID = (int)r[1];
                        inactive = _bde.GetSubscriptionInternal(reader,(int)r[0], prd.toDate.Ticks,false).subscriptionStatus != SubscriptionStatus.Active;
                        prepaid = _bde.isPrePaidInternal (reader, (int)r[0]);
                        if (data.ContainsKey(readerID))
                        {
                            int[] row = data[readerID];
                            row[0]++;
                            if (inactive)
                                row[1]++;
                            if (prepaid)
                                row[2]++;
                            if (!inactive && !prepaid)
                                row[3]++;
                        }
                        else
                        {
                            data.Add(readerID, new int[] { 1, inactive ? 1 : 0, prepaid ? 1 : 0, (!inactive && !prepaid) ? 1 : 0 });
                        }
                        _bde.Accounting.SetProgress((double)(++rowIndex)/ (double)dt.Rows.Count);
                    }
                    _bde.Accounting.PopProgress();
                    ListData ret = new ListData(readerIDs.elements.Length+1);
                    
                    ret[0]=new EData(DataType.ListData,new ListData(new EData[]{
                        new EData(DataType.Text,"N")
                        ,new EData(DataType.Text,"NInactive")
                        ,new EData(DataType.Text,"NPrepaid")
                        ,new EData(DataType.Text,"NReadable")
                    }));
                    for (int i = 0; i < readerIDs.elements.Length; i++)
                    {
                        int rid=(int)readerIDs[i].Value;
                        int[] row;
                        if (data.ContainsKey(rid))
                            row = data[rid];
                        else
                            row = new int[4];

                        ret[i+1]=new EData(DataType.ListData, new ListData(new EData[]{
                        new EData(DataType.Int,row[0])
                        ,new EData(DataType.Int,row[1])
                        ,new EData(DataType.Int,row[2])
                        ,new EData(DataType.Int,row[3])
                    }));
                    }
                    return new EData(DataType.ListData,ret);
                }
                finally
                {
                    _bde.ReleaseHelper(reader);
                }                

            }
            catch (Exception ex)
            {
                return new FSError(ex.Message).ToEData();
            }
        }

        public string Name
        {
            get { return "GetReaderAloc"; }
        }

        public int ParCount
        {
            get { return 2; }
        }

        public string Symbol
        {
            get { return "GetReaderAloc"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }

    public class PCIsSubscriptionActive : IFunction
    {
        private SubscriberManagmentBDE m_subsc;

        public PCIsSubscriptionActive(SubscriberManagmentBDE subsc)
        {
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            int subscriptionID = (int) Convert.ChangeType(Pars[0].Value, typeof(int));
            DateTime dateTime = (DateTime) Pars[1].Value;
            Subscription subsc = this.m_subsc.GetSubscription(subscriptionID, dateTime.Ticks);
            if(subsc==null)
                return new EData(DataType.Bool, false);
            return new EData(DataType.Bool, this.m_subsc.GetSubscription(subscriptionID, dateTime.Ticks).subscriptionStatus == SubscriptionStatus.Active);
        }

        public string Name
        {
            get
            {
                return "SMIsActive";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "SMIsActive";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

