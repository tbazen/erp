
    using INTAPS.Accounting;
    using INTAPS.Evaluator;
    using INTAPS.Accounting.BDE;
    using INTAPS.RDBMS;
    using INTAPS.SubscriberManagment;
    using System;
    using System.Data;
namespace INTAPS.SubscriberManagment.BDE
{

    public class PCReceiptList : IFunction
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCReceiptList(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            //EData data3;
            //DateTime time = (DateTime) Pars[0].Value;
            //DateTime time2 = (DateTime) Pars[1].Value;
            //SQLHelper readerHelper = this.m_finance.GetReaderHelper();
            //try
            //{
            //    string sql = string.Format("SELECT     ExecDate,paperRef,-Amount,Document.ID\r\nFROM         [Document] INNER JOIN\r\n                      AccountTransaction ON [Document].ID = AccountTransaction.BatchID\r\n                      where Document.DocumentTypeID=18 and AccountID={0}\r\n                      and AccountTransaction.ExecDate between '{1}' and '{2}' ", this.m_subsc.SysPars.incomeAccount, time, time2);
            //    DataTable dataTable = readerHelper.GetDataTable(sql);
            //    ListData v = new ListData(dataTable.Rows.Count);
            //    int num = 0;
            //    foreach (DataRow row in dataTable.Rows)
            //    {
            //        PreppaidReceipt accountDocument = this.m_finance.GetAccountDocument((int) row[3], true) as PreppaidReceipt;
            //        ListData data2 = new ListData(11);
            //        data2[0] = new EData(DataType.DateTime, Convert.ToDateTime(row[0]));
            //        data2[1] = new EData(DataType.Text, accountDocument.PaperRef);
            //        data2[2] = new EData(DataType.Text, accountDocument.subscription.contractNo);
            //        data2[3] = new EData(DataType.Text, accountDocument.subscription.subscriber.name);
            //        data2[4] = new EData(DataType.Float, accountDocument.data.cubicMeters);
            //        data2[5] = new EData(DataType.Float, (double) accountDocument.data.rentPeriods.Length);
            //        double num2 = 0.0;
            //        foreach (double num3 in accountDocument.data.rents)
            //        {
            //            num2 += num3;
            //        }
            //        data2[6] = new EData(DataType.Float, accountDocument.data.paymentAmount - num2);
            //        data2[7] = new EData(DataType.Float, num2);
            //        data2[8] = new EData(DataType.Float, accountDocument.data.paymentAmount);
            //        data2[9] = new EData(DataType.Bool, accountDocument.reversed);
            //        Account account = this.m_finance.GetAccount<Account>(accountDocument.cashAccount);
            //        data2[10] = new EData(DataType.Text, account.Name);
            //        v[num] = new EData(DataType.ListData, data2);
            //        num++;
            //    }
            //    data3 = new EData(DataType.ListData, v);
            //}
            //catch (Exception exception)
            //{
            //    data3 = new FSError("Error retreiving prepaid data " + exception.Message).ToEData();
            //}
            //finally
            //{
            //    this.m_finance.ReleaseHelper(readerHelper);
            //}
            //return data3;
            throw new NotImplementedException("Function " + this.Symbol + " is not upgraded");
        }

        public string Name
        {
            get
            {
                return "Prepaid Receipt List";
            }
        }

        public int ParCount
        {
            get
            {
                return 2;
            }
        }

        public string Symbol
        {
            get
            {
                return "PPReceiptList";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }
}

