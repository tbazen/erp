
using INTAPS.Accounting;
using INTAPS.Evaluator;


using INTAPS.SubscriberManagment;
using System;
using System.Collections.Generic;
using INTAPS.Accounting.BDE;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{

    public class PCSalesReport : IFunction
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCSalesReport(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            int ntransactions;
            AccountBalance balance;
            int accountID = (int)Convert.ChangeType(Pars[0].Value, typeof(int));
            DateTime from = (DateTime)Pars[1].Value;
            DateTime to = (DateTime)Pars[2].Value;
            AccountTransaction[] transactionArray = this.m_finance.GetTransactions(accountID, 0, true, from, to, 0, -1, out ntransactions, out balance);
            List<ListData> list = new List<ListData>();
            HashSet<int> docs = new HashSet<int>();
            foreach (AccountTransaction transaction in transactionArray)
            {
                if (docs.Contains(transaction.documentID))
                    continue;
                docs.Add(transaction.documentID);
                try
                {
                    AccountDocument document = this.m_finance.GetAccountDocument(transaction.documentID, true);
                    string remark = string.IsNullOrEmpty(transaction.Note) ? (document==null?"<Null Doc>": document.ShortDescription) : transaction.Note;
                    DocumentType docType = document == null ? null:this.m_finance.GetDocumentTypeByID(document.DocumentTypeID);
                    System.Type typeObject = docType==null?null:docType.GetTypeObject();
                    DateTime documentDate = document == null ?DateTime.Now:document.DocumentDate;
                    ListData row = new ListData(9);
                    TransactionOfBatch[] transactionsOfDocument = this.m_finance.GetTransactionsOfDocument(transaction.documentID);
                    double totalCredit = 0.0;
                    double totalDebit = 0.0;
                    
                    foreach (TransactionOfBatch batch in transactionsOfDocument)
                    {
                        if (batch.AccountID != accountID)
                        {
                            if (batch.Amount > 0.0)
                            {
                                totalDebit += batch.Amount;
                            }
                            else
                            {
                                totalCredit -= batch.Amount;
                            }
                        }
                    }
                    if (totalCredit > totalDebit)
                    {
                        totalCredit -= totalDebit;
                        totalDebit = 0.0;
                    }
                    else
                    {
                        totalDebit -= totalCredit;
                        totalCredit = 0.0;
                    }
                    //if (!Account.AmountEqual(totalDebit + totalCredit, 0.0))
                    {
                        double totalInstrument = 0;
                        string instruments;
                        string customerCode;
                        string shortDescription;
                        string paperRef = document == null ?"<Null Doc>": document.PaperRef;
                        int payID = -1;
                        if (typeObject!=null && ((typeObject == typeof(CustomerPaymentReceipt)) || typeObject.IsSubclassOf(typeof(CustomerPaymentReceipt))))
                        {
                            payID = document == null ?-1: document.AccountDocumentID;
                        }
                        else 
                        {
                        }
                        if (payID == -1)
                        {
                            customerCode = "";
                            shortDescription = "";
                            instruments = "";
                        }
                        else
                        {
                            try
                            {
                                CustomerBillRecord[] paidBills = this.m_subsc.getBillRecordsByPayment(payID);
                                if (paidBills.Length == 0)
                                {
                                    customerCode = "[Receipt wit No Bill]";
                                    shortDescription = document == null ? "<Null Doc>":document.ShortDescription;
                                }
                                else
                                {
                                    Subscriber customer = this.m_subsc.GetSubscriber(paidBills[0].customerID);
                                    customerCode = customer.customerCode;
                                    shortDescription = customer.name;
                                }
                                instruments = "";
                                CustomerPaymentReceipt receipt = document == null ? null : document as CustomerPaymentReceipt;
                                if (receipt != null)
                                {
                                    foreach (PaymentInstrumentItem instItem in receipt.paymentInstruments)
                                    {
                                        string thisInst;
                                        totalInstrument += instItem.amount;
                                        if (instItem.instrumentTypeItemCode.Equals(m_subsc.bERP.SysPars.cashInstrumentCode))
                                        {
                                            if (receipt.paymentInstruments.Length == 1)
                                                break;
                                            thisInst = "Cash :" + AccountBase.FormatAmount(instItem.amount);
                                        }
                                        else
                                        {
                                            thisInst = m_subsc.bERP.getPaymentInstrumentType(instItem.instrumentTypeItemCode).name + "(" + instItem.documentReference + "): " + AccountBase.FormatAmount(instItem.amount);
                                        }
                                        instruments = instruments == "" ? thisInst : instruments + ", " + thisInst;
                                    }
                                }

                            }
                            catch
                            {
                                customerCode = "Error";
                                shortDescription = "Error";
                                instruments = "Error";
                            }
                        }
                        row[0] = new EData(DataType.Text, paperRef);
                        row[1] = new EData(DataType.Text, customerCode);
                        row[2] = new EData(DataType.Text, shortDescription);
                        row[3] = new EData(DataType.DateTime, documentDate);
                        row[4] = new EData(DataType.Float, totalCredit);
                        row[5] = new EData(DataType.Float, totalDebit);
                        row[6] = new EData(DataType.Text, instruments);
                        row[7] = new EData(DataType.Text, remark);
                        row[8] = new EData(DataType.Float, totalInstrument);
                        list.Add(row);
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message+"\n"+exception.StackTrace);
                    throw new Exception(exception.Message, exception);
                }
            }
            ListData v = new ListData(list.Count);

            int num6 = 0;
            foreach (ListData data in list)
            {
                v[num6] = new EData(DataType.ListData, data);
                num6++;
            }
            return new EData(DataType.ListData, v);
        }

        public string Name
        {
            get
            {
                return "Sales Report";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "SalesRep";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }

    public class PCSalesSummaryReport : IFunction
    {
        enum SummaryItemType
        {
            ThisMonthBill=1,
            OtherMonthBill=2,
            OtherPayments=3,
        }
        class SummaryItem:IComparable<SummaryItem>
        {
            public SummaryItemType type;
            public int documentType;
            public double amount=0;
            public double consumption=0;
            public double rent=0;
            public double serviceCharge=0;
            public double volume=0;
            public string getKey
            {
                get
                {
                    return type + "." + documentType;
                }
            }

            public int CompareTo(SummaryItem other)
            {
                int c = ((int)this.type).CompareTo((int)other.type);
                if (c != 0)
                    return c;
                return this.documentType.CompareTo(other.documentType);
            }
        }
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCSalesSummaryReport(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            int ntransactions;
            AccountBalance balance;
            int periodID = (int)Convert.ChangeType(Pars[0].Value, typeof(int));
            int accountID = (int)Convert.ChangeType(Pars[1].Value, typeof(int));
            DateTime from = (DateTime)Pars[2].Value;
            DateTime to = (DateTime)Pars[3].Value;
            AccountTransaction[] transactionArray = this.m_finance.GetTransactions(accountID, 0, true, from, to, 0, -1, out ntransactions, out balance);
            List<ListData> list = new List<ListData>();
            SortedList<string, SummaryItem> summary = new SortedList<string, SummaryItem>();
            BillPeriod prd=m_subsc.GetBillPeriod(periodID);
            foreach (AccountTransaction transaction in transactionArray)
            {


                AccountDocument document = this.m_finance.GetAccountDocument(transaction.documentID, true);
                string remark = string.IsNullOrEmpty(transaction.Note) ? document.ShortDescription : transaction.Note;
                DocumentType docType = this.m_finance.GetDocumentTypeByID(document.DocumentTypeID);
                System.Type typeObject = docType.GetTypeObject();

                int payID = -1;
                if ((typeObject == typeof(CustomerPaymentReceipt)) || typeObject.IsSubclassOf(typeof(CustomerPaymentReceipt)))
                    payID = document.AccountDocumentID;
                else
                    continue;
                
                CustomerPaymentReceipt receipt = document as CustomerPaymentReceipt;                
                foreach (int billID in receipt.settledBills)
                {
                    CustomerBillDocument bill = m_finance.GetAccountDocument(billID, true) as CustomerBillDocument;
                    SummaryItemType type;
                    double consumption=0;
                    double rent=0;
                    double serviceCharge=0;
                    double volume=0;
                    if (bill is WaterBillDocument)
                    {
                        WaterBillDocument wb = (WaterBillDocument)bill;
                        if (wb.period.id == periodID)
                        {
                            type = SummaryItemType.ThisMonthBill;
                        }
                        else
                            type = SummaryItemType.OtherMonthBill;
                        consumption=wb.getBillItemAmountByType(0);
                        rent=wb.getBillItemAmountByType(1);
                        serviceCharge=wb.getBillItemAmountByType(2);
                        volume=wb.reading==null?0:wb.reading.consumption;
                    }
                    else
                        type = SummaryItemType.OtherPayments;
                    string key = type + "." + bill.DocumentTypeID;
                    SummaryItem si;
                    if (summary.ContainsKey(key))
                        si = summary[key];
                    else
                    {
                        si = new SummaryItem();
                        si.type = type;
                        si.documentType = bill.DocumentTypeID;
                        summary.Add(si.getKey, si);
                    }
                    si.amount += bill.total;
                    si.consumption += consumption;
                    si.rent += rent;
                    si.serviceCharge += serviceCharge;
                    si.volume += volume;
                }
            }
            
            ListData v = new ListData(summary.Count);
            int i = 0;
            foreach (KeyValuePair<string, SummaryItem> si in summary)
            {
                ListData ld = new ListData(6);
                string description;
                switch(si.Value.type)
                {
                    case SummaryItemType.ThisMonthBill:
                        description="Water Bill for "+prd.name;
                        break;
                    case SummaryItemType.OtherMonthBill:
                        description="Outsanding Bills";
                        break;
                    default:
                        description=m_finance.GetDocumentTypeByID(si.Value.documentType).name;
                        break;
                }
                ld[0] = new EData(DataType.Text, description);
                ld[1] = new EData(DataType.Float, si.Value.volume);
                ld[2] = new EData(DataType.Float, si.Value.consumption);
                ld[3] = new EData(DataType.Float, si.Value.rent);
                ld[4] = new EData(DataType.Float, si.Value.serviceCharge);
                ld[5] = new EData(DataType.Float, si.Value.amount);
                v[i] = new EData(DataType.ListData, ld);
                i++;
            }
            return new EData(DataType.ListData, v);
        }

        public string Name
        {
            get
            {
                return "Collection Summary";
            }
        }

        public int ParCount
        {
            get
            {
                return 4;
            }
        }

        public string Symbol
        {
            get
            {
                return "CollSum";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }

}

