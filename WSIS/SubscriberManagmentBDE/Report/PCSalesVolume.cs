
    using INTAPS.Evaluator;
    using INTAPS.SubscriberManagment;
    using System;
using INTAPS.Accounting.BDE;
namespace INTAPS.SubscriberManagment.BDE
{

    public class PCSalesVolume : IFunction
    {
        private AccountingBDE m_finance;
        private SubscriberManagmentBDE m_subsc;

        public PCSalesVolume(SubscriberManagmentBDE subsc, AccountingBDE bde)
        {
            this.m_finance = bde;
            this.m_subsc = subsc;
        }

        public EData Evaluate(EData[] Pars)
        {
            throw new NotImplementedException("The function is not upgraded to the new billing framework");
            //int accountID = (int) Convert.ChangeType(Pars[0].Value, typeof(int));
            //DateTime from = (DateTime) Pars[1].Value;
            //DateTime to = (DateTime) Pars[2].Value;
            //int[] docTypes = new int[] { this.m_finance.GetDocumentTypeByType(typeof(Receipt)).id, this.m_finance.GetDocumentTypeByType(typeof(WaterBillReceipt)).id};
            //return new EData(DataType.Float, this.m_finance.SumTransactions(accountID, 0, true, from, to, docTypes));
        }

        public string Name
        {
            get
            {
                return "Sales Volume";
            }
        }

        public int ParCount
        {
            get
            {
                return 3;
            }
        }

        public string Symbol
        {
            get
            {
                return "SalesVol";
            }
        }

        public FunctionType Type
        {
            get
            {
                return FunctionType.PreFix;
            }
        }
    }



    
}

