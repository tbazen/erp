using INTAPS.Accounting.BDE;
using INTAPS.Evaluator;
using System;
namespace INTAPS.SubscriberManagment.BDE
{

    internal class PaymentCenterReport : DateFilterProcessor
    {
        public override object[] GetDefaultPars()
        {
            object[] defaultPars = base.GetDefaultPars();
            object[] array = new object[defaultPars.Length + 2];
            defaultPars.CopyTo(array, 0);
            array[defaultPars.Length] = -1;
            array[defaultPars.Length + 1] = -1;
            return array;
        }

        public override void PrepareEvaluator(EHTML data, params object[] paramter)
        {
            int periodID = Convert.ToInt32(paramter[3]);
            data.AddVariable("prd", new EData(DataType.Int, periodID));

            int paymentCenterID = Convert.ToInt32(paramter[2]);
            data.AddVariable("pcID", new EData(DataType.Int, paymentCenterID));
            base.PrepareEvaluator(data, paramter);
        }
    }
}