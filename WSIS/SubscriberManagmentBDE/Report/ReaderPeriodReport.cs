
    using INTAPS.Accounting.BDE;
    using INTAPS.Evaluator;
    using System;
namespace INTAPS.SubscriberManagment.BDE
{

    public class ReaderPeriodReport : ReportProcessorBase
    {
        public override object[] GetDefaultPars()
        {
            return new object[] { -1, -1 };
        }

        public override void PrepareEvaluator(EHTML data, params object[] paramter)
        {
            int v = Convert.ToInt32(paramter[0]);
            int num2 = Convert.ToInt32(paramter[1]);
            data.AddVariable("periodID", new EData(DataType.Int, v));
            data.AddVariable("readerID", new EData(DataType.Int, num2));
            base.PrepareEvaluator(data, paramter);
        }
    }
}

