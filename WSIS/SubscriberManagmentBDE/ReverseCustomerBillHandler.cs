using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.BDE
{
    public class ReverseCustomerBillHandler : INTAPS.Accounting.IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if (_bdeSubsc == null)
                    _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public ReverseCustomerBillHandler(AccountingBDE accounting)
        {
            _accounting = accounting;


        }

        public virtual bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return false;
        }
        public virtual void DeleteDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Bill reversal document can't be deleted");
        }
        public string GetHTML(Accounting.AccountDocument _doc)
        {
            ReverseCustomerBillDocument cb = (ReverseCustomerBillDocument)_doc;
            return "Bill Reversal";
        }

        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    ReverseCustomerBillDocument cb = (ReverseCustomerBillDocument)_doc;

                    if (cb.AccountDocumentID != -1)
                        _accounting.DeleteAccountDocument(AID, cb.AccountDocumentID, true);
                    CustomerBillDocument bill = _accounting.GetAccountDocument(cb.billID,true) as CustomerBillDocument;
                    if (bill.reversed)
                        throw new ServerUserMessage("The bill is already reversed");
                    bdeSubsc.deleteCustomerBillRecord(AID, bill.AccountDocumentID);
                    bill.reversed = true;
                    _accounting.UpdateAccountDocumentData(AID, bill);
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(bill.AccountDocumentID,bill));
                    AccountTransaction[] tran=_accounting.GetTransactionsOfDocument(bill.AccountDocumentID);
                    TransactionOfBatch[] reverse = new TransactionOfBatch[tran.Length];
                    for (int j = 0; j < tran.Length; j++)
                    {
                        reverse[j] = new TransactionOfBatch(tran[j]);
                        reverse[j].Amount = -reverse[j].Amount;
                        reverse[j].Note = "Bill Reversal";
                    }
                    TransactionSummerizer sum = new TransactionSummerizer(this._accounting, _bdeSubsc.bERP.SysPars.summaryRoots);
                    sum.addTransaction(AID, reverse);
                    reverse=ArrayExtension.mergeArray(reverse, sum.getSummary());
                    cb.AccountDocumentID = _accounting.RecordTransaction(AID, cb, reverse);
                    _accounting.WriterHelper.CommitTransaction();
                    return cb.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Reverse transaction not supported");
        }
    }

    
}
