using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Accounting;
namespace INTAPS.SubscriberManagment.BDE
{
    public class ReverseCustomerPaymentReceiptHandler : INTAPS.Accounting.IDocumentServerHandler
    {
        AccountingBDE _accounting;
        SubscriberManagmentBDE bdeSubsc
        {
            get
            {
                if (_bdeSubsc == null)
                    _bdeSubsc = ApplicationServer.GetBDE("Subscriber") as SubscriberManagmentBDE;
                return _bdeSubsc;
            }
        }
        SubscriberManagmentBDE _bdeSubsc = null;
        public ReverseCustomerPaymentReceiptHandler(AccountingBDE accounting)
        {
            _accounting = accounting;
        }
        public virtual bool CheckPostPermission(int docID, Accounting.AccountDocument _doc, ClientServer.UserSessionData userSession)
        {
            return false;
        }
        public virtual void DeleteDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Receipt reversal document can't be deleted");
        }
        public string GetHTML(Accounting.AccountDocument _doc)
        {
            ReverseCustomerPaymentReceiptDocument  cb = (ReverseCustomerPaymentReceiptDocument )_doc;
            return "Receipt Reversal";
        }

        public int Post(int AID, Accounting.AccountDocument _doc)
        {
            lock (_accounting.WriterHelper)
            {
                _accounting.WriterHelper.BeginTransaction();
                try
                {
                    ReverseCustomerPaymentReceiptDocument  rdoc = (ReverseCustomerPaymentReceiptDocument )_doc;

                    if (rdoc.AccountDocumentID != -1)
                        _accounting.DeleteAccountDocument(AID, rdoc.AccountDocumentID, true);
                    CustomerPaymentReceipt receipt = _accounting.GetAccountDocument(rdoc.receiptID, true) as CustomerPaymentReceipt;
                    if (receipt.reversed)
                        throw new ServerUserMessage("The receipt is already reversed");
                    receipt.reversed = true;
                    _accounting.UpdateAccountDocumentData(AID, receipt);
                    AccountTransaction[] tran = _accounting.GetTransactionsOfDocument(receipt.AccountDocumentID);
                    TransactionOfBatch[] reverse = new TransactionOfBatch[tran.Length];
                    for (int j = 0; j < tran.Length; j++)
                    {
                        reverse[j] = new TransactionOfBatch(tran[j]);
                        reverse[j].Amount = -reverse[j].Amount;
                        reverse[j].Note = "Receipt Reversal";
                    }
                    TransactionSummerizer sum = new TransactionSummerizer(this._accounting, bdeSubsc.bERP.SysPars.summaryRoots);
                    sum.addTransaction(AID, reverse);
                    reverse = ArrayExtension.mergeArray(reverse, sum.getSummary());

                    rdoc.AccountDocumentID = _accounting.RecordTransaction(AID, rdoc, reverse);
                    
                    CustomerBillRecord[] record = bdeSubsc.getBillRecordsByPayment(receipt.AccountDocumentID);
                    foreach (CustomerBillRecord r in record)
                    {
                        r.paymentDocumentID = -1;
                        bdeSubsc.WriterHelper.logDeletedData(AID, bdeSubsc.DBName + ".dbo.CustomerBill", "id=" + r.id);
                        bdeSubsc.WriterHelper.UpdateSingleTableRecord(bdeSubsc.DBName, r, new string[] { "__AID" }, new object[] { AID }, "id=" + r.id);
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(r.id, r));
                    }
                    foreach (int d in receipt.bankDepsitDocuments)
                        _accounting.DeleteAccountDocument(AID, d, false);

                    _accounting.WriterHelper.CommitTransaction();
                    return rdoc.AccountDocumentID;
                }
                catch
                {
                    _accounting.WriterHelper.RollBackTransaction();
                    throw;
                }
            }
        }
        public void ReverseDocument(int AID, int docID)
        {
            throw new ServerUserMessage("Reverse transaction not supported");
        }
    }
}
