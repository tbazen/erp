﻿using System;
using System.Collections.Generic;
using System.Text;
using INTAPS.Evaluator;

namespace INTAPS.SubscriberManagment.BDE
{
    class SMSettingFunction:IFunction
    {
        SubscriberManagmentBDE _bde;
        public SMSettingFunction(SubscriberManagmentBDE bde)
        {
            _bde = bde;
        }
        public EData Evaluate(EData[] Pars)
        {
            string parName = Pars[0].Value as string;
            if (parName == null)
                return new FSError("Invalid setting name-SMSetting").ToEData();
            object val=_bde.GetSystemParameters(new string[] { parName })[0];
            return Accounting.BDE.AccountingEvaluatorUtils.toEData(val);

        }

        public string Name
        {
            get { return "Get subscriber managment module setting"; }
        }

        public int ParCount
        {
            get { return 1; }
        }

        public string Symbol
        {
            get { return "SMSetting"; }
        }

        public FunctionType Type
        {
            get { return FunctionType.PreFix; }
        }
    }
}
