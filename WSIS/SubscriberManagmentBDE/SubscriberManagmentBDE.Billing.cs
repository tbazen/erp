using INTAPS;
using INTAPS.Accounting;
using INTAPS.Accounting.BDE;
using INTAPS.ClientServer;
using INTAPS.Evaluator;
using INTAPS.Payroll;
using INTAPS.Payroll.BDE;
using INTAPS.RDBMS;
using INTAPS.SubscriberManagment;
using INTAPS.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using BIZNET.iERP;
namespace INTAPS.SubscriberManagment.BDE
{
    public partial class SubscriberManagmentBDE : BDEBase
    {
        IBillingRule _billingRule;
        public IBillingRule getRule()
        {
            return _billingRule;
        }
        void initializeBillingSubsystem()
        {
            loadBillingRuleEngine();
        }
        void loadBillingRuleEngine()
        {

            string an = System.Configuration.ConfigurationManager.AppSettings["billingRuleAssembly"];
            if (an != null)
            {
                Assembly a = null;


                if (a == null)
                {
                    try
                    {
                        a = System.Reflection.Assembly.Load(an);
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        a = null;
                    }
                }

                if (a == null)
                {
                    try
                    {
                        a = System.Reflection.Assembly.LoadFile(System.IO.Path.Combine(Environment.CurrentDirectory, an));
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        a = null;
                    }
                }
                
                if (a == null)
                    ApplicationServer.EventLoger.Log(EventLogType.Errors, $"Failed to load billingRuleAssembly: {an}");
                try
                {
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.GetInterface(typeof(IBillingRule).Name)!=null)
                        {
                            ConstructorInfo ci = t.GetConstructor(new Type[] { typeof(SubscriberManagmentBDE) });
                            if (ci == null)
                            {
                                ApplicationServer.EventLoger.Log(EventLogType.Errors, "Constructor not found for type ");
                                continue;
                            }
                            try
                            {
                                IBillingRule o = ci.Invoke(new object[] { this }) as IBillingRule;
                                if (o == null)
                                {
                                    ApplicationServer.EventLoger.Log(EventLogType.Errors, "Billing rule handler " + t + " couldn't be loaded");
                                    continue;
                                }
                                _billingRule = o;
                                ApplicationServer.EventLoger.Log(EventLogType.ServerActions, "Billing rule handler " + t + " loaded succesfully.");
                                return;
                            }
                            catch (Exception tex)
                            {
                                ApplicationServer.EventLoger.LogException("Error trying to instantiate billing rule handler " + t, tex);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ApplicationServer.EventLoger.LogException("Failed to load billing rule handler", ex);
                }
            }
            ApplicationServer.EventLoger.Log(EventLogType.Errors, "No billing rule handler found");
        }
        public int addBill(int AID, CustomerBillDocument bill, BillItem[] items)
        {
            CustomerBillRecord record = new CustomerBillRecord();
            record.billDate = bill.DocumentDate;
            record.billDocumentTypeID = bdeAccounting.GetDocumentTypeByType(bill.GetType()).id;
            record.customerID = bill.customer.id;
            record.connectionID = -1;
            record.periodID = -1;
            record.draft = bill.draft;
            return addBill(AID, bill, record, items);
        }
        public int addBill(int AID, CustomerBillDocument bill, BillItem[] items,int connectionID)
        {
            CustomerBillRecord record = new CustomerBillRecord();
            record.billDate = bill.DocumentDate;
            record.billDocumentTypeID = bdeAccounting.GetDocumentTypeByType(bill.GetType()).id;
            record.customerID = bill.customer.id;
            record.connectionID = connectionID;
            record.periodID = -1;
            record.draft = bill.draft;
            return addBill(AID, bill, record, items);
        }
        public double getUnpaidSettlementFromDeposit(DateTime date, int customerID)
        {
            string sql = @"SELECT     isnull(sum(settledFromDepositAmount),0)
                        FROM         {0}.dbo.CustomerBill INNER JOIN
                      {0}.dbo.CustomerBillItem ON CustomerBill.id = CustomerBillItem.customerBillID
                      where CustomerID={1} and paymentDocumentID=-1 and billDate>='{2}'";
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return (double)dspReader.ExecuteScalar(string.Format(sql, this.DBName, customerID, date));
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public CreditScheme getCreditScheme(int schemeID)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CreditScheme[] ret= dspReader.GetSTRArrayByFilter<CreditScheme>("id=" + schemeID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public CreditScheme[] getCustomerCreditSchemes(int customerID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<CreditScheme>("customerID=" + customerID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public int addNoneBillCreditScheme(int AID, CreditScheme scheme)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    BillPeriod bp = GetBillPeriod(scheme.startPeriodID);
                    Subscriber customer = GetSubscriber(scheme.customerID);
                    customer.accountID = getOrCreateCustomerAccountID(AID, customer);
                    int csid = bdeAccounting.GetOrCreateCostCenterAccount(AID, bERP.SysPars.mainCostCenterID, customer.accountID).id;
                    scheme.id = AutoIncrement.GetKey("Subscriber.CreditSchemeID");
                    scheme.active = true;
                    dspWriter.InsertSingleTableRecord(this.DBName, scheme, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                    return scheme.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        public void closeScheme(int AID, int schemeID)
        {
            CreditScheme scheme= getCreditScheme(schemeID);
            if (scheme == null)
                throw new ServerUserMessage("Invlaid credit scheme {0}", schemeID);
            if (!scheme.active)
                throw new ServerUserMessage("Credit scheme already closed");
            scheme.active = false;
            dspWriter.logDeletedData(AID, this.DBName + ".dbo.CreditScheme", "id=" + schemeID);
            dspWriter.UpdateSingleTableRecord(this.DBName, scheme, new string[] { "__AID" }, new object[] { AID });
        }
        public int differePayment(int AID, int[] bills,CreditScheme scheme)
        {
            //if(bills.Length==0)
              //  throw new ServerUserMessage("At least one bill should be included in the credit scheme");
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                    
                {
                    BillPeriod bp=GetBillPeriod(scheme.startPeriodID);
                    /*if(scheme.issueDate>=bp.toDate)
                        throw new ServerUserMessage("Credit settlement should can't start in past periods");*/
                    Subscriber customer=GetSubscriber(scheme.customerID);
                    customer.accountID = getOrCreateCustomerAccountID(AID, customer);
                    int csid = bdeAccounting.GetOrCreateCostCenterAccount(AID, bERP.SysPars.mainCostCenterID, customer.accountID).id;
                    double totalAmount=bdeAccounting.GetNetBalanceAsOf(csid,  0,scheme.issueDate);
                    foreach (int id in bills)
                    {
                        CustomerBillRecord rec = getCustomerBillRecord(id);
                        if (rec.draft)
                            throw new ServerUserMessage("Draft bills can't be used for this operation");
                        if (rec.isPayedOrDiffered)
                            throw new ServerUserMessage("Only payment of unpaid bills can be differed");
                        if (scheme.issueDate < rec.postDate)
                            throw new ServerUserMessage("Credit scheme can't be issued for future bills");
                            if(scheme.customerID!=rec.customerID)
                                throw new ServerUserMessage("All bills should be of a single customer");
                        rec.paymentDiffered = true;
                        rec.paymentDate = scheme.issueDate;
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.CustomerBill", "id=" + rec.id);
                        dspWriter.UpdateSingleTableRecord(this.DBName, rec, new string[] { "__AID" }, new object[] { AID }, "id=" + rec.id);
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(rec.id, rec));
                        foreach(BillItem item in getCustomerBillItems(rec.id))
                            totalAmount+=item.netPayment;
                    }
                    if(AccountBase.AmountGreater(scheme.paymentAmount,totalAmount) || !AccountBase.AmountGreater(scheme.paymentAmount,0))
                        throw new ServerUserMessage("Invalid payment amount "+scheme.paymentAmount);
                    scheme.id = AutoIncrement.GetKey("Subscriber.CreditSchemeID");
                    scheme.creditedAmount = totalAmount;
                    scheme.active = true;
                    dspWriter.InsertSingleTableRecord(this.DBName, scheme,new string[]{"__AID"},new object[]{AID});
                    foreach (int id in bills)
                        dspWriter.Insert(this.DBName, "DifferedBill", new string[] { "creditSchemeID", "billRecordID", "__AID" }
                            , new object[] { scheme.id, id, AID });
                    dspWriter.CommitTransaction();
                    return scheme.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void deleteCreditScheme(int AID, int schemeID, bool deleteBills)
        {
            CreditScheme scheme = getCreditScheme(schemeID);
            CustomerBillDocument[] creditBills = getBillDocuments(bdeAccounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id, scheme.customerID, -1, -1, false);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {

                    List<CreditBillDocument> deleteList = new List<CreditBillDocument>();
                    foreach (CreditBillDocument c in creditBills)
                    {
                        if (c.creditSchemeID == schemeID)
                        {
                            if(!deleteBills)
                                throw new ServerUserMessage("The credit scheme can't be deleted because credit bills are already generated");
                            if (isBillPaid(c.AccountDocumentID)) 
                                throw new ServerUserMessage("The credit scheme can't be deleted because some of the credit is already settled");
                            deleteList.Add(c);
                        }
                    }
                    foreach (CreditBillDocument d in deleteList)
                        Accounting.DeleteGenericDocument(AID, d.AccountDocumentID);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.DifferedBill", "creditSchemeID=" + schemeID);
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.CreditScheme", "id=" + schemeID);
                    int [] differdBills=dspWriter.GetColumnArray<int>(string.Format("Select billRecordID from {0}.dbo.DifferedBill where creditSchemeID={1}",this.DBName,schemeID));
                    dspWriter.Delete(this.DBName, "DifferedBill", "creditSchemeID=" + schemeID);
                    foreach(int db in differdBills)
                    {
                        dspWriter.logDeletedData(AID,this.DBName+".dbo.CustomerBill","id="+db);
                        dspWriter.Update(this.DBName,"CustomerBill", new string[]{"paymentDiffered","__AID"},new object[]{false,AID},"id="+db);
                    }
                    dspWriter.Delete(this.DBName, "CreditScheme", "id=" + schemeID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public CreditScheme[] getCustomerCreditSchemes(int customerID, out double[] settlement)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CustomerBillDocument[] creditBills = getBillDocuments(bdeAccounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id, customerID, -1, -1,false);
                if (creditBills == null)
                    creditBills = new CreditBillDocument[0];
                CreditScheme[] allSchemes = dspReader.GetSTRArrayByFilter<CreditScheme>("customerID=" + customerID);
                settlement = new double[allSchemes.Length];
                for (int i = 0; i < allSchemes.Length; i++)
                {
                    settlement[i]=0;
                    foreach(CreditBillDocument c in creditBills)
                    {
                        if (c.creditSchemeID == allSchemes[i].id)
                        {
                            settlement[i] += c.amount;
                        }
                    }
                }
                return allSchemes;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }



        void assertNoNullCreditScheme(CustomerBillDocument bill)
        {
            if(bill is CreditBillDocument)
            {
                if (((CreditBillDocument)bill).creditSchemeID < 1)
                    throw new ServerUserMessage("BUG:Credit scheme must be set for credits bills");
            }
        }
        public int addBill(int AID,CustomerBillDocument bill, CustomerBillRecord record, BillItem[] items)
        {
            assertNoNullCreditScheme(bill);
            if (bill.customer == null)
                throw new ServerUserMessage("Customer data not set for the bill");
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    double currentBalance = -this.bdeAccounting.GetNetBalanceAsOf(bERP.SysPars.mainCostCenterID, bill.customer.accountID, 0, bill.DocumentDate.AddSeconds(1));

                    bill.AccountDocumentID=record.id= Accounting.PostGenericDocument(AID, bill);
                    record.postDate = bill.DocumentDate;
                    dspWriter.InsertSingleTableRecord(this.DBName, record, new string[] { "__AID" }, new object[] { AID });
                    foreach (BillItem bi in items)
                    {
                        bi.customerBillID = record.id;
                        if (bi.accounting == BillItemAccounting.Invoice && AccountBase.AmountGreater(currentBalance, 0))
                        {
                            bi.settledFromDepositAmount = Math.Min(bi.price, currentBalance);
                            currentBalance -= bi.settledFromDepositAmount;
                        }
                        dspWriter.InsertSingleTableRecord(this.DBName, bi, new string[]{"__AID"},new object[]{AID});
                    }
                    


                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(bill.AccountDocumentID, bill));
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(record.id, record));
                    foreach (BillItem bi in items)
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getAddDiff(bi.customerBillID, bi.itemTypeID, bi));
                    dspWriter.CommitTransaction();
                    return record.id;
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        
        public void deleteCustomerBillRecord(int AID, int billRecordID)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    BillItem[] items = getCustomerBillItems(billRecordID);
                    foreach (BillItem item in items)
                    {
                        this.WriterHelper.logDeletedData(AID, this.DBName + ".dbo.CustomerBillItem", "customerBillID=" + billRecordID + " and itemTypeID=" + item.itemTypeID);
                        dspWriter.Delete(this.DBName, "CustomerBillItem", "customerBillID=" + billRecordID + " and itemTypeID=" + item.itemTypeID);
                        ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(typeof(BillItem), item.customerBillID, item.itemTypeID));
                    }
                    this.WriterHelper.logDeletedData(AID, this.DBName + ".dbo.CustomerBill", "id=" + billRecordID);
                    dspWriter.Delete(this.DBName, "CustomerBill", "id=" + billRecordID);
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getDeleteDiff(typeof(CustomerBillRecord), billRecordID));
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void deleteCustomerBill(int AID, int billRecordID, params DocumentTypedReference[] refs)
        {
            deleteCustomerBill(AID, billRecordID, DateTime.Now, refs);
        }
        public void deleteCustomerBill(int AID, int billRecordID,DateTime date, params DocumentTypedReference []refs)
        {
            CustomerBillRecord rec = getCustomerBillRecord(billRecordID);
            if (rec == null)
                throw new ServerUserMessage("Invalid customer bill record ID:" + billRecordID);
            if (rec.paymentDiffered || rec.paymentDocumentID != -1)
                throw new ServerUserMessage("Paid or differed bill can't be deleted");
            CustomerBillDocument bill = Accounting.GetAccountDocument<CustomerBillDocument>(billRecordID);
            if (bill.billBatchID == -1)
                Accounting.DeleteGenericDocument(AID, rec.id);
            else
            {
                ReverseCustomerBillDocument reverse = new ReverseCustomerBillDocument();
                reverse.ShortDescription = "Reverse of " + bill.ShortDescription;
                reverse.billID = bill.AccountDocumentID;
                reverse.DocumentDate = date;
                reverse.AccountDocumentID=Accounting.PostGenericDocument(AID, reverse);
                Accounting.addDocumentSerials(AID, reverse, refs);
            }
        }
        // BDE exposed
        public BatchJobResult[] deleteUnpaidPeriodicBillByKebele(int AID,int periodID, int[] kebeles)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            int[] billsIDs;
            List<BatchJobResult> ret = new List<BatchJobResult>();
            Accounting.PushProgress("Retrieving bills list");
            try
            {
                if (kebeles == null || kebeles.Length == 0)
                    billsIDs = dspReader.GetColumnArray<int>(string.Format("Select b.id from {0}.dbo.Subscriber s inner join {0}.dbo.CustomerBill b on s.id=b.customerID where periodID={1}", this.DBName, periodID), 0);
                else
                {
                    string cr = null;
                    foreach (int k in kebeles)
                        cr = INTAPS.StringExtensions.AppendOperand(cr, ",", k.ToString());
                    billsIDs = dspReader.GetColumnArray<int>(string.Format("Select b.id from {0}.dbo.Subscriber s inner join {0}.dbo.CustomerBill b on s.id=b.customerID where kebele in ({1}) and periodID={2}", this.DBName, cr, periodID), 0);
                    
                    
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
                Accounting.PopProgress();
            }
            Accounting.PushProgress("Deleting bills");
            try
            {
                int i = -1;
                foreach (int billID in billsIDs)
                {
                    i++;
                    try
                    {
                        Accounting.SetChildWeight(1 / (double)billsIDs.Length);
                        CustomerBillRecord rec = getCustomerBillRecord(billID);
                        if (rec.isPayedOrDiffered)
                            continue;
                        if (!Accounting.GetDocumentTypeByID(rec.billDocumentTypeID).GetTypeObject().IsSubclassOf(typeof(PeriodicBill)))
                            continue;
                        deleteCustomerBill(AID, billID);
                        Accounting.SetProgress((double)(i + 1) / (double)billsIDs.Length);
                    }
                    catch (Exception ex)
                    {
                        ret.Add(new BatchJobResult(billID.ToString(), billID.ToString(), ex.Message, ex));
                    }
                }
                return ret.ToArray();
            }
            finally
            {
                Accounting.PopProgress();
            }
            
        }
        // BDE exposed
        public void postBill(int AID, DateTime time, int billRecordID)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    CustomerBillRecord brec = getCustomerBillRecord(billRecordID);
                    if (!brec.draft)
                        throw new ServerUserMessage("Bill record id:" + brec.id + " already posted");

                    CustomerBillDocument doc = Accounting.GetAccountDocument(billRecordID, true) as CustomerBillDocument;
                    doc.draft = false;
                    brec.draft = false;
                    brec.postDate = time;
                    Accounting.PostGenericDocument(AID, doc);
                    dspWriter.UpdateSingleTableRecord(this.DBName, brec, new string[] { "__AID" }, new object[] { AID });
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(doc.AccountDocumentID, doc));
                    ApplicationServer.sendDiffMsg(AID, GenericDiff.getUpdateDiff(brec.id, brec));
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public BatchJobResult[] postBills(int AID, DateTime time, int periodID, int[] billIDs)
        {
            Accounting.PushProgress("Posting bills");
            List<BatchJobResult> ret = new List<BatchJobResult>();
            try
            {
                int i = -1;
                foreach (int billID in billIDs)
                {
                    i++;
                    try
                    {
                        Accounting.SetChildWeight(1 / (double)billIDs.Length);
                        postBill(AID, time, billID);
                    }
                    catch (Exception ex)
                    {
                        ret.Add(new BatchJobResult(billID.ToString(), billID.ToString(), ex.Message, ex));
                    }
                }
                return ret.ToArray();
            }
            finally
            {
                Accounting.PopProgress();
            }
        }
        // BDE exposed
        public BatchJobResult[] postBillsByKebele(int AID, DateTime time, int periodID, int[] kebeles)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            int[] billsIDs;
            List<BatchJobResult> ret = new List<BatchJobResult>();
            Accounting.PushProgress("Retrieving bills list");
            try
            {
                if (kebeles == null || kebeles.Length == 0)
                    billsIDs = dspReader.GetColumnArray<int>(string.Format("Select b.id from {0}.dbo.Subscriber s inner join {0}.dbo.CustomerBill b on s.id=b.customerID where periodID={1} and draft=1", this.DBName, periodID), 0);
                else
                {
                    string cr = null;
                    foreach (int k in kebeles)
                        cr = INTAPS.StringExtensions.AppendOperand(cr, ",", k.ToString());
                    billsIDs = dspReader.GetColumnArray<int>(string.Format("Select b.id from {0}.dbo.Subscriber s inner join {0}.dbo.CustomerBill b on s.id=b.customerID where kebele in ({1}) and periodID={2}and draft=1", this.DBName, cr, periodID), 0);


                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
                Accounting.PopProgress();
            }
            Accounting.PushProgress("Posting bills");
            try
            {
                int i = -1;
                foreach (int billID in billsIDs)
                {
                    i++;
                    try
                    {
                        Accounting.SetChildWeight(1 / (double)billsIDs.Length);
                        postBill(AID, time, billID);
                    }
                    catch (Exception ex)
                    {
                        ret.Add(new BatchJobResult(billID.ToString(), billID.ToString(), ex.Message, ex));
                    }
                }
                return ret.ToArray();
            }
            finally
            {
                Accounting.PopProgress();
            }
        }
        public bool generateMonthlyBill(DateTime time, BillPeriod period, Subscriber customer,out CustomerBillDocument[] docs,  out CustomerBillRecord[] bills, out BillItem[][] items, out string message)
        {
            verifyBillingSubsystem();
            bills = null;
            docs = null;
            items = null;
            message = null;
            if (!_billingRule.buildMonthlyBill(customer, time,period, out docs, out bills, out items, out message))
                return false;
            
            //credit
            int billTypeID = Accounting.GetDocumentTypeByType(typeof(CreditBillDocument)).id;
            double receivable = Accounting.GetNetBalanceAsOf(bdeBERP.SysPars.mainCostCenterID, customer.accountID, 0, time);

            CustomerBillDocument[] existingCreditBills = getBillDocuments(billTypeID, customer.id,-1, period.id,false);
            if (existingCreditBills == null)
                existingCreditBills = new CreditBillDocument[0];
            foreach (CustomerBillDocument cr in existingCreditBills)
                if (!getCustomerBillRecord(cr.AccountDocumentID).isPayedOrDiffered)
                    receivable -= cr.total;

            double[] settlments;
            CreditScheme[] scheme=getCustomerCreditSchemes(customer.id,out settlments);
            for (int i = 0; i < scheme.Length && AccountBase.AmountGreater(receivable,0); i++)
            {
                if (!scheme[i].active  || !AccountBase.AmountLess(settlments[i], scheme[i].creditedAmount))
                    continue;
                bool generated = false;
                foreach (CreditBillDocument e in existingCreditBills)
                {
                    if (e.creditSchemeID == scheme[i].id)
                    {
                        generated = true;
                        break;
                    }
                }
                if (generated)
                    continue;

                CustomerBillRecord mainBill = new CustomerBillRecord();
                mainBill.billDocumentTypeID = billTypeID;
                mainBill.connectionID = -1;
                mainBill.customerID = customer.id;
                mainBill.periodID = period.id;
                mainBill.billDate = time;
                
                CreditBillDocument doc = new CreditBillDocument();
                doc.creditSchemeID = scheme[i].id;
                doc.period = period;
                doc.customer = customer;
                doc.amount = Math.Min(scheme[i].creditedAmount - settlments[i], scheme[i].paymentAmount);
                doc.ShortDescription = "Credit bill for customer " + customer.customerCode + " period:" + doc.period.name;
                docs=ArrayExtension.appendToArray<CustomerBillDocument>(docs, doc);
                bills=ArrayExtension.appendToArray<CustomerBillRecord>(bills, mainBill);
                items=ArrayExtension.appendToArray<BillItem[]>(items, doc.itemsLessExemption.ToArray());
                receivable -= doc.total;
            }
            
            return true;
        }

        public SubscriptionStatus GetSubscriptionStatusAtDate(int subscriptionID, DateTime dateTime)
        {
            return GetSubscription(subscriptionID, dateTime.Ticks).subscriptionStatus;
        }
        private int[] getUnpaidBillIDs(int customerID)
        {
             INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetColumnArray<int>(
                    string.Format("Select id from {0}.dbo.CustomerBill where customerID={1} and draft=0 and paymentDocumentID=-1 and paymentDiffered=0", this.DBName, customerID)
                    ,0);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CustomerBillRecord[] getBillRecordsByPayment(int paymentDocumentID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<CustomerBillRecord>("paymentDocumentID="+paymentDocumentID);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public CustomerBillRecord[] getBills(int mainTypeID, int customerID, int connectionID, int periodID)
        {
            string cr = null;
            if (mainTypeID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "billDocumentTypeID=" + mainTypeID);
            if (customerID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "customerID=" + customerID);
            if (connectionID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "connectionID=" + connectionID);
            if (periodID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "periodID=" + periodID);
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<CustomerBillRecord>(cr);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public CustomerBillDocument[] getBillDocuments(int mainTypeID, int customerID, int connectionID, int periodID, bool exculdePaid)
        {
            string cr = null;
            if (mainTypeID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "billDocumentTypeID=" + mainTypeID);
            if (customerID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "customerID=" + customerID);
            if (connectionID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "connectionID=" + connectionID);
            if (periodID != -1)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "periodID=" + periodID);
            if(exculdePaid)
                cr = INTAPS.StringExtensions.AppendOperand(cr, "AND", "paymentDocumentID=-1 AND paymentDiffered=0");
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                int[] billIDs = dspReader.GetColumnArray<int>(string.Format("Select id from {0}.dbo.CustomerBill {1}", this.DBName, cr == null ? "" : " where " + cr), 0);
                List<CustomerBillDocument> ret = new List<CustomerBillDocument>();
                foreach(int id in billIDs)
                    ret.Add(Accounting.GetAccountDocument(id,true) as CustomerBillDocument);
                return ret.ToArray();
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public CustomerBillRecord getCustomerBillRecord(int billRecordID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CustomerBillRecord[] ret = dspReader.GetSTRArrayByFilter<CustomerBillRecord>("id=" + billRecordID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public CustomerBillRecord getCustomerBillRecordByAccountDocument(int billRecordAccountDocumentID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                CustomerBillRecord[] ret = dspReader.GetSTRArrayByFilter<CustomerBillRecord>("acountDocumentID=" + billRecordAccountDocumentID);
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public BillItem[] getCustomerBillItems(int billRecordID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BillItem[] ret = dspReader.GetSTRArrayByFilter<BillItem>("customerBillID=" + billRecordID);
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        private void verifyBillingSubsystem()
        {
            if (_billingRule == null)
                throw new ServerUserMessage("Billing subsystem not initialized");
        }
        bool _cancelBillGeneration = false;
        public BatchJobResult[] generateBills(int AID, DateTime time, int periodID, int[] custList)
        {
            _cancelBillGeneration = false;
            BillPeriod period = GetBillPeriod(periodID);
            List<BatchJobResult> res = new List<BatchJobResult>();
            Accounting.PushProgress("Generating bills");
            try
            {
                int batchCount=0;   
                for (int i = 0; i < custList.Length; )
                {
                    lock (dspWriter)
                    {
                        dspWriter.BeginTransaction();
                        try
                        {
                            TransactionSummerizer sum=null;
                            if(SysPars.summerizeTransactions)
                                sum= new TransactionSummerizer(this.Accounting, bERP.SysPars.summaryRoots);
                            
                            List<int> batchItems = new List<int>();
                            List<CustomerBillDocument> batchBills = new List<CustomerBillDocument>(); 
                            for (; i < custList.Length && batchItems.Count < SysPars.billBatchSize; i++)
                            {
                                if (_cancelBillGeneration)
                                    break;
                                //Console.CursorTop--;
                                Console.WriteLine("{0}\t{1}             ",(custList.Length - i),batchItems.Count);
                                Subscriber cust = GetSubscriber(custList[i]);

                                try
                                {
                                    CustomerBillDocument[] docs;
                                    CustomerBillRecord[] bills;
                                    BillItem[][] items;
                                    string msg;
                                    bool ok = generateMonthlyBill(time, period, cust, out docs, out bills, out items, out msg);
                                    if (ok)
                                    {
                                        Accounting.SetChildWeight(1 / (double)custList.Length);
                                        for (int j = 0; j < bills.Length; j++)
                                        {
                                            docs[j].draft = false;
                                            bills[j].draft = false;
                                            docs[j].summerizeTransaction = false;
                                            docs[j].billBatchID = -1;
                                            int id = addBill(AID, docs[j], bills[j], items[j]);

                                            if(SysPars.summerizeTransactions)
                                                sum.addTransaction(AID,Accounting.GetTransactionsOfDocument(id));
                                            batchItems.Add(id);
                                            batchBills.Add(docs[j]);
                                        }
                                    }
                                    else
                                        res.Add(new BatchJobResult(cust.customerCode, cust.customerCode, msg, null, BatchJobResultType.Error));
                                    Accounting.SetProgress((double)(i + 1) / (double)custList.Length);
                                }
                                catch (Exception ex)
                                {
                                    res.Add(new BatchJobResult(cust.customerCode, cust.customerCode, "System error", ex));
                                }
                            }

                            if (SysPars.summerizeTransactions)
                            {

                                CustomerBillSummaryDocument summaryBill = new CustomerBillSummaryDocument();
                                summaryBill.ShortDescription = string.Format("Bill Batch summary {0} - {1}", period.name, batchCount + 1);
                                summaryBill.billIDs = batchItems.ToArray();
                                summaryBill.transactions = sum.getSummary();
                                summaryBill.DocumentDate = time;
                                
                                Accounting.PostGenericDocument(AID, summaryBill);
                                
                                batchCount++;
                            }

                            dspWriter.CommitTransaction();
                        }
                        catch
                        {
                            dspWriter.RollBackTransaction();
                            throw;
                        }

                    }
                }
                return res.ToArray();
            }
            finally
            {
                Accounting.PopProgress();
            }
        }

        // BDE exposed
        public void cancelBillGeneration()
        {
            _cancelBillGeneration = true;
        }
        // BDE exposed
       
       
        public BatchJobResult[] generateBillsByKebele(int AID, DateTime time, int periodID, int[] kebeles)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            int[] custList;
            Accounting.PushProgress("Loasing bills list");
            try
            {
                if (kebeles == null || kebeles.Length == 0)
                    custList = dspReader.GetColumnArray<int>(string.Format("Select id from {0}.dbo.Subscriber", this.DBName), 0);
                else
                {
                    string cr = null;
                    foreach (int k in kebeles)
                        cr = INTAPS.StringExtensions.AppendOperand(cr, ",", k.ToString());
                    custList = dspReader.GetColumnArray<int>(string.Format("Select id from {0}.dbo.Subscriber where kebele in ({1})", this.DBName, cr), 0);
                }
            }
            finally
            {
                base.ReleaseHelper(dspReader);
                Accounting.PopProgress();
            }
            return generateBills(AID, time, periodID, custList);
        }
        // BDE exposed
        public BatchJobResult[] generatCustomerPeriodicBills(int AID, DateTime time, int customerID, int periodID)
        {
            BillPeriod period = GetBillPeriod(periodID);
            Subscriber customer = GetSubscriber(customerID);
            CustomerBillDocument[] docs;
            CustomerBillRecord[] bills;
            BillItem[][] items;
            string msg;
            bool res = generateMonthlyBill(time, period, customer, out docs,out bills, out items, out msg);
            if (!res)
                return new BatchJobResult[] { new BatchJobResult("", "", msg, null,BatchJobResultType.Error) };
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    for (int j = 0; j < bills.Length; j++)
                    {
                        docs[j].draft = false;
                        bills[j].draft = false;
                        docs[j].summerizeTransaction = SysPars.summerizeTransactions;
                        addBill(AID, docs[j], bills[j], items[j]);
                    }
                    dspWriter.CommitTransaction();
                    return new BatchJobResult[0];
                }
                catch(Exception ex)
                {
                    dspWriter.RollBackTransaction();
                    return new BatchJobResult[] { new BatchJobResult("", "", "System Error", ex) };
                }
            }
            
        }
        public int getOrCreateCustomerAccountID(int AID, Subscriber customer)
        {
            return getOrCreateCustomerAccountID(AID, customer, false);
        }
        class CustomerAccountFormulaSymbolProvider : INTAPS.Evaluator.ISymbolProvider
        {
            Subscriber customer;
            public CustomerAccountFormulaSymbolProvider(Subscriber customer)
            {
                this.customer = customer;
            }
            public FunctionDocumentation[] GetAvialableFunctions()
            {
                return new FunctionDocumentation[0];
            }

            public EData GetData(URLIden iden)
            {
                return this.GetData(iden.Element.ToUpper());
            }

            public EData GetData(string symbol)
            {
                switch (symbol.ToUpper())
                {
                    case "TYPE":
                        return new EData((int)customer.subscriberType);
                    case "SUBTYPE":
                        return new EData((int)customer.subTypeID);
                    case "KEBELE":
                        return new EData((int)customer.Kebele);
                }
                return EData.Empty;
            }

            public FunctionDocumentation GetDocumentation(IFunction f)
            {
                return null;    
            }

            public FunctionDocumentation GetDocumentation(URLIden iden)
            {
                return null;
            }

            public FunctionDocumentation GetDocumentation(string Symbol)
            {
                return null;
            }

            public IFunction GetFunction(URLIden iden)
            {
                return this.GetFunction(iden.Element);
            }

            public IFunction GetFunction(string symbol)
            {
                if (CalcGlobal.Functions.Contains(symbol.ToUpper()))
                {
                    return (IFunction)CalcGlobal.Functions[symbol.ToUpper()];
                }
                return null;
            }

            public bool SymbolDefined(string Name)
            {
                return (this.GetData(Name).Type != DataType.Empty);
            }
        }
        public int getOrCreateCustomerAccountID(int AID,Subscriber customer,bool deposit)
        {
            if ((deposit?customer.depositAccountID: customer.accountID) <1)
            {
                String formula = deposit ? m_sysPars.customerDepositAccountFormula : m_sysPars.receivableAccountFormula;
                int calculatedAccountID = -1;
                if(!string.IsNullOrEmpty(formula))
                {
                    try
                    {
                        Symbolic s = new Symbolic();
                        s.SetSymbolProvider(new CustomerAccountFormulaSymbolProvider(customer));
                        s.Expression = formula;
                        var res = s.Evaluate();
                        if (res.Value is int || res.Value is double)
                            calculatedAccountID = Convert.ToInt32(res.Value);
                        else if(res.Value is String)
                        {
                            calculatedAccountID = this.Accounting.GetAccountID<Account>((String)res.Value);
                        }
                        else if (res.Type == DataType.Error)
                        {
                            throw new ServerUserMessage($"Error evaluating customer account {(deposit ? "deposit" : "receivable")} formula\n{res.Value}");
                        }
                    }
                    catch(ServerUserMessage)
                    {
                        throw;
                    }
                    catch(Exception ex)
                    {
                        throw new ServerUserMessage($"Error evaluating customer account {(deposit ? "deposit" : "receivable")} formula\n{ex.Message}");
                    }
                }
                int defaultAccountID= deposit ? m_sysPars.customerDepositAccount : m_sysPars.receivableAccount;
                int parentAccountID = calculatedAccountID == -1 ? defaultAccountID : calculatedAccountID;

                Account parent = Accounting.GetAccount<Account>(parentAccountID);
                if (parent == null)
                    throw new ServerUserMessage((deposit?"Customer Deposit Account":"Customer Receivable") +" account is not setup");
                Account ac = new Account();
                ac.Name = parent.Name + "-" + customer.name;
                ac.Code = parent.Code + "-" + customer.customerCode;
                ac.PID = parent.id;
                ac.protection = AccountProtection.SystemAccount;
                ac.ActivateDate = DateTime.Now;
                ac.CreditAccount = false;
                ac.Status = AccountStatus.Activated;
                Account test = bdeAccounting.GetAccount<Account>(ac.Code);
                if (test == null)
                    ac.id = bdeAccounting.CreateAccount(AID, ac);
                else
                {
                    ac.id = test.id;
                    bdeAccounting.UpdateAccount(AID, ac);
                }
                lock (dspWriter)
                {
                    dspWriter.BeginTransaction();
                    try
                    {
                        dspWriter.logDeletedData(AID, this.DBName + ".dbo.Subscriber", "id=" + customer.id);
                        dspWriter.Update(this.DBName, "Subscriber", new string[] { deposit ? "depositAccountID" : "accountID", "__AID" }, new object[] { ac.id, AID }, "id=" + customer.id);
                        dspWriter.CommitTransaction();
                        return ac.id;
                    }
                    catch
                    {
                        dspWriter.RollBackTransaction();
                        throw;
                    }
                }
            }
            else
                return deposit?customer.depositAccountID: customer.accountID;
        }

        // BDE exposed
        public bool isBillPaid(int invoiceNo)
        {
            CustomerBillRecord bill = this.getCustomerBillRecord(invoiceNo);
            return bill != null && bill.isPayedOrDiffered;
        }

        public CustomerPaymentReceipt getCustomerPaymentReceipt(int accountDocumentID)
        {
            CustomerPaymentReceipt ret = Accounting.GetAccountDocument(accountDocumentID, true) as CustomerPaymentReceipt;
            if (ret == null)
                return ret;
            ret.billItems=new List<BillItem>();
            foreach (int docID in ret.settledBills)
            {
                foreach (BillItem bi in getCustomerBillItems(docID))
                {
                    ret.billItems.Add(bi);
                }
            }
            return ret;
        }
        // BDE exposed
        public CustomerPaymentReceipt[] searchCustomerReceipts(string query, SubscriberSearchField field, int casherAccountID, int resultSize)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                if (field == SubscriberSearchField.ConstactNo)
                {
                    Subscription s= GetSubscription(query,DateTime.Now.Ticks);
                    if (s == null)
                        return new CustomerPaymentReceipt[0];
                    query = GetSubscriber(s.subscriberID).customerCode;
                }
                string sql = @"SELECT distinct  {2} b.paymentDocumentID
                                FROM {0}.[dbo].[CustomerBill] b inner join 
                                {0}.[dbo].Subscriber s on b.customerID=s.id where customerCode='{1}' and b.paymentDocumentID<>-1";
                int[] rids = dspReader.GetColumnArray<int>(string.Format(sql, this.DBName, query,resultSize==-1?"":"Top "+resultSize));
                CustomerPaymentReceipt[] ret = new CustomerPaymentReceipt[rids.Length];
                for (int i = 0; i < rids.Length; i++)
                {
                    ret[i] = Accounting.GetAccountDocument(rids[i], true) as CustomerPaymentReceipt;
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public BillingRuleSettingType[] getSettingTypes()
        {
            verifyBillingSubsystem();
            return _billingRule.getSettingTypes();
        }
        // BDE exposed
        public void setBillingRuleSetting(int AID,Type t, object setting)
        {
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    int count = (int)dspWriter.ExecuteScalar(string.Format("Select count(*) from {0}.dbo.BillingRuleSetting where settingType='{1}'", this.DBName, t.ToString()));
                    if (count == 0)
                    {
                        dspWriter.Insert(this.DBName, "BillingRuleSetting", new string[] { "settingType", "settingValue" }
                            , new object[] { t.ToString(), setting==null?null:SerializationExtensions.getXml(setting) });
                    }
                    else
                    {
                        dspWriter.Update(this.DBName, "BillingRuleSetting", new string[] {"settingValue" }
                            , new object[] { setting == null ? null : SerializationExtensions.getXml(setting) }
                            ,"settingType='"+t+"'");
                    }
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public object getBillingRuleSetting(Type t)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                string val = dspReader.ExecuteScalar(string.Format("Select settingValue from {0}.dbo.BillingRuleSetting where settingType='{1}'", this.DBName, t.ToString())) as string;
                if (string.IsNullOrEmpty(val))
                    return null;
                return SerializationExtensions.loadFromXml(t, val);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }


        // BDE exposed
        public PrePaymentBill PreviewPrepaidReceipt(DateTime date, int subscriptionID, double cubicMeter)
        {
            SQLHelper helper = base.GetReaderHelper();
            try
            {
                date = DateTime.Now;
                Subscription sub = this.GetSubscription(subscriptionID,date.Ticks);
                if (sub.subscriptionStatus != SubscriptionStatus.Active)
                {
                    throw new Exception("You can't sell water to an inactive subscription");
                }
                if (sub == null)
                {
                    throw new Exception("subscription not found ID:" + subscriptionID);
                }
                Subscription[] subscriptionHistory = this.GetSubscriptionHistory(subscriptionID);
                if (subscriptionHistory.Length == 0)
                {
                    throw new Exception("Database inconsistency registation history entry found for subscription no:" + sub.contractNo);
                }
                Subscription prevSubsc = subscriptionHistory[subscriptionHistory.Length - 1];
                bool prevActive =prevSubsc.subscriptionStatus==SubscriptionStatus.Active;
                
                List<BillPeriod> activePeriods = new List<BillPeriod>();
                for(int i=subscriptionHistory.Length-2;i>=0;i--)
                {
                    Subscription thisSubsc= subscriptionHistory[i];
                    bool thisActive = thisSubsc.subscriptionStatus == SubscriptionStatus.Active;
                    if (!prevActive)
                        AddToActivePeriods(helper, new DateTime(prevSubsc.ticksFrom), new DateTime(thisSubsc.ticksFrom), activePeriods);
                    prevActive = thisActive;
                }
                if(prevActive)
                    AddToActivePeriods(helper, new DateTime(prevSubsc.ticksFrom), date, activePeriods);
                BillPeriod p = AccountingPeriodHelper.GetPeriodForDate<BillPeriod>(helper, "BillPeriod", date);
                if (p == null)
                {
                    throw new Exception("Billing period not found in database");
                }
                int documentTypeID = Accounting.GetDocumentTypeByType(typeof(PrePaymentBill)).id;
                int waterBillDocumentTypeID = Accounting.GetDocumentTypeByType(typeof(WaterBillDocument)).id;
                CustomerBillDocument[] existingData = this.getBillDocuments(documentTypeID,sub.subscriberID,sub.id,-1,false);
                List<BillPeriod> unpaidPeriods = new List<BillPeriod>();
                CustomerBillDocument[] waterBills = this.getBillDocuments(waterBillDocumentTypeID, sub.subscriberID, sub.id, -1, false);
                foreach (BillPeriod activePeriod in activePeriods)
                {
                    bool paid = false;
                    foreach (PrePaymentBill data in existingData)
                    {
                        foreach (BillPeriod period in data.rentPeriods)
                        {
                            if (period.id == activePeriod.id)
                            {
                                paid = true;
                                break;
                            }
                        }
                        if (paid)
                        {
                            break;
                        }
                    }
                    if (!paid)
                    {
                        foreach (WaterBillDocument data in waterBills)
                        {
                            if (data.period.id == activePeriod.id)
                            {
                                paid = true;
                                break;
                            }
                        }
                    }
                    if (!paid)
                    {
                        unpaidPeriods.Add(activePeriod);
                    }
                }
                
                PrePaymentBill ret = new PrePaymentBill();
                SingeRateSetting singleRate = this.GetSingleRate(sub, p);
                if (singleRate == null)
                {
                    throw new Exception("Invalid rate configuration");
                }
                ret.paymentAmount = singleRate.Evaluate(cubicMeter);
                ret.rents = new double[unpaidPeriods.Count];
                ret.rentPeriods = unpaidPeriods.ToArray();
                ret.cubicMeters = cubicMeter;
                ret.connection = sub;
                ret.useIncomeAccountID=this.SysPars.incomeAccount;
                ret.rentIncomeAccountID = this.SysPars.rentIncomeAccount;
                ret.customer = GetSubscriber(sub.subscriberID);
                ret.ShortDescription = "Prepayment for contract no " + ret.connection.contractNo;
                int index = 0;
                double paymentAmount = ret.paymentAmount;
                
                foreach (BillPeriod period4 in unpaidPeriods)
                {
                    singleRate = this.GetSingleRate(sub, period4);
                    if (singleRate == null)
                    {
                        ret.rents[index] = 0;
                        //throw new Exception("Invalid rate configuration");
                    }
                    else
                        ret.rents[index] = singleRate.EvaluateRent(GetSubscription(sub.id,period4.toDate.Ticks).itemCode);
                    paymentAmount += ret.rents[index];
                    index++;
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(helper);
            }
            
        }
        // BDE exposed
        public CustomerPaymentReceipt GeneratePrepaidReceipt(int AID, DateTime date, int paymentCenterID, int subscriptionID, double cubicMeter, PaymentInstrumentItem[] paymentInstruments, out PrePaymentBill bill)
        {
            lock (base.dspWriter)
            {
                PaymentCenter paymentCenter = this.GetPaymentCenter(paymentCenterID);
                int batchID = paymentCenter.GetBatchID(this.Accounting.GetDocumentTypeByType(typeof(CustomerPaymentReceipt)).id);
                if (this.Accounting.GetCostCenterAccount(paymentCenter.casherAccountID) == null)
                {
                    throw new Exception("The account ID of the payment center is not valid");
                }
                bill = this.PreviewPrepaidReceipt(date, subscriptionID, cubicMeter);
                bill.draft = false;
                try
                {
                    base.dspWriter.BeginTransaction();
                    CustomerPaymentReceipt ret;
                    
                    bill.AccountDocumentID=addBill(AID, bill, bill.itemsLessExemption.ToArray(),bill.connection.id);
                    ret = new CustomerPaymentReceipt();
                    ret.paymentInstruments = new PaymentInstrumentItem[] { new PaymentInstrumentItem(bdeBERP.SysPars.cashInstrumentCode, bill.total) };
                    ret.assetAccountID = paymentCenter.casherAccountID;
                    ret.customer = GetSubscriber(bill.connection.subscriberID);
                    ret.settledBills = new int[] { bill.AccountDocumentID };
                    ret.ShortDescription = "Prepayment for contract no " + bill.connection.contractNo;
                    ret.AccountDocumentID = Accounting.PostGenericDocument(AID, ret);
                    base.dspWriter.CommitTransaction();
                    return ret;
                }
                catch (Exception exception)
                {
                    base.dspWriter.RollBackTransaction();
                    throw new Exception("Error posting prepaid payment", exception);
                }
            }
            
        }
        // BDE exposed
        public BillingCycle getCurrentBillingCycle()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BillingCycle[] ret = dspReader.GetSTRArrayByFilter < BillingCycle>("status=" + (int)BillingCycleStatus.Open);
                if (ret.Length == 0)
                    return null;
                if (ret.Length > 1)
                    throw new ServerUserMessage("Database inconsistency: multiple open billing cycles found");
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public BillingCycle getBillingCycle(int periodID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                BillingCycle[] ret = dspReader.GetSTRArrayByFilter<BillingCycle>("periodID=" + periodID);
                if (ret.Length == 0)
                    return null;
                if (ret.Length > 1)
                    throw new ServerUserMessage("Database inconsistency: multiple open billing cycles found");
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        void assertValidReadingCycle(BillPeriod period, BillingCycle cycle)
        {
            if (cycle.collectionEndTime.Subtract(cycle.collectionStartTime).TotalDays < 2)
                throw new ServerUserMessage("Invalid collection period");
        }
        // BDE exposed
        public void createBillingCycle(int AID, BillingCycle cycle)
        {
            BillPeriod period = GetBillPeriod(cycle.periodID);
            if (period == null)
                throw new ServerUserMessage("Invalid period ID");
            BillingCycle existing = getBillingCycle(cycle.periodID);
            if (existing != null)
                throw new ServerUserMessage("There is already a billing cycle defined for period " + period.name);
            if(cycle.status==BillingCycleStatus.Open)
                if(getCurrentBillingCycle()!=null)
                    throw new ServerUserMessage("There is already an open cycle");
            assertValidReadingCycle(period, cycle);
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.InsertSingleTableRecord(this.DBName, cycle,new string[]{"__AID"},new object[]{AID});
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }
        }
        // BDE exposed
        public void deleteBillingCycle(int AID, int periodID)
        {
            BillPeriod period = GetBillPeriod(periodID);
            if (period == null)
                throw new ServerUserMessage("Invalid period ID");
            BillingCycle existing = getBillingCycle(periodID);
            if (existing == null)
                throw new ServerUserMessage("No billing cycle defined for period " + period.name);
            if (existing.status == BillingCycleStatus.Open)
            {
                throw new ServerUserMessage("Open billing cycle can't be deleted");
            }
            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.BillingCycle", "periodID=" + periodID);
                    dspWriter.DeleteSingleTableRecrod<BillingCycle>(this.DBName, periodID);
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }

        }
        // BDE exposed
        public void updateBillingCycle(int AID, BillingCycle cycle)
        {
            BillPeriod period = GetBillPeriod(cycle.periodID);
            if (period == null)
                throw new ServerUserMessage("Invalid period ID");
            BillingCycle existing = getBillingCycle(cycle.periodID);
            if (existing == null)
                throw new ServerUserMessage("No billing cycle defined for period " + period.name);
            assertValidReadingCycle(period, cycle);
            if (cycle.status == BillingCycleStatus.Open)
            {
                existing = getCurrentBillingCycle();
                if (existing != null && existing.periodID != cycle.periodID)
                    throw new ServerUserMessage("There is already an open cycle");
            }

            lock (dspWriter)
            {
                dspWriter.BeginTransaction();
                try
                {
                    dspWriter.logDeletedData(AID, this.DBName + ".dbo.BillingCycle", "periodID=" + cycle.periodID);
                    dspWriter.UpdateSingleTableRecord(this.DBName, cycle, new string[] { "__AID" }, new object[] { AID });
                    dspWriter.CommitTransaction();
                }
                catch
                {
                    dspWriter.RollBackTransaction();
                    throw;
                }
            }

        }
        // BDE exposed
        public BillingCycle[] getAllBillingCycles()
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                return dspReader.GetSTRArrayByFilter<BillingCycle>(null);
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        // BDE exposed
        public string[] getReadingCustomFields()
        {
            if (_readingAdapter == null)
                return new string[0];
            return _readingAdapter.getCustomFields();
        }


        public DistrictMeteringZone[] getAllDMAs()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<DistrictMeteringZone>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public PressureZone[] getAllPressureZones()
        {
            INTAPS.RDBMS.SQLHelper reader = base.GetReaderHelper();
            try
            {
                return reader.GetSTRArrayByFilter<PressureZone>(null);
            }
            finally
            {
                base.ReleaseHelper(reader);
            }
        }

        public DistrictMeteringZone getDMA(int id)
        {
            return base.getSingleObjectIntID<DistrictMeteringZone>(id);
        }

        public PressureZone getPressureZone(int id)
        {
            return base.getSingleObjectIntID<PressureZone>(id);
        }

        public ReadingSheetRecord[] getReadingShowRows2(String userID,int periodID, int rowIndex, int n)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                ReadingSheetRecord[] ret = dspReader.GetSTRArrayByFilter<ReadingSheetRecord>("periodID={0} and orderN>={1} and orderN<{2}".format(periodID, rowIndex, rowIndex + n));
                bool canPay = false;
                if (this.SysPars.enable_mobile_payment)
                {
                    PaymentCenter pc = this.GetPaymentCenter(userID);
                    if (pc != null && pc.collectionMethod == (CollectionMethod.Mobile | CollectionMethod.Offline))
                        canPay = true;
                } 
                foreach(ReadingSheetRecord r in ret)
                {
                    r.readTime = ReadingSheetRecord.toJavaTicks(r.readTime_date);
                    if (string.IsNullOrEmpty(r.bill_json))
                        r.bill = new ReadingSheetRecord.MRBill();
                    else
                        r.bill = Newtonsoft.Json.JsonConvert.DeserializeObject<ReadingSheetRecord.MRBill>(r.bill_json);
                    r.customFields = string.IsNullOrEmpty(r.customFields_json) ? null :
                        Newtonsoft.Json.JsonConvert.DeserializeObject<String[]>(r.customFields_json);
                    r.canPay = canPay && r.validPhoneNo();
                    r.billTotal = r.bill==null?0:r.bill.totalAmount();
                }
                return ret;
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }

        public TransactionItems[] getMeterItems()
        {
            return bdeBERP.GetItemsInCategory(this.m_sysPars.meterMaterialCategory);
        }

        DataUpdate.DataUpdateRequest getDataUpdateRequest(int periodID,int connection,String userID)
        {
            INTAPS.RDBMS.SQLHelper dspReader = base.GetReaderHelper();
            try
            {
                DataUpdate.DataUpdateRequest[] ret = dspReader.GetSTRArrayByFilter < DataUpdate.DataUpdateRequest>("periodID={0} and connectionID={1} and userID='{2}'".format(periodID, connection, userID));
                if (ret.Length == 0)
                    return null;
                return ret[0];
            }
            finally
            {
                base.ReleaseHelper(dspReader);
            }
        }
        public void savedDataUpdateRequest(int AID, String userName, int conID, int data_class, DataUpdate update)
        {
            ReadingCycle cycle = this.getCurrentReadingCycle();
            if (cycle == null)
                throw new ServerUserMessage("Data can't be updated becuase there is no open reading cycle now.");
            DataUpdate.DataUpdateRequest request=getDataUpdateRequest(cycle.periodID, conID, userName);
            if (request == null)
            {
                request = new DataUpdate.DataUpdateRequest();
                request.periodID=cycle.periodID;
                request.connectionID=conID;
                request.userID=userName;
                request.dataClass=(DataUpdate.DataClass)data_class;
                request.updateData=Newtonsoft.Json.JsonConvert.SerializeObject(update);
                request.status=DataUpdate.RequestStatus.Requested;
                dspWriter.InsertSingleTableRecord(this.DBName, request, new string[] {  "__AID" }, new object[] { AID });
            }
            else
            {
                if (request.status != DataUpdate.RequestStatus.Requested)
                    throw new ServerUserMessage("You request for updating the data for this connection for this month is already processed. Send the request next  month");
                request.updateData = Newtonsoft.Json.JsonConvert.SerializeObject(update);
                dspWriter.logDeletedData(AID, this.DBName + ".dbo.DataUpdateRequest", "periodID={0} and connectionID={1} and userID='{1}'".format(cycle.periodID,conID,userName));
                dspWriter.UpdateSingleTableRecord(this.DBName, request, new string[] { "__AID" }, new object[] { AID });
            }
        }

        
    }
}